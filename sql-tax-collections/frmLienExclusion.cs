﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienExclusion.
	/// </summary>
	public partial class frmLienExclusion : BaseForm
	{
		public frmLienExclusion()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienExclusion InstancePtr
		{
			get
			{
				return (frmLienExclusion)Sys.GetInstance(typeof(frmLienExclusion));
			}
		}

		protected frmLienExclusion _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/18/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/18/2006              *
		// ********************************************************
		int lngAction;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		clsDRWrapper rsValidate = new clsDRWrapper();
		bool boolLoaded;
		int lngRK;
		bool boolDONOTACTIVATE;
		int lngColCheck;
		int lngColAccount;
		int lngColName;
		int lngColLocation;
		int lngColBillKey;
		int lngColOrigCheck;

		public void Init(int lngType, ref int lngRateRecNumber)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngAction = lngType;
				lngRK = lngRateRecNumber;
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Form");
			}
		}

		private void frmLienExclusion_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				FormatGrid(true);
				ShowGridFrame();
				SetAct();
				boolLoaded = true;
				if (!boolDONOTACTIVATE)
				{
					if (!FillDemandGrid())
					{
						boolDONOTACTIVATE = true;
						Close();
					}
					else
					{
						boolDONOTACTIVATE = false;
					}
				}
				else
				{
					// MsgBox "Stop"
				}
			}
		}

		private void frmLienExclusion_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienExclusion.Icon	= "frmLienExclusion.frx":0000";
			//frmLienExclusion.FillStyle	= 0;
			//frmLienExclusion.ScaleWidth	= 9045;
			//frmLienExclusion.ScaleHeight	= 7395;
			//frmLienExclusion.LinkTopic	= "Form2";
			//frmLienExclusion.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15266;
			//vsDemand._cy	= 9320;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 0;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 1;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.DataMode	= 0;
			//vsDemand.VirtualData	= -1  'True;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmLienExclusion.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngColCheck = 0;
			lngColOrigCheck = 1;
			lngColAccount = 2;
			lngColName = 3;
			lngColLocation = 5;
			lngColBillKey = 4;
			boolLoaded = false;
		}

		private void frmLienExclusion_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolDONOTACTIVATE = false;
		}

		private void frmLienExclusion_Resize(object sender, System.EventArgs e)
		{
			ShowGridFrame();

		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			// this will clear all account checkboxes in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
		}

		private void FormatGrid(bool boolReset)
		{
			FormatGrid(ref boolReset);
		}

		private void FormatGrid(ref bool boolReset)
		{
			int wid = 0;
			vsDemand.Cols = 6;
			if (boolReset)
			{
				vsDemand.Rows = 1;
			}
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngColCheck, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(lngColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(lngColOrigCheck, 0);
			// Hidden check field
			vsDemand.ColWidth(lngColBillKey, 0);
			// Hidden Bill Key field
			vsDemand.ColWidth(lngColLocation, FCConvert.ToInt32(wid * 0.3));
			// Location
			vsDemand.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColDataType(lngColOrigCheck, FCGrid.DataTypeSettings.flexDTBoolean);
            //FC:FINAL:AM:#1988 - set the datatype
            vsDemand.ColDataType(lngColAccount, FCGrid.DataTypeSettings.flexDTLong);
            vsDemand.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColBillKey, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.TextMatrix(0, lngColAccount, "Account");
			vsDemand.TextMatrix(0, lngColName, "Name");
			vsDemand.TextMatrix(0, lngColLocation, "Location");
		}

		private bool FillDemandGrid()
		{
			bool FillDemandGrid = false;
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL = "";
				int lngIndex/*unused?*/;
				clsDRWrapper rsMaster = new clsDRWrapper();
				double dblXInt = 0;
				FillDemandGrid = true;
				rsMaster.OpenRecordset("SELECT RSAccount, RSCard, InBankruptcy FROM Master", modExtraModules.strREDatabase);
				lngError = 1;
				vsDemand.Rows = 1;
				lngError = 2;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				lngError = 3;
				switch (lngAction)
				{
					case 1:
						{
							// 30dn
							if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 0)
							{
								strSQL = "SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND RateKey = " + FCConvert.ToString(lngRK) + " AND (LienProcessStatus = 0 OR LienProcessStatus = 1) ORDER BY Name1, Account";
								// this will be all the records that have had Lien Maturity Notices
							}
							else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
							{
								strSQL = "SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND RateKey = " + FCConvert.ToString(lngRK) + " AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND Name1 > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY Name1, Account";
							}
							else
							{
								strSQL = "SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND RateKey = " + FCConvert.ToString(lngRK) + " AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND Account >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND Account <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY Name1, Account";
							}
							break;
						}
					case 2:
						{
							// lien
							if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 0)
							{
								strSQL = "SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND RateKey = " + FCConvert.ToString(lngRK) + " AND (LienProcessStatus = 2 OR LienProcessStatus = 3) ORDER BY Name1, Account";
								// this will be all the records that have had Lien Maturity Notices
							}
							else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
							{
								strSQL = "SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND RateKey = " + FCConvert.ToString(lngRK) + " AND (LienProcessStatus = 2 OR LienProcessStatus = 3) AND Name1 > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY Name1, Account";
							}
							else
							{
								strSQL = "SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND RateKey = " + FCConvert.ToString(lngRK) + " AND (LienProcessStatus = 2 OR LienProcessStatus = 3) AND Account >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND Account <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY Name1, Account";
							}
							break;
						}
					case 3:
						{
							// lien mat
							if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 0)
							{
								strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE LienRec.RateKey = " + FCConvert.ToString(lngRK) + " AND (LienStatusEligibility = 4 OR LienStatusEligibility = 5) ORDER BY Name1, Account";
								// this will be all the records that have had Lien Maturity Notices
							}
							else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
							{
								strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE LienRec.RateKey = " + FCConvert.ToString(lngRK) + " AND (LienStatusEligibility = 4 OR LienStatusEligibility = 5) AND Name1 > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY Name1, Account";
							}
							else
							{
								strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE LienRec.RateKey = " + FCConvert.ToString(lngRK) + " AND (LienStatusEligibility = 4 OR LienStatusEligibility = 5) AND Account >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND Account <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY Name1, Account";
							}
							rsLData.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
							// this is a recordset of all the lien records
							break;
						}
				}
				//end switch
				lngError = 4;
				// strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 5 AND LienStatusEligibility = 5 ORDER BY Name1"      'this will be all the records that have had 30 Day Notices printed
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					App.DoEvents();
					FillDemandGrid = false;
					FCMessageBox.Show("There are no accounts eligible.", MsgBoxStyle.Information, "No Eligible Accounts - " + FCConvert.ToString(lngRK));
					return FillDemandGrid;
				}
				lngError = 5;
				while (!rsData.EndOfFile())
				{
					App.DoEvents();
					// find the first lien record
					TryAgain:
					;
					lngError = 11;
					switch (lngAction)
					{
						case 1:
						case 2:
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblXInt) <= 0)
								{
									// skip this account
									goto SKIP30DN;
								}
								// add a row/element
								vsDemand.AddItem("");
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCheck, rsData.Get_Fields_Boolean("LienProcessExclusion"));
								// flag
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColOrigCheck, rsData.Get_Fields_Boolean("LienProcessExclusion"));
								// original flag setting
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, rsData.Get_Fields("Account"));
								// account number
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, rsData.Get_Fields_String("Name1"));
								// name
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, rsData.Get_Fields_Int32("ID"));
								// billkey   'kk08112014 trocls-49  'rsData.Get_Fields("BillKey")
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(rsData.Get_Fields("StreetNumber")) != 0)
								{
									// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColLocation, rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
								}
								else
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColLocation, rsData.Get_Fields_String("StreetName"));
								}
								SKIP30DN:
								;
								rsData.MoveNext();
								break;
							}
						case 3:
							{
								rsLData.FindFirstRecord("LienRecordNumber", rsData.Get_Fields_Int32("LienRecordNumber"));
								if (rsLData.NoMatch)
								{
									// if there is no match, then report the error
									lngError = 12;
									frmWait.InstancePtr.Unload();
									FCMessageBox.Show("Cannot find Lien Record #" + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + ".", MsgBoxStyle.Critical, "Missing Lien Record");
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
									if (Conversion.Val(rsLData.Get_Fields("Principal")) - Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")) <= 0)
									{
										rsData.MoveNext();
										if (rsData.EndOfFile())
											break;
										goto TryAgain;
									}
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									rsMaster.FindFirstRecord2("RSAccount,RSCard", FCConvert.ToString(rsData.Get_Fields("Account")) + ",1", ",");
									if (rsMaster.NoMatch)
									{
										lngError = 12;
										frmWait.InstancePtr.Unload();
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										FCMessageBox.Show("Cannot find Master Record for account #" + FCConvert.ToString(rsData.Get_Fields("Account")) + ".", MsgBoxStyle.Critical, "Missing Master Record");
										rsData.MoveNext();
									}
									else
									{
										if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("InBankruptcy")))
										{
											// this account is not eligible for LMFs
											rsData.MoveNext();
										}
										else
										{
											if (modCLCalculations.CalculateAccountCLLien(rsData, DateTime.Today, ref dblXInt, true) <= 0)
											{
												goto SKIPLDN;
											}
											lngError = 13;
											vsDemand.AddItem("");
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCheck, rsData.Get_Fields_Boolean("LienProcessExclusion"));
											// flag
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColOrigCheck, rsData.Get_Fields_Boolean("LienProcessExclusion"));
											// original flag setting
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, rsData.Get_Fields("Account"));
											// account number
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, rsData.Get_Fields_String("Name1"));
											// name
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, rsData.Get_Fields_Int32("ID"));
											// billkey   'kk08112014 trocls-49  'rsData.Get_Fields("BillKey")
											// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
											if (Conversion.Val(rsData.Get_Fields("StreetNumber")) != 0)
											{
												// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
												vsDemand.TextMatrix(vsDemand.Rows - 1, lngColLocation, rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
											}
											else
											{
												vsDemand.TextMatrix(vsDemand.Rows - 1, lngColLocation, rsData.Get_Fields_String("StreetName"));
											}
											lngError = 14;
											SKIPLDN:
											;
											rsData.MoveNext();
										}
									}
								}
								break;
							}
					}
					//end switch
				}
				lngError = 9;
				// this sets the height of the grid
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
				//{
				//	vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				lngError = 11;
				frmWait.InstancePtr.Unload();
				return FillDemandGrid;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FillDemandGrid = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid - " + FCConvert.ToString(lngError));
			}
			return FillDemandGrid;
		}

		private void SetAct()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will change all of the menu options
				switch (lngAction)
				{
					case 1:
						{
							// 30 DN validation
							this.Text = "30 Day Notice Exclusion";
							lblInstruction.Text = "To exclude accounts from 30 Day Notices:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Save and Continue' or press F12.";
							break;
						}
					case 2:
						{
							// Lien Notice
							this.Text = "Lien Notice Exclusion";
							lblInstruction.Text = "To exclude accounts from Lien Notices:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Save and Continue' or press F12.";
							break;
						}
					case 3:
						{
							// Lien Mat notice
							this.Text = "Lien Maturity Notice Exclusion";
							lblInstruction.Text = "To exclude accounts from Lien Maturity Notices:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Save and Continue' or press F12.";
							break;
						}
				}
				this.HeaderText.Text = this.Text;
				//end switch
				ShowGridFrame();
				cmdFileSelectAll.Visible = true;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Set Action");
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (ExcludeBills())
			{
				Close();
			}
		}

		private bool ExcludeBills()
		{
			bool ExcludeBills = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				ExcludeBills = true;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Bills");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					App.DoEvents();
					if (vsDemand.TextMatrix(lngCT, lngColCheck) != vsDemand.TextMatrix(lngCT, lngColOrigCheck))
					{
						rsData.FindFirstRecord("ID", Conversion.Val(vsDemand.TextMatrix(lngCT, lngColBillKey)));
						if (!rsData.NoMatch)
						{
							lngDemandCount += 1;
							rsData.Edit();
							//FC:FINAL:AM:#73 - set the boolean value
							//rsData.Set_Fields("LienProcessExclusion", vsDemand.TextMatrix(lngCT, lngColCheck));
							rsData.Set_Fields("LienProcessExclusion", FCConvert.ToBoolean(vsDemand.TextMatrix(lngCT, lngColCheck)) == true);
							rsData.Update();
						}
					}
				}
				frmWait.InstancePtr.Unload();
				if (lngDemandCount == 1)
				{
					FCMessageBox.Show("1 bill was affected.", MsgBoxStyle.Information, "Exclusion List Updated");
				}
				else
				{
					FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " bills were affected.", MsgBoxStyle.Information, "Exclusion List Updated");
				}
				return ExcludeBills;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Excluding Bills");
			}
			return ExcludeBills;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		private void vsDemand_Click(object sender, EventArgs e)
		{
			if (vsDemand.Col == lngColCheck)
			{
				//FC:FINAL:BCU #i171 - use FCConvert.CBool instead
				//if (FCConvert.ToBoolean(vsDemand.TextMatrix(vsDemand.Row, lngColCheck)) == true)
				if (FCConvert.CBool(vsDemand.TextMatrix(vsDemand.Row, lngColCheck)) == true)
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheck, FCConvert.ToString(false));
				}
				else
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheck, FCConvert.ToString(true));
				}
			}
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			if (vsDemand.MouseCol == 0 && vsDemand.MouseRow == 0)
			{
				cmdFileSelectAll_Click(cmdFileSelectAll, EventArgs.Empty);
			}
		}

		private void vsDemand_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
		{
			// Dim lngMR           As Long
			// Dim lngMC           As Long
			// 
			// lngMR = vsDemand.MouseRow
			// lngMC = vsDemand.MouseCol
			// If lngMR > 0 And lngMC > 0 Then
			// If Val(vsDemand.TextMatrix(lngMR, 6)) = -1 Then
			// vsDemand.ToolTipText = "A Lien Record has already been created for this account.  This account is not eligible."
			// Else
			// vsDemand.ToolTipText = ""
			// End If
			// End If
		}

		private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
