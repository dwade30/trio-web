﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxRateReport.
	/// </summary>
	public partial class frmTaxRateReport : BaseForm
	{
		public frmTaxRateReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxRateReport InstancePtr
		{
			get
			{
				return (frmTaxRateReport)Sys.GetInstance(typeof(frmTaxRateReport));
			}
		}

		protected frmTaxRateReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/28/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/28/2005              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		bool boolDirty;
		bool boolLoaded;
		int intType;

		public void Init()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Form");
			}
		}

		private void frmTaxRateReport_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				LoadYearLists();
			}
		}

		private void frmTaxRateReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmTaxRateReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxRateReport.Icon	= "frmTaxRateReport.frx":0000";
			//frmTaxRateReport.FillStyle	= 0;
			//frmTaxRateReport.ScaleWidth	= 4530;
			//frmTaxRateReport.ScaleHeight	= 2475;
			//frmTaxRateReport.LinkTopic	= "Form2";
			//frmTaxRateReport.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmTaxRateReport.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
		}

		private void frmTaxRateReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (ShowReport())
			{
				Close();
			}
		}

		private bool ShowReport()
		{
			bool ShowReport = false;
			string strStartYear;
			string strEndYear;
			strStartYear = cboStartYear.Text;
			strEndYear = cboEndYear.Text;
			if (strStartYear != "" && strEndYear != "")
			{
				rptTaxRateReport.InstancePtr.Init(strStartYear, strEndYear);
				ShowReport = true;
			}
			else
			{
				ShowReport = false;
				if (strStartYear == "" && strEndYear != "")
				{
					FCMessageBox.Show("Please enter a valid Start Year.", MsgBoxStyle.Exclamation, "Invalid Start Year");
				}
				else if (strStartYear != "" && strEndYear == "")
				{
					FCMessageBox.Show("Please enter a valid End Year.", MsgBoxStyle.Exclamation, "Invalid End Year");
				}
				else
				{
					FCMessageBox.Show("Please enter a valid Start and End Year.", MsgBoxStyle.Exclamation, "Invalid Years");
				}
			}
			return ShowReport;
		}

		private void LoadYearLists()
		{
			clsDRWrapper rsRate = new clsDRWrapper();
			rsRate.OpenRecordset("SELECT DISTINCT [Year] FROM RateRec ORDER BY [Year]", modExtraModules.strCLDatabase);
			if (rsRate.RecordCount() > 0)
			{
				rsRate.MoveFirst();
				// Start Combo
				while (!rsRate.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					cboStartYear.AddItem(FCConvert.ToString(rsRate.Get_Fields("Year")));
					rsRate.MoveNext();
				}
				// End Combo
				rsRate.MoveFirst();
				while (!rsRate.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					cboEndYear.AddItem(FCConvert.ToString(rsRate.Get_Fields("Year")));
					rsRate.MoveNext();
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}
	}
}
