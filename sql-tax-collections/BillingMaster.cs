﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TWCL0000
{

    [Table("BillingMaster")]
    public partial class BillingMaster
    {
        public int ID { get; set; }

        public int? Account { get; set; }

        [StringLength(2)]
        public string BillingType { get; set; }

        public int? BillingYear { get; set; }

        [StringLength(255)]
        public string Name1 { get; set; }

        [StringLength(255)]
        public string Name2 { get; set; }

        [StringLength(255)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(255)]
        public string Address3 { get; set; }

        [StringLength(20)]
        public string MapLot { get; set; }

        public int? streetnumber { get; set; }

        [StringLength(1)]
        public string Apt { get; set; }

        [StringLength(255)]
        public string StreetName { get; set; }

        public int? LandValue { get; set; }

        public int? BuildingValue { get; set; }

        public int? ExemptValue { get; set; }

        public int? TranCode { get; set; }

        public int? LandCode { get; set; }

        public int? BuildingCode { get; set; }

        public int? OtherCode1 { get; set; }

        public int? OtherCode2 { get; set; }

        [Column(TypeName = "money")]
        public decimal? TaxDue1 { get; set; }

        [Column(TypeName = "money")]
        public decimal? TaxDue2 { get; set; }

        [Column(TypeName = "money")]
        public decimal? TaxDue3 { get; set; }

        [Column(TypeName = "money")]
        public decimal? TaxDue4 { get; set; }

        public int? LienRecordNumber { get; set; }

        [Column(TypeName = "money")]
        public decimal? PrincipalPaid { get; set; }

        [Column(TypeName = "money")]
        public decimal? InterestPaid { get; set; }

        [Column(TypeName = "money")]
        public decimal? InterestCharged { get; set; }

        [Column(TypeName = "money")]
        public decimal? DemandFees { get; set; }

        [Column(TypeName = "money")]
        public decimal? DemandFeesPaid { get; set; }

        public DateTime? InterestAppliedThroughDate { get; set; }

        public int? RateKey { get; set; }

        public DateTime? TransferFromBillingDateFirst { get; set; }

        public DateTime? TransferFromBillingDateLast { get; set; }

        [StringLength(1)]
        public string WhetherBilledBefore { get; set; }

        public int? OwnerGroup { get; set; }

        public int? Category1 { get; set; }

        public int? Category2 { get; set; }

        public int? Category3 { get; set; }

        public int? Category4 { get; set; }

        public int? Category5 { get; set; }

        public int? Category6 { get; set; }

        public int? Category7 { get; set; }

        public int? Category8 { get; set; }

        public int? Category9 { get; set; }

        public double? Acres { get; set; }

        public double? HomesteadExemption { get; set; }

        public double? OtherExempt1 { get; set; }

        public double? OtherExempt2 { get; set; }

        [StringLength(255)]
        public string BookPage { get; set; }

        public int? PPAssessment { get; set; }

        public int? Exempt1 { get; set; }

        public int? Exempt2 { get; set; }

        public int? Exempt3 { get; set; }

        public int? LienStatusEligibility { get; set; }

        public int? LienProcessStatus { get; set; }

        public int? Copies { get; set; }

        [StringLength(255)]
        public string CertifiedMailNumber { get; set; }

        public bool? AbatementPaymentMade { get; set; }

        public double? TGMixedAcres { get; set; }

        public double? TGSoftAcres { get; set; }

        public double? TGHardAcres { get; set; }

        public int? TGMixedValue { get; set; }

        public int? TGSoftValue { get; set; }

        public int? TGHardValue { get; set; }

        [StringLength(255)]
        public string Ref2 { get; set; }

        [StringLength(255)]
        public string Ref1 { get; set; }

        [StringLength(50)]
        public string Zip { get; set; }

        public bool? ShowRef1 { get; set; }

        public bool? LienProcessExclusion { get; set; }

        [StringLength(30)]
        public string IMPBTrackingNumber { get; set; }

        [StringLength(255)]
        public string MailingAddress3 { get; set; }
        public bool IsLien()
        {
            return LienRecordNumber > 0;
        }
    }
}
