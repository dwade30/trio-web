//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptTaxClubPaymentsOverdue.
	/// </summary>
	public class rptTaxClubPaymentsOverdue : FCSectionReport
    {

// nObj = 1
//   0	rptTaxClubPaymentsOverdue	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/26/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/15/2006              *
		// ********************************************************

		string strSql;
		short intType;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		int lngTotalAccounts;
		int lngCount;
		clsDRWrapper rsKeepCROpen; // kk03272015 trocl-1135  Stop File Lock errors by keeping CR db open
		bool boolSummaryOnly; // trocl-1367 8.24.17 kjr add number of payment opt
		bool boolAllAccounts;
		bool boolExactPayments;
		short intNumberOfPayments;

		public void Init(ref bool boolPassSummaryOnly = true, ref bool boolPassAllAccounts = true, ref short intPassNumberOfPayments = 0, ref bool boolPassExactPayments = true)
		{
			boolSummaryOnly = boolPassSummaryOnly; // trocl-1367 8.24.17 kjr add number of payments option
			boolAllAccounts = boolPassAllAccounts;
			intNumberOfPayments = intPassNumberOfPayments;
			boolExactPayments = boolPassExactPayments;

			// this sub will set the SQL string
			strSql = BuildSQL();

			rsData.OpenRecordset(strSql, modExtraModules.strCLDatabase);
			rsLData.OpenRecordset("SELECT * FROM LienRec");
			lngTotalAccounts = rsData.RecordCount();

			if (lngTotalAccounts==0) { // do not show the report if there are no records
				MessageBox.Show("There are no accounts eligible.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			} else {
				if (modGlobalConstants.gboolCR) { // kk03272015 trocl-1135  Stop File Lock errors in srptActivityDetail.GetActualReceiptNumber by keeping CR db open
					rsKeepCROpen = new clsDRWrapper();
					rsKeepCROpen.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
				}

				frmReportViewer.InstancePtr.Init(ref this); // Me.Show , MDIParent
			}
		}

		private void ActiveReport_FetchData(ref bool EOF)
		{
			EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, short Shift)
		{
			switch (KeyCode) {
				
				case Keys.Escape:
				{
					Close();
					break;
				}
			} //end switch
		}

		private void ActiveReport_PageStart()
		{
			lblPage.Caption = "Page "+this.pageNumber;
		}

		private void ActiveReport_ReportEnd()
		{
			// save this report for future access
			if (this.Pages.Count>0) {
				modGlobalFunctions.IncrementSavedReports("LastCLActivity");
				this.Pages.Save("RPT\\LastCLActivity1.RDF");
			}
		}

		private void ActiveReport_ReportStart()
		{
			lblDate.Caption = Strings.Format(DateTime.Today, "MM/DD/YYYY");
			lblTime.Caption = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "HH:MM AMPM");
			lblMuniName.Caption = modGlobalConstants.MuniName;

			lngCount = 0; // this is the count of how many accounts were actually processed
			modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			bool boolRetry;
			int lngLRN = 0; // Lien Record Number
			double intPaymentsOverdue = 0;
			// vbPorter upgrade warning: dblPaymentsExpected As double	OnWrite(long)
			double dblPaymentsExpected = 0;
			double dblAmountExpected = 0;
			double dblCalculatedDifference = 0;

		TRYNEXTACCOUNT: ;

			boolRetry = false;
			fldAcct.Visible = false;
			fldName.Visible = false;
			fldAmountExpected.Visible = false;
			fldTotalPaid.Visible = false;
			fldPaymentsOverdue.Visible = false;
			fldOwed.Visible = false;
			srptTaxClubActivityDetail.Object = null;
			srptTaxClubActivityDetail.Visible = false;
			lblReceiptDate.Visible = false;
			lblPaymentCode.Visible = false;
			lblPaymentRef.Visible = false;
			lblPaymentRecipt.Visible = false;
			lblPaymentAmount.Visible = false;

			if (!rsData.EndOfFile()) {
				lngLRN = Convert.ToInt32(rsData.Get_Fields("LienRecordNumber"));

				// set the lien record if needed
				if (lngLRN==0) {
					// this is a regular bill

				} else {
					// this is a lien
					rsLData.FindFirstRecord("LienRecordNumber", lngLRN);
				}

				// check to see if this account is elegible to be shown and
				// the principal bill has been not been completely paid
				if (lngLRN==0) {
					// this is a regular bill
					if ((rsData.Get_Fields("TaxDue1")+rsData.Get_Fields("TaxDue2")+rsData.Get_Fields("TaxDue3")+rsData.Get_Fields("TaxDue4"))>rsData.Get_Fields("PrincipalPaid")) {
						// show this account
						boolRetry = false;
					} else {
						// do not show this account
						boolRetry = true;
					}
				} else {
					// this is a lien
					if (rsLData.Get_Fields("Principal")>rsLData.Get_Fields("PrincipalPaid")) {
						// show this account
						boolRetry = false;
					} else {
						// do not show this account
						boolRetry = true;
					}
				}

				if (!boolRetry && !boolAllAccounts) {
					DateTime dtTemp;
					// dtTemp = CDate("December 15,2016") '''''''''''''''''''''''''''''''''''''''''''''''TAKE THIS OUT''''''''''''''''''''''''''
					dblPaymentsExpected = fecherFoundation.DateAndTime.DateDiff("m", (DateTime)rsData.Get_Fields("AgreementDate"), DateTime.Now, FirstDayOfWeek.System, FirstWeekOfYear.System);
					dblAmountExpected = modGNBas.Round(dblPaymentsExpected*rsData.Get_Fields("PaymentAmount"), 2);
					dblCalculatedDifference = modGNBas.Round(dblAmountExpected-rsData.Get_Fields("TotalPaid"), 2);
					if (dblCalculatedDifference<=0) {
						// do not show this account
						boolRetry = true;
					} else {
						intPaymentsOverdue = FCUtils.Fix(dblCalculatedDifference/rsData.Get_Fields("PaymentAmount"));
						if (boolExactPayments && intPaymentsOverdue!=intNumberOfPayments) {
							// (not boolexactpayments and intPaymentsOverdue < intNumberOfPayments) Then
							// do not show this account
							boolRetry = true;
						} else {
							if (!boolExactPayments && intPaymentsOverdue<intNumberOfPayments) {
								// do not show this account
								boolRetry = true;
							} else {
								boolRetry = false;
							}
						}
					}
				}

				if (boolRetry) {
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				} else {
					lngCount += 1;
				}

				fldAcct.Visible = true;
				fldName.Visible = true;
				fldAmountExpected.Visible = true;
				fldTotalPaid.Visible = true;
				fldPaymentsOverdue.Visible = true;
				fldOwed.Visible = true;
				fldAcct.Text = rsData.Get_Fields("Account");
				fldName.Text = fecherFoundation.Strings.Trim(Convert.ToString(rsData.Get_Fields("Name1")));
				fldAmountExpected.Text = Strings.Format(dblAmountExpected, "#,##0.00");
				fldTotalPaid.Text = Strings.Format(rsData.Get_Fields("TotalPaid"), "#,##0.00");
				fldPaymentsOverdue = intPaymentsOverdue;

				if (!boolSummaryOnly) {
					srptTaxClubActivityDetail.Visible = true;
					lblReceiptDate.Visible = true;
					lblPaymentCode.Visible = true;
					lblPaymentRef.Visible = true;
					lblPaymentRecipt.Visible = true;
					lblPaymentAmount.Visible = true;
				}

				if (lngLRN==0) {
					fldOwed.Text = Strings.Format((rsData.Get_Fields("TaxDue1")+rsData.Get_Fields("TaxDue2")+rsData.Get_Fields("TaxDue3")+rsData.Get_Fields("TaxDue4")), "#,##0.00");
					if (!boolSummaryOnly) {
						// pass the BillKey for the subreport to use to find all of the payments that match this account and year
						srptTaxClubActivityDetail.Object = new srptActivityDetail();
						srptTaxClubActivityDetail.Object.Tag = rsData.Get_Fields("BillKey");
					}
				} else {
					fldOwed.Text = Strings.Format(rsLData.Get_Fields("Principal"), "#,##0.00"); // kk03272015 trocl-1135  '(rsData.Fields("Principal")), "#,##0.00")
					if (!boolSummaryOnly) {
						// pass the LienRecordNumber for the subreport to use to find all of the payments that match this account and year
						srptTaxClubActivityDetail.Object = new srptActivityDetailLien();
						srptTaxClubActivityDetail.Object.Tag = rsData.Get_Fields("LienRecordNumber");
					}
				}

				// move to the next record in the query
				rsData.MoveNext();
			}
		}

		private void Detail_Format()
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblFooter.Caption = "There were "+Convert.ToString(lngCount)+" accounts processed.";
		}

		private void ReportFooter_Format()
		{
			SetupTotals();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this function will return the SQL string for the criteria that has been selected
			string strMod = "";

			if (modStatusPayments.boolRE) { // this will select the correct records from the tax table
				strMod = "RE";
			} else {
				strMod = "PP";
			}

			// kgk 2-14-2012  BuildSQL = "SELECT * FROM TaxClubJoin WHERE BillingType = '" & strMod & "' AND Type = '" & strMod & "'"
			// TaxClubJoin = "SELECT BillingMaster.BillKey AS BK, BillingMaster.Account AS Acct, * FROM BillingMaster INNER JOIN TaxClub ON BillingMaster.Billkey = TaxClub.BillKey"
			BuildSQL = "SELECT BillingMaster.ID AS BK, BillingMaster.Account AS Acct, * FROM BillingMaster INNER JOIN TaxClub ON BillingMaster.ID = TaxClub.BillKey WHERE BillingType = '"+strMod+"' AND Type = '"+strMod+"'";
			return BuildSQL;
		}

		private void rptTaxClubPaymentsOverdue_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTaxClubPaymentsOverdue properties;
			//rptTaxClubPaymentsOverdue.Caption	= "Tax Club Outstanding Balance Report";
			//rptTaxClubPaymentsOverdue.Icon	= "rptTaxClubPaymentsOverdue.dsx":0000";
			//rptTaxClubPaymentsOverdue.Left	= 0;
			//rptTaxClubPaymentsOverdue.Top	= 0;
			//rptTaxClubPaymentsOverdue.Width	= 23760;
			//rptTaxClubPaymentsOverdue.Height	= 17430;
			//rptTaxClubPaymentsOverdue.StartUpPosition	= 3;
			//rptTaxClubPaymentsOverdue.SectionData	= "rptTaxClubPaymentsOverdue.dsx":058A;
			//End Unmaped Properties
		}
	}
}
