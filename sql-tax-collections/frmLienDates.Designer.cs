﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienDates.
	/// </summary>
	partial class frmLienDates : BaseForm
	{
		public fecherFoundation.FCGrid vsRate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienDates));
			this.vsRate = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 435);
			this.BottomPanel.Size = new System.Drawing.Size(931, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsRate);
			this.ClientArea.Size = new System.Drawing.Size(931, 375);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(931, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(130, 30);
			this.HeaderText.Text = "Lien Dates";
			// 
			// vsRate
			// 
			this.vsRate.AllowSelection = false;
			this.vsRate.AllowUserToResizeColumns = false;
			this.vsRate.AllowUserToResizeRows = false;
			this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsRate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsRate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsRate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsRate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsRate.BackColorSel = System.Drawing.Color.Empty;
			this.vsRate.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsRate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsRate.ColumnHeadersHeight = 30;
			this.vsRate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsRate.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsRate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsRate.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			this.vsRate.FixedCols = 0;
			this.vsRate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsRate.FrozenCols = 0;
			this.vsRate.FrozenRows = 0;
			this.vsRate.GridColor = System.Drawing.Color.Empty;
			this.vsRate.Location = new System.Drawing.Point(30, 30);
			this.vsRate.Name = "vsRate";
			this.vsRate.ReadOnly = true;
			this.vsRate.RowHeadersVisible = false;
			this.vsRate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsRate.RowHeightMin = 0;
			this.vsRate.Rows = 1;
			this.vsRate.ShowColumnVisibilityMenu = false;
			this.vsRate.Size = new System.Drawing.Size(871, 330);
			this.vsRate.StandardTab = false;
			this.vsRate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsRate.TabIndex = 0;
			this.vsRate.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRate_BeforeEdit);
			this.vsRate.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsRate_AfterEdit);
			this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
			this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
			this.vsRate.DoubleClick += new System.EventHandler(this.vsRate_DblClick);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(414, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(102, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(854, 30);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(47, 24);
			this.cmdFilePrint.TabIndex = 54;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.cmdFilePrint_Click);
			// 
			// frmLienDates
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(931, 543);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmLienDates";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Lien Dates";
			this.Load += new System.EventHandler(this.frmLienDates_Load);
			this.Activated += new System.EventHandler(this.frmLienDates_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienDates_KeyPress);
			this.Resize += new System.EventHandler(this.frmLienDates_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
		public FCButton cmdFilePrint;
	}
}
