﻿using System;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using Wisej.Web;

namespace TWCL0000
{
    public partial class frmRealEstateTaxAccountSearch : BaseForm, IModalView<IRealEstateTaxAccountSearchViewModel>
    {
        private bool cancelling = true;
        public frmRealEstateTaxAccountSearch()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            btnSearch.Click += BtnSearch_Click;
            this.Resize += FrmRealEstateTaxAccountSearch_Resize;
            this.Load += FrmRealEstateTaxAccountSearch_Load;
            btnSaveExit.Click += BtnSaveExit_Click;
            SearchGrid.DoubleClick += SearchGrid_DoubleClick;
        }

        private void SearchGrid_DoubleClick(object sender, EventArgs e)
        {
            SelectRow();
        }

        private void SelectRow()
        {
            if (SearchGrid.Row > 0)
            {
                ViewModel.SelectAccount(SearchGrid.TextMatrix(SearchGrid.Row, (int)SearchColumns.Account).ToIntegerValue());
                CloseWithoutCancelling();
            }
        }

        private void BtnSaveExit_Click(object sender, EventArgs e)
        {
            SelectRow();
        }

        private void FrmRealEstateTaxAccountSearch_Load(object sender, EventArgs e)
        {
            SetupGrid();
        }

        private void FrmRealEstateTaxAccountSearch_Resize(object sender, EventArgs e)
        {
            ResizeGrid();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            if (string.IsNullOrWhiteSpace(txtName.Text) && string.IsNullOrWhiteSpace(txtMapLot.Text))
            {
                return;
            }
            ViewModel.Search(txtName.Text.Trim(),txtMapLot.Text.Trim());
            ShowResults();
        }

        private void ShowResults()
        {
            SearchGrid.Rows = 1;
            var searchResults = ViewModel.SearchResults.ToList();
            foreach (var result in searchResults)
            {
                SearchGrid.Rows += 1;
                var row = SearchGrid.Rows - 1;
                SearchGrid.TextMatrix(row, (int) SearchColumns.Account, result.Account.ToString());
                SearchGrid.TextMatrix(row, (int) SearchColumns.Name, result.Name);
                SearchGrid.TextMatrix(row, (int) SearchColumns.Maplot, result.MapLot);
            }
        }

        private void SelectAccount(int account)
        {
            ViewModel.SelectAccount(account);
            Close();
        }
        public frmRealEstateTaxAccountSearch(IRealEstateTaxAccountSearchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
        public IRealEstateTaxAccountSearchViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void CloseWithoutCancelling()
        {
            
            Close();
        }

        private void SetupGrid()
        {
            SearchGrid.Cols = 3;
            SearchGrid.Rows = 1;
            SearchGrid.TextMatrix(0, (int) SearchColumns.Account, "Account");
            SearchGrid.TextMatrix(0, (int) SearchColumns.Name, "Name");
            SearchGrid.TextMatrix(0, (int) SearchColumns.Maplot, "Map Lot");
        }

        private void ResizeGrid()
        {
            int gridWidth = SearchGrid.WidthOriginal;
            SearchGrid.ColWidth((int) SearchColumns.Account, (gridWidth * .15).ToInteger());
            SearchGrid.ColWidth((int) SearchColumns.Name, (gridWidth * .49).ToInteger());
            SearchGrid.ColWidth((int) SearchColumns.Maplot, (gridWidth * .34).ToInteger());
        }

        private enum SearchColumns
        {
            Account = 0,
            Name = 1,
            Maplot = 2
        }
    }
}
