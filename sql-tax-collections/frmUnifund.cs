﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmUnifund.
	/// </summary>
	public partial class frmUnifund : BaseForm
	{
		public frmUnifund()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUnifund InstancePtr
		{
			get
			{
				return (frmUnifund)Sys.GetInstance(typeof(frmUnifund));
			}
		}

		protected frmUnifund _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsUnifundExport tUni;

		private void Command1_Click()
		{
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// App.MainForm.CommonDialog1.Flags = cdlOFNExplorer+cdlOFNOverwritePrompt+cdlOFNLongNames+cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				App.MainForm.CommonDialog1.DialogTitle = "Save Export As";
				App.MainForm.CommonDialog1.CancelError = true;
				App.MainForm.CommonDialog1.DefaultExt = ".txt";
				string strFileName = "";
				strFileName = Strings.Trim(txtFile.Text);
				if (strFileName != "")
				{
					if (!File.Exists(strFileName))
					{
						strFileName = "";
					}
				}
				if (!(strFileName == ""))
				{
					App.MainForm.CommonDialog1.InitDir = Directory.GetParent(strFileName).FullName;
					App.MainForm.CommonDialog1.FileName = Path.GetFileNameWithoutExtension(strFileName);
				}
				else
				{
					App.MainForm.CommonDialog1.FileName = "";
				}
				App.MainForm.CommonDialog1.ShowSave();
				txtFile.Text = App.MainForm.CommonDialog1.FileName;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			if (!(tUni == null))
			{
				tUni.Cancel = true;
				App.DoEvents();
			}
			Close();
		}

		private void cmdExport_Click(object sender, System.EventArgs e)
		{
			ExportUnifund();
		}

		private void frmUnifund_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmUnifund_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUnifund.Icon	= "frmUnifund.frx":0000";
			//frmUnifund.FillStyle	= 0;
			//frmUnifund.ScaleWidth	= 5880;
			//frmUnifund.ScaleHeight	= 4065;
			//frmUnifund.LinkTopic	= "Form2";
			//frmUnifund.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmUnifund.frx":05B8";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			clsDRWrapper rsLoad = new clsDRWrapper();
			// VBto upgrade warning: intBillYear As short --> As int	OnWrite(string)
			int intBillYear = 0;
			int intLast;
			// cmbBillType.ItemData(0) = 0
			cmbBillType.ItemData(0, 1);
			cmbBillType.ItemData(1, 2);
			cmbStart.Clear();
			cmbEnd.Clear();
			intLast = 0;
			rsLoad.OpenRecordset("select billingyear from billingmaster where convert(int, billingyear) > 0 group by billingyear order by billingyear", modExtraModules.strCLDatabase);
			while (!rsLoad.EndOfFile())
			{
				intBillYear = FCConvert.ToInt32(Strings.Left(FCConvert.ToString(rsLoad.Get_Fields_Int32("billingyear")), 4));
				if (intBillYear != intLast)
				{
					cmbStart.AddItem(FCConvert.ToString(intBillYear));
					cmbStart.ItemData(cmbStart.NewIndex, intBillYear);
					cmbEnd.AddItem(FCConvert.ToString(intBillYear));
					cmbEnd.ItemData(cmbEnd.NewIndex, intBillYear);
					intLast = intBillYear;
				}
				rsLoad.MoveNext();
			}
			cmbStart.SelectedIndex = 0;
			cmbEnd.SelectedIndex = cmbEnd.Items.Count - 1;
			cmbBillType.SelectedIndex = 0;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			ExportUnifund();
		}

		private void tUni_ProcessingRecord(object strAcct, int intBillYear, string strMod)
		{
			lblAccount.Text = FCConvert.ToString(strAcct);
			lblYear.Text = FCConvert.ToString(intBillYear);
			lblAccount.Refresh();
			lblYear.Refresh();
			lblType.Text = strMod;
			lblType.Refresh();
			App.DoEvents();
		}

		private void tUni_ProcessingFinished()
		{
			lblAccount.Text = "";
			lblYear.Text = "";
			FCMessageBox.Show("Export is complete", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Export Finished");
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ExportUnifund()
		{
			string strFileName;
			string strSQL;
			if (cmbStart.ItemData(cmbStart.SelectedIndex) > cmbEnd.ItemData(cmbEnd.SelectedIndex))
			{
				FCMessageBox.Show("You have chosen an invalid year range", MsgBoxStyle.Exclamation, "Invalid Range");
				return;
			}
			if (Strings.Trim(txtFile.Text) == "")
			{
				FCMessageBox.Show("You must select a file name to export", MsgBoxStyle.Exclamation, "Invalid File Name");
				return;
			}
			strFileName = Strings.Trim(txtFile.Text);
			if (!Directory.Exists(Directory.GetParent(strFileName).FullName))
			{
				FCMessageBox.Show("The folder could not be found", MsgBoxStyle.Exclamation, "Invalid File Name");
				return;
			}
			strSQL = "Select * from billingmaster where billingyear between " + FCConvert.ToString(cmbStart.ItemData(cmbStart.SelectedIndex)) + "1 and " + FCConvert.ToString(cmbEnd.ItemData(cmbEnd.SelectedIndex)) + "9 ";
			switch (cmbBillType.ItemData(cmbBillType.SelectedIndex))
			{
				case 0:
					{
						// both
						strSQL += " order by billingyear, billingtype, maplot, account ";
						break;
					}
				case 1:
					{
						// real estate
						strSQL += " and billingtype = 'RE' order by billingyear, maplot, account";
						break;
					}
				case 2:
					{
						// personal property
						strSQL += " and billingtype = 'PP' order by billingyear, account";
						break;
					}
			}
			//end switch
			tUni = new clsUnifundExport();
			tUni.Export(strSQL, strFileName);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
