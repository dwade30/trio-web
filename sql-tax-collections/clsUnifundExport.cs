﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	public class clsUnifundExport
	{
		//=========================================================
		// VBto upgrade warning: strAcct As Variant --> As string
		public delegate void ProcessingRecordEventHandler(string strAcct, int intBillYear, string strMod);

		public event ProcessingRecordEventHandler ProcessingRecord;

		public delegate void ProcessingFinishedEventHandler();

		public event ProcessingFinishedEventHandler ProcessingFinished;

		private struct UnifundFormat
		{
			public string SubSystem;
			public int FiscalYear;
			public string PropID;
			public string TransID;
			public string BillToLine1;
			public string BillToLine2;
			public string BillToLine3;
			public string BillToLine4;
			public string BillToLine5;
			public string BillToLine6;
			public int BillNumber;
			public string EntryDate;
			public string TransDetailID;
			public string TransType;
			public string ClassCode;
			public string TransKind;
			public string DueDate;
			// VBto upgrade warning: Installment As string	OnWriteFCConvert.ToInt16(
			public string Installment;
			public double Amount;
			public string Notes;
			public string DetailType;
			public string ProjName;
			// VBto upgrade warning: AppliedID As string	OnWrite(int, string)
			public string AppliedID;
			public double AppliedAMT;
			public double Balance;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public UnifundFormat(int unusedParam)
			{
				this.SubSystem = String.Empty;
				this.FiscalYear = 0;
				this.PropID = String.Empty;
				this.TransID = String.Empty;
				this.BillToLine1 = String.Empty;
				this.BillToLine2 = String.Empty;
				this.BillToLine3 = String.Empty;
				this.BillToLine4 = String.Empty;
				this.BillToLine5 = String.Empty;
				this.BillToLine6 = String.Empty;
				this.BillNumber = 0;
				this.EntryDate = String.Empty;
				this.TransDetailID = String.Empty;
				this.TransType = String.Empty;
				this.DueDate = String.Empty;
				this.Installment = String.Empty;
				this.Amount = 0;
				this.Notes = String.Empty;
				this.DetailType = String.Empty;
				this.AppliedAMT = 0;
				this.AppliedID = String.Empty;
				this.Balance = 0;
				this.ClassCode = String.Empty;
				this.TransKind = String.Empty;
				this.ProjName = String.Empty;
			}
		};

		private bool boolCancel;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsRateRec = new clsDRWrapper();
		private clsDRWrapper rsRateRec_AutoInitialized;

		private clsDRWrapper rsRateRec
		{
			get
			{
				if (rsRateRec_AutoInitialized == null)
				{
					rsRateRec_AutoInitialized = new clsDRWrapper();
				}
				return rsRateRec_AutoInitialized;
			}
			set
			{
				rsRateRec_AutoInitialized = value;
			}
		}

		public bool Cancel
		{
			set
			{
				boolCancel = true;
			}
		}

		private void InitializeData()
		{
			rsRateRec.OpenRecordset("select * from raterec", modExtraModules.strCLDatabase);
		}

		public void Export(string strSQL, string strFileName)
		{
			boolCancel = false;
			InitializeData();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			ExportBill(ref rsLoad, strFileName);
		}

		private string BuildLine(ref UnifundFormat tRec)
		{
			string BuildLine = "";
			string strReturn;
			strReturn = "";
			strReturn += tRec.SubSystem + "\t";
			strReturn += FCConvert.ToString(tRec.FiscalYear) + "\t";
			strReturn += tRec.PropID + "\t";
			strReturn += tRec.TransID + "\t";
			strReturn += tRec.BillToLine1 + "\t";
			strReturn += tRec.BillToLine2 + "\t";
			strReturn += tRec.BillToLine3 + "\t";
			strReturn += tRec.BillToLine4 + "\t";
			strReturn += tRec.BillToLine5 + "\t";
			strReturn += tRec.BillToLine6 + "\t";
			strReturn += FCConvert.ToString(tRec.BillNumber) + "\t";
			strReturn += tRec.EntryDate + "\t";
			strReturn += tRec.TransDetailID + "\t";
			strReturn += tRec.TransType + "\t";
			strReturn += tRec.ClassCode + "\t";
			strReturn += tRec.TransKind + "\t";
			strReturn += tRec.DueDate + "\t";
			strReturn += tRec.Installment + "\t";
			strReturn += Strings.Format(tRec.Amount, "0.00") + "\t";
			strReturn += tRec.Notes + "\t";
			strReturn += tRec.DetailType + "\t";
			strReturn += tRec.ProjName + "\t";
			strReturn += tRec.AppliedID + "\t";
			strReturn += Strings.Format(tRec.AppliedAMT, "0.00");
			// & vbCrLf
			strReturn += "\t" + Strings.Format(tRec.Balance, "0.00");
			BuildLine = strReturn;
			return BuildLine;
		}

		private string ExportBill(ref clsDRWrapper tBillRec, string strFileName)
		{
			string ExportBill = "";
			StreamWriter tOut = null;
			bool boolFileOpen = false;
            clsDRWrapper rsPayments = new clsDRWrapper();
            clsDRWrapper rsLien = new clsDRWrapper();
            clsDRWrapper rsTemp = new clsDRWrapper();
            UnifundFormat URec = new UnifundFormat(0);

            try
            {
                // On Error GoTo ErrorHandler

                // VBto upgrade warning: lngBillID As int	OnRead(string)
                int lngBillID = 0;

                // VBto upgrade warning: intBillYear As short --> As int	OnWrite(string)
                int intBillYear = 0;
                string strLine = "";
                string lngLienID = "";

                // VBto upgrade warning: lastID As int	OnRead(string)
                int lastID = 0;
                string strFakeID = "";
                boolFileOpen = false;
                double[] arDblAmount = new double[4 + 1];
                double dblCurBal = 0;

                // VBto upgrade warning: lngLastInt As string	OnWrite(short, string)
                string lngLastInt = "";
                string[] strDue = new string[2 + 1];

                // VBto upgrade warning: lngLastDmd As string	OnWrite(short, string)
                string lngLastDmd = "";
                double dblPer1Due = 0;
                double dblOP = 0;
                string strBillingType = "";
                bool bool2ndInstall = false;
                int intRecNum = 0;
                tOut = new StreamWriter(strFileName);
                boolFileOpen = true;

                while (!tBillRec.EndOfFile())
                {
                    lastID = 0;
                    lngLastInt = FCConvert.ToString(0);
                    lngLastDmd = FCConvert.ToString(0);
                    dblCurBal = 0;
                    strBillingType = tBillRec.Get_Fields_String("BillingType");
                    lngLienID = FCConvert.ToString(tBillRec.Get_Fields_Int32("lienrecordnumber"));
                    URec.SubSystem = tBillRec.Get_Fields_String("BillingType");
                    lngBillID = tBillRec.Get_Fields_Int32("BillKey");
                    intBillYear = FCConvert.ToInt32(Strings.Left(FCConvert.ToString(tBillRec.Get_Fields_Int32("billingyear")), 4));

                    // RaiseEvent ProcessingRecord(tBillRec.Get_Fields("Account"), intBillYear)
                    // DoEvents
                    URec.FiscalYear = intBillYear;

                    if (tBillRec.Get_Fields_String("BillingType") == "RE")
                    {
                        if (Strings.Trim(tBillRec.Get_Fields_String("maplot")) != "")
                        {
                            URec.PropID = tBillRec.Get_Fields_String("MapLot");
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            rsTemp.OpenRecordset("select rsmaplot from master where rscard = 1 and rsaccount = " + FCConvert.ToString(Conversion.Val(tBillRec.Get_Fields("account"))), modExtraModules.strREDatabase);

                            URec.PropID = !rsTemp.EndOfFile() ? FCConvert.ToString(rsTemp.Get_Fields_String("rsmaplot")) : tBillRec.Get_Fields_String("account");
                        }
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        URec.PropID = tBillRec.Get_Fields_String("Account");
                    }

                    if (this.ProcessingRecord != null) this.ProcessingRecord(URec.PropID, intBillYear, URec.SubSystem);
                    App.DoEvents();

                    if (boolCancel)
                    {
                        tOut.Close();

                        return ExportBill;
                    }

                    URec.TransID = FCConvert.ToString(tBillRec.Get_Fields_Int32("Billkey"));
                    URec.FiscalYear = intBillYear;
                    URec.BillToLine1 = tBillRec.Get_Fields_String("Name1");
                    URec.BillToLine2 = tBillRec.Get_Fields_String("Name2");

                    // address
                    URec.BillToLine3 = tBillRec.Get_Fields_String("Address1");
                    URec.BillToLine4 = tBillRec.Get_Fields_String("Address2");
                    URec.BillToLine5 = tBillRec.Get_Fields_String("Address3");

                    if (Strings.Trim(URec.BillToLine3) == "" && Strings.Trim(URec.BillToLine4) == "" && Strings.Trim(URec.BillToLine5) == "")
                    {
                        // get from re/pp
                        if (tBillRec.Get_Fields_String("BillingType") == "RE")
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            rsTemp.OpenRecordset("select * from master where rsaccount = " + tBillRec.Get_Fields("account") + " and rscard = 1", modExtraModules.strREDatabase);

                            if (!rsTemp.EndOfFile())
                            {
                                URec.BillToLine3 = FCConvert.ToString(rsTemp.Get_Fields_String("rsaddr1"));
                                URec.BillToLine4 = FCConvert.ToString(rsTemp.Get_Fields_String("rsaddr2"));
                                URec.BillToLine5 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rsaddr3")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("rsstate")) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rszip")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("rszip4"))));
                            }
                        }
                        else
                            if (tBillRec.Get_Fields_String("Billingtype") == "PP")
                            {
                                // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                                rsTemp.OpenRecordset("select * from ppmaster where account = " + tBillRec.Get_Fields("account"), modExtraModules.strPPDatabase);

                                if (!rsTemp.EndOfFile())
                                {
                                    URec.BillToLine3 = FCConvert.ToString(rsTemp.Get_Fields_String("address1"));
                                    URec.BillToLine4 = FCConvert.ToString(rsTemp.Get_Fields_String("address2"));
                                    URec.BillToLine5 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("city")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("state")) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("zip")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("zip4"))));
                                }
                            }
                    }

                    URec.BillNumber = tBillRec.Get_Fields_Int32("billkey");
                    URec.TransType = "inv";
                    URec.TransDetailID = tBillRec.Get_Fields_Int32("billkey") + "P1";
                    URec.ClassCode = "";
                    URec.TransKind = "i";
                    URec.Installment = FCConvert.ToString(1);
                    URec.Notes = "";
                    URec.DetailType = "R";
                    URec.AppliedAMT = 0;
                    URec.AppliedID = FCConvert.ToString(0);
                    URec.ClassCode = URec.SubSystem + "Tax";
                    URec.Amount = Conversion.Val(tBillRec.Get_Fields_Decimal("TaxDue1") + tBillRec.Get_Fields_Decimal("TaxDue2") + tBillRec.Get_Fields_Decimal("TaxDue3") + tBillRec.Get_Fields_Decimal("TaxDue4"));
                    dblPer1Due = Conversion.Val(tBillRec.Get_Fields_Decimal("TaxDue1"));
                    dblCurBal = URec.Amount;
                    URec.Balance = dblCurBal;

                    if (tBillRec.Get_Fields_Int32("ratekey") > 0)
                    {
                        if (rsRateRec.FindFirstRecord("ratekey", tBillRec.Get_Fields_Int32("ratekey")))
                        {
                            URec.EntryDate = Strings.Format(rsRateRec.Get_Fields_DateTime("BillingDate"), "MM/dd/yyyy");
                            URec.DueDate = Strings.Format(rsRateRec.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
                            strDue[1] = URec.DueDate;
                            strDue[2] = URec.DueDate;

                            // now second installment
                            if (Conversion.Val(rsRateRec.Get_Fields_Int16("numberofperiods")) == 2)
                            {
                                URec.Amount = Conversion.Val(tBillRec.Get_Fields_Decimal("taxdue1"));
                                URec.Balance = URec.Amount;
                                strLine = BuildLine(ref URec);
                                tOut.WriteLine(strLine);
                                URec.Amount = Conversion.Val(tBillRec.Get_Fields_Decimal("Taxdue2"));
                                URec.Balance = dblCurBal;
                                URec.DueDate = Strings.Format(rsRateRec.Get_Fields_DateTime("duedate2"), "MM/dd/yyyy");
                                URec.TransDetailID = tBillRec.Get_Fields_Int32("billkey") + "P2";
                                strDue[2] = URec.DueDate;
                                URec.Installment = FCConvert.ToString(2);
                                strLine = BuildLine(ref URec);
                                tOut.WriteLine(strLine);
                            }
                            else
                            {
                                strLine = BuildLine(ref URec);
                                tOut.WriteLine(strLine);
                            }
                        }
                        else
                        {
                            strLine = BuildLine(ref URec);
                            tOut.WriteLine(strLine);
                        }
                    }

                    bool2ndInstall = false;
                    rsPayments.OpenRecordset("Select * from paymentrec where billkey = " + FCConvert.ToString(lngBillID) + " and billcode <> 'L' order by recordedtransactiondate,id", modExtraModules.strCLDatabase);

                    while (!rsPayments.EndOfFile())
                    {
                        dblOP = 0;
                        URec.ClassCode = URec.SubSystem + "Tax";
                        URec.EntryDate = Strings.Format(rsPayments.Get_Fields_DateTime("recordedtransactiondate"), "MM/dd/yyyy");

                        if (!(rsPayments.Get_Fields_DateTime("recordedtransactiondate") is DateTime))
                        {
                            URec.EntryDate = Strings.Format(rsPayments.Get_Fields_DateTime("actualsystemdate"), "MM/dd/yyyy");
                        }

                        URec.TransDetailID = FCConvert.ToString(rsPayments.Get_Fields_Int32("ID"));
                        URec.Notes = "";
                        URec.AppliedAMT = 0;
                        URec.AppliedID = FCConvert.ToString(0);
                        arDblAmount[0] = 0;
                        arDblAmount[1] = 0;
                        arDblAmount[2] = 0;
                        arDblAmount[3] = 0;

                        if (bool2ndInstall)
                        {
                            URec.Installment = FCConvert.ToString(2);
                            URec.DueDate = strDue[2];
                        }
                        else
                        {
                            URec.Installment = FCConvert.ToString(1);
                            URec.DueDate = strDue[1];
                        }

                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        URec.Amount = Conversion.Val(rsPayments.Get_Fields("Principal")) + Conversion.Val(rsPayments.Get_Fields_Decimal("PrelienInterest")) + Conversion.Val(rsPayments.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPayments.Get_Fields_Decimal("liencost"));
                        URec.Amount *= -1;

                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        arDblAmount[0] = Conversion.Val(rsPayments.Get_Fields("Principal")) * -1;
                        arDblAmount[1] = Conversion.Val(rsPayments.Get_Fields_Decimal("PrelienInterest")) * -1;
                        arDblAmount[2] = Conversion.Val(rsPayments.Get_Fields_Decimal("CurrentInterest")) * -1;
                        arDblAmount[3] = Conversion.Val(rsPayments.Get_Fields_Decimal("liencost")) * -1;

                        if (dblPer1Due > 0)
                        {
                            dblPer1Due += arDblAmount[0];

                            if (dblPer1Due <= 0)
                            {
                                bool2ndInstall = true;

                                if (dblPer1Due < 0)
                                {
                                    dblOP = -1 * dblPer1Due;
                                }
                            }
                        }

                        URec.TransKind = "a";
                        string VBtoVar = rsPayments.Get_Fields_String("code");

                        switch (VBtoVar)
                        {
                            case "P":
                            case "Y":
                                URec.TransType = "pmt";

                                // URec.Amount = URec.Amount * -1
                                break;
                            case "I":
                                URec.TransType = "int";

                                // URec.Amount = URec.Amount * -1
                                URec.TransKind = "i";
                                lngLastInt = URec.TransDetailID;
                                URec.ClassCode = URec.SubSystem + "Int";

                                break;
                            case "C":
                            {
                                URec.TransType = "adj";

                                if (FCConvert.ToBoolean(rsPayments.Get_Fields_Boolean("reversal")))
                                {
                                    URec.TransType = "rev";

                                    // look up reversed id
                                    URec.AppliedID = FCConvert.ToString(lastID);
                                    URec.AppliedAMT = URec.Amount;
                                }

                                break;
                            }
                            case "X":
                                URec.TransType = "adj";

                                // URec.Amount = URec.Amount * -1
                                break;
                            case "R":
                                // refunded abatement
                                break;
                            case "A":
                                // abatement
                                URec.TransType = "ab";

                                // URec.Amount = URec.Amount * -1
                                break;
                            case "L":
                                // liencost
                                URec.TransType = "fee";
                                URec.TransKind = "i";
                                lngLastDmd = URec.TransDetailID;

                                break;
                            case "3":
                                // demand fee
                                URec.TransType = "dmd";
                                URec.TransKind = "i";
                                lngLastDmd = URec.TransDetailID;

                                // URec.Amount = URec.Amount * -1
                                break;
                            case "F":
                                // overpay refund
                                URec.TransType = "rf";

                                // URec.Amount = URec.Amount * -1
                                break;
                            case "S":
                                // supplemental
                                break;
                            case "N":
                                // writeoff
                                URec.TransType = "adj";
                                URec.Notes = "Write-off";

                                // urec.Amount = urec.Amount * -1
                                break;
                        }

                        URec.DetailType = FCConvert.ToString(rsPayments.Get_Fields_String("billcode")) != "L" ? "R" : "L";

                        lastID = FCConvert.ToInt32(rsPayments.Get_Fields_Int32("ID"));

                        // strLine = BuildLine(URec)
                        // tOut.WriteLine (strLine)
                        intRecNum = 0;

                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        if (FCConvert.ToString(rsPayments.Get_Fields("code")) == "C" && URec.TransType == "rev")
                        {
                            // principal
                            dblCurBal += arDblAmount[0];
                            URec.ClassCode = URec.SubSystem + "Tax";

                            if (arDblAmount[0] != 0)
                            {
                                intRecNum += 1;
                                URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                            }

                            strLine = BuildApplied(URec.AppliedID, arDblAmount[0], dblCurBal, ref URec);

                            if (strLine != "")
                            {
                                tOut.WriteLine(strLine);
                            }

                            // interest
                            if (arDblAmount[2] != 0)
                            {
                                intRecNum += 1;

                                if (lngLastInt == URec.TransDetailID)
                                {
                                    lngLastInt = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                            }

                            dblCurBal += arDblAmount[2];
                            URec.ClassCode = URec.SubSystem + "Int";
                            strLine = BuildApplied(URec.AppliedID, arDblAmount[2], dblCurBal, ref URec);

                            if (strLine != "")
                            {
                                tOut.WriteLine(strLine);
                            }

                            // lien cost
                            dblCurBal += arDblAmount[3];

                            if (arDblAmount[3] != 0)
                            {
                                intRecNum += 1;

                                if (lngLastDmd == URec.TransDetailID)
                                {
                                    lngLastDmd = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                            }

                            URec.ClassCode = URec.SubSystem + "Fee";
                            strLine = BuildApplied(URec.AppliedID, arDblAmount[3], dblCurBal, ref URec);

                            if (strLine != "")
                            {
                                tOut.WriteLine(strLine);
                            }
                        }
                        else
                        {
                            // principal
                            URec.ClassCode = URec.SubSystem + "Tax";

                            if (dblOP > 0 && strDue[2] != strDue[1])
                            {
                                dblCurBal += arDblAmount[0] + dblOP;

                                if (arDblAmount[0] != 0 || dblOP != 0)
                                {
                                    intRecNum += 1;
                                    URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                strLine = BuildApplied(FCConvert.ToString(lngBillID) + "P" + URec.Installment, arDblAmount[0] + dblOP, dblCurBal, ref URec);

                                if (strLine != "")
                                {
                                    tOut.WriteLine(strLine);
                                }

                                URec.Installment = FCConvert.ToString(2);
                                URec.DueDate = strDue[2];
                                dblCurBal -= dblOP;

                                if (dblOP != 0)
                                {
                                    intRecNum += 1;
                                    URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                strLine = BuildApplied(FCConvert.ToString(lngBillID) + "P" + URec.Installment, -dblOP, dblCurBal, ref URec);

                                if (strLine != "")
                                {
                                    tOut.WriteLine(strLine);
                                }
                            }
                            else
                            {
                                dblCurBal += arDblAmount[0];

                                if (arDblAmount[0] != 0)
                                {
                                    intRecNum += 1;
                                    URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                strLine = BuildApplied(FCConvert.ToString(lngBillID) + "P" + URec.Installment, arDblAmount[0], dblCurBal, ref URec);

                                if (strLine != "")
                                {
                                    tOut.WriteLine(strLine);
                                }
                            }

                            // interest
                            URec.ClassCode = URec.SubSystem + "Int";
                            dblCurBal += arDblAmount[2];

                            if (arDblAmount[2] != 0)
                            {
                                intRecNum += 1;

                                if (lngLastInt == URec.TransDetailID)
                                {
                                    lngLastInt = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                            }

                            if (Conversion.Val(rsPayments.Get_Fields_Int32("chgintnumber")) > 0)
                            {
                                lngLastInt = FCConvert.ToString(Conversion.Val(rsPayments.Get_Fields_Int32("chgintnumber"))) + "P1";
                            }

                            strLine = BuildApplied(lngLastInt, arDblAmount[2], dblCurBal, ref URec);

                            if (strLine != "")
                            {
                                tOut.WriteLine(strLine);
                            }

                            // lien cost
                            dblCurBal += arDblAmount[3];
                            URec.ClassCode = URec.SubSystem + "Fee";

                            if (arDblAmount[3] != 0)
                            {
                                intRecNum += 1;

                                if (lngLastDmd == URec.TransDetailID)
                                {
                                    lngLastDmd = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                }

                                URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                            }

                            strLine = BuildApplied(lngLastDmd, arDblAmount[3], dblCurBal, ref URec);

                            if (strLine != "")
                            {
                                tOut.WriteLine(strLine);
                            }
                        }

                        rsPayments.MoveNext();
                    }

                    URec.Installment = FCConvert.ToString(1);

                    // now get lien info
                    if (FCConvert.ToDouble(lngLienID) > 0)
                    {
                        rsLien.OpenRecordset("select * from lienrec where lienrecordnumber = " + lngLienID, modExtraModules.strCLDatabase);

                        if (!rsLien.EndOfFile())
                        {
                            URec.TransID = lngLienID;
                            lngLastInt = FCConvert.ToString(0);
                            lngLastDmd = FCConvert.ToString(0);

                            if (Information.IsDate(rsLien.Get_Fields("DateCreated")))
                            {
                                URec.EntryDate = Strings.Format(rsLien.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
                            }

                            if (rsRateRec.FindFirstRecord("ratekey", rsLien.Get_Fields_Int32("ratekey")))
                            {
                                URec.DueDate = Strings.Format(rsRateRec.Get_Fields_DateTime("duedate1"), "MM/dd/yyyy");
                            }

                            URec.TransDetailID = lngLienID;
                            URec.TransType = "tt";
                            URec.Notes = "Transferred to Lien";

                            // payoff bill first
                            URec.TransKind = "a";
                            URec.DetailType = "R";
                            URec.AppliedID = FCConvert.ToString(lngBillID);
                            URec.Balance = 0;
                            URec.AppliedAMT = -1 * dblCurBal;
                            URec.Amount = -1 * dblCurBal;
                            URec.ClassCode = "RETax";
                            strLine = BuildLine(ref URec);
                            tOut.WriteLine(strLine);

                            // now create lien
                            URec.TransKind = "i";
                            URec.DetailType = "L";
                            URec.ClassCode = "RETax";

                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            URec.Amount = Conversion.Val(rsLien.Get_Fields("Principal"));
                            dblCurBal = URec.Amount;
                            URec.Balance = URec.Amount;
                            URec.AppliedAMT = 0;
                            URec.AppliedID = FCConvert.ToString(0);
                            strLine = BuildLine(ref URec);
                            tOut.WriteLine(strLine);

                            // prelien int
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            if (Conversion.Val(rsLien.Get_Fields("Interest")) != 0)
                            {
                                URec.TransType = "ttp";
                                URec.ClassCode = URec.SubSystem + "Int";
                                URec.TransKind = "i";
                                URec.TransDetailID = GetFakeID(FCConvert.ToInt32(lngLienID), "int");
                                strFakeID = URec.TransDetailID;

                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                URec.Amount = Conversion.Val(rsLien.Get_Fields("Interest"));

                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                dblCurBal += Conversion.Val(rsLien.Get_Fields("Interest"));

                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                strLine = BuildApplied(strFakeID, FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(rsLien.Get_Fields("Interest")))), dblCurBal, ref URec);

                                if (strLine != "")
                                {
                                    tOut.WriteLine(strLine);
                                }
                            }

                            // costs
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            if (Conversion.Val(rsLien.Get_Fields("Costs")) != 0)
                            {
                                URec.ClassCode = URec.SubSystem + "Fee";

                                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                URec.Amount = Conversion.Val(rsLien.Get_Fields("Costs"));
                                URec.TransDetailID = GetFakeID(FCConvert.ToInt32(lngLienID), "fee");
                                strFakeID = URec.TransDetailID;
                                URec.TransType = "ttp";
                                URec.TransKind = "i";

                                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                dblCurBal += Conversion.Val(rsLien.Get_Fields("Costs"));

                                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                strLine = BuildApplied(strFakeID, FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(rsLien.Get_Fields("Costs")))), dblCurBal, ref URec);

                                if (strLine != "")
                                {
                                    tOut.WriteLine(strLine);
                                }
                            }

                            lastID = 0;
                            rsPayments.OpenRecordset("Select * from paymentrec where billkey = " + lngLienID + " and billcode = 'L' order by recordedtransactiondate,id", modExtraModules.strCLDatabase);

                            while (!rsPayments.EndOfFile())
                            {
                                URec.EntryDate = Strings.Format(rsPayments.Get_Fields_DateTime("recordedtransactiondate"), "MM/dd/yyyy");

                                if (!(rsPayments.Get_Fields_DateTime("recordedtransactiondate") is DateTime))
                                {
                                    URec.EntryDate = Strings.Format(rsPayments.Get_Fields_DateTime("actualsystemdate"), "MM/dd/yyyy");
                                }

                                URec.TransDetailID = FCConvert.ToString(rsPayments.Get_Fields_Int32("ID"));
                                URec.Notes = "";
                                URec.AppliedAMT = 0;
                                URec.AppliedID = FCConvert.ToString(0);
                                URec.TransType = "pmt";
                                URec.TransKind = "a";
                                arDblAmount[0] = 0;
                                arDblAmount[1] = 0;
                                arDblAmount[2] = 0;
                                arDblAmount[3] = 0;

                                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                URec.Amount = Conversion.Val(rsPayments.Get_Fields("Principal")) + Conversion.Val(rsPayments.Get_Fields_Decimal("PrelienInterest")) + Conversion.Val(rsPayments.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPayments.Get_Fields_Decimal("liencost"));
                                URec.Amount *= -1;

                                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                arDblAmount[0] = Conversion.Val(rsPayments.Get_Fields("Principal")) * -1;
                                arDblAmount[1] = Conversion.Val(rsPayments.Get_Fields_Decimal("PrelienInterest")) * -1;
                                arDblAmount[2] = Conversion.Val(rsPayments.Get_Fields_Decimal("CurrentInterest")) * -1;
                                arDblAmount[3] = Conversion.Val(rsPayments.Get_Fields_Decimal("liencost")) * -1;

                                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                                string VBtoVar1 = FCConvert.ToString(rsPayments.Get_Fields("code"));

                                if ((VBtoVar1 == "P") || (VBtoVar1 == "Y"))
                                {
                                    URec.TransType = "pmt";
                                    URec.AppliedID = FCConvert.ToString(rsPayments.Get_Fields_Int32("BILLkey"));
                                    URec.AppliedAMT = URec.Amount;
                                }
                                else
                                    if (VBtoVar1 == "I")
                                    {
                                        URec.TransType = "int";
                                        URec.TransKind = "i";
                                        lngLastInt = URec.TransDetailID;
                                        URec.ClassCode = "Int";
                                    }
                                    else
                                        if ((VBtoVar1 == "C") || (VBtoVar1 == "X"))
                                        {
                                            URec.TransType = "adj";

                                            if (FCConvert.ToBoolean(rsPayments.Get_Fields_Boolean("reversal")))
                                            {
                                                URec.TransType = "rev";

                                                // look up reversed id
                                                URec.AppliedID = FCConvert.ToString(lastID);
                                                URec.AppliedAMT = URec.Amount;
                                            }

                                            // Case "X"
                                            // URec.TransType = "adj"
                                        }
                                        else
                                            if (VBtoVar1 == "L")
                                            {
                                                // liencost
                                                URec.TransType = "fee";
                                                URec.TransKind = "i";
                                                lngLastDmd = URec.TransDetailID;
                                            }
                                            else
                                                if (VBtoVar1 == "3")
                                                {
                                                    // demand fee
                                                    URec.TransType = "dmd";
                                                    URec.TransKind = "i";
                                                    lngLastDmd = URec.TransDetailID;
                                                }
                                                else
                                                    if (VBtoVar1 == "F")
                                                    {
                                                        // overpay refund
                                                        URec.TransType = "rf";
                                                    }
                                                    else
                                                        if (VBtoVar1 == "N")
                                                        {
                                                            // writeoff
                                                            URec.TransType = "adj";
                                                            URec.Notes = "Write-off";
                                                        }

                                lastID = FCConvert.ToInt32(rsPayments.Get_Fields_Int32("ID"));

                                if (!(URec.TransType == "rev"))
                                {
                                    intRecNum = 0;

                                    // principal
                                    dblCurBal += arDblAmount[0];
                                    URec.ClassCode = URec.SubSystem + "Tax";

                                    if (arDblAmount[0] != 0)
                                    {
                                        intRecNum += 1;
                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    strLine = BuildApplied(lngLienID, arDblAmount[0], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }

                                    // prelien
                                    dblCurBal += arDblAmount[1];
                                    URec.ClassCode = URec.SubSystem + "Int";
                                    strFakeID = GetFakeID(FCConvert.ToInt32(lngLienID), "int");

                                    if (arDblAmount[1] != 0)
                                    {
                                        intRecNum += 1;
                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    strLine = BuildApplied(strFakeID, arDblAmount[1], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }

                                    // interest
                                    dblCurBal += arDblAmount[2];
                                    URec.ClassCode = URec.SubSystem + "Int";

                                    if (arDblAmount[2] != 0)
                                    {
                                        intRecNum += 1;

                                        if (lngLastInt == URec.TransDetailID)
                                        {
                                            lngLastInt = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                        }

                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    if (Conversion.Val(rsPayments.Get_Fields_Int32("chgintnumber")) > 0)
                                    {
                                        lngLastInt = FCConvert.ToString(Conversion.Val(rsPayments.Get_Fields_Int32("chgintnumber")));
                                    }

                                    strLine = BuildApplied(lngLastInt, arDblAmount[2], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }

                                    // costs
                                    dblCurBal += arDblAmount[3];
                                    URec.ClassCode = URec.SubSystem + "Fee";

                                    if (arDblAmount[3] != 0)
                                    {
                                        intRecNum += 1;

                                        if (lngLastDmd == URec.TransDetailID)
                                        {
                                            lngLastDmd = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                        }

                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    if (Conversion.Val(lngLastDmd) == 0)
                                    {
                                        lngLastDmd = lngLienID;
                                    }

                                    if (lngLastDmd == lngLienID)
                                    {
                                        strFakeID = GetFakeID(FCConvert.ToInt32(lngLienID), "fee");
                                    }
                                    else
                                    {
                                        strFakeID = lngLastDmd;
                                    }

                                    strLine = BuildApplied(strFakeID, arDblAmount[3], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }
                                }
                                else
                                {
                                    intRecNum = 0;

                                    // principal
                                    dblCurBal += arDblAmount[0];
                                    URec.ClassCode = URec.SubSystem + "Tax";

                                    if (arDblAmount[0] != 0)
                                    {
                                        intRecNum += 1;
                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    strLine = BuildApplied(URec.AppliedID, arDblAmount[0], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }

                                    // prelien interest
                                    dblCurBal += arDblAmount[1];

                                    if (arDblAmount[1] != 0)
                                    {
                                        intRecNum += 1;
                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    URec.ClassCode = URec.SubSystem + "Int";
                                    strLine = BuildApplied(URec.AppliedID, arDblAmount[1], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }

                                    // interest
                                    URec.ClassCode = URec.SubSystem + "Int";
                                    dblCurBal += arDblAmount[2];

                                    if (arDblAmount[2] != 0)
                                    {
                                        intRecNum += 1;
                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    strLine = BuildApplied(URec.AppliedID, arDblAmount[2], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }

                                    // lien cost
                                    URec.ClassCode = URec.SubSystem + "Fee";
                                    dblCurBal += arDblAmount[3];

                                    if (arDblAmount[3] != 0)
                                    {
                                        intRecNum += 1;
                                        URec.TransDetailID = FCConvert.ToString(lastID) + "P" + FCConvert.ToString(intRecNum);
                                    }

                                    strLine = BuildApplied(URec.AppliedID, arDblAmount[3], dblCurBal, ref URec);

                                    if (strLine != "")
                                    {
                                        tOut.WriteLine(strLine);
                                    }
                                }

                                rsPayments.MoveNext();
                            }
                        }
                    }

                    tBillRec.MoveNext();
                }

                tOut.Close();
                if (this.ProcessingFinished != null) this.ProcessingFinished();
                App.DoEvents();

                return ExportBill;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                if (boolFileOpen)
                {
                    tOut.Close();
                }

                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In ExportBill", MsgBoxStyle.Critical, "Error");
            }
            finally
            {
                 rsPayments.DisposeOf();
                 rsLien.DisposeOf();
                 rsTemp.DisposeOf();
            }
			return ExportBill;
		}
		// VBto upgrade warning: lngID As int	OnWrite(string)
		private string GetFakeID(int lngID, string strType)
		{
			string GetFakeID = "";
			string strReturn = "";
			if (Strings.LCase(strType) == "int")
			{
				strReturn = FCConvert.ToString(lngID) + "i";
				// strReturn = lngID & "000000"
			}
			else if ((Strings.LCase(strType) == "fee") || (Strings.LCase(strType) == "cost"))
			{
				strReturn = FCConvert.ToString(lngID) + "c";
				// strreturn = lngID & "0000000000"
			}
			GetFakeID = strReturn;
			return GetFakeID;
		}
		// VBto upgrade warning: dblAmt As double	OnWrite(double, string)
		private string BuildApplied(string strID, double dblAmt, double dblBal, ref UnifundFormat tRec)
		{
			string BuildApplied = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strReturn;
				strReturn = "";
				if (dblAmt != 0)
				{
					if (tRec.TransKind == "a")
					{
						tRec.Amount = dblAmt;
						tRec.AppliedAMT = dblAmt;
						tRec.AppliedID = strID;
					}
					else
					{
						tRec.AppliedAMT = 0;
						tRec.AppliedID = FCConvert.ToString(0);
					}
					tRec.Balance = dblBal;
					strReturn = BuildLine(ref tRec);
				}
				BuildApplied = strReturn;
				return BuildApplied;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In BuildApplied", MsgBoxStyle.Critical, "Error");
			}
			return BuildApplied;
		}
	}
}
