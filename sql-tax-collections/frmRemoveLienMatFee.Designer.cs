﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRemoveLienMatFee.
	/// </summary>
	partial class frmRemoveLienMatFee : BaseForm
	{
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblyearTtitle;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel lblDate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemoveLienMatFee));
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.lblyearTtitle = new fecherFoundation.FCLabel();
			this.lblAccountTitle = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 367);
			this.BottomPanel.Size = new System.Drawing.Size(380, 10);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.cmbYear);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.lblDate);
			this.ClientArea.Controls.Add(this.lblyearTtitle);
			this.ClientArea.Controls.Add(this.lblAccountTitle);
			this.ClientArea.Size = new System.Drawing.Size(380, 307);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(380, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(313, 30);
			this.HeaderText.Text = "Remove Lien Maturity Fees";
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.Enabled = false;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(157, 149);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(153, 40);
			this.cmbYear.TabIndex = 4;
			this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.Location = new System.Drawing.Point(157, 89);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(153, 40);
			this.txtAccount.TabIndex = 2;
			this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
			this.txtAccount.TextChanged += new System.EventHandler(this.txtAccount_TextChanged);
			// 
			// lblyearTtitle
			// 
			this.lblyearTtitle.AutoSize = true;
			this.lblyearTtitle.Location = new System.Drawing.Point(30, 163);
			this.lblyearTtitle.Name = "lblyearTtitle";
			this.lblyearTtitle.Size = new System.Drawing.Size(41, 16);
			this.lblyearTtitle.TabIndex = 3;
			this.lblyearTtitle.Text = "YEAR";
			this.lblyearTtitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.AutoSize = true;
			this.lblAccountTitle.Location = new System.Drawing.Point(30, 103);
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Size = new System.Drawing.Size(69, 16);
			this.lblAccountTitle.TabIndex = 1;
			this.lblAccountTitle.Text = "ACCOUNT";
			this.lblAccountTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDate
			// 
			this.lblDate.Location = new System.Drawing.Point(30, 30);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(332, 39);
			this.lblDate.TabIndex = 0;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 239);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(157, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save & Preview";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmRemoveLienMatFee
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(380, 377);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmRemoveLienMatFee";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Remove Lien Maturity Fees";
			this.Load += new System.EventHandler(this.frmRemoveLienMatFee_Load);
			this.Activated += new System.EventHandler(this.frmRemoveLienMatFee_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRemoveLienMatFee_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
