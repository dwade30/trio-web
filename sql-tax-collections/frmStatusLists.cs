﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.Drawing;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmStatusLists.
	/// </summary>
	public partial class frmStatusLists : BaseForm
	{
        private StatusListViewModel viewModel;
		private frmStatusLists()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmStatusLists(StatusListViewModel model)
        {
            viewModel = model;
            InitializeComponent();
            InitializeComponentEx();
        }

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmStatusLists InstancePtr
		{
			get
			{
				return (frmStatusLists)Sys.GetInstance(typeof(frmStatusLists));
			}
		}

		protected frmStatusLists _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		string strYearChoice = "";
		bool boolLoaded;
		string strGridToolTipText = "";
		bool blnSearchValid;
		// these will be to pass the string on to the reports when needed
		public string strRSWhere = "";
		public string strRSFrom = "";
		public string strRSOrder = "";
		public string strBinaryWhere = "";
		public int lngMax;
		public bool boolFullStatusAmounts;
		public bool boolShowCurrentInterest;
		public bool boolShowLocation;
		public bool boolShowMapLot;
		public bool boolShowAddress;
		public int intShowOwnerType;
		public int intMasterReportType;
		public bool boolShowPaymentBreakdown;
		public bool boolPreLienOnly;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			//// WAS PREVIOUSELY SAVED
			//string strTemp = "";
			//int intCT;
			//int intStart = 0;
			//if (cmbReport.SelectedIndex == 2)
			//{
   //             DeleteReport(cboSavedReport.SelectedIndex);                
   //         }
			//else if (cmbReport.SelectedIndex == 1)
			//{
			//	// chekc to make sure that a report was selected
			//	if (cboSavedReport.SelectedIndex < 0)
			//		return;
   //             // show the report
   //             var savedReport = viewModel.GetStatusReport(cboSavedReport.ItemData(cboSavedReport.SelectedIndex));				
			//	if (savedReport != null)
			//	{
   //                 // this will make the format of the report correct
   //                 // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
   //                 modCustomReport.Statics.strReportType = savedReport.Type;
			//		modCustomReport.SetFormFieldCaptions(this, modCustomReport.Statics.strReportType);
			//		if (savedReport.ShowSummaryOnly.Value)
			//		{
			//			chkSummaryOnly.CheckState = Wisej.Web.CheckState.Checked;
			//		}
			//		else
			//		{
			//			chkSummaryOnly.CheckState = Wisej.Web.CheckState.Unchecked;
			//		}
			//		if (savedReport.ShowPayments.HasValue ? savedReport.ShowPayments.Value : false)
			//		{
			//			chkShowPayments.CheckState = Wisej.Web.CheckState.Checked;
			//		}
			//		else
			//		{
			//			chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
			//		}
			//		if (savedReport.ShowCurrentInterest.HasValue ? savedReport.ShowCurrentInterest.Value:false)
			//		{
			//			chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
			//		}
			//		else
			//		{
			//			chkUseFullStatus.CheckState = Wisej.Web.CheckState.Unchecked;
			//		}
			//		if (savedReport.ExcludePaid.HasValue ? savedReport.ExcludePaid.Value:false)
			//		{
			//			chkExcludePaid.CheckState = Wisej.Web.CheckState.Checked;
			//		}
			//		else
			//		{
			//			chkExcludePaid.CheckState = Wisej.Web.CheckState.Unchecked;
			//		}
   //                 // select all of the fields selected in the where list
   //                 strTemp = savedReport.WhereSelection;
			//		for (intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
			//		{
			//			if (Strings.Mid(strTemp, intCT + 1, 1) == "1")
			//			{
			//				lstFields.SetSelected(intCT, true);
			//			}
			//			else
			//			{
			//				lstFields.SetSelected(intCT, false);
			//			}
			//		}
			//		// clear the sort list
			//		lstSort.Clear();
   //                 // load all of the selected fields in order
   //                 strTemp = savedReport.SortSelection;
			//		if (strTemp.Length > 0)
			//		{
			//			intCT = 0;
			//			do
			//			{
			//				intStart = intCT + 1;
			//				intCT = Strings.InStr(intStart, strTemp, ",");
			//				if (intCT > intStart)
			//				{
			//					// intStart will be the index of the next sort
			//					intStart = Convert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, intStart, intCT - 1)));
			//				}
			//				else
			//				{
			//					intStart = Convert.ToInt32(Math.Round(Conversion.Val(Strings.Right(strTemp, strTemp.Length - (intStart - 1))));
			//				}
			//				// add the item to the list
			//				if (modCustomReport.Statics.strCaptions[intStart] != "")
			//				{
			//					lstSort.AddItem(modCustomReport.Statics.strCaptions[intStart]);
			//					// enter the index in the item data
			//					lstSort.ItemData(lstSort.NewIndex, intStart);
			//					lstSort.SetSelected(lstSort.NewIndex, true);
			//					intStart = intCT + 1;
			//				}
			//			}
			//			while (!(intCT == 0));
			//		}
			//		// load the rest of the fields that are not selected
			//		for (intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
			//		{
			//			if (modCustomReport.Statics.strCaptions[intCT] != "")
			//			{
			//				if (ItemInSortList(intCT))
			//				{
			//					// do nothing
			//				}
			//				else
			//				{
			//					lstSort.AddItem(lstFields.Items[intCT].Text);
			//					lstSort.ItemData(lstSort.NewIndex, lstFields.ItemData(intCT));
			//				}
			//			}
			//		}
			//		// load the contraints
			//		for (intCT = 0; intCT <= vsWhere.Rows - 1; intCT++)
			//		{
			//			if (intCT < 10)
			//			{
			//				// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
			//				if (Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||") != 0)
			//				{
			//					// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
			//					// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
			//					vsWhere.TextMatrix(intCT, 1, Strings.Left(FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||") - 1));
			//					// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
			//					// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
			//					// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
			//					strTemp = Strings.Right(FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))).Length - Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||") - 2);
			//					strTemp = strTemp.Replace("|", "");
			//					vsWhere.TextMatrix(intCT, 2, strTemp);
			//				}
			//				else
			//				{
			//					vsWhere.TextMatrix(intCT, 1, "");
			//					vsWhere.TextMatrix(intCT, 2, "");
			//				}
			//			}
			//		}
			//	}
			//	else
			//	{
			//		FCMessageBox.Show("There was an error while opening this file.", MsgBoxStyle.Critical, "Load Error");
			//	}
			//	cmbReport.SelectedIndex = 0;
			//	// set it back to create
			//}
		}

		private void chkHardCode_Click(object sender, System.EventArgs e)
		{
			//int intCT;
			//vsWhere.TextMatrix(6, 0, "Show Payment From");
			//if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
			//{
			//	// this turns the hard coded report on
			//	chkUseFullStatus.Enabled = true;
			//	fcLabel1.Enabled = true;
			//	cmbHardCode.Enabled = true;
			//	chkREPPHardCoded.Enabled = true;
			//	chkShowPayments.Enabled = false;
			//	chkExcludePaid.Enabled = false;
			//	chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
			//	chkExcludePaid.CheckState = Wisej.Web.CheckState.Unchecked;
			//	fraSave.Enabled = false;
			//	cmbReport.Enabled = false;
			//	// chkCurrentInterest.Enabled = False
			//	// chkUseFullStatus.Enabled = False
			//	// chkUseFullStatus.Value = Wisej.Web.CheckState.Unchecked
			//	cmbNameOption.SelectedIndex = 1;
			//	cmbNameOption.Enabled = false;
			//	cmdAdd.Enabled = false;
			//	if (cmbHardCode.Items.Count > 0)
			//	{
			//		// this should select the first item in the combo box
			//		cmbHardCode.SelectedIndex = 0;
			//	}
			//	fcLabel3.Enabled = false;
			//	lstSort.Enabled = false;
			//	for (intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
			//	{
			//		lstFields.SetSelected(intCT, false);
			//		// deselect all of the boxes
			//	}
			//	fcLabel2.Enabled = false;
			//	lstFields.Enabled = false;
			//	// set the grid properties because some of the options do not work with hard coded reports
			//	// grey some of the boxes out
			//	// 3 - Balance Due
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//	// 6 - Payments From
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 6, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//	// MAL@20080813: Activate Tax Acquired option for all hard coded reports
			//	// Tracker Reference: 11805
			//	// 8 - Tax Acquired
			//	// vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 2) = TRIOCOLORGRAYBACKGROUND
			//	// 10- Lien or Regular
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 10, 1, 10, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//}
			//else
			//{
			//	// this turns the hard coded reports off
			//	fcLabel1.Enabled = false;
			//	cmbHardCode.Enabled = false;
			//	cmbHardCode.SelectedIndex = -1;
			//	chkREPPHardCoded.Enabled = false;
			//	fraSave.Enabled = true;
			//	chkShowPayments.Enabled = true;
			//	chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
			//	chkUseFullStatus.Enabled = false;
			//	chkExcludePaid.Enabled = true;
			//	cmbReport.Enabled = true;
			//	cmdAdd.Enabled = true;
			//	fcLabel3.Enabled = true;
			//	lstSort.Enabled = true;
			//	fcLabel2.Enabled = true;
			//	lstFields.Enabled = true;
			//	cmbNameOption.Enabled = true;
			//	// chkCurrentInterest.Enabled = True
			//	// chkUseFullStatus.Enabled = True
			//	// set the grid properties
			//	// set the background to white again
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 1, 1, System.Drawing.Color.White);
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, 3, 2, System.Drawing.Color.White);
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 6, 2, System.Drawing.Color.White);
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 1, System.Drawing.Color.White);
			//	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 10, 1, 10, 1, System.Drawing.Color.White);
			//}
			//ShowAutomaticFields();
		}

		private void chkHardCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (Convert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
				{
					chkHardCode.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				else
				{
					chkHardCode.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkUseFullStatus_Click(object sender, System.EventArgs e)
		{
			// If chkUseFullStatus.Value Then
			// chkCurrentInterest.Enabled = True
			// Else
			// chkCurrentInterest.Enabled = False
			// End If
		}

		private void cmbHardCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ShowAutomaticFields();
		}

		private void cmbNameOption_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbNameOption.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbNameOption_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (Convert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Space)
			{
				if (modAPIsConst.SendMessageByNum(cmbNameOption.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
				{
					modAPIsConst.SendMessageByNum(cmbNameOption.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
					KeyCode = 0;
				}
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			//// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			//// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			//// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			//// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			//// IMPORTANT
			//string strReturn;
			//clsDRWrapper rsSave = new clsDRWrapper();
			//int intRow/*unused?*/;
			//int intCol/*unused?*/;
			//clsDRWrapper RSLayout = new clsDRWrapper();
			//string strTemp = "";
			//int intCT;
			//// If SaveAnswers Then
			//strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report");
			//strReturn = modGlobalFunctions.RemoveApostrophe(strReturn);
			//if (strReturn == string.Empty)
			//{
			//	// DO NOT SAVE REPORT
			//}
			//else
			//{
			//	// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
			//	// NOT SHOW IT
			//	boolSaveReport = true;
			//	cmdPrint_Click();
			//	boolSaveReport = false;
			//	// SAVE THE REPORT
			//	rsSave.OpenRecordset("SELECT * FROM SavedStatusReports WHERE ReportName = '" + strReturn + "' and Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DEFAULTDATABASE);
			//	if (!rsSave.EndOfFile())
			//	{
			//		FCMessageBox.Show("A report by that name already exists. A different name must be selected.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
			//		return;
			//	}
			//	else
			//	{
			//		rsSave.AddNew();
			//		rsSave.Set_Fields("ReportName", strReturn);
			//		rsSave.Set_Fields("Type", modCustomReport.Statics.strReportType);
			//		rsSave.Set_Fields("ShowPayments", chkShowPayments.CheckState == Wisej.Web.CheckState.Checked);
			//		rsSave.Set_Fields("ShowCurrentInterest", chkUseFullStatus.CheckState == Wisej.Web.CheckState.Checked);
			//		rsSave.Set_Fields("ExcludePaid", chkExcludePaid.CheckState == Wisej.Web.CheckState.Checked);
			//		rsSave.Set_Fields("ShowSummaryOnly", chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked);
			//		// build where string then save it into the Where Field
			//		// this will have a 0 when a field is not added and a 1 when it is added
			//		strTemp = "";
			//		for (intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
			//		{
			//			if (lstFields.Selected(intCT))
			//			{
			//				strTemp += "1";
			//			}
			//			else
			//			{
			//				strTemp += "0";
			//			}
			//		}
			//		rsSave.Set_Fields("WhereSelection", strTemp);
			//		// create a sort selection string by creating a comma delilited string
			//		// that holds the order and the index number of each field
			//		strTemp = "";
			//		for (intCT = 0; intCT <= lstSort.Items.Count - 1; intCT++)
			//		{
			//			if (lstSort.Selected(intCT))
			//			{
			//				strTemp += FCConvert.ToString(lstSort.ItemData(intCT)) + ",";
			//			}
			//			else
			//			{
			//				// do nothing
			//			}
			//		}
			//		if (strTemp.Length > 0)
			//		{
			//			strTemp = Strings.Left(strTemp, strTemp.Length - 1);
			//			// this removes the last comma
			//		}
			//		rsSave.Set_Fields("SortSelection", strTemp);
			//		// this will fill all of the where constraints in from the grid
			//		for (intCT = 0; intCT <= vsWhere.Rows - 1; intCT++)
			//		{
			//			if (intCT < 10)
			//			{
			//				rsSave.Set_Fields("FieldConstraint" + intCT, GetWhereConstraint(intCT));
			//			}
			//		}

			//		if (rsSave.Update())
			//		{
			//			FCMessageBox.Show("Custom Report saved as " + strReturn, MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
			//		}
			//		LoadCombo();
			//	}
			//}

		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
            ClearFilters();
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			//// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			//try
			//{
			//	// On Error GoTo ERROR_HANDLER
			//	int intCT;
			//	// set the cursor to an hourglass
			//	FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			//	// check to see if the full amounts should be shown
			//	if (!(chkHardCode.CheckState == Wisej.Web.CheckState.Checked))
			//	{
			//		chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
			//	}
			//	boolFullStatusAmounts = FCConvert.CBool(chkUseFullStatus.CheckState == Wisej.Web.CheckState.Checked);
			//	chkCurrentInterest.CheckState = chkUseFullStatus.CheckState;
			//	// make them the same
			//	boolFullStatusAmounts = true;
				
			//	intShowOwnerType = cmbNameOption.ItemData(cmbNameOption.SelectedIndex);
			//	if (boolFullStatusAmounts)
			//	{
			//		boolShowCurrentInterest = FCConvert.CBool(chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked);
			//	}
			//	else
			//	{
			//		boolShowCurrentInterest = false;
			//	}
			//	// kgk trocl-766 12-15-2011  Force prelien info to be in payments detail
			//	if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(10, 1), 3)) == "PRE")
			//	{
			//		chkShowPayments.CheckState = Wisej.Web.CheckState.Checked;
			//	}
			//	boolShowPaymentBreakdown = FCConvert.CBool(chkShowPayments.CheckState == Wisej.Web.CheckState.Checked);

			//	vsWhere.Select(0, 0);

			//	if (blnSearchValid)
			//	{
			//		if (!ValidateWhereGrid())
			//		{
			//			return;
			//		}
			//		SetExtraFields();
			//		// PREPARE THE SQL TO SHOW THE REPORT
			//		if (cmbReport.SelectedIndex == 0)
			//		{
			//			// BUILD THE SQL FOR THE NEW CUSTOM REPORT
			//			if (!BuildSQL())
			//			{
			//				return;
			//			}
			//			// IF NO FIELDS WERE CHOSEN TO PRINT THEN DO NOT SHOW THE REPORT
			//			// If intNumberOfSQLFields < 0 Then GoTo NoFields
			//		}
			//		else
			//		{
			//			// IF THE USER DOES NOT CHOSE A REPORT THEN DO NOT SHOW ONE
			//			if (cboSavedReport.SelectedIndex < 0)
			//			{
			//				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			//				return;
			//			}
			//		}
			//		// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			//		// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			//		modCustomReport.GetNumberOfFields(modCustomReport.Statics.strCustomSQL);
			//		if (modCustomReport.Statics.intNumberOfSQLFields < 0)
			//		{
			//			NOFIELDS:
			//			;
			//			FCMessageBox.Show("No fields were selected to display.", MsgBoxStyle.Information, "TRIO Software");
			//			return;
			//		}
			//		// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			//		// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			//		// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			//		// Call SetColumnCaptions(Me)
			//		if (!boolSaveReport)
			//		{
			//			// SHOW THE REPORT
			//			if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
			//			{
			//				if (cmbHardCode.SelectedIndex > -1)
			//				{
			//					ShowHardCodedReport_2((short)cmbHardCode.ItemData(cmbHardCode.SelectedIndex));
			//				}
			//				else
			//				{
			//					FCMessageBox.Show("Please select a report to show.", MsgBoxStyle.Information, "No Selection");
			//					cmbHardCode.Focus();
			//				}
			//			}
			//			else
			//			{
			//				// setup the binary string that represents which fields are to be shown
			//				strBinaryWhere = "";
			//				for (intCT = 0; intCT <= lngMax; intCT++)
			//				{
			//					strBinaryWhere += "0";
			//				}
			//				if ((Strings.UCase(modCustomReport.Statics.strReportType) == "ACCOUNT") || (Strings.UCase(modCustomReport.Statics.strReportType) == "ABATE") || (Strings.UCase(modCustomReport.Statics.strReportType) == "SUPPLEMENTAL") || (Strings.UCase(modCustomReport.Statics.strReportType) == "DISCOUNT") || (Strings.UCase(modCustomReport.Statics.strReportType) == "TAXCLUB") || (Strings.UCase(modCustomReport.Statics.strReportType) == "PREPAYMENT") || (Strings.UCase(modCustomReport.Statics.strReportType) == "COSTS") || (Strings.UCase(modCustomReport.Statics.strReportType) == "REFUNDEDABATE") || (Strings.UCase(modCustomReport.Statics.strReportType) == "PAYMENTS") || (Strings.UCase(modCustomReport.Statics.strReportType) == "CORRECTIONS"))
			//				{
			//					frmReportViewer.InstancePtr.Init(arStatusLists.InstancePtr);
			//					// arStatusLists.Show , MDIParent
			//					// use the hidden form to size the report
			//				}
			//				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "TAXLIEN")
			//				{
			//					// use the hidden form to size the report
			//				}
			//				else
			//				{

			//				}
			//			}
			//		}
			//		FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			//	}
			//	return;
			//}
			//catch (Exception ex)
			//{
			//	// ERROR_HANDLER:
			//	FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			//	FCMessageBox.Show("ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, MsgBoxStyle.Critical, "Print Status Lists ERROR");
			//}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public bool BuildSQL()
		{
            bool BuildSQL = false;
            //try
            //{
            //	// On Error GoTo ERROR_HANDLER
            //	// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
            //	int intCounter;
            //	int intRow/*unused?*/;
            //	int intCol/*unused?*/;
            //	string[] strSelectedFields = new string[500 + 1];
            //	string strPaymentDateRange = "";
            //	string strDateCheck = "";
            //	if (modGlobal.Statics.gboolUseAsOfDate)
            //	{
            //		strDateCheck = " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(modGlobal.Statics.gdtStatusListAsOfDate) + "'";
            //	}
            //	else
            //	{
            //		strDateCheck = "";
            //	}
            //	// CLEAR OUT VARIABLES
            //	modCustomReport.Statics.intNumberOfSQLFields = 0;
            //	modCustomReport.Statics.strCustomSQL = string.Empty;
            //	// GET THE FIELD NAMES THAT THE USER HAS SELECTED
            //	for (intCounter = 0; intCounter <= 499; intCounter++)
            //	{
            //		strSelectedFields[intCounter] = string.Empty;
            //	}
            //	if (modGlobal.Statics.gboolSLDateRange)
            //	{
            //		strPaymentDateRange = " AND (RecordedTransactionDate >= '" + FCConvert.ToString(modGlobal.Statics.gdtSLPaymentDate1) + "' AND RecordedTransactionDate <= '" + FCConvert.ToString(modGlobal.Statics.gdtSLPaymentDate2) + "')" + strDateCheck;
            //	}
            //	else
            //	{
            //		strPaymentDateRange = strDateCheck;
            //	}
            //	// Build the SQL string from the grid
            //	// this will find out which tables to get the data from
            //	if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "")
            //	{
            //		// kk01302014 trocl-1105  Force Payment Date range to be used if no payment type is selected
            //		if (modGlobal.Statics.gboolSLDateRange)
            //		{
            //			strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code IN ('P','C','A','R','Y','U','D')" + strPaymentDateRange;
            //			modCustomReport.Statics.strReportType = "PAYMENTS";
            //		}
            //		else
            //		{
            //			strRSFrom = "SELECT Distinct BillingType, ID AS BK FROM BillingMaster";
            //			modCustomReport.Statics.strReportType = "ACCOUNT";
            //		}
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "PAYMENTS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'P'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "PAYMENTS";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "CORRECTIONS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'C'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "CORRECTIONS";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "ABATEMENTS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'A'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "ABATE";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "REFUNDED ABATEMENTS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'R'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "REFUNDEDABATE";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "PRE PAYMENTS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'Y'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "PREPAYMENT";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "TAX CLUBS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'U'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "TAXCLUB";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "SUPPLEMENTALS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'S'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "SUPPLEMENTAL";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "DISCOUNTS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE Code = 'D'" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "DISCOUNT";
            //	}
            //	else if (Strings.UCase(vsWhere.TextMatrix(4, 1)) == "LIEN / 30 DAY COSTS")
            //	{
            //		strRSFrom = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec WHERE (Code = 'L' OR Code = '3')" + strPaymentDateRange;
            //		modCustomReport.Statics.strReportType = "COSTS";
            //	}
            //	// find out which fields will be used (Maybe pass the 010101 string for the report to use)
            //	for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
            //	{
            //		if (lstFields.Selected(intCounter))
            //		{
            //			strSelectedFields[intCounter] = lstFields.Items[lstFields.SelectedIndex].Text;
            //			// If intCounter = 0 Then
            //			// strCustomSQL = strFields(intCounter)
            //			// Else
            //			// strCustomSQL = ", " & strFields(intCounter)
            //			// End If
            //			modCustomReport.Statics.intNumberOfSQLFields += 1;
            //		}
            //	}
            //	// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
            //	// strCustomSQL = "SELECT " & strCustomSQL & " " & fraFields.Tag
            //	// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
            //	strRSWhere = BuildWhereParameter();
            //	// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
            //	strRSOrder = BuildSortParameter();
            //	BuildSQL = true;
            //	return BuildSQL;
            //}
            //catch (Exception ex)
            //{
            //	// ERROR_HANDLER:
            //	BuildSQL = true;
            //	FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Building SQL");
            //}
            return BuildSQL;
        }

		public string BuildSortParameter()
		{
			string BuildSortParameter = "";
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					// it is checked
					if (strSort != " ")
						strSort += ", ";
					switch (lstSort.ItemData(intCounter))
					{
						case 0:
							{
								// kk08212015 trocls-36  Alias PPMaster.Account so we don't have ambiguity
								strSort += "Account";
								// If boolRE Then
								// strSort = strSort & "Account"
								// Else
								// strSort = strSort & "BillingMaster.Account"
								// End If
								break;
							}
						case 1:
							{
								if (intShowOwnerType == 0 || intShowOwnerType == 3)
								{
									// kk 08212015 trocls-36 PP and RE both use the CP database
									strSort += "Own1FullName";
									// If boolRE Then
									// strSort = strSort & "Own1FullName"
									// Else
									// strSort = strSort & "Name1"
									// End If
								}
								else
								{
									strSort += "Name1";
								}
								break;
							}
						case 2:
							{
								strSort += "BillingYear";
								break;
							}
					}
					//end switch
				}
			}
			if (Strings.Trim(strSort) == "")
			{
				if (intShowOwnerType == 0 || intShowOwnerType == 3)
				{
					// kk08212015 trocls-36  Alias PPMaster.Account to remove ambiguity, Use CP for PP and RE
					strSort = "Own1FullName, Account, BillingYear";
					// default sort order - Show Current Owner
					// strSort = "Own1FullName, Account, BillingYear"
					// If boolRE Then
					// strSort = "Own1FullName, Account, BillingYear"              'default sort order
					// Else
					// strSort = "Name1, BillingMaster.Account, BillingYear"              'default sort order      'MAL@20080729: Added the qualifier ; Tracker Reference: 14805
					// End If
				}
				else
				{
					// kk08212015 trocls-36  Error on BillingMaster.Account, Alias PPMaster.Account to remove ambiguity
					strSort = "Name1, Account, BillingYear";
					// default sort order - Show Billed Owner
					// strSort = "Name1, BillingMaster.Account, BillingYear"
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			BuildSortParameter = strSort;
			return BuildSortParameter;
		}

		public string BuildWhereParameter()
		{
			string BuildWhereParameter = "";
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere = "";
			//int intCounter/*unused?*/;
			//bool boolNoLien/*unused?*/;
			//// CLEAR THE VARIABLES
			//strWhere = " ";
			//// create the where string here
			//if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != "")
			//{
			//	// Account Number
			//	if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
			//	{
			//		strWhere += "Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(0, 2))) + " AND Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(0, 1)));
			//	}
			//	else
			//	{
			//		strWhere += "Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(0, 1)));
			//	}
			//}
			//else
			//{
			//	if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
			//	{
			//		strWhere += "Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(0, 2)));
			//	}
			//}
			//if (Strings.Trim(vsWhere.TextMatrix(1, 1)) != "")
			//{
			//	// Name
			//	if (Strings.Trim(vsWhere.TextMatrix(1, 1)) == "")
			//	{
			//		if (modStatusPayments.Statics.boolRE)
			//		{
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			strWhere += "Own1FullName >= '" + vsWhere.TextMatrix(1, 1) + "    ' AND Own1FullName < '" + vsWhere.TextMatrix(1, 1) + "zzzz'";
			//		}
			//		else
			//		{
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			strWhere += "Own1FullName >= '" + vsWhere.TextMatrix(1, 1) + "    ' AND Own1FullName < '" + vsWhere.TextMatrix(1, 1) + "zzzz'";
			//		}
			//	}
			//	else
			//	{
			//		// this is when they both have values in the fields and will be a range by name
			//		if (modStatusPayments.Statics.boolRE)
			//		{
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			strWhere += "Own1FullName >= '" + vsWhere.TextMatrix(1, 1) + "    ' AND Own1FullName < '" + vsWhere.TextMatrix(1, 2) + "zzzz'";
			//		}
			//		else
			//		{
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			strWhere += "Name1 >= '" + vsWhere.TextMatrix(1, 1) + "    ' AND Name1 < '" + vsWhere.TextMatrix(1, 2) + "zzzz'";
			//		}
			//	}

			//}
			//if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != "")
			//{
			//	// Tax Year
			//	if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != "")
			//	{
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		strWhere += "BillingYear >= " + modExtraModules.FormatYear(vsWhere.TextMatrix(2, 1)) + " AND BillingYear <= " + modExtraModules.FormatYear(vsWhere.TextMatrix(2, 2));
			//	}
			//	else
			//	{
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		strWhere += "BillingYear = " + modExtraModules.FormatYear(vsWhere.TextMatrix(2, 1));
			//	}
			//}
			//if (chkHardCode.CheckState == Wisej.Web.CheckState.Unchecked)
			//{
			//	if (modStatusPayments.Statics.boolRE)
			//	{
			//		if (Strings.Trim(vsWhere.TextMatrix(7, 1)) != "")
			//		{
			//			// RE Trancode
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			if (Strings.Trim(vsWhere.TextMatrix(7, 2)) != "")
			//			{
			//				strWhere += "RITranCode <= " + vsWhere.TextMatrix(7, 2) + " AND RITranCode >= " + vsWhere.TextMatrix(7, 1);
			//			}
			//			else
			//			{
			//				strWhere += "RITranCode >= " + vsWhere.TextMatrix(7, 1);
			//			}
			//		}
			//		else
			//		{
			//			if (Strings.Trim(vsWhere.TextMatrix(7, 2)) != "")
			//			{
			//				if (strWhere != " ")
			//					strWhere += " AND ";
			//				strWhere += "RITranCode <= " + vsWhere.TextMatrix(7, 2);
			//			}
			//		}
			//	}
			//	else
			//	{
			//		if (Strings.Trim(vsWhere.TextMatrix(7, 1)) != "")
			//		{
			//			// PP Trancode
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			if (Strings.Trim(vsWhere.TextMatrix(7, 2)) != "")
			//			{
			//				strWhere += "TranCode <= " + vsWhere.TextMatrix(7, 2) + " AND TranCode >= " + vsWhere.TextMatrix(7, 1);
			//			}
			//			else
			//			{
			//				strWhere += "TranCode >= " + vsWhere.TextMatrix(7, 1);
			//			}
			//		}
			//		else
			//		{
			//			if (Strings.Trim(vsWhere.TextMatrix(7, 2)) != "")
			//			{
			//				if (strWhere != " ")
			//					strWhere += " AND ";
			//				strWhere += "TranCode <= " + vsWhere.TextMatrix(7, 2);
			//			}
			//		}
			//	}
			//}
			//if (Strings.Trim(vsWhere.TextMatrix(8, 1)) != "" && chkHardCode.CheckState == Wisej.Web.CheckState.Unchecked)
			//{

			//	if (Strings.Trim(Strings.Left(vsWhere.TextMatrix(8, 1), 1)) == "N")
			//	{
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		// non tax acquired
			//		strWhere += " TaxAcquired = 0 ";
			//	}
			//	else if (Strings.Trim(Strings.Left(vsWhere.TextMatrix(8, 1), 1)) == "T")
			//	{
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		strWhere += " TaxAcquired = 1 ";
			//	}
			//}
			//if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked && cmbHardCode.SelectedIndex == 3)
			//{
			//	if (Strings.Trim(vsWhere.TextMatrix(9, 1)) != "")
			//	{
			//		// Rate Keys
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		if (Strings.Trim(vsWhere.TextMatrix(9, 2)) != "")
			//		{
			//			strWhere += "LienRec.RateKey <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 2))) + " AND LienRec.RateKey >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 1)));
			//		}
			//		else
			//		{
			//			strWhere += "LienRec.RateKey >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 1)));
			//		}
			//	}
			//	else
			//	{
			//		if (Strings.Trim(vsWhere.TextMatrix(9, 2)) != "")
			//		{
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			strWhere += "LienRec.RateKey <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 2)));
			//		}
			//	}
			//}
			//else
			//{
			//	if (Strings.Trim(vsWhere.TextMatrix(9, 1)) != "")
			//	{
			//		// Rate Keys
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		if (Strings.Trim(vsWhere.TextMatrix(9, 2)) != "")
			//		{
			//			strWhere += "RateKey <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 2))) + " AND RateKey >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 1)));
			//		}
			//		else
			//		{
			//			strWhere += "RateKey >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 1)));
			//		}
			//	}
			//	else
			//	{
			//		if (Strings.Trim(vsWhere.TextMatrix(9, 2)) != "")
			//		{
			//			if (strWhere != " ")
			//				strWhere += " AND ";
			//			strWhere += "RateKey <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(9, 2)));
			//		}
			//	}
			//}
			//if (chkHardCode.CheckState == Wisej.Web.CheckState.Unchecked)
			//{
			//	boolPreLienOnly = false;
			//	if (Strings.Left(Strings.Trim(vsWhere.TextMatrix(10, 1)), 1) == "R")
			//	{
			//		// Liened or Regular Records
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		strWhere += "LienRecordNumber = 0";
			//	}
			//	else if (Strings.Left(Strings.Trim(vsWhere.TextMatrix(10, 1)), 1) == "L")
			//	{
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		strWhere += "LienRecordNumber <> 0";
			//	}
			//	else if (Strings.Left(Strings.Trim(vsWhere.TextMatrix(10, 1)), 1) == "P")
			//	{
			//		if (strWhere != " ")
			//			strWhere += " AND ";
			//		strWhere += "LienRecordNumber <> 0";
			//		boolPreLienOnly = true;
			//	}
			//}
			
			BuildWhereParameter = strWhere;
			return BuildWhereParameter;
		}

		private void frmStatusLists_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				// load fresh
				// fraCustom.Left = (Me.Width - fraCustom.Width) / 2
				// fraCustom.Top = (Me.Height - fraCustom.Height) / 2
				// fraCustom.Visible = True
				lngMax = 11;
				// put the combobox in the first question so that the user can choose
				EnableFrames_2(true);
				App.DoEvents();
				modCustomReport.SetFormFieldCaptions(this, "ACCOUNT");
				ShowWhereQuestions();
				ShowAutomaticFields();
				boolLoaded = true;
				blnSearchValid = true;
				// MAL@20071011: Set default
			}
		}

		private void frmStatusLists_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (Convert.ToInt32(e.KeyData) / 0x10000);
			// Case vbKeyF2
			// mnuAddRow_Click
			// 
			// Case vbKeyF3
			// mnuAddColumn_Click
			// 
			// Case vbKeyF4
			// mnuDeleteRow_Click
			// 
			// Case vbKeyF5
			// mnuDeleteColumn_Click
			// 
			// Case vbKeyF10
			// mnuPrint_Click
			// 
			if (KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void frmStatusLists_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStatusLists.Icon	= "frmStatusLists.frx":0000";
			//frmStatusLists.ScaleWidth	= 9045;
			//frmStatusLists.ScaleHeight	= 7350;
			//frmStatusLists.LinkTopic	= "Form1";
			//frmStatusLists.LockControls	= -1  'True;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsWhere.BackColor	= "-2147483643";
			//			//vsWhere.ForeColor	= "-2147483640";
			//vsWhere.BorderStyle	= 1;
			//vsWhere.FillStyle	= 0;
			//vsWhere.Appearance	= 1;
			//vsWhere.GridLines	= 1;
			//vsWhere.WordWrap	= 0;
			//vsWhere.ScrollBars	= 3;
			//vsWhere.RightToLeft	= 0;
			//vsWhere._cx	= 9816;
			//vsWhere._cy	= 3734;
			//vsWhere._ConvInfo	= 1;
			//vsWhere.MousePointer	= 0;
			//vsWhere.BackColorFixed	= -2147483633;
			//			//vsWhere.ForeColorFixed	= -2147483630;
			//vsWhere.BackColorSel	= -2147483635;
			//			//vsWhere.ForeColorSel	= -2147483634;
			//vsWhere.BackColorBkg	= -2147483636;
			//vsWhere.BackColorAlternate	= -2147483643;
			//vsWhere.GridColor	= -2147483633;
			//vsWhere.GridColorFixed	= -2147483632;
			//vsWhere.TreeColor	= -2147483632;
			//vsWhere.FloodColor	= 192;
			//vsWhere.SheetBorder	= -2147483642;
			//vsWhere.FocusRect	= 1;
			//vsWhere.HighLight	= 1;
			//vsWhere.AllowSelection	= -1  'True;
			//vsWhere.AllowBigSelection	= -1  'True;
			//vsWhere.AllowUserResizing	= 0;
			//vsWhere.SelectionMode	= 0;
			//vsWhere.GridLinesFixed	= 2;
			//vsWhere.GridLineWidth	= 1;
			//vsWhere.RowHeightMin	= 0;
			//vsWhere.RowHeightMax	= 0;
			//vsWhere.ColWidthMin	= 0;
			//vsWhere.ColWidthMax	= 0;
			//vsWhere.ExtendLastCol	= -1  'True;
			//vsWhere.FormatString	= "";
			//vsWhere.ScrollTrack	= -1  'True;
			//vsWhere.ScrollTips	= 0   'False;
			//vsWhere.MergeCells	= 0;
			//vsWhere.MergeCompare	= 0;
			//vsWhere.AutoResize	= -1  'True;
			//vsWhere.AutoSizeMode	= 0;
			//vsWhere.AutoSearch	= 0;
			//vsWhere.AutoSearchDelay	= 2;
			//vsWhere.MultiTotals	= -1  'True;
			//vsWhere.SubtotalPosition	= 1;
			//vsWhere.OutlineBar	= 0;
			//vsWhere.OutlineCol	= 0;
			//vsWhere.Ellipsis	= 0;
			//vsWhere.ExplorerBar	= 0;
			//vsWhere.PicturesOver	= 0   'False;
			//vsWhere.PictureType	= 0;
			//vsWhere.TabBehavior	= 1;
			//vsWhere.OwnerDraw	= 0;
			//vsWhere.ShowComboButton	= -1  'True;
			//vsWhere.TextStyle	= 0;
			//vsWhere.TextStyleFixed	= 0;
			//vsWhere.OleDragMode	= 0;
			//vsWhere.OleDropMode	= 0;
			//vsWhere.DataMode	= 0;
			//vsWhere.VirtualData	= -1  'True;
			//vsWhere.ComboSearch	= 3;
			//vsWhere.AutoSizeMouse	= -1  'True;
			//vsWhere.AllowUserFreezing	= 0;
			//vsWhere.BackColorFrozen	= 0;
			//			//vsWhere.ForeColorFrozen	= 0;
			//vsWhere.WallPaperAlignment	= 9;
			//vsQuestions.BackColor	= "16777215";
			//			//vsQuestions.ForeColor	= "-2147483640";
			//vsQuestions.BorderStyle	= 1;
			//vsQuestions.FillStyle	= 0;
			//vsQuestions.Appearance	= 1;
			//vsQuestions.GridLines	= 1;
			//vsQuestions.WordWrap	= 0;
			//vsQuestions.ScrollBars	= 3;
			//vsQuestions.RightToLeft	= 0;
			//vsQuestions._cx	= 2566;
			//vsQuestions._cy	= 2351;
			//vsQuestions._ConvInfo	= 1;
			//vsQuestions.MousePointer	= 0;
			//vsQuestions.BackColorFixed	= -2147483633;
			//			//vsQuestions.ForeColorFixed	= -2147483630;
			//vsQuestions.BackColorSel	= -2147483635;
			//			//vsQuestions.ForeColorSel	= -2147483634;
			//vsQuestions.BackColorBkg	= -2147483636;
			//vsQuestions.BackColorAlternate	= 16777215;
			//vsQuestions.GridColor	= -2147483633;
			//vsQuestions.GridColorFixed	= -2147483632;
			//vsQuestions.TreeColor	= -2147483632;
			//vsQuestions.FloodColor	= 192;
			//vsQuestions.SheetBorder	= -2147483642;
			//vsQuestions.FocusRect	= 1;
			//vsQuestions.HighLight	= 1;
			//vsQuestions.AllowSelection	= -1  'True;
			//vsQuestions.AllowBigSelection	= -1  'True;
			//vsQuestions.AllowUserResizing	= 0;
			//vsQuestions.SelectionMode	= 0;
			//vsQuestions.GridLinesFixed	= 2;
			//vsQuestions.GridLineWidth	= 1;
			//vsQuestions.RowHeightMin	= 0;
			//vsQuestions.RowHeightMax	= 0;
			//vsQuestions.ColWidthMin	= 0;
			//vsQuestions.ColWidthMax	= 0;
			//vsQuestions.ExtendLastCol	= -1  'True;
			//vsQuestions.FormatString	= "";
			//vsQuestions.ScrollTrack	= -1  'True;
			//vsQuestions.ScrollTips	= 0   'False;
			//vsQuestions.MergeCells	= 0;
			//vsQuestions.MergeCompare	= 0;
			//vsQuestions.AutoResize	= -1  'True;
			//vsQuestions.AutoSizeMode	= 0;
			//vsQuestions.AutoSearch	= 0;
			//vsQuestions.AutoSearchDelay	= 2;
			//vsQuestions.MultiTotals	= -1  'True;
			//vsQuestions.SubtotalPosition	= 1;
			//vsQuestions.OutlineBar	= 0;
			//vsQuestions.OutlineCol	= 0;
			//vsQuestions.Ellipsis	= 0;
			//vsQuestions.ExplorerBar	= 0;
			//vsQuestions.PicturesOver	= 0   'False;
			//vsQuestions.PictureType	= 0;
			//vsQuestions.TabBehavior	= 0;
			//vsQuestions.OwnerDraw	= 0;
			//vsQuestions.ShowComboButton	= -1  'True;
			//vsQuestions.TextStyle	= 0;
			//vsQuestions.TextStyleFixed	= 0;
			//vsQuestions.OleDragMode	= 0;
			//vsQuestions.OleDropMode	= 0;
			//vsQuestions.ComboSearch	= 3;
			//vsQuestions.AutoSizeMouse	= -1  'True;
			//vsQuestions.AllowUserFreezing	= 0;
			//vsQuestions.BackColorFrozen	= 0;
			//			//vsQuestions.ForeColorFrozen	= 0;
			//vsQuestions.WallPaperAlignment	= 9;
			//Images.NumListImages	= 1;
			//vsElasticLight1.OleObjectBlob	= "frmStatusLists.frx":058A";
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			this.Text = "Status Lists";
			this.HeaderText.Text = this.Text;
			// kgk    CheckReportTable
			// fill the hard coded combo list with the completed reports
			FillHardCodeCombo();
			// create the strYearChoice string for the year combo box question
			// FindAllYears
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//txtNotes.BackColor = this.BackColor;
			FormatGrid();
			// txtAsOfDate.Text = Format(Date, "MM/dd/yyyy")
			modGlobal.Statics.gdtStatusListAsOfDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy") + " 11:59:59 PM");
			// set the default as of date
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION         FIX THIS!!!
			// Image1.Stretch = True
			// vsLayout.rows = 2
			// vsLayout.Cols = 1
			// Image1.Width = 1440 * 14
			// Image1.Picture = ImageList1.ListImages(1).Picture
			// HScroll1.Min = 0
			// If Image1.Width > Frame1.Width Then
			// HScroll1.Max = Image1.Width - Frame1.Width
			// Else
			// HScroll1.Enabled = False
			// End If
			// 
			// vsLayout.Width = Image1.Width - 720
			// vsLayout.MergeCells = flexMergeFree
			// vsLayout.MergeRow(1) = True
			// vsLayout.Left = Image1.Left + 720
			// Line1.X1 = Image1.Left + (1440 * 8) - 900
			// Line1.X2 = Image1.Left + (1440 * 8) - 900
			// Line1.Y2 = vsLayout.Top + vsLayout.Height
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("SELECT * FROM SavedStatusReports WHERE Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DEFAULTDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, Convert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// If lstFields.ListIndex < 0 Then Exit Sub
			// If vsLayout.Row < 1 Then vsLayout.Row = 1
			// 
			// vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col) = lstFields.List(lstFields.ListIndex)
			// vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col) = lstFields.ItemData(lstFields.ListIndex)
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optReport_Click(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			cboSavedReport.Visible = (Index == 1) || (Index == 2);
			cmdAdd.Visible = Index == 0;
			//FC:FINAL:CHN: Add missing changing enabling.
			fcLabel3.Enabled = Index == 0;
			lstSort.Enabled = fcLabel3.Enabled;
			fcLabel2.Enabled = Index == 0;
			lstFields.Enabled = fcLabel2.Enabled;
			fraWhere.Enabled = Index == 0;
			fraQuestions.Enabled = Index == 0;
		}

		private void optReport_Click(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_Click(index, sender, e);
		}

		private void vsQuestions_Click(object sender, EventArgs e)
		{
			vsQuestions_RowColChange(vsQuestions, EventArgs.Empty);
		}

		private void vsQuestions_DblClick(object sender, EventArgs e)
		{
			vsQuestions_RowColChange(vsQuestions, EventArgs.Empty);
		}

		private void vsQuestions_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space)
			{
				vsQuestions_RowColChange(vsQuestions, EventArgs.Empty);
			}
			else if (e.KeyCode == Keys.Return)
			{
				vsQuestions_Validate_2(false);
			}
		}

		private void vsQuestions_RowColChange(object sender, EventArgs e)
		{
			if (vsQuestions.Col == 1)
			{
				vsQuestions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				switch (vsQuestions.Row)
				{
					case 0:
						{
							// Type of Report
							vsQuestions.ComboList = "#0;Account Listing|#1;Pre Payments|#2;Tax Club Payments|#3;Outstanding Tax Lien|#4;Accounts with Abatements";
							break;
						}
					case 1:
						{
							// Naming Convention
							vsQuestions.ComboList = "#0;Name on Account at time of Billing|#1;Current Property Owner|#2;Name on Account at time of Billing with 'C/O'|#3;Current Property Owner with Previous Owner";
							break;
						}
					case 2:
						{
							// Payment Summary
							vsQuestions.ComboList = "#0;Yes|#1;No";
							break;
						}
					default:
						{
							vsQuestions.Editable = FCGrid.EditableSettings.flexEDNone;
							vsQuestions.ComboList = "";
							break;
						}
				}
				//end switch
				// check to see if this is the last row shown
				if (vsQuestions.Row > 0)
				{
					// if it is not the first question
					// If .Row = 4 Then                                'is it the last row
					// .TabBehavior = flexTabControls
					// Else                                            'it is not the last row
					// If .RowHeight(4) = 0 Then                   'is the last row hidden
					// If .Row = 3 Then                        'if so, is it the next to last
					// .TabBehavior = flexTabControls
					// Else
					// If .RowHeight(3) = 0 Then
					if (vsQuestions.Row == 2)
					{
						vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						if (vsQuestions.RowHeight(2) == 0)
						{
							if (vsQuestions.Row == 1)
							{
								vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							}
							else
							{
								vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
							}
						}
						else
						{
							vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
						}
					}
					// Else
					// .TabBehavior = flexTabCells
					// End If
					// End If
					// Else
					// .TabBehavior = flexTabCells
					// End If
					// End If
				}
			}
			else
			{
				vsQuestions.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsQuestions_Validate_2(bool Cancel)
		{
			vsQuestions_Validate(ref Cancel);
		}

		private void vsQuestions_Validate(ref bool Cancel)
		{
			int lngRow;
			for (lngRow = 0; lngRow <= 2; lngRow++)
			{
				if (Conversion.Val(vsQuestions.ComboData()) < 0 && Conversion.Val(vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1)) != ColorTranslator.ToOle(vsQuestions.BackColorFixed))
				{
					FCMessageBox.Show("Please answer all of the questions.", MsgBoxStyle.Critical, "Insufficient Data");
					Cancel = true;
					return;
				}
			}
			if (vsQuestions.RowHeight(0) > 0 && Conversion.Val(vsQuestions.ComboData()) > 0 && Convert.ToBoolean(Conversion.Val(FCConvert.ToString(vsQuestions.RowHeight(1) > 0 || vsQuestions.RowHeight(2) > 0))))
			{
				if (fcLabel3.Enabled == false)
				{
					// if it makes it this far, then the user has answered all of the questions needed for this report
					// so we can enable the other frame and show the data at the bottom
					EnableFrames_2(true);
					ShowWhereQuestions();
				}
				// Else
				// MsgBox "Please answer all of the questions.", MsgBoxStyle.Critical, "Insufficient Data"
				// Cancel = True
				// Exit Sub
			}
		}

		private void vsQuestions_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsQuestions.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsQuestions.GetFlexRowIndex(e.RowIndex);
				int col = vsQuestions.GetFlexColIndex(e.ColumnIndex);
				if (col == 1)
				{
					vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, vsQuestions.ComboData(row));
					switch (row)
					{
						case 0:
							{
								// Type of Report
								ChangeReportType(Convert.ToInt32(vsQuestions.ComboData()));
								break;
							}
					}
					//end switch
				}
				else
				{
					// do nothing
				}
			}
		}


		



		private void EnableFrames_2(bool boolEnable)
		{
			EnableFrames(ref boolEnable);
		}

		private void EnableFrames(ref bool boolEnable)
		{
			fcLabel2.Enabled = boolEnable;
			// fraNotes.Enabled = boolEnable
			fcLabel3.Enabled = boolEnable;
			// fraSave.Enabled = boolEnable
			fraWhere.Enabled = boolEnable;
			lstFields.Enabled = boolEnable;
			// lstFields.Enabled = False
			// fraFields.Enabled = False
			lstSort.Enabled = boolEnable;
			// fraQuestions.Enabled = Not boolEnable
		}

		private void FormatGrid()
		{
			int lngWidth = 0;
			string strTemp = "";
			// this will format the Questions Grid and add the questions and answers to it
			lngWidth = vsQuestions.WidthOriginal;
			vsQuestions.Cols = 2;
			vsQuestions.ColWidth(0, Convert.ToInt32(lngWidth * 0.8));
			vsQuestions.ColWidth(1, Convert.ToInt32(lngWidth * 0.17));
			vsQuestions.Rows = 4;
			vsQuestions.TextMatrix(0, 0, "Which type of report would you like to create?");
			// if selection for question 1 is #1,2,3,4,5,7 then
			vsQuestions.TextMatrix(1, 0, "How would you like to show the name?");
			// if selection for question 1 is #6 then
			vsQuestions.TextMatrix(2, 0, "Complete file?");
			// hide the other questions
			//FC:FINAL:MHO:#i182 - Hide the row instead of setting its height to 0
			//vsQuestions.RowHeight(1, 0);
			//vsQuestions.RowHeight(2, 0);
			vsQuestions.RowHidden(1, true);
			vsQuestions.RowHidden(2, true);
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//vsQuestions.HeightOriginal = vsQuestions.RowHeight(0) + 50;
			// 
			// strTemp = "1. Choose the type of report to create from the drop down box at the top of the screen or Load a Saved Report from the drop down boxes at the right of the screen." & vbCrLf & vbCrLf
			// strTemp = strTemp & "2. Answer all of the questions about the report that you have chosen." & vbCrLf & vbCrLf
			// strTemp = strTemp & "3. Choose the fields that you would like shown from the 'Fields to display on Report' list." & vbCrLf & vbCrLf
			// strTemp = strTemp & "4. Choose the fields that you would like the report sorted by from the 'Fields to sort by' list." & vbCrLf & vbCrLf
			// strTemp = strTemp & "5. Set any criteria needed in the 'Select Search Criteria' list." & vbCrLf
			// strTemp = strTemp & vbCrLf & "Some fields will be printed automatically:" & vbCrLf
			// txtNotes.Text = strTemp
			strTemp = "1. Create a new report, a default report or select a saved report from the drop down box on the right of the screen." + "\r\n" + "\r\n";
			strTemp += "2. Choose the fields that you would like on your report from the Fields To Display On Report." + "\r\n" + "\r\n";
			strTemp += "3. The Fields To Sort By section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
			strTemp += "4. Set any criteria needed in the Select Search Criteria list." + "\r\n" + "\r\n";
			strTemp += "Other Notes:" + "\r\n" + "Some fields will be printed automatically." + "\r\n" + "\r\n";
			strTemp += "\r\n" + "If you choose a default report, only the account number and the tax year criteria will affect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			//txtNotes.Text = strTemp;
		}
		// VBto upgrade warning: intType As int	OnWrite(string)
		private void ChangeReportType(int intType)
		{
			int lngWid = 0;
			int intRows = 0;
			switch (intType)
			{
				case 0:
					{
						// Account List
						ChangeQuestionStatus_8(false, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 1;
						modCustomReport.SetFormFieldCaptions(this, "ACCOUNT");
						break;
					}
				case 1:
					{
						// Pre Payments
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
						modCustomReport.SetFormFieldCaptions(this, "PREPAYMENTS");
						break;
					}
				case 2:
					{
						// Tax Club
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
						modCustomReport.SetFormFieldCaptions(this, "TAXCLUB");
						break;
					}
				case 3:
					{
						// Outstanding Tax Lien
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
						modCustomReport.SetFormFieldCaptions(this, "TAXLIEN");
						break;
					}
				case 4:
					{
						// Accounts with Abatements
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
						modCustomReport.SetFormFieldCaptions(this, "ABATE");
						break;
					}
				default:
					{
						ChangeQuestionStatus_8(false, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 1;
						lstFields.Clear();
						lstSort.Clear();
						break;
					}
			}
			//end switch
			ShowWhereQuestions();
			// set the height
			//vsQuestions.Height = vsQuestions.RowHeight(0) * intRows + 70;
		}

		private void ChangeQuestionStatus_8(bool boolReady, int lngRow)
		{
			ChangeQuestionStatus(ref boolReady, ref lngRow);
		}

		private void ChangeQuestionStatus(ref bool boolReady, ref int lngRow)
		{
			// this will enable/disable the questions in the row depending on what the user selects
			// change the background of the cell in the grid
			// maybe resize the row to height of 0
			if (boolReady)
			{
				vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, 1, vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1));
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//vsQuestions.RowHeight(lngRow, vsQuestions.RowHeight(0));
			}
			else
			{
				vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, 1, vsQuestions.BackColorFixed);
				//vsQuestions.RowHeight(lngRow, 0);
			}
		}

		private void FindAllYears()
		{
			// this will fill the variable strYearChoice with all of the possible years and an All Years Option
			clsDRWrapper rsYear = new clsDRWrapper();
			int intNum = 0;
			rsYear.OpenRecordset("SELECT * FROM PaymentYears", modExtraModules.strCLDatabase);
			// rsYear.OpenRecordset "SELECT DISTINCT [Year] FROM PaymentRec", strCLDATABASE
			if (rsYear.EndOfFile() != true && rsYear.BeginningOfFile() != true)
			{
				strYearChoice = "";
				intNum = 1;
				strYearChoice = "#0;All Years|";
				while (!rsYear.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					strYearChoice += "#" + FCConvert.ToString(intNum) + ";" + modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields("Year"))) + "|";
					intNum += 1;
					rsYear.MoveNext();
				}
				// take off the last pipe character '|'
				strYearChoice = Strings.Left(strYearChoice, strYearChoice.Length - 1);
			}
			else
			{
				strYearChoice = "#0;All Years";
			}
		}

		private void ShowWhereQuestions()
		{


		}

		private void CheckReportTable()
		{
			// Dim rsCreateTable As New clsDRWrapper
			// 
			// With rsCreateTable
			// CREATE A NEW TABLE IF IT DOESN'T EXIST
			// If .CreateNewDatabaseTable("SavedStatusReports", DEFAULTDATABASE) Then
			// CREATE THE NEW FIELDS
			// .CreateTableField "ID", dbLong
			// .CreateTableField "ReportName", dbText
			// .CreateTableField "Type", dbText
			// .CreateTableField "SQL", dbMemo
			// .CreateTableField "LastUpdated", dbDate
			// 
			// .CreateTableField "WhereSelection", dbText
			// .CreateTableField "SortSelection", dbText
			// .CreateTableField "FieldConstraint0", dbText
			// .CreateTableField "FieldConstraint1", dbText
			// .CreateTableField "FieldConstraint2", dbText
			// .CreateTableField "FieldConstraint3", dbText
			// .CreateTableField "FieldConstraint4", dbText
			// .CreateTableField "FieldConstraint5", dbText
			// .CreateTableField "FieldConstraint6", dbText
			// .CreateTableField "FieldConstraint7", dbText
			// .CreateTableField "FieldConstraint8", dbText
			// .CreateTableField "FieldConstraint9", dbText
			// 
			// SET THE PROPERTIES OF THE NEW FIELDS
			// .SetFieldAttribute "ID", dbAutoIncrField
			// .SetFieldDefaultValue "LastUpdated", "NOW()"
			// .SetFieldAllowZeroLength "ReportName", True
			// .SetFieldAllowZeroLength "Type", True
			// .SetFieldAllowZeroLength "SQL", True
			// 
			// .SetFieldAllowZeroLength "WhereSelection", True
			// .SetFieldAllowZeroLength "SortSelection", True
			// .SetFieldAllowZeroLength "FieldConstraint0", True
			// .SetFieldAllowZeroLength "FieldConstraint1", True
			// .SetFieldAllowZeroLength "FieldConstraint2", True
			// .SetFieldAllowZeroLength "FieldConstraint3", True
			// .SetFieldAllowZeroLength "FieldConstraint4", True
			// .SetFieldAllowZeroLength "FieldConstraint5", True
			// .SetFieldAllowZeroLength "FieldConstraint6", True
			// .SetFieldAllowZeroLength "FieldConstraint7", True
			// .SetFieldAllowZeroLength "FieldConstraint8", True
			// .SetFieldAllowZeroLength "FieldConstraint9", True
			// 
			// DO THE ACTUAL CREATION OF THE TABLE
			// .UpdateTableCreation
			// End If
			// End With
			// 
			// With rsCreateTable
			// CREATE A NEW TABLE IF IT DOESN'T EXIST
			// If .CreateNewDatabaseTable("tblReportLayout", DEFAULTDATABASE) Then
			// CREATE THE NEW FIELDS
			// .CreateTableField "AutoID", dbLong
			// .CreateTableField "ReportID", dbInteger
			// .CreateTableField "RowID", dbInteger
			// .CreateTableField "ColumnID", dbInteger
			// .CreateTableField "FieldID", dbInteger
			// .CreateTableField "Width", dbInteger
			// .CreateTableField "DisplayText", dbText
			// .CreateTableField "LastUpdated", dbDate
			// 
			// SET THE PROPERTIES OF THE NEW FIELDS
			// .SetFieldAttribute "AutoID", dbAutoIncrField
			// .SetFieldDefaultValue "LastUpdated", "NOW()"
			// .SetFieldAllowZeroLength "DisplayText", True
			// 
			// DO THE ACTUAL CREATION OF THE TABLE
			// .UpdateTableCreation
			// End If
			// End With
		}
		// VBto upgrade warning: intRow As short	OnWriteConvert.ToInt32(
		private string GetWhereConstraint(int intRow)
		{
			string GetWhereConstraint = "";
			//// this will go to the grid and get the constraint for that row and return it
			//// if there is none, then it will return a nullstring
			//int lngType/*unused?*/;
			//string strTemp;
			//strTemp = FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 1)) + "|||" + FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 2));
			//GetWhereConstraint = strTemp;
			return GetWhereConstraint;
		}

		private bool SaveAnswers()
		{
			bool SaveAnswers = false;
			// this will take the answers given, validate them, find out what report
			// the user wants and show the frmStatusLists screen
			string strTypeOfReport = "";
			int intQuestions/*unused?*/;
			SaveAnswers = true;
			if (Strings.Trim(vsQuestions.TextMatrix(0, 1)) != "")
			{
				strTypeOfReport = vsQuestions.TextMatrix(0, 1);
				if (strTypeOfReport == "Account Listing")
				{
					// all set, that is the only answer it needs
				}
				else
				{
					if (Strings.Trim(vsQuestions.TextMatrix(1, 1)) != "")
					{
					}
					else
					{
						FCMessageBox.Show("Please choose an answer for all of the questions.", MsgBoxStyle.Information, "Missing Answers");
						SaveAnswers = false;
						return SaveAnswers;
					}
				}
			}
			else
			{
				FCMessageBox.Show("Please choose an answer for all of the questions.", MsgBoxStyle.Information, "Missing Answers");
				SaveAnswers = false;
				return SaveAnswers;
			}
			return SaveAnswers;
		}
		// VBto upgrade warning: intCounter As short	OnWriteConvert.ToInt32(
		private bool ItemInSortList(int intCounter)
		{
			bool ItemInSortList = false;
			// this will return true if the item has already been added to the Sort List
			int intTemp;
			ItemInSortList = false;
			for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
			{
				if (lstSort.ItemData(intTemp) == intCounter)
				{
					ItemInSortList = true;
					break;
				}
			}
			return ItemInSortList;
		}

		public void LoadSortList()
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			lstSort.Clear();
			int intCounter;
			for (intCounter = 0; intCounter <= lngMax - 1; intCounter++)
			{
				if (!String.IsNullOrEmpty(modCustomReport.Statics.strCaptions[intCounter]))
				{
					lstSort.AddItem(modCustomReport.Statics.strCaptions[intCounter]);
					lstSort.ItemData(lstSort.NewIndex, intCounter);
				}
			}
		}

		private void FillHardCodeCombo()
		{
			// this will fill the hard coded combo list with the list of reports that the user can choose
			cmbHardCode.Clear();
			cmbHardCode.AddItem("Non Zero Balance on All Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 2);
			cmbHardCode.AddItem("Non Zero Balance on Non Lien Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 0);
			cmbHardCode.AddItem("Non Zero Balance on Lien Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 1);
			cmbHardCode.AddItem("Lien Breakdown");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 3);
			cmbHardCode.AddItem("Zero Balance Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 4);
			cmbHardCode.AddItem("Negative Balance Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 5);
			cmbHardCode.AddItem("Supplemental Outstanding Balance Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 6);
			cmbHardCode.AddItem("Supplemental Negative Balance Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 7);
			cmbHardCode.AddItem("Supplemental Zero Balance Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 8);
			cmbHardCode.AddItem("Outstanding Balance By Period");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 9);
			cmbHardCode.AddItem("Account Detail Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 10);
			cmbHardCode.AddItem("Audit Summary Report");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 11);
			cmbHardCode.AddItem("Supplemental Bills");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 12);
			cmbHardCode.AddItem("Non Zero Balance on All Accounts by Year");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 13);
			cmbHardCode.AddItem("Bankrupt Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 14);
			if (modStatusPayments.Statics.boolRE)
			{
				chkREPPHardCoded.Text = "Show PP Information";
			}
			else
			{
				chkREPPHardCoded.Text = "Show RE Information";
			}
			// this will also fill the name options combo
			cmbNameOption.Clear();
			cmbNameOption.AddItem("Show Current Owner");
			cmbNameOption.ItemData(cmbNameOption.NewIndex, 0);
			cmbNameOption.AddItem("Show Billed Owner");
			cmbNameOption.ItemData(cmbNameOption.NewIndex, 1);
			cmbNameOption.AddItem("Show Billed Owner C\\O Current Owner");
			cmbNameOption.ItemData(cmbNameOption.NewIndex, 2);
			cmbNameOption.AddItem("Show Current Owner Bill= Billed Owner");
			cmbNameOption.ItemData(cmbNameOption.NewIndex, 3);
			cmbNameOption.SelectedIndex = 0;
		}
		// VBto upgrade warning: intIndex As short	OnWriteConvert.ToInt32(
		private void ShowHardCodedReport_2(short intIndex)
		{
			ShowHardCodedReport(ref intIndex);
		}

		private void ShowHardCodedReport(ref short intIndex)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				switch (intIndex)
				{
					case 0:
						{
							// Non Zero Balance on Non Lien Accounts
							frmReportViewer.InstancePtr.Init(rptOutstandingBalances.InstancePtr);
							break;
						}
					case 1:
						{
							// Non Zero Balance on Lien Accounts
							frmReportViewer.InstancePtr.Init(rptOutstandingLienBalances.InstancePtr);
							break;
						}
					case 2:
						{
							// Non Zero Balance on All Accounts
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							intMasterReportType = 0;
							break;
						}
					case 3:
						{
							// Lien Breakdown Report
							frmReportViewer.InstancePtr.Init(arLienStatusReport.InstancePtr);
							break;
						}
					case 4:
						{
							// Zero Balance Report
							intMasterReportType = 4;
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							break;
						}
					case 5:
						{
							// Negative Balance Report
							intMasterReportType = 5;
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							break;
						}
					case 6:
						{
							// Supplemental Outstanding Balance Report
							intMasterReportType = 6;
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							break;
						}
					case 7:
						{
							// Supplemental Negative Balance Report
							intMasterReportType = 7;
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							break;
						}
					case 8:
						{
							// Supplemental Zero Balance Report
							intMasterReportType = 8;
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							break;
						}
					case 9:
						{
							// Outstanding Balance By Period
							frmRateRecChoice.InstancePtr.intRateType = 50;
							frmRateRecChoice.InstancePtr.Show(App.MainForm);
							break;
						}
					case 10:
						{
							intMasterReportType = 10;
							frmReportViewer.InstancePtr.Init(rptStatusListAccountDetail.InstancePtr);
							break;
						}
					case 11:
						{
							intMasterReportType = 11;
							frmReportViewer.InstancePtr.Init(rptAuditStatusAll.InstancePtr);
							break;
						}
					case 12:
						{
							intMasterReportType = 12;
							frmReportViewer.InstancePtr.Init(rptAuditStatusAll.InstancePtr);
							break;
						}
					case 13:
						{
							// Non Zero Balance on All Accounts Ordered By Year / Name
							intMasterReportType = 13;
							frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
							break;
						}
					case 14:
						{
							// Bankrupt Account Information
							intMasterReportType = 14;
							frmReportViewer.InstancePtr.Init(rptBankruptAccounts.InstancePtr);
							break;
						}
					default:
						{
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Showing Hard Coded Report");
			}
		}

		private void ShowAutomaticFields()
		{
			//string strRptType = "";
			//chkUseFullStatus.Enabled = true;
			//// MAL@20070910: Change to make the Show Current Interest require the user to purposely choose this option
			//chkUseFullStatus.CheckState = Wisej.Web.CheckState.Unchecked;
			//if (chkHardCode.CheckState != Wisej.Web.CheckState.Checked)
			//{
			//	// this is not a hard coded report
			//	chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
			//	chkUseFullStatus.Enabled = false;
			//	if (vsWhere.TextMatrix(vsWhere.Rows - 1, 1) != "")
			//	{
			//		// this is a specialty report
			//		if (vsWhere.Row == vsWhere.Rows - 1)
			//		{
			//			if (Strings.UCase(vsWhere.TextMatrix(vsWhere.Rows - 1, 1)) != Strings.UCase(vsWhere.EditText) && Strings.Trim(vsWhere.EditText) != "")
			//			{
			//				strRptType = Strings.UCase(vsWhere.EditText);
			//			}
			//			else
			//			{
			//				strRptType = Strings.UCase(vsWhere.TextMatrix(vsWhere.Rows - 1, 1));
			//			}
			//		}
			//		else
			//		{
			//			strRptType = Strings.UCase(vsWhere.TextMatrix(vsWhere.Rows - 1, 1));
			//		}
			//		if ((strRptType == "ABATEMENTS") || (strRptType == "DISCOUNTS") || (strRptType == "REFUNDED ABATEMENTS") || (strRptType == "SUPPLEMENTALS") || (strRptType == "PRE PAYMENTS") || (strRptType == "LIEN/30 DAY COSTS"))
			//		{
			//			lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Type" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Tax Due" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Payment Received" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Abatements and Adjustments" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Balance Due" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Refunded Abatements" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
			//		}
			//		else if (strRptType == "LIEN BREAKDOWN")
			//		{
			//			chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
			//			chkUseFullStatus.Enabled = false;
			//			lblShowFields.Text = "Fields automatically included on the Lien Breakdown Report:" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Principal" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Pre Lien Interest" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Costs" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Current Interest" + "\r\n";
			//			lblShowFields.Text = lblShowFields.Text + "    Total Due" + "\r\n";
			//		}
			//	}
			//	else
			//	{
			//		// this is a normal report
			//		lblShowFields.Text = "Fields automatically included on the Status List:" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Type" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Tax Due" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Payment Received" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Abatements and Adjustments" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Balance Due" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Refunded Abatements" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
			//	}
			//}
			//else
			//{
			//	// this is a hardcoded report
			//	if (cmbHardCode.SelectedIndex == -1)
			//	{
			//		lblShowFields.Text = "Please select a report from the 'Default Report' list." + "\r\n";
			//	}
			//	else
			//	{
			//		vsWhere.TextMatrix(6, 0, "Show Payment From");
			//		switch (cmbHardCode.ItemData(cmbHardCode.SelectedIndex))
			//		{
			//			case 0:
			//				{
			//					chkREPPHardCoded.Enabled = true;
			//					break;
			//				}
			//			case 3:
			//				{
			//					// Lien Breakdown
			//					chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
			//					chkUseFullStatus.Enabled = false;
			//					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 1, System.Drawing.Color.White);
			//					chkREPPHardCoded.CheckState = Wisej.Web.CheckState.Unchecked;
			//					chkREPPHardCoded.Enabled = false;
			//					break;
			//				}
			//			case 12:
			//				{
			//					// Supplemental Bills Report
			//					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 6, 2, System.Drawing.Color.White);
			//					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 1, System.Drawing.Color.White);
			//					// vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 1) = TRIOCOLORGRAYBACKGROUND
			//					vsWhere.TextMatrix(6, 0, "Supplemental Bill Date");
			//					chkREPPHardCoded.CheckState = Wisej.Web.CheckState.Unchecked;
			//					chkREPPHardCoded.Enabled = false;
			//					break;
			//				}
			//			case 13:
			//				{
			//					chkREPPHardCoded.Enabled = true;
			//					break;
			//				}
			//			default:
			//				{
			//					// MAL@20080813: Change to always be active
			//					// Tracker Reference: 11805
			//					// vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 1) = TRIOCOLORGRAYBACKGROUND
			//					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 8, 1, 8, 1, System.Drawing.Color.White);
			//					chkREPPHardCoded.CheckState = Wisej.Web.CheckState.Unchecked;
			//					chkREPPHardCoded.Enabled = false;
			//					break;
			//				}
			//		}
			//		//end switch
			//		lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Type" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Year" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Original Tax" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Payments and Adjustments" + "\r\n";
			//		lblShowFields.Text = lblShowFields.Text + "    Amount Due" + "\r\n";
			//	}
			//}
		}

		private bool ValidateWhereGrid()
		{
			bool ValidateWhereGrid = false;

			return ValidateWhereGrid;
		}

		private void SetExtraFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will check the fields list to see if any other fields should be added
				if (lstFields.Selected(0))
				{
					// check the location
					boolShowLocation = true;
				}
				else
				{
					boolShowLocation = false;
				}
				if (lstFields.Selected(1))
				{
					// check the map lot
					boolShowMapLot = true;
				}
				else
				{
					boolShowMapLot = false;
				}
				if (lstFields.Selected(2))
				{
					// check the address
					boolShowAddress = true;
				}
				else
				{
					boolShowAddress = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				boolShowLocation = false;
				boolShowMapLot = false;
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPrint_Click(sender, e);
		}

		private void cmdSearch_Click(object sender, EventArgs e)
		{
			this.mnuClear_Click(sender, e);
		}

        private void ClearFilters()
        {
            viewModel.ClearFilter();
        }

        private void DeleteReport(int selectedIndex)
        {
            if (selectedIndex < 0)
            {
                return;
            }

            if (FCMessageBox.Show("This will delete the custom report " + cboSavedReport.Items[selectedIndex].ToString() + ". Continue?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "TRIO Software") == DialogResult.Yes)
            {
                var reportID = cboSavedReport.ItemData(selectedIndex);
                viewModel.DeleteSavedStatusReport(reportID);
                LoadCombo();
                FCMessageBox.Show("Custom report deleted successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
            }
        }

        private void LoadSavedReport(int selectedIndex)
        {
            if (selectedIndex < 0)
            {
                return;
            }
            // show the report
            var savedReport = viewModel.GetStatusReport(cboSavedReport.ItemData(cboSavedReport.SelectedIndex));
            if (savedReport != null)
            {
                // this will make the format of the report correct
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                modCustomReport.Statics.strReportType = savedReport.Type;
                modCustomReport.SetFormFieldCaptions(this, modCustomReport.Statics.strReportType);
                if (savedReport.ShowSummaryOnly.Value)
                {
                    chkSummaryOnly.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkSummaryOnly.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                if (savedReport.ShowPayments.HasValue ? savedReport.ShowPayments.Value : false)
                {
                    chkShowPayments.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                if (savedReport.ShowCurrentInterest.HasValue ? savedReport.ShowCurrentInterest.Value : false)
                {
                    chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkUseFullStatus.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                if (savedReport.ExcludePaid.HasValue ? savedReport.ExcludePaid.Value : false)
                {
                    chkExcludePaid.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkExcludePaid.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                // select all of the fields selected in the where list
                strTemp = savedReport.WhereSelection;
                for (var intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
                {
                    if (Strings.Mid(strTemp, intCT + 1, 1) == "1")
                    {
                        lstFields.SetSelected(intCT, true);
                    }
                    else
                    {
                        lstFields.SetSelected(intCT, false);
                    }
                }
                // clear the sort list
                lstSort.Clear();
                // load all of the selected fields in order
                strTemp = savedReport.SortSelection;
                if (strTemp.Length > 0)
                {
                    var intCT = 0;
                    do
                    {
                        intStart = intCT + 1;
                        intCT = Strings.InStr(intStart, strTemp, ",");
                        if (intCT > intStart)
                        {
                            // intStart will be the index of the next sort
                            intStart = Convert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, intStart, intCT - 1))));
                        }
                        else
                        {
                            intStart = Convert.ToInt32(Math.Round(Conversion.Val(Strings.Right(strTemp, strTemp.Length - (intStart - 1)))));
                        }
                        // add the item to the list
                        if (modCustomReport.Statics.strCaptions[intStart] != "")
                        {
                            lstSort.AddItem(modCustomReport.Statics.strCaptions[intStart]);
                            // enter the index in the item data
                            lstSort.ItemData(lstSort.NewIndex, intStart);
                            lstSort.SetSelected(lstSort.NewIndex, true);
                            intStart = intCT + 1;
                        }
                    }
                    while (!(intCT == 0));
                }
                // load the rest of the fields that are not selected
                for (var intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
                {
                    if (modCustomReport.Statics.strCaptions[intCT] != "")
                    {
                        if (ItemInSortList(intCT))
                        {
                            // do nothing
                        }
                        else
                        {
                            lstSort.AddItem(lstFields.Items[intCT].Text);
                            lstSort.ItemData(lstSort.NewIndex, lstFields.ItemData(intCT));
                        }
                    }
                }
                // load the contraints
                //for (var intCT = 0; intCT <= vsWhere.Rows - 1; intCT++)
                //{
                //    if (intCT < 10)
                //    {
                //        // TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
                //        if (Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||") != 0)
                //        {
                //            vsWhere.TextMatrix(intCT, 1, Strings.Left(FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||") - 1));
                //            strTemp = Strings.Right(FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))).Length - Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||") - 2);
                //            strTemp = strTemp.Replace("|", "");
                //            vsWhere.TextMatrix(intCT, 2, strTemp);
                //        }
                //        else
                //        {
                //            vsWhere.TextMatrix(intCT, 1, "");
                //            vsWhere.TextMatrix(intCT, 2, "");
                //        }
                //    }
                //}
            }
            else
            {
                FCMessageBox.Show("There was an error while opening this file.", MsgBoxStyle.Critical, "Load Error");
            }
            cmbReport.SelectedIndex = 0;
        }
	}
}
