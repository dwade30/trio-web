﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReprintCMForm.
	/// </summary>
	public partial class frmReprintCMForm : BaseForm
	{
		public frmReprintCMForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprintCMForm InstancePtr
		{
			get
			{
				return (frmReprintCMForm)Sys.GetInstance(typeof(frmReprintCMForm));
			}
		}

		protected frmReprintCMForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/02/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/01/2004              *
		// ********************************************************
		int lngType;
		bool boolLoaded;
		int lngGRIDCOLTree;
		int lngGridColAccount;
		int lngGRIDCOLYear;
		int lngGridColName;
		int lngGridColLocation;
		int lngGRIDCOLMailNumber;
		int lngGridColBillKey;
		int lngGRIDCOLHidden;

		public void Init(int lngPIT)
		{
			// this will set the type
			lngType = lngPIT;
			this.Show(App.MainForm);
		}

		private void cmbForm_DropDown(object sender, System.EventArgs e)
		{
			// this will set the width of the dropdown to see the whole string
			modAPIsConst.SendMessageByNum(this.cmbForm.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbForm_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (modAPIsConst.SendMessageByNum(cmbForm.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
				{
					modAPIsConst.SendMessageByNum(cmbForm.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
				}
			}
		}

		private void frmReprintCMForm_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				boolLoaded = true;
				lblInstructions.Text = "Select the type of mailer that this will be printed on and select the accounts that you would like to reprint.  Press F12 to continue.";
				FillFormCombo();
				FillPaymentGrid();
			}
		}

		private void frmReprintCMForm_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmReprintCMForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprintCMForm.Icon	= "frmReprintCMForms.frx":0000";
			//frmReprintCMForm.FillStyle	= 0;
			//frmReprintCMForm.ScaleWidth	= 9045;
			//frmReprintCMForm.ScaleHeight	= 7335;
			//frmReprintCMForm.LinkTopic	= "Form2";
			//frmReprintCMForm.LockControls	= -1  'True;
			//frmReprintCMForm.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsPayments.BackColor	= "-2147483643";
			//			//vsPayments.ForeColor	= "-2147483640";
			//vsPayments.BorderStyle	= 1;
			//vsPayments.FillStyle	= 0;
			//vsPayments.Appearance	= 1;
			//vsPayments.GridLines	= 1;
			//vsPayments.WordWrap	= 0;
			//vsPayments.ScrollBars	= 3;
			//vsPayments.RightToLeft	= 0;
			//vsPayments._cx	= 15478;
			//vsPayments._cy	= 6666;
			//vsPayments._ConvInfo	= 1;
			//vsPayments.MousePointer	= 0;
			//vsPayments.BackColorFixed	= -2147483633;
			//			//vsPayments.ForeColorFixed	= -2147483630;
			//vsPayments.BackColorSel	= -2147483635;
			//			//vsPayments.ForeColorSel	= -2147483634;
			//vsPayments.BackColorBkg	= -2147483636;
			//vsPayments.BackColorAlternate	= -2147483643;
			//vsPayments.GridColor	= -2147483633;
			//vsPayments.GridColorFixed	= -2147483632;
			//vsPayments.TreeColor	= -2147483632;
			//vsPayments.FloodColor	= 192;
			//vsPayments.SheetBorder	= -2147483642;
			//vsPayments.FocusRect	= 1;
			//vsPayments.HighLight	= 1;
			//vsPayments.AllowSelection	= -1  'True;
			//vsPayments.AllowBigSelection	= -1  'True;
			//vsPayments.AllowUserResizing	= 0;
			//vsPayments.SelectionMode	= 0;
			//vsPayments.GridLinesFixed	= 2;
			//vsPayments.GridLineWidth	= 1;
			//vsPayments.RowHeightMin	= 0;
			//vsPayments.RowHeightMax	= 0;
			//vsPayments.ColWidthMin	= 0;
			//vsPayments.ColWidthMax	= 0;
			//vsPayments.ExtendLastCol	= 0   'False;
			//vsPayments.FormatString	= "";
			//vsPayments.ScrollTrack	= -1  'True;
			//vsPayments.ScrollTips	= 0   'False;
			//vsPayments.MergeCells	= 0;
			//vsPayments.MergeCompare	= 0;
			//vsPayments.AutoResize	= -1  'True;
			//vsPayments.AutoSizeMode	= 0;
			//vsPayments.AutoSearch	= 0;
			//vsPayments.AutoSearchDelay	= 2;
			//vsPayments.MultiTotals	= -1  'True;
			//vsPayments.SubtotalPosition	= 1;
			//vsPayments.OutlineBar	= 0;
			//vsPayments.OutlineCol	= 0;
			//vsPayments.Ellipsis	= 0;
			//vsPayments.ExplorerBar	= 1;
			//vsPayments.PicturesOver	= 0   'False;
			//vsPayments.PictureType	= 0;
			//vsPayments.TabBehavior	= 0;
			//vsPayments.OwnerDraw	= 0;
			//vsPayments.ShowComboButton	= -1  'True;
			//vsPayments.TextStyle	= 0;
			//vsPayments.TextStyleFixed	= 0;
			//vsPayments.OleDragMode	= 0;
			//vsPayments.OleDropMode	= 0;
			//vsPayments.ComboSearch	= 3;
			//vsPayments.AutoSizeMouse	= -1  'True;
			//vsPayments.AllowUserFreezing	= 0;
			//vsPayments.BackColorFrozen	= 0;
			//			//vsPayments.ForeColorFrozen	= 0;
			//vsPayments.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmReprintCMForms.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngGRIDCOLTree = 0;
			lngGridColAccount = 1;
			lngGRIDCOLYear = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGRIDCOLMailNumber = 5;
			lngGridColBillKey = 6;
			lngGRIDCOLHidden = 7;
		}

		private void frmReprintCMForm_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmReprintCMForm_Resize(object sender, System.EventArgs e)
		{
			FormatPaymentGrid();
		}

		private void mnuFileClearAll_Click(object sender, System.EventArgs e)
		{
			ClearAll();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FillFormCombo()
		{
			cmbForm.AddItem("Hygrade Certified Mailer (Laser Format)");
			cmbForm.ItemData(cmbForm.NewIndex, 0);
			cmbForm.SelectedIndex = 0;
		}

		private void FormatPaymentGrid()
		{
			// this will format the payment grid and set the widths of rows, header titles ect
			vsPayments.Cols = 8;
			vsPayments.Rows = 1;
			vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsPayments.Cols - 1, true);
			// .Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, .Cols - 1) = 12
			vsPayments.ExtendLastCol = true;
			// set the column headers
			vsPayments.TextMatrix(0, lngGridColAccount, "Account");
			vsPayments.TextMatrix(0, lngGRIDCOLYear, "Year");
			vsPayments.TextMatrix(0, lngGridColName, "Name");
			vsPayments.TextMatrix(0, lngGridColLocation, "Location");
			vsPayments.TextMatrix(0, lngGRIDCOLMailNumber, "Mail Number");
			vsPayments.TextMatrix(0, lngGridColBillKey, "");
			vsPayments.TextMatrix(0, lngGRIDCOLHidden, "");
			// alignment and other formatting
			vsPayments.ColAlignment(lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGRIDCOLYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGRIDCOLMailNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGridColBillKey, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGRIDCOLHidden, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsPayments.Cols - 1, true);
			vsPayments.ColDataType(lngGRIDCOLTree, FCGrid.DataTypeSettings.flexDTBoolean);
			vsPayments.Visible = true;
			lblPayments.Visible = true;
		}

		private void SetPaymentGridColWidths()
		{
			int lngWid = 0;
			// set the widths
			lngWid = vsPayments.WidthOriginal;
			vsPayments.Cols = 8;
			vsPayments.ColWidth(lngGRIDCOLTree, FCConvert.ToInt32(lngWid * 0.04));
			// Check Box
			vsPayments.ColWidth(lngGridColAccount, FCConvert.ToInt32(lngWid * 0.1));
			// Account
			vsPayments.ColWidth(lngGRIDCOLYear, FCConvert.ToInt32(lngWid * 0.1));
			// Year
			vsPayments.ColWidth(lngGridColName, FCConvert.ToInt32(lngWid * 0.22));
			// Name
			vsPayments.ColWidth(lngGridColLocation, FCConvert.ToInt32(lngWid * 0.22));
			// Location
			vsPayments.ColWidth(lngGRIDCOLMailNumber, FCConvert.ToInt32(lngWid * 0.19));
			// Mail Number
			vsPayments.ColWidth(lngGridColBillKey, 0);
			// BillKey
			vsPayments.ColWidth(lngGRIDCOLHidden, 0);
		}

		private void FillPaymentGrid()
		{
			// this will fill the payment grid with the lien payments of the account that was entered
			clsDRWrapper rsPay = new clsDRWrapper();
			string strAccount = "";
			string strSQL;
			int lngEligible = 0;
			switch (lngType)
			{
				case 0:
					{
						// 30 Day Notices
						lngEligible = 2;
						break;
					}
				case 1:
					{
						// Lien Transfer Process
						lngEligible = 4;
						break;
					}
				case 2:
					{
						// Lien Maturity
						lngEligible = 5;
						break;
					}
			}
			//end switch
			strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = " + FCConvert.ToString(lngEligible) + " AND CertifiedMailNumber <> ''";
			vsPayments.Rows = 1;
			rsPay.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			while (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				strAccount = "\t" + FCConvert.ToString(rsPay.Get_Fields("Account")) + "\t" + FCConvert.ToString(rsPay.Get_Fields_Int32("BillingYear")) + "\t" + FCConvert.ToString(rsPay.Get_Fields("StreetNumber")) + FCConvert.ToString(rsPay.Get_Fields_String("StreetName")) + "\t" + FCConvert.ToString(rsPay.Get_Fields_String("CertifiedMailNumber")) + "\t" + FCConvert.ToString(rsPay.Get_Fields_Int32("ID"));
				vsPayments.AddItem("");
				// strAccount
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColAccount, FCConvert.ToString(rsPay.Get_Fields("Account")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGRIDCOLYear, FCConvert.ToString(rsPay.Get_Fields_Int32("BillingYear")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColName, FCConvert.ToString(rsPay.Get_Fields_String("Name1")));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColLocation, FCConvert.ToString(rsPay.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsPay.Get_Fields_String("StreetName")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGRIDCOLMailNumber, FCConvert.ToString(rsPay.Get_Fields_String("CertifiedMailNumber")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColBillKey, FCConvert.ToString(rsPay.Get_Fields_Int32("ID")));
				rsPay.MoveNext();
			}
			SetPaymentGridColWidths();
			vsPayments.Visible = true;
			lblPayments.Visible = true;
			vsPayments.Refresh();
		}

		private void SelectAll()
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsPayments.Rows - 1; lngCT++)
			{
				vsPayments.TextMatrix(lngCT, lngGRIDCOLTree, FCConvert.ToString(-1));
			}
		}

		private void ClearAll()
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsPayments.Rows - 1; lngCT++)
			{
				vsPayments.TextMatrix(lngCT, lngGRIDCOLTree, FCConvert.ToString(0));
			}
		}

		private void mnuFileReprint_Click(object sender, System.EventArgs e)
		{
			ReprintCertifiedMailForms();
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			SelectAll();
		}

		private void ReprintCertifiedMailForms()
		{
			// this function will ask the user which printer to use then show the Certified Mail Forms
			bool boolOK;
			int lngCT;
			string strAccounts;
			string strSQL = "";
			string strPrinterName = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			string strFont = "";
			int intCPI = 0;
			int x;
			// check to see if an account is selected...if one is, then call the report
			boolOK = false;
			strAccounts = "";
			// Find all the accounts that are selected
			for (lngCT = 1; lngCT <= vsPayments.Rows - 1; lngCT++)
			{
				if (Conversion.Val(vsPayments.TextMatrix(lngCT, lngGRIDCOLTree)) == -1)
				{
					boolOK = true;
					strAccounts += vsPayments.TextMatrix(lngCT, lngGridColBillKey) + ", ";
				}
			}
			if (boolOK)
			{
				// if accounts were selected
				/*? On Error Resume Next  *///FC:FINAL:SBE - if cancel is selected, and CancelError is true, exception is thrown (on error resume next)
				//try
				//{
				//	App.MainForm.CommonDialog1.ShowPrinter();
				//}
				//catch
				//{
				//}
				// show the printer dialog box
				if (Information.Err().Number == 0)
				{
					strPrinterName = FCGlobal.Printer.DeviceName;
					// set the name of the printer
					NumFonts = FCGlobal.Printer.FontCount;
					// check the number of fonts in the selected printer
					boolUseFont = false;
					intCPI = 10;
					for (x = 0; x <= NumFonts - 1; x++)
					{
						// find the forst font that is 10 CPI
						strFont = FCGlobal.Printer.Fonts[x];
						if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
						{
							strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
							if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
							{
								boolUseFont = true;
								strFont = FCGlobal.Printer.Fonts[x];
								break;
							}
						}
					}
					// x
					if (!boolUseFont)
					{
						strFont = "";
					}
					strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
					// get rid of the last space and the last comma
					// XXXX
					// strSQL = "SELECT * FROM (BillingMaster INNER JOIN " & strDbRE & "Master ON BillingMaster.Account = Master.RSAccount) WHERE BillKey IN (" & strAccounts & ")"    'create the SQL string
					strSQL = "SELECT b.ID, b.RateKey, b.BillingType, b.Account, b.Name1, b.CertifiedMailNumber FROM (BillingMaster b INNER JOIN " + modGlobal.Statics.strDbRE + "Master m ON b.Account = m.RSAccount) WHERE b.ID IN (" + strAccounts + ")";
					// initialize the report
					//rptCustomForms.InstancePtr.Init(strSQL, "FORMS", cmbForm.ItemData(cmbForm.SelectedIndex), strPrinterName, strFont, 20 + lngType, false);
                    rptCustomForms.InstancePtr.Init(strSQL, "FORMS", 1, strPrinterName, strFont, 20 + lngType, false);
                }
				else
				{
					// no accounts were selected so prompt the user
					FCMessageBox.Show("Please select an account(s) to reprint.", MsgBoxStyle.Information, "No Accounts Selected");
				}
			}
		}

		private void mnuFileSelectToBottom_Click(object sender, System.EventArgs e)
		{
			// this will select the current row and all of the others to the bottom
			int lngRW;
			for (lngRW = vsPayments.Row; lngRW <= vsPayments.Rows - 1; lngRW++)
			{
				if (vsPayments.Row > 0)
				{
					vsPayments.TextMatrix(lngRW, lngGRIDCOLTree, FCConvert.ToString(-1));
				}
			}
		}

		private void vsPayments_RowColChange(object sender, EventArgs e)
		{
			switch (vsPayments.Col)
			{
				case 0:
					{
						vsPayments.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsPayments.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileReprint_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void cmdFileSelectToBottom_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectToBottom_Click(sender, e);
		}

		private void cmdFileClearAll_Click(object sender, EventArgs e)
		{
			this.mnuFileClearAll_Click(sender, e);
		}
	}
}
