﻿using fecherFoundation;
using Global;

namespace TWCL0000
{
	public class modUseCR
	{
		//public static void FillCLInfo()
		//{
		//	// this is a dummy function for CL and CR
		//}

		//public static void DeleteCLPaymentFromShoppingList(ref int lngPaymentRecID)
		//{
		//	// this is a dummy function for CL and CR
		//}

		//public static void CreateCLBatchReceiptEntry(ref bool blnPrint)
		//{
		//	// this is a dummy function
		//}

		//public static bool AlreadyABatch()
		//{
		//	bool AlreadyABatch = false;
		//	// this is for the CL program
		//	AlreadyABatch = false;
		//	return AlreadyABatch;
		//}

		//public static void AttemptToStartBatch()
		//{
		//	if (frmCLGetAccount.InstancePtr.vsBatch.Rows <= 1)
		//	{
		//		// this will pop up a form that the user can input a batch update
		//		frmCLGetAccount.InstancePtr.ShowBatchQuestions();
		//	}
		//	else
		//	{
		//		// clear out the rows (still saved in the table) and start a new batch
		//		frmCLGetAccount.InstancePtr.vsBatch.Rows = 1;
		//		// clear out the batch info too
		//		frmCLGetAccount.InstancePtr.txtPaidBy.Text = "";
		//		frmCLGetAccount.InstancePtr.txtTellerID.Text = "";
		//		// txtPaymentDate.Text = ""
		//		frmCLGetAccount.InstancePtr.txtPaymentDate2.Text = "";
		//		// set this with a default
		//		frmCLGetAccount.InstancePtr.txtTaxYear.Text = "";
		//	}
		//}

		public static int GetNextReceiptNumber()
		{
			int GetNextReceiptNumber = 0;
			// this isa dummy function that will return nothing
			return GetNextReceiptNumber;
		}

		public class StaticVariables
		{
			// forms that are also used in other modules alone.
			// modUseCR is used to hold all of the functions shared in Cash Receipting and need to be in the
			// ********************************************************
			// Last Updated   :               04/14/2003              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// Date           :               07/23/2002              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
