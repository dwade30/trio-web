﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWCL0000
{
    public class DescriptionIDPair
    {
        public string Description { get; set; } = "";
        public int ID { get; set; } = 0;
    }
}
