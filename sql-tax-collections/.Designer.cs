using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;


namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCLEditBillInfo.
	/// </summary>
	partial class frmCLEditBillInfo : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbRE;
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCFrame fraEditInfo;
		public fecherFoundation.FCGrid vsEditInfo;
		public fecherFoundation.FCGrid vsYearInfo;
		public fecherFoundation.FCGrid vsSearch;
		public fecherFoundation.FCPanel fraSearch;
		public fecherFoundation.FCFrame fraGetAccount;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCLabel lblInstructions1;
		public fecherFoundation.FCFrame fraSearchCriteria;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCTextBox txtHold;
		public fecherFoundation.FCLabel lblSearchListInstruction;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessGetAccount;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
				_InstancePtr = null;
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCLEditBillInfo));
			this.cmbRE = new fecherFoundation.FCComboBox();
			this.cmbSearchType = new fecherFoundation.FCComboBox();
			this.fraEditInfo = new fecherFoundation.FCFrame();
			this.vsEditInfo = new fecherFoundation.FCGrid();
			this.vsYearInfo = new fecherFoundation.FCGrid();
			this.vsSearch = new fecherFoundation.FCGrid();
			this.fraSearch = new fecherFoundation.FCPanel();
			this.fraGetAccount = new fecherFoundation.FCFrame();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.lblInstructions1 = new fecherFoundation.FCLabel();
			this.fraSearchCriteria = new fecherFoundation.FCFrame();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.txtHold = new fecherFoundation.FCTextBox();
			this.lblSearchListInstruction = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessClearSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessGetAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.fcLabel2 = new fecherFoundation.FCLabel();
			((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).BeginInit();
			this.fraEditInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
			this.fraSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).BeginInit();
			this.fraGetAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).BeginInit();
			this.fraSearchCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			this.SuspendLayout();
			// 
			// cmbRE
			// 
			this.cmbRE.FormattingEnabled = true;
			this.cmbRE.Items.AddRange(new object[] {
            "Real Estate",
            "Personal Property"});
			this.cmbRE.Location = new System.Drawing.Point(261, 41);
			this.cmbRE.Name = "cmbRE";
			this.cmbRE.Size = new System.Drawing.Size(121, 22);
			this.cmbRE.TabIndex = 15;
			this.cmbRE.Text = "Real Estate";
			this.cmbRE.Visible = false;
			this.cmbRE.Click += new System.EventHandler(this.optRE_Click);
			// 
			// cmbSearchType
			// 
			this.cmbSearchType.FormattingEnabled = true;
			this.cmbSearchType.Items.AddRange(new object[] {
            "Name",
            "Street Name",
            "Map / Lot"});
			this.cmbSearchType.Location = new System.Drawing.Point(8, 17);
			this.cmbSearchType.Name = "cmbSearchType";
			this.cmbSearchType.Size = new System.Drawing.Size(121, 22);
			this.cmbSearchType.TabIndex = 16;
			this.cmbSearchType.Text = "Name";
			// 
			// fraEditInfo
			// 
			this.fraEditInfo.Controls.Add(this.fcLabel2);
			this.fraEditInfo.Controls.Add(this.vsEditInfo);
			this.fraEditInfo.Controls.Add(this.vsYearInfo);

			this.fraEditInfo.Location = new System.Drawing.Point(44, 30);
			this.fraEditInfo.Name = "fraEditInfo";
			this.fraEditInfo.Size = new System.Drawing.Size(535, 441);
			this.fraEditInfo.TabIndex = 19;
			this.fraEditInfo.Text = "Edit Account Information";
			this.fraEditInfo.UseMnemonic = false;
			this.fraEditInfo.Visible = false;
			// 
			// vsEditInfo
			// 
			this.vsEditInfo.AllowSelection = false;
			this.vsEditInfo.AllowUserToResizeColumns = false;
			this.vsEditInfo.AllowUserToResizeRows = false;
			this.vsEditInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsEditInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsEditInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsEditInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsEditInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsEditInfo.Cols = 2;
			this.vsEditInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsEditInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsEditInfo.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsEditInfo.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsEditInfo.FixedRows = 0;

			this.vsEditInfo.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.vsEditInfo.FrozenCols = 0;
			this.vsEditInfo.FrozenRows = 0;
			this.vsEditInfo.GridColor = System.Drawing.Color.Empty;
			this.vsEditInfo.Location = new System.Drawing.Point(73, 228);
			this.vsEditInfo.Name = "vsEditInfo";
			this.vsEditInfo.ReadOnly = true;
			this.vsEditInfo.RowHeadersMinimumWidth = 2;
			this.vsEditInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsEditInfo.RowHeightMin = 0;
			this.vsEditInfo.Rows = 11;
			this.vsEditInfo.Size = new System.Drawing.Size(397, 190);
			this.vsEditInfo.StandardTab = true;
			this.vsEditInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsEditInfo.TabIndex = 21;
			this.vsEditInfo.CurrentCellChanged += new System.EventHandler(this.vsEditInfo_RowColChange);
			// 
			// vsYearInfo
			// 
			this.vsYearInfo.AllowSelection = false;
			this.vsYearInfo.AllowUserToResizeColumns = false;
			this.vsYearInfo.AllowUserToResizeRows = false;
			this.vsYearInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsYearInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsYearInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsYearInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsYearInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsYearInfo.Cols = 4;
			this.vsYearInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsYearInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsYearInfo.DefaultCellStyle = dataGridViewCellStyle4;

			this.vsYearInfo.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.vsYearInfo.FrozenCols = 0;
			this.vsYearInfo.FrozenRows = 0;
			this.vsYearInfo.GridColor = System.Drawing.Color.Empty;
			this.vsYearInfo.Location = new System.Drawing.Point(36, 30);
			this.vsYearInfo.Name = "vsYearInfo";
			this.vsYearInfo.ReadOnly = true;
			this.vsYearInfo.RowHeadersMinimumWidth = 2;
			this.vsYearInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsYearInfo.RowHeightMin = 0;
			this.vsYearInfo.Rows = 1;
			this.vsYearInfo.Size = new System.Drawing.Size(466, 145);
			this.vsYearInfo.StandardTab = true;
			this.vsYearInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsYearInfo.TabIndex = 20;
			this.vsYearInfo.CurrentCellChanged += new System.EventHandler(this.vsYearInfo_RowColChange);
			// 
			// vsSearch
			// 
			this.vsSearch.AllowSelection = false;
			this.vsSearch.AllowUserToResizeColumns = false;
			this.vsSearch.AllowUserToResizeRows = false;
			this.vsSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSearch.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSearch.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSearch.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSearch.BackColorSel = System.Drawing.Color.Empty;
			this.vsSearch.Cols = 5;
			this.vsSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSearch.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsSearch.FixedCols = 0;

			this.vsSearch.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.vsSearch.FrozenCols = 0;
			this.vsSearch.FrozenRows = 0;
			this.vsSearch.GridColor = System.Drawing.Color.Empty;
			this.vsSearch.Location = new System.Drawing.Point(368, 0);
			this.vsSearch.Name = "vsSearch";
			this.vsSearch.ReadOnly = true;
			this.vsSearch.RowHeadersMinimumWidth = 2;
			this.vsSearch.RowHeadersVisible = false;
			this.vsSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSearch.RowHeightMin = 0;
			this.vsSearch.Rows = 1;
			this.vsSearch.Size = new System.Drawing.Size(114, 56);
			this.vsSearch.StandardTab = true;
			this.vsSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSearch.TabIndex = 8;
			this.vsSearch.Visible = false;
			this.vsSearch.CurrentCellChanged += new System.EventHandler(this.vsSearch_RowColChange);
			this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
			// 
			// fraSearch
			// 
			this.fraSearch.Controls.Add(this.fraGetAccount);
			this.fraSearch.Controls.Add(this.fraSearchCriteria);
			this.fraSearch.Controls.Add(this.cmbRE);
			this.fraSearch.Controls.Add(this.fcLabel1);
			this.fraSearch.Controls.Add(this.lblLastAccount);

			this.fraSearch.Location = new System.Drawing.Point(105, 47);
			this.fraSearch.Name = "fraSearch";
			this.fraSearch.Size = new System.Drawing.Size(381, 377);
			this.fraSearch.TabIndex = 4;
			this.fraSearch.Visible = false;
			// 
			// fraGetAccount
			// 
			this.fraGetAccount.Controls.Add(this.cmdGetAccountNumber);
			this.fraGetAccount.Controls.Add(this.txtGetAccountNumber);
			this.fraGetAccount.Controls.Add(this.lblInstructions1);

			this.fraGetAccount.Location = new System.Drawing.Point(49, 21);
			this.fraGetAccount.Name = "fraGetAccount";
			this.fraGetAccount.Size = new System.Drawing.Size(268, 111);
			this.fraGetAccount.TabIndex = 9;
			this.fraGetAccount.Text = "Last Account Accessed ...";
			this.fraGetAccount.UseMnemonic = false;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.BackColor = System.Drawing.SystemColors.Control;

			this.cmdGetAccountNumber.Location = new System.Drawing.Point(170, 68);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(92, 28);
			this.cmdGetAccountNumber.TabIndex = 14;
			this.cmdGetAccountNumber.Text = "Get Account";
			this.cmdGetAccountNumber.Visible = false;
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;

			this.txtGetAccountNumber.Location = new System.Drawing.Point(198, 32);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(60, 22);
			this.txtGetAccountNumber.TabIndex = 10;
			this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
			this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
			// 
			// lblInstructions1
			// 

			this.lblInstructions1.Location = new System.Drawing.Point(20, 30);
			this.lblInstructions1.Name = "lblInstructions1";
			this.lblInstructions1.Size = new System.Drawing.Size(161, 66);
			this.lblInstructions1.TabIndex = 13;
			// 
			// fraSearchCriteria
			// 
			this.fraSearchCriteria.Controls.Add(this.Frame4);
			this.fraSearchCriteria.Controls.Add(this.cmbSearchType);
			this.fraSearchCriteria.Controls.Add(this.fcLabel2);

			this.fraSearchCriteria.Location = new System.Drawing.Point(4, 199);
			this.fraSearchCriteria.Name = "fraSearchCriteria";
			this.fraSearchCriteria.Size = new System.Drawing.Size(371, 151);
			this.fraSearchCriteria.TabIndex = 5;
			this.fraSearchCriteria.Text = "Search";
			this.fraSearchCriteria.UseMnemonic = false;
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.txtSearch);
			this.Frame4.Controls.Add(this.cmdSearch);
			this.Frame4.Controls.Add(this.cmdClear);

			this.Frame4.Location = new System.Drawing.Point(158, 21);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(199, 77);
			this.Frame4.TabIndex = 15;
			this.Frame4.Text = "Search For";
			this.Frame4.UseMnemonic = false;
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;

			this.txtSearch.Location = new System.Drawing.Point(8, 34);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(181, 22);
			this.txtSearch.TabIndex = 18;
			this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
			// 
			// cmdSearch
			// 
			this.cmdSearch.BackColor = System.Drawing.SystemColors.Control;

			this.cmdSearch.Location = new System.Drawing.Point(93, 41);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(78, 28);
			this.cmdSearch.TabIndex = 17;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Visible = false;
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.BackColor = System.Drawing.SystemColors.Control;

			this.cmdClear.Location = new System.Drawing.Point(8, 41);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(78, 28);
			this.cmdClear.TabIndex = 16;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Visible = false;
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(152, 52);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(146, 17);
			this.fcLabel1.TabIndex = 22;
			this.fcLabel1.Text = "Last Account Accessed ...";
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.AutoSize = true;

			this.lblLastAccount.Location = new System.Drawing.Point(330, 11);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(4, 18);
			this.lblLastAccount.TabIndex = 7;
			// 
			// txtHold
			// 
			this.txtHold.BackColor = System.Drawing.SystemColors.Window;
			this.txtHold.Location = new System.Drawing.Point(15, 78);
			this.txtHold.Name = "txtHold";
			this.txtHold.Size = new System.Drawing.Size(132, 22);
			this.txtHold.TabIndex = 3;
			this.txtHold.Visible = false;
			// 
			// lblSearchListInstruction
			// 

			this.lblSearchListInstruction.Location = new System.Drawing.Point(85, 0);
			this.lblSearchListInstruction.Name = "lblSearchListInstruction";
			this.lblSearchListInstruction.Size = new System.Drawing.Size(236, 22);
			this.lblSearchListInstruction.TabIndex = 22;
			this.lblSearchListInstruction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblSearchListInstruction.Visible = false;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.Add(this.mnuProcess);
			this.MainMenu1.Visible = true;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.Add(this.mnuProcessClearSearch);
			this.mnuProcess.MenuItems.Add(this.mnuProcessSearch);
			this.mnuProcess.MenuItems.Add(this.mnuFileSave);
			this.mnuProcess.MenuItems.Add(this.mnuProcessGetAccount);
			this.mnuProcess.MenuItems.Add(this.mnuProcessSeperator);
			this.mnuProcess.MenuItems.Add(this.mnuProcessQuit);
			this.mnuProcess.Text = "&File";
			// 
			// mnuProcessClearSearch
			// 
			this.mnuProcessClearSearch.Index = 0;
			this.mnuProcessClearSearch.Text = "&Clear Search";
			this.mnuProcessClearSearch.Click += new System.EventHandler(this.mnuProcessClearSearch_Click);
			// 
			// mnuProcessSearch
			// 
			this.mnuProcessSearch.Index = 1;
			this.mnuProcessSearch.Text = "&Search";
			this.mnuProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 2;
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Sa&ve";
			this.mnuFileSave.Visible = false;
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuProcessGetAccount
			// 
			this.mnuProcessGetAccount.Index = 3;
			this.mnuProcessGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessGetAccount.Text = "P&rocess";
			this.mnuProcessGetAccount.Click += new System.EventHandler(this.mnuProcessGetAccount_Click);
			// 
			// mnuProcessSeperator
			// 
			this.mnuProcessSeperator.Index = 4;
			this.mnuProcessSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Text = "E&xit    (Esc)";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// fcLabel2
			// 
			this.fcLabel2.AutoSize = true;
			this.fcLabel2.Location = new System.Drawing.Point(-14, 235);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(79, 18);
			this.fcLabel2.TabIndex = 22;
			this.fcLabel2.Text = "Search By:";
			// 
			// frmCLEditBillInfo
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 483);
			this.Controls.Add(this.fraEditInfo);
			this.Controls.Add(this.vsSearch);
			this.Controls.Add(this.fraSearch);
			this.Controls.Add(this.txtHold);
			this.Controls.Add(this.lblSearchListInstruction);
			this.FillColor = 16777215;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCLEditBillInfo";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Bill Information";
			this.Load += new System.EventHandler(this.frmCLEditBillInfo_Load);
			this.Activated += new System.EventHandler(this.frmCLEditBillInfo_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCLEditBillInfo_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCLEditBillInfo_KeyPress);
			this.Resize += new System.EventHandler(this.frmCLEditBillInfo_Resize);
			((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).EndInit();
			this.fraEditInfo.ResumeLayout(false);
			this.fraEditInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
			this.fraSearch.ResumeLayout(false);
			this.fraSearch.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).EndInit();
			this.fraGetAccount.ResumeLayout(false);
			this.fraGetAccount.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).EndInit();
			this.fraSearchCriteria.ResumeLayout(false);
			this.fraSearchCriteria.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel fcLabel1;
		private FCLabel fcLabel2;
	}
}