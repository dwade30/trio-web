﻿using System.Web.UI;
using Autofac;
using Global;
using SharedApplication;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Comment;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Notes;
using SharedApplication.TaxCollections.Receipting;
using TWCL0000.AccountPayment;
using TWCL0000.Notes;

namespace TWCL0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmTaxAccountSearch>().As<IView<ITaxAccountSearchViewModel>>();
            builder.RegisterType<frmTaxAccountStatus>().As<IView<ITaxAccountStatusViewModel>>();
            builder.RegisterType<frmCLComment>().As<IModalView<ICollectionsCommentViewModel>>();
            builder.RegisterType<frmCollectionNote>().As<IModalView<ICollectionsNoteViewModel>>();
            builder.RegisterType<frmTaxBillInfo>().As<IView<ITaxBillInfoViewModel>>();
            builder.RegisterType<frmTaxPaymentInfo>().As<IView<ITaxPaymentInfoViewModel>>();
            builder.RegisterType<frmTaxLienInfo>().As<IView<ITaxLienInfoViewModel>>();
            builder.RegisterType<frmShowRealEstateTaxAccountInfo>().As<IView<IShowRealEstateTaxAccountInfoViewModel>>();
            builder.RegisterType<frmShowPersonalPropertyTaxAccountInfo>()
                .As<IView<IShowPersonalPropertyTaxAccountInfoViewModel>>();
            builder.RegisterType<rptTaxAccountDetail>().As<IView<ITaxAccountDetailReportModel>>();
            builder.RegisterType<TaxAccountDetailReportModel>().As<ITaxAccountDetailReportModel>();
            builder.RegisterType<rptTaxBillInfo>().As<IView<ITaxBillInfoReportModel>>();
            builder.RegisterType<frmNewLienDischargeNotice>().As<IModalView<ITaxLienDischargeNoticeViewModel>>();
            builder.RegisterType<rptLienDischarge>().As<IView<ITaxLienDischargeAlternateNoticeReportModel>>();
            builder.RegisterType<frmLienDischargeNoticeReport>().As<IView<ITaxLienDischargeNoticeReportViewModel>>();
            builder.RegisterType<rptCLReceipt>().As<IView<ICLReceiptViewModel>>();
            builder.RegisterType<frmCLBatchSetup>().As<IModalView<ICLBatchInputSetupViewModel>>();
            builder.RegisterType<frmCLBatchProcess>().As<IView<ITaxCollectionsBatchViewModel>>();
            builder.RegisterType<frmSelectCLBatch>().As<IModalView<ISelectCLBatchViewModel>>();
            builder.RegisterType<rptBatchListing>().As<IView<IPropertyTaxBatchListingViewModel>>();
            builder.RegisterType<frmRealEstateTaxAccountSearch>()
                .As<IModalView<IRealEstateTaxAccountSearchViewModel>>();
            builder.RegisterType<frmNonBudgetaryPayment>().As<IModalView<INonBudgetaryPaymentViewModel>>();
            builder.RegisterType<frmChooseDiscount>().As<IModalView<IGetPropertyTaxDiscountViewModel>>();
            builder.RegisterType<frmNewPartialPaymentWaiver>().As<IModalView<IPartialPaymentWaiverViewModel>>();
        }
    }
}