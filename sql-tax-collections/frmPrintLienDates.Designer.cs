﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmPrintLienDates.
	/// </summary>
	partial class frmPrintLienDates : BaseForm
	{
		public Wisej.Web.ComboBox cmbPrint;
		public fecherFoundation.FCComboBox cmbRK;
		public fecherFoundation.FCComboBox cmbYear;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintLienDates));
            this.cmbPrint = new Wisej.Web.ComboBox();
            this.cmbRK = new fecherFoundation.FCComboBox();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.flexLayoutPanel1 = new Wisej.Web.FlexLayoutPanel();
            this.tableLayoutPanel2 = new Wisej.Web.TableLayoutPanel();
            this.RateKeyLayout = new Wisej.Web.TableLayoutPanel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.YearLayout = new Wisej.Web.TableLayoutPanel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.flexLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.RateKeyLayout.SuspendLayout();
            this.YearLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 212);
            this.BottomPanel.Size = new System.Drawing.Size(616, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.flexLayoutPanel1);
            this.ClientArea.Size = new System.Drawing.Size(636, 320);
            this.ClientArea.Controls.SetChildIndex(this.flexLayoutPanel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(636, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(187, 30);
            this.HeaderText.Text = "Print Lien Dates";
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "",
            "",
            "",
            "",
            ""});
            this.cmbPrint.Location = new System.Drawing.Point(103, 3);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(318, 40);
            this.cmbPrint.TabIndex = 2;
            // 
            // cmbRK
            // 
            this.cmbRK.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRK.Location = new System.Drawing.Point(103, 3);
            this.cmbRK.Name = "cmbRK";
            this.cmbRK.Size = new System.Drawing.Size(318, 40);
            this.cmbRK.TabIndex = 4;
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(103, 3);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(318, 40);
            this.cmbYear.TabIndex = 6;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // lblPrint
            // 
            this.lblPrint.Dock = Wisej.Web.DockStyle.Fill;
            this.lblPrint.Location = new System.Drawing.Point(3, 3);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(94, 40);
            this.lblPrint.TabIndex = 5;
            this.lblPrint.Text = "PRINT";
            this.lblPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(145, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(166, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Save & Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // flexLayoutPanel1
            // 
            this.flexLayoutPanel1.Controls.Add(this.tableLayoutPanel2);
            this.flexLayoutPanel1.Controls.Add(this.RateKeyLayout);
            this.flexLayoutPanel1.Controls.Add(this.YearLayout);
            this.flexLayoutPanel1.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.flexLayoutPanel1.Location = new System.Drawing.Point(98, 31);
            this.flexLayoutPanel1.Name = "flexLayoutPanel1";
            this.flexLayoutPanel1.Size = new System.Drawing.Size(440, 181);
            this.flexLayoutPanel1.TabIndex = 4;
            this.flexLayoutPanel1.TabStop = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.cmbPrint, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblPrint, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(434, 46);
            this.tableLayoutPanel2.TabIndex = 1;
            this.tableLayoutPanel2.TabStop = true;
            // 
            // RateKeyLayout
            // 
            this.RateKeyLayout.ColumnCount = 2;
            this.RateKeyLayout.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 100F));
            this.RateKeyLayout.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.RateKeyLayout.Controls.Add(this.fcLabel1, 0, 0);
            this.RateKeyLayout.Controls.Add(this.cmbRK, 1, 0);
            this.RateKeyLayout.Location = new System.Drawing.Point(3, 65);
            this.RateKeyLayout.Name = "RateKeyLayout";
            this.RateKeyLayout.RowCount = 1;
            this.RateKeyLayout.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.RateKeyLayout.Size = new System.Drawing.Size(434, 46);
            this.RateKeyLayout.TabIndex = 3;
            this.RateKeyLayout.TabStop = true;
            this.RateKeyLayout.Visible = false;
            // 
            // fcLabel1
            // 
            this.fcLabel1.Dock = Wisej.Web.DockStyle.Fill;
            this.fcLabel1.Location = new System.Drawing.Point(3, 3);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(94, 40);
            this.fcLabel1.TabIndex = 6;
            this.fcLabel1.Text = "RATE KEY";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // YearLayout
            // 
            this.YearLayout.ColumnCount = 2;
            this.YearLayout.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 100F));
            this.YearLayout.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.YearLayout.Controls.Add(this.fcLabel2, 0, 0);
            this.YearLayout.Controls.Add(this.cmbYear, 1, 0);
            this.YearLayout.Location = new System.Drawing.Point(3, 127);
            this.YearLayout.Name = "YearLayout";
            this.YearLayout.RowCount = 1;
            this.YearLayout.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.YearLayout.Size = new System.Drawing.Size(434, 46);
            this.YearLayout.TabIndex = 5;
            this.YearLayout.TabStop = true;
            this.YearLayout.Visible = false;
            // 
            // fcLabel2
            // 
            this.fcLabel2.Dock = Wisej.Web.DockStyle.Fill;
            this.fcLabel2.Location = new System.Drawing.Point(3, 3);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(94, 40);
            this.fcLabel2.TabIndex = 6;
            this.fcLabel2.Text = "YEAR";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmPrintLienDates
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(636, 380);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrintLienDates";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Print Lien Dates";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmPrintLienDates_Load);
            this.Activated += new System.EventHandler(this.frmPrintLienDates_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintLienDates_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintLienDates_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.flexLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.RateKeyLayout.ResumeLayout(false);
            this.YearLayout.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblPrint;
		private FCButton btnProcess;
        private FlexLayoutPanel flexLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private TableLayoutPanel RateKeyLayout;
        private FCLabel fcLabel1;
        private TableLayoutPanel YearLayout;
        private FCLabel fcLabel2;
    }
}
