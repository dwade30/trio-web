﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienPartialPayment.
	/// </summary>
	partial class frmLienPartialPayment : BaseForm
	{
		public fecherFoundation.FCFrame fraVariables;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblYear;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienPartialPayment));
            this.fraVariables = new fecherFoundation.FCFrame();
            this.vsData = new fecherFoundation.FCGrid();
            this.lblName = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariables)).BeginInit();
            this.fraVariables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 519);
            this.BottomPanel.Size = new System.Drawing.Size(815, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraVariables);
            this.ClientArea.Location = new System.Drawing.Point(0, 84);
            this.ClientArea.Size = new System.Drawing.Size(835, 525);
            this.ClientArea.Controls.SetChildIndex(this.fraVariables, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.lblYear);
            this.TopPanel.Size = new System.Drawing.Size(835, 84);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblYear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(157, 30);
            this.HeaderText.Text = "Confirm Data";
            // 
            // fraVariables
            // 
            this.fraVariables.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraVariables.AppearanceKey = "groupBoxNoBorders";
            this.fraVariables.Controls.Add(this.vsData);
            this.fraVariables.Controls.Add(this.lblName);
            this.fraVariables.Location = new System.Drawing.Point(30, 6);
            this.fraVariables.Name = "fraVariables";
            this.fraVariables.Size = new System.Drawing.Size(785, 513);
            this.fraVariables.TabIndex = 1001;
            this.fraVariables.UseMnemonic = false;
            // 
            // vsData
            // 
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.Cols = 3;
            this.vsData.ColumnHeadersVisible = false;
            this.vsData.FixedRows = 0;
            this.vsData.Location = new System.Drawing.Point(0, 14);
            this.vsData.Name = "vsData";
            this.vsData.Rows = 1;
            this.vsData.Size = new System.Drawing.Size(785, 479);
            this.vsData.TabIndex = 2;
            this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
            this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
            // 
            // lblName
            // 
            this.lblName.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblName.Location = new System.Drawing.Point(310, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(455, 30);
            this.lblName.TabIndex = 1;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.Name = "lblYear";
            this.lblYear.TabIndex = 1;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(322, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(161, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Save & Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmLienPartialPayment
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(835, 609);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLienPartialPayment";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Partial Payment ";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmLienPartialPayment_Load);
            this.Activated += new System.EventHandler(this.frmLienPartialPayment_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienPartialPayment_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariables)).EndInit();
            this.fraVariables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
	}
}
