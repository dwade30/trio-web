﻿using System;

namespace TWCL0000
{
    public class TaxBillCalculationSummary
    {
        public Decimal CostsCharged { get; set; }
        public Decimal CostsPaid { get; set; }
        public Decimal InterestPaid { get; set; }
        public Decimal ChargedInterest { get; set; }
        public Decimal Charged { get; set; }
        public Decimal PrincipalPaid { get; set; }
        public Decimal Balance { get; set; }
        public Decimal Interest { get; set; }

        public Decimal InterestDue()
        {
            return ChargedInterest + Interest - InterestPaid;
        }

        public Decimal PrincipalDue()
        {
            return Charged - PrincipalPaid;
        }

        public Decimal CostsDue()
        {
            return CostsCharged - CostsPaid;
        }
        
    }
}