﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRemoveLien.
	/// </summary>
	partial class frmRemoveLien : BaseForm
	{
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCGrid vsPayments;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemoveLien));
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.lblYear = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.vsPayments = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.lblPayments = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 471);
			this.BottomPanel.Size = new System.Drawing.Size(782, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.lblPayments);
			this.ClientArea.Controls.Add(this.cmbYear);
			this.ClientArea.Controls.Add(this.vsPayments);
			this.ClientArea.Controls.Add(this.lblYear);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Size = new System.Drawing.Size(782, 411);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(782, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(404, 35);
			this.HeaderText.Text = "Remove Account From Lien Status";
			// 
			// txtAccount
			// 
			this.txtAccount.MaxLength = 10;
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.Location = new System.Drawing.Point(156, 30);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(131, 40);
			this.txtAccount.TabIndex = 1;
			this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
			this.txtAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAccount_KeyPress);
			this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(156, 90);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(131, 40);
			this.cmbYear.TabIndex = 3;
			this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
			this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
			// 
			// lblYear
			// 
			this.lblYear.AutoSize = true;
			this.lblYear.Location = new System.Drawing.Point(30, 104);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(41, 16);
			this.lblYear.TabIndex = 2;
			this.lblYear.Text = "YEAR";
			this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccount
			// 
			this.lblAccount.AutoSize = true;
			this.lblAccount.Location = new System.Drawing.Point(30, 44);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(69, 16);
			this.lblAccount.TabIndex = 0;
			this.lblAccount.Text = "ACCOUNT";
			this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// vsPayments
			// 
			this.vsPayments.AllowSelection = false;
			this.vsPayments.AllowUserToResizeColumns = false;
			this.vsPayments.AllowUserToResizeRows = false;
			this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsPayments.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsPayments.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsPayments.BackColorBkg = System.Drawing.Color.Empty;
			this.vsPayments.BackColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.BackColorSel = System.Drawing.Color.Empty;
			this.vsPayments.Cols = 2;
			this.vsPayments.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle1.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsPayments.DefaultCellStyle = dataGridViewCellStyle1;
			this.vsPayments.FixedCols = 0;
			this.vsPayments.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.FrozenCols = 0;
			this.vsPayments.FrozenRows = 0;
			this.vsPayments.GridColor = System.Drawing.Color.Empty;
			this.vsPayments.Location = new System.Drawing.Point(30, 186);
			this.vsPayments.Name = "vsPayments";
			this.vsPayments.ReadOnly = true;
			this.vsPayments.RowHeadersVisible = false;
			this.vsPayments.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsPayments.RowHeightMin = 0;
			this.vsPayments.Rows = 2;
			this.vsPayments.ShowColumnVisibilityMenu = false;
			this.vsPayments.Size = new System.Drawing.Size(732, 205);
			this.vsPayments.StandardTab = true;
			this.vsPayments.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsPayments.TabIndex = 5;
			this.vsPayments.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// lblPayments
			// 
			this.lblPayments.AutoSize = true;
			this.lblPayments.Location = new System.Drawing.Point(30, 150);
			this.lblPayments.Name = "lblPayments";
			this.lblPayments.Size = new System.Drawing.Size(106, 16);
			this.lblPayments.TabIndex = 4;
			this.lblPayments.Text = "LIEN PAYMENTS";
			this.lblPayments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblPayments.Visible = false;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(331, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmRemoveLien
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(782, 579);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmRemoveLien";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Remove Account From Lien Status";
			this.Load += new System.EventHandler(this.frmRemoveLien_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRemoveLien_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRemoveLien_KeyPress);
			this.Resize += new System.EventHandler(this.frmRemoveLien_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblPayments;
		private FCButton btnProcess;
	}
}
