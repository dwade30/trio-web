﻿using Global;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;

namespace TWCL0000
{
    public class AddRECLCommentHandler : CommandHandler<AddRECLComment>
    {
        protected override void Handle(AddRECLComment command)
        {
            frmRECLComment.InstancePtr.Init(command.Account, command.AsModal);
        }
    }
}