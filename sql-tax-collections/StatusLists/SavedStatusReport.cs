﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWCL0000
{
    public partial class SavedStatusReport
    {
        public int ID { get; set; }

        public string ReportName { get; set; }

        public string Type { get; set; }

        public string SQL { get; set; }

        public DateTime? LastUpdated { get; set; }

        public string WhereSelection { get; set; }

        public string SortSelection { get; set; }

        public string FieldConstraint0 { get; set; }

        public string FieldConstraint1 { get; set; }

        public string FieldConstraint2 { get; set; }

        public string FieldConstraint3 { get; set; }

        public string FieldConstraint4 { get; set; }
        public string FieldConstraint5 { get; set; }
        public string FieldConstraint6 { get; set; }

        public string FieldConstraint7 { get; set; }

        public string FieldConstraint8 { get; set; }

        public string FieldConstraint9 { get; set; }

        public bool? ShowSummaryOnly { get; set; }

        public bool? ExcludePaid { get; set; }

        public bool? ShowPayments { get; set; }

        public bool? ShowCurrentInterest { get; set; }
    }
}
