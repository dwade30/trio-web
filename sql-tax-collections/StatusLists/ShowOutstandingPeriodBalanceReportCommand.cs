﻿using Global;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Interfaces;

namespace TWCL0000
{
    public class ShowOutstandingPeriodBalanceReportCommand : ITaxRateChoiceCommand
    {
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        private string rateList = "";
        private int period = 4;
        public ShowOutstandingPeriodBalanceReportCommand( TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }
        public void Execute()
        {
            ShowReport();
        }

        public void SetRateIdList(string rateIdList)
        {
            rateList = rateIdList;
        }

        public void SetPeriod(int value)
        {
	        period = value;
        }

        private void ShowReport()
        {
            var report = new rptOutstandingPeriodBalances();
            report.SetReportOption(reportConfiguration,rateList,period);
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Report Information");
            frmReportViewer.InstancePtr.Init(report);
            frmWait.InstancePtr.Unload();
        }
    }
}