﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Models;
using TWSharedLibrary;

namespace TWCL0000
{

	public partial class frmStatusLists : BaseForm
	{
        private StatusListViewModel viewModel;
		private frmStatusLists()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
            
		}


        public frmStatusLists(StatusListViewModel model)
        {
            viewModel = model;
            InitializeComponent();
            InitializeComponentEx();
            AddEventHandlers();
        }

        private void AddEventHandlers()
        {
            this.chkHardCode.CheckedChanged += chkHardCode_CheckedChanged;
            viewModel.AvailableFieldsChanged += ViewModel_AvailableFieldsChanged;
            viewModel.SortOptionsChanged += ViewModel_SortOptionsChanged;
            viewModel.FilterChanged += ViewModel_FilterChanged;
            viewModel.StatusReportsChanged += ViewModel_StatusReportsChanged;
            this.cboSavedReport.SelectedIndexChanged += cboSavedReport_SelectedIndexChanged;
            this.cmdAdd.Click += cmdAdd_Click;
            viewModel.SelectedOptionsChanged += ViewModel_SelectedOptionsChanged;
        }

        private void ViewModel_SelectedOptionsChanged(object sender, EventArgs e)
        {
            ShowOptions();
        }

        private void ViewModel_AvailableFieldsChanged(object sender, EventArgs e)
        {
            FillLstFields();
        }

        private void ViewModel_StatusReportsChanged(object sender, EventArgs e)
        {
            FillcboSavedReport();
        }

        private void ViewModel_FilterChanged(object sender, EventArgs e)
        {
            ShowFilter();
        }

        private void ShowFilter()
        {
            var filter = viewModel.GetFilter();
            txtAccountStart.Text =  filter.AccountMin.HasValue ? filter.AccountMin.Value.ToString() : "";
            txtAccountEnd.Text = filter.AccountMax.HasValue ? filter.AccountMax.Value.ToString() : "";
            txtNameStart.Text = filter.NameMin;
            txtNameEnd.Text = filter.NameMax;
            txtBalanceDue.Text = filter.BalanceDueAmount.HasValue ? filter.BalanceDueAmount.Value.ToString() : "";
            dtpAsOfDate.Text = filter.AsOfDate.HasValue ? filter.AsOfDate.Value.ToShortDateString() : "";
            dtpShowPaymentStart.Text =
                filter.PaymentDateMin.HasValue ? filter.PaymentDateMin.Value.ToShortDateString() : "";
            dtpShowPaymentEnd.Text = filter.PaymentDateMax.HasValue ? filter.PaymentDateMax.Value.ToShortDateString() : "";
            if (filter.BalanceDueComparisonType.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbBalanceDueOption.Items)
                {
                    if (idPair.ID == (int) filter.BalanceDueComparisonType.Value)
                    {
                        cmbBalanceDueOption.SelectedItem = idPair;
                    }
                }
            }
            else
            {
                cmbBalanceDueOption.SelectedIndex = -1;
            }

            if (filter.IsTaxAcquired.HasValue)
            {
                cmbTaxAcquired.SelectedValue = filter.IsTaxAcquired.Value 
                    ? TaxAcquiredOptionType.TaxAcquired 
                    : TaxAcquiredOptionType.NonTaxAcquired;
            }
            else
            {
                cmbTaxAcquired.SelectedIndex = -1;
            }

            if (filter.TaxBillStatus.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbBillStatusType.Items)
                {
                    if (idPair.ID == (int)filter.TaxBillStatus.Value)
                    {
                        cmbBillStatusType.SelectedItem = idPair;                        
                    }
                }
            }
            else
            {
                cmbBillStatusType.SelectedIndex = -1;
            }

            if (filter.RateRecordMin.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbRateRecordStart.Items)
                {
                    if (idPair.ID == filter.RateRecordMin)
                    {
                        cmbRateRecordStart.SelectedItem = idPair;
                    }
                }
            }
            else
            {
                cmbRateRecordStart.SelectedIndex = -1;
            }

            if (filter.RateRecordMax.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbRateRecordEnd.Items)
                {
                    if (idPair.ID == filter.RateRecordMax)
                    {
                        cmbRateRecordEnd.SelectedItem = idPair;
                    }
                }
            }
            else
            {
                cmbRateRecordEnd.SelectedIndex = -1;
            }

            if (filter.TranCodeMin.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbTranCodeStart.Items)
                {
                    if (idPair.ID == filter.TranCodeMin)
                    {
                        cmbTranCodeStart.SelectedItem = idPair;
                    }
                }
            }
            else
            {
                cmbTranCodeStart.SelectedIndex = -1;
            }

            if (filter.TranCodeMax.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbTranCodeEnd.Items)
                {
                    if (idPair.ID == filter.TranCodeMax)
                    {
                        cmbTranCodeEnd.SelectedItem = idPair;
                    }
                }
            }
            else
            {
                cmbTranCodeEnd.SelectedIndex = -1;
            }

            if (filter.TaxYearMin.HasValue)
            {
                foreach (string taxYear in cmbTaxYearStart.Items)
                {
                    if (taxYear.Replace("-", "") == filter.TaxYearMin.ToString())
                    {
                        cmbTaxYearStart.SelectedItem = taxYear;
                    }
                }
            }
            else
            {
                cmbTaxYearStart.SelectedIndex = -1;
            }

            if (filter.TaxYearMax.HasValue)
            {
                foreach (string taxYear in cmbTaxYearEnd.Items)
                {
                    if (taxYear.Replace("-", "") == filter.TaxYearMax.ToString())
                    {
                        cmbTaxYearEnd.SelectedItem = taxYear;
                    }
                }
            }
            else
            {
                cmbTaxYearEnd.SelectedIndex = -1;
            }

            if (filter.AccountPaymentType.HasValue)
            {
                foreach (DescriptionIDPair idPair in cmbPaymentType.Items)
                {
                    if (idPair.ID == filter.AccountPaymentType)
                    {
                        cmbPaymentType.SelectedItem = idPair;
                    }
                }
            }
            else
            {
                cmbPaymentType.SelectedIndex = -1;
            }
        }

        private void ViewModel_SortOptionsChanged(object sender, EventArgs e)
        {
            LoadSortList();
        }

        private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmStatusLists InstancePtr
		{
			get
			{
				return (frmStatusLists)Sys.GetInstance(typeof(frmStatusLists));
			}
		}

		protected frmStatusLists _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		string strYearChoice = "";
		bool boolLoaded;
		string strGridToolTipText = "";
		bool blnSearchValid;
		// these will be to pass the string on to the reports when needed
		public string strRSWhere = "";
		public string strRSFrom = "";
		public string strRSOrder = "";
		public string strBinaryWhere = "";
		public int lngMax;
		public bool boolFullStatusAmounts;
		public bool boolShowCurrentInterest;
		public bool boolShowLocation;
		public bool boolShowMapLot;
		public bool boolShowAddress;
		public int intShowOwnerType;
		public int intMasterReportType;
		public bool boolShowPaymentBreakdown;
		public bool boolPreLienOnly;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            // THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
            // WAS PREVIOUSELY SAVED
            string strTemp = "";
            int intCT;
            int intStart = 0;
            if (cmbReport.SelectedIndex == 2)
            {
                DeleteReport(cboSavedReport.SelectedIndex);
            }
            else if (cmbReport.SelectedIndex == 1)
            {
                LoadSavedReport(cmbReport.SelectedIndex);
            }
        }

        private void chkHardCode_Click(object sender, System.EventArgs e)
		{
            //HardCodeOptionChanged();   
           
        }

        private void HardCodeOptionChanged()
        {
            int intCT;
            //vsWhere.TextMatrix(6, 0, "Show Payment From");
            viewModel.UseHardCodedReport = (chkHardCode.CheckState == Wisej.Web.CheckState.Checked);
            if (viewModel.UseHardCodedReport)
            {
                // this turns the hard coded report on
                chkUseFullStatus.Enabled = true;               
                cmbHardCode.Enabled = true;
                chkREPPHardCoded.Enabled = true;
                chkShowPayments.Enabled = false;
                chkExcludePaid.Enabled = false;
                chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
                chkExcludePaid.CheckState = Wisej.Web.CheckState.Unchecked;
                fraSave.Enabled = false;
                cmbReport.Enabled = false;
                cmbNameOption.SelectedIndex = 1;
                cmbNameOption.Enabled = false;
                cmdAdd.Enabled = false;
                if (cmbHardCode.Items.Count > 0)
                {
                    // this should select the first item in the combo box
                    cmbHardCode.SelectedIndex = 0;
                }
                fcLabel3.Enabled = false;
                lstSort.Enabled = false;
                for (intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
                {
                    lstFields.SetSelected(intCT, false);
                    // deselect all of the boxes
                }
                fcLabel2.Enabled = false;
                lstFields.Enabled = false;
                // set the grid properties because some of the options do not work with hard coded reports
                // grey some of the boxes out
                // 3 - Balance Due
                //	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                BalanceDuePanel.Visible = false;
                //	// 6 - Payments From
                //	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 6, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                PaymentDateRangePanel.Visible = false;

                // 10- Lien or Regular
                //	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 10, 1, 10, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                BillStatusPanel.Visible = false;
            }
            else
            {
                // this turns the hard coded reports off
                cmbHardCode.Enabled = false;
                cmbHardCode.SelectedIndex = -1;
                chkREPPHardCoded.Enabled = false;
                fraSave.Enabled = true;
                chkShowPayments.Enabled = true;
                chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                chkUseFullStatus.Enabled = false;
                chkExcludePaid.Enabled = true;
                cmbReport.Enabled = true;
                cmdAdd.Enabled = true;
                fcLabel3.Enabled = true;
                lstSort.Enabled = true;
                fcLabel2.Enabled = true;
                lstFields.Enabled = true;
                cmbNameOption.Enabled = true;

                //	vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 1, 1, System.Drawing.Color.White);
                BalanceDuePanel.Visible = true;
                PaymentDateRangePanel.Visible = true;
                BillStatusPanel.Visible = true;
            }
            ShowAutomaticFields();
        }
		private void chkHardCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData)) / 0x10000;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
				{
					chkHardCode.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				else
				{
					chkHardCode.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkUseFullStatus_Click(object sender, System.EventArgs e)
		{

		}

		private void cmbHardCode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbHardCode.SelectedIndex >= 0)
            {
                var selectedReportType = cmbHardCode.ItemData(cmbHardCode.SelectedIndex);
                viewModel.DefaultReportType = (TaxBillingHardCodedReport) selectedReportType;
            }

            ShowAutomaticFields();
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
            SaveReport();
        }

        private void SaveReport()
        {
            if (!ValidateCriteriaControls())
            {
                return;
            }

            var reportFilter = GetFilter();
            if (!ValidateFilter(reportFilter))
            {
                return;
            }
            var reportName = Interaction.InputBox("Enter name for new report", "New Custom Report");
            reportName = modGlobalFunctions.RemoveApostrophe(reportName);
            if (string.IsNullOrWhiteSpace(reportName))
            {
                return;
            }
            viewModel.SetFilter(reportFilter);
            viewModel.SetOption(GetReportOption());

            viewModel.SaveReport(reportName);
            FCMessageBox.Show("Custom Report saved as " + reportName, MsgBoxStyle.Information | MsgBoxStyle.OkOnly,
                "TRIO Software");
            FillcboSavedReport();
        }
        private void cmdClear_Click(object sender, System.EventArgs e)
		{
            ClearFilters();
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
            //// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
            try
            {
                if (!ValidateCriteriaControls())
                {
                    return;
                }
                var reportFilter = GetFilter();
                if (!ValidateFilter(reportFilter))
                {
                    return;
                }

               
                viewModel.SetFilter(reportFilter);
                int intCT;

                if (!viewModel.UseHardCodedReport)
                {                 
                    chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                }
                
                
                 chkCurrentInterest.CheckState = chkUseFullStatus.CheckState;
                 var reportOption = GetReportOption();
                 if (reportFilter.PaymentDateRangeUsed() && reportOption.ReportType.ToLower() == "account" || reportOption.ReportType.ToLower() == "")
                 {
                     reportOption.ReportType = "Payments";
                     viewModel.SetReportType("Payments");
                 }
                 SetSortOptions();
                boolFullStatusAmounts = true;
                reportOption.FullStatusAmounts = true;
                intShowOwnerType = ((DescriptionIDPair)cmbNameOption.SelectedItem).ID;
              
                if (boolFullStatusAmounts)
                {
                    boolShowCurrentInterest = FCConvert.CBool(chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked);
                }
                else
                {
                    boolShowCurrentInterest = false;
                }
                if (reportFilter.TaxBillStatus == TaxBillStatusType.PreLien)                
                {
                    chkShowPayments.CheckState = Wisej.Web.CheckState.Checked;
                }
                boolShowPaymentBreakdown = FCConvert.CBool(chkShowPayments.CheckState == Wisej.Web.CheckState.Checked);
                reportOption.ShowPayments = boolShowPaymentBreakdown;
                if (reportFilter.AsOfDate != null)
                {
	                reportOption.AsOfDate = reportFilter.AsOfDate.Value;
                }
                else
				{ 
					reportOption.AsOfDate = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                
                viewModel.SetOption(reportOption);

                if (cmbReport.SelectedIndex <= 0)
                {
                   
                }
                else
                {
                    if (cboSavedReport.SelectedIndex < 0)
                    {
                        return;
                    }
                }

                if (!boolSaveReport)
                {
                    if (!viewModel.UseHardCodedReport)
                    {
                        if (IsAStatusListReport(viewModel.ReportType))
                        {
                            if (StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
                            {
                                viewModel.ShowStatusList();
                            }
                            else
                            {
                                var statusList = new arStatusLists();
                                var reportViewer = new frmReportViewer();
                                var reportConfiguration = viewModel.GetReportConfiguration();
                                statusList.Init(ref reportConfiguration);
                                reportViewer.Init(statusList, allowCancel: true);
                            }
                        }
                    }
                    else
                    {
                        var reportConfiguration = viewModel.GetReportConfiguration();
                        ShowHardCodedReport(viewModel.DefaultReportType,reportConfiguration);   
                    }
                }
                else
                {
                    var reportConfig = viewModel.GetReportConfiguration();

                }
               

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FCMessageBox.Show("ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message, MsgBoxStyle.Critical, "Print Status Lists ERROR");
			}
}

        private void SetSortOptions()
        {
            for (var index = 0; index < lstSort.ListCount; index ++)
            {
                var sortItem = viewModel.SortOptions.Where(s => s.Item.ID == lstSort.ItemData(index)).FirstOrDefault();
                if (sortItem != null)
                {
                    sortItem.IsSelected = lstSort.Selected(index);
                    sortItem.Item.OrderNumber = index + 1;
                }
            }
        }


        private TaxCollectionReportOption GetReportOption()
        {
            var reportOption = new TaxCollectionReportOption();
            try
            {
                if (chkHardCode.CheckState != CheckState.Checked)
                {
                    chkUseFullStatus.CheckState = CheckState.Checked;
                }

                chkCurrentInterest.CheckState = chkUseFullStatus.CheckState;
                reportOption.ShowCurrentInterest = chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked;
                reportOption.NameToShow = (TaxReportNameOption) ((DescriptionIDPair)cmbNameOption.SelectedItem).ID;
                reportOption.ShowSummaryOnly = chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked;
                reportOption.FullStatusAmounts = true;
                reportOption.ShowPayments = chkShowPayments.CheckState == CheckState.Checked;
                if (lstFields.Selected(0))
                {
                    reportOption.ShowLocation = true;
                }

                if (lstFields.Selected(1))
                {
                    reportOption.ShowMapLot = true;
                }
                if (lstFields.Selected(2))
                {
                    reportOption.ShowAddress = true;
                }

                if (chkREPPHardCoded.CheckState == CheckState.Checked)
                {
                    reportOption.ShowPersonalPropertyInformation = true;
                }
                else
                {
                    reportOption.ShowPersonalPropertyInformation = false;
                }
            }
            catch
            {
                reportOption.ShowLocation = false;
                reportOption.ShowMapLot = false;
            }
            return reportOption;
        }


        private bool ValidateFilter(TaxCollectionReportFilter reportFilter)
        {
            if (reportFilter.AccountMax.HasValue && reportFilter.AccountMin.HasValue)
            {
                if (reportFilter.AccountMax.Value < reportFilter.AccountMin.Value)
                {
                    MessageBox.Show("That maximum account cannot be less than the minimum account", "Invalid Range", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }
            }

            if (reportFilter.TaxYearMax.HasValue && reportFilter.TaxYearMin.HasValue &&
                reportFilter.TaxYearMax < reportFilter.TaxYearMin)
            {
                MessageBox.Show("The tax year range is invalid", "Invalid Range", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            if (!string.IsNullOrWhiteSpace(reportFilter.NameMax) && reportFilter.NameMax.CompareTo(reportFilter.NameMin) < 0)
            {
                MessageBox.Show("The end name cannot be less than the start name", "Invalid Range", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
       

            if (reportFilter.PaymentDateMax.HasValue && reportFilter.PaymentDateMin.HasValue &&
                reportFilter.PaymentDateMax.Value.CompareTo(
                    reportFilter.PaymentDateMin) < 0)
            {
                MessageBox.Show("The end payment date cannot be before the start payment date", "Invalid Range", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            if (reportFilter.RateRecordMax.HasValue && reportFilter.RateRecordMin.HasValue &&
                reportFilter.RateRecordMax < reportFilter.RateRecordMin)
            {
                MessageBox.Show("The maximum rate record cannot be less than the minimum rate record", "Invalid Range",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (reportFilter.TranCodeMax.HasValue && reportFilter.TranCodeMin.HasValue &&
                reportFilter.TranCodeMax < reportFilter.TranCodeMin)
            {
                MessageBox.Show("The maximum tran code cannot be less than the minimum tran code", "Invalid Range",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private TaxCollectionReportFilter GetFilter()
        {
            var reportFilter = new TaxCollectionReportFilter();

            if (AccountPanel.Visible)
            {
                if (!String.IsNullOrWhiteSpace(txtAccountStart.Text))
                {
                    reportFilter.AccountMin = Convert.ToInt32(txtAccountStart.Text);
                }

                if (!String.IsNullOrWhiteSpace(txtAccountEnd.Text))
                {
                    reportFilter.AccountMax = Convert.ToInt32(txtAccountEnd.Text);
                }
            }

            if (namePanel.Visible)
            {
                if (!String.IsNullOrWhiteSpace(txtNameStart.Text))
                {
                    reportFilter.NameMin = txtNameStart.Text.Trim();
                }

                if (!String.IsNullOrWhiteSpace(txtNameEnd.Text))
                {
                    reportFilter.NameMax = txtNameEnd.Text.Trim();
                }
            }

            if (taxYearPanel.Visible)
            {
                if (cmbTaxYearStart.SelectedIndex >= 0)
                {
                    reportFilter.TaxYearMin = Convert.ToInt32( cmbTaxYearStart.Items[cmbTaxYearStart.SelectedIndex].ToString().Replace("-",""));
                }

                if (cmbTaxYearEnd.SelectedIndex >= 0)
                {
                    reportFilter.TaxYearMax =
                        Convert.ToInt32(cmbTaxYearEnd.Items[cmbTaxYearEnd.SelectedIndex].ToString().Replace("-", ""));
                }
            }

            if (BalanceDuePanel.Visible)
            {
                if (cmbBalanceDueOption.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbBalanceDueOption.SelectedItem;
                    reportFilter.BalanceDueComparisonType = (ComparisonOptionType) idPair.ID;
                }

                if (!String.IsNullOrWhiteSpace(txtBalanceDue.Text))
                {
                    reportFilter.BalanceDueAmount = Convert.ToDecimal(txtBalanceDue.Text);
                }
            }

            if (DateRangePanel.Visible)
            {
                reportFilter.AsOfDate = dtpAsOfDate.NullableValue;
            }

            if (AccountTypePanel.Visible)
            {
                if (cmbPaymentType.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbPaymentType.SelectedItem;
                    reportFilter.AccountPaymentType = idPair.ID;
                }
            }

            if (PaymentDateRangePanel.Visible)
            {
                reportFilter.PaymentDateMin = dtpShowPaymentStart.NullableValue;
                reportFilter.PaymentDateMax = dtpShowPaymentEnd.NullableValue;
            }

            if (SupplementalBillPanel.Visible)
            {
                reportFilter.SupplementalBillDateStart = dtpSupplementalBillStart.NullableValue;
                reportFilter.SupplementalBillDateEnd = dtpSupplementalBillEnd.NullableValue;
            }

            if (TranCodePanel.Visible)
            {
                if (cmbTranCodeStart.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbTranCodeStart.SelectedItem;
                    reportFilter.TranCodeMin = idPair.ID;
                }

                if (cmbTranCodeEnd.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbTranCodeEnd.SelectedItem;
                    reportFilter.TranCodeMax = idPair.ID;
                }
            }

            if (TaxAcquiredPanel.Visible)
            {
                if (cmbTaxAcquired.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbTaxAcquired.SelectedItem;
                    reportFilter.IsTaxAcquired = idPair.ID == (int)TaxAcquiredOptionType.TaxAcquired;
                }
            }

            if (RateRecordPanel.Visible)
            {
                if (cmbRateRecordStart.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbRateRecordStart.SelectedItem;
                    reportFilter.RateRecordMin = idPair.ID;
                }

                if (cmbRateRecordEnd.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbRateRecordEnd.SelectedItem;
                    reportFilter.RateRecordMax = idPair.ID;
                }
            }

            if (BillStatusPanel.Visible)
            {
                if (cmbBillStatusType.SelectedIndex >= 0)
                {
                    DescriptionIDPair idPair = (DescriptionIDPair) cmbBillStatusType.SelectedItem;                    
                    switch (idPair.ID)
                    {
                        case 1:
                            reportFilter.TaxBillStatus = TaxBillStatusType.Regular;
                            break;
                        case 2:
                            reportFilter.TaxBillStatus = TaxBillStatusType.Lien;
                            break;
                        case 3:
                            reportFilter.TaxBillStatus = TaxBillStatusType.PreLien;
                            break;
                    }
                }
            }
            return reportFilter;
        }

        private void ShowHardCodedReport(TaxBillingHardCodedReport defaultReportType,TaxCollectionStatusReportConfiguration reportConfiguration)
        {
            switch (defaultReportType)
            {
                case TaxBillingHardCodedReport.AccountDetailReport:
                    reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.AccountDetailReport;
                    var accountDetailReport = new rptStatusListAccountDetail();
                    accountDetailReport.SetReportOption(reportConfiguration);
                    frmReportViewer.InstancePtr.Init(accountDetailReport, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.NonZeroBalanceOnNonLienAccounts:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.NonZeroBalanceOnNonLienAccounts;
                    var outstandingBalances = new rptOutstandingBalances();
                    outstandingBalances.SetReportOptionAndParent(reportConfiguration, null,0);
                    frmReportViewer.InstancePtr.Init(outstandingBalances, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.AuditSummaryReport:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.AuditSummaryReport;
                    var auditSummary = new rptAuditStatusAll();                   
                    auditSummary.SetReportOption(reportConfiguration);
                    frmReportViewer.InstancePtr.Init(auditSummary, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.BankruptAccounts:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.BankruptAccounts;
                    var bankruptAccounts = new rptBankruptAccounts();                    
                    bankruptAccounts.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(bankruptAccounts, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.LienBreakdown:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.LienBreakdown;
                    var report = new arLienStatusReport();                    
                    report.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(report, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.NegativeBalanceReport:
                    reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.NegativeBalanceReport;
                    var negativeBalanceReport = new rptOutstandingBalancesAll();
                    negativeBalanceReport.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(negativeBalanceReport, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.NonZeroBalanceOnAllAccounts:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.NonZeroBalanceOnAllAccounts;
                    var outstandingBalancesAll = new rptOutstandingBalancesAll();
                    outstandingBalancesAll.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(outstandingBalancesAll, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.NonZeroBalanceOnAllAccountsByYear:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.NonZeroBalanceOnAllAccountsByYear;
	                var nzbByYear = new rptOutstandingBalancesAll();
                    nzbByYear.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(nzbByYear, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.NonZeroBalanceOnLienAccounts:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.NonZeroBalanceOnLienAccounts;
                    var outstandingLienBalances = new rptOutstandingLienBalances();
                    outstandingLienBalances.SetReportOptionAndParent(reportConfiguration,null,0);
                    frmReportViewer.InstancePtr.Init(outstandingLienBalances, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.OutstandingBalanceByPeriod:
                    var outstandingBalanceCommand = new ShowOutstandingPeriodBalanceReportCommand(reportConfiguration);
                    var rateChoice = new frmRateRecChoice();
                    rateChoice.intRateType = 50;
                    rateChoice.SetCommand(outstandingBalanceCommand);
                    rateChoice.Show(App.MainForm);
                    break;
                case TaxBillingHardCodedReport.SupplementalBills:
	                reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.SupplementalBills;
                    var supplementalBills = new rptAuditStatusAll();                    
                    supplementalBills.SetReportOption(reportConfiguration);
                    frmReportViewer.InstancePtr.Init(supplementalBills, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.SupplementalNegativeBalanceReport:
                    reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.SupplementalNegativeBalanceReport;
                    var supplementalNegativeBalance = new rptOutstandingBalancesAll();
                    supplementalNegativeBalance.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(supplementalNegativeBalance, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.SupplementalOutstandingBalanceReport:
                    reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.SupplementalOutstandingBalanceReport;
                    var supplementalOutstandingBalance = new rptOutstandingBalancesAll();
                    supplementalOutstandingBalance.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(supplementalOutstandingBalance, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.SupplementalZeroBalanceReport:
                    reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.SupplementalZeroBalanceReport;
                    var supplementalZeroBalance = new rptOutstandingBalancesAll();
                    supplementalZeroBalance.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(supplementalZeroBalance, allowCancel: true);
                    break;
                case TaxBillingHardCodedReport.ZeroBalanceReport:
                    reportConfiguration.Options.DefaultReportType = TaxBillingHardCodedReport.ZeroBalanceReport;
                    var zeroBalanceReport = new rptOutstandingBalancesAll();
                    zeroBalanceReport.SetReportOption(ref reportConfiguration);
                    frmReportViewer.InstancePtr.Init(zeroBalanceReport, allowCancel: true);
                    break;
            }
        }

		public void cmdPrint_Click()
        {
            btnProcess.Focus();
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void frmStatusLists_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				lngMax = 11;
				// put the combobox in the first question so that the user can choose
				EnableFrames_2(true);
				App.DoEvents();                
                viewModel.SetReportType("Account");
				//modCustomReport.SetFormFieldCaptions(this, "ACCOUNT");
				ShowWhereQuestions();
				ShowAutomaticFields();
				boolLoaded = true;
				blnSearchValid = true;
			}
		}

		private void frmStatusLists_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData)) / 0x10000;
			if (KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void frmStatusLists_Load(object sender, System.EventArgs e)
		{
			this.Text = "Status Lists";
			this.HeaderText.Text = this.Text;
			FillHardCodeCombo();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);		
			FormatGrid();
			//modGlobal.Statics.gdtStatusListAsOfDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy") + " 11:59:59 PM");
            SetupControls();
		}

        private void SetupControls()
        {
            LoadSortList();
            FillNameCombo();
            FillTaxYearCombos();
            FillCmbPaymentType();
            FillBillStatusType();
            FillTranCodeCombos();
            FillBalanceDueComparisonOptions();
            FillTaxAcquiredCombo();
            FillRateRecCombo();
        }

        private void FillTaxAcquiredCombo()
        {
            var taxAcquiredOptions = viewModel.TaxAcquiredOptions;
            foreach (var option in taxAcquiredOptions)
            {
                cmbTaxAcquired.Items.Add(option);
            }
        }

        private void FillRateRecCombo()
        {
            var rateRecs = viewModel.RateRecords;
            foreach (var rateRec in rateRecs)
            {
                cmbRateRecordStart.Items.Add(rateRec);
                cmbRateRecordEnd.Items.Add(rateRec);
            }
        }

        public void FillcboSavedReport()
		{            
            cboSavedReport.Items.Clear();
            var savedReports = viewModel.StatusReportDescriptions;
            foreach(var report in savedReports)
            {
                cboSavedReport.Items.Add(report);
            }
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		//private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		//{
		//	cmdClear_Click();
		//}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// If lstFields.ListIndex < 0 Then Exit Sub
			// If vsLayout.Row < 1 Then vsLayout.Row = 1
			// 
			// vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col) = lstFields.List(lstFields.ListIndex)
			// vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col) = lstFields.ItemData(lstFields.ListIndex)
		}

        private void mnuClear_Click(object sender, System.EventArgs e)
        {
            cmdClear_Click(sender,e);
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optReport_Click(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = Index == 0;
			//FC:FINAL:CHN: Add missing changing enabling.
			fcLabel3.Enabled = Index == 0;
			lstSort.Enabled = fcLabel3.Enabled;
			fcLabel2.Enabled = Index == 0;
			lstFields.Enabled = fcLabel2.Enabled;
			filterPanel.Enabled = Index == 0;
			fraQuestions.Enabled = Index == 0;
            fraQuestions2.Enabled = fraQuestions.Enabled;
        }

		private void optReport_Click(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_Click(index, sender, e);
		}

		private void vsQuestions_Click(object sender, EventArgs e)
		{
			vsQuestions_RowColChange(vsQuestions, EventArgs.Empty);
		}

		private void vsQuestions_DblClick(object sender, EventArgs e)
		{
			vsQuestions_RowColChange(vsQuestions, EventArgs.Empty);
		}

		private void vsQuestions_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space)
			{
				vsQuestions_RowColChange(vsQuestions, EventArgs.Empty);
			}
			else if (e.KeyCode == Keys.Return)
			{
				vsQuestions_Validate_2(false);
			}
		}

		private void vsQuestions_RowColChange(object sender, EventArgs e)
		{
			if (vsQuestions.Col == 1)
			{
				vsQuestions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				switch (vsQuestions.Row)
				{
					case 0:
						{
							// Type of Report
							vsQuestions.ComboList = "#0;Account Listing|#1;Pre Payments|#2;Tax Club Payments|#3;Outstanding Tax Lien|#4;Accounts with Abatements";
							break;
						}
					case 1:
						{
							// Naming Convention
							vsQuestions.ComboList = "#0;Name on Account at time of Billing|#1;Current Property Owner|#2;Name on Account at time of Billing with 'C/O'|#3;Current Property Owner with Previous Owner";
							break;
						}
					case 2:
						{
							// Payment Summary
							vsQuestions.ComboList = "#0;Yes|#1;No";
							break;
						}
					default:
						{
							vsQuestions.Editable = FCGrid.EditableSettings.flexEDNone;
							vsQuestions.ComboList = "";
							break;
						}
				}
				//end switch
				// check to see if this is the last row shown
				if (vsQuestions.Row > 0)
				{
					// if it is not the first question
					// If .Row = 4 Then                                'is it the last row
					// .TabBehavior = flexTabControls
					// Else                                            'it is not the last row
					// If .RowHeight(4) = 0 Then                   'is the last row hidden
					// If .Row = 3 Then                        'if so, is it the next to last
					// .TabBehavior = flexTabControls
					// Else
					// If .RowHeight(3) = 0 Then
					if (vsQuestions.Row == 2)
					{
						vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						if (vsQuestions.RowHeight(2) == 0)
						{
							if (vsQuestions.Row == 1)
							{
								vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							}
							else
							{
								vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
							}
						}
						else
						{
							vsQuestions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
						}
					}
					// Else
					// .TabBehavior = flexTabCells
					// End If
					// End If
					// Else
					// .TabBehavior = flexTabCells
					// End If
					// End If
				}
			}
			else
			{
				vsQuestions.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsQuestions_Validate_2(bool Cancel)
		{
			vsQuestions_Validate(ref Cancel);
		}

		private void vsQuestions_Validate(ref bool Cancel)
		{
			int lngRow;
			for (lngRow = 0; lngRow <= 2; lngRow++)
			{
				if (Conversion.Val(vsQuestions.ComboData()) < 0 && Conversion.Val(vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1)) != ColorTranslator.ToOle(vsQuestions.BackColorFixed))
				{
					FCMessageBox.Show("Please answer all of the questions.", MsgBoxStyle.Critical, "Insufficient Data");
					Cancel = true;
					return;
				}
			}
			if (vsQuestions.RowHeight(0) > 0 && Conversion.Val(vsQuestions.ComboData()) > 0 && FCConvert.ToBoolean(Conversion.Val(vsQuestions.RowHeight(1) > 0 || vsQuestions.RowHeight(2) > 0)))
			{
				if (fcLabel3.Enabled == false)
				{
					// if it makes it this far, then the user has answered all of the questions needed for this report
					// so we can enable the other frame and show the data at the bottom
					EnableFrames_2(true);
					ShowWhereQuestions();
				}
				// Else
				// MsgBox "Please answer all of the questions.", MsgBoxStyle.Critical, "Insufficient Data"
				// Cancel = True
				// Exit Sub
			}
		}

		private void vsQuestions_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsQuestions.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsQuestions.GetFlexRowIndex(e.RowIndex);
				int col = vsQuestions.GetFlexColIndex(e.ColumnIndex);
				if (col == 1)
				{
					vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, vsQuestions.ComboData(row));
					switch (row)
					{
						case 0:
							{
								// Type of Report
								ChangeReportType(FCConvert.ToInt32(vsQuestions.ComboData()));
								break;
							}
					}
					//end switch
				}
				else
				{
					// do nothing
				}
			}
		}


		



		private void EnableFrames_2(bool boolEnable)
		{
			EnableFrames(ref boolEnable);
		}

		private void EnableFrames(ref bool boolEnable)
		{
			fcLabel2.Enabled = boolEnable;
			// fraNotes.Enabled = boolEnable
			fcLabel3.Enabled = boolEnable;
			// fraSave.Enabled = boolEnable
			filterPanel.Enabled = boolEnable;
			lstFields.Enabled = boolEnable;
			// lstFields.Enabled = False
			// fraFields.Enabled = False
			lstSort.Enabled = boolEnable;
			// fraQuestions.Enabled = Not boolEnable
		}

		private void FormatGrid()
		{
			int lngWidth = 0;
			string strTemp = "";
			// this will format the Questions Grid and add the questions and answers to it
			lngWidth = vsQuestions.WidthOriginal;
			vsQuestions.Cols = 2;
			vsQuestions.ColWidth(0, FCConvert.ToInt32(lngWidth * 0.8));
			vsQuestions.ColWidth(1, FCConvert.ToInt32(lngWidth * 0.17));
			vsQuestions.Rows = 4;
			vsQuestions.TextMatrix(0, 0, "Which type of report would you like to create?");
			// if selection for question 1 is #1,2,3,4,5,7 then
			vsQuestions.TextMatrix(1, 0, "How would you like to show the name?");
			// if selection for question 1 is #6 then
			vsQuestions.TextMatrix(2, 0, "Complete file?");
			// hide the other questions
			//FC:FINAL:MHO:#i182 - Hide the row instead of setting its height to 0
			//vsQuestions.RowHeight(1, 0);
			//vsQuestions.RowHeight(2, 0);
			vsQuestions.RowHidden(1, true);
			vsQuestions.RowHidden(2, true);
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//vsQuestions.HeightOriginal = vsQuestions.RowHeight(0) + 50;
			// 
			// strTemp = "1. Choose the type of report to create from the drop down box at the top of the screen or Load a Saved Report from the drop down boxes at the right of the screen." & vbCrLf & vbCrLf
			// strTemp = strTemp & "2. Answer all of the questions about the report that you have chosen." & vbCrLf & vbCrLf
			// strTemp = strTemp & "3. Choose the fields that you would like shown from the 'Fields to display on Report' list." & vbCrLf & vbCrLf
			// strTemp = strTemp & "4. Choose the fields that you would like the report sorted by from the 'Fields to sort by' list." & vbCrLf & vbCrLf
			// strTemp = strTemp & "5. Set any criteria needed in the 'Select Search Criteria' list." & vbCrLf
			// strTemp = strTemp & vbCrLf & "Some fields will be printed automatically:" & vbCrLf
			// txtNotes.Text = strTemp
			strTemp = "1. Create a new report, a default report or select a saved report from the drop down box on the right of the screen." + "\r\n" + "\r\n";
			strTemp += "2. Choose the fields that you would like on your report from the Fields To Display On Report." + "\r\n" + "\r\n";
			strTemp += "3. The Fields To Sort By section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
			strTemp += "4. Set any criteria needed in the Select Search Criteria list." + "\r\n" + "\r\n";
			strTemp += "Other Notes:" + "\r\n" + "Some fields will be printed automatically." + "\r\n" + "\r\n";
			strTemp += "\r\n" + "If you choose a default report, only the account number and the tax year criteria will affect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			//txtNotes.Text = strTemp;
		}
		// VBto upgrade warning: intType As int	OnWrite(string)
		private void ChangeReportType(int intType)
		{
			int lngWid = 0;
			int intRows = 0;
			switch (intType)
			{
				case 0:
					{
						// Account List
						ChangeQuestionStatus_8(false, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 1;
                        viewModel.SetReportType("Account");
						break;
					}
				case 1:
					{
						// Pre Payments
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
                        viewModel.SetReportType("PrePayments");
						break;
					}
				case 2:
					{
						// Tax Club
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
                        viewModel.SetReportType("TaxClub");					
						break;
					}
				case 3:
					{
						// Outstanding Tax Lien
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
                        viewModel.SetReportType("TaxLien");
						break;
					}
				case 4:
					{
						// Accounts with Abatements
						ChangeQuestionStatus_8(true, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 2;
                        viewModel.SetReportType("Abate");
						break;
					}
				default:
					{
						ChangeQuestionStatus_8(false, 1);
						ChangeQuestionStatus_8(false, 2);
						intRows = 1;
						lstFields.Clear();
						lstSort.Clear();
						break;
					}
			}
			ShowWhereQuestions();
		}

		private void ChangeQuestionStatus_8(bool boolReady, int lngRow)
		{
			ChangeQuestionStatus(ref boolReady, ref lngRow);
		}

		private void ChangeQuestionStatus(ref bool boolReady, ref int lngRow)
		{
			// this will enable/disable the questions in the row depending on what the user selects
			// change the background of the cell in the grid
			// maybe resize the row to height of 0
			if (boolReady)
			{
				vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, 1, vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1));
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//vsQuestions.RowHeight(lngRow, vsQuestions.RowHeight(0));
			}
			else
			{
				vsQuestions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, 1, vsQuestions.BackColorFixed);
				//vsQuestions.RowHeight(lngRow, 0);
			}
		}

		//private void FindAllYears()
		//{
		//	// this will fill the variable strYearChoice with all of the possible years and an All Years Option
		//	clsDRWrapper rsYear = new clsDRWrapper();
		//	int intNum = 0;
		//	rsYear.OpenRecordset("SELECT * FROM PaymentYears", modExtraModules.strCLDatabase);
		//	// rsYear.OpenRecordset "SELECT DISTINCT [Year] FROM PaymentRec", strCLDATABASE
		//	if (rsYear.EndOfFile() != true && rsYear.BeginningOfFile() != true)
		//	{
		//		strYearChoice = "";
		//		intNum = 1;
		//		strYearChoice = "#0;All Years|";
		//		while (!rsYear.EndOfFile())
		//		{
		//			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
		//			strYearChoice += "#" + FCConvert.ToString(intNum) + ";" + modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields("Year"))) + "|";
		//			intNum += 1;
		//			rsYear.MoveNext();
		//		}
		//		// take off the last pipe character '|'
		//		strYearChoice = Strings.Left(strYearChoice, strYearChoice.Length - 1);
		//	}
		//	else
		//	{
		//		strYearChoice = "#0;All Years";
		//	}
		//}

		private void ShowWhereQuestions()
		{


		}

	
		

		//private bool SaveAnswers()
		//{
		//	bool SaveAnswers = false;
		//	// this will take the answers given, validate them, find out what report
		//	// the user wants and show the frmStatusLists screen
		//	string strTypeOfReport = "";
		//	int intQuestions/*unused?*/;
		//	SaveAnswers = true;
		//	if (Strings.Trim(vsQuestions.TextMatrix(0, 1)) != "")
		//	{
		//		strTypeOfReport = vsQuestions.TextMatrix(0, 1);
		//		if (strTypeOfReport == "Account Listing")
		//		{
		//			// all set, that is the only answer it needs
		//		}
		//		else
		//		{
		//			if (Strings.Trim(vsQuestions.TextMatrix(1, 1)) != "")
		//			{
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("Please choose an answer for all of the questions.", MsgBoxStyle.Information, "Missing Answers");
		//				SaveAnswers = false;
		//				return SaveAnswers;
		//			}
		//		}
		//	}
		//	else
		//	{
		//		FCMessageBox.Show("Please choose an answer for all of the questions.", MsgBoxStyle.Information, "Missing Answers");
		//		SaveAnswers = false;
		//		return SaveAnswers;
		//	}
		//	return SaveAnswers;
		//}
		// VBto upgrade warning: intCounter As short	OnWriteFCConvert.ToInt32(
		private bool ItemInSortList(int intCounter)
		{
			bool ItemInSortList = false;
			// this will return true if the item has already been added to the Sort List
			int intTemp;
			ItemInSortList = false;
			for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
			{
				if (lstSort.ItemData(intTemp) == intCounter)
				{
					ItemInSortList = true;
					break;
				}
			}
			return ItemInSortList;
		}

		public void LoadSortList()
		{
            lstSort.Clear();
            var sortOptions = viewModel.SortOptions.OrderBy(o => o.Item.OrderNumber).ToList();
            foreach (var selectableDescriptionIDPair in sortOptions)
            {
                var descriptionIDPair = selectableDescriptionIDPair.Item;
                AddItemToSortList(descriptionIDPair.Description, descriptionIDPair.ID,selectableDescriptionIDPair.IsSelected);
            }
		}

        public void FillLstFields()
        {
            lstFields.Clear();
            // new Binding("Text", viewModel.AvailableFields, "Item.Description");
            foreach (var selectableDescriptionIDPair in viewModel.AvailableFields)
            {
                var descriptionIDPair = selectableDescriptionIDPair.Item;
                AddItemToFieldsList( descriptionIDPair.Description, descriptionIDPair.ID,selectableDescriptionIDPair.IsSelected);
            }
        }
        private void AddItemToFieldsList(string itemText, int itemData, bool isSelected)
        {
            lstFields.AddItem(itemText);
           // lstFields.Items.Add(new ListViewItem() { Text = itemText,   });
            var index = lstFields.Items.Count - 1;
            lstFields.ItemData(index, itemData);
            lstFields.SetSelected(index, isSelected);            
        }
 
        private void AddItemToSortList(string itemText, int itemData, bool selected)
        {
            lstSort.AddItem(itemText);
            var index = lstSort.Items.Count - 1;
            lstSort.ItemData(index, itemData);
            lstSort.Items[index].Selected = selected;
            lstSort.SetSelected(index, selected);
        }

		private void FillHardCodeCombo()
		{
			// this will fill the hard coded combo list with the list of reports that the user can choose
			cmbHardCode.Clear();
            foreach (var defaultReport in viewModel.DefaultReportTypes)
            {
                cmbHardCode.AddItem(defaultReport.Description);
                cmbHardCode.ItemData(cmbHardCode.NewIndex, defaultReport.ID);
            }
			
			if ( modStatusPayments.Statics.boolRE)
			{
				chkREPPHardCoded.Text = "Show PP Information";
			}
			else
			{
				chkREPPHardCoded.Text = "Show RE Information";
			}

		}
		// VBto upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		//private void ShowHardCodedReport_2(short intIndex)
		//{
		//	ShowHardCodedReport(ref intIndex);
		//}

		//private void ShowHardCodedReport(ref short intIndex)
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		switch (intIndex)
		//		{
		//			case 0:
		//				{
		//					// Non Zero Balance on Non Lien Accounts
  //                          var outstandingBalances = new rptOutstandingBalances();
  //                          outstandingBalances.SetReportOptionAndParent(reportConfiguration ,null);
		//					frmReportViewer.InstancePtr.Init(new rptOutstandingBalances());
		//					break;
		//				}
		//			case 1:
		//				{
		//					// Non Zero Balance on Lien Accounts
		//					frmReportViewer.InstancePtr.Init( new rptOutstandingLienBalances());
		//					break;
		//				}
		//			case 2:
		//				{
		//					// Non Zero Balance on All Accounts
		//					frmReportViewer.InstancePtr.Init(new rptOutstandingBalancesAll());
		//					intMasterReportType = 0;
		//					break;
		//				}
		//			case 3:
		//				{
		//					// Lien Breakdown Report
  //                          var report = new arLienStatusReport();
  //                          var reportConfiguration = viewModel.GetReportConfiguration();
  //                          report.SetReportOption(ref reportConfiguration);
  //                          frmReportViewer.InstancePtr.Init(report);
		//					break;
		//				}
		//			case 4:
		//				{
		//					// Zero Balance Report
		//					intMasterReportType = 4;
		//					frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
		//					break;
		//				}
		//			case 5:
		//				{
		//					// Negative Balance Report
		//					intMasterReportType = 5;
		//					frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
		//					break;
		//				}
		//			case 6:
		//				{
		//					// Supplemental Outstanding Balance Report
		//					intMasterReportType = 6;
		//					frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
		//					break;
		//				}
		//			case 7:
		//				{
		//					// Supplemental Negative Balance Report
		//					intMasterReportType = 7;
		//					frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
		//					break;
		//				}
		//			case 8:
		//				{
		//					// Supplemental Zero Balance Report
		//					intMasterReportType = 8;
		//					frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
		//					break;
		//				}
		//			case 9:
		//				{
		//					// Outstanding Balance By Period
		//					frmRateRecChoice.InstancePtr.intRateType = 50;
		//					frmRateRecChoice.InstancePtr.Show(App.MainForm);
		//					break;
		//				}
		//			case 10:
		//				{
		//					intMasterReportType = 10;
		//					frmReportViewer.InstancePtr.Init(rptStatusListAccountDetail.InstancePtr);
		//					break;
		//				}
		//			case 11:
		//				{
		//					intMasterReportType = 11;
  //                          var report = new rptAuditStatusAll();
  //                          var reportConfiguration = viewModel.GetReportConfiguration();
  //                          report.SetReportOption(ref reportConfiguration);
  //                          frmReportViewer.InstancePtr.Init(report);
		//					break;
		//				}
		//			case 12:
		//				{
		//					intMasterReportType = 12;
  //                          var report = new rptAuditStatusAll();
  //                          var reportConfiguration = viewModel.GetReportConfiguration();
  //                          report.SetReportOption(ref reportConfiguration);
  //                          frmReportViewer.InstancePtr.Init(report);                            
		//					break;
		//				}
		//			case 13:
		//				{
		//					// Non Zero Balance on All Accounts Ordered By Year / Name
		//					intMasterReportType = 13;
		//					frmReportViewer.InstancePtr.Init(rptOutstandingBalancesAll.InstancePtr);
		//					break;
		//				}
		//			case 14:
		//				{
		//					// Bankrupt Account Information
		//					intMasterReportType = 14;
  //                          var report = new rptBankruptAccounts();
  //                          var reportConfiguration = viewModel.GetReportConfiguration();
  //                          report.SetReportOption(ref reportConfiguration);
		//					frmReportViewer.InstancePtr.Init(report);
		//					break;
		//				}
		//			default:
		//				{
		//					break;
		//				}
		//		}
		//		//end switch
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Showing Hard Coded Report");
		//	}
		//}

		private void ShowAutomaticFields()
		{
            string strRptType = "";
            strRptType = viewModel.ReportType;
            chkUseFullStatus.Enabled = true;
            // MAL@20070910: Change to make the Show Current Interest require the user to purposely choose this option
            chkUseFullStatus.CheckState = Wisej.Web.CheckState.Unchecked;
            PaymentDateRangePanel.Visible = true;
            SupplementalBillPanel.Visible = false;
            if (!viewModel.UseHardCodedReport)
            //if (chkHardCode.CheckState != Wisej.Web.CheckState.Checked)
            {
                // this is not a hard coded report
                chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                chkUseFullStatus.Enabled = false;
                if (strRptType == "LIEN BREAKDOWN")
                {
                    chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                    chkUseFullStatus.Enabled = false;
                }                
            }
            else
            {
                // this is a hardcoded report
                if (cmbHardCode.SelectedIndex != -1)
                {                    

                    switch (cmbHardCode.ItemData(cmbHardCode.SelectedIndex))
                    {
                        case (int)TaxBillingHardCodedReport.NonZeroBalanceOnNonLienAccounts:
                            {
                                chkREPPHardCoded.Enabled = true;
                                break;
                            }
                        case (int)TaxBillingHardCodedReport.LienBreakdown:
                            {
                                chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                                chkUseFullStatus.Enabled = false;                               
                                chkREPPHardCoded.CheckState = Wisej.Web.CheckState.Unchecked;
                                chkREPPHardCoded.Enabled = false;
                                break;
                            }
                        case (int)TaxBillingHardCodedReport.SupplementalBills:
                            {
                                PaymentDateRangePanel.Visible = false;
                                SupplementalBillPanel.Visible = true;
                                chkREPPHardCoded.CheckState = Wisej.Web.CheckState.Unchecked;
                                chkREPPHardCoded.Enabled = false;
                                break;
                            }
                        case (int)TaxBillingHardCodedReport.NonZeroBalanceOnAllAccountsByYear:
                            {
                                chkREPPHardCoded.Enabled = true;
                                break;
                            }
                        default:
                            {
                                chkREPPHardCoded.CheckState = Wisej.Web.CheckState.Unchecked;
                                chkREPPHardCoded.Enabled = false;
                                break;
                            }
                    }
                    //end switch
                }
            }
        }

		private bool ValidateCriteriaControls()
        {            
            var numericRegEx = new Regex(@"^([0-9]+)?(\.[0-9][0-9]?)?$");
            var integerRegEx = new Regex("^([0-9]+)?$");
            if (AccountPanel.Visible)
            {
                if (txtAccountStart.Text != "" && !integerRegEx.IsMatch(txtAccountStart.Text))
                {
                    MessageBox.Show("Invalid minimum account", "Invalid Account", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }

                if (txtAccountEnd.Text != "" && !integerRegEx.IsMatch(txtAccountEnd.Text))
                {
                    MessageBox.Show("Invalid maximum account", "Invalid Account", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }
            }
            if (BalanceDuePanel.Visible)
            {
                if (txtBalanceDue.Text != "" &&  !numericRegEx.IsMatch(txtBalanceDue.Text))
                {
                    MessageBox.Show("Invalid balance amount", "Invalid Balance", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }
            }

            return true;
        }

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPrint_Click(sender, e);
		}

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            this.mnuClear_Click(sender, e);
        }

        private void ClearFilters()
        {
            viewModel.ClearFilter();
        }

        private void DeleteReport(int selectedIndex)
        {
            if (selectedIndex < 0)
            {
                return;
            }
            var reportDescriptionIdPair = (DescriptionIDPair)cboSavedReport.SelectedItem;
            if (FCMessageBox.Show("This will delete the custom report " + reportDescriptionIdPair.Description + ". Continue?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "TRIO Software") == DialogResult.Yes)
            {    
                var reportID = reportDescriptionIdPair.ID;
                viewModel.DeleteSavedStatusReport(reportID);
                //FillcboSavedReport();
                FCMessageBox.Show("Custom report deleted successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
            }
        }
        private void LoadSavedReport(int selectedIndex)
        {
            if (selectedIndex < 0)
            {
                return;
            }
            // show the report
            var selectedItem = (DescriptionIDPair)cboSavedReport.SelectedItem;
            var savedReport = viewModel.GetStatusReport(selectedItem.ID);
            viewModel.LoadReport(selectedItem.ID);
            //if (savedReport != null)
            //{
            //    ShowSavedReport(savedReport);
            //}
            //else
            //{
            //    FCMessageBox.Show("There was an error while opening this file.", MsgBoxStyle.Critical, "Load Error");
            //}
            cmbReport.SelectedIndex = 0;
        }
        private void ShowSavedReport(SavedStatusReport savedReport)
        {          
            SetCheckBoxesFromReport(savedReport);
            SelectFieldsFromReport(savedReport);
            FillSortListFromReport(savedReport);
            FillFilter(savedReport);
        }
        private void SetCheckBoxesFromReport(SavedStatusReport savedReport)
        {
            if (savedReport.ShowSummaryOnly ?? false)
            {
                chkSummaryOnly.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkSummaryOnly.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (savedReport.ShowPayments ?? false)
            {
                chkShowPayments.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (savedReport.ShowCurrentInterest ?? false)
            {
                chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkUseFullStatus.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (savedReport.ExcludePaid ?? false)
            {
                chkExcludePaid.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkExcludePaid.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }
        private void FillSortListFromReport(SavedStatusReport savedReport)
        {
            lstSort.Clear();
            var strTemp = savedReport.SortSelection;
            if (strTemp.Length > 0)
            {
                var intCT = 0;
                do
                {
                    intStart = intCT + 1;
                    intCT = Strings.InStr(intStart, strTemp, ",");
                    if (intCT > intStart)
                    {
                        // intStart will be the index of the next sort
                        intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, intStart, intCT - 1))));
                    }
                    else
                    {
                        intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(strTemp, strTemp.Length - (intStart - 1)))));
                    }
                    // add the item to the list
                    if (modCustomReport.Statics.strCaptions[intStart] != "")
                    {
                        lstSort.AddItem(modCustomReport.Statics.strCaptions[intStart]);
                        // enter the index in the item data
                        lstSort.ItemData(lstSort.NewIndex, intStart);
                        lstSort.SetSelected(lstSort.NewIndex, true);
                        intStart = intCT + 1;
                    }
                }
                while (!(intCT == 0));
            }
            // load the rest of the fields that are not selected
            for (var intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
            {
                if (modCustomReport.Statics.strCaptions[intCT] != "")
                {
                    if (ItemInSortList(intCT))
                    {
                        // do nothing
                    }
                    else
                    {
                        lstSort.AddItem(lstFields.Items[intCT].Text);
                        lstSort.ItemData(lstSort.NewIndex, lstFields.ItemData(intCT));
                    }
                }
            }
        }
        private void SelectFieldsFromReport(SavedStatusReport savedReport)
        {
            var selection = savedReport.WhereSelection;
            for (var intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
            {
                if (Strings.Mid(selection, intCT + 1, 1) == "1")
                {
                    lstFields.SetSelected(intCT, true);
                }
                else
                {
                    lstFields.SetSelected(intCT, false);
                }
            }
        }
        private void FillFilter(SavedStatusReport statusReport)
        {
            for (var intCT = 0; intCT < 10; intCT++)
            {
                switch (intCT)
                {
                    case 0:
                        var accountTuple = ParseConstraint(statusReport.FieldConstraint0);
                        txtAccountEnd.Text = accountTuple.start;
                        txtAccountStart.Text = accountTuple.end;
                        break;
                    case 1:
                        var nameTuple = ParseConstraint(statusReport.FieldConstraint1);
                        txtNameStart.Text = nameTuple.start;
                        txtNameEnd.Text = nameTuple.end;
                        break;
                    case 2:
                        var taxYearTuple = ParseConstraint(statusReport.FieldConstraint2);
                        SetCombo(cmbTaxYearStart,taxYearTuple.start);
                        SetCombo(cmbTaxYearEnd,taxYearTuple.end);
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                }
            }
        }
        private (string start, string end) ParseConstraint(string constraint)
        {
            var processedConstraint = constraint.Replace("|||", "|");
            (string start, string end) parsedValues = (start: "", end: "");
            var constraintArray = processedConstraint.Split('|');
            if (constraintArray.Length > 0)
            {
                parsedValues.start = constraintArray[0].Trim();
            }
            if (constraintArray.Length > 1)
            {
                parsedValues.end = constraintArray[1].Trim();
            }
            return parsedValues;
        }
        private void SetCombo(ComboBox comboBox,string comboSetting)
        {
            var itemIndex = comboBox.Items.IndexOf(comboSetting);
            if (itemIndex >= 0)
            {
                comboBox.SelectedIndex = itemIndex;
            }
            else
            {
                comboBox.SelectedIndex = -1;
            }
        }

        private void FillNameCombo()
        {
            var nameOptions = viewModel.NameOptions;
            cmbNameOption.Items.Clear();
            foreach (var idPair in nameOptions)
            {
                cmbNameOption.Items.Add(idPair);
            }

            cmbNameOption.SelectedItem = cmbNameOption.Items[0];
        }
        private void FillTaxYearCombos()
        {
            var taxYears = viewModel.TaxYears;
            foreach(string taxYear in taxYears)
            {
                cmbTaxYearStart.Items.Add(taxYear);                
                cmbTaxYearEnd.Items.Add(taxYear);
            }
        }

        private void FillTranCodeCombos()
        {
            var tranCodes = viewModel.TranCodes;
            foreach (var tranCode in tranCodes)
            {
                cmbTranCodeStart.Items.Add(tranCode);
                cmbTranCodeEnd.Items.Add(tranCode);
            }
        }

        private void FillBalanceDueComparisonOptions()
        {
            var options = viewModel.ComparisonOptions;
            foreach (var option in options)
            {
                cmbBalanceDueOption.Items.Add(option);               
            }
        }

        private void FillCmbPaymentType()
        {
            var transactionTypes = viewModel.TransactionTypes;
            foreach (var transactionType in transactionTypes)
            {
                cmbPaymentType.Items.Add(transactionType);
            }
        }

        private void SetAvailableFieldsListFromScreen()
        {
            for (int x = 0; x < lstFields.Items.Count - 1; x++)
            {
                var item = viewModel.AvailableFields.Where(o => o.Item.ID == lstFields.ItemData(x)).FirstOrDefault();
                if (item != null)
                {
                    item.IsSelected = lstFields.Selected(x);
                }
            }
        }

        private void chkHardCode_CheckedChanged(object sender, EventArgs e)
        {
            HardCodeOptionChanged();
        }

        private void FillBillStatusType()
        {
            var statusTypes = viewModel.BillStatusTypes;
            foreach (var statusType in statusTypes)
            {
                cmbBillStatusType.Items.Add(statusType);
            }
        }

        private bool IsAStatusListReport(string reportType)
        {
            if ((reportType.ToUpper() == "ACCOUNT") || (reportType.ToUpper() == "ABATE") ||
                (reportType.ToUpper() == "SUPPLEMENTAL") || (reportType.ToUpper() == "DISCOUNT") ||
                (reportType.ToUpper() == "TAXCLUB") || (reportType.ToUpper() == "PREPAYMENT") ||
                (reportType.ToUpper() == "COSTS") || (reportType.ToUpper() == "REFUNDEDABATE") ||
                (reportType.ToUpper() == "PAYMENTS") || (reportType.ToUpper() == "CORRECTIONS"))
            {
                return true;
            }

            return false;
        }

        private void ShowOptions()
        {
            if (viewModel.ReportOption.ExcludePaid)
            {
                chkExcludePaid.Value = FCCheckBox.ValueSettings.Checked;
            }
            else
            {
                chkExcludePaid.Value = FCCheckBox.ValueSettings.Unchecked;
            }

            if (viewModel.ReportOption.ShowCurrentInterest)
            {
                chkCurrentInterest.Value = FCCheckBox.ValueSettings.Checked;
            }
            else
            {
                chkCurrentInterest.Value = FCCheckBox.ValueSettings.Unchecked;
            }

            chkShowPayments.Value = viewModel.ReportOption.ShowPayments
                ? FCCheckBox.ValueSettings.Checked
                : FCCheckBox.ValueSettings.Unchecked;
            chkSummaryOnly.Value = viewModel.ReportOption.ShowSummaryOnly
                ? FCCheckBox.ValueSettings.Checked
                : FCCheckBox.ValueSettings.Unchecked;
        }

        private void chkExcludePaid_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cmdAsOfDateCalendar_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpAsOfDate.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpAsOfDate.Value);
            }
            else
            {
               frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }
            
            if (datSelectedDate.ToOADate() != 0)
            {
                dtpAsOfDate.Value = datSelectedDate;
            }
        }

        private void cmdShowPaymentFromCalendar_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpShowPaymentStart.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpShowPaymentStart.Value);
            }
            else
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }

            if (datSelectedDate.ToOADate() != 0)
            {
                dtpShowPaymentStart.Value = datSelectedDate;
            }
        }

        private void fcButton1_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpShowPaymentEnd.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpShowPaymentEnd.Value);
            }
            else
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }

            if (datSelectedDate.ToOADate() != 0)
            {
                dtpShowPaymentEnd.Value = datSelectedDate;
            }
        }

        private void dtpSupplementalBillStart_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cmdSupplementalBillStartShowCalendar_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpSupplementalBillStart.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpSupplementalBillStart.Value);
            }
            else
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }

            if (datSelectedDate.ToOADate() != 0)
            {
                dtpSupplementalBillStart.Value = datSelectedDate;
            }
        }

        private void cmdSupplementalBillEndShowCalendar_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpSupplementalBillEnd.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpSupplementalBillEnd.Value);
            }
            else
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }

            if (datSelectedDate.ToOADate() != 0)
            {
                dtpSupplementalBillEnd.Value = datSelectedDate;
            }
        }
	}
}
