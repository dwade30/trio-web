﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Collections.Generic;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmStatusLists.
	/// </summary>
	partial class frmStatusLists : BaseForm
	{
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCCheckBox chkExcludePaid;
		public fecherFoundation.FCCheckBox chkShowPayments;
		public fecherFoundation.FCCheckBox chkSummaryOnly;
		public fecherFoundation.FCCheckBox chkCurrentInterest;
		public fecherFoundation.FCCheckBox chkUseFullStatus;
		public fecherFoundation.FCGrid vsQuestions;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStatusLists));
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdExit = new fecherFoundation.FCButton();
            this.fraQuestions = new fecherFoundation.FCFrame();
            this.cmbHardCode = new fecherFoundation.FCComboBox();
            this.chkCurrentInterest = new fecherFoundation.FCCheckBox();
            this.vsQuestions = new fecherFoundation.FCGrid();
            this.chkHardCode = new fecherFoundation.FCCheckBox();
            this.cmbNameOption = new Wisej.Web.ComboBox();
            this.chkExcludePaid = new fecherFoundation.FCCheckBox();
            this.chkShowPayments = new fecherFoundation.FCCheckBox();
            this.chkSummaryOnly = new fecherFoundation.FCCheckBox();
            this.chkUseFullStatus = new fecherFoundation.FCCheckBox();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.filterPanel = new Wisej.Web.FlexLayoutPanel();
            this.AccountPanel = new Wisej.Web.TableLayoutPanel();
            this.txtAccountStart = new Wisej.Web.TextBox();
            this.label3 = new fecherFoundation.FCLabel();
            this.txtAccountEnd = new Wisej.Web.TextBox();
            this.namePanel = new Wisej.Web.TableLayoutPanel();
            this.txtNameStart = new Wisej.Web.TextBox();
            this.label1 = new fecherFoundation.FCLabel();
            this.txtNameEnd = new Wisej.Web.TextBox();
            this.taxYearPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbTaxYearEnd = new Wisej.Web.ComboBox();
            this.cmbTaxYearStart = new Wisej.Web.ComboBox();
            this.label4 = new fecherFoundation.FCLabel();
            this.BalanceDuePanel = new Wisej.Web.TableLayoutPanel();
            this.label5 = new fecherFoundation.FCLabel();
            this.cmbBalanceDueOption = new Wisej.Web.ComboBox();
            this.txtBalanceDue = new Wisej.Web.TextBox();
            this.AccountTypePanel = new Wisej.Web.TableLayoutPanel();
            this.cmbPaymentType = new Wisej.Web.ComboBox();
            this.label6 = new fecherFoundation.FCLabel();
            this.DateRangePanel = new Wisej.Web.TableLayoutPanel();
            this.lblAsOfDate = new fecherFoundation.FCLabel();
            this.tableLayoutPanel1 = new Wisej.Web.TableLayoutPanel();
            this.dtpAsOfDate = new fecherFoundation.FCDateTimePicker();
            this.cmdAsOfDateCalendar = new fecherFoundation.FCButton();
            this.PaymentDateRangePanel = new Wisej.Web.TableLayoutPanel();
            this.tableLayoutPanel4 = new Wisej.Web.TableLayoutPanel();
            this.dtpShowPaymentEnd = new fecherFoundation.FCDateTimePicker();
            this.cmdShowPaymentEndCalendar = new fecherFoundation.FCButton();
            this.label2 = new fecherFoundation.FCLabel();
            this.tableLayoutPanel2 = new Wisej.Web.TableLayoutPanel();
            this.dtpShowPaymentStart = new fecherFoundation.FCDateTimePicker();
            this.cmdShowPaymentFromCalendar = new fecherFoundation.FCButton();
            this.SupplementalBillPanel = new Wisej.Web.TableLayoutPanel();
            this.tableLayoutPanel3 = new Wisej.Web.TableLayoutPanel();
            this.cmdSupplementalBillEndShowCalendar = new fecherFoundation.FCButton();
            this.dtpSupplementalBillEnd = new fecherFoundation.FCDateTimePicker();
            this.label11 = new fecherFoundation.FCLabel();
            this.tableLayoutPanel5 = new Wisej.Web.TableLayoutPanel();
            this.cmdSupplementalBillStartShowCalendar = new fecherFoundation.FCButton();
            this.dtpSupplementalBillStart = new fecherFoundation.FCDateTimePicker();
            this.TranCodePanel = new Wisej.Web.TableLayoutPanel();
            this.cmbTranCodeEnd = new Wisej.Web.ComboBox();
            this.cmbTranCodeStart = new Wisej.Web.ComboBox();
            this.label7 = new fecherFoundation.FCLabel();
            this.TaxAcquiredPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbTaxAcquired = new Wisej.Web.ComboBox();
            this.label8 = new fecherFoundation.FCLabel();
            this.RateRecordPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbRateRecordEnd = new Wisej.Web.ComboBox();
            this.cmbRateRecordStart = new Wisej.Web.ComboBox();
            this.label9 = new fecherFoundation.FCLabel();
            this.BillStatusPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbBillStatusType = new Wisej.Web.ComboBox();
            this.label10 = new fecherFoundation.FCLabel();
            this.cboSavedReport = new Wisej.Web.ComboBox();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdClear1 = new fecherFoundation.FCButton();
            this.fraQuestions2 = new fecherFoundation.FCFrame();
            this.fraSave = new fecherFoundation.FCFrame();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.chkREPPHardCoded = new fecherFoundation.FCCheckBox();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
            this.fraQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludePaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseFullStatus)).BeginInit();
            this.filterPanel.SuspendLayout();
            this.AccountPanel.SuspendLayout();
            this.namePanel.SuspendLayout();
            this.taxYearPanel.SuspendLayout();
            this.BalanceDuePanel.SuspendLayout();
            this.AccountTypePanel.SuspendLayout();
            this.DateRangePanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAsOfDateCalendar)).BeginInit();
            this.PaymentDateRangePanel.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowPaymentEndCalendar)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowPaymentFromCalendar)).BeginInit();
            this.SupplementalBillPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSupplementalBillEndShowCalendar)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSupplementalBillStartShowCalendar)).BeginInit();
            this.TranCodePanel.SuspendLayout();
            this.TaxAcquiredPanel.SuspendLayout();
            this.RateRecordPanel.SuspendLayout();
            this.BillStatusPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions2)).BeginInit();
            this.fraQuestions2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).BeginInit();
            this.fraSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkREPPHardCoded)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 769);
            this.BottomPanel.Size = new System.Drawing.Size(988, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraQuestions2);
            this.ClientArea.Controls.Add(this.fraQuestions);
            this.ClientArea.Controls.Add(this.filterPanel);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Location = new System.Drawing.Point(0, 65);
            this.ClientArea.Size = new System.Drawing.Size(1008, 693);
            this.ClientArea.Controls.SetChildIndex(this.cmdExit, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.filterPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraQuestions, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraQuestions2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear1);
            this.TopPanel.Size = new System.Drawing.Size(1008, 65);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear1, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(165, 28);
            this.HeaderText.Text = "Custom Report";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Cursor = Wisej.Web.Cursors.Default;
            this.cmdPrint.Location = new System.Drawing.Point(705, 725);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(96, 26);
            this.cmdPrint.TabIndex = 7;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.Cursor = Wisej.Web.Cursors.Default;
            this.cmdExit.Location = new System.Drawing.Point(823, 725);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(96, 26);
            this.cmdExit.TabIndex = 8;
            this.cmdExit.Text = "Exit";
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // fraQuestions
            // 
            this.fraQuestions.AppearanceKey = "groupBoxNoBorders";
            this.fraQuestions.Controls.Add(this.cmbHardCode);
            this.fraQuestions.Controls.Add(this.chkCurrentInterest);
            this.fraQuestions.Controls.Add(this.vsQuestions);
            this.fraQuestions.Controls.Add(this.chkHardCode);
            this.fraQuestions.Location = new System.Drawing.Point(3, 11);
            this.fraQuestions.Name = "fraQuestions";
            this.fraQuestions.Size = new System.Drawing.Size(605, 42);
            this.fraQuestions.TabIndex = 19;
            this.fraQuestions.UseMnemonic = false;
            // 
            // cmbHardCode
            // 
            this.cmbHardCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbHardCode.Enabled = false;
            this.cmbHardCode.Location = new System.Drawing.Point(238, 0);
            this.cmbHardCode.Name = "cmbHardCode";
            this.cmbHardCode.Size = new System.Drawing.Size(351, 40);
            this.cmbHardCode.TabIndex = 2;
            this.cmbHardCode.SelectedIndexChanged += new System.EventHandler(this.cmbHardCode_SelectedIndexChanged);
            // 
            // chkCurrentInterest
            // 
            this.chkCurrentInterest.Enabled = false;
            this.chkCurrentInterest.Location = new System.Drawing.Point(800, 108);
            this.chkCurrentInterest.Name = "chkCurrentInterest";
            this.chkCurrentInterest.Size = new System.Drawing.Size(158, 22);
            this.chkCurrentInterest.TabIndex = 7;
            this.chkCurrentInterest.Text = "Show Current Interest";
            this.chkCurrentInterest.Visible = false;
            // 
            // vsQuestions
            // 
            this.vsQuestions.ColumnHeadersVisible = false;
            this.vsQuestions.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsQuestions.FixedRows = 0;
            this.vsQuestions.Location = new System.Drawing.Point(800, 21);
            this.vsQuestions.Name = "vsQuestions";
            this.vsQuestions.ReadOnly = false;
            this.vsQuestions.Rows = 5;
            this.vsQuestions.Size = new System.Drawing.Size(185, 57);
            this.vsQuestions.TabIndex = 11;
            this.vsQuestions.Visible = false;
            this.vsQuestions.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsQuestions_ValidateEdit);
            this.vsQuestions.CurrentCellChanged += new System.EventHandler(this.vsQuestions_RowColChange);
            this.vsQuestions.Click += new System.EventHandler(this.vsQuestions_Click);
            this.vsQuestions.DoubleClick += new System.EventHandler(this.vsQuestions_DblClick);
            this.vsQuestions.KeyDown += new Wisej.Web.KeyEventHandler(this.vsQuestions_KeyDown);
            // 
            // chkHardCode
            // 
            this.chkHardCode.Location = new System.Drawing.Point(27, 10);
            this.chkHardCode.Name = "chkHardCode";
            this.chkHardCode.Size = new System.Drawing.Size(153, 22);
            this.chkHardCode.TabIndex = 1;
            this.chkHardCode.Text = "Use a Default Report";
            // 
            // cmbNameOption
            // 
            this.cmbNameOption.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbNameOption.AutoSize = false;
            this.cmbNameOption.DisplayMember = "Description";
            this.cmbNameOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbNameOption.Location = new System.Drawing.Point(6, 129);
            this.cmbNameOption.Name = "cmbNameOption";
            this.cmbNameOption.Size = new System.Drawing.Size(322, 40);
            this.cmbNameOption.TabIndex = 6;
            this.cmbNameOption.ValueMember = "ID";
            // 
            // chkExcludePaid
            // 
            this.chkExcludePaid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.chkExcludePaid.Location = new System.Drawing.Point(0, 90);
            this.chkExcludePaid.Name = "chkExcludePaid";
            this.chkExcludePaid.Size = new System.Drawing.Size(108, 22);
            this.chkExcludePaid.TabIndex = 4;
            this.chkExcludePaid.Text = "Exclude Paid";
            this.ToolTip1.SetToolTip(this.chkExcludePaid, "This will exclude all the bills that have a zero balance.");
            this.chkExcludePaid.CheckedChanged += new System.EventHandler(this.chkExcludePaid_CheckedChanged);
            // 
            // chkShowPayments
            // 
            this.chkShowPayments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.chkShowPayments.Location = new System.Drawing.Point(0, 30);
            this.chkShowPayments.Name = "chkShowPayments";
            this.chkShowPayments.Size = new System.Drawing.Size(126, 22);
            this.chkShowPayments.TabIndex = 2;
            this.chkShowPayments.Text = "Show Payments";
            this.ToolTip1.SetToolTip(this.chkShowPayments, "This will show all of the payments for this account.");
            // 
            // chkSummaryOnly
            // 
            this.chkSummaryOnly.Location = new System.Drawing.Point(0, 60);
            this.chkSummaryOnly.Name = "chkSummaryOnly";
            this.chkSummaryOnly.Size = new System.Drawing.Size(154, 22);
            this.chkSummaryOnly.TabIndex = 3;
            this.chkSummaryOnly.Text = "Show Summary Only";
            // 
            // chkUseFullStatus
            // 
            this.chkUseFullStatus.Name = "chkUseFullStatus";
            this.chkUseFullStatus.Size = new System.Drawing.Size(158, 22);
            this.chkUseFullStatus.TabIndex = 1;
            this.chkUseFullStatus.Text = "Show Current Interest";
            this.chkUseFullStatus.Click += new System.EventHandler(this.chkUseFullStatus_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = -1;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            // 
            // filterPanel
            // 
            this.filterPanel.AutoSize = true;
            this.filterPanel.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
            this.filterPanel.Controls.Add(this.AccountPanel);
            this.filterPanel.Controls.Add(this.namePanel);
            this.filterPanel.Controls.Add(this.taxYearPanel);
            this.filterPanel.Controls.Add(this.BalanceDuePanel);
            this.filterPanel.Controls.Add(this.AccountTypePanel);
            this.filterPanel.Controls.Add(this.DateRangePanel);
            this.filterPanel.Controls.Add(this.PaymentDateRangePanel);
            this.filterPanel.Controls.Add(this.SupplementalBillPanel);
            this.filterPanel.Controls.Add(this.TranCodePanel);
            this.filterPanel.Controls.Add(this.TaxAcquiredPanel);
            this.filterPanel.Controls.Add(this.RateRecordPanel);
            this.filterPanel.Controls.Add(this.BillStatusPanel);
            this.filterPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.filterPanel.Location = new System.Drawing.Point(30, 71);
            this.filterPanel.Name = "filterPanel";
            this.filterPanel.Size = new System.Drawing.Size(583, 698);
            this.filterPanel.TabIndex = 12;
            this.filterPanel.TabStop = true;
            // 
            // AccountPanel
            // 
            this.AccountPanel.AutoSize = true;
            this.AccountPanel.ColumnCount = 3;
            this.AccountPanel.ColumnStyles.Clear();
            this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountPanel.Controls.Add(this.txtAccountStart, 0, 0);
            this.AccountPanel.Controls.Add(this.label3, 0, 0);
            this.AccountPanel.Controls.Add(this.txtAccountEnd, 2, 0);
            this.AccountPanel.Location = new System.Drawing.Point(3, 3);
            this.AccountPanel.Name = "AccountPanel";
            this.AccountPanel.RowCount = 1;
            this.AccountPanel.RowStyles.Clear();
            this.AccountPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountPanel.Size = new System.Drawing.Size(577, 46);
            this.AccountPanel.TabIndex = 3;
            this.AccountPanel.TabStop = true;
            // 
            // txtAccountStart
            // 
            this.txtAccountStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.javaScript1.SetJavaScript(this.txtAccountStart, resources.GetString("txtAccountStart.JavaScript"));
            this.txtAccountStart.Location = new System.Drawing.Point(166, 3);
            this.txtAccountStart.Name = "txtAccountStart";
            this.txtAccountStart.Size = new System.Drawing.Size(201, 22);
            this.txtAccountStart.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 40);
            this.label3.TabIndex = 1;
            this.label3.Text = "ACCOUNT";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAccountEnd
            // 
            this.txtAccountEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.javaScript1.SetJavaScript(this.txtAccountEnd, resources.GetString("txtAccountEnd.JavaScript"));
            this.txtAccountEnd.Location = new System.Drawing.Point(373, 3);
            this.txtAccountEnd.Name = "txtAccountEnd";
            this.txtAccountEnd.Size = new System.Drawing.Size(201, 22);
            this.txtAccountEnd.TabIndex = 6;
            // 
            // namePanel
            // 
            this.namePanel.AutoSize = true;
            this.namePanel.ColumnCount = 3;
            this.namePanel.ColumnStyles.Clear();
            this.namePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.namePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.namePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.namePanel.Controls.Add(this.txtNameStart, 0, 0);
            this.namePanel.Controls.Add(this.label1, 0, 0);
            this.namePanel.Controls.Add(this.txtNameEnd, 2, 0);
            this.namePanel.Location = new System.Drawing.Point(3, 65);
            this.namePanel.Name = "namePanel";
            this.namePanel.RowCount = 1;
            this.namePanel.RowStyles.Clear();
            this.namePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.namePanel.Size = new System.Drawing.Size(577, 46);
            this.namePanel.TabIndex = 4;
            this.namePanel.TabStop = true;
            // 
            // txtNameStart
            // 
            this.txtNameStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtNameStart.Location = new System.Drawing.Point(166, 3);
            this.txtNameStart.Name = "txtNameStart";
            this.txtNameStart.Size = new System.Drawing.Size(201, 22);
            this.txtNameStart.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "NAME";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNameEnd
            // 
            this.txtNameEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtNameEnd.Location = new System.Drawing.Point(373, 3);
            this.txtNameEnd.Name = "txtNameEnd";
            this.txtNameEnd.Size = new System.Drawing.Size(201, 22);
            this.txtNameEnd.TabIndex = 4;
            // 
            // taxYearPanel
            // 
            this.taxYearPanel.AutoSize = true;
            this.taxYearPanel.ColumnCount = 3;
            this.taxYearPanel.ColumnStyles.Clear();
            this.taxYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.taxYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.taxYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.taxYearPanel.Controls.Add(this.cmbTaxYearEnd, 2, 0);
            this.taxYearPanel.Controls.Add(this.cmbTaxYearStart, 1, 0);
            this.taxYearPanel.Controls.Add(this.label4, 0, 0);
            this.taxYearPanel.Location = new System.Drawing.Point(3, 127);
            this.taxYearPanel.Name = "taxYearPanel";
            this.taxYearPanel.RowCount = 1;
            this.taxYearPanel.RowStyles.Clear();
            this.taxYearPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.taxYearPanel.Size = new System.Drawing.Size(577, 46);
            this.taxYearPanel.TabIndex = 5;
            this.taxYearPanel.TabStop = true;
            // 
            // cmbTaxYearEnd
            // 
            this.cmbTaxYearEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbTaxYearEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTaxYearEnd.Location = new System.Drawing.Point(373, 3);
            this.cmbTaxYearEnd.Name = "cmbTaxYearEnd";
            this.cmbTaxYearEnd.Size = new System.Drawing.Size(201, 22);
            this.cmbTaxYearEnd.TabIndex = 5;
            // 
            // cmbTaxYearStart
            // 
            this.cmbTaxYearStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbTaxYearStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTaxYearStart.Location = new System.Drawing.Point(166, 3);
            this.cmbTaxYearStart.Name = "cmbTaxYearStart";
            this.cmbTaxYearStart.Size = new System.Drawing.Size(201, 22);
            this.cmbTaxYearStart.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 40);
            this.label4.TabIndex = 1;
            this.label4.Text = "TAX YEAR";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BalanceDuePanel
            // 
            this.BalanceDuePanel.AutoSize = true;
            this.BalanceDuePanel.ColumnCount = 3;
            this.BalanceDuePanel.ColumnStyles.Clear();
            this.BalanceDuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.BalanceDuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.BalanceDuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.BalanceDuePanel.Controls.Add(this.label5, 0, 0);
            this.BalanceDuePanel.Controls.Add(this.cmbBalanceDueOption, 1, 0);
            this.BalanceDuePanel.Controls.Add(this.txtBalanceDue, 2, 0);
            this.BalanceDuePanel.Location = new System.Drawing.Point(3, 189);
            this.BalanceDuePanel.Name = "BalanceDuePanel";
            this.BalanceDuePanel.RowCount = 1;
            this.BalanceDuePanel.RowStyles.Clear();
            this.BalanceDuePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.BalanceDuePanel.Size = new System.Drawing.Size(577, 46);
            this.BalanceDuePanel.TabIndex = 6;
            this.BalanceDuePanel.TabStop = true;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 40);
            this.label5.TabIndex = 1;
            this.label5.Text = "BALANCE  DUE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbBalanceDueOption
            // 
            this.cmbBalanceDueOption.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbBalanceDueOption.DisplayMember = "Description";
            this.cmbBalanceDueOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbBalanceDueOption.Location = new System.Drawing.Point(166, 3);
            this.cmbBalanceDueOption.Name = "cmbBalanceDueOption";
            this.cmbBalanceDueOption.Size = new System.Drawing.Size(201, 22);
            this.cmbBalanceDueOption.TabIndex = 6;
            this.cmbBalanceDueOption.ValueMember = "ID";
            // 
            // txtBalanceDue
            // 
            this.txtBalanceDue.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.javaScript1.SetJavaScript(this.txtBalanceDue, resources.GetString("txtBalanceDue.JavaScript"));
            this.txtBalanceDue.Location = new System.Drawing.Point(373, 3);
            this.txtBalanceDue.Name = "txtBalanceDue";
            this.txtBalanceDue.Size = new System.Drawing.Size(201, 22);
            this.txtBalanceDue.TabIndex = 7;
            // 
            // AccountTypePanel
            // 
            this.AccountTypePanel.AutoSize = true;
            this.AccountTypePanel.ColumnCount = 3;
            this.AccountTypePanel.ColumnStyles.Clear();
            this.AccountTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.AccountTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountTypePanel.Controls.Add(this.cmbPaymentType, 0, 0);
            this.AccountTypePanel.Controls.Add(this.label6, 0, 0);
            this.AccountTypePanel.Location = new System.Drawing.Point(3, 251);
            this.AccountTypePanel.Name = "AccountTypePanel";
            this.AccountTypePanel.RowCount = 1;
            this.AccountTypePanel.RowStyles.Clear();
            this.AccountTypePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountTypePanel.Size = new System.Drawing.Size(577, 46);
            this.AccountTypePanel.TabIndex = 7;
            this.AccountTypePanel.TabStop = true;
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbPaymentType.DisplayMember = "Description";
            this.cmbPaymentType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPaymentType.Location = new System.Drawing.Point(166, 3);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(201, 22);
            this.cmbPaymentType.TabIndex = 5;
            this.cmbPaymentType.ValueMember = "ID";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 40);
            this.label6.TabIndex = 1;
            this.label6.Text = "ACCOUNTS WITH TYPE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateRangePanel
            // 
            this.DateRangePanel.AutoSize = true;
            this.DateRangePanel.ColumnCount = 3;
            this.DateRangePanel.ColumnStyles.Clear();
            this.DateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.DateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.DateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.DateRangePanel.Controls.Add(this.lblAsOfDate, 0, 0);
            this.DateRangePanel.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.DateRangePanel.Location = new System.Drawing.Point(3, 313);
            this.DateRangePanel.Name = "DateRangePanel";
            this.DateRangePanel.RowCount = 1;
            this.DateRangePanel.RowStyles.Clear();
            this.DateRangePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.DateRangePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 20F));
            this.DateRangePanel.Size = new System.Drawing.Size(577, 46);
            this.DateRangePanel.TabIndex = 8;
            this.DateRangePanel.TabStop = true;
            // 
            // lblAsOfDate
            // 
            this.lblAsOfDate.Location = new System.Drawing.Point(3, 3);
            this.lblAsOfDate.Name = "lblAsOfDate";
            this.lblAsOfDate.Size = new System.Drawing.Size(81, 40);
            this.lblAsOfDate.TabIndex = 1;
            this.lblAsOfDate.Text = "AS OF DATE";
            this.lblAsOfDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Clear();
            this.tableLayoutPanel1.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Controls.Add(this.dtpAsOfDate);
            this.tableLayoutPanel1.Controls.Add(this.cmdAsOfDateCalendar, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(166, 0);
            this.tableLayoutPanel1.Margin = new Wisej.Web.Padding(3, 0, 3, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Clear();
            this.tableLayoutPanel1.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(201, 42);
            this.tableLayoutPanel1.TabIndex = 23;
            this.tableLayoutPanel1.TabStop = true;
            // 
            // dtpAsOfDate
            // 
            this.dtpAsOfDate.AutoSize = false;
            this.dtpAsOfDate.CustomFormat = "MM/dd/yyyy";
            this.dtpAsOfDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpAsOfDate.Location = new System.Drawing.Point(3, 3);
            this.dtpAsOfDate.Mask = "00/00/0000";
            this.dtpAsOfDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpAsOfDate.Name = "dtpAsOfDate";
            this.dtpAsOfDate.ShowCalendar = false;
            this.dtpAsOfDate.Size = new System.Drawing.Size(145, 36);
            this.dtpAsOfDate.TabIndex = 2;
            this.dtpAsOfDate.NullableValue = null;
            // 
            // cmdAsOfDateCalendar
            // 
            this.cmdAsOfDateCalendar.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdAsOfDateCalendar.AppearanceKey = "imageButton";
            this.cmdAsOfDateCalendar.ImageSource = "icon - calendar?color=#707884";
            this.cmdAsOfDateCalendar.Location = new System.Drawing.Point(154, 3);
            this.cmdAsOfDateCalendar.Name = "cmdAsOfDateCalendar";
            this.cmdAsOfDateCalendar.Size = new System.Drawing.Size(44, 36);
            this.cmdAsOfDateCalendar.TabIndex = 5;
            this.cmdAsOfDateCalendar.Click += new System.EventHandler(this.cmdAsOfDateCalendar_Click);
            // 
            // PaymentDateRangePanel
            // 
            this.PaymentDateRangePanel.AutoSize = true;
            this.PaymentDateRangePanel.ColumnCount = 3;
            this.PaymentDateRangePanel.ColumnStyles.Clear();
            this.PaymentDateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.PaymentDateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.PaymentDateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.PaymentDateRangePanel.Controls.Add(this.tableLayoutPanel4, 2, 0);
            this.PaymentDateRangePanel.Controls.Add(this.label2, 0, 0);
            this.PaymentDateRangePanel.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.PaymentDateRangePanel.Location = new System.Drawing.Point(3, 375);
            this.PaymentDateRangePanel.Name = "PaymentDateRangePanel";
            this.PaymentDateRangePanel.RowCount = 1;
            this.PaymentDateRangePanel.RowStyles.Clear();
            this.PaymentDateRangePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.PaymentDateRangePanel.Size = new System.Drawing.Size(577, 46);
            this.PaymentDateRangePanel.TabIndex = 9;
            this.PaymentDateRangePanel.TabStop = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Clear();
            this.tableLayoutPanel4.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.Controls.Add(this.dtpShowPaymentEnd, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdShowPaymentEndCalendar, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(373, 0);
            this.tableLayoutPanel4.Margin = new Wisej.Web.Padding(3, 0, 3, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Clear();
            this.tableLayoutPanel4.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(201, 42);
            this.tableLayoutPanel4.TabIndex = 24;
            this.tableLayoutPanel4.TabStop = true;
            // 
            // dtpShowPaymentEnd
            // 
            this.dtpShowPaymentEnd.AutoSize = false;
            this.dtpShowPaymentEnd.CustomFormat = "MM/dd/yyyy";
            this.dtpShowPaymentEnd.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpShowPaymentEnd.Location = new System.Drawing.Point(3, 3);
            this.dtpShowPaymentEnd.Mask = "00/00/0000";
            this.dtpShowPaymentEnd.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpShowPaymentEnd.Name = "dtpShowPaymentEnd";
            this.dtpShowPaymentEnd.ShowCalendar = false;
            this.dtpShowPaymentEnd.Size = new System.Drawing.Size(145, 36);
            this.dtpShowPaymentEnd.TabIndex = 4;
            this.dtpShowPaymentEnd.NullableValue = null;
            // 
            // cmdShowPaymentEndCalendar
            // 
            this.cmdShowPaymentEndCalendar.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdShowPaymentEndCalendar.AppearanceKey = "imageButton";
            this.cmdShowPaymentEndCalendar.ImageSource = "icon - calendar?color=#707884";
            this.cmdShowPaymentEndCalendar.Location = new System.Drawing.Point(154, 3);
            this.cmdShowPaymentEndCalendar.Name = "cmdShowPaymentEndCalendar";
            this.cmdShowPaymentEndCalendar.Size = new System.Drawing.Size(44, 36);
            this.cmdShowPaymentEndCalendar.TabIndex = 7;
            this.cmdShowPaymentEndCalendar.Click += new System.EventHandler(this.fcButton1_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 40);
            this.label2.TabIndex = 1;
            this.label2.Text = "SHOW PAYMENT FROM";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Clear();
            this.tableLayoutPanel2.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.Controls.Add(this.dtpShowPaymentStart, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmdShowPaymentFromCalendar, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(166, 0);
            this.tableLayoutPanel2.Margin = new Wisej.Web.Padding(3, 0, 3, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Clear();
            this.tableLayoutPanel2.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(201, 42);
            this.tableLayoutPanel2.TabIndex = 23;
            this.tableLayoutPanel2.TabStop = true;
            // 
            // dtpShowPaymentStart
            // 
            this.dtpShowPaymentStart.AutoSize = false;
            this.dtpShowPaymentStart.CustomFormat = "MM/dd/yyyy";
            this.dtpShowPaymentStart.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpShowPaymentStart.Location = new System.Drawing.Point(3, 3);
            this.dtpShowPaymentStart.Mask = "00/00/0000";
            this.dtpShowPaymentStart.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpShowPaymentStart.Name = "dtpShowPaymentStart";
            this.dtpShowPaymentStart.ShowCalendar = false;
            this.dtpShowPaymentStart.Size = new System.Drawing.Size(145, 36);
            this.dtpShowPaymentStart.TabIndex = 3;
            this.dtpShowPaymentStart.NullableValue = null;
            // 
            // cmdShowPaymentFromCalendar
            // 
            this.cmdShowPaymentFromCalendar.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdShowPaymentFromCalendar.AppearanceKey = "imageButton";
            this.cmdShowPaymentFromCalendar.ImageSource = "icon - calendar?color=#707884";
            this.cmdShowPaymentFromCalendar.Location = new System.Drawing.Point(154, 3);
            this.cmdShowPaymentFromCalendar.Name = "cmdShowPaymentFromCalendar";
            this.cmdShowPaymentFromCalendar.Size = new System.Drawing.Size(44, 36);
            this.cmdShowPaymentFromCalendar.TabIndex = 6;
            this.cmdShowPaymentFromCalendar.Click += new System.EventHandler(this.cmdShowPaymentFromCalendar_Click);
            // 
            // SupplementalBillPanel
            // 
            this.SupplementalBillPanel.AutoSize = true;
            this.SupplementalBillPanel.ColumnCount = 3;
            this.SupplementalBillPanel.ColumnStyles.Clear();
            this.SupplementalBillPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.SupplementalBillPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.SupplementalBillPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.SupplementalBillPanel.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.SupplementalBillPanel.Controls.Add(this.label11, 0, 0);
            this.SupplementalBillPanel.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.SupplementalBillPanel.Location = new System.Drawing.Point(3, 437);
            this.SupplementalBillPanel.Name = "SupplementalBillPanel";
            this.SupplementalBillPanel.RowCount = 1;
            this.SupplementalBillPanel.RowStyles.Clear();
            this.SupplementalBillPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.SupplementalBillPanel.Size = new System.Drawing.Size(577, 46);
            this.SupplementalBillPanel.TabIndex = 10;
            this.SupplementalBillPanel.TabStop = true;
            this.SupplementalBillPanel.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Clear();
            this.tableLayoutPanel3.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.Controls.Add(this.cmdSupplementalBillEndShowCalendar, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.dtpSupplementalBillEnd);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(373, 0);
            this.tableLayoutPanel3.Margin = new Wisej.Web.Padding(3, 0, 3, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Clear();
            this.tableLayoutPanel3.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(201, 42);
            this.tableLayoutPanel3.TabIndex = 24;
            this.tableLayoutPanel3.TabStop = true;
            // 
            // cmdSupplementalBillEndShowCalendar
            // 
            this.cmdSupplementalBillEndShowCalendar.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdSupplementalBillEndShowCalendar.AppearanceKey = "imageButton";
            this.cmdSupplementalBillEndShowCalendar.ImageSource = "icon - calendar?color=#707884";
            this.cmdSupplementalBillEndShowCalendar.Location = new System.Drawing.Point(154, 3);
            this.cmdSupplementalBillEndShowCalendar.Name = "cmdSupplementalBillEndShowCalendar";
            this.cmdSupplementalBillEndShowCalendar.Size = new System.Drawing.Size(44, 36);
            this.cmdSupplementalBillEndShowCalendar.TabIndex = 7;
            this.cmdSupplementalBillEndShowCalendar.Click += new System.EventHandler(this.cmdSupplementalBillEndShowCalendar_Click);
            // 
            // dtpSupplementalBillEnd
            // 
            this.dtpSupplementalBillEnd.AutoSize = false;
            this.dtpSupplementalBillEnd.CustomFormat = "MM/dd/yyyy";
            this.dtpSupplementalBillEnd.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpSupplementalBillEnd.Location = new System.Drawing.Point(3, 3);
            this.dtpSupplementalBillEnd.Mask = "00/00/0000";
            this.dtpSupplementalBillEnd.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpSupplementalBillEnd.Name = "dtpSupplementalBillEnd";
            this.dtpSupplementalBillEnd.ShowCalendar = false;
            this.dtpSupplementalBillEnd.Size = new System.Drawing.Size(145, 36);
            this.dtpSupplementalBillEnd.TabIndex = 4;
            this.dtpSupplementalBillEnd.NullableValue = null;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 40);
            this.label11.TabIndex = 1;
            this.label11.Text = "SUPPLEMENTAL BILL DATE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Clear();
            this.tableLayoutPanel5.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.tableLayoutPanel5.Controls.Add(this.cmdSupplementalBillStartShowCalendar, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.dtpSupplementalBillStart);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(166, 0);
            this.tableLayoutPanel5.Margin = new Wisej.Web.Padding(3, 0, 3, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Clear();
            this.tableLayoutPanel5.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(201, 42);
            this.tableLayoutPanel5.TabIndex = 23;
            this.tableLayoutPanel5.TabStop = true;
            // 
            // cmdSupplementalBillStartShowCalendar
            // 
            this.cmdSupplementalBillStartShowCalendar.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdSupplementalBillStartShowCalendar.AppearanceKey = "imageButton";
            this.cmdSupplementalBillStartShowCalendar.ImageSource = "icon - calendar?color=#707884";
            this.cmdSupplementalBillStartShowCalendar.Location = new System.Drawing.Point(154, 3);
            this.cmdSupplementalBillStartShowCalendar.Name = "cmdSupplementalBillStartShowCalendar";
            this.cmdSupplementalBillStartShowCalendar.Size = new System.Drawing.Size(44, 36);
            this.cmdSupplementalBillStartShowCalendar.TabIndex = 7;
            this.cmdSupplementalBillStartShowCalendar.Click += new System.EventHandler(this.cmdSupplementalBillStartShowCalendar_Click);
            // 
            // dtpSupplementalBillStart
            // 
            this.dtpSupplementalBillStart.AutoSize = false;
            this.dtpSupplementalBillStart.CustomFormat = "MM/dd/yyyy";
            this.dtpSupplementalBillStart.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpSupplementalBillStart.Location = new System.Drawing.Point(3, 3);
            this.dtpSupplementalBillStart.Mask = "00/00/0000";
            this.dtpSupplementalBillStart.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpSupplementalBillStart.Name = "dtpSupplementalBillStart";
            this.dtpSupplementalBillStart.ShowCalendar = false;
            this.dtpSupplementalBillStart.Size = new System.Drawing.Size(145, 36);
            this.dtpSupplementalBillStart.TabIndex = 3;
            this.dtpSupplementalBillStart.NullableValue = null;
            this.dtpSupplementalBillStart.ValueChanged += new System.EventHandler(this.dtpSupplementalBillStart_ValueChanged);
            // 
            // TranCodePanel
            // 
            this.TranCodePanel.AutoSize = true;
            this.TranCodePanel.ColumnCount = 3;
            this.TranCodePanel.ColumnStyles.Clear();
            this.TranCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.TranCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.TranCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.TranCodePanel.Controls.Add(this.cmbTranCodeEnd, 0, 0);
            this.TranCodePanel.Controls.Add(this.cmbTranCodeStart, 0, 0);
            this.TranCodePanel.Controls.Add(this.label7, 0, 0);
            this.TranCodePanel.Location = new System.Drawing.Point(3, 499);
            this.TranCodePanel.Name = "TranCodePanel";
            this.TranCodePanel.RowCount = 1;
            this.TranCodePanel.RowStyles.Clear();
            this.TranCodePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.TranCodePanel.Size = new System.Drawing.Size(577, 46);
            this.TranCodePanel.TabIndex = 11;
            this.TranCodePanel.TabStop = true;
            // 
            // cmbTranCodeEnd
            // 
            this.cmbTranCodeEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbTranCodeEnd.DisplayMember = "Description";
            this.cmbTranCodeEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTranCodeEnd.Location = new System.Drawing.Point(373, 3);
            this.cmbTranCodeEnd.Name = "cmbTranCodeEnd";
            this.cmbTranCodeEnd.Size = new System.Drawing.Size(201, 22);
            this.cmbTranCodeEnd.TabIndex = 6;
            // 
            // cmbTranCodeStart
            // 
            this.cmbTranCodeStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbTranCodeStart.DisplayMember = "Description";
            this.cmbTranCodeStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTranCodeStart.Location = new System.Drawing.Point(166, 3);
            this.cmbTranCodeStart.Name = "cmbTranCodeStart";
            this.cmbTranCodeStart.Size = new System.Drawing.Size(201, 22);
            this.cmbTranCodeStart.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 40);
            this.label7.TabIndex = 1;
            this.label7.Text = "TRAN CODE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TaxAcquiredPanel
            // 
            this.TaxAcquiredPanel.AutoSize = true;
            this.TaxAcquiredPanel.ColumnCount = 3;
            this.TaxAcquiredPanel.ColumnStyles.Clear();
            this.TaxAcquiredPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.TaxAcquiredPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.TaxAcquiredPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.TaxAcquiredPanel.Controls.Add(this.cmbTaxAcquired, 0, 0);
            this.TaxAcquiredPanel.Controls.Add(this.label8, 0, 0);
            this.TaxAcquiredPanel.Location = new System.Drawing.Point(3, 561);
            this.TaxAcquiredPanel.Name = "TaxAcquiredPanel";
            this.TaxAcquiredPanel.RowCount = 1;
            this.TaxAcquiredPanel.RowStyles.Clear();
            this.TaxAcquiredPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.TaxAcquiredPanel.Size = new System.Drawing.Size(577, 46);
            this.TaxAcquiredPanel.TabIndex = 12;
            this.TaxAcquiredPanel.TabStop = true;
            // 
            // cmbTaxAcquired
            // 
            this.cmbTaxAcquired.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbTaxAcquired.DisplayMember = "Description";
            this.cmbTaxAcquired.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTaxAcquired.Location = new System.Drawing.Point(166, 3);
            this.cmbTaxAcquired.Name = "cmbTaxAcquired";
            this.cmbTaxAcquired.Size = new System.Drawing.Size(201, 22);
            this.cmbTaxAcquired.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 40);
            this.label8.TabIndex = 1;
            this.label8.Text = "TAX ACQUIRED";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RateRecordPanel
            // 
            this.RateRecordPanel.AutoSize = true;
            this.RateRecordPanel.ColumnCount = 3;
            this.RateRecordPanel.ColumnStyles.Clear();
            this.RateRecordPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.RateRecordPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.RateRecordPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.RateRecordPanel.Controls.Add(this.cmbRateRecordEnd, 0, 0);
            this.RateRecordPanel.Controls.Add(this.cmbRateRecordStart, 0, 0);
            this.RateRecordPanel.Controls.Add(this.label9, 0, 0);
            this.RateRecordPanel.Location = new System.Drawing.Point(3, 623);
            this.RateRecordPanel.Name = "RateRecordPanel";
            this.RateRecordPanel.RowCount = 1;
            this.RateRecordPanel.RowStyles.Clear();
            this.RateRecordPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.RateRecordPanel.Size = new System.Drawing.Size(577, 28);
            this.RateRecordPanel.TabIndex = 13;
            this.RateRecordPanel.TabStop = true;
            // 
            // cmbRateRecordEnd
            // 
            this.cmbRateRecordEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbRateRecordEnd.DisplayMember = "Description";
            this.cmbRateRecordEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRateRecordEnd.Location = new System.Drawing.Point(373, 3);
            this.cmbRateRecordEnd.Name = "cmbRateRecordEnd";
            this.cmbRateRecordEnd.Size = new System.Drawing.Size(201, 22);
            this.cmbRateRecordEnd.TabIndex = 7;
            // 
            // cmbRateRecordStart
            // 
            this.cmbRateRecordStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbRateRecordStart.DisplayMember = "Description";
            this.cmbRateRecordStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRateRecordStart.Location = new System.Drawing.Point(166, 3);
            this.cmbRateRecordStart.Name = "cmbRateRecordStart";
            this.cmbRateRecordStart.Size = new System.Drawing.Size(201, 22);
            this.cmbRateRecordStart.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(3, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 22);
            this.label9.TabIndex = 1;
            this.label9.Text = "RATE RECORD";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BillStatusPanel
            // 
            this.BillStatusPanel.AutoSize = true;
            this.BillStatusPanel.ColumnCount = 3;
            this.BillStatusPanel.ColumnStyles.Clear();
            this.BillStatusPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
            this.BillStatusPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.BillStatusPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.BillStatusPanel.Controls.Add(this.cmbBillStatusType, 0, 0);
            this.BillStatusPanel.Controls.Add(this.label10, 0, 0);
            this.BillStatusPanel.Location = new System.Drawing.Point(3, 667);
            this.BillStatusPanel.Name = "BillStatusPanel";
            this.BillStatusPanel.RowCount = 1;
            this.BillStatusPanel.RowStyles.Clear();
            this.BillStatusPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.BillStatusPanel.Size = new System.Drawing.Size(577, 28);
            this.BillStatusPanel.TabIndex = 14;
            this.BillStatusPanel.TabStop = true;
            // 
            // cmbBillStatusType
            // 
            this.cmbBillStatusType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbBillStatusType.DisplayMember = "Description";
            this.cmbBillStatusType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbBillStatusType.Location = new System.Drawing.Point(166, 3);
            this.cmbBillStatusType.Name = "cmbBillStatusType";
            this.cmbBillStatusType.Size = new System.Drawing.Size(201, 22);
            this.cmbBillStatusType.TabIndex = 6;
            this.cmbBillStatusType.ValueMember = "ID";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "BILL STATUS";
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.AutoSize = false;
            this.cboSavedReport.DisplayMember = "Description";
            this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(311, 40);
            this.cboSavedReport.TabIndex = 2;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Cursor = Wisej.Web.Cursors.Default;
            this.btnProcess.Location = new System.Drawing.Point(444, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdClear1
            // 
            this.cmdClear1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear1.Cursor = Wisej.Web.Cursors.Default;
            this.cmdClear1.Location = new System.Drawing.Point(833, 29);
            this.cmdClear1.Name = "cmdClear1";
            this.cmdClear1.Size = new System.Drawing.Size(152, 24);
            this.cmdClear1.TabIndex = 52;
            this.cmdClear1.Text = "Clear Search Criteria";
            this.cmdClear1.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // fraQuestions2
            // 
            this.fraQuestions2.AppearanceKey = "groupBoxNoBorders";
            this.fraQuestions2.Controls.Add(this.cmbNameOption);
            this.fraQuestions2.Controls.Add(this.chkSummaryOnly);
            this.fraQuestions2.Controls.Add(this.chkExcludePaid);
            this.fraQuestions2.Controls.Add(this.fraSave);
            this.fraQuestions2.Controls.Add(this.chkShowPayments);
            this.fraQuestions2.Controls.Add(this.fcLabel3);
            this.fraQuestions2.Controls.Add(this.lstFields);
            this.fraQuestions2.Controls.Add(this.fcLabel2);
            this.fraQuestions2.Controls.Add(this.chkUseFullStatus);
            this.fraQuestions2.Controls.Add(this.lstSort);
            this.fraQuestions2.Controls.Add(this.chkREPPHardCoded);
            this.fraQuestions2.Location = new System.Drawing.Point(620, 11);
            this.fraQuestions2.Name = "fraQuestions2";
            this.fraQuestions2.Size = new System.Drawing.Size(365, 736);
            this.fraQuestions2.TabIndex = 18;
            // 
            // fraSave
            // 
            this.fraSave.Controls.Add(this.cboSavedReport);
            this.fraSave.Controls.Add(this.cmdAdd);
            this.fraSave.Controls.Add(this.cmbReport);
            this.fraSave.Location = new System.Drawing.Point(6, 511);
            this.fraSave.Name = "fraSave";
            this.fraSave.Size = new System.Drawing.Size(351, 176);
            this.fraSave.TabIndex = 9;
            this.fraSave.Text = "Report";
            this.fraSave.UseMnemonic = false;
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Cursor = Wisej.Web.Cursors.Default;
            this.cmdAdd.Location = new System.Drawing.Point(20, 130);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(311, 40);
            this.cmdAdd.TabIndex = 3;
            this.cmdAdd.Text = "Add Custom report to Library";
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Edit or Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(311, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.Text = "Edit or Create New Report";
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(6, 341);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(132, 15);
            this.fcLabel3.TabIndex = 22;
            this.fcLabel3.Text = "FIELDS TO SORT BY";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lstFields
            // 
            this.lstFields.AllowDrag = false;
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.CheckBoxes = true;
            this.lstFields.Location = new System.Drawing.Point(6, 199);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(351, 130);
            this.lstFields.Style = 1;
            this.lstFields.TabIndex = 7;
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(6, 177);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(211, 15);
            this.fcLabel2.TabIndex = 20;
            this.fcLabel2.Text = "FIELDS TO DISPLAY ON REPORT";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(6, 362);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(351, 130);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 8;
            // 
            // chkREPPHardCoded
            // 
            this.chkREPPHardCoded.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.chkREPPHardCoded.Enabled = false;
            this.chkREPPHardCoded.Location = new System.Drawing.Point(192, 0);
            this.chkREPPHardCoded.Name = "chkREPPHardCoded";
            this.chkREPPHardCoded.Size = new System.Drawing.Size(154, 22);
            this.chkREPPHardCoded.TabIndex = 5;
            this.chkREPPHardCoded.Text = "Show PP Information";
            // 
            // frmStatusLists
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1008, 758);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmStatusLists";
            this.Text = "Custom Report";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmStatusLists_Load);
            this.Activated += new System.EventHandler(this.frmStatusLists_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmStatusLists_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
            this.fraQuestions.ResumeLayout(false);
            this.fraQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludePaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseFullStatus)).EndInit();
            this.filterPanel.ResumeLayout(false);
            this.filterPanel.PerformLayout();
            this.AccountPanel.ResumeLayout(false);
            this.AccountPanel.PerformLayout();
            this.namePanel.ResumeLayout(false);
            this.namePanel.PerformLayout();
            this.taxYearPanel.ResumeLayout(false);
            this.taxYearPanel.PerformLayout();
            this.BalanceDuePanel.ResumeLayout(false);
            this.BalanceDuePanel.PerformLayout();
            this.AccountTypePanel.ResumeLayout(false);
            this.AccountTypePanel.PerformLayout();
            this.DateRangePanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAsOfDateCalendar)).EndInit();
            this.PaymentDateRangePanel.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowPaymentEndCalendar)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowPaymentFromCalendar)).EndInit();
            this.SupplementalBillPanel.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSupplementalBillEndShowCalendar)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSupplementalBillStartShowCalendar)).EndInit();
            this.TranCodePanel.ResumeLayout(false);
            this.TranCodePanel.PerformLayout();
            this.TaxAcquiredPanel.ResumeLayout(false);
            this.TaxAcquiredPanel.PerformLayout();
            this.RateRecordPanel.ResumeLayout(false);
            this.RateRecordPanel.PerformLayout();
            this.BillStatusPanel.ResumeLayout(false);
            this.BillStatusPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions2)).EndInit();
            this.fraQuestions2.ResumeLayout(false);
            this.fraQuestions2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).EndInit();
            this.fraSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkREPPHardCoded)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdClear1;
        private FlexLayoutPanel filterPanel;
        private TableLayoutPanel AccountPanel;
        private TextBox txtAccountStart;
        private FCLabel label3;
        private TextBox txtAccountEnd;
        private TableLayoutPanel namePanel;
        private TextBox txtNameStart;
        private FCLabel label1;
        private TextBox txtNameEnd;
        private TableLayoutPanel taxYearPanel;
        private ComboBox cmbTaxYearEnd;
        private ComboBox cmbTaxYearStart;
        private FCLabel label4;
        private TableLayoutPanel BalanceDuePanel;
        private FCLabel label5;
        private TableLayoutPanel AccountTypePanel;
        private ComboBox cmbPaymentType;
        private FCLabel label6;
        private TableLayoutPanel DateRangePanel;
        private FCDateTimePicker dtpAsOfDate;
        private FCLabel lblAsOfDate;
        private TableLayoutPanel PaymentDateRangePanel;
        private FCDateTimePicker dtpShowPaymentEnd;
        
        private FCDateTimePicker dtpShowPaymentStart;
        private FCLabel label2;
        private TableLayoutPanel TranCodePanel;
        private ComboBox cmbTranCodeEnd;
        private ComboBox cmbTranCodeStart;
        private FCLabel label7;
        private TableLayoutPanel TaxAcquiredPanel;
        private ComboBox cmbTaxAcquired;
        private FCLabel label8;
        private TableLayoutPanel RateRecordPanel;
        private FCLabel label9;
        private TableLayoutPanel BillStatusPanel;
        private ComboBox cmbBillStatusType;
        private FCLabel label10;
        private TableLayoutPanel SupplementalBillPanel;
        private FCDateTimePicker dtpSupplementalBillEnd;
        private FCDateTimePicker dtpSupplementalBillStart;
        private FCLabel label11;
        private FCFrame fraQuestions2;
        public FCComboBox cmbHardCode;
        public FCFrame fraSave;
        public FCButton cmdAdd;
        public FCComboBox cmbReport;
        private FCLabel fcLabel3;
        public FCDraggableListBox lstFields;
        private FCLabel fcLabel2;
        public FCDraggableListBox lstSort;
        public FCCheckBox chkREPPHardCoded;
        public FCCheckBox chkHardCode;
        private JavaScript javaScript1;
        private ComboBox cmbRateRecordEnd;
        private ComboBox cmbRateRecordStart;
        private ComboBox cmbNameOption;
        private ComboBox cmbBalanceDueOption;
        private TextBox txtBalanceDue;
        private ComboBox cboSavedReport;
        private TableLayoutPanel tableLayoutPanel1;
        public FCButton cmdAsOfDateCalendar;
        private TableLayoutPanel tableLayoutPanel2;
        public FCButton cmdShowPaymentFromCalendar;
        private TableLayoutPanel tableLayoutPanel4;
        public FCButton cmdShowPaymentEndCalendar;
        private TableLayoutPanel tableLayoutPanel3;
        public FCButton cmdSupplementalBillEndShowCalendar;
        private TableLayoutPanel tableLayoutPanel5;
        public FCButton cmdSupplementalBillStartShowCalendar;
    }
}
