﻿using SharedApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000
{
    public interface ISavedStatusReportsRepository : IRepository<SavedStatusReport>
    {
        void Remove(int id);
        IEnumerable<DescriptionIDPair> GetDescriptions(string reportType);
    }
}
