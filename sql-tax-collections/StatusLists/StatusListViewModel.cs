﻿using Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
//using Microsoft.Ajax.Utilities;
using SharedDataAccess;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty.Extensions;
using SharedApplication.PersonalProperty.Models;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess.TaxCollections;
using TWCL0000.DataAccess;
using PredicateBuilder = SharedApplication.PredicateBuilder;

namespace TWCL0000
{
   
    public class StatusListViewModel
    {
        public event EventHandler FilterChanged;
        public event EventHandler SortOptionsChanged;
        public event EventHandler StatusReportsChanged;
        public event EventHandler AvailableFieldsChanged;
        public event EventHandler SelectedOptionsChanged;
        public IEnumerable<DescriptionIDPair> NameOptions { get; set; } = new List<DescriptionIDPair>();
        public IEnumerable<string> TaxYears { get; private set; } = new List<string>();
        public IEnumerable<DescriptionIDPair> TransactionTypes { get; private set; } = new List<DescriptionIDPair>();
        public IEnumerable<DescriptionIDPair> StatusReportDescriptions { get; private set; } = new List<DescriptionIDPair>();
        public IEnumerable<SelectableItem<OrderedDescriptionIDPair>> SortOptions {get; protected set;} = new List<SelectableItem<OrderedDescriptionIDPair>>();
        public IEnumerable<SelectableItem<DescriptionIDPair>> AvailableFields { get; private set; } = new List<SelectableItem<DescriptionIDPair>>();
        public IEnumerable<DescriptionIDPair> DefaultReportTypes { get; private set; } = new List<DescriptionIDPair>();
        public string ReportType { get; private set; } = "";
        public IEnumerable<DescriptionIDPair> BillStatusTypes { get; private set; } = new List<DescriptionIDPair>();
        public IEnumerable<DescriptionIDPair> TranCodes { get; protected set; } = new List<DescriptionIDPair>();
        public IEnumerable<DescriptionIDPair> TaxAcquiredOptions { get; private set; } = new List<DescriptionIDPair>();
        public IEnumerable<DescriptionIDPair> RateRecords { get; private set; } = new List<DescriptionIDPair>();
        public IEnumerable<DescriptionIDPair> ComparisonOptions { get; private set; } = new List<DescriptionIDPair>();
        public TaxCollectionReportOption ReportOption
        {
            get { return reportOption; }
        }
        public bool UseHardCodedReport { get;  set; }

        public TaxBillingHardCodedReport DefaultReportType { get; set; } =
            TaxBillingHardCodedReport.NonZeroBalanceOnAllAccounts;

        private ITrioContextFactory contextFactory;
        private IPropertyTaxStatusQueryHandler statusQueryHandler;
        private IPropertyTaxBillCalculator taxBillCalculator;
        private CommandDispatcher commandDispatcher;
        private IDateTimeService dateTimeService;
        public StatusListViewModel(PropertyTaxBillType billType, ISavedStatusReportsRepository reportsRepository, IBillingMasterRepository billsRepository,ITrioContextFactory trioContextFactory,IPropertyTaxStatusQueryHandler statusQueryHandler, IPropertyTaxBillCalculator taxBillCalculator,CommandDispatcher commandDispatcher,IDateTimeService dateTimeService)
        {
            this.dateTimeService = dateTimeService;
            this.commandDispatcher = commandDispatcher;
            contextFactory = trioContextFactory;
            billingType = billType;
            statusReportsRepository = reportsRepository;
            billingMasterRepository = billsRepository;
            this.statusQueryHandler = statusQueryHandler;
            this.taxBillCalculator = taxBillCalculator;
            GetListOfStatusReports();
            GetTaxYears();
            FillTransactionTypes();
            FillBillStatusTypes();
            FillDefaultReportTypes();
            FillTaxAcquiredOptions();
            FillNameOptions();
            FillComparisonOptions();
            GetTranCodes(billType);
            GetRateRecords();            
        }

        public void SetReportType(string reportType)
        {
            if (reportType != ReportType)
            {
                ReportType = reportType;
                UpdateForReportType();
            }
        }
        private void UpdateForReportType()
        {
            FillAvailableFields();
            FillSortOptions();
            GetListOfStatusReports();
        }

        private TaxCollectionReportFilter reportFilter = new TaxCollectionReportFilter();
        private TaxCollectionReportOption reportOption = new TaxCollectionReportOption();
        private ISavedStatusReportsRepository statusReportsRepository;
        private IBillingMasterRepository billingMasterRepository;
        private PropertyTaxBillType billingType = PropertyTaxBillType.Real;
        public void SetFilter(TaxCollectionReportFilter filter)
        {
            reportFilter = filter.GetCopy();
        }
        public TaxCollectionReportFilter GetFilter()
        {
            return reportFilter.GetCopy();
        }
        public void ClearFilter()
        {
            reportFilter = new TaxCollectionReportFilter();
            OnFilterChanged(new EventArgs() { });
        }

        public void SetOption(TaxCollectionReportOption option)
        {
            reportOption = option.GetCopy();
        }

        public SavedStatusReport GetStatusReport(int id)
        {
            return statusReportsRepository.Get(id);
        }
        public void DeleteSavedStatusReport(int id)
        {
            statusReportsRepository.Remove(id);
            GetListOfStatusReports();
        }
        protected void OnFilterChanged(EventArgs e)
        {
            EventHandler handler = FilterChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected void OnSortOptionsChanged(EventArgs e)
        {
            EventHandler handler = SortOptionsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected void OnStatusReportsChanged(EventArgs e)
        {
            EventHandler handler = StatusReportsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected void OnAvailableFieldsChanged(EventArgs e)
        {
            EventHandler handler = AvailableFieldsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnSelectedOptionsChanged(EventArgs e)
        {
            EventHandler handler = SelectedOptionsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void GetListOfStatusReports()
        {
            StatusReportDescriptions = statusReportsRepository.GetDescriptions(ReportType);
            OnStatusReportsChanged(new EventArgs(){});
        }
        private void GetTaxYears()
        {
            var taxYears = billingMasterRepository.GetTaxYears();
            TaxYears = taxYears.Select(value => value.ToString().Substring(0, 4) + "-" + value.ToString().Reverse().FirstOrDefault());
        }
        private void FillTransactionTypes()
        {           
            TransactionTypes = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair(){ Description = "Abatements", ID = 0 },
                new DescriptionIDPair(){Description = "Discounts", ID = 1},
                new DescriptionIDPair() {Description = "Refunded Abatements", ID = 2},
                new DescriptionIDPair() {Description = "Tax Clubs", ID = 3},
                new DescriptionIDPair() {Description = "Pre Payments", ID = 4},
                new DescriptionIDPair() {Description = "Lien / 30 Day Costs", ID = 5},
                new DescriptionIDPair() {Description = "Payments", ID = 6},
                new DescriptionIDPair() {Description = "Corrections", ID = 7}
            };
        }

        private void GetTranCodes(PropertyTaxBillType billType)
        {
            switch (billType)
            {
                case PropertyTaxBillType.Personal:
                    GetPersonalPropertyTranCodes();
                    break;
                case PropertyTaxBillType.Real:
                    GetRealEstateTranCodes();
                    break;
            }
        }

        private void GetRealEstateTranCodes()
        {
            var reContext = contextFactory.GetRealEstateContext();
            var tranRepository = new Repository<SharedApplication.RealEstate.Models.TranCode>(reContext);  
            TranCodes = tranRepository.GetAll().OrderBy(o => o.Code).MapToDescriptionIDPairsShowingCode().ToList();            
        }

        private void GetPersonalPropertyTranCodes()
        {
            var ppContext = contextFactory.GetPersonalPropertyContext();
            var tranRepository = new Repository<SharedApplication.PersonalProperty.Models.TranCode>(ppContext);
            TranCodes = tranRepository.GetAll().OrderBy(o => o.Code).MapToDescriptionIDPairsShowingCode().ToList();
        }

        private void FillSortOptions()
        {
            switch (ReportType.ToLower())
            {
                case "taxclub":
                    FillSortOptionsForTaxClub();
                    break;
                case "taxlien":
                    FillSortOptionsForTaxLien();
                    break;
                case "prepayments":
                    FillSortOptionsForPrePayments();
                    break;
                default:
                    FillSortOptionsForAccount();
                    break;
            }
            OnSortOptionsChanged(new EventArgs() { });
        }

        private void FillSortOptionsForAccount()
        {
            SortOptions = new List<SelectableItem<OrderedDescriptionIDPair>>()
            {
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){ Description = "Account", ID = (int)TaxReportAccountSortType.Account, OrderNumber = 1} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){ Description = "Name", ID =  (int)TaxReportAccountSortType.Name , OrderNumber = 2} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){ Description  = "Year", ID = (int)TaxReportAccountSortType.Year, OrderNumber = 3} }
            };
        }

        private void FillSortOptionsForPrePayments()
        {
            SortOptions = new List<SelectableItem<OrderedDescriptionIDPair>>()
            {
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){ Description = "Account", ID = 0, OrderNumber = 1} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){Description = "Name",ID = 1, OrderNumber = 2} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){Description = "Date",ID = 2, OrderNumber = 3} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){Description = "Amount",ID = 3, OrderNumber = 4} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){Description = "Period",ID = 4, OrderNumber = 5} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair(){Description = "Reference",ID = 5 , OrderNumber = 6} }
            };
        }

        private void FillSortOptionsForTaxClub()
        {
            SortOptions = new List<SelectableItem<OrderedDescriptionIDPair>>()
            {
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Account", ID = 0, OrderNumber = 1} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Name", ID = 1, OrderNumber = 2} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Date", ID = 2, OrderNumber = 3} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Amount", ID = 3, OrderNumber = 4} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Period", ID = 4, OrderNumber = 5} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Reference", ID = 5 , OrderNumber = 6} }
            };
        }
        private void FillSortOptionsForTaxLien()
        {
            SortOptions = new List<SelectableItem<OrderedDescriptionIDPair>>()
            {
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Account", ID = 0, OrderNumber = 1} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Name", ID = 1, OrderNumber = 2} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Date", ID = 2, OrderNumber = 3} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Amount", ID = 3, OrderNumber = 4} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Period", ID = 4, OrderNumber = 5} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Reference", ID = 5 , OrderNumber = 6} },
                new SelectableItem<OrderedDescriptionIDPair>(){IsSelected = false, Item = new OrderedDescriptionIDPair() {Description = "Total", ID = 6, OrderNumber = 7} }
            };
        }

        private void FillAvailableFields()
        {
            switch (ReportType.ToLower())
            {
                case "taxclub":
                    FillAvailableFieldsForTaxClub();
                    break;
                case "taxlien":
                    FillAvailableFieldsForTaxLien();
                    break;
                case "prepayments":
                    FillAvailableFieldsForPrePayments();
                    break;
                default:
                    FillAvailableFieldsForAccount();
                    break;
            }
            OnAvailableFieldsChanged(new EventArgs() { });
        }

        private void FillAvailableFieldsForAccount()
        {
            AvailableFields = new List<SelectableItem<DescriptionIDPair>>()
            {
                 new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Location", ID = 0 } },
                 new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Map / Lot", ID = 1 } },
                 new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Mailing Address", ID = 2 } }
            };
        }

        private void FillAvailableFieldsForPrePayments()
        {
            AvailableFields = new List<SelectableItem<DescriptionIDPair>>()
            {
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Account", ID = 0} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Name", ID = 1} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Date", ID = 2} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Amount", ID = 3} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Period", ID = 4} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Reference", ID = 5} }
            };
        }

        private void FillAvailableFieldsForTaxClub()
        {
            AvailableFields = new List<SelectableItem<DescriptionIDPair>>()
            {
               new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item =  new DescriptionIDPair() {Description = "Account", ID = 0} },
               new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Name", ID = 1} },
               new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item =  new DescriptionIDPair() {Description = "Date", ID = 2} },
               new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item =  new DescriptionIDPair() {Description = "Amount", ID = 3} },
               new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item =  new DescriptionIDPair() {Description = "Period", ID = 4} },
               new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item =  new DescriptionIDPair() {Description = "Reference", ID = 5} }
            };
        }

        private void FillAvailableFieldsForTaxLien()
        {
            AvailableFields = new List<SelectableItem<DescriptionIDPair>>()
            {
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Account", ID = 0} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Name", ID = 1} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Principal", ID = 2} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Pre Lien Interest", ID = 3} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Costs", ID = 4} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Current Interest", ID = 5} },
                new SelectableItem<DescriptionIDPair>(){IsSelected = false, Item = new DescriptionIDPair() {Description = "Total", ID = 6} }
            };
        }

        public TaxCollectionStatusReportConfiguration GetReportConfiguration()
        {
            var reportConfiguration = new TaxCollectionStatusReportConfiguration(reportOption, reportFilter);
            reportConfiguration.BillingType = billingType;            
            reportConfiguration.SelectClause = CreateSelectStatement(reportFilter, reportOption);
            reportConfiguration.WhereClause = CreateWhereClause(reportFilter, reportOption);
            reportConfiguration.SortClause = CreateSortParameter( reportOption);
            reportConfiguration.SQLStatement = reportConfiguration.SelectClause + reportConfiguration.WhereClause.Trim() != "" ? " Where " + reportConfiguration.WhereClause : ""  +
                                               reportConfiguration.SortClause.Trim() != "" ? " order by " + reportConfiguration.SortClause : "";
            return reportConfiguration;
        }

        //private string CreateSQLStatement(TaxCollectionReportFilter filter, TaxCollectionReportOption option)
        //{
        //    return CreateSelectStatement(filter,option) + CreateWhereClause(filter,option) + CreateSortParameter(option);
        //}

        private string CreateSelectStatement(TaxCollectionReportFilter filter,  TaxCollectionReportOption option)
        {
            string strDateCheck = "";
            string strPaymentDateRange = "";
            string selectStatement = "";
            if (option.UseAsOfDate())
            {
                strDateCheck = " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + option.AsOfDate.ToShortDateString() + "'";
            }
            else
            {
                strDateCheck = "";
            }
            if (filter.PaymentDateMin.HasValue && filter.PaymentDateMax.HasValue)
            {
                strPaymentDateRange = " AND (RecordedTransactionDate >= '" + filter.PaymentDateMin.Value.ToShortDateString() + "' AND RecordedTransactionDate <= '" + filter.PaymentDateMax.Value.ToShortDateString() + "')" + strDateCheck;
            }
            else
            {
                strPaymentDateRange = strDateCheck;
            }
            if (!filter.AccountPaymentType.HasValue)
            {
                if (filter.PaymentDateMin.HasValue || filter.PaymentDateMax.HasValue)
                {
                    selectStatement =
                        "Select Distinct BillCode, BillKey as BK from PaymentRec Where Code In ('P','C','A','R','Y','U','D') " +
                        strPaymentDateRange;
                }
                else
                {

                    selectStatement = "Select Distinct BillingType, ID as BK from BillingMaster";                    
                }
            }
            else
            {
                selectStatement = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec ";

                switch (filter.AccountPaymentType.Value)
                {
                    case 6:
                        selectStatement += " Where Code = 'P'";
                        break;
                    case 7:
                        selectStatement += " Where Code = 'C'";
                        break;
                    case 0:
                        selectStatement += " Where Code = 'A'";
                        break;
                    case 2:
                        selectStatement += " Where Code = 'R'";
                        break;
                    case 4:
                        selectStatement += " Where Code = 'Y'";
                        break;
                    case 3:
                        selectStatement += " Where Code = 'U'";
                        break;
                    //case "SUPPLEMENTALS":
                    //    selectStatement += " Where Code = 'S'";
                    //    break;
                    case 1:
                        break;
                    case 5:
                        break;
                }
                selectStatement += strPaymentDateRange;
            }

            return selectStatement;
        }

        private string CreateWhereClause(TaxCollectionReportFilter filter, TaxCollectionReportOption option)
        {   
                string whereParameter = " ";

                if (filter.AccountMin.HasValue)
                {                
                	if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                	{                
                        whereParameter += " Account between " + filter.AccountMin.ToString() + " and " +
                                  filter.AccountMax.ToString();
                    }
                	else
                    {
                        whereParameter += "Account >= " + filter.AccountMin.ToString();                        
                    }
                }
                else
                {
                	if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                    {
                        whereParameter += " Account <= " + filter.AccountMax.ToString();                        
                    }
                }

                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(whereParameter))
                    {
                        whereParameter += " and ";
                    }
                    if (modStatusPayments.Statics.boolRE)
                    {
                        whereParameter += " Own1FullName between '" + filter.NameMin + "' and '" +
                                          filter.NameMax + "zzzz'";
                    }
                    else
                    {
                        whereParameter += " Name1 between '" + filter.NameMin + "' and '" + filter.NameMax +
                                          "zzzz' ";                        
                    }
                }
                
                if (filter.TaxYearMin.HasValue && filter.TaxYearMin.Value > 0)
                {
                    if (!String.IsNullOrWhiteSpace(whereParameter))
                    {
                        whereParameter += " and ";
                    }
                    if (filter.TaxYearMax.HasValue && filter.TaxYearMax.Value > 0)
                    {
                        whereParameter += " BillingYear between " + filter.TaxYearMin.ToString() + " and " +
                                          filter.TaxYearMax.ToString();
                    }
                	else
                    {
                        whereParameter += "BillingYear = " + filter.TaxYearMin.ToString();
                    }
                }
                
                if (!option.UseDefaultReport)
                {

                    if (filter.TranCodeMin.HasValue)
                    {
                        if (!String.IsNullOrWhiteSpace(whereParameter))
                        {
                            whereParameter += " and ";
                        }

                        if (filter.TranCodeMax.HasValue)
                        {
                            whereParameter += "TranCode between " + filter.TranCodeMin.ToString() + " and " +
                                              filter.TranCodeMax.ToString();
                        }
                        else
                        {
                            whereParameter += "TranCode >= " + filter.TranCodeMin.ToString();
                        }
                    }
                    else
                    {
                        if (filter.TranCodeMax.HasValue)
                        {
                            if (!String.IsNullOrWhiteSpace(whereParameter))
                            {
                                whereParameter += " and ";
                            }

                            whereParameter += "TranCode <= " + filter.TranCodeMax.ToString();
                        }
                    }


                    if (filter.IsTaxAcquired.HasValue)
                    {
                        if (!String.IsNullOrWhiteSpace(whereParameter))
                        {
                            whereParameter += " and ";
                        }

                        whereParameter += " TaxAcquired = " + (filter.IsTaxAcquired.Value ? "1" : "0");
                    }

                    if (filter.TaxBillStatus.HasValue)
                    {
                        if (!String.IsNullOrWhiteSpace(whereParameter))
                        {
                            whereParameter += " and ";
                        }
                        switch (filter.TaxBillStatus.Value)
                        {
                            case TaxBillStatusType.Regular:
                                whereParameter += " LienRecordNumber = 0 ";
                                break;
                            case TaxBillStatusType.Lien:
                                whereParameter += " LienRecordNumber <> 0";
                                break;
                            case TaxBillStatusType.PreLien:
                                whereParameter += " LienRecordNumber <> 0";
                            break;
                        }
                    }
                }

                if (option.UseDefaultReport && option.DefaultReportType == TaxBillingHardCodedReport.LienBreakdown)
                {
                    if (filter.RateRecordMin.HasValue && filter.RateRecordMin.Value > 0)
                    {
                       if (!String.IsNullOrWhiteSpace(whereParameter))
                       {
                            whereParameter += " and ";
                       }

                       if (filter.RateRecordMax.HasValue && filter.RateRecordMax.Value > 0)
                       {
                           whereParameter += "LienRec.RateKey between " + filter.RateRecordMin.ToString() + " and " +
                                             filter.RateRecordMax.ToString();
                       }
                       else
                       {
                           whereParameter += "LienRec.RateKey >= " + filter.RateRecordMin.ToString();
                       }
                    }
                    else if (filter.RateRecordMax.HasValue && filter.RateRecordMax.Value > 0)
                    {
                        if (!String.IsNullOrWhiteSpace(whereParameter))
                        {
                            whereParameter += " and ";
                        }

                        whereParameter += "LienRec.RateKey <= " + filter.RateRecordMax.ToString();
                    }
                }
                else
                {
                    if (filter.RateRecordMin.HasValue && filter.RateRecordMin.Value > 0)
                    {
                        if (!String.IsNullOrWhiteSpace(whereParameter))
                        {
                            whereParameter += " and ";
                        }

                        if (filter.RateRecordMax.HasValue && filter.RateRecordMax.Value > 0)
                        {
                            whereParameter += "RateKey between " + filter.RateRecordMin.ToString() + " and " +
                                              filter.RateRecordMax.ToString();
                        }
                        else
                        {
                            whereParameter += "RateKey >= " + filter.RateRecordMin.ToString();
                        }
                    }
                    else if (filter.RateRecordMax.HasValue && filter.RateRecordMax.Value > 0)
                    {
                        if (!String.IsNullOrWhiteSpace(whereParameter))
                        {
                            whereParameter += " and ";
                        }

                        whereParameter += "RateKey <= " + filter.RateRecordMax.ToString();
                    }
                }                
                
                return whereParameter;           
        }

        private Expression<Func<RealEstateBillAccountPartyView,bool>> CreateRealWherePredicate(TaxCollectionReportFilter filter, TaxCollectionReportOption option)
        {
            var wherePredicate = PredicateBuilder.True<RealEstateBillAccountPartyView>();
            if (filter.AccountMin.HasValue)
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    wherePredicate = wherePredicate.And(b =>
                        b.Account >= filter.AccountMin && b.Account <= filter.AccountMax);
                }
                else
                {
                    wherePredicate = wherePredicate.And(b => b.Account >= filter.AccountMin);
                }
            }
            else
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    wherePredicate = wherePredicate.And(b => b.Account <= filter.AccountMax.Value);
                }
            }

            if (!filter.NameMin.IsNullOrWhiteSpace())
            {
                wherePredicate =
                    wherePredicate.And(b => String.Compare(b.DeedName1,filter.NameMin) >= 0 && String.Compare(b.DeedName1,filter.NameMax) <= 0);
            }

            if (filter.TaxYearMin.HasValue && filter.TaxYearMin.Value > 0)
            {
                if (filter.TaxYearMax.HasValue && filter.TaxYearMax.Value > 0)
                {
                    wherePredicate = wherePredicate.And(b =>
                        b.BillingYear >= filter.TaxYearMin && b.BillingYear <= filter.TaxYearMax);
                }
                else
                {
                    wherePredicate = wherePredicate.And(b => b.BillingYear == filter.TaxYearMin);
                }
            }
            if (!option.UseDefaultReport)
            {
                if (filter.RateRecordRangeUsed())
                {
                    if (filter.RateRecordMin.HasValue)
                    {
                        if (filter.RateRecordMax.HasValue)
                        {
                            wherePredicate = wherePredicate.And(b =>
                                (b.RateKey >= filter.RateRecordMin && b.RateKey <= filter.RateRecordMax) ||
                                (b.LienRateKey >= filter.RateRecordMin && b.LienRateKey <= filter.RateRecordMax));
                        }
                        else
                        {
                            wherePredicate = wherePredicate.And(b =>
                                (b.RateKey >= filter.RateRecordMin) || (b.LienRateKey >= filter.RateRecordMin));
                        }
                    }
                    else
                    {
                        if (filter.RateRecordMax.HasValue)
                        {
                            wherePredicate = wherePredicate.And(b =>
                                b.RateKey < filter.RateRecordMax ||
                                (b.LienRateKey > 0 && b.LienRateKey <= filter.RateRecordMax));
                        }
                    }
                }
                if (filter.TaxBillStatus.HasValue)
                {
                    switch (filter.TaxBillStatus.Value)
                    {
                        case TaxBillStatusType.Regular:
                            wherePredicate = wherePredicate.And(b => b.LienRecordNumber == 0);
                            break;
                        case TaxBillStatusType.Lien:
                            wherePredicate = wherePredicate.And(b => b.LienRecordNumber > 0);
                            break;
                        case TaxBillStatusType.PreLien:
                            wherePredicate = wherePredicate.And(b => b.LienRecordNumber > 0);
                            break;
                    }
                }
                if (filter.TranCodeMin.HasValue)
                {

                    if (filter.TranCodeMax.HasValue)
                    {
                        wherePredicate = wherePredicate.And(b =>
                            b.TranCode >= filter.TranCodeMin && b.TranCode <= filter.TranCodeMax);
                    }
                    else
                    {
                        wherePredicate = wherePredicate.And(b => b.TranCode >= filter.TranCodeMin);
                    }
                }
                else
                {
                    if (filter.TranCodeMax.HasValue)
                    {
                        wherePredicate = wherePredicate.And(b => b.TranCode <= filter.TranCodeMax);
                    }
                }
                if (filter.IsTaxAcquired.HasValue)
                {
                    wherePredicate = wherePredicate.And(b => b.TaxAcquired == filter.IsTaxAcquired);
                }
            }

            //if (filter.RateRecordRangeUsed())
            //{
            //    if (filter.RateRecordMin.HasValue)
            //    {
            //        if (filter.RateRecordMax.HasValue)
            //        {
            //            wherePredicate = wherePredicate.And(b => b.LienRateKey >= filter.RateRecordMin && b.LienRateKey <= filter.RateRecordMax);
            //        }
            //        else
            //        {
            //            wherePredicate = wherePredicate.And(b => b.LienRateKey >= filter.RateRecordMin);
            //        }
            //    }
            //    else
            //    {
            //        if (filter.RateRecordMax.HasValue)
            //        {
            //            wherePredicate = wherePredicate.And(b => b.LienRateKey > 0 && b.LienRateKey <= filter.RateRecordMax);
            //        }
            //    }
            //}

            
            if (option.UseDefaultReport && option.DefaultReportType == TaxBillingHardCodedReport.LienBreakdown)
            {
                if (filter.RateRecordRangeUsed())
                {
                    if (filter.RateRecordMin.HasValue)
                    {
                        if (filter.RateRecordMax.HasValue)
                        {
                            wherePredicate = wherePredicate.And(b =>
                                (b.RateKey >= filter.RateRecordMin && b.RateKey <= filter.RateRecordMax) ||
                                (b.LienRateKey >= filter.RateRecordMin && b.LienRateKey <= filter.RateRecordMax));
                        }
                        else
                        {
                            wherePredicate = wherePredicate.And(b =>
                                (b.RateKey >= filter.RateRecordMin) || (b.LienRateKey >= filter.RateRecordMin));
                        }
                    }
                    else
                    {
                        if (filter.RateRecordMax.HasValue)
                        {
                            wherePredicate = wherePredicate.And(b =>
                                b.RateKey < filter.RateRecordMax ||
                                (b.LienRateKey > 0 && b.LienRateKey <= filter.RateRecordMax));
                        }
                    }
                }
            }
            else
            {
                if (filter.RateRecordRangeUsed())
                {
                    if (filter.RateRecordMin.HasValue)
                    {
                        if (filter.RateRecordMax.HasValue)
                        {
                            wherePredicate = wherePredicate.And(b =>
                                b.RateKey >= filter.RateRecordMin && b.RateKey <= filter.RateRecordMax);
                        }
                        else
                        {
                            wherePredicate = wherePredicate.And(b =>
                                b.RateKey >= filter.RateRecordMin);
                        }
                    }
                    else
                    {
                        if (filter.RateRecordMax.HasValue)
                        {
                            wherePredicate = wherePredicate.And(b =>
                                b.RateKey <= filter.RateRecordMax );
                        }
                    }
                }
            }
            return wherePredicate;
        }

        private Expression<Func<PersonalPropertyBillAccountPartyView, bool>> CreatePersonalWherePredicate(
            TaxCollectionReportFilter filter, TaxCollectionReportOption option)
        {
            var wherePredicate = PredicateBuilder.True<PersonalPropertyBillAccountPartyView>();
            if (filter.AccountMin.HasValue)
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    wherePredicate = wherePredicate.And(b =>
                        b.Account >= filter.AccountMin && b.Account <= filter.AccountMax);
                }
                else
                {
                    wherePredicate = wherePredicate.And(b => b.Account >= filter.AccountMin);
                }
            }
            else
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    wherePredicate = wherePredicate.And(b => b.Account <= filter.AccountMax.Value);
                }
            }
            if (!filter.NameMin.IsNullOrWhiteSpace())
            {
                wherePredicate =
                    wherePredicate.And(b => String.Compare(b.Name1, filter.NameMin) >= 0 && String.Compare(b.Name1, filter.NameMax) <= 0);
            }
            if (filter.TaxYearMin.HasValue && filter.TaxYearMin.Value > 0)
            {
                if (filter.TaxYearMax.HasValue && filter.TaxYearMax.Value > 0)
                {
                    wherePredicate = wherePredicate.And(b =>
                        b.BillingYear >= filter.TaxYearMin && b.BillingYear <= filter.TaxYearMax);
                }
                else
                {
                    wherePredicate = wherePredicate.And(b => b.BillingYear == filter.TaxYearMin);
                }
            }
            if (!option.UseDefaultReport)
            {
                if (filter.TranCodeMin.HasValue)
                {

                    if (filter.TranCodeMax.HasValue)
                    {
                        wherePredicate = wherePredicate.And(b =>
                            b.TranCode >= filter.TranCodeMin && b.TranCode <= filter.TranCodeMax);
                    }
                    else
                    {
                        wherePredicate = wherePredicate.And(b => b.TranCode >= filter.TranCodeMin);
                    }
                }
                else
                {
                    if (filter.TranCodeMax.HasValue)
                    {
                        wherePredicate = wherePredicate.And(b => b.TranCode <= filter.TranCodeMax);
                    }
                }

            }

            if (filter.RateRecordRangeUsed())
            {
                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        wherePredicate = wherePredicate.And(b =>
                            b.RateKey >= filter.RateRecordMin && b.RateKey <= filter.RateRecordMax);
                    }
                    else
                    {
                        wherePredicate = wherePredicate.And(b =>
                            b.RateKey >= filter.RateRecordMin);
                    }
                }
                else
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        wherePredicate = wherePredicate.And(b =>
                            b.RateKey <= filter.RateRecordMax);
                    }
                }
            }

            return wherePredicate;
        }

        private string CreateSortParameter(TaxCollectionReportOption options)
        {
            string sortParameter = " ";
            string separator = "";
            // BUILD THE SORT CRITERIA FOR THE SQL STATEMENT

            // GET THE FIELDS TO SORT BY
            var selectedOptions = SortOptions.Where(o => o.IsSelected).OrderBy(o => o.Item.OrderNumber);
            foreach (var sortOption in selectedOptions)
            {
                sortParameter += separator;
                separator = ", ";
                switch (sortOption.Item.ID)
                {
                    case (int)TaxReportAccountSortType.Account:
                        sortParameter += "Account";
                        break;
                    case (int)TaxReportAccountSortType.Name:
                        if (options.NameToShow == TaxReportNameOption.CurrentOwner ||
                            options.NameToShow == TaxReportNameOption.CurrentOwnerBilledAsOwner)
                        {
                            sortParameter += "Own1FullName";
                        }
                        else
                        {
                            sortParameter += "Name1";
                        }
                        break;
                    case (int)TaxReportAccountSortType.Year:
                        sortParameter += "BillingYear";
                        break;
                }
            }

            if (String.IsNullOrWhiteSpace(sortParameter))
            {
                if (options.NameToShow == TaxReportNameOption.CurrentOwner ||
                    options.NameToShow == TaxReportNameOption.CurrentOwnerBilledAsOwner)
                {
                    sortParameter = "Own1FullName, Account, BillingYear";
                }
                else
                {
                    sortParameter = "Name1, Account, BillingYear";
                }
            }

            return sortParameter;
        }

        private void FillBillStatusTypes()
        {   
            BillStatusTypes = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair(){Description = "Both",ID = 0},
                new DescriptionIDPair(){Description = "Regular", ID = 1},
                new DescriptionIDPair(){Description =  "Lien",ID = 2},
                new DescriptionIDPair(){Description =  "PreLien Information", ID = 3}
            };
        }

        private void FillNameOptions()
        {
            NameOptions = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair(){Description = "Show Current Owner", ID = (int)TaxReportNameOption.CurrentOwner},
                new DescriptionIDPair(){Description = "Show Billed Owner", ID = (int)TaxReportNameOption.BilledOwner},
                new DescriptionIDPair(){Description = @"Show Billed Owner C\O Current Owner", ID = (int)TaxReportNameOption.BilledOwnerCareOfCurrentOwner},                
            };
        }

        private void FillComparisonOptions()
        {
            ComparisonOptions = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair(){Description = ComparisonOptionType.LessThan.ToVerboseString(), ID = (int)ComparisonOptionType.LessThan},
                new DescriptionIDPair(){Description = ComparisonOptionType.GreaterThan.ToVerboseString(),ID = (int)ComparisonOptionType.GreaterThan},
                new DescriptionIDPair(){Description = ComparisonOptionType.EqualTo.ToVerboseString(),ID = (int)ComparisonOptionType.EqualTo},
                new DescriptionIDPair(){Description = ComparisonOptionType.NotEqualTo.ToVerboseString(),ID = (int)ComparisonOptionType.NotEqualTo}
            };
        }

        private void SetFilterOptionsByType(string reportType)
        {
            switch (reportType.ToLower())
            {
                case "":
                    break;
                default:
                    break;
            }
        }

        private void FillDefaultReportTypes()
        {
            DefaultReportTypes = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair() {Description = "Non Zero Balance on All Accounts", ID = (int)TaxBillingHardCodedReport.NonZeroBalanceOnAllAccounts },
                new DescriptionIDPair(){ Description = "Non Zero Balance on Non Lien Accounts", ID = (int)TaxBillingHardCodedReport.NonZeroBalanceOnNonLienAccounts },
                new DescriptionIDPair() {Description = "Non Zero Balance on Lien Accounts", ID = (int)TaxBillingHardCodedReport.NonZeroBalanceOnLienAccounts },
                new DescriptionIDPair() {Description = "Lien Breakdown", ID = (int)TaxBillingHardCodedReport.LienBreakdown},
                new DescriptionIDPair() {Description = "Zero Balance Report", ID = (int)TaxBillingHardCodedReport.ZeroBalanceReport},
                new DescriptionIDPair() {Description = "Negative Balance Report", ID = (int)TaxBillingHardCodedReport.NegativeBalanceReport},
                new DescriptionIDPair() {Description = "Supplemental Outstanding Balance Report", ID = (int)TaxBillingHardCodedReport.SupplementalOutstandingBalanceReport},
                new DescriptionIDPair() {Description = "Supplemental Negative Balance Report", ID = (int)TaxBillingHardCodedReport.SupplementalNegativeBalanceReport},
                new DescriptionIDPair() {Description = "Supplemental Zero Balance Report", ID = (int)TaxBillingHardCodedReport.SupplementalZeroBalanceReport},
                new DescriptionIDPair() {Description = "Outstanding Balance By Period", ID = (int)TaxBillingHardCodedReport.OutstandingBalanceByPeriod},
                new DescriptionIDPair() {Description = "Account Detail Report", ID = (int)TaxBillingHardCodedReport.AccountDetailReport},
                new DescriptionIDPair() {Description = "Audit Summary Report", ID = (int)TaxBillingHardCodedReport.AuditSummaryReport},
                new DescriptionIDPair() {Description = "Supplemental Bills", ID = (int)TaxBillingHardCodedReport.SupplementalBills},
                new DescriptionIDPair() {Description = "Non Zero Balance on All Accounts by Year", ID = (int)TaxBillingHardCodedReport.NonZeroBalanceOnAllAccountsByYear},
                new DescriptionIDPair() {Description = "Bankrupt Accounts", ID = (int)TaxBillingHardCodedReport.BankruptAccounts}
            };

        }

        private void FillTaxAcquiredOptions()
        {
            TaxAcquiredOptions = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair(){Description = "Tax Acquired",ID = (int)TaxAcquiredOptionType.TaxAcquired},
                new DescriptionIDPair(){Description = "Non-Tax Acquired",ID = (int)TaxAcquiredOptionType.NonTaxAcquired}
            };
        }

        private void GetRateRecords()
        {
            var clContext = contextFactory.GetTaxCollectionsContext();
            var rateRepository = new Repository<RateRec>(clContext);
            RateRecords = rateRepository.GetAll().OrderBy(r => r.Id).MapToDescriptionIDPairsShowingIDAndDate().ToList();            
        }

        public void SaveReport(string reportName)
        {
            var clContext = contextFactory.GetTaxCollectionsContext();
            var savedRepo  = new SavedStatusReportRepository(clContext);
            var savedReport = savedRepo.Find(r => r.ReportName == reportName).FirstOrDefault();
            if (savedReport == null)
            {
                savedReport = new SavedStatusReport();
                clContext.SavedStatusReports.Add(savedReport);
            }
            savedReport.ReportName = reportName;
            savedReport.ExcludePaid = reportOption.ExcludePaid;
            savedReport.LastUpdated = DateTime.Now;
            savedReport.SQL = "";
            savedReport.ShowCurrentInterest = reportOption.ShowCurrentInterest;
            savedReport.ShowPayments = reportOption.ShowPayments;
            savedReport.ShowSummaryOnly = reportOption.ShowSummaryOnly;
            savedReport.Type = reportOption.ReportType;
            SetConstraintsForReport(ref savedReport);
            savedReport.SortSelection = GetSortOptionsToSave();
            clContext.SaveChanges();
        }

        private void SetConstraintsForReport(ref SavedStatusReport savedReport)
        {
            var separator = @"|||";
            savedReport.FieldConstraint0 = reportFilter.AccountMin.ToString() + separator + reportFilter.AccountMax.ToString();
            savedReport.FieldConstraint1 = reportFilter.NameMin + separator + reportFilter.NameMax;
            savedReport.FieldConstraint2 =
                reportFilter.TaxYearMin.ToString() + separator + reportFilter.TaxYearMax.ToString();
            savedReport.FieldConstraint3 = GetBalanceDueDescription(reportFilter.BalanceDueComparisonType) + separator + reportFilter.BalanceDueAmount.ToString();
            savedReport.FieldConstraint4 = GetAccountPaymentTypeDescription(reportFilter.AccountPaymentType) + separator;
            if (reportFilter.AsOfDate.HasValue)
            {
                savedReport.FieldConstraint5 = reportFilter.AsOfDate.Value.FormatAndPadShortDate() + separator;
            }
            else
            {
                savedReport.FieldConstraint5 = separator;
            }
            savedReport.FieldConstraint6 = reportFilter.PaymentDateMin.ToString() + separator + reportFilter.PaymentDateMax.ToString();
            savedReport.FieldConstraint7 =
                reportFilter.TranCodeMin.ToString() + separator + reportFilter.TranCodeMax.ToString();
            savedReport.FieldConstraint8 = GetTaxAcquiredDescription(reportFilter.IsTaxAcquired) + separator;
            savedReport.FieldConstraint9 = reportFilter.RateRecordMin.ToString() + separator +
                                           reportFilter.RateRecordMax.ToString();

        }

        private string GetBalanceDueDescription(ComparisonOptionType? optionType)
        {
            if (!optionType.HasValue)
            {
                return "";
            }

            switch (optionType.Value)
            {
                case ComparisonOptionType.EqualTo:
                    return "=";
                case ComparisonOptionType.GreaterThan:
                    return ">";
                case ComparisonOptionType.LessThan:
                    return "<";
                case ComparisonOptionType.NotEqualTo:
                    return "<>";
                default:
                    return "";
            }
        }

        private string GetAccountPaymentTypeDescription(int? paymentType)
        {
            if (!paymentType.HasValue)
            {
                return "";
            }

            switch (paymentType.Value)
            {
                case 0:
                    return "Abatements";
                    break;
                case 1:
                    return "Discounts";
                case 2:
                    return "Refunded Abatements";
                case 3:
                    return "Tax Clubs";
                case 4:
                    return "Pre Payments";
                case 5:
                    return @"Lien / 30 Day Costs";
                case 6:
                    return "Payments";
                case 7:
                    return "Corrections";
                default:
                    return "";
            }            
        }

        private string GetTaxAcquiredDescription(bool? isTaxAcquired)
        {
            if (!isTaxAcquired.HasValue)
            {
                return "";
            }

            if (isTaxAcquired.Value)
            {
                return "Tax Acquired";
            }

            return "Non Tax Acquired";
        }
        private string GetSortOptionsToSave()
        {
            var sortString = "";
            var separator = "";
            foreach (var idPair in SortOptions)
            {
                if (idPair.IsSelected)
                {
                    sortString += separator + idPair.Item.ID.ToString();
                    separator = ",";
                }
            }

            return sortString;
        }

        public void LoadReport(int reportId)
        {
            var clContext = contextFactory.GetTaxCollectionsContext();
            var savedRepo = new SavedStatusReportRepository(clContext);
            var savedReport = savedRepo.Find(r => r.ID == reportId).FirstOrDefault();
            if (savedReport == null)
            {
                return;
            }

            reportOption.ExcludePaid = savedReport.ExcludePaid ?? false;
            reportOption.ShowCurrentInterest = savedReport.ShowCurrentInterest ?? false;
            reportOption.ShowPayments = savedReport.ShowPayments ?? false;
            reportOption.ShowSummaryOnly = savedReport.ShowSummaryOnly ?? false;
            reportOption.ReportType = savedReport.Type;
            GetConstraintsFromReport(savedReport);
            GetSortOptionsFromReport(savedReport);
            NotifyOptionsChanged();
        }

        private void GetConstraintsFromReport(SavedStatusReport savedReport)
        {
            var separator = @"|||";
            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint0))
            {

                var accounts = ParseConstraint(savedReport.FieldConstraint0);
                if (!String.IsNullOrWhiteSpace(accounts.start))
                {
                    reportFilter.AccountMin = Convert.ToInt32(accounts.start);
                }
                else
                {
                    reportFilter.AccountMin = null;
                }

                if (!String.IsNullOrWhiteSpace(accounts.end))
                {
                    reportFilter.AccountMax = Convert.ToInt32(accounts.end);
                }
                else
                {
                    reportFilter.AccountMax = null;
                }                
            }
            else
            {
                reportFilter.AccountMin = null;
                reportFilter.AccountMax = null;
            }

            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint1))
            {
                var names = ParseConstraint(savedReport.FieldConstraint1);
                reportFilter.NameMin = names.start;
                reportFilter.NameMax = names.end;

            }
            else
            {
                reportFilter.NameMin = "";
                reportFilter.NameMax = "";
            }


            var taxYears = ParseConstraint(savedReport.FieldConstraint2);
            if (!String.IsNullOrWhiteSpace(taxYears.start))
            {
                reportFilter.TaxYearMin = Convert.ToInt32(taxYears.start.Replace("-",""));
            }
            else
            {
                reportFilter.TaxYearMin = null;
            }

            if (!String.IsNullOrWhiteSpace(taxYears.end))
            {
                reportFilter.TaxYearMax = Convert.ToInt32(taxYears.end.Replace("-", ""));
            }
            else
            {
                reportFilter.TaxYearMax = null;
            }
            
            var balanceDues = ParseConstraint( savedReport.FieldConstraint3);
            reportFilter.BalanceDueComparisonType = GetBalanceDueComparisonTypeFromDescription(balanceDues.start);
            if (!String.IsNullOrWhiteSpace(balanceDues.end))
            {
                reportFilter.BalanceDueAmount = Convert.ToInt32(balanceDues.end);
            }
            else
            {
                reportFilter.BalanceDueAmount = null;
            }

            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint4))
            {
                var accountPaymentType = savedReport.FieldConstraint4.Replace(separator, "");
                reportFilter.AccountPaymentType = GetAccountPaymentTypeFromDescription(accountPaymentType);
            }
            else
            {
                reportFilter.AccountPaymentType = null;
            }

            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint5))
            {
                var asOfDate = savedReport.FieldConstraint5.Replace(separator, "");
                if (asOfDate.IsDate())
                {
                    reportFilter.AsOfDate = Convert.ToDateTime(asOfDate);
                }
                else
                {
                    reportFilter.AsOfDate = null;
                }
            }
            else
            {
                reportFilter.AsOfDate = null;
            }


            var paymentDates = ParseConstraint(savedReport.FieldConstraint6);
            if (paymentDates.start.IsDate())
            {
                reportFilter.PaymentDateMin = Convert.ToDateTime(paymentDates.start);
            }
            else
            {
                reportFilter.PaymentDateMin = null;
            }

            if (paymentDates.end.IsDate())
            {
                reportFilter.PaymentDateMax = Convert.ToDateTime(paymentDates.end);
            }
            else
            {
                reportFilter.PaymentDateMax = null;
            }

            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint7))
            {
                var tranCodes = ParseConstraint(savedReport.FieldConstraint7);
                if (!String.IsNullOrWhiteSpace(tranCodes.start))
                {
                    reportFilter.TranCodeMin = Convert.ToInt32(tranCodes.start);
                }
                else
                {
                    reportFilter.TranCodeMin = null;
                }

                if ( !String.IsNullOrWhiteSpace(tranCodes.end))
                {
                    reportFilter.TranCodeMax = Convert.ToInt32(tranCodes.end);
                }
                else
                {
                    reportFilter.TranCodeMax = null;
                }
            }
            else
            {
                reportFilter.TranCodeMin = null;
                reportFilter.TranCodeMax = null;
            }

            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint8))
            {
                var taxAcquired = savedReport.FieldConstraint8.Replace(separator, "");
                if (!String.IsNullOrWhiteSpace(taxAcquired))
                {
                    reportFilter.IsTaxAcquired =taxAcquired.ToLower() == "tax acquired";
                }
                else
                {
                    reportFilter.IsTaxAcquired = null;
                }
            }
            else
            {
                reportFilter.IsTaxAcquired = null;
            }

            if (!String.IsNullOrWhiteSpace(savedReport.FieldConstraint9))
            {
                var rateRecords = ParseConstraint(savedReport.FieldConstraint9);
                if (!String.IsNullOrWhiteSpace(rateRecords.start))
                {
                    reportFilter.RateRecordMin = Convert.ToInt32(rateRecords.start);
                }
                else
                {
                    reportFilter.RateRecordMin = null;
                }

                if ( !String.IsNullOrWhiteSpace(rateRecords.end))
                {
                    reportFilter.RateRecordMax = Convert.ToInt32(rateRecords.end);
                }
                else
                {
                    reportFilter.RateRecordMax = null;
                }
            }
            else
            {
                reportFilter.RateRecordMin = null;
                reportFilter.RateRecordMax = null;
            }           
        }

        private (string start, string end) ParseConstraint(string constraint)
        {            
            (string start, string end) parsedValues = (start: "", end: "");
            if (!String.IsNullOrWhiteSpace(constraint))
            {
                var processedConstraint = constraint.Replace("|||", "|");
                var constraintArray = processedConstraint.Split('|');
                if (constraintArray.Length > 0)
                {
                    parsedValues.start = constraintArray[0].Trim();
                }

                if (constraintArray.Length > 1)
                {
                    parsedValues.end = constraintArray[1].Trim();
                }
            }

            return parsedValues;
        }

        private int? GetAccountPaymentTypeFromDescription(string accountPaymentType)
        {
            if (String.IsNullOrWhiteSpace(accountPaymentType))
            {
                return null;
            }

            switch (accountPaymentType.ToLower())
            {
                case "abatements":
                    return 0;
                case "discounts":
                    return 1;
                case "refunded abatements":
                    return 2;
                case "tax clubs":
                    return 3;
                case "pre payments":
                    return 4;
                case @"lien / 30 day costs":
                    return 5;
                case "payments":
                    return 6;
                case "corrections":
                    return 7;
                default:
                    return null;
            }
        }

        private ComparisonOptionType? GetBalanceDueComparisonTypeFromDescription(string description)
        {
            if (String.IsNullOrWhiteSpace(description))
            {
                return null;
            }

            switch (description)
            {
                case "=":
                    return ComparisonOptionType.EqualTo;
                case ">":
                    return ComparisonOptionType.GreaterThan;
                case "<":
                    return ComparisonOptionType.LessThan;
                case "<>":
                    return ComparisonOptionType.NotEqualTo;
                default:
                    return null;
            }
        }

        private void GetSortOptionsFromReport(SavedStatusReport savedReport)
        {
            var sortString = savedReport.SortSelection;
            var separator = ",";
            ClearSortOptions();
            var newSortOptions = new List<SelectableItem<OrderedDescriptionIDPair>>();
            var sortOptions = SortOptions.ToList();
            var index = 0;
            if (!String.IsNullOrWhiteSpace(sortString))
            {
                var sortIds = sortString.Split(separator.ToCharArray(), StringSplitOptions.None);
                foreach (var idString in sortIds)
                {
                    var id = Convert.ToInt32(idString);
                    var sortOption = sortOptions.Where(o => o.Item.ID == id).FirstOrDefault();
                    if (sortOption != null)
                    {
                        index++;
                        sortOption.IsSelected = true;
                        sortOption.Item.OrderNumber = index;
                        newSortOptions.Add(sortOption);
                        sortOptions.Remove(sortOption);
                    }
                }
                newSortOptions.AddRange(sortOptions);
                SortOptions = newSortOptions;
            }
        }

        private void ClearSortOptions()
        {
            foreach (var idPair in SortOptions)
            {
                idPair.IsSelected = false;
            }
        }

        private void NotifyOptionsChanged()
        {
            OnSelectedOptionsChanged(new EventArgs() { });
            OnSortOptionsChanged(new EventArgs() { });
            OnFilterChanged(new EventArgs() { });

        }

        public void ShowStatusList()
        {
            var reportConfiguration = GetReportConfiguration();
            var filter = GetFilter();
            var statusListViewModel = new PropertyTaxStatusListViewModel();
            if (reportConfiguration.BillingType == PropertyTaxBillType.Personal)
            {
                var personalWherePredicate = CreatePersonalWherePredicate(filter, reportConfiguration.Options);
                IEnumerable<PersonalPropertyAccountBill> personalAccountBills;
                DateTime effectiveDate = dateTimeService.Now();
                if (reportConfiguration.Options.UseAsOfDate())
                {
                    personalAccountBills = this.statusQueryHandler.FindPersonalAsOf(personalWherePredicate,
                        reportConfiguration.Options.UseAsOfDate(), reportConfiguration.Options.AsOfDate);
                    effectiveDate = reportConfiguration.Options.AsOfDate;
                }
                else
                {
                    personalAccountBills = this.statusQueryHandler.FindPersonal(personalWherePredicate);
                }
                
                if (filter.PaymentDateRangeUsed())
                {
                    personalAccountBills = personalAccountBills.Where(r =>
                        r.Payments.HasPaymentsInRange(filter.PaymentDateMin ?? DateTime.MinValue,
                            filter.PaymentDateMax ?? DateTime.MaxValue));
                }

                if (filter.AccountPaymentType > 0)
                {
                    PropertyTaxPaymentCode? paymentCode = null;
                    switch (filter.AccountPaymentType.GetValueOrDefault())
                    {
                        case 6:
                            paymentCode = PropertyTaxPaymentCode.RegularPayment;
                            break;
                        case 7:
                            paymentCode = PropertyTaxPaymentCode.Correction;
                            break;
                        case 0:
                            paymentCode = PropertyTaxPaymentCode.Abatement;
                            break;
                        case 2:
                            paymentCode = PropertyTaxPaymentCode.RefundedAbatement;
                            break;
                        case 4:
                            paymentCode = PropertyTaxPaymentCode.PrePayment;
                            break;
                        case 3:
                            paymentCode = PropertyTaxPaymentCode.TaxClubPayment;
                            break;
                        case 5:
                            paymentCode = PropertyTaxPaymentCode.LienMaturityFee;
                            break;
                    }

                    if (paymentCode.HasValue)
                    {
                        if (paymentCode != PropertyTaxPaymentCode.LienMaturityFee)
                        {
                            personalAccountBills =
                                personalAccountBills.Where(r => r.Payments.HasPaymentType(paymentCode.Value));
                        }
                        else
                        {
                            personalAccountBills =
                                personalAccountBills.Where(r => r.Payments.HasPaymentType(PropertyTaxPaymentCode.LienMaturityFee) || r.Payments.HasPaymentType(PropertyTaxPaymentCode.ThirtyDayNoticeFee));
                        }
                    }
                }

                if (reportConfiguration.Options.ShowCurrentInterest || reportConfiguration.Filter.BalanceDueUsed())
                {
                    var tempList = new List<PersonalPropertyAccountBill>();
                    foreach (var accountBill in personalAccountBills)
                    {
                        if (accountBill.TaxBill.BillSummary.GetNetOwed() != 0)
                        {
                            if (reportConfiguration.Options.ShowCurrentInterest)
                            {
                                var summary = taxBillCalculator.CalculateBill(accountBill.TaxBill, effectiveDate);
                                accountBill.TaxBill.BillSummary.InterestCharged += summary.Interest;
                                accountBill.NewInterest = summary.Interest;
                            }
                        }
                        if (reportConfiguration.Filter.BalanceDueUsed())
                        {
                            switch (reportConfiguration.Filter.BalanceDueComparisonType.Value)
                            {
                                case ComparisonOptionType.EqualTo:
                                    if (accountBill.TaxBill.BillSummary.GetNetOwed() ==
                                        reportConfiguration.Filter.BalanceDueAmount && (!reportConfiguration.Options.ExcludePaid || accountBill.TaxBill.BillSummary.GetNetOwed() != 0))
                                    {
                                        tempList.Add(accountBill);
                                    }
                                    break;
                                case ComparisonOptionType.GreaterThan:
                                    if (accountBill.TaxBill.BillSummary.GetNetOwed() >
                                        reportConfiguration.Filter.BalanceDueAmount)
                                    {
                                        tempList.Add(accountBill);
                                    }
                                    break;
                                case ComparisonOptionType.LessThan:
                                    if (accountBill.TaxBill.BillSummary.GetNetOwed() <
                                        reportConfiguration.Filter.BalanceDueAmount && (!reportConfiguration.Options.ExcludePaid || accountBill.TaxBill.BillSummary.GetNetOwed() != 0))
                                    {
                                        tempList.Add(accountBill);
                                    }
                                    break;
                                case ComparisonOptionType.NotEqualTo:
                                    if (accountBill.TaxBill.BillSummary.GetNetOwed() !=
                                        reportConfiguration.Filter.BalanceDueAmount && (!reportConfiguration.Options.ExcludePaid || accountBill.TaxBill.BillSummary.GetNetOwed() != 0))
                                    {
                                        tempList.Add(accountBill);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            if (!reportConfiguration.Options.ExcludePaid ||
                                accountBill.TaxBill.BillSummary.GetNetOwed() != 0)
                            {
                                tempList.Add(accountBill);
                            }
                        }
                    }
                    if (reportConfiguration.Filter.BalanceDueUsed() || reportConfiguration.Options.ExcludePaid)
                    {
                        personalAccountBills = tempList;
                        tempList = null;
                    }
                }
                statusListViewModel.AccountBills = personalAccountBills;
               
            }
            else
            {
                var realWherePredicate = CreateRealWherePredicate(filter, reportConfiguration.Options);
                IEnumerable<RealEstateAccountBill> realAccountBills;
                DateTime effectiveDate = dateTimeService.Now();
                if (reportConfiguration.Options.UseAsOfDate())
                {
                    realAccountBills  = this.statusQueryHandler.FindRealAsOf(realWherePredicate,
                        reportConfiguration.Options.UseAsOfDate(), reportConfiguration.Options.AsOfDate);
                    effectiveDate = reportConfiguration.Options.AsOfDate;
                }
                else
                {
                    realAccountBills = this.statusQueryHandler.FindReal(realWherePredicate);
                }
                
                if (filter.PaymentDateRangeUsed())
                {
                    realAccountBills = realAccountBills.Where(r =>
                        r.Payments.HasPaymentsInRange(filter.PaymentDateMin??DateTime.MinValue, filter.PaymentDateMax??DateTime.MaxValue));
                }

                if (filter.AccountPaymentType > 0)
                {
                    PropertyTaxPaymentCode? paymentCode = null;
                    switch (filter.AccountPaymentType.GetValueOrDefault())
                    {
                        case 6:
                            paymentCode = PropertyTaxPaymentCode.RegularPayment;
                            break;
                        case 7:
                            paymentCode = PropertyTaxPaymentCode.Correction;
                            break;
                        case 0:
                            paymentCode = PropertyTaxPaymentCode.Abatement;
                            break;
                        case 2:
                            paymentCode = PropertyTaxPaymentCode.RefundedAbatement;
                            break;
                        case 4:
                            paymentCode = PropertyTaxPaymentCode.PrePayment;
                            break;
                        case 3:
                            paymentCode = PropertyTaxPaymentCode.TaxClubPayment;
                            break;
                        case 5:
                            paymentCode = PropertyTaxPaymentCode.LienMaturityFee;
                            break;
                    }

                    if (paymentCode.HasValue)
                    {
                        if (paymentCode != PropertyTaxPaymentCode.LienMaturityFee)
                        {
                            realAccountBills =
                                realAccountBills.Where(r => r.Payments.HasPaymentType(paymentCode.Value));
                        }
                        else
                        {
                            realAccountBills =
                                realAccountBills.Where(r => r.Payments.HasPaymentType(PropertyTaxPaymentCode.LienMaturityFee) || r.Payments.HasPaymentType(PropertyTaxPaymentCode.ThirtyDayNoticeFee));
                        }
                    }
                }


                if (reportConfiguration.Options.ShowCurrentInterest || reportConfiguration.Filter.BalanceDueUsed() || reportConfiguration.Options.ExcludePaid)
                {
                    var tempList = new List<RealEstateAccountBill>();
                    foreach (var accountBill in realAccountBills)
                    {
                        if ((accountBill.TaxBill.BillSummary.GetNetOwed() != 0 && !accountBill.Bill.HasLien()) || (accountBill.Bill.HasLien() && accountBill.Bill.Lien.LienSummary.GetNetOwed() != 0))
                        {
                            if (reportConfiguration.Options.ShowCurrentInterest)
                            {
                                var summary = taxBillCalculator.CalculateBill(accountBill.TaxBill, effectiveDate);
                                if (!accountBill.Bill.HasLien())
                                {
                                    accountBill.TaxBill.BillSummary.InterestCharged += summary.Interest;
                                }
                                else
                                {
                                    accountBill.Bill.Lien.LienSummary.OriginalInterestCharged += summary.Interest;
                                    accountBill.Bill.Lien.LienSummary.AdjustedInterestCharged += summary.Interest;
                                }

                                accountBill.NewInterest = summary.Interest;
                            }
                        }

                        decimal netOwed;
                        if (!accountBill.Bill.HasLien())
                        {
                            netOwed = accountBill.Bill.BillSummary.GetNetOwed();
                        }
                        else
                        {
                            netOwed = accountBill.Bill.Lien.LienSummary.GetNetOwed();
                        }

                        if (reportConfiguration.Filter.BalanceDueUsed())
                        {
                            
                            
                            switch (reportConfiguration.Filter.BalanceDueComparisonType.Value)
                            {
                                case ComparisonOptionType.EqualTo:
                                    if (netOwed ==
                                        reportConfiguration.Filter.BalanceDueAmount &&
                                        (!reportConfiguration.Options.ExcludePaid ||
                                         netOwed != 0))
                                    {
                                        tempList.Add(accountBill);
                                    }

                                    break;
                                case ComparisonOptionType.GreaterThan:
                                    if (netOwed >
                                        reportConfiguration.Filter.BalanceDueAmount)
                                    {
                                        tempList.Add(accountBill);
                                    }

                                    break;
                                case ComparisonOptionType.LessThan:
                                    if (netOwed <
                                        reportConfiguration.Filter.BalanceDueAmount &&
                                        (!reportConfiguration.Options.ExcludePaid ||
                                         netOwed != 0))
                                    {
                                        tempList.Add(accountBill);
                                    }

                                    break;
                                case ComparisonOptionType.NotEqualTo:
                                    if (netOwed !=
                                        reportConfiguration.Filter.BalanceDueAmount &&
                                        (!reportConfiguration.Options.ExcludePaid ||
                                         netOwed != 0))
                                    {
                                        tempList.Add(accountBill);
                                    }

                                    break;
                            }
                            
                        }
                        else
                        {
                            if (!reportConfiguration.Options.ExcludePaid ||
                               netOwed != 0)
                            {
                                tempList.Add(accountBill);
                            }
                        }
                    }

                    if (reportConfiguration.Filter.BalanceDueUsed() || reportConfiguration.Options.ExcludePaid)
                    {
                        realAccountBills = tempList;
                        tempList = null;
                    }
                }
                statusListViewModel.AccountBills = realAccountBills;
            }

            statusListViewModel.ReportConfiguration = reportConfiguration;
            var statusListReport = new rptStatusLists(statusListViewModel, reportConfiguration);
            statusListReport.Show();
        }

        public void ShowAllOutstandingBalances()
        {

        }
    }
}
