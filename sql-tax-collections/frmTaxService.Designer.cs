﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxService.
	/// </summary>
	partial class frmTaxService : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPeriod;
		public fecherFoundation.FCFrame fraDataEntry;
		public fecherFoundation.FCComboBox cmbTownCode;
		public T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtDueDate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxService));
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.fraDataEntry = new fecherFoundation.FCFrame();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.lblTownCode = new fecherFoundation.FCLabel();
            this.cmbTownCode = new fecherFoundation.FCComboBox();
            this.lblDate = new fecherFoundation.FCLabel();
            this.txtDate = new Global.T2KDateBox();
            this.lblYear = new fecherFoundation.FCLabel();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.txtDueDate = new fecherFoundation.FCTextBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDataEntry)).BeginInit();
            this.fraDataEntry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 640);
            this.BottomPanel.Size = new System.Drawing.Size(554, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraDataEntry);
            this.ClientArea.Controls.Add(this.txtDueDate);
            this.ClientArea.Size = new System.Drawing.Size(554, 580);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(554, 60);
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbPeriod.Location = new System.Drawing.Point(362, 80);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(124, 40);
            this.cmbPeriod.TabIndex = 7;
            this.cmbPeriod.Text = "1";
            this.cmbPeriod.SelectedIndexChanged += new System.EventHandler(this.optPeriod_Click);
            // 
            // fraDataEntry
            // 
            this.fraDataEntry.Controls.Add(this.fcLabel1);
            this.fraDataEntry.Controls.Add(this.lblTownCode);
            this.fraDataEntry.Controls.Add(this.cmbTownCode);
            this.fraDataEntry.Controls.Add(this.lblDate);
            this.fraDataEntry.Controls.Add(this.txtDate);
            this.fraDataEntry.Controls.Add(this.lblYear);
            this.fraDataEntry.Controls.Add(this.cmbYear);
            this.fraDataEntry.Controls.Add(this.lblPeriod);
            this.fraDataEntry.Controls.Add(this.cmbPeriod);
            this.fraDataEntry.Location = new System.Drawing.Point(30, 30);
            this.fraDataEntry.Name = "fraDataEntry";
            this.fraDataEntry.Size = new System.Drawing.Size(506, 190);
            this.fraDataEntry.Text = "Enter Extract Information";
            this.fraDataEntry.UseMnemonic = false;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.BackColor = System.Drawing.Color.FromName("@window");
            this.fcLabel1.Location = new System.Drawing.Point(20, 44);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(114, 17);
            this.fcLabel1.TabIndex = 12;
            this.fcLabel1.Text = "INTEREST DATE";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTownCode
            // 
            this.lblTownCode.AutoSize = true;
            this.lblTownCode.BackColor = System.Drawing.Color.FromName("@window");
            this.lblTownCode.Location = new System.Drawing.Point(298, 44);
            this.lblTownCode.Name = "lblTownCode";
            this.lblTownCode.Size = new System.Drawing.Size(49, 17);
            this.lblTownCode.TabIndex = 2;
            this.lblTownCode.Text = "TOWN";
            this.lblTownCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTownCode.Visible = false;
            // 
            // cmbTownCode
            // 
            this.cmbTownCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTownCode.Location = new System.Drawing.Point(362, 30);
            this.cmbTownCode.Name = "cmbTownCode";
            this.cmbTownCode.Size = new System.Drawing.Size(124, 40);
            this.cmbTownCode.TabIndex = 3;
            this.cmbTownCode.Visible = false;
            this.cmbTownCode.DropDown += new System.EventHandler(this.cmbTownCode_DropDown);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.FromName("@window");
            this.lblDate.Location = new System.Drawing.Point(165, 45);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(4, 16);
            this.lblDate.TabIndex = 13;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(165, 30);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.Name = "txtDate";
            this.txtDate.TabIndex = 1;
            this.txtDate.Visible = false;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(20, 94);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(45, 17);
            this.lblYear.TabIndex = 4;
            this.lblYear.Text = "YEAR";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(165, 80);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(100, 40);
            this.cmbYear.TabIndex = 5;
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(298, 94);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(59, 17);
            this.lblPeriod.TabIndex = 6;
            this.lblPeriod.Text = "PERIOD";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDueDate
            // 
            this.txtDueDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDueDate.Location = new System.Drawing.Point(429, 3);
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(82, 40);
            this.txtDueDate.TabIndex = 2;
            this.txtDueDate.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(196, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(156, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmTaxService
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(554, 748);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmTaxService";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Tax Service Extract";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmTaxService_Load);
            this.Activated += new System.EventHandler(this.frmTaxService_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxService_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDataEntry)).EndInit();
            this.fraDataEntry.ResumeLayout(false);
            this.fraDataEntry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblPeriod;
		private System.ComponentModel.IContainer components;
		public FCLabel lblTownCode;
		private FCLabel lblYear;
		private FCButton btnProcess;
        public FCLabel fcLabel1;
    }
}
