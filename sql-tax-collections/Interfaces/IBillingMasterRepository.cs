﻿using System.Collections.Generic;
using SharedApplication;

namespace TWCL0000
{
    public interface IBillingMasterRepository : IRepository<BillingMaster>
    {
        IEnumerable<int> GetTaxYears();
    }
}
