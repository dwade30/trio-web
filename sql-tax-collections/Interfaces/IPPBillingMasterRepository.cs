﻿using SharedApplication;

namespace TWCL0000
{
    public interface IPPBillingMasterRepository: IBillingMasterRepository, IPurge
    {
    }
}
