﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections;

namespace TWCL0000
{
    public class InitializeCollectionsStaticsHandler : CommandHandler<InitializeCollectionsStatics>
    {
        public InitializeCollectionsStaticsHandler()
        {

        }
        protected override void Handle(InitializeCollectionsStatics command)
        {
            modGlobal.InitializeCollectionsStatics();
        }
    }
}