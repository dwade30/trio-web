﻿namespace TWCL0000.Notes
{
    partial class frmCollectionNote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkShowInRE = new fecherFoundation.FCCheckBox();
            this.chkPriority = new fecherFoundation.FCCheckBox();
            this.txtNote = new fecherFoundation.FCTextBox();
            this.lblRemainChars = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowInRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 288);
            this.BottomPanel.Size = new System.Drawing.Size(470, 91);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkShowInRE);
            this.ClientArea.Controls.Add(this.chkPriority);
            this.ClientArea.Controls.Add(this.txtNote);
            this.ClientArea.Controls.Add(this.lblRemainChars);
            this.ClientArea.Controls.Add(this.Label15);
            this.ClientArea.Size = new System.Drawing.Size(470, 228);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(470, 60);
            // 
            // chkShowInRE
            // 
            this.chkShowInRE.Location = new System.Drawing.Point(72, 172);
            this.chkShowInRE.Name = "chkShowInRE";
            this.chkShowInRE.Size = new System.Drawing.Size(222, 24);
            this.chkShowInRE.TabIndex = 116;
            this.chkShowInRE.Text = "Display pop-up in Real Estate?";
            // 
            // chkPriority
            // 
            this.chkPriority.Location = new System.Drawing.Point(72, 147);
            this.chkPriority.Name = "chkPriority";
            this.chkPriority.Size = new System.Drawing.Size(192, 24);
            this.chkPriority.TabIndex = 115;
            this.chkPriority.Text = "Display pop-up reminder?";
            // 
            // txtNote
            // 
            this.txtNote.AcceptsReturn = true;
            this.txtNote.BackColor = System.Drawing.SystemColors.Window;
            this.txtNote.Location = new System.Drawing.Point(21, 13);
            this.txtNote.MaxLength = 255;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(428, 85);
            this.txtNote.TabIndex = 114;
            // 
            // lblRemainChars
            // 
            this.lblRemainChars.Location = new System.Drawing.Point(422, 99);
            this.lblRemainChars.Name = "lblRemainChars";
            this.lblRemainChars.Size = new System.Drawing.Size(25, 18);
            this.lblRemainChars.TabIndex = 118;
            this.lblRemainChars.Text = "255";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(174, 99);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(176, 18);
            this.Label15.TabIndex = 117;
            this.Label15.Text = "CHARACTERS REMAINING";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(185, 21);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "OK";
            // 
            // frmCollectionNote
            // 
            this.ClientSize = new System.Drawing.Size(470, 379);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCollectionNote";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Note";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowInRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCCheckBox chkShowInRE;
        public fecherFoundation.FCCheckBox chkPriority;
        public fecherFoundation.FCTextBox txtNote;
        public fecherFoundation.FCLabel lblRemainChars;
        public fecherFoundation.FCLabel Label15;
        private fecherFoundation.FCButton cmdSave;
    }
}