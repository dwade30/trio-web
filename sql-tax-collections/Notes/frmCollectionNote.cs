﻿using System;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Notes;
using Wisej.Web;

namespace TWCL0000.Notes
{
    public partial class frmCollectionNote : BaseForm, IModalView<ICollectionsNoteViewModel>
    {
        private int charCount;
        public frmCollectionNote()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmCollectionNote_Load;
            txtNote.KeyPress += TxtNote_KeyPress;
            txtNote.TextChanged += TxtNote_TextChanged;
            cmdSave.Click += CmdSave_Click;
        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
           Save();
           Close();
        }

        private void TxtNote_TextChanged(object sender, EventArgs e)
        {
            CheckNoteLength();
        }

        private void TxtNote_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckNoteLength();
        }

        private void CheckNoteLength()
        {
            charCount = txtNote.Text.Length;
            var intRemaining = 255 - charCount;
            lblRemainChars.Text = intRemaining.ToString();
            if (intRemaining <= 0)
            {
                lblRemainChars.Text = "0";
            }
        }

        private void FrmCollectionNote_Load(object sender, EventArgs e)
        {
            ShowNote();
        }

        private void ShowNote()
        {
            chkShowInRE.Visible = ViewModel.BillType == PropertyTaxBillType.Real;
            txtNote.Text = ViewModel.Comment;
            chkShowInRE.Checked = ViewModel.ShowInRealEstate;
            chkPriority.Checked = ViewModel.DisplayPopupReminder();
        }
        public frmCollectionNote(ICollectionsNoteViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        public ICollectionsNoteViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void Save()
        {
            var comment = txtNote.Text.Trim();
            if (comment.Length > 255)
            {
                comment = comment.Left(255);
            }
            ViewModel.Comment = comment;
            ViewModel.ShowInRealEstate = chkShowInRE.Checked;
            ViewModel.Priority = chkPriority.Checked ? 1 : 0;
            ViewModel.Save();
        }
    }
}
