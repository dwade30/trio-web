﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	public partial class frmCustomize : BaseForm
	{
		public frmCustomize()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomize InstancePtr
		{
			get
			{
				return (frmCustomize)Sys.GetInstance(typeof(frmCustomize));
			}
		}

		protected frmCustomize _InstancePtr = null;

		public string auxCmbOptionsItem5 = "Tax Service Options";
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		bool boolDirty;

		private void chkDefaultAccount_Click(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkDisableAutoPmtFill_Click(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkDefaultRTDDate_Click(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkMapLot_Click(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkShowReceiptOnAcctDetail_Click(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkUseRKTaxRate_Click(object sender, System.EventArgs e)
		{
			// MAL@20070910: Added code to disable abatement interest rate when Rate Key Interest Rate is checked
			if (chkUseRKTaxRate.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtAbatementInterest.Enabled = false;
				txtAbatementInterest.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				txtAbatementInterest.Enabled = true;
				txtAbatementInterest.BackColor = System.Drawing.Color.White;
			}
		}

		private void frmCustomize_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				LoadSettings();
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmCustomize_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomize.Icon	= "frmCustomize.frx":0000";
			//frmCustomize.FillStyle	= 0;
			//frmCustomize.ScaleWidth	= 9300;
			//frmCustomize.ScaleHeight	= 8055;
			//frmCustomize.LinkTopic	= "Form2";
			//frmCustomize.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtReminderFormAdjustH.MaxLength	= 5;
			//txtReminderFormAdjustV.MaxLength	= 5;
			//txtLDNSideAdjust.MaxLength	= 5;
			//txtLienSideAdjust.MaxLength	= 5;
			//txtLDNBottomAdjust.MaxLength	= 5;
			//txtLienBottomAdjust.MaxLength	= 5;
			//txtLienTopAdjust.MaxLength	= 5;
			//txtLDNTopAdjust.MaxLength	= 5;
			//txtSigAdjust_Lien.MaxLength	= 5;
			//txtCMFAdjust.MaxLength	= 5;
			//txtLabelAdjustment.MaxLength	= 5;
			//txtDMVLabelAdjust.MaxLength	= 5;
			//txtDMHLabelAdjust.MaxLength	= 5;
			//txtTaxInfoSheetMsg.MaxLength	= 125;
			//txtLienAbate.MaxLength	= 255;
			//vsTSEmailGrid.BackColor	= "-2147483643";
			//			//vsTSEmailGrid.ForeColor	= "-2147483640";
			//vsTSEmailGrid.BorderStyle	= 1;
			//vsTSEmailGrid.FillStyle	= 0;
			//vsTSEmailGrid.Appearance	= 1;
			//vsTSEmailGrid.GridLines	= 1;
			//vsTSEmailGrid.WordWrap	= 0;
			//vsTSEmailGrid.ScrollBars	= 3;
			//vsTSEmailGrid.RightToLeft	= 0;
			//vsTSEmailGrid._cx	= 8070;
			//vsTSEmailGrid._cy	= 2014;
			//vsTSEmailGrid._ConvInfo	= 1;
			//vsTSEmailGrid.MousePointer	= 0;
			//vsTSEmailGrid.BackColorFixed	= -2147483633;
			//			//vsTSEmailGrid.ForeColorFixed	= -2147483630;
			//vsTSEmailGrid.BackColorSel	= -2147483635;
			//			//vsTSEmailGrid.ForeColorSel	= -2147483634;
			//vsTSEmailGrid.BackColorBkg	= -2147483636;
			//vsTSEmailGrid.BackColorAlternate	= -2147483643;
			//vsTSEmailGrid.GridColor	= -2147483633;
			//vsTSEmailGrid.GridColorFixed	= -2147483632;
			//vsTSEmailGrid.TreeColor	= -2147483632;
			//vsTSEmailGrid.FloodColor	= 192;
			//vsTSEmailGrid.SheetBorder	= -2147483642;
			//vsTSEmailGrid.FocusRect	= 1;
			//vsTSEmailGrid.HighLight	= 1;
			//vsTSEmailGrid.AllowSelection	= -1  'True;
			//vsTSEmailGrid.AllowBigSelection	= -1  'True;
			//vsTSEmailGrid.AllowUserResizing	= 0;
			//vsTSEmailGrid.SelectionMode	= 0;
			//vsTSEmailGrid.GridLinesFixed	= 2;
			//vsTSEmailGrid.GridLineWidth	= 1;
			//vsTSEmailGrid.RowHeightMin	= 0;
			//vsTSEmailGrid.RowHeightMax	= 0;
			//vsTSEmailGrid.ColWidthMin	= 0;
			//vsTSEmailGrid.ColWidthMax	= 0;
			//vsTSEmailGrid.ExtendLastCol	= 0   'False;
			//vsTSEmailGrid.FormatString	= "";
			//vsTSEmailGrid.ScrollTrack	= 0   'False;
			//vsTSEmailGrid.ScrollTips	= 0   'False;
			//vsTSEmailGrid.MergeCells	= 0;
			//vsTSEmailGrid.MergeCompare	= 0;
			//vsTSEmailGrid.AutoResize	= -1  'True;
			//vsTSEmailGrid.AutoSizeMode	= 0;
			//vsTSEmailGrid.AutoSearch	= 0;
			//vsTSEmailGrid.AutoSearchDelay	= 2;
			//vsTSEmailGrid.MultiTotals	= -1  'True;
			//vsTSEmailGrid.SubtotalPosition	= 1;
			//vsTSEmailGrid.OutlineBar	= 0;
			//vsTSEmailGrid.OutlineCol	= 0;
			//vsTSEmailGrid.Ellipsis	= 0;
			//vsTSEmailGrid.ExplorerBar	= 0;
			//vsTSEmailGrid.PicturesOver	= 0   'False;
			//vsTSEmailGrid.PictureType	= 0;
			//vsTSEmailGrid.TabBehavior	= 0;
			//vsTSEmailGrid.OwnerDraw	= 0;
			//vsTSEmailGrid.ShowComboButton	= -1  'True;
			//vsTSEmailGrid.TextStyle	= 0;
			//vsTSEmailGrid.TextStyleFixed	= 0;
			//vsTSEmailGrid.OleDragMode	= 0;
			//vsTSEmailGrid.OleDropMode	= 0;
			//vsTSEmailGrid.ComboSearch	= 3;
			//vsTSEmailGrid.AutoSizeMouse	= 0   'False;
			//vsTSEmailGrid.AllowUserFreezing	= 0;
			//vsTSEmailGrid.BackColorFrozen	= 0;
			//			//vsTSEmailGrid.ForeColorFrozen	= 0;
			//vsTSEmailGrid.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmCustomize.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// move the frames into position to be shown
			//fraRateInfo.Top = 0;
			//// use fraRateInfo's left property to se the other frams
			//fraAdjustments.Top = 0;
			//fraAdjustments.Left = fraRateInfo.Left;
			//fraAuditOptions.Top = 0;
			//fraAuditOptions.Left = fraRateInfo.Left;
			//fraTownInformation.Top = 0;
			//fraTownInformation.Left = fraRateInfo.Left;
			//fraPaymentOptions.Top = 0;
			//fraPaymentOptions.Left = fraRateInfo.Left;
			// kk09222014 trocl-1156  Added tool tips
			LoadToolTips();
		}

		private void frmCustomize_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{

			boolLoaded = false;
			boolDirty = false;
		}

		private void frmCustomize_Resize(object sender, System.EventArgs e)
		{
			vsTSEmailGrid.ColWidth(0, FCConvert.ToInt32(vsTSEmailGrid.WidthOriginal * 0.94));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MAL@20070910: Added to set focus back to the parent form
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			if (boolDirty)
			{
				DialogResult messageBoxResult = FCMessageBox.Show("Items have been changed on this screen.  Would you like to save before exiting?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Save");
				if (messageBoxResult == DialogResult.Yes)
				{
					if (!SaveSettings())
					{
						return;
					}
					else
					{
						Close();
					}
				}
				else if (messageBoxResult == DialogResult.No)
				{
					// keep going
					Close();
				}
				else if (messageBoxResult == DialogResult.Cancel)
				{
					// Cancel = True
					return;
				}
			}
			else
			{
				// MAL@20080729: Added exit if things have not changed
				// Tracker Reference: 14888
				Close();
			}
			boolLoaded = false;
			boolDirty = false;
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
		}

		private bool SaveSettings()
		{
			int intError = 0;
			bool SaveSettings = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strError = "";
				// this will save all of the settings for Collections
				intError = 1;
				rsSettings.Edit();
				intError = 2;
				if (cmbBasis.SelectedIndex == 0)
				{
					rsSettings.Set_Fields("Basis", 365);
					if (modGlobal.Statics.gintBasis != 365)
					{
						modGlobalFunctions.AddCYAEntry_26("CL", "Customize Screen", "Changed Interest Days in Year to 365");
					}
					modGlobal.Statics.gintBasis = 365;
					intError = 3;
				}
				else
				{
					if (modGlobal.Statics.gintBasis != 360)
					{
						modGlobalFunctions.AddCYAEntry_26("CL", "Customize Screen", "Changed Interest Days in Year to 360");
					}
					rsSettings.Set_Fields("Basis", 360);
					modGlobal.Statics.gintBasis = 360;
					intError = 4;
				}

                StaticSettings.TaxCollectionSettings.DaysInAYear = modGlobal.Statics.gintBasis;

				// PageBreakForTCBooklet
				rsSettings.Set_Fields("PageBreakForTCBooklet", FCConvert.CBool(chkExtraTCPage.CheckState == Wisej.Web.CheckState.Checked));
				// Payment Options
				rsSettings.Set_Fields("DoNotAutofillPrepayment", FCConvert.CBool(chkDoNotAutofillPrepay.CheckState == Wisej.Web.CheckState.Checked));
				rsSettings.Set_Fields("DefaultPaymentsToAuto", FCConvert.CBool(chkDefaultToAuto.CheckState == Wisej.Web.CheckState.Checked));
				modStatusPayments.Statics.gboolDefaultPaymentsToAuto = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DefaultPaymentsToAuto"));
				if (cmbAssessmentValues.SelectedIndex == 0)
				{
					rsSettings.Set_Fields("AcctDetailAssesment", "C");
                    StaticSettings.TaxCollectionSettings.AccountDetailAssessmentSetting = "C";
                }
				else if (cmbAssessmentValues.SelectedIndex == 1)
				{
					rsSettings.Set_Fields("AcctDetailAssesment", "B");
                    StaticSettings.TaxCollectionSettings.AccountDetailAssessmentSetting = "B";
                }
				else
				{
					rsSettings.Set_Fields("AcctDetailAssesment", "P");
                    StaticSettings.TaxCollectionSettings.AccountDetailAssessmentSetting = "P";
                }
				// kk09242014 trocl-1197  Enter rates as percentages and store as decimals; same as on Rate Key edits
				if (Conversion.Val(txtDiscountPercent.Text) > 0)
				{
					rsSettings.Set_Fields("DiscountPercent", FCConvert.ToDouble(txtDiscountPercent.Text) / 100.0);
                    StaticSettings.TaxCollectionSettings.DiscountRate =
                        txtDiscountPercent.Text.ToDecimalValue() / 100M;

                }
				else
				{
					rsSettings.Set_Fields("DiscountPercent", 0);
                    StaticSettings.TaxCollectionSettings.DiscountRate = 0;
                }
				intError = 5;
				if (Conversion.Val(txtPPayInterestRate.Text) > 0)
				{
					rsSettings.Set_Fields("OverPayInterestRate", FCConvert.ToDouble(txtPPayInterestRate.Text) / 100.0);
                    StaticSettings.TaxCollectionSettings.OverPaymentInterestRate =
                        txtPPayInterestRate.Text.ToDecimalValue() / 100M;
                }
				else
				{
					rsSettings.Set_Fields("OverPayInterestRate", 0);
                    StaticSettings.TaxCollectionSettings.OverPaymentInterestRate = 0;
                }
				if (Conversion.Val(txtAbatementInterest.Text) > 0)
				{
					rsSettings.Set_Fields("AbatementInterestRate", FCConvert.ToDouble(txtAbatementInterest.Text) / 100.0);
				}
				else
				{
					rsSettings.Set_Fields("AbatementInterestRate", 0);
				}
				rsSettings.Set_Fields("UseRKRateForAbate", FCConvert.CBool(chkUseRKTaxRate.CheckState == Wisej.Web.CheckState.Checked));
				modExtraModules.Statics.dblOverPayRate = FCConvert.ToDouble(Conversion.Val(rsSettings.Get_Fields_Double("OverPayInterestRate")));
				intError = 6;
				rsSettings.Set_Fields("AuditSeqReceipt", cmbSeq.SelectedIndex == 0);
				intError = 7;
				
				if (ValidateAdjustments(ref strError))
				{
					if (Conversion.Val(txtCMFAdjust.Text) == 0)
					{
						intError = 10;
						modGlobal.Statics.setCont.SaveSetting("0", "AdjustmentCMFLaser", "", "", "", "");
						modGlobal.Statics.gdblCMFAdjustment = 0;
					}
					else
					{
						intError = 11;
						modGlobal.Statics.setCont.SaveSetting(txtCMFAdjust.Text, "AdjustmentCMFLaser", "", "", "", "");
						modGlobal.Statics.gdblCMFAdjustment = Conversion.Val(txtCMFAdjust.Text);
					}
					if (Conversion.Val(txtLabelAdjustment.Text) == 0)
					{
						intError = 12;
						modGlobal.Statics.setCont.SaveSetting("0", "AdjustmentLabels", "", "", "", "");
						modGlobal.Statics.gdblLabelsAdjustment = 0;
					}
					else
					{
						intError = 13;
						modGlobal.Statics.setCont.SaveSetting(txtLabelAdjustment.Text, "AdjustmentLabels", "", "", "", "");
						modGlobal.Statics.gdblLabelsAdjustment = Conversion.Val(txtLabelAdjustment.Text);
					}

					// Lien Top Adjustment
					if (Conversion.Val(txtLienTopAdjust.Text) == 0)
					{
						intError = 310;
						modGlobal.Statics.setCont.SaveSetting("0", "gdblLienAdjustmentTop", "", "", "", "");
						modGlobal.Statics.gdblLienAdjustmentTop = 0;
					}
					else
					{
						intError = 311;
						modGlobal.Statics.setCont.SaveSetting(txtLienTopAdjust.Text, "gdblLienAdjustmentTop", "", "", "", "");
						modGlobal.Statics.gdblLienAdjustmentTop = Conversion.Val(txtLienTopAdjust.Text);
					}
					// Lien Bottom Adjustment - kk04212014 trocl-1156  Add option to adjust bottom margin
					if (Conversion.Val(txtLienBottomAdjust.Text) == 0)
					{
						intError = 312;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentBottom", "0");
						modGlobal.Statics.setCont.SaveSetting("0", "gdblLienAdjustmentBottom", "", "", "", "");
						modGlobal.Statics.gdblLienAdjustmentBottom = 0;
					}
					else
					{
						intError = 313;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentBottom", txtLienBottomAdjust.Text);
						modGlobal.Statics.setCont.SaveSetting(txtLienBottomAdjust.Text, "gdblLienAdjustmentBottom", "", "", "", "");
						modGlobal.Statics.gdblLienAdjustmentBottom = Conversion.Val(txtLienBottomAdjust.Text);
					}
					// Lien Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
					if (Conversion.Val(txtLienSideAdjust.Text) == 0)
					{
						intError = 314;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentSide", "0");
						modGlobal.Statics.setCont.SaveSetting("0", "gdblLienAdjustmentSide", "", "", "", "");
						modGlobal.Statics.gdblLienAdjustmentSide = 0;
					}
					else
					{
						intError = 315;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentSide", txtLienSideAdjust.Text);
						modGlobal.Statics.setCont.SaveSetting(txtLienSideAdjust.Text, "gdblLienAdjustmentSide", "", "", "", "");
						modGlobal.Statics.gdblLienAdjustmentSide = Conversion.Val(txtLienSideAdjust.Text);
					}
					// kk02082016 trout-1197  Specs for registries changed to stright margins - removed the top right box stuff
					// Lien Top Right Box Size - kk08072015 trocl-1250  Add option to define the box at the top right of form
					// Lien Discharge Top Adjustment
					if (Conversion.Val(txtLDNTopAdjust.Text) == 0)
					{
						intError = 410;
						modGlobal.Statics.setCont.SaveSetting("0", "gdblLDNAdjustmentTop", "", "", "", "");
						modGlobal.Statics.gdblLDNAdjustmentTop = 0;
					}
					else
					{
						intError = 411;
						modGlobal.Statics.setCont.SaveSetting(txtLDNTopAdjust.Text, "gdblLDNAdjustmentTop", "", "", "", "");
						modGlobal.Statics.gdblLDNAdjustmentTop = Conversion.Val(txtLDNTopAdjust.Text);
					}
					// Lien Discharge Bottom Adjustment - kk04212014 trocl-1156  Add option to adjust bottom margin
					if (Conversion.Val(txtLDNBottomAdjust.Text) == 0)
					{
						intError = 412;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentBottom", "0");
						modGlobal.Statics.setCont.SaveSetting("0", "gdblLDNAdjustmentBottom", "", "", "", "");
						modGlobal.Statics.gdblLDNAdjustmentBottom = 0;
					}
					else
					{
						intError = 413;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentBottom", txtLDNBottomAdjust.Text);
						modGlobal.Statics.setCont.SaveSetting(txtLDNBottomAdjust.Text, "gdblLDNAdjustmentBottom", "", "", "", "");
						modGlobal.Statics.gdblLDNAdjustmentBottom = Conversion.Val(txtLDNBottomAdjust.Text);
					}
					// Lien Discharge Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
					if (Conversion.Val(txtLDNSideAdjust.Text) == 0)
					{
						intError = 314;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentSide", "0");
						modGlobal.Statics.setCont.SaveSetting("0", "gdblLDNAdjustmentSide", "", "", "", "");
						modGlobal.Statics.gdblLDNAdjustmentSide = 0;
					}
					else
					{
						intError = 315;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentSide", txtLDNSideAdjust.Text);
						modGlobal.Statics.setCont.SaveSetting(txtLDNSideAdjust.Text, "gdblLDNAdjustmentSide", "", "", "", "");
						modGlobal.Statics.gdblLDNAdjustmentSide = Conversion.Val(txtLDNSideAdjust.Text);
					}
					// Lien Signature Adjustment
					if (Conversion.Val(txtSigAdjust_Lien.Text) == 0)
					{
						intError = 10;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "AdjustmentLienSignature", "0");
						modGlobal.Statics.setCont.SaveSetting("0", "AdjustmentLienSignature", "", "", "", "");
						modGlobal.Statics.gdblLienSigAdjust = 0;
					}
					else
					{
						intError = 11;
						//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "AdjustmentLienSignature", txtSigAdjust_Lien.Text);
						modGlobal.Statics.setCont.SaveSetting(txtSigAdjust_Lien.Text, "AdjustmentLienSignature", "", "", "", "");
						modGlobal.Statics.gdblLienSigAdjust = Conversion.Val(txtSigAdjust_Lien.Text);
					}
				}
				else
				{
					FCMessageBox.Show("There is an error in your Adjustments settings." + "\r\n" + strError + "\r\n" + "Please review your settings and try again.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "Error in Adjustments");
					rsSettings.CancelUpdate();
					return SaveSettings;
				}
				if (chkDefaultAccount.CheckState == Wisej.Web.CheckState.Checked)
				{
					intError = 82;
					rsSettings.Set_Fields("ShowLastCLAccountInCR", true);
                    StaticSettings.TaxCollectionSettings.ShowLastAccountInCR = true;
                }
				else
				{
					intError = 83;
					rsSettings.Set_Fields("ShowLastCLAccountInCR", false);
                    StaticSettings.TaxCollectionSettings.ShowLastAccountInCR =false;
                }
				modGlobal.Statics.gboolShowLastCLAccountInCR = FCConvert.CBool(rsSettings.Get_Fields_Boolean("ShowLastCLAccountInCR"));
				if (FCConvert.CBool(cmbReceiptSize.SelectedIndex != 0))
				{
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLNarrowReceipt", "TRUE");
					modGlobal.Statics.setCont.SaveSetting("TRUE", "CLNarrowReceipt", "", "", "", "");
				}
				else
				{
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLNarrowReceipt", "FALSE");
					modGlobal.Statics.setCont.SaveSetting("FALSE", "CLNarrowReceipt", "", "", "", "");
				}
				// Adjustements for Reminder Notice Forms
				// horizontal
				if (Conversion.Val(txtReminderFormAdjustH.Text) == 0)
				{
					intError = 102;
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderFormAdjustH", "0");
					modGlobal.Statics.setCont.SaveSetting("0", "ReminderFormAdjustH", "", "", "", "");
					modGlobal.Statics.gdblReminderFormAdjustH = 0;
				}
				else
				{
					intError = 112;
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderFormAdjustH", txtReminderFormAdjustH.Text);
					modGlobal.Statics.setCont.SaveSetting(txtReminderFormAdjustH.Text, "ReminderFormAdjustH", "", "", "", "");
					modGlobal.Statics.gdblReminderFormAdjustH = Conversion.Val(txtReminderFormAdjustH.Text);
				}
				// vertical
				if (Conversion.Val(txtReminderFormAdjustV.Text) == 0)
				{
					intError = 1021;
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderFormAdjustV", "0");
					modGlobal.Statics.setCont.SaveSetting("0", "ReminderFormAdjustV", "", "", "", "");
					modGlobal.Statics.gdblReminderFormAdjustV = 0;
				}
				else
				{
					intError = 1121;
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderFormAdjustV", txtReminderFormAdjustV.Text);
					modGlobal.Statics.setCont.SaveSetting(txtReminderFormAdjustV.Text, "ReminderFormAdjustV", "", "", "", "");
					modGlobal.Statics.gdblReminderFormAdjustV = Conversion.Val(txtReminderFormAdjustV.Text);
				}
				// gboolShowMapLotInCLAudit
				rsSettings.Set_Fields("ShowMapLotInCLAudit", FCConvert.CBool(chkMapLot.CheckState == Wisej.Web.CheckState.Checked));
				modCLCalculations.Statics.gboolShowMapLotInCLAudit = FCConvert.CBool(rsSettings.Get_Fields_Boolean("ShowMapLotInCLAudit"));
				// ldn abatement text
				rsSettings.Set_Fields("LienAbateText", txtLienAbate.Text);
				intError = 100;
				rsSettings.Set_Fields("ShowTownSeal30DN", FCConvert.CBool(chkTownSeal30DN.CheckState == Wisej.Web.CheckState.Checked));
				modCLCalculations.Statics.gboolCLUseTownSeal30DN = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSeal30DN"));
				intError = 101;
				rsSettings.Set_Fields("ShowTownSealLien", FCConvert.CBool(chkTownSealLien.CheckState == Wisej.Web.CheckState.Checked));
				modCLCalculations.Statics.gboolCLUseTownSealLien = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSealLien"));
				intError = 102;
				rsSettings.Set_Fields("ShowTownSealLienMat", FCConvert.CBool(chkTownSealLienMaturity.CheckState == Wisej.Web.CheckState.Checked));
				modCLCalculations.Statics.gboolCLUseTownSealLienMat = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSealLienMat"));
				intError = 103;
				rsSettings.Set_Fields("ShowTownSealLienDischarge", FCConvert.CBool(chkTownSealLienDischarge.CheckState == Wisej.Web.CheckState.Checked));
				modCLCalculations.Statics.gboolCLUseTownSealLienDischarge = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSealLienDischarge"));
				intError = 104;
				rsSettings.Set_Fields("DefaultRTDToAutoChange", FCConvert.CBool(chkDefaultRTDDate.CheckState == Wisej.Web.CheckState.Checked));
				modCLCalculations.Statics.gboolDefaultRTDToAutoChange = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DefaultRTDToAutoChange"));
				// kk03162015 trocrs-36  Add option to disable autofill payment amount on Insert/dblclick
				rsSettings.Set_Fields("DisableAutoPmtFill", FCConvert.CBool(chkDisableAutoPmtFill.CheckState == Wisej.Web.CheckState.Checked));
                StaticSettings.TaxCollectionSettings.DisableAutoPaymentFill = chkDisableAutoPmtFill.Checked;
                intError = 105;
				if (cmbTCOrder.SelectedIndex == 0)
				{
					// Name
					rsSettings.Set_Fields("TCReport", 0);
				}
				else
				{
					// Account
					rsSettings.Set_Fields("TCReport", 1);
				}
				if (cmbLoadbackOrder.SelectedIndex == 0)
				{
					rsSettings.Set_Fields("LBOrder", 0);
				}
				else if (cmbLoadbackOrder.SelectedIndex == 1)
				{
					rsSettings.Set_Fields("LBOrder", 1);
				}
				else if (cmbLoadbackOrder.SelectedIndex == 2)
				{
					rsSettings.Set_Fields("LBOrder", 2);
				}
				else if (cmbLoadbackOrder.SelectedIndex == 3)
				{
					rsSettings.Set_Fields("LBOrder", 3);
				}
				rsSettings.Set_Fields("TaxInfoSheetMessage", txtTaxInfoSheetMsg.Text);
				rsSettings.Set_Fields("ShowTSReminder", FCConvert.CBool(chkTSReminders.CheckState == Wisej.Web.CheckState.Checked));
				rsSettings.Set_Fields("ReceiptNumonAcctDetail", FCConvert.CBool(chkShowReceiptOnAcctDetail.CheckState == Wisej.Web.CheckState.Checked));
                modGlobalConstants.Statics.gblnShowReceiptNumonDetail = FCConvert.CBool(chkShowReceiptOnAcctDetail.CheckState == Wisej.Web.CheckState.Checked);
                StaticSettings.TaxCollectionSettings.ShowReceiptNumberOnDetail =
                    chkShowReceiptOnAcctDetail.CheckState == CheckState.Checked;
				StoreTaxServiceEMail();
				intError = 110;
                
				if (rsSettings.Update(true))
				{
					SaveSettings = true;
					FCMessageBox.Show("Save Successful.", MsgBoxStyle.Information, "Save Complete");
					// Please restart Tax Collections for the changes to take effect.
					boolDirty = false;
                    //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                    App.MainForm.ReloadNavigationMenu();
                }
				else
				{
					SaveSettings = false;
					FCMessageBox.Show("Save not successful.", MsgBoxStyle.Exclamation, "Not Successful - " + FCConvert.ToString(intError));
				}
				return SaveSettings;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Settings - " + FCConvert.ToString(intError));
			}
			return SaveSettings;
		}

		private string SetSigFile(ref string strPath)
		{
			string SetSigFile = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will accept a filepath, validate it, copy the file to the
				// data directory then return the new path
				if (Strings.Trim(strPath) != "")
				{
					if (File.Exists(strPath))
					{
						File.Copy(strPath, FCFileSystem.Statics.UserDataFolder + "\\TWSIG." + Strings.Right(strPath, 3), true);
						// this will copy the file to the data directory and change the name to TWSIG. and use the extention from the original file
						SetSigFile = FCFileSystem.Statics.UserDataFolder + "\\TWSIG." + Strings.Right(strPath, 3);
					}
					else
					{
						FCMessageBox.Show("The path specified is not valid.  Signature file not saved." + "\r\n" + strPath, MsgBoxStyle.Exclamation, "Signature File Not Saved");
					}
				}
				return SetSigFile;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				SetSigFile = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Settings");
			}
			return SetSigFile;
		}

		private void LoadSettings()
		{
			try
			{
				string strTemp = "";
				rsSettings.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
				if (rsSettings.EndOfFile() != true && rsSettings.BeginningOfFile() != true)
				{
				}
				else
				{
					rsSettings.AddNew();
					// add defaults....
					rsSettings.Update(true);
				}
				if (Conversion.Val(rsSettings.Get_Fields_Int32("Basis")) > 0)
				{
					if (((rsSettings.Get_Fields_Int32("Basis"))) == 365)
					{
						cmbBasis.SelectedIndex = 0;
					}
					else
					{
						cmbBasis.SelectedIndex = 1;
					}
				}
				else
				{
					cmbBasis.SelectedIndex = 0;
					// default value
				}
				// PageBreakForTCBooklet
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("PageBreakForTCBooklet")))
				{
					chkExtraTCPage.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkExtraTCPage.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// Payment Options
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DoNotAutofillPrepayment")))
				{
					chkDoNotAutofillPrepay.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDoNotAutofillPrepay.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DefaultPaymentsToAuto")))
				{
					chkDefaultToAuto.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDefaultToAuto.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				//FC:FINAL:CHN: Fix variable type to correct comparison.
				// object VBtoVar = rsSettings.Get_Fields_String("AcctDetailAssesment");
				string VBtoVar = rsSettings.Get_Fields_String("AcctDetailAssesment");
				if (VBtoVar == "C")
				{
					// Current Assessment
					cmbAssessmentValues.SelectedIndex = 0;
				}
				else if (VBtoVar == "B")
				{
					// Last Billed Assessment
					cmbAssessmentValues.SelectedIndex = 1;
				}
				else
				{
					// Prompt
					cmbAssessmentValues.SelectedIndex = 2;
				}
				// kk09242014 trocl-1197  Enter rates as percentages and store as decimals; same as on Rate Key edits
				// TODO Get_Fields: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
				if (Conversion.Val(rsSettings.Get_Fields("DiscountPercent")) > 0)
				{
					// this is the percentage of discount a town offers when a person pays in full early
					// TODO Get_Fields: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
					txtDiscountPercent.Text = Strings.Format((Conversion.Val(rsSettings.Get_Fields("DiscountPercent")) * 100.0), "0.00");
					// trocl-1197  (rsSettings.Get_Fields("DiscountPercent") / 100)
				}
				else
				{
					txtDiscountPercent.Text = "0.00";
				}
				if (Conversion.Val(rsSettings.Get_Fields_Double("OverPayInterestRate")) > 0)
				{
					// this is the amount of interest paid to a customer that has over paid and has not gotten a refund yet
					txtPPayInterestRate.Text = Strings.Format((Conversion.Val(rsSettings.Get_Fields_Double("OverPayInterestRate")) * 100.0), "0.00");
					// trocl-1197  rsSettings.Get_Fields("OverPayInterestRate")
				}
				else
				{
					txtPPayInterestRate.Text = "0.00";
				}
				if (Conversion.Val(rsSettings.Get_Fields_Double("AbatementInterestRate")) > 0)
				{
					// this is the percentage of abatements when having to pay the customer back
					txtAbatementInterest.Text = Strings.Format((Conversion.Val(rsSettings.Get_Fields_Double("AbatementInterestRate")) * 100.0), "0.00");
					// trocl-1197  (rsSettings.Get_Fields("AbatementInterestRate") / 100)
				}
				else
				{
					txtAbatementInterest.Text = "0.00";
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("UseRKRateForAbate")))
				{
					chkUseRKTaxRate.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkUseRKTaxRate.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AuditSeqReceipt")))
				{
					cmbSeq.SelectedIndex = 0;
				}
				else
				{
					cmbSeq.SelectedIndex = 1;
				}
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "AdjustmentCMFLaser", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("AdjustmentCMFLaser", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtCMFAdjust.Text = "0";
				}
				else
				{
					// MAL@20080715: Removed formatting to allow for decimals
					// Tracker Reference: 14279
					// txtCMFAdjust.Text = Format(CDbl(strTemp), "#0")
					txtCMFAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "AdjustmentLabels", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("AdjustmentLabels", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLabelAdjustment.Text = "0";
				}
				else
				{
					// txtLabelAdjustment.Text = Format(CDbl(strTemp), "#0")
					txtLabelAdjustment.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}

				// Lien Top Adjustment
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentTop", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("gdblLienAdjustmentTop", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLienTopAdjust.Text = "0";
				}
				else
				{
					// txtLienTopAdjust.Text = Format(CDbl(strTemp), "#0")
					txtLienTopAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Bottom Adjustment - kk04212014 trocl-1156  Add option to adjust bottom margin
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentBottom", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("gdblLienAdjustmentBottom", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLienBottomAdjust.Text = "0";
				}
				else
				{
					txtLienBottomAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLienAdjustmentSide", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("gdblLienAdjustmentSide", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLienSideAdjust.Text = "0";
				}
				else
				{
					txtLienSideAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// kk02082016 trout-1197  Specs for registries changed to stright margins - removed the top right box stuff
				// Lien Top Right Box Size - kk08072015 trocl-1250  Add option to define the box at the top right of form
				// Lien Discharge Top Adjustment
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentTop", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("gdblLDNAdjustmentTop", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLDNTopAdjust.Text = "0";
				}
				else
				{
					// txtLDNTopAdjust.Text = Format(CDbl(strTemp), "#0")
					txtLDNTopAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Discharge Bottom Adjustment - kk04212014 trocl-1156  Add option to adjust bottom margin
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentBottom", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("gdblLDNAdjustmentBottom", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLDNBottomAdjust.Text = "0";
				}
				else
				{
					txtLDNBottomAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Discharge Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "gdblLDNAdjustmentSide", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("gdblLDNAdjustmentSide", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtLDNSideAdjust.Text = "0";
				}
				else
				{
					txtLDNSideAdjust.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// horizontal
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderFormAdjustH", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderFormAdjustH", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtReminderFormAdjustH.Text = "0";
				}
				else
				{
					// txtReminderFormAdjustH.Text = Format(CDbl(strTemp), "#0")
					txtReminderFormAdjustH.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Vertical
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderFormAdjustV", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderFormAdjustV", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtReminderFormAdjustV.Text = "0";
				}
				else
				{
					// txtReminderFormAdjustV.Text = Format(CDbl(strTemp), "#0")
					txtReminderFormAdjustV.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// MAL@20080616: Add new signature line adjustments
				// Tracker Reference: 14279
				// Lien Signature
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "AdjustmentLienSignature", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("AdjustmentLienSignature", "", "", "", "");
				if (Conversion.Val(strTemp) == 0)
				{
					txtSigAdjust_Lien.Text = "0";
				}
				else
				{
					txtSigAdjust_Lien.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// gboolShowMapLotInCLAudit
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowMapLotInCLAudit")))
				{
					chkMapLot.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkMapLot.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtLienAbate.Text = FCConvert.ToString(rsSettings.Get_Fields_String("LienAbateText"));
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSeal30DN")))
				{
					chkTownSeal30DN.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTownSeal30DN.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSealLien")))
				{
					chkTownSealLien.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTownSealLien.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSealLienMat")))
				{
					chkTownSealLienMaturity.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTownSealLienMaturity.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTownSealLienDischarge")))
				{
					chkTownSealLienDischarge.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTownSealLienDischarge.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				cmbLoadbackOrder.SelectedIndex = rsSettings.Get_Fields_Int32("LBOrder");
				cmbTCOrder.SelectedIndex = rsSettings.Get_Fields_Int32("TCReport");
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DefaultRTDToAutoChange")))
				{
					chkDefaultRTDDate.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDefaultRTDDate.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// kk03162015 trocrs-36  Add option to disable autofill payment amount on Insert/dblclick
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DisableAutoPmtFill")))
				{
					chkDisableAutoPmtFill.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDisableAutoPmtFill.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// MAL@20080130: Tax Info Message
				// Call Reference: 111333
				txtTaxInfoSheetMsg.Text = FCConvert.ToString(rsSettings.Get_Fields_String("TaxInfoSheetMessage"));
				// cash receipting default
				if (modGlobalConstants.Statics.gboolCR)
				{
					fraDefaultAccount.Visible = true;
					cmbReceiptSize.Visible = false;
					fcLabel3.Visible = false;
					if (FCConvert.CBool(rsSettings.Get_Fields_Boolean("ShowLastCLAccountInCR")))
					{
						chkDefaultAccount.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDefaultAccount.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					// hide the CR frame and show the CL frame
					fraDefaultAccount.Visible = false;
					cmbReceiptSize.Visible = true;
					// get the default receipt length
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLNarrowReceipt", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("CLNarrowReceipt", "", "", "", "");
					if (Strings.Trim(Strings.UCase(strTemp)) != "TRUE")
					{
						cmbReceiptSize.SelectedIndex = 0;
					}
					else
					{
						cmbReceiptSize.SelectedIndex = 1;
					}
				}
				// MAL@20080708: Add support for new TS Reminder options
				// Tracker Reference: 13727
				if (modGlobalConstants.Statics.gboolTS)
				{
					if (cmbOptions.Items[5].ToString() != auxCmbOptionsItem5)
					{
						cmbOptions.Items.Insert(5, auxCmbOptionsItem5);
					}
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTSReminder")) == true)
					{
						chkTSReminders.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTSReminders.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					cmbOptions.Items.RemoveAt(5);
					chkTSReminders.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// kk01182017 trocl-1326  Load tax service emails stored in Collections table
				LoadTaxServiceEMail();
				// MAL@20080718: Add option for showing receipt number on Acct Detail report
				// Tracker Reference: 11810
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ReceiptNumonAcctDetail")) == true)
				{
					chkShowReceiptOnAcctDetail.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkShowReceiptOnAcctDetail.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Default Information");
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveSettings())
			{
				Close();
			}
		}

		private void optBasis_Click(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optBasis_Click(object sender, System.EventArgs e)
		{
			int index = cmbBasis.SelectedIndex;
			optBasis_Click(index, sender, e);
		}

		private void optOptions_Click(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// Rate Information
						fraRateInfo.Visible = true;
						fraAdjustments.Visible = false;
						fraAuditOptions.Visible = false;
						fraTownInformation.Visible = false;
						fraPaymentOptions.Visible = false;
						fraTaxService.Visible = false;
						fraOtherOptions.Visible = false;
						break;
					}
				case 1:
					{
						// Printing Adjustments
						fraRateInfo.Visible = false;
						fraAdjustments.Visible = true;
						fraAuditOptions.Visible = false;
						fraTownInformation.Visible = false;
						fraPaymentOptions.Visible = false;
						fraTaxService.Visible = false;
						fraOtherOptions.Visible = false;
						break;
					}
				case 2:
					{
						// Audit and Receipt Options
						fraRateInfo.Visible = false;
						fraAdjustments.Visible = false;
						fraAuditOptions.Visible = true;
						fraTownInformation.Visible = false;
						fraPaymentOptions.Visible = false;
						fraTaxService.Visible = false;
						fraOtherOptions.Visible = false;
						break;
					}
				case 3:
					{
						// Town Information
						fraRateInfo.Visible = false;
						fraAdjustments.Visible = false;
						fraAuditOptions.Visible = false;
						fraTownInformation.Visible = true;
						fraPaymentOptions.Visible = false;
						fraTaxService.Visible = false;
						fraOtherOptions.Visible = false;
						break;
					}
				case 4:
					{
						// Payment Options
						fraRateInfo.Visible = false;
						fraAdjustments.Visible = false;
						fraAuditOptions.Visible = false;
						fraTownInformation.Visible = false;
						fraPaymentOptions.Visible = true;
						fraTaxService.Visible = false;
						fraOtherOptions.Visible = false;
						// MAL@20080708: Add TS Reminder Options
						// Tracker Reference: 13727
						break;
					}
				case 5:
					{
						if (cmbOptions.Items[5].ToString() != auxCmbOptionsItem5)
						{
							goto case 6;
						}
						else
						{
							// Tax Service Options
							fraRateInfo.Visible = false;
							fraAdjustments.Visible = false;
							fraAuditOptions.Visible = false;
							fraTownInformation.Visible = false;
							fraPaymentOptions.Visible = false;
							fraTaxService.Visible = true;
							fraOtherOptions.Visible = false;
							// MAL@20080718
							break;
						}
					}
				case 6:
					{
						// Other Options
						fraRateInfo.Visible = false;
						fraAdjustments.Visible = false;
						fraAuditOptions.Visible = false;
						fraTownInformation.Visible = false;
						fraPaymentOptions.Visible = false;
						fraTaxService.Visible = false;
						fraOtherOptions.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optOptions_Click(object sender, System.EventArgs e)
		{
			int index = cmbOptions.SelectedIndex;
			optOptions_Click(index, sender, e);
		}

		private void optReceiptSize_Click(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optReceiptSize_Click(object sender, System.EventArgs e)
		{
			int index = cmbReceiptSize.SelectedIndex;
			optReceiptSize_Click(index, sender, e);
		}

		private void optSeq_Click(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optSeq_Click(object sender, System.EventArgs e)
		{
			int index = cmbSeq.SelectedIndex;
			optSeq_Click(index, sender, e);
		}

		private void txtCMFAdjust_Enter(object sender, System.EventArgs e)
		{
			txtCMFAdjust.SelectionStart = 0;
			txtCMFAdjust.SelectionLength = txtCMFAdjust.Text.Length;
		}

		private void txtCMFAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtCMFAdjust_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// MAL@20080610: Changed maximum allowed adjustments
			// Tracker Reference: ?
			// If Val(.Text) >= 4 Then
			// .Text = "4"
			// End If
			// If Val(.Text) <= -4 Then
			// .Text = "-4"
			// End If
			if (Conversion.Val(txtCMFAdjust.Text) >= 10)
			{
				txtCMFAdjust.Text = "10";
			}
			if (Conversion.Val(txtCMFAdjust.Text) <= 0)
			{
				txtCMFAdjust.Text = "0";
			}
			boolDirty = true;
		}

		private void txtAbatementInterest_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtAbatementInterest_Enter(object sender, System.EventArgs e)
		{
			txtAbatementInterest.SelectionStart = 0;
			txtAbatementInterest.SelectionLength = txtAbatementInterest.Text.Length;
		}

		private void txtAbatementInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAbatementInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			double dblTempRate;
			dblTempRate = FCConvert.ToDouble(txtAbatementInterest.Text);
			if (dblTempRate > 100.0 || dblTempRate < 0.0)
			{
				FCMessageBox.Show("Abatement Interest Rate must be between 0 and 100%.", MsgBoxStyle.Information, "Invalid Rate");
				e.Cancel = true;
			}
			else if (dblTempRate < 1.0)
			{
				if (FCMessageBox.Show("Abatement Interest Rate is less than 1%. Is this correct?", MsgBoxStyle.YesNo, "Confirm Rate") == DialogResult.No)
				{
					e.Cancel = true;
				}
			}
		}

		private void txtDiscountPercent_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDiscountPercent_Enter(object sender, System.EventArgs e)
		{
			txtDiscountPercent.SelectionStart = 0;
			txtDiscountPercent.SelectionLength = txtDiscountPercent.Text.Length;
		}

		private void txtDiscountPercent_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDiscountPercent_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			double dblTempRate;
			dblTempRate = FCConvert.ToDouble(txtDiscountPercent.Text);
			if (dblTempRate > 100.0 || dblTempRate < 0.0)
			{
				FCMessageBox.Show("Discount must be between 0 and 100%.", MsgBoxStyle.Information, "Invalid Discount");
				e.Cancel = true;
			}
			else if (dblTempRate < 1.0)
			{
				if (FCMessageBox.Show("Discount is less than 1%. Is this correct?", MsgBoxStyle.YesNo, "Confirm Discount") == DialogResult.No)
				{
					e.Cancel = true;
				}
			}
		}


		private void txtLabelAdjustment_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLabelAdjustment_Enter(object sender, System.EventArgs e)
		{
			txtLabelAdjustment.SelectionStart = 0;
			txtLabelAdjustment.SelectionLength = txtLabelAdjustment.Text.Length;
		}

		private void txtLabelAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtLabelAdjustment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtLabelAdjustment.Text) >= 4)
			{
				txtLabelAdjustment.Text = "4.00";
			}
			if (Conversion.Val(txtLabelAdjustment.Text) <= 0)
			{
				txtLabelAdjustment.Text = "0.00";
			}
		}

		private void txtLDNBottomAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLDNBottomAdjust_Enter(object sender, System.EventArgs e)
		{
			txtLDNBottomAdjust.SelectionStart = 0;
			txtLDNBottomAdjust.SelectionLength = txtLDNBottomAdjust.Text.Length;
		}

		private void txtLDNBottomAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtLDNSideAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLDNSideAdjust_Enter(object sender, System.EventArgs e)
		{
			txtLDNSideAdjust.SelectionStart = 0;
			txtLDNSideAdjust.SelectionLength = txtLDNSideAdjust.Text.Length;
		}

		private void txtLDNSideAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtLDNTopAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLDNTopAdjust_Enter(object sender, System.EventArgs e)
		{
			txtLDNTopAdjust.SelectionStart = 0;
			txtLDNTopAdjust.SelectionLength = txtLDNTopAdjust.Text.Length;
		}

		private void txtLDNTopAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtLienBottomAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLienBottomAdjust_Enter(object sender, System.EventArgs e)
		{
			txtLienBottomAdjust.SelectionStart = 0;
			txtLienBottomAdjust.SelectionLength = txtLienBottomAdjust.Text.Length;
		}

		private void txtLienBottomAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtLienSideAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLienSideAdjust_Enter(object sender, System.EventArgs e)
		{
			txtLienSideAdjust.SelectionStart = 0;
			txtLienSideAdjust.SelectionLength = txtLienSideAdjust.Text.Length;
		}

		private void txtLienSideAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtLienTopAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLienTopAdjust_Enter(object sender, System.EventArgs e)
		{
			txtLienTopAdjust.SelectionStart = 0;
			txtLienTopAdjust.SelectionLength = txtLienTopAdjust.Text.Length;
		}

		private void txtLienTopAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtPPayInterestRate_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtPPayInterestRate_Enter(object sender, System.EventArgs e)
		{
			txtPPayInterestRate.SelectionStart = 0;
			txtPPayInterestRate.SelectionLength = txtPPayInterestRate.Text.Length;
		}

		private void txtPPayInterestRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPPayInterestRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			double dblTempRate;
			dblTempRate = FCConvert.ToDouble(txtPPayInterestRate.Text);
			if (dblTempRate > 100.0 || dblTempRate < 0.0)
			{
				FCMessageBox.Show("Prepayment Interest Rate must be between 0 and 100%.", MsgBoxStyle.Information, "Invalid Rate");
				e.Cancel = true;
			}
			else if (dblTempRate < 1.0)
			{
				if (FCMessageBox.Show("Prepayment Interest Rate is less than 1%. Is this correct?", MsgBoxStyle.YesNo, "Confirm Rate") == DialogResult.No)
				{
					e.Cancel = true;
				}
			}
		}

		private void txtTreasSig_Change()
		{
			boolDirty = true;
		}

		private bool ValidateAdjustments(ref string strError)
		{
			bool ValidateAdjustments = false;
			// kk08072015 trocl-1250  Rewrite this so it's easier to follow and reports more than 1 issue at a time.
			bool blnOK;
			blnOK = true;
			// Optimistic
			strError = "";
			// CMF Laser
			if (FCConvert.ToDouble(txtCMFAdjust.Text) > 10 || FCConvert.ToDouble(txtCMFAdjust.Text) < 0)
			{
				blnOK = false;
				strError += "Certified Mail Form Adjustment must be between 0 and 10" + "\r\n";
			}
			// Label Laser
			if (FCConvert.ToDouble(txtLabelAdjustment.Text) > 4 || FCConvert.ToDouble(txtLabelAdjustment.Text) < 0)
			{
				blnOK = false;
				strError += "Laser Label Vertical Adjustment must be between 0 and 4" + "\r\n";
			}
		
			// Lien Top - Default margin is 1.5" so allow -1.5 to 4" adjustment          'kk02082016 trout-1197  Change def to 1.5"
			if (Conversion.Val(txtLienTopAdjust.Text) > 4 || Conversion.Val(txtLienTopAdjust.Text) < -1.5)
			{
				// kk08062015 trocl-1250    >= 0
				blnOK = false;
				strError += "Lien Notice Top Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// Lien Bottom - Default margin is 1.5" so allow -1.5 to 4" adjustment       'kk05082014 trocl-1156  Add bottom adj   'kk02082016 trout-1197  Change def to 1.5"
			if (Conversion.Val(txtLienBottomAdjust.Text) > 4 || Conversion.Val(txtLienBottomAdjust.Text) < -1.5)
			{
				// kk08062015 trocl-1250    >= 0
				blnOK = false;
				strError += "Lien Notice Bottom Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// kk02082016 trout-1197  New specs for registry filings. Removed the top right box.  Added side margin adjustments
			// Lien Side - Default margin is 0.75" so allow -0.75 to 1" adjustment; Applies to left and right sides
			if (Conversion.Val(txtLienSideAdjust.Text) > 1 || Conversion.Val(txtLienSideAdjust.Text) < -0.75)
			{
				blnOK = false;
				strError += "Lien Notice Side Adjustment must be between -0.75 and 1 inches" + "\r\n";
			}
			// Lien Discharge Top - Default margin is 1.5" so allow -1.5 to 4" adjustment            'kk02082016 trout-1197  Change to -1.5
			if (Conversion.Val(txtLDNTopAdjust.Text) > 4 || Conversion.Val(txtLDNTopAdjust.Text) < -1.5)
			{
				// kk08062015 trocl-1250    >= 0
				blnOK = false;
				strError += "Lien Discharge Notice Top Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// Lien Discharge Bottom - Default margin is 1.5" so allow -1.5 to 4" adjustment     'kk05082014 trocl-1156  Add bottom adj   'kk02082016 trout-1197  Change def to 1.5"
			if (Conversion.Val(txtLDNBottomAdjust.Text) > 4 || Conversion.Val(txtLDNBottomAdjust.Text) < -1.5)
			{
				// kk08062015 trocl-1250    >= 0
				blnOK = false;
				strError += "Lien Discharge Notice Bottom Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// Lien Side - Default margin is 0.75" so allow -0.75 to 1" adjustment; Applies to left and right sides   'kk02082016 trout-1197  Added side margin adjustments
			if (Conversion.Val(txtLDNSideAdjust.Text) > 1 || Conversion.Val(txtLDNSideAdjust.Text) < -0.75)
			{
				blnOK = false;
				strError += "Lien Discharge Notice Side Adjustment must be between -0.75 and 1 inches" + "\r\n";
			}
			// Reminder Form H
			if (FCConvert.ToDouble(txtReminderFormAdjustH.Text) > 4 || FCConvert.ToDouble(txtReminderFormAdjustH.Text) < 0)
			{
				blnOK = false;
				strError += "Reminder Form Horizontal Adjustment must be between 0 and 4" + "\r\n";
			}
			// Reminder Form V
			if (FCConvert.ToDouble(txtReminderFormAdjustV.Text) > 4 || FCConvert.ToDouble(txtReminderFormAdjustV.Text) < 0)
			{
				blnOK = false;
				strError += "Reminder Form Vertical Adjustment must be between 0 and 4" + "\r\n";
			}
			// Lien Notice Signature        'kk02082016 trout-1197  Change to 0 to 1.3 inches                   'MAL@20080616: Add new adjusmtnts for signature line - Tracker Reference: 14279
			if (Conversion.Val(txtSigAdjust_Lien.Text) > 0.5 || FCConvert.ToDouble(txtSigAdjust_Lien.Text) < 0)
			{
				// kjr 12.7.16 trout-807  Change limit to match max line movement in form of .5 inch
				blnOK = false;
				strError += "Lien Notice Signature Adjustment must be between 0 and .5 inches" + "\r\n";
			}
			ValidateAdjustments = blnOK;
			return ValidateAdjustments;
		}

		private void LoadToolTips()
		{
			// Laser Adjustments
			ToolTip1.SetToolTip(txtCMFAdjust, "This is the CMF adjustment in lines.");
			ToolTip1.SetToolTip(txtLabelAdjustment, "This is the laser label adjustment in lines.");
			ToolTip1.SetToolTip(txtSigAdjust_Lien, "This is the vertical adjustment in inches. Valid values are 0 to .5.");
			// Registry Adjustments for filings
			ToolTip1.SetToolTip(txtLienTopAdjust, "This is the top margin adjustment in inches. Valid values are -1.5 to 4.0.");
			ToolTip1.SetToolTip(txtLienBottomAdjust, "This is the bottom margin adjustment in inches. Valid values are -1.5 to 4.0.");
			ToolTip1.SetToolTip(txtLienSideAdjust, "This is the side margin adjustment in inches. Valid values are -0.75 to 1.");
			ToolTip1.SetToolTip(txtLDNTopAdjust, "This is the top margin adjustment in inches. Valid values are -1.5 to 4.0.");
			ToolTip1.SetToolTip(txtLDNBottomAdjust, "This is the bottom margin adjustment in inches. Valid values are -1.5 to 4.0.");
			ToolTip1.SetToolTip(txtLDNSideAdjust, "This is the side margin adjustment in inches. Valid values are -0.75 to 1.");
			// Reminder Notice Adjustmants
			ToolTip1.SetToolTip(txtReminderFormAdjustH, "This is the full page reminder left margin adjustment in inches. Valid values are 0 to 4.0.");
			ToolTip1.SetToolTip(txtReminderFormAdjustV, "This is the full page reminder top margin adjustment in inches. Valid values are 0 to 4.0.");
		}

		private void txtReminderFormAdjustH_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtReminderFormAdjustH_Enter(object sender, System.EventArgs e)
		{
			txtReminderFormAdjustH.SelectionStart = 0;
			txtReminderFormAdjustH.SelectionLength = txtReminderFormAdjustH.Text.Length;
		}

		private void txtReminderFormAdjustH_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtReminderFormAdjustH_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtSigAdjust_Lien.Text) >= 4)
			{
				txtSigAdjust_Lien.Text = "4.00";
			}
			if (Conversion.Val(txtSigAdjust_Lien.Text) <= 0)
			{
				txtSigAdjust_Lien.Text = "0.00";
			}
		}

		private void txtReminderFormAdjustV_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtReminderFormAdjustV_Enter(object sender, System.EventArgs e)
		{
			txtReminderFormAdjustV.SelectionStart = 0;
			txtReminderFormAdjustV.SelectionLength = txtReminderFormAdjustV.Text.Length;
		}

		private void txtReminderFormAdjustV_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtReminderFormAdjustV_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtReminderFormAdjustV.Text) >= 4)
			{
				txtReminderFormAdjustV.Text = "6.00";
			}
			if (Conversion.Val(txtReminderFormAdjustV.Text) <= 0)
			{
				txtReminderFormAdjustV.Text = "0.00";
			}
		}

		private void txtSigAdjust_Lien_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtSigAdjust_Lien_Enter(object sender, System.EventArgs e)
		{
			txtSigAdjust_Lien.SelectionStart = 0;
			txtSigAdjust_Lien.SelectionLength = txtSigAdjust_Lien.Text.Length;
		}

		private void txtSigAdjust_Lien_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtSigAdjust_Lien_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// kk02082016 trout-1197  Changed signature adjustment from +/-6 Lines to 0-1.3 inches   'kjr 12.7.16 trout-807  Change limit to match max line movement in form of .5 inch
			if (Conversion.Val(txtSigAdjust_Lien.Text) >= 0.5)
			{
				txtSigAdjust_Lien.Text = ".50";
			}
			if (Conversion.Val(txtSigAdjust_Lien.Text) <= 0)
			{
				txtSigAdjust_Lien.Text = "0";
			}
		}

		private void StoreTaxServiceEMail()
		{
			// kk01182017 trocl-1326  Store tax service email(s) in Collections table
			string strEmailList;
			int I;
			strEmailList = "";
			for (I = 0; I <= vsTSEmailGrid.Rows - 1; I++)
			{
				if (vsTSEmailGrid.TextMatrix(I, 0) != "")
				{
					if (I == 0)
					{
						strEmailList = Strings.Trim(vsTSEmailGrid.TextMatrix(I, 0));
					}
					else
					{
						strEmailList += "," + Strings.Trim(vsTSEmailGrid.TextMatrix(I, 0));
					}
				}
				else
				{
					break;
				}
			}
			if (strEmailList == "")
			{
				strEmailList = modGlobal.gstrDefTaxServiceEmail;
				// Set the default
			}
			rsSettings.Set_Fields("TaxServiceEmail", strEmailList);
		}

		private void LoadTaxServiceEMail()
		{
			// kk01182017 trocl-1326  Load tax service emails stored in Collections table
			string[] arrEmail = null;
			string strEmailList = "";
			int I;
			if (FCConvert.ToString(rsSettings.Get_Fields_String("TaxServiceEmail")) != "")
			{
				strEmailList = FCConvert.ToString(rsSettings.Get_Fields_String("TaxServiceEmail"));
			}
			else
			{
				strEmailList = modGlobal.gstrDefTaxServiceEmail;
			}
			arrEmail = Strings.Split(strEmailList, ",", -1, CompareConstants.vbBinaryCompare);
			for (I = 0; I <= Information.UBound(arrEmail, 1); I++)
			{
				vsTSEmailGrid.TextMatrix(I, 0, Strings.Trim(arrEmail[I]));
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdFileSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
