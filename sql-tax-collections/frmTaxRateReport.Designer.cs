﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxRateReport.
	/// </summary>
	partial class frmTaxRateReport : BaseForm
	{
		public fecherFoundation.FCComboBox cboEndYear;
		public fecherFoundation.FCComboBox cboStartYear;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblAccount;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxRateReport));
			this.cboEndYear = new fecherFoundation.FCComboBox();
			this.cboStartYear = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 188);
			this.BottomPanel.Size = new System.Drawing.Size(489, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboEndYear);
			this.ClientArea.Controls.Add(this.cboStartYear);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Size = new System.Drawing.Size(489, 128);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(489, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(194, 35);
			this.HeaderText.Text = "Tax Rate Report";
			// 
			// cboEndYear
			// 
			this.cboEndYear.AutoSize = false;
			this.cboEndYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndYear.FormattingEnabled = true;
			this.cboEndYear.Location = new System.Drawing.Point(349, 66);
			this.cboEndYear.Name = "cboEndYear";
			this.cboEndYear.Size = new System.Drawing.Size(122, 40);
			this.cboEndYear.TabIndex = 4;
			// 
			// cboStartYear
			// 
			this.cboStartYear.AutoSize = false;
			this.cboStartYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartYear.FormattingEnabled = true;
			this.cboStartYear.Location = new System.Drawing.Point(99, 66);
			this.cboStartYear.Name = "cboStartYear";
			this.cboStartYear.Size = new System.Drawing.Size(122, 40);
			this.cboStartYear.TabIndex = 2;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(162, 16);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "ENTER THE YEAR RANGE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 80);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(48, 16);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "START";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccount
			// 
			this.lblAccount.AutoSize = true;
			this.lblAccount.Location = new System.Drawing.Point(290, 80);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(34, 16);
			this.lblAccount.TabIndex = 3;
			this.lblAccount.Text = "END";
			this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Visible = true;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(179, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmTaxRateReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(489, 296);
			this.FillColor = 0;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmTaxRateReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Tax Rate Report";
			this.Load += new System.EventHandler(this.frmTaxRateReport_Load);
			this.Activated += new System.EventHandler(this.frmTaxRateReport_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTaxRateReport_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxRateReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
