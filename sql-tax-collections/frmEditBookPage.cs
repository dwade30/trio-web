﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmEditBookPage.
	/// </summary>
	public partial class frmEditBookPage : BaseForm
	{
		public frmEditBookPage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.txtAccount.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditBookPage InstancePtr
		{
			get
			{
				return (frmEditBookPage)Sys.GetInstance(typeof(frmEditBookPage));
			}
		}

		protected frmEditBookPage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/05/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/14/2006              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		bool boolDirty;
		bool boolLoaded;
		int intType;

		public void Init(short intPassType = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// defaults to saving the lien book and page
				intType = intPassType;
				switch (intType)
				{
					case 0:
						{
							this.Text = "Edit Lien Book / Page";
							break;
						}
					case 1:
						{
							this.Text = "Edit LDN Book / Page";
							break;
						}
				}
				//end switch
				boolDirty = false;
				// trocl-772 kgk 08-05-2011  Reset the dirty flag
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Form");
			}
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolContinue = false;
				// trocl-772 kgk 08-05-2011  Add a check for dirty form before changing year
				// load the info from this account
				// this will load the lien record's book and page info
				if (cmbYear.SelectedIndex >= 0)
				{
					boolContinue = true;
					// trocl-772 kgk 08-05-2011  Check for unsaved changes before changing year
					if (boolDirty)
					{
						DialogResult messageBoxResult = FCMessageBox.Show("Changes have been made.  Would you like to save now?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Save Changes");
						if (messageBoxResult == DialogResult.Yes)
						{
							// save the changes - this will reset the form
							SaveInfo();
							boolContinue = false;
							txtAccount.Focus();
						}
						else if (messageBoxResult == DialogResult.No)
						{
							// do not save the changes
							boolContinue = true;
						}
					}
					if (boolContinue)
					{
						// trocl-772 kgk 08-05-2011  Reset the form and load the selected year
						txtBook.Text = "";
						txtPage.Text = "";
						boolDirty = false;
						switch (intType)
						{
							case 0:
								{
									rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)));
									if (!rsLien.EndOfFile())
									{
										// this will load the info and unlock the fields
										// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
										txtBook.Text = FCConvert.ToString(rsLien.Get_Fields("Book"));
										// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
										txtPage.Text = FCConvert.ToString(rsLien.Get_Fields("Page"));
										rsLien.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)));
										if (!rsLien.EndOfFile())
										{
											// this will change the name to be correct
											lblName.Text = FCConvert.ToString(rsLien.Get_Fields_String("Name1"));
										}
										LockFields(false);
									}
									else
									{
										FCMessageBox.Show("Could not find lien record " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", MsgBoxStyle.Exclamation, "Load Lien Error");
									}
									break;
								}
							case 1:
								{
									rsLien.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strCLDatabase);
									if (!rsLien.EndOfFile())
									{
										// this will load the info and unlock the fields
										// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
										txtBook.Text = FCConvert.ToString(rsLien.Get_Fields("Book"));
										// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
										txtPage.Text = FCConvert.ToString(rsLien.Get_Fields("Page"));
										rsLien.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)));
										if (!rsLien.EndOfFile())
										{
											// this will change the name to be correct
											lblName.Text = FCConvert.ToString(rsLien.Get_Fields_String("Name1"));
										}
										LockFields(false);
									}
									else
									{
										FCMessageBox.Show("Could not find lien record " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", MsgBoxStyle.Exclamation, "Load Lien Error");
									}
									break;
								}
						}
						//end switch
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Load Lien Record Error");
			}
		}

		private void frmEditBookPage_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmEditBookPage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmEditBookPage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditBookPage.Icon	= "frmEditBookPage.frx":0000";
			//frmEditBookPage.FillStyle	= 0;
			//frmEditBookPage.ScaleWidth	= 3885;
			//frmEditBookPage.ScaleHeight	= 2355;
			//frmEditBookPage.LinkTopic	= "Form2";
			//frmEditBookPage.LockControls	= -1  'True;
			//frmEditBookPage.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtAccount.MaxLength	= 10;
			//txtPage.MaxLength	= 6;
			//txtBook.MaxLength	= 6;
			//vsElasticLight1.OleObjectBlob	= "frmEditBookPage.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
		}

		private void frmEditBookPage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int lngCT/*unused?*/;
			if (boolDirty)
			{
				DialogResult messageBoxResult = FCMessageBox.Show("Changes have been made.  Would you like to save them before you exit?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
				if (messageBoxResult == DialogResult.Yes)
				{
					// save the changes and exit
					SaveInfo();
				}
				else if (messageBoxResult == DialogResult.No)
				{
					// do not save the changes and exit
				}
				else if (messageBoxResult == DialogResult.Cancel)
				{
					// do not save and cancel the exit
					e.Cancel = true;
				}
			}
			if (e.Cancel)
			{
				// do nothing
			}
			else
			{
				boolLoaded = false;
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileGetAccount_Click(object sender, System.EventArgs e)
		{
			// get the account information
			if (boolDirty)
			{
				DialogResult messageBoxResult = FCMessageBox.Show("Changes have been made.  Would you like to save now?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
				if (messageBoxResult == DialogResult.Yes)
				{
					// save the changes
					SaveInfo();
					// get the account years that are liened
					if (Conversion.Val(txtAccount.Text) > 0)
					{
						GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
					}
				}
				else if (messageBoxResult == DialogResult.No)
				{
					// do not save the changes and exit
					// get the account years that are liened
					if (Conversion.Val(txtAccount.Text) > 0)
					{
						GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
					}
				}
				else if (messageBoxResult == DialogResult.Cancel)
				{
					// do nothing...the user cancelled
				}
			}
			else
			{
				// get the account years that are liened
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
				}
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			Close();
		}
		// VBto upgrade warning: lngLienAccount As int	OnWriteFCConvert.ToDouble(
		private void GetLienYears_2(int lngLienAccount)
		{
			GetLienYears(ref lngLienAccount);
		}

		private void GetLienYears(ref int lngLienAccount)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLDN = new clsDRWrapper();
				// this will fill the year combo with all of the years that this account has liens
				// and it will clear out the book and page fields
				cmbYear.Clear();
				// trocl-772 kgk 08-05-2011  Always clear the Book and Page
				txtBook.Text = "";
				txtPage.Text = "";
				rsLien.OpenRecordset("SELECT * FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber WHERE Account = " + FCConvert.ToString(lngLienAccount) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				if (rsLien.RecordCount() > 0)
				{
					while (!rsLien.EndOfFile())
					{
						switch (intType)
						{
							case 0:
								{
									cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear"))));
									cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienRecordNumber")));
									rsLien.MoveNext();
									break;
								}
							case 1:
								{
									// MAL@20080115: Changed to take lien discharge printed flag into account
									// Tracker Reference: 11920
									rsLDN.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(rsLien.Get_Fields_Int32("LienRecordNumber")) + " AND Printed = 1", modExtraModules.strCLDatabase);
									if (!rsLDN.EndOfFile())
									{
										// if there is not a LDN for this lien then do not show this information
										cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear"))));
										cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienRecordNumber")));
									}
									rsLien.MoveNext();
									break;
								}
						}
						//end switch
					}
					rsLien.MoveFirst();
					lblName.Text = FCConvert.ToString(rsLien.Get_Fields_String("Name1"));
					// trocl-772 kgk 08-05-2011  Moved outside of If     LockFields True, False
				}
				else
				{
					FCMessageBox.Show("This account does not have any liened years.", MsgBoxStyle.Information, "No Liened Years");
					lblName.Text = "";
					// trocl-772 kgk 08-05-2011  Moved outside of If
					// txtBook.Text = ""
					// txtPage.Text = ""
					// LockFields True
				}
				LockFields(true, false);
				// trocl-772 kgk 08-05-2011  Always Lock fields but not cmbYear so tab will work
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Account");
			}
		}

		private void SaveInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save the book page information into the lien record
				switch (intType)
				{
					case 0:
						{
							// save this lien record's book and page
							if (cmbYear.SelectedIndex >= 0)
							{
								rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strCLDatabase);
								if (!rsLien.EndOfFile())
								{
									rsLien.Edit();
									// this will save the info, then clear and lock the fields
									rsLien.Set_Fields("Book", txtBook.Text);
									rsLien.Set_Fields("Page", txtPage.Text);
									rsLien.Update();
									LockFields(false);
								}
								else
								{
									FCMessageBox.Show("Could not find lien record " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", MsgBoxStyle.Exclamation, "Load Lien Error");
								}
							}
							break;
						}
					case 1:
						{
							// Save the book and page to the Lien Discharge Notice
							if (cmbYear.SelectedIndex >= 0)
							{
								rsLien.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strCLDatabase);
								if (!rsLien.EndOfFile())
								{
									rsLien.Edit();
									// this will save the info, then clear and lock the fields
									rsLien.Set_Fields("Book", txtBook.Text);
									rsLien.Set_Fields("Page", txtPage.Text);
									rsLien.Update();
									LockFields(false);
								}
								else
								{
									FCMessageBox.Show("Could not find lien discharge notice " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", MsgBoxStyle.Exclamation, "Load Lien Error");
								}
							}
							break;
						}
				}
				//end switch
				// clear the fields
				lblName.Text = "";
				txtAccount.Text = "";
				cmbYear.Clear();
				txtBook.Text = "";
				txtPage.Text = "";
				// clear slate
				boolDirty = false;
				// trocl-772 kgk 08-05-2011  Don't lock cmbYear so tab will work
				// lock the fields until another valid account is entered
				// LockFields True
				LockFields(true, false);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Lien");
			}
		}

		private void LockFields(bool boolLock, bool boolYear = true)
		{
			if (boolYear)
			{
				cmbYear.Enabled = !boolLock;
			}
			else
			{
				cmbYear.Enabled = true;
			}
			txtBook.Enabled = !boolLock;
			txtPage.Enabled = !boolLock;
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
            //FC:FINAL:SBE - #2185 - changing text selection will trigger validating event again, and there is no fix for this from ITG
            //if (txtAccount.Text != "")
            //{
            //	txtAccount.SelectionStart = 0;
            //	txtAccount.SelectionLength = txtAccount.Text.Length;
            //}
        }

        private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
                //FC:FINAL:BSE i2185 Validate should not come twice both from control keys event and from form events
                //txtAccount_Validate(false);
                Support.SendKeys("{TAB}");
            }
		}

		private void txtAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// allow these characters
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// get the account information
			if (boolDirty)
			{
				DialogResult messageBoxResult = FCMessageBox.Show("Changes have been made.  Would you like to save now?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
				if (messageBoxResult == DialogResult.Yes)
				{
					// save the changes
					SaveInfo();
					// get the account years that are liened
					if (Conversion.Val(txtAccount.Text) > 0)
					{
						GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
					}
				}
				else if (messageBoxResult == DialogResult.No)
				{
					// do not save the changes and exit
					// get the account years that are liened
					if (Conversion.Val(txtAccount.Text) > 0)
					{
						GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
					}
				}
				else if (messageBoxResult == DialogResult.Cancel)
				{
					// do nothing...the user cancelled
				}
			}
			else
			{
				// get the account years that are liened
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
				}
			}
			// trocl-772 kgk 08-05-2011  Keep focus if there are no lien years for the account
			if (cmbYear.Items.Count == 0)
			{
				e.Cancel = true;
			}
		}

		public void txtAccount_Validate(bool Cancel)
		{
			txtAccount_Validating(txtAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtBook_Enter(object sender, System.EventArgs e)
		{
            //FC:FINAL:SBE - #2185 - changing text selection will trigger validating event again, and there is no fix for this from ITG
            //if (txtBook.Text != "")
            //{
            //	txtBook.SelectionStart = 0;
            //	txtBook.SelectionLength = txtBook.Text.Length;
            //}
        }

        private void txtBook_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// kk01272016 trocls-68 Allow alpha chars
			// Select Case KeyAscii
			// Case vbKey0 To vbKey9, vbKeyBack
			if (txtBook.Enabled)
			{
				boolDirty = true;
			}
			// Case Else
			// KeyAscii = 0
			// End Select
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtPage_Enter(object sender, System.EventArgs e)
		{
            //FC:FINAL:SBE - #2185 - changing text selection will trigger validating event again, and there is no fix for this from ITG
			//if (txtPage.Text != "")
			//{
			//	txtPage.SelectionStart = 0;
			//	txtPage.SelectionLength = txtPage.Text.Length;
			//}
		}

		private void txtPage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// kk01272016 trocls-68 Allow alpha chars
			// Select Case KeyAscii
			// Case vbKey0 To vbKey9, vbKeyBack
			if (txtPage.Enabled)
			{
				boolDirty = true;
			}
			// Case Else
			// KeyAscii = 0
			// End Select
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdSearch_Click(object sender, EventArgs e)
		{
			this.mnuFileGetAccount_Click(sender, e);
		}

		private void cmdFileSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
