﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	public class modUTFunctions
	{
		public static int GetAccountKeyUT(ref int lngAccountNumber)
		{
			int GetAccountKeyUT = 0;
			// this function will accept the account number and return the account key
			// if there is no matching account then it will return 0
			// and if there is an error then it will return a -1
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("SELECT AccountNumber, ID FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAccountNumber), modExtraModules.strUTDatabase);
				if (!rs.EndOfFile())
				{
					// TODO Get_Fields: Field [Key] not found!! (maybe it is an alias?)
					GetAccountKeyUT = FCConvert.ToInt32(rs.Get_Fields("ID"));
				}
				else
				{
					GetAccountKeyUT = 0;
				}
				return GetAccountKeyUT;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				GetAccountKeyUT = -1;
				FCMessageBox.Show("Error # " + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Retrieving Account Number");
			}
			return GetAccountKeyUT;
		}

		public class StaticVariables
		{
			//=========================================================
			public bool gboolPayWaterFirst;
			public string TownService = "";
			// (W)ater (S)ewer or (B)oth
			public bool gboolAutoPayOldestBillFirst;
			public bool gboolShowLastUTAccountInCR;
			public double gdblUTMuniTaxRate;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
