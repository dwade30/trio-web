﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCertOfSettlement.
	/// </summary>
	partial class frmCertOfSettlement : BaseForm
	{
		public fecherFoundation.FCFrame fraVariables;
		public fecherFoundation.FCGrid vsData;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCertOfSettlement));
            this.fraVariables = new fecherFoundation.FCFrame();
            this.vsData = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariables)).BeginInit();
            this.fraVariables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 420);
            this.BottomPanel.Size = new System.Drawing.Size(905, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraVariables);
            this.ClientArea.Size = new System.Drawing.Size(905, 360);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(905, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(281, 30);
            this.HeaderText.Text = "Certificate Of Settlement";
            // 
            // fraVariables
            // 
            this.fraVariables.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraVariables.AppearanceKey = "groupBoxNoBorders";
            this.fraVariables.Controls.Add(this.vsData);
            this.fraVariables.Location = new System.Drawing.Point(30, 30);
            this.fraVariables.Name = "fraVariables";
			this.fraVariables.Size = new System.Drawing.Size(855, 340);
			this.fraVariables.TabIndex = 0;
            this.fraVariables.Text = "Confirm Data";
            this.fraVariables.UseMnemonic = false;
            // 
            // vsData
            // 
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.Cols = 3;
            this.vsData.ColumnHeadersVisible = false;
            this.vsData.ExtendLastCol = true;
            this.vsData.FixedCols = 0;
            this.vsData.FixedRows = 0;
            this.vsData.Location = new System.Drawing.Point(0, 37);
            this.vsData.Name = "vsData";
            this.vsData.RowHeadersVisible = false;
            this.vsData.Rows = 1;
            this.vsData.Size = new System.Drawing.Size(839, 297);
            this.vsData.TabIndex = 1;
            this.vsData.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsData_AfterEdit);
            this.vsData.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VsData_CellFormatting);
            this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
            this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(361, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(182, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save & Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmCertOfSettlement
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(905, 528);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCertOfSettlement";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Certificate Of Settlement";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCertOfSettlement_Load);
            this.Activated += new System.EventHandler(this.frmCertOfSettlement_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCertOfSettlement_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariables)).EndInit();
            this.fraVariables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
	}
}
