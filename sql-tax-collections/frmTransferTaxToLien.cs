﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTransferTaxToLien.
	/// </summary>
	public partial class frmTransferTaxToLien : BaseForm
	{
		public frmTransferTaxToLien()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTransferTaxToLien InstancePtr
		{
			get
			{
				return (frmTransferTaxToLien)Sys.GetInstance(typeof(frmTransferTaxToLien));
			}
		}

		protected frmTransferTaxToLien _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/13/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsValidate = new clsDRWrapper();
		bool boolLoaded;
		double dblFilingFee;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeNewOwner;
		bool boolChargeCert;
		// VBto upgrade warning: dtMailDate As DateTime	OnWrite(string)
		DateTime dtMailDate;
		int lngRK;
		string strRK;
		bool blnChargeIntParty;

		public void Init(string strPassRK)
		{
			strRK = strPassRK;
			// this is the rate key list to select from
			this.Show(App.MainForm);
		}

		private void frmTransferTaxToLien_Activated(object sender, System.EventArgs e)
		{
			lngRK = frmRateRecChoice.InstancePtr.lngRateRecNumber;
			//FC:BCU #i189 - check if form is disposed
			if (lngRK > 0 && !this.IsDisposed)
			{
				if (!boolLoaded)
				{
					FormatGrid();
					ShowGridFrame();
					this.Text = "Transfer Tax to Lien";
					lblInstruction.Text = "To transfer to liened status:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Process' or press F12.";
					lblValidateInstruction.Text = "If these values are correct, Press F12 to advance.  If not, then please rerun your Tax Lien Certificates.";
					SetAct(0);
					FillValidateGrid();
					if (modGlobalConstants.Statics.gboolBD)
					{
						modValidateAccount.SetBDFormats();
					}
					if (FillDemandGrid())
					{
						boolLoaded = true;
					}
					else
					{
						boolLoaded = false;
						Close();
					}
				}
			}
			else
			{
				FCMessageBox.Show("The rate record selected is 0.  Please choose another rate record.", MsgBoxStyle.Information, "Rate Record Violation");
				frmRateRecChoice.InstancePtr.Show(App.MainForm);
				Close();
			}
		}

		private void frmTransferTaxToLien_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmTransferTaxToLien_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransferTaxToLien.Icon	= "frmTransferTaxToLien.frx":0000";
			//frmTransferTaxToLien.FillStyle	= 0;
			//frmTransferTaxToLien.ScaleWidth	= 9045;
			//frmTransferTaxToLien.ScaleHeight	= 7095;
			//frmTransferTaxToLien.LinkTopic	= "Form2";
			//frmTransferTaxToLien.LockControls	= -1  'True;
			//frmTransferTaxToLien.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15584;
			//vsDemand._cy	= 8546;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 0;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 1;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.DataMode	= 0;
			//vsDemand.VirtualData	= -1  'True;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsValidate.BackColor	= "-2147483643";
			//			//vsValidate.ForeColor	= "-2147483640";
			//vsValidate.BorderStyle	= 1;
			//vsValidate.FillStyle	= 0;
			//vsValidate.Appearance	= 1;
			//vsValidate.GridLines	= 1;
			//vsValidate.WordWrap	= 0;
			//vsValidate.ScrollBars	= 3;
			//vsValidate.RightToLeft	= 0;
			//vsValidate._cx	= 11139;
			//vsValidate._cy	= 7881;
			//vsValidate._ConvInfo	= 1;
			//vsValidate.MousePointer	= 0;
			//vsValidate.BackColorFixed	= -2147483633;
			//			//vsValidate.ForeColorFixed	= -2147483630;
			//vsValidate.BackColorSel	= -2147483635;
			//			//vsValidate.ForeColorSel	= -2147483634;
			//vsValidate.BackColorBkg	= -2147483636;
			//vsValidate.BackColorAlternate	= -2147483643;
			//vsValidate.GridColor	= -2147483633;
			//vsValidate.GridColorFixed	= -2147483632;
			//vsValidate.TreeColor	= -2147483632;
			//vsValidate.FloodColor	= 192;
			//vsValidate.SheetBorder	= -2147483642;
			//vsValidate.FocusRect	= 1;
			//vsValidate.HighLight	= 1;
			//vsValidate.AllowSelection	= -1  'True;
			//vsValidate.AllowBigSelection	= -1  'True;
			//vsValidate.AllowUserResizing	= 0;
			//vsValidate.SelectionMode	= 0;
			//vsValidate.GridLinesFixed	= 2;
			//vsValidate.GridLineWidth	= 1;
			//vsValidate.RowHeightMin	= 0;
			//vsValidate.RowHeightMax	= 0;
			//vsValidate.ColWidthMin	= 0;
			//vsValidate.ColWidthMax	= 0;
			//vsValidate.ExtendLastCol	= -1  'True;
			//vsValidate.FormatString	= "";
			//vsValidate.ScrollTrack	= -1  'True;
			//vsValidate.ScrollTips	= 0   'False;
			//vsValidate.MergeCells	= 0;
			//vsValidate.MergeCompare	= 0;
			//vsValidate.AutoResize	= -1  'True;
			//vsValidate.AutoSizeMode	= 0;
			//vsValidate.AutoSearch	= 0;
			//vsValidate.AutoSearchDelay	= 2;
			//vsValidate.MultiTotals	= -1  'True;
			//vsValidate.SubtotalPosition	= 1;
			//vsValidate.OutlineBar	= 0;
			//vsValidate.OutlineCol	= 0;
			//vsValidate.Ellipsis	= 0;
			//vsValidate.ExplorerBar	= 0;
			//vsValidate.PicturesOver	= 0   'False;
			//vsValidate.PictureType	= 0;
			//vsValidate.TabBehavior	= 0;
			//vsValidate.OwnerDraw	= 0;
			//vsValidate.ShowComboButton	= -1  'True;
			//vsValidate.TextStyle	= 0;
			//vsValidate.TextStyleFixed	= 0;
			//vsValidate.OleDragMode	= 0;
			//vsValidate.OleDropMode	= 0;
			//vsValidate.ComboSearch	= 3;
			//vsValidate.AutoSizeMouse	= -1  'True;
			//vsValidate.AllowUserFreezing	= 0;
			//vsValidate.BackColorFrozen	= 0;
			//			//vsValidate.ForeColorFrozen	= 0;
			//vsValidate.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmTransferTaxToLien.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmTransferTaxToLien_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			// unload frmRateRecChoice if need be
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == "frmRateRecChoice")
				{
					frmRateRecChoice.InstancePtr.Unload();
					break;
				}
			}
		}

		private void frmTransferTaxToLien_Resize(object sender, System.EventArgs e)
		{
			if (fraGrid.Visible)
			{
				ShowGridFrame();
				// this sets the height of the grid
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if (((vsDemand.Rows * vsDemand.RowHeight(0)) + 70) > fraGrid.Height - vsDemand.Top - 400)
				//{
				//	vsDemand.Height = fraGrid.Height - vsDemand.Top - 400;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
			}
			else
			{
				ShowValidateFrame();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, "");
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowValidateFrame()
		{
			// this will show/center the frame with the grid on it
			//FC:FINAL:DDU:#2039 - designer manages this part
			//fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			//fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//FC:FINAL:DDU:#2039 - designer manages this part
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
		}

		private void FormatGrid()
		{
			int wid = 0;
			vsDemand.Cols = 8;
			vsDemand.Rows = 1;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(0, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(1, FCConvert.ToInt32(wid * 0.15));
			// Acct
			vsDemand.ColWidth(2, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(3, FCConvert.ToInt32(wid * 0.15));
			// Total Notices Sent
			vsDemand.ColWidth(4, FCConvert.ToInt32(wid * 0.1));
			// Total Lien Costs
			vsDemand.ColWidth(5, 0);
			// Hidden Key Field
			vsDemand.ColWidth(6, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColWidth(7, 0);
			// holds the billing year
			vsDemand.ColFormat(4, "#,##0.00");
			vsDemand.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, 1, "Account");
			vsDemand.TextMatrix(0, 2, "Name");
			vsDemand.TextMatrix(0, 3, "Notices");
			vsDemand.TextMatrix(0, 4, "Amount");
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.6));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.32));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.TextMatrix(0, 1, "Parameters");
			vsValidate.TextMatrix(0, 2, "Value");
		}

		private bool FillDemandGrid()
		{
			bool FillDemandGrid = false;
			int intCT = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL = "";
				double dblXInt = 0;
				double[] dblAbate = new double[4 + 1];
				double dblAmount = 0;
				int lngCopies = 0;
				int lngMH = 0;
				int lngIP = 0;
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				FillDemandGrid = true;
				intCT = 1;
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Info"
				intCT = 2;
				vsDemand.Rows = 1;
				if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 0)
				{
					strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 3 AND LienStatusEligibility = 3 AND RateKey IN " + strRK + " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0";
					// this will be all the records that have had 30 Day Notices printed
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
				{
					strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 3 AND LienStatusEligibility = 3 AND RateKey IN " + strRK + " AND Name1 > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0";
				}
				else
				{
					strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 3 AND LienStatusEligibility = 3 AND RateKey IN " + strRK + " AND Account >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND Account <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0";
				}
				intCT = 3;
				strSQL += " ORDER BY Name1, Account";
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsData.EndOfFile())
				{
					intCT = 4;
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no accounts eligible to transfer to lien.", MsgBoxStyle.Information, "No Eligible Accounts");
					FillDemandGrid = false;
					return FillDemandGrid;
				}
				intCT = 5;
				int x;
				while (!rsData.EndOfFile())
				{
					for (x = 0; x <= 3; x++)
					{
						dblAbate[x] = 0;
					}
					// x
					modCLCalculations.CheckForAbatementPayments(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")), FCConvert.ToInt32(rsData.Get_Fields_Int32("RATEKEY")), FCConvert.ToString(rsData.Get_Fields_String("billingtype")) == "RE", ref dblAbate[0], ref dblAbate[1], ref dblAbate[2], ref dblAbate[3], Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtMailDate, ref dblXInt, ref dblAbate[0], ref dblAbate[1], ref dblAbate[2], ref dblAbate[3]) <= 0)
					{
						goto SKIP;
					}
					vsDemand.AddItem("");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsDemand.TextMatrix(vsDemand.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("Account")));
					// account number
					vsDemand.TextMatrix(vsDemand.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("Name1")));
					// name
					// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
					vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields("Copies")));
					// number of copies
					intCT = 6;
					// amount
					if (((rsData.Get_Fields_Decimal("DemandFees"))) == 0)
					{
						// highlight the row and set the value = zero
						intCT = 7;
						//						vsDemand.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsDemand.Rows - 1, 0, vsDemand.Rows - 1, vsDemand.Cols - 1, Color.Red);
						vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(0));
						if (boolChargeCert)
						{
							vsDemand.TextMatrix(vsDemand.Rows - 1, 6, FCConvert.ToString(0));
						}
						else
						{
							// this will allow no charges to be added
							vsDemand.TextMatrix(vsDemand.Rows - 1, 6, FCConvert.ToString(0));
						}
					}
					else
					{
						intCT = 8;
						// kk 01162013 trocl-972  Rewrite to allow CMF for MH, IP and New Owner without fee for tax payer
						if (boolChargeCert)
						{
							dblAmount = dblCertMailFee;
							lngCopies = 1;
						}
						else
						{
							dblAmount = 0;
							lngCopies = 0;
						}
						if (boolChargeMort)
						{
							// MAL@20071127
							lngMH = CalculateMortgageHolders();
							if (lngMH > 0)
							{
								lngCopies += lngMH;
								dblAmount += (lngMH * dblCertMailFee);
							}
						}
						// Check for Interested Parties
						if (blnChargeIntParty)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							lngIP = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")));
							if (lngIP > 0)
							{
								lngCopies += lngIP;
								dblAmount += (lngIP * dblCertMailFee);
							}
						}
						// Check for New Owner
						rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
						if (!rsRK.EndOfFile())
						{
							dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
						}
						else
						{
							dtBillDate = (DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (boolChargeNewOwner && modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")), FCConvert.ToInt32(rsData.Get_Fields("Account")), dtBillDate))
						{
							// kk NewOwnerCMF(rsData.Get_Fields("Account")) Then
							lngCopies += 1;
							dblAmount += dblCertMailFee;
						}
						vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToString(lngCopies));
						// kk 01162013 trocl-972  Copies charged, not just number printed
						vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblAmount + dblFilingFee + (Conversion.Val(rsData.Get_Fields_Decimal("DemandFees") - rsData.Get_Fields_Decimal("DemandFeespaid")))));
						// If boolChargeCert Then
						// If boolChargeMort Then
						// .TextMatrix(.rows - 1, 4) = (rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// ElseIf blnChargeIntParty Then
						// .TextMatrix(.rows - 1, 4) = (rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// Else
						// may have to add the new owner charge here...
						// If NewOwnerCMF(rsData.Get_Fields("Account")) And boolChargeNewOwner Then
						// .TextMatrix(.rows - 1, 4) = dblCertMailFee + dblCertMailFee + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// Else
						// .TextMatrix(.rows - 1, 4) = dblCertMailFee + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// End If
						// End If
						// Else
						// If boolChargeMort Then
						// If NewOwnerCMF(rsData.Get_Fields("Account")) Then
						// If boolChargeNewOwner Then
						// .TextMatrix(.rows - 1, 4) = ((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// Else
						// .TextMatrix(.rows - 1, 4) = ((rsData.Get_Fields("Copies") - 2) * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// End If
						// Else
						// .TextMatrix(.rows - 1, 4) = ((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// End If
						// ElseIf blnChargeIntParty Then
						// If NewOwnerCMF(rsData.Get_Fields("Account")) Then
						// If boolChargeNewOwner Then
						// .TextMatrix(.rows - 1, 4) = ((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// Else
						// .TextMatrix(.rows - 1, 4) = ((rsData.Get_Fields("Copies") - 2) * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// End If
						// Else
						// .TextMatrix(.rows - 1, 4) = ((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// End If
						// Else
						// .TextMatrix(.rows - 1, 4) = dblFilingFee + (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
						// End If
						// End If
					}
					intCT = 20;
					vsDemand.TextMatrix(vsDemand.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					// billkey
					vsDemand.TextMatrix(vsDemand.Rows - 1, 7, FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
					// billingyear
					SKIP:
					;
					rsData.MoveNext();
				}
				if (vsDemand.Rows <= 1)
				{
					intCT = 4;
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no accounts eligible to transfer to lien.", MsgBoxStyle.Information, "No Eligible Accounts");
					FillDemandGrid = false;
					return FillDemandGrid;
				}
				intCT = 21;
				// check all of the accounts
				mnuFileSelectAll_Click();
				intCT = 25;
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if (((vsDemand.Rows * vsDemand.RowHeight(0)) + 70) > fraGrid.Height - vsDemand.Top - 400)
				//{
				//	vsDemand.Height = fraGrid.Height - vsDemand.Top - 400;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//	intCT = 30;
				//}
				//else
				//{
				//	intCT = 31;
				//	vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				intCT = 32;
				frmWait.InstancePtr.Unload();
				return FillDemandGrid;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FillDemandGrid = false;
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Transfer List - " + FCConvert.ToString(intCT));
			}
			return FillDemandGrid;
		}

		private void FillValidateGrid()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			vsValidate.Rows = 1;
			rsValidate.OpenRecordset("SELECT * FROM Control_LienProcess");
			if (rsValidate.EndOfFile())
			{
				FCMessageBox.Show("Please return to and run 'Print Tax Lien'.", MsgBoxStyle.Information, "No Control Record");
				Close();
			}
			else
			{
				vsValidate.AddItem("Billing Year" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_Int32("BillingYear")));
				dtMailDate = FCConvert.ToDateTime(Strings.Format(rsValidate.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
				vsValidate.AddItem("Interest/Mailing Date" + "\t" + Strings.Format(dtMailDate, "MM/dd/yyyy"));
				// vsValidate.AddItem "Recorded Date" & vbTab & Format(.Get_Fields("DateCreated"), "MM/dd/yyyy")
				// vsValidate.AddItem "Certified Mail Fee" & vbTab & Format(.Get_Fields("CertMailFee"), "#,##0.00")
				dblFilingFee = Conversion.Val(rsValidate.Get_Fields_Double("FilingFee"));
				vsValidate.AddItem("Filing Fee" + "\t" + Strings.Format(dblFilingFee, "#,##0.00"));
				boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
				boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
				// kk
				vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
				dblCertMailFee = Conversion.Val(rsValidate.Get_Fields_Double("CertMailFee"));
				if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
				{
					if (Strings.UCase(Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 7)) == Strings.UCase("Yes, ch"))
					{
						boolChargeNewOwner = true;
					}
					else
					{
						boolChargeNewOwner = false;
					}
				}
				else
				{
					boolChargeNewOwner = false;
				}
				if (boolChargeCert)
				{
					// kk                vsValidate.AddItem "Certified Mail Fee" & vbTab & Format(.Get_Fields("CertMailFee"), "#,##0.00")
					// dblCertMailFee = .Get_Fields("CertMailFee")
					vsValidate.AddItem("Charge taxpayer for Cert Mail Fee?" + "\t" + "Yes");
				}
				else
				{
					// If boolChargeMort Or boolChargeNewOwner Then
					// vsValidate.AddItem "Certified Mail Fee" & vbTab & Format(.Get_Fields("CertMailFee"), "#,##0.00")
					// dblCertMailFee = .Get_Fields("CertMailFee")
					// Else
					// vsValidate.AddItem "Certified Mail Fee" & vbTab & "0.00"
					// dblCertMailFee = 0
					// End If
					vsValidate.AddItem("Charge taxpayer for Cert Mail Fee?" + "\t" + "No");
				}
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToMortHolder")))
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "Yes");
					}
					else
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
				}
				// MAL@20080102: Add Interested Party
				blnChargeIntParty = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeforIntParty"));
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToIntParty")))
				{
					if (blnChargeIntParty)
					{
						vsValidate.AddItem("Charge for each interested party?" + "\t" + "Yes");
					}
					else
					{
						vsValidate.AddItem("Charge for each interested party?" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Charge for each interested party?" + "\t" + "No");
				}
				if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
				{
					if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 2) == "Ye")
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")));
					}
					else
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")));
					}
				}
				else
				{
					vsValidate.AddItem("Send to New Owner" + "\t" + "No");
				}
			}
			// set the height of the grid
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//if (vsValidate.Rows * vsValidate.RowHeight(0) > fraValidate.Height - 300)
			//{
			//	vsValidate.Height = fraValidate.Height - 300;
			//	vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//	vsValidate.Height = vsValidate.Rows * vsValidate.RowHeight(0) + 70;
			//	vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void SetAct(short intAct)
		{
			// this will change all of the menu options
			switch (intAct)
			{
				case 0:
					{
						ShowValidateFrame();
						fraGrid.Visible = false;
						btnProcess.Text = "Process";
						cmdFileSelectAll.Visible = false;
						intAction = 0;
						break;
					}
				case 1:
					{
						ShowGridFrame();
						fraValidate.Visible = false;
						cmdFileSelectAll.Visible = true;
						btnProcess.Text = "Process";
						intAction = 1;
						break;
					}
			}
			//end switch
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						FCMessageBox.Show("After you transfer tax to lien, you will not be able to reprint the notices, labels or certified mailers.", MsgBoxStyle.Exclamation, "Transfer Tax To Lien");
						SetAct(1);
						break;
					}
				case 1:
					{
						// Transfer Tax to Lien
						TransferTaxToLien();
						Close();
						break;
					}
			}
			//end switch
		}

		private void TransferTaxToLien()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				string strAcctList = "";
				double dblPrin = 0;
				double dblTransferAmt = 0;
				double dblTransferAmtTA = 0;
				double dblTotalTransfer = 0;
				int lngYear = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				clsDRWrapper rsTaxAcq = new clsDRWrapper();
				double[] dblFundAmt = new double[100 + 1];
				double[] dblFundAmtTA = new double[100 + 1];
				int lngFund = 0;
				int intCT;
				int lngJ = 0;
				bool boolTA = false;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Records");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					App.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 6)) >= 0)
						{
							// then what?
							rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, 5));
							if (rsData.NoMatch)
							{
								FCMessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, 1) + ".  No lien record was created.", MsgBoxStyle.Critical, "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, 5));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsTaxAcq.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(rsData.Get_Fields("Account")), modExtraModules.strREDatabase);
								if (!rsTaxAcq.EndOfFile())
								{
									boolTA = FCConvert.ToBoolean(rsTaxAcq.Get_Fields_Boolean("TaxAcquired"));
								}
								else
								{
									boolTA = false;
								}
								lngYear = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear"));
								rsData.Edit();
								strAcctList += FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + ", ";
								rsData.Set_Fields("LienRecordNumber", CreateLienRecord(rsValidate.Get_Fields_DateTime("FilingDate"), ref dblPrin));
								if (modGlobal.Statics.gboolMultipleTowns)
								{
									// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
									lngFund = FCConvert.ToInt32(rsData.Get_Fields("TranCode"));
									if (boolTA)
									{
										// kk10162014 trocl-1192  Need to separate TA accounts
										dblFundAmtTA[lngFund] += dblPrin;
									}
									else
									{
										dblFundAmt[lngFund] += dblPrin;
									}
									dblTotalTransfer += dblPrin;
									// this will accumulate the total transfer of principal for the BD entry made
								}
								else
								{
									if (boolTA)
									{
										// kk10162014 trocl-1192  Need to separate TA accounts
										dblTransferAmtTA += dblPrin;
										// this will accumulate the total transfer of principal for the BD entry made
									}
									else
									{
										dblTransferAmt += dblPrin;
										// this will accumulate the total transfer of principal for the BD entry made
									}
									dblTotalTransfer += dblPrin;
									// this will accumulate the total transfer of principal for the BD entry made
								}
								rsData.Set_Fields("InterestAppliedThroughDate", rsValidate.Get_Fields_DateTime("FilingDate"));
								rsData.Set_Fields("WhetherBilledBefore", "L");
								rsData.Set_Fields("LienProcessStatus", 4);
								rsData.Set_Fields("LienStatusEligibility", 5);
								rsData.Update();
								lngDemandCount += 1;
							}
						}
						else
						{
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				if (Strings.Right(strAcctList, 2) == ", ")
				{
					strAcctList = Strings.Left(strAcctList, strAcctList.Length - 2);
				}
				if (modGlobalConstants.Statics.gboolBD)
				{
					// create the automatic entry for BD
					modGlobalFunctions.AddCYAEntry_242("CL", "Sent Transfer Tax To Lien Entry To BD", dblTotalTransfer.ToString(), (lngYear / 10).ToString(), "Filing Date :" + Strings.Format(rsValidate.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						for (intCT = 1; intCT <= 100; intCT++)
						{
							if (dblFundAmt[intCT] != 0 || dblFundAmtTA[intCT] != 0)
							{
								// this will create an entry for each trancode, the next routine will figure out what fund it will go to
								modGlobal.CreateTransferTaxToLienBDEntry(dblFundAmt[intCT], dblFundAmtTA[intCT], lngYear / 10, intCT, lngJ);
								// kk10172014 trocl-1192  Add separate TA amounts
							}
						}
					}
					else
					{
						modGlobal.CreateTransferTaxToLienBDEntry(dblTransferAmt, dblTransferAmtTA, lngYear / 10);
						// kk10172014 trocl-1192  Add separate TA amounts
					}
					FCMessageBox.Show("Automatic journal entries have been created in TRIO Budgetary.", MsgBoxStyle.Exclamation, "Journal Entries");
				}
				rsData.OpenRecordset("SELECT Distinct RateKey AS RK FROM BillingMaster WHERE ID IN (" + strAcctList + ")", modExtraModules.strCLDatabase);
				while (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
					rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields("RK")), modExtraModules.strCLDatabase);
					if (!(rsRate.Get_Fields_DateTime("LienDate") is DateTime))
					{
						rsRate.Edit();
						rsRate.Set_Fields("LienDate", rsValidate.Get_Fields_DateTime("FilingDate"));
						rsRate.Update();
					}
					rsData.MoveNext();
				}
				// create a report with these accounts shown and save it as the transfer tax to lien report
				arLienTransferReport.InstancePtr.Init(strAcctList);
				frmReportViewer.InstancePtr.Init(arLienTransferReport.InstancePtr);
				frmWait.InstancePtr.Unload();
				// show the user how many accounts were affected
				FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", MsgBoxStyle.Information, "Liened Accounts Added");
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error During Transfer");
			}
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(cmdFileSelectAll, new System.EventArgs());
		}

		private int CreateLienRecord(DateTime dtLienDate, ref double dblOrigPrin/*= 0*/)
		{
			int CreateLienRecord = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will calculate the CHGINT line for the account and then return
				// the CHGINT number to store in the payment record
				// this will use rsData which is set to the correct record
				double dblCurInt = 0;
				double dblInterest = 0;
				double dblPrin = 0;
				double dblCosts = 0;
				double dblTotalDue;
				clsDRWrapper rsLien = new clsDRWrapper();
				clsDRWrapper rsRK = new clsDRWrapper();
				double dblCMFee = 0;
				int lngMH = 0;
				int lngIP = 0;
				DateTime dtBillDate;
				double[] dblAbate = new double[4 + 1];
				// calculate the current interest to this date (needs to be the lien date)
				// dblTotalDue = CalculateAccountCL(rsData, rsData.Get_Fields("Account"), Date, dblCurInt, dblPrin, dblInterest, dblCosts)
				int x;
				for (x = 0; x <= 3; x++)
				{
					dblAbate[x] = 0;
				}
				// x
				modCLCalculations.CheckForAbatementPayments(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")), FCConvert.ToInt32(rsData.Get_Fields_Int32("RATEKEY")), FCConvert.ToString(rsData.Get_Fields_String("billingtype")) == "RE", ref dblAbate[0], ref dblAbate[1], ref dblAbate[2], ref dblAbate[3], Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtLienDate, ref dblCurInt, ref dblPrin, ref dblInterest, ref dblCosts, ref dblAbate[0], ref dblAbate[1], ref dblAbate[2], ref dblAbate[3]);
				if (dblTotalDue > 0)
				{
					// if there is interest due, then
					// this will actually create the payment record
					rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = 0", modExtraModules.strCLDatabase);
					rsLien.AddNew();
					// CreateLienRecord = .Get_Fields("LienRecordNumber")
					rsLien.Set_Fields("Principal", modGlobal.Round(dblPrin, 2));
					dblOrigPrin = dblPrin;
					rsLien.Set_Fields("Interest", modGlobal.Round(FCConvert.ToDouble(rsData.Get_Fields_Decimal("InterestCharged") * -1) + dblCurInt - Conversion.Val(rsData.Get_Fields_Decimal("InterestPaid")), 2));
					// If boolChargeCert Then
					// If boolChargeMort Then
					// dblCMFee = (rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee '+ (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
					// Else    'may have to add the new owner charge here...
					// dblCMFee = dblCertMailFee + dblFilingFee '+ (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
					// End If
					// Else
					// If boolChargeMort Then
					// dblCMFee = ((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee '+ (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
					// Else    'may have to add the new owner charge here...
					// dblCMFee = dblCertMailFee + dblFilingFee '+ (rsData.Get_Fields("DemandFees") - rsData.Get_Fields("DemandFeespaid"))
					// End If
					// End If
					// kk06012016 trocl-1309  Apply Cert Main Fees Individually for Tax Payer, Mortgage Holder, Interested Parties and New Owner
					// If rsValidate.Get_Fields("PayCertMailFee") Then
					// If rsValidate.Get_Fields("ChargeForMortHolder") Then        'I think I need to check about the new owner too...
					// dblCMFee = rsValidate.Get_Fields("CertMailFee") * rsData.Get_Fields("Copies")
					// Else
					// dblCMFee = rsValidate.Get_Fields("CertMailFee")
					// End If
					// Else
					// If rsValidate.Get_Fields("ChargeForMortHolder") Then        'I think I need to check about the new owner too...
					// dblCMFee = rsValidate.Get_Fields("CertMailFee") * (rsData.Get_Fields("Copies") - 1)
					// Else
					// dblCMFee = 0
					// End If
					// End If
					// Charge cert mail fee for tax payer original copy?
					if (boolChargeCert)
					{
						dblCMFee = dblCertMailFee;
					}
					// Charge cert mail fee for each mortgage holder copy?
					if (boolChargeMort)
					{
						lngMH = CalculateMortgageHolders();
						if (lngMH > 0)
						{
							dblCMFee += (lngMH * dblCertMailFee);
						}
					}
					// Charge cert mail fee for each interested party copy?
					if (blnChargeIntParty)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						lngIP = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")));
						if (lngIP > 0)
						{
							dblCMFee += (lngIP * dblCertMailFee);
						}
					}
					// Charge cert mail fee for New Owner copy?
					rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
					if (!rsRK.EndOfFile())
					{
						dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
					}
					else
					{
						dtBillDate = (DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (boolChargeNewOwner && modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")), FCConvert.ToInt32(rsData.Get_Fields("Account")),  dtBillDate))
					{
						// kk NewOwnerCMF(rsData.Get_Fields("Account")) Then
						dblCMFee += dblCertMailFee;
					}
					rsLien.Set_Fields("Costs", Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsData.Get_Fields_Decimal("DemandFeesPaid")) + Conversion.Val(rsValidate.Get_Fields_Double("FilingFee")) + dblCMFee);
					rsLien.Set_Fields("RateKey", lngRK);
					rsLien.Set_Fields("DateCreated", DateTime.Today);
					rsLien.Set_Fields("InterestAppliedThroughDate", rsValidate.Get_Fields_DateTime("FilingDate"));
					rsLien.Set_Fields("Status", "P");
					rsLien.Set_Fields("InterestCharged", 0);
					// Initialize fields to zero so they aren't NULL
					rsLien.Set_Fields("MaturityFee", 0);
					rsLien.Set_Fields("PrincipalPaid", 0);
					rsLien.Set_Fields("InterestPaid", 0);
					rsLien.Set_Fields("PLIPaid", 0);
					rsLien.Set_Fields("CostsPaid", 0);
					rsLien.Set_Fields("PrintedLDN", 0);
					rsLien.Set_Fields("Book", "");
					rsLien.Set_Fields("Page", "");
					rsLien.Update();
					CreateLienRecord = FCConvert.ToInt32(rsLien.Get_Fields_Int32("ID"));
				}
				return CreateLienRecord;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Lien Record");
			}
			return CreateLienRecord;
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

        //private void vsDemand_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngMR;
        //	int lngMC;
        //	lngMR = vsDemand.MouseRow;
        //	lngMC = vsDemand.MouseCol;
        //	if (lngMR > 0 && lngMC > 0)
        //	{
        //		if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
        //		{
        //			ToolTip1.SetToolTip(vsDemand, "A Lien Record has already been created for this account.  This account is not eligible.");
        //		}
        //		else
        //		{
        //			if (vsDemand.TextMatrix(lngMR, 7) != "")
        //			{
        //				ToolTip1.SetToolTip(vsDemand, "");
        //				// vsDemand.TextMatrix(lngMR, 7)
        //			}
        //			else
        //			{
        //				ToolTip1.SetToolTip(vsDemand, "");
        //			}
        //		}
        //	}
        //}

        private void vsDemand_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            int lngMC;
            lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
            lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
            if (lngMR > 0 && lngMC > 0)
            {
                DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
                if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
                {
                    cell.ToolTipText = "A Lien Record has already been created for this account.  This account is not eligible.";
                }
                else
                {
                    if (vsDemand.TextMatrix(lngMR, 7) != "")
                    {
                        cell.ToolTipText = "";
                        // vsDemand.TextMatrix(lngMR, 7)
                    }
                    else
                    {
                        cell.ToolTipText = "";
                    }
                }
            }
        }

        private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			switch (vsDemand.Col)
			{
				case 0:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsDemand.EditCell();
						break;
					}
				default:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private bool NewOwnerCMF(ref int lngAcct)
		{
			bool NewOwnerCMF = false;
			// This function will check the CMFNumbers table to see if there is a NewOwner in the group
			// to add a fee for the extra copy
			clsDRWrapper rsCMF = new clsDRWrapper();
			rsCMF.OpenRecordset("SELECT Account, NewOwner FROM CMFNumbers WHERE NewOwner = 1 AND Type = 21 AND Account = " + FCConvert.ToString(lngAcct), modExtraModules.strCLDatabase);
			NewOwnerCMF = !rsCMF.EndOfFile();
			return NewOwnerCMF;
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				clsDRWrapper rsMort = new clsDRWrapper();
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND Module = 'RE'", "CentralData");
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				return CalculateMortgageHolders;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}
	}
}
