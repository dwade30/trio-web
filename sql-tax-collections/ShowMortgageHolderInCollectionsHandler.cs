﻿using System;
using fecherFoundation;
using Global;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;

namespace TWCL0000
{
    public class ShowMortgageHolderInCollectionsHandler : CommandHandler<ShowMortgageHolderInCollections>
    {
        protected override void Handle(ShowMortgageHolderInCollections command)
        {
            frmMortgageHolder.InstancePtr.Init(command.Account,Convert.ToInt16( command.AccountType));
            frmMortgageHolder.InstancePtr.Show(App.MainForm);
        }
    }
}