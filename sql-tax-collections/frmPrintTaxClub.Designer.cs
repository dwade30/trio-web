﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmPrintTaxClub.
	/// </summary>
	partial class frmPrintTaxClub : BaseForm
	{
		public fecherFoundation.FCComboBox cmbChoice;
		public fecherFoundation.FCGrid vsTaxClub;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintTaxClub));
            this.cmbChoice = new fecherFoundation.FCComboBox();
            this.vsTaxClub = new fecherFoundation.FCGrid();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.lblChoice = new fecherFoundation.FCLabel();
            this.lblTaxClub = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTaxClub)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 421);
            this.BottomPanel.Size = new System.Drawing.Size(920, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblTaxClub);
            this.ClientArea.Controls.Add(this.vsTaxClub);
            this.ClientArea.Controls.Add(this.lblChoice);
            this.ClientArea.Controls.Add(this.cmbChoice);
            this.ClientArea.Size = new System.Drawing.Size(920, 361);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(920, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(110, 30);
            this.HeaderText.Text = "Tax Club";
            // 
            // cmbChoice
            // 
            this.cmbChoice.Items.AddRange(new object[] {
            "All Accounts",
            "Negative Balance",
            "Overdue Payments",
            "Positive Balance",
            "Zero Balance"});
            this.cmbChoice.Location = new System.Drawing.Point(195, 30);
            this.cmbChoice.Name = "cmbChoice";
            this.cmbChoice.Size = new System.Drawing.Size(121, 40);
            this.cmbChoice.TabIndex = 3;
            // 
            // vsTaxClub
            // 
            this.vsTaxClub.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTaxClub.Cols = 10;
            this.vsTaxClub.FixedCols = 0;
            this.vsTaxClub.Location = new System.Drawing.Point(30, 129);
            this.vsTaxClub.Name = "vsTaxClub";
            this.vsTaxClub.RowHeadersVisible = false;
            this.vsTaxClub.Rows = 1;
            this.vsTaxClub.Size = new System.Drawing.Size(864, 209);
            this.vsTaxClub.TabIndex = 1;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint});
            this.MainMenu1.Name = null;
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePrint.Text = "Print Preview";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // lblChoice
            // 
            this.lblChoice.AutoSize = true;
            this.lblChoice.Location = new System.Drawing.Point(30, 44);
            this.lblChoice.Name = "lblChoice";
            this.lblChoice.Size = new System.Drawing.Size(128, 17);
            this.lblChoice.TabIndex = 1;
            this.lblChoice.Text = "PRINT SELECTION";
            // 
            // lblTaxClub
            // 
            this.lblTaxClub.AutoSize = true;
            this.lblTaxClub.Location = new System.Drawing.Point(30, 90);
            this.lblTaxClub.Name = "lblTaxClub";
            this.lblTaxClub.Size = new System.Drawing.Size(139, 17);
            this.lblTaxClub.TabIndex = 2;
            this.lblTaxClub.Text = "LIST OF ACCOUNTS";
            // 
            // frmPrintTaxClub
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(920, 529);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPrintTaxClub";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Tax Club";
            this.Load += new System.EventHandler(this.frmPrintTaxClub_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintTaxClub_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTaxClub)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblChoice;
		private FCLabel lblTaxClub;
	}
}
