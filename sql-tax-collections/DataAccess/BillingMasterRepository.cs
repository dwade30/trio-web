﻿using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedDataAccess.TaxCollections;

namespace TWCL0000.DataAccess
{
    public class BillingMasterRepository : Repository<BillingMaster>, IBillingMasterRepository
    {
        protected TaxCollectionsContext collectionsContext;
        public BillingMasterRepository(TaxCollectionsContext context) : base(context)
        {
            collectionsContext = context;
        }

        public PropertyTaxBillType BillingType { get; set; } = PropertyTaxBillType.Real;
        
        public IEnumerable<int> GetTaxYears()
        {
            return collectionsContext.BillingMasters.Where(o => o.BillingType == BillingTypeCode() && o.BillingYear != null).Select(o => o.BillingYear.Value).Distinct().OrderBy(value => value).ToList();
        }

        protected string BillingTypeCode ()
        {
            if (BillingType == PropertyTaxBillType.Personal)
            {
                return "PP";
            }
            return "RE";
        }
    }
}
