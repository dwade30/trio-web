﻿using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Global;
using SharedApplication.TaxCollections.Interfaces;
using SharedDataAccess.TaxCollections;
using TWSharedLibrary;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000.DataAccess
{
    public class BillingMasterRepository_Legacy : IBillingMasterRepository
    {
        private TaxCollectionsContext collectionsContext;

        public BillingMasterRepository_Legacy()
        {
            var deets = StaticSettings.gGlobalSettings.GetContextDetails();
            var ctx = new TrioContextFactory(deets, StaticSettings.gGlobalSettings);
            collectionsContext = ctx.GetTaxCollectionsContext();
        }

        public PropertyTaxBillType BillingType { get; set; } = PropertyTaxBillType.Real;
        
        public IEnumerable<int> GetTaxYears()
        {
            return collectionsContext.BillingMasters.Where(o => o.BillingType == BillingTypeCode() && o.BillingYear != null).Select(o => o.BillingYear.Value).Distinct().OrderBy(value => value).ToList();
        }

        protected string BillingTypeCode ()
        {
            if (BillingType == PropertyTaxBillType.Personal)
            {
                return "PP";
            }
            return "RE";
        }

        #region Implementation of IRepository<BillingMaster>

        /// <inheritdoc />
        public SharedApplication.TaxCollections.Models.BillingMaster Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BillingMaster> GetIncluding(Expression<Func<BillingMaster, bool>> whereProperty, params Expression<Func<BillingMaster, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public IEnumerable<SharedApplication.TaxCollections.Models.BillingMaster> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public IEnumerable<SharedApplication.TaxCollections.Models.BillingMaster> Find(Expression<Func<SharedApplication.TaxCollections.Models.BillingMaster, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Add(BillingMaster entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(BillingMaster entity)
        {
            throw new NotImplementedException();
        }

        public void AddRange(IEnumerable<BillingMaster> entities)
        {
            throw new NotImplementedException();
        }

        public void RemoveRange(IEnumerable<BillingMaster> entities)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
