﻿using System;
using System.Collections.Generic;
using SharedDataAccess.TaxCollections;

namespace TWCL0000.DataAccess
{
    public class REBillingMasterRepository : BillingMasterRepository, IREBillingMasterRepository
    {       
        public REBillingMasterRepository(TaxCollectionsContext context) : base(context)
        {
            base.BillingType = PropertyTaxBillType.Real;
        }

        #region Implementation of IPurge

        /// <inheritdoc />
        public Dictionary<string, int> Purge(List<string> listYearsToPurge)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
