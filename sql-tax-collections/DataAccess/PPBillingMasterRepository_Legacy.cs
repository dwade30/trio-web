﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using Global;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;
using SharedDataAccess.TaxCollections;

namespace TWCL0000.DataAccess
{
    public class PPBillingMasterRepository_Legacy :BillingMasterRepository_Legacy, IPPBillingMasterRepository
    {
            public PPBillingMasterRepository_Legacy()
            {
                base.BillingType = PropertyTaxBillType.Personal;
            }

            #region Implementation of IPPBillingMasterRepository.IPurge

            /// <inheritdoc />
            public Dictionary<string, int> Purge(List<string> yearsToPurge)
            {
                foreach (var strYear in yearsToPurge)
                {
                    modCLPurge.PurgeCLRecords_8(2, DateAndTime.DateValue($"12/31/{strYear}"));  // 2 = Personal Property
                }

                // we cannot pull the count out so just return an empty dictionary
                return new Dictionary<string, int>();
            }

            #endregion
    }
}
