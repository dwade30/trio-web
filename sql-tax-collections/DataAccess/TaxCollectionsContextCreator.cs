﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedDataAccess;
namespace TWCL0000.DataAccess
{
    public class TaxCollectionsContextFactory 
    {
        private DataContextDetails dataContextDetails;
        public TaxCollectionsContextFactory(DataContextDetails contextDetails) 
        {
            dataContextDetails = contextDetails;
        }
        public TaxCollectionsContext Create(string databaseName) 
        {
            var stringBuilder = new SqlConnectionStringBuilder();
            stringBuilder.DataSource = dataContextDetails.DataSource;
            stringBuilder.UserID = dataContextDetails.UserID;
            stringBuilder.Password = dataContextDetails.Password;
            stringBuilder.IntegratedSecurity = false;
            stringBuilder.InitialCatalog = databaseName;
            var optionsBuilder = new DbContextOptionsBuilder<TaxCollectionsContext>();
            optionsBuilder.UseSqlServer(stringBuilder.ToString());
            return new TaxCollectionsContext(optionsBuilder.Options);
        }
    }
}
