﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using fecherFoundation;
using SharedApplication;
using TWCL0000;

public class CYABillingMasterRepository_Legacy : IPurge
{
        
    public CYABillingMasterRepository_Legacy()
    {
           
    }

    #region Implementation of IPurge

    /// <inheritdoc />
    public Dictionary<string, int> Purge(List<string> listYearsToPurge)
    {
        foreach (var strYear in listYearsToPurge)
        {
            var targetDate = DateAndTime.DateValue("12/31/" + strYear);
            modCLPurge.PurgeCYAEntries_8("CL", targetDate); 
        }
           
        // we cannot pull the count out so just return an empty dictionary
        return new Dictionary<string, int>();
    }

    #endregion
}