﻿using Microsoft.EntityFrameworkCore;
using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWCL0000.DataAccess
{
    public class TaxCollectionsContext : DataContext<TaxCollectionsContext>
    {
        public DbSet<BillingMaster> BillingMasters { get; set; }
        public DbSet<SavedStatusReport> SavedStatusReports {get;set;}        
        public TaxCollectionsContext(DbContextOptions<TaxCollectionsContext> options) : base(options)
        {

        }
    }
}
