﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;

namespace TWCL0000.DataAccess
{
    public class REBillingMasterRepository_Legacy : BillingMasterRepository_Legacy, IREBillingMasterRepository
    {
        public REBillingMasterRepository_Legacy()
        {
            base.BillingType = PropertyTaxBillType.Real;
        }

        #region Implementation of IPurge

        /// <inheritdoc />
        public Dictionary<string, int> Purge(List<string> listYearsToPurge)
        {
            foreach (var strYear in listYearsToPurge)
            {
                var targetDate = DateAndTime.DateValue("12/31/" + strYear);
                modCLPurge.PurgeCLRecords_8(0, targetDate); // non-lien
                modCLPurge.PurgeCLRecords_8(1, targetDate); // lien
            }
           
            // we cannot pull the count out so just return an empty dictionary
            return new Dictionary<string, int>();
        }

        #endregion
    }
}
