﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCLDailyAudit.
	/// </summary>
	partial class frmCLDailyAudit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAudit;
		public fecherFoundation.FCComboBox cmbREPP;
		public fecherFoundation.FCComboBox cmbAlignment;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCFrame fraAuditPassword;
		public fecherFoundation.FCButton cmdPassword;
		public fecherFoundation.FCTextBox txtAuditPassword;
		public fecherFoundation.FCLabel lblAuditPassword;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCLDailyAudit));
			this.cmbAudit = new fecherFoundation.FCComboBox();
			this.cmbREPP = new fecherFoundation.FCComboBox();
			this.cmbAlignment = new fecherFoundation.FCComboBox();
			this.cmdDone = new fecherFoundation.FCButton();
			this.fraAuditPassword = new fecherFoundation.FCFrame();
			this.cmdPassword = new fecherFoundation.FCButton();
			this.txtAuditPassword = new fecherFoundation.FCTextBox();
			this.lblAuditPassword = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.lblAudit = new fecherFoundation.FCLabel();
			this.lblREPP = new fecherFoundation.FCLabel();
			this.lblAlignment = new fecherFoundation.FCLabel();
			this.fcFrame1 = new fecherFoundation.FCFrame();
			this.btnProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAuditPassword)).BeginInit();
			this.fraAuditPassword.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).BeginInit();
			this.fcFrame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 517);
			this.BottomPanel.Size = new System.Drawing.Size(650, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.fcFrame1);
			this.ClientArea.Controls.Add(this.fraAuditPassword);
			this.ClientArea.Size = new System.Drawing.Size(650, 457);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(650, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(211, 30);
			this.HeaderText.Text = "Daily Audit Report";
			// 
			// cmbAudit
			// 
			this.cmbAudit.AutoSize = false;
			this.cmbAudit.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAudit.FormattingEnabled = true;
			this.cmbAudit.Items.AddRange(new object[] {
				"Audit Preview",
				"Audit Report and Close Out"
			});
			this.cmbAudit.Location = new System.Drawing.Point(167, 32);
			this.cmbAudit.Name = "cmbAudit";
			this.cmbAudit.Size = new System.Drawing.Size(242, 40);
			this.cmbAudit.TabIndex = 1;
			this.cmbAudit.Text = "Audit Preview";
			this.cmbAudit.SelectedIndexChanged += new System.EventHandler(this.optAudit_Click);
			// 
			// cmbREPP
			// 
			this.cmbREPP.AutoSize = false;
			this.cmbREPP.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbREPP.FormattingEnabled = true;
			this.cmbREPP.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property",
				"Both"
			});
			this.cmbREPP.Location = new System.Drawing.Point(167, 91);
			this.cmbREPP.Name = "cmbREPP";
			this.cmbREPP.Size = new System.Drawing.Size(242, 40);
			this.cmbREPP.TabIndex = 3;
			this.cmbREPP.Text = "Real Estate";
			// 
			// cmbAlignment
			// 
			this.cmbAlignment.AutoSize = false;
			this.cmbAlignment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAlignment.FormattingEnabled = true;
			this.cmbAlignment.Items.AddRange(new object[] {
				"Portrait",
				"Landscape"
			});
			this.cmbAlignment.Location = new System.Drawing.Point(167, 150);
			this.cmbAlignment.Name = "cmbAlignment";
			this.cmbAlignment.Size = new System.Drawing.Size(242, 40);
			this.cmbAlignment.TabIndex = 5;
			this.cmbAlignment.Text = "Portrait";
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "toolbarButton";
			this.cmdDone.Location = new System.Drawing.Point(430, 151);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(78, 23);
			this.cmdDone.TabIndex = 6;
			this.cmdDone.Text = "Process";
			this.cmdDone.Visible = false;
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// fraAuditPassword
			// 
			this.fraAuditPassword.Controls.Add(this.cmdPassword);
			this.fraAuditPassword.Controls.Add(this.txtAuditPassword);
			this.fraAuditPassword.Controls.Add(this.lblAuditPassword);
			this.fraAuditPassword.Location = new System.Drawing.Point(30, 329);
			this.fraAuditPassword.Name = "fraAuditPassword";
			this.fraAuditPassword.Size = new System.Drawing.Size(546, 97);
			this.fraAuditPassword.TabIndex = 1;
			this.fraAuditPassword.Text = "Enter Password";
			this.fraAuditPassword.UseMnemonic = false;
			this.fraAuditPassword.Visible = false;
			// 
			// cmdPassword
			// 
			this.cmdPassword.AppearanceKey = "toolbarButton";
			this.cmdPassword.Location = new System.Drawing.Point(448, 56);
			this.cmdPassword.Name = "cmdPassword";
			this.cmdPassword.Size = new System.Drawing.Size(78, 23);
			this.cmdPassword.TabIndex = 2;
			this.cmdPassword.Text = "Process";
			// 
			// txtAuditPassword
			// 
			this.txtAuditPassword.AutoSize = false;
			this.txtAuditPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtAuditPassword.Location = new System.Drawing.Point(281, 39);
			this.txtAuditPassword.Name = "txtAuditPassword";
			this.txtAuditPassword.Size = new System.Drawing.Size(149, 40);
			this.txtAuditPassword.TabIndex = 1;
			// 
			// lblAuditPassword
			// 
			this.lblAuditPassword.AutoSize = true;
			this.lblAuditPassword.Location = new System.Drawing.Point(20, 53);
			this.lblAuditPassword.Name = "lblAuditPassword";
			this.lblAuditPassword.Size = new System.Drawing.Size(255, 16);
			this.lblAuditPassword.TabIndex = 0;
			this.lblAuditPassword.Text = "PLEASE ENTER YOUR AUDIT PASSWORD";
			this.lblAuditPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// lblAudit
			// 
			this.lblAudit.AutoSize = true;
			this.lblAudit.Location = new System.Drawing.Point(20, 46);
			this.lblAudit.Name = "lblAudit";
			this.lblAudit.Size = new System.Drawing.Size(45, 16);
			this.lblAudit.TabIndex = 0;
			this.lblAudit.Text = "AUDIT";
			this.lblAudit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblREPP
			// 
			this.lblREPP.AutoSize = true;
			this.lblREPP.Location = new System.Drawing.Point(20, 106);
			this.lblREPP.Name = "lblREPP";
			this.lblREPP.Size = new System.Drawing.Size(40, 16);
			this.lblREPP.TabIndex = 2;
			this.lblREPP.Text = "TYPE";
			this.lblREPP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAlignment
			// 
			this.lblAlignment.AutoSize = true;
			this.lblAlignment.Location = new System.Drawing.Point(20, 164);
			this.lblAlignment.Name = "lblAlignment";
			this.lblAlignment.Size = new System.Drawing.Size(92, 16);
			this.lblAlignment.TabIndex = 4;
			this.lblAlignment.Text = "ORIENTATION";
			this.lblAlignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame1
			// 
			this.fcFrame1.Controls.Add(this.cmbAudit);
			this.fcFrame1.Controls.Add(this.lblAlignment);
			this.fcFrame1.Controls.Add(this.cmbAlignment);
			this.fcFrame1.Controls.Add(this.lblREPP);
			this.fcFrame1.Controls.Add(this.cmbREPP);
			this.fcFrame1.Controls.Add(this.lblAudit);
			this.fcFrame1.Controls.Add(this.cmdDone);
			this.fcFrame1.Location = new System.Drawing.Point(30, 20);
			this.fcFrame1.Name = "fcFrame1";
			this.fcFrame1.Size = new System.Drawing.Size(426, 208);
			this.fcFrame1.TabIndex = 0;
			this.fcFrame1.Text = "Select Audit Process";
			this.fcFrame1.UseMnemonic = false;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 261);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(145, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Print Preview";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmCLDailyAudit
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(650, 517);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCLDailyAudit";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Daily Audit Report";
			this.Load += new System.EventHandler(this.frmCLDailyAudit_Load);
			this.Activated += new System.EventHandler(this.frmCLDailyAudit_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCLDailyAudit_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAuditPassword)).EndInit();
			this.fraAuditPassword.ResumeLayout(false);
			this.fraAuditPassword.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).EndInit();
			this.fcFrame1.ResumeLayout(false);
			this.fcFrame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel lblAudit;
		private FCLabel lblREPP;
		private FCLabel lblAlignment;
		private FCFrame fcFrame1;
		private FCButton btnProcess;
	}
}
