﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienFeeIncr.
	/// </summary>
	public partial class frmLienFeeIncr : BaseForm
	{
		public frmLienFeeIncr()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienFeeIncr InstancePtr
		{
			get
			{
				return (frmLienFeeIncr)Sys.GetInstance(typeof(frmLienFeeIncr));
			}
		}

		protected frmLienFeeIncr _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/02/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/07/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		bool boolLoaded;
		double dblFilingFee;
		DateTime dtEffDate;
		int lngRK;
		bool boolDONOTACTIVATE;
		string strRateKeys = "";

		private void frmLienFeeIncr_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsColl = new clsDRWrapper();
			if (!boolLoaded)
			{
				dtEffDate = DateAndTime.DateValue("10/08/2013");
				boolLoaded = true;
				rsColl.OpenRecordset("SELECT LienFeeIncrRunOn FROM Collections", modExtraModules.strCLDatabase);
				if (Conversion.Val(rsColl.Get_Fields_DateTime("LienFeeIncrRunOn")) != FCConvert.ToDateTime(0).ToOADate())
				{
					FCMessageBox.Show("This process can only be run one time", MsgBoxStyle.Critical, null);
					Close();
				}
				else
				{
					if (!boolDONOTACTIVATE)
					{
						if (!FillProcessArray())
						{
							boolDONOTACTIVATE = true;
							Close();
						}
						else
						{
							boolDONOTACTIVATE = false;
						}
					}
					else
					{
						// MsgBox "Stop"
					}
				}
			}
		}

		private void frmLienFeeIncr_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienFeeIncr.Icon	= "frmLienFeeIncr.frx":0000";
			//frmLienFeeIncr.FillStyle	= 0;
			//frmLienFeeIncr.ScaleWidth	= 7620;
			//frmLienFeeIncr.ScaleHeight	= 5130;
			//frmLienFeeIncr.LinkTopic	= "Form2";
			//frmLienFeeIncr.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmLienFeeIncr.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoaded = false;
			// clear the array of user defined types
			FCUtils.EraseSafe(modGlobal.Statics.arrDemand);
			this.Text = "Lien Discharge Filing Fee Increase";
			mnuFileSave.Text = "Apply Fee Increase";
			lblInstr1.Text = "Effective October 8, 2013 the fees for recording and discharge of a lien are increasing from $13 to $19 for the first recorded page." + "\r\n" + "\r\n" + "This process will apply the increase for the discharge to all unpaid liens created before the effective date." + "\r\n" + "A report of the effected liens will be generated at the end of the process." + "\r\n" + "\r\n" + "To run the process press F12 or select 'Apply Fee Increase' from the File menu." + "To skip the process press Esc, select Exit from the File menu or close this window.";
		}

		private void frmLienFeeIncr_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
				return;
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolDONOTACTIVATE = false;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool FillProcessArray()
		{
			bool FillProcessArray = false;
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL;
				int lngIndex;
				clsDRWrapper rsMaster = new clsDRWrapper();
				double dblXInt = 0;
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				double dblAmount/*unused?*/;
				FillProcessArray = true;
				// XX1  rsMaster.OpenRecordset "SELECT * FROM Master", strREDatabase
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				// XX2  rsLData.OpenRecordset "SELECT * FROM LienRec", strCLDatabase            'this is a recordset of all the lien records
				// XX3  rsRK.OpenRecordset "SELECT * FROM RateRec WHERE RateType = 'L'", strCLDatabase
				strSQL = "SELECT BillingMaster.ID AS BillKey, * FROM BillingMaster INNER JOIN LienRec ON [BillingMaster].[LienRecordNumber]=[LienRec].[ID] WHERE Principal<>[LienRec].[PrincipalPaid] ORDER BY Account, BillingYear";
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					App.DoEvents();
					FillProcessArray = false;
					FCMessageBox.Show("There are no accounts eligible.", MsgBoxStyle.Information, "No Eligible Accounts - " + FCConvert.ToString(lngRK));
					return FillProcessArray;
				}
				lngIndex = 0;
				while (!rsData.EndOfFile())
				{
					App.DoEvents();
					// find the first lien record
					// XX2  rsLData.FindFirstRecord "ID", rsData.Get_Fields("LienRecordNumber")
					// XX2  If rsLData.NoMatch Then
					rsLData.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
					if (rsLData.EndOfFile())
					{
						// if there is no match, then report the error
						frmWait.InstancePtr.Visible = false;
						FCMessageBox.Show("Cannot find Lien Record #" + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + ".", MsgBoxStyle.Critical, "Missing Lien Record");
						frmWait.InstancePtr.Visible = true;
					}
					else
					{
						// XX3  rsRK.FindFirstRecord "ID", rsLData.Get_Fields("RateKey")
						// XX3  If rsRK.NoMatch Then
						rsRK.OpenRecordset("SELECT * FROM RateRec WHERE RateType = 'L' AND ID = " + FCConvert.ToString(rsLData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
						if (rsRK.EndOfFile())
						{
							frmWait.InstancePtr.Visible = false;
							FCMessageBox.Show("Cannot find Lien Rate Key " + FCConvert.ToString(rsLData.Get_Fields_Int32("RateKey")) + ".", MsgBoxStyle.Critical, "Missing Rate Key");
							frmWait.InstancePtr.Visible = true;
						}
						else
						{
							dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							if (Conversion.Val(rsLData.Get_Fields("Principal")) - Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")) > 0 && dtBillDate.ToOADate() <= dtEffDate.ToOADate())
							{
								// XX1  rsMaster.FindFirstRecord2 "RSAccount,RSCard", rsData.Get_Fields("Account") & ",1", ","
								// XX1  If rsMaster.NoMatch Then
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsMaster.OpenRecordset("SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(rsData.Get_Fields("Account")) + "AND RSCard = 1", modExtraModules.strREDatabase);
								if (rsMaster.EndOfFile())
								{
									frmWait.InstancePtr.Visible = false;
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									FCMessageBox.Show("Cannot find Master Record for account #" + FCConvert.ToString(rsData.Get_Fields("Account")) + ".", MsgBoxStyle.Critical, "Missing Master Record");
									frmWait.InstancePtr.Visible = true;
								}
								else
								{
									// kk If CalculateAccountCLLien(rsData, dtEffDate, dblXInt, , , , , True) > 0 Then
									if (modCLCalculations.CalculateAccountCLLien(rsLData, dtEffDate, ref dblXInt) > 0)
									{
										Array.Resize(ref modGlobal.Statics.arrDemand, lngIndex + 1);
										// this will make sure that there are enough elements to use
										//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
										modGlobal.Statics.arrDemand[lngIndex] = new modGlobal.AccountFeesAdded(0);
										modGlobal.Statics.arrDemand[lngIndex].Used = true;
										modGlobal.Statics.arrDemand[lngIndex].Processed = false;
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										modGlobal.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
										modGlobal.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
										modGlobal.Statics.arrDemand[lngIndex].Year = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear"));
										modGlobal.Statics.arrDemand[lngIndex].BillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillKey"));
										lngIndex += 1;
									}
								}
							}
						}
					}
					App.DoEvents();
					rsData.MoveNext();
				}
				if (!modGlobal.Statics.arrDemand[0].Used)
				{
					frmWait.InstancePtr.Unload();
					App.DoEvents();
					FillProcessArray = false;
					FCMessageBox.Show("There are no accounts eligible.", MsgBoxStyle.Information, "No Eligible Accounts - " + FCConvert.ToString(lngRK));
					return FillProcessArray;
				}
				lngError = 9;
				frmWait.InstancePtr.Unload();
				return FillProcessArray;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FillProcessArray = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid - " + FCConvert.ToString(lngError));
			}
			return FillProcessArray;
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			// Apply the Lien Maturity Fees for the accounts selected
			if (ApplyAdditionalFees())
			{
				Close();
			}
			return;
		}

		private bool ApplyAdditionalFees()
		{
			bool ApplyAdditionalFees = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsColl = new clsDRWrapper();
				DialogResult response = 0;
				ApplyAdditionalFees = true;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Applying Fees");
				for (lngCT = 0; lngCT <= Information.UBound(modGlobal.Statics.arrDemand, 1); lngCT++)
				{
					// for each account in the array
					App.DoEvents();
					if (modGlobal.Statics.arrDemand[lngCT].BillKey >= 0)
					{
						// then find the bill record
						rsData.FindFirstRecord("BillKey", modGlobal.Statics.arrDemand[lngCT].BillKey);
						if (rsData.NoMatch)
						{
							frmWait.InstancePtr.Visible = false;
							response = FCMessageBox.Show("Error processing account " + FCConvert.ToString(modGlobal.Statics.arrDemand[lngCT].Account) + ".  No lien record was found.", MsgBoxStyle.Critical | MsgBoxStyle.OkCancel, "Cannot Find BillKey - " + FCConvert.ToString(modGlobal.Statics.arrDemand[lngCT].BillKey));
							if (response == DialogResult.Cancel)
							{
								frmWait.InstancePtr.Unload();
								ApplyAdditionalFees = false;
								return ApplyAdditionalFees;
							}
							else
							{
								frmWait.InstancePtr.Visible = true;
								App.DoEvents();
							}
						}
						else
						{
							modGlobal.Statics.arrDemand[lngCT].Processed = true;
							CreateFeeRecord();
							lngDemandCount += 1;
						}
					}
					else
					{
						// no bill... do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				// Mark it Done in the Collections Table
				rsColl.Execute("UPDATE Collections SET LienFeeIncrRunOn = '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "'", modExtraModules.strCLDatabase);
				FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " liens were affected.", MsgBoxStyle.Information, "Liened Accounts Added");
				modGlobalFunctions.AddCYAEntry_26("CL", "Processed Lien Discharge Filing Fee Increase", FCConvert.ToString(lngDemandCount) + " liens affected");
				if (lngDemandCount > 0)
				{
					// print the list of accounts/liens that have been affected
					rptLienFeeIncr.InstancePtr.Init("yes", "no");
					// frmReportViewer.Init rptLienMaturityFees
				}
				return ApplyAdditionalFees;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Applying Fees");
			}
			return ApplyAdditionalFees;
		}

		private int CreateFeeRecord()
		{
			int CreateFeeRecord = 0;
			// this will use rsData which is set to the correct record
			double dblTotalDue = 0;
			double dblInt = 0;
			double dblFee/*unused?*/;
			// XX2  rsLData.FindFirstRecord "ID", rsData.Get_Fields("LienRecordNumber")
			// XX2  If rsLData.NoMatch Then
			rsLData.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
			if (rsLData.EndOfFile())
			{
				// no match for the lien record...do not process the account
			}
			else
			{
				// calculate the current interest to this date
				dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsLData, dtEffDate, ref dblInt);
				if (dblTotalDue > 0)
				{
					// Add fee increase directly to lien (no way to detect this?)
					rsLData.Edit();
					// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
					rsLData.Set_Fields("Costs", (Conversion.Val(rsLData.Get_Fields("Costs")) + 6.0));
					rsLData.Update();
				}
			}
			return CreateFeeRecord;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
