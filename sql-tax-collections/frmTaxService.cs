﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CentralDocuments;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.TaxExtract;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxService.
	/// </summary>
	public partial class frmTaxService : BaseForm
	{
		public frmTaxService()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            base.Cancelled += Extract_Cancelled;
			taxExtractService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxExtractService()).Result;
            taxExtractService.ProgressIncremented += Progress_Incremented;
        }

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxService InstancePtr
		{
			get
			{
				return (frmTaxService)Sys.GetInstance(typeof(frmTaxService));
			}
		}

		protected frmTaxService _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/11/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/30/2006              *
		// ********************************************************
        clsDRWrapper rsAccounts = new clsDRWrapper();
		bool boolLoaded;
		int lngTCNumber;
		int lngRateKey;
		public string strTownEmail = "";
        private bool cancelled = false;
        private ITaxExtractService taxExtractService;
        IEnumerable<TaxServiceExtractRecord> extractData = new List<TaxServiceExtractRecord>();

		private void Extract_Cancelled(object sender, EventArgs e)
        {
            taxExtractService.Cancel();
            cancelled = true;
        }

        private void Progress_Incremented(object sender, (int Max, int Current) e)
        {
            UpdateWait("", e.Current, e.Max);
        }

		private void cmbTownCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbTownCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CheckInterestDate_2(true);
		}

		private void frmTaxService_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				if (!FillYearCombo())
				{
					FCMessageBox.Show("There are no billing years found in the database.", MsgBoxStyle.Exclamation, "Filling Year Combo");
					Close();
				}
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					cmbTownCode.Visible = true;
					lblTownCode.Visible = true;
					FillTownCombo();
				}

				boolLoaded = true;
			}
		}

		private void frmTaxService_Load(object sender, System.EventArgs e)
		{			
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetToolTipText();
		}

		private void frmTaxService_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void mnuFileCreateZip_Click(object sender, System.EventArgs e)
		{
			// create extract zip file
			if (CreateExtract(true))
			{
				Close();
			}
		}

		private bool CreateExtract(bool boolZip = false)
		{
			int lngYear = 0;
			int lngPeriod;
			DateTime dtTSDate;
			int lngTownCode = 0;

			try
			{
				if (cmbYear.SelectedIndex < 0)
				{
					FCMessageBox.Show("Please select a valid year.", MsgBoxStyle.Exclamation, "Select Year");

					if (cmbYear.Enabled && cmbYear.Visible)
					{
						cmbYear.Focus();
					}
				}

				if (Strings.Trim(lblDate.Text) == "")
				{
					FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");

					return false;
				}

				if (!Information.IsDate(lblDate.Text))
				{
					FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");

					return false;
				}
				else if (DateAndTime.DateValue(lblDate.Text).ToOADate() == 0 || DateAndTime.DateValue(lblDate.Text).Year < 1900)
				{
					FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");

					return false;
				}

				if (modGlobal.Statics.gboolMultipleTowns)
				{
					lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbTownCode.Items[cmbTownCode.SelectedIndex].ToString(), 2))));
				}
				else
				{
					lngTownCode = 0;
				}

				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex))))));
				lngPeriod = cmbPeriod.SelectedIndex >= 0 ? cmbPeriod.SelectedIndex + 1 : 4;
				dtTSDate = FCConvert.ToDateTime(txtDueDate.Text);

				//var start = DateTime.Now;

				ShowWait( true, true, "Please Wait..." + "\r\n" + "Processing Accounts" , true);
                Application.StartTask(() =>
                {
                    try
                    {
                        var extractData = taxExtractService.GetExtractData(
                            modStatusPayments.Statics.boolRE ? PropertyTaxBillType.Real : PropertyTaxBillType.Personal,
                            lngYear, lngPeriod, dtTSDate, lngTownCode);

                        //var end = DateTime.Now;
                        //TimeSpan duration = end.Subtract(start);

                        //FCMessageBox.Show(duration.ToString());
                        EndWait();
                        if (extractData.Any())
                        {
                            CreateExtractFile(extractData, lngYear, lngPeriod, boolZip, lngTownCode);
                            rptTaxServiceSummary.InstancePtr.Init(DateAndTime.DateValue(lblDate.Text), extractData);
                            return true;
                        }
                        else
                        {
                            if (!cancelled)
                            {
                                FCMessageBox.Show("There are no eligible accounts.", MsgBoxStyle.Exclamation,
                                    "No Accounts");
                            }

                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
						EndWait();
                        FCMessageBox.Show(ex.Message, MsgBoxStyle.Critical, "Error");
                    }

                    return false;
                });
            }
			catch (Exception ex)
			{
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Extract");
			}

			return false;
		}

		private bool CreateExtractFile(IEnumerable<TaxServiceExtractRecord> extractData, int billedYear, int period, bool zipFile, int townCode)
        {
            var path = StaticSettings.gGlobalSettings.GlobalDataDirectory;

	        try
	        {
		        bool boolEmpty = false;
		        string strFileNamed = "";
		        string strZipFile = "";
		        string strBaseFileName = "";

		        if (Strings.Right(path, 1) == "\\")
		        {
                    path = Strings.Left(path, path.Length - 1);
		        }

				try
				{
					FCFileSystem.FileOpen(38, path + "\\TSCoreLogic.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					strFileNamed = path + "\\TSCoreLogic.ASC";
					strZipFile = path + "\\TSCoreLogic.ZIP";
					strBaseFileName = "TSCoreLogic.zip";
					FCFileSystem.FileOpen(39, path + "\\TTCoreLogic.CSV", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);

					foreach (var record in extractData)
					{
						
						if (record.PaidFlag != "PD" || record.Year == billedYear)
                        {
                            var fixedLengthRecordToAdd = record.ToFixedLengthRecordFormat();
                            var commaDelimitedRecordToAdd = record.ToCommaDelimitedRecordFormat();

							FCFileSystem.PrintLine(38, fixedLengthRecordToAdd);
                            FCFileSystem.Write(39, commaDelimitedRecordToAdd);
						}
					}

					FCFileSystem.FileClose(38);
					FCFileSystem.FileClose(39);

					
					if (File.Exists(strZipFile))
					{
						File.Delete(strZipFile);
					}

					var zip = fecherFoundation.ZipFile.Open(strZipFile, System.IO.Compression.ZipArchiveMode.Create);
					zip.CreateEntryFromFile(System.IO.Path.Combine(path, "TSCoreLogic.ASC"), "TSCoreLogic.ASC", System.IO.Compression.CompressionLevel.Optimal);
					zip.Dispose();
					FCUtils.Download(strZipFile, strBaseFileName);

                    taxExtractService.UpdateLastExtractRunInfo(billedYear, period);

					if (!zipFile)
					{
						FCMessageBox.Show("Extract completed successfully.", MsgBoxStyle.Information, "Success");
                    }

					if (FCMessageBox.Show("Would you like to E-mail this file to CoreLogic?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Email") == DialogResult.Yes)
					{
						modTaxService.EmailFirstAmerican(strZipFile, strTownEmail, false, townCode);
					}

					return true;
				}
				catch (Exception ex)
				{
					FCMessageBox.Show("Error #"
									  + FCConvert.ToString(Information.Err(ex)
																	  .Number)
									  + " - "
									  + ex.GetBaseException()
										  .Message
									  + ".", MsgBoxStyle.Critical, "Error Creating Extract");
					FCMessageBox.Show("Extract not completed.", MsgBoxStyle.Information, "ERROR");
				}
			}
	        finally
	        {
		        FCFileSystem.FileClose(38);
		        FCFileSystem.FileClose(39);
	        }

            return false;
        }

		//private bool CreateExtract(bool boolZip = false)
		//{
		//	bool CreateExtract = false;
		//	// this will fill calculate the accounts and put it in a grid
		//	clsDRWrapper rsLien = new clsDRWrapper();

		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		int lngYear = 0;
		//		int lngPeriod;
		//		string strTSFilePath = "";

		//		// VBto upgrade warning: dtTSDate As DateTime	OnWrite(string)
		//		DateTime dtTSDate;
		//		string strTownCode = "";
		//		int lngTownCode = 0;

		//		if (modGlobal.Statics.gboolMultipleTowns)
		//		{
		//			strTownCode = " AND Trancode = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbTownCode.Items[cmbTownCode.SelectedIndex].ToString(), 2)));
		//			lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbTownCode.Items[cmbTownCode.SelectedIndex].ToString(), 2))));
		//		}
		//		else
		//		{
		//			strTownCode = "";
		//			lngTownCode = 0;
		//		}

		//		if (cmbYear.SelectedIndex >= 0)
		//		{
		//			// get the year
		//			lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex))))));

		//			// get the period
		//			if (cmbPeriod.SelectedIndex == 0)
		//			{
		//				lngPeriod = 1;
		//			}
		//			else
		//			{
		//				if (cmbPeriod.SelectedIndex == 1)
		//				{
		//					lngPeriod = 2;
		//				}
		//				else
		//				{
		//					if (cmbPeriod.SelectedIndex == 2)
		//					{
		//						lngPeriod = 3;
		//					}
		//					else
		//					{
		//						lngPeriod = 4;
		//					}
		//				}
		//			}

		//			strTSFilePath = FCFileSystem.Statics.UserDataFolder;

		//			if (Strings.Trim(lblDate.Text) == "")
		//			{
		//				FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");

		//				return false;
		//			}

		//			if (!Information.IsDate(lblDate.Text))
		//			{
		//				FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");

		//				return false;
		//			}
		//			else
		//				if (DateAndTime.DateValue(lblDate.Text).ToOADate() == 0 || DateAndTime.DateValue(lblDate.Text).Year < 1900)
		//			{
		//				FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");

		//				return false;
		//			}

		//                  var start = DateTime.Now;

		//			dtTSDate = FCConvert.ToDateTime(txtDueDate.Text);
		//			rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);

		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				rsAccounts.OpenRecordset("SELECT Distinct Account FROM BillingMaster WHERE BillingYear <= " + FCConvert.ToString(lngYear) + "9 AND BillingType = 'RE' " + strTownCode + " order by account");
		//			}
		//			else
		//			{
		//				rsAccounts.OpenRecordset("SELECT Distinct Account FROM BillingMaster WHERE BillingYear <= " + FCConvert.ToString(lngYear) + "9 AND BillingType = 'PP' " + strTownCode + " order by account");
		//			}

		//			if (!rsAccounts.EndOfFile())
		//			{
		//				// MAL@20081203: Add check for multiple towns for e-mail subject
		//				// Tracker Reference: 15673
		//				int temp1 = 0;
		//				int temp2 = 0;

		//				modTaxService.CreateExtractFile(rsAccounts, ref strTSFilePath, lngYear, FCConvert.ToInt16(lngPeriod), dtTSDate, true, ref temp1, ref temp2, boolZip, "", strTownEmail, lngTownCode);
		//				CreateExtract = true;
		//                      var end = DateTime.Now;

		//                      TimeSpan duration = end.Subtract(start);

		//                      FCMessageBox.Show(duration.ToString());
		//				// show Tax Service Summary
		//				//rptTaxServiceSummary.InstancePtr.Init(strTSFilePath);
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("There are no eligible accounts.", MsgBoxStyle.Exclamation, "No Accounts");
		//			}
		//		}
		//		else
		//		{
		//			// no year entered
		//			FCMessageBox.Show("Please select a valid year.", MsgBoxStyle.Exclamation, "Select Year");

		//			if (cmbYear.Enabled && cmbYear.Visible)
		//			{
		//				cmbYear.Focus();
		//			}
		//		}

		//		return CreateExtract;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		CreateExtract = false;
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Extract");
		//	}
		//	finally
		//	{
		//		rsLien.DisposeOf();
		//		frmWait.InstancePtr.Unload();

		//	}
		//	return CreateExtract;
		//}

		private bool FillYearCombo()
		{
			bool FillYearCombo = false;
			// this will fill the year combo with eligible years
			clsDRWrapper rsYear = new clsDRWrapper();
			rsYear.OpenRecordset("SELECT Distinct (BillingYear / 10) AS [Year] FROM BillingMaster ORDER BY BillingYear / 10 desc", modExtraModules.strCLDatabase);
			while (!rsYear.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				cmbYear.AddItem(FCConvert.ToString(rsYear.Get_Fields("Year")));
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields("Year")));
				rsYear.MoveNext();
			}
			if (cmbYear.Items.Count > 0)
			{
				FillYearCombo = true;
			}
			else
			{
				FillYearCombo = false;
			}
			return FillYearCombo;
		}

		private void SetToolTipText()
		{
			// this routine will set the tool tip text for all the fields on the screen
			ToolTip1.SetToolTip(txtDate, "Date that the interest will be calculated until.");
			ToolTip1.SetToolTip(lblDate, "Date that the interest will be calculated until.");
			txtDate.ToolTipText = "Date that the interest will be calculated until.";
			ToolTip1.SetToolTip(lblYear, "Select the year to extract.");
			ToolTip1.SetToolTip(cmbYear, "Select the year to extract.");
		}

		private void CheckInterestDate_2(bool boolYear)
		{
			CheckInterestDate(ref boolYear);
		}

		private void CheckInterestDate(ref bool boolYear)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will check to see if a year and period are choosen
				// if so, then it will calculate the correct date and enter it into the
				// lblDate object  if not then it will blank out the date
				// VBto upgrade warning: lngYear As int	OnWrite(string)
				int lngYear = 0;
				int intPeriod = 0;
				clsDRWrapper rsDate = new clsDRWrapper();
				int intPer;
				if (cmbYear.SelectedIndex >= 0)
				{
					// is a year selected
					lngYear = FCConvert.ToInt32(cmbYear.Items[cmbYear.SelectedIndex].ToString());
					if (lngYear > 1950)
					{
						// is the year > 1950
						if (cmbPeriod.SelectedIndex == 0)
						{
							// is a period is selected?
							intPeriod = 1;
						}
						else if (cmbPeriod.SelectedIndex == 1)
						{
							intPeriod = 2;
						}
						else if (cmbPeriod.SelectedIndex == 2)
						{
							intPeriod = 3;
						}
						else if (cmbPeriod.SelectedIndex == 3)
						{
							intPeriod = 4;
						}
						else
						{
							lblDate.Text = "";
							return;
						}
					}
					else
					{
						lblDate.Text = "";
						return;
					}
				}
				else
				{
					lblDate.Text = "";
					return;
				}
				if (boolYear)
				{
					rsDate.OpenRecordset("SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(lngYear) + " AND (RateType = 'R')");
					// OR RateType = 'S');
					if (!rsDate.EndOfFile())
					{
						if (rsDate.RecordCount() == 1)
						{
							lngRateKey = FCConvert.ToInt32(rsDate.Get_Fields_Int32("ID"));
						}
						else if (rsDate.RecordCount() > 1)
						{
							lngRateKey = frmSelectRateKey.InstancePtr.Init(ref rsDate, "Select Primary Rate Key");
							rsDate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey));
						}
					}
				}
				else
				{
					rsDate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey));
				}
				// find the rate record and the dates
				if (!rsDate.EndOfFile())
				{
					if (boolYear)
					{
						//FC:FINAL:CHN: Incorrect working of ComboBox after redesign from RadioButtons.
						var oldSelectedIndex = cmbPeriod.SelectedIndex;
						cmbPeriod.Clear();
						for (intPer = 1; intPer <= 4; intPer++)
						{
							if (rsDate.Get_Fields_Int16("NumberOfPeriods") >= intPer)
							{
								cmbPeriod.Items.Add(intPer);
							}
						}
						if (cmbPeriod.Items.Count > oldSelectedIndex)
						{
							cmbPeriod.SelectedIndex = oldSelectedIndex;
						}
						else if (cmbPeriod.Items.Count > 0)
						{
							cmbPeriod.SelectedIndex = 0;
						}
					
					}
					// MAL@20080729: Changed to show the Interest Start Date on the form and add the Due Date to a hidden textbox
					// Tracker Reference: 14849
					if (intPeriod == 1)
					{
						// use the correct due date from the rate record
						// lblDate.Text = Format(rsDate.Get_Fields("DueDate1"), "MM/dd/yyyy")
						lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy");
						// for call 65211 03/16/2005
						txtDueDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
					}
					else if (intPeriod == 2)
					{
						if (rsDate.Get_Fields_DateTime("DueDate2") is DateTime)
						{
							// lblDate.Text = Format(rsDate.Get_Fields("DueDate2"), "MM/dd/yyyy")
							lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yyyy");
							txtDueDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");
						}
						else
						{
							FCMessageBox.Show("Period " + FCConvert.ToString(intPeriod) + " has no due date please use another period.", MsgBoxStyle.Exclamation, "Invalid Due Date");
							cmbPeriod.SelectedIndex = 0;
						}
					}
					else if (intPeriod == 3)
					{
						if (rsDate.Get_Fields_DateTime("DueDate3") is DateTime)
						{
							// lblDate.Text = Format(rsDate.Get_Fields("DueDate3"), "MM/dd/yyyy")
							lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("InterestStartDate3"), "MM/dd/yyyy");
							txtDueDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy");
						}
						else
						{
							FCMessageBox.Show("Period " + FCConvert.ToString(intPeriod) + " has no due date please use another period.", MsgBoxStyle.Exclamation, "Invalid Due Date");
							cmbPeriod.SelectedIndex = 0;
						}
					}
					else if (intPeriod == 4)
					{
						if (rsDate.Get_Fields_DateTime("DueDate4") is DateTime)
						{
							// lblDate.Text = Format(rsDate.Get_Fields("DueDate4"), "MM/dd/yyyy")
							lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("InterestStartDate4"), "MM/dd/yyyy");
							txtDueDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate4"), "MM/dd/yyyy");
						}
						else
						{
							FCMessageBox.Show("Period " + FCConvert.ToString(intPeriod) + " has no due date please use another period.", MsgBoxStyle.Exclamation, "Invalid Due Date");
							cmbPeriod.SelectedIndex = 0;
						}
					}
				}
				else
				{
					FCMessageBox.Show("There is no rate record for this year.", MsgBoxStyle.Exclamation, "Missing Rate Record");
					lblDate.Text = "";
					lngRateKey = 0;
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				lblDate.Text = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Interest Date");
			}
		}

		private void optPeriod_Click(int Index, object sender, System.EventArgs e)
		{
			CheckInterestDate_2(false);
		}

		private void optPeriod_Click(object sender, System.EventArgs e)
		{
			int index = cmbPeriod.SelectedIndex;
			optPeriod_Click(index, sender, e);
		}

		private void FillTownCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRC = new clsDRWrapper();
				cmbTownCode.Clear();
				// rsRC.OpenRecordset "SELECT * FROM Type WHERE TypeCode = " & lngTypeNumber & " ORDER BY TownKey", strCRDatabase
				rsRC.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 0 ORDER BY TownNumber", "CentralData");
				while (!rsRC.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					cmbTownCode.AddItem(Strings.Format(rsRC.Get_Fields("TownNumber"), "00") + " - " + Strings.Trim(FCConvert.ToString(rsRC.Get_Fields_String("TownName"))));
					rsRC.MoveNext();
				}
				App.DoEvents();
				if (cmbTownCode.Items.Count > 1)
				{
					cmbTownCode.Visible = true;
					cmbTownCode.SelectedIndex = 0;
				}
				else
				{
					cmbTownCode.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Town Combo");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileCreateZip_Click(sender, e);
		}
	}
}
