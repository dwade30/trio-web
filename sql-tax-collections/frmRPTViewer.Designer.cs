﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRPTViewer.
	/// </summary>
	partial class frmRPTViewer : BaseForm
	{
		public fecherFoundation.FCFrame fraRecreateReport;
		public fecherFoundation.FCComboBox cmbRecreate;
		public fecherFoundation.FCLabel lblRecreateInstructions;
		public fecherFoundation.FCFrame fraNumber;
		public fecherFoundation.FCComboBox cmbNumber;
		public fecherFoundation.FCLabel lblInstruction;
		public ARViewer arView;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		//public fecherFoundation.FCToolStripMenuItem mnuFileEmail;
		//public fecherFoundation.FCToolStripMenuItem mnuEMailRTF;
		//public fecherFoundation.FCToolStripMenuItem mnuEmailPDF;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExport;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExportRTF;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExportPDF;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExportHTML;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExportExcel;
		//public fecherFoundation.FCToolStripMenuItem mnuFileSpacer;
		//public fecherFoundation.FCToolStripMenuItem mnuFileChange;
		//public fecherFoundation.FCToolStripMenuItem mnuFileSwitch;
		//public fecherFoundation.FCToolStripMenuItem mnuFileRecreate;
		//public fecherFoundation.FCToolStripMenuItem mnuFileSave;
        public fecherFoundation.FCButton cmdFileRecreate;
        public fecherFoundation.FCButton cmdChangeReport;
        public fecherFoundation.FCButton cmdProcess;
		//public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRPTViewer));
			this.fraRecreateReport = new fecherFoundation.FCFrame();
			this.cmbRecreate = new fecherFoundation.FCComboBox();
			this.lblRecreateInstructions = new fecherFoundation.FCLabel();
			this.fraNumber = new fecherFoundation.FCFrame();
			this.cmbNumber = new fecherFoundation.FCComboBox();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.arView = new Global.ARViewer();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.toolBar1 = new Wisej.Web.ToolBar();
			this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportHTML = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportExcel = new Wisej.Web.ToolBarButton();
			this.toolBarButtonPrint = new Wisej.Web.ToolBarButton();
			this.cmdFileRecreate = new fecherFoundation.FCButton();
			this.cmdChangeReport = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecreateReport)).BeginInit();
			this.fraRecreateReport.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNumber)).BeginInit();
			this.fraNumber.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRecreate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdChangeReport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 498);
			this.BottomPanel.Size = new System.Drawing.Size(630, 100);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.fraNumber);
			this.ClientArea.Controls.Add(this.fraRecreateReport);
			this.ClientArea.Controls.Add(this.arView);
			this.ClientArea.Controls.Add(this.toolBar1);
			this.ClientArea.Location = new System.Drawing.Point(0, 55);
			this.ClientArea.Size = new System.Drawing.Size(650, 495);
			this.ClientArea.Controls.SetChildIndex(this.toolBar1, 0);
			this.ClientArea.Controls.SetChildIndex(this.arView, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraRecreateReport, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdProcess, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdChangeReport);
			this.TopPanel.Controls.Add(this.cmdFileRecreate);
			this.TopPanel.Size = new System.Drawing.Size(650, 55);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileRecreate, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdChangeReport, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(245, 30);
			this.HeaderText.Text = "Redisplay Daily Audit";
			// 
			// fraRecreateReport
			// 
			this.fraRecreateReport.Controls.Add(this.cmbRecreate);
			this.fraRecreateReport.Controls.Add(this.lblRecreateInstructions);
			this.fraRecreateReport.Location = new System.Drawing.Point(18, 64);
			this.fraRecreateReport.Name = "fraRecreateReport";
			this.fraRecreateReport.Size = new System.Drawing.Size(307, 103);
			this.fraRecreateReport.TabIndex = 2;
			this.fraRecreateReport.Visible = false;
			// 
			// cmbRecreate
			// 
			this.cmbRecreate.BackColor = System.Drawing.SystemColors.Window;
			this.cmbRecreate.Location = new System.Drawing.Point(30, 55);
			this.cmbRecreate.Name = "cmbRecreate";
			this.cmbRecreate.Size = new System.Drawing.Size(228, 40);
			this.cmbRecreate.TabIndex = 1;
			// 
			// lblRecreateInstructions
			// 
			this.lblRecreateInstructions.AutoSize = true;
			this.lblRecreateInstructions.Location = new System.Drawing.Point(30, 25);
			this.lblRecreateInstructions.Name = "lblRecreateInstructions";
			this.lblRecreateInstructions.Size = new System.Drawing.Size(278, 16);
			this.lblRecreateInstructions.TabIndex = 2;
			this.lblRecreateInstructions.Text = "CHOOSE THE REPORT NUMBER TO VIEW";
			this.lblRecreateInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraNumber
			// 
			this.fraNumber.Controls.Add(this.cmbNumber);
			this.fraNumber.Controls.Add(this.lblInstruction);
			this.fraNumber.Location = new System.Drawing.Point(626, 328);
			this.fraNumber.Name = "fraNumber";
			this.fraNumber.Size = new System.Drawing.Size(305, 103);
			this.fraNumber.TabIndex = 1;
			// 
			// cmbNumber
			// 
			this.cmbNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbNumber.Location = new System.Drawing.Point(30, 55);
			this.cmbNumber.Name = "cmbNumber";
			this.cmbNumber.Size = new System.Drawing.Size(126, 40);
			this.cmbNumber.TabIndex = 1;
			// 
			// lblInstruction
			// 
			this.lblInstruction.AutoSize = true;
			this.lblInstruction.Location = new System.Drawing.Point(30, 25);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(278, 16);
			this.lblInstruction.TabIndex = 2;
			this.lblInstruction.Text = "CHOOSE THE REPORT NUMBER TO VIEW";
			this.lblInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// arView
			// 
			this.arView.Dock = Wisej.Web.DockStyle.Fill;
			this.arView.Location = new System.Drawing.Point(0, 40);
			this.arView.Name = "arView";
			this.arView.ScrollBars = false;
			this.arView.Size = new System.Drawing.Size(633, 438);
			this.arView.TabIndex = 2;
			this.arView.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
			// 
			// toolBar1
			// 
			this.toolBar1.AutoSize = false;
			this.toolBar1.BackColor = System.Drawing.Color.FromArgb(244, 247, 249);
			this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
            this.toolBarButtonEmailPDF,
            this.toolBarButtonEmailRTF,
            this.toolBarButtonExportPDF,
            this.toolBarButtonExportRTF,
            this.toolBarButtonExportHTML,
            this.toolBarButtonExportExcel,
            this.toolBarButtonPrint});
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.Size = new System.Drawing.Size(633, 40);
			this.toolBar1.TabIndex = 0;
			this.toolBar1.TabStop = false;
			this.toolBar1.ButtonClick += new Wisej.Web.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
			// 
			// toolBarButtonEmailPDF
			// 
			this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
			this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
			this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
			// 
			// toolBarButtonEmailRTF
			// 
			this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
			this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
			this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
			// 
			// toolBarButtonExportPDF
			// 
			this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
			this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
			this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
			// 
			// toolBarButtonExportRTF
			// 
			this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
			this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
			this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
			// 
			// toolBarButtonExportHTML
			// 
			this.toolBarButtonExportHTML.ImageSource = "icon-report-export-html";
			this.toolBarButtonExportHTML.Name = "toolBarButtonExportHTML";
			this.toolBarButtonExportHTML.ToolTipText = "Export as HTML";
			// 
			// toolBarButtonExportExcel
			// 
			this.toolBarButtonExportExcel.ImageSource = "icon-report-export-excel";
			this.toolBarButtonExportExcel.Name = "toolBarButtonExportExcel";
			this.toolBarButtonExportExcel.ToolTipText = "Export as Excel";
			// 
			// toolBarButtonPrint
			// 
			this.toolBarButtonPrint.ImageSource = "icon-report-print";
			this.toolBarButtonPrint.Name = "toolBarButtonPrint";
			this.toolBarButtonPrint.ToolTipText = "Print";
			// 
			// cmdFileRecreate
			// 
			this.cmdFileRecreate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileRecreate.Location = new System.Drawing.Point(499, 0);
			this.cmdFileRecreate.Name = "cmdFileRecreate";
			this.cmdFileRecreate.Size = new System.Drawing.Size(148, 24);
			this.cmdFileRecreate.TabIndex = 1;
			this.cmdFileRecreate.Text = "Recreate Daily Audit";
			this.cmdFileRecreate.Click += new System.EventHandler(this.mnuFileRecreate_Click);
			// 
			// cmdChangeReport
			// 
			this.cmdChangeReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdChangeReport.Location = new System.Drawing.Point(341, 0);
			this.cmdChangeReport.Name = "cmdChangeReport";
			this.cmdChangeReport.Size = new System.Drawing.Size(148, 24);
			this.cmdChangeReport.TabIndex = 2;
			this.cmdChangeReport.Text = "Change Report";
			this.cmdChangeReport.Click += new System.EventHandler(this.mnuFileChange_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(700, 450);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(157, 48);
			this.cmdProcess.TabIndex = 4;
			this.cmdProcess.Text = "Save & Continue";
			this.cmdProcess.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmRPTViewer
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(650, 550);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmRPTViewer";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Redisplay Last Daily Audit Report";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmRPTViewer_Load);
			this.Activated += new System.EventHandler(this.frmRPTViewer_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRPTViewer_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecreateReport)).EndInit();
			this.fraRecreateReport.ResumeLayout(false);
			this.fraRecreateReport.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNumber)).EndInit();
			this.fraNumber.ResumeLayout(false);
			this.fraNumber.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRecreate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdChangeReport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion


        private ToolBar toolBar1;
        private ToolBarButton toolBarButtonEmailPDF;
        private ToolBarButton toolBarButtonExportPDF;
        private ToolBarButton toolBarButtonEmailRTF;
        private ToolBarButton toolBarButtonExportRTF;
        private ToolBarButton toolBarButtonExportHTML;
        private ToolBarButton toolBarButtonExportExcel;
        private ToolBarButton toolBarButtonPrint;
    }
}
