﻿using fecherFoundation;

namespace TWCL0000
{
	partial class MDIParent
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            fecherFoundation.FCMainFormModule fcMainFormModule1 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule2 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule3 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule4 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule5 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule6 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule7 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule8 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule9 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule10 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule11 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule12 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule13 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule14 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule15 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule16 = new fecherFoundation.FCMainFormModule();
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.label1 = new Wisej.Web.Label();
            this.pictureBox1 = new Wisej.Web.PictureBox();
            this.label2 = new Wisej.Web.Label();
            this.navigationPanel.SuspendLayout();
            this.favouritesPanel.SuspendLayout();
            this.homePagePanel.SuspendLayout();
            this.modulesPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.centerPanel.SuspendLayout();
            this.aboutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // navigationPanel
            // 
            this.navigationPanel.Size = new System.Drawing.Size(300, 480);
            // 
            // menuTree
            // 
            this.menuTree.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.menuTree.Location = new System.Drawing.Point(0, 0);
            this.menuTree.Margin = new Wisej.Web.Padding(0);
            this.menuTree.Size = new System.Drawing.Size(300, 425);
            // 
            // favouritesPanel
            // 
            this.favouritesPanel.Size = new System.Drawing.Size(60, 480);
            // 
            // homePageButton
            // 
            this.homePageButton.AppearanceKey = "FlatButton";
            this.homePageButton.Display = Wisej.Web.Display.Icon;
            this.homePageButton.Image = global::TWCL0000.Properties.Resources.icon_all_apps1;
            this.homePageButton.Location = new System.Drawing.Point(11, 20);
            this.homePageButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.homePageButton.TextImageRelation = Wisej.Web.TextImageRelation.Overlay;
            // 
            // homePagePanel
            // 
            this.homePagePanel.Size = new System.Drawing.Size(630, 480);
            // 
            // modulesPanel
            // 
            this.modulesPanel.Location = new System.Drawing.Point(95, 20);
            this.modulesPanel.Size = new System.Drawing.Size(800, 635);
            this.modulesPanel.Controls.SetChildIndex(this.pinnedModulesLabel, 0);
            // 
            // topPanel
            // 
            this.topPanel.Size = new System.Drawing.Size(630, 84);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(0, 344);
            this.bottomPanel.Size = new System.Drawing.Size(630, 136);
            // 
            // centerPanel
            // 
            this.centerPanel.Size = new System.Drawing.Size(630, 260);
            // 
            // pinnedModulesLabel
            // 
            this.pinnedModulesLabel.Location = new System.Drawing.Point(20, 0);
            // 
            // aboutPanel
            // 
            this.aboutPanel.Controls.Add(this.label2);
            this.aboutPanel.Controls.Add(this.pictureBox1);
            this.aboutPanel.Controls.Add(this.label1);
            this.aboutPanel.Location = new System.Drawing.Point(320, 17);
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Color = System.Drawing.Color.Black;
            this.CommonDialog1.DefaultExt = null;
            this.CommonDialog1.FilterIndex = ((short)(0));
            this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CommonDialog1.FontName = "Microsoft Sans Serif";
            this.CommonDialog1.FontSize = 8.25F;
            this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
            this.CommonDialog1.FromPage = 0;
            this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.PrinterSettings = null;
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            this.CommonDialog1.TabIndex = 0;
            this.CommonDialog1.ToPage = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Proxima Nova Regular", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(118)))));
            this.label1.Location = new System.Drawing.Point(135, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "A PRODUCT OF";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TWCL0000.Properties.Resources.harris_logo;
            this.pictureBox1.Location = new System.Drawing.Point(88, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 62);
            this.pictureBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(165)))), ((int)(((byte)(165)))));
            this.label2.Location = new System.Drawing.Point(40, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(293, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "© 2017 Harris Local Government. All rights reserved";
            // 
            // MDIParent
            // 
            this.ClientSize = new System.Drawing.Size(990, 480);
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
            fcMainFormModule1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(105)))), ((int)(((byte)(255)))));
            fcMainFormModule1.Image = null;
            fcMainFormModule1.IsPinned = true;
            fcMainFormModule1.PinnedImage = global::TWCL0000.Properties.Resources.icon_real_estate;
            fcMainFormModule1.Text = "Real Estate";
            fcMainFormModule2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(227)))), ((int)(((byte)(194)))));
            fcMainFormModule2.Image = null;
            fcMainFormModule2.IsPinned = true;
            fcMainFormModule2.PinnedImage = global::TWCL0000.Properties.Resources.icon_personal_property;
            fcMainFormModule2.Text = "Personal Property";
            fcMainFormModule3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(166)))), ((int)(((byte)(35)))));
            fcMainFormModule3.Image = null;
            fcMainFormModule3.IsPinned = false;
            fcMainFormModule3.PinnedImage = global::TWCL0000.Properties.Resources.icon_tax_billing;
            fcMainFormModule3.Text = "Tax Billing";
            fcMainFormModule4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(165)))), ((int)(((byte)(214)))));
            fcMainFormModule4.Image = null;
            fcMainFormModule4.IsPinned = false;
            fcMainFormModule4.PinnedImage = global::TWCL0000.Properties.Resources.icon_personal_property_collections;
            fcMainFormModule4.Text = "Personal Property Collections";
            fcMainFormModule5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(72)))), ((int)(((byte)(134)))));
            fcMainFormModule5.Image = null;
            fcMainFormModule5.IsPinned = false;
            fcMainFormModule5.PinnedImage = global::TWCL0000.Properties.Resources.icon_clerk;
            fcMainFormModule5.Text = "Clerk";
            fcMainFormModule6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(202)))), ((int)(((byte)(123)))));
            fcMainFormModule6.Image = null;
            fcMainFormModule6.IsPinned = false;
            fcMainFormModule6.PinnedImage = global::TWCL0000.Properties.Resources.icon_code_enforcement;
            fcMainFormModule6.Text = "Code Enforcement";
            fcMainFormModule7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(191)))), ((int)(((byte)(74)))));
            fcMainFormModule7.Image = null;
            fcMainFormModule7.IsPinned = false;
            fcMainFormModule7.PinnedImage = global::TWCL0000.Properties.Resources.icon_budgetary_system;
            fcMainFormModule7.Text = "Budgetary System";
            fcMainFormModule8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(109)))), ((int)(((byte)(96)))));
            fcMainFormModule8.Image = null;
            fcMainFormModule8.IsPinned = false;
            fcMainFormModule8.PinnedImage = global::TWCL0000.Properties.Resources.icon_cash_receipting;
            fcMainFormModule8.Text = "Cash Receipting";
            fcMainFormModule9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(154)))), ((int)(((byte)(169)))));
            fcMainFormModule9.Image = null;
            fcMainFormModule9.IsPinned = false;
            fcMainFormModule9.PinnedImage = global::TWCL0000.Properties.Resources.icon_utility_billing;
            fcMainFormModule9.Text = "Utillity Billing";
            fcMainFormModule10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(112)))), ((int)(((byte)(254)))));
            fcMainFormModule10.Image = null;
            fcMainFormModule10.IsPinned = false;
            fcMainFormModule10.PinnedImage = global::TWCL0000.Properties.Resources.icon_real_estate_collections;
            fcMainFormModule10.Text = "Real Estate Collections";
            fcMainFormModule11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(84)))), ((int)(((byte)(186)))));
            fcMainFormModule11.Image = null;
            fcMainFormModule11.IsPinned = false;
            fcMainFormModule11.PinnedImage = global::TWCL0000.Properties.Resources.icon_blue_book;
            fcMainFormModule11.Text = "Blue Book";
            fcMainFormModule12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(122)))), ((int)(((byte)(189)))));
            fcMainFormModule12.Image = null;
            fcMainFormModule12.IsPinned = false;
            fcMainFormModule12.PinnedImage = global::TWCL0000.Properties.Resources.icon_motor_vehicle_registration;
            fcMainFormModule12.Text = "Motor Vehicle Registration";
            fcMainFormModule13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(201)))), ((int)(((byte)(158)))));
            fcMainFormModule13.Image = null;
            fcMainFormModule13.IsPinned = false;
            fcMainFormModule13.PinnedImage = global::TWCL0000.Properties.Resources.icon_enhanced_911;
            fcMainFormModule13.Text = "Enhanced 911";
            fcMainFormModule14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(87)))), ((int)(((byte)(42)))));
            fcMainFormModule14.Image = null;
            fcMainFormModule14.IsPinned = false;
            fcMainFormModule14.PinnedImage = global::TWCL0000.Properties.Resources.icon_fixed_assets;
            fcMainFormModule14.Text = "Fixed Assets";
            fcMainFormModule15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(99)))), ((int)(((byte)(75)))));
            fcMainFormModule15.Image = null;
            fcMainFormModule15.IsPinned = false;
            fcMainFormModule15.PinnedImage = global::TWCL0000.Properties.Resources.icon_accounts_receivable;
            fcMainFormModule15.Text = "Accounts Receivable";
            fcMainFormModule16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(69)))), ((int)(((byte)(146)))));
            fcMainFormModule16.Image = null;
            fcMainFormModule16.IsPinned = false;
            fcMainFormModule16.PinnedImage = global::TWCL0000.Properties.Resources.icon_payroll;
            fcMainFormModule16.Text = "Payroll";
            this.Modules.Add(fcMainFormModule1);
            this.Modules.Add(fcMainFormModule2);
            this.Modules.Add(fcMainFormModule3);
            this.Modules.Add(fcMainFormModule4);
            this.Modules.Add(fcMainFormModule5);
            this.Modules.Add(fcMainFormModule6);
            this.Modules.Add(fcMainFormModule7);
            this.Modules.Add(fcMainFormModule8);
            this.Modules.Add(fcMainFormModule9);
            this.Modules.Add(fcMainFormModule10);
            this.Modules.Add(fcMainFormModule11);
            this.Modules.Add(fcMainFormModule12);
            this.Modules.Add(fcMainFormModule13);
            this.Modules.Add(fcMainFormModule14);
            this.Modules.Add(fcMainFormModule15);
            this.Modules.Add(fcMainFormModule16);
            this.Name = "MDIParent";
            this.Text = "Window";
            this.WindowState = Wisej.Web.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.navigationPanel.ResumeLayout(false);
            this.favouritesPanel.ResumeLayout(false);
            this.homePagePanel.ResumeLayout(false);
            this.modulesPanel.ResumeLayout(false);
            this.modulesPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.centerPanel.ResumeLayout(false);
            this.aboutPanel.ResumeLayout(false);
            this.aboutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private Wisej.Web.Label label1;
        private Wisej.Web.PictureBox pictureBox1;
        private Wisej.Web.Label label2;
    }
}
