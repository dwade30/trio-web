﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmEditBookPage.
	/// </summary>
	partial class frmEditBookPage : BaseForm
	{
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCTextBox txtPage;
		public fecherFoundation.FCTextBox txtBook;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblPage;
		public fecherFoundation.FCLabel lblBook;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditBookPage));
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.txtPage = new fecherFoundation.FCTextBox();
			this.txtBook = new fecherFoundation.FCTextBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblPage = new fecherFoundation.FCLabel();
			this.lblBook = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdFileGetAccount = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileGetAccount)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 338);
			this.BottomPanel.Size = new System.Drawing.Size(510, 22);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.cmbYear);
			this.ClientArea.Controls.Add(this.txtPage);
			this.ClientArea.Controls.Add(this.txtBook);
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblPage);
			this.ClientArea.Controls.Add(this.lblBook);
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Location = new System.Drawing.Point(0, 61);
			this.ClientArea.Size = new System.Drawing.Size(510, 277);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileGetAccount);
			this.TopPanel.Size = new System.Drawing.Size(510, 61);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileGetAccount, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(181, 30);
			this.HeaderText.Text = "Edit Book Page";
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(134, 125);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(100, 40);
			this.cmbYear.TabIndex = 2;
			this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(134, 65);
			this.txtAccount.MaxLength = 10;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(100, 40);
			this.txtAccount.TabIndex = 1;
			this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
			this.txtAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAccount_KeyPress);
			this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
			this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
			// 
			// txtPage
			// 
			this.txtPage.AutoSize = false;
			this.txtPage.BackColor = System.Drawing.SystemColors.Window;
			this.txtPage.LinkItem = null;
			this.txtPage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPage.LinkTopic = null;
			this.txtPage.Location = new System.Drawing.Point(381, 125);
			this.txtPage.MaxLength = 6;
			this.txtPage.Name = "txtPage";
			this.txtPage.Size = new System.Drawing.Size(100, 40);
			this.txtPage.TabIndex = 4;
			this.txtPage.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPage_KeyPress);
			this.txtPage.Enter += new System.EventHandler(this.txtPage_Enter);
			// 
			// txtBook
			// 
			this.txtBook.AutoSize = false;
			this.txtBook.BackColor = System.Drawing.SystemColors.Window;
			this.txtBook.LinkItem = null;
			this.txtBook.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBook.LinkTopic = null;
			this.txtBook.Location = new System.Drawing.Point(381, 65);
			this.txtBook.MaxLength = 6;
			this.txtBook.Name = "txtBook";
			this.txtBook.Size = new System.Drawing.Size(100, 40);
			this.txtBook.TabIndex = 3;
			this.txtBook.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBook_KeyPress);
			this.txtBook.Enter += new System.EventHandler(this.txtBook_Enter);
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(30, 25);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(335, 20);
			this.lblName.TabIndex = 9;
			this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 79);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(69, 16);
			this.Label2.TabIndex = 8;
			this.Label2.Text = "ACCOUNT";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 139);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(41, 16);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "YEAR";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPage
			// 
			this.lblPage.AutoSize = true;
			this.lblPage.Location = new System.Drawing.Point(303, 139);
			this.lblPage.Name = "lblPage";
			this.lblPage.Size = new System.Drawing.Size(42, 16);
			this.lblPage.TabIndex = 6;
			this.lblPage.Text = "PAGE";
			this.lblPage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblBook
			// 
			this.lblBook.AutoSize = true;
			this.lblBook.Location = new System.Drawing.Point(303, 79);
			this.lblBook.Name = "lblBook";
			this.lblBook.Size = new System.Drawing.Size(43, 16);
			this.lblBook.TabIndex = 5;
			this.lblBook.Text = "BOOK";
			this.lblBook.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 204);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(99, 48);
			this.btnProcess.TabIndex = 10;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdFileGetAccount
			// 
			this.cmdFileGetAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileGetAccount.AppearanceKey = "toolbarButton";
			this.cmdFileGetAccount.Location = new System.Drawing.Point(363, 29);
			this.cmdFileGetAccount.Name = "cmdFileGetAccount";
			this.cmdFileGetAccount.Size = new System.Drawing.Size(121, 24);
			this.cmdFileGetAccount.TabIndex = 52;
			this.cmdFileGetAccount.Text = "Get Lien Account";
			this.cmdFileGetAccount.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// frmEditBookPage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(510, 360);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmEditBookPage";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Book Page";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmEditBookPage_Load);
			this.Activated += new System.EventHandler(this.frmEditBookPage_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditBookPage_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditBookPage_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileGetAccount)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
		public FCButton cmdFileGetAccount;
	}
}
