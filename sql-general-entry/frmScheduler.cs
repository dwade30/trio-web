﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmScheduler.
	/// </summary>
	public partial class frmScheduler : BaseForm
	{
		public frmScheduler()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmScheduler InstancePtr
		{
			get
			{
				return (frmScheduler)Sys.GetInstance(typeof(frmScheduler));
			}
		}

		protected frmScheduler _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           10/21/2003
		// This form will be used to check out what is happening
		// on a certain month
		// ********************************************************
		int SaturdayCol;
		int SundayCol;
		int MondayCol;
		int TuesdayCol;
		int WednesdayCol;
		int ThursdayCol;
		int FridayCol;
		int WeekOneRow;
		int WeekTwoRow;
		int WeekThreeRow;
		int WeekFourRow;
		int WeekFiveRow;
		int WeekSixRow;
		bool blnEdit;
		int intMonthStartCol;
		int intMonthStartRow;
		int intDaysInMonth;
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime datReturnedDate;
		int KeyCol;
		int DateCol;
		int DescriptionCol;

		private void cboMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FormatCalender();
			LoadSchedule();
		}

		private void cboYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FormatCalender();
			LoadSchedule();
		}

		private void chkViewAll_CheckedChanged(object sender, System.EventArgs e)
		{
			LoadSchedule();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			txtEventDate.Text = "";
			txtEventTitle.Text = "";
			fraEvent.Visible = false;
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdDeleteCancel_Click(object sender, System.EventArgs e)
		{
			txtDeleteEventDate.Text = "";
			fraDeleteEvent.Visible = false;
		}

		public void cmdDeleteCancel_Click()
		{
			cmdDeleteCancel_Click(cmdDeleteCancel, new System.EventArgs());
		}

		private void cmdDeleteDate_Click(object sender, System.EventArgs e)
		{
			// pop up the calender screen and let the user select a date
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtDeleteEventDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdDeleteOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			if (!Information.IsDate(txtDeleteEventDate.Text))
			{
				MessageBox.Show("You must enter a valid date before you may proceed.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtDeleteEventDate.Focus();
				return;
			}
			rsDeleteInfo.OpenRecordset("SELECT * FROM Tasks WHERE TaskDate = '" + FCConvert.ToString(DateAndTime.DateValue(txtDeleteEventDate.Text)) + "' AND TaskPerson = ''", "SystemSettings");
			if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
			{
				rsDeleteInfo.Delete();
                //FC:FINAL:AM:#4061 - add missing Update
                rsDeleteInfo.Update();
				MessageBox.Show("Event Deleted Successfully.", "Event Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cmdDeleteCancel_Click();
				FormatCalender();
				LoadSchedule();
			}
			else
			{
				MessageBox.Show("No events were found for that date.", "No Events", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsEventInfo = new clsDRWrapper();
			if (!Information.IsDate(txtEventDate.Text))
			{
				MessageBox.Show("You must enter a valid date before you may proceed.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEventDate.Focus();
				return;
			}
			else if (Strings.Trim(txtEventTitle.Text) == "")
			{
				MessageBox.Show("You must enter an event title before you may proceed.", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEventTitle.Focus();
				return;
			}
			rsEventInfo.OpenRecordset("SELECT * FROM Tasks WHERE TaskDate = '" + FCConvert.ToString(DateAndTime.DateValue(txtEventDate.Text)) + "' AND TaskPerson = ''", "SystemSettings");
			if (rsEventInfo.EndOfFile() != true && rsEventInfo.BeginningOfFile() != true)
			{
				rsEventInfo.Edit();
			}
			else
			{
				rsEventInfo.AddNew();
			}
			rsEventInfo.Set_Fields("TaskDate", DateAndTime.DateValue(txtEventDate.Text));
			rsEventInfo.Set_Fields("TaskPerson", "");
			rsEventInfo.Set_Fields("TaskDescription", Strings.Trim(txtEventTitle.Text));
			rsEventInfo.Update(false);
			cmdCancel_Click();
			FormatCalender();
			LoadSchedule();
		}

		private void cmdRequestDate_Click(object sender, System.EventArgs e)
		{
			// pop up the calender screen and let the user select a date
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEventDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdSearchCancel_Click(object sender, System.EventArgs e)
		{
			cboMonth.Enabled = true;
			cboYear.Enabled = true;
			vsCalender.Enabled = true;
			fraSelectTown.Visible = false;
		}

		private void cmdSearchOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsResults = new clsDRWrapper();
			rsResults.OpenRecordset("SELECT * FROM Tasks WHERE TaskPerson = '" + modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID() + "' AND TaskDescription LIKE '*" + txtCriteria.Text + "*' ORDER BY TaskDate DESC", "SystemSettings");
			if (rsResults.EndOfFile() != true && rsResults.BeginningOfFile() != true)
			{
				if (rsResults.RecordCount() == 1)
				{
					fraSelectTown.Visible = false;
					OpenWeeklyScheduler_8(Strings.Format(rsResults.Get_Fields_DateTime("TaskDate"), "MM/dd/yyyy"), rsResults.Get_Fields_Int32("ID"));
				}
				else
				{
					vsResults.Rows = 1;
					vsResults.WordWrap = true;
					do
					{
						vsResults.AddItem("");
						vsResults.TextMatrix(vsResults.Rows - 1, KeyCol, FCConvert.ToString(rsResults.Get_Fields_Int32("ID")));
						vsResults.TextMatrix(vsResults.Rows - 1, DateCol, Strings.Format(rsResults.Get_Fields_DateTime("TaskDate"), "MM/dd/yyyy"));
						vsResults.TextMatrix(vsResults.Rows - 1, DescriptionCol, rsResults.Get_Fields_String("TaskPerson") + " - " + rsResults.Get_Fields_String("TaskDescription"));
						rsResults.MoveNext();
					}
					while (rsResults.EndOfFile() != true);
					fraResults.Visible = true;
					fraSelectTown.Visible = false;
					vsResults.AutoSize(DateCol, DescriptionCol);
					vsResults.AutoSize(DateCol, DescriptionCol);
					vsResults.Row = 1;
					vsResults.Focus();
				}
			}
			else
			{
				MessageBox.Show("No reminders were found that matched the search criteria.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		public void cmdSearchOK_Click()
		{
			cmdSearchOK_Click(cmdSearchOK, new System.EventArgs());
		}

		private void frmScheduler_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			if (!modSecurity2.ValidPermissions(this, modSecurity2.CNSTCALENDAR))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Unload();
				return;
			}
			this.Refresh();
		}

		private void frmScheduler_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmScheduler properties;
			//frmScheduler.FillStyle	= 0;
			//frmScheduler.ScaleWidth	= 9045;
			//frmScheduler.ScaleHeight	= 7410;
			//frmScheduler.LinkTopic	= "Form2";
			//frmScheduler.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			SundayCol = 0;
			MondayCol = 1;
			TuesdayCol = 2;
			WednesdayCol = 3;
			ThursdayCol = 4;
			FridayCol = 5;
			SaturdayCol = 6;
			WeekOneRow = 1;
			WeekTwoRow = 5;
			WeekThreeRow = 9;
			WeekFourRow = 13;
			WeekFiveRow = 17;
			WeekSixRow = 21;
			vsCalender.TextMatrix(0, SundayCol, "Sunday");
			vsCalender.TextMatrix(0, MondayCol, "Monday");
			vsCalender.TextMatrix(0, TuesdayCol, "Tuesday");
			vsCalender.TextMatrix(0, WednesdayCol, "Wednesday");
			vsCalender.TextMatrix(0, ThursdayCol, "Thursday");
			vsCalender.TextMatrix(0, FridayCol, "Friday");
			vsCalender.TextMatrix(0, SaturdayCol, "Saturday");
			//vsCalender.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsCalender.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			for (counter = 0; counter <= vsCalender.Cols - 2; counter++)
			{
				vsCalender.ColWidth(counter, FCConvert.ToInt32(vsCalender.WidthOriginal / 7.0));
			}
			KeyCol = 0;
			DateCol = 1;
			DescriptionCol = 2;
			vsResults.TextMatrix(0, DateCol, "Date");
			vsResults.TextMatrix(0, DescriptionCol, "Description");
			vsResults.ColHidden(KeyCol, true);
			vsResults.ColWidth(DateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.21));
			//vsResults.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsResults.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			blnEdit = true;
			for (counter = DateTime.Now.Year - 10; counter <= DateTime.Now.Year + 2; counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
			cboMonth.SelectedIndex = FCConvert.ToInt32(Strings.Format(DateTime.Today, "MM")) - 1;
			cboYear.SelectedIndex = 10;
			blnEdit = false;
			FormatCalender();
			LoadSchedule();
			// LoadTowns
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			this.fraEvent.CenterToContainer(this.ClientArea);
			this.fraResults.CenterToContainer(this.ClientArea);
			this.fraDeleteEvent.CenterToContainer(this.ClientArea);
			this.fraSelectTown.CenterToContainer(this.ClientArea);
		}

		private void frmScheduler_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraResults.Visible == true)
				{
					cboMonth.Enabled = true;
					cboYear.Enabled = true;
					vsCalender.Enabled = true;
					fraResults.Visible = false;
				}
				else
				{
					this.Unload();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmScheduler_Resize(object sender, System.EventArgs e)
		{
			vsCalender.ColWidth(SundayCol, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.1428135));
			vsCalender.ColWidth(MondayCol, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.1428135));
			vsCalender.ColWidth(TuesdayCol, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.1428135));
			vsCalender.ColWidth(WednesdayCol, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.1428135));
			vsCalender.ColWidth(ThursdayCol, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.1428135));
			vsCalender.ColWidth(FridayCol, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.1428135));
			vsResults.ColWidth(DateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.21));
			this.fraEvent.CenterToContainer(this.ClientArea);
			this.fraResults.CenterToContainer(this.ClientArea);
			this.fraDeleteEvent.CenterToContainer(this.ClientArea);
			this.fraSelectTown.CenterToContainer(this.ClientArea);
		}

		private void imgBack_Click(object sender, System.EventArgs e)
		{
			mnuFilePrevious_Click();
		}

		private void imgForward_Click(object sender, System.EventArgs e)
		{
			mnuFileNext_Click();
		}

		private void mnuFileAddEvent_Click(object sender, System.EventArgs e)
		{
			fraEvent.Visible = true;
			txtEventDate.Focus();
		}

		private void mnuFileDeleteEvent_Click(object sender, System.EventArgs e)
		{
			fraDeleteEvent.Visible = true;
			txtDeleteEventDate.Focus();
		}

		private void mnuFileNext_Click(object sender, System.EventArgs e)
		{
			if (cboMonth.SelectedIndex < cboMonth.Items.Count - 1)
			{
				cboMonth.SelectedIndex = cboMonth.SelectedIndex + 1;
			}
			else
			{
				if (cboYear.SelectedIndex < cboYear.Items.Count - 1)
				{
					cboYear.SelectedIndex = cboYear.SelectedIndex + 1;
					cboMonth.SelectedIndex = 0;
				}
			}
		}

		public void mnuFileNext_Click()
		{
			mnuFileNext_Click(mnuFileNext, new System.EventArgs());
		}

		private void mnuFilePrevious_Click(object sender, System.EventArgs e)
		{
			if (cboMonth.SelectedIndex > 0)
			{
				cboMonth.SelectedIndex = cboMonth.SelectedIndex - 1;
			}
			else
			{
				if (cboYear.SelectedIndex > 0)
				{
					cboYear.SelectedIndex = cboYear.SelectedIndex - 1;
					cboMonth.SelectedIndex = cboMonth.Items.Count - 1;
				}
			}
		}

		public void mnuFilePrevious_Click()
		{
			mnuFilePrevious_Click(mnuFilePrevious, new System.EventArgs());
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			cboMonth.Enabled = false;
			cboYear.Enabled = false;
			vsCalender.Enabled = false;
			fraSelectTown.Visible = true;
			txtCriteria.Text = "";
			txtCriteria.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FormatCalender()
		{
			int counter;
			int counter2;
			// vbPorter upgrade warning: datFirstDay As DateTime	OnWrite(string)
			DateTime datFirstDay;
			// vbPorter upgrade warning: datLastDay As DateTime	OnWrite(string, DateTime)
			DateTime datLastDay;
			string strDay = "";
			int intDayOfWeek = 0;
			if (!blnEdit)
			{
				vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, 24, 6, Color.White);
				for (counter = 1; counter <= WeekSixRow; counter += 4)
				{
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpAlignment, counter + 2, 0, counter + 2, vsCalender.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					for (counter2 = 0; counter2 <= vsCalender.Cols - 1; counter2++)
					{
						vsCalender.TextMatrix(counter, counter2, "");
						vsCalender.TextMatrix(counter + 1, counter2, "");
						vsCalender.TextMatrix(counter + 2, counter2, "");
						vsCalender.Cell(FCGrid.CellPropertySettings.flexcpFontBold, counter + 2, counter2, false);
					}
				}
				// show a border between the titles and the data input section of the grid
				for (counter = 1; counter <= WeekSixRow; counter += 4)
				{
					for (counter2 = 0; counter2 <= vsCalender.Cols - 1; counter2++)
					{
						vsCalender.Select(counter, counter2, counter + 3, counter2);
						vsCalender.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, 1, 1, -1, -1);
					}
				}
				datFirstDay = FCConvert.ToDateTime(FCConvert.ToString(cboMonth.SelectedIndex + 1) + "/01/" + cboYear.Text);
				strDay = Strings.Format(datFirstDay, "dddd");
				if (Strings.UCase(strDay) == "SUNDAY")
				{
					intDayOfWeek = 0;
				}
				else if (Strings.UCase(strDay) == "MONDAY")
				{
					intDayOfWeek = 1;
				}
				else if (Strings.UCase(strDay) == "TUESDAY")
				{
					intDayOfWeek = 2;
				}
				else if (Strings.UCase(strDay) == "WEDNESDAY")
				{
					intDayOfWeek = 3;
				}
				else if (Strings.UCase(strDay) == "THURSDAY")
				{
					intDayOfWeek = 4;
				}
				else if (Strings.UCase(strDay) == "FRIDAY")
				{
					intDayOfWeek = 5;
				}
				else if (Strings.UCase(strDay) == "SATURDAY")
				{
					intDayOfWeek = 6;
				}
				intMonthStartCol = intDayOfWeek;
				intMonthStartRow = 0;
				if (intDayOfWeek > 0)
				{
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, 4, intDayOfWeek - 1, 0xE0E0E0);
				}
				if (datFirstDay.Month == 12)
				{
					datLastDay = FCConvert.ToDateTime("01/01/" + FCConvert.ToString(datFirstDay.Year + 1));
				}
				else
				{
					datLastDay = FCConvert.ToDateTime(FCConvert.ToString(datFirstDay.Month + 1) + "/01/" + FCConvert.ToString(datFirstDay.Year));
				}
				datLastDay = DateAndTime.DateAdd("d", -1, datLastDay);
				intDaysInMonth = datLastDay.Day;
				counter = WeekOneRow;
				counter2 = 1;
				do
				{
					vsCalender.TextMatrix(counter, intDayOfWeek, FCConvert.ToString(counter2));
					counter2 += 1;
					intDayOfWeek += 1;
					if (intDayOfWeek > 6)
					{
						intDayOfWeek = 0;
						counter += 4;
					}
				}
				while (counter2 <= datLastDay.Day);
				if (intDayOfWeek < 7)
				{
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, intDayOfWeek, counter + 3, 6, 0xE0E0E0);
				}
				if (counter < WeekSixRow)
				{
					counter = WeekSixRow;
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter + 3, 6, 0xE0E0E0);
				}
			}
		}

		private void LoadSchedule()
		{
			int counter;
			clsDRWrapper rsTaskInfo = new clsDRWrapper();
			clsDRWrapper rsEventInfo = new clsDRWrapper();
			int intWeekCounter = 0;
			int intDayCounter = 0;
			// vbPorter upgrade warning: datSelectedDate As DateTime	OnWrite(string)
			DateTime datSelectedDate;
			if (!blnEdit)
			{
				intDayCounter = intMonthStartCol;
				intWeekCounter = WeekOneRow;
				for (counter = 1; counter <= intDaysInMonth; counter++)
				{
					datSelectedDate = FCConvert.ToDateTime(FCConvert.ToString(cboMonth.SelectedIndex + 1) + "/" + FCConvert.ToString(counter) + "/" + cboYear.Text);
					if (datSelectedDate.ToOADate() == DateTime.Today.ToOADate())
					{
						vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intWeekCounter, intDayCounter, intWeekCounter + 3, intDayCounter, 0x808080);
					}
					if (chkViewAll.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsTaskInfo.OpenRecordset("SELECT Count(ID) as TaskTotal FROM Tasks WHERE TaskDate = '" + FCConvert.ToString(datSelectedDate) + "'", "SystemSettings");
					}
					else
					{
						rsTaskInfo.OpenRecordset("SELECT Count(ID) as TaskTotal FROM Tasks WHERE TaskPerson = '" + modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID() + "' AND TaskDate = '" + FCConvert.ToString(datSelectedDate) + "'", "SystemSettings");
					}
					rsEventInfo.OpenRecordset("SELECT * FROM Tasks WHERE TaskPerson = '' AND TaskDate = '" + FCConvert.ToString(datSelectedDate) + "'", "SystemSettings");
					if (rsTaskInfo.EndOfFile() != true && rsTaskInfo.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Field [TaskTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTaskInfo.Get_Fields("TaskTotal")) > 0)
						{
							// TODO Get_Fields: Field [TaskTotal] not found!! (maybe it is an alias?)
							vsCalender.TextMatrix(intWeekCounter + 2, intDayCounter, "(" + FCConvert.ToString(Conversion.Val(rsTaskInfo.Get_Fields("TaskTotal"))) + ")");
							vsCalender.Cell(FCGrid.CellPropertySettings.flexcpFontBold, intWeekCounter + 2, intDayCounter, false);
						}
					}
					if (rsEventInfo.EndOfFile() != true && rsEventInfo.BeginningOfFile() != true)
					{
						vsCalender.TextMatrix(intWeekCounter + 1, intDayCounter, FCConvert.ToString(rsEventInfo.Get_Fields_String("TaskDescription")));
					}
					intDayCounter += 1;
					if (intDayCounter > 6)
					{
						intDayCounter = 0;
						intWeekCounter += 4;
					}
				}
			}
		}

		private void txtCriteria_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdSearchOK_Click();
			}
		}

		private void vsCalender_DblClick(object sender, System.EventArgs e)
		{
			int intCurrentWeek = 0;
			int counter;
			// vbPorter upgrade warning: datStartDate As DateTime	OnWrite(string, DateTime)
			DateTime datStartDate;
			DateTime datEndDate;
			if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col)) == 0xE0E0E0)
			{
				return;
			}
			switch (vsCalender.Row)
			{
				case 1:
				case 2:
				case 3:
				case 4:
					{
						intCurrentWeek = 1;
						break;
					}
				case 5:
				case 6:
				case 7:
				case 8:
					{
						intCurrentWeek = 5;
						break;
					}
				case 9:
				case 10:
				case 11:
				case 12:
					{
						intCurrentWeek = 9;
						break;
					}
				case 13:
				case 14:
				case 15:
				case 16:
					{
						intCurrentWeek = 13;
						break;
					}
				case 17:
				case 18:
				case 19:
				case 20:
					{
						intCurrentWeek = 17;
						break;
					}
				case 21:
				case 22:
				case 23:
				case 24:
					{
						intCurrentWeek = 21;
						break;
					}
			}
			//end switch
			counter = 0;
			while (vsCalender.TextMatrix(intCurrentWeek, counter) == "")
			{
				counter += 1;
			}
			datStartDate = FCConvert.ToDateTime(FCConvert.ToString(cboMonth.SelectedIndex + 1) + "/" + vsCalender.TextMatrix(intCurrentWeek, counter) + "/" + cboYear.Text);
			if (counter > 0)
			{
				for (counter = counter; counter >= 1; counter--)
				{
					datStartDate = DateAndTime.DateAdd("d", -1, datStartDate);
				}
			}
			datStartDate = DateAndTime.DateAdd("d", 1, datStartDate);
			datEndDate = DateAndTime.DateAdd("d", 4, datStartDate);
			frmSchedulerWeekly.InstancePtr.Init(ref datStartDate, ref datEndDate);
			FormatCalender();
			LoadSchedule();
			frmSchedulerWeekly.InstancePtr.Unload();
		}

		private void OpenWeeklyScheduler_6(string strDate, int lngKey)
		{
			OpenWeeklyScheduler(ref strDate, ref lngKey);
		}

		private void OpenWeeklyScheduler_8(string strDate, int lngKey)
		{
			OpenWeeklyScheduler(ref strDate, ref lngKey);
		}

		private void OpenWeeklyScheduler(ref string strDate, ref int lngKey)
		{
			int RowCounter;
			int ColCounter;
			string strDay;
			int intCurrentWeek = 0;
			// vbPorter upgrade warning: datStartDate As DateTime	OnWrite(string, DateTime)
			DateTime datStartDate;
			DateTime datEndDate;
			int counter;
			cboMonth.Enabled = true;
			cboYear.Enabled = true;
			vsCalender.Enabled = true;
			strDay = FCConvert.ToString(Conversion.Val(Strings.Mid(strDate, 4, 2)));
			cboMonth.SelectedIndex = DateAndTime.DateValue(strDate).Month - 1;
			for (counter = 0; counter <= cboYear.Items.Count - 1; counter++)
			{
				if (cboYear.Items[counter].ToString() == Strings.Right(strDate, 4))
				{
					cboYear.SelectedIndex = counter;
				}
			}
			for (RowCounter = 1; RowCounter <= WeekSixRow; RowCounter += 4)
			{
				for (ColCounter = 0; ColCounter <= vsCalender.Cols - 1; ColCounter++)
				{
					if (vsCalender.TextMatrix(RowCounter, ColCounter) == strDay)
					{
						intCurrentWeek = RowCounter;
						counter = 0;
						while (vsCalender.TextMatrix(intCurrentWeek, counter) == "")
						{
							counter += 1;
						}
						datStartDate = FCConvert.ToDateTime(FCConvert.ToString(cboMonth.SelectedIndex + 1) + "/" + vsCalender.TextMatrix(intCurrentWeek, counter) + "/" + cboYear.Text);
						if (counter > 0)
						{
							for (counter = counter; counter >= 1; counter--)
							{
								datStartDate = DateAndTime.DateAdd("d", -1, datStartDate);
							}
						}
						datStartDate = DateAndTime.DateAdd("d", 1, datStartDate);
						datEndDate = DateAndTime.DateAdd("d", 4, datStartDate);
						frmSchedulerWeekly.InstancePtr.Init(ref datStartDate, ref datEndDate, lngKey, DateAndTime.DateValue(strDate));
						FormatCalender();
						LoadSchedule();
						frmSchedulerWeekly.InstancePtr.Unload();
					}
				}
			}
		}

		private void vsResults_DblClick(object sender, System.EventArgs e)
		{
			if (vsResults.Row > 0)
			{
				OpenWeeklyScheduler_6(vsResults.TextMatrix(vsResults.Row, DateCol), FCConvert.ToInt32(FCConvert.ToDouble(vsResults.TextMatrix(vsResults.Row, KeyCol))));
			}
		}

		private void vsResults_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				//e.KeyCode = 0;
				if (vsResults.Row > 0)
				{
					OpenWeeklyScheduler_6(vsResults.TextMatrix(vsResults.Row, DateCol), FCConvert.ToInt32(FCConvert.ToDouble(vsResults.TextMatrix(vsResults.Row, KeyCol))));
				}
			}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity2.CNSTEDITCALENDARALL:
						{
							if (strPerm == "N")
							{
								chkViewAll.Visible = false;
							}
							else
							{
								chkViewAll.Visible = true;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}
	}
}
