//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmCDDVDBackup.
	/// </summary>
	public partial class frmCDDVDBackup : fecherFoundation.FCForm
	{
;

		public frmCDDVDBackup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.optCloseDisk = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optZip = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optCloseDisk.AddControlArrayElement(optCloseDisk_0, 0 );
			this.optCloseDisk.AddControlArrayElement(optCloseDisk_1, 1 );
			this.optZip.AddControlArrayElement(optZip_0, 0 );
			this.optZip.AddControlArrayElement(optZip_1, 1 );

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================

		string strListOfFiles = "";
		int mlngTrackBlocksToWrite;
		int mintTracksToWrite;
		string strTempDirOrFile = ""; // store the directory or temp file to delete when done burning
		string strDriveSavedTo = "";
		string strPathFileSavedTo = "";
		string strZipFileSavedTo = "";
		private string strExtendedList = "";



		private void CDWriterXP1_ClosingDisc(object sender, System.EventArgs e)
		{
			frmWait.InstancePtr.Init("Closing Disk. Please wait", , , true);
			//Application.DoEvents();
		}

		private void CDWriterXP1_ClosingSession(object sender, System.EventArgs e)
		{
			frmWait.InstancePtr.Init("Closing Session. Please wait", , , true);
			//Application.DoEvents();
		}


		private void CDWriterXP1_ISOItemAdded(object sender, AxCDWriterXPLib._DCDWriterXPEvents_ISOItemAddedEvent e)
		{
			string strTemp = "";
			// MsgBox ParentDestPath & "  " & ItemDestPath & "  " & ItemName & "  " & SourceFilePath & "  " & FileDate & "  " & FileSize

		}



		private void CDWriterXP1_PreparingToWrite(object sender, System.EventArgs e)
		{
			frmWait.InstancePtr.Init("Preparing to write.  Please wait.", false, , true);
		}

		private void CDWriterXP1_TrackFileError(object sender, AxCDWriterXPLib._DCDWriterXPEvents_TrackFileErrorEvent e)
		{
			CDWriterXP1.CancelWrite();
			
			if (e.fileError==CDWriterXPLib.eTrackFileError.tfeFileBufferError)
			{
				MessageBox.Show("Track File Error"+"\n"+"File Buffer Error");
			}
			else if (e.fileError==CDWriterXPLib.eTrackFileError.tfeFileOpenError)
			{
				MessageBox.Show("Track File Error"+"\n"+"File Open Error");
			}
			else if (e.fileError==CDWriterXPLib.eTrackFileError.tfeFileReadError)
			{
				MessageBox.Show("Track File Error"+"\n"+"File Read Error");
			}
			else if (e.fileError==CDWriterXPLib.eTrackFileError.tfeFileWriteError)
			{
				MessageBox.Show("Track File Error"+"\n"+"File Write Error");
			}
			EnableThisForm_2(true);
		}

		private void CDWriterXP1_TrackWriteStart(object sender, AxCDWriterXPLib._DCDWriterXPEvents_TrackWriteStartEvent e)
		{
			frmWait.InstancePtr.Unload();
			prgTrackProgress.Value = 0;
			mlngTrackBlocksToWrite = e.blocksToWrite;
		}

		private void CDWriterXP1_TrackWriteStatus(object sender, AxCDWriterXPLib._DCDWriterXPEvents_TrackWriteStatusEvent e)
		{
			// vbPorter upgrade warning: intPercentTrackWritten As short --> As int	OnWrite(double)
			int intPercentTrackWritten;
			// vbPorter upgrade warning: intPercentTotalTracksWritten As short --> As int	OnWrite(double)
			int intPercentTotalTracksWritten;

			 /*? On Error Resume Next  */
try {

			// Update Drive Buffer Display
			// lblDriveBuffer.Caption = "Drive Buffer: " & Format(DeviceBufferUsed, "0#") & " %"

			// Calc Percent of Current track done
			intPercentTrackWritten = Convert.ToInt32((((double)e.blocksWritten / mlngTrackBlocksToWrite)*100));

			// Calc the percentage of the current track into the Total
			intPercentTotalTracksWritten = Convert.ToInt32((((e.track-1+(intPercentTrackWritten / 100.0))/mintTracksToWrite)*100));

			// Set Progress Bars
			prgTrackProgress.Value = intPercentTrackWritten;
			prgTotalProgress.Value = intPercentTotalTracksWritten;
			prgTrackProgress.Refresh();
			prgTotalProgress.Refresh();
			// sbrStatus.Panels(1).Text = "Writing Track #" & Format(Track, "0#") & "... "
} catch { }
		}

		private void CDWriterXP1_WriteError(object sender, AxCDWriterXPLib._DCDWriterXPEvents_WriteErrorEvent e)
		{
			string strTemp = "";

			
			if (e.writeError==CDWriterXPLib.eWriteError.errDriveError)
			{
				strTemp = " drive error";
			}
			else if (e.writeError==CDWriterXPLib.eWriteError.errFileError)
			{
				strTemp = " file error";
			}
			else if (e.writeError==CDWriterXPLib.eWriteError.errInvalidFormat)
			{
				strTemp = " invalid format";
			}
			else if (e.writeError==CDWriterXPLib.eWriteError.errNoTracksQueued)
			{
				strTemp = " no tracks queued";
			}
			else if (e.writeError==CDWriterXPLib.eWriteError.errReadBufferInitFailed)
			{
				strTemp = " read buffer init failed.";
			}
			else if (e.writeError==CDWriterXPLib.eWriteError.errWriteBufferInitFailed)
			{
				strTemp = " write buffer init failed.";
			}


			MessageBox.Show("Write Error "+strTemp+". Operation cancelled.");

			CDWriterXP1.CancelWrite();
			EnableThisForm_2(true);
		}

		private void CDWriterXP1_WritingCancelled(object sender, System.EventArgs e)
		{
			EnableThisForm_2(true);
		}

		private void CDWriterXP1_WritingComplete(object sender, System.EventArgs e)
		{

			try
			{	// On Error GoTo ErrorHandler

				frmWait.InstancePtr.Unload();
				MessageBox.Show("Burn operation complete.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
				EnableThisForm_2(true);
				if (optZip[0].Checked==true) {
					// delete the temp zip file
					if ((Strings.UCase(Environment.CurrentDirectory+"\\")!=Strings.UCase(strTempDirOrFile)) && (Strings.UCase(Application.StartupPath+"\\")!=Strings.UCase(strTempDirOrFile)) && strTempDirOrFile!="" && strTempDirOrFile!="\\") {
						fso.DeleteFile(strTempDirOrFile, true);
					}
				} else {
					if ((chkDelete.CheckState==Wisej.Web.CheckState.Checked || chkDelete.CheckState==CheckState.Indeterminate) && (chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked || chkCopyToLocal.CheckState==CheckState.Indeterminate)) {
						if ((Strings.UCase(Environment.CurrentDirectory+"\\")!=Strings.UCase(strTempDirOrFile)) && (Strings.UCase(Application.StartupPath+"\\")!=Strings.UCase(strTempDirOrFile)) && strTempDirOrFile!="" && strTempDirOrFile!="\\" && Strings.UCase(strTempDirOrFile)!="WINDOWS\\" && Strings.UCase(strTempDirOrFile)!="C:\\") {
							// make sure we don't delete the data directory or the vbtrio directory
							fso.DeleteFile(strTempDirOrFile+"*.vb1", true);
							fso.DeleteFile(strTempDirOrFile+"*.exe", true);
						}
					}
				}
				rptBackup.InstancePtr.Init(ref optZip[0].Checked, ref strExtendedList, strDriveSavedTo+"\\"+strPathFileSavedTo, ref strZipFileSavedTo);
				CDWriterXP1.Eject();
				// Unload Me
				return;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In WritingComplete", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}





		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			string strZipDir;

			strZipDir = frmPath.InstancePtr.Init(false, "Select the path to store the temp file(s) or create a new temp directory in.", false);
			if (strZipDir!=string.Empty) {
				txtTempDir.Text = strZipDir;
			}

		}



		private void frmCDDVDBackup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCDDVDBackup properties;
			//frmCDDVDBackup.ScaleWidth	= 5880;
			//frmCDDVDBackup.ScaleHeight	= 4200;
			//frmCDDVDBackup.LinkTopic	= "Form1";
			//frmCDDVDBackup.LockControls	= true;
			//End Unmaped Properties

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			txtNewDirectory.Text = Strings.Format(DateTime.Today, "mmddyyyy");
			txtTempDir.Text = Application.StartupPath+"\\temp\\";

		}

		// vbPorter upgrade warning: strPath As object	OnWrite(string)
		// vbPorter upgrade warning: strFileToMake As object	OnWrite(string)
		// vbPorter upgrade warning: strListToZip As object	OnWrite(string)
		private bool CreateZipFile(ref string strPath, ref string strFileToMake, ref string strListToZip)
		{
			bool CreateZipFile = false;
			int x;
			string[] arListArray = null;
			string strExePath;
			string strDBPath;
			// vbPorter upgrade warning: zipreturn As abeError	OnWrite(AbaleZipLibrary.abeError)
			AbaleZipLibrary.abeError zipreturn;

			arListArray = Strings.Split(Convert.ToString(strListToZip), ",", -1, CompareConstants.vbTextCompare);
			strDBPath = Environment.CurrentDirectory+"\\";
			// strExePath = Left(CurDir, 1) & ":\triodata\triomast\"
			strExePath = modGlobalFunctions.gGlobalSettings.MasterPath;
			if (strExePath!="" && Strings.Right("\\"+strExePath, 1)!="\\") {
				strExePath += "\\";
			}

			AbaleZip1.PreservePaths = false;
			AbaleZip1.FilesToProcess = "";
			AbaleZip1.ProcessSubfolders = false;
			AbaleZip1.SpanMultipleDisks = AbaleZipLibrary.abeDiskSpanning.adsNever;
			for(x=0; x<=Information.UBound(arListArray, 1); x++) {


				//Application.DoEvents();
				AbaleZip1.ZipFilename = strPath.ToString()+strFileToMake+".zip";

				
				if (Strings.UCase(Strings.Right(arListArray[x], 3))=="EXE")
				{
					AbaleZip1.AddFilesToProcess(strExePath+arListArray[x]);
				}
				else if (Strings.UCase(Strings.Right(arListArray[x], 3))=="VB1")
				{
					AbaleZip1.AddFilesToProcess(strDBPath+arListArray[x]);
				}



				this.Refresh();
			}
			zipreturn = AbaleZip1.Zip();

			if (zipreturn!=AbaleZipLibrary.abeError.aerSuccess) {
				CreateZipFile = false;
			} else {
				CreateZipFile = true;
			}
			return CreateZipFile;
		}

		private void EnableThisForm_2(bool boolEnable) { EnableThisForm(ref boolEnable); }
		private void EnableThisForm(ref bool boolEnable)
		{
			if (boolEnable) {
				// enable
				framZipOptions.Enabled = true;
				framSessionOptions.Enabled = true;
				framBurnOptions.Enabled = true;
				mnuBurn.Enabled = true;
				mnuExit.Enabled = true;
				mnuAbort.Enabled = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			} else {
				// disable
				framZipOptions.Enabled = false;
				framSessionOptions.Enabled = false;
				framBurnOptions.Enabled = false;
				mnuBurn.Enabled = false;
				mnuExit.Enabled = false;
				mnuAbort.Enabled = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			}
		}

		private void Form_Unload(ref short Cancel)
		{
			modGNBas.clsGESecurity = new clsTrioSecurity();
			modGlobalConstants.clsSecurityClass = new clsTrioSecurity();
			modGNBas.clsGESecurity.Init("GE", "systemsettings");
			modGlobalConstants.clsSecurityClass.Init("GE", "systemsettings", , modGNBas.clsGESecurity.Get_UserID());
			// Call secRS.OpenRecordset("SELECT * FROM Security", "systemsettings")
			// Call secRS.FindFirstRecord("ID", clsGESecurity.Get_UserID)
			MDIParent.InstancePtr.Grid.Focus();
		}

		private void mnuAbort_Click(object sender, System.EventArgs e)
		{
			CDWriterXP1.CancelWrite();
			EnableThisForm_2(true);
		}

		private void mnuBurn_Click(object sender, System.EventArgs e)
		{
			string strTempPath = "";
			
			string strZipFileName = "";
			Scripting.Drive dr;
			string[] arListArray = null;
			bool boolCopyFiles = false;
			string strDestPath = "";
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			string strSrcPath = "";
			bool boolLoadLastSession;


			int x;


			try
			{	// On Error GoTo ErrorHandler
				// save defaults
				if (optCloseDisk[0].Checked) {
					modRegistry.SaveRegistryKey("BackupCloseDisk", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupCloseDisk", Convert.ToString(false), "GN");
				}
				if (optZip[0].Checked) {
					modRegistry.SaveRegistryKey("BackupZip", "Single", "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupZip", "Multiple", "GN");
				}
				if (chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked) {
					modRegistry.SaveRegistryKey("BackupCopyToTemp", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupCopyToTemp", Convert.ToString(false), "GN");
				}
				if (chkDelete.CheckState==Wisej.Web.CheckState.Checked) {
					modRegistry.SaveRegistryKey("BackupDeleteTemp", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupDeleteTemp", Convert.ToString(false), "GN");
				}
				modRegistry.SaveRegistryKey("BackupTempDir", txtTempDir.Text, "GN");
				if (chkNewDirectory.CheckState==Wisej.Web.CheckState.Checked) {
					modRegistry.SaveRegistryKey("BackupNewDirectory", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupNewDirectory", Convert.ToString(false), "GN");
				}
				modRegistry.SaveRegistryKey("BackupWriteSpeed", Convert.ToString(cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex)), "GN");

				//Application.DoEvents();
				// secRS.DisconnectDatabase

				// rsVariables.DisconnectDatabase
				// secDB.DisconnectDatabase
				modGNBas.clsGESecurity = null;
				modGlobalConstants.clsSecurityClass = null;

				//Application.DoEvents();


				CDWriterXP1.ClearISOImage();
				boolLoadLastSession = false;
				CDWriterXP1.SetWriteSpeed((short)cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex));

				vbPorterVar = CDWriterXP1.GetLastSessionStatus();
				
				if (vbPorterVar==CDWriterXPLib.eSessionStatus.ssEmpty)
				{
					boolLoadLastSession = false;
				}
				else if (vbPorterVar==CDWriterXPLib.eSessionStatus.ssIncomplete)
				{
					boolLoadLastSession = true;
					CDWriterXP1.CloseLastSession(false);
					frmWait.InstancePtr.Unload();
				}
				else if (vbPorterVar==CDWriterXPLib.eSessionStatus.ssComplete)
				{
					boolLoadLastSession = true;
				}
				else if (vbPorterVar==CDWriterXPLib.eSessionStatus.ssReserved)
				{
					// unknown
				}

				CDWriterXP1.ImportISOTrack(true);
				// If boolLoadLastSession Then
				// If Not CDWriterXP1.ImportISOTrack(True) Then
				// intRes = MsgBox("Could not import last session.  If there is other data on this CD/DVD it will be lost when this session is created." & vbNewLine & "Do you want to continue?", vbQuestion + vbYesNo, "Error")
				// If intRes = vbNo Then
				// Exit Sub
				// End If
				// End If
				// End If

				if (optZip[0].Checked || chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked || chkCopyToLocal.CheckState==CheckState.Indeterminate) {
					if (txtTempDir.Text=="") {
						strTempPath = Application.StartupPath+"\\";
					} else {
						strTempPath = txtTempDir.Text;
						if (fso.FolderExists(txtTempDir.Text)) {
							strTempPath = txtTempDir.Text;
							if (Strings.Right(strTempPath, 1)!="\\") strTempPath += "\\";
						} else {
							intRes = MessageBox.Show("Temp directory "+strTempPath+" does not exist. Create it now?", "Path not found", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intRes==DialogResult.Yes) {
								fso.CreateFolder(Strings.Mid(strTempPath, 1, strTempPath.Length-1));
								// 20                        MsgBox "Could not create temp directory " & strTempPath & ". Operation cancelled."
								// Exit Sub
								// End If
							} else {
								MessageBox.Show("Operation cancelled.");
								return;
							}
						}
					}
				}


				if (chkNewDirectory.CheckState==Wisej.Web.CheckState.Checked) {
					if (Strings.Trim(txtNewDirectory.Text)==string.Empty) {
						strDestPath = "";
					} else {
						strDestPath = Strings.Trim(txtNewDirectory.Text);
					}
				} else {
					strDestPath = "";
				}

				// always close the session
				CDWriterXP1.CloseSession = true;
				if (optCloseDisk[0].Checked) {
					CDWriterXP1.CloseDisc = true;
				} else {
					CDWriterXP1.CloseDisc = false;
				}

				EnableThisForm_2(false);

				strTempDirOrFile = strTempPath;
				CDWriterXP1.WriteType = CDWriterXPLib.eWriteType.wtpData;
				if (!CDWriterXP1.IsMediaLoaded()) {
					MessageBox.Show("There is no CD/DVD media in the drive.");
					return;
				}
				strPathFileSavedTo = strDestPath;
				strZipFileSavedTo = "";
				int lngBytesLeft;
				lngBytesLeft = CDWriterXP1.GetDiscFreeSpaceBlocks(CDWriterXPLib.eWriteType.wtpData);
				lngBytesLeft = CDWriterXP1.ConvertBlocksToBytes(lngBytesLeft, CDWriterXPLib.eWriteType.wtpData);

				if (optZip[0].Checked) {
					// burning a zip file
					strZipFileName = "tz"+Strings.Format(DateTime.Today, "mmddyy");
					strZipFileSavedTo = strZipFileName;
					strTempDirOrFile = strTempPath+strZipFileName+".zip";
					if (fso.FileExists(strTempPath+strZipFileName+".zip")) fso.DeleteFile(strTempPath+strZipFileName+".zip", true);
					frmWait.InstancePtr.Init("Creating Zip file.  Please Wait.", false, , true);
					if (CreateZipFile(ref strTempPath, ref strZipFileName, ref strListOfFiles)) {
						// burn it
						frmWait.InstancePtr.Unload();
						mintTracksToWrite = 1;

						if (fso.GetFile(strTempPath+strZipFileName+".zip").Size<lngBytesLeft) {
							if (CDWriterXP1.InsertISOItem(strDestPath+"\\"+strZipFileName+".zip", strTempPath+strZipFileName+".zip")) {
								// its in the list so burn it

								CDWriterXP1.WriteDisc();
							}
						} else {
							frmWait.InstancePtr.Unload();
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							MessageBox.Show("There is not enough free space on the CD/DVD to save the backup."+"\n"+"Please insert a new CD/DVD and try again", "Insufficient Room", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}

					} else {
						frmWait.InstancePtr.Unload();
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Error.  Could not create zip file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}

				} else {
					// burning each file
					mintTracksToWrite = 1; // not sure about this one
					arListArray = Strings.Split(strListOfFiles, ",", -1, CompareConstants.vbTextCompare);
					boolCopyFiles = false;
					dr = fso.GetDrive(Strings.Left(Environment.CurrentDirectory, 1));
					if (dr.DriveType==DriveTypeConst.Remote) {
						// is a network drive so copy the files locally before burning
						if (chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked) boolCopyFiles = true;
					}

					if (boolCopyFiles) {
						strSrcPath = strTempPath;
						frmWait.InstancePtr.Init("Copying files to temp directory.  Please wait.", false, , true);
						for(x=0; x<=Information.UBound(arListArray, 1); x++) {
							fso.CopyFile(arListArray[x], strTempPath, true);
						} // x
						frmWait.InstancePtr.Unload();
					} else {
						strSrcPath = Environment.CurrentDirectory+"\\";
					}

					for(x=0; x<=Information.UBound(arListArray, 1); x++) {
						if (!CDWriterXP1.InsertISOItem(strDestPath+"\\"+arListArray[x], strSrcPath+arListArray[x])) {
							MessageBox.Show("Error inserting "+strSrcPath+". Operation cancelled.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							EnableThisForm_2(true);
							return;
						}

					} // x
					if (CDWriterXP1.ConvertBlocksToBytes(CDWriterXP1.GetISOVolumeSizeBlocks(), CDWriterXPLib.eWriteType.wtpData)<lngBytesLeft) {
						CDWriterXP1.WriteDisc();
					} else {
						frmWait.InstancePtr.Unload();
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("There is not enough free space on the CD/DVD to save the backup."+"\n"+"Please insert a new CD/DVD and try again", "Insufficient Room", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}

				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				frmWait.InstancePtr.Unload();
				EnableThisForm_2(true);
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In line "+Erl+" in mnuBurn.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void Init(ref string strFileList, ref string strDrive, ref string strVerboseList)
		{
			int x;
			bool boolGotDrive;

			try
			{	// On Error GoTo ErrorHandler
				// don't keep the gn database locked
				//Application.DoEvents();

				strListOfFiles = strFileList;
				strExtendedList = strVerboseList;
				strDriveSavedTo = strDrive;

				boolGotDrive = false;
				for(x=0; x<=CDWriterXP1.GetDriveCount()-1; x++) {
					if (Strings.UCase(CDWriterXP1.GetDriveLetter((short)x))==Strings.UCase(Strings.Left(strDrive, 1))) {
						if (!CDWriterXP1.OpenDrive((short)x)) {
							MessageBox.Show("Error. Could not open drive "+Strings.UCase(Strings.Left(strDrive, 1))+" cannot continue.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						} else {
							boolGotDrive = true;
						}
					}
				} // x

				if (!boolGotDrive) {
					MessageBox.Show("Error. "+Strings.UCase(Strings.Left(strDrive, 1))+" not a valid CD/DVD writer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				// initialize some things
				LoadWriteSpeedCombo();


				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupCloseDisk", "GN", Convert.ToString(true)))) {
					optCloseDisk[0].Checked = true;
				} else {
					optCloseDisk[1].Checked = true;
				}

				if (modRegistry.GetRegistryKey("BackupZip", "GN", "Single")=="Single") {
					optZip[0].Checked = true;
				} else {
					optZip[1].Checked = true;
				}

				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupCopyToTemp", "GN", Convert.ToString(false)))) {
					chkCopyToLocal.CheckState = Wisej.Web.CheckState.Checked;
				} else {
					chkCopyToLocal.CheckState = Wisej.Web.CheckState.Unchecked;
				}

				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupDeleteTemp", "GN", Convert.ToString(true)))) {
					chkDelete.CheckState = Wisej.Web.CheckState.Checked;
				} else {
					chkDelete.CheckState = Wisej.Web.CheckState.Unchecked;
				}

				txtTempDir.Text = modRegistry.GetRegistryKey("BackupTempDir", "GN");

				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupNewDirectory", "GN", Convert.ToString(false)))) {
					chkNewDirectory.CheckState = Wisej.Web.CheckState.Checked;
				} else {
					chkNewDirectory.CheckState = Wisej.Web.CheckState.Unchecked;
				}



				modRegistry.SaveRegistryKey("BackupWriteSpeed", Convert.ToString(cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex)), "GN");


				if (CDWriterXP1.IsDriveBurnProofCapable()) CDWriterXP1.SetBurnProofMode(true);

				this.Show(MDIParent.InstancePtr);
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


		private void LoadWriteSpeedCombo()
		{
			int intMaxWriteSpeed;
			// vbPorter upgrade warning: intSpeed As short --> As int	OnWrite(int, double)
			int intSpeed;
			int intDefaultSpeed;
			int intDefaultIndex;

			intDefaultSpeed = (int)Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BackupWriteSpeed", "GN", Convert.ToString(0))));
			// Get Max Write Speed
			intMaxWriteSpeed = CDWriterXP1.GetMaxWriteSpeed();

			// Clear Combo
			cboWriteSpeed.Clear();

			intSpeed = intMaxWriteSpeed;
			intDefaultIndex = -1;
			// If speed is not zero then
			if (intMaxWriteSpeed>0) {
				do { // Add Speeds in Multiples of 4...and then 1/2 after 4
					cboWriteSpeed.AddItem(Convert.ToString(intSpeed)+"x");
					cboWriteSpeed.ItemData( cboWriteSpeed.NewIndex, intSpeed);
					if (intSpeed==intDefaultSpeed && intSpeed>0) {
						intDefaultIndex = cboWriteSpeed.NewIndex;
					}
					if (intSpeed<=4) {
						intSpeed /= 2;
					} else {
						intSpeed -= 4;
					}
				} while ((intSpeed>0));
			} else {
				// Some drives don't report speed
				cboWriteSpeed.AddItem("Default");
			}
			// Set to Max
			if (cboWriteSpeed.Items.Count>0) {
				if (intDefaultIndex>0) {
					cboWriteSpeed.SelectedIndex = intDefaultIndex;
				} else {
					cboWriteSpeed.SelectedIndex = 0;
				}
			}

		}

	}
}