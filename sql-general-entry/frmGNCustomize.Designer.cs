﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmGNCustomize.
	/// </summary>
	partial class frmGNCustomize : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCentralPartyCheckNo;
		public fecherFoundation.FCLabel lblCentralPartyCheckNo;
		public fecherFoundation.FCComboBox cmbYes;
		public fecherFoundation.FCLabel lblYes;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtBulkMail;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cmbCityTown;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtFTPUser;
		public fecherFoundation.FCTextBox txtFTPPassword;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame frmBulkMailText;
		public fecherFoundation.FCTextBox txtBulkMail_5;
		public fecherFoundation.FCTextBox txtBulkMail_4;
		public fecherFoundation.FCTextBox txtBulkMail_3;
		public fecherFoundation.FCTextBox txtBulkMail_2;
		public fecherFoundation.FCTextBox txtBulkMail_1;
		public fecherFoundation.FCFrame fraTownSeal;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtTownSeal;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGNCustomize));
			this.cmbCentralPartyCheckNo = new fecherFoundation.FCComboBox();
			this.lblCentralPartyCheckNo = new fecherFoundation.FCLabel();
			this.cmbYes = new fecherFoundation.FCComboBox();
			this.lblYes = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cmbCityTown = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtFTPUser = new fecherFoundation.FCTextBox();
			this.txtFTPPassword = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.frmBulkMailText = new fecherFoundation.FCFrame();
			this.txtBulkMail_5 = new fecherFoundation.FCTextBox();
			this.txtBulkMail_4 = new fecherFoundation.FCTextBox();
			this.txtBulkMail_3 = new fecherFoundation.FCTextBox();
			this.txtBulkMail_2 = new fecherFoundation.FCTextBox();
			this.txtBulkMail_1 = new fecherFoundation.FCTextBox();
			this.fraTownSeal = new fecherFoundation.FCFrame();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtTownSeal = new fecherFoundation.FCTextBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.cmdConfigurePayportSettings = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.frmBulkMailText)).BeginInit();
			this.frmBulkMailText.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTownSeal)).BeginInit();
			this.fraTownSeal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdConfigurePayportSettings)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 523);
			this.BottomPanel.Size = new System.Drawing.Size(878, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbCentralPartyCheckNo);
			this.ClientArea.Controls.Add(this.lblCentralPartyCheckNo);
			this.ClientArea.Controls.Add(this.cmbYes);
			this.ClientArea.Controls.Add(this.lblYes);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.frmBulkMailText);
			this.ClientArea.Controls.Add(this.fraTownSeal);
			this.ClientArea.Size = new System.Drawing.Size(898, 662);
			this.ClientArea.Controls.SetChildIndex(this.fraTownSeal, 0);
			this.ClientArea.Controls.SetChildIndex(this.frmBulkMailText, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblYes, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbYes, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCentralPartyCheckNo, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbCentralPartyCheckNo, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdConfigurePayportSettings);
			this.TopPanel.Size = new System.Drawing.Size(898, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdConfigurePayportSettings, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(117, 28);
			this.HeaderText.Text = "Customize";
			// 
			// cmbCentralPartyCheckNo
			// 
			this.cmbCentralPartyCheckNo.Items.AddRange(new object[] {
            "Yes",
            "No"});
			this.cmbCentralPartyCheckNo.Location = new System.Drawing.Point(625, 300);
			this.cmbCentralPartyCheckNo.Name = "cmbCentralPartyCheckNo";
			this.cmbCentralPartyCheckNo.Size = new System.Drawing.Size(224, 40);
			this.cmbCentralPartyCheckNo.TabIndex = 7;
			this.cmbCentralPartyCheckNo.Text = "Yes";
			// 
			// lblCentralPartyCheckNo
			// 
			this.lblCentralPartyCheckNo.AutoSize = true;
			this.lblCentralPartyCheckNo.Location = new System.Drawing.Point(376, 314);
			this.lblCentralPartyCheckNo.Name = "lblCentralPartyCheckNo";
			this.lblCentralPartyCheckNo.Size = new System.Drawing.Size(242, 15);
			this.lblCentralPartyCheckNo.TabIndex = 6;
			this.lblCentralPartyCheckNo.Text = "CENTRAL PARTY ADDITIONAL CHECK";
			// 
			// cmbYes
			// 
			this.cmbYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
			this.cmbYes.Location = new System.Drawing.Point(625, 250);
			this.cmbYes.Name = "cmbYes";
			this.cmbYes.Size = new System.Drawing.Size(224, 40);
			this.cmbYes.TabIndex = 5;
			this.cmbYes.Text = "Yes";
			// 
			// lblYes
			// 
			this.lblYes.AutoSize = true;
			this.lblYes.Location = new System.Drawing.Point(376, 264);
			this.lblYes.Name = "lblYes";
			this.lblYes.Size = new System.Drawing.Size(209, 15);
			this.lblYes.TabIndex = 4;
			this.lblYes.Text = "NOTIFY OF PROGRAM UPDATES";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cmbCityTown);
			this.Frame2.Location = new System.Drawing.Point(499, 134);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(383, 82);
			this.Frame2.TabIndex = 3;
			this.Frame2.Text = "City / Town Designation";
			// 
			// cmbCityTown
			// 
			this.cmbCityTown.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCityTown.Location = new System.Drawing.Point(20, 30);
			this.cmbCityTown.Name = "cmbCityTown";
			this.cmbCityTown.Size = new System.Drawing.Size(330, 40);
			this.cmbCityTown.TabIndex = 0;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtFTPUser);
			this.Frame1.Controls.Add(this.txtFTPPassword);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(436, 186);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Sftp Credentials";
			// 
			// txtFTPUser
			// 
			this.txtFTPUser.BackColor = System.Drawing.SystemColors.Window;
			this.txtFTPUser.Location = new System.Drawing.Point(150, 46);
			this.txtFTPUser.MaxLength = 30;
			this.txtFTPUser.Name = "txtFTPUser";
			this.txtFTPUser.Size = new System.Drawing.Size(205, 40);
			this.txtFTPUser.TabIndex = 3;
			// 
			// txtFTPPassword
			// 
			this.txtFTPPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtFTPPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtFTPPassword.Location = new System.Drawing.Point(150, 96);
			this.txtFTPPassword.MaxLength = 30;
			this.txtFTPPassword.Name = "txtFTPPassword";
			this.txtFTPPassword.PasswordChar = '*';
			this.txtFTPPassword.Size = new System.Drawing.Size(205, 40);
			this.txtFTPPassword.TabIndex = 5;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 110);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(88, 20);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "PASSWORD";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 60);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(66, 20);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "USER";
			// 
			// frmBulkMailText
			// 
			this.frmBulkMailText.Controls.Add(this.txtBulkMail_5);
			this.frmBulkMailText.Controls.Add(this.txtBulkMail_4);
			this.frmBulkMailText.Controls.Add(this.txtBulkMail_3);
			this.frmBulkMailText.Controls.Add(this.txtBulkMail_2);
			this.frmBulkMailText.Controls.Add(this.txtBulkMail_1);
			this.frmBulkMailText.Location = new System.Drawing.Point(30, 234);
			this.frmBulkMailText.Name = "frmBulkMailText";
			this.frmBulkMailText.Size = new System.Drawing.Size(315, 289);
			this.frmBulkMailText.TabIndex = 1;
			this.frmBulkMailText.Text = "Bulk Mail Text";
			// 
			// txtBulkMail_5
			// 
			this.txtBulkMail_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtBulkMail_5.Location = new System.Drawing.Point(20, 230);
			this.txtBulkMail_5.Name = "txtBulkMail_5";
			this.txtBulkMail_5.Size = new System.Drawing.Size(262, 40);
			this.txtBulkMail_5.TabIndex = 4;
			// 
			// txtBulkMail_4
			// 
			this.txtBulkMail_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtBulkMail_4.Location = new System.Drawing.Point(20, 180);
			this.txtBulkMail_4.MaxLength = 30;
			this.txtBulkMail_4.Name = "txtBulkMail_4";
			this.txtBulkMail_4.Size = new System.Drawing.Size(262, 40);
			this.txtBulkMail_4.TabIndex = 3;
			// 
			// txtBulkMail_3
			// 
			this.txtBulkMail_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtBulkMail_3.Location = new System.Drawing.Point(20, 130);
			this.txtBulkMail_3.MaxLength = 30;
			this.txtBulkMail_3.Name = "txtBulkMail_3";
			this.txtBulkMail_3.Size = new System.Drawing.Size(262, 40);
			this.txtBulkMail_3.TabIndex = 2;
			// 
			// txtBulkMail_2
			// 
			this.txtBulkMail_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtBulkMail_2.Location = new System.Drawing.Point(20, 80);
			this.txtBulkMail_2.MaxLength = 30;
			this.txtBulkMail_2.Name = "txtBulkMail_2";
			this.txtBulkMail_2.Size = new System.Drawing.Size(262, 40);
			this.txtBulkMail_2.TabIndex = 1;
			// 
			// txtBulkMail_1
			// 
			this.txtBulkMail_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtBulkMail_1.Location = new System.Drawing.Point(20, 30);
			this.txtBulkMail_1.MaxLength = 30;
			this.txtBulkMail_1.Name = "txtBulkMail_1";
			this.txtBulkMail_1.Size = new System.Drawing.Size(262, 40);
			this.txtBulkMail_1.TabIndex = 0;
			// 
			// fraTownSeal
			// 
			this.fraTownSeal.Controls.Add(this.cmdBrowse);
			this.fraTownSeal.Controls.Add(this.txtTownSeal);
			this.fraTownSeal.Location = new System.Drawing.Point(499, 30);
			this.fraTownSeal.Name = "fraTownSeal";
			this.fraTownSeal.Size = new System.Drawing.Size(383, 87);
			this.fraTownSeal.TabIndex = 2;
			this.fraTownSeal.Text = "Set Town Seal";
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.ForeColor = System.Drawing.Color.White;
			this.cmdBrowse.Location = new System.Drawing.Point(286, 30);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(76, 40);
			this.cmdBrowse.TabIndex = 1;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// txtTownSeal
			// 
			this.txtTownSeal.BackColor = System.Drawing.SystemColors.Window;
			this.txtTownSeal.Location = new System.Drawing.Point(20, 30);
			this.txtTownSeal.Name = "txtTownSeal";
			this.txtTownSeal.Size = new System.Drawing.Size(250, 40);
			this.txtTownSeal.TabIndex = 0;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSaveExit,
            this.Seperator,
            this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 1;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 3;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(409, 30);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(80, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Save";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// cmdConfigurePayportSettings
			// 
			this.cmdConfigurePayportSettings.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdConfigurePayportSettings.Location = new System.Drawing.Point(696, 31);
			this.cmdConfigurePayportSettings.Name = "cmdConfigurePayportSettings";
			this.cmdConfigurePayportSettings.Size = new System.Drawing.Size(181, 24);
			this.cmdConfigurePayportSettings.TabIndex = 2;
			this.cmdConfigurePayportSettings.Text = "Configure PayPort Settings";
			this.cmdConfigurePayportSettings.Visible = false;
			this.cmdConfigurePayportSettings.Click += new System.EventHandler(this.cmdConfigurePayportSettings_Click);
			// 
			// frmGNCustomize
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(898, 722);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmGNCustomize";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Customize";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmGNCustomize_Load);
			this.Activated += new System.EventHandler(this.frmGNCustomize_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGNCustomize_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.frmBulkMailText)).EndInit();
			this.frmBulkMailText.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTownSeal)).EndInit();
			this.fraTownSeal.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdConfigurePayportSettings)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
		private FCButton cmdConfigurePayportSettings;
	}
}
