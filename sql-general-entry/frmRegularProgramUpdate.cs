//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRegularProgramUpdate.
	/// </summary>
	public partial class frmRegularProgramUpdate : fecherFoundation.FCForm
	{
;

		public frmRegularProgramUpdate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.optZip = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optZip.AddControlArrayElement(optZip_1, 1 );
			this.optZip.AddControlArrayElement(optZip_0, 0 );

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null && Wisej.Web.Application.OpenForms.Count == 0)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRegularProgramUpdate InstancePtr
		{
			get
			{
				if (_InstancePtr == null) // || _InstancePtr.IsDisposed
					_InstancePtr = new frmRegularProgramUpdate();
				return _InstancePtr;
			}
		}
		protected static frmRegularProgramUpdate _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		int intProgram;
		string BackupDrive = "";
		string datNewDate = "";
		int intCounter;
		string strTemp = "";
		FileInfo fNewFile;
		Scripting.Folder fNewFolder;
		bool boolZip;
		Scripting.Folder fFolder;
		string strPath = "";
		bool boolLoading;
		bool boolDLL;
		bool boolUpdated;
		Scripting.Drive dDrive;
		private cZip m_cZ;


		private void cmdBackup_Click(object sender, System.EventArgs e)
		{
			int intCounter;

			if (modBackupReplace.strDriveLetter==string.Empty) {
				MessageBox.Show("A drive and folder must be chosen first.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			boolUpdated = true;

			modBackupReplace.gstrFileName = string.Empty;
			for(intCounter=0; intCounter<=lstFiles.Items.Count-1; intCounter++) {
				if (lstFiles.GetSelected(intCounter)) {
					modBackupReplace.gstrFileName = lstFiles.Items[intCounter].ToString();
					break;
				}
			}

			if (modBackupReplace.gstrFileName==string.Empty) {
				MessageBox.Show("A file must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}

			RestoreModule();
		}

		private void cmdDir_Click(object sender, System.EventArgs e)
		{
			string strTemp;

			modGNBas.boolCancelled = false;
			//! Load frmPath;
			frmPath.InstancePtr.Text = "Program Update";
			frmPath.InstancePtr.lblTitle.Text = "Choose the drive where the update files are located.";
			frmPath.InstancePtr.File1.Visible = false;
			frmPath.InstancePtr.Dir1.Left = frmPath.InstancePtr.Drive1.Left;
			frmPath.InstancePtr.Dir1.Visible = false;
			frmPath.InstancePtr.Show(FCForm.FormShowEnum.Modal);

			if (modGNBas.boolCancelled) {
				modGNBas.boolCancelled = false;
				return;
			}

			modBackupReplace.strDriveLetter = modGNBas.gstrFilePath;
			if (modBackupReplace.strDriveLetter==string.Empty) return;
			strTemp = Environment.CurrentDirectory;
			dDrive = ff.GetDrive(Strings.Left(modBackupReplace.strDriveLetter, 1));
			Environment.CurrentDirectory = strTemp;
			if (modBackupReplace.strDriveLetter.Length==1) {
				fNewFolder = ff.GetFolder(modBackupReplace.strDriveLetter+":\\");
				modBackupReplace.strDriveLetter += ":\\";
			} else {
				fNewFolder = ff.GetFolder(modBackupReplace.strDriveLetter);
			}

			Label3.Text = string.Empty;
			Label3.Text = "Update source directory: "+modBackupReplace.strDriveLetter;

			if (modBackupReplace.strDriveLetter==string.Empty) {
				MessageBox.Show("Drive letter must be set.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			GetValidFiles();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void RestoreModule()
		{
			try
			{	// On Error GoTo ErrorHandler
				modTrioStart.STARTUPINFO si = new modTrioStart.STARTUPINFO();
				modTrioStart.PROCESS_INFORMATION pi = new modTrioStart.PROCESS_INFORMATION();
				int lngRet;
				bool boolCopiedDlls;
				string strFileName = "";
				string strTemp = "";
				string strMast;

				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;

				modTrioStart.boolNoDLLsRegistered = true;
				if (modBackupReplace.strDriveLetter.Length==1) {
					fFolder = ff.GetFolder(modBackupReplace.strDriveLetter+":\\");
					strPath = modBackupReplace.strDriveLetter+":\\";
				} else {
					fFolder = ff.GetFolder(modBackupReplace.strDriveLetter);
					strPath = modBackupReplace.strDriveLetter;
				}

				// strMast = Left(CurDir, 1) & ":\TrioData\TrioMast\"
				strMast = modGlobalFunctions.gGlobalSettings.MasterPath;
				if (Strings.Right("\\"+strMast, 1)!="\\") {
					strMast += "\\";
				}
				// if zip file is chosen prepare file
				// If optZip(0) Then
				// Call GetZipFiles
				// 
				// Else
				for(intCounter=0; intCounter<=lstFiles.Items.Count-1; intCounter++) {
					if (lstFiles.GetSelected(intCounter)) {
						if (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="ZIP") {
							modBackupReplace.gstrFileName = lstFiles.Items[intCounter].ToString();
							GetZipFiles();
							// ElseIf UCase(lstFiles.List(intCounter)) = "TWPERM.VB1" Then
							// if this is twperm then copy it and fill the genenty database
							// Call ff.CopyFile(strPath & "twperm.vb1", CurDir & "\twperm.vb1")
							// Set fNewFile = ff.GetFile("twperm.vb1")
							// fNewFile.Attributes = Normal
							// twperm is copied over. Now fill systemsettings with the info
							// Call UpdateSecurityTables

						} else if (Strings.UCase(lstFiles.Items[intCounter].ToString())=="DLL'S OR OCX'S") {
							strFileName = FCFileSystem.Dir(strPath+"*.*", 0);
							while (strFileName!=string.Empty) {
								strTemp = Strings.UCase(Strings.Right(strFileName, 3));
								
								if ((strTemp=="OCX") || (strTemp=="TLB") || (strTemp=="DLL"))
								{
									// Copy it to TrioMast
									ff.CopyFile(strPath+strFileName, strMast+strFileName, true);
									fNewFile = ff.GetFile(strMast+strFileName);
									fNewFile.Attributes = FileAttributes.Normal;
									fNewFile = null;
									boolCopiedDlls = true;
								}
								else 
								{
								}

								strFileName = FCFileSystem.Dir();
							}
						} else {

							// copy file if this exists
							if (((Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3)))=="EXE") || (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="DLL") || (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="OCX") || (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="TLB") || ((Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3)))=="HLP") || ((Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3)))=="CNT")) {
								if (!(Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="EXE")) {
									boolCopiedDlls = true;
								} else {
									boolCopiedDlls = false;
								}
								// gstrBackupPath = Left(CurDir, 1) & ":\TrioData\TrioMast"
								modGNBas.gstrBackupPath = modGlobalFunctions.gGlobalSettings.MasterPath;
							} else {
								modGNBas.gstrBackupPath = Environment.CurrentDirectory;
							}
							modGNBas.gstrBackupPath = (Strings.Right(modGNBas.gstrBackupPath, 1)!="\\" ? modGNBas.gstrBackupPath+"\\" : modGNBas.gstrBackupPath);

							if (File.Exists(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString())) {
								if (File.Exists(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString()+".old")) {
									fNewFile = new FileInfo(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString()+".old");
									if (fNewFile.Attributes && Scripting.FileAttribute.ReadOnly) {
										// fNewFile.Attributes = fNewFile.Attributes - READONLY
										fNewFile.Attributes = FileAttributes.Normal;
									}
								}
								fNewFile = ff.GetFile(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString());
								fNewFile.Copy(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString()+".old", true);
							}

						CopyFile: ;
							if (ff.FileExists(strPath+lstFiles.Items[intCounter].ToString())) {
								fNewFile = ff.GetFile(strPath+lstFiles.Items[intCounter].ToString());
								fNewFile.Copy(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString());
							}

							fNewFile = ff.GetFile(modGNBas.gstrBackupPath+lstFiles.Items[intCounter].ToString());
							fNewFile.Attributes = FileAttributes.Normal;
							// If fNewFile.Attributes And Archive Then
							// fNewFile.Attributes = fNewFile.Attributes - Archive
							// End If
							// 
							// If fNewFile.Attributes And READONLY Then
							// fNewFile.Attributes = fNewFile.Attributes - READONLY
							// End If
						}
					}
				}

				MessageBox.Show("Update of programs successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;

				// If boolCopiedDlls Then
				// boolFromUpdateProgs = True
				// 
				// Call SetupDLLs
				// boolFromUpdateProgs = False
				// End If

				if (!modTrioStart.boolNoDLLsRegistered) {
					MessageBox.Show("Update has Registered some new DLL files.  In order to use these TRIO will have to exit."+"\n"+"Please wait a few seconds before restarting TRIO to make sure it has had enough time to shut down properly.", "Must restart TRIO", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// lngRet = CreateProcess(App.Path & "\" & App.EXEName & ".exe", vbNullString, 0, 0, False, CREATE_NEW_PROCESS_GROUP And NORMAL_PRIORITY_CLASS, 0, vbNullString, si, pi)

					// If lngRet = 0 Then
					// MsgBox "Error in SetupDLLs, couldn't restart" & vbNewLine & "Trio will now terminate." & vbNewLine & "You will have to restart yourself.", vbCritical
					// End
					// Else
					// End
					// End If
					// 
					// DoEvents
					Application.Exit();
				}


				return;

			}
			catch (Exception ex)
            {	// ErrorHandler:
				if (Information.Err(ex).Number==53) {
					// file not found
					// Err.Clear
					// Err.Number = 0
					// GoTo CopyFile
					{/*? Resume Next; */}
				} else {
					// MsgBox Err.Description
					{/*? Resume Next; */}
				}
			}
		}

		private void frmRegularProgramUpdate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRegularProgramUpdate properties;
			//frmRegularProgramUpdate.ScaleWidth	= 5880;
			//frmRegularProgramUpdate.ScaleHeight	= 4365;
			//frmRegularProgramUpdate.LinkTopic	= "Form1";
			//frmRegularProgramUpdate.LockControls	= true;
			//End Unmaped Properties

			 /*? On Error Resume Next  */
try {
			// secDB.DisconnectDatabase
			// dbVariables.Close

			try
			{	// On Error GoTo EndTag
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.TRIOCOLORBLUE);

				// BackupDrive = UCase(IIf(GetRegistryKey("UpdateDisk") <> vbNullString, gstrReturn, "0"))
				// strDriveLetter = UCase(IIf(GetRegistryKey("UpdateDisk") <> vbNullString, gstrReturn, ""))
				// 
				// If Trim(strDriveLetter) <> vbNullString Then
				// If Len(strDriveLetter) = 2 Then
				// strDriveLetter = strDriveLetter & "\"
				// ElseIf Len(strDriveLetter) = 1 Then
				// strDriveLetter = strDriveLetter & ":\"
				// End If
				// Label3.Caption = "Update source directory: " & strDriveLetter
				// Call GetValidFiles
				// End If
				return;

			}
			catch (Exception ex)
                {	// EndTag:
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
} catch { }
			}
		}

		private void Form_Unload(ref short Cancel)
		{
			// frmEntryMenu.Show
			if (boolUpdated) {
				MessageBox.Show("In order to take advantage of updated programs or register DLL files"+"\n"+"anyone running a TRIO windows program will need to exit TRIO and restart.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		static int counter = 0;
		private void lstFiles_SelectedIndexChanged(object sender, System.EventArgs e)
		{


			if (boolLoading) return;
			if (counter==2) {
				counter = 1;
				return;
			}

			if (Strings.UCase(lstFiles.Items[lstFiles.SelectedIndex].ToString())=="DLL'S OR OCX'S") {
				return;
			}

			if (Strings.Right(modBackupReplace.strDriveLetter, 1)!="\\") modBackupReplace.strDriveLetter += "\\";
			if (FCFileSystem.Dir(modBackupReplace.strDriveLetter+lstFiles.Items[lstFiles.SelectedIndex].ToString(), Microsoft.VisualBasic.FileAttribute.Normal || Microsoft.VisualBasic.FileAttribute.System || Microsoft.VisualBasic.FileAttribute.Archive || Microsoft.VisualBasic.FileAttribute.ReadOnly || Microsoft.VisualBasic.FileAttribute.Hidden)==string.Empty) {
				counter = 2;
				lstFiles.SetSelected(lstFiles.ListIndex, false);

				MessageBox.Show("File '"+lstFiles.Items[lstFiles.SelectedIndex].ToString()+"' is not in the current directory."+modBackupReplace.strDriveLetter, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}

			// If lstFiles.Selected(lstFiles.ListIndex) Then
			// check to see if any other files are selected.
			// Dim intCounter As Integer
			// For intCounter = 0 To lstFiles.ListCount - 1
			// If lstFiles.Selected(intCounter) And intCounter <> lstFiles.ListIndex Then
			// counter = 2
			// lstFiles.Selected(intCounter) = False
			// End If
			// Next
			// End If
		}

		public void GetValidFiles()
		{
			Scripting.File fFile;

			if (modBackupReplace.strDriveLetter==string.Empty) modBackupReplace.strDriveLetter = Application.StartupPath;
			if (modBackupReplace.strDriveLetter.Length==1) {
				fNewFolder = ff.GetFolder(modBackupReplace.strDriveLetter+":\\");
			} else {
				fNewFolder = ff.GetFolder(modBackupReplace.strDriveLetter);
			}
			boolDLL = false;

			lstFiles.Clear();
			foreach (int fFile in fNewFolder.Files) {
				// If optZip(0) Then
				// If UCase(Right(fFile.Name, 3)) = "ZIP" Then
				
				if ((Strings.UCase(Strings.Right(fFile.Name, 3))=="ZIP") || (Strings.UCase(Strings.Right(fFile.Name, 3))=="EXE") || (Strings.UCase(Strings.Right(fFile.Name, 3))=="VB1"))
				{
					lstFiles.AddItem(fFile.Name);
					// lstFiles.Selected(lstFiles.NewIndex) = True
					// End If
					// Else
					// If UCase(Right(fFile.Name, 3)) <> "ZIP" Then
					// lstFiles.AddItem fFile.Name
					lstFiles.SetSelected(lstFiles.NewIndex, true);
					// End If
					// End If
				}
				else if ((Strings.UCase(Strings.Right(fFile.Name, 3))=="DLL") || (Strings.UCase(Strings.Right(fFile.Name, 3))=="TLB") || (Strings.UCase(Strings.Right(fFile.Name, 3))=="OCX"))
				{

					boolDLL = true;
				}
				else 
				{
					lstFiles.AddItem(fFile.Name);
					lstFiles.SetSelected(lstFiles.NewIndex, true);
				}
			}

			if (boolDLL) {
				lstFiles.AddItem("DLL's or OCX's");
				lstFiles.SetSelected(lstFiles.NewIndex, true);
			}
		}

		public void GetZipFiles()
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;

			this.Hide();
			frmUnzipFiles.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			vbPorterConverter2.ShowModeless(this);
		}

		private void optZip_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (modBackupReplace.strDriveLetter==string.Empty) {
				MessageBox.Show("Drive letter must be updated.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			GetValidFiles();
		}
		private void optZip_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = optZip.GetIndex((FCRadioButton)sender);
			optZip_CheckedChanged(index, sender, e);
		}


		private void cmbZip_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbZip.SelectedIndex == 0)
			{
				optZip_CheckedChanged(sender, e);
			}
			else if (cmbZip.SelectedIndex == 1)
			{
				optZip_CheckedChanged(sender, e);
			}
		}
	}
}
