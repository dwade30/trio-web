//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmProgramUpdate.
	/// </summary>
	public partial class frmProgramUpdate : fecherFoundation.FCForm
	{
;

		public frmProgramUpdate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null && Wisej.Web.Application.OpenForms.Count == 0)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmProgramUpdate InstancePtr
		{
			get
			{
				if (_InstancePtr == null) // || _InstancePtr.IsDisposed
					_InstancePtr = new frmProgramUpdate();
				return _InstancePtr;
			}
		}
		protected static frmProgramUpdate _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================

		string strType = "";

		private void cmdDir_Click(object sender, System.EventArgs e)
		{

			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CDRom Drive", Strings.Left(Drive1.Text, 1));
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Data Drive", Strings.Left(Environment.CurrentDirectory, 1)+":\\");



			if (File.Exists(Strings.Left(Drive1.Drive, 1)+":\\setup.exe")) {
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "InstallShield Type", ref strType);
				Interaction.Shell(Strings.Left(Drive1.Text, 1)+":\\setup.exe", AppWinStyle.MaximizedFocus, false, -1);
				modReplaceWorkFiles.EntryFlagFile(true, "", true);
				this.Unload();
				Application.Exit();
			} else {
				MessageBox.Show("Unable to find Install executable "+Strings.Left(Drive1.Drive, 1)+":\\setup.exe", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}

			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			this.Unload();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void frmProgramUpdate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmProgramUpdate properties;
			//frmProgramUpdate.ScaleWidth	= 3885;
			//frmProgramUpdate.ScaleHeight	= 2505;
			//frmProgramUpdate.LinkTopic	= "Form1";
			//End Unmaped Properties

			if (strType=="AUTO") {
				this.Text = "Process Update Disk";
			} else {
				this.Text = "Process DLL Disk";
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			// Label2.ForeColor = TRIOCOLORBLUE
		}

		public void Init(ref bool boolFullInstall)
		{
			if (boolFullInstall) {
				strType = "AUTO";
			} else {
				strType = "DLLONLY";
			}
			this.Show(MDIParent.InstancePtr);
		}

	}
}