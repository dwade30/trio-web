//Fecher vbPorter - Version 1.0.0.40

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmUpdateType.
	/// </summary>
	public partial class frmUpdateType : fecherFoundation.FCForm
	{
;

		public frmUpdateType()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		private void cmdDLLOnly_Click(object sender, System.EventArgs e)
		{
			frmProgramUpdate.InstancePtr.Init(false);
			this.Unload();
		}

		private void cmdInstallShield_Click(object sender, System.EventArgs e)
		{
			frmProgramUpdate.InstancePtr.Init(true);
			this.Unload();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void cmdRegularUpdate_Click(object sender, System.EventArgs e)
		{
			frmRegularProgramUpdate.InstancePtr.Show();
			this.Unload();
		}

		private void frmUpdateType_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUpdateType properties;
			//frmUpdateType.ScaleWidth	= 3975;
			//frmUpdateType.ScaleHeight	= 2565;
			//frmUpdateType.LinkTopic	= "Form1";
			//End Unmaped Properties

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}


	}
}