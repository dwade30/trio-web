﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptPrinterFontTest.
	/// </summary>
	public partial class rptPrinterFontTest : BaseSectionReport
	{
		public rptPrinterFontTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Printer Font Test";
		}

		public static rptPrinterFontTest InstancePtr
		{
			get
			{
				return (rptPrinterFontTest)Sys.GetInstance(typeof(rptPrinterFontTest));
			}
		}

		protected rptPrinterFontTest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrinterFontTest	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strPName;

		public void Init(ref string strPrinterName)
		{
			strPName = strPrinterName;
			this.PrintReport(false);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strFont;
			if (Strings.Trim(strPName) != string.Empty)
			{
				foreach (FCPrinter tempPrinter in FCGlobal.Printers)
				{
					if (tempPrinter.DeviceName == strPName)
					{
						this.Document.Printer.PrinterName = tempPrinter.DeviceName;
						break;
					}
				}
			}
			//this.Printer.RenderMode = 1;
			strFont = modGNBas.GetFont_6(this.Document.Printer.PrinterName, 10);
			if (strFont != string.Empty)
			{
				txt10CPI.Font = new Font(strFont, txt10CPI.Font.Size);
				txt10CPI.Text = "10 CPI Font using " + strFont;
			}
			else
			{
				txt10CPI.Text = "Could not find a 10 CPI font";
			}
			strFont = modGNBas.GetFont_6(this.Document.Printer.PrinterName, 12);
			if (strFont != string.Empty)
			{
				txt12cpi.Font = new Font(strFont, txt12cpi.Font.Size);
				txt12cpi.Text = "12 CPI Font using " + strFont;
			}
			else
			{
				txt12cpi.Text = "Could not find a 12 CPI font";
			}
			strFont = modGNBas.GetFont_6(this.Document.Printer.PrinterName, 17);
			if (strFont != string.Empty)
			{
				txt17cpi.Font = new Font(strFont, txt17cpi.Font.Size);
				txt17cpi.Text = "17 CPI Font using " + strFont;
			}
			else
			{
				txt17cpi.Text = "Could not find a 17 CPI font";
			}
		}

		
	}
}
