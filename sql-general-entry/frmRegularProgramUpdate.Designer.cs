//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRegularProgramUpdate.
	/// </summary>
	partial class frmRegularProgramUpdate : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbZip;
		public fecherFoundation.FCLabel lblZip;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optZip;
		public fecherFoundation.FCButton cmdDir;
		public fecherFoundation.FCProgressBar pbrProgress;
		public fecherFoundation.FCListBox lstFiles;
		public fecherFoundation.FCButton cmdBackup;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
				_InstancePtr = null;
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbZip = new fecherFoundation.FCComboBox();
			this.lblZip = new fecherFoundation.FCLabel();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmRegularProgramUpdate));
			this.components = new System.ComponentModel.Container();
			//this.optZip = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.cmdDir = new fecherFoundation.FCButton();
			this.pbrProgress = new fecherFoundation.FCProgressBar();
			this.lstFiles = new fecherFoundation.FCListBox();
			this.cmdBackup = new fecherFoundation.FCButton();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.lblPercent = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			
			this.SuspendLayout();
			//
			// cmbZip
			//
			this.cmbZip.Items.Add("Yes");
			this.cmbZip.Items.Add("No");
			this.cmbZip.Name = "cmbZip";
			this.cmbZip.Location = new System.Drawing.Point(16, 20);
			this.cmbZip.Size = new System.Drawing.Size(120, 40);
			this.cmbZip.Text = "Yes";
			this.cmbZip.SelectedIndexChanged += new System.EventHandler(this.cmbZip_SelectedIndexChanged);
			//
			// lblZip
			//
			this.lblZip.Name = "lblZip";
			this.lblZip.Text = "lblZip";
			this.lblZip.Location = new System.Drawing.Point(1, 20);
			this.lblZip.AutoSize = true;

			//((System.ComponentModel.ISupportInitialize)(this.optZip)).BeginInit();
			//
			// cmdDir
			//
			this.cmdDir.Name = "cmdDir";
			this.cmdDir.TabIndex = 10;
			this.cmdDir.Location = new System.Drawing.Point(219, 25);
			this.cmdDir.Size = new System.Drawing.Size(159, 23);
			this.cmdDir.Text = "Choose Source Drive";
			this.cmdDir.BackColor = System.Drawing.SystemColors.Control;
			this.cmdDir.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdDir.Click += new System.EventHandler(this.cmdDir_Click);
			//
			// pbrProgress
			//
			this.pbrProgress.Name = "pbrProgress";
			this.pbrProgress.TabIndex = 7;
			this.pbrProgress.Location = new System.Drawing.Point(22, 306);
			this.pbrProgress.Size = new System.Drawing.Size(410, 14);
			this.pbrProgress.Minimum = 0;
			this.pbrProgress.Maximum = 100;
			//
			// lstFiles
			//
			this.lstFiles.Name = "lstFiles";
			this.lstFiles.TabIndex = 2;
			this.lstFiles.Location = new System.Drawing.Point(20, 25);
			this.lstFiles.Size = new System.Drawing.Size(179, 204);
			this.lstFiles.BackColor = System.Drawing.SystemColors.Window;
			this.lstFiles.SelectedIndexChanged += new System.EventHandler(this.lstFiles_SelectedIndexChanged);
			//
			// cmdBackup
			//
			this.cmdBackup.Name = "cmdBackup";
			this.cmdBackup.TabIndex = 1;
			this.cmdBackup.Location = new System.Drawing.Point(219, 49);
			this.cmdBackup.Size = new System.Drawing.Size(159, 23);
			this.cmdBackup.Text = "Process";
			this.cmdBackup.BackColor = System.Drawing.SystemColors.Control;
			this.cmdBackup.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdBackup.Click += new System.EventHandler(this.cmdBackup_Click);
			//
			// cmdQuit
			//
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.TabIndex = 0;
			this.cmdQuit.Location = new System.Drawing.Point(219, 73);
			this.cmdQuit.Size = new System.Drawing.Size(159, 23);
			this.cmdQuit.Text = "Quit";
			this.cmdQuit.BackColor = System.Drawing.SystemColors.Control;
			this.cmdQuit.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			//
			//
			//
			// lblPercent
			//
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.TabIndex = 9;
			this.lblPercent.Location = new System.Drawing.Point(439, 306);
			this.lblPercent.Size = new System.Drawing.Size(91, 16);
			this.lblPercent.Text = "";
			this.lblPercent.BackColor = System.Drawing.SystemColors.Control;
			this.lblPercent.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.lblPercent.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.Tag = "Zipping File";
			this.Label3.TabIndex = 8;
			this.Label3.Location = new System.Drawing.Point(22, 250);
			this.Label3.Size = new System.Drawing.Size(352, 16);
			this.Label3.Text = "";
			this.Label3.BackColor = System.Drawing.SystemColors.Control;
			this.Label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label3.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 3;
			this.Label2.Location = new System.Drawing.Point(61, 5);
			this.Label2.Size = new System.Drawing.Size(105, 16);
			this.Label2.Text = "Select Files";
			this.Label2.BackColor = System.Drawing.SystemColors.Control;
			this.Label2.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(330, 150);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// frmRegularProgramUpdate
			//
			this.ClientSize = new System.Drawing.Size(396, 294);
			this.Controls.Add(this.cmdDir);
			this.Controls.Add(this.pbrProgress);
			this.Controls.Add(this.lstFiles);
			this.Controls.Add(this.cmdBackup);
			this.Controls.Add(this.cmdQuit);
			this.Controls.Add(this.cmbZip);
			this.Controls.Add(this.lblZip);

			this.Controls.Add(this.lblPercent);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			//this.Controls.Add(this.vsElasticLight1);
			this.Name = "frmRegularProgramUpdate";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmRegularProgramUpdate.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Load += new System.EventHandler(this.frmRegularProgramUpdate_Load);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmRegularProgramUpdate_FormClosing);
			this.Text = "Program Update";
			//((System.ComponentModel.ISupportInitialize)(this.optZip)).EndInit();
			this.cmdDir.ResumeLayout(false);
			this.cmdBackup.ResumeLayout(false);
			this.cmdQuit.ResumeLayout(false);
			this.lblPercent.ResumeLayout(false);
			this.Label3.ResumeLayout(false);
			this.Label2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}