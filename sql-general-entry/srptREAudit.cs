﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptREAudit.
	/// </summary>
	public partial class srptREAudit : FCSectionReport
	{
		public srptREAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptREAudit";
		}

		public static srptREAudit InstancePtr
		{
			get
			{
				return (srptREAudit)Sys.GetInstance(typeof(srptREAudit));
			}
		}

		protected srptREAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptREAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void Detail_Format(object sender, EventArgs e)
		{
			clsLoad.OpenRecordset("select sum(convert(float,rllandval)) as totcalcland,sum(convert(float,rlbldgval)) as totcalcbldg,sum(convert(float,lastlandval)) as totland,sum(convert(float,lastbldgval)) as totbldg,sum(convert(float,rlexemption)) as totexempt,sum(convert(float,RSsoft)) AS TotSOFTACRES,sum(convert(float,rssoftvalue)) as Totsoftvalue,sum(convert(float,rsmixed)) as TotMixedAcres,sum(convert(float,rsmixedvalue)) as TotMixedValue,sum(convert(float,rshard)) as TotHardAcres,sum(convert(float,rshardvalue)) as TotHardValue,sum(convert(float,Rsother)) as TotOtherAcres,sum(convert(float,RsotherValue)) as TotOtherValue from master where not rsdeleted = 1", "twre0000.vb1");
			// TODO Get_Fields: Field [totland] not found!! (maybe it is an alias?)
			txtLand.Text = Strings.Format(clsLoad.Get_Fields("totland"), "#,###,###,##0");
			// TODO Get_Fields: Field [totbldg] not found!! (maybe it is an alias?)
			txtBldg.Text = Strings.Format(clsLoad.Get_Fields("totbldg"), "#,###,###,##0");
			if (modGlobalConstants.Statics.gboolRE)
			{
				// TODO Get_Fields: Field [totcalcland] not found!! (maybe it is an alias?)
				txtCalcLand.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcalcland")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcalcbldg] not found!! (maybe it is an alias?)
				txtCalcBldg.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcalcbldg")), "#,###,###,##0");
			}
			else
			{
				txtCalcLand.Text = "N/A";
				txtCalcBldg.Text = "N/A";
			}
			// TODO Get_Fields: Field [totexempt] not found!! (maybe it is an alias?)
			txtExempt.Text = Strings.Format(clsLoad.Get_Fields("totexempt"), "#,###,###,##0");
			// TODO Get_Fields: Field [totsoftacres] not found!! (maybe it is an alias?)
			txtSoftAcres.Text = Strings.Format(clsLoad.Get_Fields("totsoftacres"), "#,###,##0.00");
			// TODO Get_Fields: Field [totmixedacres] not found!! (maybe it is an alias?)
			txtMixedAcres.Text = Strings.Format(clsLoad.Get_Fields("totmixedacres"), "#,###,##0.00");
			// TODO Get_Fields: Field [tothardacres] not found!! (maybe it is an alias?)
			txtHardAcres.Text = Strings.Format(clsLoad.Get_Fields("tothardacres"), "#,###,##0.00");
			// TODO Get_Fields: Field [Totsoftvalue] not found!! (maybe it is an alias?)
			txtSoftValue.Text = Strings.Format(clsLoad.Get_Fields("Totsoftvalue"), "#,###,###,##0");
			// TODO Get_Fields: Field [totmixedvalue] not found!! (maybe it is an alias?)
			txtMixedValue.Text = Strings.Format(clsLoad.Get_Fields("totmixedvalue"), "#,###,###,##0");
			// TODO Get_Fields: Field [tothardvalue] not found!! (maybe it is an alias?)
			txtHardValue.Text = Strings.Format(clsLoad.Get_Fields("tothardvalue"), "#,###,###,##0");
			// TODO Get_Fields: Field [TotOtherAcres] not found!! (maybe it is an alias?)
			txtOtherAcres.Text = Strings.Format(clsLoad.Get_Fields("TotOtherAcres"), "#,###,##0.00");
			// TODO Get_Fields: Field [TotOtherValue] not found!! (maybe it is an alias?)
			txtOtherValue.Text = Strings.Format(clsLoad.Get_Fields("TotOtherValue"), "#,###,###,##0");
			// TODO Get_Fields: Field [totsoftacres] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [totmixedacres] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [tothardacres] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [tototheracres] not found!! (maybe it is an alias?)
			txtTotalAcres.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totsoftacres")) + Conversion.Val(clsLoad.Get_Fields("totmixedacres")) + Conversion.Val(clsLoad.Get_Fields("tothardacres")) + Conversion.Val(clsLoad.Get_Fields("tototheracres")), "#,###,##0.00");
			// TODO Get_Fields: Field [totsoftvalue] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [totmixedvalue] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [tothardvalue] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [totothervalue] not found!! (maybe it is an alias?)
			txtTotalValue.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totsoftvalue")) + Conversion.Val(clsLoad.Get_Fields("totmixedvalue")) + Conversion.Val(clsLoad.Get_Fields("tothardvalue")) + Conversion.Val(clsLoad.Get_Fields("totothervalue")), "#,###,###,##0");
			// TODO Get_Fields: Field [totland] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [totbldg] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [totexempt] not found!! (maybe it is an alias?)
			txtNet.Text = Strings.Format(clsLoad.Get_Fields("totland") + clsLoad.Get_Fields("totbldg") - clsLoad.Get_Fields("totexempt"), "#,###,###,##0");
			clsLoad.OpenRecordset("select count(rsaccount) as totaccounts from master where rscard = 1 and not rsdeleted = 1", "twre0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [totaccounts] not found!! (maybe it is an alias?)
				txtAccount.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totaccounts")), "#,###,###,##0");
			}
			else
			{
				txtAccount.Text = "0";
			}
		}

		
	}
}
