﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmEditPassword.
	/// </summary>
	partial class frmEditPassword : BaseForm
	{
		public fecherFoundation.FCTextBox txtNew2;
		public fecherFoundation.FCTextBox txtNew1;
		public fecherFoundation.FCTextBox txtCurrentPassword;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditPassword));
			this.txtNew2 = new fecherFoundation.FCTextBox();
			this.txtNew1 = new fecherFoundation.FCTextBox();
			this.txtCurrentPassword = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 336);
			this.BottomPanel.Size = new System.Drawing.Size(552, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtNew2);
			this.ClientArea.Controls.Add(this.txtNew1);
			this.ClientArea.Controls.Add(this.txtCurrentPassword);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(552, 276);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(552, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(169, 30);
			this.HeaderText.Text = "Edit Password";
			// 
			// txtNew2
			// 
			this.txtNew2.AutoSize = false;
			this.txtNew2.BackColor = System.Drawing.SystemColors.Window;
			this.txtNew2.LinkItem = null;
			this.txtNew2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNew2.LinkTopic = null;
			this.txtNew2.Location = new System.Drawing.Point(252, 150);
			this.txtNew2.Name = "txtNew2";
			this.txtNew2.Size = new System.Drawing.Size(236, 40);
			this.txtNew2.TabIndex = 5;
			// 
			// txtNew1
			// 
			this.txtNew1.AutoSize = false;
			this.txtNew1.BackColor = System.Drawing.SystemColors.Window;
			this.txtNew1.LinkItem = null;
			this.txtNew1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNew1.LinkTopic = null;
			this.txtNew1.Location = new System.Drawing.Point(252, 90);
			this.txtNew1.Name = "txtNew1";
			this.txtNew1.Size = new System.Drawing.Size(236, 40);
			this.txtNew1.TabIndex = 3;
			// 
			// txtCurrentPassword
			// 
			this.txtCurrentPassword.AutoSize = false;
			this.txtCurrentPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtCurrentPassword.LinkItem = null;
			this.txtCurrentPassword.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCurrentPassword.LinkTopic = null;
			this.txtCurrentPassword.Location = new System.Drawing.Point(252, 30);
			this.txtCurrentPassword.Name = "txtCurrentPassword";
			this.txtCurrentPassword.Size = new System.Drawing.Size(236, 40);
			this.txtCurrentPassword.TabIndex = 1;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 164);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(180, 18);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "CONFIRM NEW PASSWORD";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(154, 18);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "NEW PASSWORD";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(180, 18);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "CURRENT PASSWORD";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(181, 36);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(181, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmEditPassword
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(552, 444);
			this.FillColor = 0;
			this.Name = "frmEditPassword";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Edit Password";
			this.Load += new System.EventHandler(this.frmEditPassword_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditPassword_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveContinue;
	}
}
