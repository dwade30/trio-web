﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptCL3.
	/// </summary>
	partial class srptCL3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCL3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBillingYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNumberOfBills = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDifference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkFlag = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.lblNoInformation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillingYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFlag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoInformation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBillingYear,
				this.fldCL,
				this.fldNumberOfBills,
				this.fldGL,
				this.fldDifference,
				this.chkFlag,
				this.lblNoInformation
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8
			});
			this.GroupHeader1.Height = 0.5104167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Tax Acquired O/S Bill Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.4375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 6.84375F;
			this.Line1.X1 = 0.4375F;
			this.Line1.X2 = 7.28125F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.46875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: left";
			this.Label2.Text = "Year";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 1.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.8125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "C/L";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 0.78125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.96875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "G/L";
			this.Label5.Top = 0.3125F;
			this.Label5.Width = 0.78125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Diff";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.78125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: center";
			this.Label7.Text = "Flag";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 0.53125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "text-align: right";
			this.Label8.Text = "# of Bills";
			this.Label8.Top = 0.3125F;
			this.Label8.Width = 0.78125F;
			// 
			// fldBillingYear
			// 
			this.fldBillingYear.Height = 0.19F;
			this.fldBillingYear.Left = 0.4375F;
			this.fldBillingYear.Name = "fldBillingYear";
			this.fldBillingYear.Style = "font-size: 9pt; text-align: left";
			this.fldBillingYear.Text = "Field1";
			this.fldBillingYear.Top = 0F;
			this.fldBillingYear.Width = 1.0625F;
			// 
			// fldCL
			// 
			this.fldCL.Height = 0.19F;
			this.fldCL.Left = 2.5F;
			this.fldCL.Name = "fldCL";
			this.fldCL.Style = "font-size: 9pt; text-align: right";
			this.fldCL.Text = "Field1";
			this.fldCL.Top = 0F;
			this.fldCL.Width = 1.09375F;
			// 
			// fldNumberOfBills
			// 
			this.fldNumberOfBills.Height = 0.19F;
			this.fldNumberOfBills.Left = 1.625F;
			this.fldNumberOfBills.Name = "fldNumberOfBills";
			this.fldNumberOfBills.Style = "font-size: 9pt; text-align: right";
			this.fldNumberOfBills.Text = "Field1";
			this.fldNumberOfBills.Top = 0F;
			this.fldNumberOfBills.Width = 0.78125F;
			// 
			// fldGL
			// 
			this.fldGL.Height = 0.19F;
			this.fldGL.Left = 3.65625F;
			this.fldGL.Name = "fldGL";
			this.fldGL.Style = "font-size: 9pt; text-align: right";
			this.fldGL.Text = "Field1";
			this.fldGL.Top = 0F;
			this.fldGL.Width = 1.09375F;
			// 
			// fldDifference
			// 
			this.fldDifference.Height = 0.19F;
			this.fldDifference.Left = 4.8125F;
			this.fldDifference.Name = "fldDifference";
			this.fldDifference.Style = "font-size: 9pt; text-align: right";
			this.fldDifference.Text = "Field1";
			this.fldDifference.Top = 0F;
			this.fldDifference.Width = 1.09375F;
			// 
			// chkFlag
			// 
			this.chkFlag.Height = 0.125F;
			this.chkFlag.Left = 6.15625F;
			this.chkFlag.Name = "chkFlag";
			this.chkFlag.Style = "";
			this.chkFlag.Text = null;
			this.chkFlag.Top = 0.03125F;
			this.chkFlag.Width = 0.1875F;
			// 
			// lblNoInformation
			// 
			this.lblNoInformation.Height = 0.19F;
			this.lblNoInformation.HyperLink = null;
			this.lblNoInformation.Left = 2.885417F;
			this.lblNoInformation.Name = "lblNoInformation";
			this.lblNoInformation.Style = "text-align: center";
			this.lblNoInformation.Text = "No Information";
			this.lblNoInformation.Top = 0F;
			this.lblNoInformation.Visible = false;
			this.lblNoInformation.Width = 1.75F;
			// 
			// srptCL3
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.53125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillingYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFlag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoInformation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillingYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCL;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumberOfBills;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGL;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDifference;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFlag;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoInformation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
