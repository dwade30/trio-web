﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptCR1.
	/// </summary>
	public partial class srptCR1 : FCSectionReport
	{
		public srptCR1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptCR1";
		}

		public static srptCR1 InstancePtr
		{
			get
			{
				return (srptCR1)Sys.GetInstance(typeof(srptCR1));
			}
		}

		protected srptCR1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCR1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int lngTransactionsTotal;
		// vbPorter upgrade warning: curAmountTotal As Decimal	OnWrite(short, Decimal)
		Decimal curAmountTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rsInfo.DisposeOf();
			rsInfo = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT ReceiptType as TypeCode, COUNT(receipttype) as TransactionsTotal, SUM(Amount1) as Amount1Total, SUM(Amount2) as Amount2Total, SUM(Amount3) as Amount3Total, SUM(Amount4) as Amount4Total, SUM(Amount5) as Amount5Total, SUM(Amount6) as Amount6Total FROM Archive GROUP By ReceiptType ORDER BY ReceiptType", "TWCR0000.vb1");
			blnFirstRecord = true;
			lngTransactionsTotal = 0;
			curAmountTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            if (!rsInfo.EndOfFile())
            {
                using (clsDRWrapper rsTypeInfo = new clsDRWrapper())
                {
             

                    rsTypeInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + rsInfo.Get_Fields_Int32("TypeCode"),
                        "TWCR0000.vb1");
                    if (rsTypeInfo.EndOfFile() != true)
                    {
                        fldType.Text = Strings.Format(rsInfo.Get_Fields_Int32("TypeCode"), "00") + " " +
                                       rsTypeInfo.Get_Fields_String("TypeTitle");
                    }
                    else
                    {
                        fldType.Text = Strings.Format(rsInfo.Get_Fields_Int32("TypeCode"), "00") + " UNKNOWN";
                    }

                    // TODO Get_Fields: Field [TransactionsTotal] not found!! (maybe it is an alias?)
                    fldNumberOfTransactions.Text = Strings.Format(rsInfo.Get_Fields("TransactionsTotal"), "#,##0");

                    fldAmount.Text = Strings.Format(
                        rsInfo.Get_Fields("Amount1Total") + rsInfo.Get_Fields("Amount2Total") +
                        rsInfo.Get_Fields("Amount3Total") + rsInfo.Get_Fields("Amount4Total") +
                        rsInfo.Get_Fields("Amount5Total") + rsInfo.Get_Fields("Amount6Total"), "#,##0.00");
                    // TODO Get_Fields: Field [TransactionsTotal] not found!! (maybe it is an alias?)
                    lngTransactionsTotal += rsInfo.Get_Fields("TransactionsTotal");

                    curAmountTotal += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount1Total") +
                                                      rsInfo.Get_Fields("Amount2Total") +
                                                      rsInfo.Get_Fields("Amount3Total") +
                                                      rsInfo.Get_Fields("Amount4Total") +
                                                      rsInfo.Get_Fields("Amount5Total") +
                                                      rsInfo.Get_Fields("Amount6Total"));
             
                }
            }
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldNumberOfTransactionsTotal.Text = Strings.Format(lngTransactionsTotal, "#,##0");
			fldAmountTotal.Text = Strings.Format(curAmountTotal, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsDateInfo = new clsDRWrapper())
            {
                string strStartDate = "";
                string strEndDate = "";
                rsDateInfo.OpenRecordset("SELECT * FROM Archive ORDER BY ArchiveDate", "TWCR0000.vb1");
                if (!rsDateInfo.EndOfFile())
                {
                    rsDateInfo.MoveFirst();
                    strStartDate = Strings.Format(rsDateInfo.Get_Fields_DateTime("ArchiveDate"), "MM/dd/yyyy");
                    rsDateInfo.MoveLast();
                    strEndDate = Strings.Format(rsDateInfo.Get_Fields_DateTime("ArchiveDate"), "MM/dd/yyyy");
                    lblDateRange.Text = strStartDate + " To " + strEndDate;
                }
            }
        }

		
	}
}
