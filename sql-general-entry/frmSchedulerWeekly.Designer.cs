﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSchedulerWeekly.
	/// </summary>
	partial class frmSchedulerWeekly : BaseForm
	{
		public fecherFoundation.FCGrid vsWeek;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsWeek = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWeek)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 506);
			this.BottomPanel.Size = new System.Drawing.Size(883, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsWeek);
			this.ClientArea.Size = new System.Drawing.Size(883, 446);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(883, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(202, 30);
			this.HeaderText.Text = "Weekly Schedule";
			// 
			// vsWeek
			// 
			this.vsWeek.AllowSelection = false;
			this.vsWeek.AllowUserToResizeColumns = false;
			this.vsWeek.AllowUserToResizeRows = false;
			this.vsWeek.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsWeek.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsWeek.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsWeek.BackColorBkg = System.Drawing.Color.Empty;
			this.vsWeek.BackColorFixed = System.Drawing.Color.Empty;
			this.vsWeek.BackColorSel = System.Drawing.Color.Empty;
			this.vsWeek.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsWeek.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsWeek.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsWeek.ColumnHeadersHeight = 30;
			this.vsWeek.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsWeek.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsWeek.DragIcon = null;
			this.vsWeek.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsWeek.ExtendLastCol = true;
			this.vsWeek.FixedCols = 0;
			this.vsWeek.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsWeek.FrozenCols = 0;
			this.vsWeek.GridColor = System.Drawing.Color.Empty;
			this.vsWeek.GridColorFixed = System.Drawing.Color.Empty;
			this.vsWeek.Location = new System.Drawing.Point(30, 20);
			this.vsWeek.Name = "vsWeek";
			this.vsWeek.ReadOnly = true;
			this.vsWeek.RowHeadersVisible = false;
			this.vsWeek.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsWeek.RowHeightMin = 0;
			this.vsWeek.Rows = 50;
			this.vsWeek.ScrollTipText = null;
			this.vsWeek.ShowColumnVisibilityMenu = false;
			this.vsWeek.ShowFocusCell = false;
			this.vsWeek.Size = new System.Drawing.Size(823, 402);
			this.vsWeek.StandardTab = true;
			this.vsWeek.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsWeek.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsWeek.TabIndex = 0;
			this.vsWeek.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsWeek_MouseMoveEvent);
			this.vsWeek.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsWeek_MouseDownEvent);
			this.vsWeek.MouseUp += new Wisej.Web.MouseEventHandler(this.vsWeek_MouseUpEvent);
			this.vsWeek.DoubleClick += new System.EventHandler(this.vsWeek_DblClick);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnProcessSave
			// 
			this.btnProcessSave.AppearanceKey = "acceptButton";
			this.btnProcessSave.Location = new System.Drawing.Point(377, 30);
			this.btnProcessSave.Name = "btnProcessSave";
			this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessSave.Size = new System.Drawing.Size(129, 48);
			this.btnProcessSave.TabIndex = 0;
			this.btnProcessSave.Text = "Save & Exit";
			this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmSchedulerWeekly
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(883, 614);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSchedulerWeekly";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Weekly Schedule";
			this.Load += new System.EventHandler(this.frmSchedulerWeekly_Load);
			this.Activated += new System.EventHandler(this.frmSchedulerWeekly_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSchedulerWeekly_KeyPress);
			this.Resize += new System.EventHandler(this.frmSchedulerWeekly_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWeek)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcessSave;
	}
}
