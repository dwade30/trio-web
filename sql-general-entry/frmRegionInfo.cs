﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Web.Ext.CustomProperties;
using Wisej.Core;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRegionInfo.
	/// </summary>
	public partial class frmRegionInfo : BaseForm
	{
		public frmRegionInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRegionInfo InstancePtr
		{
			get
			{
				return (frmRegionInfo)Sys.GetInstance(typeof(frmRegionInfo));
			}
		}

		protected frmRegionInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 09/23/2005
		// ********************************************************
		const int CNSTGRIDREGIONCOLNUMBER = 0;
		const int CNSTGRIDREGIONCOLNAME = 3;
		const int CNSTGRIDREGIONCOLABBR = 1;
		const int CNSTGRIDREGIONCOLRESCODE = 2;
		const int CNSTGRIDREGIONCOLAUTOID = 4;
		const int CNSTGRIDREGIONACCOUNTSCOLNUMBER = 0;
		const int CNSTGRIDREGIONACCOUNTSCOLFUND = 1;
		const int CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT = 2;
		const int CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT = 3;
		const int CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT = 4;
		const int CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT = 5;
		const int CNSTGRIDREGIONACCOUNTSCOLAUTOID = 6;
		const int CNSTGRIDBREAKDOWNCOLAUTOID = 0;
		const int CNSTGRIDBREAKDOWNCOLCODE = 1;
		const int CNSTGRIDBREAKDOWNCOLDESCRIPTION = 2;
		const int CNSTGRIDBREAKDOWNCOLFIRSTTOWN = 3;
		private bool boolDontSave;

		private void frmRegionInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRegionInfo properties;
			//frmRegionInfo.FillStyle	= 0;
			//frmRegionInfo.ScaleWidth	= 9300;
			//frmRegionInfo.ScaleHeight	= 7830;
			//frmRegionInfo.LinkTopic	= "Form2";
			//frmRegionInfo.LockControls	= true;
			//frmRegionInfo.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupRegionGrids();
			LoadRegionGrids();
			if (modGlobalConstants.Statics.gboolBD)
			{
				GridRegionAccounts.Enabled = true;
				gridBreakdown.Enabled = true;
				modValidateAccount.SetBDFormats();
				GridRegionAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				modNewAccountBox.GetFormats();
			}
			else
			{
				gridBreakdown.Enabled = false;
				gridBreakdown.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
				GridRegionAccounts.Enabled = false;
				GridRegionAccounts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			}
		}

		private void SetupRegionGrids()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strList = "";
			GridRegion.Cols = 5;
			GridRegion.Rows = 1;
			GridRegion.TextMatrix(0, CNSTGRIDREGIONCOLNUMBER, "No.");
			GridRegion.TextMatrix(0, CNSTGRIDREGIONCOLNAME, "Town Name");
			GridRegion.TextMatrix(0, CNSTGRIDREGIONCOLABBR, "Abbr");
			GridRegion.TextMatrix(0, CNSTGRIDREGIONCOLRESCODE, "Res Code");
			GridRegion.ColHidden(CNSTGRIDREGIONCOLAUTOID, true);
			if (modGlobalConstants.Statics.gboolMV)
			{
				clsLoad.OpenRecordset("select * from residence order by town", modGNBas.strMVDatabase);
				strList = "";
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [Town] not found!! (maybe it is an alias?)
					strList += clsLoad.Get_Fields("Code") + "\t" + clsLoad.Get_Fields("Town") + "|";
					clsLoad.MoveNext();
				}
				if (strList != string.Empty)
				{
					strList = Strings.Mid(strList, 1, strList.Length - 1);
				}
				GridRegion.ColComboList(CNSTGRIDREGIONCOLRESCODE, strList);
			}
			GridRegionAccounts.Cols = 7;
			GridRegionAccounts.Rows = 1;
			GridRegionAccounts.TextMatrix(0, CNSTGRIDREGIONACCOUNTSCOLNUMBER, "No.");
			GridRegionAccounts.TextMatrix(0, CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT, "RE Commitment");
			GridRegionAccounts.TextMatrix(0, CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT, "PP Commitment");
			GridRegionAccounts.TextMatrix(0, CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT, "RE Supplemental");
			GridRegionAccounts.TextMatrix(0, CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT, "PP Supplemental");
			GridRegionAccounts.TextMatrix(0, CNSTGRIDREGIONACCOUNTSCOLFUND, "Fund");
			GridRegionAccounts.ColHidden(CNSTGRIDREGIONACCOUNTSCOLAUTOID, true);
			gridBreakdown.Rows = 1;
			gridBreakdown.Cols = 3;
			gridBreakdown.TextMatrix(0, CNSTGRIDBREAKDOWNCOLCODE, "Code");
			gridBreakdown.TextMatrix(0, CNSTGRIDBREAKDOWNCOLDESCRIPTION, "Description");
			gridBreakdown.ColHidden(CNSTGRIDBREAKDOWNCOLAUTOID, true);
		}

		private void ResizeRegionGrids()
		{
			int GridWidth = 0;
			GridWidth = GridRegion.WidthOriginal;
			GridRegion.ColWidth(CNSTGRIDREGIONCOLNUMBER, FCConvert.ToInt32(0.05 * GridWidth));
			GridRegion.ColWidth(CNSTGRIDREGIONCOLABBR, FCConvert.ToInt32(0.1 * GridWidth));
			GridRegion.ColWidth(CNSTGRIDREGIONCOLRESCODE, FCConvert.ToInt32(0.15 * GridWidth));
			GridWidth = GridRegionAccounts.WidthOriginal;
			GridRegionAccounts.ColWidth(CNSTGRIDREGIONACCOUNTSCOLNUMBER, FCConvert.ToInt32(0.05 * GridWidth));
			GridRegionAccounts.ColWidth(CNSTGRIDREGIONACCOUNTSCOLFUND, FCConvert.ToInt32(0.07 * GridWidth));
			GridRegionAccounts.ColWidth(CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT, FCConvert.ToInt32(0.21 * GridWidth));
			GridRegionAccounts.ColWidth(CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT, FCConvert.ToInt32(0.21 * GridWidth));
			GridRegionAccounts.ColWidth(CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT, FCConvert.ToInt32(0.21 * GridWidth));
			GridWidth = gridBreakdown.WidthOriginal;
			gridBreakdown.ColWidth(CNSTGRIDBREAKDOWNCOLCODE, FCConvert.ToInt32(0.06 * GridWidth));
			gridBreakdown.ColWidth(CNSTGRIDBREAKDOWNCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			if (gridBreakdown.Cols >= CNSTGRIDBREAKDOWNCOLFIRSTTOWN + 1)
			{
				gridBreakdown.AutoSize(CNSTGRIDBREAKDOWNCOLFIRSTTOWN, gridBreakdown.Cols - 1);
			}
		}

		private void LoadRegionGrids()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL;
			int lngRow;
			int lngCol;
			GridRegion.Rows = 1;
			GridRegionAccounts.Rows = 1;
			strSQL = "Select * from tblRegions order by townnumber";
			clsLoad.OpenRecordset(strSQL, "CentralData");
			while (!clsLoad.EndOfFile())
			{
				GridRegion.Rows += 1;
				lngRow = GridRegion.Rows - 1;
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				GridRegion.TextMatrix(lngRow, CNSTGRIDREGIONCOLNUMBER, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("townnumber"))));
				GridRegion.TextMatrix(lngRow, CNSTGRIDREGIONCOLABBR, FCConvert.ToString(clsLoad.Get_Fields_String("townabbreviation")));
				GridRegion.TextMatrix(lngRow, CNSTGRIDREGIONCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("townname")));
				// TODO Get_Fields: Check the table for the column [rescode] and replace with corresponding Get_Field method
				GridRegion.TextMatrix(lngRow, CNSTGRIDREGIONCOLRESCODE, FCConvert.ToString(clsLoad.Get_Fields("rescode")));
				// TODO Get_Fields: Check the table for the column [rescode] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("rescode"))) == string.Empty && modGlobalConstants.Statics.gboolMV)
				{
					clsTemp.OpenRecordset("select * from residence", modGNBas.strMVDatabase);
					if (clsTemp.FindFirstRecord("Town", clsLoad.Get_Fields_String("townname")))
					{
						// TODO Get_Fields: Check the table for the column [CODE] and replace with corresponding Get_Field method
						GridRegion.TextMatrix(lngRow, CNSTGRIDREGIONCOLRESCODE, FCConvert.ToString(clsTemp.Get_Fields("CODE")));
					}
				}
				GridRegion.TextMatrix(lngRow, CNSTGRIDREGIONCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("id")));
				GridRegionAccounts.Rows += 1;
				lngRow = GridRegionAccounts.Rows - 1;
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLNUMBER, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("townnumber"))));
				// TODO Get_Fields: Check the table for the column [fund] and replace with corresponding Get_Field method
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLFUND, FCConvert.ToString(clsLoad.Get_Fields("fund")));
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT, FCConvert.ToString(clsLoad.Get_Fields_String("RECOMMITMENTACCOUNT")));
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT, FCConvert.ToString(clsLoad.Get_Fields_String("PPCommitmentaccount")));
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT, FCConvert.ToString(clsLoad.Get_Fields_String("RESupplementalAccount")));
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT, FCConvert.ToString(clsLoad.Get_Fields_String("PPSupplementalAccount")));
				GridRegionAccounts.TextMatrix(lngRow, CNSTGRIDREGIONACCOUNTSCOLAUTOID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("id"))));
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(clsLoad.Get_Fields("townnumber")) > 0)
				{
					gridBreakdown.Cols += 1;
					gridBreakdown.TextMatrix(0, gridBreakdown.Cols - 1, FCConvert.ToString(clsLoad.Get_Fields_String("townname")));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					gridBreakdown.ColData(gridBreakdown.Cols - 1, Conversion.Val(clsLoad.Get_Fields("townnumber")));
				}
				clsLoad.MoveNext();
			}
			strSQL = "select * from regionalbreakdowncodes order by code";
			clsLoad.OpenRecordset(strSQL, "CentralData");
			while (!clsLoad.EndOfFile())
			{
				gridBreakdown.Rows += 1;
				lngRow = gridBreakdown.Rows - 1;
				gridBreakdown.RowData(lngRow, false);
				gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLAUTOID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("id"))));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))));
				gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLDESCRIPTION, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				strSQL = "select * from regionaltownbreakdown WHERE code = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + " order by townnumber";
				clsTemp.OpenRecordset(strSQL, "CentralData");
				while (!clsTemp.EndOfFile())
				{
					for (lngCol = CNSTGRIDBREAKDOWNCOLFIRSTTOWN; lngCol <= gridBreakdown.Cols - 1; lngCol++)
					{
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						if (Conversion.Val(gridBreakdown.ColData(lngCol)) == Conversion.Val(clsTemp.Get_Fields("townnumber")))
						{
							gridBreakdown.TextMatrix(lngRow, lngCol, FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Double("percentage")) * 100));
							break;
						}
					}
					// lngCol
					clsTemp.MoveNext();
				}
				clsLoad.MoveNext();
			}
		}

		private void frmRegionInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						return;
					}
			}
			//end switch
			if (KeyCode == Keys.F12)
			{
				return;
			}
			if (Strings.UCase(FCGlobal.Screen.ActiveControl.Name) == "GRIDREGIONACCOUNTS")
			{
				switch (GridRegionAccounts.Col)
				{
					case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
					case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
					case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
					case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
						{
							modNewAccountBox.CheckFormKeyDown(GridRegionAccounts, GridRegionAccounts.Row, GridRegionAccounts.Col, KeyCode, Shift, GridRegionAccounts.EditSelStart, GridRegionAccounts.EditText, GridRegionAccounts.EditSelLength);
							break;
						}
				}
				//end switch
			}
		}

		private void frmRegionInfo_Resize(object sender, System.EventArgs e)
		{
			ResizeRegionGrids();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		// Private Sub gridBreakdown_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKeyInsert
		// AddBreakdownCode
		// Case vbKeyDelete
		// DeleteBreakdownCode
		// End Select
		// End Sub
		private void gridBreakdown_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = gridBreakdown.GetFlexRowIndex(e.RowIndex);
			int col = gridBreakdown.GetFlexColIndex(e.ColumnIndex);
			if (row > 0)
			{
				gridBreakdown.RowData(row, true);
			}
		}

		private void GridRegionAccounts_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			switch (e.ColumnIndex)
			{
				case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
					{
						GridRegionAccounts.EditMaxLength = 23;
						break;
					}
			}
			//end switch
		}

		private void GridRegionAccounts_Enter(object sender, System.EventArgs e)
		{
			switch (GridRegionAccounts.Col)
			{
				case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
					{
						modNewAccountBox.SetGridFormat(GridRegionAccounts, GridRegionAccounts.Row, GridRegionAccounts.Col, false, "", "R");
						break;
					}
			}
			//end switch
		}

		private void GridRegionAccounts_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.KeyDown -= GridRegionAccounts_KeyDownEdit;
				//e.Control.KeyPress -= GridRegionAccounts_KeyPressEdit;
				//e.Control.KeyUp -= GridRegionAccounts_KeyUpEdit;
				e.Control.KeyDown += GridRegionAccounts_KeyDownEdit;
				//e.Control.KeyPress += GridRegionAccounts_KeyPressEdit;
				//e.Control.KeyUp += GridRegionAccounts_KeyUpEdit;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (col == CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT || col == CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT || col == CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT || col == CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = col;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void GridRegionAccounts_KeyDownEdit(object sender, KeyEventArgs e)
		{
			// this is for the left and right arrow key
			switch (GridRegionAccounts.Col)
			{
				case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
					{
						string temp = GridRegionAccounts.EditText;
						modNewAccountBox.CheckKeyDownEditF2(GridRegionAccounts, GridRegionAccounts.Row, GridRegionAccounts.Col, e.KeyCode, e.Shift ? 1 : 0, GridRegionAccounts.EditSelStart, ref temp, GridRegionAccounts.EditSelLength);
						GridRegionAccounts.EditText = temp;
						break;
					}
			}
			//end switch
		}

		private void GridRegionAccounts_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			switch (GridRegionAccounts.Col)
			{
				case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
					{
						modNewAccountBox.CheckAccountKeyPress(GridRegionAccounts, GridRegionAccounts.Row, GridRegionAccounts.Col, FCConvert.ToInt16(e.KeyChar), GridRegionAccounts.EditSelStart, GridRegionAccounts.EditText);
						break;
					}
			}
			//end switch
		}

		private void GridRegionAccounts_KeyUpEdit(object sender, KeyEventArgs e)
		{
			switch (GridRegionAccounts.Col)
			{
				case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
					{
						if (FCConvert.ToInt32(e.KeyCode) != 40 && FCConvert.ToInt32(e.KeyCode) != 38)
						{
							// up and down arrows
							modNewAccountBox.CheckAccountKeyCode(GridRegionAccounts, GridRegionAccounts.Row, GridRegionAccounts.Col, FCConvert.ToInt16(e.KeyCode), 0, GridRegionAccounts.EditSelStart, GridRegionAccounts.EditText, GridRegionAccounts.EditSelLength);
						}
						break;
					}
			}
			//end switch
		}

		private void GridRegionAccounts_RowColChange(object sender, System.EventArgs e)
		{
			string strTemp = "";
			switch (GridRegionAccounts.Col)
			{
				case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
				case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
					{
						modNewAccountBox.SetGridFormat(GridRegionAccounts, GridRegionAccounts.Row, GridRegionAccounts.Col, true, "", "R");
						break;
					}
			}
			//end switch
		}

		private void GridRegionAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: lngFund As string	OnWriteFCConvert.ToInt32(
			string lngFund = "";
			//FC:FINAL:MSH - savve and use correct indexes of the cell
			int row = GridRegionAccounts.GetFlexRowIndex(e.RowIndex);
			int col = GridRegionAccounts.GetFlexColIndex(e.ColumnIndex);
			if (row > 0)
			{
				switch (col)
				{
					case CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT:
					case CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT:
					case CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT:
					case CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT:
						{
							e.Cancel = modNewAccountBox.CheckAccountValidate(GridRegionAccounts, row, col, e.Cancel);
							if (e.Cancel)
								boolDontSave = true;
							if (!e.Cancel && GridRegionAccounts.EditText != string.Empty)
							{
								lngFund = FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount(GridRegionAccounts.EditText, false));
								if (FCConvert.ToDouble(lngFund) != Conversion.Val(GridRegionAccounts.TextMatrix(row, CNSTGRIDREGIONACCOUNTSCOLFUND)))
								{
									MessageBox.Show("The Account doesn't match the fund for this town", "Bad Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private bool CheckPercentagesAndCodes()
		{
			bool CheckPercentagesAndCodes = false;
			int lngRow;
			int lngCol;
			double dblPerc = 0;
			int lngLastCode;
			CheckPercentagesAndCodes = false;
			lngLastCode = -1;
			gridBreakdown.Col = CNSTGRIDBREAKDOWNCOLCODE;
			gridBreakdown.Sort = FCGrid.SortSettings.flexSortNumericAscending;
			for (lngRow = 1; lngRow <= gridBreakdown.Rows - 1; lngRow++)
			{
				if (Conversion.Val(gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE)) == 0)
				{
					MessageBox.Show(FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE))) + " is not a valid breakdown code.  All breakdown codes must be > 0.", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckPercentagesAndCodes;
				}
				if (Conversion.Val(gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE)) != lngLastCode)
				{
					lngLastCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE))));
				}
				else
				{
					MessageBox.Show("You have more than one code " + FCConvert.ToString(lngLastCode) + " you can not repeat codes", "Bad Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckPercentagesAndCodes;
				}
				dblPerc = 0;
				for (lngCol = CNSTGRIDBREAKDOWNCOLFIRSTTOWN; lngCol <= gridBreakdown.Cols - 1; lngCol++)
				{
					if (Conversion.Val(gridBreakdown.TextMatrix(lngRow, lngCol)) < 0)
					{
						MessageBox.Show("You cannot have a negative percentage", "Bad Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CheckPercentagesAndCodes;
					}
					dblPerc += Conversion.Val(gridBreakdown.TextMatrix(lngRow, lngCol));
				}
				// lngCol
				if (Conversion.Val(dblPerc) != 100)
				{
					MessageBox.Show("The percentages for code " + gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE) + " do not add up to 100 percent", "Bad Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckPercentagesAndCodes;
				}
			}
			// lngRow
			CheckPercentagesAndCodes = true;
			return CheckPercentagesAndCodes;
			ErrorHandler:
			;
			MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In CheckPercentages", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			return CheckPercentagesAndCodes;
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				int lngCol;
				string strSQL = "";
				SaveData = false;
				GridRegionAccounts.Col = 0;
				for (x = 1; x <= GridRegion.Rows - 1; x++)
				{
					strSQL = "update tblregions set ";
					strSQL += "TOWNname = '" + GridRegion.TextMatrix(x, CNSTGRIDREGIONCOLNAME) + "'";
					strSQL += ",townabbreviation = '" + GridRegion.TextMatrix(x, CNSTGRIDREGIONCOLABBR) + "'";
					strSQL += ",rescode = '" + GridRegion.TextMatrix(x, CNSTGRIDREGIONCOLRESCODE) + "'";
					strSQL += " where id = " + FCConvert.ToString(Conversion.Val(GridRegion.TextMatrix(x, CNSTGRIDREGIONCOLAUTOID)));
					clsSave.Execute(strSQL, "CentralData");
				}
				// x
				if (modGlobalConstants.Statics.gboolBD)
				{
					for (x = 1; x <= GridRegionAccounts.Rows - 1; x++)
					{
						strSQL = "update tblregions set ";
						strSQL += "fund = '" + GridRegionAccounts.TextMatrix(x, CNSTGRIDREGIONACCOUNTSCOLFUND) + "'";
						strSQL += ",recommitmentaccount = '" + GridRegionAccounts.TextMatrix(x, CNSTGRIDREGIONACCOUNTSCOLRCOMMITMENT) + "'";
						strSQL += ",ppcommitmentaccount = '" + GridRegionAccounts.TextMatrix(x, CNSTGRIDREGIONACCOUNTSCOLPCOMMITMENT) + "'";
						strSQL += ",resupplementalaccount = '" + GridRegionAccounts.TextMatrix(x, CNSTGRIDREGIONACCOUNTSCOLRSUPPLEMENT) + "'";
						strSQL += ",ppsupplementalaccount = '" + GridRegionAccounts.TextMatrix(x, CNSTGRIDREGIONACCOUNTSCOLPSUPPLEMENT) + "'";
						strSQL += " where id = " + FCConvert.ToString(Conversion.Val(GridRegionAccounts.TextMatrix(x, CNSTGRIDREGIONACCOUNTSCOLAUTOID)));
						clsSave.Execute(strSQL, "CentralData");
					}
					// x
				}
				if (CheckPercentagesAndCodes())
				{
					for (x = 0; x <= gridDeletedBreakdown.Rows - 1; x++)
					{
						clsSave.Execute("delete from regionaltownbreakdown where code = " + FCConvert.ToString(Conversion.Val(gridDeletedBreakdown.TextMatrix(x, 1))), "CentralData");
						clsSave.Execute("delete from regionalbreakdowncodes where id = " + FCConvert.ToString(Conversion.Val(gridDeletedBreakdown.TextMatrix(x, 0))), "CentralData");
					}
					// x
					gridDeletedBreakdown.Rows = 0;
					for (x = 1; x <= gridBreakdown.Rows - 1; x++)
					{
						if (FCConvert.ToBoolean(gridBreakdown.RowData(x)))
						{
							clsSave.OpenRecordset("select * from regionalbreakdowncodes where id = " + FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLAUTOID))), "CentralData");
							if (!clsSave.EndOfFile())
							{
								clsSave.Edit();
							}
							else
							{
								clsSave.AddNew();
								// .TextMatrix(x, CNSTGRIDBREAKDOWNCOLAUTOID) = clsSave.Fields("id")
							}
							clsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLCODE))));
							clsSave.Set_Fields("description", Strings.Trim(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLDESCRIPTION)));
							clsSave.Update();
							gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("id")));
							for (lngCol = CNSTGRIDBREAKDOWNCOLFIRSTTOWN; lngCol <= gridBreakdown.Cols - 1; lngCol++)
							{
								clsSave.OpenRecordset("select * from regionaltownbreakdown where code = " + FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLCODE))) + " and townnumber = " + FCConvert.ToString(Conversion.Val(gridBreakdown.ColData(lngCol))), "CentralData");
								if (!clsSave.EndOfFile())
								{
									if (Conversion.Val(gridBreakdown.TextMatrix(x, lngCol)) > 0)
									{
										clsSave.Edit();
									}
									else
									{
										clsSave.Delete();
									}
								}
								else
								{
									if (Conversion.Val(gridBreakdown.TextMatrix(x, lngCol)) > 0)
									{
										clsSave.AddNew();
										clsSave.Set_Fields("townnumber", FCConvert.ToString(Conversion.Val(gridBreakdown.ColData(lngCol))));
									}
								}
								if (Conversion.Val(gridBreakdown.TextMatrix(x, lngCol)) > 0)
								{
									clsSave.Set_Fields("percentage", Conversion.Val(gridBreakdown.TextMatrix(x, lngCol)) / 100);
									clsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLCODE))));
									clsSave.Update();
								}
							}
							// lngCol
							gridBreakdown.RowData(x, false);
						}
					}
					// x
				}
				else
				{
					return SaveData;
				}
				SaveData = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuAddCode_Click(object sender, System.EventArgs e)
		{
			AddBreakdownCode();
		}

		private void AddBreakdownCode()
		{
			int lngRow = 0;
			int x;
			int lngCode = 0;
			gridBreakdown.Rows += 1;
			lngRow = gridBreakdown.Rows - 1;
			gridBreakdown.TopRow = lngRow;
			gridBreakdown.ShowCell(lngRow, CNSTGRIDBREAKDOWNCOLCODE);
			lngCode = 0;
			gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLAUTOID, FCConvert.ToString(0));
			gridBreakdown.RowData(lngRow, true);
			for (x = 1; x <= gridBreakdown.Rows - 2; x++)
			{
				if (Conversion.Val(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLCODE)) > lngCode)
				{
					lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridBreakdown.TextMatrix(x, CNSTGRIDBREAKDOWNCOLCODE))));
				}
			}
			// x
			lngCode += 1;
			gridBreakdown.TextMatrix(lngRow, CNSTGRIDBREAKDOWNCOLCODE, FCConvert.ToString(lngCode));
		}

		private void mnuDeleteCode_Click(object sender, System.EventArgs e)
		{
			if (Strings.UCase(FCGlobal.Screen.ActiveControl.Name) != "GRIDBREAKDOWN")
			{
				return;
			}
			if (gridBreakdown.Row < 1)
			{
				MessageBox.Show("You must select a code to delete first", "No Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			DeleteBreakdownCode();
		}

		private void DeleteBreakdownCode()
		{
			int lngRow;
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLAUTOID)) <= 0)
			{
				gridBreakdown.RemoveItem(gridBreakdown.Row);
			}
			else
			{
				// now check to see if the code is used in budgetary or not
				if (modGlobalConstants.Statics.gboolBD)
				{
					clsLoad.OpenRecordset("select * from deptdivtitles where breakdowncode = " + FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLCODE))), modGNBas.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						MessageBox.Show("You cannot delete code " + FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLCODE))) + " because it is still being used in Budgetary as a department / division object", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					clsLoad.OpenRecordset("select * from expobjtitles where breakdowncode = " + FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLCODE))), modGNBas.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						MessageBox.Show("You cannot delete code " + FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLCODE))) + " because it is still being used with expense objects in Budgetary", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (MessageBox.Show("Are you sure you want to delete this code?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					gridDeletedBreakdown.Rows += 1;
					lngRow = gridDeletedBreakdown.Rows - 1;
					gridDeletedBreakdown.TextMatrix(lngRow, 0, FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLAUTOID))));
					gridDeletedBreakdown.TextMatrix(lngRow, 1, FCConvert.ToString(Conversion.Val(gridBreakdown.TextMatrix(gridBreakdown.Row, CNSTGRIDBREAKDOWNCOLCODE))));
					gridBreakdown.RemoveItem(gridBreakdown.Row);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			gridBreakdown.Col = 0;
			GridRegion.Row = 0;
			GridRegionAccounts.Row = 0;
			//Application.DoEvents();
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			boolDontSave = false;
			gridBreakdown.Col = 0;
			GridRegion.Row = 0;
			GridRegionAccounts.Row = 0;
			//Application.DoEvents();
			if (!boolDontSave)
			{
				if (SaveData())
				{
					mnuExit_Click();
				}
			}
		}
	}
}
