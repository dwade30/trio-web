﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD7.
	/// </summary>
	public partial class srptBD7 : FCSectionReport
	{
		public srptBD7()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD7";
		}

		public static srptBD7 InstancePtr
		{
			get
			{
				return (srptBD7)Sys.GetInstance(typeof(srptBD7));
			}
		}

		protected srptBD7 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD7	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		string strType = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckNext:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile())
				{
					if (strType == "D")
					{
						rsInfo.OpenRecordset("SELECT Expense, SUM(Amount) as AcctTotal, COUNT(Account) as AcctCount FROM ExpenseDetailInfo GROUP BY Expense ORDER BY Expense", "TWBD0000.vb1");
						if (rsInfo.EndOfFile() != true && rsInfo.EndOfFile() != true)
						{
							blnFirstRecord = true;
							eArgs.EOF = false;
						}
						strType = "E";
						goto CheckNext;
					}
					else if (strType == "E")
					{
						rsInfo.OpenRecordset("SELECT Department, SUM(Amount) as AcctTotal, COUNT(Account) as AcctCount FROM RevenueDetailInfo GROUP BY Department ORDER BY Department", "TWBD0000.vb1");
						if (rsInfo.EndOfFile() != true && rsInfo.EndOfFile() != true)
						{
							blnFirstRecord = true;
							eArgs.EOF = false;
						}
						strType = "R";
						goto CheckNext;
					}
					else if (strType == "R")
					{
						// rsInfo.OpenRecordset "SELECT Account, SUM(Amount) as AcctTotal, COUNT(Account) as AcctCount FROM LedgerDetailInfo GROUP BY Account ORDER BY Account", "TWBD0000.vb1"
						// If rsInfo.EndOfFile <> True And rsInfo.EndOfFile <> True Then
						// blnFirstRecord = True
						// strType = "G"
						// EOF = False
						// End If
						// GoTo CheckNext
						// Else
						eArgs.EOF = true;
					}
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			if (eArgs.EOF == false)
			{
				this.Fields["Binder"].Value = strType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT Department, SUM(Amount) as AcctTotal, COUNT(Account) as AcctCount FROM ExpenseDetailInfo GROUP BY Department ORDER BY Department", "TWBD0000.vb1");
			blnFirstRecord = true;
			strType = "D";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsInfo.EndOfFile())
			{
				Detail.Visible = true;
				if (strType == "D")
				{
					// If Not ExpDivFlag Then
					// fldAccount = rsInfo.Fields("Department") & "-" & rsInfo.Fields("Division")
					// Else
					fldAccount.Text = rsInfo.Get_Fields_String("Department");
					// End If
				}
				else if (strType == "E")
				{
					// If Not ObjFlag Then
					// fldAccount = rsInfo.Fields("Expense") & "-" & rsInfo.Fields("Object")
					// Else
					fldAccount.Text = rsInfo.Get_Fields_String("Expense");
					// End If
				}
				else if (strType == "R")
				{
					fldAccount.Text = rsInfo.Get_Fields_String("Department");
					// Else
					// fldAccount = rsInfo.Fields("Account")
				}
				// TODO Get_Fields: Field [AcctCount] not found!! (maybe it is an alias?)
				fldNumberOfEntries.Text = Strings.Format(rsInfo.Get_Fields("AcctCount"), "#,##0");
				// TODO Get_Fields: Field [AcctTotal] not found!! (maybe it is an alias?)
				fldAmount.Text = Strings.Format(rsInfo.Get_Fields("AcctTotal"), "#,##0.00");
			}
			else
			{
				Detail.Visible = false;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (this.Fields["Binder"].Value == "E")
			{
				// If ObjFlag Then
				lblType.Text = "Expense";
				// Else
				// lblType = "Expense / Object"
				// End If
			}
			else if (this.Fields["Binder"].Value == "R")
			{
				lblType.Text = "Revenue Department";
			}
			else if (this.Fields["Binder"].Value == "D")
			{
				// If ExpDivFlag Then
				lblType.Text = "Expense Department";
				// Else
				// lblType = "Department / Division"
				// End If
				// ElseIf Me.Fields["Binder"].Value = "G" Then
				// lblType = "General Ledger"
			}
		}

		
	}
}
