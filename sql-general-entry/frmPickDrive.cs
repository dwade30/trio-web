﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPickDrive.
	/// </summary>
	public partial class frmPickDrive : BaseForm
	{
		public frmPickDrive()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			string strDrive;
			strDrive = Strings.Mid(Drive1.Drive, 1, 2);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "UpdateDisk", Strings.UCase(strDrive));
			this.Unload();
		}

		private void frmPickDrive_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickDrive properties;
			//frmPickDrive.ScaleWidth	= 2895;
			//frmPickDrive.ScaleHeight	= 1575;
			//frmPickDrive.LinkTopic	= "Form1";
			//End Unmaped Properties
			string dletter;
			int x;
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
				modGlobalFunctions.SetTRIOColors(this, false);
				strTemp = FCFileSystem.Statics.UserDataFolder;
				dletter = Strings.UCase(modRegistry.GetRegistryKey("UpdateDisk") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "None");
				lblDrive.Text = "Current Drive: " + dletter;
				if (dletter != "None")
				{
					Drive1.Drive = dletter;
				}
				//Environment.CurrentDirectory = strTemp;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 68)
				{
					lblDrive.Text = "Current Drive: " + Drive1.Drive;
					return;
				}
				else
				{
					MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}
	}
}
