﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptMVPrintTest.
	/// </summary>
	public partial class rptMVPrintTest : BaseSectionReport
	{
		public rptMVPrintTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport Preview Print Test";
		}

		public static rptMVPrintTest InstancePtr
		{
			get
			{
				return (rptMVPrintTest)Sys.GetInstance(typeof(rptMVPrintTest));
			}
		}

		protected rptMVPrintTest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMVPrintTest	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public string strPrinterName = "";

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			foreach (FCPrinter tempPrinter in FCGlobal.Printers)
			{
				// If Windows2000 Then
				// If tempFCGlobal.Printer.DeviceName = Mvr3Printer Then
				// rptMVPrintTest.FCGlobal.Printer.DeviceName = tempFCGlobal.Printer.DeviceName
				// Exit For
				// End If
				// Else
				if (tempPrinter.DeviceName == strPrinterName)
				{
					rptMVPrintTest.InstancePtr.Document.Printer.PrinterName = tempPrinter.DeviceName;
					break;
				}
			}
			//rptMVPrintTest.InstancePtr.Printer.RenderMode = 1;
		}

		public void SetPrinter(string strPrinter)
		{
			strPrinterName = strPrinter;
		}

		
	}
}
