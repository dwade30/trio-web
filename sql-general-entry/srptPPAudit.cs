﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptPPAudit.
	/// </summary>
	public partial class srptPPAudit : FCSectionReport
	{
		public srptPPAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptPPAudit";
		}

		public static srptPPAudit InstancePtr
		{
			get
			{
				return (srptPPAudit)Sys.GetInstance(typeof(srptPPAudit));
			}
		}

		protected srptPPAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptPPAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void Detail_Format(object sender, EventArgs e)
		{
			string strSQL = "";
			// vbPorter upgrade warning: lngTotVal As int	OnWrite(int, double)
			int lngTotVal = 0;
			// vbPorter upgrade warning: lngNumAccts As int	OnWrite(int, double)
			int lngNumAccts = 0;
			// vbPorter upgrade warning: lngExempt As int	OnWrite(int, double)
			int lngExempt = 0;
			int lngNet = 0;
			lngTotVal = 0;
			lngNumAccts = 0;
			lngExempt = 0;
			strSQL = "select count (account) as numaccts,SUM(EXEMPTION) AS totexempt,sum(CATEGOry1) as totcat1,sum(category2) as totcat2,sum(category3) as totcat3,sum(category4) as totcat4,sum(category5) as totcat5 ";
			strSQL += ",sum(category6) as totcat6,sum(category7) as totcat7,sum(category8) as totcat8,sum(category9) as totcat9,SUM(VALUE) AS TOTVALUE ";
			strSQL += " from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and orcode <> 'Y'";
			clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [totcat1] not found!! (maybe it is an alias?)
				txtCat1.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat1")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcat2] not found!! (maybe it is an alias?)
				txtCat2.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat2")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcat3] not found!! (maybe it is an alias?)
				txtCat3.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat3")), "#,###,###,##0");
				// TODO Get_Fields: Field [Totcat4] not found!! (maybe it is an alias?)
				txtCat4.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("Totcat4")), "#,###,###,##0");
				// TODO Get_Fields: Field [Totcat5] not found!! (maybe it is an alias?)
				txtCat5.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("Totcat5")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcat6] not found!! (maybe it is an alias?)
				txtCat6.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat6")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcat7] not found!! (maybe it is an alias?)
				txtCat7.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat7")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcat8] not found!! (maybe it is an alias?)
				txtCat8.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat8")), "#,###,###,##0");
				// TODO Get_Fields: Field [totcat9] not found!! (maybe it is an alias?)
				txtCat9.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("totcat9")), "#,###,###,##0");
				// TODO Get_Fields: Field [totvalue] not found!! (maybe it is an alias?)
				lngTotVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("totvalue"))));
				// TODO Get_Fields: Field [totexempt] not found!! (maybe it is an alias?)
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("totexempt"))));
				// TODO Get_Fields: Field [numaccts] not found!! (maybe it is an alias?)
				lngNumAccts = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("numaccts"))));
			}
			strSQL = "select count(account) as numaccts,sum(exemption) as totexempt,sum(overrideAMOUNT) as ovvalue,sum(value) as totvalue";
			strSQL += " from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and orcode = 'Y'";
			clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [totvalue] not found!! (maybe it is an alias?)
				lngTotVal += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("totvalue")));
				// TODO Get_Fields: Field [ovvalue] not found!! (maybe it is an alias?)
				txtOverride.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("ovvalue")), "#,###,###,##0");
				// TODO Get_Fields: Field [numaccts] not found!! (maybe it is an alias?)
				lngNumAccts += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("numaccts")));
				// TODO Get_Fields: Field [totexempt] not found!! (maybe it is an alias?)
				lngExempt += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("totexempt")));
			}
			txtTotal.Text = Strings.Format(lngTotVal, "#,###,###,##0");
			txtAccounts.Text = Strings.Format(lngNumAccts, "#,###,##0");
			txtExempt.Text = Strings.Format(lngExempt, "#,###,###,##0");
			lngNet = lngTotVal - lngExempt;
			txtNet.Text = Strings.Format(lngNet, "#,###,###,##0");
			strSQL = "select * from ratiotrends order by type";
			clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					this.Detail.Controls["lblCat" + clsLoad.Get_Fields("type")].Text = clsLoad.Get_Fields_String("description");
				}
				clsLoad.MoveNext();
			}
		}

		
	}
}
