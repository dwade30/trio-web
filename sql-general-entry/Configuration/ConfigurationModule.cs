﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.CentralData;
using TWCR0000;
using TWGNENTY.Payport;

namespace TWGNENTY.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<frmConfigurePayportSettings>().As<IModalView<IConfigurePayportSettingsViewModel>>();
		}
	}
}
