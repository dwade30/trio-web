﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for dlgContactTrio.
	/// </summary>
	partial class dlgContactTrio : BaseForm
	{
		public fecherFoundation.FCButton cmdTakeCode;
		public fecherFoundation.FCTextBox txtCode;
		public fecherFoundation.FCButton OKButton;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblEnterCode;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdTakeCode = new fecherFoundation.FCButton();
            this.txtCode = new fecherFoundation.FCTextBox();
            this.OKButton = new fecherFoundation.FCButton();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblEnterCode = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTakeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OKButton)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdTakeCode);
            this.BottomPanel.Location = new System.Drawing.Point(0, 283);
            this.BottomPanel.Size = new System.Drawing.Size(619, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtCode);
            this.ClientArea.Controls.Add(this.OKButton);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblEnterCode);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(619, 223);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(619, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(231, 30);
            this.HeaderText.Text = "Invalid System Date";
            // 
            // cmdTakeCode
            // 
            this.cmdTakeCode.AppearanceKey = "acceptButton";
            this.cmdTakeCode.Location = new System.Drawing.Point(270, 34);
            this.cmdTakeCode.Name = "cmdTakeCode";
            this.cmdTakeCode.Size = new System.Drawing.Size(79, 40);
            this.cmdTakeCode.TabIndex = 4;
            this.cmdTakeCode.Text = "OK";
            this.cmdTakeCode.Visible = false;
            this.cmdTakeCode.Click += new System.EventHandler(this.cmdTakeCode_Click);
            // 
            // txtCode
            // 
            this.txtCode.AutoSize = false;
            this.txtCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode.LinkItem = null;
            this.txtCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCode.LinkTopic = null;
            this.txtCode.Location = new System.Drawing.Point(419, 96);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(161, 40);
            this.txtCode.TabIndex = 1;
            this.txtCode.Visible = false;
            // 
            // OKButton
            // 
            this.OKButton.AppearanceKey = "actionButton";
            this.OKButton.ForeColor = System.Drawing.Color.White;
            this.OKButton.Location = new System.Drawing.Point(30, 158);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(79, 40);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 70);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(258, 15);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "PLEASE CONTACT TRIO IMMEDIATELY";
            // 
            // lblEnterCode
            // 
            this.lblEnterCode.Location = new System.Drawing.Point(30, 110);
            this.lblEnterCode.Name = "lblEnterCode";
            this.lblEnterCode.Size = new System.Drawing.Size(402, 17);
            this.lblEnterCode.TabIndex = 3;
            this.lblEnterCode.Text = "PLEASE ENTER A VALID CODE. (CALL TRIO FOR TODAY\'S CODE)";
            this.lblEnterCode.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(568, 33);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "THE CURRENT SYSTEM DATE IS OLDER THAN THE MOST RECENT RECORDED DATE TRIO WAS RUN." +
    "";
            // 
            // dlgContactTrio
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(619, 391);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dlgContactTrio";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Invalid System Date";
            this.Load += new System.EventHandler(this.dlgContactTrio_Load);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTakeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OKButton)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
