﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTellerID.
	/// </summary>
	partial class frmConfigurePayportSettings : BaseForm
	{

		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtGeoCode = new fecherFoundation.FCTextBox();
			this.cmdSave = new fecherFoundation.FCButton();
			this.lblGeoCode = new fecherFoundation.FCLabel();
			this.chkSyncRealEstate = new fecherFoundation.FCCheckBox();
			this.chkSyncPersonalProperty = new fecherFoundation.FCCheckBox();
			this.chkSyncUtilityBilling = new fecherFoundation.FCCheckBox();
			this.chkCombineUtilityBillServices = new fecherFoundation.FCCheckBox();
			this.lblCombinedName = new fecherFoundation.FCLabel();
			this.cmbCombinedServiceType = new fecherFoundation.FCComboBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSyncRealEstate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSyncPersonalProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSyncUtilityBilling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCombineUtilityBillServices)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 215);
			this.BottomPanel.Size = new System.Drawing.Size(512, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbCombinedServiceType);
			this.ClientArea.Controls.Add(this.lblCombinedName);
			this.ClientArea.Controls.Add(this.chkCombineUtilityBillServices);
			this.ClientArea.Controls.Add(this.chkSyncUtilityBilling);
			this.ClientArea.Controls.Add(this.chkSyncPersonalProperty);
			this.ClientArea.Controls.Add(this.chkSyncRealEstate);
			this.ClientArea.Controls.Add(this.lblGeoCode);
			this.ClientArea.Controls.Add(this.txtGeoCode);
			this.ClientArea.Size = new System.Drawing.Size(532, 366);
			this.ClientArea.Controls.SetChildIndex(this.txtGeoCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblGeoCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkSyncRealEstate, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkSyncPersonalProperty, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkSyncUtilityBilling, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkCombineUtilityBillServices, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCombinedName, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbCombinedServiceType, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(532, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(290, 28);
			this.HeaderText.Text = "Configure Payport Settings";
			// 
			// txtGeoCode
			// 
			this.txtGeoCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeoCode.Location = new System.Drawing.Point(171, 175);
			this.txtGeoCode.Name = "txtGeoCode";
			this.txtGeoCode.Size = new System.Drawing.Size(170, 40);
			this.txtGeoCode.TabIndex = 27;
			this.txtGeoCode.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtGeoCode.TextChanged += new System.EventHandler(this.txtGeoCode_TextChanged);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(206, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save";
			// 
			// lblGeoCode
			// 
			this.lblGeoCode.AutoSize = true;
			this.lblGeoCode.Location = new System.Drawing.Point(30, 190);
			this.lblGeoCode.Name = "lblGeoCode";
			this.lblGeoCode.Size = new System.Drawing.Size(74, 15);
			this.lblGeoCode.TabIndex = 28;
			this.lblGeoCode.Text = "GEO CODE";
			// 
			// chkSyncRealEstate
			// 
			this.chkSyncRealEstate.Location = new System.Drawing.Point(21, 21);
			this.chkSyncRealEstate.Name = "chkSyncRealEstate";
			this.chkSyncRealEstate.Size = new System.Drawing.Size(131, 22);
			this.chkSyncRealEstate.TabIndex = 1001;
			this.chkSyncRealEstate.Text = "Sync Real Estate";
			this.chkSyncRealEstate.CheckedChanged += new System.EventHandler(this.chkSyncRealEstate_CheckedChanged);
			// 
			// chkSyncPersonalProperty
			// 
			this.chkSyncPersonalProperty.Location = new System.Drawing.Point(21, 53);
			this.chkSyncPersonalProperty.Name = "chkSyncPersonalProperty";
			this.chkSyncPersonalProperty.Size = new System.Drawing.Size(168, 22);
			this.chkSyncPersonalProperty.TabIndex = 1002;
			this.chkSyncPersonalProperty.Text = "Sync Personal Property";
			this.chkSyncPersonalProperty.CheckedChanged += new System.EventHandler(this.chkSyncPersonalProperty_CheckedChanged);
			// 
			// chkSyncUtilityBilling
			// 
			this.chkSyncUtilityBilling.Location = new System.Drawing.Point(266, 21);
			this.chkSyncUtilityBilling.Name = "chkSyncUtilityBilling";
			this.chkSyncUtilityBilling.Size = new System.Drawing.Size(134, 22);
			this.chkSyncUtilityBilling.TabIndex = 1003;
			this.chkSyncUtilityBilling.Text = "Sync Utility Billing";
			this.chkSyncUtilityBilling.CheckedChanged += new System.EventHandler(this.chkSyncUtilityBilling_CheckedChanged);
			// 
			// chkCombineUtilityBillServices
			// 
			this.chkCombineUtilityBillServices.Location = new System.Drawing.Point(21, 85);
			this.chkCombineUtilityBillServices.Name = "chkCombineUtilityBillServices";
			this.chkCombineUtilityBillServices.Size = new System.Drawing.Size(211, 22);
			this.chkCombineUtilityBillServices.TabIndex = 1004;
			this.chkCombineUtilityBillServices.Text = "Combine Utility Billing Services";
			this.chkCombineUtilityBillServices.CheckedChanged += new System.EventHandler(this.chkCombineUtilityBillServices_CheckedChanged);
			// 
			// lblCombinedName
			// 
			this.lblCombinedName.AutoSize = true;
			this.lblCombinedName.Location = new System.Drawing.Point(30, 134);
			this.lblCombinedName.Name = "lblCombinedName";
			this.lblCombinedName.Size = new System.Drawing.Size(115, 15);
			this.lblCombinedName.TabIndex = 1006;
			this.lblCombinedName.Text = "COMBINED NAME";
			// 
			// cmbCombinedServiceType
			// 
			this.cmbCombinedServiceType.Location = new System.Drawing.Point(171, 121);
			this.cmbCombinedServiceType.Name = "cmbCombinedServiceType";
			this.cmbCombinedServiceType.Size = new System.Drawing.Size(170, 40);
			this.cmbCombinedServiceType.TabIndex = 26;
			this.cmbCombinedServiceType.SelectedIndexChanged += new System.EventHandler(this.cmbCombinedServiceType_SelectedIndexChanged);
			// 
			// frmConfigurePayportSettings
			// 
			this.ClientSize = new System.Drawing.Size(532, 426);
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmConfigurePayportSettings";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Configure Payport Settings";
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSyncRealEstate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSyncPersonalProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSyncUtilityBilling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCombineUtilityBillServices)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        public FCTextBox txtGeoCode;
        private FCButton cmdSave;
        private FCLabel lblGeoCode;
		private FCLabel lblCombinedName;
		private FCCheckBox chkCombineUtilityBillServices;
		private FCCheckBox chkSyncUtilityBilling;
		private FCCheckBox chkSyncPersonalProperty;
		private FCCheckBox chkSyncRealEstate;
		private FCComboBox cmbCombinedServiceType;
	}
}
