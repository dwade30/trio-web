﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CentralData;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using TWGNENTY.Payport;
using TWSharedLibrary;

namespace TWCR0000
{
    /// <summary>
    /// Summary description for frmTellerID.
    /// </summary>
    public partial class frmConfigurePayportSettings : BaseForm, IModalView<IConfigurePayportSettingsViewModel>
    {
        public frmConfigurePayportSettings()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
           
        }

        public frmConfigurePayportSettings(IConfigurePayportSettingsViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }


        private void InitializeComponentEx()
        {
            this.cmdSave.Click += CmdSave_Click;
            this.Load += FrmConfigurePayportSettings_Load;
        }

        private void FrmConfigurePayportSettings_Load(object sender, EventArgs e)
        {
            ViewModel.LoadSettings();

            LoadCombinedServiceTypes();
	        chkSyncRealEstate.Checked = ViewModel.SyncRealEstate;
	        chkSyncPersonalProperty.Checked = ViewModel.SyncPersonalProperty;
	        chkSyncUtilityBilling.Checked = ViewModel.SyncUtilityBilling;
	        chkCombineUtilityBillServices.Checked = ViewModel.CombineUtilityServices;
            if (ViewModel.CombineUtilityServices)
            {
                cmbCombinedServiceType.Text = ViewModel.CombinedUtilityType.Trim();
            }
            else
            {
                cmbCombinedServiceType.Enabled = false;
            }
	        
	        txtGeoCode.Text = ViewModel.GeoCode.Trim();
        }

        private void LoadCombinedServiceTypes()
        {
            cmbCombinedServiceType.Clear();
            foreach (var entry in ViewModel.CombinesServiceDescriptionOptions)
            {
                cmbCombinedServiceType.Items.Add(entry);
            }

        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void SaveSettings()
        {
	        if (ViewModel.SaveSettings())
	        {
		        MessageBox.Show("Settings have been updated.", "Settings Saved", MessageBoxButtons.OK,
			        MessageBoxIcon.Information);

		        Close();
            }
	        else
	        {
		        MessageBox.Show("Error trying to save settings.  Settings have NOT been updated.", "Settings NOT Saved", MessageBoxButtons.OK,
			        MessageBoxIcon.Information);
            }
        }

        public IConfigurePayportSettingsViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void chkSyncRealEstate_CheckedChanged(object sender, EventArgs e)
        {
	        ViewModel.SyncRealEstate = chkSyncRealEstate.Checked;
        }

        private void chkSyncPersonalProperty_CheckedChanged(object sender, EventArgs e)
        {
	        ViewModel.SyncPersonalProperty = chkSyncPersonalProperty.Checked;
        }

        private void chkCombineUtilityBillServices_CheckedChanged(object sender, EventArgs e)
        {
	        ViewModel.CombineUtilityServices = chkCombineUtilityBillServices.Checked;
            cmbCombinedServiceType.Enabled = chkCombineUtilityBillServices.Checked;
            if (!ViewModel.CombineUtilityServices)
            {
                cmbCombinedServiceType.Text = "";
            }
        }

        private void chkSyncUtilityBilling_CheckedChanged(object sender, EventArgs e)
        {
	        ViewModel.SyncUtilityBilling = chkSyncUtilityBilling.Checked;
        }

         private void txtGeoCode_TextChanged(object sender, EventArgs e)
        {
	        ViewModel.GeoCode = txtGeoCode.Text.Trim();
        }

        private void cmbCombinedServiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewModel.CombinedUtilityType = cmbCombinedServiceType.Text.Trim();
        }
    }

}
