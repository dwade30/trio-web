﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CentralData;
using SharedApplication.Messaging;

namespace TWGNENTY.Payport
{
	public class ConfigurePayportSettingsHandler : CommandHandler<ConfigurePayportSettings>
	{
		private IModalView<IConfigurePayportSettingsViewModel> configurePayportSettingsView;
		public ConfigurePayportSettingsHandler(IModalView<IConfigurePayportSettingsViewModel> configurePayportSettingsView)
		{
			this.configurePayportSettingsView = configurePayportSettingsView;
		}
		protected override void Handle(ConfigurePayportSettings command)
		{
			configurePayportSettingsView.ShowModal();
		}
	}
}
