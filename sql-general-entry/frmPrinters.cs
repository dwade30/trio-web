﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using System.Collections.Generic;
using Newtonsoft.Json;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPrinters.
	/// </summary>
	public partial class frmPrinters : BaseForm
	{
		public frmPrinters()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.cmdTestPage = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrinters InstancePtr
		{
			get
			{
				return (frmPrinters)Sys.GetInstance(typeof(frmPrinters));
			}
		}

		protected frmPrinters _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		char[] FormLoaded = new char[1];
		int ReceiptIndex;
		int MotorVehicleIndex;
		int MVR10Index;
		int CTAIndex;
		int UseTaxIndex;
		string Receipt = "";
        private string MOTORVEHICLE = "";
		string MVR10 = "";
		string CTA = "";
		private string UseTax = "";

		bool ok;
		const int PRTROWRECEIPT = 1;
		const int PRTROWMOTORVEHICLE = 2;
		const int PRTROWCTA = 3;
		const int PRTROWMVR10 = 5;
		private const int PRTROWUSETAX = 4;

		private void PrinterTest(string strPrinter)
		{
			try
			{
				string prnt;
				prnt = strPrinter;
				switch (modGNBas.Statics.gstrPrintType)
                {
                    case "ARMVR3":
                        // ACTIVE REPORT WITH NO PREVIEW
                        rptMVPrintTest.InstancePtr.SetPrinter(prnt);
                        rptMVPrintTest.InstancePtr.PrintReport(false);

                        break;
                    case "ARPREVIEW":
                        // ACTIVE REPORT THAT PREVIEWS THE REPORT
                        frmReportViewer.InstancePtr.Init(rptPrintTest.InstancePtr, showModal: true);

                        break;
                    case "FSO":
                    {
                        StreamWriter PrintTest;
                        PrintTest = File.CreateText(prnt);
                        PrintTest.Write("FSO object print tested successfully.");
                        PrintTest.Close();
                        using (FileStream downloadStream = new FileStream(prnt, FileMode.Open))
                        {
                            FCUtils.Download(downloadStream, prnt);
                        }

                        break;
                    }
                    case "PRINTER":
                        FCGlobal.Printer.Print("Printer object test successful.");
                        FCGlobal.Printer.EndDoc();

                        break;
                    case "OPEN":
                    {
                        FCFileSystem.FileOpen(22, prnt, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                        FCFileSystem.PrintLine(22, "");
                        FCFileSystem.PrintLine(22, prnt + " printed a test page successfully.");
                        FileStream stream = FCFileSystem.GetFileStream(22);
                        string fileName = stream.Name;
                        FCFileSystem.FileClose(22);
                        FCUtils.Download(fileName, prnt);

                        break;
                    }
                    case "VERT":
                        // ACTIVE REPORT WITH NO PREVIEW
                        rptVerticalAlignment.InstancePtr.SetPrinter(prnt);
                        rptVerticalAlignment.InstancePtr.PrintReport(false);

                        break;
                    case "PRINTFONT":
                        // activ report with printer font
                        rptPrinterFontTest.InstancePtr.Init(ref prnt);

                        break;
                    case "DIRECTPRINT":
                    {
                        clsPrinterFunctions printerFunctions = new clsPrinterFunctions();
                        printerFunctions.OpenPrinterObject(strPrinter);
                        printerFunctions.PrintText("Test");
                        //printerFunctions.CloseSession();
                        break;
                    }
                }

                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdCancelPrinterList_Click(object sender, System.EventArgs e)
		{
			framPrinterList.Visible = false;
		}

		private void cmdPrintTestPage_Click(object sender, System.EventArgs e)
		{
			int lngGRow;
			if (GridPrinter.Row < 1)
				return;
			lngGRow = GridPrinter.Row;
			frmPrintTestType.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			PrinterTest(GridPrinter.TextMatrix(lngGRow, 1));
		}

		private void frmPrinters_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
		}

		private void NewGetPrinters()
		{
            var machinePrinters = GetMachinePrinters();

            foreach (var machinePrinter in machinePrinters)
            {
                switch (machinePrinter.Item1)
                {
                    case "RCPTPrinterName":
                        Receipt = machinePrinter.Item2;
                        break;
                    case "UseTaxPrinterName":
                        UseTax = machinePrinter.Item2;
                        break;
                    case "CTAPrinterName":
                        CTA = machinePrinter.Item2;
                        break;
                    case "MVR3PrinterName":
                        MOTORVEHICLE = machinePrinter.Item2;
                        break;
                    case "MVR10PrinterName":
                        MVR10 = machinePrinter.Item2;
                        break;
                }
            }

			GridPrinter.TextMatrix(PRTROWRECEIPT, 1, Receipt);
			GridPrinter.TextMatrix(PRTROWMOTORVEHICLE, 1, MOTORVEHICLE);
			GridPrinter.TextMatrix(PRTROWCTA, 1, CTA);
			GridPrinter.TextMatrix(PRTROWUSETAX, 1, UseTax);
			GridPrinter.TextMatrix(PRTROWMVR10, 1, MVR10);
		}

		private void frmPrinters_Load(object sender, System.EventArgs e)
		{
			SetupGridPrinter();
			NewGetPrinters();
			
            this.textBox1.Text = clsPrinterFunctions.GetApplicationKey() ?? "";
		}

		private void frmPrinters_Resize(object sender, System.EventArgs e)
		{
			ResizeGridPrinter();
		}

		private void SetupGridPrinter()
		{
			GridPrinter.Rows = 6;
			GridPrinter.Cols = 3;
			GridPrinter.ColHidden(2, true);
			GridPrinter.TextMatrix(0, 1, "Printer Name");
			GridPrinter.TextMatrix(1, 0, "Receipt");
			GridPrinter.TextMatrix(2, 0, "Motor Vehicle");
			GridPrinter.TextMatrix(3, 0, "CTA");
			GridPrinter.TextMatrix(4, 0, "Use Tax");
			GridPrinter.TextMatrix(5, 0, "MVR10");
			GridPrinter.TextMatrix(1, 2, "RCPTPrinterName");
			GridPrinter.TextMatrix(2, 2, "MVR3PrinterName");
			GridPrinter.TextMatrix(3, 2, "CTAPrinterName");
			GridPrinter.TextMatrix(4, 2, "UseTaxPrinterName");
			GridPrinter.TextMatrix(5, 2, "MVR10PrinterName");

			GridPrinterList.Rows = 1;
			GridPrinterList.Cols = 1;
			GridPrinterList.ExtendLastCol = true;
			GridPrinterList.TextMatrix(0, 0, "Printer Name");
		}

		private void ResizeGridPrinter()
		{
			int GridWidth = 0;
			GridWidth = GridPrinter.WidthOriginal;
			GridPrinter.ColWidth(0, FCConvert.ToInt32(0.35 * GridWidth));
			GridPrinter.ColWidth(1, FCConvert.ToInt32(0.5 * GridWidth));
			GridWidth = GridPrinterList.WidthOriginal;
			GridPrinterList.ColWidth(0, FCConvert.ToInt32(0.6 * GridWidth));
		}

		private void FillPrinterList()
		{
            List<Printer> clientPrinters = clsPrinterFunctions.GetClientPrinters();
            GridPrinterList.Rows = 1;
            if (clientPrinters != null)
            {
                foreach (var printer in clientPrinters)
                {
                    GridPrinterList.AddItem(printer.Name);
                }
            }
        }

        private void GridPrinter_DblClick(object sender, System.EventArgs e)
		{
            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
                return;
            }
            FillPrinterList();
			if (GridPrinter.Row < 1)
				return;
			framPrinterList.Visible = true;
		}

		private void GridPrinterList_DblClick(object sender, System.EventArgs e)
		{
            int GridPrinterListRow = GridPrinterList.MouseRow;
            string strTemp = "";
			if (GridPrinterListRow < 1)
				return;
			if (GridPrinter.Row > 0)
			{
				GridPrinter.TextMatrix(GridPrinter.Row, 1, GridPrinterList.TextMatrix(GridPrinterListRow, 0));
			}
			framPrinterList.Visible = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool SavePrinters()
		{
			try
			{
                SaveMachinePrinters();
	
				//StaticSettings.GlobalSettingService.SaveSetting(SettingOwnerType.Machine, "PrintAssistantKEY", this.textBox1.Text);
                string printingKey = PrintingViaWebSocketsVariables.Statics.MachineName;
                if (printingKey != this.textBox1.Text)
                {
                    PrintingViaWebSocketsVariables.Statics.MachineName = this.textBox1.Text;
                }

				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SavePrinters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}
		}

        private void SaveMachinePrinters()
        {
            var printerJSON = new List<Tuple<string, string>>();

            for (var x = 1; x <= GridPrinter.Rows - 1; x++)
            {
                var strPrint = GridPrinter.TextMatrix(x, 1);
                var strPName = GridPrinter.TextMatrix(x, 2);
                var printerTypeName = $"{strPName}";
                StaticSettings.GlobalSettingService.SaveSetting(SettingOwnerType.Machine, printerTypeName, strPrint);
                printerJSON.Add(new Tuple<string, string>(printerTypeName, strPrint));
            }

            clsPrinterFunctions.SaveMachinePrinters(JsonConvert.SerializeObject(printerJSON));
        }

        private List<Tuple<string, string>> GetMachinePrinters()
        {
            return clsPrinterFunctions.GetMachinePrinters();
        }

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SavePrinters();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SavePrinters())
			{
				mnuExit_Click();
			}
		}
    }
}
