﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptREAudit.
	/// </summary>
	partial class srptREAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptREAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSoftAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoftValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCalcLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCalcBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalcLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalcBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.txtAccount,
				this.Label3,
				this.txtLand,
				this.Label4,
				this.txtBldg,
				this.Label5,
				this.txtExempt,
				this.Label6,
				this.txtNet,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.txtSoftAcres,
				this.txtSoftValue,
				this.txtMixedAcres,
				this.txtMixedValue,
				this.txtOtherAcres,
				this.txtOtherValue,
				this.txtTotalAcres,
				this.txtTotalValue,
				this.txtHardAcres,
				this.txtHardValue,
				this.Label14,
				this.Label15,
				this.txtCalcLand,
				this.txtCalcBldg
			});
			this.Detail.Height = 4.125F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1
			});
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Real Estate";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Accounts";
			this.Label2.Top = 0.0625F;
			this.Label2.Width = 0.75F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.875F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.0625F;
			this.txtAccount.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "Land";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 0.625F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1875F;
			this.txtLand.Left = 2.9375F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.3125F;
			this.txtLand.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: left";
			this.Label4.Text = "Building";
			this.Label4.Top = 0.5625F;
			this.Label4.Width = 0.6875F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.1875F;
			this.txtBldg.Left = 2.9375F;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0.5625F;
			this.txtBldg.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: left";
			this.Label5.Text = "Exempt";
			this.Label5.Top = 0.8125F;
			this.Label5.Width = 0.6875F;
			// 
			// txtExempt
			// 
			this.txtExempt.Height = 0.1875F;
			this.txtExempt.Left = 2.9375F;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.Style = "text-align: right";
			this.txtExempt.Text = null;
			this.txtExempt.Top = 0.8125F;
			this.txtExempt.Width = 1F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.1875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: left";
			this.Label6.Text = "Net";
			this.Label6.Top = 1.0625F;
			this.Label6.Width = 0.625F;
			// 
			// txtNet
			// 
			this.txtNet.Height = 0.1875F;
			this.txtNet.Left = 2.9375F;
			this.txtNet.Name = "txtNet";
			this.txtNet.Style = "text-align: right";
			this.txtNet.Text = null;
			this.txtNet.Top = 1.0625F;
			this.txtNet.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.8125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Soft Wood";
			this.Label7.Top = 1.375F;
			this.Label7.Width = 0.8125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.6875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Mixed Wood";
			this.Label8.Top = 1.375F;
			this.Label8.Width = 0.9375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.6875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Hard Wood";
			this.Label9.Top = 1.375F;
			this.Label9.Width = 0.875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4.625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: right";
			this.Label10.Text = "Other";
			this.Label10.Top = 1.375F;
			this.Label10.Width = 0.9375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Text = "Total";
			this.Label11.Top = 1.375F;
			this.Label11.Width = 0.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Text = "Acres";
			this.Label12.Top = 1.625F;
			this.Label12.Width = 0.8125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Text = "Value";
			this.Label13.Top = 1.875F;
			this.Label13.Width = 0.8125F;
			// 
			// txtSoftAcres
			// 
			this.txtSoftAcres.Height = 0.1875F;
			this.txtSoftAcres.Left = 1.75F;
			this.txtSoftAcres.Name = "txtSoftAcres";
			this.txtSoftAcres.Style = "text-align: right";
			this.txtSoftAcres.Text = null;
			this.txtSoftAcres.Top = 1.625F;
			this.txtSoftAcres.Width = 0.875F;
			// 
			// txtSoftValue
			// 
			this.txtSoftValue.Height = 0.1875F;
			this.txtSoftValue.Left = 1.75F;
			this.txtSoftValue.Name = "txtSoftValue";
			this.txtSoftValue.Style = "text-align: right";
			this.txtSoftValue.Text = null;
			this.txtSoftValue.Top = 1.875F;
			this.txtSoftValue.Width = 0.875F;
			// 
			// txtMixedAcres
			// 
			this.txtMixedAcres.Height = 0.1875F;
			this.txtMixedAcres.Left = 2.6875F;
			this.txtMixedAcres.Name = "txtMixedAcres";
			this.txtMixedAcres.Style = "text-align: right";
			this.txtMixedAcres.Text = null;
			this.txtMixedAcres.Top = 1.625F;
			this.txtMixedAcres.Width = 0.9375F;
			// 
			// txtMixedValue
			// 
			this.txtMixedValue.Height = 0.1875F;
			this.txtMixedValue.Left = 2.6875F;
			this.txtMixedValue.Name = "txtMixedValue";
			this.txtMixedValue.Style = "text-align: right";
			this.txtMixedValue.Text = null;
			this.txtMixedValue.Top = 1.875F;
			this.txtMixedValue.Width = 0.9375F;
			// 
			// txtOtherAcres
			// 
			this.txtOtherAcres.Height = 0.1875F;
			this.txtOtherAcres.Left = 4.625F;
			this.txtOtherAcres.Name = "txtOtherAcres";
			this.txtOtherAcres.Style = "text-align: right";
			this.txtOtherAcres.Text = null;
			this.txtOtherAcres.Top = 1.625F;
			this.txtOtherAcres.Width = 0.9375F;
			// 
			// txtOtherValue
			// 
			this.txtOtherValue.Height = 0.1875F;
			this.txtOtherValue.Left = 4.625F;
			this.txtOtherValue.Name = "txtOtherValue";
			this.txtOtherValue.Style = "text-align: right";
			this.txtOtherValue.Text = null;
			this.txtOtherValue.Top = 1.875F;
			this.txtOtherValue.Width = 0.9375F;
			// 
			// txtTotalAcres
			// 
			this.txtTotalAcres.Height = 0.1875F;
			this.txtTotalAcres.Left = 5.625F;
			this.txtTotalAcres.Name = "txtTotalAcres";
			this.txtTotalAcres.Style = "text-align: right";
			this.txtTotalAcres.Text = null;
			this.txtTotalAcres.Top = 1.625F;
			this.txtTotalAcres.Width = 0.9375F;
			// 
			// txtTotalValue
			// 
			this.txtTotalValue.Height = 0.1875F;
			this.txtTotalValue.Left = 5.625F;
			this.txtTotalValue.Name = "txtTotalValue";
			this.txtTotalValue.Style = "text-align: right";
			this.txtTotalValue.Text = null;
			this.txtTotalValue.Top = 1.875F;
			this.txtTotalValue.Width = 0.9375F;
			// 
			// txtHardAcres
			// 
			this.txtHardAcres.Height = 0.1875F;
			this.txtHardAcres.Left = 3.6875F;
			this.txtHardAcres.Name = "txtHardAcres";
			this.txtHardAcres.Style = "text-align: right";
			this.txtHardAcres.Text = null;
			this.txtHardAcres.Top = 1.625F;
			this.txtHardAcres.Width = 0.875F;
			// 
			// txtHardValue
			// 
			this.txtHardValue.Height = 0.1875F;
			this.txtHardValue.Left = 3.6875F;
			this.txtHardValue.Name = "txtHardValue";
			this.txtHardValue.Style = "text-align: right";
			this.txtHardValue.Text = null;
			this.txtHardValue.Top = 1.875F;
			this.txtHardValue.Width = 0.875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Text = "Billing";
			this.Label14.Top = 0.0625F;
			this.Label14.Width = 0.9375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Text = "Calculated";
			this.Label15.Top = 0.0625F;
			this.Label15.Width = 0.9375F;
			// 
			// txtCalcLand
			// 
			this.txtCalcLand.Height = 0.1875F;
			this.txtCalcLand.Left = 4F;
			this.txtCalcLand.Name = "txtCalcLand";
			this.txtCalcLand.Style = "text-align: right";
			this.txtCalcLand.Text = null;
			this.txtCalcLand.Top = 0.3125F;
			this.txtCalcLand.Width = 1F;
			// 
			// txtCalcBldg
			// 
			this.txtCalcBldg.Height = 0.1875F;
			this.txtCalcBldg.Left = 4F;
			this.txtCalcBldg.Name = "txtCalcBldg";
			this.txtCalcBldg.Style = "text-align: right";
			this.txtCalcBldg.Text = null;
			this.txtCalcBldg.Top = 0.5625F;
			this.txtCalcBldg.Width = 1F;
			// 
			// srptREAudit
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalcLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalcBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalcLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalcBldg;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
