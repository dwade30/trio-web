﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptMV1.
	/// </summary>
	partial class srptMV1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMV1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldActive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPending = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTerminated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpired = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldActiveTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPendingTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTerminatedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpiredTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPending)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTerminated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActiveTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPendingTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTerminatedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpiredTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClass,
				this.fldActive,
				this.fldPending,
				this.fldTerminated,
				this.fldExpired
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7
			});
			this.GroupHeader1.Height = 0.5208333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldActiveTotal,
				this.fldPendingTotal,
				this.fldTerminatedTotal,
				this.fldExpiredTotal,
				this.Line2
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Registration Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 5.53125F;
			this.Line1.X1 = 1F;
			this.Line1.X2 = 6.53125F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.09375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: left";
			this.Label2.Text = "Class";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 2.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.21875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "Active ";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 0.78125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.03125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Pending";
			this.Label5.Top = 0.3125F;
			this.Label5.Width = 0.78125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.84375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Terminated";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.78125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.71875F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: right";
			this.Label7.Text = "Expired";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 0.78125F;
			// 
			// fldClass
			// 
			this.fldClass.Height = 0.1666667F;
			this.fldClass.Left = 1F;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-size: 9pt; text-align: left";
			this.fldClass.Text = null;
			this.fldClass.Top = 0F;
			this.fldClass.Width = 2.083333F;
			// 
			// fldActive
			// 
			this.fldActive.Height = 0.1666667F;
			this.fldActive.Left = 3.15625F;
			this.fldActive.Name = "fldActive";
			this.fldActive.Style = "font-size: 9pt; text-align: right";
			this.fldActive.Text = null;
			this.fldActive.Top = 0F;
			this.fldActive.Width = 0.7604167F;
			// 
			// fldPending
			// 
			this.fldPending.Height = 0.1666667F;
			this.fldPending.Left = 4F;
			this.fldPending.Name = "fldPending";
			this.fldPending.Style = "font-size: 9pt; text-align: right";
			this.fldPending.Text = null;
			this.fldPending.Top = 0F;
			this.fldPending.Width = 0.75F;
			// 
			// fldTerminated
			// 
			this.fldTerminated.Height = 0.1666667F;
			this.fldTerminated.Left = 4.833333F;
			this.fldTerminated.Name = "fldTerminated";
			this.fldTerminated.Style = "font-size: 9pt; text-align: right";
			this.fldTerminated.Text = null;
			this.fldTerminated.Top = 0F;
			this.fldTerminated.Width = 0.8333333F;
			// 
			// fldExpired
			// 
			this.fldExpired.Height = 0.1666667F;
			this.fldExpired.Left = 5.71875F;
			this.fldExpired.Name = "fldExpired";
			this.fldExpired.Style = "font-size: 9pt; text-align: right";
			this.fldExpired.Text = null;
			this.fldExpired.Top = 0F;
			this.fldExpired.Width = 0.78125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt; font-weight: bold; text-align: left";
			this.Field1.Text = "Total:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 2.09375F;
			// 
			// fldActiveTotal
			// 
			this.fldActiveTotal.Height = 0.1875F;
			this.fldActiveTotal.Left = 3.15625F;
			this.fldActiveTotal.Name = "fldActiveTotal";
			this.fldActiveTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldActiveTotal.Text = "Field1";
			this.fldActiveTotal.Top = 0.03125F;
			this.fldActiveTotal.Width = 0.78125F;
			// 
			// fldPendingTotal
			// 
			this.fldPendingTotal.Height = 0.1875F;
			this.fldPendingTotal.Left = 4F;
			this.fldPendingTotal.Name = "fldPendingTotal";
			this.fldPendingTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldPendingTotal.Text = "Field1";
			this.fldPendingTotal.Top = 0.03125F;
			this.fldPendingTotal.Width = 0.78125F;
			// 
			// fldTerminatedTotal
			// 
			this.fldTerminatedTotal.Height = 0.1875F;
			this.fldTerminatedTotal.Left = 4.84375F;
			this.fldTerminatedTotal.Name = "fldTerminatedTotal";
			this.fldTerminatedTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTerminatedTotal.Text = "Field1";
			this.fldTerminatedTotal.Top = 0.03125F;
			this.fldTerminatedTotal.Width = 0.78125F;
			// 
			// fldExpiredTotal
			// 
			this.fldExpiredTotal.Height = 0.1875F;
			this.fldExpiredTotal.Left = 5.71875F;
			this.fldExpiredTotal.Name = "fldExpiredTotal";
			this.fldExpiredTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldExpiredTotal.Text = "Field1";
			this.fldExpiredTotal.Top = 0.03125F;
			this.fldExpiredTotal.Width = 0.78125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.53125F;
			this.Line2.X1 = 1F;
			this.Line2.X2 = 6.53125F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// srptMV1
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPending)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTerminated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActiveTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPendingTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTerminatedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpiredTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldActive;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPending;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTerminated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpired;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldActiveTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPendingTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTerminatedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpiredTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
