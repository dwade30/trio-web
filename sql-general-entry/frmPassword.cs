﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPassword.
	/// </summary>
	public partial class frmPassword : BaseForm
	{
		public frmPassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPassword InstancePtr
		{
			get
			{
				return (frmPassword)Sys.GetInstance(typeof(frmPassword));
			}
		}

		protected frmPassword _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.gstrPassword = string.Empty;
			modGlobalConstants.Statics.gstrUserID = string.Empty;
			modGNBas.Statics.boolPasswordGood = false;
			this.Hide();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.gstrPassword = txtPassword.Text;
			modGlobalConstants.Statics.gstrUserID = txtUserId.Text;
			modGNBas.Statics.boolPasswordGood = true;
			this.Hide();
		}

		private void frmPassword_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPassword.InstancePtr.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPassword_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this, false);
			modGNBas.Statics.gstrPassword = string.Empty;
			modGlobalConstants.Statics.gstrUserID = string.Empty;
			modGNBas.Statics.boolPasswordGood = false;
		}
	}
}
