﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRestore.
	/// </summary>
	public partial class frmRestore : BaseForm
	{
		public frmRestore()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRestore InstancePtr
		{
			get
			{
				return (frmRestore)Sys.GetInstance(typeof(frmRestore));
			}
		}

		protected frmRestore _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intCounter;
		clsDRWrapper rsArchive = new clsDRWrapper();
		const int ColIdentifier = 0;
		const int ColFileName = 3;
		const int ColDataGroup = 2;
		const int ColDBType = 1;
		private string strBackupDirectory = string.Empty;
		private cBackupRestore backRest = new cBackupRestore();
		private string strBackupPath = "";
		private FCCollection collBackups = new FCCollection();
		private int currentBackupIdentifier;

		private void cmdRestore_Click(object sender, System.EventArgs e)
		{
			RestoreDB();
		}

		private void RestoreDB()
		{
			//frmBusy bWait = null;
			FCUtils.StartTask(this, () =>
			{
				this.ShowWait();
				try
				{
					// On Error GoTo ErrorHandler
					if (currentBackupIdentifier > 0)
					{
						if (cboRestorePoints.SelectedIndex >= 0)
						{
							int lngFileNum = 0;
							lngFileNum = cboRestorePoints.ItemData(cboRestorePoints.SelectedIndex);
							if (lngFileNum >= 0)
							{
								foreach (cGenericSortableItem genItem in collBackups)
								{
									if (genItem.Identifier == currentBackupIdentifier)
									{
										cDBBackup backFile;
										backFile = (cDBBackup)genItem.Item;
										//bWait = new frmBusy();
										//bWait.Message = "Restoring " + backFile.FileName;
										//bWait.StartBusy();
										this.UpdateWait("Restoring " + backFile.FileName);
										//bWait.Show(FormShowEnum.Modeless);
										//backRest.RestoreAs(backFile.FilePath + backFile.FileName, backFile.FullDBName, lngFileNum);
										//bWait.StopBusy();
										//bWait.Unload();
										/*- bWait = null; */
										if (backRest.HadError)
										{
											MessageBox.Show("Error " + FCConvert.ToString(backRest.LastErrorNumber) + " " + backRest.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										}
										else
										{
											MessageBox.Show(backFile.FullDBName + " restored");
										}
										break;
									}
								}
							}
							else
							{
								MessageBox.Show("You must pick a restore point", "No restore point selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else
						{
							MessageBox.Show("You must pick a restore point", "No restore point selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						MessageBox.Show("You must pick a database to restore", "No backup selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					this.EndWait();
					return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					//if (!(bWait == null))
					//{
					//	bWait.StopBusy();
					//	bWait.Unload();
					//	/*- bWait = null; */
					//}
					this.EndWait();
					MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			});
		}

		private void frmRestore_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRestore properties;
			//frmRestore.FillStyle	= 0;
			//frmRestore.ScaleWidth	= 9045;
			//frmRestore.ScaleHeight	= 6930;
			//frmRestore.LinkTopic	= "Form2";
			//frmRestore.LockControls	= true;
			//frmRestore.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SetTRIOColors Me
			strBackupDirectory = StaticSettings.gGlobalSettings.BackupPath;
			if (Strings.Right(" " + strBackupDirectory, 1) != "/" && Strings.Right(" " + strBackupDirectory, 1) != "\\")
			{
				strBackupDirectory += "\\";
			}
			SetupGridBackups();
			BuildList();
			FillGridBackups();
			// Set temp = New BACKUPRESTORE
		}

		private void frmRestore_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
				// ElseIf KeyAscii = 13 Then
				// KeyAscii = 0
				// SendKeys "{TAB}"
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmRestore_Resize(object sender, System.EventArgs e)
		{
			ResizeBackupGrid();
		}

		private void gridBackups_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow;
			string strFileName = "";
			// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
			int lngTemp = 0;
			lngRow = gridBackups.Row;
			if (lngRow > 0)
			{
				lngTemp = FCConvert.ToInt32(gridBackups.TextMatrix(lngRow, ColIdentifier));
				if (lngTemp != currentBackupIdentifier)
				{
					currentBackupIdentifier = lngTemp;
					ShowRestorePoints();
				}
			}
			else
			{
				if (currentBackupIdentifier > 0)
				{
					currentBackupIdentifier = 0;
					ShowRestorePoints();
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowRestorePoints()
		{
			cDBBackup currentBackup = null;
			if (currentBackupIdentifier > 0)
			{
				foreach (cGenericSortableItem currBackup in collBackups)
				{
					if (currBackup.Identifier == currentBackupIdentifier)
					{
						currentBackup = (cDBBackup)currBackup.Item;
						break;
					}
				}
			}
			if (!(currentBackup == null))
			{
				lblRestorePoints.Visible = true;
				cboRestorePoints.Visible = true;
				cboRestorePoints.Clear();
				if (currentBackup.RestorePoints.Count != 1)
				{
					cboRestorePoints.AddItem("Select a restore point");
					cboRestorePoints.ItemData(cboRestorePoints.NewIndex, -1);
				}
				foreach (cRestoreFileDetails rfd in currentBackup.RestorePoints)
				{
					cboRestorePoints.AddItem(Strings.Format(rfd.BackupDate, "MM/dd/yyyy hh:mm tt"));
					cboRestorePoints.ItemData(cboRestorePoints.NewIndex, rfd.FileNumber);
				}
				cboRestorePoints.SelectedIndex = 0;
			}
			else
			{
				lblRestorePoints.Visible = false;
				cboRestorePoints.Visible = false;
			}
		}

		private void BuildList()
		{
			FCCollection fileColl = new FCCollection();
			cGenericCollection restPoints;
			cRestoreFileDetails restDet;
			cDBBackup backFile;
			cGenericSortableItem genComp;
			int lngTemp;
			foreach (cDBBackup tRec in collBackups)
			{
				collBackups.Remove(1);
			}
			// tRec
			fileColl = GetListOfBackups(strBackupDirectory);
			lngTemp = 0;
			foreach (object strFile in fileColl)
			{
				restPoints = backRest.GetBackupHeadersFromFile(strBackupDirectory + strFile);
				if (restPoints.ItemCount() > 0)
				{
					restPoints.MoveFirst();
					restDet = (cRestoreFileDetails)restPoints.GetCurrentItem();
					backFile = CreateBackupObjectFromDBName(restDet.DatabaseName);
					if (!(backFile == null))
					{
						backFile.FilePath = strBackupDirectory;
						backFile.FileName = FCConvert.ToString(strFile);
						while (restPoints.IsCurrent())
						{
							backFile.RestorePoints.Add(restPoints.GetCurrentItem());
							restPoints.MoveNext();
						}
						genComp = new cGenericSortableItem();
						genComp.Item = backFile;
						lngTemp += 1;
						genComp.Identifier = lngTemp;
						genComp.SortValues.Add(backFile.DatabaseType);
						genComp.SortValues.Add(backFile.DataGroup);
						genComp.SortValues.Add(backFile.FileName);
						collBackups.Add(genComp);
					}
				}
			}
		}

		private cDBBackup CreateBackupObjectFromDBName(string strName)
		{
			cDBBackup CreateBackupObjectFromDBName = null;
			cDBBackup retObj = null;
			string[] strAry = null;
			string strGroup = "";
			string strDB = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (strName != "")
				{
					if (Strings.LCase(Strings.Left(strName + "    ", 4)) == "trio")
					{
						strAry = Strings.Split(strName, "_", -1, CompareConstants.vbBinaryCompare);
						strGroup = Strings.LCase(Strings.Trim(strAry[2]));
						if (strGroup == "live")
						{
							strDB = strAry[3];
							retObj = new cDBBackup();
							retObj.DataGroup = strAry[1];
							retObj.DatabaseType = strDB;
							retObj.FullDBName = strName;
							retObj.GroupType = strAry[2];
						}
					}
				}
				CreateBackupObjectFromDBName = retObj;
				return CreateBackupObjectFromDBName;
			}
			catch
			{
				// ErrorHandler:
			}
			return CreateBackupObjectFromDBName;
		}

		private FCCollection GetListOfBackups(string strBackupDir)
		{
			FCCollection GetListOfBackups = null;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strSQL;
			FCCollection retColl = new FCCollection();
			strSQL = "IF OBJECT_ID('tempdb..#DirectoryTree') IS NOT NULL  DROP TABLE #DirectoryTree; CREATE TABLE #DirectoryTree (id int IDENTITY(1,1),subdirectory nvarchar(512),depth int,isfile bit); INSERT #DirectoryTree (subdirectory,depth,isfile) EXEC Master.sys.xp_dirtree '" + strBackupDir + "',1,1; SELECT * FROM #DirectoryTree WHERE isfile = 1 AND RIGHT(subdirectory,4) = '.BAK' ORDER BY id;";
			rsTemp.OpenRecordset(strSQL, "SystemSettings");
			while (!rsTemp.EndOfFile())
			{
				// TODO Get_Fields: Field [subdirectory] not found!! (maybe it is an alias?)
				retColl.Add(rsTemp.Get_Fields("subdirectory"));
				rsTemp.MoveNext();
			}
			GetListOfBackups = retColl;
			return GetListOfBackups;
		}

		private void ResizeBackupGrid()
		{
			int lngGridWidth;
			lngGridWidth = gridBackups.WidthOriginal;
			gridBackups.ColWidth(ColDBType, FCConvert.ToInt32(0.2 * lngGridWidth));
			gridBackups.ColWidth(ColDataGroup, FCConvert.ToInt32(0.2 * lngGridWidth));
		}

		private void SetupGridBackups()
		{
			gridBackups.ColHidden(ColIdentifier, true);
			gridBackups.TextMatrix(0, ColDBType, "Database");
			gridBackups.TextMatrix(0, ColFileName, "File");
			gridBackups.TextMatrix(0, ColDataGroup, "Data Group");
		}

		private void FillGridBackups()
		{
			int lngRow;
			cGenericComparator genComp = new cGenericComparator();
			cCollectionHelper colHelp = new cCollectionHelper();
			cDBBackup backFile;
			gridBackups.Rows = 1;
			collBackups = colHelp.Sort(collBackups, genComp);
			foreach (cGenericSortableItem genSort in collBackups)
			{
				backFile = (cDBBackup)genSort.Item;
				gridBackups.Rows += 1;
				lngRow = gridBackups.Rows - 1;
				gridBackups.TextMatrix(lngRow, ColDataGroup, backFile.DataGroup);
				gridBackups.TextMatrix(lngRow, ColFileName, backFile.FileName);
				gridBackups.TextMatrix(lngRow, ColDBType, backFile.DatabaseType);
				gridBackups.TextMatrix(lngRow, ColIdentifier, FCConvert.ToString(genSort.Identifier));
			}
		}
	}
}
