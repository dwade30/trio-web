﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;
using System.Windows.Forms;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Keys = Wisej.Web.Keys;
using MessageBox = Wisej.Web.MessageBox;
using MessageBoxButtons = Wisej.Web.MessageBoxButtons;
using MessageBoxIcon = Wisej.Web.MessageBoxIcon;

namespace TWGNENTY
{
    /// <summary>
    /// Summary description for frmBackup.
    /// </summary>
    public partial class frmBackup : BaseForm
    {
        private int SelectCol = 0;
        private int DescriptionCol = 1;
        private int DBNameCol = 2;
        private int DBTypeCol = 3;
        private int DBGroupCol = 4;

        public frmBackup()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();

            // create backups folder on website if it doesn't exist
            var backupPath = modGlobalRoutines.GetBackupPath();
            if (!Directory.Exists(backupPath)) Directory.CreateDirectory(backupPath);


        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmBackup InstancePtr
        {
            get
            {
                return (frmBackup)Sys.GetInstance(typeof(frmBackup));
            }
        }

        protected frmBackup _InstancePtr = null;
 
        private void cmdBackup_Click(object sender, System.EventArgs e)
        {
            BackupDBs();
        }

        private void BackupDBs()
        {
            int intCounter;
            List<DatabaseMetaData> databaseInfo = new List<DatabaseMetaData>();
            for (intCounter = 0; intCounter < grdModules.Rows; intCounter++)
            {
                if (grdModules.TextMatrix(intCounter, SelectCol).ToBoolean())
                {
                    databaseInfo.Add(new DatabaseMetaData
                    {
                        DatabaseType = grdModules.TextMatrix(intCounter, DBTypeCol),
                        Description = grdModules.TextMatrix(intCounter, DescriptionCol),
                        FullDBName = grdModules.TextMatrix(intCounter, DBNameCol),
                        Group = grdModules.TextMatrix(intCounter, DBGroupCol)
                    });
                }
            }

            if (databaseInfo.Count > 0)
            {
                MakeBackups(databaseInfo);
            }
            else
            {
                MessageBox.Show("You didn't select any databases to back up", "No databases selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void MakeBackups(List<DatabaseMetaData> databaseInfo)
        {
            FCUtils.StartTask(this, () =>
            {
                this.ShowWait();
                string strSavePath;
                string strDBName = "";
                string strDBType = "";
                cBackupRestore backRest = new cBackupRestore();
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strFileName = "";

                try
                {
                    cmdBackup.Enabled = false;
                    this.UpdateWait("Making backup(s)");

                    strSavePath = modGlobalRoutines.GetBackupPath();

                    foreach (DatabaseMetaData dbInfo in databaseInfo)
                    {
                        this.UpdateWait("Backing up " + dbInfo.Description);

                        strDBName = dbInfo.FullDBName;
                        strDBType = dbInfo.DatabaseType;
                        strFileName = rsLoad.MegaGroup + "_" + dbInfo.Group + "_" + strDBType + "_" +
                                      DateTime.Now.ToString("MM-dd-yyyy hhmmss") + ".bak";

                        backRest.MakeBackup(strDBName, strSavePath + strFileName, false);

                        if (backRest.HadError)
                        {
                            Information.Err().Raise(backRest.LastErrorNumber, null, backRest.LastErrorMessage, null, null);
                        }
                        else
                        {
                            LoadBackupGrid();
                        }
                    }

                    this.EndWait();

                    MessageBox.Show("Database(s) backed up", "Backups Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmdBackup.Enabled = true;
                }
                catch (Exception ex)
                {
                    cmdBackup.Enabled = true;

                    this.EndWait();
                    MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeBackups", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                finally
                {
                    rsLoad.DisposeOf();
                }
            });
        }

        private void cmdSelectAll_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < grdModules.Rows; i++)
            {
                grdModules.TextMatrix(i, SelectCol, true);
            }
        }

        private void cmdUnselect_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < grdModules.Rows; i++)
            {
                grdModules.TextMatrix(i, SelectCol, false);
            }
        }

        private void frmBackup_Activated(object sender, System.EventArgs e)
        {
            if (modGlobalRoutines.FormExist(this))
            {
                return;
            }
            ResizeGrid();
            this.Refresh();
        }

        private void frmBackup_Load(object sender, System.EventArgs e)
        {
 
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            SetupGrid();
            FillModuleGrid();
            LoadBackupGrid();
        }

        private void FillModuleGrid()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strDBName;
            string strFullName;

            strDBName = "SystemSettings";
            strFullName = rsLoad.Get_GetFullDBName(strDBName);
            AddModule("System Settings", strFullName, "Live", strDBName);

            if (modEntryMenuForm.Statics.AccountsReceivableAllowed)
            {
                strDBName = "AccountsReceivable";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Accounts Receivable", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.BillingAllowed)
            {
                strDBName = "Billing";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Billing", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.BudgetaryAllowed)
            {
                strDBName = "Budgetary";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Budgetary", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.CashReceiptAllowed)
            {
                strDBName = "CashReceipts";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Cash Receipts", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.ClerkAllowed)
            {
                strDBName = "Clerk";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Clerk", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.CodeAllowed)
            {
                strDBName = "CodeEnforcement";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Code Enforcement", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.CollectionsAllowed || modEntryMenuForm.Statics.BillingAllowed)
            {
                strDBName = "Collections";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Collections", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.FixedAssetAllowed)
            {
                strDBName = "FixedAssets";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Fixed Assets", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.MotorVehicleAllowed)
            {
                strDBName = "MotorVehicle";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Motor Vehicle", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.PayrollAllowed)
            {
                strDBName = "Payroll";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Payroll", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.PersonalPropertyAllowed || modEntryMenuForm.Statics.BillingAllowed)
            {
                strDBName = "PersonalProperty";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Personal Property", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.RealEstateAllowed || modEntryMenuForm.Statics.BillingAllowed)
            {
                strDBName = "RealEstate";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Real Estate", strFullName, "Live", strDBName);
            }

            if (modEntryMenuForm.Statics.UtilitiesAllowed)
            {
                strDBName = "UtilityBilling";
                strFullName = rsLoad.Get_GetFullDBName(strDBName);
                AddModule("Utility Billing", strFullName, "Live", strDBName);
            }

            AddArchives();
        }

        private void AddArchives()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strFullDBName;
            string strGroupName;
            string strDBName;

            rsLoad.OpenRecordset("Select * from archives order by archivetype,archiveid", "SystemSettings");

            while (!rsLoad.EndOfFile())
            {
                strGroupName = rsLoad.Get_Fields_String("archivetype") + "_" + rsLoad.Get_Fields_String("archiveid");
                if (rsLoad.Get_Fields_String("archivetype").ToLower() == "archive" && modEntryMenuForm.Statics.BudgetaryAllowed)
                {
                    strDBName = "Budgetary";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Budgetary Archive", strFullDBName, strGroupName, strDBName);

                    strDBName = "CentralData";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Budgetary Archive Central Data", strFullDBName,
                        strGroupName, strDBName);

                    strDBName = "CentralDocuments";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Budgetary Archive Central Documents",
                        strFullDBName, strGroupName, strDBName);
                }
                else if (rsLoad.Get_Fields_String("archivetype").ToLower() == "commitmentarchive" &&
                         (modEntryMenuForm.Statics.RealEstateAllowed || modEntryMenuForm.Statics.BillingAllowed ||
                          modEntryMenuForm.Statics.PersonalPropertyAllowed))
                {
                    strDBName = "RealEstate";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Real Estate Tax Archive", strFullDBName,
                        strGroupName, strDBName);


                    strDBName = "PersonalProperty";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Personal Property Tax Archive", strFullDBName,
                        strGroupName, strDBName);


                    strDBName = "CentralData";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Central Data Tax Archive", strFullDBName,
                        strGroupName, strDBName);


                    strDBName = "CentralDocuments";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Central Documents Tax Archive", strFullDBName,
                        strGroupName, strDBName);


                    strDBName = "CentralParties";
                    strFullDBName = rsLoad.Get_GetFullDBName(strDBName);
                    strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddModule(rsLoad.Get_Fields_String("archiveid") + " Central Parties Tax Archive", strFullDBName,
                        strGroupName, strDBName);
                }

                rsLoad.MoveNext();
            }
        }

        private void LoadBackupGrid()
        {
            var cls = new Global.cBackupRestore();
            bindingSource1.Clear();
            bindingSource1.DataSource = cls.GetBackupListing(modGlobalRoutines.GetBackupPath());
            grdBackups.Refresh();

        }

        private void SetupGrid()
        {
            grdModules.Cols = 5;
            grdModules.ColHidden(DBNameCol, true);
            grdModules.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
            grdModules.ColHidden(DBTypeCol, true);
            grdModules.ColHidden(DBGroupCol, true);
            grdModules.ColAlignment(SelectCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
        }

        private void ResizeGrid()
        {
            int gridWidth = grdModules.WidthOriginal;
            grdModules.ColWidth(SelectCol, (0.1 * gridWidth).ToInteger());
        }

        private void AddModule(string description, string fullName, string group, string dbType)
        {
            int row;

            grdModules.Rows = grdModules.Rows + 1;
            row = grdModules.Rows - 1;

            grdModules.TextMatrix(row, SelectCol, false);
            grdModules.TextMatrix(row, DescriptionCol, description);
            grdModules.TextMatrix(row, DBNameCol, fullName);
            grdModules.TextMatrix(row, DBGroupCol, group);
            grdModules.TextMatrix(row, DBTypeCol, dbType);
        }
        
        private class DatabaseMetaData
        {
            public string DatabaseType { get; set; }
            public string FullDBName { get; set; }
            public string Group { get; set; }
            public string Description { get; set; }
        }

        private void grdModules_Click(object sender, EventArgs e)
        {
            if (grdModules.MouseRow >= 0)
            {
                grdModules.TextMatrix(grdModules.MouseRow, SelectCol,
                    !grdModules.TextMatrix(grdModules.MouseRow, SelectCol).ToBoolean());
            }
        }
    }
}
