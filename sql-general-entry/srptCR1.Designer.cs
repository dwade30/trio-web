﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptCR1.
	/// </summary>
	partial class srptCR1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCR1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNumberOfTransactions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNumberOfTransactionsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfTransactions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfTransactionsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldType,
				this.fldNumberOfTransactions,
				this.fldAmount
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.lblDateRange
			});
			this.GroupHeader1.Height = 0.6666667F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldNumberOfTransactionsTotal,
				this.fldAmountTotal,
				this.Line2
			});
			this.GroupFooter1.Height = 0.5F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Receipt Type Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.75F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.65625F;
			this.Line1.Width = 4.03125F;
			this.Line1.X1 = 1.75F;
			this.Line1.X2 = 5.78125F;
			this.Line1.Y1 = 0.65625F;
			this.Line1.Y2 = 0.65625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.75F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: left";
			this.Label2.Text = "Receipt Type";
			this.Label2.Top = 0.46875F;
			this.Label2.Width = 1.59375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "# of Trans";
			this.Label4.Top = 0.46875F;
			this.Label4.Width = 0.71875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.65625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Amount";
			this.Label5.Top = 0.46875F;
			this.Label5.Width = 1.125F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 2.5625F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "text-align: center";
			this.lblDateRange.Text = "Receipt Type";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 2.5F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 1.75F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-size: 9pt; text-align: left";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 2.0625F;
			// 
			// fldNumberOfTransactions
			// 
			this.fldNumberOfTransactions.Height = 0.1875F;
			this.fldNumberOfTransactions.Left = 3.875F;
			this.fldNumberOfTransactions.Name = "fldNumberOfTransactions";
			this.fldNumberOfTransactions.Style = "font-size: 9pt; text-align: right";
			this.fldNumberOfTransactions.Text = null;
			this.fldNumberOfTransactions.Top = 0F;
			this.fldNumberOfTransactions.Width = 0.71875F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 4.65625F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-size: 9pt; text-align: right";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.75F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt; font-weight: bold; text-align: left";
			this.Field1.Text = "Total:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 2.0625F;
			// 
			// fldNumberOfTransactionsTotal
			// 
			this.fldNumberOfTransactionsTotal.Height = 0.1875F;
			this.fldNumberOfTransactionsTotal.Left = 3.875F;
			this.fldNumberOfTransactionsTotal.Name = "fldNumberOfTransactionsTotal";
			this.fldNumberOfTransactionsTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldNumberOfTransactionsTotal.Text = "Field1";
			this.fldNumberOfTransactionsTotal.Top = 0.03125F;
			this.fldNumberOfTransactionsTotal.Width = 0.71875F;
			// 
			// fldAmountTotal
			// 
			this.fldAmountTotal.Height = 0.1875F;
			this.fldAmountTotal.Left = 4.65625F;
			this.fldAmountTotal.Name = "fldAmountTotal";
			this.fldAmountTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldAmountTotal.Text = "Field1";
			this.fldAmountTotal.Top = 0.03125F;
			this.fldAmountTotal.Width = 1.125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 4.03125F;
			this.Line2.X1 = 1.75F;
			this.Line2.X2 = 5.78125F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// srptCR1
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfTransactions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfTransactionsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumberOfTransactions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumberOfTransactionsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
