//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptRegistryData.
	/// </summary>
	public partial class rptRegistryData : GrapeCity.ActiveReports.SectionReport
	{

		public rptRegistryData()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "rptRegistryData";
		}

		public static rptRegistryData InstancePtr
		{
			get
			{
				return (rptRegistryData)Sys.GetInstance(typeof(rptRegistryData));
			}
		}

		protected rptRegistryData _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

// nObj = 1
//   0	rptRegistryData	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		int intCounter;
		cRegistry RegistryClass;
		string[] RegistryNames = null;
		string strReturn = "";
		int intPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (intCounter==Information.UBound(RegistryNames, 1)) {
				eArgs.EOF = true;
				return;
			}

			modRegistry.GetKeyValues_8(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", ref RegistryNames[intCounter], ref strReturn);
			txtName.Text = RegistryNames[intCounter];
			txtData.Text = strReturn;

			intCounter += 1;
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			RegistryClass = new cRegistry();
			RegistryClass.EnumerateValues(ref RegistryNames, 4);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			intCounter = 1;
			intPage = 1;
			txtMuniname.Text = modGlobalConstants.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm AMPM");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page "+Convert.ToString(intPage);
			intPage += 1;
		}

		private void rptRegistryData_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRegistryData properties;
			//rptRegistryData.Caption	= "Registry Data";
			//rptRegistryData.Icon	= "rptRegistryData.dsx":0000";
			//rptRegistryData.Left	= 0;
			//rptRegistryData.Top	= 0;
			//rptRegistryData.Width	= 11880;
			//rptRegistryData.Height	= 8595;
			//rptRegistryData.StartUpPosition	= 3;
			//rptRegistryData.SectionData	= "rptRegistryData.dsx":08CA;
			//End Unmaped Properties
		}
	}
}
