﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptUT3.
	/// </summary>
	partial class srptUT3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptUT3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerAccounts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterAccounts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerAccountsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterAccountsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerAccountsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterAccountsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCategory,
				this.fldSewerAccounts,
				this.fldWaterAccounts
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label4,
				this.Label5
			});
			this.GroupHeader1.Height = 0.5208333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldSewerAccountsTotal,
				this.fldWaterAccountsTotal,
				this.Line2
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Category Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.65625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 4.3125F;
			this.Line1.X1 = 1.65625F;
			this.Line1.X2 = 5.96875F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.75F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: left";
			this.Label2.Text = "Category";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 2.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "Sewer Accts";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 0.90625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.96875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Water Accts";
			this.Label5.Top = 0.3125F;
			this.Label5.Width = 0.90625F;
			// 
			// fldCategory
			// 
			this.fldCategory.Height = 0.1875F;
			this.fldCategory.Left = 1.65625F;
			this.fldCategory.Name = "fldCategory";
			this.fldCategory.Style = "font-size: 9pt; text-align: left";
			this.fldCategory.Text = "Field1";
			this.fldCategory.Top = 0F;
			this.fldCategory.Width = 2.09375F;
			// 
			// fldSewerAccounts
			// 
			this.fldSewerAccounts.Height = 0.1875F;
			this.fldSewerAccounts.Left = 3.875F;
			this.fldSewerAccounts.Name = "fldSewerAccounts";
			this.fldSewerAccounts.Style = "font-size: 9pt; text-align: right";
			this.fldSewerAccounts.Text = "Field1";
			this.fldSewerAccounts.Top = 0F;
			this.fldSewerAccounts.Width = 0.90625F;
			// 
			// fldWaterAccounts
			// 
			this.fldWaterAccounts.Height = 0.1875F;
			this.fldWaterAccounts.Left = 4.9375F;
			this.fldWaterAccounts.Name = "fldWaterAccounts";
			this.fldWaterAccounts.Style = "font-size: 9pt; text-align: right";
			this.fldWaterAccounts.Text = "Field1";
			this.fldWaterAccounts.Top = 0F;
			this.fldWaterAccounts.Width = 0.90625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.65625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt; font-weight: bold; text-align: left";
			this.Field1.Text = "Total:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 2.09375F;
			// 
			// fldSewerAccountsTotal
			// 
			this.fldSewerAccountsTotal.Height = 0.1875F;
			this.fldSewerAccountsTotal.Left = 3.875F;
			this.fldSewerAccountsTotal.Name = "fldSewerAccountsTotal";
			this.fldSewerAccountsTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldSewerAccountsTotal.Text = "Field1";
			this.fldSewerAccountsTotal.Top = 0.03125F;
			this.fldSewerAccountsTotal.Width = 0.90625F;
			// 
			// fldWaterAccountsTotal
			// 
			this.fldWaterAccountsTotal.Height = 0.1875F;
			this.fldWaterAccountsTotal.Left = 4.9375F;
			this.fldWaterAccountsTotal.Name = "fldWaterAccountsTotal";
			this.fldWaterAccountsTotal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldWaterAccountsTotal.Text = "Field1";
			this.fldWaterAccountsTotal.Top = 0.03125F;
			this.fldWaterAccountsTotal.Width = 0.90625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.65625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 4.3125F;
			this.Line2.X1 = 1.65625F;
			this.Line2.X2 = 5.96875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// srptUT3
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerAccountsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterAccountsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerAccounts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterAccounts;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerAccountsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterAccountsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
