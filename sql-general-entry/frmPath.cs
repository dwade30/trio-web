//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
    /// <summary>
    /// Summary description for frmPath.
    /// </summary>
    public partial class frmPath : fecherFoundation.FCForm
    {


        public frmPath()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null && Wisej.Web.Application.OpenForms.Count == 0)
                _InstancePtr = this;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmPath InstancePtr
        {
            get
            {
                if (_InstancePtr == null) // || _InstancePtr.IsDisposed
                    _InstancePtr = new frmPath();
                return _InstancePtr;
            }
        }
        protected static frmPath _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        


        //=========================================================

        bool boolAllowCDDVDNotReadyError;
        string strPathToReturn;

        public string Init(ref bool boolAllowCDDVDWriterNotReadyError = false, ref string strTitle = "", ref bool boolShowFile = true, ref string strDefaultDir = "")
        {
            string Init = "";
            boolAllowCDDVDNotReadyError = boolAllowCDDVDWriterNotReadyError;
            Init = "";
            if (strTitle != string.Empty)
            {
                lblTitle.Text = strTitle;
            }
            if (!boolShowFile)
            {
                File1.Visible = false;
                Dir1.Left = frmPath.InstancePtr.Drive1.Left;
            }
            if (strDefaultDir != string.Empty)
            {

                if (fso.FolderExists(strDefaultDir))
                {
                    Drive1.Drive = Strings.Left(strDefaultDir, 1);
                    Dir1.Path = strDefaultDir;
                }
            }
            strPathToReturn = "";
            this.Show(FCForm.FormShowEnum.Modal);
            Init = strPathToReturn;
            return Init;
        }

        private void cmdCancel_Click(object sender, System.EventArgs e)
        {
            modGNBas.gstrFilePath = "";
            modGNBas.gstrFileN = "";
            modGNBas.boolCancelled = true;
            strPathToReturn = "";
            this.Unload();
        }
        public void cmdCancel_Click()
        {
            cmdCancel_Click(cmdCancel, new System.EventArgs());
        }


        private void cmdSelect_Click(object sender, System.EventArgs e)
        {
            try
            {   // On Error GoTo ErrorHandler

                if (Dir1.Path != string.Empty)
                {
                    modGNBas.gstrFilePath = Dir1.Path;
                }
                else
                {
                    modGNBas.gstrFilePath = "";
                }
                if (File1.FileName != string.Empty)
                {
                    modGNBas.gstrFileN = File1.FileName;
                }
                else
                {
                    modGNBas.gstrFileN = "";
                }

                if (Dir1.Visible == false)
                {
                    modGNBas.gstrFilePath = Drive1.Drive;
                    modGNBas.gstrFileN = modGNBas.gstrFilePath;
                }

                modGNBas.boolCancelled = false;
                strPathToReturn = modGNBas.gstrFilePath;
                this.Unload();
                return;

            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error Number " + Convert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\n" + "In Select_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        public void cmdSelect_Click()
        {
            cmdSelect_Click(cmdSelect, new System.EventArgs());
        }



        private void Dir1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            File1.Path = Dir1.Path;
        }

        private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                string strTemp = "";
                int x;

                /*? On Error Resume Next  */


                Dir1.Path = Drive1.Drive;
                if (Information.Err().Number != 0)
                {
                    if (Information.Err().Number == 68)
                    {
                        Information.Err().Clear();

                        if (boolAllowCDDVDNotReadyError)
                        {
                            for (x = 0; x <= CDWriterXP1.GetDriveCount() - 1; x++)
                            {
                                if (Strings.UCase(CDWriterXP1.GetDriveLetter((short)x)) + ":" == Strings.UCase(Drive1.Drive))
                                {
                                    CDWriterXP1.OpenDrive((short)x);
                                    if (CDWriterXP1.GetDriveCapability(CDWriterXPLib.eCDRCapabilities.WriteDVDR) || CDWriterXP1.GetDriveCapability(CDWriterXPLib.eCDRCapabilities.WriteDVDRam) || CDWriterXP1.GetDriveCapability(CDWriterXPLib.eCDRCapabilities.WritesCDR) || CDWriterXP1.GetDriveCapability(CDWriterXPLib.eCDRCapabilities.WritesCDRW))
                                    {
                                        Dir1.Visible = false;
                                        return;
                                    }
                                }
                            } // x
                        }
                        else
                        {
                            MessageBox.Show("Device not ready", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                        Dir1.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error Number " + Convert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\n" + "In drive1 validate", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        Information.Err().Clear();
                    }
                    Dir1.Visible = true;
                    Drive1.Drive = Strings.Left(Dir1.Path, 1);

                    return;
                }

                Dir1.Path = Drive1.Text;
                File1.Path = Dir1.Path;
                return;
            }
            catch (Exception ex)
            {
                ErrorHandler:;
                MessageBox.Show("Error Number " + Convert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\n" + "In drive1 change", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }





        private void frmPath_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = ((int)e.KeyData) / 0x10000;

            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            } //end switch
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            cmdCancel_Click();
        }
        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }


        private void mnuProcess_Click(object sender, System.EventArgs e)
        {


            cmdSelect_Click();
        }


        private void frmPath_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmPath properties;
            //frmPath.ScaleWidth	= 5880;
            //frmPath.ScaleHeight	= 4245;
            //frmPath.LinkTopic	= "Form1";
            //frmPath.LockControls	= true;
            //End Unmaped Properties


            modGNBas.gstrFilePath = "";
            modGNBas.gstrFileN = "";
            modGNBas.boolCancelled = true;
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
            modGlobalFunctions.SetTRIOColors(this);
        }


    }
}