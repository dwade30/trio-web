﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptMV2.
	/// </summary>
	partial class srptMV2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMV2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDifference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOthGL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalGL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDifference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalOthGL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOthGL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalGL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDifference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOthGL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMonth,
				this.fldMV,
				this.fldGL,
				this.fldDifference,
				this.fldOthGL
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7
			});
			this.GroupHeader1.Height = 0.5104167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldTotalMV,
				this.Line2,
				this.fldTotalGL,
				this.fldTotalDifference,
				this.fldTotalOthGL
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Excise Tax Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.96875F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 5.5F;
			this.Line1.X1 = 0.96875F;
			this.Line1.X2 = 6.46875F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "text-align: left";
			this.Label3.Text = "Month";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 0.96875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.09375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "MV Excise";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 0.96875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "BD Excise";
			this.Label5.Top = 0.3125F;
			this.Label5.Width = 0.96875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Diff";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.96875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.28125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: right";
			this.Label7.Text = "Other Excise";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 0.96875F;
			// 
			// fldMonth
			// 
			this.fldMonth.Height = 0.1875F;
			this.fldMonth.Left = 1F;
			this.fldMonth.Name = "fldMonth";
			this.fldMonth.Style = "font-size: 9pt; text-align: left";
			this.fldMonth.Text = null;
			this.fldMonth.Top = 0F;
			this.fldMonth.Width = 0.96875F;
			// 
			// fldMV
			// 
			this.fldMV.Height = 0.1875F;
			this.fldMV.Left = 2.09375F;
			this.fldMV.Name = "fldMV";
			this.fldMV.Style = "font-size: 9pt; text-align: right";
			this.fldMV.Text = null;
			this.fldMV.Top = 0F;
			this.fldMV.Width = 0.96875F;
			// 
			// fldGL
			// 
			this.fldGL.Height = 0.1875F;
			this.fldGL.Left = 3.1875F;
			this.fldGL.Name = "fldGL";
			this.fldGL.Style = "font-size: 9pt; text-align: right";
			this.fldGL.Text = null;
			this.fldGL.Top = 0F;
			this.fldGL.Width = 0.96875F;
			// 
			// fldDifference
			// 
			this.fldDifference.Height = 0.1875F;
			this.fldDifference.Left = 5.375F;
			this.fldDifference.Name = "fldDifference";
			this.fldDifference.Style = "font-size: 9pt; text-align: right";
			this.fldDifference.Text = null;
			this.fldDifference.Top = 0F;
			this.fldDifference.Width = 0.96875F;
			// 
			// fldOthGL
			// 
			this.fldOthGL.Height = 0.1875F;
			this.fldOthGL.Left = 4.28125F;
			this.fldOthGL.Name = "fldOthGL";
			this.fldOthGL.Style = "font-size: 9pt; text-align: right";
			this.fldOthGL.Text = null;
			this.fldOthGL.Top = 0F;
			this.fldOthGL.Width = 0.96875F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt; font-weight: bold; text-align: left";
			this.Field1.Text = "Total:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 0.96875F;
			// 
			// fldTotalMV
			// 
			this.fldTotalMV.Height = 0.1875F;
			this.fldTotalMV.Left = 2.09375F;
			this.fldTotalMV.Name = "fldTotalMV";
			this.fldTotalMV.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalMV.Text = "Field1";
			this.fldTotalMV.Top = 0.03125F;
			this.fldTotalMV.Width = 0.96875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.96875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.5F;
			this.Line2.X1 = 0.96875F;
			this.Line2.X2 = 6.46875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldTotalGL
			// 
			this.fldTotalGL.Height = 0.1875F;
			this.fldTotalGL.Left = 3.1875F;
			this.fldTotalGL.Name = "fldTotalGL";
			this.fldTotalGL.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalGL.Text = "Field1";
			this.fldTotalGL.Top = 0.03125F;
			this.fldTotalGL.Width = 0.96875F;
			// 
			// fldTotalDifference
			// 
			this.fldTotalDifference.Height = 0.1875F;
			this.fldTotalDifference.Left = 5.375F;
			this.fldTotalDifference.Name = "fldTotalDifference";
			this.fldTotalDifference.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalDifference.Text = "Field1";
			this.fldTotalDifference.Top = 0.03125F;
			this.fldTotalDifference.Width = 0.96875F;
			// 
			// fldTotalOthGL
			// 
			this.fldTotalOthGL.Height = 0.1875F;
			this.fldTotalOthGL.Left = 4.28125F;
			this.fldTotalOthGL.Name = "fldTotalOthGL";
			this.fldTotalOthGL.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalOthGL.Text = "Field1";
			this.fldTotalOthGL.Top = 0.03125F;
			this.fldTotalOthGL.Width = 0.96875F;
			// 
			// srptMV2
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOthGL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalGL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDifference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOthGL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMV;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGL;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDifference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOthGL;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMV;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalGL;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDifference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOthGL;
	}
}
