﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptFAAudit.
	/// </summary>
	partial class srptFAAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptFAAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEntries = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalEntries = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalOriginalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalNetValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEntries)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalEntries)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOriginalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldYear,
				this.fldEntries,
				this.fldOriginalValue,
				this.fldDepreciation,
				this.fldNetValue
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 1.21875F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-size: 9pt; text-align: left";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.78125F;
			// 
			// fldEntries
			// 
			this.fldEntries.Height = 0.1875F;
			this.fldEntries.Left = 2.09375F;
			this.fldEntries.Name = "fldEntries";
			this.fldEntries.Style = "font-size: 9pt; text-align: right";
			this.fldEntries.Text = "Field1";
			this.fldEntries.Top = 0F;
			this.fldEntries.Width = 1F;
			// 
			// fldOriginalValue
			// 
			this.fldOriginalValue.Height = 0.1875F;
			this.fldOriginalValue.Left = 3.15625F;
			this.fldOriginalValue.Name = "fldOriginalValue";
			this.fldOriginalValue.Style = "font-size: 9pt; text-align: right";
			this.fldOriginalValue.Text = "Field1";
			this.fldOriginalValue.Top = 0F;
			this.fldOriginalValue.Width = 1F;
			// 
			// fldDepreciation
			// 
			this.fldDepreciation.Height = 0.1875F;
			this.fldDepreciation.Left = 4.21875F;
			this.fldDepreciation.Name = "fldDepreciation";
			this.fldDepreciation.Style = "font-size: 9pt; text-align: right";
			this.fldDepreciation.Text = "Field1";
			this.fldDepreciation.Top = 0F;
			this.fldDepreciation.Width = 1F;
			// 
			// fldNetValue
			// 
			this.fldNetValue.Height = 0.1875F;
			this.fldNetValue.Left = 5.28125F;
			this.fldNetValue.Name = "fldNetValue";
			this.fldNetValue.Style = "font-size: 9pt; text-align: right";
			this.fldNetValue.Text = "Field1";
			this.fldNetValue.Top = 0F;
			this.fldNetValue.Width = 1F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Line1,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7
			});
			this.GroupHeader1.Height = 0.8541667F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Fixed Assets";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 2.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label2.Text = "Asset Summary";
			this.Label2.Top = 0.375F;
			this.Label2.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.84375F;
			this.Line1.Width = 5.53125F;
			this.Line1.X1 = 1.125F;
			this.Line1.X2 = 6.65625F;
			this.Line1.Y1 = 0.84375F;
			this.Line1.Y2 = 0.84375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.21875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "text-align: left";
			this.Label3.Text = "Year";
			this.Label3.Top = 0.65625F;
			this.Label3.Width = 0.78125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.09375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "# of Entries";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.15625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Orig Value";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 1F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.21875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Depreciation";
			this.Label6.Top = 0.65625F;
			this.Label6.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.28125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: right";
			this.Label7.Text = "Net Value";
			this.Label7.Top = 0.65625F;
			this.Label7.Width = 1F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldTotalEntries,
				this.fldTotalOriginalValue,
				this.fldTotalDepreciation,
				this.fldTotalNetValue,
				this.Line2
			});
			this.GroupFooter1.Height = 0.2291667F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.21875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt; font-weight: bold; text-align: left";
			this.Field1.Text = "Total:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 0.78125F;
			// 
			// fldTotalEntries
			// 
			this.fldTotalEntries.Height = 0.1875F;
			this.fldTotalEntries.Left = 2.09375F;
			this.fldTotalEntries.Name = "fldTotalEntries";
			this.fldTotalEntries.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalEntries.Text = "Field1";
			this.fldTotalEntries.Top = 0.03125F;
			this.fldTotalEntries.Width = 1F;
			// 
			// fldTotalOriginalValue
			// 
			this.fldTotalOriginalValue.Height = 0.1875F;
			this.fldTotalOriginalValue.Left = 3.15625F;
			this.fldTotalOriginalValue.Name = "fldTotalOriginalValue";
			this.fldTotalOriginalValue.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalOriginalValue.Text = "Field1";
			this.fldTotalOriginalValue.Top = 0.03125F;
			this.fldTotalOriginalValue.Width = 1F;
			// 
			// fldTotalDepreciation
			// 
			this.fldTotalDepreciation.Height = 0.1875F;
			this.fldTotalDepreciation.Left = 4.21875F;
			this.fldTotalDepreciation.Name = "fldTotalDepreciation";
			this.fldTotalDepreciation.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalDepreciation.Text = "Field1";
			this.fldTotalDepreciation.Top = 0.03125F;
			this.fldTotalDepreciation.Width = 1F;
			// 
			// fldTotalNetValue
			// 
			this.fldTotalNetValue.Height = 0.1875F;
			this.fldTotalNetValue.Left = 5.28125F;
			this.fldTotalNetValue.Name = "fldTotalNetValue";
			this.fldTotalNetValue.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.fldTotalNetValue.Text = "Field1";
			this.fldTotalNetValue.Top = 0.03125F;
			this.fldTotalNetValue.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.53125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.53125F;
			this.Line2.X1 = 1.53125F;
			this.Line2.X2 = 7.0625F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// srptFAAudit
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.srptFAAudit_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEntries)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalEntries)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOriginalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEntries;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetValue;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalEntries;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOriginalValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNetValue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
