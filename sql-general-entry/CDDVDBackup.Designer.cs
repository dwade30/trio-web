//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmCDDVDBackup.
	/// </summary>
	partial class frmCDDVDBackup : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbCloseDisk;
		public fecherFoundation.FCLabel lblCloseDisk;
		public fecherFoundation.FCComboBox cmbZip;
		public fecherFoundation.FCLabel lblZip;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optCloseDisk;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optZip;
		public Wisej.Web.PrintDialog CommonDialog1;
		public fecherFoundation.FCFrame framBurnOptions;
		public fecherFoundation.FCCheckBox chkDelete;
		public fecherFoundation.FCCheckBox chkCopyToLocal;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtTempDir;
		public fecherFoundation.FCTextBox txtNewDirectory;
		public fecherFoundation.FCCheckBox chkNewDirectory;
		public fecherFoundation.FCComboBox cboWriteSpeed;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCProgressBar prgTotalProgress;
		public fecherFoundation.FCProgressBar prgTrackProgress;
		public AxCDWriterXPLib.AxCDWriterXP CDWriterXP1;
		public AxAbaleZipLibrary.AxAbaleZip AbaleZip1;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuBurn;
		public fecherFoundation.FCToolStripMenuItem mnuAbort;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbCloseDisk = new fecherFoundation.FCComboBox();
			this.lblCloseDisk = new fecherFoundation.FCLabel();
			this.cmbZip = new fecherFoundation.FCComboBox();
			this.lblZip = new fecherFoundation.FCLabel();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCDDVDBackup));
			this.components = new System.ComponentModel.Container();
			//this.optCloseDisk = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			//this.optZip = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.CommonDialog1 = new Wisej.Web.PrintDialog();
			this.framBurnOptions = new fecherFoundation.FCFrame();
			this.chkDelete = new fecherFoundation.FCCheckBox();
			this.chkCopyToLocal = new fecherFoundation.FCCheckBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtTempDir = new fecherFoundation.FCTextBox();
			this.txtNewDirectory = new fecherFoundation.FCTextBox();
			this.chkNewDirectory = new fecherFoundation.FCCheckBox();
			this.cboWriteSpeed = new fecherFoundation.FCComboBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.prgTotalProgress = new fecherFoundation.FCProgressBar();
			this.prgTrackProgress = new fecherFoundation.FCProgressBar();
			this.CDWriterXP1 = new AxCDWriterXPLib.AxCDWriterXP();
			this.AbaleZip1 = new AxAbaleZipLibrary.AxAbaleZip();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBurn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAbort = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//
			// cmbCloseDisk
			//
			this.cmbCloseDisk.Items.Add("Close session leave disk open");
			this.cmbCloseDisk.Items.Add("Close disk when done");
			this.cmbCloseDisk.Name = "cmbCloseDisk";
			this.cmbCloseDisk.Location = new System.Drawing.Point(6, 44);
			this.cmbCloseDisk.Size = new System.Drawing.Size(120, 40);
			this.cmbCloseDisk.Text = "Close disk when done";
			//
			// lblCloseDisk
			//
			this.lblCloseDisk.Name = "lblCloseDisk";
			this.lblCloseDisk.Text = "lblCloseDisk";
			this.lblCloseDisk.Location = new System.Drawing.Point(1, 44);
			this.lblCloseDisk.AutoSize = true;

			//
			// cmbZip
			//
			this.cmbZip.Items.Add("Copy as multiple files");
			this.cmbZip.Items.Add("Copy as single Zip file");
			this.cmbZip.Name = "cmbZip";
			this.cmbZip.Location = new System.Drawing.Point(16, 46);
			this.cmbZip.Size = new System.Drawing.Size(120, 40);
			this.cmbZip.Text = "Copy as single Zip file";
			//
			// lblZip
			//
			this.lblZip.Name = "lblZip";
			this.lblZip.Text = "lblZip";
			this.lblZip.Location = new System.Drawing.Point(1, 46);
			this.lblZip.AutoSize = true;

			//((System.ComponentModel.ISupportInitialize)(this.optCloseDisk)).BeginInit();
			//((System.ComponentModel.ISupportInitialize)(this.optZip)).BeginInit();
			//
			// CommonDialog1
			//
			//
			// framBurnOptions
			//
			this.framBurnOptions.Controls.Add(this.chkDelete);
			this.framBurnOptions.Controls.Add(this.chkCopyToLocal);
			this.framBurnOptions.Controls.Add(this.cmdBrowse);
			this.framBurnOptions.Controls.Add(this.txtTempDir);
			this.framBurnOptions.Controls.Add(this.txtNewDirectory);
			this.framBurnOptions.Controls.Add(this.chkNewDirectory);
			this.framBurnOptions.Controls.Add(this.cboWriteSpeed);
			this.framBurnOptions.Controls.Add(this.Label3);
			this.framBurnOptions.Controls.Add(this.Label2);
			this.framBurnOptions.Name = "framBurnOptions";
			this.framBurnOptions.TabIndex = 6;
			this.framBurnOptions.Location = new System.Drawing.Point(4, 107);
			this.framBurnOptions.Size = new System.Drawing.Size(389, 137);
			this.framBurnOptions.Text = "Burn Options";
			this.framBurnOptions.BackColor = System.Drawing.SystemColors.Control;
			this.framBurnOptions.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// chkDelete
			//
			this.chkDelete.Name = "chkDelete";
			this.chkDelete.TabIndex = 19;
			this.ToolTip1.SetToolTip(this.chkDelete, "This will delete *ALL* files from the temp directory even if they were there before this backup process.");
			this.chkDelete.Location = new System.Drawing.Point(172, 48);
			this.chkDelete.Size = new System.Drawing.Size(211, 19);
			this.chkDelete.Text = "Delete files from temp when done";
			this.chkDelete.BackColor = System.Drawing.SystemColors.Control;
			//
			// chkCopyToLocal
			//
			this.chkCopyToLocal.Name = "chkCopyToLocal";
			this.chkCopyToLocal.TabIndex = 18;
			this.chkCopyToLocal.Location = new System.Drawing.Point(172, 29);
			this.chkCopyToLocal.Size = new System.Drawing.Size(201, 19);
			this.chkCopyToLocal.Text = "Copy files to temp directory first";
			this.chkCopyToLocal.BackColor = System.Drawing.SystemColors.Control;
			this.chkCopyToLocal.Checked = true;
			this.chkCopyToLocal.CheckState = Wisej.Web.CheckState.Checked;
			//
			// cmdBrowse
			//
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.TabIndex = 16;
			this.ToolTip1.SetToolTip(this.cmdBrowse, "Browse for a suitable directory to use as a temp directory or create the temp directory in.");
			this.cmdBrowse.Location = new System.Drawing.Point(284, 77);
			this.cmdBrowse.Size = new System.Drawing.Size(54, 23);
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.BackColor = System.Drawing.SystemColors.Control;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			//
			// txtTempDir
			//
			this.txtTempDir.Name = "txtTempDir";
			this.txtTempDir.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtTempDir, "Zip files and networked files (if copying files is checked) will be copied here in this temp directory before writing to disk.");
			this.txtTempDir.Location = new System.Drawing.Point(173, 100);
			this.txtTempDir.Size = new System.Drawing.Size(166, 23);
			this.txtTempDir.Text = "";
			this.txtTempDir.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtNewDirectory
			//
			this.txtNewDirectory.Name = "txtNewDirectory";
			this.txtNewDirectory.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtNewDirectory, "Create a new directory to store the file(s) to backup in.");
			this.txtNewDirectory.Location = new System.Drawing.Point(11, 100);
			this.txtNewDirectory.Size = new System.Drawing.Size(141, 23);
			this.txtNewDirectory.Text = "";
			this.txtNewDirectory.BackColor = System.Drawing.SystemColors.Window;
			//
			// chkNewDirectory
			//
			this.chkNewDirectory.Name = "chkNewDirectory";
			this.chkNewDirectory.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.chkNewDirectory, "Create a new directory to store the file(s) to backup in.");
			this.chkNewDirectory.Location = new System.Drawing.Point(10, 79);
			this.chkNewDirectory.Size = new System.Drawing.Size(145, 17);
			this.chkNewDirectory.Text = "Create New Directory";
			this.chkNewDirectory.BackColor = System.Drawing.SystemColors.Control;
			//
			// cboWriteSpeed
			//
			this.cboWriteSpeed.Name = "cboWriteSpeed";
			this.cboWriteSpeed.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.cboWriteSpeed, "Choose what speed the drive writes at.  Some media may not be rated to record at the drives top speed.");
			this.cboWriteSpeed.Location = new System.Drawing.Point(10, 43);
			this.cboWriteSpeed.Size = new System.Drawing.Size(80, 22);
			this.cboWriteSpeed.Text = "Combo1";
			this.cboWriteSpeed.BackColor = System.Drawing.SystemColors.Window;
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.TabIndex = 17;
			this.ToolTip1.SetToolTip(this.Label3, "Zip files and networked files (if copying files is checked) will be copied here in this temp directory.");
			this.Label3.Location = new System.Drawing.Point(172, 80);
			this.Label3.Size = new System.Drawing.Size(110, 16);
			this.Label3.Text = "Temp directory";
			this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.Label2, "Choose what speed the drive writes at.  Some media may not be rated to record at the drives top speed.");
			this.Label2.Location = new System.Drawing.Point(12, 20);
			this.Label2.Size = new System.Drawing.Size(79, 18);
			this.Label2.Text = "Write Speed";
			this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//
			//
			//
			//
			//
			// prgTotalProgress
			//
			this.prgTotalProgress.Name = "prgTotalProgress";
			this.prgTotalProgress.TabIndex = 11;
			this.prgTotalProgress.Location = new System.Drawing.Point(103, 279);
			this.prgTotalProgress.Size = new System.Drawing.Size(191, 18);
			this.prgTotalProgress.Minimum = 0;
			this.prgTotalProgress.Maximum = 100;
			//
			// prgTrackProgress
			//
			this.prgTrackProgress.Name = "prgTrackProgress";
			this.prgTrackProgress.Visible = false;
			this.prgTrackProgress.TabIndex = 12;
			this.prgTrackProgress.Location = new System.Drawing.Point(4, 279);
			this.prgTrackProgress.Size = new System.Drawing.Size(191, 18);
			this.prgTrackProgress.Minimum = 0;
			this.prgTrackProgress.Maximum = 100;
			//
			// CDWriterXP1
			//
			this.CDWriterXP1.Name = "CDWriterXP1";
			this.CDWriterXP1.Location = new System.Drawing.Point(318, 24);
			this.CDWriterXP1._StockProps = 0;
			this.CDWriterXP1.LicenseCode = "09692067199093982821B";
			this.CDWriterXP1.ClosingDisc += new System.EventHandler(this.CDWriterXP1_ClosingDisc);
			this.CDWriterXP1.ClosingSession += new System.EventHandler(this.CDWriterXP1_ClosingSession);
			this.CDWriterXP1.ISOItemAdded += new AxCDWriterXPLib._DCDWriterXPEvents_ISOItemAddedEventHandler(this.CDWriterXP1_ISOItemAdded);
			this.CDWriterXP1.PreparingToWrite += new System.EventHandler(this.CDWriterXP1_PreparingToWrite);
			this.CDWriterXP1.TrackFileError += new AxCDWriterXPLib._DCDWriterXPEvents_TrackFileErrorEventHandler(this.CDWriterXP1_TrackFileError);
			this.CDWriterXP1.TrackWriteStart += new AxCDWriterXPLib._DCDWriterXPEvents_TrackWriteStartEventHandler(this.CDWriterXP1_TrackWriteStart);
			this.CDWriterXP1.TrackWriteStatus += new AxCDWriterXPLib._DCDWriterXPEvents_TrackWriteStatusEventHandler(this.CDWriterXP1_TrackWriteStatus);
			this.CDWriterXP1.WriteError += new AxCDWriterXPLib._DCDWriterXPEvents_WriteErrorEventHandler(this.CDWriterXP1_WriteError);
			this.CDWriterXP1.WritingCancelled += new System.EventHandler(this.CDWriterXP1_WritingCancelled);
			this.CDWriterXP1.WritingComplete += new System.EventHandler(this.CDWriterXP1_WritingComplete);
			//
			// AbaleZip1
			//
			this.AbaleZip1.Name = "AbaleZip1";
			this.AbaleZip1.Location = new System.Drawing.Point(293, 28);
			this.AbaleZip1.BasePath = "";
			this.AbaleZip1.CompressionLevel = 6;
			this.AbaleZip1.EncryptionPassword = "";
			this.AbaleZip1.RequiredFileAttributes = 0;
			this.AbaleZip1.ExcludedFileAttributes = 24;
			this.AbaleZip1.FilesToProcess = "";
			this.AbaleZip1.FilesToExclude = "";
			this.AbaleZip1.MinDateToProcess = 2;
			this.AbaleZip1.MaxDateToProcess = 2958465;
			this.AbaleZip1.MinSizeToProcess = 0;
			this.AbaleZip1.MaxSizeToProcess = 0;
			this.AbaleZip1.SplitSize = 0;
			this.AbaleZip1.PreservePaths = true;
			this.AbaleZip1.ProcessSubfolders = false;
			this.AbaleZip1.SkipIfExisting = false;
			this.AbaleZip1.SkipIfNotExisting = false;
			this.AbaleZip1.SkipIfOlderDate = false;
			this.AbaleZip1.SkipIfOlderVersion = false;
			this.AbaleZip1.TempFolder = "";
			this.AbaleZip1.UseTempFile = true;
			this.AbaleZip1.UnzipToFolder = "";
			this.AbaleZip1.ZipFilename = "";
			this.AbaleZip1.SpanMultipleDisks = 2;
			this.AbaleZip1.ExtraHeaders = 10;
			this.AbaleZip1.ZipOpenedFiles = false;
			this.AbaleZip1.SfxBinaryModule = "";
			this.AbaleZip1.DeleteZippedFiles = false;
			this.AbaleZip1.FirstDiskFreeSpace = 0;
			this.AbaleZip1.MinDiskFreeSpace = 0;
			this.AbaleZip1.EventsToTrigger = 4194303;
			this.AbaleZip1.CompressionMethod = 8;
			//
			// Label5
			//
			this.Label5.Name = "Label5";
			this.Label5.Visible = false;
			this.Label5.TabIndex = 14;
			this.Label5.Location = new System.Drawing.Point(6, 259);
			this.Label5.Size = new System.Drawing.Size(185, 16);
			this.Label5.Text = "Track Written";
			this.Label5.BackColor = System.Drawing.SystemColors.Control;
			this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// Label4
			//
			this.Label4.Name = "Label4";
			this.Label4.TabIndex = 13;
			this.Label4.Location = new System.Drawing.Point(106, 259);
			this.Label4.Size = new System.Drawing.Size(185, 16);
			this.Label4.Text = "Total Written";
			this.Label4.BackColor = System.Drawing.SystemColors.Control;
			this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(340, 40);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuBurn, this.mnuAbort, this.mnuSepar1, this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "&File";
			//
			// mnuBurn
			//
			this.mnuBurn.Name = "mnuBurn";
			this.mnuBurn.Text = "Burn CD/DVD";
			this.mnuBurn.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuBurn.Click += new System.EventHandler(this.mnuBurn_Click);
			//
			// mnuAbort
			//
			this.mnuAbort.Name = "mnuAbort";
			this.mnuAbort.Enabled = false;
			this.mnuAbort.Text = "Abort burn process";
			this.mnuAbort.Click += new System.EventHandler(this.mnuAbort_Click);
			//
			// mnuSepar1
			//
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "E&xit  (Esc)";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuFile});
			//
			// frmCDDVDBackup
			//
			this.ClientSize = new System.Drawing.Size(396, 283);
			this.Controls.Add(this.framBurnOptions);
			this.Controls.Add(this.cmbCloseDisk);
			this.Controls.Add(this.lblCloseDisk);

			this.Controls.Add(this.cmbZip);
			this.Controls.Add(this.lblZip);

			this.Controls.Add(this.prgTotalProgress);
			this.Controls.Add(this.prgTrackProgress);
			this.Controls.Add(this.CDWriterXP1);
			this.Controls.Add(this.AbaleZip1);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Label4);
			//this.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmCDDVDBackup";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmCDDVDBackup.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.WindowsDefaultLocation;
			this.Load += new System.EventHandler(this.frmCDDVDBackup_Load);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmCDDVDBackup_FormClosing);
			this.Text = "CD/DVD Backup";
			//((System.ComponentModel.ISupportInitialize)(this.optCloseDisk)).EndInit();
			//((System.ComponentModel.ISupportInitialize)(this.optZip)).EndInit();
			this.framBurnOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.CDWriterXP1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.AbaleZip1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}