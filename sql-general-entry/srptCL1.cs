﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptCL1.
	/// </summary>
	public partial class srptCL1 : FCSectionReport
	{
		public srptCL1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptCL1";
		}

		public static srptCL1 InstancePtr
		{
			get
			{
				return (srptCL1)Sys.GetInstance(typeof(srptCL1));
			}
		}

		protected srptCL1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCL1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curCLTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCLTotal;
		// vbPorter upgrade warning: curGLTotal As Decimal	OnWrite(short, double)
		Decimal curGLTotal;
		string strType = "";
		int lngNumberOfBills;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					CheckType2:
					;
					IncrementType();
					if (strType != "")
					{
						SetRecordSet();
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							eArgs.EOF = false;
						}
						else
						{
							goto CheckType2;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = strType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			strType = "RR";
			SetRecordSet();
			CheckType:
			;
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				IncrementType();
				if (strType != "")
				{
					SetRecordSet();
					goto CheckType;
				}
				else
				{
					this.Cancel();
					return;
				}
			}
			blnFirstRecord = true;
			curCLTotal = 0;
			curGLTotal = 0;
			lngNumberOfBills = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsInfo.EndOfFile())
			{
				clsDRWrapper rsBudInfo = new clsDRWrapper();
				clsDRWrapper rsAcctInfo = new clsDRWrapper();
				// TODO Get_Fields: Check the table for the column [BillYear] and replace with corresponding Get_Field method
				fldBillingYear.Text = FCConvert.ToString(rsInfo.Get_Fields("BillYear"));
				// TODO Get_Fields: Field [PrinOwedTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [PrinPaidTotal] not found!! (maybe it is an alias?)
				fldCL.Text = Strings.Format((rsInfo.Get_Fields("PrinOwedTotal") - rsInfo.Get_Fields("PrinPaidTotal")), "#,##0.00");
				// TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
				fldNumberOfBills.Text = Strings.Format(rsInfo.Get_Fields("NumberOfBills"), "#,##0");
				// TODO Get_Fields: Field [PrinOwedTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [PrinPaidTotal] not found!! (maybe it is an alias?)
				curCLTotal += (rsInfo.Get_Fields("PrinOwedTotal") - rsInfo.Get_Fields("PrinPaidTotal"));
				// TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
				lngNumberOfBills += rsInfo.Get_Fields("NumberOfBills");
				rsAcctInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = '" + strType + "'", "TWBD0000.vb1");
				if (rsAcctInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Conversion.Val(rsAcctInfo.Get_Fields("Account")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [BillYear] and replace with corresponding Get_Field method
						rsBudInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE LedgerAccount = '" + rsAcctInfo.Get_Fields("Account") + "' AND Suffix = '" + Strings.Mid(FCConvert.ToString(rsInfo.Get_Fields("BillYear")), 3, 2) + "'", "TWBD0000.vb1");
						if (rsBudInfo.EndOfFile() != true && rsBudInfo.BeginningOfFile() != true)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							curGLTotal = FCConvert.ToDecimal(Conversion.Val(rsBudInfo.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsBudInfo.Get_Fields("BudgetAdjustmentsTotal")) + Conversion.Val(rsBudInfo.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsBudInfo.Get_Fields("EncumbActivityTotal")) + Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")));
						}
						else
						{
							curGLTotal = 0;
						}
					}
					else
					{
						curGLTotal = 0;
					}
				}
				else
				{
					curGLTotal = 0;
				}
				fldGL.Text = Strings.Format(curGLTotal, "#,##0.00");
				fldDifference.Text = Strings.Format(curCLTotal - curGLTotal, "#,##0.00");
				if (Strings.Format(curCLTotal, "#,##0.00") != Strings.Format(curGLTotal, "#,##0.00"))
				{
					chkFlag.Checked = true;
				}
				else
				{
					chkFlag.Checked = false;
				}
				curCLTotal = 0;
				curGLTotal = 0;
				rsAcctInfo.Dispose();
				rsBudInfo.Dispose();
			}
		}

		private void SetRecordSet()
		{
			if (strType == "RR")
			{
				rsInfo.OpenRecordset("SELECT left(BillingYear, 4) AS BillYear, COUNT(left(BillingYear, 4)) as NumberOfBills, SUM(TaxDue1) + SUM(TaxDue2) + SUM(TaxDue3) + SUM(TaxDue4) as PrinOwedTotal, SUM(PrincipalPaid) as PrinPaidTotal, 0 as CostOwedTotal, 0 as CostPaidTotal, 0 as InterestOwedTotal, 0 as InterestPaidTotal FROM BillingMaster INNER JOIN " + rsInfo.CurrentPrefix + "RealEstate.dbo.Master ON BillingMaster.Account = " + rsInfo.CurrentPrefix + "RealEstate.dbo.Master.RSAccount WHERE RSCard = 1 AND not TaxAcquired = 1 AND BillingType = 'RE' AND LienRecordNumber = 0 GROUP BY left(BillingYear, 4) HAVING (SUM(TaxDue1) + SUM(TaxDue2) + SUM(TaxDue3) + SUM(TaxDue4)) - SUM(PrincipalPaid) <> 0 ORDER BY left(BillingYear, 4)", "TWCL0000.vb1");
			}
			else if (strType == "PR")
			{
				rsInfo.OpenRecordset("SELECT left(BillingYear, 4) AS BillYear, COUNT(left(BillingYear, 4)) as NumberOfBills, SUM(TaxDue1) + SUM(TaxDue2) + SUM(TaxDue3) + SUM(TaxDue4) as PrinOwedTotal, SUM(PrincipalPaid) as PrinPaidTotal, 0 as CostOwedTotal, 0 as CostPaidTotal, 0 as InterestOwedTotal, 0 as InterestPaidTotal FROM BillingMaster WHERE BillingType = 'PP' AND LienRecordNumber = 0 GROUP BY left(BillingYear, 4) HAVING (SUM(TaxDue1) + SUM(TaxDue2) + SUM(TaxDue3) + SUM(TaxDue4)) - SUM(PrincipalPaid) <> 0 ORDER BY left(BillingYear, 4)", "TWCL0000.vb1");
			}
		}

		private void IncrementType()
		{
			if (strType == "RR")
			{
				strType = "PR";
			}
			else
			{
				strType = "";
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			if (strType == "RR")
			{
				fldBillType.Text = "R/E Bills";
			}
			else if (strType == "PR")
			{
				fldBillType.Text = "P/P Bills";
			}
		}

		

		private void srptCL1_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
