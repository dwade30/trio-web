﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Authorization;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess;
using System;
using System.IO;
using System.Runtime.InteropServices;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Models;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;
using clsDRWrapper = Global.clsDRWrapper;
using SettingsInfo = Global.SettingsInfo;
using WorkstationGroup = Global.WorkstationGroup;

namespace TWGNENTY
{
    public class modTrioStart
    {
        public const int KEYEVENTF_EXTENDEDKEY = 0x1;
        public const int KEYEVENTF_KEYUP = 0x2;
        public const int NORMAL_PRIORITY_CLASS = 0x20;
        public const int CREATE_NEW_PROCESS_GROUP = 0x200;
        public const int CKLEVELFULLCLERK = 0;
        // Levels are ored together.  6 would be vitals only population level2
        public const int CKLEVELPOPLEVEL1 = 1;
        // POP 4000
        public const int CKLEVELPOPLEVEL2 = 2;
        // POP 10000
        public const int CKLEVELVITALSONLY = 4;
        public const int CKLEVELDOGSONLY = 8;
        // Public gGlobalSettings As New cGlobalSettings
        public struct PROCESS_INFORMATION
        {
            public int hProcess;
            public int hThread;
            public int dwProcessID;
            public int dwThreadId;
        };

        public struct STARTUPINFO
        {
            public int cb;
            public int lpReserved;
            public int lpDesktop;
            public int lpTitle;
            public int dwX;
            public int dwY;
            public int dwXSize;
            public int dwYSize;
            public int dwXCountChars;
            public int dwYCountChars;
            public int dwFillAttribute;
            public int dwFlags;
            public int lpReserved2;
            public int hStdInput;
            public int hStdOutput;
            public int hStdError;
            public short wShowWindow;
            public short cbReserved2;
        };
        //[DllImport("kernel32", EntryPoint = "CreateProcessA")]
        //public static extern int CreateProcess(string lpApplicationName, string lpCommandLine, int lpProcessAttributes, int lpThreadAttributes, int bInheritHandles, int dwCreationFlags, ref void* lpEnvironment, string lpCurrentDirectory, ref STARTUPINFO lpStartupInfo, ref PROCESS_INFORMATION lpProcessInformation);
        [DllImport("kernel32")]
        public static extern int OpenProcess(uint dwDesiredAccess, int bInheritHandle, int dwProcessID);

        [DllImport("kernel32")]
        public static extern int CloseHandle(int hObject);

        [DllImport("kernel32")]
        public static extern int WaitForSingleObject(int hHandle, int dwMilliseconds);

        [DllImport("kernel32", EntryPoint = "SetEnvironmentVariableA")]
        public static extern int SetEnvironmentVariable(string lpName, IntPtr lpValue);

        public static int SetEnvironmentVariableWrp(string lpName, ref string lpValue)
        {
            int ret;
            IntPtr plpValue = FCUtils.GetByteFromString(lpValue);
            ret = SetEnvironmentVariable(lpName, plpValue);
            FCUtils.GetStringFromByte(ref lpValue, plpValue);
            return ret;
        }

        public const int INFINITE = -1;
        public const uint SYNCHRONIZE = 0x100000;

        public struct UnderlineInfo
        {
            public short intStart;
            public short intLength;
        };
        // Private Sub CheckFTPUser()
        // Dim clsSave As New clsDRWrapper
        // Call clsSave.Execute("update globalvariables set ftpsiteuser =  MuniName  where ftpsiteuser = 'TRIO' OR ftpsiteuser = ''", "twgn0000.vb1")
        //
        // End Sub
        private static bool UpdateResCodes()
        {
            bool UpdateResCodes = false;
            clsDRWrapper clsLoad = new clsDRWrapper();
            clsDRWrapper clsSave = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                UpdateResCodes = false;

                if (modGlobalConstants.Statics.gboolMV)
                {
                    if (modRegionalTown.IsRegionalTown())
                    {
                        clsLoad.OpenRecordset("select * from residence", modGNBas.strMVDatabase);
                        clsSave.OpenRecordset("select * from tblregions order by townnumber", "CentralData");

                        while (!clsSave.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [rescode] and replace with corresponding Get_Field method
                            if (Strings.Trim(FCConvert.ToString(clsSave.Get_Fields("rescode"))) == string.Empty)
                            {
                                if (clsLoad.FindFirstRecord("Town", clsSave.Get_Fields_String("townname")))
                                {
                                    clsSave.Edit();

                                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                                    clsSave.Set_Fields("rescode", clsLoad.Get_Fields("code"));
                                    clsSave.Update();
                                }
                            }

                            clsSave.MoveNext();
                        }
                    }
                }

                UpdateResCodes = true;

                return UpdateResCodes;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In UpdateResCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsSave.DisposeOf();
                clsLoad.DisposeOf();
            }
            return UpdateResCodes;
        }

        public static bool CheckVersion()
        {
            bool result = false;
            cSystemSettings sysSet = new cSystemSettings();
            result = sysSet.CheckVersion();

            if (result)
            {
	            cCentralDataDB cenDB = new cCentralDataDB();
	            result = cenDB.CheckVersion();
            }
            
            return result;
        }

        private static bool CheckSystemDate()
        {
            bool CheckSystemDate = false;
            // this routine will set the MaxDate field to the current date
            // if the user has reset their system date back and tried to run TRIO then
            // this function will pick that up and not run.
            clsDRWrapper rs = new clsDRWrapper();
            modEntryMenuForm.Statics.gboolShowMessage = false;

            try
            {
                // On Error GoTo ErrorHandler
                rs.OpenRecordset("Select * from GlobalVariables", "SystemSettings");

                if (!rs.EndOfFile())
                {
                    if (Strings.Trim(FCConvert.ToString(rs.Get_Fields("MaxDate")) + " ") == string.Empty)
                    {
                        rs.Edit();
                        rs.Set_Fields("MaxDate", DateTime.Today);
                        rs.Update();

                        // Call MakeGEBackup
                    }
                    else
                        if (rs.Get_Fields_DateTime("MaxDate").ToOADate() > DateTime.Today.ToOADate())
                        {
                            if (Conversion.Val(rs.Get_Fields_Int16("Violations") + "") < 10)
                            {
                                // MsgBox "The current system date is older than the most recent recorded date TRIO was run." & vbNewLine & "You will have " & (9 - Val(RS.Fields("violations") & "")) & " more times before the problem must be corrected.", vbExclamation
                                rs.Edit();
                                rs.Set_Fields("violations", Conversion.Val("0" + rs.Get_Fields_Int16("violations")) + 1);
                                rs.Update();
                                if (Conversion.Val(rs.Get_Fields_Int16("violations")) < 10) CheckSystemDate = true;
                            }

                            if (dlgContactTrio.InstancePtr.GetCode(FCConvert.ToInt16(10 - Conversion.Val(rs.Get_Fields_Int16("violations") + ""))))
                            {
                                modEntryMenuForm.Statics.gboolShowMessage = true;
                                MessageBox.Show("Code Accepted", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                rs.Edit();
                                rs.Set_Fields("maxdate", DateTime.Today);
                                rs.Set_Fields("violations", 0);
                                rs.Update();
                                CheckSystemDate = true;

                                // Call MakeGEBackup
                            }

                            if (!CheckSystemDate) return CheckSystemDate;
                        }
                        else
                            if (rs.Get_Fields_DateTime("MaxDate").ToOADate() == DateTime.Today.ToOADate())
                            { }
                            else
                            {
                                modEntryMenuForm.Statics.gboolShowMessage = true;
                                rs.Edit();
                                rs.Set_Fields("MaxDate", DateTime.Today);
                                rs.Update();

                                // Call MakeGEBackup
                            }
                }

                CheckSystemDate = true;

               
                return CheckSystemDate;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rs.DisposeOf();
            }
            return CheckSystemDate;
        }

        private static TrioContextFactory GetContextFactory()
        {
            var dataDetails = new DataContextDetails();
            dataDetails.DataSource = StaticSettings.gGlobalSettings.DataSource;
            dataDetails.DataEnvironment = StaticSettings.gGlobalSettings.DataEnvironment;
            dataDetails.Password = StaticSettings.gGlobalSettings.Password;
            dataDetails.UserID = StaticSettings.gGlobalSettings.UserName;
            var contextFactory = new TrioContextFactory(dataDetails, StaticSettings.gGlobalSettings);
            return contextFactory;
        }

        public static StartupResponse Main()
        {
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsDateCheck = new clsDRWrapper();
            clsDRWrapper rsLogin = new clsDRWrapper();

            try
            {
                /* On Error GoTo ErrorHandler */ // Dim db              As DAO.Database
                bool found;
                FileInfo tempFile;

                //Scripting.File tempFile2;
                int x;
                string strArguments;
                string strAppPath;
                string strDataPath;
                string clientName = "";
                string strConfigPath;
                cSystemSettings sysSet = new cSystemSettings();
                SettingsInfo tSet = new SettingsInfo();

	            Statics.gboolUseRegistryFromDB = false;
                strArguments = FCUtils.GetCommandLine();

                if (Strings.Trim(strArguments) != "")
                {
                    string[] strAry = null;
                    strAry = Strings.Split(strArguments, " ", -1, CompareConstants.vbBinaryCompare);

                    for (x = 0; x <= Information.UBound(strAry, 1); x++)
                    {
                        if (Strings.LCase(strAry[x]) == "-regfromdb")
                        {
                            Statics.gboolUseRegistryFromDB = true;
                        }
                    }

                    // x
                }

                strAppPath = FCFileSystem.Statics.UserDataFolder;

                if (Strings.Right(strAppPath, 1) != "\\")
                {
                    strAppPath += "\\";
                }

                strDataPath = FCFileSystem.Statics.UserDataFolder;

                if (Strings.Right(strDataPath, 1) != "\\")
                {
                    strDataPath += "\\";
                }

                strConfigPath = Application.MapPath("\\");

                if (Strings.Right(strConfigPath, 1) != "\\")
                {
                    strConfigPath += "\\";
                }

                string url = Application.Url;
                if (url.ToUpper().Contains(".TRIO-WEB.COM"))
                {
	                clientName = url.Left(url.ToUpper().IndexOf(".TRIO-WEB.COM"));
	                clientName = clientName.ToUpper().Replace("HTTPS://", "");
                }
                if (!tSet.LoadSettings(strConfigPath + tSet.GetDefaultSettingsFileName(), clientName))
                {
                    MessageBox.Show($"The site you are trying to visit cannot be found.  Please contact TRIO support for assistance.", "Page Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return StartupResponse.RedirectToSupport;
                }
                else
                {
					StaticSettings.gGlobalSettings.IsHarrisStaffComputer = tSet.HarrisStaffUser;
				}

                User tUser;
                modGlobalConstants.Statics.gstrEntryFlag = "";

                modReplaceWorkFiles.EntryFlagFile(false, "", true);

                if ((modGlobalConstants.Statics.gstrEntryFlag == string.Empty) || (Convert.ToByte(Strings.Left(modGlobalConstants.Statics.gstrEntryFlag + " ", 1)[0]) == 0))
                {
                    WorkstationGroup[] wsgInfo;
                    wsgInfo = tSet.GetAvailableSetups();

                    var dataDetails = new DataContextDetails();
                    dataDetails.DataSource = tSet.GetItem("DataSource");
                    dataDetails.DataEnvironment = wsgInfo[0]
                       .GroupName;
                    dataDetails.Password = tSet.GetItem("Password");
                    dataDetails.UserID = tSet.GetItem("UserName");
                    cClientSettingsDB csDB = new cClientSettingsDB(new SQLConfigInfo(){ServerInstance = dataDetails.DataSource,UserName = dataDetails.UserID, Password =   dataDetails.Password},"");
                    csDB.CheckVersion();
                    var contextFactory = new TrioContextFactory(dataDetails, StaticSettings.gGlobalSettings);

                    var loginForm = new frmLogin(contextFactory);
                    tUser = loginForm.Login("", StaticSettings.gGlobalSettings.IsHarrisStaffComputer);

                    if (tUser == null)
                    {
                        MessageBox.Show("Unable to authenticate user", "Unauthorized", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        Application.Exit();

                        return StartupResponse.Logout;
                    }

                    if (tUser.Id == 0)
                    {
                        MessageBox.Show("Unable to authenticate user", "Unauthorized", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        Application.Exit();

                        return StartupResponse.Logout;
                    }

                    TWSharedLibrary.Variables.Statics.IntUserID = tUser.Id;
                    if (TWSharedLibrary.Data.DataReader.Statics.ConnectionPools == null ||
                        String.IsNullOrWhiteSpace(TWSharedLibrary.Data.DataReader.Statics.ConnectionPools.DefaultMegaGroup))
                    {
                        DataConfigurator.LoadSQLConfig();
                    }
                }
                else
                {
                    modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;

                    if (!modGlobalFunctions.LoadSQLConfig())
                    {
                        //FC:TODO:AM
                        //goto GetLogin;
                    }

                    var contextFactory = GetContextFactory();
                    var clientSettingsContext = contextFactory.GetClientSettingsContext();
                    var userDataAccess = new UserDataAccess(clientSettingsContext, StaticSettings.gGlobalSettings);

                    tUser = userDataAccess.GetUserByID(modReplaceWorkFiles.Statics.gintSecurityID);
                }

                modReplaceWorkFiles.Statics.gintSecurityID = tUser.Id;

                rsLogin.OpenRecordset("select * from globalvariables", "SystemSettings");

                modReplaceWorkFiles.EntryFlagFile(true, "GN", true);
                StaticSettings.gGlobalSettings.MasterPath = tSet.MasterDirectory;
                StaticSettings.gGlobalSettings.ApplicationPath = tSet.ApplicationDirectory;
                StaticSettings.gGlobalSettings.LocalDataDirectory = tSet.LocalDataDirectory;
                StaticSettings.gGlobalSettings.GlobalDataDirectory = tSet.GlobalDataDirectory;
                StaticSettings.gGlobalSettings.BackupPath = tSet.BackupDirectory;  //Application.StartupPath + "\\backups";
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "SecurityID", FCConvert.ToString(tUser.Id));
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Data Drive", Strings.Left(FCFileSystem.Statics.UserDataFolder, 1) + ":\\");
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Data Directory", FCFileSystem.Statics.UserDataFolder);

                if (File.Exists("townseal.pic"))
                {
                    modGlobalConstants.Statics.gstrTownSealPath = FCFileSystem.Statics.UserDataFolder;

                    if (Strings.Right(modGlobalConstants.Statics.gstrTownSealPath, 1) != "\\")
                    {
                        modGlobalConstants.Statics.gstrTownSealPath += "\\";
                    }

                    modGlobalConstants.Statics.gstrTownSealPath += "TownSeal.pic";
                }
                else
                {
                    modGlobalConstants.Statics.gstrTownSealPath = "";
                }

                modGlobalConstants.Statics.gstrEntryFlag = "";
                modReplaceWorkFiles.EntryFlagFile(false, "", true);

                if (((modGlobalConstants.Statics.gstrEntryFlag == string.Empty) || (Convert.ToByte(Strings.Left(modGlobalConstants.Statics.gstrEntryFlag + " ", 1)[0]) == 0)) || Strings.LCase(modGlobalConstants.Statics.gstrEntryFlag) == "gn")
                {
                    if (!sysSet.CheckVersion())
                    {
                        MessageBox.Show("Database check/update failed.  ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        Application.Exit();
                    }

                    cCentralParties cPart = new cCentralParties();

                    if (!cPart.CheckVersion())
                    {
                        MessageBox.Show("Database check/update failed. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }

                    cCentralDataDB cData = new cCentralDataDB();

                    if (!cData.CheckVersion())
                    {
	                    MessageBox.Show("Database check/update failed. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }

                cCentralDocumentDB cdDB = new cCentralDocumentDB();
                cdDB.CheckVersion();

                

                clsDRWrapper rsCreateTable = new clsDRWrapper();

                try
                {
                    rsCreateTable.OpenRecordset("Select * from GlobalVariables", "SystemSettings");

                    if (!rsCreateTable.EndOfFile())
                    {
                        Statics.CityTown = Strings.Trim(FCConvert.ToString(rsCreateTable.Get_Fields_String("CityTown")));
                        rsCreateTable.Edit();
                        rsCreateTable.Set_Fields("ReleaseNumber", "W1706A");

                        rsCreateTable.Update();
                    }
                }
                finally
                {
                    rsCreateTable.DisposeOf();
                }

                modReplaceWorkFiles.GetGlobalVariables();
                Statics.MuninameToShow = modGlobalConstants.Statics.MuniName;

                if (!CheckSystemDate())
                {
                    //FC:FINAL:AM:#1882 - don't exit the application
                    //Application.Exit();
                    return StartupResponse.Logout;
                }

                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "NetworkFlag", "Y");
                modReplaceWorkFiles.Statics.gstrNetworkFlag = (modRegistry.GetRegistryKey("NetworkFlag") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "Y");
               
                modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;
                modReplaceWorkFiles.Statics.rsVariables = null;

                if (!SQLCheckProgramDates())
                {
                    MessageBox.Show("Error 4 has been Encountered.  Please call TRIO Immediately!", "Error 4", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    MDIParent.InstancePtr.UpdateDatesOneMonth(true);

                    return StartupResponse.Logout;
                }

                /* On Error GoTo ErrorTag */
                if (!modCheckDigits.CheckTownData())
                {
                    MessageBox.Show("Error 5 has been Encountered.  Please call TRIO Immediately!", "Error 5", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                    return StartupResponse.Logout;
                }

                // Pop up a reminder to run EOY processes in CR, PY, and BD
                modGlobalFunctions.UpdateUsedModules();

                if (modEntryMenuForm.Statics.gboolShowMessage)
                {
                    if (modGlobalConstants.Statics.gboolBD)
                    {
                        rsDateCheck.OpenRecordset("SELECT * FROM Budgetary", "TWBD0000.vb1");

                        if (rsDateCheck.EndOfFile() != true && rsDateCheck.BeginningOfFile() != true)
                        {
                            if (DateTime.Today.Month == FCConvert.ToInt32(rsDateCheck.Get_Fields_String("FiscalStart")) - 1 || (DateTime.Today.Month == 12 && FCConvert.ToInt32(rsDateCheck.Get_Fields_String("FiscalStart")) == 1))
                            {
                                if (DateTime.Today.Day >= 15)
                                {
                                    MessageBox.Show("You are nearing the end of your fiscal year.  Please be sure to run all the Fiscal EOY Processes in any modules that contain them before your new year starts.", "EOY Process Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                                if (DateTime.Today.Month == 12)
                            {
                                if (modGlobalConstants.Statics.gboolCR || modGlobalConstants.Statics.gboolBD || modGlobalConstants.Statics.gboolPY)
                                {
                                    if (DateTime.Today.Day >= 15)
                                    {
                                        MessageBox.Show("You are nearing the end of your calendar year.  Please be sure to run all the Calendar EOY Processes in any modules that contain them before your new year starts.", "EOY Process Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (DateTime.Today.Month == 12)
                            {
                                if (modGlobalConstants.Statics.gboolCR || modGlobalConstants.Statics.gboolBD || modGlobalConstants.Statics.gboolPY)
                                {
                                    if (DateTime.Today.Day >= 15)
                                    {
                                        MessageBox.Show("You are nearing the end of your calendar year.  Please be sure to run all the Calendar EOY Processes in any modules that contain them before your new year starts.", "EOY Process Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (DateTime.Today.Month == 12)
                        {
                            if (modGlobalConstants.Statics.gboolCR || modGlobalConstants.Statics.gboolPY)
                            {
                                if (DateTime.Today.Day >= 15)
                                {
                                    MessageBox.Show("You are nearing the end of your calendar year.  Please be sure to run all the Calendar EOY Processes in any modules that contain them before your new year starts.", "EOY Process Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                    }
                }

                return StartupResponse.StartNormally;

                //if (!((modGlobalConstants.Statics.gstrEntryFlag == string.Empty) || (Convert.ToByte(Strings.Left(modGlobalConstants.Statics.gstrEntryFlag + " ", 1)[0]) == 0)))
                //{
                //    modEntryMenuForm.EMMain();

                //    return true;
                //}

                //Statics.CityTown = Strings.Trim(Statics.CityTown);

                //if (Strings.UCase(Statics.CityTown) == "TOWN" || Strings.UCase(Statics.CityTown) == "CITY")
                //{
                //    Statics.strCityTown = Statics.CityTown + " of " + Statics.MuninameToShow;
                //}
                //else
                //{
                //    Statics.strCityTown = Statics.MuninameToShow;
                //}

                //modGlobalConstants.Statics.gstrCityTown = Strings.Trim(Statics.CityTown);

                //modGNBas.Statics.FormLoading = true;

                //MDIParent.InstancePtr.Init();
                //modGNBas.Statics.FormLoading = false;

                //if (Statics.gboolUseRegistryFromDB)
                //{
                //    RegistryEntriesFromDB();
                //}

                //return true;
            }
            catch (Exception ex)
            {
                var telem = StaticSettings.GlobalTelemetryService;
                if (telem != null)
                {
                    telem.TrackException(ex);

                }

                MessageBox.Show("Error number "
                                + FCConvert.ToString(Information.Err(ex)
                                    .Number)
                                + "  "
                                + Information.Err(ex)
                                    .Description
                                + " has been encountered in function Main in line "
                                + Information.Erl(), "TRIO Software Corporation", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                //Application.Exit();
                return StartupResponse.Logout;
            }
            finally
            {
                rs.DisposeOf();
                rsLogin.DisposeOf();
                rsDateCheck.DisposeOf();
            }

        }

        public static void RegistryEntriesFromDB()
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            string strSQL;
            int lngID;
            lngID = modGNBas.Statics.clsGESecurity.Get_UserID();
            strSQL = "select * from RegistryEntries where userid = " + FCConvert.ToString(lngID);
            rsTemp.OpenRecordset(strSQL, "SystemSettings");
            string strKeyValue = "";
            int intTemp = 0;
            string strPrinterName = "";
            string strBasePRinterName = "";

            try
            {
                while (!rsTemp.EndOfFile())
                {
                    var keyName = rsTemp.Get_Fields_String("KeyName");
                    var keyValue = rsTemp.Get_Fields_String("KeyValue");

                    if (Strings.LCase(modGlobalConstants.Statics.MuniName) != "hancock county")
                    {
                        modRegistry.SaveRegistryKey(keyName, keyValue, modGlobalConstants.REGISTRYKEY + rsTemp.Get_Fields_String("SubPath"));
                    }
                    else
                    {
                        strKeyValue = keyValue;
                        if (Strings.InStr(1, keyName, "printer", CompareConstants.vbTextCompare) > 0)
                        {
                            intTemp = Strings.InStr(1, strKeyValue, "session", CompareConstants.vbTextCompare);
                            if (intTemp > 0)
                            {
                                strBasePRinterName = Strings.Trim(Strings.Mid(strKeyValue, 1, intTemp - 1));
                                // loop through printers
                                foreach (FCPrinter tPrinter in FCGlobal.Printers)
                                {
                                    strPrinterName = tPrinter.DeviceName;
                                    intTemp = Strings.InStr(1, strPrinterName, "session", CompareConstants.vbTextCompare);
                                    if (intTemp > 0)
                                    {
                                        if (Strings.LCase(strBasePRinterName) == Strings.LCase(Strings.Trim(Strings.Mid(strPrinterName, 1, intTemp - 1))))
                                        {
                                            strKeyValue = strPrinterName;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        modRegistry.SaveRegistryKey(keyName, strKeyValue, modGlobalConstants.REGISTRYKEY + rsTemp.Get_Fields_String("SubPath"));
                    }
                    rsTemp.MoveNext();
                }
            }
            finally
            {
                rsTemp.DisposeOf();
            }
        }

        public static string GetDBRegistryEntry(string strKeyName, string strSubPath, int lngUserID)
        {
            string GetDBRegistryEntry = "";
            string strReturn;
            strReturn = "";
            clsDRWrapper rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset("select * from RegistryEntries where userid = " + FCConvert.ToString(lngUserID) + " and Keyname = '" + strKeyName + "' and SubPath = '" + strSubPath + "'", "SystemSettings");
            if (!rsTemp.EndOfFile())
            {
                strReturn = FCConvert.ToString(rsTemp.Get_Fields_String("KeyValue"));
            }
            rsTemp.DisposeOf();
            GetDBRegistryEntry = strReturn;
            return GetDBRegistryEntry;
        }

        public static void SaveDBRegistryEntry(string strKeyName, string strKeyValue, string strSubPath, int lngUserID)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();

            try
            {
                rsTemp.OpenRecordset("select * from RegistryEntries where userid = " + FCConvert.ToString(lngUserID) + " and Keyname = '" + strKeyName + "' and SubPath = '" + strSubPath + "'", "SystemSettings");
                if (!rsTemp.EndOfFile())
                {
                    rsTemp.Edit();
                }
                else
                {
                    rsTemp.AddNew();
                    rsTemp.Set_Fields("KeyName", strKeyName);
                    rsTemp.Set_Fields("SubPath", strSubPath);
                    rsTemp.Set_Fields("UserID", lngUserID);
                }
                rsTemp.Set_Fields("KeyValue", strKeyValue);
                rsTemp.Update();
            }
            finally
            {
                rsTemp.DisposeOf();
            }
        }

        public static bool SQLCheckProgramDates()
        {
            bool SQLCheckProgramDates = false;
            int DateSum = 0;
            clsDRWrapper rs = new clsDRWrapper();

            try
            {
                rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
                cSubscriptionSQLController ssc = new cSubscriptionSQLController();
                cSubscriptionInfo sInfo;
                sInfo = ssc.LoadSubscriptions("", rs.MegaGroup);
                if (sInfo != null)
                {
                    DateSum = ssc.CalculateCheckDigits(ref sInfo);
                }
                else
                {
                    DateSum = 0;
                }
                if (DateSum == FCConvert.ToInt32(rs.Get_Fields_String("CheckDigits")))
                {
                    SQLCheckProgramDates = true;
                }
                return SQLCheckProgramDates;
            }
            finally
            {
                rs.DisposeOf();
            }
        }

        public static bool UpdateProgramVersionsFile(bool boolUseTestSite = false)
        {
            bool UpdateProgramVersionsFile = false;
            string downloadfile = "";
            string strFTPSiteAddress = "";
            string strFTPSiteUser = "";
            string strFTPSitePassword = "";
            clsDRWrapper rsDefaults = new clsDRWrapper();

            try
            {
                rsDefaults.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
                if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
                {
                    strFTPSiteAddress = FCConvert.ToString(rsDefaults.Get_Fields_String("FTPSiteAddress"));
                    if (!boolUseTestSite)
                    {
                        strFTPSiteUser = "TRIOUpdatesSQL";
                    }
                    else
                    {
                        strFTPSiteUser = "TRIOUpdatesSQLTest";
                    }
                    // strFTPSitePassword = "trio"
                    // strFTPSitePassword = "3PbtETPp"
                    strFTPSitePassword = "zfbuf9AB";
                }
                // if we have not notified them today then connect to the FTP site
                UpdateProgramVersionsFile = true;
                bool success;
                bool boolRetried;
                boolRetried = false;
            TryConnect:
                ;
                try
                {
                    // On Error GoTo Err_Connect
                    // MDIParent.FTP1.OptionConnectTimeout = 8000
                    // MDIParent.FTP1.Connect strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21
                    //MDIParent.InstancePtr.ChilkatFtp1.UnlockComponent("HARRISFTP_32huCB6cpQnV");
                    MDIParent.InstancePtr.ChilkatFtp1.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
                    MDIParent.InstancePtr.ChilkatFtp1.Passive = true;
                    MDIParent.InstancePtr.ChilkatFtp1.Hostname = strFTPSiteAddress;
                    MDIParent.InstancePtr.ChilkatFtp1.Username = strFTPSiteUser;
                    MDIParent.InstancePtr.ChilkatFtp1.Password = strFTPSitePassword;
                    success = MDIParent.InstancePtr.ChilkatFtp1.Connect();
                    if (success != true)
                    {
                        if (!boolRetried)
                        {
                            boolRetried = true;
                            strFTPSitePassword = "trio";
                            goto TryConnect;
                        }
                        UpdateProgramVersionsFile = false;
                    }
                    // If MDIParent.FTP1.Connected Then
                    if (success)
                    {
                        // set the name of the file to eb downloaded
                        if (Strings.Right(Application.MapPath("\\"), 1) == "\\")
                        {
                            downloadfile = Application.MapPath("\\") + "ProgramVersions.csv";
                        }
                        else
                        {
                            downloadfile = Application.MapPath("\\") + "\\ProgramVersions.csv";
                        }
                        if (FCFileSystem.Dir(downloadfile, 0) != "")
                        {
                            FCFileSystem.Kill(downloadfile);
                        }
                        success = MDIParent.InstancePtr.ChilkatFtp1.GetFile("ProgramVersions.csv", downloadfile);
                        UpdateProgramVersionsFile = success == true;
                        // download the ProgramVersions file
                        // If Right(CurDir, 1) = "\" Then
                        // MDIParent.FTP1.GetFile "ProgramVersions.csv", CurDir, "ProgramVersions.csv"
                        // 
                        // Else
                        // MDIParent.FTP1.GetFile "ProgramVersions.csv", CurDir & "\", "ProgramVersions.csv"
                        // End If
                        // UpdateProgramVersionsFile = True
                    }
                    return UpdateProgramVersionsFile;
                }
                catch (Exception ex)
                {
                    // Err_Connect:
                    if (Strings.Left(Information.Err(ex).Description + "     ", 5) == "12014" && strFTPSitePassword != "trio")
                    {
                        strFTPSitePassword = "trio";
                        goto TryConnect;
                    }
                    UpdateProgramVersionsFile = false;
                }
                return UpdateProgramVersionsFile;
            }
            finally
            {
                rsDefaults.DisposeOf();
            }
        }

        public static void WaitForTerm(ref int pID)
        {
            int pHND;
            int pCheck = 0;
            pHND = OpenProcess(SYNCHRONIZE, 0, pID);
            if (pHND != 0)
            {
                foreach (Form ff in Application.OpenForms)
                {
                    ff.Enabled = false;
                }
                // ff
                do
                {
                    pCheck = WaitForSingleObject(pHND, 0);
                    if (pCheck != 0x102)
                        break;
                    //Application.DoEvents();
                }
                while (true);
                foreach (Form ff in Application.OpenForms)
                {
                    ff.Enabled = true;
                }
                // ff
                CloseHandle(pHND);
            }
        }

        public static void SetRegistryNotificationOption()
        {
            if (modRegistry.GetRegistryKey_3("SetRegistryNotificationOption", FCConvert.ToString(false)) == FCConvert.ToString(true))
            {
                return;
            }

            var rsInfo = new clsDRWrapper();

            rsInfo.OpenRecordset("SELECT * FROM GlobalVariables");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                modRegistry.SaveRegistryKey("UpdateNotification", rsInfo.Get_Fields_Boolean("UpdateNotification").ToString());
                modRegistry.SaveRegistryKey("SetRegistryNotificationOption", FCConvert.ToString(true));
            }
            rsInfo.DisposeOf();
        }

        public class StaticVariables
        {
            //=========================================================
            public bool gboolUseRegistryFromDB;
            
            public string CityTown = "";
            public string strCityTown = "";
            public int Wait;
            public string MuninameToShow = string.Empty;
            public int intArrayCounter;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
