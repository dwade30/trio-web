﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptInternalAudit.
	/// </summary>
	public partial class rptInternalAudit : BaseSectionReport
	{
		public rptInternalAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Internal Audit Report";
		}

		public static rptInternalAudit InstancePtr
		{
			get
			{
				return (rptInternalAudit)Sys.GetInstance(typeof(rptInternalAudit));
			}
		}

		protected rptInternalAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInternalAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int intModule;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckNext = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (frmInternalAudit.InstancePtr.chkModule[FCConvert.ToInt16(intModule)].CheckState == Wisej.Web.CheckState.Checked)
				{
					//SetReport();
					eArgs.EOF = false;
				}
				else
				{
					executeCheckNext = true;
					goto CheckNext;
				}
			}
			else
			{
				executeCheckNext = true;
				goto CheckNext;
			}
			CheckNext:
			;
			if (executeCheckNext)
			{
				intModule += 1;
				if (intModule > 9)
				{
					eArgs.EOF = true;
					return;
				}
				if (frmInternalAudit.InstancePtr.chkModule[FCConvert.ToInt16(intModule)].CheckState == Wisej.Web.CheckState.Checked)
				{
					//SetReport();
					eArgs.EOF = false;
				}
				else
				{
					executeCheckNext = true;
					goto CheckNext;
				}
				executeCheckNext = false;
			}
		}

		private void Detail_Format(object sender, System.EventArgs e)
		{
			SetReport();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsAuditInfo = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngAccts = 0;
			string strSQL = "";
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			intModule = 0;
			rsAuditInfo.OpenRecordset("SELECT * FROM AuditInfo WHERE AuditDate = '" + DateTime.Today.ToShortDateString() + "' ORDER BY AuditDate", "CentralData");
			if (rsAuditInfo.EndOfFile() != true)
			{
				this.UserData = rsAuditInfo.Get_Fields_Int32("ID");
			}
			else
			{
				rsAuditInfo.OpenRecordset("SELECT * FROM AuditInfo WHERE AuditDate = '" + DateTime.Today.ToShortDateString() + "' ORDER BY AuditDate", "CentralData");
				if (rsAuditInfo.RecordCount() >= 10)
				{
					rsAuditInfo.Delete();
					rsAuditInfo.MoveNext();
				}
				rsAuditInfo.AddNew();
				// Me.Tag = rsAuditInfo.Fields("ID")
				rsAuditInfo.Set_Fields("AuditDate", DateTime.Today);
				rsAuditInfo.Update();
				this.UserData = rsAuditInfo.Get_Fields_Int32("ID");
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				rsInfo.OpenRecordset("SELECT COUNT(CheckName) as VendorCount FROM VendorMaster", "TWBD0000.vb1");
				if (rsInfo.EndOfFile() != true)
				{
					rsAuditInfo.OpenRecordset("SELECT * FROM AuditInfo WHERE ID = " + rptInternalAudit.InstancePtr.UserData, "CentralData");
					rsAuditInfo.Edit();
					// TODO Get_Fields: Field [VendorCount] not found!! (maybe it is an alias?)
					rsAuditInfo.Set_Fields("BDVendors", FCConvert.ToString(Conversion.Val(rsInfo.Get_Fields("VendorCount"))));
					rsAuditInfo.Update();
				}
				else
				{
					rsAuditInfo.OpenRecordset("SELECT * FROM AuditInfo WHERE ID = " + rptInternalAudit.InstancePtr.UserData, "CentralData");
					rsAuditInfo.Edit();
					rsAuditInfo.Set_Fields("BDVendors", 0);
					rsAuditInfo.Update();
				}
			}
			if (modGlobalConstants.Statics.gboolE9)
			{
				lngAccts = 0;
				strSQL = "Select count(RecordNumber) as numaccts from E911";
				rsAuditInfo.OpenRecordset(strSQL, "twe90000.vb1");
				if (!rsAuditInfo.EndOfFile())
				{
					// TODO Get_Fields: Field [numaccts] not found!! (maybe it is an alias?)
					lngAccts = FCConvert.ToInt32(Math.Round(Conversion.Val(rsAuditInfo.Get_Fields("numaccts"))));
				}
				strSQL = "select * from auditinfo where id = " + FCConvert.ToString(Conversion.Val(rptInternalAudit.InstancePtr.UserData));
				rsAuditInfo.OpenRecordset(strSQL, "CentralData");
				if (!rsAuditInfo.EndOfFile())
				{
					rsAuditInfo.Edit();
				}
				else
				{
					rsAuditInfo.AddNew();
				}
				rsAuditInfo.Set_Fields("e9ACCOUNTS", lngAccts);
				rsAuditInfo.Update();
			}
			rsAuditInfo.Dispose();
			rsInfo.Dispose();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void SetReport()
		{
			switch (intModule)
			{
				case 0:
					{
						subAudit.Report = new srptARAudit();
						break;
					}
				case 1:
					{
						subAudit.Report = new srptBDAudit();
						break;
					}
				case 2:
					{
						subAudit.Report = new srptCRAudit();
						break;
					}
				case 3:
					{
						subAudit.Report = new srptCLAudit();
						break;
					}
				case 4:
					{
						
						break;
					}
				case 5:
					{
						subAudit.Report = new srptFAAudit();
						break;
					}
				case 6:
					{
						subAudit.Report = new srptMVAudit();
						break;
					}
				case 7:
					{
						subAudit.Report = new srptPPAudit();
						break;
					}
				case 8:
					{
						subAudit.Report = new srptREAudit();
						break;
					}
				case 9:
					{
						subAudit.Report = new srptUTAudit();
						break;
					}
			}
			//end switch
		}

		private void rptInternalAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptInternalAudit properties;
			//rptInternalAudit.Caption	= "Internal Audit Report";
			//rptInternalAudit.Icon	= "rptInternalAudit.dsx":0000";
			//rptInternalAudit.Left	= 0;
			//rptInternalAudit.Top	= 0;
			//rptInternalAudit.Width	= 11880;
			//rptInternalAudit.Height	= 8595;
			//rptInternalAudit.StartUpPosition	= 3;
			//rptInternalAudit.SectionData	= "rptInternalAudit.dsx":08CA;
			//End Unmaped Properties
		}
	}
}
