﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Text;
using System.Runtime.InteropServices;

namespace TWGNENTY
{
	public class CFileVersionInfo
	{
		//=========================================================
		// *********************************************************************
		// Copyright (C)1995-98 Karl E. Peterson, All Rights Reserved
		// http://www.mvps.org/vb
		// *********************************************************************
		// Warning: This computer program is protected by copyright law and
		// international treaties. Unauthorized reproduction or distribution
		// of this program, or any portion of it, may result in severe civil
		// and criminal penalties, and will be prosecuted to the maximum
		// extent possible under the law.
		// *********************************************************************
		//
		// API declarations
		//
		[DllImport("kernel32", EntryPoint = "GetFullPathNameA")]
		private static extern int GetFullPathName(IntPtr lpFileName, int nBufferLength, IntPtr lpBuffer, ref int lpFilePart);

		private int GetFullPathNameWrp(ref string lpFileName, int nBufferLength, ref string lpBuffer, ref int lpFilePart)
		{
			int ret;
			IntPtr plpFileName = FCUtils.GetByteFromString(lpFileName);
			IntPtr plpBuffer = FCUtils.GetByteFromString(lpBuffer);
			ret = GetFullPathName(plpFileName, nBufferLength, plpBuffer, ref lpFilePart);
			FCUtils.GetStringFromByte(ref lpFileName, plpFileName);
			FCUtils.GetStringFromByte(ref lpBuffer, plpBuffer);
			return ret;
		}

		[DllImport("Version.dll", EntryPoint = "GetFileVersionInfoA")]
		private static extern int GetFileVersionInfo(IntPtr lptstrFilename, int dwhandle, int dwlen, ref byte lpData);

		private int GetFileVersionInfoWrp(ref string lptstrFilename, short dwhandle, int dwlen, ref byte lpData)
		{
			int ret;
			IntPtr plptstrFilename = FCUtils.GetByteFromString(lptstrFilename);
			ret = GetFileVersionInfo(plptstrFilename, dwhandle, dwlen, ref lpData);
			FCUtils.GetStringFromByte(ref lptstrFilename, plptstrFilename);
			return ret;
		}

		[DllImport("Version.dll", EntryPoint = "GetFileVersionInfoSizeA")]
		private static extern int GetFileVersionInfoSize(IntPtr lptstrFilename, ref int lpdwHandle);

		private int GetFileVersionInfoSizeWrp(ref string lptstrFilename, ref int lpdwHandle)
		{
			int ret;
			IntPtr plptstrFilename = FCUtils.GetByteFromString(lptstrFilename);
			ret = GetFileVersionInfoSize(plptstrFilename, ref lpdwHandle);
			FCUtils.GetStringFromByte(ref lptstrFilename, plptstrFilename);
			return ret;
		}

		[DllImport("Version.dll", EntryPoint = "VerQueryValueA")]
		private static extern int VerQueryValue(ref byte pBlock, string lpSubBlock, ref int lplpBuffer, ref int puLen);

		[DllImport("kernel32", EntryPoint = "VerLanguageNameA")]
		private static extern int VerLanguageName(int wLang, IntPtr szLang, int nSize);

		private int VerLanguageNameWrp(int wLang, ref string szLang, int nSize)
		{
			int ret;
			IntPtr pszLang = FCUtils.GetByteFromString(szLang);
			ret = VerLanguageName(wLang, pszLang, nSize);
			FCUtils.GetStringFromByte(ref szLang, pszLang);
			return ret;
		}

		[DllImport("kernel32", EntryPoint = "GetSystemDirectoryA")]
		private static extern int GetSystemDirectory(string Path, int cbBytes);

		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMem(ref byte Destination, ref int Source, int Length);

		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMem(string Destination, ref int Source, int Length);

		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMem(IntPtr Destination, ref int Source, int Length);

		private void CopyMemWrp(IntPtr Destination, ref int Source, int Length)
		{
			CopyMem(Destination, ref Source, Length);
		}

		[DllImport("kernel32")]
		private static extern int lstrlenA(int lpString);

		[DllImport("kernel32")]
		private static extern int lstrlenW(int lpString);
		//
		// API structures.
		//
		[StructLayout(LayoutKind.Sequential)]
		private struct VS_FIXEDFILEINFO
		{
			public int dwSignature;
			public short dwStrucVersionl;
			// e.g. = &h0000 = 0
			public short dwStrucVersionh;
			// e.g. = &h0042 = .42
			public short dwFileVersionMSl;
			// e.g. = &h0003 = 3
			public short dwFileVersionMSh;
			// e.g. = &h0075 = .75
			public short dwFileVersionLSl;
			// e.g. = &h0000 = 0
			public short dwFileVersionLSh;
			// e.g. = &h0031 = .31
			public short dwProductVersionMSl;
			// e.g. = &h0003 = 3
			public short dwProductVersionMSh;
			// e.g. = &h0010 = .1
			public short dwProductVersionLSl;
			// e.g. = &h0000 = 0
			public short dwProductVersionLSh;
			// e.g. = &h0031 = .31
			public int dwFileFlagsMask;
			// = &h3F for version "0.42"
			public int dwFileFlags;
			// e.g. VFF_DEBUG Or VFF_PRERELEASE
			public int dwFileOS;
			// e.g. VOS_DOS_WINDOWS16
			public int dwFileType;
			// e.g. VFT_DRIVER
			public int dwFileSubtype;
			// e.g. VFT2_DRV_KEYBOARD
			public int dwFileDateMS;
			// e.g. 0
			public int dwFileDateLS;
			// e.g. 0
		};
		//
		// API constants.
		//
		const int MAX_PATH = 260;
		// ----- VS_VERSION.dwFileFlags -----
		const uint VS_FFI_SIGNATURE = 0xFEEF04BD;
		const uint VS_FFI_STRUCVERSION = 0x10000;
		const int VS_FFI_FILEFLAGSMASK = 0x3F;
		// ----- VS_VERSION.dwFileFlags -----
		const int VS_FF_DEBUG = 0x1;
		const int VS_FF_PRERELEASE = 0x2;
		const int VS_FF_PATCHED = 0x4;
		const int VS_FF_PRIVATEBUILD = 0x8;
		const int VS_FF_INFOINFERRED = 0x10;
		const int VS_FF_SPECIALBUILD = 0x20;
		// ----- VS_VERSION.dwFileOS -----
		const int VOS_UNKNOWN = 0x0;
		const uint VOS_DOS = 0x10000;
		const uint VOS_OS216 = 0x20000;
		const uint VOS_OS232 = 0x30000;
		const uint VOS_NT = 0x40000;
		const uint VOS_DOS_WINDOWS16 = 0x10001;
		const uint VOS_DOS_WINDOWS32 = 0x10004;
		const uint VOS_OS216_PM16 = 0x20002;
		const uint VOS_OS232_PM32 = 0x30003;
		const uint VOS_NT_WINDOWS32 = 0x40004;
		// ----- VS_VERSION.dwFileType -----
		const int VFT_UNKNOWN = 0x0;
		const int VFT_APP = 0x1;
		const int VFT_DLL = 0x2;
		const int VFT_DRV = 0x3;
		const int VFT_FONT = 0x4;
		const int VFT_VXD = 0x5;
		const int VFT_STATIC_LIB = 0x7;
		// **** VS_VERSION.dwFileSubtype for VFT_WINDOWS_FONT ****
		const int VFT2_FONT_RASTER = 0x1;
		const int VFT2_FONT_VECTOR = 0x2;
		const int VFT2_FONT_TRUETYPE = 0x3;
		// ----- VS_VERSION.dwFileSubtype for VFT_WINDOWS_DRV -----
		const int VFT2_UNKNOWN = 0x0;
		const int VFT2_DRV_PRINTER = 0x1;
		const int VFT2_DRV_KEYBOARD = 0x2;
		const int VFT2_DRV_LANGUAGE = 0x3;
		const int VFT2_DRV_DISPLAY = 0x4;
		const int VFT2_DRV_MOUSE = 0x5;
		const int VFT2_DRV_NETWORK = 0x6;
		const int VFT2_DRV_SYSTEM = 0x7;
		const int VFT2_DRV_INSTALLABLE = 0x8;
		const int VFT2_DRV_SOUND = 0x9;
		const int VFT2_DRV_COMM = 0xA;
		//
		// Member variables.
		//
		private string m_PathName = "";
		private bool m_Available;
		private string m_StrucVer = "";
		// Structure Version - NOT USED
		private string m_FileVer = "";
		// File Version
		private string m_ProdVer = "";
		// Product Version
		private string m_FileFlags = "";
		// Boolean attributes of file
		private string m_FileOS = "";
		// OS file is designed for
		private string m_FileType = "";
		// Type of file
		private string m_FileSubType = "";
		// Sub-type of file
		private string m_VerLanguage = "";
		private string m_VerCompany = "";
		private string m_VerDescription = "";
		private string m_VerFileVer = "";
		private string m_VerInternalName = "";
		private string m_VerCopyright = "";
		private string m_VerTrademarks = "";
		private string m_VerOrigFilename = "";
		private string m_VerProductName = "";
		private string m_VerProductVer = "";
		// ********************************************
		// Initialize and Terminate
		// ********************************************
		public CFileVersionInfo() : base()
		{
			// 
			// All member variables can be left to defaults.
			// 
		}

		~CFileVersionInfo()
		{
			// 
			// No special cleanup required.
			// 
		}
		// ********************************************
		// Public Properties
		// ********************************************
		public string FullPathName
		{
			set
			{
				string Buffer;
				int nFilePart = 0;
				int nRet;
				// 
				// Retrieve fully qualified path/name specs.
				// 
				Buffer = Strings.Space(MAX_PATH);
				nRet = GetFullPathNameWrp(ref value, Buffer.Length, ref Buffer, ref nFilePart);
				if (FCConvert.ToBoolean(nRet))
				{
					m_PathName = Strings.Left(Buffer, nRet);
					Refresh();
				}
			}
			get
			{
				string FullPathName = "";
				// Returns fully-qualified path/name spec.
				FullPathName = m_PathName;
				return FullPathName;
			}
		}

		public bool Available
		{
			get
			{
				bool Available = false;
				// Returns whether version information is available
				Available = m_Available;
				return Available;
			}
		}
		// ********************************************
		// Standard Version Information
		// ********************************************
		public string FileFlags
		{
			get
			{
				string FileFlags = "";
				FileFlags = m_FileFlags;
				return FileFlags;
			}
		}

		public string FileOS
		{
			get
			{
				string FileOS = "";
				FileOS = m_FileOS;
				return FileOS;
			}
		}

		public string FileType
		{
			get
			{
				string FileType = "";
				FileType = m_FileType;
				return FileType;
			}
		}

		public string FileSubType
		{
			get
			{
				string FileSubType = "";
				FileSubType = m_FileSubType;
				return FileSubType;
			}
		}

		public string VerFile
		{
			get
			{
				string VerFile = "";
				VerFile = m_FileVer;
				return VerFile;
			}
		}

		public string VerProduct
		{
			get
			{
				string VerProduct = "";
				VerProduct = m_ProdVer;
				return VerProduct;
			}
		}

		public string VerStructure
		{
			get
			{
				string VerStructure = "";
				VerStructure = m_StrucVer;
				return VerStructure;
			}
		}
		// ********************************************
		// Better Version Information
		// ********************************************
		public string CompanyName
		{
			get
			{
				string CompanyName = "";
				CompanyName = m_VerCompany;
				return CompanyName;
			}
		}

		public string FileDescription
		{
			get
			{
				string FileDescription = "";
				FileDescription = m_VerDescription;
				return FileDescription;
			}
		}

		public string FileVersion
		{
			get
			{
				string FileVersion = "";
				FileVersion = m_VerFileVer;
				return FileVersion;
			}
		}

		public string InternalName
		{
			get
			{
				string InternalName = "";
				InternalName = m_VerInternalName;
				return InternalName;
			}
		}

		public string Language
		{
			get
			{
				string Language = "";
				Language = m_VerLanguage;
				return Language;
			}
		}

		public string LegalCopyright
		{
			get
			{
				string LegalCopyright = "";
				LegalCopyright = m_VerCopyright;
				return LegalCopyright;
			}
		}

		public string LegalTrademarks
		{
			get
			{
				string LegalTrademarks = "";
				LegalTrademarks = m_VerTrademarks;
				return LegalTrademarks;
			}
		}

		public string OriginalFilename
		{
			get
			{
				string OriginalFilename = "";
				OriginalFilename = m_VerOrigFilename;
				return OriginalFilename;
			}
		}

		public string ProductName
		{
			get
			{
				string ProductName = "";
				ProductName = m_VerProductName;
				return ProductName;
			}
		}

		public string ProductVersion
		{
			get
			{
				string ProductVersion = "";
				ProductVersion = m_VerProductVer;
				return ProductVersion;
			}
		}
		// ********************************************
		// Public Methods
		// ********************************************
		public void Refresh()
		{
			int nDummy = 0;
			int nRet = 0;
			byte[] sBuffer = null;
			int lBufferLen;
			int lplpBuffer = 0;
			VS_FIXEDFILEINFO udtVerBuffer = new VS_FIXEDFILEINFO();
			int puLen = 0;
			string sBlock = "";
			string sTemp = "";
			// 
			// Get size
			// 
			lBufferLen = GetFileVersionInfoSizeWrp(ref m_PathName, ref nDummy);
			if (FCConvert.ToBoolean(lBufferLen))
			{
				m_Available = true;
			}
			else
			{
				m_Available = false;
				return;
			}
			// 
			// Store info to udtVerBuffer struct
			// 
			sBuffer = new byte[lBufferLen + 1];
			GetFileVersionInfoWrp(ref m_PathName, 0, lBufferLen, ref sBuffer[0]);
			VerQueryValue(ref sBuffer[0], "\\", ref lplpBuffer, ref puLen);
			//FC:TODO:AM
			//CopyMemWrp(udtVerBuffer, ref lplpBuffer, Marshal.SizeOf(udtVerBuffer));
			// 
			// Determine Structure Version number - NOT USED
			// 
			m_StrucVer = FCConvert.ToString(udtVerBuffer.dwStrucVersionh) + "." + FCConvert.ToString(udtVerBuffer.dwStrucVersionl);
			// 
			// Determine File Version number
			// 
			m_FileVer = FCConvert.ToString(udtVerBuffer.dwFileVersionMSh) + "." + Strings.Format(udtVerBuffer.dwFileVersionMSl, "00") + ".";
			if (udtVerBuffer.dwFileVersionLSh > 0)
			{
				m_FileVer += Strings.Format(udtVerBuffer.dwFileVersionLSh, "00") + Strings.Format(udtVerBuffer.dwFileVersionLSl, "00");
			}
			else
			{
				m_FileVer += Strings.Format(udtVerBuffer.dwFileVersionLSl, "0000");
			}
			// 
			// Determine Product Version number
			// 
			m_ProdVer = FCConvert.ToString(udtVerBuffer.dwProductVersionMSh) + "." + Strings.Format(udtVerBuffer.dwProductVersionMSl, "00") + ".";
			if (udtVerBuffer.dwProductVersionLSh > 0)
			{
				m_ProdVer += Strings.Format(udtVerBuffer.dwProductVersionLSh, "00") + Strings.Format(udtVerBuffer.dwProductVersionLSl, "00");
			}
			else
			{
				m_ProdVer += Strings.Format(udtVerBuffer.dwProductVersionLSl, "0000");
			}
			// 
			// Determine Boolean attributes of File
			// 
			m_FileFlags = "";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VS_FF_DEBUG))
				m_FileFlags = "Debug ";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VS_FF_PRERELEASE))
				m_FileFlags += "PreRel ";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VS_FF_PATCHED))
				m_FileFlags += "Patched ";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VS_FF_PRIVATEBUILD))
				m_FileFlags += "Private ";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VS_FF_INFOINFERRED))
				m_FileFlags += "Info ";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VS_FF_SPECIALBUILD))
				m_FileFlags += "Special ";
			if (FCConvert.ToBoolean(udtVerBuffer.dwFileFlags & VFT2_UNKNOWN))
				m_FileFlags += "Unknown ";
			m_FileFlags = Strings.Trim(m_FileFlags);
			// 
			// Determine OS for which file was designed
			// 
			switch ((uint)udtVerBuffer.dwFileOS)
			{
				case VOS_DOS_WINDOWS16:
					{
						m_FileOS = "DOS-Win16";
						break;
					}
				case VOS_DOS_WINDOWS32:
					{
						m_FileOS = "DOS-Win32";
						break;
					}
				case VOS_OS216_PM16:
					{
						m_FileOS = "OS/2-16 PM-16";
						break;
					}
				case VOS_OS232_PM32:
					{
						m_FileOS = "OS/2-16 PM-32";
						break;
					}
				case VOS_NT_WINDOWS32:
					{
						m_FileOS = "NT-Win32";
						break;
					}
				default:
					{
						m_FileOS = "Unknown";
						break;
					}
			}
			//end switch
			// 
			// Determine type of file
			// 
			switch (udtVerBuffer.dwFileType)
			{
				case VFT_APP:
					{
						m_FileType = "Application";
						break;
					}
				case VFT_DLL:
					{
						m_FileType = "DLL";
						break;
					}
				case VFT_DRV:
					{
						m_FileType = "Driver";
						switch (udtVerBuffer.dwFileSubtype)
						{
							case VFT2_DRV_PRINTER:
								{
									m_FileSubType = "Printer drv";
									break;
								}
							case VFT2_DRV_KEYBOARD:
								{
									m_FileSubType = "Keyboard drv";
									break;
								}
							case VFT2_DRV_LANGUAGE:
								{
									m_FileSubType = "Language drv";
									break;
								}
							case VFT2_DRV_DISPLAY:
								{
									m_FileSubType = "Display drv";
									break;
								}
							case VFT2_DRV_MOUSE:
								{
									m_FileSubType = "Mouse drv";
									break;
								}
							case VFT2_DRV_NETWORK:
								{
									m_FileSubType = "Network drv";
									break;
								}
							case VFT2_DRV_SYSTEM:
								{
									m_FileSubType = "System drv";
									break;
								}
							case VFT2_DRV_INSTALLABLE:
								{
									m_FileSubType = "Installable";
									break;
								}
							case VFT2_DRV_SOUND:
								{
									m_FileSubType = "Sound drv";
									break;
								}
							case VFT2_DRV_COMM:
								{
									m_FileSubType = "Comm drv";
									break;
								}
							case VFT2_UNKNOWN:
								{
									m_FileSubType = "Unknown";
									break;
								}
						}
						//end switch
						break;
					}
				case VFT_FONT:
					{
						m_FileType = "Font";
						switch (udtVerBuffer.dwFileSubtype)
						{
							case VFT2_FONT_RASTER:
								{
									m_FileSubType = "Raster Font";
									break;
								}
							case VFT2_FONT_VECTOR:
								{
									m_FileSubType = "Vector Font";
									break;
								}
							case VFT2_FONT_TRUETYPE:
								{
									m_FileSubType = "TrueType Font";
									break;
								}
						}
						//end switch
						break;
					}
				case VFT_VXD:
					{
						m_FileType = "VxD";
						break;
					}
				case VFT_STATIC_LIB:
					{
						m_FileType = "Lib";
						break;
					}
				default:
					{
						m_FileType = "Unknown";
						break;
					}
			}
			//end switch
			// 
			// Get language translations
			// 
			if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], "\\VarFileInfo\\Translation", ref lplpBuffer, ref puLen)))
			{
				if (FCConvert.ToBoolean(puLen))
				{
					sTemp = PointerToStringB(ref lplpBuffer, ref puLen);
					sTemp = Strings.Right("0" + Convert.ToString(FCConvert.ToInt32(Convert.ToByte(Strings.Mid(sTemp, 2, 1)[0])), 16).ToUpper(), 2) + Strings.Right("0" + Convert.ToString(FCConvert.ToInt32(Convert.ToByte(Strings.Mid(sTemp, 1, 1)[0])), 16).ToUpper(), 2) + Strings.Right("0" + Convert.ToString(FCConvert.ToInt32(Convert.ToByte(Strings.Mid(sTemp, 4, 1)[0])), 16).ToUpper(), 2) + Strings.Right("0" + Convert.ToString(FCConvert.ToInt32(Convert.ToByte(Strings.Mid(sTemp, 3, 1)[0])), 16).ToUpper(), 2);
					sBlock = "\\StringFileInfo\\" + sTemp + "\\";
					// 
					// Determine language
					// 
					m_VerLanguage = Strings.Space(256);
					//nRet = VerLanguageNameWrp(FCConvert.ToInt32(FCConvert.ToDouble("&H"+Strings.Left(sTemp, 4))), ref m_VerLanguage, m_VerLanguage.Length);
					nRet = VerLanguageNameWrp(Int32.Parse(Strings.Left(sTemp, 4)), ref m_VerLanguage, m_VerLanguage.Length);
					if (FCConvert.ToBoolean(nRet))
					{
						m_VerLanguage = Strings.Left(m_VerLanguage, nRet);
					}
					else
					{
						m_VerLanguage = "";
					}
					// 
					// Get predefined version resources
					// 
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "CompanyName", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerCompany = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "FileDescription", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerDescription = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "FileVersion", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerFileVer = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "InternalName", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerInternalName = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "LegalCopyright", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerCopyright = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "LegalTrademarks", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerTrademarks = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "OriginalFilename", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerOrigFilename = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "ProductName", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerProductName = PointerToString(ref lplpBuffer);
						}
					}
					if (FCConvert.ToBoolean(VerQueryValue(ref sBuffer[0], sBlock + "ProductVersion", ref lplpBuffer, ref puLen)))
					{
						if (FCConvert.ToBoolean(puLen))
						{
							m_VerProductVer = PointerToString(ref lplpBuffer);
						}
					}
				}
			}
		}
		// ********************************************
		// Private Methods
		// ********************************************
		// vbPorter upgrade warning: 'Return' As string	OnWrite(byte())
		private string PointerToStringW(ref int lpStringW)
		{
			string PointerToStringW = "";
			byte[] Buffer = null;
			int nLen = 0;
			if (FCConvert.ToBoolean(lpStringW))
			{
				nLen = lstrlenW(lpStringW) * 2;
				if (FCConvert.ToBoolean(nLen))
				{
					Buffer = new byte[(nLen - 1) + 1];
					CopyMem(ref Buffer[0], ref lpStringW, nLen);
					PointerToStringW = Encoding.UTF8.GetString(Buffer);
				}
			}
			return PointerToStringW;
		}

		private string PointerToString(ref int lpString)
		{
			string PointerToString = "";
			string Buffer = "";
			int nLen = 0;
			if (FCConvert.ToBoolean(lpString))
			{
				nLen = lstrlenA(lpString);
				if (FCConvert.ToBoolean(nLen))
				{
					Buffer = Strings.Space(nLen);
					CopyMem(Buffer, ref lpString, nLen);
					PointerToString = Buffer;
				}
			}
			return PointerToString;
		}

		private string PointerToStringB(ref int lpString, ref int nBytes)
		{
			string PointerToStringB = "";
			string Buffer = "";
			if (FCConvert.ToBoolean(nBytes))
			{
				Buffer = Strings.Space(nBytes);
				CopyMem(Buffer, ref lpString, nBytes);
				PointerToStringB = Buffer;
			}
			return PointerToStringB;
		}
	}
}
