﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptPPAudit.
	/// </summary>
	partial class srptPPAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptPPAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccounts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOverride = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.txtAccounts,
				this.txtCat1,
				this.txtCat2,
				this.txtCat3,
				this.txtCat4,
				this.txtCat5,
				this.txtCat6,
				this.txtCat7,
				this.txtCat8,
				this.Label15,
				this.txtTotal,
				this.Label16,
				this.txtExempt,
				this.Label17,
				this.txtNet,
				this.Label18,
				this.txtOverride,
				this.txtCat9,
				this.lblCat1,
				this.lblCat2,
				this.lblCat3,
				this.lblCat4,
				this.lblCat5,
				this.lblCat6,
				this.lblCat7,
				this.lblCat8,
				this.lblCat9,
				this.Line1
			});
			this.Detail.Height = 5.59375F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1
			});
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Personal Property";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Accounts";
			this.Label2.Top = 4.625F;
			this.Label2.Width = 0.75F;
			// 
			// txtAccounts
			// 
			this.txtAccounts.Height = 0.1875F;
			this.txtAccounts.Left = 3.875F;
			this.txtAccounts.Name = "txtAccounts";
			this.txtAccounts.Style = "text-align: right";
			this.txtAccounts.Text = "0";
			this.txtAccounts.Top = 4.625F;
			this.txtAccounts.Width = 0.6875F;
			// 
			// txtCat1
			// 
			this.txtCat1.Height = 0.1875F;
			this.txtCat1.Left = 2.75F;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "text-align: right";
			this.txtCat1.Text = "0";
			this.txtCat1.Top = 0.25F;
			this.txtCat1.Width = 1.8125F;
			// 
			// txtCat2
			// 
			this.txtCat2.Height = 0.1875F;
			this.txtCat2.Left = 2.75F;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "text-align: right";
			this.txtCat2.Text = "0";
			this.txtCat2.Top = 0.6875F;
			this.txtCat2.Width = 1.8125F;
			// 
			// txtCat3
			// 
			this.txtCat3.Height = 0.1875F;
			this.txtCat3.Left = 2.75F;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Style = "text-align: right";
			this.txtCat3.Text = "0";
			this.txtCat3.Top = 1.125F;
			this.txtCat3.Width = 1.8125F;
			// 
			// txtCat4
			// 
			this.txtCat4.Height = 0.1875F;
			this.txtCat4.Left = 2.75F;
			this.txtCat4.Name = "txtCat4";
			this.txtCat4.Style = "text-align: right";
			this.txtCat4.Text = "0";
			this.txtCat4.Top = 1.5625F;
			this.txtCat4.Width = 1.8125F;
			// 
			// txtCat5
			// 
			this.txtCat5.Height = 0.1875F;
			this.txtCat5.Left = 2.75F;
			this.txtCat5.Name = "txtCat5";
			this.txtCat5.Style = "text-align: right";
			this.txtCat5.Text = "0";
			this.txtCat5.Top = 2F;
			this.txtCat5.Width = 1.8125F;
			// 
			// txtCat6
			// 
			this.txtCat6.Height = 0.1875F;
			this.txtCat6.Left = 2.75F;
			this.txtCat6.Name = "txtCat6";
			this.txtCat6.Style = "text-align: right";
			this.txtCat6.Text = "0";
			this.txtCat6.Top = 2.4375F;
			this.txtCat6.Width = 1.8125F;
			// 
			// txtCat7
			// 
			this.txtCat7.Height = 0.1875F;
			this.txtCat7.Left = 2.75F;
			this.txtCat7.Name = "txtCat7";
			this.txtCat7.Style = "text-align: right";
			this.txtCat7.Text = "0";
			this.txtCat7.Top = 2.875F;
			this.txtCat7.Width = 1.8125F;
			// 
			// txtCat8
			// 
			this.txtCat8.Height = 0.1875F;
			this.txtCat8.Left = 2.75F;
			this.txtCat8.Name = "txtCat8";
			this.txtCat8.Style = "text-align: right";
			this.txtCat8.Text = "0";
			this.txtCat8.Top = 3.3125F;
			this.txtCat8.Width = 1.8125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.5625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold";
			this.Label15.Text = "Total";
			this.Label15.Top = 4.875F;
			this.Label15.Width = 0.5625F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 3.125F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "text-align: right";
			this.txtTotal.Text = "0";
			this.txtTotal.Top = 4.875F;
			this.txtTotal.Width = 1.4375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.5625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold";
			this.Label16.Text = "Exempt";
			this.Label16.Top = 5.125F;
			this.Label16.Width = 0.625F;
			// 
			// txtExempt
			// 
			this.txtExempt.Height = 0.1875F;
			this.txtExempt.Left = 3.3125F;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.Style = "text-align: right";
			this.txtExempt.Text = "0";
			this.txtExempt.Top = 5.125F;
			this.txtExempt.Width = 1.25F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.5625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold";
			this.Label17.Text = "Net";
			this.Label17.Top = 5.375F;
			this.Label17.Width = 0.4375F;
			// 
			// txtNet
			// 
			this.txtNet.Height = 0.1875F;
			this.txtNet.Left = 3.125F;
			this.txtNet.Name = "txtNet";
			this.txtNet.Style = "text-align: right";
			this.txtNet.Text = "0";
			this.txtNet.Top = 5.375F;
			this.txtNet.Width = 1.4375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.75F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: right";
			this.Label18.Text = "Override";
			this.Label18.Top = 4F;
			this.Label18.Width = 1.8125F;
			// 
			// txtOverride
			// 
			this.txtOverride.Height = 0.1875F;
			this.txtOverride.Left = 2.75F;
			this.txtOverride.Name = "txtOverride";
			this.txtOverride.Style = "text-align: right";
			this.txtOverride.Text = "0";
			this.txtOverride.Top = 4.1875F;
			this.txtOverride.Width = 1.8125F;
			// 
			// txtCat9
			// 
			this.txtCat9.Height = 0.1875F;
			this.txtCat9.Left = 2.75F;
			this.txtCat9.Name = "txtCat9";
			this.txtCat9.Style = "text-align: right";
			this.txtCat9.Text = "0";
			this.txtCat9.Top = 3.75F;
			this.txtCat9.Width = 1.8125F;
			// 
			// lblCat1
			// 
			this.lblCat1.Height = 0.1875F;
			this.lblCat1.Left = 2F;
			this.lblCat1.Name = "lblCat1";
			this.lblCat1.Style = "font-weight: bold; text-align: right";
			this.lblCat1.Text = "Category 1";
			this.lblCat1.Top = 0.0625F;
			this.lblCat1.Width = 2.5625F;
			// 
			// lblCat2
			// 
			this.lblCat2.Height = 0.1875F;
			this.lblCat2.Left = 2F;
			this.lblCat2.Name = "lblCat2";
			this.lblCat2.Style = "font-weight: bold; text-align: right";
			this.lblCat2.Text = "Category 2";
			this.lblCat2.Top = 0.5F;
			this.lblCat2.Width = 2.5625F;
			// 
			// lblCat3
			// 
			this.lblCat3.Height = 0.1875F;
			this.lblCat3.Left = 2F;
			this.lblCat3.Name = "lblCat3";
			this.lblCat3.Style = "font-weight: bold; text-align: right";
			this.lblCat3.Text = "Category 3";
			this.lblCat3.Top = 0.9375F;
			this.lblCat3.Width = 2.5625F;
			// 
			// lblCat4
			// 
			this.lblCat4.Height = 0.1875F;
			this.lblCat4.Left = 2F;
			this.lblCat4.Name = "lblCat4";
			this.lblCat4.Style = "font-weight: bold; text-align: right";
			this.lblCat4.Text = "Category 4";
			this.lblCat4.Top = 1.375F;
			this.lblCat4.Width = 2.5625F;
			// 
			// lblCat5
			// 
			this.lblCat5.Height = 0.1875F;
			this.lblCat5.Left = 2F;
			this.lblCat5.Name = "lblCat5";
			this.lblCat5.Style = "font-weight: bold; text-align: right";
			this.lblCat5.Text = "Category 5";
			this.lblCat5.Top = 1.8125F;
			this.lblCat5.Width = 2.5625F;
			// 
			// lblCat6
			// 
			this.lblCat6.Height = 0.1875F;
			this.lblCat6.Left = 2F;
			this.lblCat6.Name = "lblCat6";
			this.lblCat6.Style = "font-weight: bold; text-align: right";
			this.lblCat6.Text = "Category 6";
			this.lblCat6.Top = 2.25F;
			this.lblCat6.Width = 2.5625F;
			// 
			// lblCat7
			// 
			this.lblCat7.Height = 0.1875F;
			this.lblCat7.Left = 2F;
			this.lblCat7.Name = "lblCat7";
			this.lblCat7.Style = "font-weight: bold; text-align: right";
			this.lblCat7.Text = "Category 7";
			this.lblCat7.Top = 2.6875F;
			this.lblCat7.Width = 2.5625F;
			// 
			// lblCat8
			// 
			this.lblCat8.Height = 0.1875F;
			this.lblCat8.Left = 2F;
			this.lblCat8.Name = "lblCat8";
			this.lblCat8.Style = "font-weight: bold; text-align: right";
			this.lblCat8.Text = "Category 8";
			this.lblCat8.Top = 3.125F;
			this.lblCat8.Width = 2.5625F;
			// 
			// lblCat9
			// 
			this.lblCat9.Height = 0.1875F;
			this.lblCat9.Left = 2F;
			this.lblCat9.Name = "lblCat9";
			this.lblCat9.Style = "font-weight: bold; text-align: right";
			this.lblCat9.Text = "Category 9";
			this.lblCat9.Top = 3.5625F;
			this.lblCat9.Width = 2.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.25F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 4.5F;
			this.Line1.Width = 2.75F;
			this.Line1.X1 = 2.25F;
			this.Line1.X2 = 5F;
			this.Line1.Y1 = 4.5F;
			this.Line1.Y2 = 4.5F;
			// 
			// srptPPAudit
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccounts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverride;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCat9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
