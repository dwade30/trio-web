//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	public class cMRU
	{

		//=========================================================

		// ======================================================================================

		// ======================================================================================

		private int m_iMaxCount;
		private int m_iCount;
		private string[] m_sItems = null;

		public int MaxCount
		{
			get
			{
					int MaxCount = 0;
				MaxCount = m_iMaxCount;
				return MaxCount;
			}

			set
			{
				m_iMaxCount = value;
				if (m_iCount>m_iMaxCount) {
					m_iCount = m_iMaxCount;
				}
			}
		}

		public int Count
		{
			get
			{
					int Count = 0;
				Count = m_iCount;
				return Count;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public string Get_Item(int nIndex)
		{
				string Item = "";
			Item = m_sItems[nIndex];
			return Item;
		}
		public void Add(string sItem)
		{
			int i;
			int iIdx = 0;

			for(i=1; i<=m_iCount; i++) {
				if (m_sItems[i]==sItem) {
					iIdx = i;
					break;
				}
			} // i

			if (iIdx>0) {
				// swap from 1 -> iIdx-1
				for(i=iIdx-1; i>=1; i--) {
					m_sItems[i+1] = m_sItems[i];
				} // i
			} else {
				if (m_iCount<m_iMaxCount) {
					m_iCount += 1;
					Array.Resize(ref m_sItems, m_iCount + 1);
				}
				for(i=m_iCount-1; i>=1; i--) {
					m_sItems[i+1] = m_sItems[i];
				} // i
			}
			m_sItems[1] = sItem;

		}
		public bool Serialise(ref cRegistry cR)
		{
			bool Serialise = false;
			int i;
			cR.ValueType = cRegistry.ERegistryValueTypes.REG_DWORD;
			cR.ValueKey = "MaxCount";
			cR.Value = m_iMaxCount;
			cR.ValueKey = "Count";
			cR.ValueType = cRegistry.ERegistryValueTypes.REG_SZ;
			cR.Value = m_iCount;
			for(i=1; i<=m_iCount; i++) {
				cR.ValueKey = "Item"+Convert.ToString(i);
				cR.Value = m_sItems[i];
			} // i
			return Serialise;
		}
		public bool DeSerialise(ref cRegistry cR)
		{
			bool DeSerialise = false;
			int i;
			cR.ValueType = cRegistry.ERegistryValueTypes.REG_DWORD;
			cR.ValueKey = "MaxCount";
			m_iMaxCount = cR.Value;
			if (m_iMaxCount<1) m_iMaxCount = 16;
			cR.ValueKey = "Count";
			m_iCount = cR.Value;
			if (m_iCount>m_iMaxCount) {
				m_iCount = m_iMaxCount;
			}
			if (m_iCount>0) {
				m_sItems = new string[m_iCount + 1];
				cR.ValueType = cRegistry.ERegistryValueTypes.REG_SZ;
				for(i=1; i<=m_iCount; i++) {
					cR.ValueKey = "Item"+Convert.ToString(i);
					m_sItems[i] = cR.Value;
				} // i
			} else {
				m_iCount = 0;
			}
			return DeSerialise;
		}

		public cMRU() : base()
		{
			m_iMaxCount = 16;
		}

	}
}