﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmProgramDates.
	/// </summary>
	partial class frmProgramDates : BaseForm
	{
		public fecherFoundation.FCComboBox cmbModules;
		public fecherFoundation.FCTextBox txtVersionDate;
		public fecherFoundation.FCTextBox txtVersion;
		public fecherFoundation.FCListBox lstPrograms;
		public fecherFoundation.FCFileListBox filPrograms;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblVersion;
		public fecherFoundation.FCLabel lblVersionDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProgramDates));
			this.cmbModules = new fecherFoundation.FCComboBox();
			this.txtVersionDate = new fecherFoundation.FCTextBox();
			this.txtVersion = new fecherFoundation.FCTextBox();
			this.lstPrograms = new fecherFoundation.FCListBox();
			this.columnHeader1 = new Wisej.Web.ColumnHeader();
			this.columnHeader2 = new Wisej.Web.ColumnHeader();
			this.filPrograms = new fecherFoundation.FCFileListBox();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.lblVersion = new fecherFoundation.FCLabel();
			this.lblVersionDate = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 698);
			this.BottomPanel.Controls.Add(this.cmdQuit);
			this.BottomPanel.Size = new System.Drawing.Size(847, 70);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbModules);
			this.ClientArea.Controls.Add(this.lstPrograms);
			this.ClientArea.Controls.Add(this.filPrograms);
			//this.ClientArea.Controls.Add(this.cmdQuit);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(847, 638);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(847, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(237, 30);
			this.HeaderText.Text = "Program Information";
			// 
			// cmbModules
			// 
			this.cmbModules.AutoSize = false;
			this.cmbModules.BackColor = System.Drawing.SystemColors.Window;
			this.cmbModules.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbModules.FormattingEnabled = true;
			this.cmbModules.Location = new System.Drawing.Point(320, 30);
			this.cmbModules.Name = "cmbModules";
			this.cmbModules.Size = new System.Drawing.Size(498, 40);
			this.cmbModules.TabIndex = 1;
			this.cmbModules.SelectedIndexChanged += new System.EventHandler(this.cmbModules_SelectedIndexChanged);
			// 
			// txtVersionDate
			// 
			this.txtVersionDate.AutoSize = false;
			this.txtVersionDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtVersionDate.Enabled = false;
			this.txtVersionDate.LinkItem = null;
			this.txtVersionDate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVersionDate.LinkTopic = null;
			this.txtVersionDate.Location = new System.Drawing.Point(319, 30);
			this.txtVersionDate.Name = "txtVersionDate";
			this.txtVersionDate.Size = new System.Drawing.Size(107, 40);
			this.txtVersionDate.TabIndex = 3;
			this.txtVersionDate.Visible = false;
			// 
			// txtVersion
			// 
			this.txtVersion.AutoSize = false;
			this.txtVersion.BackColor = System.Drawing.SystemColors.Window;
			this.txtVersion.Enabled = false;
			this.txtVersion.LinkItem = null;
			this.txtVersion.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVersion.LinkTopic = null;
			this.txtVersion.Location = new System.Drawing.Point(95, 30);
			this.txtVersion.Name = "txtVersion";
			this.txtVersion.Size = new System.Drawing.Size(84, 40);
			this.txtVersion.TabIndex = 1;
			this.txtVersion.Visible = false;
			// 
			// lstPrograms
			// 
			this.lstPrograms.Appearance = 0;
			this.lstPrograms.BackColor = System.Drawing.SystemColors.Window;
			this.lstPrograms.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
				this.columnHeader1,
				this.columnHeader2
			});
			this.lstPrograms.Location = new System.Drawing.Point(30, 90);
			this.lstPrograms.MultiSelect = 0;
			this.lstPrograms.Name = "lstPrograms";
			this.lstPrograms.Size = new System.Drawing.Size(788, 512);
			this.lstPrograms.Sorted = false;
			this.lstPrograms.TabIndex = 3;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Name = "columnHeader1";
			this.columnHeader1.Text = "columnHeader1";
			this.columnHeader1.Width = 200;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Name = "columnHeader2";
			this.columnHeader2.Text = "columnHeader2";
			this.columnHeader2.Width = 200;
			// 
			// filPrograms
			// 
			this.filPrograms.Location = new System.Drawing.Point(30, 492);
			this.filPrograms.Name = "filPrograms";
			this.filPrograms.Size = new System.Drawing.Size(316, 20);
			this.filPrograms.TabIndex = 6;
			this.filPrograms.Visible = false;
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.ForeColor = System.Drawing.Color.White;
			this.cmdQuit.Location = new System.Drawing.Point(350, 0);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(146, 40);
			this.cmdQuit.TabIndex = 4;
			this.cmdQuit.Text = "Return to Menu";
			this.cmdQuit.Visible = true;
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.lblVersion);
			this.Frame1.Controls.Add(this.txtVersionDate);
			this.Frame1.Controls.Add(this.lblVersionDate);
			this.Frame1.Controls.Add(this.txtVersion);
			this.Frame1.Location = new System.Drawing.Point(190, 400);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(446, 86);
			this.Frame1.TabIndex = 5;
			this.Frame1.Text = "Database";
			this.Frame1.Visible = false;
			// 
			// lblVersion
			// 
			this.lblVersion.Location = new System.Drawing.Point(20, 44);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(79, 18);
			this.lblVersion.TabIndex = 0;
			this.lblVersion.Text = "VERSION #";
			this.lblVersion.Visible = false;
			// 
			// lblVersionDate
			// 
			this.lblVersionDate.Location = new System.Drawing.Point(214, 44);
			this.lblVersionDate.Name = "lblVersionDate";
			this.lblVersionDate.Size = new System.Drawing.Size(108, 18);
			this.lblVersionDate.TabIndex = 2;
			this.lblVersionDate.Text = "VERSION DATE";
			this.lblVersionDate.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(244, 21);
			this.Label2.TabIndex = 0;
			this.Label2.Text = "SELECT MODULE INFORMATION TO VIEW";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 78);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(231, 20);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "LIST OF PROGRAMS AND DATE CREATED";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.Label1.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 0;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// frmProgramDates
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(847, 758);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmProgramDates";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Program Information";
			this.Load += new System.EventHandler(this.frmProgramDates_Load);
			this.Activated += new System.EventHandler(this.frmProgramDates_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProgramDates_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private ColumnHeader columnHeader1;
		private ColumnHeader columnHeader2;
	}
}
