﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Chilkat;
using fecherFoundation;
using Global;
using Newtonsoft.Json;
using Stream = System.IO.Stream;
using Task = Chilkat.Task;

namespace TWGNENTY
{
	public class cTRIOWebAPI
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private string strBaseUrl = string.Empty;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngError, string strMessage)
		{
			strLastError = strMessage;
			lngLastError = lngError;
		}

		public string BaseUrl
		{
			set
			{
				strBaseUrl = value;
			}
			get
			{
				string BaseUrl = "";
				BaseUrl = strBaseUrl;
				return BaseUrl;
			}
		}

		public string GetSubscription(string strMuni)
		{
			string GetSubscription = "";
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cWebAPI sender = new cWebAPI();
				sender.UseSecureHTTP = false;
				sender.URL = strBaseUrl;
				sender.Path = "WebAPI/api/Subscription/" + strMuni;
				string strEncrypted;
				strEncrypted = sender.GetQuickGetString();
				if (strEncrypted != "")
				{
					string strDecrypted = "";
					string strSalt = "";
					cCryptoUtility tCryptUtil = new cCryptoUtility();
					strSalt = tCryptUtil.HashString(Strings.LCase(strMuni));
					strDecrypted = tCryptUtil.DecryptByPassword(strEncrypted, Strings.LCase(strMuni), strSalt);
					GetSubscription = strDecrypted;
				}
				return GetSubscription;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(9999, ex.Message);
			}
			return GetSubscription;
		}

        //public Boolean AuthenticateStaff(string userName, string password)
        //{
        //    try
        //    {
        //        ClearErrors();
        //        AuthenticateAsync(userName, password).ContinueWith((finishedTask) => { },
        //            TaskScheduler.FromCurrentSynchronizationContext());
        //    }
        //    catch (Exception ex)
        //    {
        //        SetError(Information.Err(ex).Number, Information.Err(ex).Description);
        //    }
        //}

        public async Task<Boolean> AuthenticateStaffAsync(string userName, string password)
        {
            //return await TestAsyncTiming();
            var client = new HttpClient();
            client.BaseAddress = new Uri(@"https://" + strBaseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var credentials = new PassedCredentials()
            {
                UserName = userName,
                Password = password
            };
            var content = new StringContent(JsonConvert.SerializeObject(credentials), Encoding.UTF8,
                "application/json");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync("WebAPI/api/Authenticate", content);
            var responseString = await response.Content.ReadAsStringAsync();
            
            return responseString.ToLower() == "true";
        }

        public bool AuthenticateSuperUser(string userName, string password)
        {
			string pass = GetSuperUserPassword();

            return userName.ToLower().Trim() == "harrisuser" && password == pass;
        }

        private static string GetSuperUserPassword()
        {
            string pass = "zzzIII2223812723$#%"; // gibberish that the method can return if you get a null datastream back from the azure function

            var request = WebRequest.Create("https://azurefunctions20200514132729.azurewebsites.net/api/GetPassToCheck?code=V9G6XedrXAFb9xLXVh4O1rcr97asaxjYMieabIOG6a2BT2ucd6xzvw==");
            var response = request.GetResponse();

            using (var dataStream = response.GetResponseStream())
            {
                if (dataStream != null)
                {
                    var reader = new StreamReader(dataStream);
                    pass = reader.ReadToEnd();
					reader.Dispose();
                }
            }

            response.Close();

            return pass;
        }

        public async Task<Boolean> TestAsyncTiming()
        {
            return await System.Threading.Tasks.Task.FromResult<Boolean>(JustReturnTrue());
        }

        private Boolean JustReturnTrue()
        {
            return true;
        }
        public cTRIOWebAPI() : base()
		{
			strBaseUrl = "www.hlgbangor.net";
		}
	}
}
