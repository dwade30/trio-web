//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptProgramNotes.
	/// </summary>
	public partial class rptProgramNotes : GrapeCity.ActiveReports.SectionReport
	{

		public rptProgramNotes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "rptProgramNotes";
		}

		public static rptProgramNotes InstancePtr
		{
			get
			{
				return (rptProgramNotes)Sys.GetInstance(typeof(rptProgramNotes));
			}
		}

		protected rptProgramNotes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

// nObj = 1
//   0	rptProgramNotes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		clsDRWrapper rsModuleInfo = new clsDRWrapper();
		string[] strInfo = null;
		StreamReader tsInfo;
		string strFileInfo = "";
		int counter;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// loop through the file to see if any of the programs we have in TRIOMast do not match the current version on the FTP site
		CheckNext: ;
			if (tsInfo.AtEndOfStream==true) {
				eArgs.EOF = true;
			} else {
				strFileInfo = tsInfo.ReadLine();
				strInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
				if (strInfo[0]=="GN") {
					if (Information.UBound(strInfo, 1)>=2) {
						if (Strings.Trim(strInfo[2])!="") {
							this.Fields("Binder").Value = "General Entry";
							eArgs.EOF = false;
						} else {
							goto CheckNext;
						}
					} else {
						goto CheckNext;
					}
				} else if (strInfo[0]=="Notes") {
					if (Strings.Trim(strInfo[2])!="") {
						this.Fields("Binder").Value = "General Information";
						eArgs.EOF = false;
					} else {
						goto CheckNext;
					}
				} else {
					if (rsModuleInfo.Get_Fields(strInfo[0]) && rsModuleInfo.Get_Fields(strInfo[0]+"Date")>DateTime.Today.ToOADate()) {
						if (Information.UBound(strInfo, 1)>=2) {
							if (Strings.Trim(strInfo[2])!="") {
								// TODO Get_Fields: Field [Binder] not found!! (maybe it is an alias?)
								this.Get_Fields("Binder").Value = CreateModuleDescription(ref strInfo[0]);
								eArgs.EOF = false;
							} else {
								goto CheckNext;
							}
						} else {
							goto CheckNext;
						}
					} else {
						goto CheckNext;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			txtMuniname.Text = modGlobalConstants.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm AMPM");
			rsModuleInfo.OpenRecordset("SELECT * FROM Modules", "systemsettings");
			tsInfo = File.OpenText("ProgramVersions.csv");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtProgramNotes.Text = "";
			txtProgramNotes.Font = new Font( "Tahoma", txtProgramNotes.Font.Size);
			txtProgramNotes.Font = new Font(txtProgramNotes.Font.Name,  9);

			for(counter=2; counter<=Information.UBound(strInfo, 1); counter++) {
				if (Strings.Trim(strInfo[counter])!="") {
					txtProgramNotes.Text = txtProgramNotes.Text+"-- "+Strings.Trim(strInfo[counter])+"\n";
					if (counter<Information.UBound(strInfo, 1)) {
						txtProgramNotes.Text = txtProgramNotes.Text+"\n";
					}
				}
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			object fldModule;	// - "AutoDim"

			fldModule = this.Fields("Binder").Value;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As Variant --> As string
			string txtPage;	// - "AutoDim"

			txtPage = "Page "+this.PageNumber;
		}

		private string CreateModuleString(ref int intRow, ref short intStartRow = 0)
		{
			string CreateModuleString = "";
			switch (intRow) {
				
				case 0+intStartRow:
				{
					CreateModuleString = "AR";
					break;
				}
				case 1+intStartRow:
				{
					CreateModuleString = "RB";
					break;
				}
				case 2+intStartRow:
				{
					CreateModuleString = "BL";
					break;
				}
				case 3+intStartRow:
				{
					CreateModuleString = "BD";
					break;
				}
				case 4+intStartRow:
				{
					CreateModuleString = "CR";
					break;
				}
				case 5+intStartRow:
				{
					CreateModuleString = "CK";
					break;
				}
				case 6+intStartRow:
				{
					CreateModuleString = "CE";
					break;
				}
				case 7+intStartRow:
				{
					CreateModuleString = "CL";
					break;
				}
				case 8+intStartRow:
				{
					CreateModuleString = "E9";
					break;
				}
				case 9+intStartRow:
				{
					CreateModuleString = "FA";
					break;
				}
				case 10+intStartRow:
				{
					CreateModuleString = "GN";
					break;
				}
				case 11+intStartRow:
				{
					CreateModuleString = "HR";
					break;
				}
				case 12+intStartRow:
				{
					CreateModuleString = "IV";
					break;
				}
				case 13+intStartRow:
				{
					CreateModuleString = "RE";
					break;
				}
				case 14+intStartRow:
				{
					CreateModuleString = "REMVR";
					break;
				}
				case 15+intStartRow:
				{
					CreateModuleString = "CM";
					break;
				}
				case 16+intStartRow:
				{
					CreateModuleString = "RH";
					break;
				}
				case 17+intStartRow:
				{
					CreateModuleString = "SK";
					break;
				}
				case 18+intStartRow:
				{
					CreateModuleString = "XF";
					break;
				}
				case 19+intStartRow:
				{
					CreateModuleString = "MV";
					break;
				}
				case 20+intStartRow:
				{
					CreateModuleString = "RR";
					break;
				}
				case 21+intStartRow:
				{
					CreateModuleString = "PP";
					break;
				}
				case 22+intStartRow:
				{
					CreateModuleString = "PY";
					break;
				}
				case 23+intStartRow:
				{
					CreateModuleString = "PYTaxUpdate";
					break;
				}
				case 24+intStartRow:
				{
					CreateModuleString = "TS";
					break;
				}
				case 25+intStartRow:
				{
					CreateModuleString = "UT";
					break;
				}
				case 26+intStartRow:
				{
					CreateModuleString = "VR";
					break;
				}
			} //end switch
			return CreateModuleString;
		}

		private string CreateModuleDescription(ref string strCode)
		{
			string CreateModuleDescription = "";
			
			if (strCode=="AR")
			{
				CreateModuleDescription = "Account Receivable";
			}
			else if (strCode=="RB")
			{
				CreateModuleDescription = "Blue Book";
			}
			else if (strCode=="BL")
			{
				CreateModuleDescription = "Billing";
			}
			else if (strCode=="BD")
			{
				CreateModuleDescription = "Budgetary";
			}
			else if (strCode=="CR")
			{
				CreateModuleDescription = "Cash Receipts";
			}
			else if (strCode=="CK")
			{
				CreateModuleDescription = "Clerk";
			}
			else if (strCode=="CE")
			{
				CreateModuleDescription = "Code Enforcement";
			}
			else if (strCode=="CL")
			{
				CreateModuleDescription = "Collections";
			}
			else if (strCode=="E9")
			{
				CreateModuleDescription = "E911";
			}
			else if (strCode=="FA")
			{
				CreateModuleDescription = "Fixed Assets";
			}
			else if (strCode=="GN")
			{
				CreateModuleDescription = "General Entry";
			}
			else if (strCode=="HR")
			{
				CreateModuleDescription = "Human Resources";
			}
			else if (strCode=="IV")
			{
				CreateModuleDescription = "Inventory";
			}
			else if (strCode=="RE")
			{
				CreateModuleDescription = "Real Estate";
			}
			else if (strCode=="REMVR")
			{
				CreateModuleDescription = "Real Estate MVR Excel File";
			}
			else if (strCode=="RH")
			{
				CreateModuleDescription = "Real Estate Handheld";
			}
			else if (strCode=="SK")
			{
				CreateModuleDescription = "Real Estate Sketching";
			}
			else if (strCode=="XF")
			{
				CreateModuleDescription = "Real Estate Transfer";
			}
			else if (strCode=="MV")
			{
				CreateModuleDescription = "Motor Vehicle";
			}
			else if (strCode=="RR")
			{
				CreateModuleDescription = "MV Rapid Renewal";
			}
			else if (strCode=="PP")
			{
				CreateModuleDescription = "Personal Property";
			}
			else if (strCode=="PY")
			{
				CreateModuleDescription = "Payroll";
			}
			else if (strCode=="PYTaxUpdate")
			{
				CreateModuleDescription = "Payroll Tax Table Update";
			}
			else if (strCode=="TS")
			{
				CreateModuleDescription = "Tax Service";
			}
			else if (strCode=="UT")
			{
				CreateModuleDescription = "Utility Billing";
			}
			else if (strCode=="VR")
			{
				CreateModuleDescription = "Voter Registration";
			}
			else if (strCode=="CM")
			{
				CreateModuleDescription = "Commercial Assessment";
			}
			return CreateModuleDescription;
		}

		private void rptProgramNotes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptProgramNotes properties;
			//rptProgramNotes.Caption	= "Program Notifications";
			//rptProgramNotes.Left	= 0;
			//rptProgramNotes.Top	= 0;
			//rptProgramNotes.Width	= 11880;
			//rptProgramNotes.Height	= 8595;
			//rptProgramNotes.StartUpPosition	= 3;
			//rptProgramNotes.SectionData	= "rptProgramNotes.dsx":0000;
			//End Unmaped Properties
		}
	}
}
