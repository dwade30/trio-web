﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmReminders.
	/// </summary>
	public partial class frmReminders : BaseForm
	{
		public frmReminders()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReminders InstancePtr
		{
			get
			{
				return (frmReminders)Sys.GetInstance(typeof(frmReminders));
			}
		}

		protected frmReminders _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int KeyCol;
		int NotesCol;
		int DescriptionCol;
		int DateCol;

		private void cmdDismiss_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (vsReminders.Row > 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + vsReminders.TextMatrix(vsReminders.Row, KeyCol), "SystemSettings");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					rsInfo.Edit();
					rsInfo.Set_Fields("TaskReminder", false);
					rsInfo.Set_Fields("ReminderStartDate", null);
					rsInfo.Update();
				}
			}
			vsReminders.RemoveItem(vsReminders.Row);
			if (vsReminders.Rows == 1)
			{
				mnuProcessQuit_Click();
			}
		}

		private void cmdDismissAll_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			for (counter = 1; counter <= vsReminders.Rows - 1; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + vsReminders.TextMatrix(counter, KeyCol), "SystemSettings");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					rsInfo.Edit();
					rsInfo.Set_Fields("TaskReminder", false);
					rsInfo.Set_Fields("ReminderStartDate", null);
					rsInfo.Update();
				}
			}
			vsReminders.Rows = 1;
			mnuProcessQuit_Click();
		}

		private void cmdSnooze_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			switch (cboSnooze.SelectedIndex)
			{
				case 0:
					{
						rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + vsReminders.TextMatrix(vsReminders.Row, KeyCol), "SystemSettings");
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReminderStartDate", DateAndTime.DateAdd("d", 1, DateTime.Today));
							rsInfo.Update();
						}
						break;
					}
				case 1:
					{
						rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + vsReminders.TextMatrix(vsReminders.Row, KeyCol), "SystemSettings");
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReminderStartDate", DateAndTime.DateAdd("d", -1, (DateTime)rsInfo.Get_Fields_DateTime("TaskDate")));
							rsInfo.Update();
						}
						break;
					}
				case 2:
					{
						rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + vsReminders.TextMatrix(vsReminders.Row, KeyCol), "SystemSettings");
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReminderStartDate", rsInfo.Get_Fields_DateTime("TaskDate"));
							rsInfo.Update();
						}
						break;
					}
				default:
					{
						return;
					}
			}
			//end switch
			vsReminders.RemoveItem(vsReminders.Row);
			if (vsReminders.Rows == 1)
			{
				mnuProcessQuit_Click();
			}
		}

		private void frmReminders_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmReminders_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReminders properties;
			//frmReminders.FillStyle	= 0;
			//frmReminders.ScaleWidth	= 5880;
			//frmReminders.ScaleHeight	= 4260;
			//frmReminders.LinkTopic	= "Form2";
			//frmReminders.LockControls	= true;
			//frmReminders.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			KeyCol = 0;
			NotesCol = 1;
			DescriptionCol = 2;
			DateCol = 3;
			vsReminders.ColHidden(KeyCol, true);
			vsReminders.ColHidden(NotesCol, true);
			vsReminders.ColWidth(DescriptionCol, FCConvert.ToInt32(vsReminders.Width * 0.8));
			vsReminders.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsReminders.TextMatrix(0, DescriptionCol, "Description");
			vsReminders.TextMatrix(0, DateCol, "Date");
			LoadRemindersGrid();
			vsReminders.Row = 1;
			cboSnooze.SelectedIndex = 0;
		}

		private void frmReminders_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void LoadRemindersGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE TaskDate >= '" + DateTime.Today.ToShortDateString() + "' AND ReminderStartDate <= '" + DateTime.Today.ToShortDateString() + "' AND TaskPerson = '" + modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID() + "'", "SystemSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsReminders.Rows += 1;
					vsReminders.TextMatrix(vsReminders.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vsReminders.TextMatrix(vsReminders.Rows - 1, NotesCol, FCConvert.ToString(rsInfo.Get_Fields_String("TaskNotes")));
					vsReminders.TextMatrix(vsReminders.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields_String("TaskDescription")));
					vsReminders.TextMatrix(vsReminders.Rows - 1, DateCol, Strings.Format(rsInfo.Get_Fields_DateTime("TaskDate"), "MM/dd/yyyy"));
					rsInfo.Edit();
					rsInfo.Set_Fields("ReminderShownDate", DateTime.Now);
					rsInfo.Update();
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void vsReminders_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsReminders[e.ColumnIndex, e.RowIndex];
            if (vsReminders.GetFlexRowIndex(e.RowIndex) >= 0)
			{
				//ToolTip1.SetToolTip(vsReminders, vsReminders.TextMatrix(vsReminders.MouseRow, NotesCol));
				cell.ToolTipText =  vsReminders.TextMatrix(vsReminders.GetFlexRowIndex(e.RowIndex), NotesCol);
			}
			else
			{
                //ToolTip1.SetToolTip(vsReminders, "");
                cell.ToolTipText = "";
			}
		}
	}
}
