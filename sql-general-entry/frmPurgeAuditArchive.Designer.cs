﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPurgeAuditArchive.
	/// </summary>
	partial class frmPurgeAuditArchive : BaseForm
	{
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCButton cmdPurge;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblInstructions;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeAuditArchive));
			this.cmdExit = new fecherFoundation.FCButton();
			this.cmdPurge = new fecherFoundation.FCButton();
			this.txtDate = new Global.T2KDateBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 310);
			this.BottomPanel.Size = new System.Drawing.Size(577, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdExit);
			this.ClientArea.Controls.Add(this.cmdPurge);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(577, 250);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(577, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(360, 30);
			this.HeaderText.Text = "Purge Audit Archive Information";
			// 
			// cmdExit
			// 
			this.cmdExit.AppearanceKey = "actionButton";
			this.cmdExit.ForeColor = System.Drawing.Color.White;
			this.cmdExit.Location = new System.Drawing.Point(145, 162);
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.Size = new System.Drawing.Size(96, 40);
			this.cmdExit.TabIndex = 3;
			this.cmdExit.Text = "Exit";
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			// 
			// cmdPurge
			// 
			this.cmdPurge.AppearanceKey = "actionButton";
			this.cmdPurge.ForeColor = System.Drawing.Color.White;
			this.cmdPurge.Location = new System.Drawing.Point(30, 162);
			this.cmdPurge.Name = "cmdPurge";
			this.cmdPurge.Size = new System.Drawing.Size(99, 40);
			this.cmdPurge.TabIndex = 2;
			this.cmdPurge.Text = "Purge";
			this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(30, 100);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.MaxLength = 10;
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 1;
			this.txtDate.Text = "  /  /";
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(494, 52);
			this.lblInstructions.TabIndex = 0;
			this.lblInstructions.Text = "PLEASE ENTER A DATE AT LEAST ONE YEAR IN THE PAST AND CLICK THE \'PURGE\' BUTTON.  " + "ALL AUDIT ARCHIVE INFORMATION THAT THAT WAS SAVED ON OR BEFORE THAT DATE WILL BE" + " DELETED";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmPurgeAuditArchive
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(577, 418);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPurgeAuditArchive";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purge Audit Archive Information";
			this.Load += new System.EventHandler(this.frmPurgeAuditArchive_Load);
			this.Activated += new System.EventHandler(this.frmPurgeAuditArchive_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeAuditArchive_KeyPress);
			this.Resize += new System.EventHandler(this.frmPurgeAuditArchive_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
