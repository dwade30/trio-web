﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	public class modGlobalRoutines
	{
		
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			foreach (Form frm in Application.OpenForms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}
	
		public static void LoadStates(ref FCComboBox objState, ref clsDRWrapper dbDatabase)
		{
			int intCounter;
			// Dim rsUser As DAO.Recordset
			dbDatabase.OpenRecordset("Select * from tblStates", "CentralData");
			if (!dbDatabase.EndOfFile())
			{
				dbDatabase.MoveLast();
				dbDatabase.MoveFirst();
				for (intCounter = 0; intCounter <= dbDatabase.RecordCount() - 1; intCounter++)
				{
					objState.AddItem(dbDatabase.Get_Fields_String("StateCode") + " - " + dbDatabase.Get_Fields_String("Description"));
					dbDatabase.MoveNext();
				}
			}
			// Set dbDatabase = Nothing
		}

        public static string GetBackupPath()
        {
            var strSavePath = StaticSettings.gGlobalSettings.BackupPath;

            if (Strings.Right(" " + strSavePath, 1) != "/" && Strings.Right(" " + strSavePath, 1) != "\\")
            {
                strSavePath += "\\";
            }

            return strSavePath;
        }
    }
}
