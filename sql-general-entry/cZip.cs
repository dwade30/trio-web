//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	public class cZip
	{

		//=========================================================

		// ======================================================================================

		// ======================================================================================


		public enum EZPMsgLevel
		{
			ezpAllMessages = 0,
			ezpPartialMessages = 1,
			ezpNoMessages = 2
		};

		public delegate void CancelEventHandler(string sMsg, ref bool bCancel);
		public event CancelEventHandler Cancel;
		public delegate void PasswordRequestEventHandler(ref string sPassword, ref bool bCancel);
		public event PasswordRequestEventHandler Event_PasswordRequest;
		public delegate void ProgressEventHandler(int lCount, string sMsg);
		public event ProgressEventHandler Progress;


		private mZip.ZPOPT m_tZPOPT = new mZip.ZPOPT();
		private string m_sFileName = "";
		private string[] m_sFileSpecs = null;
		private int m_iCount;

		// Set zip options
		// m_tZPOPT.fSuffix = 0        ' include suffixes (not yet implemented)
		// m_tZPOPT.fExtra = 0         ' 1 if including extra attributes
		// m_tZPOPT.date = vbNullString ' "12/31/79"? US Date?
		// m_tZPOPT.fExcludeDate = 0   ' 1 if excluding files earlier than a specified date
		// m_tZPOPT.fIncludeDate = 0   ' 1 if including files earlier than a specified date
		// m_tZPOPT.fJunkSFX = 0       ' 1 if junking sfx prefix
		// m_tZPOPT.fOffsets = 0       ' 1 if updating archive offsets for sfx Files

		// m_tZPOPT.fComment = 0       ' 1 if putting comment in zip file

		// m_tZPOPT.fGrow = 0          ' 1 if allow appending to zip file
		// m_tZPOPT.fForce = 0         ' 1 if making entries using DOS names
		// m_tZPOPT.fMove = 0          ' 1 if deleting files added or updated
		// m_tZPOPT.fDeleteEntries = 0 ' 1 if files passed have to be deleted
		// m_tZPOPT.fLatestTime = 0    ' 1 if setting zip file time to time of latest file in archive
		// m_tZPOPT.fPrivilege = 0     ' 1 if not saving privileges
		// m_tZPOPT.fEncryption = 0    'Read only property!
		// m_tZPOPT.fRepair = 0        ' 1=> fix archive, 2=> try harder to fix
		// m_tZPOPT.flevel = 0         ' compression level - should be 0!!!



		public string ZipFile
		{
			get
			{
					string ZipFile = "";
				ZipFile = m_sFileName;
				return ZipFile;
			}

			set
			{
				m_sFileName = value;
			}
		}

		public string BasePath
		{
			get
			{
					string BasePath = "";
				BasePath = m_tZPOPT.szRootDir;
				return BasePath;
			}

			set
			{
				m_tZPOPT.szRootDir = value;
			}
		}

		public bool Encrpyt
		{
			get
			{
					bool Encrpyt = false;
				Encrypt = !(m_tZPOPT.fEncrypt==0);
				return Encrpyt;
			}
		}
		public bool Encrypt
		{
			set
			{
				m_tZPOPT.fEncrypt = Math.Abs((value ? -1 : 0));
			}
		}
		public bool IncludeSystemAndHiddenFiles
		{
			get
			{
					bool IncludeSystemAndHiddenFiles = false;
				IncludeSystemAndHiddenFiles = !(m_tZPOPT.fSystem==0); // 1 to include system/hidden files
				return IncludeSystemAndHiddenFiles;
			}

			set
			{
				m_tZPOPT.fSystem = Math.Abs((value ? -1 : 0)); // 1 to include system/hidden files
			}
		}

		public bool StoreVolumeLabel
		{
			get
			{
					bool StoreVolumeLabel = false;
				StoreVolumeLabel = !(m_tZPOPT.fVolume==0); // 1 if storing volume label
				return StoreVolumeLabel;
			}

			set
			{
				m_tZPOPT.fVolume = Math.Abs((value ? -1 : 0));
			}
		}

		public bool StoreDirectories
		{
			get
			{
					bool StoreDirectories = false;
				StoreDirectories = !(m_tZPOPT.fNoDirEntries==0); // 1 if ignoring directory entries
				return StoreDirectories;
			}

			set
			{
				m_tZPOPT.fNoDirEntries = Math.Abs((!(value) ? -1 : 0));
			}
		}

		public bool StoreFolderNames
		{
			get
			{
					bool StoreFolderNames = false;
				StoreFolderNames = (m_tZPOPT.fJunkDir==0);
				return StoreFolderNames;
			}

			set
			{
				m_tZPOPT.fJunkDir = Math.Abs((!(value) ? -1 : 0));
			}
		}

		public bool RecurseSubDirs
		{
			get
			{
					bool RecurseSubDirs = false;
				RecurseSubDirs = !(m_tZPOPT.fRecurse==0); // 1 if recursing into subdirectories
				return RecurseSubDirs;
			}

			set
			{
				if (value) {
					m_tZPOPT.fRecurse = 2;
				} else {
					m_tZPOPT.fRecurse = 0;
				}
			}
		}


		public bool UpdateOnlyIfNewer
		{
			get
			{
					bool UpdateOnlyIfNewer = false;
				UpdateOnlyIfNewer = !(m_tZPOPT.fUpdate==0); // 1 if updating zip file--overwrite only if newer
				return UpdateOnlyIfNewer;
			}

			set
			{
				m_tZPOPT.fUpdate = Math.Abs((value ? -1 : 0)); // 1 if updating zip file--overwrite only if newer
			}
		}

		public bool FreshenFiles
		{
			get
			{
					bool FreshenFiles = false;
				FreshenFiles = !(m_tZPOPT.fFreshen==0); // 1 if freshening zip file--overwrite only
				return FreshenFiles;
			}

			set
			{
				m_tZPOPT.fUpdate = Math.Abs((value ? -1 : 0)); // 1 if updating zip file--overwrite only if newer
			}
		}

		public EZPMsgLevel MessageLevel
		{
			get
			{
					EZPMsgLevel MessageLevel = (EZPMsgLevel)0;
				if (!(m_tZPOPT.fVerbose==0)) {
					MessageLevel = EZPMsgLevel.ezpAllMessages;
				} else if (!(m_tZPOPT.fQuiet==0)) {
					MessageLevel = EZPMsgLevel.ezpPartialMessages;
				} else {
					MessageLevel = EZPMsgLevel.ezpNoMessages;
				}
				return MessageLevel;
			}

			set
			{
				switch (value) {
					
					case EZPMsgLevel.ezpPartialMessages:
					{
						m_tZPOPT.fQuiet = 1;
						m_tZPOPT.fVerbose = 0;
						break;
					}
					case EZPMsgLevel.ezpNoMessages:
					{
						m_tZPOPT.fQuiet = 0;
						m_tZPOPT.fVerbose = 0;
						break;
					}
					case EZPMsgLevel.ezpAllMessages:
					{
						m_tZPOPT.fQuiet = 0;
						m_tZPOPT.fVerbose = 1;
						break;
					}
				} //end switch
			}
		}

		public bool ConvertCRLFToLF
		{
			get
			{
					bool ConvertCRLFToLF = false;
				ConvertCRLFToLF = (m_tZPOPT.fCRLF_LF!=0);
				return ConvertCRLFToLF;
			}

			set
			{
				m_tZPOPT.fCRLF_LF = Math.Abs((value ? -1 : 0));
			}
		}

		public bool ConvertLFToCRLF
		{
			get
			{
					bool ConvertLFToCRLF = false;
				ConvertLFToCRLF = (m_tZPOPT.fLF_CRLF!=0);
				return ConvertLFToCRLF;
			}

			set
			{
				m_tZPOPT.fLF_CRLF = Math.Abs((value ? -1 : 0));
			}
		}


		public void ProgressReport(string sMsg)
		{
			if (this.Progress != null) this.Progress(1, sMsg);
		}
		public void PasswordRequest(ref string sPassword, ref bool bCancel)
		{
			if (this.Event_PasswordRequest != null) this.Event_PasswordRequest(ref sPassword, ref bCancel);
		}
		public void Service(string sMsg, ref bool bCancel)
		{
			if (this.Cancel != null) this.Cancel(sMsg, ref bCancel);
		}
		public void ClearFileSpecs()
		{
			m_iCount = 0;
			FCUtils.EraseSafe(m_sFileSpecs);
		}
		public int AddFileSpec(string sSpec)
		{
			int AddFileSpec = 0;
			m_iCount += 1;
			Array.Resize(ref m_sFileSpecs, m_iCount + 1);
			m_sFileSpecs[m_iCount] = sSpec;
			return AddFileSpec;
		}
		public int FileSpecCount
		{
			get
			{
					int FileSpecCount = 0;
				FileSpecCount = m_iCount;
				return FileSpecCount;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public string Get_FileSpec(int nIndex)
		{
				string FileSpec = "";
			FileSpec = m_sFileSpecs[nIndex];
			return FileSpec;
		}
		public bool AllowAppend
		{
			get
			{
					bool AllowAppend = false;
				AllowAppend = (m_tZPOPT.fGrow==1);
				return AllowAppend;
			}

			set
			{
				m_tZPOPT.fGrow = Math.Abs((value ? -1 : 0));
			}
		}

		public int Zip()
		{
			int Zip = 0;
			Zip = mZip.VBZip(ref Me, ref m_tZPOPT, ref m_sFileSpecs, ref m_iCount);
			return Zip;
		}
		public void Delete()
		{
			// Deletes the entries specified by the file specs:
			m_tZPOPT.fDeleteEntries = 1;
			mZip.VBZip(ref Me, ref m_tZPOPT, ref m_sFileSpecs, ref m_iCount);
			m_tZPOPT.fDeleteEntries = 0;
		}

		public cZip() : base()
		{
			StoreDirectories = false;
			StoreFolderNames = false;
			RecurseSubDirs = false;
		}

	}
}