﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using SharedApplication.Backups;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmBackup.
	/// </summary>
	partial class frmBackup : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkMod;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCButton cmdBackup;
		public fecherFoundation.FCButton cmdUnselect;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBackup));
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.cmdBackup = new fecherFoundation.FCButton();
            this.cmdUnselect = new fecherFoundation.FCButton();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.grdBackups = new fecherFoundation.FCDataGridInternal();
            this.colFilename = new Wisej.Web.DataGridViewTextBoxColumn();
            this.colBackupdate = new Wisej.Web.DataGridViewDateTimePickerColumn();
            this.bindingSource1 = new Wisej.Web.BindingSource(this.components);
            this.grdModules = new fecherFoundation.FCGrid();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBackup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnselect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdBackups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdModules)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdBackup);
            this.BottomPanel.Location = new System.Drawing.Point(0, 475);
            this.BottomPanel.Size = new System.Drawing.Size(927, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.grdBackups);
            this.ClientArea.Controls.Add(this.grdModules);
            this.ClientArea.Controls.Add(this.cmdSelectAll);
            this.ClientArea.Controls.Add(this.cmdUnselect);
            this.ClientArea.Size = new System.Drawing.Size(947, 612);
            this.ClientArea.Controls.SetChildIndex(this.cmdUnselect, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSelectAll, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.grdModules, 0);
            this.ClientArea.Controls.SetChildIndex(this.grdBackups, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(947, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(84, 28);
            this.HeaderText.Text = "Backup";
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.ForeColor = System.Drawing.Color.White;
            this.cmdSelectAll.Location = new System.Drawing.Point(110, 435);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(94, 40);
            this.cmdSelectAll.TabIndex = 1;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // cmdBackup
            // 
            this.cmdBackup.AppearanceKey = "acceptButton";
            this.cmdBackup.ForeColor = System.Drawing.Color.White;
            this.cmdBackup.Location = new System.Drawing.Point(194, 31);
            this.cmdBackup.Name = "cmdBackup";
            this.cmdBackup.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdBackup.Size = new System.Drawing.Size(109, 48);
            this.cmdBackup.TabIndex = 3;
            this.cmdBackup.Text = "Backup";
            this.cmdBackup.Click += new System.EventHandler(this.cmdBackup_Click);
            // 
            // cmdUnselect
            // 
            this.cmdUnselect.AppearanceKey = "actionButton";
            this.cmdUnselect.ForeColor = System.Drawing.Color.White;
            this.cmdUnselect.Location = new System.Drawing.Point(226, 435);
            this.cmdUnselect.Name = "cmdUnselect";
            this.cmdUnselect.Size = new System.Drawing.Size(115, 40);
            this.cmdUnselect.TabIndex = 2;
            this.cmdUnselect.Text = "Unselect All";
            this.cmdUnselect.Click += new System.EventHandler(this.cmdUnselect_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 0;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            // 
            // grdBackups
            // 
            this.grdBackups.AllowUpdate = false;
            this.grdBackups.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.grdBackups.AutoGenerateColumns = false;
            this.grdBackups.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.grdBackups.Columns.AddRange(new Wisej.Web.DataGridViewColumn[] {
            this.colFilename,
            this.colBackupdate});
            this.grdBackups.DataSource = this.bindingSource1;
            this.grdBackups.Location = new System.Drawing.Point(458, 26);
            this.grdBackups.Name = "grdBackups";
            this.grdBackups.ReadOnly = true;
            this.grdBackups.RowTemplate.Height = 20;
            this.grdBackups.ShowColumnVisibilityMenu = false;
            this.grdBackups.Size = new System.Drawing.Size(465, 388);
            this.grdBackups.TabIndex = 1004;
            // 
            // colFilename
            // 
            this.colFilename.AutoSizeMode = Wisej.Web.DataGridViewAutoSizeColumnMode.AllCells;
            this.colFilename.DataPropertyName = "FileName";
            this.colFilename.HeaderText = "File Name";
            this.colFilename.MinimumWidth = 200;
            this.colFilename.Name = "colFilename";
            this.colFilename.SortMode = Wisej.Web.DataGridViewColumnSortMode.NotSortable;
            this.colFilename.Width = 500;
            // 
            // colBackupdate
            // 
            this.colBackupdate.AutoSizeMode = Wisej.Web.DataGridViewAutoSizeColumnMode.Fill;
            this.colBackupdate.CustomFormat = "g";
            this.colBackupdate.DataPropertyName = "BackupDate";
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "g";
            this.colBackupdate.DefaultCellStyle = dataGridViewCellStyle1;
            this.colBackupdate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.colBackupdate.HeaderText = "Backup Date";
            this.colBackupdate.MinimumWidth = 100;
            this.colBackupdate.Name = "colBackupdate";
            this.colBackupdate.SortMode = Wisej.Web.DataGridViewColumnSortMode.NotSortable;
            this.colBackupdate.Width = 200;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(SharedApplication.Backups.backupItem);
            this.bindingSource1.RefreshValueOnChange = true;
            // 
            // grdModules
            // 
            this.grdModules.ColumnHeadersVisible = false;
            this.grdModules.ExtendLastCol = true;
            this.grdModules.FixedCols = 0;
            this.grdModules.FixedRows = 0;
            this.grdModules.Location = new System.Drawing.Point(17, 26);
            this.grdModules.Name = "grdModules";
            this.grdModules.RowHeadersVisible = false;
            this.grdModules.Rows = 0;
            this.grdModules.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
            this.grdModules.Size = new System.Drawing.Size(417, 388);
            this.grdModules.TabIndex = 1001;
            this.grdModules.Click += new System.EventHandler(this.grdModules_Click);
            // 
            // frmBackup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(947, 672);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBackup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Backup";
            this.Load += new System.EventHandler(this.frmBackup_Load);
            this.Activated += new System.EventHandler(this.frmBackup_Activated);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBackup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnselect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdBackups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdModules)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
        private BindingSource bindingSource1;
        private FCDataGridInternal grdBackups;
        private DataGridViewTextBoxColumn colFilename;
        private DataGridViewDateTimePickerColumn colBackupdate;
        private FCGrid grdModules;
    }
}
