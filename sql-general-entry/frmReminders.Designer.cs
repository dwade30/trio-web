﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmReminders.
	/// </summary>
	partial class frmReminders : BaseForm
	{
		public fecherFoundation.FCComboBox cboSnooze;
		public fecherFoundation.FCButton cmdSnooze;
		public fecherFoundation.FCButton cmdDismiss;
		public fecherFoundation.FCButton cmdDismissAll;
		public FCGrid vsReminders;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReminders));
			this.cboSnooze = new fecherFoundation.FCComboBox();
			this.cmdSnooze = new fecherFoundation.FCButton();
			this.cmdDismiss = new fecherFoundation.FCButton();
			this.cmdDismissAll = new fecherFoundation.FCButton();
			this.vsReminders = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSnooze)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDismiss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDismissAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsReminders)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 451);
			this.BottomPanel.Size = new System.Drawing.Size(522, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboSnooze);
			this.ClientArea.Controls.Add(this.cmdSnooze);
			this.ClientArea.Controls.Add(this.cmdDismiss);
			this.ClientArea.Controls.Add(this.cmdDismissAll);
			this.ClientArea.Controls.Add(this.vsReminders);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(522, 391);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(522, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(160, 30);
			this.HeaderText.Text = "Reminders(s)";
			// 
			// cboSnooze
			// 
			this.cboSnooze.AutoSize = false;
			this.cboSnooze.BackColor = System.Drawing.SystemColors.Window;
			this.cboSnooze.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSnooze.FormattingEnabled = true;
			this.cboSnooze.Items.AddRange(new object[] {
				"Until Tomorrow",
				"Until 1 Day Before Event",
				"Until Day of Event"
			});
			this.cboSnooze.Location = new System.Drawing.Point(31, 290);
			this.cboSnooze.Name = "cboSnooze";
			this.cboSnooze.Size = new System.Drawing.Size(321, 40);
			this.cboSnooze.TabIndex = 3;
			// 
			// cmdSnooze
			// 
			this.cmdSnooze.AppearanceKey = "actionButton";
			this.cmdSnooze.ForeColor = System.Drawing.Color.White;
			this.cmdSnooze.Location = new System.Drawing.Point(374, 290);
			this.cmdSnooze.Name = "cmdSnooze";
			this.cmdSnooze.Size = new System.Drawing.Size(101, 40);
			this.cmdSnooze.TabIndex = 4;
			this.cmdSnooze.Text = "Snooze";
			this.cmdSnooze.Click += new System.EventHandler(this.cmdSnooze_Click);
			// 
			// cmdDismiss
			// 
			this.cmdDismiss.AppearanceKey = "actionButton";
			this.cmdDismiss.ForeColor = System.Drawing.Color.White;
			this.cmdDismiss.Location = new System.Drawing.Point(375, 230);
			this.cmdDismiss.Name = "cmdDismiss";
			this.cmdDismiss.Size = new System.Drawing.Size(100, 40);
			this.cmdDismiss.TabIndex = 2;
			this.cmdDismiss.Text = "Dismiss";
			this.cmdDismiss.Click += new System.EventHandler(this.cmdDismiss_Click);
			// 
			// cmdDismissAll
			// 
			this.cmdDismissAll.AppearanceKey = "actionButton";
			this.cmdDismissAll.ForeColor = System.Drawing.Color.White;
			this.cmdDismissAll.Location = new System.Drawing.Point(30, 230);
			this.cmdDismissAll.Name = "cmdDismissAll";
			this.cmdDismissAll.Size = new System.Drawing.Size(101, 40);
			this.cmdDismissAll.TabIndex = 1;
			this.cmdDismissAll.Text = "Dismiss All";
			this.cmdDismissAll.Click += new System.EventHandler(this.cmdDismissAll_Click);
			// 
			// vsReminders
			// 
			this.vsReminders.AllowSelection = false;
			this.vsReminders.AllowUserToResizeColumns = false;
			this.vsReminders.AllowUserToResizeRows = false;
			this.vsReminders.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsReminders.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsReminders.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsReminders.BackColorBkg = System.Drawing.Color.Empty;
			this.vsReminders.BackColorFixed = System.Drawing.Color.Empty;
			this.vsReminders.BackColorSel = System.Drawing.Color.Empty;
			this.vsReminders.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsReminders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsReminders.ColumnHeadersHeight = 30;
			this.vsReminders.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsReminders.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsReminders.DragIcon = null;
			this.vsReminders.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsReminders.ExtendLastCol = true;
			this.vsReminders.FixedCols = 0;
			this.vsReminders.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsReminders.FrozenCols = 0;
			this.vsReminders.GridColor = System.Drawing.Color.Empty;
			this.vsReminders.GridColorFixed = System.Drawing.Color.Empty;
			this.vsReminders.Location = new System.Drawing.Point(30, 30);
			this.vsReminders.Name = "vsReminders";
			this.vsReminders.ReadOnly = true;
			this.vsReminders.RowHeadersVisible = false;
			this.vsReminders.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsReminders.RowHeightMin = 0;
			this.vsReminders.Rows = 1;
			this.vsReminders.ScrollTipText = null;
			this.vsReminders.ShowColumnVisibilityMenu = false;
			this.vsReminders.Size = new System.Drawing.Size(445, 187);
			this.vsReminders.StandardTab = true;
			this.vsReminders.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsReminders.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsReminders.TabIndex = 0;
			this.vsReminders.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsReminders_MouseMoveEvent);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 350);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(426, 19);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "NOTE  REMINDERS WILL ONLY SHOW UNTIL THE DAY OF THE EVENT";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmReminders
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(522, 559);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReminders";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reminder(s)";
			this.Load += new System.EventHandler(this.frmReminders_Load);
			this.Activated += new System.EventHandler(this.frmReminders_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReminders_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSnooze)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDismiss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDismissAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsReminders)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
