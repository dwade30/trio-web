﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Drawing;
using SharedApplication.ClientSettings.Models;
using TWGNENTY.Authentication;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmUpdatePassword.
	/// </summary>
	public partial class frmUpdatePassword : BaseForm
	{
		private AuthenticationService _authenticationService;
		private User _user;
		public frmUpdatePassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		public frmUpdatePassword(User passedUser,  AuthenticationService passedAuthenticationService)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();

			_authenticationService = passedAuthenticationService;
			_user = passedUser;
		}

		private void InitializeComponentEx()
		{
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label2.AddControlArrayElement(Label2_0, 0);
			this.Label2.AddControlArrayElement(Label2_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdatePassword InstancePtr
		{
			get
			{
				return (frmUpdatePassword)Sys.GetInstance(typeof(frmUpdatePassword));
			}
		}

		protected frmUpdatePassword _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private string strNewPass = string.Empty;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			strNewPass = "";
			this.Unload();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (Strings.UCase(txtPassword1.Text) != Strings.UCase(txtPassword2.Text))
			{
				MessageBox.Show("Passwords do not match.", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPassword1.Text = "";
				txtPassword2.Text = "";
			}
			else
			{
				var result = _authenticationService.GetAuthenticatedUser(_user.UserID, txtCurrentPassword.Text);
				if (result.result == AuthenticationResult.Success)
				{
					PasswordValidationService passwordValidationService = new PasswordValidationService();

					var validationResult = passwordValidationService.IsValidPassword(txtPassword1.Text,
						_authenticationService.GetPreviousPasswords(_user.Id));
						
					if (validationResult == PasswordValidationResult.CommonPassword)
					{
						MessageBox.Show("This password is too common to use.", "Common Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPassword1.Text = "";
						txtPassword2.Text = "";
					}
					else if (validationResult == PasswordValidationResult.PreviousPassword)
					{
						MessageBox.Show("You have already used this password.", "Previously Used Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPassword1.Text = "";
						txtPassword2.Text = "";
					}
					else if (validationResult == PasswordValidationResult.DoesNotMeetMinimumRequirements)
					{
						MessageBox.Show("This password does not meet the minimum requirements.  The password length must be at least 8 characters.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPassword1.Text = "";
						txtPassword2.Text = "";
					}
					else if (validationResult == PasswordValidationResult.GoodPassword)
					{
						strNewPass = txtPassword1.Text;
						this.Unload();
					}
				}
				else if (result.result == AuthenticationResult.InvalidCredentials)
				{
					MessageBox.Show("Invalid current password", "Invalid Credentials", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else if (result.result == AuthenticationResult.InvalidForSelectedEnvironment)
				{
					MessageBox.Show("Invalid user for the selected data group", "Invalid User For Selected Data Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else if (result.result == AuthenticationResult.LockedOut)
				{
					MessageBox.Show("This user is currently locked out.  You can either wait 10 minutes or have an admin unlock the user.", "Locked Out", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					strNewPass = "";
					this.Unload();
				}
			}
		}

		private void frmUpdatePassword_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
		}

		public string Init()
		{
			string Init = "";
			strNewPass = "";
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strNewPass;
			return Init;
		}

		private void mnuExit_Click()
		{
			this.Unload();
		}

		private void txtPassword1_KeyPress(object sender, KeyPressEventArgs e)
		{
			UpdatePasswordStrength();
		}

		private void UpdatePasswordStrength()
		{
			var result = PasswordStrengthService.GetPasswordStrength(txtPassword1.Text);

			switch (result)
			{
				case PasswordStrengthResult.Blank:
				{
					txtPasswordStrengthIndicator.BackColor = Color.Red;
					txtPasswordStrengthIndicator.Text = "BLANK";
					break;
				}
				case PasswordStrengthResult.VeryWeak:
				{
					txtPasswordStrengthIndicator.BackColor = Color.OrangeRed;
					txtPasswordStrengthIndicator.Text = "VERY WEAK";
					break;
				}
				case PasswordStrengthResult.Weak:
				{
					txtPasswordStrengthIndicator.BackColor = Color.Orange;
					txtPasswordStrengthIndicator.Text = "WEAK";
					break;
				}
				case PasswordStrengthResult.Medium:
				{
					txtPasswordStrengthIndicator.BackColor = Color.Yellow;
					txtPasswordStrengthIndicator.Text = "MEDIUM";
					break;
				}
				case PasswordStrengthResult.Strong:
				{
					txtPasswordStrengthIndicator.BackColor = Color.GreenYellow;
					txtPasswordStrengthIndicator.Text = "STRONG";
					break;
				}
				case PasswordStrengthResult.VeryStrong:
				{
					txtPasswordStrengthIndicator.BackColor = Color.Green;
					txtPasswordStrengthIndicator.Text = "VERY STRONG";
					break;
				}
			}
		}
	}
}
