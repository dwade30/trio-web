﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmChilkatUpload.
	/// </summary>
	partial class frmChilkatUpload : BaseForm
	{
		public fecherFoundation.FCFileListBox filUpload;
		public fecherFoundation.FCListBox lstMessages;
		public FCGrid vsUpload;
		public fecherFoundation.FCProgressBar ProgressBar1;
		//public Chilkat.Ftp2 ChilkatFtp1;
		public Chilkat.SFtp ChilkatFtp1;
		public fecherFoundation.FCLabel lblPercent;
		//public AxAbaleZipLibrary.AxAbaleZip AbaleZip1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChilkatUpload));
            this.filUpload = new fecherFoundation.FCFileListBox();
            this.lstMessages = new fecherFoundation.FCListBox();
            this.vsUpload = new fecherFoundation.FCGrid();
            this.ProgressBar1 = new fecherFoundation.FCProgressBar();
            this.lblPercent = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdUpload = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsUpload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUpload)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdUpload);
            this.BottomPanel.Location = new System.Drawing.Point(0, 478);
            this.BottomPanel.Size = new System.Drawing.Size(551, 94);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lstMessages);
            this.ClientArea.Controls.Add(this.vsUpload);
            this.ClientArea.Controls.Add(this.ProgressBar1);
            this.ClientArea.Controls.Add(this.lblPercent);
            this.ClientArea.Controls.Add(this.filUpload);
            this.ClientArea.Size = new System.Drawing.Size(571, 587);
            this.ClientArea.Controls.SetChildIndex(this.filUpload, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPercent, 0);
            this.ClientArea.Controls.SetChildIndex(this.ProgressBar1, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsUpload, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstMessages, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(571, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(125, 28);
            this.HeaderText.Text = "Upload File";
            // 
            // filUpload
            // 
            this.filUpload.Location = new System.Drawing.Point(313, 26);
            this.filUpload.Name = "filUpload";
            this.filUpload.Size = new System.Drawing.Size(203, 103);
            this.filUpload.TabIndex = 4;
            this.filUpload.Visible = false;
            // 
            // lstMessages
            // 
            this.lstMessages.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.lstMessages.Location = new System.Drawing.Point(25, 393);
            this.lstMessages.Name = "lstMessages";
            this.lstMessages.Size = new System.Drawing.Size(521, 85);
            this.lstMessages.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.lstMessages, "FTP Status Messages");
            // 
            // vsUpload
            // 
            this.vsUpload.Cols = 10;
            this.vsUpload.ColumnHeadersVisible = false;
            this.vsUpload.ExtendLastCol = true;
            this.vsUpload.FixedCols = 0;
            this.vsUpload.FixedRows = 0;
            this.vsUpload.Location = new System.Drawing.Point(54, 26);
            this.vsUpload.Name = "vsUpload";
            this.vsUpload.RowHeadersVisible = false;
            this.vsUpload.Rows = 0;
            this.vsUpload.Size = new System.Drawing.Size(463, 275);
            this.vsUpload.TabIndex = 0;
            this.vsUpload.Click += new System.EventHandler(this.vsUpload_ClickEvent);
            this.vsUpload.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsUpload_KeyPressEvent);
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(30, 354);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(290, 27);
            this.ProgressBar1.Step = 1;
            this.ProgressBar1.TabIndex = 2;
            // 
            // lblPercent
            // 
            this.lblPercent.Location = new System.Drawing.Point(30, 328);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(163, 17);
            this.lblPercent.TabIndex = 0;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            // 
            // cmdUpload
            // 
            this.cmdUpload.AppearanceKey = "acceptButton";
            this.cmdUpload.Location = new System.Drawing.Point(228, 28);
            this.cmdUpload.Name = "cmdUpload";
            this.cmdUpload.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdUpload.Size = new System.Drawing.Size(94, 48);
            this.cmdUpload.TabIndex = 2;
            this.cmdUpload.Text = "Upload";
            this.cmdUpload.Click += new System.EventHandler(this.cmdUpload_Click);
            // 
            // frmChilkatUpload
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(571, 647);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmChilkatUpload";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Upload File";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmChilkatUpload_Load);
            this.Resize += new System.EventHandler(this.frmChilkatUpload_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChilkatUpload_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsUpload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUpload)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdUpload;
	}
}
