//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmExpirationFromWeb.
	/// </summary>
	partial class frmExpirationFromWeb : fecherFoundation.FCForm
	{
		public fecherFoundation.FCListBox lstMessages;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public CHILKATFTP2LibCtl.ChilkatFtp2 ChilkatFtp1;
		public fecherFoundation.FCLabel lblPercent;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmExpirationFromWeb));
			this.components = new System.ComponentModel.Container();
			this.lstMessages = new fecherFoundation.FCListBox();
			this.ProgressBar1 = new fecherFoundation.FCProgressBar();
			this.ChilkatFtp1 = new CHILKATFTP2LibCtl.ChilkatFtp2();
			this.lblPercent = new fecherFoundation.FCLabel();
			
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//
			// lstMessages
			//
			this.lstMessages.Name = "lstMessages";
			this.lstMessages.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lstMessages, "FTP Status Messages");
			this.lstMessages.Location = new System.Drawing.Point(2, 156);
			this.lstMessages.Size = new System.Drawing.Size(389, 125);
			this.lstMessages.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			//
			// ProgressBar1
			//
			this.ProgressBar1.Name = "ProgressBar1";
			this.ProgressBar1.TabIndex = 1;
			this.ProgressBar1.Location = new System.Drawing.Point(52, 94);
			this.ProgressBar1.Size = new System.Drawing.Size(290, 27);
			this.ProgressBar1.Minimum = 0;
			this.ProgressBar1.Maximum = 100;
			this.ProgressBar1.Step = 1;
			//
			// ChilkatFtp1
			//
			this.ChilkatFtp1.Location = new System.Drawing.Point(178, 39);
			//
			// lblPercent
			//
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.TabIndex = 2;
			this.lblPercent.Location = new System.Drawing.Point(115, 74);
			this.lblPercent.Size = new System.Drawing.Size(163, 16);
			this.lblPercent.Text = "";
			this.lblPercent.BackColor = System.Drawing.SystemColors.Control;
			this.lblPercent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblPercent.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(350, 24);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuSaveContinue, this.Seperator, this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "&File";
			//
			// mnuSaveContinue
			//
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Text = "S&ave && Continue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "E&xit    (Esc)";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuProcess});
			//
			// frmExpirationFromWeb
			//
			this.ClientSize = new System.Drawing.Size(396, 283);
			this.Controls.Add(this.lstMessages);
			this.Controls.Add(this.ProgressBar1);
			this.Controls.Add(this.ChilkatFtp1);
			this.Controls.Add(this.lblPercent);
			//this.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmExpirationFromWeb";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmExpirationFromWeb.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExpirationFromWeb_KeyDown);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmExpirationFromWeb_FormClosing);
			this.Activated += new System.EventHandler(this.frmExpirationFromWeb_Activated);
			this.Load += new System.EventHandler(this.frmExpirationFromWeb_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExpirationFromWeb_KeyPress);
			this.Text = "Expiration Update";
			((System.ComponentModel.ISupportInitialize)(this.ChilkatFtp1)).EndInit();
			this.lblPercent.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}