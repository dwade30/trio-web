﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD1.
	/// </summary>
	public partial class srptBD1 : FCSectionReport
	{
		public srptBD1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD1";
		}

		public static srptBD1 InstancePtr
		{
			get
			{
				return (srptBD1)Sys.GetInstance(typeof(srptBD1));
			}
		}

		protected srptBD1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		bool blnNoData;
		clsDRWrapper rsBalanceInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				CheckBalance:
				;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM ExpenseReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				}
				else if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "R")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
				}
				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsBalanceInfo.Get_Fields("OriginalBudgetTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("BudgetAdjustmentsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedCreditsTotal")) != 0)
				{
					blnFirstRecord = false;
					eArgs.EOF = false;
				}
				else
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile())
					{
						blnNoData = true;
						blnFirstRecord = false;
						eArgs.EOF = false;
					}
					else
					{
						goto CheckBalance;
					}
				}
			}
			else
			{
				CheckBalance2:
				;
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM ExpenseReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					}
					else if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "R")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					}
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsBalanceInfo.Get_Fields("OriginalBudgetTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("BudgetAdjustmentsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedCreditsTotal")) != 0)
					{
						eArgs.EOF = false;
					}
					else
					{
						goto CheckBalance2;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT DISTINCT Account FROM (SELECT DISTINCT Account FROM APJournalDetail UNION ALL SELECT DISTINCT Account FROM JournalEntries UNION ALL SELECT DISTINCT Account FROM EncumbranceDetail) as temp WHERE Account NOT IN (SELECT DISTINCT Account FROM AccountMaster WHERE Valid = 1) ORDER BY Account", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				blnFirstRecord = true;
				blnNoData = false;
			}
			else
			{
				blnNoData = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!blnNoData)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount1.Text = rsInfo.Get_Fields_String("Account");
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount2.Text = rsInfo.Get_Fields_String("Account");
				}
				else
				{
					fldAccount2.Text = "";
				}
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount3.Text = rsInfo.Get_Fields_String("Account");
				}
				else
				{
					fldAccount3.Text = "";
				}
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount4.Text = rsInfo.Get_Fields_String("Account");
				}
				else
				{
					fldAccount4.Text = "";
				}
			}
			else
			{
				fldAccount1.Text = "";
				fldAccount2.Text = "";
				fldAccount3.Text = "";
				fldAccount4.Text = "";
				fldNone.Visible = true;
			}
		}

		
	}
}
