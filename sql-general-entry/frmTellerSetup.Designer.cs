﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmTellerSetup.
	/// </summary>
	partial class frmTellerSetup : BaseForm
	{
		public fecherFoundation.FCFrame fraMainOptions;
		public fecherFoundation.FCTextBox txtDeptDiv;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCComboBox cmbTellerID;
		public fecherFoundation.FCLabel lblDeptDiv;
		public fecherFoundation.FCLabel lblPassword;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblTellerID;
		public fecherFoundation.FCButton cmdMV;
		public fecherFoundation.FCPanel fraGRID;
		public fecherFoundation.FCGrid GRID;
		public fecherFoundation.FCFrame fraNew;
		public fecherFoundation.FCButton cmdCancelNew;
		public fecherFoundation.FCButton cmdAddNew;
		public fecherFoundation.FCTextBox txtNew;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileNew;
		public fecherFoundation.FCToolStripMenuItem mnuFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuSAveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTellerSetup));
			this.fraMainOptions = new fecherFoundation.FCFrame();
			this.txtDeptDiv = new fecherFoundation.FCTextBox();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.txtName = new fecherFoundation.FCTextBox();
			this.cmbTellerID = new fecherFoundation.FCComboBox();
			this.lblDeptDiv = new fecherFoundation.FCLabel();
			this.lblPassword = new fecherFoundation.FCLabel();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblTellerID = new fecherFoundation.FCLabel();
			this.cmdMV = new fecherFoundation.FCButton();
			this.fraGRID = new fecherFoundation.FCPanel();
			this.GRID = new fecherFoundation.FCGrid();
			this.fraNew = new fecherFoundation.FCFrame();
			this.cmdCancelNew = new fecherFoundation.FCButton();
			this.cmdAddNew = new fecherFoundation.FCButton();
			this.txtNew = new fecherFoundation.FCTextBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSAveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.cmdFileDelete = new fecherFoundation.FCButton();
			this.cmdFileNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMainOptions)).BeginInit();
			this.fraMainOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraGRID)).BeginInit();
			this.fraGRID.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraNew)).BeginInit();
			this.fraNew.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 366);
			this.BottomPanel.Size = new System.Drawing.Size(855, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraGRID);
			this.ClientArea.Controls.Add(this.fraNew);
			this.ClientArea.Controls.Add(this.fraMainOptions);
			this.ClientArea.Size = new System.Drawing.Size(855, 306);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileNew);
			this.TopPanel.Controls.Add(this.cmdFileDelete);
			this.TopPanel.Size = new System.Drawing.Size(855, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(171, 30);
			this.HeaderText.Text = "Edit Operators";
			// 
			// fraMainOptions
			// 
			this.fraMainOptions.AppearanceKey = "groupBoxNoBorders";
			this.fraMainOptions.Controls.Add(this.txtDeptDiv);
			this.fraMainOptions.Controls.Add(this.txtPassword);
			this.fraMainOptions.Controls.Add(this.txtName);
			this.fraMainOptions.Controls.Add(this.cmbTellerID);
			this.fraMainOptions.Controls.Add(this.lblDeptDiv);
			this.fraMainOptions.Controls.Add(this.lblPassword);
			this.fraMainOptions.Controls.Add(this.lblName);
			this.fraMainOptions.Controls.Add(this.lblTellerID);
			this.fraMainOptions.Location = new System.Drawing.Point(0, 0);
			this.fraMainOptions.Name = "fraMainOptions";
			this.fraMainOptions.Size = new System.Drawing.Size(429, 297);
			this.fraMainOptions.TabIndex = 0;
			// 
			// txtDeptDiv
			// 
			this.txtDeptDiv.AutoSize = false;
			this.txtDeptDiv.BackColor = System.Drawing.SystemColors.Window;
			this.txtDeptDiv.LinkItem = null;
			this.txtDeptDiv.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDeptDiv.LinkTopic = null;
			this.txtDeptDiv.Location = new System.Drawing.Point(163, 210);
			this.txtDeptDiv.MaxLength = 50;
			this.txtDeptDiv.Name = "txtDeptDiv";
			this.txtDeptDiv.Size = new System.Drawing.Size(238, 40);
			this.txtDeptDiv.TabIndex = 7;
			// 
			// txtPassword
			// 
			this.txtPassword.AutoSize = false;
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword.LinkItem = null;
			this.txtPassword.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPassword.LinkTopic = null;
			this.txtPassword.Location = new System.Drawing.Point(163, 150);
			this.txtPassword.MaxLength = 10;
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(238, 40);
			this.txtPassword.TabIndex = 5;
			this.txtPassword.Enter += new System.EventHandler(this.txtPassword_Enter);
			// 
			// txtName
			// 
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.LinkItem = null;
			this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName.LinkTopic = null;
			this.txtName.Location = new System.Drawing.Point(163, 90);
			this.txtName.MaxLength = 30;
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(238, 40);
			this.txtName.TabIndex = 3;
			this.txtName.Enter += new System.EventHandler(this.txtName_Enter);
			// 
			// cmbTellerID
			// 
			this.cmbTellerID.AutoSize = false;
			this.cmbTellerID.BackColor = System.Drawing.SystemColors.Window;
			this.cmbTellerID.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTellerID.FormattingEnabled = true;
			this.cmbTellerID.Location = new System.Drawing.Point(163, 30);
			this.cmbTellerID.Name = "cmbTellerID";
			this.cmbTellerID.Size = new System.Drawing.Size(238, 40);
			this.cmbTellerID.TabIndex = 1;
			this.cmbTellerID.SelectedIndexChanged += new System.EventHandler(this.cmbTellerID_SelectedIndexChanged);
			this.cmbTellerID.DropDown += new System.EventHandler(this.cmbTellerID_DropDown);
			this.cmbTellerID.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbTellerID_KeyDown);
			// 
			// lblDeptDiv
			// 
			this.lblDeptDiv.Location = new System.Drawing.Point(30, 224);
			this.lblDeptDiv.Name = "lblDeptDiv";
			this.lblDeptDiv.Size = new System.Drawing.Size(62, 18);
			this.lblDeptDiv.TabIndex = 6;
			this.lblDeptDiv.Text = "DEPT/DIV";
			// 
			// lblPassword
			// 
			this.lblPassword.Location = new System.Drawing.Point(30, 164);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(75, 23);
			this.lblPassword.TabIndex = 4;
			this.lblPassword.Text = "PASSWORD";
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(30, 104);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(46, 18);
			this.lblName.TabIndex = 2;
			this.lblName.Text = "NAME";
			// 
			// lblTellerID
			// 
			this.lblTellerID.Location = new System.Drawing.Point(30, 44);
			this.lblTellerID.Name = "lblTellerID";
			this.lblTellerID.Size = new System.Drawing.Size(75, 27);
			this.lblTellerID.TabIndex = 0;
			this.lblTellerID.Text = "TELLER ID";
			// 
			// cmdMV
			// 
			this.cmdMV.AppearanceKey = "actionButton";
			this.cmdMV.Location = new System.Drawing.Point(0, 212);
			this.cmdMV.Name = "cmdMV";
			this.cmdMV.Size = new System.Drawing.Size(355, 40);
			this.cmdMV.TabIndex = 1;
			this.cmdMV.Text = "Tie Users To Motor Vehicle OpIDs";
			this.cmdMV.Click += new System.EventHandler(this.cmdMV_Click);
			// 
			// fraGRID
			// 
			this.fraGRID.Controls.Add(this.cmdMV);
			this.fraGRID.Controls.Add(this.GRID);
			this.fraGRID.Location = new System.Drawing.Point(441, 30);
			this.fraGRID.Name = "fraGRID";
			this.fraGRID.Size = new System.Drawing.Size(382, 259);
			this.fraGRID.TabIndex = 11;
			// 
			// GRID
			// 
			this.GRID.AllowSelection = false;
			this.GRID.AllowUserToResizeColumns = false;
			this.GRID.AllowUserToResizeRows = false;
			this.GRID.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GRID.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GRID.BackColorAlternate = System.Drawing.Color.Empty;
			this.GRID.BackColorBkg = System.Drawing.Color.Empty;
			this.GRID.BackColorFixed = System.Drawing.Color.Empty;
			this.GRID.BackColorSel = System.Drawing.Color.Empty;
			this.GRID.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GRID.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GRID.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GRID.ColumnHeadersHeight = 30;
			this.GRID.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GRID.DefaultCellStyle = dataGridViewCellStyle2;
			this.GRID.DragIcon = null;
			this.GRID.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GRID.ExtendLastCol = true;
			this.GRID.ForeColorFixed = System.Drawing.Color.Empty;
			this.GRID.FrozenCols = 0;
			this.GRID.GridColor = System.Drawing.Color.Empty;
			this.GRID.GridColorFixed = System.Drawing.Color.Empty;
			this.GRID.Location = new System.Drawing.Point(0, 0);
			this.GRID.Name = "GRID";
			this.GRID.ReadOnly = true;
			this.GRID.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GRID.RowHeightMin = 0;
			this.GRID.Rows = 6;
			this.GRID.ScrollTipText = null;
			this.GRID.ShowColumnVisibilityMenu = false;
			this.GRID.ShowFocusCell = false;
			this.GRID.Size = new System.Drawing.Size(355, 192);
			this.GRID.StandardTab = true;
			this.GRID.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GRID.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GRID.TabIndex = 0;
			this.GRID.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GRID_ChangeEdit);
			this.GRID.CurrentCellChanged += new System.EventHandler(this.GRID_RowColChange);
			this.GRID.Enter += new System.EventHandler(this.GRID_Enter);
			// 
			// fraNew
			// 
			this.fraNew.Controls.Add(this.cmdCancelNew);
			this.fraNew.Controls.Add(this.cmdAddNew);
			this.fraNew.Controls.Add(this.txtNew);
			this.fraNew.Location = new System.Drawing.Point(30, 30);
			this.fraNew.Name = "fraNew";
			this.fraNew.Size = new System.Drawing.Size(239, 134);
			this.fraNew.TabIndex = 10;
			this.fraNew.Text = "New Teller";
			this.fraNew.Visible = false;
			// 
			// cmdCancelNew
			// 
			this.cmdCancelNew.AppearanceKey = "actionButton";
			this.cmdCancelNew.ForeColor = System.Drawing.Color.White;
			this.cmdCancelNew.Location = new System.Drawing.Point(108, 86);
			this.cmdCancelNew.Name = "cmdCancelNew";
			this.cmdCancelNew.Size = new System.Drawing.Size(72, 40);
			this.cmdCancelNew.TabIndex = 2;
			this.cmdCancelNew.Text = "Cancel";
			this.cmdCancelNew.Click += new System.EventHandler(this.cmdCancelNew_Click);
			// 
			// cmdAddNew
			// 
			this.cmdAddNew.AppearanceKey = "actionButton";
			this.cmdAddNew.ForeColor = System.Drawing.Color.White;
			this.cmdAddNew.Location = new System.Drawing.Point(20, 86);
			this.cmdAddNew.Name = "cmdAddNew";
			this.cmdAddNew.Size = new System.Drawing.Size(72, 40);
			this.cmdAddNew.TabIndex = 1;
			this.cmdAddNew.Text = "Add";
			this.cmdAddNew.Click += new System.EventHandler(this.cmdAddNew_Click);
			// 
			// txtNew
			// 
			this.txtNew.AutoSize = false;
			this.txtNew.BackColor = System.Drawing.SystemColors.Window;
            this.txtNew.CharacterCasing = CharacterCasing.Upper;
            this.txtNew.LinkItem = null;
			this.txtNew.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNew.LinkTopic = null;
			this.txtNew.Location = new System.Drawing.Point(20, 30);
			this.txtNew.MaxLength = 3;
			this.txtNew.Name = "txtNew";
			this.txtNew.Size = new System.Drawing.Size(201, 40);
			this.txtNew.TabIndex = 0;
			this.txtNew.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtNew.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNew_KeyDown);
			this.txtNew.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNew_KeyPress);
			this.txtNew.Enter += new System.EventHandler(this.txtNew_Enter);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileNew,
				this.mnuFileDelete,
				this.mnuSepar,
				this.mnuFileSave,
				this.mnuSAveExit,
				this.mnuSeperator,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileNew
			// 
			this.mnuFileNew.Index = 0;
			this.mnuFileNew.Name = "mnuFileNew";
			this.mnuFileNew.Text = "New";
			this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// mnuFileDelete
			// 
			this.mnuFileDelete.Index = 1;
			this.mnuFileDelete.Name = "mnuFileDelete";
			this.mnuFileDelete.Text = "Delete";
			this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 2;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 3;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuSAveExit
			// 
			this.mnuSAveExit.Index = 4;
			this.mnuSAveExit.Name = "mnuSAveExit";
			this.mnuSAveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSAveExit.Text = "Save & Exit";
			this.mnuSAveExit.Click += new System.EventHandler(this.mnuSAveExit_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 5;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(387, 30);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(80, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Save";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFileDelete
			// 
			this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDelete.AppearanceKey = "toolbarButton";
			this.cmdFileDelete.Location = new System.Drawing.Point(764, 29);
			this.cmdFileDelete.Name = "cmdFileDelete";
			this.cmdFileDelete.Size = new System.Drawing.Size(59, 25);
			this.cmdFileDelete.TabIndex = 1;
			this.cmdFileDelete.Text = "Delete";
			this.cmdFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdFileNew
			// 
			this.cmdFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileNew.AppearanceKey = "toolbarButton";
			this.cmdFileNew.Location = new System.Drawing.Point(712, 29);
			this.cmdFileNew.Name = "cmdFileNew";
			this.cmdFileNew.Size = new System.Drawing.Size(48, 25);
			this.cmdFileNew.TabIndex = 2;
			this.cmdFileNew.Text = "New";
			this.cmdFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// frmTellerSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(855, 474);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmTellerSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Edit Operators";
			this.Load += new System.EventHandler(this.frmTellerSetup_Load);
			this.Activated += new System.EventHandler(this.frmTellerSetup_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTellerSetup_KeyDown);
			this.Resize += new System.EventHandler(this.frmTellerSetup_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMainOptions)).EndInit();
			this.fraMainOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraGRID)).EndInit();
			this.fraGRID.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraNew)).EndInit();
			this.fraNew.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
		private FCButton cmdFileDelete;
		private FCButton cmdFileNew;
	}
}
