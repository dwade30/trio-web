//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmCommandLine.
	/// </summary>
	public partial class frmCommandLine : fecherFoundation.FCForm
	{
;

		public frmCommandLine()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.optExample = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optExample.AddControlArrayElement(optExample_0, 0 );
			this.optExample.AddControlArrayElement(optExample_1, 1 );
			this.optExample.AddControlArrayElement(optExample_2, 2 );
			this.optExample.AddControlArrayElement(optExample_3, 3 );

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		double dblResolution;


		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			frmCommandLine.InstancePtr.Unload();
		}
		// 
		// Private Sub cmdExamples_Click()
		// If cmdExamples.Caption = "See Examples" Then
		// fraExamples.Visible = True
		// cmdExamples.Caption = "Hide Examples"
		// Else
		// HideFrames
		// cmdExamples.Caption = "See Examples"
		// End If
		// End Sub

		private void cmdExecute_Click(object sender, System.EventArgs e)
		{
			try
			{	// On Error GoTo ErrorHandler

				string CommandLine = "";
				string strExecutable = "";

				if (txtCommand.Text!="") {
					if (Strings.UCase(Strings.Trim(txtCommand.Text))=="TRIOFIX") {
						frmFixProcess.InstancePtr.Show();
						this.Unload();
						return;
					}
					CommandLine = txtCommand.Text;
					if (CommandLine.Length>=3) {
						if (Strings.UCase(Strings.Mid(CommandLine, 1, 3))=="DIR") {
							// print the results of the shell command into
							// a textbox or listbox or some kind of visual
							// control so the user can view what actually
							// has happened.
						}
					}

					
					Scripting.File FilePath;
					string flPath = "";
					string WindowsPath = "";
					string DrivePath = "";
					if (!modSysInfo.IsNTBased()) {
						WindowsPath = ff.GetSpecialFolder(SpecialFolderConst.WindowsFolder).Path;
						strExecutable = "\\command.com";
					} else {
						WindowsPath = ff.GetSpecialFolder(SpecialFolderConst.SystemFolder).Path;
						strExecutable = "\\cmd.exe";
					}
					if (ff.FileExists(WindowsPath+"\\command.com")) {
						flPath = WindowsPath+strExecutable;
					} else {
						DrivePath = ff.GetDriveName(WindowsPath);
						if (ff.FileExists(DrivePath+strExecutable)) {
							flPath = DrivePath+strExecutable;
						} else {
							MessageBox.Show("Couldn't find "+strExecutable, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}


					Interaction.Shell(flPath+" /k "+CommandLine, AppWinStyle.NormalFocus, false, -1);

				}
				return;

			}
			catch (Exception ex)
            {	// ErrorHandler:
				if (Information.Err(ex).Number==53) {
					MessageBox.Show("Could not find the file Command.com needed to run dos commands.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				} else {
					MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}
		public void cmdExecute_Click()
		{
			cmdExecute_Click(cmdExecute, new System.EventArgs());
		}


		private void Command1_Click(object sender, System.EventArgs e)
		{
			string strTemp;

			try
			{	// On Error GoTo ErrorHandler

				strTemp = txtCommand.Text;
				if (Strings.UCase(Strings.Right("   "+strTemp, 3))=="EXE") {
					if (Interaction.Shell(txtCommand.Text, AppWinStyle.NormalFocus, false, -1)==0) {
						MessageBox.Show("Could not shell to "+txtCommand.Text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					return;
				} else {
					MessageBox.Show("This function can only be done with executable files", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In Command1_Click");
			}
		}

		private void frmCommandLine_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCommandLine properties;
			//frmCommandLine.ScaleWidth	= 9045;
			//frmCommandLine.ScaleHeight	= 7620;
			//frmCommandLine.LinkTopic	= "Form1";
			//frmCommandLine.LockControls	= true;
			//End Unmaped Properties

			dblResolution = 1;
			if (FCGlobal.Screen.Width<10000) {
				dblResolution = 1;
			} else if (FCGlobal.Screen.Width<13000) {
				dblResolution = 1.3;
			} else if (FCGlobal.Screen.Width<16000) {
				dblResolution = 1.6;
			}
			lstResults.Height = Convert.ToInt32(3180*dblResolution);
			lstResults.Width = Convert.ToInt32(8625*dblResolution);
			lstResults.Left = 270;
			lstResults.Top = 2790;
			// 
			// cmdExamples.Caption = "See Examples"
			// HideFrames

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			Label16.ForeColor = ColorTranslator.FromOle(modGlobalConstants.TRIOCOLORBLUE);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void optExample_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index) {
				
				case 0:
				{
					fraCopy.Visible = true;
					fraDirectory.Visible = false;
					fraPkzip.Visible = false;
					fraUnzip.Visible = false;
					break;
				}
				case 1:
				{
					fraCopy.Visible = false;
					fraDirectory.Visible = false;
					fraPkzip.Visible = true;
					fraUnzip.Visible = false;
					break;
				}
				case 2:
				{
					fraCopy.Visible = false;
					fraDirectory.Visible = false;
					fraPkzip.Visible = false;
					fraUnzip.Visible = true;
					break;
				}
				case 3:
				{
					fraCopy.Visible = false;
					fraDirectory.Visible = true;
					fraPkzip.Visible = false;
					fraUnzip.Visible = false;
					break;
				}
			} //end switch
		}
		private void optExample_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = optExample.GetIndex((FCRadioButton)sender);
			optExample_CheckedChanged(index, sender, e);
		}

		public void HideFrames()
		{
			fraCopy.Visible = false;
			fraDirectory.Visible = false;
			fraExamples.Visible = false;
			fraPkzip.Visible = false;
			fraUnzip.Visible = false;
		}

		private void txtCommand_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			if (KeyCode==Keys.Return) {
				KeyCode = (Keys)0;
				if (Strings.UCase(txtCommand.Text)=="UPDATE REDBOOK") {
					// Call Update_Redbook
				} else {
					cmdExecute_Click();
				}
			}
		}


		private void cmbExample_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbExample.SelectedIndex == 0)
			{
				optExample_CheckedChanged(sender, e);
			}
		}
	}
}
