﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPassword.
	/// </summary>
	partial class frmPassword : BaseForm
	{
		public fecherFoundation.FCTextBox txtUserId;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtUserId = new fecherFoundation.FCTextBox();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 263);
			this.BottomPanel.Size = new System.Drawing.Size(400, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtUserId);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Controls.Add(this.txtPassword);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(400, 203);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(400, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "TRIO Password";
			// 
			// txtUserId
			// 
			this.txtUserId.BackColor = System.Drawing.SystemColors.Window;
			this.txtUserId.Location = new System.Drawing.Point(164, 30);
			this.txtUserId.Name = "txtUserId";
			this.txtUserId.Size = new System.Drawing.Size(193, 40);
			this.txtUserId.TabIndex = 1;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(140, 150);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(88, 40);
			this.cmdCancel.TabIndex = 5;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.DialogResult = Wisej.Web.DialogResult.OK;
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(30, 150);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.RightToLeft = Wisej.Web.RightToLeft.No;
			this.cmdOK.Size = new System.Drawing.Size(91, 40);
			this.cmdOK.TabIndex = 4;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// txtPassword
			// 
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword.Location = new System.Drawing.Point(164, 90);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(193, 40);
			this.txtPassword.TabIndex = 3;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(106, 26);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "PASSWORD";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(106, 26);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "USER ID";
			// 
			// frmPassword
			// 
			this.AcceptButton = this.cmdOK;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(400, 371);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmPassword";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "TRIO Password";
			this.Load += new System.EventHandler(this.frmPassword_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPassword_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
