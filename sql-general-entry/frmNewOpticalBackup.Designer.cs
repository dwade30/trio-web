//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmNewOpticalBackup.
	/// </summary>
	partial class frmNewOpticalBackup : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbZip;
		public fecherFoundation.FCLabel lblZip;
		public fecherFoundation.FCComboBox cmbCloseDisk;
		public fecherFoundation.FCLabel lblCloseDisk;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optZip;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optCloseDisk;
		public fecherFoundation.FCFrame framBurnOptions;
		public fecherFoundation.FCComboBox cboWriteSpeed;
		public fecherFoundation.FCCheckBox chkNewDirectory;
		public fecherFoundation.FCTextBox txtNewDirectory;
		public fecherFoundation.FCTextBox txtTempDir;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCCheckBox chkCopyToLocal;
		public fecherFoundation.FCCheckBox chkDelete;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.PrintDialog CommonDialog1;
		public AxAbaleZipLibrary.AxAbaleZip AbaleZip1;
		public fecherFoundation.FCLabel lblProgress;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuBurn;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbZip = new fecherFoundation.FCComboBox();
			this.lblZip = new fecherFoundation.FCLabel();
			this.cmbCloseDisk = new fecherFoundation.FCComboBox();
			this.lblCloseDisk = new fecherFoundation.FCLabel();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmNewOpticalBackup));
			this.components = new System.ComponentModel.Container();
			//this.optZip = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			//this.optCloseDisk = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.framBurnOptions = new fecherFoundation.FCFrame();
			this.cboWriteSpeed = new fecherFoundation.FCComboBox();
			this.chkNewDirectory = new fecherFoundation.FCCheckBox();
			this.txtNewDirectory = new fecherFoundation.FCTextBox();
			this.txtTempDir = new fecherFoundation.FCTextBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.chkCopyToLocal = new fecherFoundation.FCCheckBox();
			this.chkDelete = new fecherFoundation.FCCheckBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.CommonDialog1 = new Wisej.Web.PrintDialog();
			this.AbaleZip1 = new AxAbaleZipLibrary.AxAbaleZip();
			this.lblProgress = new fecherFoundation.FCLabel();
			
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBurn = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//
			// cmbZip
			//
			this.cmbZip.Items.Add("Copy as single Zip file");
			this.cmbZip.Items.Add("Copy as multiple files");
			this.cmbZip.Name = "cmbZip";
			this.cmbZip.Location = new System.Drawing.Point(16, 23);
			this.cmbZip.Size = new System.Drawing.Size(120, 40);
			this.cmbZip.Text = "Copy as single Zip file";
			//
			// lblZip
			//
			this.lblZip.Name = "lblZip";
			this.lblZip.Text = "lblZip";
			this.lblZip.Location = new System.Drawing.Point(1, 23);
			this.lblZip.AutoSize = true;

			//
			// cmbCloseDisk
			//
			this.cmbCloseDisk.Items.Add("Close disk when done");
			this.cmbCloseDisk.Items.Add("Close session leave disk open");
			this.cmbCloseDisk.Name = "cmbCloseDisk";
			this.cmbCloseDisk.Location = new System.Drawing.Point(6, 24);
			this.cmbCloseDisk.Size = new System.Drawing.Size(120, 40);
			this.cmbCloseDisk.Text = "Close disk when done";
			//
			// lblCloseDisk
			//
			this.lblCloseDisk.Name = "lblCloseDisk";
			this.lblCloseDisk.Text = "lblCloseDisk";
			this.lblCloseDisk.Location = new System.Drawing.Point(1, 24);
			this.lblCloseDisk.AutoSize = true;

			//((System.ComponentModel.ISupportInitialize)(this.optZip)).BeginInit();
			//((System.ComponentModel.ISupportInitialize)(this.optCloseDisk)).BeginInit();
			//
			// framBurnOptions
			//
			this.framBurnOptions.Controls.Add(this.cboWriteSpeed);
			this.framBurnOptions.Controls.Add(this.chkNewDirectory);
			this.framBurnOptions.Controls.Add(this.txtNewDirectory);
			this.framBurnOptions.Controls.Add(this.txtTempDir);
			this.framBurnOptions.Controls.Add(this.cmdBrowse);
			this.framBurnOptions.Controls.Add(this.chkCopyToLocal);
			this.framBurnOptions.Controls.Add(this.chkDelete);
			this.framBurnOptions.Controls.Add(this.Label2);
			this.framBurnOptions.Controls.Add(this.Label3);
			this.framBurnOptions.Name = "framBurnOptions";
			this.framBurnOptions.TabIndex = 6;
			this.framBurnOptions.Location = new System.Drawing.Point(3, 105);
			this.framBurnOptions.Size = new System.Drawing.Size(389, 137);
			this.framBurnOptions.Text = "Burn Options";
			this.framBurnOptions.BackColor = System.Drawing.SystemColors.Control;
			this.framBurnOptions.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// cboWriteSpeed
			//
			this.cboWriteSpeed.Name = "cboWriteSpeed";
			this.cboWriteSpeed.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.cboWriteSpeed, "Choose what speed the drive writes at.  Some media may not be rated to record at the drives top speed.");
			this.cboWriteSpeed.Location = new System.Drawing.Point(10, 43);
			this.cboWriteSpeed.Size = new System.Drawing.Size(80, 22);
			this.cboWriteSpeed.Text = "Combo1";
			this.cboWriteSpeed.BackColor = System.Drawing.SystemColors.Window;
			//
			// chkNewDirectory
			//
			this.chkNewDirectory.Name = "chkNewDirectory";
			this.chkNewDirectory.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.chkNewDirectory, "Create a new directory to store the file(s) to backup in.");
			this.chkNewDirectory.Location = new System.Drawing.Point(10, 79);
			this.chkNewDirectory.Size = new System.Drawing.Size(145, 17);
			this.chkNewDirectory.Text = "Create New Directory";
			this.chkNewDirectory.BackColor = System.Drawing.SystemColors.Control;
			//
			// txtNewDirectory
			//
			this.txtNewDirectory.Name = "txtNewDirectory";
			this.txtNewDirectory.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtNewDirectory, "Create a new directory to store the file(s) to backup in.");
			this.txtNewDirectory.Location = new System.Drawing.Point(11, 100);
			this.txtNewDirectory.Size = new System.Drawing.Size(141, 23);
			this.txtNewDirectory.Text = "";
			this.txtNewDirectory.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtTempDir
			//
			this.txtTempDir.Name = "txtTempDir";
			this.txtTempDir.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtTempDir, "Zip files and networked files (if copying files is checked) will be copied here in this temp directory before writing to disk.");
			this.txtTempDir.Location = new System.Drawing.Point(173, 100);
			this.txtTempDir.Size = new System.Drawing.Size(166, 23);
			this.txtTempDir.Text = "";
			this.txtTempDir.BackColor = System.Drawing.SystemColors.Window;
			//
			// cmdBrowse
			//
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.cmdBrowse, "Browse for a suitable directory to use as a temp directory or create the temp directory in.");
			this.cmdBrowse.Location = new System.Drawing.Point(284, 77);
			this.cmdBrowse.Size = new System.Drawing.Size(54, 23);
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.BackColor = System.Drawing.SystemColors.Control;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			//
			// chkCopyToLocal
			//
			this.chkCopyToLocal.Name = "chkCopyToLocal";
			this.chkCopyToLocal.TabIndex = 8;
			this.chkCopyToLocal.Location = new System.Drawing.Point(172, 29);
			this.chkCopyToLocal.Size = new System.Drawing.Size(201, 19);
			this.chkCopyToLocal.Text = "Copy files to temp directory first";
			this.chkCopyToLocal.BackColor = System.Drawing.SystemColors.Control;
			this.chkCopyToLocal.Checked = true;
			this.chkCopyToLocal.CheckState = Wisej.Web.CheckState.Checked;
			//
			// chkDelete
			//
			this.chkDelete.Name = "chkDelete";
			this.chkDelete.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.chkDelete, "This will delete *ALL* files from the temp directory even if they were there before this backup process.");
			this.chkDelete.Location = new System.Drawing.Point(172, 48);
			this.chkDelete.Size = new System.Drawing.Size(211, 19);
			this.chkDelete.Text = "Delete files from temp when done";
			this.chkDelete.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.Label2, "Choose what speed the drive writes at.  Some media may not be rated to record at the drives top speed.");
			this.Label2.Location = new System.Drawing.Point(12, 20);
			this.Label2.Size = new System.Drawing.Size(79, 18);
			this.Label2.Text = "Write Speed";
			this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.Label3, "Zip files and networked files (if copying files is checked) will be copied here in this temp directory.");
			this.Label3.Location = new System.Drawing.Point(172, 80);
			this.Label3.Size = new System.Drawing.Size(110, 16);
			this.Label3.Text = "Temp directory";
			this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//
			//
			//
			//
			//
			// CommonDialog1
			//
			//
			// AbaleZip1
			//
			this.AbaleZip1.Name = "AbaleZip1";
			this.AbaleZip1.Location = new System.Drawing.Point(0, 24);
			this.AbaleZip1.BasePath = "";
			this.AbaleZip1.CompressionLevel = 6;
			this.AbaleZip1.EncryptionPassword = "";
			this.AbaleZip1.RequiredFileAttributes = 0;
			this.AbaleZip1.ExcludedFileAttributes = 24;
			this.AbaleZip1.FilesToProcess = "";
			this.AbaleZip1.FilesToExclude = "";
			this.AbaleZip1.MinDateToProcess = 2;
			this.AbaleZip1.MaxDateToProcess = 2958465;
			this.AbaleZip1.MinSizeToProcess = 0;
			this.AbaleZip1.MaxSizeToProcess = 0;
			this.AbaleZip1.SplitSize = 0;
			this.AbaleZip1.PreservePaths = true;
			this.AbaleZip1.ProcessSubfolders = false;
			this.AbaleZip1.SkipIfExisting = false;
			this.AbaleZip1.SkipIfNotExisting = false;
			this.AbaleZip1.SkipIfOlderDate = false;
			this.AbaleZip1.SkipIfOlderVersion = false;
			this.AbaleZip1.TempFolder = "";
			this.AbaleZip1.UseTempFile = true;
			this.AbaleZip1.UnzipToFolder = "";
			this.AbaleZip1.ZipFilename = "";
			this.AbaleZip1.SpanMultipleDisks = 2;
			this.AbaleZip1.ExtraHeaders = 10;
			this.AbaleZip1.ZipOpenedFiles = false;
			this.AbaleZip1.SfxBinaryModule = "";
			this.AbaleZip1.DeleteZippedFiles = false;
			this.AbaleZip1.FirstDiskFreeSpace = 0;
			this.AbaleZip1.MinDiskFreeSpace = 0;
			this.AbaleZip1.EventsToTrigger = 4194303;
			this.AbaleZip1.CompressionMethod = 8;
			//
			// lblProgress
			//
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.TabIndex = 16;
			this.lblProgress.Location = new System.Drawing.Point(10, 254);
			this.lblProgress.Size = new System.Drawing.Size(376, 30);
			this.lblProgress.Text = "";
			this.lblProgress.BackColor = System.Drawing.SystemColors.Control;
			this.lblProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(361, 31);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuBurn, this.Seperator, this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "&File";
			//
			// mnuBurn
			//
			this.mnuBurn.Name = "mnuBurn";
			this.mnuBurn.Text = "Burn Media";
			this.mnuBurn.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuBurn.Click += new System.EventHandler(this.mnuBurn_Click);
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "E&xit    (Esc)";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuProcess});
			//
			// frmNewOpticalBackup
			//
			this.ClientSize = new System.Drawing.Size(396, 280);
			this.Controls.Add(this.framBurnOptions);
			this.Controls.Add(this.cmbZip);
			this.Controls.Add(this.lblZip);

			this.Controls.Add(this.cmbCloseDisk);
			this.Controls.Add(this.lblCloseDisk);

			this.Controls.Add(this.AbaleZip1);
			this.Controls.Add(this.lblProgress);
			//this.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmNewOpticalBackup";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmNewOpticalBackup.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewOpticalBackup_KeyDown);
			this.Load += new System.EventHandler(this.frmNewOpticalBackup_Load);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmNewOpticalBackup_FormClosing);
			this.Text = "Optical Backup";
			//((System.ComponentModel.ISupportInitialize)(this.optZip)).EndInit();
			//((System.ComponentModel.ISupportInitialize)(this.optCloseDisk)).EndInit();
			this.framBurnOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AbaleZip1)).EndInit();
			//((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}