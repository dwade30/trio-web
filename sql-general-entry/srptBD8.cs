﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using System;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD8.
	/// </summary>
	public partial class srptBD8 : FCSectionReport
	{
		public srptBD8()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD8";
		}

		public static srptBD8 InstancePtr
		{
			get
			{
				return (srptBD8)Sys.GetInstance(typeof(srptBD8));
			}
		}

		protected srptBD8 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD8	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT * FROM AuditInfo ORDER BY AuditDate", "CentralData");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				// Me.Cancel
				//this.Stop();
				this.Cancel();
				return;
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lblVendors.Text = Strings.Format(rsInfo.Get_Fields_DateTime("AuditDate"), "MM/dd/yyyy");
			txtAccounts.Text = Strings.Format(rsInfo.Get_Fields_Int32("BDVendors"), "#,##0");
		}

		

		private void srptBD8_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
