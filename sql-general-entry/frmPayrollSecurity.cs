﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPayrollSecurity.
	/// </summary>
	public partial class frmPayrollSecurity : BaseForm
	{
		public frmPayrollSecurity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPayrollSecurity InstancePtr
		{
			get
			{
				return (frmPayrollSecurity)Sys.GetInstance(typeof(frmPayrollSecurity));
			}
		}

		protected frmPayrollSecurity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLTYPE = 1;
		const int CNSTGRIDCOLUSER = 2;
		const int CNSTGRIDCOLVAL = 3;
		const int CNSTGRIDCOLUSERID = 4;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;

		private void frmPayrollSecurity_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				case Keys.F2:
					{
						KeyCode = (Keys)0;
						if (Grid.Col == CNSTGRIDCOLVAL && Grid.Row > 0)
						{
							int lngRow = 0;
							string strTemp = "";
							lngRow = Grid.Row;
							strTemp = frmPayrollSelectItem.InstancePtr.Init(FCConvert.ToInt16(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)));
							if (strTemp != string.Empty)
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLVAL, strTemp);
								Grid.EditText = strTemp;
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmPayrollSecurity_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPayrollSecurity properties;
			//frmPayrollSecurity.FillStyle	= 0;
			//frmPayrollSecurity.ScaleWidth	= 5880;
			//frmPayrollSecurity.ScaleHeight	= 4305;
			//frmPayrollSecurity.LinkTopic	= "Form2";
			//frmPayrollSecurity.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void frmPayrollSecurity_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// VB6 Bad Scope Dim:
			int lngRow;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						//e.KeyCode = 0;
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(0));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(0));
						Grid.RowData(lngRow, true);
						break;
					}
				case Keys.Delete:
					{
						//e.KeyCode = 0;
						if (Grid.Row < 1)
							return;
						GridDelete.Rows += 1;
						GridDelete.TextMatrix(GridDelete.Rows - 1, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLAUTOID))));
						Grid.RemoveItem(Grid.Row);
						break;
					}
			}
			//end switch
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			Grid.RowData(Grid.GetFlexRowIndex(e.RowIndex), true);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.Cols = 5;
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			Grid.ColHidden(CNSTGRIDCOLUSERID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLTYPE, "Type");
			string strTemp = "";
			strTemp = "#0;Group|#1;Employee|#2;Sequence|#3;Dept-Div|#4;Department";
			Grid.ColComboList(CNSTGRIDCOLTYPE, strTemp);
			Grid.ColAlignment(CNSTGRIDCOLVAL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.TextMatrix(0, CNSTGRIDCOLUSER, "User");
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' order by userid", "clientsettings");
			strTemp = "";
			while (!rsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
				strTemp += "#" + rsLoad.Get_Fields_Int32("id") + ";" + rsLoad.Get_Fields("userid") + "|";
				rsLoad.MoveNext();
			}
			if (!(Strings.Trim(strTemp) == string.Empty))
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			Grid.ColComboList(CNSTGRIDCOLUSER, strTemp);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLTYPE, FCConvert.ToInt32(0.22 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLUSER, FCConvert.ToInt32(0.22 * GridWidth));
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				rsLoad.OpenRecordset("select * from payrollpermissions order by user,type,val", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					// TODO Get_Fields: Field [val] not found!! (maybe it is an alias?)
					Grid.TextMatrix(lngRow, CNSTGRIDCOLVAL, FCConvert.ToString(rsLoad.Get_Fields("val")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(rsLoad.Get_Fields("type")));
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUSER, FCConvert.ToString(rsLoad.Get_Fields("userid")));
					Grid.RowData(lngRow, false);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				this.Unload();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				SaveData = false;
				Grid.Row = 0;
				//Application.DoEvents();
				for (lngRow = 0; lngRow <= GridDelete.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from payrollpermissions where id = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(lngRow, 0))), "payroll");
				}
				// lngRow
				GridDelete.Rows = 0;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (FCConvert.ToBoolean(Grid.RowData(lngRow)))
					{
						rsSave.OpenRecordset("select * from payrollpermissions where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))), "payroll");
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
						rsSave.Set_Fields("userid", Grid.TextMatrix(lngRow, CNSTGRIDCOLUSER));
						rsSave.Set_Fields("val", Grid.TextMatrix(lngRow, CNSTGRIDCOLVAL));
						rsSave.Set_Fields("type", Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE));
						rsSave.Update();
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(rsSave.Get_Fields_Int32("id")));
					}
					Grid.RowData(lngRow, false);
				}
				// lngRow
				SaveData = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
	}
}
