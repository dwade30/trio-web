//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmUnzipFiles.
	/// </summary>
	partial class frmUnzipFiles : fecherFoundation.FCForm
	{
		public fecherFoundation.FCButton cmdRestore;
		public fecherFoundation.FCListBox lstFiles;
		public AxAbaleZipLibrary.AxAbaleZip AbaleZip1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
				_InstancePtr = null;
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmUnzipFiles));
			this.cmdRestore = new fecherFoundation.FCButton();
			this.lstFiles = new fecherFoundation.FCListBox();
			this.AbaleZip1 = new AxAbaleZipLibrary.AxAbaleZip();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblPercent = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			
			this.SuspendLayout();
			//
			// cmdRestore
			//
			this.cmdRestore.Name = "cmdRestore";
			this.cmdRestore.TabIndex = 1;
			this.cmdRestore.Location = new System.Drawing.Point(247, 218);
			this.cmdRestore.Size = new System.Drawing.Size(131, 23);
			this.cmdRestore.Text = "Process";
			this.cmdRestore.BackColor = System.Drawing.SystemColors.Control;
			this.cmdRestore.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdRestore.Click += new System.EventHandler(this.cmdRestore_Click);
			//
			// lstFiles
			//
			this.lstFiles.Name = "lstFiles";
			this.lstFiles.TabIndex = 0;
			this.lstFiles.Location = new System.Drawing.Point(10, 36);
			this.lstFiles.Size = new System.Drawing.Size(368, 166);
			this.lstFiles.BackColor = System.Drawing.SystemColors.Window;
			this.lstFiles.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// AbaleZip1
			//
			this.AbaleZip1.Name = "AbaleZip1";
			this.AbaleZip1.Location = new System.Drawing.Point(350, 8);
			this.AbaleZip1.BasePath = "";
			this.AbaleZip1.CompressionLevel = 6;
			this.AbaleZip1.EncryptionPassword = "";
			this.AbaleZip1.RequiredFileAttributes = 0;
			this.AbaleZip1.ExcludedFileAttributes = 24;
			this.AbaleZip1.FilesToProcess = "";
			this.AbaleZip1.FilesToExclude = "";
			this.AbaleZip1.MinDateToProcess = 2;
			this.AbaleZip1.MaxDateToProcess = 2958465;
			this.AbaleZip1.MinSizeToProcess = 0;
			this.AbaleZip1.MaxSizeToProcess = 0;
			this.AbaleZip1.SplitSize = 0;
			this.AbaleZip1.PreservePaths = true;
			this.AbaleZip1.ProcessSubfolders = false;
			this.AbaleZip1.SkipIfExisting = false;
			this.AbaleZip1.SkipIfNotExisting = false;
			this.AbaleZip1.SkipIfOlderDate = false;
			this.AbaleZip1.SkipIfOlderVersion = false;
			this.AbaleZip1.TempFolder = "";
			this.AbaleZip1.UseTempFile = true;
			this.AbaleZip1.UnzipToFolder = "";
			this.AbaleZip1.ZipFilename = "";
			this.AbaleZip1.SpanMultipleDisks = 2;
			this.AbaleZip1.ExtraHeaders = 10;
			this.AbaleZip1.ZipOpenedFiles = false;
			this.AbaleZip1.SfxBinaryModule = "";
			this.AbaleZip1.DeleteZippedFiles = false;
			this.AbaleZip1.FirstDiskFreeSpace = 0;
			this.AbaleZip1.MinDiskFreeSpace = 0;
			this.AbaleZip1.EventsToTrigger = 4194303;
			this.AbaleZip1.CompressionMethod = 8;
			this.AbaleZip1.FileStatus += new AxAbaleZipLibrary._IAbaleZipEvents_FileStatusEventHandler(this.AbaleZip1_FileStatus);
			this.AbaleZip1.InsertDisk += new AxAbaleZipLibrary._IAbaleZipEvents_InsertDiskEventHandler(this.AbaleZip1_InsertDisk);
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.Tag = "UnZipping File";
			this.Label3.TabIndex = 4;
			this.Label3.Location = new System.Drawing.Point(67, 255);
			this.Label3.Size = new System.Drawing.Size(256, 16);
			this.Label3.Text = "";
			this.Label3.BackColor = System.Drawing.SystemColors.Control;
			this.Label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label3.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblPercent
			//
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.Visible = false;
			this.lblPercent.TabIndex = 3;
			this.lblPercent.Location = new System.Drawing.Point(286, 255);
			this.lblPercent.Size = new System.Drawing.Size(102, 14);
			this.lblPercent.Text = "";
			this.lblPercent.BackColor = System.Drawing.SystemColors.Control;
			this.lblPercent.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.lblPercent.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label1
			//
			this.Label1.Name = "Label1";
			this.Label1.TabIndex = 2;
			this.Label1.Location = new System.Drawing.Point(4, 6);
			this.Label1.Size = new System.Drawing.Size(388, 17);
			this.Label1.Text = "Select files to Update.";
			this.Label1.BackColor = System.Drawing.SystemColors.Control;
			this.Label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.Label1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(253, 6);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// frmUnzipFiles
			//
			this.ClientSize = new System.Drawing.Size(396, 293);
			this.Controls.Add(this.cmdRestore);
			this.Controls.Add(this.lstFiles);
			this.Controls.Add(this.AbaleZip1);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.lblPercent);
			this.Controls.Add(this.Label1);
			//this.Controls.Add(this.vsElasticLight1);
			this.Name = "frmUnzipFiles";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmUnzipFiles.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.WindowsDefaultLocation;
			this.Activated += new System.EventHandler(this.frmUnzipFiles_Activated);
			this.Load += new System.EventHandler(this.frmUnzipFiles_Load);
			this.Text = "Files to Unzip";
			this.cmdRestore.ResumeLayout(false);
			this.lstFiles.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AbaleZip1)).EndInit();
			this.Label3.ResumeLayout(false);
			this.lblPercent.ResumeLayout(false);
			this.Label1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}