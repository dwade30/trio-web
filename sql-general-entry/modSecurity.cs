﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using SharedApplication.ClientSettings.Models;
using TWGNENTY.Authentication;

namespace TWGNENTY
{
	public class modSecurity
	{
		
		public static void CheckPasswords(AuthenticationService authenticationService)
		{
			// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
			DialogResult intReturn;
			 var intID = FCConvert.ToInt16(modReplaceWorkFiles.Statics.gintSecurityID) / 1;
			if (intID > 0)
			{
				User tUser;
				tUser = authenticationService.GetUserByID(intID);
				if (!(tUser == null))
				{
					frmPassword.InstancePtr.txtUserId.Text = tUser.UserID;
					TryOverTag:
					frmPassword.InstancePtr.txtPassword.Text = "";
					frmPassword.InstancePtr.txtPassword.Select();
					frmPassword.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					if (modGNBas.Statics.clsGESecurity.Get_OpID() != "SuperUser")
					{
						if (modGNBas.Statics.boolPasswordGood)
						{
							if (authenticationService.GetAuthenticatedUser(tUser.UserID, modGNBas.Statics.gstrPassword).result != AuthenticationResult.Success)
							{
								goto TryOverTag;
							}
							clsDRWrapper clstempdc = new clsDRWrapper();
							clstempdc.OpenRecordset("Select * from permissionstable where modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PASSWORDSETUP) + " and userid = " + FCConvert.ToString(intID), "systemsettings");
							if (clstempdc.EndOfFile())
							{
								MessageBox.Show("Error. No permissions found for this user", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								modGNBas.Statics.boolPasswordGood = false;
								return;
							}
							if (FCConvert.ToString(clstempdc.GetData("permission")) != "F")
							{
								intReturn = MessageBox.Show("Permission not set to edit security." + "\r\n" + "If you wanted to edit your password this option is found on the 'File -> Options -> Change User Password' menu option." + "\r\n" + "Would you like to edit your password now?", "Not Allowed", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (intReturn == DialogResult.Yes)
								{
									authenticationService.ChangePasswordNow(tUser);
								}
								modGNBas.Statics.boolPasswordGood = false;
								return;
							}
						}
						else
						{
							modGNBas.Statics.boolPasswordGood = false;
						}
					}
					else
					{
						modGNBas.Statics.boolPasswordGood = true;
					}
				}
			}
		}


	}
}
