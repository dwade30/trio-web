﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSetupAuditArchiveReport.
	/// </summary>
	partial class frmSetupAuditArchiveReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbNameSelected;
		public fecherFoundation.FCLabel lblNameSelected;
		public fecherFoundation.FCComboBox cmbDateAll;
		public fecherFoundation.FCLabel lblDateAll;
		public fecherFoundation.FCComboBox cmbScreenAll;
		public fecherFoundation.FCLabel lblScreenAll;
		public fecherFoundation.FCComboBox cmbPartyIdAll;
		public fecherFoundation.FCLabel lblPartyIdAll;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCFrame fraSelectedName;
		public fecherFoundation.FCComboBox cboName;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame fraSelectedScreen;
		public fecherFoundation.FCComboBox cboScreen;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraSelectedPartyId;
		public fecherFoundation.FCComboBox cboPartyID;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupAuditArchiveReport));
			this.cmbNameSelected = new fecherFoundation.FCComboBox();
			this.lblNameSelected = new fecherFoundation.FCLabel();
			this.cmbDateAll = new fecherFoundation.FCComboBox();
			this.lblDateAll = new fecherFoundation.FCLabel();
			this.cmbScreenAll = new fecherFoundation.FCComboBox();
			this.lblScreenAll = new fecherFoundation.FCLabel();
			this.cmbPartyIdAll = new fecherFoundation.FCComboBox();
			this.lblPartyIdAll = new fecherFoundation.FCLabel();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.fraSelectedName = new fecherFoundation.FCFrame();
			this.cboName = new fecherFoundation.FCComboBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtLowDate = new Global.T2KDateBox();
			this.txtHighDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.fraSelectedScreen = new fecherFoundation.FCFrame();
			this.cboScreen = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraSelectedPartyId = new fecherFoundation.FCFrame();
			this.cboPartyID = new fecherFoundation.FCComboBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedName)).BeginInit();
			this.fraSelectedName.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedScreen)).BeginInit();
			this.fraSelectedScreen.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedPartyId)).BeginInit();
			this.fraSelectedPartyId.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 465);
			this.BottomPanel.Size = new System.Drawing.Size(1008, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(1008, 405);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(1008, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(238, 30);
			this.HeaderText.Text = "Audit Archive Report";
			// 
			// cmbNameSelected
			// 
			this.cmbNameSelected.AutoSize = false;
			this.cmbNameSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNameSelected.FormattingEnabled = true;
			this.cmbNameSelected.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbNameSelected.Location = new System.Drawing.Point(184, 30);
			this.cmbNameSelected.Name = "cmbNameSelected";
			this.cmbNameSelected.Size = new System.Drawing.Size(299, 40);
			this.cmbNameSelected.TabIndex = 1;
			this.cmbNameSelected.Text = "All";
			this.cmbNameSelected.SelectedIndexChanged += new System.EventHandler(this.cmbNameSelected_SelectedIndexChanged);
			// 
			// lblNameSelected
			// 
			this.lblNameSelected.AutoSize = true;
			this.lblNameSelected.Location = new System.Drawing.Point(20, 44);
			this.lblNameSelected.Name = "lblNameSelected";
			this.lblNameSelected.Size = new System.Drawing.Size(112, 15);
			this.lblNameSelected.TabIndex = 0;
			this.lblNameSelected.Text = "NAME SELECTED";
			// 
			// cmbDateAll
			// 
			this.cmbDateAll.AutoSize = false;
			this.cmbDateAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDateAll.FormattingEnabled = true;
			this.cmbDateAll.Items.AddRange(new object[] {
				"All",
				"Date Range"
			});
			this.cmbDateAll.Location = new System.Drawing.Point(145, 30);
			this.cmbDateAll.Name = "cmbDateAll";
			this.cmbDateAll.Size = new System.Drawing.Size(301, 40);
			this.cmbDateAll.TabIndex = 1;
			this.cmbDateAll.Text = "All";
			this.cmbDateAll.SelectedIndexChanged += new System.EventHandler(this.cmbDateAll_SelectedIndexChanged);
			// 
			// lblDateAll
			// 
			this.lblDateAll.AutoSize = true;
			this.lblDateAll.Location = new System.Drawing.Point(20, 44);
			this.lblDateAll.Name = "lblDateAll";
			this.lblDateAll.Size = new System.Drawing.Size(66, 15);
			this.lblDateAll.TabIndex = 0;
			this.lblDateAll.Text = "DATE ALL";
			// 
			// cmbScreenAll
			// 
			this.cmbScreenAll.AutoSize = false;
			this.cmbScreenAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbScreenAll.FormattingEnabled = true;
			this.cmbScreenAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbScreenAll.Location = new System.Drawing.Point(161, 30);
			this.cmbScreenAll.Name = "cmbScreenAll";
			this.cmbScreenAll.Size = new System.Drawing.Size(234, 40);
			this.cmbScreenAll.TabIndex = 1;
			this.cmbScreenAll.Text = "All";
			this.cmbScreenAll.SelectedIndexChanged += new System.EventHandler(this.cmbScreenAll_SelectedIndexChanged);
			// 
			// lblScreenAll
			// 
			this.lblScreenAll.AutoSize = true;
			this.lblScreenAll.Location = new System.Drawing.Point(20, 44);
			this.lblScreenAll.Name = "lblScreenAll";
			this.lblScreenAll.Size = new System.Drawing.Size(84, 15);
			this.lblScreenAll.TabIndex = 0;
			this.lblScreenAll.Text = "SCREEN ALL";
			// 
			// cmbPartyIdAll
			// 
			this.cmbPartyIdAll.AutoSize = false;
			this.cmbPartyIdAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPartyIdAll.FormattingEnabled = true;
			this.cmbPartyIdAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbPartyIdAll.Location = new System.Drawing.Point(161, 30);
			this.cmbPartyIdAll.Name = "cmbPartyIdAll";
			this.cmbPartyIdAll.Size = new System.Drawing.Size(234, 40);
			this.cmbPartyIdAll.TabIndex = 1;
			this.cmbPartyIdAll.Text = "All";
			this.cmbPartyIdAll.SelectedIndexChanged += new System.EventHandler(this.cmbPartyIdAll_SelectedIndexChanged);
			// 
			// lblPartyIdAll
			// 
			this.lblPartyIdAll.AutoSize = true;
			this.lblPartyIdAll.Location = new System.Drawing.Point(20, 44);
			this.lblPartyIdAll.Name = "lblPartyIdAll";
			this.lblPartyIdAll.Size = new System.Drawing.Size(90, 15);
			this.lblPartyIdAll.TabIndex = 0;
			this.lblPartyIdAll.Text = "PARTY ID ALL";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.fraSelectedName);
			this.Frame4.Controls.Add(this.cmbNameSelected);
			this.Frame4.Controls.Add(this.lblNameSelected);
			this.Frame4.Location = new System.Drawing.Point(481, 30);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(501, 155);
			this.Frame4.TabIndex = 2;
			this.Frame4.Text = "Name";
			// 
			// fraSelectedName
			// 
			this.fraSelectedName.AppearanceKey = " groupBoxNoBorder";
			this.fraSelectedName.Controls.Add(this.cboName);
			this.fraSelectedName.Enabled = false;
			this.fraSelectedName.Location = new System.Drawing.Point(20, 90);
			this.fraSelectedName.Name = "fraSelectedName";
			this.fraSelectedName.Size = new System.Drawing.Size(475, 40);
			this.fraSelectedName.TabIndex = 2;
			// 
			// cboName
			// 
			this.cboName.AutoSize = false;
			this.cboName.BackColor = System.Drawing.SystemColors.Window;
			this.cboName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboName.Enabled = false;
			this.cboName.FormattingEnabled = true;
			this.cboName.Location = new System.Drawing.Point(164, 0);
			this.cboName.Name = "cboName";
			this.cboName.Size = new System.Drawing.Size(299, 40);
			this.cboName.TabIndex = 0;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.fraDateRange);
			this.Frame3.Controls.Add(this.cmbDateAll);
			this.Frame3.Controls.Add(this.lblDateAll);
			this.Frame3.Location = new System.Drawing.Point(481, 206);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(467, 153);
			this.Frame3.TabIndex = 3;
			this.Frame3.Text = "Date Range";
			// 
			// fraDateRange
			// 
			this.fraDateRange.AppearanceKey = "groupBoxNoBorders";
			this.fraDateRange.Controls.Add(this.txtLowDate);
			this.fraDateRange.Controls.Add(this.txtHighDate);
			this.fraDateRange.Controls.Add(this.Label2);
			this.fraDateRange.Enabled = false;
			this.fraDateRange.Location = new System.Drawing.Point(20, 90);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(426, 50);
			this.fraDateRange.TabIndex = 13;
			// 
			// txtLowDate
			// 
			this.txtLowDate.Enabled = false;
			this.txtLowDate.Location = new System.Drawing.Point(125, 0);
			this.txtLowDate.Mask = "##/##/####";
			this.txtLowDate.Name = "txtLowDate";
			this.txtLowDate.Size = new System.Drawing.Size(115, 40);
			this.txtLowDate.TabIndex = 0;
			this.txtLowDate.Text = "  /  /";
			// 
			// txtHighDate
			// 
			this.txtHighDate.Enabled = false;
			this.txtHighDate.Location = new System.Drawing.Point(313, 0);
			this.txtHighDate.Mask = "##/##/####";
			this.txtHighDate.Name = "txtHighDate";
			this.txtHighDate.Size = new System.Drawing.Size(115, 40);
			this.txtHighDate.TabIndex = 2;
			this.txtHighDate.Text = "  /  /";
			// 
			// Label2
			// 
			this.Label2.Enabled = false;
			this.Label2.Location = new System.Drawing.Point(267, 14);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(19, 22);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "TO";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.fraSelectedScreen);
			this.Frame2.Controls.Add(this.cmbScreenAll);
			this.Frame2.Controls.Add(this.lblScreenAll);
			this.Frame2.Location = new System.Drawing.Point(30, 206);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(418, 153);
			this.Frame2.TabIndex = 1;
			this.Frame2.Text = "Screen";
			// 
			// fraSelectedScreen
			// 
			this.fraSelectedScreen.AppearanceKey = " groupBoxNoBorder";
			this.fraSelectedScreen.Controls.Add(this.cboScreen);
			this.fraSelectedScreen.Enabled = false;
			this.fraSelectedScreen.Location = new System.Drawing.Point(20, 90);
			this.fraSelectedScreen.Name = "fraSelectedScreen";
			this.fraSelectedScreen.Size = new System.Drawing.Size(375, 62);
			this.fraSelectedScreen.TabIndex = 2;
			// 
			// cboScreen
			// 
			this.cboScreen.AutoSize = false;
			this.cboScreen.BackColor = System.Drawing.SystemColors.Window;
			this.cboScreen.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboScreen.Enabled = false;
			this.cboScreen.FormattingEnabled = true;
			this.cboScreen.Location = new System.Drawing.Point(141, 0);
			this.cboScreen.Name = "cboScreen";
			this.cboScreen.Size = new System.Drawing.Size(234, 40);
			this.cboScreen.TabIndex = 0;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.fraSelectedPartyId);
			this.Frame1.Controls.Add(this.cmbPartyIdAll);
			this.Frame1.Controls.Add(this.lblPartyIdAll);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(418, 155);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Party Id";
			// 
			// fraSelectedPartyId
			// 
			this.fraSelectedPartyId.AppearanceKey = " groupBoxNoBorder";
			this.fraSelectedPartyId.Controls.Add(this.cboPartyID);
			this.fraSelectedPartyId.Enabled = false;
			this.fraSelectedPartyId.Location = new System.Drawing.Point(20, 90);
			this.fraSelectedPartyId.Name = "fraSelectedPartyId";
			this.fraSelectedPartyId.Size = new System.Drawing.Size(391, 43);
			this.fraSelectedPartyId.TabIndex = 2;
			// 
			// cboPartyID
			// 
			this.cboPartyID.AutoSize = false;
			this.cboPartyID.BackColor = System.Drawing.SystemColors.Window;
			this.cboPartyID.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPartyID.Enabled = false;
			this.cboPartyID.FormattingEnabled = true;
			this.cboPartyID.Location = new System.Drawing.Point(141, 0);
			this.cboPartyID.Name = "cboPartyID";
			this.cboPartyID.Size = new System.Drawing.Size(234, 40);
			this.cboPartyID.TabIndex = 0;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(470, 27);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(131, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(922, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(48, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmSetupAuditArchiveReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1008, 573);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupAuditArchiveReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Audit Archive Report";
			this.Load += new System.EventHandler(this.frmSetupAuditArchiveReport_Load);
			this.Activated += new System.EventHandler(this.frmSetupAuditArchiveReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupAuditArchiveReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedName)).EndInit();
			this.fraSelectedName.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedScreen)).EndInit();
			this.fraSelectedScreen.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedPartyId)).EndInit();
			this.fraSelectedPartyId.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFilePreview;
		private FCButton cmdFilePrint;
	}
}
