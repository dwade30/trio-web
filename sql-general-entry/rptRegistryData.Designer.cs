namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptRegistryData.
	/// </summary>
	partial class rptRegistryData
	{

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRegistryData));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtData,
            this.txtName});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtDate,
            this.txtMuniname,
            this.txtPage,
            this.txtTime});
			this.PageHeader.Height = 0.5625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Registry Entries for TRIO Software";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.125F;
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.1875F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Text = "Field1";
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtData
			// 
			this.txtData.Height = 0.1875F;
			this.txtData.Left = 2F;
			this.txtData.Name = "txtData";
			this.txtData.Text = null;
			this.txtData.Top = 0F;
			this.txtData.Width = 4.4375F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 1.875F;
			// 
			// rptRegistryData
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtData;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
