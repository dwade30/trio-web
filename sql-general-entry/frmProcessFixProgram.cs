﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmProcessFixProgram.
	/// </summary>
	public partial class frmProcessFixProgram : BaseForm
	{
		long currentFileSize = 0;

		public frmProcessFixProgram()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//this.FTP1 = new Chilkat.Ftp2();
            this.FTP1 = new Chilkat.SFtp();
            FTP1.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
            //var testftp = new Chilkat.SFtp();
            //testftp.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
            //var success = testftp.Connect("64.26.137.209", 22);
            //if (success)
            //{
            //    success = testftp.AuthenticatePw(@"Bangor@trio", "WH9jFNpR");
            //}
            //else
            //{

            //}
			this.FTP1.EnableEvents = true;
			//this.FTP1.OnProgressInfo += new Chilkat.Ftp2.ProgressInfoEventHandler(FTP1_TransferProgress);
            this.FTP1.OnProgressInfo += new Chilkat.SFtp.ProgressInfoEventHandler(FTP1_TransferProgress);
            //this.FTP1.OnBeginDownloadFile += new Chilkat.Ftp2.BeginDownloadFileEventHandler(FTP1_TransferStarting);
           
            this.FTP1.OnDownloadRate += FTP1_OnDownloadRate;
			if (_InstancePtr == null)
				_InstancePtr = this;
		}


        private void FTP1_OnDownloadRate(object sender, Chilkat.DataRateEventArgs args)
		{
			double dblPerc = 0;
			// vbPorter upgrade warning: lngPerc As int	OnWrite(double, short)
			int lngPerc;
           
			try
			{
				if (args.ByteCount != 0)
				{
					dblPerc = FCConvert.ToDouble(args.ByteCount) / FCConvert.ToDouble(currentFileSize);
				}
				else
				{
					dblPerc = 0;
				}
				lngPerc = FCConvert.ToInt32(dblPerc * 100);
				if (lngPerc > 100)
					lngPerc = 100;
				ProgressBar1.Value = lngPerc;
				FCUtils.ApplicationUpdate(ProgressBar1);
				// Me.Refresh
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FTP1_TransferProgress line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmProcessFixProgram InstancePtr
		{
			get
			{
				return (frmProcessFixProgram)Sys.GetInstance(typeof(frmProcessFixProgram));
			}
		}

		protected frmProcessFixProgram _InstancePtr = null;
		string downloadfile;
		string strFTPSiteAddress = "";
		string strFTPSiteUser = "";
		string strFTPSitePassword = "";
		int FileCol;
		int SelectCol;
		int DescriptionCol;

		private void cmdDownload_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnFilesSelected;
			string strDataDest;
			bool boolEXEs;
			string strShellProgram = "";
			int intCounter;
			int intDownload;
            var fixes = new List<string>();
			try
			{
				// On Error GoTo ErrorHandler
				if (vsUpdates.Rows == 0)
				{
					/*? 4 */
					return;
				}
				intCounter = 0;
				blnFilesSelected = false;
				for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsUpdates.TextMatrix(counter, SelectCol)) == true)
					{
						intCounter += 1;
						blnFilesSelected = true;
					}
				}
				for (counter = 0; counter <= vsSupport.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsSupport.TextMatrix(counter, SelectCol)) == true)
					{
						intCounter += 1;
					}
				}
				if (!blnFilesSelected)
				{
					MessageBox.Show("You must select at least one update before you may proceed", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Information);
					/*? 28 */
					return;
				}
				// strDataDest = CurDir
				strDataDest = Application.StartupPath;
				if (Strings.Right(strDataDest, 1) != "\\")
				{
					strDataDest += "\\";
				}
				intDownload = 1;
				for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (FCConvert.CBool(vsUpdates.TextMatrix(counter, SelectCol)) == true)
					{
						// download exe file
						lblPercent.Text = "Downloading files:  " + FCConvert.ToString(intDownload) + " of " + FCConvert.ToString(intCounter);
						FCUtils.ApplicationUpdate(lblPercent);
						strShellProgram = strDataDest + vsUpdates.TextMatrix(counter, FileCol);
                        fixes.Add(strShellProgram);
                        DLFile(vsUpdates.TextMatrix(counter, FileCol), strDataDest, vsUpdates.TextMatrix(counter, FileCol));
						intDownload += 1;
					}
				}
				for (counter = 0; counter <= vsSupport.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (FCConvert.CBool(vsSupport.TextMatrix(counter, SelectCol)) == true)
					{
						// download exe file
						lblPercent.Text = "Downloading files:  " + FCConvert.ToString(intDownload) + " of " + FCConvert.ToString(intCounter);
						FCUtils.ApplicationUpdate(lblPercent);
						DLFile(vsSupport.TextMatrix(counter, FileCol), strDataDest, vsSupport.TextMatrix(counter, FileCol));
						intDownload += 1;
					}
				}
				lblPercent.Text = "";
				FCUtils.ApplicationUpdate(lblPercent);
				ProgressBar1.Value = 0;
				FCUtils.ApplicationUpdate(ProgressBar1);
				modTrioStart.Statics.Wait = 0;
				modGNBas.WriteYY();
                int processedFixes = 0;
                foreach (var fix in fixes)
                {
                    switch (fix.Right(3))
                    {
                        case "sql":
                            processedFixes += 1;
                            ProcessSQLFile(fix);
                            break;
                        case "exe":
                            processedFixes += 1;
                            ProcessEXEFix(fix);
                            break;
                    }
                    this.ZOrder(ZOrderConstants.BringToFront);
                    this.Focus();
                    FCFileSystem.Kill(fix);
                }

                if (processedFixes > 0)
                {
                    var fixMessage = (processedFixes == 1 ? "Fix process is" : "Fix processes are");
                    FCMessageBox.Show(fixMessage + " complete", MsgBoxStyle.OkOnly, "Process Finished");
                }
				for (counter = 0; counter <= vsSupport.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsSupport.TextMatrix(counter, SelectCol)) == true)
					{
						if (Strings.Right(strDataDest, 1) != "\\")
						{
							FCFileSystem.Kill(strDataDest + "\\" + vsSupport.TextMatrix(counter, FileCol));
						}
						else
						{
							FCFileSystem.Kill(strDataDest + vsSupport.TextMatrix(counter, FileCol));
						}
					}
				}
				// 68    Unload Me
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In cmdDownload " + "line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void ProcessEXEFix(string fileName)
        {
            var strConfigPath = Application.MapPath("\\");
            var clientEnvironment = StaticSettings.gGlobalSettings.ClientEnvironment;
            if (clientEnvironment.IsNullOrWhiteSpace())
            {
                clientEnvironment = "default";
            }
            var arguments = clientEnvironment + " " + StaticSettings.gGlobalSettings.DataEnvironment + " " + '"' + strConfigPath + '"';
            var exeWithArguments = fileName + " " + arguments;
            try
            {
                modTrioStart.Statics.Wait =
                    Interaction.Shell(exeWithArguments, System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                if (modTrioStart.Statics.Wait != 0)
                {
                    modTrioStart.WaitForTerm(ref modTrioStart.Statics.Wait);
                }
            }
            catch
            {

            }
        }

        private void ProcessSQLFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                var sqlScript = File.ReadAllText(fileName);
                sqlScript = sqlScript.Replace(@"{Environment_Name}", StaticSettings.gGlobalSettings.DataEnvironment);
                var rsSave = new clsDRWrapper();
                try
                {
                    rsSave.Execute(sqlScript, "SystemSettings");
                }
                catch
                {

                }
            }
        }

		private void frmProcessFixProgram_Activated(object sender, System.EventArgs e)
		{
			try
			{
				int counter;
				clsDRWrapper rsModules = new clsDRWrapper();
				bool boolHasBilling;
				if (modGlobalRoutines.FormExist(this))
				{
					return;
				}
				this.Refresh();
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Connecting to FTP Site");
				try
				{
                    //FTP1.Connect(strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21);
                    //var success = FTP1.Connect(strFTPSiteAddress, 22);
                    ////FTP1.Passive = true;
                    //FTP1.Hostname = strFTPSiteAddress;
                    //FTP1.Username = strFTPSiteUser;
                    //FTP1.Password = strFTPSitePassword;
                    //FTP1.VerboseLogging = true;
                    ////FTP1.Port = 990;
                    //FTP1.AuthTls = true;
                    //FTP1.Ssl = false;                                       

                    var success = FTP1.Connect(strFTPSiteAddress, 22);
                    if (success)
                    {
                        success = FTP1.AuthenticatePw(strFTPSiteUser, strFTPSitePassword);
                        if (success)
                        {
                            success = FTP1.InitializeSftp();
                        }
                    }

                    if (!success)
                    {
                        frmWait.InstancePtr.Unload();
                        MessageBox.Show("Connection failed", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        AddStatusMsg_2("Connection failed");
                        return;
                    }
                }
				catch (Exception ex)
				{
					Err_Connect:
					;
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("Connect failed: " + "\r\n" + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					AddStatusMsg_2("Connect failed: " + Information.Err(ex).Description);
					return;
				}
				//FTP1.ChangeRemoteDir("FixPrograms");
                //FTP1.OpenDir("FixPrograms");
				//FTP1.LCD = Environment.CurrentDirectory;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building Fix Program List", true, 25);
				RefreshSocket();
				frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Maximum;
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				//Application.DoEvents();
				if (Information.Err(ex).Number == -2147217408)
				{
					MessageBox.Show("Unable to locate Fix Programs directory.", "No Fix Program Directory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In form_activate line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void frmProcessFixProgram_Load(object sender, System.EventArgs e)
		{

			clsDRWrapper rsDefaults = new clsDRWrapper();
			FileCol = 0;
			SelectCol = 1;
			DescriptionCol = 2;
			/*? 8 */
			vsUpdates.ColHidden(FileCol, true);
			vsUpdates.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsUpdates.ColWidth(SelectCol, FCConvert.ToInt32(vsUpdates.WidthOriginal * 0.2));
			vsSupport.ColHidden(FileCol, true);
			vsSupport.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSupport.ColWidth(SelectCol, FCConvert.ToInt32(vsUpdates.WidthOriginal * 0.2));
			rsDefaults.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
			if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
			{
				strFTPSiteAddress = StaticSettings.gGlobalSettings.SFTPSiteAddress;
                strFTPSiteUser = FCConvert.ToString(rsDefaults.Get_Fields_String("FTPSiteUser")) + "@trio";
				strFTPSitePassword = FCConvert.ToString(rsDefaults.Get_Fields_String("FTPSitePassword"));
			}
			lstMessages.Clear();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			return;
			ErrorHandler:
			;
			MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In form_load line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		private void frmProcessFixProgram_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void RefreshSocket()
		{
			FCCollection loDirCollection = new FCCollection();
			//DevPowerFTP.dpFTPListItem loDirCollectionEntry = new DevPowerFTP.dpFTPListItem();
			try
			{
                // On Error GoTo ErrorHandler
                if (!FTP1.IsConnected)
                    return;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                string handle = FTP1.OpenDir(@"FixPrograms");
                var dirListing = FTP1.ReadDir(handle);
                var dirCount = dirListing.NumFilesAndDirs;
				for (int i = 0; i <= dirCount - 1; i++)
                {
                    var fileObj = dirListing.GetFileObject(i);
                    
					if (fileObj.IsDirectory)
					{
						// do nothing
					}
					else
                    {
                        string fileName = fileObj.Filename;
                        var extension = fileName.Right(3).ToUpper();

                        if (extension == "EXE" || extension == "SQL")
						{
							vsUpdates.Rows += 1;
							vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, fileName);
							vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
							vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, fileName);
						}
						else
						{
							vsSupport.Rows += 1;
							vsSupport.TextMatrix(vsSupport.Rows - 1, FileCol, fileName);
							vsSupport.TextMatrix(vsSupport.Rows - 1, SelectCol, FCConvert.ToString(false));
							vsSupport.TextMatrix(vsSupport.Rows - 1, DescriptionCol, fileName);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In RefreshSocket line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddStatusMsg_2(string Message)
		{
			AddStatusMsg(ref Message);
		}

		private void AddStatusMsg(ref string Message)
		{
			try
			{
				// On Error GoTo ErrorHandler
				lstMessages.AddItem(Message);
				lstMessages.SetSelected(lstMessages.ListCount - 1, true);
				lstMessages.Refresh();
				FCUtils.ApplicationUpdate(lstMessages);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AddStatusMsg line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsUpdates_ClickEvent(object sender, System.EventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo ErrorHandler
				if (vsUpdates.MouseRow >= 0)
				{
					if (FCConvert.CBool(vsUpdates.TextMatrix(vsUpdates.MouseRow, SelectCol)) == false)
					{
						vsUpdates.TextMatrix(vsUpdates.MouseRow, SelectCol, FCConvert.ToString(true));
						for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
						{
							if (counter != vsUpdates.Row)
							{
								vsUpdates.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
							}
						}
					}
					else
					{
						vsUpdates.TextMatrix(vsUpdates.MouseRow, SelectCol, FCConvert.ToString(false));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsUpdates_Click line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsUpdates_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo ErrorHandler
				if (e.KeyChar == 32)
				{
					e.KeyChar = (char)0;
					if (FCConvert.CBool(vsUpdates.TextMatrix(vsUpdates.Row, SelectCol)) == false)
					{
						vsUpdates.TextMatrix(vsUpdates.Row, SelectCol, FCConvert.ToString(true));
						for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
						{
							//Application.DoEvents();
							if (counter != vsUpdates.Row)
							{
								vsUpdates.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
							}
						}
					}
					else
					{
						vsUpdates.TextMatrix(vsUpdates.Row, SelectCol, FCConvert.ToString(false));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsUpdates_KeyPress line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsSupport_ClickEvent(object sender, System.EventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo ErrorHandler
				if (vsSupport.MouseRow >= 0)
				{
					if (FCConvert.CBool(vsSupport.TextMatrix(vsSupport.MouseRow, SelectCol)) == false)
					{
						vsSupport.TextMatrix(vsSupport.MouseRow, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsSupport.TextMatrix(vsSupport.MouseRow, SelectCol, FCConvert.ToString(false));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsSupport_Click line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsSupport_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo ErrorHandler
				if (e.KeyChar == 32)
				{
					e.KeyChar = (char)0;
					if (vsSupport.Row >= 0)
					{
						if (FCConvert.CBool(vsSupport.TextMatrix(vsSupport.Row, SelectCol)) == false)
						{
							vsSupport.TextMatrix(vsSupport.Row, SelectCol, FCConvert.ToString(true));
						}
						else
						{
							vsSupport.TextMatrix(vsSupport.Row, SelectCol, FCConvert.ToString(false));
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsSupport_KeyPress line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		//private void FTP1_ServerResponse(object sender, AxDevPowerFTP.__FTP_ServerResponseEvent e)
		//{
		//	AddStatusMsg(ref e.message);
		//}
		private void FTP1_TransferProgress(object sender, Chilkat.ProgressInfoEventArgs e)
		{
			AddStatusMsg_2(e.Name + ": " + e.Value);           
		}

		//private void FTP1_TransferStarting(object sender, Chilkat.FtpTreeEventArgs e)
		//{
		//	//currentFileSize = FTP1.GetSizeByName64(Path.GetFileName(e.Path));
  //          currentFileSize = FTP1.GetFileSize64(Path.GetFileName(e.Path),false,false);
		//	try
		//	{
		//		// On Error GoTo ErrorHandler
		//		ProgressBar1.Value = 0;
		//		// ProgressBar1.Max = TotalBytes
		//		ProgressBar1.Maximum = 100;
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ErrorHandler:
		//		MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In transferstarting line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}

		private bool DLFile(string FileName, string downloadpath, string downloadas)
		{
            StartOver:
            try
			{	
				downloadfile = downloadpath + "\\" + downloadas;
				if (FCFileSystem.Dir(downloadpath + "\\" + downloadas, 0) != "")
				{
					FCFileSystem.Kill(downloadpath + "\\" + downloadas);
				}

                //var success = FTP1.GetFile(FileName, Path.Combine(downloadpath, downloadas));
                var handle = FTP1.OpenFile(Path.Combine(@"FixPrograms" ,FileName), "readOnly", "openExisting");
                currentFileSize = FTP1.GetFileSize64(handle, false, true);
                var success = FTP1.DownloadFile(handle, Path.Combine(downloadpath, downloadas));

                return success;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				if (Information.Erl() == 8 && Information.Err(ex).Number == -2147217408)
				{
					/*? On Error Resume Next  *///FTP1.Connect(strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21);
					//FTP1.Connect();
					if (Information.Err(ex).Number != 0)
					{
						// give up
						MessageBox.Show("Connection Failed in DLFile" + "\r\n" + "Cannot download " + FileName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return false;
					}
					goto StartOver;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In DLFile " + "line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return false;
		}
	}
}
