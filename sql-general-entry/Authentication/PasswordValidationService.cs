﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.ClientSettings.Models;

namespace TWGNENTY.Authentication
{
	public class PasswordValidationService : IPasswordValidationService
	{
		private HashSet<string> _commonPasswords;

		public PasswordValidationService()
		{
			_commonPasswords = LoadPasswordList("Top10000CommonPasswords.txt");
		}

		public PasswordValidationResult IsValidPassword(string strPassword, IEnumerable<PasswordHistory> previousPasswords)
		{
			if (IsPasswordCommon(strPassword))
			{
				return PasswordValidationResult.CommonPassword;
			}
			else if (IsPreviousPassword(strPassword, previousPasswords))
			{
				return PasswordValidationResult.PreviousPassword;
			}
			else if (MeetsMinimumRequirements(strPassword))
			{
				return PasswordValidationResult.GoodPassword;
			}
			else
			{
				return PasswordValidationResult.DoesNotMeetMinimumRequirements;
			}
		}

		private bool IsPasswordCommon(string strPassword)
		{
			if (_commonPasswords.Contains(strPassword))
			{
				return true;
			}

			return false;
		}

		private bool IsPreviousPassword(string strPassword, IEnumerable<PasswordHistory> previousPasswords)
		{
			CryptoUtility cryptoUtility = new CryptoUtility();

			foreach (var previousPassword in previousPasswords)
			{
				var strCalcHash = cryptoUtility.ComputeSHA512Hash(strPassword, previousPassword.Salt ?? "");
				if (strCalcHash == previousPassword.Password)
				{
					return true;
				}
			}

			return false;
		}

		private bool MeetsMinimumRequirements(string strPassword)
		{
			if (strPassword.Length >= 8 && strPassword.Length <= 64)
			{
				return true;
			}

			return false;
		}

		private HashSet<string> LoadPasswordList(string resourceName)
		{
			HashSet<string> hashset;

			var assembly = typeof(PasswordValidationService).GetTypeInfo().Assembly;

			string fullResourceName = assembly.GetManifestResourceNames()
				.Single(str => str.EndsWith(resourceName));

			using (var stream = assembly.GetManifestResourceStream(fullResourceName))
			{
				using (var streamReader = new StreamReader(stream))
				{
					hashset = new HashSet<string>(
						GetLines(streamReader),
						StringComparer.OrdinalIgnoreCase);
				}
			}
			return hashset;
		}

		private IEnumerable<string> GetLines(StreamReader reader)
		{
			while (!reader.EndOfStream)
			{
				yield return reader.ReadLine();
			}
		}
	}
}
