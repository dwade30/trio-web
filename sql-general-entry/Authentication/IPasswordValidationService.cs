﻿using System.Collections.Generic;
using SharedApplication.ClientSettings.Models;

namespace TWGNENTY.Authentication
{
	public interface IPasswordValidationService
	{
		PasswordValidationResult IsValidPassword(string strPassword, IEnumerable<PasswordHistory> previousPasswords);
	}
}