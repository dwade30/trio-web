﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWGNENTY.Authentication
{
	public static class PasswordStrengthService
	{
		public static PasswordStrengthResult GetPasswordStrength(string password)
		{
			int score = 0;
			if (String.IsNullOrEmpty(password) || String.IsNullOrEmpty(password.Trim())) return PasswordStrengthResult.Blank;
			if (!HasMinimumLength(password, 5)) return PasswordStrengthResult.VeryWeak;
			if (HasMinimumLength(password, 8)) score++;
			if (HasUpperCaseLetter(password) && HasLowerCaseLetter(password)) score++;
			if (HasDigit(password)) score++;
			if (HasSpecialChar(password)) score++;
			return (PasswordStrengthResult)score + 1;
		}

		#region Helper Methods

		public static bool HasMinimumLength(string password, int minLength)
		{
			return password.Length >= minLength;
		}

		public static bool HasDigit(string password)
		{
			return password.Any(c => char.IsDigit(c));
		}

		public static bool HasSpecialChar(string password)
		{
			return password.IndexOfAny("!@#$%^&*?_~-£().,".ToCharArray()) != -1;
		}

		public static bool HasUpperCaseLetter(string password)
		{
			return password.Any(c => char.IsUpper(c));
		}

		public static bool HasLowerCaseLetter(string password)
		{
			return password.Any(c => char.IsLower(c));
		}
		#endregion
	}
}
