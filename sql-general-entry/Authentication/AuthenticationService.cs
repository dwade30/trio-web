﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;
using SharedApplication.SystemSettings.Interfaces;
using SharedDataAccess;

namespace TWGNENTY.Authentication
{
	public class AuthenticationService : IAuthenticationService
	{
		protected IUserDataAccess _userDataAccess;
		protected ISystemSettingsContext _systemSettingsContext;

		public AuthenticationService(IUserDataAccess passedUserDataAccess, ISystemSettingsContext passedSystemSettingsContext)
		{
			_userDataAccess = passedUserDataAccess;
			_systemSettingsContext = passedSystemSettingsContext;
		}

		public User GetUserByID(int intUserID)
		{
			return _userDataAccess.GetUserByID(intUserID);
		}

		public bool UnlockUser(User user)
		{
			try
			{
				user.FailedAttempts = 0;
				user.LockedOut = false;
				user.LockoutDateTime = null;

				_userDataAccess.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public IEnumerable<PasswordHistory> GetPreviousPasswords(int userId)
		{
			return _userDataAccess.GetPreviousPasswords(userId);
		}

		public bool LockUser(User user)
		{
			try
			{
				user.FailedAttempts = 0;
				user.LockedOut = true;
				user.LockoutDateTime = DateTime.Now;

				_userDataAccess.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public (User security, AuthenticationResult result) GetAuthenticatedUser(string strUser, string strPassword)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (strUser.Trim() != "" && strPassword.Trim() != "")
				{
					User tUser;
					tUser = _userDataAccess.GetUserByUserId(strUser);
					if (tUser != null)
					{
						if (tUser.Inactive ?? false)
						{
							return (security: null, result: AuthenticationResult.InactiveUser);
						}

						if (tUser.LockedOut ?? false)
						{
							if ((tUser.LockoutDateTime ?? DateTime.Now).AddMinutes(10) < DateTime.Now)
							{
								UnlockUser(tUser);
							}
							else
							{
								return (security: null, result: AuthenticationResult.LockedOut);
							}
						}

						string strCalcHash = "";

						CryptoUtility cryptoUtility = new CryptoUtility();

						strCalcHash = cryptoUtility.ComputeSHA512Hash(strPassword, tUser.Salt ?? "");
						if (strCalcHash == tUser.Password)
						{
							ResetFailedAttempt(tUser);

							var permissions = _systemSettingsContext.UserPermissions.Where(x => x.UserID == tUser.Id);
							if (permissions.Any())
							{
								return (security: tUser, result: AuthenticationResult.Success);
							}
							else
							{
								return (security: null, result: AuthenticationResult.InvalidForSelectedEnvironment);
							}
						}
						else
						{
							AddFailedAttempt(tUser);
						}
					}
				}
				return (security: null, result: AuthenticationResult.InvalidCredentials);
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				return (security: null, result: AuthenticationResult.InvalidCredentials);
			}
		}

		public bool ResetFailedAttempt(User user)
		{
			try
			{
				user.FailedAttempts = 0;
				_userDataAccess.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public bool AddFailedAttempt(User user)
		{
			try
			{

				if (user.FailedAttempts < 3)
				{
					user.FailedAttempts++;
					_userDataAccess.SaveChanges();
				}
				else
				{
					LockUser(user);
				}

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public int DaysUntilPasswordExpired(User user)
		{
			int intDays = 0;

			if (user != null)
			{
				if (user.DateChanged != null)
				{
					if ((user.Frequency ?? 0) > 0)
					{
						intDays = ((TimeSpan)(DateTime.Today - ((DateTime)user.DateChanged).Date)).Days;
						if (user.Frequency - intDays < 0)
						{
							return 0;
						}

						intDays = (user.Frequency ?? 0) - intDays;
					}
				}
			}
			return intDays;
		}

		private bool UpdatePassword(User user, string newPassword)
		{
			try
			{
				var result = CalculatePasswordHashAndSalt(newPassword);

				return _userDataAccess.UpdatePassword(user, result.password, result.salt);
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private (string password, string salt) CalculatePasswordHashAndSalt(string newPassword)
		{
			CryptoUtility cryptoUtility = new CryptoUtility();

			var salt = cryptoUtility.GetRandomSalt();
			var strCalcHash = cryptoUtility.ComputeSHA512Hash(newPassword, salt);

			return (password: strCalcHash, salt: salt);
		}

		public (string password, string salt) GetPasswordHashAndSalt(string newPassword)
		{
			return CalculatePasswordHashAndSalt(newPassword);
		}

		public bool ChangePasswordNow(User user)
		{
			bool result = false;

			if (user == null)
				return result;

			string strPass;

			var updateForm = new frmUpdatePassword(user, this);
			strPass = updateForm.Init();
			if (strPass == "")
			{
				return result;
			}

			result = UpdatePassword(user, strPass);
			return result;
		}
	}
}
