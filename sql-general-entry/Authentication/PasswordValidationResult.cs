﻿namespace TWGNENTY.Authentication
{
	public enum PasswordValidationResult
	{
		GoodPassword = 0,
		PreviousPassword = 1,
		CommonPassword = 2,
		DoesNotMeetMinimumRequirements = 3
	}
}