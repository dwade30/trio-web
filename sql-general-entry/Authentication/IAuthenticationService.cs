﻿using System.Collections.Generic;
using SharedApplication.ClientSettings.Models;

namespace TWGNENTY.Authentication
{
	public interface IAuthenticationService
	{
		User GetUserByID(int intUserID);
		bool UnlockUser(User user);
		IEnumerable<PasswordHistory> GetPreviousPasswords(int userId);
		bool LockUser(User user);
		(User security, AuthenticationResult result) GetAuthenticatedUser(string strUser, string strPassword);
		bool ResetFailedAttempt(User user);
		bool AddFailedAttempt(User user);
		int DaysUntilPasswordExpired(User user);
		(string password, string salt) GetPasswordHashAndSalt(string newPassword);
		bool ChangePasswordNow(User user);
	}
}