﻿namespace TWGNENTY
{
	public enum AuthenticationResult
	{
		Success = 0,
		LockedOut = 1,
		InactiveUser = 2,
		InvalidCredentials = 3,
		InvalidForSelectedEnvironment = 4
	}
}