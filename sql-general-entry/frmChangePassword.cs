﻿//TODO: TRIO Delete
//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmChangePassword.
	/// </summary>
	public partial class frmChangePassword : BaseForm
	{
		public frmChangePassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label2.AddControlArrayElement(Label2_1, 1);
			this.Label2.AddControlArrayElement(Label2_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public static frmChangePassword InstancePtr
		{
			get
			{
				return (frmChangePassword)Sys.GetInstance(typeof(frmChangePassword));
			}
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			frmSecurity.InstancePtr.NewPassword = "";
			frmSecurity.InstancePtr.GoodPassword = false;
			this.Unload();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (Strings.UCase(txtPassword1.Text) != Strings.UCase(txtPassword2.Text))
			{
				MessageBox.Show("Passwords do not match.", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPassword1.Text = "";
				txtPassword2.Text = "";
			}
			else
			{
				frmSecurity.InstancePtr.NewPassword = txtPassword1.Text;
				frmSecurity.InstancePtr.GoodPassword = true;
				this.Unload();
			}
		}

		private void frmChangePassword_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChangePassword properties;
			//frmChangePassword.ScaleWidth	= 3885;
			//frmChangePassword.ScaleHeight	= 2475;
			//frmChangePassword.LinkTopic	= "Form1";
			//frmChangePassword.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			frmSecurity.InstancePtr.NewPassword = "";
			frmSecurity.InstancePtr.GoodPassword = false;
		}
	}
}
