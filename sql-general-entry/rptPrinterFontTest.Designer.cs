﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptPrinterFontTest.
	/// </summary>
	partial class rptPrinterFontTest
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPrinterFontTest));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt10CPI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12cpi = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt17cpi = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt10CPI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12cpi)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt17cpi)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine1,
				this.txt10CPI,
				this.txt12cpi,
				this.txt17cpi
			});
			this.Detail.Height = 1.15625F;
			this.Detail.Name = "Detail";
			// 
			// txtLine1
			// 
			this.txtLine1.Height = 0.1666667F;
			this.txtLine1.Left = 0.08333334F;
			this.txtLine1.Name = "txtLine1";
			this.txtLine1.Style = "font-family: \'Courier New\'";
			this.txtLine1.Text = "Active Reports printer font test";
			this.txtLine1.Top = 0.08333334F;
			this.txtLine1.Width = 6.083333F;
			// 
			// txt10CPI
			// 
			this.txt10CPI.Height = 0.1666667F;
			this.txt10CPI.Left = 0.08333334F;
			this.txt10CPI.Name = "txt10CPI";
			this.txt10CPI.Text = "This line is 10 CPI";
			this.txt10CPI.Top = 0.3333333F;
			this.txt10CPI.Width = 6.083333F;
			// 
			// txt12cpi
			// 
			this.txt12cpi.Height = 0.1666667F;
			this.txt12cpi.Left = 0.08333334F;
			this.txt12cpi.Name = "txt12cpi";
			this.txt12cpi.Text = "This line is 12 CPI";
			this.txt12cpi.Top = 0.5833333F;
			this.txt12cpi.Width = 6.083333F;
			// 
			// txt17cpi
			// 
			this.txt17cpi.Height = 0.1666667F;
			this.txt17cpi.Left = 0.08333334F;
			this.txt17cpi.Name = "txt17cpi";
			this.txt17cpi.Text = "This line is 17 CPI";
			this.txt17cpi.Top = 0.8333333F;
			this.txt17cpi.Width = 6.083333F;
			// 
			// rptPrinterFontTest
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt10CPI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12cpi)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt17cpi)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt10CPI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12cpi;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt17cpi;
	}
}
