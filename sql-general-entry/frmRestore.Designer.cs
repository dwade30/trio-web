﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRestore.
	/// </summary>
	partial class frmRestore : BaseForm
	{
		public FCGrid gridBackups;
		public fecherFoundation.FCButton cmdRestore;
		public fecherFoundation.FCComboBox cboRestorePoints;
		public fecherFoundation.FCLabel lblRestorePoints;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRestore));
			this.gridBackups = new fecherFoundation.FCGrid();
			this.cmdRestore = new fecherFoundation.FCButton();
			this.cboRestorePoints = new fecherFoundation.FCComboBox();
			this.lblRestorePoints = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridBackups)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRestore)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdRestore);
			this.BottomPanel.Location = new System.Drawing.Point(0, 491);
			this.BottomPanel.Size = new System.Drawing.Size(770, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.gridBackups);
			this.ClientArea.Controls.Add(this.cboRestorePoints);
			this.ClientArea.Controls.Add(this.lblRestorePoints);
			this.ClientArea.Size = new System.Drawing.Size(770, 431);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(770, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(98, 30);
			this.HeaderText.Text = "Restore";
			// 
			// gridBackups
			// 
			this.gridBackups.AllowSelection = false;
			this.gridBackups.AllowUserToResizeColumns = false;
			this.gridBackups.AllowUserToResizeRows = false;
			this.gridBackups.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridBackups.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridBackups.BackColorBkg = System.Drawing.Color.Empty;
			this.gridBackups.BackColorFixed = System.Drawing.Color.Empty;
			this.gridBackups.BackColorSel = System.Drawing.Color.Empty;
			this.gridBackups.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridBackups.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridBackups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridBackups.ColumnHeadersHeight = 30;
			this.gridBackups.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridBackups.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridBackups.DragIcon = null;
			this.gridBackups.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridBackups.ExtendLastCol = true;
			this.gridBackups.FixedCols = 0;
			this.gridBackups.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridBackups.FrozenCols = 0;
			this.gridBackups.GridColor = System.Drawing.Color.Empty;
			this.gridBackups.GridColorFixed = System.Drawing.Color.Empty;
			this.gridBackups.Location = new System.Drawing.Point(30, 30);
			this.gridBackups.Name = "gridBackups";
			this.gridBackups.ReadOnly = true;
			this.gridBackups.RowHeadersVisible = false;
			this.gridBackups.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridBackups.RowHeightMin = 0;
			this.gridBackups.Rows = 1;
			this.gridBackups.ScrollTipText = null;
			this.gridBackups.ShowColumnVisibilityMenu = false;
			this.gridBackups.ShowFocusCell = false;
			this.gridBackups.Size = new System.Drawing.Size(694, 306);
			this.gridBackups.StandardTab = true;
			this.gridBackups.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridBackups.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridBackups.TabIndex = 3;
			this.gridBackups.CurrentCellChanged += new System.EventHandler(this.gridBackups_RowColChange);
			// 
			// cmdRestore
			// 
			this.cmdRestore.AppearanceKey = "acceptButton";
			this.cmdRestore.ForeColor = System.Drawing.Color.White;
			this.cmdRestore.Location = new System.Drawing.Point(330, 30);
			this.cmdRestore.Name = "cmdRestore";
			this.cmdRestore.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdRestore.Size = new System.Drawing.Size(110, 48);
			this.cmdRestore.TabIndex = 2;
			this.cmdRestore.Text = "Restore";
			this.cmdRestore.Click += new System.EventHandler(this.cmdRestore_Click);
			// 
			// cboRestorePoints
			// 
			this.cboRestorePoints.AutoSize = false;
			this.cboRestorePoints.BackColor = System.Drawing.SystemColors.Window;
			this.cboRestorePoints.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRestorePoints.FormattingEnabled = true;
			this.cboRestorePoints.Location = new System.Drawing.Point(246, 360);
			this.cboRestorePoints.Name = "cboRestorePoints";
			this.cboRestorePoints.Size = new System.Drawing.Size(478, 40);
			this.cboRestorePoints.TabIndex = 0;
			// 
			// lblRestorePoints
			// 
			this.lblRestorePoints.Location = new System.Drawing.Point(30, 374);
			this.lblRestorePoints.Name = "lblRestorePoints";
			this.lblRestorePoints.Size = new System.Drawing.Size(159, 15);
			this.lblRestorePoints.TabIndex = 1;
			this.lblRestorePoints.Text = "SELECT RESTORE POINT";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmRestore
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(770, 599);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmRestore";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Restore";
			this.Load += new System.EventHandler(this.frmRestore_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRestore_KeyPress);
			this.Resize += new System.EventHandler(this.frmRestore_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridBackups)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRestore)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
