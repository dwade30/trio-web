﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmFixProcess.
	/// </summary>
	public partial class frmFixProcess : BaseForm
	{
		public frmFixProcess()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFixProcess InstancePtr
		{
			get
			{
				return (frmFixProcess)Sys.GetInstance(typeof(frmFixProcess));
			}
		}

		protected frmFixProcess _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdRun_Click(object sender, System.EventArgs e)
		{
			// frmWait.Show
			// frmWait.lblMessage.Caption = "Please Wait..." & vbNewLine & "Executing Program"
			// frmWait.Refresh
			if (Strings.Right(Dir1.Path, 1) == "\\")
			{
				Interaction.Shell(Dir1.Path + File1, System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
			}
			else
			{
				Interaction.Shell(Dir1.Path + "\\" + File1, System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
			}
			// Unload frmWait
		}

		public void cmdRun_Click()
		{
			cmdRun_Click(cmdRun, new System.EventArgs());
		}

		private void Dir1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			File1.Path = Dir1.Text;
		}

		private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Dir1.Path = Drive1.Text;
		}

		private void File1_DoubleClick(object sender, System.EventArgs e)
		{
			cmdRun_Click();
		}

		private void frmFixProcess_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
				return;
			lblScreenTitle.Text = "General Entry System";
			this.Refresh();
		}

		private void frmFixProcess_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuProcessQuit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmFixProcess_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFixProcess properties;
			//frmFixProcess.FillStyle	= 0;
			//frmFixProcess.ScaleWidth	= 9300;
			//frmFixProcess.ScaleHeight	= 7575;
			//frmFixProcess.LinkTopic	= "Form2";
			//frmFixProcess.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmFixProcess_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmFixProcess_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			Support.SendKeys("{F11}", false);
		}
	}
}
