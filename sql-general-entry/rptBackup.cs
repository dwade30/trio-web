﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptBackup.
	/// </summary>
	public partial class rptBackup : BaseSectionReport
	{
		public rptBackup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Backup";
		}

		public static rptBackup InstancePtr
		{
			get
			{
				return (rptBackup)Sys.GetInstance(typeof(rptBackup));
			}
		}

		protected rptBackup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBackup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string[] strFileAry = null;
		int lngIndex;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngIndex > Information.UBound(strFileAry, 1);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		public void Init(ref bool boolAsZip, ref string strFileList, ref string strPath, ref string strFileName)
		{
			if (boolAsZip)
			{
				if (strPath != "")
				{
					if (Strings.Right(strPath, 1) != "\\")
					{
						strPath += "\\";
					}
				}
				txtFileInfo.Text = "The following file(s) were saved in the zip file " + strPath + strFileName + ":";
			}
			else
			{
				txtFileInfo.Text = "The following file(s) were saved in " + strPath + ":";
			}
			if (Strings.Trim(strFileList) == string.Empty)
			{
				this.Close();
				return;
			}
			strFileAry = Strings.Split(strFileList, ",", -1, CompareConstants.vbTextCompare);
			lngIndex = 0;
			//FC:TODO:AM
			//this.Show(MDIParent.InstancePtr);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtFilename.Text = strFileAry[lngIndex];
			lngIndex += 1;
		}

		private void rptBackup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBackup properties;
			//rptBackup.Caption	= "Backup";
			//rptBackup.Left	= 0;
			//rptBackup.Top	= 0;
			//rptBackup.Width	= 19080;
			//rptBackup.Height	= 14850;
			//rptBackup.StartUpPosition	= 3;
			//rptBackup.SectionData	= "rptBackup.dsx":0000;
			//End Unmaped Properties
		}
	}
}
