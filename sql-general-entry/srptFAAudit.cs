﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptFAAudit.
	/// </summary>
	public partial class srptFAAudit : FCSectionReport
	{
		public srptFAAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptFAAudit";
		}

		public static srptFAAudit InstancePtr
		{
			get
			{
				return (srptFAAudit)Sys.GetInstance(typeof(srptFAAudit));
			}
		}

		protected srptFAAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptFAAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		// vbPorter upgrade warning: curOriginalTotal As Decimal	OnWrite(short, Decimal)
		Decimal curOriginalTotal;
		// vbPorter upgrade warning: curDepreciationTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDepreciationTotal;
		int lngItemTotal;
		clsDRWrapper rsInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			lngItemTotal = 0;
			curOriginalTotal = 0;
			curDepreciationTotal = 0;
			rsInfo.OpenRecordset("SELECT Year(PurchaseDate) as PurchaseYear, COUNT(PurchaseDate) as ItemCount, SUM(BasisCost) as OriginalTotal, SUM(TotalDepreciated) as DepreciationTotal FROM tblMaster GROUP BY Year(PurchaseDate) ORDER BY Year(PurchaseDate)", "TWFA0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldYear.Text = FCConvert.ToString(rsInfo.Get_Fields_Int32("PurchaseYear"));
			// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
			fldEntries.Text = FCConvert.ToString(rsInfo.Get_Fields("ItemCount"));
			// TODO Get_Fields: Field [OriginalTotal] not found!! (maybe it is an alias?)
			fldOriginalValue.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("OriginalTotal")), "#,##0.00");
			// TODO Get_Fields: Field [DepreciationTotal] not found!! (maybe it is an alias?)
			fldDepreciation.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("DepreciationTotal")), "#,##0.00");
			// TODO Get_Fields: Field [OriginalTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [DepreciationTotal] not found!! (maybe it is an alias?)
			fldNetValue.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("OriginalTotal")) - Conversion.Val(rsInfo.Get_Fields("DepreciationTotal")), "#,##0.00");
			// TODO Get_Fields: Field [OriginalTotal] not found!! (maybe it is an alias?)
            //FC:FINAL:MSH - issue #1596: added FCConvert.ToDecimal to avoid conversion exceptions 
			curOriginalTotal += FCConvert.ToDecimal(Conversion.Val(rsInfo.Get_Fields("OriginalTotal")));
			// TODO Get_Fields: Field [DepreciationTotal] not found!! (maybe it is an alias?)
            //FC:FINAL:MSH - issue #1596: added FCConvert.ToDecimal to avoid conversion exceptions 
			curDepreciationTotal += FCConvert.ToDecimal(Conversion.Val(rsInfo.Get_Fields("DepreciationTotal")));
			// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
            //FC:FINAL:MSH - issue #1596: Get_Fields replaced for correct getting data
			lngItemTotal += rsInfo.Get_Fields_Int32("ItemCount");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalOriginalValue.Text = Strings.Format(curOriginalTotal, "#,##0.00");
			fldTotalDepreciation.Text = Strings.Format(curDepreciationTotal, "#,##0.00");
			fldTotalNetValue.Text = Strings.Format(curOriginalTotal - curDepreciationTotal, "#,##0.00");
			fldTotalEntries.Text = Strings.Format(lngItemTotal, "#,##0");
		}

		

		private void srptFAAudit_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
