//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmExpirationFromWeb.
	/// </summary>
	public partial class frmExpirationFromWeb : fecherFoundation.FCForm
	{
;

		public frmExpirationFromWeb()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation

		// Written By
		// Date


		// ********************************************************


		string downloadfile;
		string strFTPSiteAddress = "";
		string strFTPSiteUser = "";
		string strFTPSitePassword = "";
		int FileCol;
		int SelectCol;
		int DescriptionCol;



		private void ChilkatFtp1_BeginDownloadFile(string Path, ref int skip)
		{
			ProgressBar1.Value = 0;
		}

		private void ChilkatFtp1_GetProgress(int pctDone)
		{
			ProgressBar1.Value = pctDone;
		}


		private void frmExpirationFromWeb_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			switch (KeyCode) {
				
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					mnuExit_Click();
					break;
				}
			} //end switch
		}

		private void Form_Unload(ref short Cancel)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}



		private void DownloadUpdate()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			const int curOnErrorGoToLabel_ErrorHandler = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				int counter;
				bool blnFilesSelected;
				string strDataDest;
				bool boolEXEs;
				
				string strShellProgram = "";
				// Dim strRootDir As String
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */


				lblPercent.Text = "Downloading";
				strDataDest = Environment.CurrentDirectory;
				 /*? On Error Resume Next  */
				// strRootDir = fso.GetDriveName(CurDir)
				// If Not strRootDir = "" Then
				// If Right(strRootDir, 1) <> "\" Then strRootDir = strRootDir & "\"
				// End If
				if (!fso.FolderExists(strDataDest+"\\Expiration")) {
					fso.CreateFolder(strDataDest+"\\Expiration");
				}
				if (fso.FolderExists(strDataDest+"\\Expiration")) {
					strDataDest += "\\Expiration";
				}
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				DLFile_20("TWUpdate.xml", ref strDataDest, "TWUpdate.xml");
				DLFile_20("TWUpdate.vb1", ref strDataDest, "TWUpdate.vb1");
				DLFile_20("TSUpdate.td1", ref strDataDest, "TSUpdate.td1");

				// On Error Resume Next
				// If fso.FileExists(strDataDest & "\tsupdate.td1") Then
				// Call fso.CopyFile(strDataDest & "\tsupdate.td1", strRootDir & "tsupdate.td1", True)
				// End If
				// On Error GoTo ErrorHandler

				if (Strings.Right(strDataDest, 1)!="\\") strDataDest += "\\";
				// gstrFilePath = strDataDest
				if (fso.FileExists(strDataDest+"TWUpdate.xml")) {
					// 37        Call UpdateWindowsProgramInformation
					// 38        Call UpdateCheckDigits

					// Dim tSet As New cSystemSettings
					cSysSettingsController tSet = new cSysSettingsController();
					if (!tSet.UpdateSubscriptionsFromFile(strDataDest+"TWUpdate.xml")) {
						frmWait.InstancePtr.Unload();
						MessageBox.Show(tSet.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					} else {
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Expiration Update Complete", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					frmWait.InstancePtr.Unload();
					this.Unload();
					return;
				} else {
					frmWait.InstancePtr.Unload();
					MessageBox.Show(strDataDest+"Twupdate.xml not found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Unload();
					return;
				}

				lblPercent.Text = "";
				ProgressBar1.Value = 0;

				return;

			}
			catch(Exception ex)
			{
                ErrorHandler:;
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + Convert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\n" + "In DownloadUpdate " + "line " + Erl, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);

            }
        }

		private void frmExpirationFromWeb_Activated(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_Err_Connect = 1;
			const int curOnErrorGoToLabel_ErrorHandler = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				int counter;
				clsDRWrapper rsModules = new clsDRWrapper();
				bool boolHasBilling;
				int success;
				bool boolRetried;
				if (modGlobalRoutines.FormExist(this)) {
					return;
				}
				boolRetried = false;
				this.Refresh();
				frmWait.InstancePtr.Init("Please Wait..."+"\n"+"Connecting to FTP Site");
				ChilkatFtp1.UnlockComponent("HARRISFTP_32huCB6cpQnV");
				vOnErrorGoToLabel = curOnErrorGoToLabel_Err_Connect; /* On Error GoTo Err_Connect */
				ChilkatFtp1.HostName = strFTPSiteAddress;
				ChilkatFtp1.UserName = strFTPSiteUser;
				ChilkatFtp1.Password = strFTPSitePassword;
			TryConnect: ;
				success = ChilkatFtp1.Connect;
				if (success!=1) {
					// error
					if (!boolRetried) {
						boolRetried = true;
						strFTPSitePassword = "trio";
						goto TryConnect;
					}
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Could not connect to ftp site."+"\n"+ChilkatFtp1.LastErrorText, "No Connection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					this.Unload();
					return;
				}
				// 8    FTP1.Connect strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				success = ChilkatFtp1.ChangeRemoteDir("Expiration");
				if (success==1) {


					frmWait.InstancePtr.Init("Please Wait..."+"\n"+"Checking for expiration information", true, 28);

					RefreshSocket();

					frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Maximum;
					frmWait.InstancePtr.Unload();
				} else {
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("Unable to locate Expiration directory.", "No Expiration Directory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}


				return;
			Err_Connect: ;
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Connect failed: "+"\n"+Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				AddStatusMsg_2("Connect failed: "+Information.Err(ex).Description);
				return;
			ErrorHandler: ;
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				//Application.DoEvents();
				if (Information.Err(ex).Number==-2147217408) {
					MessageBox.Show("Unable to locate Expiration directory.", "No Expiration Directory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				} else {
					MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In form_activate line "+Erl, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}

			}
			catch
			{
				switch(vOnErrorGoToLabel) {
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_Err_Connect:
						//? goto Err_Connect;
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
				}
			}
		}

		private void frmExpirationFromWeb_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExpirationFromWeb properties;
			//frmExpirationFromWeb.FillStyle	= 0;
			//frmExpirationFromWeb.ScaleWidth	= 5880;
			//frmExpirationFromWeb.ScaleHeight	= 4200;
			//frmExpirationFromWeb.LinkTopic	= "Form2";
			//frmExpirationFromWeb.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties

			clsDRWrapper rsDefaults = new clsDRWrapper();


			rsDefaults.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
			if (rsDefaults.EndOfFile()!=true && rsDefaults.BeginningOfFile()!=true) {
				strFTPSiteAddress = Convert.ToString(rsDefaults.Get_Fields_String("FTPSiteAddress"));
				strFTPSiteUser = Convert.ToString(rsDefaults.Get_Fields_String("FTPSiteUser"));
				strFTPSitePassword = Convert.ToString(rsDefaults.Get_Fields_String("FTPSitePassword"));
			}

			lstMessages.Clear();

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			return;
		ErrorHandler: ;
			MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In form_load line "+Erl, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		private void frmExpirationFromWeb_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

			// catches the escape and enter keys
			if (KeyAscii==Keys.Escape) {
				KeyAscii = (Keys)0;
				this.Unload();
			} else if (KeyAscii==Keys.Return) {
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}

			e.KeyChar = Strings.Chr((int)KeyAscii);
		}

		private void mnuProcessQuit_Click()
		{
			this.Unload();
		}

		private bool RefreshSocket()
		{
			bool RefreshSocket = false;
			// Dim loDirCollection As New VBA.Collection
			// Dim loDirCollectionEntry      As dpFTPListItem
			int intFilesAndFolders;
			bool boolFound;
			boolFound = false;
			try
			{	// On Error GoTo ErrorHandler
				string strName = "";
				intFilesAndFolders = 0;

				if (!(ChilkatFtp1.IsConnected==1)) return RefreshSocket;

				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// 10    Set loDirCollection = FTP1.List
				// 12    Set loDirCollection = FTP1.SortList(loDirCollection, ftpNameAsc)
				intFilesAndFolders = ChilkatFtp1.NumFilesAndDirs;
				if (intFilesAndFolders<0) {
					frmWait.InstancePtr.Unload();
					MessageBox.Show(ChilkatFtp1.LastErrorText);
					return RefreshSocket;
				}
				// Loop through list entries:
				// 14    For Each loDirCollectionEntry In loDirCollection
				// 16        With loDirCollectionEntry
				// If .ItemType = ftpDir Then
				// do nothing
				// Else
				// If UCase(.Name) = "TWUPDATE.VB1" Then
				// DownloadUpdate
				// End If
				// End If
				// End With
				// Next


				int I;
				for(I=0; I<=intFilesAndFolders-1; I++) {
					strName = ChilkatFtp1.GetFileName(I);
					if (ChilkatFtp1.GetIsDirectory(I)==1) {
					} else {
						if (Strings.UCase(strName)=="TWUPDATE.XML") {
							boolFound = true;
							DownloadUpdate();
							break;
						}
					}
				} // I
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (!boolFound) {
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No expiration file found", "No File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Unload();
				}
				return RefreshSocket;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In RefreshSocket line "+Erl, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return RefreshSocket;
		}

		private void AddStatusMsg_2(string Message) { AddStatusMsg(ref Message); }
		private void AddStatusMsg(ref string Message)
		{
			try
			{	// On Error GoTo ErrorHandler
				lstMessages.AddItem(Message);
				lstMessages.SetSelected(lstMessages.ListCount-1, true);
				lstMessages.Refresh();
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In AddStatusMsg line "+Erl, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}




		// Private Sub FTP1_ServerResponse(ByVal Message As String)
		// AddStatusMsg Message
		// End Sub
		// 
		// Private Sub FTP1_TransferProgress(ByVal RemoteFileName As String, ByVal LocalFileName As String, ByVal Download As Boolean, ByVal Bytes As Long, ByVal TotalBytes As Long, Cancel As Boolean)
		// Dim dblPerc As Double
		// Dim lngPerc As Long
		// On Error GoTo ErrorHandler
		// 
		// If TotalBytes <> 0 Then
		// 2    dblPerc = Bytes / TotalBytes
		// Else
		// dblPerc = 0
		// End If
		// 4    lngPerc = dblPerc * 100
		// 6    If lngPerc > 100 Then lngPerc = 100
		// 
		// 8    ProgressBar1.Value = lngPerc
		// Me.Refresh
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In FTP1_TransferProgress line " & Erl, vbCritical, "Error"
		// End Sub

		// Private Sub FTP1_TransferStarting(ByVal RemoteFileName As String, ByVal LocalFileName As String, ByVal Download As Boolean, ByVal TotalBytes As Long, Cancel As Boolean)
		// On Error GoTo ErrorHandler
		// 
		// 2    ProgressBar1.Value = 0
		// ProgressBar1.Max = TotalBytes
		// 4    ProgressBar1.Max = 100
		// 
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In transferstarting line " & Erl, vbCritical, "Error"
		// End Sub

		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool DLFile_20(string FileName, string downloadpath, string downloadas) { return DLFile(ref FileName, ref downloadpath, ref downloadas); }
		private bool DLFile(ref string FileName, ref string downloadpath, ref string downloadas)
		{
			bool DLFile = false;
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			const int curOnErrorGoToLabel_ErrorHandler = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */

			StartOver: ;
				downloadfile = downloadpath+"\\"+downloadas;
				if (FCFileSystem.Dir(downloadpath+"\\"+downloadas, 0)!="") {
					FCFileSystem.Kill(downloadpath+"\\"+downloadas);
				}

				// FTP1.GetFile Filename, downloadpath, downloadas
				int success;
				success = ChilkatFtp1.GetFile(FileName, downloadfile);
				if (success==0) {
					MessageBox.Show(ChilkatFtp1.LastErrorText);
					return DLFile;
				}
				DLFile = true;
				return DLFile;
			ErrorHandler: ;
				if (Erl==8 && Information.Err(ex).Number==-2147217408) {
					 /*? On Error Resume Next  */
					// FTP1.Connect strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21
					ChilkatFtp1.HostName = strFTPSiteAddress;
					ChilkatFtp1.UserName = strFTPSiteUser;
					ChilkatFtp1.Password = strFTPSitePassword;
				TryConnect: ;
					ChilkatFtp1.Connect();
					if (Information.Err(ex).Number!=0) {
						// give up
						if (Strings.Left(Information.Err(ex).Description+"     ", 5)=="12014" && strFTPSitePassword!="trio") {
							strFTPSitePassword = "trio";
							goto TryConnect;
						}
						MessageBox.Show("Connection Failed in DLFile"+"\n"+"Cannot download "+FileName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return DLFile;
					}
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
					goto StartOver;
				}
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In DLFile "+"line "+Erl, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);

			}
			catch
			{
				switch(vOnErrorGoToLabel) {
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
				}
			}
			return DLFile;
		}


	}
}
