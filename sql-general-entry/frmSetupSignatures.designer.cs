﻿using Global;
using fecherFoundation;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSetupSignatures.
	/// </summary>
	partial class frmSetupSignatures : BaseForm
	{
		public Wisej.Web.GroupBox framMain;
		public fecherFoundation.FCComboBox cmbType;
		public FCGrid GridDelete;
		public FCGrid gridUser;
		public fecherFoundation.FCLabel Label1;
		public Wisej.Web.GroupBox framSignature;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtConfirmPassword;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCPictureBox imgSignature;
		public fecherFoundation.FCLabel lblConfirm;
		public fecherFoundation.FCLabel lblPassword;
		public fecherFoundation.FCLabel Shape1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupSignatures));
			this.framMain = new Wisej.Web.GroupBox();
			this.cmbType = new fecherFoundation.FCComboBox();
			this.GridDelete = new fecherFoundation.FCGrid();
			this.gridUser = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.framSignature = new Wisej.Web.GroupBox();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOk = new fecherFoundation.FCButton();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtConfirmPassword = new fecherFoundation.FCTextBox();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.imgSignature = new fecherFoundation.FCPictureBox();
			this.lblConfirm = new fecherFoundation.FCLabel();
			this.lblPassword = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnSave = new Wisej.Web.Button();
			this.btnAddUser = new Wisej.Web.Button();
			this.btnDelete = new Wisej.Web.Button();
			this.fcPanel1 = new fecherFoundation.FCPanel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.framMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridUser)).BeginInit();
			this.framSignature.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel1)).BeginInit();
			this.fcPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 374);
			this.BottomPanel.Size = new System.Drawing.Size(1104, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framMain);
			this.ClientArea.Controls.Add(this.framSignature);
			this.ClientArea.Size = new System.Drawing.Size(1104, 314);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnDelete);
			this.TopPanel.Controls.Add(this.btnAddUser);
			this.TopPanel.Size = new System.Drawing.Size(1104, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnAddUser, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(130, 30);
			this.HeaderText.Text = "Signatures";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// framMain
			// 
			this.framMain.AppearanceKey = "groupBoxNoBorders";
			this.framMain.Controls.Add(this.cmbType);
			this.framMain.Controls.Add(this.GridDelete);
			this.framMain.Controls.Add(this.gridUser);
			this.framMain.Controls.Add(this.Label1);
			this.framMain.Location = new System.Drawing.Point(30, 20);
			this.framMain.Name = "framMain";
			this.framMain.Size = new System.Drawing.Size(468, 280);
			this.framMain.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.framMain, null);
			// 
			// cmbType
			// 
			this.cmbType.AutoSize = false;
			this.cmbType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbType.FormattingEnabled = true;
			this.cmbType.Location = new System.Drawing.Point(102, 0);
			this.cmbType.Name = "cmbType";
			this.cmbType.Size = new System.Drawing.Size(360, 41);
			this.cmbType.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.cmbType, null);
			this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
			// 
			// GridDelete
			// 
			this.GridDelete.AllowSelection = false;
			this.GridDelete.AllowUserToResizeColumns = false;
			this.GridDelete.AllowUserToResizeRows = false;
			this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.GridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDelete.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDelete.ColumnHeadersHeight = 30;
			this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDelete.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDelete.DragIcon = null;
			this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.FrozenCols = 0;
			this.GridDelete.GridColor = System.Drawing.Color.Empty;
			this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.Location = new System.Drawing.Point(374, 47);
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.ReadOnly = true;
			this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDelete.RowHeightMin = 0;
			this.GridDelete.Rows = 2;
			this.GridDelete.ScrollTipText = null;
			this.GridDelete.ShowColumnVisibilityMenu = false;
			this.GridDelete.ShowFocusCell = false;
			this.GridDelete.Size = new System.Drawing.Size(69, 12);
			this.GridDelete.StandardTab = true;
			this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDelete.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.GridDelete, null);
			this.GridDelete.Visible = false;
			// 
			// gridUser
			// 
			this.gridUser.AllowSelection = false;
			this.gridUser.AllowUserToResizeColumns = false;
			this.gridUser.AllowUserToResizeRows = false;
			this.gridUser.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridUser.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridUser.BackColorBkg = System.Drawing.Color.Empty;
			this.gridUser.BackColorFixed = System.Drawing.Color.Empty;
			this.gridUser.BackColorSel = System.Drawing.Color.Empty;
			this.gridUser.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridUser.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridUser.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.gridUser.ColumnHeadersHeight = 30;
			this.gridUser.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridUser.DefaultCellStyle = dataGridViewCellStyle4;
			this.gridUser.DragIcon = null;
			this.gridUser.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridUser.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridUser.FrozenCols = 0;
			this.gridUser.GridColor = System.Drawing.Color.Empty;
			this.gridUser.GridColorFixed = System.Drawing.Color.Empty;
			this.gridUser.Location = new System.Drawing.Point(0, 60);
			this.gridUser.Name = "gridUser";
			this.gridUser.ReadOnly = true;
			this.gridUser.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridUser.RowHeightMin = 0;
			this.gridUser.Rows = 2;
			this.gridUser.ScrollTipText = null;
			this.gridUser.ShowColumnVisibilityMenu = false;
			this.gridUser.ShowFocusCell = false;
			this.gridUser.Size = new System.Drawing.Size(462, 220);
			this.gridUser.StandardTab = true;
			this.gridUser.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridUser.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridUser.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.gridUser, "Double click on an entry to edit signature or change password");
			this.gridUser.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.gridUser_KeyDownEdit);
			this.gridUser.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.gridUser_AfterEdit);
			this.gridUser.CurrentCellChanged += new System.EventHandler(this.gridUser_RowColChange);
			this.gridUser.KeyDown += new Wisej.Web.KeyEventHandler(this.gridUser_KeyDownEvent);
			this.gridUser.DoubleClick += new System.EventHandler(this.gridUser_DblClick);
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(0, 14);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(40, 15);
			this.Label1.TabIndex = 12;
			this.Label1.Text = "TYPE";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// framSignature
			// 
			this.framSignature.Controls.Add(this.fcPanel1);
			this.framSignature.Controls.Add(this.cmdCancel);
			this.framSignature.Controls.Add(this.cmdOk);
			this.framSignature.Controls.Add(this.txtConfirmPassword);
			this.framSignature.Controls.Add(this.txtPassword);
			this.framSignature.Controls.Add(this.imgSignature);
			this.framSignature.Controls.Add(this.lblConfirm);
			this.framSignature.Controls.Add(this.lblPassword);
			this.framSignature.Controls.Add(this.Shape1);
			this.framSignature.Enabled = false;
			this.framSignature.Location = new System.Drawing.Point(523, 20);
			this.framSignature.Name = "framSignature";
			this.framSignature.Size = new System.Drawing.Size(557, 280);
			this.framSignature.TabIndex = 2;
			this.framSignature.Text = "Signature";
			this.ToolTip1.SetToolTip(this.framSignature, null);
			// 
			// cmdClear
			// 
			this.cmdClear.AppearanceKey = "actionButton";
			this.cmdClear.Enabled = false;
			this.cmdClear.Location = new System.Drawing.Point(86, 10);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(80, 40);
			this.cmdClear.TabIndex = 12;
			this.cmdClear.Text = "Clear";
			this.ToolTip1.SetToolTip(this.cmdClear, null);
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Enabled = false;
			this.cmdCancel.Location = new System.Drawing.Point(78, 220);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(74, 40);
			this.cmdCancel.TabIndex = 9;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOk
			// 
			this.cmdOk.AppearanceKey = "actionButton";
			this.cmdOk.Enabled = false;
			this.cmdOk.Location = new System.Drawing.Point(20, 220);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new System.Drawing.Size(52, 40);
			this.cmdOk.TabIndex = 8;
			this.cmdOk.Text = "OK";
			this.ToolTip1.SetToolTip(this.cmdOk, null);
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Enabled = false;
			this.cmdBrowse.Location = new System.Drawing.Point(0, 10);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(80, 40);
			this.cmdBrowse.TabIndex = 7;
			this.cmdBrowse.Text = "Browse";
			this.ToolTip1.SetToolTip(this.cmdBrowse, null);
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// txtConfirmPassword
			// 
			this.txtConfirmPassword.AutoSize = false;
			this.txtConfirmPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtConfirmPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtConfirmPassword.LinkItem = null;
			this.txtConfirmPassword.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtConfirmPassword.LinkTopic = null;
			this.txtConfirmPassword.Location = new System.Drawing.Point(218, 80);
			this.txtConfirmPassword.Name = "txtConfirmPassword";
			this.txtConfirmPassword.PasswordChar = '*';
			this.txtConfirmPassword.Size = new System.Drawing.Size(234, 40);
			this.txtConfirmPassword.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtConfirmPassword, null);
			// 
			// txtPassword
			// 
			this.txtPassword.AutoSize = false;
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword.LinkItem = null;
			this.txtPassword.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPassword.LinkTopic = null;
			this.txtPassword.Location = new System.Drawing.Point(218, 20);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(234, 40);
			this.txtPassword.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtPassword, null);
			// 
			// imgSignature
			// 
			this.imgSignature.AllowDrop = true;
			this.imgSignature.DrawStyle = ((short)(0));
			this.imgSignature.DrawWidth = ((short)(1));
			this.imgSignature.FillStyle = ((short)(1));
			this.imgSignature.FontTransparent = true;
			this.imgSignature.Image = ((System.Drawing.Image)(resources.GetObject("imgSignature.Image")));
			this.imgSignature.Location = new System.Drawing.Point(20, 140);
			this.imgSignature.Name = "imgSignature";
			this.imgSignature.Picture = ((System.Drawing.Image)(resources.GetObject("imgSignature.Picture")));
			this.imgSignature.Size = new System.Drawing.Size(180, 60);
			this.imgSignature.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgSignature.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.imgSignature, null);
			// 
			// lblConfirm
			// 
			this.lblConfirm.AutoSize = true;
			this.lblConfirm.Location = new System.Drawing.Point(20, 94);
			this.lblConfirm.Name = "lblConfirm";
			this.lblConfirm.Size = new System.Drawing.Size(141, 15);
			this.lblConfirm.TabIndex = 6;
			this.lblConfirm.Text = "CONFIRM PASSWORD";
			this.lblConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblConfirm, null);
			// 
			// lblPassword
			// 
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(20, 34);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(112, 15);
			this.lblPassword.TabIndex = 5;
			this.lblPassword.Text = "NEW PASSWORD";
			this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblPassword, null);
			// 
			// Shape1
			// 
			this.Shape1.BorderStyle = 1;
			this.Shape1.Location = new System.Drawing.Point(245, 120);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(207, 53);
			this.Shape1.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.Shape1, null);
			this.Shape1.Visible = false;
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(502, 30);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(100, 48);
			this.btnSave.TabIndex = 0;
			this.btnSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnSave, null);
			this.btnSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// btnAddUser
			// 
			this.btnAddUser.AppearanceKey = "toolbarButton";
			this.btnAddUser.Location = new System.Drawing.Point(797, 26);
			this.btnAddUser.Name = "btnAddUser";
			this.btnAddUser.Size = new System.Drawing.Size(139, 25);
			this.btnAddUser.TabIndex = 1;
			this.btnAddUser.Text = "Add New Signature";
			this.ToolTip1.SetToolTip(this.btnAddUser, null);
			this.btnAddUser.Click += new System.EventHandler(this.mnuAddUser_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.AppearanceKey = "toolbarButton";
			this.btnDelete.Location = new System.Drawing.Point(961, 26);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(119, 25);
			this.btnDelete.TabIndex = 2;
			this.btnDelete.Text = "Delete Signature";
			this.ToolTip1.SetToolTip(this.btnDelete, null);
			this.btnDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// fcPanel1
			// 
			this.fcPanel1.Controls.Add(this.cmdClear);
			this.fcPanel1.Controls.Add(this.cmdBrowse);
			this.fcPanel1.Location = new System.Drawing.Point(218, 140);
			this.fcPanel1.Name = "fcPanel1";
			this.fcPanel1.Size = new System.Drawing.Size(166, 60);
			this.fcPanel1.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.fcPanel1, null);
			// 
			// frmSetupSignatures
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1104, 482);
			this.FillColor = 0;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSetupSignatures";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Signatures";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmSetupSignatures_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSetupSignatures_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.framMain.ResumeLayout(false);
			this.framMain.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridUser)).EndInit();
			this.framSignature.ResumeLayout(false);
			this.framSignature.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel1)).EndInit();
			this.fcPanel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Wisej.Web.Button btnSave;
		public FCButton cmdClear;
		private Wisej.Web.Button btnDelete;
		private Wisej.Web.Button btnAddUser;
		private FCPanel fcPanel1;
	}
}
