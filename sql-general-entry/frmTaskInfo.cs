﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmTaskInfo.
	/// </summary>
	public partial class frmTaskInfo : BaseForm
	{
		public frmTaskInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaskInfo InstancePtr
		{
			get
			{
				return (frmTaskInfo)Sys.GetInstance(typeof(frmTaskInfo));
			}
		}

		protected frmTaskInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           10/22/03
		// This form will be used to enter task information to the
		// scheduler
		// ********************************************************
		int lngRecordKey;
		DateTime datTaskDate;
		int SelectCol;
		int DescriptionCol;
		bool blnEditUserOnly;
		bool blnUnload;

		private void frmTaskInfo_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			if (blnUnload)
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Unload();
				return;
			}
			this.Refresh();
		}

		private void frmTaskInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaskInfo properties;
			//frmTaskInfo.FillStyle	= 0;
			//frmTaskInfo.ScaleWidth	= 9045;
			//frmTaskInfo.ScaleHeight	= 6930;
			//frmTaskInfo.LinkTopic	= "Form2";
			//frmTaskInfo.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsTaskInfo = new clsDRWrapper();
			SelectCol = 0;
			DescriptionCol = 1;
			vsInstallers.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsInstallers.ColWidth(SelectCol, FCConvert.ToInt32(vsInstallers.WidthOriginal * 0.2));
			if (!modSecurity2.ValidPermissions(this, modSecurity2.CNSTCALENDAR))
			{
				blnUnload = true;
			}
			else
			{
				blnUnload = false;
			}
			LoadInstallersToGrid();
			LoadUsers();
			blnEditUserOnly = false;
			if (lngRecordKey != 0)
			{
                //FC:FINAL:MSH - issue #1600: change state of toolbar button
                //mnuFileChangeDate.Enabled = true;
                cmdFileChangeDate.Enabled = true;
				fraEditStaff.Visible = true;
				fraInstallers.Visible = false;
				linCreatedBy.Visible = true;
				lblCreatedByLabel.Visible = true;
				lblCreatedByName.Visible = true;
				rsTaskInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + FCConvert.ToString(lngRecordKey), "SystemSettings");
				if (rsTaskInfo.EndOfFile() != true && rsTaskInfo.BeginningOfFile() != true)
				{
					SetUserCombo_2(rsTaskInfo.Get_Fields_String("TaskPerson"));
					if (FCConvert.ToBoolean(rsTaskInfo.Get_Fields_Boolean("TaskReminder")))
					{
						cmbYes.SelectedIndex = 0;
						SetReminderTimeCombo_2(rsTaskInfo.Get_Fields_DateTime("ReminderStartDate"), datTaskDate);
					}
					else
					{
						cmbYes.SelectedIndex = 1;
					}
                    rtfNotes.Text = rsTaskInfo.Get_Fields_String("TaskNotes");
                    txtDescription.Text = FCConvert.ToString(rsTaskInfo.Get_Fields_String("TaskDescription"));
					lblCreatedByName.Text = FCConvert.ToString(rsTaskInfo.Get_Fields_String("TaskCreatedBy"));
				}
			}
			else
			{
				fraEditStaff.Visible = false;
				fraInstallers.Visible = true;
				linCreatedBy.Visible = false;
				lblCreatedByLabel.Visible = false;
				lblCreatedByName.Visible = false;
                rtfNotes.Text = "";
                txtDescription.Text = "";
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmTaskInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileChangeDate_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime datReturnedDate;
			clsDRWrapper rsInfo = new clsDRWrapper();
			// pop up the calender screen and let the user select a date
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate, datTaskDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + FCConvert.ToString(lngRecordKey), "SystemSettings");
				rsInfo.Edit();
				rsInfo.Set_Fields("TaskDate", datReturnedDate);
				datTaskDate = datReturnedDate;
				rsInfo.Update();
				MessageBox.Show("Task Date Changed", "Date Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: Ans As short, int --> As DialogResult
			DialogResult Ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			Ans = MessageBox.Show("Are you sure you wish to delete this entry?", "Delete Entry?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (Ans == DialogResult.Yes)
			{
				rsDelete.Execute("DELETE * FROM Tasks WHERE ID = " + FCConvert.ToString(lngRecordKey), "SystemSettings");
			}
			this.Unload();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void Init(int lngKey, DateTime datDate)
		{
			lngRecordKey = lngKey;
			datTaskDate = datDate;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTaskInfo = new clsDRWrapper();
			clsDRWrapper rsTaskCount = new clsDRWrapper();
			int counter;
			bool blnSave = false;
			string strModuleData = "";
			clsDRWrapper rsTownInfo = new clsDRWrapper();
			string strModuleName = "";
			int Ans;
			if (lngRecordKey == 0)
			{
				blnSave = false;
				for (counter = 0; counter <= vsInstallers.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsInstallers.TextMatrix(counter, SelectCol)) == true)
					{
						blnSave = true;
						break;
					}
				}
				if (!blnSave)
				{
					MessageBox.Show("You must enter at least one person before you may continue.", "Invalid TRIO Staff", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a decription before you may proceed.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtDescription.Focus();
				return;
			}
			if (lngRecordKey != 0)
			{
				rsTaskInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + FCConvert.ToString(lngRecordKey), "SystemSettings");
				rsTaskInfo.Edit();
				rsTaskInfo.Set_Fields("TaskPerson", cboUser.Text);
				rsTaskInfo.Set_Fields("TaskDate", datTaskDate);
				if (cmbYes.SelectedIndex == 0)
				{
					rsTaskInfo.Set_Fields("TaskReminder", true);
					rsTaskInfo.Set_Fields("ReminderStartDate", CalculateReminderStartDate());
				}
				else
				{
					rsTaskInfo.Set_Fields("TaskReminder", false);
					rsTaskInfo.Set_Fields("ReminderStartDate", null);
				}
				rsTaskCount.OpenRecordset("SELECT TOP 1 * FROM Tasks WHERE TaskDate = '" + FCConvert.ToString(datTaskDate) + "' ORDER BY RowOrder DESC", "SystemSettings");
				if (rsTaskCount.EndOfFile() != true && rsTaskCount.BeginningOfFile() != true)
				{
					rsTaskInfo.Set_Fields("RowOrder", Conversion.Val(rsTaskCount.Get_Fields_Int32("RowOrder")) + 1);
				}
				else
				{
					rsTaskInfo.Set_Fields("RowOrder", 1);
				}
				rsTaskInfo.Set_Fields("TaskDescription", txtDescription.Text);
                rsTaskInfo.Set_Fields("TaskNotes", rtfNotes.Text);
                rsTaskInfo.Update();
			}
			else
			{
				rsTaskInfo.OpenRecordset("SELECT * FROM Tasks", "SystemSettings");
				for (counter = 0; counter <= vsInstallers.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsInstallers.TextMatrix(counter, SelectCol)) == true)
					{
						rsTaskInfo.AddNew();
						rsTaskInfo.Set_Fields("TaskPerson", vsInstallers.TextMatrix(counter, DescriptionCol));
						rsTaskInfo.Set_Fields("TaskDate", datTaskDate);
						if (cmbYes.SelectedIndex == 0)
						{
							rsTaskInfo.Set_Fields("TaskReminder", true);
							rsTaskInfo.Set_Fields("ReminderStartDate", CalculateReminderStartDate());
						}
						else
						{
							rsTaskInfo.Set_Fields("TaskReminder", false);
							rsTaskInfo.Set_Fields("ReminderStartDate", null);
						}
						rsTaskCount.OpenRecordset("SELECT TOP 1 * FROM Tasks WHERE TaskDate = '" + FCConvert.ToString(datTaskDate) + "' ORDER BY RowOrder DESC", "SystemSettings");
						if (rsTaskCount.EndOfFile() != true && rsTaskCount.BeginningOfFile() != true)
						{
							rsTaskInfo.Set_Fields("RowOrder", Conversion.Val(rsTaskCount.Get_Fields_Int32("RowOrder")) + 1);
						}
						else
						{
							rsTaskInfo.Set_Fields("RowOrder", 1);
						}
						rsTaskInfo.Set_Fields("TaskDescription", txtDescription.Text);
                        rsTaskInfo.Set_Fields("TaskNotes", rtfNotes.Text);
                        rsTaskInfo.Set_Fields("TaskCreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
						rsTaskInfo.Update();
					}
				}
			}
			this.Unload();
		}

		private void LoadInstallersToGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			vsInstallers.Rows = 0;
			rsInfo.OpenRecordset("SELECT * FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' ORDER BY UserID", "ClientSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (!blnEditUserOnly)
				{
					do
					{
						vsInstallers.AddItem("");
						vsInstallers.TextMatrix(vsInstallers.Rows - 1, SelectCol, FCConvert.ToString(false));
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						vsInstallers.TextMatrix(vsInstallers.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields("UserID")));
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				else
				{
					vsInstallers.AddItem("");
					vsInstallers.TextMatrix(vsInstallers.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsInstallers.TextMatrix(vsInstallers.Rows - 1, DescriptionCol, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID()));
				}
			}
		}

		public void SetInstallersGrid(ref string strSearch)
		{
			string[] strInstallers = null;
			int counter;
			int intRowCounter;
			for (counter = 0; counter <= vsInstallers.Rows - 1; counter++)
			{
				vsInstallers.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
			strInstallers = Strings.Split(strSearch, ",", -1, CompareConstants.vbBinaryCompare);
			for (counter = 0; counter <= Information.UBound(strInstallers, 1); counter++)
			{
				for (intRowCounter = 0; intRowCounter <= vsInstallers.Rows - 1; intRowCounter++)
				{
					if (Strings.Trim(Strings.Left(vsInstallers.TextMatrix(intRowCounter, DescriptionCol), strInstallers[counter].Length)) == Strings.Trim(strInstallers[counter]))
					{
						vsInstallers.TextMatrix(intRowCounter, SelectCol, FCConvert.ToString(true));
					}
				}
			}
		}

		private void LoadUsers()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboUser.Clear();
			rsInfo.OpenRecordset("SELECT * FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' ORDER BY UserID", "ClientSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (!blnEditUserOnly)
				{
					do
					{
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						cboUser.AddItem(FCConvert.ToString(rsInfo.Get_Fields("UserID")));
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				else
				{
					cboUser.AddItem(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
				}
			}
		}

		public void SetUserCombo_2(string strSearch)
		{
			SetUserCombo(ref strSearch);
		}

		public void SetUserCombo(ref string strSearch)
		{
			int counter;
			if (strSearch == "")
			{
				cboUser.SelectedIndex = -1;
			}
			else
			{
				for (counter = 0; counter <= cboUser.Items.Count - 1; counter++)
				{
					if (Strings.Left(cboUser.Items[counter].ToString(), strSearch.Length) == strSearch)
					{
						cboUser.SelectedIndex = counter;
						break;
					}
				}
			}
		}

		private void optNo_CheckedChanged(object sender, System.EventArgs e)
		{
			cboReminderTime.Enabled = false;
			cboReminderTime.SelectedIndex = -1;
		}

		private void optYes_CheckedChanged(object sender, System.EventArgs e)
		{
			cboReminderTime.Enabled = true;
			cboReminderTime.SelectedIndex = 0;
		}

		private void vsInstallers_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsInstallers.Row >= 0)
			{
				if (FCConvert.CBool(vsInstallers.TextMatrix(vsInstallers.Row, SelectCol)) == false)
				{
					vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsInstallers_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space)
			{
				if (vsInstallers.Row >= 0)
				{
					//e.KeyCode = 0;
					if (FCConvert.CBool(vsInstallers.TextMatrix(vsInstallers.Row, SelectCol)) == false)
					{
						vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void SetReminderTimeCombo_2(DateTime datReminderStart, DateTime datTaskDate)
		{
			SetReminderTimeCombo(ref datReminderStart, ref datTaskDate);
		}

		private void SetReminderTimeCombo(ref DateTime datReminderStart, ref DateTime datTaskDate)
		{
			// vbPorter upgrade warning: intDays As short, int --> As long
			long intDays;
			intDays = DateAndTime.DateDiff("d", datReminderStart, datTaskDate);
			if (intDays <= 0)
			{
				cboReminderTime.SelectedIndex = 0;
			}
			else if (intDays == 1)
			{
				cboReminderTime.SelectedIndex = 1;
			}
			else
			{
				cboReminderTime.SelectedIndex = 2;
			}
		}

		private DateTime CalculateReminderStartDate()
		{
			DateTime CalculateReminderStartDate = System.DateTime.Now;
			if (cboReminderTime.SelectedIndex == 0)
			{
				CalculateReminderStartDate = datTaskDate;
			}
			else if (cboReminderTime.SelectedIndex == 1)
			{
				CalculateReminderStartDate = DateAndTime.DateAdd("d", -1, datTaskDate);
			}
			else
			{
				CalculateReminderStartDate = DateAndTime.DateAdd("d", -7, datTaskDate);
			}
			return CalculateReminderStartDate;
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity2.CNSTEDITCALENDARALL:
						{
							if (strPerm == "N")
							{
								blnEditUserOnly = true;
							}
							else
							{
								blnEditUserOnly = false;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void cmbYes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbYes.Text == "Yes")
			{
				optYes_CheckedChanged(sender, e);
			}
			else
			{
				optNo_CheckedChanged(sender, e);
			}
		}
	}
}
