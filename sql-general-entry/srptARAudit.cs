﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptARAudit.
	/// </summary>
	public partial class srptARAudit : FCSectionReport
	{
		public srptARAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptARAudit";
		}

		public static srptARAudit InstancePtr
		{
			get
			{
				return (srptARAudit)Sys.GetInstance(typeof(srptARAudit));
			}
		}

		protected srptARAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptARAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int intReport;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				//SetReport();
				eArgs.EOF = false;
			}
			else
			{
				intReport += 1;
				if (intReport > 0)
				{
					eArgs.EOF = true;
				}
				else
				{
					//SetReport();
					eArgs.EOF = false;
				}
			}
		}

		private void Detail_Format(object sender, System.EventArgs e)
		{
			SetReport();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			intReport = 0;
			modValidateAccount.SetBDFormats();
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Recalculating Account Summary Information");
			frmWait.InstancePtr.Show();
			// If gboolTownAccounts Then
			modBudgetaryAccounting.CalculateAccountInfo();
			// End If
			frmWait.InstancePtr.Unload();
		}

		private void SetReport()
		{
			switch (intReport)
			{
				case 0:
					{
						subCLReport.Report = new srptAR1();
						break;
					}
			}
			//end switch
		}

		
	}
}
