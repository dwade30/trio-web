﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD2.
	/// </summary>
	public partial class srptBD2 : FCSectionReport
	{
		public srptBD2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD2";
		}

		public static srptBD2 InstancePtr
		{
			get
			{
				return (srptBD2)Sys.GetInstance(typeof(srptBD2));
			}
		}

		protected srptBD2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		bool blnNoData;
		clsDRWrapper rsBalanceInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				CheckNext:
				;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (AccountIsBad_2(rsInfo.Get_Fields_String("Account")))
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM ExpenseReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					}
					else if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "R")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					}
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsBalanceInfo.Get_Fields("OriginalBudgetTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("BudgetAdjustmentsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedCreditsTotal")) != 0)
					{
						eArgs.EOF = false;
					}
					else
					{
						rsInfo.MoveNext();
						if (rsInfo.EndOfFile() != true)
						{
							goto CheckNext;
						}
						else
						{
							blnNoData = true;
							eArgs.EOF = false;
						}
					}
				}
				else
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile() != true)
					{
						goto CheckNext;
					}
					else
					{
						blnNoData = true;
						eArgs.EOF = false;
					}
				}
			}
			else
			{
				CheckNext2:
				;
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (AccountIsBad_2(rsInfo.Get_Fields_String("Account")))
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM ExpenseReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						}
						else if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBalanceInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = '" + rsInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
						}
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsBalanceInfo.Get_Fields("OriginalBudgetTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("BudgetAdjustmentsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsBalanceInfo.Get_Fields("PostedCreditsTotal")) != 0)
						{
							eArgs.EOF = false;
						}
						else
						{
							goto CheckNext2;
						}
					}
					else
					{
						goto CheckNext2;
					}
				}
				else
				{
					if (blnNoData && fldNone.Visible == false)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT DISTINCT Account FROM (SELECT DISTINCT Account FROM APJournalDetail UNION ALL SELECT DISTINCT Account FROM JournalEntries UNION ALL SELECT DISTINCT Account FROM EncumbranceDetail) as temp ORDER BY Account", "TWBD0000.vb1");
			blnNoData = true;
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				blnFirstRecord = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount1.Text = rsInfo.Get_Fields_String("Account");
				blnNoData = false;
			}
			fldAccount2.Text = "";
			fldAccount3.Text = "";
			fldAccount4.Text = "";
			rsInfo.MoveNext();
			if (rsInfo.EndOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (AccountIsBad_2(rsInfo.Get_Fields_String("Account")))
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						fldAccount2.Text = rsInfo.Get_Fields_String("Account");
						blnNoData = false;
						break;
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			rsInfo.MoveNext();
			if (rsInfo.EndOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (AccountIsBad_2(rsInfo.Get_Fields_String("Account")))
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						fldAccount3.Text = rsInfo.Get_Fields_String("Account");
						blnNoData = false;
						break;
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			rsInfo.MoveNext();
			if (rsInfo.EndOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (AccountIsBad_2(rsInfo.Get_Fields_String("Account")))
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						fldAccount4.Text = rsInfo.Get_Fields_String("Account");
						blnNoData = false;
						break;
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			if (blnNoData && rsInfo.EndOfFile())
			{
				fldAccount1.Text = "";
				fldAccount2.Text = "";
				fldAccount3.Text = "";
				fldAccount4.Text = "";
				fldNone.Visible = true;
			}
		}

		private bool AccountIsBad_2(string strAcct)
		{
			return AccountIsBad(ref strAcct);
		}

		private bool AccountIsBad(ref string strAcct)
		{
			bool AccountIsBad = false;
			if (Strings.Left(strAcct, 1) == "E")
			{
				AccountIsBad = BadExpense(ref strAcct);
			}
			else if (Strings.Left(strAcct, 1) == "R" || Strings.Left(strAcct, 1) == "G")
			{
				AccountIsBad = BadRevOrLedger(ref strAcct);
			}
			else
			{
				AccountIsBad = true;
			}
			return AccountIsBad;
		}

		private bool BadExpense(ref string strAcct)
		{
			bool BadExpense = false;
            using (clsDRWrapper rsInfo = new clsDRWrapper())
            {
                BadExpense = false;
                if (modAccountTitle.Statics.ExpDivFlag)
                {
                    rsInfo.OpenRecordset(
                        "SELECT * FROM DeptDivTitles WHERE Department = '" +
                        modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '0'", "TWBD0000.vb1");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        BadExpense = true;
                        return BadExpense;
                    }
                }
                else
                {
                    rsInfo.OpenRecordset(
                        "SELECT * FROM DeptDivTitles WHERE Department = '" +
                        modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '" +
                        modBudgetaryAccounting.GetExpDivision(strAcct) + "'", "TWBD0000.vb1");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        BadExpense = true;
                        return BadExpense;
                    }
                }

                if (modAccountTitle.Statics.ObjFlag)
                {
                    rsInfo.OpenRecordset(
                        "SELECT * FROM ExpObjTitles WHERE Expense = '" + modBudgetaryAccounting.GetExpense(strAcct) +
                        "' AND Object = '0'", "TWBD0000.vb1");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        BadExpense = true;
                        return BadExpense;
                    }
                }
                else
                {
                    rsInfo.OpenRecordset(
                        "SELECT * FROM ExpObjTitles WHERE Expense = '" + modBudgetaryAccounting.GetExpense(strAcct) +
                        "' AND Object = '" + modBudgetaryAccounting.GetObject(strAcct) + "'", "TWBD0000.vb1");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        BadExpense = true;
                        return BadExpense;
                    }
                }
            }

            return BadExpense;
		}

		private bool BadRevOrLedger(ref string strAcct)
		{
			bool BadRevOrLedger = false;
            using (clsDRWrapper rsInfo = new clsDRWrapper())
            {
                BadRevOrLedger = false;
                rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + strAcct + "'", "TWBD0000.vb1");
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    // do nothing
                }
                else
                {
                    BadRevOrLedger = true;
                }
            }

            return BadRevOrLedger;
		}

		
	}
}
