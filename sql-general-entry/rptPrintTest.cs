﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptPrintTest.
	/// </summary>
	public partial class rptPrintTest : BaseSectionReport
	{
		public rptPrintTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport Preview Print Test";
		}

		public static rptPrintTest InstancePtr
		{
			get
			{
				return (rptPrintTest)Sys.GetInstance(typeof(rptPrintTest));
			}
		}

		protected rptPrintTest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrintTest	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
		}

		
	}
}
