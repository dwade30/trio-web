﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptAuditArchiveReport.
	/// </summary>
	public partial class rptAuditArchiveReport : BaseSectionReport
	{
		public rptAuditArchiveReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Archive Report";
		}

		public static rptAuditArchiveReport InstancePtr
		{
			get
			{
				return (rptAuditArchiveReport)Sys.GetInstance(typeof(rptAuditArchiveReport));
			}
		}

		protected rptAuditArchiveReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditArchiveReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsData.MoveNext();
				eArgs.EOF = rsData.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			/*? On Error Resume Next  */
			try
			{
				// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
				// MUNINAME, DATE AND TIME
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
				//txtMuniName = gstrMuniName;
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				blnFirstRecord = true;
				strSQL = "";
				if (frmSetupAuditArchiveReport.InstancePtr.cmbPartyIdAll.SelectedIndex == 0)
				{
					// do nothing
				}
				else
				{
					strSQL += "UserField1 = '" + Strings.Trim(frmSetupAuditArchiveReport.InstancePtr.cboPartyID.Text) + "' AND ";
				}
				if (frmSetupAuditArchiveReport.InstancePtr.cmbNameSelected.SelectedIndex == 0)
				{
					// do nothing
				}
				else
				{
					strSQL += "UserField2 = '" + Strings.Trim(frmSetupAuditArchiveReport.InstancePtr.cboName.Text) + "' AND ";
				}
				if (frmSetupAuditArchiveReport.InstancePtr.cmbScreenAll.SelectedIndex == 0)
				{
					// do nothing
				}
				else
				{
					strSQL += "Location = '" + frmSetupAuditArchiveReport.InstancePtr.cboScreen.Text + "' AND ";
				}
				if (frmSetupAuditArchiveReport.InstancePtr.cmbDateAll.SelectedIndex == 0)
				{
					// do nothing
				}
				else
				{
					strSQL += "(DateUpdated >= '" + frmSetupAuditArchiveReport.InstancePtr.txtLowDate.Text + "' AND DateUpdated <= '" + frmSetupAuditArchiveReport.InstancePtr.txtHighDate.Text + "') AND ";
				}
				if (strSQL != "")
				{
					strSQL = Strings.Left(strSQL, strSQL.Length - 5);
					rsData.OpenRecordset("Select * from AuditChangesArchive WHERE " + strSQL + " ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1, UserField2", modGNBas.DEFAULTDATABASE);
				}
				else
				{
					rsData.OpenRecordset("Select * from AuditChangesArchive ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1, UserField2", modGNBas.DEFAULTDATABASE);
				}
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Cancel();
				}
			}
			catch
			{
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//object fldPartyID, fldName;	// - "AutoDim"
			fldPartyID.Text = rsData.Get_Fields_String("UserField1");
			fldName.Text = rsData.Get_Fields_String("UserField2");
			// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
			txtUser.Text = rsData.Get_Fields_String("UserID");
			//FC:FINAL:CHN - i.issue #1617: Incorrect data on report. 
			// txtDateTimeField.Text = rsData.Get_Fields_DateTime("DateUpdated")+" "+rsData.Get_Fields_DateTime("TimeUpdated");
			txtDateTimeField.Text = Strings.Format(rsData.Get_Fields("DateUpdated"), "MM/dd/yyyy") + " " + rsData.Get_Fields_DateTime("TimeUpdated");
			fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
			fldDescription.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void rptAuditArchiveReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAuditArchiveReport properties;
			//rptAuditArchiveReport.Caption	= "Audit Archive Report";
			//rptAuditArchiveReport.Icon	= "rptAuditArchiveReport.dsx":0000";
			//rptAuditArchiveReport.Left	= 0;
			//rptAuditArchiveReport.Top	= 0;
			//rptAuditArchiveReport.Width	= 19080;
			//rptAuditArchiveReport.Height	= 12990;
			//rptAuditArchiveReport.StartUpPosition	= 3;
			//rptAuditArchiveReport.WindowState	= 2;
			//rptAuditArchiveReport.SectionData	= "rptAuditArchiveReport.dsx":058A;
			//End Unmaped Properties
		}

		private void rptAuditArchiveReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
