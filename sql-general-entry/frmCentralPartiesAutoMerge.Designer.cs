﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmCentralPartiesAutoMerge.
	/// </summary>
	partial class frmCentralPartiesAutoMerge : BaseForm
	{
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCButton cmdStop;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCProgressBar prgProgress;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblProgress;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCentralPartiesAutoMerge));
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.cmdStop = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.prgProgress = new fecherFoundation.FCProgressBar();
			this.lblRange = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblProgress = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdStop)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 374);
			this.BottomPanel.Size = new System.Drawing.Size(416, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.cmdStop);
			this.ClientArea.Controls.Add(this.prgProgress);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblProgress);
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Size = new System.Drawing.Size(416, 314);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(416, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(290, 30);
			this.HeaderText.Text = "Central Party Auto Merge";
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(250, 58);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(138, 40);
			this.txtEnd.TabIndex = 5;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(30, 58);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(138, 40);
			this.txtStart.TabIndex = 3;
			// 
			// cmdStop
			// 
			this.cmdStop.AppearanceKey = "actionButton";
			this.cmdStop.ForeColor = System.Drawing.Color.White;
			this.cmdStop.Location = new System.Drawing.Point(159, 187);
			this.cmdStop.Name = "cmdStop";
			this.cmdStop.Size = new System.Drawing.Size(93, 48);
			this.cmdStop.TabIndex = 7;
			this.cmdStop.Text = "Stop";
			this.cmdStop.Click += new System.EventHandler(this.cmdStop_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.ForeColor = System.Drawing.Color.White;
			this.cmdProcess.Location = new System.Drawing.Point(30, 187);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(109, 48);
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.TabIndex = 6;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// prgProgress
			// 
			this.prgProgress.Location = new System.Drawing.Point(30, 147);
			this.prgProgress.Name = "prgProgress";
			this.prgProgress.Size = new System.Drawing.Size(356, 20);
			this.prgProgress.TabIndex = 1;
			this.prgProgress.Visible = false;
			// 
			// lblRange
			// 
			this.lblRange.Location = new System.Drawing.Point(30, 30);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(356, 17);
			this.lblRange.TabIndex = 2;
			this.lblRange.Text = "ENTER A NAME RANGE TO LIMIT THE NAMES SEARCHED";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(186, 72);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(46, 21);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(30, 117);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(173, 23);
			this.lblProgress.TabIndex = 0;
			this.lblProgress.Text = "LABEL1";
			this.lblProgress.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmCentralPartiesAutoMerge
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(416, 322);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmCentralPartiesAutoMerge";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Central Party Auto Merge";
			this.Load += new System.EventHandler(this.frmCentralPartiesAutoMerge_Load);
			this.Activated += new System.EventHandler(this.frmCentralPartiesAutoMerge_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCentralPartiesAutoMerge_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdStop)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
