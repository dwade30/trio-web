﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.DataBaseLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmTieMVtoUser.
	/// </summary>
	partial class frmTieMVtoUser : BaseForm
	{
		public fecherFoundation.FCGrid grid1;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		//public AxvsElasticLightLib.AxvsElasticLight vsElasticLight2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.grid1 = new fecherFoundation.FCGrid();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.grid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 321);
			this.BottomPanel.Size = new System.Drawing.Size(462, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.grid1);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Size = new System.Drawing.Size(462, 261);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(462, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(379, 30);
			this.HeaderText.Text = "Tie Users to Motor Vehicle OpIDs";
			// 
			// grid1
			// 
			this.grid1.AllowSelection = false;
			this.grid1.AllowUserToResizeColumns = false;
			this.grid1.AllowUserToResizeRows = false;
			this.grid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.grid1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.grid1.BackColorAlternate = System.Drawing.Color.Empty;
			this.grid1.BackColorBkg = System.Drawing.Color.Empty;
			this.grid1.BackColorFixed = System.Drawing.Color.Empty;
			this.grid1.BackColorSel = System.Drawing.Color.Empty;
			this.grid1.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.grid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.grid1.ColumnHeadersHeight = 30;
			this.grid1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.grid1.DefaultCellStyle = dataGridViewCellStyle2;
			this.grid1.DragIcon = null;
			this.grid1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.grid1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.grid1.ExtendLastCol = true;
			this.grid1.FixedCols = 0;
			this.grid1.ForeColorFixed = System.Drawing.Color.Empty;
			this.grid1.FrozenCols = 0;
			this.grid1.GridColor = System.Drawing.Color.Empty;
			this.grid1.GridColorFixed = System.Drawing.Color.Empty;
			this.grid1.Location = new System.Drawing.Point(30, 30);
			this.grid1.Name = "grid1";
			this.grid1.RowHeadersVisible = false;
			this.grid1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.grid1.RowHeightMin = 0;
			this.grid1.Rows = 1;
			this.grid1.ScrollTipText = null;
			this.grid1.ShowColumnVisibilityMenu = false;
			this.grid1.Size = new System.Drawing.Size(408, 170);
			this.grid1.StandardTab = true;
			this.grid1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.grid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.grid1.TabIndex = 0;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(130, 215);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(81, 40);
			this.cmdCancel.TabIndex = 2;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(30, 215);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(84, 40);
			this.cmdOK.TabIndex = 1;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// frmTieMVtoUser
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(462, 429);
			this.ControlBox = false;
			this.Name = "frmTieMVtoUser";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Tie Users to Motor Vehicle OpIDs";
			this.Load += new System.EventHandler(this.frmTieMVtoUser_Load);
			this.Activated += new System.EventHandler(this.frmTieMVtoUser_Activated);
			this.Resize += new System.EventHandler(this.frmTieMVtoUser_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.grid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
