namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptProgramNotes.
	/// </summary>
	partial class rptProgramNotes
	{

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProgramNotes));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModule = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProgramNotes = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtProgramNotes});
			this.Detail.Height = 0.05208333F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtDate,
            this.txtMuniname,
            this.txtPage,
            this.txtTime});
			this.PageHeader.Height = 0.5F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldModule,
            this.Binder});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.1875F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.208333F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Program Notifications";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.84375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.125F;
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.1875F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Text = "Field1";
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 2.15625F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.84375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.3125F;
			// 
			// fldModule
			// 
			this.fldModule.Height = 0.1875F;
			this.fldModule.Left = 0F;
			this.fldModule.Name = "fldModule";
			this.fldModule.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-decoration: underl" +
    "ine; ddo-char-set: 1";
			this.fldModule.Text = "Field1";
			this.fldModule.Top = 0F;
			this.fldModule.Width = 2.96875F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 4.125F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field2";
			this.Binder.Top = 0.0625F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.90625F;
			// 
			// txtProgramNotes
			// 
			this.txtProgramNotes.Font = new System.Drawing.Font("Arial", 10F);
			this.txtProgramNotes.Height = 0.0625F;
			this.txtProgramNotes.Left = 0.28125F;
			this.txtProgramNotes.Name = "txtProgramNotes";
			this.txtProgramNotes.RTF = resources.GetString("txtProgramNotes.RTF");
			this.txtProgramNotes.Top = 0F;
			this.txtProgramNotes.Width = 7.1875F;
			// 
			// rptProgramNotes
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox txtProgramNotes;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModule;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
