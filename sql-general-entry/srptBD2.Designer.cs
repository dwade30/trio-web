﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD2.
	/// </summary>
	partial class srptBD2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBD2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAccount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount1,
				this.fldAccount2,
				this.fldAccount3,
				this.fldAccount4,
				this.fldNone
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1
			});
			this.ReportHeader.Height = 0.2708333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Bad Account List";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.59375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.25F;
			this.Line1.Width = 6.28125F;
			this.Line1.X1 = 0.59375F;
			this.Line1.X2 = 6.875F;
			this.Line1.Y1 = 0.25F;
			this.Line1.Y2 = 0.25F;
			// 
			// fldAccount1
			// 
			this.fldAccount1.Height = 0.1875F;
			this.fldAccount1.Left = 0.59375F;
			this.fldAccount1.Name = "fldAccount1";
			this.fldAccount1.Style = "font-size: 9pt";
			this.fldAccount1.Text = "Field1";
			this.fldAccount1.Top = 0F;
			this.fldAccount1.Width = 1.5F;
			// 
			// fldAccount2
			// 
			this.fldAccount2.Height = 0.1875F;
			this.fldAccount2.Left = 2.1875F;
			this.fldAccount2.Name = "fldAccount2";
			this.fldAccount2.Style = "font-size: 9pt";
			this.fldAccount2.Text = "Field1";
			this.fldAccount2.Top = 0F;
			this.fldAccount2.Width = 1.5F;
			// 
			// fldAccount3
			// 
			this.fldAccount3.Height = 0.1875F;
			this.fldAccount3.Left = 3.78125F;
			this.fldAccount3.Name = "fldAccount3";
			this.fldAccount3.Style = "font-size: 9pt";
			this.fldAccount3.Text = "Field1";
			this.fldAccount3.Top = 0F;
			this.fldAccount3.Width = 1.5F;
			// 
			// fldAccount4
			// 
			this.fldAccount4.Height = 0.1875F;
			this.fldAccount4.Left = 5.375F;
			this.fldAccount4.Name = "fldAccount4";
			this.fldAccount4.Style = "font-size: 9pt";
			this.fldAccount4.Text = "Field3";
			this.fldAccount4.Top = 0F;
			this.fldAccount4.Width = 1.5F;
			// 
			// fldNone
			// 
			this.fldNone.Height = 0.1875F;
			this.fldNone.Left = 3F;
			this.fldNone.Name = "fldNone";
			this.fldNone.Style = "font-size: 9pt; text-align: center";
			this.fldNone.Text = "NONE";
			this.fldNone.Top = 0F;
			this.fldNone.Visible = false;
			this.fldNone.Width = 1.5F;
			// 
			// srptBD2
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNone;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
