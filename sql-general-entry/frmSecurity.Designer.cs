﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSecurity.
	/// </summary>
	partial class frmSecurity : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lbl1;
		public fecherFoundation.FCFrame fraSecurity;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCTextBox txtT;
		public fecherFoundation.FCButton cmdSaveOptions;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCListBox lstUsers;
		public fecherFoundation.FCComboBox cmbModule;
		public fecherFoundation.FCFrame fraPayroll;
		public FCGrid vsPayroll;
		public fecherFoundation.FCFrame fraInfo;
		public fecherFoundation.FCTextBox txtPasswordConfirm;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCTextBox txtFrequency;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtUserID;
		public fecherFoundation.FCTextBox txtOPID;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCLabel lbl1_1;
		public fecherFoundation.FCLabel lbl1_2;
		public fecherFoundation.FCLabel lbl1_3;
		public fecherFoundation.FCLabel lbl1_0;
		public fecherFoundation.FCLabel Label3;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddPayrollDeptDiv;
		public fecherFoundation.FCToolStripMenuItem mnuDeletePayrollDeptDiv;
		public fecherFoundation.FCToolStripMenuItem mnuPayrollSecurity;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuEditSignatures;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteInfo;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuSaveInfo;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeparator1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSecurity));
            this.fraSecurity = new fecherFoundation.FCFrame();
            this.vs1 = new fecherFoundation.FCGrid();
            this.txtT = new fecherFoundation.FCTextBox();
            this.cmdSaveOptions = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.lstUsers = new fecherFoundation.FCListBox();
            this.cmbModule = new fecherFoundation.FCComboBox();
            this.fraPayroll = new fecherFoundation.FCFrame();
            this.vsPayroll = new fecherFoundation.FCGrid();
            this.fraInfo = new fecherFoundation.FCFrame();
            this.lblUpdateSite = new fecherFoundation.FCLabel();
            this.chkAllowSiteUpdate = new fecherFoundation.FCCheckBox();
            this.btnUnlockUser = new fecherFoundation.FCButton();
            this.txtPasswordConfirm = new fecherFoundation.FCTextBox();
            this.txtPassword = new fecherFoundation.FCTextBox();
            this.txtFrequency = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.txtUserID = new fecherFoundation.FCTextBox();
            this.txtOPID = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lbl1_1 = new fecherFoundation.FCLabel();
            this.lbl1_2 = new fecherFoundation.FCLabel();
            this.lbl1_3 = new fecherFoundation.FCLabel();
            this.lbl1_0 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddPayrollDeptDiv = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeletePayrollDeptDiv = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayrollSecurity = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditSignatures = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeparator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSaveInfo = new fecherFoundation.FCButton();
            this.cmdDeleteInfo = new fecherFoundation.FCButton();
            this.cmdPayrollSecurity = new fecherFoundation.FCButton();
            this.cmdDeletePayrollDeptDiv = new fecherFoundation.FCButton();
            this.cmdAddPayrollDeptDiv = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSecurity)).BeginInit();
            this.fraSecurity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayroll)).BeginInit();
            this.fraPayroll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayroll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraInfo)).BeginInit();
            this.fraInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowSiteUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUnlockUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPayrollSecurity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeletePayrollDeptDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPayrollDeptDiv)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveInfo);
            this.BottomPanel.Location = new System.Drawing.Point(0, 694);
            this.BottomPanel.Size = new System.Drawing.Size(1090, 89);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSecurity);
            this.ClientArea.Controls.Add(this.lstUsers);
            this.ClientArea.Controls.Add(this.cmbModule);
            this.ClientArea.Controls.Add(this.fraInfo);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.fraPayroll);
            this.ClientArea.Size = new System.Drawing.Size(1110, 818);
            this.ClientArea.Controls.SetChildIndex(this.fraPayroll, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbModule, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstUsers, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraSecurity, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddPayrollDeptDiv);
            this.TopPanel.Controls.Add(this.cmdDeletePayrollDeptDiv);
            this.TopPanel.Controls.Add(this.cmdPayrollSecurity);
            this.TopPanel.Controls.Add(this.cmdDeleteInfo);
            this.TopPanel.Size = new System.Drawing.Size(1110, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPayrollSecurity, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeletePayrollDeptDiv, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddPayrollDeptDiv, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(160, 28);
            this.HeaderText.Text = "Security Setup";
            // 
            // fraSecurity
            // 
            this.fraSecurity.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraSecurity.Controls.Add(this.vs1);
            this.fraSecurity.Controls.Add(this.txtT);
            this.fraSecurity.Controls.Add(this.cmdSaveOptions);
            this.fraSecurity.Controls.Add(this.cmdCancel);
            this.fraSecurity.Location = new System.Drawing.Point(533, 82);
            this.fraSecurity.Name = "fraSecurity";
            this.fraSecurity.Size = new System.Drawing.Size(746, 612);
            this.fraSecurity.TabIndex = 10;
            this.fraSecurity.Text = "Security Setup";
            this.fraSecurity.Visible = false;
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 6;
            this.vs1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vs1.ExtendLastCol = true;
            this.vs1.Location = new System.Drawing.Point(20, 30);
            this.vs1.Name = "vs1";
            this.vs1.ReadOnly = false;
            this.vs1.Rows = 1;
            this.vs1.ShowFocusCell = false;
            this.vs1.Size = new System.Drawing.Size(706, 512);
            this.vs1.TabIndex = 0;
            this.vs1.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_AfterEdit);
            this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_BeforeEdit);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // txtT
            // 
            this.txtT.BackColor = System.Drawing.SystemColors.Window;
            this.txtT.Location = new System.Drawing.Point(210, 503);
            this.txtT.Name = "txtT";
            this.txtT.Size = new System.Drawing.Size(25, 40);
            this.txtT.TabIndex = 22;
            this.txtT.Visible = false;
            // 
            // cmdSaveOptions
            // 
            this.cmdSaveOptions.AppearanceKey = "actionButton";
            this.cmdSaveOptions.ForeColor = System.Drawing.Color.White;
            this.cmdSaveOptions.Location = new System.Drawing.Point(232, 556);
            this.cmdSaveOptions.Name = "cmdSaveOptions";
            this.cmdSaveOptions.Size = new System.Drawing.Size(78, 40);
            this.cmdSaveOptions.TabIndex = 1;
            this.cmdSaveOptions.Text = "Save";
            this.cmdSaveOptions.Click += new System.EventHandler(this.cmdSaveOptions_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(363, 556);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(74, 24);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "Exit";
            this.cmdCancel.Visible = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // lstUsers
            // 
            this.lstUsers.BackColor = System.Drawing.SystemColors.Window;
            this.lstUsers.Location = new System.Drawing.Point(30, 30);
            this.lstUsers.Name = "lstUsers";
            this.lstUsers.Size = new System.Drawing.Size(443, 169);
            this.lstUsers.TabIndex = 11;
            this.lstUsers.SelectedIndexChanged += new System.EventHandler(this.lstUsers_SelectedIndexChanged);
            // 
            // cmbModule
            // 
            this.cmbModule.BackColor = System.Drawing.SystemColors.Window;
            this.cmbModule.Location = new System.Drawing.Point(817, 30);
            this.cmbModule.Name = "cmbModule";
            this.cmbModule.Size = new System.Drawing.Size(258, 40);
            this.cmbModule.Sorted = true;
            this.cmbModule.TabIndex = 9;
            this.cmbModule.SelectedIndexChanged += new System.EventHandler(this.cmbModule_SelectedIndexChanged);
            // 
            // fraPayroll
            // 
            this.fraPayroll.Controls.Add(this.vsPayroll);
            this.fraPayroll.Location = new System.Drawing.Point(30, 233);
            this.fraPayroll.Name = "fraPayroll";
            this.fraPayroll.Size = new System.Drawing.Size(443, 342);
            this.fraPayroll.TabIndex = 7;
            this.fraPayroll.Text = "Payroll Dept/Div Passwords";
            this.fraPayroll.Visible = false;
            // 
            // vsPayroll
            // 
            this.vsPayroll.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPayroll.FixedCols = 0;
            this.vsPayroll.Location = new System.Drawing.Point(20, 30);
            this.vsPayroll.Name = "vsPayroll";
            this.vsPayroll.RowHeadersVisible = false;
            this.vsPayroll.ShowFocusCell = false;
            this.vsPayroll.Size = new System.Drawing.Size(395, 283);
            this.vsPayroll.StandardTab = false;
            this.vsPayroll.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsPayroll.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.vsPayroll, "Insert key will enter a new record");
            this.vsPayroll.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsPayroll_KeyDownEdit);
            this.vsPayroll.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayroll_KeyDownEvent);
            // 
            // fraInfo
            // 
            this.fraInfo.Controls.Add(this.lblUpdateSite);
            this.fraInfo.Controls.Add(this.chkAllowSiteUpdate);
            this.fraInfo.Controls.Add(this.btnUnlockUser);
            this.fraInfo.Controls.Add(this.txtPasswordConfirm);
            this.fraInfo.Controls.Add(this.txtPassword);
            this.fraInfo.Controls.Add(this.txtFrequency);
            this.fraInfo.Controls.Add(this.txtName);
            this.fraInfo.Controls.Add(this.txtUserID);
            this.fraInfo.Controls.Add(this.txtOPID);
            this.fraInfo.Controls.Add(this.Label2);
            this.fraInfo.Controls.Add(this.Label1);
            this.fraInfo.Controls.Add(this.lbl1_1);
            this.fraInfo.Controls.Add(this.lbl1_2);
            this.fraInfo.Controls.Add(this.lbl1_3);
            this.fraInfo.Controls.Add(this.lbl1_0);
            this.fraInfo.Location = new System.Drawing.Point(30, 234);
            this.fraInfo.Name = "fraInfo";
            this.fraInfo.Size = new System.Drawing.Size(374, 383);
            this.fraInfo.TabIndex = 16;
            this.fraInfo.Text = "Selected User";
            // 
            // lblUpdateSite
            // 
            this.lblUpdateSite.Location = new System.Drawing.Point(20, 344);
            this.lblUpdateSite.Name = "lblUpdateSite";
            this.lblUpdateSite.Size = new System.Drawing.Size(131, 18);
            this.lblUpdateSite.TabIndex = 29;
            this.lblUpdateSite.Text = "CAN UPDATE SITE";
            this.lblUpdateSite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUpdateSite.Visible = false;
            // 
            // chkAllowSiteUpdate
            // 
            this.chkAllowSiteUpdate.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAllowSiteUpdate.Location = new System.Drawing.Point(218, 345);
            this.chkAllowSiteUpdate.Name = "chkAllowSiteUpdate";
            this.chkAllowSiteUpdate.Size = new System.Drawing.Size(32, 22);
            this.chkAllowSiteUpdate.TabIndex = 12;
            this.chkAllowSiteUpdate.Visible = false;
            // 
            // btnUnlockUser
            // 
            this.btnUnlockUser.Location = new System.Drawing.Point(137, 32);
            this.btnUnlockUser.Name = "btnUnlockUser";
            this.btnUnlockUser.Size = new System.Drawing.Size(100, 36);
            this.btnUnlockUser.TabIndex = 26;
            this.btnUnlockUser.Text = "Unlock User";
            this.btnUnlockUser.Visible = false;
            this.btnUnlockUser.Click += new System.EventHandler(this.btnUnlockUser_Click);
            // 
            // txtPasswordConfirm
            // 
            this.txtPasswordConfirm.BackColor = System.Drawing.SystemColors.Window;
            this.txtPasswordConfirm.InputType.Type = Wisej.Web.TextBoxType.Password;
            this.txtPasswordConfirm.Location = new System.Drawing.Point(218, 280);
            this.txtPasswordConfirm.Name = "txtPasswordConfirm";
            this.txtPasswordConfirm.PasswordChar = '*';
            this.txtPasswordConfirm.Size = new System.Drawing.Size(112, 40);
            this.txtPasswordConfirm.TabIndex = 11;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
            this.txtPassword.Location = new System.Drawing.Point(218, 230);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(112, 40);
            this.txtPassword.TabIndex = 10;
            // 
            // txtFrequency
            // 
            this.txtFrequency.BackColor = System.Drawing.SystemColors.Window;
            this.txtFrequency.Location = new System.Drawing.Point(218, 180);
            this.txtFrequency.Name = "txtFrequency";
            this.txtFrequency.Size = new System.Drawing.Size(112, 40);
            this.txtFrequency.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtFrequency, "The number of days until the user must change their password.  Enter 0 for never");
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(218, 130);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(112, 40);
            this.txtName.TabIndex = 8;
            // 
            // txtUserID
            // 
            this.txtUserID.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserID.Enabled = false;
            this.txtUserID.Location = new System.Drawing.Point(218, 30);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(112, 40);
            this.txtUserID.TabIndex = 15;
            this.txtUserID.Visible = false;
            // 
            // txtOPID
            // 
            this.txtOPID.BackColor = System.Drawing.SystemColors.Window;
            this.txtOPID.Location = new System.Drawing.Point(218, 80);
            this.txtOPID.Name = "txtOPID";
            this.txtOPID.Size = new System.Drawing.Size(112, 40);
            this.txtOPID.TabIndex = 7;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 294);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(131, 18);
            this.Label2.TabIndex = 25;
            this.Label2.Text = "PWD CONFIRMATION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 244);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(82, 20);
            this.Label1.TabIndex = 24;
            this.Label1.Text = "PASSWORD";
            // 
            // lbl1_1
            // 
            this.lbl1_1.Location = new System.Drawing.Point(20, 94);
            this.lbl1_1.Name = "lbl1_1";
            this.lbl1_1.Size = new System.Drawing.Size(84, 16);
            this.lbl1_1.TabIndex = 20;
            this.lbl1_1.Text = "USER ID";
            // 
            // lbl1_2
            // 
            this.lbl1_2.Location = new System.Drawing.Point(20, 144);
            this.lbl1_2.Name = "lbl1_2";
            this.lbl1_2.Size = new System.Drawing.Size(84, 16);
            this.lbl1_2.TabIndex = 19;
            this.lbl1_2.Text = "USER NAME";
            // 
            // lbl1_3
            // 
            this.lbl1_3.Location = new System.Drawing.Point(20, 194);
            this.lbl1_3.Name = "lbl1_3";
            this.lbl1_3.Size = new System.Drawing.Size(84, 16);
            this.lbl1_3.TabIndex = 18;
            this.lbl1_3.Text = "FREQUENCY";
            this.ToolTip1.SetToolTip(this.lbl1_3, "The number of days until the user must change their password.  Enter 0 for never");
            // 
            // lbl1_0
            // 
            this.lbl1_0.Location = new System.Drawing.Point(24, 64);
            this.lbl1_0.Name = "lbl1_0";
            this.lbl1_0.Size = new System.Drawing.Size(53, 16);
            this.lbl1_0.TabIndex = 17;
            this.lbl1_0.Text = "USER ID";
            this.lbl1_0.Visible = false;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(536, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(270, 20);
            this.Label3.TabIndex = 30;
            this.Label3.Text = "SELECT A MODULE TO EDIT PERMISSIONS";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddPayrollDeptDiv,
            this.mnuDeletePayrollDeptDiv,
            this.mnuPayrollSecurity,
            this.mnuSP1,
            this.mnuEditSignatures,
            this.mnuSepar3,
            this.mnuDeleteInfo,
            this.mnuSepar,
            this.mnuSaveInfo,
            this.mnuSaveExit,
            this.mnuFileSeparator1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
            // 
            // mnuAddPayrollDeptDiv
            // 
            this.mnuAddPayrollDeptDiv.Index = 0;
            this.mnuAddPayrollDeptDiv.Name = "mnuAddPayrollDeptDiv";
            this.mnuAddPayrollDeptDiv.Text = "Add Payroll Dept/Div";
            this.mnuAddPayrollDeptDiv.Click += new System.EventHandler(this.mnuAddPayrollDeptDiv_Click);
            // 
            // mnuDeletePayrollDeptDiv
            // 
            this.mnuDeletePayrollDeptDiv.Index = 1;
            this.mnuDeletePayrollDeptDiv.Name = "mnuDeletePayrollDeptDiv";
            this.mnuDeletePayrollDeptDiv.Text = "Delete Payroll Dept/Div";
            this.mnuDeletePayrollDeptDiv.Click += new System.EventHandler(this.mnuDeletePayrollDeptDiv_Click);
            // 
            // mnuPayrollSecurity
            // 
            this.mnuPayrollSecurity.Index = 2;
            this.mnuPayrollSecurity.Name = "mnuPayrollSecurity";
            this.mnuPayrollSecurity.Text = "Payroll Data Entry Security";
            this.mnuPayrollSecurity.Click += new System.EventHandler(this.mnuPayrollSecurity_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 3;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuEditSignatures
            // 
            this.mnuEditSignatures.Index = 4;
            this.mnuEditSignatures.Name = "mnuEditSignatures";
            this.mnuEditSignatures.Text = "Edit Signature Passwords";
            this.mnuEditSignatures.Visible = false;
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 5;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuDeleteInfo
            // 
            this.mnuDeleteInfo.Index = 6;
            this.mnuDeleteInfo.Name = "mnuDeleteInfo";
            this.mnuDeleteInfo.Text = "Delete Info";
            this.mnuDeleteInfo.Click += new System.EventHandler(this.mnuDeleteInfo_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 7;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuSaveInfo
            // 
            this.mnuSaveInfo.Index = 8;
            this.mnuSaveInfo.Name = "mnuSaveInfo";
            this.mnuSaveInfo.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSaveInfo.Text = "Save";
            this.mnuSaveInfo.Click += new System.EventHandler(this.mnuSaveInfo_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 9;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuFileSeparator1
            // 
            this.mnuFileSeparator1.Index = 10;
            this.mnuFileSeparator1.Name = "mnuFileSeparator1";
            this.mnuFileSeparator1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 11;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveInfo
            // 
            this.cmdSaveInfo.AppearanceKey = "acceptButton";
            this.cmdSaveInfo.Location = new System.Drawing.Point(508, 19);
            this.cmdSaveInfo.Name = "cmdSaveInfo";
            this.cmdSaveInfo.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveInfo.Size = new System.Drawing.Size(94, 48);
            this.cmdSaveInfo.TabIndex = 0;
            this.cmdSaveInfo.Text = "Save";
            this.cmdSaveInfo.Click += new System.EventHandler(this.mnuSaveInfo_Click);
            // 
            // cmdDeleteInfo
            // 
            this.cmdDeleteInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteInfo.Location = new System.Drawing.Point(984, 29);
            this.cmdDeleteInfo.Name = "cmdDeleteInfo";
            this.cmdDeleteInfo.Size = new System.Drawing.Size(90, 24);
            this.cmdDeleteInfo.TabIndex = 1;
            this.cmdDeleteInfo.Text = "Delete Info";
            this.cmdDeleteInfo.Click += new System.EventHandler(this.mnuDeleteInfo_Click);
            // 
            // cmdPayrollSecurity
            // 
            this.cmdPayrollSecurity.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPayrollSecurity.Location = new System.Drawing.Point(793, 29);
            this.cmdPayrollSecurity.Name = "cmdPayrollSecurity";
            this.cmdPayrollSecurity.Size = new System.Drawing.Size(185, 24);
            this.cmdPayrollSecurity.TabIndex = 2;
            this.cmdPayrollSecurity.Text = "Payroll Data Entry Security";
            this.cmdPayrollSecurity.Click += new System.EventHandler(this.mnuPayrollSecurity_Click);
            // 
            // cmdDeletePayrollDeptDiv
            // 
            this.cmdDeletePayrollDeptDiv.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeletePayrollDeptDiv.Location = new System.Drawing.Point(624, 29);
            this.cmdDeletePayrollDeptDiv.Name = "cmdDeletePayrollDeptDiv";
            this.cmdDeletePayrollDeptDiv.Size = new System.Drawing.Size(163, 24);
            this.cmdDeletePayrollDeptDiv.TabIndex = 3;
            this.cmdDeletePayrollDeptDiv.Text = "Delete Payroll Dept/Div";
            this.cmdDeletePayrollDeptDiv.Visible = false;
            this.cmdDeletePayrollDeptDiv.Click += new System.EventHandler(this.mnuDeletePayrollDeptDiv_Click);
            // 
            // cmdAddPayrollDeptDiv
            // 
            this.cmdAddPayrollDeptDiv.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddPayrollDeptDiv.Location = new System.Drawing.Point(456, 29);
            this.cmdAddPayrollDeptDiv.Name = "cmdAddPayrollDeptDiv";
            this.cmdAddPayrollDeptDiv.Size = new System.Drawing.Size(163, 24);
            this.cmdAddPayrollDeptDiv.TabIndex = 4;
            this.cmdAddPayrollDeptDiv.Text = "Add Payroll Dept/Div";
            this.cmdAddPayrollDeptDiv.Visible = false;
            this.cmdAddPayrollDeptDiv.Click += new System.EventHandler(this.mnuAddPayrollDeptDiv_Click);
            // 
            // frmSecurity
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1110, 878);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSecurity";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Security Setup";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmSecurity_Load);
            this.Activated += new System.EventHandler(this.frmSecurity_Activated);
            this.Resize += new System.EventHandler(this.frmSecurity_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSecurity_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSecurity_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSecurity)).EndInit();
            this.fraSecurity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayroll)).EndInit();
            this.fraPayroll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsPayroll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraInfo)).EndInit();
            this.fraInfo.ResumeLayout(false);
            this.fraInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowSiteUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUnlockUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPayrollSecurity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeletePayrollDeptDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPayrollDeptDiv)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveInfo;
		private FCButton cmdAddPayrollDeptDiv;
		private FCButton cmdDeletePayrollDeptDiv;
		private FCButton cmdPayrollSecurity;
		private FCButton cmdDeleteInfo;
		private FCButton btnUnlockUser;
		private FCCheckBox chkAllowSiteUpdate;
		public FCLabel lblUpdateSite;
	}
}
