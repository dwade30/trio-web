﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	public class modDateRoutines
	{
		//=========================================================
		// vbPorter upgrade warning: MyDate As object	OnWrite(object, DateTime)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string LongDate(object MyDate)
		{
			string LongDate = "";
			// If Len(MyDate) = 8 Then Exit Function
			LongDate = Strings.Format(MyDate, "MM/dd/yyyy");
			// LongDate = ConvertDateToHaveSlashes(LongDate)
			return LongDate;
		}
		// vbPorter upgrade warning: MyDate As Variant --> As string
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string StripDateSlashes(string MyDate)
		{
			string StripDateSlashes = "";
			// strips out the dash marks if the number came from a masked
			// edit box such as a sub-account number
			int i;
			string strTempNumber;
			strTempNumber = string.Empty;
			for (i = 0; i <= FCConvert.ToString(MyDate).Length; i++)
			{
				if (Strings.Left(MyDate, 1) != "/" && Strings.Left(MyDate, 1) != "_")
				{
					strTempNumber += Strings.Left(MyDate, 1);
				}
				if (!(MyDate == string.Empty))
				{
					MyDate = Strings.Mid(MyDate, 2, FCConvert.ToString(MyDate).Length - 1);
				}
			}
			// i
			// return the new number
			StripDateSlashes = Strings.Trim(strTempNumber);
			return StripDateSlashes;
		}
		// vbPorter upgrade warning: ControlName As object --> As string
		public static void DateMask(dynamic ControlName)
		{
			string TempDate;
			TempDate = Strings.Trim(StripDateSlashes(ControlName));
			if (TempDate != string.Empty)
			{
				if (TempDate.Length == 1)
				{
					if (FCConvert.ToDouble(TempDate) > 1)
					{
						ControlName = "0" + TempDate;
						ControlName.SelStart = 3;
					}
				}
				else if (TempDate.Length == 2)
				{
					if (FCConvert.ToDouble(Strings.Left(TempDate, 1)) == 1 && FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 2)
					{
						ControlName = Strings.Left(TempDate, 1);
						ControlName.SelStart = 1;
					}
				}
				else if (TempDate.Length == 3)
				{
					if (FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 3)
					{
						ControlName = Strings.Left(TempDate, 2) + "0" + Strings.Right(TempDate, 1);
						ControlName.SelStart = 6;
					}
				}
				else if (TempDate.Length == 4)
				{
					if (FCConvert.ToDouble(Strings.Mid(TempDate, 3, 1)) == 3 && FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 1)
					{
						ControlName = Strings.Left(TempDate, 3);
						ControlName.SelStart = 4;
					}
					// No need to check 5th digit
				}
				else if (TempDate.Length == 6)
				{
					if ((FCConvert.ToDouble(Strings.Right(TempDate, 2)) != 19) && (FCConvert.ToDouble(Strings.Right(TempDate, 2)) != 20))
					{
						if (FCConvert.ToDouble(Strings.Right(TempDate, 2)) > 60)
						{
							ControlName = Strings.Left(TempDate, 4) + "19" + Strings.Right(TempDate, 2);
						}
						else
						{
							ControlName = Strings.Left(TempDate, 4) + "20" + Strings.Right(TempDate, 2);
						}
					}
				}
			}
		}

		public static string ConvertDateToHaveSlashes(string DateValue)
		{
			string ConvertDateToHaveSlashes = "";
			string str_date;
			if (DateValue == string.Empty)
			{
				DateValue = CurrentDate();
			}
			str_date = Strings.Trim(DateValue);
			ConvertDateToHaveSlashes = Strings.Left(str_date, 2) + "/" + Strings.Mid(str_date, 3, 2) + "/" + Strings.Right(str_date, 4);
			return ConvertDateToHaveSlashes;
		}

		public static string ConvertDate(string DateValue)
		{
			string ConvertDate = "";
			string str_date;
			if (DateValue == string.Empty)
			{
				return ConvertDate;
			}
			str_date = Strings.Trim(DateValue);
			ConvertDate = Strings.Left(str_date, 2) + "/" + Strings.Mid(str_date, 3, 2) + "/" + Strings.Right(str_date, 4);
			return ConvertDate;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string MakeFullDate(string DateValue)
		{
			string MakeFullDate = "";
			MakeFullDate = Strings.Format(DateValue, "MM/dd/yyyy");
			return MakeFullDate;
		}

		public static string CurrentDate()
		{
			string CurrentDate = "";
			string TempDate;
			TempDate = FCConvert.ToString(Conversion.Val(Strings.Format(DateTime.Now, "MMddyyyy")));
			if (TempDate.Length == 7)
			{
				CurrentDate = 0 + TempDate;
			}
			else
			{
				CurrentDate = TempDate;
			}
			return CurrentDate;
		}
		// vbPorter upgrade warning: DateOne As Variant --> As string
		// vbPorter upgrade warning: DateTwo As Variant --> As string
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool CompareDates(string DateOne, string DateTwo, string Symbol)
		{
			bool CompareDates = false;
			// vbPorter upgrade warning: SubtractedValue As Variant --> As double	OnWrite(string)
			double SubtractedValue = 0;
			if (DateOne == string.Empty)
				return CompareDates;
			DateOne = Strings.Format(DateOne, "yyyyMMdd");
			DateTwo = Strings.Format(DateTwo, "yyyyMMdd");
			if (Symbol == ">")
			{
				SubtractedValue = FCConvert.ToDouble(DateOne) - FCConvert.ToDouble(DateTwo);
				CompareDates = (SubtractedValue > 0 ? true : false);
			}
			else if (Symbol == "<")
			{
				SubtractedValue = FCConvert.ToDouble(DateOne) - FCConvert.ToDouble(DateTwo);
				CompareDates = (SubtractedValue > 0 ? false : true);
			}
			else if (Symbol == "=")
			{
				SubtractedValue = FCConvert.ToDouble(DateOne) - FCConvert.ToDouble(DateTwo);
				CompareDates = (SubtractedValue == 0 ? true : false);
			}
			return CompareDates;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string GetDateWithoutSlashes(string DateValue)
		{
			string GetDateWithoutSlashes = "";
			string str_date;
			str_date = Strings.Trim(DateValue);
			GetDateWithoutSlashes = Strings.Left(str_date, 2) + Strings.Mid(str_date, 3, 2) + Strings.Right(str_date, 4);
			return GetDateWithoutSlashes;
		}

		public static bool InvalidMaskedDate(ref Wisej.Web.MaskedTextBox mskTbx, ref string strFieldName)
		{
			bool InvalidMaskedDate = false;
			try
			{
				// vbPorter upgrade warning: strDate As Variant --> As string
				string strDate;
				InvalidMaskedDate = false;
				// Anticipate no problems
				strDate = ConvertDateToHaveSlashes(mskTbx.Text);
				if (!(Information.IsDate(strDate)) || FCConvert.ToDateTime(strDate).Year < 1800 || FCConvert.ToDateTime(strDate).Year > 2200)
				{
					ErrorHandler:
					;
					InvalidMaskedDate = true;
					// Problem!
					MessageBox.Show("Invalid Entry for " + strFieldName, null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					mskTbx.Focus();
					mskTbx.SelectionStart = 0;
					mskTbx.SelectionLength = 10;
				}
			}
			catch
			{
			}
			return InvalidMaskedDate;
		}
		// Routine For Selecting a Text Box value for Change
		public static void Reselect(ref FCTextBox txtTbx)
		{
			txtTbx.SelectionStart = 0;
			txtTbx.SelectionLength = txtTbx.Text.Length;
		}
		// Common subroutine to complain and highlight the bad text.
		private static void CussAndReselect(ref FCTextBox txtTbx, ref string strTbxType)
		{
			MessageBox.Show("Invalid Entry for " + strTbxType + ": " + txtTbx.GetName(), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			txtTbx.SelectionStart = 0;
			txtTbx.SelectionLength = txtTbx.Text.Length;
		}
	}
}
