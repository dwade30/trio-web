﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPrintTestType.
	/// </summary>
	partial class frmPrintTestType : BaseForm
	{
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCComboBox cboType;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdSelect = new fecherFoundation.FCButton();
			this.cboType = new fecherFoundation.FCComboBox();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 234);
			this.BottomPanel.Size = new System.Drawing.Size(472, 80);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSelect);
			this.ClientArea.Controls.Add(this.cboType);
			this.ClientArea.Size = new System.Drawing.Size(472, 174);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(472, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(249, 30);
			this.HeaderText.Text = "Select Type of Printer";
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Location = new System.Drawing.Point(348, 30);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(98, 40);
			this.cmdSelect.TabIndex = 1;
			this.cmdSelect.Text = "Select";
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// cboType
			// 
			this.cboType.AutoSize = false;
			this.cboType.BackColor = System.Drawing.SystemColors.Window;
			this.cboType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboType.FormattingEnabled = true;
			this.cboType.Items.AddRange(new object[] {
				"Printer Object ",
				"ActiveReport (Preview)",
				"ActiveReport",
				"File System Object (FSO)",
				"Open File Object (Open #1)",
				"Vertical Alignment Page",
				"Printer Font",
                "Direct Printing"
			});
			this.cboType.Location = new System.Drawing.Point(30, 30);
			this.cboType.Name = "cboType";
			this.cboType.Size = new System.Drawing.Size(298, 40);
			this.cboType.TabIndex = 0;
			// 
			// frmPrintTestType
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(472, 314);
			this.Name = "frmPrintTestType";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Select Type of Printer object ";
			this.Load += new System.EventHandler(this.frmPrintTestType_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
