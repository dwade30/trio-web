﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using fecherFoundation.VisualBasicLayer;
using SharedApplication;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmChilkatUpload.
	/// </summary>
	public partial class frmChilkatUpload : BaseForm
	{
		public frmChilkatUpload()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.ChilkatFtp1 = new Chilkat.SFtp();
			ChilkatFtp1.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");

			this.ChilkatFtp1.EnableEvents = true;
			this.ChilkatFtp1.ConnectTimeoutMs = 15000;
			this.ChilkatFtp1.IdleTimeoutMs = 15000;

			this.ChilkatFtp1.OnPercentDone += sftp_OnPercentDone;

			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void sftp_OnPercentDone(object sender, Chilkat.PercentDoneEventArgs args)
		{
			ProgressBar1.Value = args.PercentDone;
			ProgressBar1.Refresh();
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChilkatUpload InstancePtr
		{
			get
			{
				return (frmChilkatUpload)Sys.GetInstance(typeof(frmChilkatUpload));
			}
		}

		protected frmChilkatUpload _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/24/03
		// This form will be used to perform automatic uploads to
		// our FTP site.  This can be used if we need to get a database
		// and the customer does not have PCAnywhere and does not have Email
		// ********************************************************
		string strFTPSiteUser = "";
		string strFTPSitePassword = "";
		int SelectCol;
		int FileCol;
		int SortCol;
		int DisplayCol;
		const int CNSTDBCol = 4;
		const int CNSTDBFile = 5;
        const int CNSTDBFullName = 6;
        const int CNSTArchiveCol = 7;
        const int CNSTDBGroup = 8;
        const int CNSTDBAcronym = 9;

		private List<int> selectedDatabaseRows = new List<int>();
		private cBackupRestore tBack = new cBackupRestore();
        private string uploadFileName = "";

		private void UploadFiles()
		{
			try
			{
				string strDirectory;
				bool success;
                

                if (!Directory.Exists("Upload"))
				{
					Directory.CreateDirectory("Upload").Attributes = FileAttributes.Normal;
				}

				if (File.Exists("Upload.zip"))
				{
					File.Delete("Upload.zip");
				}

				for (int x = 0; x <= (vsUpload.Rows - 1); x++)
				{
					if (vsUpload.TextMatrix(x, SelectCol).ToBoolean())
					{
						if (!vsUpload.TextMatrix(x, CNSTDBCol).ToBoolean())
						{
							File.Copy(vsUpload.TextMatrix(x, FileCol), "Upload\\" + vsUpload.TextMatrix(x, FileCol), true);
                        }

						this.Refresh();
					}
				}

                try
				{
                    fecherFoundation.ZipFile.CreateFromDirectory("Upload", "Upload.zip");
				}
				catch (Exception ex)
				{
					MessageBox.Show("Upload not Successful" + "\r\n" + ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}

				for (int x = 0; x <= (vsUpload.Rows - 1); x++)
				{
					if (vsUpload.TextMatrix(x, SelectCol).ToBoolean())
					{
						if (!vsUpload.TextMatrix(x, CNSTDBCol).ToBoolean())
						{
							File.Delete("Upload\\" + vsUpload.TextMatrix(x, FileCol));
						}
						else
						{
							File.Delete("Upload\\" + Path.GetFileName(vsUpload.TextMatrix(x, CNSTDBFile)));
						}
					}
				}

				success = ChilkatFtp1.Connect(StaticSettings.gGlobalSettings.SFTPSiteAddress, 22);
				if (success)
				{
					success = ChilkatFtp1.AuthenticatePw(strFTPSiteUser + "@trio", strFTPSitePassword);
					if (success)
					{
						success = ChilkatFtp1.InitializeSftp();
					}
				}

				if (!success)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show(ChilkatFtp1.LastErrorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				strDirectory = modGlobalConstants.Statics.MuniName + DateTime.Now.ToString("MMddyyyyhhmmss");
				success = ChilkatFtp1.CreateDir(strDirectory);
				if (success != true)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Unable to create directory." + "\r\n" + ChilkatFtp1.LastErrorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				string strLocalFile = "";
				if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
				{
					strLocalFile = FCFileSystem.Statics.UserDataFolder + "\\Upload.zip";
				}
				else
				{
					strLocalFile = FCFileSystem.Statics.UserDataFolder + "Upload.zip";
				}

				string handle = ChilkatFtp1.OpenFile(strDirectory + "\\" + uploadFileName, "writeOnly", "createTruncate");
				if (ChilkatFtp1.LastMethodSuccess != true)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Unable to connect to file." + "\r\n" + ChilkatFtp1.LastErrorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				ResetProgressBar();
				lstMessages.AddItem("Uploading");
				lstMessages.Refresh();
				
				// Upload from the local file to the SSH server.
				success = ChilkatFtp1.UploadFile(handle, strLocalFile);
				if (success != true)
				{
					MessageBox.Show(ChilkatFtp1.LastErrorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ChilkatFtp1.Disconnect();
					return;
				}

				// Close the file.
				success = ChilkatFtp1.CloseHandle(handle);
				if (success != true)
				{
					MessageBox.Show(ChilkatFtp1.LastErrorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ChilkatFtp1.Disconnect();
					return;
				}

				ResetProgressBar();

				ChilkatFtp1.Disconnect();

				MessageBox.Show("Upload Completed", "Upload Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In UploadFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				if (ChilkatFtp1.IsConnected)
				{
					ChilkatFtp1.Disconnect();
				}
			}
		}

		private void ResetProgressBar()
		{
			ProgressBar1.Value = 0;
			ProgressBar1.Maximum = 100;
			lblPercent.Text = "";
			lblPercent.Refresh();
		}

		private void ClearBackups()
		{
			selectedDatabaseRows = new List<int>();
		}

		private void ProcessUploads()
		{
			bool blnData;
			int x;
			bool boolDBs;
			blnData = false;
			boolDBs = false;
            bool nonDatabaseFilesIncluded = false;

            uploadFileName = "";

			ClearBackups();
			for (x = 0; x <= vsUpload.Rows - 1; x++)
			{
				if (vsUpload.TextMatrix(x, SelectCol).ToBoolean())
				{
					blnData = true;

					if (vsUpload.TextMatrix(x, CNSTDBCol).ToBoolean())
					{
						boolDBs = true;
						selectedDatabaseRows.Add(x);
                        uploadFileName += vsUpload.TextMatrix(x, CNSTDBAcronym) + "-";
                    }
                    else
                    {
                        nonDatabaseFilesIncluded = true;
                    }
				}
			}

            if (nonDatabaseFilesIncluded)
            {
                if (uploadFileName.Length > 247)
                {
                    uploadFileName = uploadFileName.Left(247);
                }
                uploadFileName = uploadFileName + "MISC.zip";
            }
            else
            {
                if (uploadFileName != "")
                {
                    if (uploadFileName.Length > 251)
                    {
                        uploadFileName = uploadFileName.Left(251);
                    }
					uploadFileName = uploadFileName.Left(uploadFileName.Length - 1) + ".zip";
                }
                else
                {
                    uploadFileName = "Upload.zip";
                }
            }

			if (blnData == false)
			{
				MessageBox.Show("You must select a file to upload before you may proceed", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			if (!Directory.Exists("Upload"))
			{
				Directory.CreateDirectory("Upload").Attributes = FileAttributes.Normal;
			}

			if (!boolDBs)
			{
				UploadFiles();
			}
			else
			{
				MakeBackups();
			}
		}

		private void cmdUpload_Click(object sender, System.EventArgs e)
		{
			lstMessages.Clear();
			ProcessUploads();
		}

		private void MakeBackups()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strLabel;
			string strDBName;
			string strPath;
			string strFile;
            string strConnection;

			tBack = new cBackupRestore();
			SettingsInfo tSet = new SettingsInfo();
            strPath = tSet.GlobalDataDirectory;
            if (strPath.Right(1) != @"\")
            {
                strPath = strPath + @"\";
            }

			foreach (var x in selectedDatabaseRows)
			{
				strLabel = vsUpload.TextMatrix(x, DisplayCol);
				strDBName = vsUpload.TextMatrix(x, FileCol);
				lstMessages.AddItem("Backing up " + strLabel);
				lstMessages.Refresh();

                if (vsUpload.TextMatrix(x, CNSTArchiveCol).ToBoolean())
                {
                    strDBName = vsUpload.TextMatrix(x, CNSTDBGroup) + "_" + strDBName;
					strConnection = vsUpload.TextMatrix(x, CNSTDBFullName);
				}
                else
                {
                    strConnection = rsLoad.Get_GetFullDBName(strDBName);
                }

				strFile = strPath + "\\Upload\\" + strDBName + "_Upload_" + Strings.Format(DateTime.Today, "MMddyyyy") + "_" + Strings.Format(DateTime.Now, "hhmmss") + ".bak";
				vsUpload.TextMatrix(x, CNSTDBFile, strFile);
                tBack.MakeBackup(strConnection, strFile, true);
            }

			UploadFiles();
		}

		private void frmChilkatUpload_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
            string strGroupName;
            string strDBName;
            string strFullDBName;

			rsDefaults.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
			if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
			{
				strFTPSiteUser = FCConvert.ToString(rsDefaults.Get_Fields_String("FTPSiteUser"));
				strFTPSitePassword = FCConvert.ToString(rsDefaults.Get_Fields_String("FTPSitePassword"));
			}
			lstMessages.Clear();

			SelectCol = 0;
			FileCol = 1;
			SortCol = 2;
			DisplayCol = 3;
			vsUpload.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsUpload.ColHidden(FileCol, true);
			vsUpload.ColHidden(SortCol, true);
			vsUpload.ColHidden(CNSTDBCol, true);
			vsUpload.ColHidden(CNSTDBFile, true);
			vsUpload.ColWidth(SelectCol, FCConvert.ToInt32(vsUpload.WidthOriginal * 0.15));
			vsUpload.ColDataType(CNSTDBCol, FCGrid.DataTypeSettings.flexDTBoolean);
            vsUpload.ColDataType(CNSTArchiveCol, FCGrid.DataTypeSettings.flexDTBoolean);

            vsUpload.ColHidden(CNSTArchiveCol, true);
            vsUpload.ColHidden(CNSTDBFullName, true);
            vsUpload.ColHidden(CNSTDBGroup, true);
            vsUpload.ColHidden(CNSTDBAcronym, true);

			string[] tVar;
			tVar = rsDefaults.AvailableDBs();

			for (int counter = 0; counter < tVar.Length; counter++)
			{
				vsUpload.Rows += 1;
				vsUpload.TextMatrix(vsUpload.Rows - 1, FileCol, tVar[counter]);
				vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBFile, "");
				if (Strings.LCase(Strings.Right("     " + rsDefaults.GetDBDescription(tVar[counter]), 4)) != "data")
				{
					vsUpload.TextMatrix(vsUpload.Rows - 1, DisplayCol, rsDefaults.GetDBDescription(tVar[counter]) + " Data");
				}
				else
				{
					vsUpload.TextMatrix(vsUpload.Rows - 1, DisplayCol, rsDefaults.GetDBDescription(tVar[counter]));
				}
				vsUpload.TextMatrix(vsUpload.Rows - 1, SelectCol, FCConvert.ToString(false));
				vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBCol, FCConvert.ToString(true));
				vsUpload.TextMatrix(vsUpload.Rows - 1, SortCol, FCConvert.ToString(1));
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTArchiveCol, false);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBAcronym, GetDBAcronym(tVar[counter]));
            }

            rsDefaults.OpenRecordset("Select * from archives order by archivetype,archiveid", "SystemSettings");

			while (!rsDefaults.EndOfFile())
			{
				strGroupName = rsDefaults.Get_Fields_String("archivetype") + "_" + rsDefaults.Get_Fields_String("archiveid");
				if (rsDefaults.Get_Fields_String("archivetype").ToLower() == "archive")
				{
					strDBName = "Budgetary";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Budgetary Archive", strFullDBName,
                        strGroupName, strDBName, "BD" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));

					strDBName = "CentralData";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
					AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Budgetary Archive Central Data", strFullDBName,
						strGroupName, strDBName, "CD" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));

					strDBName = "CentralDocuments";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Budgetary Archive Central Documents",
						strFullDBName, strGroupName, strDBName, "CDOC" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));
				}
				else if (rsDefaults.Get_Fields_String("archivetype").ToLower() == "commitmentarchive")
				{
					strDBName = "RealEstate";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Real Estate Tax Archive", strFullDBName,
						strGroupName, strDBName, "RE" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));


					strDBName = "PersonalProperty";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Personal Property Tax Archive", strFullDBName,
						strGroupName, strDBName, "PP" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));


					strDBName = "CentralData";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Central Data Tax Archive", strFullDBName,
						strGroupName, strDBName, "CD" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));


					strDBName = "CentralDocuments";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Central Documents Tax Archive", strFullDBName,
						strGroupName, strDBName, "CDOC" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));


					strDBName = "CentralParties";
					strFullDBName = rsDefaults.Get_GetFullDBName(strDBName);
					strFullDBName = strFullDBName.Replace("Live", strGroupName);
                    AddArchive(rsDefaults.Get_Fields_String("archiveid") + " Central Parties Tax Archive", strFullDBName,
						strGroupName, strDBName, "CP" + rsDefaults.Get_Fields_String("archiveid").ToString().Right(2));
				}

                rsDefaults.MoveNext();
			}

			filUpload.Path = FCFileSystem.Statics.UserDataFolder;
			for (int counter = 0; counter <= filUpload.Items.Count - 1; counter++)
			{
				vsUpload.Rows += 1;
				vsUpload.TextMatrix(vsUpload.Rows - 1, FileCol, filUpload.Items[counter].ToString());
				vsUpload.TextMatrix(vsUpload.Rows - 1, DisplayCol, filUpload.Items[counter].ToString());
				vsUpload.TextMatrix(vsUpload.Rows - 1, SelectCol, FCConvert.ToString(false));
				vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBCol, FCConvert.ToString(false));
				vsUpload.TextMatrix(vsUpload.Rows - 1, SortCol, FCConvert.ToString(1));
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTArchiveCol, false);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBAcronym, "");

			}

			vsUpload.Select(0, DisplayCol);
		}

        private void AddArchive(string description, string fullDBName, string group, string shortDBName, string acronym)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();

            if (rsTemp.DatabaseByFullNameExists(fullDBName))
            {
                vsUpload.Rows = vsUpload.Rows + 1;

                vsUpload.TextMatrix(vsUpload.Rows - 1, FileCol, shortDBName);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBFile, "");
                vsUpload.TextMatrix(vsUpload.Rows - 1, DisplayCol, description);
                vsUpload.TextMatrix(vsUpload.Rows - 1, SelectCol, false);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBCol, true);
                vsUpload.TextMatrix(vsUpload.Rows - 1, SortCol, 1);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTArchiveCol, true);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBFullName, fullDBName);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBGroup, group);
                vsUpload.TextMatrix(vsUpload.Rows - 1, CNSTDBAcronym, acronym);
			}
		}

		public string GetDBAcronym(string strDBName)
		{
			var dbName = Strings.LCase(strDBName);

			switch (dbName)
			{
				case "accountsreceivable":
					return "AR";
				case "cashreceipts":
                    return "CR";
				case "centraldata":
                    return "CD";
				case "centralparties":
                    return "CP";
				case "codeenforcement":
                    return "CE";
				case "recommit":
                    return "RC";
				case "fixedassets":
                    return "FA";
				case "motorvehicle":
                    return "MV";
				case "personalproperty":
                    return "PP";
				case "realestate":
                    return "RE";
				case "realestatetransfer":
                    return "XF";
				case "bluebook":
                    return "BB";
				case "systemsettings":
                    return "SS";
				case "taxbilling":
                    return "TB";
				case "utilitybilling":
                    return "UB";
				case "centraldocuments":
                    return "CDOC";
				case "clientsettings":
                    return "CS";
                case "clerk":
                    return "CK";
                case "budgetary":
                    return "BD";
                case "collections":
                    return "CL";
                case "payroll":
                    return "PY";
				default:
                    return "";
			}
		}

		private void frmChilkatUpload_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            switch (KeyAscii)
            {
                // catches the escape and enter keys
                case Keys.Escape:
                    KeyAscii = (Keys)0;
                    this.Unload();

                    break;
                case Keys.Return:
                    KeyAscii = (Keys)0;
                    Support.SendKeys("{TAB}", false);

                    break;
            }
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (ChilkatFtp1.IsConnected)
				ChilkatFtp1.Disconnect();
		}

		private void frmChilkatUpload_Resize(object sender, System.EventArgs e)
		{
			vsUpload.ColWidth(SelectCol, FCConvert.ToInt32(vsUpload.WidthOriginal * 0.15));
		}
		
		private void vsUpload_ClickEvent(object sender, System.EventArgs e)
		{
			try
            {
                vsUpload.TextMatrix(vsUpload.Row, SelectCol, vsUpload.TextMatrix(vsUpload.Row, SelectCol).ToBoolean() 
                                        ? FCConvert.ToString(false) 
                                        : FCConvert.ToString(true));
            }
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsUpload_Click line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsUpload_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			try
			{
                if (e.KeyChar != 32) return;

                e.KeyChar = (char)0;
                vsUpload.TextMatrix(vsUpload.Row, SelectCol, vsUpload.TextMatrix(vsUpload.Row, SelectCol).ToBoolean() 
                                        ? FCConvert.ToString(false) 
                                        : FCConvert.ToString(true));
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsUpload_KeyPress line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
