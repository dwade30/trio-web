﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD4.
	/// </summary>
	public partial class srptBD4 : FCSectionReport
	{
		public srptBD4()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD4";
		}

		public static srptBD4 InstancePtr
		{
			get
			{
				return (srptBD4)Sys.GetInstance(typeof(srptBD4));
			}
		}

		protected srptBD4 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD4	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		string strType = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (Conversion.Val(modBudgetaryAccounting.Statics.strExpCtrAccount) != 0)
				{
					eArgs.EOF = false;
				}
				else
				{
					if (Conversion.Val(modBudgetaryAccounting.Statics.strRevCtrAccount) != 0)
					{
						strType = "R";
						eArgs.EOF = false;
					}
					else if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) != 0)
					{
						strType = "C";
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile())
				{
					if (strType == "E")
					{
						if (Conversion.Val(modBudgetaryAccounting.Statics.strRevCtrAccount) != 0)
						{
							strType = "R";
							rsInfo.MoveFirst();
							eArgs.EOF = false;
						}
						else if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) != 0)
						{
							strType = "C";
							rsInfo.MoveFirst();
							eArgs.EOF = false;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
					else if (strType == "R")
					{
						if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) != 0)
						{
							strType = "C";
							rsInfo.MoveFirst();
							eArgs.EOF = false;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			if (eArgs.EOF == false)
			{
				this.Fields["Binder"].Value = strType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles ORDER BY Fund", "TWBD0000.vb1");
			blnFirstRecord = true;
			strType = "E";
			if (Conversion.Val(modBudgetaryAccounting.Statics.strExpCtrAccount) == 0 && Conversion.Val(modBudgetaryAccounting.Statics.strRevCtrAccount) == 0 && Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) == 0)
			{
				// Me.Cancel
				//this.Stop();
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			GetFundInfo_2(rsInfo.Get_Fields("Fund"));
		}

		private void GetFundInfo_2(string strFundToCheck)
		{
			GetFundInfo(ref strFundToCheck);
		}

		private void GetFundInfo(ref string strFundToCheck)
		{
			clsDRWrapper rsActual = new clsDRWrapper();
			clsDRWrapper rsControl = new clsDRWrapper();
			// vbPorter upgrade warning: curActual As Decimal	OnWrite(double, short, Decimal)
			Decimal curActual = 0;
			// vbPorter upgrade warning: curControl As Decimal	OnWrite(double, short, Decimal)
			Decimal curControl = 0;
			fldFund.Visible = true;
			fldActual.Visible = true;
			fldControl.Visible = true;
			chkNoMatch.Visible = true;
			if (strType == "E")
			{
				rsActual.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM ExpenseReportInfo WHERE Department IN (SELECT Distinct Department FROM DeptDivTitles WHERE Fund = '" + strFundToCheck + "')", "TWBD0000.vb1");
				if (rsActual.EndOfFile() != true && rsActual.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curActual = FCConvert.ToDecimal(Conversion.Val(rsActual.Get_Fields("OriginalBudgetTotal")) - Conversion.Val(rsActual.Get_Fields("BudgetAdjustmentsTotal")) - Conversion.Val(rsActual.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsActual.Get_Fields("PostedCreditsTotal")) - Conversion.Val(rsActual.Get_Fields("EncumbActivityTotal")));
				}
				else
				{
					curActual = 0;
				}
				if (modAccountTitle.Statics.YearFlag)
				{
					rsControl.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = 'G " + strFundToCheck + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "'", "TWBD0000.vb1");
				}
				else
				{
					rsControl.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = 'G " + strFundToCheck + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "-00'", "TWBD0000.vb1");
				}
				if (rsControl.EndOfFile() != true && rsControl.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curControl = FCConvert.ToDecimal((Conversion.Val(rsControl.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsControl.Get_Fields("PostedCreditsTotal")) + Conversion.Val(rsControl.Get_Fields("EncumbActivityTotal"))) * -1);
				}
				else
				{
					curControl = 0;
				}
			}
			else if (strType == "R")
			{
				rsActual.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Department IN (SELECT Distinct Department FROM DeptDivTitles WHERE Fund = '" + strFundToCheck + "')", "TWBD0000.vb1");
				if (rsActual.EndOfFile() != true && rsActual.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curActual = FCConvert.ToDecimal(Conversion.Val(rsActual.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsActual.Get_Fields("BudgetAdjustmentsTotal")) + Conversion.Val(rsActual.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsActual.Get_Fields("PostedCreditsTotal")) + Conversion.Val(rsActual.Get_Fields("EncumbActivityTotal")));
				}
				else
				{
					curActual = 0;
				}
				curActual *= -1;
				if (modAccountTitle.Statics.YearFlag)
				{
					rsControl.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = 'G " + strFundToCheck + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "'", "TWBD0000.vb1");
				}
				else
				{
					rsControl.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = 'G " + strFundToCheck + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "-00'", "TWBD0000.vb1");
				}
				if (rsControl.EndOfFile() != true && rsControl.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					curControl = FCConvert.ToDecimal(Conversion.Val(rsControl.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsControl.Get_Fields("PostedCreditsTotal")));
				}
				else
				{
					curControl = 0;
				}
				curControl *= -1;
			}
			else if (strType == "C")
			{
				rsActual.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Department IN (SELECT Distinct Department FROM DeptDivTitles WHERE Fund = '" + strFundToCheck + "')", "TWBD0000.vb1");
				if (rsActual.EndOfFile() != true && rsActual.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curActual = FCConvert.ToDecimal(Conversion.Val(rsActual.Get_Fields("EncumbActivityTotal")));
				}
				else
				{
					curActual = 0;
				}
				rsActual.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM ExpenseReportInfo WHERE Department IN (SELECT Distinct Department FROM DeptDivTitles WHERE Fund = '" + strFundToCheck + "')", "TWBD0000.vb1");
				if (rsActual.EndOfFile() != true && rsActual.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curActual += FCConvert.ToDecimal(Conversion.Val(rsActual.Get_Fields("EncumbActivityTotal")));
				}
				rsActual.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "'", "TWBD0000.vb1");
				if (rsActual.EndOfFile() != true && rsActual.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curActual += FCConvert.ToDecimal(Conversion.Val(rsActual.Get_Fields("EncumbActivityTotal")));
				}
				// DJW@20090501 Take out G account amounts that happen in AP journal because it throws off internal audit
				// Tracker Reference: 16445
				rsActual.OpenRecordset("SELECT SUM(Encumbrance) as EncumbActivityTotal FROM LedgerDetailInfo WHERE Fund = '" + strFundToCheck + "'", "TWBD0000.vb1");
				if (rsActual.EndOfFile() != true && rsActual.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curActual += FCConvert.ToDecimal(Conversion.Val(rsActual.Get_Fields("EncumbActivityTotal")));
				}
				if (modAccountTitle.Statics.YearFlag)
				{
					rsControl.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = 'G " + strFundToCheck + "-" + modBudgetaryAccounting.Statics.strEncOffAccount + "'", "TWBD0000.vb1");
				}
				else
				{
					rsControl.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = 'G " + strFundToCheck + "-" + modBudgetaryAccounting.Statics.strEncOffAccount + "-00'", "TWBD0000.vb1");
				}
				if (rsControl.EndOfFile() != true && rsControl.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					curControl = FCConvert.ToDecimal(Conversion.Val(rsControl.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsControl.Get_Fields("PostedCreditsTotal")) + Conversion.Val(rsControl.Get_Fields("EncumbActivityTotal")));
				}
				else
				{
					curControl = 0;
				}
				curControl *= -1;
			}
			fldFund.Text = strFundToCheck;
			fldActual.Text = Strings.Format(curActual, "#,##0.00");
			fldControl.Text = Strings.Format(curControl, "#,##0.00");
			if (curActual == curControl)
			{
				chkNoMatch.Checked = false;
			}
			else
			{
				chkNoMatch.Checked = true;
			}
			if (curActual == 0 && curControl == 0)
			{
				fldFund.Visible = false;
				fldActual.Visible = false;
				fldControl.Visible = false;
				chkNoMatch.Visible = false;
			}
			rsActual.Dispose();
			rsControl.Dispose();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (FCConvert.ToString(this.Fields["Binder"].Value) == "E")
			{
				lblType.Text = "Expense";
			}
			else if (FCConvert.ToString(this.Fields["Binder"].Value) == "R")
			{
				lblType.Text = "Revenue";
			}
			else
			{
				lblType.Text = "Encumbrance";
			}
		}

	

		private void srptBD4_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
