﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmTellerSetup.
	/// </summary>
	public partial class frmTellerSetup : BaseForm
	{
		public frmTellerSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTellerSetup InstancePtr
		{
			get
			{
				return (frmTellerSetup)Sys.GetInstance(typeof(frmTellerSetup));
			}
		}

		protected frmTellerSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// Last Updated:                      04/05/2002
		clsDRWrapper rsTeller_AutoInitialized;

		private clsDRWrapper rsTeller
		{
			get
			{
				if (rsTeller_AutoInitialized == null)
				{
					rsTeller_AutoInitialized = new clsDRWrapper();
				}
				return rsTeller_AutoInitialized;
			}
			set
			{
				rsTeller_AutoInitialized = value;
			}
		}

		clsDRWrapper rsAllTellers_AutoInitialized;

		private clsDRWrapper rsAllTellers
		{
			get
			{
				if (rsAllTellers_AutoInitialized == null)
				{
					rsAllTellers_AutoInitialized = new clsDRWrapper();
				}
				return rsAllTellers_AutoInitialized;
			}
			set
			{
				rsAllTellers_AutoInitialized = value;
			}
		}

		bool boolDirty;
		bool boolSetupMV;
		bool boolSetupCR;
		int intLevel;
		bool boolBudgetary;
		// vbPorter upgrade warning: intDeptLength As short --> As int	OnWrite(string, short)
		int intDeptLength;
		// vbPorter upgrade warning: intDivLength As short --> As int	OnWrite(string, short)
		int intDivLength;

		private void cmbTellerID_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ShowTeller(cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString());
		}

		private void cmbTellerID_DropDown(object sender, System.EventArgs e)
		{
			// This will expand the dropdown portion of the combobox
			// Call SendMessageByNum(Me.cmbTellerID.hwnd, CB_SETDROPPEDWIDTH, 200, 0)
		}

		private void cmbTellerID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						//if (modAPIsConst.SendMessageByNumWrp(cmbTellerID.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, string.Empty)==0) {
						//	modAPIsConst.SendMessageByNumWrp(cmbTellerID.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, string.Empty);
						//	KeyCode = (Keys)0;
						//}
						cmbTellerID.DroppedDown = true;
						break;
					}
			}
			//end switch
		}

		private void cmdAddNew_Click(object sender, System.EventArgs e)
		{
			// this will validate the teller ID that was typed in, check to see if it is already in use
			// and if everything is ok, then it will add it to the recordset and the combo box
			string strTID;
			strTID = Strings.Trim(txtNew.Text);
			if (strTID.Length != 3 || Strings.InStr(1, strTID, " ", CompareConstants.vbBinaryCompare) != 0)
			{
				MessageBox.Show("A Teller ID must have three characters.  Please enter a valid Teller ID.", "Invalid Teller ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtNew.Focus();
				return;
			}
			rsTeller.FindFirstRecord("Code", Strings.UCase(strTID));
			if (rsTeller.NoMatch)
			{
				// add the tellerID to the recordset
				rsTeller.AddNew();
				rsTeller.Set_Fields("Code", Strings.UCase(strTID));
				rsTeller.Set_Fields("securityid", 0);
				rsTeller.Update();
				// add the TellerID to the combo box
				cmbTellerID.AddItem(Strings.UCase(strTID));
				// show the teller info by selecting the teller in the combo box
				cmbTellerID.SelectedIndex = cmbTellerID.NewIndex;
				// switch back to the main options frame
				ShowFrame(fraMainOptions);
				ShowFrame(fraGRID);
				fraNew.Visible = false;
			}
			else
			{
				MessageBox.Show("This Teller ID is in use already.  Please use another Teller ID.", "Invalid Teller ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdCancelNew_Click(object sender, System.EventArgs e)
		{
			ShowFrame(fraMainOptions);
			ShowFrame(fraGRID);
			fraNew.Visible = false;
		}

		private void cmdMV_Click(object sender, System.EventArgs e)
		{
			//! Load frmTieMVtoUser;
			frmTieMVtoUser.InstancePtr.Show(FormShowEnum.Modeless);
		}

		private void frmTellerSetup_Activated(object sender, System.EventArgs e)
		{
			// load the table into the recordset
			// rsTeller.OpenRecordset "SELECT * FROM Operators", "systemsettings"
			// Call rsAllTellers.OpenRecordset("select * from operators", "systemsettings")
			// SetupModules
			// FillTellerCombo
			// FormatGrid
			// FillGrid
		}

		private void frmTellerSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						this.Unload();
						break;
					}
			}
			//end switch
		}

		private void frmTellerSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTellerSetup properties;
			//frmTellerSetup.ScaleWidth	= 9045;
			//frmTellerSetup.ScaleHeight	= 7470;
			//frmTellerSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			boolDirty = false;
			SetFieldLengths();
			rsTeller.OpenRecordset("SELECT * FROM Operators", "systemsettings");
			rsAllTellers.OpenRecordset("select * from operators", "systemsettings");
			SetupModules();
			FillTellerCombo();
			FormatGrid();
			FillGrid();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intChoice As short, int --> As DialogResult
			DialogResult intChoice;
			if (boolDirty)
			{
				intChoice = MessageBox.Show("Would you like to save your changes to Teller ID: " + cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString() + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intChoice)
				{
					case DialogResult.Yes:
						{
							// save the teller and exit
							SaveTeller_2(cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString());
							break;
						}
					case DialogResult.No:
						{
							// do nothing
							break;
						}
					case DialogResult.Cancel:
						{
							// do not save, but cancel the exit
							e.Cancel = true;
							// this will stop the unload of the form
							break;
						}
				}
				//end switch
			}
		}

		private void frmTellerSetup_Resize(object sender, System.EventArgs e)
		{
			//GRID.Height = GRID.RowHeight(0)*GRID.Rows+70;
			ResizeGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = GRID.WidthOriginal;
			GRID.ColWidth(1, FCConvert.ToInt32(0.33 * GridWidth));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// rsAllTellers.DisconnectDatabase
			// rsTeller.DisconnectDatabase
			//MDIParent.InstancePtr.Show();
		}

		private void GRID_ChangeEdit(object sender, System.EventArgs e)
		{
			if (GRID.IsCurrentCellInEditMode)
			{
				bool boolIsALevelOne = false;
				if (cmbTellerID.SelectedIndex != -1)
				{
					boolDirty = true;
					boolIsALevelOne = false;
					if (GRID.Col == 2 && GRID.Row == 1 && Conversion.Val(GRID.EditText) == 1)
					{
						if (!(rsAllTellers.EndOfFile() && rsAllTellers.BeginningOfFile()))
						{
							// rsAllTellers.Refresh
							rsAllTellers.MoveLast();
							rsAllTellers.MoveFirst();
							while (!rsAllTellers.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								if (!(FCConvert.ToString(rsAllTellers.Get_Fields("code")) == cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString()))
								{
									if (Conversion.Val(rsAllTellers.Get_Fields_String("level")) == 1)
									{
										boolIsALevelOne = true;
										break;
									}
								}
								rsAllTellers.MoveNext();
							}
							if (boolIsALevelOne)
							{
								// can't have more than one level one
								// GRID.EditText = intLevel
								// GRID.TextMatrix(1, 2) = intLevel
								GRID.ComboIndex = intLevel - 1;
								MessageBox.Show("There can only be one level one operator.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							else
							{
								intLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(GRID.EditText)));
							}
						}
						FillLevelDesc();
					}
					else if (GRID.Row == 1 && GRID.Col == 2)
					{
						intLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(GRID.EditText)));
						FillLevelDesc();
					}
					// FillLevelDesc
				}
			}
		}

		private void GRID_Enter(object sender, System.EventArgs e)
		{
			GRID.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			GRID.Select(1, 2);
		}

		private void GRID_RowColChange(object sender, System.EventArgs e)
		{
			if (GRID.Col == 2)
			{
				// the answer column
				GRID.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				switch (GRID.Row)
				{
					case 1:
						{
							GRID.ComboList = "1 - Primary Agent|2 - Alternate|3 - View Only|4 - Regular";
							break;
						}
					case 2:
					case 3:
						{
							GRID.ComboList = "0|1|2|3|4|5|6|7|8|9|10|11|12";
							break;
						}
					case 4:
						{
							GRID.ComboList = "0|1|2|3|4|5|6|7|8|9|10|11|12";
							GRID.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							// this will let the user tab out of the grid
							break;
						}
				}
				//end switch
			}
			else
			{
				GRID.Col = 2;
				// the user should only be in the answer column
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FormatGrid()
		{
			// this will set the grid up to fit correctly
			int wid = 0;
			wid = GRID.WidthOriginal;
			GRID.Rows = 5;
			GRID.ColWidth(0, FCConvert.ToInt32(wid * 0.05));
			// .ColWidth(1) = wid * 0.6
			// .ColWidth(2) = wid * 0.3
			// .RowOutlineLevel(0) = 0
			// .IsSubtotal(0) = True
			// 
			// .RowOutlineLevel(1) = 1
			// .RowOutlineLevel(2) = 1
			// .RowOutlineLevel(3) = 1
			// .RowOutlineLevel(4) = 1
			GRID.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
		}

		private void FillGrid()
		{
			// this will add the questions to the grid
			//FC:FINAL:SBE - #i1578 - simulate column header merge
			//GRID.TextMatrix(0, 1, "Motor Vehicle");
			GRID.TextMatrix(0, 2, "Motor Vehicle");
			GRID.TextMatrix(1, 1, "Agent Level");
			// 1-4
			GRID.TextMatrix(2, 1, "Plate Group");
			// 1-12
			GRID.TextMatrix(3, 1, "Sticker Group");
			// 1-12
			GRID.TextMatrix(4, 1, "MVR3 Group");
			// 1-12
			GRID.Row = 1;
			GRID.Col = 2;
			GRID.ComboList = "1 - Primary Agent|2 - Alternate|3 - View Only|4 - Regular";
			GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
			//FC:FINAL:SBE - #i1578 - simulate column header merge
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.MergeRow(0, true);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, GRID.Rows - 1, 1, ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND));
			//GRID.Height = GRID.RowHeight(0)*GRID.Rows+70;
		}

		private void FillTellerCombo()
		{
			// this will open the Operator table in GN and show all of the Operators that are set up
			cmbTellerID.Clear();
			if (rsTeller.EndOfFile() != true && rsTeller.BeginningOfFile() != true)
			{
				rsTeller.MoveFirst();
				while (!rsTeller.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbTellerID.AddItem(Strings.UCase(FCConvert.ToString(rsTeller.Get_Fields("Code"))));
					rsTeller.MoveNext();
				}
			}
		}

		private void ShowTeller(string strTellerID)
		{
			// this will show the information from the Operator Table
			if (Strings.Trim(strTellerID) != "")
			{
				rsTeller.FindFirstRecord("Code", strTellerID);
				txtName.Text = FCConvert.ToString(rsTeller.Get_Fields_String("Name"));
				txtPassword.Text = modEncrypt.ToggleEncryptCode(rsTeller.Get_Fields_String("Password"));
				// TODO Get_Fields: Check the table for the column [DeptDiv] and replace with corresponding Get_Field method
				txtDeptDiv.Text = FCConvert.ToString(rsTeller.Get_Fields("DeptDiv"));
				// kk trocr-348 11262012  Add CR Closeout by Dept
				if (Conversion.Val(rsTeller.Get_Fields_String("Level")) == 0)
				{
					// GRID.TextMatrix(1, 2) = 4
					GRID.Row = 1;
					GRID.Col = 2;
					GRID.ComboIndex = 3;
					intLevel = 4;
				}
				else
				{
					// GRID.TextMatrix(1, 2) = rsTeller.Fields("Level")
					GRID.Row = 1;
					GRID.Col = 2;
					intLevel = FCConvert.ToInt32(rsTeller.Get_Fields_String("Level"));
					// save this so we can put it back during a validate
					GRID.ComboIndex = intLevel - 1;
				}
				GRID.TextMatrix(2, 2, FCConvert.ToString(Conversion.Val(rsTeller.Get_Fields_Int16("PlateGroup"))));
				GRID.TextMatrix(3, 2, FCConvert.ToString(Conversion.Val(rsTeller.Get_Fields_Int16("StickerGroup"))));
				GRID.TextMatrix(4, 2, FCConvert.ToString(Conversion.Val(rsTeller.Get_Fields_Int16("MVR3Group"))));
			}
			else
			{
				txtName.Text = "";
				txtPassword.Text = "";
				// GRID.TextMatrix(1, 2) = 4
				GRID.Row = 1;
				GRID.Col = 2;
				GRID.ComboIndex = 3;
				GRID.TextMatrix(2, 2, FCConvert.ToString(0));
				GRID.TextMatrix(3, 2, FCConvert.ToString(0));
				GRID.TextMatrix(4, 2, FCConvert.ToString(0));
			}
			FillLevelDesc();
		}

		private void SaveTeller_2(string strTellerID)
		{
			SaveTeller(ref strTellerID);
		}

		private void SaveTeller(ref string strTellerID)
		{
			// this sub assumes that all of the validation is done on the data
			if (Strings.Trim(strTellerID) != "" && strTellerID.Length == 3)
			{
				rsTeller.FindFirstRecord("Code", strTellerID);
				if (rsTeller.NoMatch)
				{
					MessageBox.Show("There was an error finding the TellerID " + strTellerID + ".", "Save Teller", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					if (Strings.Trim(txtName.Text) == string.Empty)
					{
						MessageBox.Show("You must enter a name to save.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsTeller.Edit();
					// True
					rsTeller.Set_Fields("Name", Strings.Trim(txtName.Text));
					rsTeller.Set_Fields("Password", modEncrypt.ToggleEncryptCode(Strings.Trim(txtPassword.Text)));
					rsTeller.Set_Fields("DeptDiv", txtDeptDiv.Text);
					// kk trocr-348 11262012  Add CR Closeout by Dept
					rsTeller.Set_Fields("Level", FCConvert.ToString(Conversion.Val(GRID.TextMatrix(1, 2))));
					rsTeller.Set_Fields("PlateGroup", GRID.TextMatrix(2, 2));
					rsTeller.Set_Fields("StickerGroup", GRID.TextMatrix(3, 2));
					rsTeller.Set_Fields("MVR3Group", GRID.TextMatrix(4, 2));
					rsTeller.Update();
					boolDirty = false;
				}
			}
			else
			{
				MessageBox.Show("Please choose a valid teller to save.", "Save Teller", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void ShowFrame(Control fraFrame)
		{
			// this will place the frame in the middle of the form
			//FC:FINAL:RPU:#1259 - These were setted in designer
			//fraFrame.Left = FCConvert.ToInt32((this.Width-fraFrame.Width) / 2.0);
			string vbPorterVar = fraFrame.GetName();
			if (vbPorterVar == "fraMainOptions")
			{
				//FC:FINAL:RPU:#1259 - These were setted in designer
				//	fraFrame.Top = 540;
				fraFrame.Visible = true;
			}
			else if (vbPorterVar == "fraNew")
			{
				//FC:FINAL:RPU:#1259 - These were setted in designer
				//	fraFrame.Top = FCConvert.ToInt32((this.Height-fraFrame.Height) / 3.0);
				fraFrame.Visible = true;
			}
			else if (vbPorterVar == "fraGRID")
			{
				//FC:FINAL:RPU:#1259 - These were setted in designer
				//	fraFrame.Top = fraMainOptions.Top+fraMainOptions.Height+200;
				if (boolSetupMV)
				{
					fraFrame.Visible = true;
					cmdMV.Visible = true;
				}
			}
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDel = new clsDRWrapper();
			string strTID;
			int intTemp;
			strTID = Strings.Trim(cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString());
			if (MessageBox.Show("Are you sure that you would like to delete Teller ID: " + strTID, "Delete Teller ID", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				// yes delete the code
				if (rsDel.Execute("DELETE FROM Operators WHERE Code = '" + strTID + "'", "SystemSettings"))
				{
					MessageBox.Show("Teller ID: " + strTID + " has been deleted.", "Deletion Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// delete the user from the combo box
					for (intTemp = 0; intTemp <= cmbTellerID.Items.Count - 1; intTemp++)
					{
						if (strTID == Strings.Trim(cmbTellerID.Items[intTemp].ToString()))
						{
							cmbTellerID.Items.RemoveAt(intTemp);
							cmbTellerID.SelectedIndex = cmbTellerID.Items.Count - 1;
							break;
						}
					}
				}
				else
				{
					// if there is an error then
					MessageBox.Show("Teller ID: " + cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString() + " has not been deleted.", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				// no do not delete the code
				// do nothing
			}
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			fraMainOptions.Visible = false;
			fraGRID.Visible = false;
			cmdMV.Visible = false;
			ShowFrame(fraNew);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraNew.Visible)
				return;
			if (boolSetupMV)
			{
				// take the focus from the grid
				cmbTellerID.Focus();
				// validate all of the information
				if (Strings.Trim(GRID.TextMatrix(1, 2)) == "")
				{
					MessageBox.Show("Please select a user level.", "Incomplete Data Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					GRID.Select(1, 2);
					GRID.Focus();
					return;
				}
				if (Strings.Trim(GRID.TextMatrix(2, 2)) == "")
				{
					MessageBox.Show("Please select a plate group.", "Incomplete Data Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					GRID.Select(2, 2);
					GRID.Focus();
					return;
				}
				if (Strings.Trim(GRID.TextMatrix(3, 2)) == "")
				{
					MessageBox.Show("Please select a sticker group.", "Incomplete Data Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					GRID.Select(3, 2);
					GRID.Focus();
					return;
				}
				if (Strings.Trim(GRID.TextMatrix(4, 2)) == "")
				{
					MessageBox.Show("Please select a MVR3 group.", "Incomplete Data Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					GRID.Select(4, 2);
					GRID.Focus();
					return;
				}
			}
			if (txtDeptDiv.Text != "")
			{
				if (!ValidDeptDiv())
				{
					MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtDeptDiv.Text = string.Empty;
					txtDeptDiv.Focus();
				}
			}
			// if all of the information is present then save the teller id
			SaveTeller_2(cmbTellerID.Items[cmbTellerID.SelectedIndex].ToString());
			MessageBox.Show("Saved", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void mnuFileSave_Click()
		{
			mnuFileSave_Click(mnuFileSave, new System.EventArgs());
		}

		private void mnuSAveExit_Click(object sender, System.EventArgs e)
		{
			if (fraNew.Visible)
				return;
			mnuFileSave_Click();
			mnuExit_Click();
		}

		private bool ValidDeptDiv()
		{
			bool ValidDeptDiv = false;
			// vbPorter upgrade warning: strTemp As object	OnWrite(string(), string)
			string[] strTemp = null;
			bool boolResult;
			boolResult = true;
			if (intDivLength > 0)
			{
				strTemp = Strings.Split(txtDeptDiv.Text, "-", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strTemp, 1) > 0)
				{
					if (Strings.Trim(strTemp[0]).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) || Strings.Trim(strTemp[1]).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))
					{
						boolResult = false;
					}
				}
				else
				{
					if (Strings.Trim(strTemp[0]).Length > intDeptLength)
					{
						MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDeptDiv.Text = string.Empty;
						txtDeptDiv.Focus();
						boolResult = false;
					}
				}
			}
			else
			{
				//strTemp = txtDeptDiv.Text;
				if (Strings.Trim(txtDeptDiv.Text).Length > intDeptLength)
				{
					MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtDeptDiv.Text = string.Empty;
					txtDeptDiv.Focus();
					boolResult = false;
				}
			}
			ValidDeptDiv = boolResult;
			return ValidDeptDiv;
		}

		private void txtName_Enter(object sender, System.EventArgs e)
		{
			txtName.SelectionStart = 0;
			txtName.SelectionLength = txtName.Text.Length;
		}

		private void txtNew_Enter(object sender, System.EventArgs e)
		{
			txtNew.SelectionStart = 0;
			txtNew.SelectionLength = txtNew.Text.Length;
		}

		private void txtNew_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						cmdAddNew.Focus();
						break;
					}
			}
			//end switch
		}

		private void txtNew_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// change the letters to uppercase
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void SetupModules()
		{
			// this will check to see which modules need to be setup for the each teller
			clsDRWrapper rsMod = new clsDRWrapper();
			rsMod.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
			if (rsMod.EndOfFile() != true && rsMod.BeginningOfFile() != true)
			{
				if (FCConvert.ToBoolean(rsMod.Get_Fields_Boolean("MV")))
				{
					boolSetupMV = true;
				}
				else
				{
					boolSetupMV = false;
					fraGRID.Visible = false;
					cmdMV.Visible = false;
				}
				if (FCConvert.ToBoolean(rsMod.Get_Fields_Boolean("CR")))
					boolSetupCR = true;
				if (FCConvert.ToBoolean(rsMod.Get_Fields_Boolean("BD")))
					boolBudgetary = true;
			}
		}

		private void txtPassword_Enter(object sender, System.EventArgs e)
		{
			txtPassword.SelectionStart = 0;
			txtPassword.SelectionLength = txtPassword.Text.Length;
		}

		private void FillLevelDesc()
		{
			switch (intLevel)
			{
				case 1:
					{
						GRID.TextMatrix(1, 2, "1 - Primary Agent");
						break;
					}
				case 2:
					{
						GRID.TextMatrix(1, 2, "2 - Alternate");
						break;
					}
				case 3:
					{
						GRID.TextMatrix(1, 2, "3 - View Only");
						break;
					}
				case 4:
					{
						GRID.TextMatrix(1, 2, "4 - Regular");
						break;
					}
			}
			//end switch
		}
		// kk trocr-348 11262012  Adding CR Closeout by Dept
		private void SetFieldLengths()
		{
			if (boolBudgetary)
			{
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (!rsData.EndOfFile())
				{
					intDeptLength = FCConvert.ToInt32(Strings.Left(Strings.Trim(rsData.Get_Fields_String("Expenditure") + " "), 2));
					intDivLength = FCConvert.ToInt32(Strings.Mid(Strings.Trim(rsData.Get_Fields_String("Expenditure") + " "), 3, 2));
				}
			}
			else
			{
				intDeptLength = 50;
				// Allow free text if no Budgetary
				intDivLength = 0;
			}
		}
	}
}
