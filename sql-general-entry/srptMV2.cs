﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptMV2.
	/// </summary>
	public partial class srptMV2 : FCSectionReport
	{
		public srptMV2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptMV2";
		}

		public static srptMV2 InstancePtr
		{
			get
			{
				return (srptMV2)Sys.GetInstance(typeof(srptMV2));
			}
		}

		protected srptMV2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptMV2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intMonth;
		// vbPorter upgrade warning: curMVTotal As Decimal	OnWrite(short, Decimal)
		Decimal curMVTotal;
		// vbPorter upgrade warning: curGLTotal As Decimal	OnWrite(short, Decimal)
		Decimal curGLTotal;
		// vbPorter upgrade warning: curOthGLTotal As Decimal	OnWrite(short, Decimal)
		Decimal curOthGLTotal;
		string strGLAccount = "";
		string strOthGLAccount = "";
		int intFiscalFirstMonth;
		bool boolFirstFiscalMonthHit;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intMonth += 1;
				if (intMonth > 12)
				{
					if (intFiscalFirstMonth == 1)
					{
						eArgs.EOF = true;
					}
					else
					{
						intMonth = 1;
						eArgs.EOF = false;
					}
				}
				else if (intMonth == intFiscalFirstMonth)
				{
					// If boolFirstFiscalMonthHit Then
					eArgs.EOF = true;
					// Else
					// boolFirstFiscalMonthHit = True
					// EOF = False
					// End If
					// ElseIf intMonth = 7 Then
					// If intFiscalFirstMonth = 7 Then
					// EOF = True
					// Else
					// EOF = False
					// End If
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsAccountInfo = new clsDRWrapper())
            {
                boolFirstFiscalMonthHit = false;
                intMonth = 1;
                blnFirstRecord = true;
                curMVTotal = 0;
                curGLTotal = 0;
                curOthGLTotal = 0;
                if (modGlobalConstants.Statics.gboolBD)
                {
                    intFiscalFirstMonth =
                        FCConvert.ToInt32(
                            Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
                    intMonth = intFiscalFirstMonth;
                }

                if (modGlobalConstants.Statics.gboolCR)
                {
                    rsAccountInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 99", "TWCR0000.vb1");
                    if (rsAccountInfo.EndOfFile() != true)
                    {
                        if (FCConvert.ToString(rsAccountInfo.Get_Fields_String("Account1")) != "" &&
                            Strings.Left(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Account1")), 1) != "M")
                        {
                            strGLAccount = FCConvert.ToString(rsAccountInfo.Get_Fields_String("Account1"));
                        }

                        if (FCConvert.ToString(rsAccountInfo.Get_Fields_String("Account6")) != "" &&
                            Strings.Left(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Account6")), 1) != "M")
                        {
                            strOthGLAccount = FCConvert.ToString(rsAccountInfo.Get_Fields_String("Account6"));
                        }
                    }
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsBudInfo = new clsDRWrapper())
            {
                // vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
                Decimal curTotal;
                fldMonth.Text = Strings.Format(DateAndTime.DateValue(FCConvert.ToString(intMonth) + "/1/2004"), "MMMM");
                if (intFiscalFirstMonth == 1)
                {
                    if (intMonth < 12)
                    {
                        rsInfo.OpenRecordset(
                            "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                            FCConvert.ToString(DateAndTime.DateValue(
                                FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year))) +
                            "' and DateUpdated < '" +
                            FCConvert.ToString(DateAndTime.DateValue(
                                FCConvert.ToString(intMonth + 1) + "/1/" + FCConvert.ToString(DateTime.Today.Year))) +
                            "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                    }
                    else
                    {
                        rsInfo.OpenRecordset(
                            "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                            FCConvert.ToString(DateAndTime.DateValue(
                                FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year))) +
                            "' and DateUpdated < '" +
                            FCConvert.ToString(
                                DateAndTime.DateValue("1/1/" + FCConvert.ToString(DateTime.Today.Year + 1))) +
                            "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                    }
                }
                else
                {
                    if (DateTime.Today.Month < intFiscalFirstMonth)
                    {
                        if (intMonth < 12)
                        {
                            if (intMonth < intFiscalFirstMonth)
                            {
                                rsInfo.OpenRecordset(
                                    "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year))) + "' and DateUpdated < '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth + 1) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year))) +
                                    "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                            }
                            else
                            {
                                rsInfo.OpenRecordset(
                                    "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year - 1))) + "' and DateUpdated < '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth + 1) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year - 1))) +
                                    "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                            }
                        }
                        else
                        {
                            rsInfo.OpenRecordset(
                                "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                                FCConvert.ToString(DateAndTime.DateValue(
                                    FCConvert.ToString(intMonth) + "/1/" +
                                    FCConvert.ToString(DateTime.Today.Year - 1))) + "' and DateUpdated < '" +
                                FCConvert.ToString(
                                    DateAndTime.DateValue("1/1/" + FCConvert.ToString(DateTime.Today.Year))) +
                                "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                        }
                    }
                    else
                    {
                        if (intMonth < 12)
                        {
                            if (intMonth < intFiscalFirstMonth)
                            {
                                rsInfo.OpenRecordset(
                                    "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year + 1))) + "' and DateUpdated < '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth + 1) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year + 1))) +
                                    "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                            }
                            else
                            {
                                rsInfo.OpenRecordset(
                                    "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year))) + "' and DateUpdated < '" +
                                    FCConvert.ToString(DateAndTime.DateValue(
                                        FCConvert.ToString(intMonth + 1) + "/1/" +
                                        FCConvert.ToString(DateTime.Today.Year))) +
                                    "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                            }
                        }
                        else
                        {
                            rsInfo.OpenRecordset(
                                "SELECT SUM(ExciseTaxCharged - ExciseCreditUsed) as NetExcise, Status, TransactionType, EAP FROM ActivityMaster WHERE Status <> 'V' AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND TransactionType <> 'GVW' AND ExciseTaxCharged >= ExciseCreditUsed AND DateUpdated >= '" +
                                FCConvert.ToString(DateAndTime.DateValue(
                                    FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year))) +
                                "' and DateUpdated < '" +
                                FCConvert.ToString(
                                    DateAndTime.DateValue("1/1/" + FCConvert.ToString(DateTime.Today.Year + 1))) +
                                "' GROUP BY Status, TransactionType, EAP", "TWMV0000.vb1");
                        }
                    }
                }

                curTotal = 0;
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    do
                    {
                        if ((FCConvert.ToString(rsInfo.Get_Fields("TransactionType")) == "NRR" ||
                             FCConvert.ToString(rsInfo.Get_Fields("TransactionType")) == "NRT" ||
                             FCConvert.ToString(rsInfo.Get_Fields("TransactionType")) == "RRR" ||
                             FCConvert.ToString(rsInfo.Get_Fields("TransactionType")) == "RRT") &&
                            rsInfo.Get_Fields("EAP") == true)
                        {
                            // do nothing
                        }
                        else
                        {
                            // TODO Get_Fields: Field [NetExcise] not found!! (maybe it is an alias?)
                            //FC:FINAL:MSH - issue #1596: Get_Fields replaced to appropriate method call
                            curTotal += rsInfo.Get_Fields_Decimal("NetExcise");
                        }

                        rsInfo.MoveNext();
                    } while (rsInfo.EndOfFile() != true);
                }

                fldMV.Text = Strings.Format(curTotal, "#,##0.00");
                curMVTotal += curTotal;
                if (modGlobalConstants.Statics.gboolBD && modGlobalConstants.Statics.gboolCR && strGLAccount != "")
                {
                    if (Strings.Left(strGLAccount, 1) == "G")
                    {
                        rsBudInfo.OpenRecordset(
                            "SELECT Period, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Period = " +
                            FCConvert.ToString(intMonth) + " AND Account = '" + strGLAccount + "' GROUP BY Period",
                            "TWBD0000.vb1");
                    }
                    else if (Strings.Left(strGLAccount, 1) == "R")
                    {
                        rsBudInfo.OpenRecordset(
                            "SELECT Period, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Period = " +
                            FCConvert.ToString(intMonth) + " AND Account = '" + strGLAccount + "' GROUP BY Period",
                            "TWBD0000.vb1");
                    }

                    if (rsBudInfo.EndOfFile() != true)
                    {
                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                        fldGL.Text = Strings.Format(Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")) * -1,
                            "#,##0.00");
                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                        //FC:FINAL:MSH - issue #1596: added FCConvert.ToDecimal to avoid conversion exceptions
                        curGLTotal +=
                            FCConvert.ToDecimal((Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")) * -1));
                    }
                    else
                    {
                        fldGL.Text = "0.00";
                    }

                    if (strGLAccount != strOthGLAccount)
                    {
                        if (Strings.Left(strOthGLAccount, 1) == "G")
                        {
                            rsBudInfo.OpenRecordset(
                                "SELECT Period, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Period = " +
                                FCConvert.ToString(intMonth) + " AND Account = '" + strOthGLAccount +
                                "' GROUP BY Period", "TWBD0000.vb1");
                        }
                        else if (Strings.Left(strOthGLAccount, 1) == "R")
                        {
                            rsBudInfo.OpenRecordset(
                                "SELECT Period, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM RevenueReportInfo WHERE Period = " +
                                FCConvert.ToString(intMonth) + " AND Account = '" + strOthGLAccount +
                                "' GROUP BY Period", "TWBD0000.vb1");
                        }

                        if (rsBudInfo.EndOfFile() != true)
                        {
                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                            fldOthGL.Text =
                                Strings.Format(Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")) * -1,
                                    "#,##0.00");
                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                            //FC:FINAL:MSH - issue #1596: added FCConvert.ToDecimal to avoid conversion exceptions
                            curOthGLTotal +=
                                FCConvert.ToDecimal((Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")) * -1));
                        }
                        else
                        {
                            fldOthGL.Text = "0.00";
                        }
                    }
                    else
                    {
                        fldOthGL.Text = "0.00";
                    }

                    fldDifference.Text =
                        Strings.Format(
                            FCConvert.ToDecimal(fldMV.Text) -
                            (FCConvert.ToDecimal(fldGL.Text) + FCConvert.ToDecimal(fldOthGL.Text)), "#,##0.00");
                }
                else
                {
                    fldGL.Text = "N/A";
                    fldDifference.Text = "N/A";
                }
            }
        }

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalMV.Text = Strings.Format(curMVTotal, "#,##0.00");
			if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gboolBD && strGLAccount != "")
			{
				fldTotalGL.Text = Strings.Format(curGLTotal, "#,##0.00");
				fldTotalOthGL.Text = Strings.Format(curOthGLTotal, "#,##0.00");
				fldTotalDifference.Text = Strings.Format(curMVTotal - (curGLTotal + curOthGLTotal), "#,##0.00");
			}
			else
			{
				fldTotalGL.Text = "N/A";
				fldTotalDifference.Text = "N/A";
			}
		}

		
	}
}
