﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmProgramDates.
	/// </summary>
	public partial class frmProgramDates : BaseForm
	{
		public frmProgramDates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmProgramDates InstancePtr
		{
			get
			{
				return (frmProgramDates)Sys.GetInstance(typeof(frmProgramDates));
			}
		}

		protected frmProgramDates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		FileInfo f;

		private void cmbModules_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			/*? On Error Resume Next  */
			try
			{
				// vbPorter upgrade warning: strList As FixedString	OnWrite(string)
				//char[] strList = new char[100];
				string strList = "";
				string strFile = "";
				string strEXE = "";
				string strVB1 = "";
				string strExpire = "";
				string strAgentLevel = "";
				string strDatabase = "";
				string strRedbookPermissions = "";
				object dd = null;
				int ii;
				int Index;
				clsDRWrapper clsTemp = new clsDRWrapper();
				int lngTemp = 0;
				int lngTemp2 = 0;
				string strTemp = "";
				string strTemp2 = "";
				// Index = cmbModules.ListIndex
				Index = cmbModules.ItemData(cmbModules.SelectedIndex);
				switch (Index)
				{
					case 0:
						{
							// GN
							strEXE = "TWGN*.DLL";
							strVB1 = "TWGN*.VB1;TSGN*.VB1";
							strDatabase = "systemsettings";
							break;
						}
					case 16:
						{
							strEXE = "tWAR*.DLL";
							strVB1 = "TWAR*.VB1";
							strDatabase = "TWAR0000.vb1";
							break;
						}
					case 12:
						{
							// RE
							strEXE = "TWRE*.DLL";
							strVB1 = "TWRE*.VB1;TSRE*.VB1";
							strDatabase = "TWRE0000.vb1";
							break;
						}
					case 11:
						{
							// PP
							strEXE = "TWPP*.DLL";
							strVB1 = "TWPP*.VB1;TSPP*.VB1";
							strDatabase = "TWPP0000.vb1";
							break;
						}
					case 1:
						{
							// BL
							strEXE = "TWBL*.DLL";
							strVB1 = "TWBL*.VB1;TSBL*.VB1";
							strDatabase = "TWBL0000.vb1";
							break;
						}
					case 2:
						{
							strEXE = "TWRB*.DLL";
							strVB1 = "TWRB*.vb1";
							strDatabase = "TWRB0000.vb1";
							break;
						}
					case 13:
						{
							// CL
							strEXE = "TWCL*.DLL";
							strVB1 = "TWCL*.VB1;TSCL*.VB1";
							strDatabase = "TWCL0000.vb1";
							break;
						}
					case 5:
						{
							// CK
							strEXE = "TWCK*.DLL";
							strVB1 = "TWCK*.VB1;TSCK*.VB1";
							strDatabase = "TWCK0000.vb1";
							break;
						}
					case 15:
						{
							// VR
							strEXE = "TWVR*.DLL";
							strVB1 = "TWVR*.VB1;TSVR*.VB1";
							strDatabase = "TWVR0000.vb1";
							break;
						}
					case 6:
						{
							// CE
							strEXE = "TWCE*.DLL";
							strVB1 = "TWCE*.VB1;TSCE*.VB1";
							strDatabase = "TWCE0000.vb1";
							break;
						}
					case 3:
						{
							// BD
							strEXE = "TWBD*.DLL";
							strVB1 = "TWBD*.VB1;TSBD*.VB1";
							strDatabase = "TWBD0000.vb1";
							break;
						}
					case 4:
						{
							// CR
							strEXE = "TWCR*.DLL";
							strVB1 = "TWCR*.VB1;TSCR*.VB1";
							strDatabase = "TWCR0000.vb1";
							break;
						}
					case 10:
						{
							// PY
							strEXE = "TWPY*.DLL";
							strVB1 = "TWPY*.VB1;TSPY*.VB1";
							strDatabase = "TWPY0000.vb1";
							break;
						}
					case 14:
						{
							// UT
							strEXE = "TWUT*.DLL";
							strVB1 = "TWUT*.VB1;TSUT*.VB1";
							strDatabase = "TWUT0000.vb1";
							break;
						}
					case 9:
						{
							// MV
							strEXE = "TWMV*.DLL;TWRB*.DLL";
							strVB1 = "TWMV*.VB1;TSMV*.VB1;TWRB*.VB1";
							strDatabase = "TWMV0000.vb1";
							break;
						}
					case 7:
						{
							// E9
							strEXE = "TWE9*.DLL";
							strVB1 = "TWE9*.VB1;TSE9*.VB1";
							strDatabase = "TWE90000.vb1";
							break;
						}
					case 8:
						{
							// FA
							strEXE = "TWFA*.DLL";
							strVB1 = "TWFA*.VB1;TSFA*.VB1";
							strDatabase = "TWFA0000.vb1";
							break;
						}
				}
				//end switch
				clsDRWrapper rsExpire = new clsDRWrapper();
				clsDRWrapper rsAgentLevel = new clsDRWrapper();
				rsExpire.OpenRecordset("Select * from Modules", "systemsettings");
				if (!rsExpire.EndOfFile())
				{
					switch (Index)
					{
						case 0:
							{
								// GN
								strExpire = "";
								break;
							}
						case 2:
							{
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("RBDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("RBDate"), "MM/dd/yyyy") : "";
								strAgentLevel = FCConvert.ToString(rsExpire.GetData("MV_STATELEVEL"));
								switch (FCConvert.ToInt32(strAgentLevel))
								{
									case 1:
										{
											strAgentLevel += " - RE REG";
											break;
										}
									case 2:
										{
											strAgentLevel += " - NEW";
											break;
										}
									case 3:
										{
											strAgentLevel += " - TRUCK";
											break;
										}
									case 4:
										{
											strAgentLevel += " - TRANSIT";
											break;
										}
									case 5:
										{
											strAgentLevel += " - ENHANCED RE REG";
											break;
										}
									case 6:
										{
											strAgentLevel += " - EXCISE TAX";
											break;
										}
								}
								//end switch
								strRedbookPermissions = "";
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_AUTO")) == "Y")
								{
									strRedbookPermissions += "AU, ";
								}
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_HVTK")) == "Y")
								{
									strRedbookPermissions += "TK, ";
								}
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_MTCY")) == "Y")
								{
									strRedbookPermissions += "MC, ";
								}
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_RCVH")) == "Y")
								{
									strRedbookPermissions += "RV, ";
								}
								strRedbookPermissions = Strings.Left(strRedbookPermissions, strRedbookPermissions.Length - 2);
								break;
							}
						case 16:
							{
								// ar
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("ARDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("ARDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 12:
							{
								// RE
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("REDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("REDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 11:
							{
								// PP
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("PPDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("PPDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 1:
							{
								// BL
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("BLDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("BLDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 13:
							{
								// CL
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("CLDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("CLDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 5:
							{
								// CK
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("CKDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("CKDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 15:
							{
								// VR
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("VRDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("VRDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 7:
							{
								// CE
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("CEDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("CEDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 3:
							{
								// BD
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("BDDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("BDDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 4:
							{
								// CR
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("CRDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("CRDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 10:
							{
								// PY
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("PYDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("PYDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 14:
							{
								// UT
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("UTDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("UTDate"), "MM/dd/yyyy") : "";
								break;
							}
						case 9:
							{
								// MV
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("MVDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("MVDate"), "MM/dd/yyyy") : "";
								strAgentLevel = FCConvert.ToString(rsExpire.GetData("MV_STATELEVEL"));
								switch (FCConvert.ToInt32(strAgentLevel))
								{
									case 1:
										{
											strAgentLevel += " - RE REG";
											break;
										}
									case 2:
										{
											strAgentLevel += " - NEW";
											break;
										}
									case 3:
										{
											strAgentLevel += " - TRUCK";
											break;
										}
									case 4:
										{
											strAgentLevel += " - TRANSIT";
											break;
										}
									case 5:
										{
											strAgentLevel += " - ENHANCED RE REG";
											break;
										}
									case 6:
										{
											strAgentLevel += " - EXCISE TAX";
											break;
										}
								}
								//end switch
								strRedbookPermissions = "";
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_AUTO")) == "Y")
								{
									strRedbookPermissions += "AU,";
								}
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_HVTK")) == "Y")
								{
									strRedbookPermissions += "TK,";
								}
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_MTCY")) == "Y")
								{
									strRedbookPermissions += "MC,";
								}
								if (FCConvert.ToString(rsExpire.GetData("MV_RBLVL_RCVH")) == "Y")
								{
									strRedbookPermissions += "RV,";
								}
								strRedbookPermissions = Strings.Left(strRedbookPermissions, strRedbookPermissions.Length - 1);
								// Case 7             'TC
								// strExpire = rsExpire.GetData("TCDate")
								break;
							}
					//FC:FINAL:AM: duplicate case
					//case 7:
					//    {
					//        strExpire = FCConvert.ToString(rsExpire.GetData("E9Date"));
					//        break;
					//    }
						case 8:
							{
								strExpire = Convert.ToDateTime(rsExpire.Get_Fields_DateTime("FADate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("FADate"), "MM/dd/yyyy") : "";
								break;
							}
					}
					//end switch
				}
				lstPrograms.Clear();
				//FC:FINAL:SBE - display as table, empty spaces are not displayed
				lstPrograms.Columns.Add("");
				lstPrograms.Columns.Add("");
				lstPrograms.Columns.Add("");
				lstPrograms.Columns[0].Width = 200;
				lstPrograms.Columns[1].Width = 200;
				lstPrograms.Columns[2].Width = 200;
				lstPrograms.Columns[3].Width = 170;
				lstPrograms.GridLineStyle = GridLineStyle.None;
				// VB1's
				//lstPrograms.AddItem("   FILE               DATE & TIME            Version         ");
				lstPrograms.AddItem("FILE\tDATE & TIME\tVersion");
				lstPrograms.AddItem(" ");
				filPrograms.Path = Application.MapPath("\\");
				// filPrograms.Pattern = strVB1
				// For ii = 0 To filPrograms.ListCount - 1
				// strList = String(100, " ")
				// Set f = ff.GetFile(filPrograms.List(ii))
				// strFile = Trim$(UCase$(filPrograms.List(ii)))
				// dd = f.DateLastModified
				// Mid$(strList, 1, 18) = strFile & String(18 - Len(strFile), " ")
				// Mid$(strList, 20, 22) = dd & String(22 - Len(dd), " ")
				// Mid$(strList, 43, 10) = Format(Format(f.Size, "#,###"), "@@@@@@@@@@")
				// lstPrograms.AddItem strList
				// Next ii
				// lstPrograms.AddItem " "
				// lstPrograms.AddItem " "
				// lstPrograms.AddItem " "
				// lstPrograms.AddItem "  PROGRAM             DATE & TIME          SIZE  "
				// lstPrograms.AddItem " "
				// EXE's
				filPrograms.Path = Path.Combine(Application.StartupPath, "bin");
				filPrograms.Pattern = strEXE;
				CFileVersionInfo CurrentApp;
				for (ii = 0; ii <= filPrograms.Items.Count - 1; ii++)
				{
					f = new FileInfo(filPrograms.Path + "\\" + filPrograms.Items[ii].ToString());
					//strList = Strings.StrDup(100, " ");
					strList = string.Empty;
					strFile = Strings.Trim(Strings.UCase(filPrograms.Items[ii].ToString()));
					dd = f.LastAccessTime;
					//fecherFoundation.Strings.MidSet(ref strList, 1, 18, strFile + Strings.StrDup(18 - strFile.Length, " "));
					strList += strFile + "\t";
					//fecherFoundation.Strings.MidSet(ref strList, 20, 22, dd + Strings.StrDup(22 - FCConvert.ToString(dd).Length, " "));
					strList += FCConvert.ToString(dd) + "\t";
					// Mid$(strList, 43, 10) = Format(Format(f.Size, "#,###"), "@@@@@@@@@@")
					//FC:FINAL:MSH - issue #1251: get file version and display it
					//CurrentApp = new CFileVersionInfo();
					//CurrentApp.FullPathName = Strings.UCase(filPrograms.Path + "\\" + strFile);
					//if (CurrentApp.Available)
					//{
					//    // Mid(strList, 45, 12) = Right(String(12, " ") & CurrentApp.ProductVersion, 12)
					//    //fecherFoundation.Strings.MidSet(ref strList, 46, 12, Strings.Left(CurrentApp.ProductVersion + Strings.StrDup(12, " "), 12));
					//    strList += CurrentApp.ProductVersion;
					//}
					string assemblyPath = Strings.UCase(filPrograms.Path + "\\" + strFile);
					if (File.Exists(assemblyPath))
					{
						var assembly = System.Reflection.Assembly.LoadFrom(assemblyPath);
						var version = assembly.GetName().Version;
						strList += string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
					}
					lstPrograms.AddItem(strList);
				}
				// ii
				lstPrograms.AddItem(" ");
				lstPrograms.AddItem(" ");
				// lstPrograms.AddItem " "
				if (Index == 9 || Index == 2)
				{
					//lstPrograms.AddItem("  EXPIRATION DATE       BLUE BOOK       AGENT LEVEL");
					lstPrograms.AddItem("\tEXPIRATION DATE\tBLUE BOOK\tAGENT LEVEL");
				}
				else if (Index == 0)
				{
					lstPrograms.AddItem("EXPIRATION DATES");
				}
				else if (Index == 12)
				{
					lstPrograms.AddItem(" \tEXPIRATION DATES\tMAX CARDS\tCARDS USED");
				}
				else if (Index == 5)
				{
					// clerk
					lstPrograms.AddItem(" \tEXPIRATION DATE\tPopulation\tLevel");
				}
				else
				{
					lstPrograms.AddItem("EXPIRATION DATE");
				}
				lstPrograms.AddItem(" ");
				if (Index == 9 || Index == 2)
				{
					//lstPrograms.AddItem("Motor Vehicle " + strExpire + "   " + Strings.StrDup(11, " ") + "     " + strAgentLevel);
					//lstPrograms.AddItem("Blue Book     " + rsExpire.Get_Fields_DateTime("rbdate") + "   " + strRedbookPermissions + Strings.Space(11 - strRedbookPermissions.Length));
					lstPrograms.AddItem("Motor Vehicle\t" + strExpire + "\t\t" + strAgentLevel);
					lstPrograms.AddItem("Blue Book\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("rbdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("rbdate"), "MM/dd/yyyy") : "") + "\t" + strRedbookPermissions);
					if (Index == 9)
					{
						//lstPrograms.AddItem("Rapid Renewal " + rsExpire.Get_Fields_DateTime("rrdate"));
						lstPrograms.AddItem("Rapid Renewal\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("rrdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("rrdate"), "MM/dd/yyyy") : ""));
					}
				}
				else if (Index == 13)
				{
					lstPrograms.AddItem("Tax Collections\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("cldate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("cldate"), "MM/dd/yyyy") : ""));
					lstPrograms.AddItem("Tax Services\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("tsdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("tsdate"), "MM/dd/yyyy") : ""));
				}
				else if (Index == 0)
				{
					//FC:FINAL:CHN - i.issue #1623: Program Information.
					lstPrograms.AddItem(GetLine("Accounts Rec.", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("ARDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("ARDate"), "MM/dd/yyyy") : "", "twar0000.dll"));
					// lstPrograms.AddItem "Billing          " & rsExpire.Fields("BLDate") & "  " & VersionOfFile(filPrograms.Path & "\twbl0000.dll")
					//FC:FINAL:CHN - i.issue #1623: Program Information.
					lstPrograms.AddItem(GetLine("Billing", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("bldate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("bldate"), "MM/dd/yyyy") : "", "twbl0000.dll"));
					lstPrograms.AddItem(GetLine("Blue Book", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("RBDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("RBDate"), "MM/dd/yyyy") : "", "twrb0000.dll"));
					lstPrograms.AddItem(GetLine("Budgetary", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("bddate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("bddate"), "MM/dd/yyyy") : "", "twbd0000.dll"));
					lstPrograms.AddItem(GetLine("Cash Receipting", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("crdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("crdate"), "MM/dd/yyyy") : "", "twcr0000.dll"));
					lstPrograms.AddItem(GetLine("Clerk", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("ckdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("ckdate"), "MM/dd/yyyy") : "", "twck0000.dll"));
					lstPrograms.AddItem(GetLine("Code Enforcement", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("cedate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("cedate"), "MM/dd/yyyy") : "", "twce0000.dll"));
					lstPrograms.AddItem(GetLine("Collections", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("cldate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("cldate"), "MM/dd/yyyy") : "", "twcl0000.dll"));
					lstPrograms.AddItem("Tax Services\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("tsdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("tsdate"), "MM/dd/yyyy") : ""));
					lstPrograms.AddItem(GetLine("Fixed Assets", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("FAdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("FAdate"), "MM/dd/yyyy") : "", "twfa0000.dll"));
					lstPrograms.AddItem(GetLine("Motor Vehicle", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("mvdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("mvdate"), "MM/dd/yyyy") : "", "twmv0000.dll"));
					lstPrograms.AddItem("Rapid Renewal\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("rrdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("rrdate"), "MM/dd/yyyy") : ""));
					lstPrograms.AddItem(GetLine("PayRoll", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("pydate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("pydate"), "MM/dd/yyyy") : "", "twpy0000.dll"));
					lstPrograms.AddItem(GetLine("Personal Property", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("PPdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("PPdate"), "MM/dd/yyyy") : "", "twpp0000.dll"));
					lstPrograms.AddItem(GetLine("Real Estate", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("redate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("redate"), "MM/dd/yyyy") : "", "twre0000.dll"));
					lstPrograms.AddItem("Handheld\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("rHdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("rHdate"), "MM/dd/yyyy") : ""));
					lstPrograms.AddItem("Sketches\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("skDATE")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("skDATE"), "MM/dd/yyyy") : ""));
					lstPrograms.AddItem("Commercial\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("cmdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("cmdate"), "MM/dd/yyyy") : ""));
					if (FCConvert.ToBoolean(rsExpire.Get_Fields_Boolean("CM")))
					{
						if (Information.IsDate(rsExpire.Get_Fields("cmdate")))
						{
							if (rsExpire.Get_Fields_DateTime("Cmdate").ToOADate() != 0)
							{
								if (Conversion.Val(rsExpire.Get_Fields_Int16("CMLevel")) == 1)
								{
									lstPrograms.AddItem("Level\t" + "Limited");
								}
								else
								{
									lstPrograms.AddItem("Level\t" + "Full");
								}
							}
						}
					}
					// lstPrograms.AddItem "   Inf. Extract    " & rsExpire.Fields("rxdate")
					//FC:FINAL:CHN - i.issue #1623: Program Information.
					lstPrograms.AddItem(GetLine("Utility Billing", Convert.ToDateTime(rsExpire.Get_Fields_DateTime("UTDate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("UTDate"), "MM/dd/yyyy") : "", "twut0000.dll"));
					// lstPrograms.AddItem getLine("Voter Registration       " & rsExpire.Fields("VRDate")
				}
				else if (Index == 5)
				{
					// clerk
					if (!rsExpire.EndOfFile())
					{
						lngTemp2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsExpire.Get_Fields_Int16("cklevel"))));
						if (lngTemp2 <= 0)
						{
							lngTemp = 0;
							lngTemp2 = 0;
							strTemp = "Unlimited";
							strTemp2 = "Full Clerk ";
						}
						else
						{
							if ((lngTemp2 & modTrioStart.CKLEVELPOPLEVEL1) == modTrioStart.CKLEVELPOPLEVEL1)
							{
								strTemp = "      4000";
							}
							else if ((lngTemp2 & modTrioStart.CKLEVELPOPLEVEL2) == modTrioStart.CKLEVELPOPLEVEL2)
							{
								strTemp = "     10000";
							}
							else
							{
								strTemp = "Unlimited ";
							}
							if ((lngTemp2 & modTrioStart.CKLEVELDOGSONLY) == modTrioStart.CKLEVELDOGSONLY)
							{
								strTemp2 = "Dogs Only";
							}
							else if ((lngTemp2 & modTrioStart.CKLEVELVITALSONLY) == modTrioStart.CKLEVELVITALSONLY)
							{
								strTemp2 = "Vitals Only";
							}
							else
							{
								strTemp2 = "Full Clerk";
							}
						}
						//lstPrograms.AddItem("Clerk        " + strExpire + Strings.Space(7) + strTemp + Strings.Space(3) + strTemp2);
						lstPrograms.AddItem("Clerk\t" + strExpire + "\t" + strTemp + "\t" + strTemp2);
					}
					else
					{
						//lstPrograms.AddItem("      " + strExpire + "       ");
						lstPrograms.AddItem(" \t" + strExpire + "\t");
					}
				}
				else if (Index == 12)
				{
					if (!rsExpire.EndOfFile())
					{
						if (File.Exists("twre0000.vb1"))
						{
							clsTemp.OpenRecordset("select count(rsaccount) as numcards from master ", "twre0000.vb1");
							// TODO Get_Fields: Field [numcards] not found!! (maybe it is an alias?)
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("numcards"))));
						}
						else
						{
							lngTemp = 0;
						}
						//lstPrograms.AddItem("Real Estate  " + strExpire + "       " + Strings.Space(7) + rsExpire.Get_Fields_Int32("RE_MaxAccounts") + Strings.Space(6) + FCConvert.ToString(lngTemp));
						//lstPrograms.AddItem("Handheld     " + rsExpire.Get_Fields_DateTime("rHdate"));
						//lstPrograms.AddItem("Sketches     " + rsExpire.Get_Fields_DateTime("skDATE"));
						//lstPrograms.AddItem("Commercial   " + rsExpire.Get_Fields_DateTime("cmdate"));
						lstPrograms.AddItem("Real Estate\t" + strExpire + "\t" + rsExpire.Get_Fields_Int32("RE_MaxAccounts") + "\t" + FCConvert.ToString(lngTemp));
						lstPrograms.AddItem("Handheld\t" + rsExpire.Get_Fields("rHdate"));
						lstPrograms.AddItem("Sketches\t" + rsExpire.Get_Fields("skDATE"));
						lstPrograms.AddItem("Commercial\t" + (Convert.ToDateTime(rsExpire.Get_Fields_DateTime("cmdate")).ToOADate() != 0 ? Strings.Format(rsExpire.Get_Fields_DateTime("cmdate"), "MM/dd/yyyy") : ""));
						if (FCConvert.ToBoolean(rsExpire.Get_Fields_Boolean("CM")))
						{
							if (Information.IsDate(rsExpire.Get_Fields("cmdate")))
							{
								if (rsExpire.Get_Fields_DateTime("Cmdate").ToOADate() != 0)
								{
									if (Conversion.Val(rsExpire.Get_Fields_Int16("CMLevel")) == 1)
									{
										//lstPrograms.AddItem("    Level    " + "Limited");
										lstPrograms.AddItem("Level\t" + "Limited");
									}
									else
									{
										//lstPrograms.AddItem("    Level    " + "Full");
										lstPrograms.AddItem("Level\t" + "Full");
									}
								}
							}
						}
						// lstPrograms.AddItem "Inf. Extract " & rsExpire.Fields("rxdate")
					}
					else
					{
						//lstPrograms.AddItem("      " + strExpire + "       ");
						lstPrograms.AddItem(" \t" + strExpire + "\t");
					}
				}
				else
				{
					//lstPrograms.AddItem("    " + strExpire);
					//lstPrograms.AddItem(" \t" + strExpire);
					//FC:FINAL:MSH - issue #1252: WiseJ doesn't support spaces and tabs in ListViewItems and in AddItem implementations each tab 
					// are parsed as new column
					lstPrograms.AddItem(strExpire);
				}
				// Dim RS As DAO.Recordset
				// Dim db As DAO.Database
				// 
				// On Error GoTo ErrorHandler
				// txtVersion.Visible = False
				// txtVersionDate.Visible = False
				// lblVersion.Visible = False
				// lblVersionDate.Visible = False
				// Frame1.Visible = False
				// If ff.FileExists(strDatabase) = True Then
				// Set db = OpenDatabase(strDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
				// Set RS = db.OpenRecordset("SELECT * FROM DatabaseVersion")
				// 
				// txtVersion.Visible = True
				// txtVersionDate.Visible = True
				// lblVersion.Visible = True
				// lblVersionDate.Visible = True
				// Frame1.Visible = True
				// 
				// txtVersion = RS.Fields("VersionNumber")
				// txtVersionDate = Format(RS.Fields("LastUpdated"), "MM/dd/yyyy")
				// End If
				ErrorHandler:
				;
			}
			catch
			{
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string GetLine(string strDesc, string strDate, string strProgName)
		{
			string GetLine = "";
			string strReturn;
			//strReturn = Strings.StrDup(100, " ");
			strReturn = string.Empty;
			strDesc = Strings.Trim(strDesc);
			strReturn += strDesc + "\t";
			//fecherFoundation.Strings.MidSet(ref strReturn, 1, 17, Strings.Left(strDesc + Strings.StrDup(17, " "), 17));
			// Mid(strReturn, 18) = strDate
			//fecherFoundation.Strings.MidSet(ref strReturn, 20, 22, strDate + Strings.StrDup(22 - strDate.Length, " "));
			strReturn += strDate + "\t";
			//fecherFoundation.Strings.MidSet(ref strReturn, 46, 12, Strings.Left(VersionOfFile(filPrograms.Path + "\\" + strProgName) + Strings.StrDup(12, " "), 12));
			string assemblyPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), strProgName);
			if (File.Exists(assemblyPath))
			{
				var assembly = System.Reflection.Assembly.LoadFrom(assemblyPath);
				var version = assembly.GetName().Version;
				strReturn += string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
			}
			GetLine = strReturn;
			return GetLine;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string VersionOfFile(string strFile)
		{
			string VersionOfFile = "";
			CFileVersionInfo CurrentApp = new CFileVersionInfo();
			string strReturn = "";
			CurrentApp.FullPathName = Strings.UCase(strFile);
			if (CurrentApp.Available)
			{
				strReturn = CurrentApp.ProductVersion;
			}
			VersionOfFile = strReturn;
			return VersionOfFile;
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			frmProgramDates.InstancePtr.Unload();
		}

		private void frmProgramDates_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
				return;
			FillCombo();
		}
		// Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKey1, 97
		// If optprogram(1).Enabled Then optprogram(1).Value = True
		// Case vbKey2, 98
		// If optprogram(2).Enabled Then optprogram(2).Value = True
		// Case vbKey3, 99
		// If optprogram(3).Enabled Then optprogram(3).Value = True
		// Case vbKey4, 100
		// If optprogram(4).Enabled Then optprogram(4).Value = True
		// Case vbKey6, 102
		// If optprogram(5).Enabled Then optprogram(5).Value = True
		// Case vbKey7, 103
		// If optprogram(6).Enabled Then optprogram(6).Value = True
		// Case vbKeyA
		// If optprogram(7).Enabled Then optprogram(7).Value = True
		// Case vbKeyB
		// If optprogram(8).Enabled Then optprogram(8).Value = True
		// Case vbKeyC
		// If optprogram(9).Enabled Then optprogram(9).Value = True
		// Case vbKeyD
		// If optprogram(10).Enabled Then optprogram(10).Value = True
		// Case vbKeyG
		// If optprogram(11).Enabled Then If optprogram(1).Enabled Then optprogram(11).Value = True
		// Case vbKeyH
		// If optprogram(12).Enabled Then optprogram(12).Value = True
		// Case vbKeyJ
		// If optprogram(13).Enabled Then optprogram(13).Value = True
		// Case vbKeyK
		// If optprogram(14).Enabled Then optprogram(14).Value = True
		// End Select
		// End Sub
		private void frmProgramDates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmProgramDates.InstancePtr.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmProgramDates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmProgramDates properties;
			//frmProgramDates.ScaleWidth	= 9045;
			//frmProgramDates.ScaleHeight	= 7965;
			//frmProgramDates.LinkTopic	= "Form1";
			//End Unmaped Properties
			// Call EnableSecurityButtons(True)
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this, false);
		}
		// Public Sub EnableSecurityButtons(ByVal boolState As Boolean)
		// Dim intCounter As Integer
		// For intCounter = 0 To 14
		// If Not boolState Then
		// optprogram(intCounter).Enabled = False
		// End If
		// Next
		//
		// If boolState Then
		// Dim ff As New FCFileSystemObject
		// If ff.FileExists("TWGN0000.VB1") = False Then
		// MsgBox "Security database TWGN0000.VB1 could not be found.", vbCritical + vbOKOnly, "TRIO Software"
		// End If
		//
		// Call CheckExpirationDate
		// optprogram(0).Enabled = True
		// optprogram(1).Enabled = RealEstateAllowed
		// optprogram(2).Enabled = PersonalPropertyAllowed
		// optprogram(3).Enabled = BillingAllowed
		// optprogram(4).Enabled = CollectionsAllowed
		// optprogram(5).Enabled = ClerkAllowed
		// optprogram(6).Enabled = VoterRegAllowed
		// optprogram(7).Enabled = CodeAllowed
		// optprogram(8).Enabled = BudgetaryAllowed
		// optprogram(9).Enabled = CashReceiptAllowed
		// optprogram(10).Enabled = PayrollAllowed
		// optprogram(11).Enabled = UtilitiesAllowed
		// optprogram(12).Enabled = MotorVehicleAllowed
		// optprogram(13).Enabled = TaxCalculator
		// optprogram(14).Enabled = E911
		// End If
		// End Sub
		private void FillCombo()
		{
			cmbModules.Clear();
			cmbModules.AddItem("All TRIO Programs");
			cmbModules.ItemData(cmbModules.NewIndex, 0);
			cmbModules.AddItem("Accounts Receivable");
			cmbModules.ItemData(cmbModules.NewIndex, 16);
			cmbModules.AddItem("Billing");
			cmbModules.ItemData(cmbModules.NewIndex, 1);
			cmbModules.AddItem("Blue Book");
			cmbModules.ItemData(cmbModules.NewIndex, 2);
			cmbModules.AddItem("Budgetary");
			cmbModules.ItemData(cmbModules.NewIndex, 3);
			cmbModules.AddItem("Cash Receipting");
			cmbModules.ItemData(cmbModules.NewIndex, 4);
			cmbModules.AddItem("Clerk");
			cmbModules.ItemData(cmbModules.NewIndex, 5);
			cmbModules.AddItem("Code Enforcement");
			cmbModules.ItemData(cmbModules.NewIndex, 6);
			cmbModules.AddItem("Fixed Assets");
			cmbModules.ItemData(cmbModules.NewIndex, 8);
			cmbModules.AddItem("Motor Vehicle");
			cmbModules.ItemData(cmbModules.NewIndex, 9);
			cmbModules.AddItem("Payroll");
			cmbModules.ItemData(cmbModules.NewIndex, 10);
			cmbModules.AddItem("Personal Property");
			cmbModules.ItemData(cmbModules.NewIndex, 11);
			cmbModules.AddItem("Real Estate");
			cmbModules.ItemData(cmbModules.NewIndex, 12);
			cmbModules.AddItem("Tax Collections");
			cmbModules.ItemData(cmbModules.NewIndex, 13);
			cmbModules.AddItem("Utility Billing");
			cmbModules.ItemData(cmbModules.NewIndex, 14);
			cmbModules.AddItem("Voter Registration");
			cmbModules.ItemData(cmbModules.NewIndex, 15);
			cmbModules.SelectedIndex = 0;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			this.Unload();
		}

		// Private Sub optProgram_Click(Index As Integer)
		// On Error Resume Next
		//
		// Dim strList As String * 100
		// Dim strFile As String
		// Dim strEXE As String
		// Dim strVB1 As String
		// Dim strExpire As String
		// Dim strAgentLevel As String
		// Dim strDatabase As String
		// Dim strRedbookPermissions As String
		// Dim dd
		// Dim ii As Integer
		//
		// Select Case Index
		// Case 0              'GN
		// strEXE = "TWGN*.EXE"
		// strVB1 = "TWGN*.VB1;TSGN*.VB1"
		// strDatabase = "TWGN0000.vb1"
		// Case 16
		// strEXE = "TWAR*.EXE"
		// strVB1 = "TWAR*.VB1"
		// strDatabase = "TWAR0000.vb1"
		// Case 1              'RE
		// strEXE = "TWRE*.EXE"
		// strVB1 = "TWRE*.VB1;TSRE*.VB1"
		// strDatabase = "TWRE0000.vb1"
		// Case 2              'PP
		// strEXE = "TWPP*.EXE"
		// strVB1 = "TWPP*.VB1;TSPP*.VB1"
		// strDatabase = "TWPP0000.vb1"
		// Case 3              'BL
		// strEXE = "TWBL*.EXE"
		// strVB1 = "TWBL*.VB1;TSBL*.VB1"
		// strDatabase = "TWBL0000.vb1"
		// Case 4              'CL
		// strEXE = "TWCL*.EXE"
		// strVB1 = "TWCL*.VB1;TSCL*.VB1"
		// strDatabase = "TWCL0000.vb1"
		// Case 5              'CK
		// strEXE = "TWCK*.EXE"
		// strVB1 = "TWCK*.VB1;TSCK*.VB1"
		// strDatabase = "TWCK0000.vb1"
		// Case 6              'VR
		// strEXE = "TWVR*.EXE"
		// strVB1 = "TWVR*.VB1;TSVR*.VB1"
		// strDatabase = "TWVR0000.vb1"
		// Case 7              'CE
		// strEXE = "TWCE*.EXE"
		// strVB1 = "TWCE*.VB1;TSCE*.VB1"
		// strDatabase = "TWCE0000.vb1"
		// Case 8              'BD
		// strEXE = "TWBD*.EXE"
		// strVB1 = "TWBD*.VB1;TSBD*.VB1"
		// strDatabase = "TWBD0000.vb1"
		// Case 9              'CR
		// strEXE = "TWCR*.EXE"
		// strVB1 = "TWCR*.VB1;TSCR*.VB1"
		// strDatabase = "TWCR0000.vb1"
		// Case 10             'PY
		// strEXE = "TWPY*.EXE"
		// strVB1 = "TWPY*.VB1;TSPY*.VB1"
		// strDatabase = "TWPY0000.vb1"
		// Case 11             'UT
		// strEXE = "TWUT*.EXE"
		// strVB1 = "TWUT*.VB1;TSUT*.VB1"
		// strDatabase = "TWUT0000.vb1"
		// Case 12             'MV
		// strEXE = "TWMV*.EXE;TWRB*.EXE"
		// strVB1 = "TWMV*.VB1;TSMV*.VB1;TWRB*.VB1"
		// strDatabase = "TWMV0000.vb1"
		// Case 13             'TC
		// strEXE = "TWTC*.EXE"
		// strVB1 = "TWTC*.VB1;TSTC*.VB1"
		// strDatabase = "TWTC0000.vb1"
		// Case 14             'E9
		// strEXE = "TWE9*.EXE"
		// strVB1 = "TWE9*.VB1;TSE9*.VB1"
		// strDatabase = "TWE90000.vb1"
		//
		// End Select
		//
		// Dim rsExpire As New clsDRWrapper
		// Dim rsAgentLevel As New clsDRWrapper
		//
		// Call rsExpire.OpenRecordset("Select * from Modules", "TWGN0000.vb1")
		// If Not rsExpire.EndOfFile Then
		// Select Case Index
		// Case 0              'GN
		// strExpire = ""
		// Case 1              'RE
		// strExpire = rsExpire.GetData("REDate")
		// Case 2              'PP
		// strExpire = rsExpire.GetData("PPDate")
		// Case 3              'BL
		// strExpire = rsExpire.GetData("BLDate")
		// Case 4              'CL
		// strExpire = rsExpire.GetData("CLDate")
		// Case 5              'CK
		// strExpire = rsExpire.GetData("CKDate")
		// Case 6              'VR
		// strExpire = rsExpire.GetData("VRDate")
		// Case 7              'CE
		// strExpire = rsExpire.GetData("CEDate")
		// Case 8              'BD
		// strExpire = rsExpire.GetData("BDDate")
		// Case 9              'CR
		// strExpire = rsExpire.GetData("CRDate")
		// Case 10             'PY
		// strExpire = rsExpire.GetData("PYDate")
		// Case 11             'UT
		// strExpire = rsExpire.GetData("UTDate")
		// Case 12             'MV
		// strExpire = rsExpire.GetData("MVDate")
		// Call rsAgentLevel.OpenRecordset("SELECT * FROM WMV", "TWGN0000.vb1")
		// strAgentLevel = rsExpire.GetData("MV_STATELEVEL")
		// Select Case strAgentLevel
		// Case 1
		// strAgentLevel = strAgentLevel & " - RE REG"
		// Case 2
		// strAgentLevel = strAgentLevel & " - NEW"
		// Case 3
		// strAgentLevel = strAgentLevel & " - TRUCK"
		// Case 4
		// strAgentLevel = strAgentLevel & " - TRANSIT"
		// Case 5
		// strAgentLevel = strAgentLevel & " - ENHANCED RE REG"
		// Case 6
		// strAgentLevel = strAgentLevel & " - EXCISE TAX"
		// End Select
		// strRedbookPermissions = ""
		// If rsExpire.GetData("MV_RBLVL_AUTO") = "Y" Then
		// strRedbookPermissions = strRedbookPermissions & "AU, "
		// End If
		// If rsExpire.GetData("MV_RBLVL_HVTK") = "Y" Then
		// strRedbookPermissions = strRedbookPermissions & "TK, "
		// End If
		// If rsExpire.GetData("MV_RBLVL_MTCY") = "Y" Then
		// strRedbookPermissions = strRedbookPermissions & "MC, "
		// End If
		// If rsExpire.GetData("MV_RBLVL_RCVH") = "Y" Then
		// strRedbookPermissions = strRedbookPermissions & "RV, "
		// End If
		// strRedbookPermissions = Left(strRedbookPermissions, Len(strRedbookPermissions) - 2)
		// Case 13             'TC
		// strExpire = rsExpire.GetData("TCDate")
		// Case 14
		// strExpire = rsExpire.GetData("E9Date")
		// End Select
		// End If
		//
		// lstPrograms.Clear
		// VB1's
		// lstPrograms.AddItem "   FILE               DATE & TIME             SIZE    "
		// lstPrograms.AddItem " "
		// filPrograms.Path = CurDir
		// filPrograms.Pattern = strVB1
		// For ii = 0 To filPrograms.ListCount - 1
		// strList = String(100, " ")
		// Set f = ff.GetFile(filPrograms.List(ii))
		// strFile = Trim$(UCase$(filPrograms.List(ii)))
		// dd = f.DateLastModified
		// Mid$(strList, 1, 18) = strFile & String(18 - Len(strFile), " ")
		// Mid$(strList, 20, 22) = dd & String(22 - Len(dd), " ")
		// Mid$(strList, 43, 10) = Format(Format(f.Size, "#,###"), "@@@@@@@@@@")
		// lstPrograms.AddItem strList
		// Next ii
		// lstPrograms.AddItem " "
		// lstPrograms.AddItem " "
		// lstPrograms.AddItem " "
		// lstPrograms.AddItem "  PROGRAM             DATE & TIME          SIZE  "
		// lstPrograms.AddItem " "
		// EXE's
		// filPrograms.Path = App.Path
		// filPrograms.Pattern = strEXE
		// For ii = 0 To filPrograms.ListCount - 1
		// Set f = ff.GetFile(filPrograms.Path & "\" & filPrograms.List(ii))
		// strList = String(100, " ")
		// strFile = Trim$(UCase$(filPrograms.List(ii)))
		// dd = f.DateLastModified
		// Mid$(strList, 1, 18) = strFile & String(18 - Len(strFile), " ")
		// Mid$(strList, 20, 22) = dd & String(22 - Len(dd), " ")
		// Mid$(strList, 43, 10) = Format(Format(f.Size, "#,###"), "@@@@@@@@@@")
		// lstPrograms.AddItem strList
		// Next ii
		//
		// lstPrograms.AddItem " "
		// lstPrograms.AddItem " "
		// lstPrograms.AddItem " "
		// If Index = 12 Then
		// lstPrograms.AddItem "  EXPIRATION DATE       BLUE BOOK     AGENT LEVEL"
		// ElseIf Index = 0 Then
		// lstPrograms.AddItem "  EXPIRATION DATES"
		// ElseIf Index = 1 Then
		// lstPrograms.AddItem "  EXPIRATION DATES      NUMBER OF ACCOUNTS"
		// Else
		//
		// lstPrograms.AddItem "  EXPIRATION DATE"
		// End If
		// lstPrograms.AddItem " "
		// If Index = 12 Then
		// lstPrograms.AddItem "      " & strExpire & "       " & Space(15 - Len(strRedbookPermissions)) & strRedbookPermissions & "     " & strAgentLevel
		// ElseIf Index = 0 Then
		// lstPrograms.AddItem "Real Estate     " & rsExpire.Fields("redate")
		// lstPrograms.AddItem "   Handheld     " & rsExpire.Fields("rhdate")
		// lstPrograms.AddItem "   Sketches     " & rsExpire.Fields("skDATE")
		// lstPrograms.AddItem "   Commercial   " & rsExpire.Fields("cmdate")
		// lstPrograms.AddItem "Personal Prop.  " & rsExpire.Fields("PPdate")
		// lstPrograms.AddItem "Billing         " & rsExpire.Fields("BLDate")
		// lstPrograms.AddItem "Collections     " & rsExpire.Fields("cldate")
		// lstPrograms.AddItem "Clerk           " & rsExpire.Fields("ckdate")
		// lstPrograms.AddItem "Voter Reg.      " & rsExpire.Fields("VRDate")
		// lstPrograms.AddItem "Code Enf.       " & rsExpire.Fields("cedate")
		// lstPrograms.AddItem "Budgetary       " & rsExpire.Fields("bddate")
		// lstPrograms.AddItem "Cash Receipting " & rsExpire.Fields("crdate")
		// lstPrograms.AddItem "PayRoll         " & rsExpire.Fields("pydate")
		// lstPrograms.AddItem "Utility Billing " & rsExpire.Fields("utdate")
		// lstPrograms.AddItem "Motor Vehicle   " & rsExpire.Fields("mvdate")
		// lstPrograms.AddItem "E911            " & rsExpire.Fields("e9date")
		// lstPrograms.AddItem "Fixed Assets    " & rsExpire.Fields("FAdate")
		// lstPrograms.AddItem "Accounts Rec.   " & rsExpire.Fields("ARdate")
		// ElseIf Index = 1 Then
		// Dim rsData As New clsDRWrapper
		// Call rsData.OpenRecordset("Select * from WRE", "Twgn0000.vb1")
		// If Not rsExpire.EndOfFile Then
		// lstPrograms.AddItem "      " & strExpire & "       " & Space(10) & rsExpire.Fields("RE_MaxAccounts")
		// Else
		// lstPrograms.AddItem "      " & strExpire & "       "
		// End If
		//
		// Else
		// lstPrograms.AddItem "    " & strExpire
		// End If
		//
		// Dim rs As DAO.Recordset
		// Dim db As DAO.Database
		//
		// On Error GoTo ErrorHandler
		// txtVersion.Visible = False
		// txtVersionDate.Visible = False
		// lblVersion.Visible = False
		// lblVersionDate.Visible = False
		// Frame1.Visible = False
		//
		// If ff.FileExists(strDatabase) = True Then
		// Set db = OpenDatabase(strDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		// Set rs = db.OpenRecordset("SELECT * FROM DatabaseVersion")
		//
		// txtVersion.Visible = True
		// txtVersionDate.Visible = True
		// lblVersion.Visible = True
		// lblVersionDate.Visible = True
		// Frame1.Visible = True
		//
		// txtVersion = rs.Fields("VersionNumber")
		// txtVersionDate = Format(rs.Fields("LastUpdated"), "MM/dd/yyyy")
		// End If
		//
		// ErrorHandler:
		// End Sub
		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
	}
}
