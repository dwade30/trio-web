﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using System;
using Wisej.Web;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBDAudit.
	/// </summary>
	public partial class srptBDAudit : FCSectionReport
	{
		public srptBDAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBDAudit";
		}

		public static srptBDAudit InstancePtr
		{
			get
			{
				return (srptBDAudit)Sys.GetInstance(typeof(srptBDAudit));
			}
		}

		protected srptBDAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBDAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int intReport;
		private const int MaxBDAuditReports = 7;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				//SetReport();
				eArgs.EOF = false;
			}
			else
			{
				intReport += 1;
				if (intReport > MaxBDAuditReports)
				{
					eArgs.EOF = true;
				}
				else
				{
					//SetReport();
					eArgs.EOF = false;
				}
			}
		}

		private void Detail_Format(object sender, System.EventArgs e)
		{
			SetReport();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			intReport = 0;
			modValidateAccount.SetBDFormats();
			frmBusy bWait = new frmBusy();
			FCUtils.StartTask(frmReportViewer.InstancePtr, () =>
			{
				bWait.Message = "Recalculating Account Information";
				bWait.StartBusy();
				modBudgetaryAccounting.CalculateAccountInfo();
				//Application.DoEvents();
				bWait.StopBusy();
				bWait.Unload();
				/*- bWait = null; */
				modBudgetaryAccounting.GetControlAccounts();
				FCUtils.UnlockUserInterface();
			});
			bWait.Show(FCForm.FormShowEnum.Modal);
		}

		private void SetReport()
		{
			switch (intReport)
			{
				case 0:
					{
						subBDReport.Report = new srptBD1();
						break;
					}
				case 1:
					{
						subBDReport.Report = new srptBD2();
						break;
					}
				case 2:
					{
						subBDReport.Report = new srptBD3();
						break;
					}
				case 3:
					{
						subBDReport.Report = new srptBD4();
						break;
					}
				case 4:
					{
						subBDReport.Report = new srptBD5();
						break;
					}
				case 5:
					{
						subBDReport.Report = new srptBD7();
						break;
					}
				case 6:
					{ 
						subBDReport.Report = new srptBD9();
						break;
					}
				case 7:
					{
						subBDReport.Report = new srptBD10();
						break;
					}
			}
			//end switch
		}

		
	}
}
