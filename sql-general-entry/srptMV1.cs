﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptMV1.
	/// </summary>
	public partial class srptMV1 : FCSectionReport
	{
		public srptMV1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptMV1";
		}

		public static srptMV1 InstancePtr
		{
			get
			{
				return (srptMV1)Sys.GetInstance(typeof(srptMV1));
			}
		}

		protected srptMV1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptMV1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: lngExpiredTotal As int	OnWrite(short, double)
		int lngExpiredTotal;
		// vbPorter upgrade warning: lngActiveTotal As int	OnWrite(short, double)
		int lngActiveTotal;
		// vbPorter upgrade warning: lngPendingTotal As int	OnWrite(short, double)
		int lngPendingTotal;
		// vbPorter upgrade warning: lngTerminatedTotal As int	OnWrite(short, double)
		int lngTerminatedTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT DISTINCT BMVCode FROM Class ORDER BY BMVCode", "TWMV0000.vb1");
			blnFirstRecord = true;
			lngExpiredTotal = 0;
			lngActiveTotal = 0;
			lngPendingTotal = 0;
			lngTerminatedTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsInfo.EndOfFile())
			{
                using (clsDRWrapper rsCountInfo = new clsDRWrapper())
                {
                    // TODO Get_Fields: Field [BMVCode] not found!! (maybe it is an alias?)
                    fldClass.Text = rsInfo.Get_Fields_String("BMVCode");
                    // TODO Get_Fields: Field [BMVCode] not found!! (maybe it is an alias?)
                    rsCountInfo.OpenRecordset(
                        "SELECT COUNT(Class) as ClassCount FROM Master WHERE Class = '" + rsInfo.Get_Fields("BMVCode") +
                        "' AND (Status = 'T' OR Inactive = 1)", "TWMV0000.vb1");
                    if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        fldTerminated.Text =
                            Strings.Format(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")), "#,##0");
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        lngTerminatedTotal += FCConvert.ToInt32(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")));
                    }
                    else
                    {
                        fldTerminated.Text = "0";
                    }

                    // TODO Get_Fields: Field [BMVCode] not found!! (maybe it is an alias?)
                    rsCountInfo.OpenRecordset(
                        "SELECT COUNT(Class) as ClassCount FROM Master WHERE Class = '" + rsInfo.Get_Fields("BMVCode") +
                        "' AND ((Status <> 'T' AND Inactive = 0) AND ExpireDate < '" +
                        DateTime.Today.ToShortDateString() + "')", "TWMV0000.vb1");
                    if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        //FC:FINAL:MSH - issue #1596: added use of missing property
                        fldExpired.Text = Strings.Format(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")), "#,##0");
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        lngExpiredTotal += FCConvert.ToInt32(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")));
                    }
                    else
                    {
                        fldExpired.Text = "0";
                    }

                    // TODO Get_Fields: Field [BMVCode] not found!! (maybe it is an alias?)
                    rsCountInfo.OpenRecordset(
                        "SELECT COUNT(Class) as ClassCount FROM Master WHERE Class = '" + rsInfo.Get_Fields("BMVCode") +
                        "' AND Status = 'P' AND ExpireDate >= '" + DateTime.Today.ToShortDateString() + "'",
                        "TWMV0000.vb1");
                    if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        fldPending.Text = Strings.Format(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")), "#,##0");
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        lngPendingTotal += FCConvert.ToInt32(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")));
                    }
                    else
                    {
                        fldPending.Text = "0";
                    }

                    // TODO Get_Fields: Field [BMVCode] not found!! (maybe it is an alias?)
                    rsCountInfo.OpenRecordset(
                        "SELECT COUNT(Class) as ClassCount FROM Master WHERE Class = '" + rsInfo.Get_Fields("BMVCode") +
                        "' AND Status = 'A' AND ExpireDate >= '" + DateTime.Today.ToShortDateString() + "'",
                        "TWMV0000.vb1");
                    if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        fldActive.Text = Strings.Format(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")), "#,##0");
                        // TODO Get_Fields: Field [ClassCount] not found!! (maybe it is an alias?)
                        lngActiveTotal += FCConvert.ToInt32(Conversion.Val(rsCountInfo.Get_Fields("ClassCount")));
                    }
                    else
                    {
                        fldActive.Text = "0";
                    }
                }
            }
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldActiveTotal.Text = Strings.Format(lngActiveTotal, "#,##0");
			fldPendingTotal.Text = Strings.Format(lngPendingTotal, "#,##0");
			fldTerminatedTotal.Text = Strings.Format(lngTerminatedTotal, "#,##0");
			fldExpiredTotal.Text = Strings.Format(lngExpiredTotal, "#,##0");
		}

	
	}
}
