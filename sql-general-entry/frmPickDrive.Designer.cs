﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPickDrive.
	/// </summary>
	partial class frmPickDrive : BaseForm
	{
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCLabel lblDrive;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdDone = new fecherFoundation.FCButton();
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.lblDrive = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 207);
			this.BottomPanel.Size = new System.Drawing.Size(443, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdDone);
			this.ClientArea.Controls.Add(this.Drive1);
			this.ClientArea.Controls.Add(this.lblDrive);
			this.ClientArea.Size = new System.Drawing.Size(443, 147);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(443, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(161, 30);
			this.HeaderText.Text = "Choose Drive";
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "actionButton";
			this.cmdDone.ForeColor = System.Drawing.Color.White;
			this.cmdDone.Location = new System.Drawing.Point(30, 70);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(88, 40);
			this.cmdDone.TabIndex = 2;
			this.cmdDone.Text = "Done";
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// Drive1
			// 
			this.Drive1.Location = new System.Drawing.Point(254, 30);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(172, 21);
			this.Drive1.TabIndex = 1;
			// 
			// lblDrive
			// 
			this.lblDrive.Location = new System.Drawing.Point(30, 30);
			this.lblDrive.Name = "lblDrive";
			this.lblDrive.Size = new System.Drawing.Size(191, 19);
			this.lblDrive.TabIndex = 0;
			// 
			// frmPickDrive
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(443, 315);
			this.Name = "frmPickDrive";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Choose Drive";
			this.Load += new System.EventHandler(this.frmPickDrive_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
