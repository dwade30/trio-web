﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	public class modSecurity2
	{
		//=========================================================
		public const int BACKUPRESTORE = 3;
		public const int PASSWORDSETUP = 2;
		public const int SYSTEMMAINT = 1;
		public const int REALESTATE = 4;
		public const int MOTORVEHICLE = 5;
		public const int PERSONALPROPERTY = 6;
		public const int BILLINGENTRY = 7;
		public const int UTILITYBILLING = 8;
		public const int COLLECTIONSENTRY = 9;
		public const int CLERKENTRY = 10;
		public const int VOTERREGISTRATION = 11;
		public const int CODEENFORCEMENT = 12;
		public const int BUDGETARYSYSTEM = 13;
		public const int CASHRECEIPTS = 14;
		public const int PAYROLLENTRY = 15;
		public const int TAXRATECALCENTRY = 16;
		public const int E911ENTRY = 17;
		public const int PPCOLLECTIONS = 18;
		public const int REDBOOK = 19;
		public const int FIXEDASSETENTRY = 20;
		public const int EDITOPERATORS = 21;
		public const int COMPACTREPAIR = 23;
		public const int UploadFiles = 25;
		public const int PROGRAMUPDATE = 26;
		public const int INTERNALAUDIT = 27;
		public const int CNSTEMAIL = 28;
		public const int CNSTCUSTOMIZE = 29;
		public const int CNSTSECURITYSIGNATURES = 30;
		public const int CNSTSECURITYPRINTERSETUP = 31;
		public const int CNSTCALENDAR = 34;
		public const int CNSTEDITCALENDAR = 35;
		public const int CNSTEDITCALENDARALL = 37;
		public const int CNSTVIEWCALENDAR = 36;
		public const int CNSTACCOUNTSRECEIVABLEENTRY = 38;
		public const int CNSTAUDITCHANGES = 40;
		public const int CNSTAUDITCHANGESARCHIVE = 41;
		public const int CNSTPURGEAUDITCHANGES = 42;

		public static bool ValidPermissions(object obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			bool ValidPermissions = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strPerm;
				string strFunc = "";
				// checks the permissions and returns false if there are no permissions
				// if there are partial permissions then it calls the function for the form
				// if false is passed in then partial permissions will not be handled by the function
				// This function is for MDI menu options.  At the time this is called the child
				// menu options wont be present, so you cant disable them yet
				strPerm = FCConvert.ToString(modGNBas.Statics.clsGESecurity.Check_Permissions(lngFuncID));

				switch (strPerm)
                {
                    case "F":
                        ValidPermissions = true;

                        break;
                    case "N":
                        ValidPermissions = false;

                        break;
                    case "P":
                    {
                        ValidPermissions = true;
                        if (boolHandlePartials)
                        {
                            FCUtils.CallByName(obForm, "HandlePartialPermission", CallType.Method, lngFuncID);
                        }

                        break;
                    }
                }
				return ValidPermissions;
				// 
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidPermissions;
		}
	}
}
