//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWGNENTY
{
	public class clsADOData
	{

		//=========================================================
		// Private pADORecordSet As New ADODB.Recordset
		// Private pADOConnection As New ADODB.Connection

		private FCRecordset pADORecordSet = null;
		// vbPorter upgrade warning: pADOConnection As DAO.Database	OnWrite(OleDbConnection)
		private DAO.Database pADOConnection;

		private bool boolSQLDatabase;
		private string gstrServerName = "";
		private string gstrDatabasePath = "";

		const string Provider = "Microsoft.Jet.OLEDB.4.0";
		const string SQLPROVIDER = "SQLOLEDB.1";

		public OleDbConnection OpenConnection(string strPath)
		{
			OleDbConnection OpenConnection = null;
			pADOConnection = new OleDbConnection;

			// If pADOConnection.State = adStateOpen Then pADOConnection.Close
			// If boolSQLDatabase Then
			// pADOConnection. SQLConnectionString(SQLPROVIDER, gstrServerName, strPath, False)
			// Else
			// pADOConnection.ConnectionString = ConnectionString(Provider, strPath, False)
			// End If
			pADOConnection = OpenDatabase(strPath, false, false, ";PWD="+modGlobalConstants.DATABASEPASSWORD);

			OpenConnection = pADOConnection;
			pADOConnection = null;
			return OpenConnection;
		}

		public OleDbConnection GetConnection(string strDatabase, CursorTypeEnum strCursor, LockTypeEnum strLockType)
		{
			OleDbConnection GetConnection = null;
			pADOConnection = OpenDatabase(strDatabase, false, false, ";PWD="+modGlobalConstants.DATABASEPASSWORD);

			GetConnection = pADOConnection;
			pADOConnection = null;
			return GetConnection;
		}

		public bool ActiveDatabaseType
		{
			set
			{
				boolSQLDatabase = true;
			}

			get
			{
					bool ActiveDatabaseType = false;
				ActiveDatabaseType = boolSQLDatabase;
				return ActiveDatabaseType;
			}
		}



		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public string GetConnectionString(string PathName)
		{
			string GetConnectionString = "";
			GetConnectionString = ConnectionString_18(Provider, PathName, false);
			return GetConnectionString;
		}

		public int RecordCount(ref object FieldName = null)
		{
			int RecordCount = 0;
			try
			{	// On Error GoTo ErrorHandler

				if (boolSQLDatabase) {
					int intCount = 0;
					string strValue = "";

					if (!Information.IsNothing(FieldName)) {
						strValue = Convert.ToString(pADORecordSet.Fields[FieldName].Value);
					}

					while (!pADORecordSet.EOF) {
						intCount += 1;
						pADORecordSet.MoveNext();
					}

					if (intCount>0) {
						pADORecordSet.MoveFirst();
						RecordCount = intCount;
					}

					if (!Information.IsNothing(FieldName)) {
						// Call pADORecordSet.Seek(FieldName & " = " & strValue)
					}
				} else {
					RecordCount = pADORecordSet.RecordCount;
				}

				return RecordCount;

			}
			catch
			{	// ErrorHandler:
				RecordCount = 0;
			}
			return RecordCount;
		}

		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public string GetData(string FieldName)
		{
			string GetData = "";
			GetData = Strings.Trim(Convert.ToString(pADORecordSet.Fields[FieldName].Value));
			if (fecherFoundation.FCUtils.IsNull(GetData)) GetData = string.Empty;
			return GetData;
		}

		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public string GetFirstRecord()
		{
			string GetFirstRecord = "";
			GetFirstRecord = Strings.Trim(Convert.ToString(pADORecordSet.Fields[0].Value));
			if (fecherFoundation.FCUtils.IsNull(GetFirstRecord)) GetFirstRecord = string.Empty;
			return GetFirstRecord;
		}

		public object SetData(string FieldName, object NewValue)
		{
			object SetData = null;
			pADORecordSet.Fields[FieldName].Value = NewValue;
			return SetData;
		}

		public void Refresh()
		{
			pADORecordSet.Requery();
		}

		public bool Update()
		{
			bool Update = false;
			try
			{	// On Error GoTo ErrorHandler

				pADORecordSet.Update(1, false);
				Update = true;
				return Update;

			}
			catch
			{	// ErrorHandler:
			}
			return Update;
		}

		public DataRowView Fields()
		{
			DataRowView Fields = 0;
			Fields = pADORecordSet.Fields;
			return Fields;
		}


		public bool AddNew()
		{
			bool AddNew = false;
			pADORecordSet.AddNew();
			return AddNew;
		}

		public bool Edit()
		{
			bool Edit = false;
			pADORecordSet.Edit();
			return Edit;
		}

		public bool MoveNext()
		{
			bool MoveNext = false;
			pADORecordSet.MoveNext();
			return MoveNext;
		}

		public bool MovePrevious()
		{
			bool MovePrevious = false;
			pADORecordSet.MovePrevious();
			return MovePrevious;
		}

		public bool MoveLast()
		{
			bool MoveLast = false;
			pADORecordSet.MoveLast(0);
			return MoveLast;
		}

		public bool MoveFirst()
		{
			bool MoveFirst = false;
			pADORecordSet.MoveFirst();
			return MoveFirst;
		}

		public bool BeginingOfFile()
		{
			bool BeginingOfFile = false;
			BeginingOfFile = pADORecordSet.BOF;
			return BeginingOfFile;
		}

		public bool EndOfFile()
		{
			bool EndOfFile = false;
			EndOfFile = pADORecordSet.EOF;
			return EndOfFile;
		}

		public bool OpenRecordset(object strsql, string strDatabase, CursorTypeEnum strCursor, LockTypeEnum strLockType, ref CursorLocationEnum strCursorLocation = CursorLocationEnum.adUseServer)
		{
			bool OpenRecordset = false;
			try
			{	// On Error GoTo ErrorHandler

				// Set pADORecordSet = New ADODB.Recordset
				// Set pADOConnection = New ADODB.Connection

				// If pADOConnection.State = adStateOpen Then pADOConnection.Close
				// If boolSQLDatabase Then
				// pADOConnection.ConnectionString = SQLConnectionString(SQLPROVIDER, gstrServerName, strDatabase, False)
				// Else
				// pADOConnection.ConnectionString = ConnectionString(Provider, strDatabase, False)
				// End If

				pADOConnection = OpenDatabase(strDatabase, false, false, ";PWD="+modGlobalConstants.DATABASEPASSWORD);

				// If pADOConnection.State <> adStateOpen Then
				// MsgBox "Invalid database", vbCritical + vbOKOnly, "Open Database"
				// Else
				// pADORecordSet.CursorLocation = strCursorLocation
				pADORecordSet = pADOConnection.OpenRecordset(strsql, Type.Missing, Type.Missing, Type.Missing); // , pADOConnection, strCursor, strLockType)

				// if not padorecordset.EOF then
				// If pADORecordSet.State <> adStateOpen Then
				// MsgBox "Recordset failed to open.", vbCritical + vbOKOnly, "Open Recordset"
				// Else
				OpenRecordset = true;
				// End If
				// End If
				return OpenRecordset;

			}
			catch (Exception ex)
			{	// ErrorHandler:
				MessageBox.Show(Information.Err(ex).Description);

				// If Err.Number < -2000000000 Then
				// MsgBox "User does not have database permissions for this operation."
				// End If
			}
			return OpenRecordset;
		}

		public bool OpenSPRecordset(object StoredProcedure, string strDatabase, CursorTypeEnum strCursor, LockTypeEnum strLockType)
		{
			bool OpenSPRecordset = false;
			// Set pADORecordSet = New ADODB.Recordset
			// Set pADOConnection = New ADODB.Connection
			// 
			// If pADOConnection.State = adStateOpen Then pADOConnection.Close
			// If boolSQLDatabase Then
			// pADOConnection.ConnectionString = SQLConnectionString(SQLPROVIDER, gstrServerName, strDatabase, False)
			// Else
			// pADOConnection.ConnectionString = ConnectionString(Provider, strDatabase, False)
			// End If
			// 
			// pADOConnection.Open
			// 
			// If pADOConnection.State <> adStateOpen Then
			// MsgBox "Invalid database", vbCritical + vbOKOnly, "Open Database"
			// Else
			// Set pADORecordSet = pADOConnection.Execute(StoredProcedure)
			// If pADORecordSet.State <> adStateOpen Then
			// MsgBox "Recordset failed to open.", vbCritical + vbOKOnly, "Open Recordset"
			// Else
			// OpenSPRecordset = True
			// End If
			// End If
			return OpenSPRecordset;
		}


		public void FindRecord(string FieldName, string NewValue)
		{
			// If Not pADORecordSet.BOF Then pADORecordSet.MoveFirst
			// Call pADORecordSet.Find(FieldName & " = " & NewValue, , adSearchForward)
		}

		public string ConnectionString_18(string strProvider, string strDBName, bool boolPersistSecurity) { return ConnectionString(strProvider, strDBName, ref boolPersistSecurity); }
		public string ConnectionString(string strProvider, string strDBName, ref bool boolPersistSecurity)
		{
			string ConnectionString = "";
			string strTemp;
			strTemp = "Provider="+strProvider;
			strTemp += ";Persist Security Info="+boolPersistSecurity.ToString();
			strTemp += ";Data Source ="+strDBName;
			strTemp += ";User ID=sa;password="+modGlobalConstants.DATABASEPASSWORD;
			ConnectionString = strTemp;
			return ConnectionString;
		}


		public string SQLConnectionString(string strProvider, string strSQLServerName, string strDatabaseName, ref bool boolPersistSecurity)
		{
			string SQLConnectionString = "";
			string strTemp;
			strTemp = "Provider="+strProvider;
			// strTemp = strTemp & ";Persist Security Info=" & CStr(boolPersistSecurity)
			strTemp += ";UID=sa;";
			strTemp += ";Password=Pass911;";
			strTemp += ";Initial Catalog="+strDatabaseName;
			strTemp += ";Data Source="+strSQLServerName;

			// strTemp = "Driver={SQL Server}"
			// strTemp = strTemp & ";Server=" & strSQLServerName
			// strTemp = strTemp & ";UID=sa"
			// strTemp = strTemp & ";Database=" & strDatabaseName

			SQLConnectionString = strTemp;
			return SQLConnectionString;
		}

		public bool DisconnectDatabase()
		{
			bool DisconnectDatabase = false;
			pADOConnection.Close();
			return DisconnectDatabase;
		}


		public bool ExecuteDBaseCommand(string strsql)
		{
			bool ExecuteDBaseCommand = false;
			// Set pADOConnection = New ADODB.Connection
			// 
			// If pADOConnection.State = adStateOpen Then pADOConnection.Close
			// pADOConnection.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data Source= dbase files"
			// pADOConnection.Open
			// 
			// Call pADOConnection.Execute(strsql)
			return ExecuteDBaseCommand;
		}


		public bool ExecuteSQL(string strsql, string strDatabaseName)
		{
			bool ExecuteSQL = false;
			// Set pADOConnection = New ADODB.Connection
			// 
			// If pADOConnection.State = adStateOpen Then pADOConnection.Close
			// pADOConnection.ConnectionString = SQLConnectionString(SQLPROVIDER, gstrServerName, strDatabaseName, False)
			// pADOConnection.Open
			// 
			// Call pADOConnection.Execute(strsql)
			return ExecuteSQL;
		}

		public bool Execute(string strsql)
		{
			bool Execute = false;
			try
			{	// On Error GoTo ErrorHandler
				// 
				// Set pADOConnection = New ADODB.Connection
				// 
				// If pADOConnection.State = adStateOpen Then pADOConnection.Close
				// If boolSQLDatabase Then
				// pADOConnection.ConnectionString = SQLConnectionString(SQLPROVIDER, gstrServerName, gstrDatabasePath, False)
				// Else
				// pADOConnection.ConnectionString = ConnectionString(Provider, gstrDatabasePath, False)
				// End If

				pADOConnection = OpenDatabase(gstrDatabasePath, false, false, ";PWD="+modGlobalConstants.DATABASEPASSWORD);

				pADOConnection.Execute(strsql);
				Execute = true;
				return Execute;

			}
			catch
			{	// ErrorHandler:
			}
			return Execute;
		}

		public clsADOData() : base()
		{
			pADOConnection = null;
			pADORecordSet = null;
		}

	}
}