﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPrintTestType.
	/// </summary>
	public partial class frmPrintTestType : BaseForm
	{
		public frmPrintTestType()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintTestType InstancePtr
		{
			get
			{
				return (frmPrintTestType)Sys.GetInstance(typeof(frmPrintTestType));
			}
		}

		protected frmPrintTestType _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			if (cboType.SelectedIndex < 0)
				return;
            switch (cboType.SelectedIndex)
            {
                case 0:
                {
                    modGNBas.Statics.gstrPrintType = "PRINTER";
                    break;
                }
                case 1:
                {
                    modGNBas.Statics.gstrPrintType = "ARPREVIEW";
                    break;
                }
                case 2:
                {
                    modGNBas.Statics.gstrPrintType = "ARMVR3";
                    break;
                }
                case 3:
                {
                    modGNBas.Statics.gstrPrintType = "FSO";
                    break;
                }
                case 4:
                {
                    modGNBas.Statics.gstrPrintType = "OPEN";
                    break;
                }
                case 5:
                {
                    modGNBas.Statics.gstrPrintType = "VERT";
                    break;
                }
                case 6:
                {
                    modGNBas.Statics.gstrPrintType = "PRINTFONT";
                    break;
                }
                case 7:
                {
                    modGNBas.Statics.gstrPrintType = "DIRECTPRINT";
                    break;
                }
            }
			//end switch
			this.Unload();
		}

		private void frmPrintTestType_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintTestType properties;
			//frmPrintTestType.ScaleWidth	= 2895;
			//frmPrintTestType.ScaleHeight	= 1575;
			//frmPrintTestType.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			SetupCombo();
			cboType.SelectedIndex = 0;
		}

		private void SetupCombo()
		{
			cboType.Clear();
			cboType.AddItem("Printer Object");
			cboType.AddItem("ActiveReport (Preview)");
			cboType.AddItem("ActiveReport");
			cboType.AddItem("File System Object (FSO)");
			cboType.AddItem("Open File Object (Open #1)");
			cboType.AddItem("Vertical Alignment Page");
			cboType.AddItem("Printer Font");
            cboType.AddItem("Direct Printing");
        }
	}
}
