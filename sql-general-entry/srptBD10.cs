﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD10.
	/// </summary>
	public partial class srptBD10 : FCSectionReport
	{
		public srptBD10()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD10";
		}

		public static srptBD10 InstancePtr
		{
			get
			{
				return (srptBD10)Sys.GetInstance(typeof(srptBD10));
			}
		}

		protected srptBD10 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD10	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intType;
		// vbPorter upgrade warning: curIssuedTotal As Decimal	OnWrite(short, Decimal)
		Decimal curIssuedTotal;
		// vbPorter upgrade warning: curOutstandingTotal As Decimal	OnWrite(short, Decimal)
		Decimal curOutstandingTotal;
		// vbPorter upgrade warning: curCashedTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCashedTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intType += 1;
				if (intType <= 7)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			intType = 1;
			curIssuedTotal = 0;
			curOutstandingTotal = 0;
			curCashedTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldType.Text = GetTypeDescription();
			rsInfo.OpenRecordset("SELECT SUM(Amount) AS TotalAmount FROM CheckRecMaster WHERE Type = '" + FCConvert.ToString(intType) + "' AND Status = '1'", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				fldIssued.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields_Decimal("TotalAmount")), "#,##0.00");
				curIssuedTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldIssued.Text = Strings.Format("0", "#,##0");
			}
			rsInfo.OpenRecordset("SELECT SUM(Amount) AS TotalAmount FROM CheckRecMaster WHERE Type = '" + FCConvert.ToString(intType) + "' AND Status = '2'", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				fldOutstanding.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				curOutstandingTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldOutstanding.Text = Strings.Format("0", "#,##0");
			}
			rsInfo.OpenRecordset("SELECT SUM(Amount) AS TotalAmount FROM CheckRecMaster WHERE Type = '" + FCConvert.ToString(intType) + "' AND Status = '3'", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				fldCashed.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields_Decimal("TotalAmount")), "#,##0.00");
				curCashedTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldCashed.Text = Strings.Format("0", "#,##0.00");
			}
			fldTotal.Text = Strings.Format(Conversion.CLng(fldIssued.Text) + Conversion.CLng(fldOutstanding.Text) + Conversion.CLng(fldCashed.Text), "#,##0.00");
		}

		private string GetTypeDescription()
		{
			string GetTypeDescription = "";
			switch (intType)
			{
				case 1:
					{
						GetTypeDescription = "1 - Accounts Payable";
						break;
					}
				case 2:
					{
						GetTypeDescription = "2 - Payroll";
						break;
					}
				case 3:
					{
						GetTypeDescription = "3 - Deposit";
						break;
					}
				case 4:
					{
						GetTypeDescription = "4 - Returned Check";
						break;
					}
				case 5:
					{
						GetTypeDescription = "5 - Interest";
						break;
					}
				case 6:
					{
						GetTypeDescription = "6 - Other Credits";
						break;
					}
				case 7:
					{
						GetTypeDescription = "7 - Other Debits";
						break;
					}
			}
			//end switch
			return GetTypeDescription;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldIssuedTotal.Text = Strings.Format(curIssuedTotal, "#,##0.00");
			fldOutstandingTotal.Text = Strings.Format(curOutstandingTotal, "#,##0.00");
			fldCashedTotal.Text = Strings.Format(curCashedTotal, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(curIssuedTotal + curOutstandingTotal + curCashedTotal, "#,##0.00");
		}

		
	}
}
