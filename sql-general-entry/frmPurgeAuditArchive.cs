﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPurgeAuditArchive.
	/// </summary>
	public partial class frmPurgeAuditArchive : BaseForm
	{
		public frmPurgeAuditArchive()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgeAuditArchive InstancePtr
		{
			get
			{
				return (frmPurgeAuditArchive)Sys.GetInstance(typeof(frmPurgeAuditArchive));
			}
		}

		protected frmPurgeAuditArchive _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/11/2001
		// This form will be used to purge any outdated information
		// the town wants to get rid of to decrease the size of their
		// database
		// ********************************************************
		clsDRWrapper rsPurge = new clsDRWrapper();

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			int totalCount = 0;
			DateTime TempDate;
			// vbPorter upgrade warning: Ans As short, int --> As DialogResult
			DialogResult Ans;
			if (!Information.IsDate(txtDate.Text))
			{
				MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (DateAndTime.DateValue(txtDate.Text).ToOADate() > DateAndTime.DateValue(Strings.Format(DateAndTime.DateAdd("yyyy", -1, DateTime.Today), "MM/dd/yyyy")).ToOADate())
			{
				MessageBox.Show("You must enter a date that is at least 1 year in the past.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			TempDate = DateAndTime.DateValue(txtDate.Text);
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			rsPurge.OpenRecordset("SELECT * FROM AuditChangesArchive WHERE DateUpdated <= '" + FCConvert.ToString(TempDate) + "'", "TWMV0000.vb1");
			if (rsPurge.EndOfFile() != true && rsPurge.BeginningOfFile() != true)
			{
				rsPurge.MoveLast();
				rsPurge.MoveFirst();
				totalCount = rsPurge.RecordCount();
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				Ans = MessageBox.Show(FCConvert.ToString(totalCount) + " records will be deleted.  Do you wish to continue?", "Delete Records?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (Ans == DialogResult.Yes)
				{
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Deleting Records";
					frmWait.InstancePtr.Refresh();
					rsPurge.Execute("DELETE FROM AuditChangesArchive WHERE DateUpdated <= '" + FCConvert.ToString(TempDate) + "'", "TWMV0000.vb1");
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					MessageBox.Show("Purge Complete!!  " + FCConvert.ToString(totalCount) + " records deleted", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					return;
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No records found that were saved before that date.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			this.Unload();
		}

		private void frmPurgeAuditArchive_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPurgeAuditArchive_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeAuditArchive properties;
			//frmPurgeAuditArchive.FillStyle	= 0;
			//frmPurgeAuditArchive.ScaleWidth	= 5880;
			//frmPurgeAuditArchive.ScaleHeight	= 4050;
			//frmPurgeAuditArchive.LinkTopic	= "Form2";
			//frmPurgeAuditArchive.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeAuditArchive_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmPurgeAuditArchive_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}
	}
}
