﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptAuditReport.
	/// </summary>
	public partial class rptAuditReport : BaseSectionReport
	{
		public rptAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Archive Report";
		}

		public static rptAuditReport InstancePtr
		{
			get
			{
				return (rptAuditReport)Sys.GetInstance(typeof(rptAuditReport));
			}
		}

		protected rptAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsData.MoveNext();
				eArgs.EOF = rsData.EndOfFile();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			clsDRWrapper rsAuditInfo = new clsDRWrapper();

            try
            {
                rsAuditInfo.OpenRecordset("SELECT * FROM AuditChanges", modGNBas.DEFAULTDATABASE);
                rsArchiveInfo.OpenRecordset("SELECT * FROM AuditChangesArchive", modGNBas.DEFAULTDATABASE);
                if (rsAuditInfo.EndOfFile() != true && rsAuditInfo.BeginningOfFile() != true)
                {
                    do
                    {
                        rsArchiveInfo.AddNew();
                        rsArchiveInfo.Set_Fields("Location", rsAuditInfo.Get_Fields_String("Location"));
                        rsArchiveInfo.Set_Fields("ChangeDescription", rsAuditInfo.Get_Fields_String("ChangeDescription"));
                        rsArchiveInfo.Set_Fields("UserField1", rsAuditInfo.Get_Fields_String("UserField1"));
                        rsArchiveInfo.Set_Fields("UserField2", rsAuditInfo.Get_Fields_String("UserField2"));
                        rsArchiveInfo.Set_Fields("UserField3", rsAuditInfo.Get_Fields_String("UserField3"));
                        rsArchiveInfo.Set_Fields("UserField4", rsAuditInfo.Get_Fields_String("UserField4"));
                        // TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
                        rsArchiveInfo.Set_Fields("UserID", rsAuditInfo.Get_Fields("UserID"));
                        rsArchiveInfo.Set_Fields("DateUpdated", rsAuditInfo.Get_Fields_DateTime("DateUpdated"));
                        rsArchiveInfo.Set_Fields("TimeUpdated", rsAuditInfo.Get_Fields_DateTime("TimeUpdated"));
                        rsArchiveInfo.Update();
                        rsAuditInfo.Delete();
                        rsAuditInfo.Update();
                        rsAuditInfo.MoveNext();
                    }
                    while (rsAuditInfo.EndOfFile() != true);
                }
            }
            finally
            {
                rsArchiveInfo.DisposeOf();
                rsAuditInfo.DisposeOf();
                rsData.DisposeOf();
                rsData = new clsDRWrapper();
            }
			MessageBox.Show("Audit information archived.", "Audit Info Archived", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			try
			{
				// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
				// MUNINAME, DATE AND TIME
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
				//txtMuniName = gstrMuniName;
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				blnFirstRecord = true;
				rsData.OpenRecordset("Select * from AuditChanges ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1, UserField2", modGNBas.DEFAULTDATABASE);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Cancel();
				}
			}
			catch
			{
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//object fldPartyID, fldName;	// - "AutoDim"
			fldPartyID.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField1"));
			fldName.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField2"));
			// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
			txtUser.Text = FCConvert.ToString(rsData.Get_Fields("UserID"));
			txtDateTimeField.Text = Strings.Format(rsData.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy") + " " + Strings.Format(rsData.Get_Fields_DateTime("TimeUpdated"), "hh:mm tt");
			fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
			fldDescription.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As Variant --> As string
			//string txtPage; // - "AutoDim"
			txtPage.Text = "Page " + this.PageNumber;
		}


		private void rptAuditReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
