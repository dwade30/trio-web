﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWGNENTY
{
	public class modGNWork
	{
		//=========================================================
		public struct SECRECORD
		{
			public char[] VERSION;
			public char[] MUNI;
			public short RSYEAR;
			// 4
			public short RSMONTH;
			// 2
			public char[] PASSWORD;
			public char[] OVERRIDE;
			public short CTLD;
			// 4
			public short USECOUNT;
			// 6
			public short CTLM;
			// 4
			public int MAXACCT;
			// 6
			public char[] SDRIVE;
			public short SCOLOR;
			public char[] SPRODSUM;
			public char[] SRYEAR;
			public char[] SRMONTH;
			public char[] CMYEAR;
			public char[] CMMONTH;
			public char[] PPYEAR;
			public char[] PPMONTH;
			public char[] BLYEAR;
			public char[] BLMONTH;
			public char[] CLYEAR;
			public char[] CLMONTH;
			public char[] CEYEAR;
			public char[] CEMONTH;
			public char[] BDYEAR;
			public char[] BDMONTH;
			public char[] CRYEAR;
			public char[] CRMONTH;
			public char[] PYYEAR;
			public char[] PYMONTH;
			public char[] UTYEAR;
			public char[] UTMONTH;
			public char[] MVYEAR;
			public char[] MVMONTH;
			public char[] TCYEAR;
			public char[] TCMONTH;
			public char[] E9YEAR;
			public char[] E9MONTH;
			public char[] CKYEAR;
			public char[] CKMONTH;
			public char[] NETWORKFLAG;
			public char[] NETWORKFLAG2;
			public char[] VALDATE;
			public short VALCOUNT;
			// 2
			public char[] MAXPP;
			public char[] VRYEAR;
			public char[] VRMONTH;
			public char[] VRMAX;
			public char[] CEMAX;
			public char[] PYMAX;
			public char[] UTMAX;
			public char[] MVMAX;
			public char[] RBMAX;
			public char[] EXPANSION;
		};

		public class StaticVariables
		{
			public SECRECORD SEC = new SECRECORD();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
