﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmProcessFixProgram.
	/// </summary>
	partial class frmProcessFixProgram : BaseForm
	{
		public fecherFoundation.FCListBox lstMessages;
		public fecherFoundation.FCButton cmdDownload;
		//public Chilkat.Ftp2 FTP1;
        public Chilkat.SFtp FTP1;
		public fecherFoundation.FCGrid vsList;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public fecherFoundation.FCGrid vsUpdates;
		public fecherFoundation.FCGrid vsSupport;
		public Chilkat.Ftp2 ChilkatFtp1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lstMessages = new fecherFoundation.FCListBox();
            this.cmdDownload = new fecherFoundation.FCButton();
            this.vsList = new fecherFoundation.FCGrid();
            this.ProgressBar1 = new fecherFoundation.FCProgressBar();
            this.vsUpdates = new fecherFoundation.FCGrid();
            this.vsSupport = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblPercent = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDownload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsUpdates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSupport)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 437);
            this.BottomPanel.Size = new System.Drawing.Size(716, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lstMessages);
            this.ClientArea.Controls.Add(this.cmdDownload);
            this.ClientArea.Controls.Add(this.vsList);
            this.ClientArea.Controls.Add(this.ProgressBar1);
            this.ClientArea.Controls.Add(this.vsUpdates);
            this.ClientArea.Controls.Add(this.vsSupport);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblPercent);
            this.ClientArea.Size = new System.Drawing.Size(736, 568);
            this.ClientArea.Controls.SetChildIndex(this.lblPercent, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsSupport, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsUpdates, 0);
            this.ClientArea.Controls.SetChildIndex(this.ProgressBar1, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsList, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdDownload, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstMessages, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(736, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(227, 28);
            this.HeaderText.Text = "Process Fix Program";
            // 
            // lstMessages
            // 
            this.lstMessages.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.lstMessages.Location = new System.Drawing.Point(30, 259);
            this.lstMessages.Name = "lstMessages";
            this.lstMessages.Size = new System.Drawing.Size(681, 178);
            this.lstMessages.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.lstMessages, "FTP Status Messages");
            this.lstMessages.Visible = false;
            // 
            // cmdDownload
            // 
            this.cmdDownload.AppearanceKey = "actionButton";
            this.cmdDownload.ForeColor = System.Drawing.Color.White;
            this.cmdDownload.Location = new System.Drawing.Point(30, 185);
            this.cmdDownload.Name = "cmdDownload";
            this.cmdDownload.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdDownload.Size = new System.Drawing.Size(120, 40);
            this.cmdDownload.TabIndex = 5;
            this.cmdDownload.Text = "Process Fix";
            this.cmdDownload.Click += new System.EventHandler(this.cmdDownload_Click);
            // 
            // vsList
            // 
            this.vsList.Cols = 1;
            this.vsList.ColumnHeadersVisible = false;
            this.vsList.FixedCols = 0;
            this.vsList.FixedRows = 0;
            this.vsList.Location = new System.Drawing.Point(30, 177);
            this.vsList.Name = "vsList";
            this.vsList.RowHeadersVisible = false;
            this.vsList.Rows = 0;
            this.vsList.ShowFocusCell = false;
            this.vsList.Size = new System.Drawing.Size(43, 69);
            this.vsList.TabIndex = 4;
            this.vsList.Visible = false;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(275, 226);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(436, 27);
            this.ProgressBar1.Step = 1;
            this.ProgressBar1.TabIndex = 7;
            // 
            // vsUpdates
            // 
            this.vsUpdates.Cols = 3;
            this.vsUpdates.ColumnHeadersVisible = false;
            this.vsUpdates.ExtendLastCol = true;
            this.vsUpdates.FixedCols = 0;
            this.vsUpdates.FixedRows = 0;
            this.vsUpdates.Location = new System.Drawing.Point(30, 60);
            this.vsUpdates.Name = "vsUpdates";
            this.vsUpdates.RowHeadersVisible = false;
            this.vsUpdates.Rows = 0;
            this.vsUpdates.ShowFocusCell = false;
            this.vsUpdates.Size = new System.Drawing.Size(311, 111);
            this.vsUpdates.TabIndex = 1;
            this.vsUpdates.Click += new System.EventHandler(this.vsUpdates_ClickEvent);
            this.vsUpdates.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsUpdates_KeyPressEvent);
            // 
            // vsSupport
            // 
            this.vsSupport.Cols = 3;
            this.vsSupport.ColumnHeadersVisible = false;
            this.vsSupport.ExtendLastCol = true;
            this.vsSupport.FixedCols = 0;
            this.vsSupport.FixedRows = 0;
            this.vsSupport.Location = new System.Drawing.Point(368, 60);
            this.vsSupport.Name = "vsSupport";
            this.vsSupport.RowHeadersVisible = false;
            this.vsSupport.Rows = 0;
            this.vsSupport.ShowFocusCell = false;
            this.vsSupport.Size = new System.Drawing.Size(343, 111);
            this.vsSupport.TabIndex = 3;
            this.vsSupport.Click += new System.EventHandler(this.vsSupport_ClickEvent);
            this.vsSupport.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsSupport_KeyPressEvent);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(478, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(110, 16);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "SUPPORT FILES";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(125, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(110, 16);
            this.Label1.TabIndex = 1001;
            this.Label1.Text = "FIX PROGRAMS";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPercent
            // 
            this.lblPercent.Location = new System.Drawing.Point(30, 237);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(218, 16);
            this.lblPercent.TabIndex = 6;
            this.lblPercent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmProcessFixProgram
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(736, 628);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmProcessFixProgram";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Process Fix Program";
            this.Load += new System.EventHandler(this.frmProcessFixProgram_Load);
            this.Activated += new System.EventHandler(this.frmProcessFixProgram_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProcessFixProgram_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDownload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsUpdates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSupport)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
