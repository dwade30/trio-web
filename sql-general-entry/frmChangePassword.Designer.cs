﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmChangePassword.
	/// </summary>
	partial class frmChangePassword : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCTextBox txtCheck;
		public fecherFoundation.FCTextBox txtPassword1;
		public fecherFoundation.FCTextBox txtPassword2;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtCheck = new fecherFoundation.FCTextBox();
			this.txtPassword1 = new fecherFoundation.FCTextBox();
			this.txtPassword2 = new fecherFoundation.FCTextBox();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 311);
			this.BottomPanel.Size = new System.Drawing.Size(397, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtCheck);
			this.ClientArea.Controls.Add(this.txtPassword1);
			this.ClientArea.Controls.Add(this.txtPassword2);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2_1);
			this.ClientArea.Controls.Add(this.Label2_0);
			this.ClientArea.Size = new System.Drawing.Size(397, 251);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(397, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "TRIO Password";
			// 
			// txtCheck
			// 
			this.txtCheck.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheck.Location = new System.Drawing.Point(326, 190);
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Size = new System.Drawing.Size(21, 40);
			this.txtCheck.TabIndex = 7;
			this.txtCheck.Visible = false;
			// 
			// txtPassword1
			// 
			this.txtPassword1.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword1.Location = new System.Drawing.Point(221, 70);
			this.txtPassword1.Name = "txtPassword1";
			this.txtPassword1.Size = new System.Drawing.Size(143, 40);
			this.txtPassword1.TabIndex = 2;
			// 
			// txtPassword2
			// 
			this.txtPassword2.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword2.Location = new System.Drawing.Point(221, 130);
			this.txtPassword2.Name = "txtPassword2";
			this.txtPassword2.Size = new System.Drawing.Size(143, 40);
			this.txtPassword2.TabIndex = 4;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(30, 190);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(83, 40);
			this.cmdOK.TabIndex = 5;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(131, 190);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(88, 40);
			this.cmdCancel.TabIndex = 6;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(268, 17);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "YOU MUST TYPE YOUR PASSWORD TWICE";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(30, 84);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(106, 19);
			this.Label2_1.TabIndex = 1;
			this.Label2_1.Text = "PASSWORD";
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(30, 144);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(147, 19);
			this.Label2_0.TabIndex = 3;
			this.Label2_0.Text = "CONFIRM PASSWORD";
            // 
            // frmChangePassword
            // 
            this.AcceptButton = this.cmdOK;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(397, 419);
			this.FillColor = 0;
			this.Name = "frmChangePassword";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "TRIO Password";
			this.Load += new System.EventHandler(this.frmChangePassword_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
