﻿//Fecher vbPorter - Version 1.0.0.40
using Wisej.Web;
using fecherFoundation;
using Global;
using fecherFoundation.Extensions;
using System;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmOptWebFile.
	/// </summary>
	public partial class frmOptWebFile : BaseForm
	{
		public frmOptWebFile()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int intReturn;
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Init(ref string strMessage, ref string strCaption)
		{
			short Init = 0;
			this.Text = strCaption;
			Label1.Text = strMessage;
			intReturn = -1;
			Init = -1;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = FCConvert.ToInt16(intReturn);
			return Init;
		}

		private void frmOptWebFile_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmOptWebFile_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOptWebFile properties;
			//frmOptWebFile.FillStyle	= 0;
			//frmOptWebFile.ScaleWidth	= 3885;
			//frmOptWebFile.ScaleHeight	= 2265;
			//frmOptWebFile.LinkTopic	= "Form2";
			//frmOptWebFile.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbWhere.SelectedIndex == 0)
			{
				intReturn = 0;
			}
			else if (cmbWhere.SelectedIndex == 1)
			{
				intReturn = 1;
			}
			else
			{
				intReturn = 2;
			}
			this.Unload();
		}
	}
}
