﻿//Fecher vbPorter - Version 1.0.0.40
using Wisej.Web;
using fecherFoundation;
using System;

namespace TWGNENTY
{
	public class clsAlphaTextBox
	{
		//=========================================================
		// All of a forms textboxes should be of this type. That
		// way the code for any textbox event can be
		// located in one central place (versus the same
		// code in every GotFocus, ... event).
		//
		// By using the "WithEvents" keyword, all events that occur
		// within the textboxes are passed to this class where
		// they can be handled or not.
		public FCTextBox TextBox;
		public Wisej.Web.MaskedTextBox MaskBox = new Wisej.Web.MaskedTextBox();
		public FCComboBox cboComboBox;
		public FCCheckBox chkCheckBox;
		public FCFrame Frame;
		public FCLabel Label;
		public FCButton CommandButton;

		public delegate void ControlHasChangedEventHandler();

		public event ControlHasChangedEventHandler ControlHasChanged;
		// vbPorter upgrade warning: pFormName As Form	OnWrite(object)
		private Form pFormName = null;
		private bool pboolUseRoutine;

		public void Visible(ref bool State)
		{
			Label.Visible = State;
		}

		private void cboComboBox_Click()
		{
			TextBox_Change();
		}

		private void cboComboBox_KeyPress(ref short KeyAscii)
		{
			// this line will allow me to use the code in the form and not this class
			// If Not pboolUseRoutine Then Exit Sub
			// 
			// If ComboTextFind(cboComboBox, KeyAscii) Then
			// KeyAscii = 0
			// End If
			// 
			// KeyAscii = 0
		}

		private void chkCheckBox_Click()
		{
			TextBox_Change();
		}

		public clsAlphaTextBox() : base()
		{
			pFormName = modGNBas.Statics.gobjFormName;
			pboolUseRoutine = true;
		}

		~clsAlphaTextBox()
		{
			TextBox = null;
			MaskBox = null;
			cboComboBox = null;
			chkCheckBox = null;
			Frame = null;
			Label = null;
			CommandButton = null;
			pFormName = null;
		}

		private void Label_MouseMove(ref short Button, ref short Shift, ref float X, ref float Y)
		{
			/*? On Error Resume Next  */
			try
			{
				/* FCLabel lbl; *///foreach (Control lbl in MDIParent.InstancePtr.Controls) {
				//	lbl.BorderStyle = BorderStyle.None;
				//	lbl.Appearance = 1;
				//}
				// gobjLabel.BorderStyle = 0
				// gobjLabel.Appearance = 1
				if ((Label.BorderStyle == FCConvert.ToInt32(BorderStyle.None) ? 0 : 1) == 0)
				{
					Label.BorderStyle = 1;
					Label.Appearance = 0;
					modGNBas.Statics.gobjLabel = Label;
				}
			}
			catch
			{
			}
		}

		private void MaskBox_Change()
		{
			TextBox_Change();
		}

		private void TextBox_Change()
		{
			// Echo back what the user entered. Since the textbox is an
			// object, we must reference the appropriate properties.
			// this line will allow me to use the code in the form and not this class
			if (!pboolUseRoutine)
				return;
			/*? On Error Resume Next  */
			try
			{
				FCUtils.CallByName(App.MainForm, "ControlChanged", CallType.Method);
			}
			catch
			{
			}
		}

		private void TextBox_GotFocus()
		{
			// Highlight the text.
			TextBox.SelectionStart = 0;
			TextBox.SelectionLength = TextBox.Text.Length;
		}

		private void TextBox_LostFocus()
		{
			// TextBox.Text = Trim$(TextBox.Text)
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As Form
		public Form ActiveForm
		{
			get
			{
				Form ActiveForm = null;
				ActiveForm = pFormName;
				return ActiveForm;
			}
			// vbPorter upgrade warning: NewValue As object	OnRead(Form)
			set
			{
				pFormName = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool UseRoutine
		{
			get
			{
				bool UseRoutine = false;
				UseRoutine = pboolUseRoutine;
				return UseRoutine;
			}
			// vbPorter upgrade warning: NewValue As Variant --> As bool
			set
			{
				pboolUseRoutine = value;
			}
		}
	}
}
