﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	public class cSubscriptionSQLController
	{
		//=========================================================
		public cSubscriptionInfo LoadSubscriptions(string strConnectionInfo, string strConnName)
		{
			cSubscriptionInfo LoadSubscriptions = null;
			cSubscriptionInfo tReturn = new cSubscriptionInfo();
			if (!(strConnName == ""))
			{
				cSubscriptionInfo tSub = new cSubscriptionInfo();
				string strTemp = "";
				string strSelect = "";
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("Select * from globalvariables", "SystemSettings");
				if (!rsLoad.EndOfFile())
				{
					tSub.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Muniname"));
				}
				strSelect = "Select * from modules";
				rsLoad.OpenRecordset(strSelect, "SystemSettings");
				FillFromDataTable(ref rsLoad, ref tSub);
				tReturn = tSub;
			}
			LoadSubscriptions = tReturn;
			return LoadSubscriptions;
		}

		private void FillFromDataTable(ref clsDRWrapper tTable, ref cSubscriptionInfo tSub)
		{
			if (!(tTable == null) && !(tSub == null))
			{
				if (tTable.RecordCount() > 0)
				{
					string strColName = "";
					string strTemp = "";
					int x;
					while (!tTable.EndOfFile())
					{
						for (x = 0; x <= tTable.FieldsCount - 1; x++)
						{
							strColName = tTable.Get_FieldsIndexName(x);
							if ((Strings.LCase(strColName) == "id") || (Strings.LCase(strColName) == "autoid"))
							{
								// , "checkdigits", "towncheck"
							}
							else
							{
								strTemp = "";
								// If Not tRow[strColName] Is Nothing Then
								if (!fecherFoundation.FCUtils.IsNull(tTable.Get_Fields(strColName)))
								{
									// If tCol.DataType Is GetType(Date) Then
									// strTemp = Format(CDate(tRow[strColName].ToString), "MM/dd/yyyy")
									// Else
									strTemp = FCConvert.ToString(tTable.Get_Fields(strColName));
									// End If
								}
								// End If
								if (strTemp != "")
								{
									tSub.AddItem(strColName, strTemp, "", "");
								}
							}
						}
						// x
						tTable.MoveNext();
					}
				}
			}
		}

		private void FillTableFromInfo(ref clsDRWrapper tTable, ref cSubscriptionInfo tSub)
		{
			try
			{
				int x;
				if (!(tTable == null) && !(tSub == null))
				{
					bool boolAdd;
					if (tTable.RecordCount() > 0)
					{
						tTable.MoveFirst();
						tTable.Edit();
					}
					else
					{
						tTable.AddNew();
						boolAdd = true;
					}
					int intCheckDigits = 0;
					string strMuniCheck = "";
					strMuniCheck = CalculateTownCheckDigits(tSub.Name);
					intCheckDigits = CalculateCheckDigits(ref tSub);
					tSub.AddItem("CheckDigits", FCConvert.ToString(intCheckDigits), "", "");
					tSub.AddItem("TownCheck", strMuniCheck, "", "");
					/*? On Error Resume Next  */
					foreach (cSubscriptionItem tItem in tSub.Items)
					{
						//Application.DoEvents();
						if ((Strings.LCase(tItem.Name) == "id") || (Strings.LCase(tItem.Name) == "autoid") || (Strings.LCase(tItem.Name) == "ro") || (Strings.LCase(tItem.Name) == "rodate"))
						{
						}
						else
						{
							if (tItem.ItemValue != "")
							{
								tTable.Set_Fields(tItem.Name, tItem.ItemValue);
							}
							else
							{
								tTable.Set_Fields(tItem.Name, null);
							}
						}
					}
					// tItem
					tTable.Update();
				}
				return;
				ErrorHandler:
				;
			}
			catch
			{
			}
		}

		public bool SaveSubscriptions(ref cSubscriptionInfo SubscriptionInformation, string strConnectionInfo, string strConnName)
		{
			bool SaveSubscriptions = false;
			if (!(strConnName == ""))
			{
				string strSelect = "";
				strSelect = "Select * from modules";
				clsDRWrapper TheDataTable = new clsDRWrapper();
				SubscriptionInformation.Name = strConnName;
				TheDataTable.OpenRecordset(strSelect, "twgn0000.vb1");
				string strSQL = "";
				FillTableFromInfo(ref TheDataTable, ref SubscriptionInformation);
			}
			SaveSubscriptions = true;
			return SaveSubscriptions;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetDateAndLevelSum(ref cSubscriptionInfo tSub, string strMod)
		{
			short GetDateAndLevelSum = 0;
			if (!(strMod == ""))
			{
				string strTemp = "";
				// vbPorter upgrade warning: intReturn As short --> As int	OnWrite(int, double)
				int intReturn = 0;
				strTemp = tSub.GetSubscriptionItemValue(strMod, "");
				if (strTemp != "")
				{
					if (Strings.LCase(strTemp) == "true")
					{
						strTemp = tSub.GetSubscriptionItemValue(strMod + "Date", "");
						if (strTemp != "")
						{
							intReturn += FCConvert.ToDateTime(strTemp).Year + FCConvert.ToDateTime(strTemp).Month;
						}
						strTemp = tSub.GetSubscriptionItemValue(strMod + "Level", "");
						if (strTemp != "")
						{
							intReturn += FCConvert.ToInt32(strTemp);
						}
					}
				}
				GetDateAndLevelSum = FCConvert.ToInt16(intReturn);
			}
			else
			{
				GetDateAndLevelSum = 0;
			}
			return GetDateAndLevelSum;
		}

		public int CalculateCheckDigits(ref cSubscriptionInfo tSub)
		{
			int CalculateCheckDigits = 0;
			// vbPorter upgrade warning: intReturn As int	OnWrite(int, double)
			int intReturn = 0;
			string strTemp;
			int intREMaxAccounts;
			strTemp = tSub.GetSubscriptionItemValue("RE", "");
			if (strTemp != "")
			{
				if (Strings.LCase(strTemp) == "true")
				{
					intREMaxAccounts = FCConvert.ToInt32(Math.Round(Conversion.Val(tSub.GetSubscriptionItemValue("REMax_Accounts", ""))));
					strTemp = tSub.GetSubscriptionItemValue("REDate", "");
					if (strTemp != "")
					{
						intReturn += FCConvert.ToDateTime(strTemp).Year + FCConvert.ToDateTime(strTemp).Month;
						// End If
						intReturn += FCConvert.ToInt32(tSub.GetSubscriptionItemValue("RELevel", ""));
						strTemp = tSub.GetSubscriptionItemValue("CM", "");
						if (Strings.LCase(Strings.Trim(strTemp)) == "true")
						{
							strTemp = tSub.GetSubscriptionItemValue("CMDate", "");
							if (strTemp != "" && strTemp != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
							{
								if (Information.IsDate(strTemp))
								{
									intReturn += FCConvert.ToDateTime(strTemp).Year + FCConvert.ToDateTime(strTemp).Month;
								}
							}
						}
					}
					if (Strings.LCase(tSub.GetSubscriptionItemValue("SK", "")) == "true")
					{
						// strTemp = tSub.GetSubscriptionItemValue("SKDate", "")
						strTemp = tSub.GetSubscriptionItemValue("REDate", "");
						if (Information.IsDate(strTemp))
						{
							intReturn += FCConvert.ToDateTime(strTemp).Year + FCConvert.ToDateTime(strTemp).Month;
						}
						intReturn += FCConvert.ToInt32(tSub.GetSubscriptionItemValue("SKLevel", ""));
					}
					if (Strings.LCase(tSub.GetSubscriptionItemValue("RH", "")) == "true")
					{
						strTemp = tSub.GetSubscriptionItemValue("RHDate", "");
						if (Information.IsDate(strTemp))
						{
							intReturn += FCConvert.ToDateTime(strTemp).Year + FCConvert.ToDateTime(strTemp).Month;
						}
						intReturn += FCConvert.ToInt32(tSub.GetSubscriptionItemValue("RHLevel", ""));
					}
					// intReturn = intReturn + ((intREMaxAccounts \ 100) * 3)
				}
			}
			strTemp = tSub.GetSubscriptionItemValue("MV", "");
			if (strTemp != "")
			{
				if (Strings.LCase(strTemp) == "true")
				{
					intReturn += GetDateAndLevelSum(ref tSub, "MV");
					intReturn += GetDateAndLevelSum(ref tSub, "RR");
				}
			}
			intReturn += GetDateAndLevelSum(ref tSub, "AR");
			intReturn += GetDateAndLevelSum(ref tSub, "BD");
			intReturn += GetDateAndLevelSum(ref tSub, "BL");
			intReturn += GetDateAndLevelSum(ref tSub, "CE");
			intReturn += GetDateAndLevelSum(ref tSub, "CK");
			intReturn += GetDateAndLevelSum(ref tSub, "CL");
			intReturn += GetDateAndLevelSum(ref tSub, "CR");
			intReturn += GetDateAndLevelSum(ref tSub, "E9");
			intReturn += GetDateAndLevelSum(ref tSub, "FA");
			intReturn += GetDateAndLevelSum(ref tSub, "HR");
			intReturn += GetDateAndLevelSum(ref tSub, "IV");
			intReturn += GetDateAndLevelSum(ref tSub, "MS");
			intReturn += GetDateAndLevelSum(ref tSub, "PP");
			intReturn += GetDateAndLevelSum(ref tSub, "PY");
			intReturn += GetDateAndLevelSum(ref tSub, "RB");
			intReturn += GetDateAndLevelSum(ref tSub, "TS");
			intReturn += GetDateAndLevelSum(ref tSub, "UT");
			intReturn += GetDateAndLevelSum(ref tSub, "VR");
			intReturn += GetDateAndLevelSum(ref tSub, "XF");
			strTemp = tSub.GetSubscriptionItemValue("NT", "");
			if (strTemp != "")
			{
				if (FCConvert.CBool(strTemp))
				{
					intReturn += 1;
				}
			}
			CalculateCheckDigits = intReturn;
			return CalculateCheckDigits;
		}

		public string CalculateTownCheckDigits(string strMuniName)
		{
			string CalculateTownCheckDigits = "";
			CalculateTownCheckDigits = modCheckDigits.ReturnTownEncryption(ref strMuniName);
			// CalculateTownCheckDigits = ""
			// Dim strEncrypt As String
			// Dim strTemp As String
			// If strMuniName = "" Then
			// Exit Function
			// End If
			// strTemp = Trim(strMuniName)
			// If strTemp.Trim.length = 3 Then
			// strEncrypt = strEncrypt & strTemp.Substring(0, 1)
			// strEncrypt = strEncrypt & strTemp.Substring(strTemp.length - 1)
			// strEncrypt = strEncrypt & Mid(strTemp, 2, 1)
			// strEncrypt = strEncrypt & "ZB"
			// strEncrypt = strEncrypt.ToUpper
			// ElseIf strTemp.Trim.length = 4 Then
			// strEncrypt = strEncrypt & strTemp.Substring(0, 1)
			// strEncrypt = strEncrypt & strTemp.Substring(strTemp.length - 1)
			// strEncrypt = strEncrypt & Mid(strTemp, 2, 1)
			// strEncrypt = strEncrypt & "Z"
			// strEncrypt = strEncrypt & Mid(strTemp, 3, 1)
			// strEncrypt = strEncrypt.ToUpper
			// Else
			// strEncrypt = strEncrypt & strTemp.Substring(0, 1)
			// strEncrypt = strEncrypt & Right(strTemp, 1) 'mid(strTemp,len(strtemp) - 1)
			// strEncrypt = strEncrypt & Mid(strTemp, 2, 1)
			// strEncrypt = strEncrypt & Mid(strTemp, Len(strTemp) - 4, 1)
			// strEncrypt = strEncrypt & Mid(strTemp, Len(strTemp) - 2, 1)
			// strEncrypt = UCase(strEncrypt)
			// End If
			// CalculateTownCheckDigits = strEncrypt
			return CalculateTownCheckDigits;
		}
	}
}
