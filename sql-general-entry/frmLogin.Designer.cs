﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	partial class frmLogin : BaseForm
	{
		public fecherFoundation.FCComboBox cboDataGroup;
		public fecherFoundation.FCButton btnCancel;
		public fecherFoundation.FCButton btnLogin;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel lblHarrisStaff;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblGroup;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.cboDataGroup = new fecherFoundation.FCComboBox();
            this.btnCancel = new fecherFoundation.FCButton();
            this.btnLogin = new fecherFoundation.FCButton();
            this.txtPassword = new fecherFoundation.FCTextBox();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.lblHarrisStaff = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblGroup = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.txtUser = new fecherFoundation.FCTextBox();
            this.lblVersion = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 453);
            this.BottomPanel.Size = new System.Drawing.Size(380, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblVersion);
            this.ClientArea.Controls.Add(this.txtUser);
            this.ClientArea.Controls.Add(this.cboDataGroup);
            this.ClientArea.Controls.Add(this.btnCancel);
            this.ClientArea.Controls.Add(this.btnLogin);
            this.ClientArea.Controls.Add(this.txtPassword);
            this.ClientArea.Controls.Add(this.Image1);
            this.ClientArea.Controls.Add(this.lblHarrisStaff);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblGroup);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(400, 463);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblGroup, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblHarrisStaff, 0);
            this.ClientArea.Controls.SetChildIndex(this.Image1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPassword, 0);
            this.ClientArea.Controls.SetChildIndex(this.btnLogin, 0);
            this.ClientArea.Controls.SetChildIndex(this.btnCancel, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboDataGroup, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUser, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVersion, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(400, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // cboDataGroup
            // 
            this.cboDataGroup.BackColor = System.Drawing.SystemColors.Window;
            this.cboDataGroup.Location = new System.Drawing.Point(174, 215);
            this.cboDataGroup.Name = "cboDataGroup";
            this.cboDataGroup.Size = new System.Drawing.Size(199, 40);
            this.cboDataGroup.TabIndex = 3;
            this.cboDataGroup.SelectedIndexChanged += new System.EventHandler(this.cboDataGroup_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(178, 189, 202);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(134, 373);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 40);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.AppearanceKey = "actionButton";
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(178, 189, 202);
            this.btnLogin.DialogResult = Wisej.Web.DialogResult.OK;
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(30, 373);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(87, 40);
            this.btnLogin.TabIndex = 8;
            this.btnLogin.Text = "Log In";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
            this.txtPassword.Location = new System.Drawing.Point(174, 315);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(199, 40);
            this.txtPassword.TabIndex = 7;
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.FillColor = 16777215;
            this.Image1.ImageSource = "login-logo";
            this.Image1.Location = new System.Drawing.Point(30, 30);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(343, 133);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // lblHarrisStaff
            // 
            this.lblHarrisStaff.BackColor = System.Drawing.Color.Transparent;
            this.lblHarrisStaff.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblHarrisStaff.Location = new System.Drawing.Point(30, 170);
            this.lblHarrisStaff.Name = "lblHarrisStaff";
            this.lblHarrisStaff.Size = new System.Drawing.Size(311, 31);
            this.lblHarrisStaff.TabIndex = 1;
            this.lblHarrisStaff.Text = "HARRIS STAFF COMPUTER";
            this.lblHarrisStaff.Visible = false;
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Location = new System.Drawing.Point(30, 229);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(99, 22);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "DATA GROUP";
            // 
            // lblGroup
            // 
            this.lblGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblGroup.Location = new System.Drawing.Point(30, 174);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(343, 22);
            this.lblGroup.TabIndex = 4;
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Location = new System.Drawing.Point(30, 329);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(87, 22);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "PASSWORD";
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Location = new System.Drawing.Point(30, 279);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(87, 22);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "USER NAME";
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtUser.Location = new System.Drawing.Point(174, 265);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(199, 40);
            this.txtUser.TabIndex = 5;
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Location = new System.Drawing.Point(304, 431);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(82, 22);
            this.lblVersion.TabIndex = 1002;
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btnLogin;
            this.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(400, 463);
            this.ControlBox = false;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.Activated += new System.EventHandler(this.frmLogin_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLogin_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCTextBox txtUser;
		public FCLabel lblVersion;
	}
}
