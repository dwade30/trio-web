//Fecher vbPorter - Version 1.0.0.40

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmUpdateType.
	/// </summary>
	partial class frmUpdateType : fecherFoundation.FCForm
	{
		public fecherFoundation.FCButton cmdDLLOnly;
		public fecherFoundation.FCButton cmdInstallShield;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdRegularUpdate;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmUpdateType));
			this.cmdDLLOnly = new fecherFoundation.FCButton();
			this.cmdInstallShield = new fecherFoundation.FCButton();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdRegularUpdate = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			
			this.SuspendLayout();
			//
			// cmdDLLOnly
			//
			this.cmdDLLOnly.Name = "cmdDLLOnly";
			this.cmdDLLOnly.TabIndex = 4;
			this.cmdDLLOnly.Location = new System.Drawing.Point(34, 78);
			this.cmdDLLOnly.Size = new System.Drawing.Size(181, 25);
			this.cmdDLLOnly.Text = "DLL Only Update";
			this.cmdDLLOnly.BackColor = System.Drawing.SystemColors.Control;
			this.cmdDLLOnly.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdDLLOnly.Click += new System.EventHandler(this.cmdDLLOnly_Click);
			//
			// cmdInstallShield
			//
			this.cmdInstallShield.Name = "cmdInstallShield";
			this.cmdInstallShield.TabIndex = 1;
			this.cmdInstallShield.Location = new System.Drawing.Point(34, 49);
			this.cmdInstallShield.Size = new System.Drawing.Size(181, 25);
			this.cmdInstallShield.Text = "Process InstallShield Update";
			this.cmdInstallShield.BackColor = System.Drawing.SystemColors.Control;
			this.cmdInstallShield.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdInstallShield.Click += new System.EventHandler(this.cmdInstallShield_Click);
			//
			// cmdQuit
			//
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.TabIndex = 2;
			this.cmdQuit.Location = new System.Drawing.Point(34, 107);
			this.cmdQuit.Size = new System.Drawing.Size(181, 25);
			this.cmdQuit.Text = "&Cancel";
			this.cmdQuit.BackColor = System.Drawing.SystemColors.Control;
			this.cmdQuit.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			//
			// cmdRegularUpdate
			//
			this.cmdRegularUpdate.Name = "cmdRegularUpdate";
			this.cmdRegularUpdate.Visible = false;
			this.cmdRegularUpdate.TabIndex = 0;
			this.cmdRegularUpdate.Location = new System.Drawing.Point(91, 125);
			this.cmdRegularUpdate.Size = new System.Drawing.Size(181, 25);
			this.cmdRegularUpdate.Text = "Process Regular Update";
			this.cmdRegularUpdate.BackColor = System.Drawing.SystemColors.Control;
			this.cmdRegularUpdate.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdRegularUpdate.Click += new System.EventHandler(this.cmdRegularUpdate_Click);
			//
			// Label1
			//
			this.Label1.Name = "Label1";
			this.Label1.TabIndex = 3;
			this.Label1.Location = new System.Drawing.Point(10, 2);
			this.Label1.Size = new System.Drawing.Size(244, 35);
			this.Label1.Text = "Choose the type of Program Update that you want to process.";
			this.Label1.BackColor = System.Drawing.SystemColors.Control;
			this.Label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(255)));
			this.Label1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(233, 67);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// frmUpdateType
			//
			this.ClientSize = new System.Drawing.Size(268, 173);
			this.Controls.Add(this.cmdDLLOnly);
			this.Controls.Add(this.cmdInstallShield);
			this.Controls.Add(this.cmdQuit);
			this.Controls.Add(this.cmdRegularUpdate);
			this.Controls.Add(this.Label1);
			//this.Controls.Add(this.vsElasticLight1);
			this.Name = "frmUpdateType";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmUpdateType.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.WindowsDefaultLocation;
			this.Load += new System.EventHandler(this.frmUpdateType_Load);
			this.Text = "Program Update";
			this.cmdDLLOnly.ResumeLayout(false);
			this.cmdInstallShield.ResumeLayout(false);
			this.cmdQuit.ResumeLayout(false);
			this.cmdRegularUpdate.ResumeLayout(false);
			this.Label1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}