//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmUnzipFiles.
	/// </summary>
	public partial class frmUnzipFiles : fecherFoundation.FCForm
	{
;

		public frmUnzipFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null && Wisej.Web.Application.OpenForms.Count == 0)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUnzipFiles InstancePtr
		{
			get
			{
				if (_InstancePtr == null) // || _InstancePtr.IsDisposed
					_InstancePtr = new frmUnzipFiles();
				return _InstancePtr;
			}
		}
		protected static frmUnzipFiles _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		string strFile;
		string strSourcePath = "";
		Scripting.File f;
		Scripting.Folder fFolder;
		Scripting.File fNewFile;
		// Private WithEvents m_cUnzip As cUnzip
		// Private m_cZipMRU As cMRU
		int intCounter;
		bool boolExit;

		private void AbaleZip1_FileStatus(object sender, AxAbaleZipLibrary._IAbaleZipEvents_FileStatusEvent e)
		{
			Label3.Text = e.sFilename+" "+Convert.ToString(e.nBytesPercent)+"%";
		}

		private void AbaleZip1_InsertDisk(object sender, AxAbaleZipLibrary._IAbaleZipEvents_InsertDiskEvent e)
		{
			if (e.lDiskNumber==0) return;
			MessageBox.Show("Insert disk "+Convert.ToString(e.lDiskNumber), "Insert disk", MessageBoxButtons.OK, MessageBoxIcon.Information);
			e.bDiskInserted = true;
		}

		private void cmdRestore_Click(object sender, System.EventArgs e)
		{
			bool boolEXEs;
			bool boolVB1s;
			int iItem;
			string sFolder = "";
			string strTempDir = "";
			// vbPorter upgrade warning: zipResponse As abeError	OnWrite(AbaleZipLibrary.abeError)
			AbaleZipLibrary.abeError zipResponse;
			bool boolErr;
			string strFileNames;

			try
			{	// On Error GoTo ErrorHandler
				// Me.Tag = Me.Height
				// Me.Height = 4815


				// Choose Selected items:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// let's do exe's first
				AbaleZip1.FilesToProcess = "";
				AbaleZip1.SpanMultipleDisks = AbaleZipLibrary.abeDiskSpanning.adsAlways;
				boolErr = false;
				boolEXEs = false;
				boolVB1s = false;
				strFileNames = "";
				for(intCounter=0; intCounter<=lstFiles.Items.Count-1; intCounter++) {
					if (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="EXE" || Strings.LCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="dll") {
						// strTempDir = Left(CurDir, 1) & ":\TrioData\TrioMast"
						strTempDir = modGlobalFunctions.gGlobalSettings.MasterPath;
					} else {
						strTempDir = Environment.CurrentDirectory;
					}
					strTempDir = (Strings.Right(strTempDir, 1)!="\\" ? strTempDir+"\\" : strTempDir);

					if (lstFiles.GetSelected(intCounter)) {
						// file is to be extracted
						if (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="EXE" || Strings.LCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="dll") {
							boolEXEs = true;
						} else {
							boolVB1s = true;
							if (Strings.UCase(Strings.Right(lstFiles.Items[intCounter].ToString(), 3))=="VB1") {
								strFileNames += lstFiles.Items[intCounter].ToString()+",";
							}
						}

						AbaleZip1.AddFilesToProcess(lstFiles.Items[intCounter].ToString());
						ff = new FCFileSystemObject();
						if (!ff.FolderExists(strTempDir)) {
							ff.CreateFolder(strTempDir);
						}

						if (ff.FileExists(strTempDir+lstFiles.Items[intCounter].ToString())) {
							fNewFile = ff.GetFile(strTempDir+lstFiles.Items[intCounter].ToString());
							fNewFile.Copy(strTempDir+lstFiles.Items[intCounter].ToString()+".old");
						}

					}

				} // intCounter

				if (strFileNames!=string.Empty) {
					strFileNames = Strings.Mid(strFileNames, 1, strFileNames.Length-1);
					strFileNames = Strings.Mid(strFileNames, 1, 255);
				}
				if (AbaleZip1.FilesToProcess!=string.Empty) {
					// there is at least one file to unzip
					// exes first
					modGlobalFunctions.AddCYAEntry_8("GN", "Performed Restore", ref strFileNames);
					AbaleZip1.BasePath = "";

					if (boolEXEs) {
						// strTempDir = Left(CurDir, 1) & ":\TrioData\TrioMast"
						strTempDir = modGlobalFunctions.gGlobalSettings.MasterPath;
						AbaleZip1.UnzipToFolder = strTempDir;
						AbaleZip1.FilesToExclude = "*.vb1";
						zipResponse = AbaleZip1.Unzip();
						if (zipResponse!=AbaleZipLibrary.abeError.aerSuccess) {
							MessageBox.Show("Error unzipping executable file(s).");
							boolErr = true;
						} else {
						}
					}
					Label3.Text = "";

					// then database files
					if (boolVB1s) {
						boolErr = false;
						AbaleZip1.FilesToExclude = "*.exe|*.dll";
						strTempDir = Environment.CurrentDirectory;
						AbaleZip1.UnzipToFolder = strTempDir;
						zipResponse = AbaleZip1.Unzip();
						if (zipResponse!=AbaleZipLibrary.abeError.aerSuccess) {
							MessageBox.Show("Error unzipping database file(s).");
							boolErr = true;
						}
					}
				}

				if (!boolErr) {
					MessageBox.Show("Restore completed.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// For intCounter = 0 To lstFiles.ListCount - 1
				// m_cUnzip.FileSelected(intCounter + 1) = lstFiles.Selected(intCounter) And UCase(Right(lstFiles.List(intCounter), 3)) = "EXE"
				// 
				// If UCase(Right(lstFiles.List(intCounter), 3)) = "EXE" Then
				// strTempDir = Left(CurDir, 1) & ":\TrioData\TrioMast"
				// Else
				// strTempDir = CurDir
				// End If
				// strTempDir = IIf(Right(strTempDir, 1) <> "\", strTempDir & "\", strTempDir)
				// 
				// Set ff = New FCFileSystemObject
				// If Not ff.FolderExists(strTempDir) Then
				// Call ff.CreateFolder(strTempDir)
				// End If
				// 
				// If ff.FileExists(strTempDir & lstFiles.List(intCounter)) Then
				// Set fNewFile = ff.GetFile(strTempDir & lstFiles.List(intCounter))
				// Call fNewFile.Copy(strTempDir & lstFiles.List(intCounter) & ".old")
				// End If
				// 
				// If lstFiles.Selected(intCounter) Then
				// bSel = True
				// End If
				// Next
				// 
				// If none selected do entire zip:
				// If Not bSel Then
				// For iItem = 1 To m_cUnzip.FileCount
				// m_cUnzip.FileSelected(iItem) = True
				// Next iItem
				// End If

				// Get extract folder and do it:
				// sFolder = strFile
				// 
				// extract the exe's first
				// sFolder = Left(CurDir, 1) & ":\TrioData\TrioMast"
				// sFolder = IIf(Right(sFolder, 1) <> "\", sFolder & "\", sFolder)
				// If (sFolder <> "") Then
				// m_cUnzip.UnzipFolder = sFolder
				// Call m_cUnzip.Unzip(Label3, Me, pbrProgress, lblPercent)
				// End If
				// 
				// For intCounter = 0 To lstFiles.ListCount - 1
				// m_cUnzip.FileSelected(intCounter + 1) = lstFiles.Selected(intCounter) And UCase(Right(lstFiles.List(intCounter), 3)) <> "EXE"
				// 
				// If UCase(Right(lstFiles.List(intCounter), 3)) <> "EXE" Then
				// strTempDir = Left(CurDir, 1) & ":\TrioData\TrioMast"
				// Else
				// strTempDir = CurDir
				// End If
				// strTempDir = IIf(Right(strTempDir, 1) <> "\", strTempDir & "\", strTempDir)
				// 
				// Set ff = New FCFileSystemObject
				// If Not ff.FolderExists(strTempDir) Then
				// Call ff.CreateFolder(strTempDir)
				// End If
				// 
				// If ff.FileExists(strTempDir & lstFiles.List(intCounter)) Then
				// Set fNewFile = ff.GetFile(strTempDir & lstFiles.List(intCounter))
				// Call fNewFile.Copy(strTempDir & lstFiles.List(intCounter) & ".old")
				// End If
				// 
				// If lstFiles.Selected(intCounter) Then
				// bSel = True
				// End If
				// Next
				// 
				// If none selected do entire zip:
				// If Not bSel Then
				// For iItem = 1 To m_cUnzip.FileCount
				// m_cUnzip.FileSelected(iItem) = True
				// Next iItem
				// End If
				// 
				// Get extract folder and do it:
				// sFolder = strFile
				// 
				// extract the exe's first
				// sFolder = CurDir
				// sFolder = IIf(Right(sFolder, 1) <> "\", sFolder & "\", sFolder)
				// If (sFolder <> "") Then
				// m_cUnzip.UnzipFolder = sFolder
				// Call m_cUnzip.Unzip(Label3, Me, pbrProgress, lblPercent)
				// End If
				// 
				// Me.Height = Me.Tag
				// MsgBox "Update of files completed successfully.", vbInformation + vbOKOnly, "TRIO Software"
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				this.Unload();
				return;

			}
			catch (Exception ex)
            {	// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}



		private void frmUnzipFiles_Activated(object sender, System.EventArgs e)
		{
			if (boolExit) this.Unload();
		}

		private void frmUnzipFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUnzipFiles properties;
			//frmUnzipFiles.ScaleWidth	= 5880;
			//frmUnzipFiles.ScaleHeight	= 4350;
			//frmUnzipFiles.LinkTopic	= "Form1";
			//End Unmaped Properties

			try
			{	// On Error GoTo EndTag

				boolExit = false;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.TRIOCOLORBLUE);
				// Set m_cUnzip = New cUnzip
				GetFiles();
				return;

			}
			catch (Exception ex)
            {	// EndTag:
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void GetFiles()
		{
			try
			{	// On Error GoTo ErrorHandler
				ListViewItem itmX;
				bool bSel;
				string sFolder = "";
				int iItem;


				strFile = (Strings.Right(modGNBas.gstrBackupPath, 1)!="\\" ? modGNBas.gstrBackupPath+"\\" : modGNBas.gstrBackupPath);

				pOpen(strFile);
				return;

			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool pOpen(string sFIle)
		{
			bool pOpen = false;
			int I;
			string sIcon = "";
			ListViewItem itmX;
			AbaleZipLibrary.AbaleZipItems abObj = new AbaleZipLibrary.AbaleZipItems();
			// vbPorter upgrade warning: abFormat As abeContentsFormat	OnWrite(AbaleZipLibrary.abeContentsFormat)
			AbaleZipLibrary.abeContentsFormat abFormat;
			// vbPorter upgrade warning: ZipErr As abeError	OnWrite(AbaleZipLibrary.abeError)
			AbaleZipLibrary.abeError ZipErr;
			string strTemp = "";

			lstFiles.Clear();

			// Get the file directory:
			// m_cUnzip.ZipFile = sFIle & gstrFileN
			AbaleZip1.ZipFilename = sFIle+modGNBas.gstrFileN;
			// m_cUnzip.Directory

			// If m_cUnzip.FileCount > 0 Then
			// m_cZipMRU.Add sFIle
			// pShowOpenMRU
			// End If

			// pEnableControls
			abFormat = AbaleZipLibrary.abeContentsFormat.acfCollection;
			ZipErr = AbaleZip1.GetZipContents(abObj, abFormat);
			if (ZipErr!=AbaleZipLibrary.abeError.aerSuccess) {
				// unsuccessful
				strTemp = "Unknown Error";
				
				if (ZipErr==AbaleZipLibrary.abeError.aerBusy)
				{
					strTemp = "Busy";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerCannotAccessArray)
				{
					strTemp = "Cannot Access Array";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerCannotUpdateAndSpan)
				{
					strTemp = "Cannot Update and Span";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerCannotUpdateSpanned)
				{
					strTemp = "Cannot Update Spanned";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerCreateTempFile)
				{
					strTemp = "Create Temp File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerDiskNotEmptyAbort)
				{
					strTemp = "Disk Not Empty Abort";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerEmptyZipFile)
				{
					strTemp = "Empty Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerEndOfZipFile)
				{
					strTemp = "End of Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerFilesSkipped)
				{
					strTemp = "Files Skipped";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerInsertDiskAbort)
				{
					strTemp = "Insert Disk Abort";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerInternalError)
				{
					strTemp = "Internal Error";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerInvalidArrayDimensions)
				{
					strTemp = "Invalid Array Dimensions";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerInvalidArrayType)
				{
					strTemp = "Invalid Array Type";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerInvalidSfxProperty)
				{
					strTemp = "Invalid Sfx Property";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerMemory)
				{
					strTemp = "Memory Error";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerMoveTempFile)
				{
					strTemp = "Move Temp File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerNotAZipFile)
				{
					strTemp = "Not a Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerNothingToDo)
				{
					strTemp = "Nothing to Do";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerNotLicensed)
				{
					strTemp = "Not Licensed";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerOpenZipFile)
				{
					strTemp = "Open Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerProcessStarted)
				{
					strTemp = "Process Started";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerReadSfxBinary)
				{
					strTemp = "Read Sfx Binary";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerReadZipFile)
				{
					strTemp = "Read Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerRemoveWithoutTemp)
				{
					strTemp = "Remove Without Temp";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerSeekInZipFile)
				{
					strTemp = "Seek In Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerSfxBinaryNotFound)
				{
					strTemp = "Sfx Binary Not Found";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerSplitSizeTooSmall)
				{
					strTemp = "Split Size Too Small";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerSuccess)
				{
					strTemp = "No Error";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerUninitializedArray)
				{
					strTemp = "Uninitialized Array";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerUninitializedString)
				{
					strTemp = "Uninitialized String";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerUnsupportedDataType)
				{
					strTemp = "Unsupported Data Type";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerUserAbort)
				{
					strTemp = "User Abort";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerWarnings)
				{
					strTemp = "Warnings";

				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerWriteTempZipFile)
				{
					strTemp = "Write Temp Zip File";
				}
				else if (ZipErr==AbaleZipLibrary.abeError.aerWriteZipFile)
				{
					strTemp = "Write Zip File";
				}

				strTemp = AbaleZip1.GetErrorDescription(AbaleZipLibrary.abeValueType.avtError, ZipErr);
				if (ZipErr==AbaleZipLibrary.abeError.aerInsertDiskAbort) {
					strTemp += " The wrong file could also have been chosen.";
				}
				MessageBox.Show("Error in opening the zip file."+" "+strTemp, null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				boolExit = true;
				// Unload Me
				return pOpen;
			}

			// Display it in the ListView:
			for(I=1; I<=abObj.Count; I++) {
				// sIcon = AddIconToImageList(m_cUnzip.Filename(i), ilsIcons16, "DEFAULT")
				// sFIle = m_cUnzip.Filename(i)
				sFIle = abObj[I].FileName;

				lstFiles.AddItem(sFIle);
				lstFiles.SetSelected(lstFiles.NewIndex, true);
				// If m_cUnzip.FileEncrypted(i) Then
				// the way WinZip represents it.  I guess a nicer way would be
				// to use overlay icons/state icons and/or colour changes in the LV
				// sFIle = sFIle & "+"
				// End If
				// Set itmX = lvwZip.ListItems.Add(, "File" & i, sFIle, , sIcon)
				// itmX.SubItems(1) = m_cUnzip.FileSize(i)
				// itmX.SubItems(2) = Format$(m_cUnzip.FileDate(i), "short date") & " " & Format$(m_cUnzip.FileDate(i), "short time")
				// itmX.SubItems(3) = m_cUnzip.FilePackedSize(i)
				// itmX.SubItems(4) = m_cUnzip.FileDirectory(i)
			} // I
			return pOpen;
		}



	}
}