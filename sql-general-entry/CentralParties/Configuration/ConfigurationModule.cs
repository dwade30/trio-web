﻿using Autofac;
using Global;
using SharedApplication;
using SharedApplication.CentralParties;
using SharedApplication.ClientSettings.ScheduleUpdate;
using TWGNENTY.UpdateScheduler;

namespace TWGNENTY.CentralParties.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmCentralPartySearch>().As<IModalView<ICentralPartySearchViewModel>>();
            builder.RegisterType<frmScheduleUpdate>().As<IModalView<IScheduleUpdateViewModel>>();
        }
    }
}