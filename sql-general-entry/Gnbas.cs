﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Linq;

namespace TWGNENTY
{
	public class modGNBas
	{
		public const string strBDDatabase = "TWBD0000.vb1";
		public const string strCLDatabase = "TWCL0000.vb1";
		public const string strCKDatabase = "TWCK0000.vb1";
		public const string strCRDatabase = "TWCR0000.vb1";
		public const string strE9Database = "TWE90000.vb1";
		public const string strFADatabase = "TWFa0000.vb1";
		public const string strMVDatabase = "TWMV0000.vb1";
		public const string strPPDatabase = "TWPP0000.vb1";
		public const string strPYDatabase = "TWPY0000.vb1";
		public const string strREDatabase = "TWRE0000.vb1";
		public const string strUTDatabase = "TWUT0000.vb1";
		public const string strVRDatabase = "TWVR0000.vb1";
		public const string DEFAULTDATABASE = "SystemSettings";
		public const string Provider = "Microsoft.Jet.OLEDB.4.0";
		// These Declares are used to get the block cursor
		[DllImport("user32")]
		public static extern int CreateCaret(int hwnd, int hBitmap, int nWidth, int nHeight);

		[DllImport("user32")]
		public static extern int ShowCaret(int hwnd);

		[DllImport("user32")]
		public static extern int GetFocus();

		[DllImport("user32")]
		public static extern object SetCursorPos(int X, int Y);
		// Public Declare Function sendmessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
		[DllImport("user32")]
		public static extern int GetForegroundWindow();

		public const int WM_COMMAND = 0x111;

		[DllImport("kernel32")]
		public static extern void Sleep(int dwMilliseconds);

		[DllImport("kernel32")]
		public static extern int WinExec(string lpCmdLine, int nCmdShow);

		[DllImport("kernel32")]
		public static extern int GlobalAlloc(int wFlags, int dwBytes);

		public struct ControlProportions
		{
			public float WidthProportions;
			public float HeightProportions;
			public float TopProportions;
			public float LeftProportions;
		};
		//
		//
		public static void GetPaths()
		{
			Statics.PPDatabase = "P:\\";
			Statics.TrioEXE = "\\T2K\\";
			Statics.TrioDATA = "\\VBTRIO\\VBTEST\\";
			Statics.PictureFormat = ".BMP";
			Statics.SketchLocation = "\\VBTRIO\\VBTEST\\";
			Statics.Display = "TRIOTEMP.TXT";
		}
		// Public Sub G9996_LOCK()
		// If HighLockNumber = 0 Then HighLockNumber = LowLockNumber
		// Lock #LockFileNumber, LowLockNumber To HighLockNumber
		// If Bbort$ <> "Y" Then
		// LocksHeld(LockFileNumber) = LocksHeld(LockFileNumber) + 1
		// LowLock(LockFileNumber, LocksHeld(LockFileNumber)) = LowLockNumber
		// HighLock(LockFileNumber, LocksHeld(LockFileNumber)) = HighLockNumber
		// End If
		// HighLockNumber = 0
		// End Sub
		// Public Sub G9997_UNLOCK()
		// Dim x As Integer
		// Dim Y As Integer
		// If LocksHeld(LockFileNumber) <> 0 Then
		// For x = LocksHeld(LockFileNumber) To 1 Step -1
		// If LowLock(LockFileNumber, x) <> 0 Then
		// Unlock #LockFileNumber, LowLock(LockFileNumber, x) To HighLock(LockFileNumber, x)
		// LowLock(LockFileNumber, x) = 0
		// HighLock(LockFileNumber, x) = 0
		// End If
		// Next x
		// LocksHeld(LockFileNumber) = 0
		// End If
		// End Sub
		//
		// Public Sub G9998_ABORT()
		// Dim x As Integer
		// Dim Y As Integer
		// On Error Resume Next    ' to be removed later
		// For x = 1 To 50
		// If LocksHeld(x) > 0 Then
		// For Y = LocksHeld(x) To 1 Step -1
		// If LowLock(x, Y) <> 0 Then
		// Unlock #x, LowLock(x, Y) To HighLock(x, Y)
		// LowLock(x, Y) = 0
		// HighLock(x, Y) = 0
		// End If
		// Next Y
		// End If
		// LocksHeld(x) = 0
		// Next x
		// End Sub
		public static void ConvertToString()
		{
			Statics.IntVarTemp = Conversion.Str(Statics.IntvarNumeric);
			Statics.INTVARSTRING = "";
			Statics.IntVarTemp = Strings.LTrim(Statics.IntVarTemp);
			Statics.INTVARSTRING = Strings.StrDup(Statics.intLen - Statics.IntVarTemp.Length, "0") + Statics.IntVarTemp;
		}

		public static void ConvertEdited(ref string newfld)
		{
			int X;
			int Y;
			for (X = 1; X <= newfld.Length; X++)
			{
				if (Strings.Mid(newfld, X, 1) == "," || Strings.Mid(newfld, X, 1) == "." || Strings.Mid(newfld, X, 1) == "$")
				{
					for (Y = X; Y >= 2; Y--)
					{
						fecherFoundation.Strings.MidSet(ref newfld, Y, 1, Strings.Mid(newfld, Y - 1, 1));
					}
					// Y
					fecherFoundation.Strings.MidSet(ref newfld, 1, 1, " ");
				}
			}
			// X
			newfld = Strings.Trim(newfld);
			// IntLen = 9
			Statics.IntvarNumeric = Conversion.Val(newfld);
			ConvertToString();
			if (Statics.Bbort == "Y")
				return;
		}

		public static void MakeCaption()
		{
			Statics.TitleCaption = "TRIO Software Corporation";
			Statics.TitleTime = Strings.Format(DateAndTime.TimeOfDay, "h:mm");
			if (Strings.Mid(Statics.TitleTime, 1, 1) == "0")
				fecherFoundation.Strings.MidSet(ref Statics.TitleTime, 1, 1, " ");
		}

		public static void ErrorRoutine(Exception ex)
		{
			//vbPorterConverter.GoSubStack mGoSubStack = new vbPorterConverter.GoSubStack();
			//goto GoSubPass;
			//GoSubReturn:
			//int GoSubLastPosition = mGoSubStack.GetPosition();
			//if (GoSubLastPosition == 1) goto GoSubReturnPosition1;
			////MessageBox.Show("Return without GoSub");
			//GoSubPass:
			// vbPorter upgrade warning: answer As Variant --> As DialogResult
			DialogResult answer;
			FCGlobal.Screen.MousePointer = 0;
			if (Information.Err(ex).Number == 24 || Information.Err(ex).Number == 25 || Information.Err(ex).Number == 27 || Information.Err(ex).Number == 71)
			{
				answer = MessageBox.Show("Printer appears to not be available", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
				if (answer != DialogResult.Cancel)
				{
					MessageBox.Show("Processing-------");
				}
				else
				{
					Statics.Bbort = "Y";
				}
			}
			else if (modReplaceWorkFiles.Statics.gstrNetworkFlag != "N" && Information.Err(ex).Number > 69 && Information.Err(ex).Number < 71)
			{
				//mGoSubStack.PutPosition(1); goto Err_9999_NET; GoSubReturnPosition1:
			}
			else
			{
				MessageBox.Show("  Error Number " + Information.Err(ex).Number + "  " + Information.Err(ex).Description + "\r\n" + "  Has been encountered - Program Terminated", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Statics.Bbort = "Y";
			}
			if (Statics.EntryMenuFlag == false)
			{
				WriteYY();
				// Shell "TWGNENTY.EXE", vbNormalFocus
			}
			return;
			// End
			Err_9999_NET:
			;
			answer = MessageBox.Show("Record is locked - Do you want to retry", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.No)
				Statics.Bbort = "Y";
			//goto GoSubReturn;
		}

		public static void PrintFormat()
		{
			int fmtextraspc;
			// vbPorter upgrade warning: Formatted As Variant --> As string
			string Formatted = "";
			if (Statics.fmttype == "Whole")
			{
				Formatted = Strings.Format(Statics.fmtval, "###0");
			}
			else if (Statics.fmttype == "WholeC")
			{
				Formatted = Strings.Format(Statics.fmtval, "#,##0");
			}
			else if (Statics.fmttype == "Dollar")
			{
				Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00");
			}
			else if (Statics.fmttype == "DollarC")
			{
				Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00");
			}
			else if (Statics.fmttype == "WholeS")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "###0-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "###0+");
				}
			}
			else if (Statics.fmttype == "WholeSC")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0+");
				}
			}
			else if (Statics.fmttype == "DollarS")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "0.00-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00+");
				}
			}
			else if (Statics.fmttype == "DollarSC")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0.00-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00+");
				}
			}
			fmtextraspc = 0;
			if (FCConvert.ToString(Formatted).Length < Statics.fmtlgth)
				fmtextraspc = Statics.fmtlgth - FCConvert.ToString(Formatted).Length;
			if (fmtextraspc != 0)
				FCFileSystem.Print(40, FCFileSystem.Spc(fmtextraspc));
			if (Statics.fmtspcbef != 0)
				FCFileSystem.Print(40, FCFileSystem.Spc(Statics.fmtspcbef));
			FCFileSystem.Print(40, Formatted);
			if (Statics.fmtspcaft != 0)
				FCFileSystem.Print(40, FCFileSystem.Spc(Statics.fmtspcaft));
			if (Statics.fmtcolon != "Y")
				FCFileSystem.PrintLine(40, " ");
		}

		public static void CheckNumeric()
		{
			for (Statics.X = 1; Statics.X <= FCConvert.ToString(Statics.Resp1).Length; Statics.X++)
			{
				Statics.Resp2 = Convert.ToByte(Strings.Mid(FCConvert.ToString(Statics.Resp1), Statics.X, 1)[0]);
				if (Statics.Resp2 < 48 || Statics.Resp2 > 57)
				{
					fecherFoundation.Strings.MidSet(ref Statics.Resp1, Statics.X, Strings.Mid(Statics.Resp1, Statics.X + 1, FCConvert.ToString(Statics.Resp1).Length - 1));
					fecherFoundation.Strings.MidSet(ref Statics.Resp1, Strings.Len(Statics.Resp1), 1, " ");
					break;
				}
			}
			// X
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, string)
		// vbPorter upgrade warning: intLength As short	OnWrite(string, short)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string PadToString(object intValue, short intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}

		public static object SetInsertMode(ref short InsertMode)
		{
			object SetInsertMode = null;
			// 0 = Insert Mode        = SelLength of 0
			// 1 = OverType Mode      = SelLength of 1
			// The Variable "OverType" returns either 0 or 1 for SelLength
			// 
			//if (FCGlobal.Screen.ActiveControl.SelLength == 1)
			{
				Statics.OverType = 0;
				//else
			}
			{
				Statics.OverType = 1;
			}
			return SetInsertMode;
		}

		public static void GetValue()
		{
			int Y;
			int yy;
			Statics.NewData = Strings.Trim(Statics.NewData);
			for (Y = 1; Y <= Statics.NewData.Length; Y++)
			{
				if (Strings.Mid(Statics.NewData, Y, 1) == "," || Strings.Mid(Statics.NewData, Y, 1) == ".")
				{
					for (yy = Y; yy <= Statics.NewData.Length - 1; yy++)
					{
						fecherFoundation.Strings.MidSet(ref Statics.NewData, yy, 1, Strings.Mid(Statics.NewData, yy + 1, 1));
					}
					// yy
					fecherFoundation.Strings.MidSet(ref Statics.NewData, Strings.Len(Statics.NewData), 1, " ");
				}
				else if (Strings.UCase(Strings.Mid(Statics.NewData, Y, 1)) == "C")
				{
					Statics.NewData = "0";
					break;
				}
			}
			// Y
			Statics.NewValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.NewData)));
		}

		public static double Round_8(double dValue, short iDigits)
		{
			return Round(ref dValue, ref iDigits);
		}

		public static double Round(ref double dValue, ref short iDigits)
		{
			double Round = 0;
			Round = Conversion.Int(dValue * (Math.Pow(10, iDigits)) + 0.5) / (Math.Pow(10, iDigits));
			return Round;
		}

		public static string Quotes(ref string strValue)
		{
			string Quotes = "";
			Quotes = FCConvert.ToString(Convert.ToChar(34)) + strValue + FCConvert.ToString(Convert.ToChar(34));
			return Quotes;
		}
		// Public Sub LockRecord(lngLowNumber As Long, lngHighNumber As Long, intFileNumber As Integer)
		// On Error Resume Next
		// If lngHighNumber = 0 Then lngHighNumber = lngLowNumber
		// Lock #intFileNumber, lngLowNumber To lngHighNumber
		// LocksHeld(intFileNumber) = LocksHeld(intFileNumber) + 1
		// LowLock(intFileNumber, LocksHeld(intFileNumber)) = lngLowNumber
		// HighLock(intFileNumber, LocksHeld(intFileNumber)) = lngHighNumber
		// Err.Clear
		// End Sub
		//
		// Public Sub UnLockRecord(lngLowNumber As Long, lngHighNumber As Long, intFileNumber As Integer)
		// On Error Resume Next
		// Dim x As Integer
		// Dim Y As Integer
		// If LocksHeld(intFileNumber) <> 0 Then
		// For x = LocksHeld(intFileNumber) To 1 Step -1
		// If LowLock(intFileNumber, x) <> 0 Then
		// Unlock #intFileNumber, LowLock(intFileNumber, x) To HighLock(intFileNumber, x)
		// LowLock(intFileNumber, x) = 0
		// HighLock(intFileNumber, x) = 0
		// End If
		// Next x
		// LocksHeld(intFileNumber) = 0
		// End If
		// Err.Clear
		// End Sub
		public static void UnlockALLRecords()
		{
			/*? On Error Resume Next  */
			try
			{
				int X;
				int Y;
				for (X = 1; X <= 50; X++)
				{
					if (Statics.LocksHeld[X] > 0)
					{
						for (Y = Statics.LocksHeld[X]; Y >= 1; Y--)
						{
							if (Statics.LowLock[X, Y] != 0)
							{
								//Unlock(X, LowLock[X, Y]); /*? To */ HighLock[(X), Y]; /*? ) */
								Statics.LowLock[X, Y] = 0;
								Statics.HighLock[X, Y] = 0;
							}
						}
						// Y
					}
					Statics.LocksHeld[X] = 0;
				}
				// X
				Information.Err().Clear();
			}
			catch
			{
			}
		}

		public static bool ExitTrioNow()
		{
			bool ExitTrioNow = false;
			// vbPorter upgrade warning: Ans As Variant --> As DialogResult
			DialogResult Ans;
			// msgbox '260' = vbyesno with the 'NO' the default button.
			Ans = MessageBox.Show("Would you like to exit TRIO?", "Exit TRIO", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (Ans == DialogResult.Yes)
			{
				ExitTrioNow = true;
				FCFileSystem.FileClose();
				Wisej.Web.Application.Exit();
			}
			else
			{
				ExitTrioNow = false;
			}
			return ExitTrioNow;
		}

		public static void WriteYY()
		{
			modReplaceWorkFiles.EntryFlagFile(true, "GN", true);
		}
		// vbPorter upgrade warning: frm As Form	OnWrite(frmFixProcess, frmPurgeAuditArchive)
		public static void SaveWindowSize(Form frm)
		{
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", FCConvert.ToString(frm.Left));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", FCConvert.ToString(frm.Top));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", FCConvert.ToString(frm.Width));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", FCConvert.ToString(frm.Height));
		}

		public static void GetWindowSize(ref Form frm)
		{
			int lngHeight;
			int lngWidth;
			int lngTop;
			int lngLeft;
			// 
			lngHeight = FCGlobal.Screen.Height;
			lngLeft = 0;
			lngTop = 0;
			lngWidth = FCGlobal.Screen.Width;
			// lngHeight = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", 6500)
			// lngLeft = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", 1000)
			// lngTop = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", 1000)
			// lngWidth = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", 6500)
			if (lngHeight != frm.Height)
			{
				if (lngWidth != frm.Width)
				{
					if (frm.WindowState != FormWindowState.Normal)
					{
						frm.WindowState = FormWindowState.Normal;
					}
					frm.Height = lngHeight;
					frm.Left = lngLeft;
					frm.Top = lngTop;
					frm.Width = lngWidth;
					if (FCGlobal.Screen.Height - frm.Height <= 100)
					{
						if (FCGlobal.Screen.Width - frm.Width <= 100)
						{
							frm.WindowState = FormWindowState.Maximized;
						}
					}
				}
			}
		}

		public static void CloseChildForms()
        {
            foreach (FCForm frm in Wisej.Web.Application.OpenForms)
			{
				if (frm.Name != "MDIParent" && frm.Name != "MainForm")
				{
					if (!Statics.boolLoadHide)
						frm.Unload();
				}
			}
		}

		public static bool GESpecificCode(ref int lngInputCode)
		{
			bool GESpecificCode = false;
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
			DateTime dtDate;
			int lngMo;
			int lngDay;
			int lngYear;
			int lngTemp;
			string strMun;
			int XORNumber;
			string ModName;
			GESpecificCode = false;
			dtDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			ModName = "GE";
			strMun = modGlobalConstants.Statics.MuniName;
			lngMo = DateTime.Today.Month;
			lngDay = DateTime.Today.Day;
			lngYear = DateTime.Today.Year;
			lngYear += 42;
			lngTemp = lngYear + Convert.ToByte(Strings.LCase(Strings.Left(strMun, 2))[0]);
			lngTemp *= 256;
			// shift 8 bits
			lngTemp += (lngMo * 17) + Convert.ToByte(Strings.LCase(Strings.Left(ModName, 1))[0]);
			lngTemp += (((lngDay + 2) + Convert.ToByte(Strings.UCase(Strings.Right(ModName, 1))[0])) * 512);
			XORNumber = 113;
			XORNumber = (XORNumber * 256) + 71;
			lngTemp = lngTemp ^ XORNumber;
			if (lngTemp == lngInputCode)
				GESpecificCode = true;
			return GESpecificCode;
		}

		public static string GetFont_6(string strPrinterName, short intCPI, string strFont = "")
		{
			return GetFont(ref strPrinterName, ref intCPI, strFont);
		}

		public static string GetFont(ref string strPrinterName, ref short intCPI, string strFont = "")
		{
			string GetFont = "";
			// searches the named printer for the needed font
			int X;
			// loop counter
			string strTempFont = "";
			string strCPIFont;
			GetFont = "";
			FCPrinter printer = null;
			// must do it this way since an activereports printer object can't access the fonts
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				if (Strings.UCase(p.DeviceName) == Strings.UCase(strPrinterName))
				{
					printer = p;
					break;
				}
			}
			strCPIFont = "";
			for (X = 0; X <= printer.FontCount - 1; X++)
			{
				strTempFont = FCGlobal.Printer.Fonts[X];
				;
				if (Strings.UCase(Strings.Right(strTempFont, 3)) == "CPI")
				{
					strTempFont = Strings.Mid(strTempFont, 1, strTempFont.Length - 3);
					if (Conversion.Val(Strings.Right(strTempFont, 2)) == intCPI || Conversion.Val(Strings.Right(strTempFont, 3)) == intCPI)
					{
						strCPIFont = FCGlobal.Printer.Fonts[X];
						;
						// strPrinterFont = strCPIFont
						if (strFont != string.Empty)
						{
							if (Strings.InStr(1, strCPIFont, strFont, CompareConstants.vbTextCompare) > 0)
							{
								// got the right one
								break;
							}
						}
						else
						{
							break;
						}
					}
				}
			}
			// X
			GetFont = strCPIFont;
			return GetFont;
		}

		public class StaticVariables
		{
			public string gstrCurrentRoutine = "";
			public clsTrioSecurity clsGESecurity = new clsTrioSecurity();
			public bool boolChangedWithoutSave;
			public string gstrPrintType = "";
			public bool boolLoadHide;
			// vbPorter upgrade warning: gobjFormName As object	OnRead(Form)
			public Form gobjFormName;
			// vbPorter upgrade warning: gobjLabel As object --> As FCLabel
			public FCLabel gobjLabel;
			public bool boolPasswordGood;
			public clsDRWrapper secDB_AutoInitialized = null;
			public clsDRWrapper secDB
			{
				get
				{
					if ( secDB_AutoInitialized == null)
					{
						 secDB_AutoInitialized = new clsDRWrapper();
					}
					return secDB_AutoInitialized;
				}
				set
				{
					 secDB_AutoInitialized = value;
				}
			}
			// Public rsVariables As New clsDRWrapper
			public string gstrPassword = "";
			// Public gstrUserID As String
			public int gstrUserIndexID;
			public string gstrBackupPath = "";
			public string gstrFilePath = string.Empty;
			public string gstrFileN = string.Empty;
			public bool FormLoading;
			public int[] SpecLength = new int[10 + 1];
			public string[] SpecCase = new string[10 + 1];
			public string[] SpecKeyword = new string[10 + 1];
			public string[] SpecM1 = new string[10 + 1];
			public string[] SpecM2 = new string[10 + 1];
			public string[] SpecM3 = new string[10 + 1];
			public string[] SpecM4 = new string[10 + 1];
			public string[] SpecM5 = new string[10 + 1];
			public string[] SpecM6 = new string[10 + 1];
			public string[] SpecM7 = new string[10 + 1];
			public string[] SpecM8 = new string[10 + 1];
			public string[] SpecM9 = new string[10 + 1];
			public string[] SpecM10 = new string[10 + 1];
			public string[] SpecType = new string[10 + 1];
			public string[] SpecParam = new string[10 + 1];
			// vbPorter upgrade warning: SpecLow As string	OnWriteFCConvert.ToInt16(
			public string[] SpecLow = new string[10 + 1];
			// vbPorter upgrade warning: SpecHigh As string	OnWriteFCConvert.ToInt16(
			public string[] SpecHigh = new string[10 + 1];
			public int RespLength;
			public string RespCase = string.Empty;
			public string RespM1 = string.Empty;
			public string RespM2 = string.Empty;
			public string RespM3 = string.Empty;
			public string RespM4 = string.Empty;
			public string RespM5 = string.Empty;
			public string RespM6 = string.Empty;
			public string RespM7 = string.Empty;
			public string RespM8 = string.Empty;
			public string RespM9 = string.Empty;
			public string RespM10 = string.Empty;
			public string RespOK = "";
			public string RespType = string.Empty;
			public string RespParam = string.Empty;
			public string RespLow = string.Empty;
			public string RespHigh = string.Empty;
			public object response;
			// vbPorter upgrade warning: RespKeyword As Variant --> As string
			public string RespKeyword = string.Empty;
			public string Resp1 = string.Empty;
			// vbPorter upgrade warning: Resp2 As Variant --> As int
			public int Resp2;
			public object Resp3;
			public object Resp4;
			public object Resp5;
			public object Resp6;
			public object Resp7;
			public object Resp8;
			public object Resp9;
			public object Resp10;
			public bool SystemSetup;
			public bool LinkPicture;
			// Message Box Response Variables
			// These are used to hold a Message Box Response
			public object MessageResp1;
			public object MessageResp2;
			public object MessageResp3;
			// Prefixes and Variables used for Pictures and Sketches
			public string PictureFormat = string.Empty;
			public string PictureLocation = "";
			public string DocumentLocationName = "";
			public string SketchLocation = string.Empty;
			// record locking fields
			public int LockFileNumber;
			public int LowLockNumber;
			public int HighLockNumber;
			public int[,] LowLock = new int[50 + 1, 20 + 1];
			public int[,] HighLock = new int[50 + 1, 20 + 1];
			public int[] LocksHeld = new int[50 + 1];
			// Prefixes for open statements
			public string PPDatabase = string.Empty;
			public string TrioEXE = string.Empty;
			public string TrioDATA = string.Empty;
			public string TrioSDrive = "";
			// File Editting copying, moving, deleting
			// screen setup items
			const string Logo = "TRIO Software Corporation";
			public string Application = "";
			public string ProgramName = "";
			public string ProgramShort = "";
			public string TitleCaption = string.Empty;
			public string TitleTime = string.Empty;
			public string Ltime = "";
			// used in format print statements
			public int fmtlgth;
			public int fmtval;
			public string fmttype = "";
			public string fmtcolon = "";
			//new char[1];
			public int fmtspcbef;
			public int fmtspcaft;
			// printers
			// Public RegularPrinter       As String
			// Public PrintWidth           As String
			// Public ReceiptPrinter       As String
			// Public ReceiptWidth         As String
			// Public CheckPrinter         As String
			// Public LabelPrinter         As String
			// Public Mvr3Printer          As String
			// Public CtaPrinter           As String
			// used in GNMenu
			public string MenuTitle = "";
			public string MenuShort = "";
			public int MenuCount;
			public char[] MenuOptions = new char[19];
			public string[] MenuTitles = new string[19 + 1];
			public char[] MenuSelected = new char[1];
			// used in ConvertToString
			public double IntvarNumeric;
			public string INTVARSTRING = string.Empty;
			public string IntVarTemp = string.Empty;
			public int intLen;
			public string newfld = "";
			// used by search routines
			public string SEQ = "";
			public int House;
			public string SEARCH = "";
			public bool LongScreenSearch;
			// used for insert/overtype mode in text boxes
			public int OverType;
			// used in getvalue
			public string NewData = "";
			public int NewValue;
			//
			public char[] EntryMenuResponse = new char[2];
			public char[] MainMenuResponse = new char[1];
			public char[] MenuResponse = new char[1];
			//
			public char[] CurrentApplication = new char[1];
			public string Escape = "";
			public int CommentAccount;
			public string CommentName = "";
			public int X;
			public string ABORT = "";
			public string Bbort = "";
			public string SortFileSpec = "";
			public int NextNumber;
			public bool SketcherCalled;
			public const string SearchInputBoxTitle = "Real Estate Assessment Search";
			// Public wrkworkspace As Workspace
			//=========================================================
			public bool boolCancelled;
			public int Bp;
			// Public Const CB_SHOWDROPDOWN = &H14F
			public string Display = string.Empty;
			public bool EntryMenuFlag;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
