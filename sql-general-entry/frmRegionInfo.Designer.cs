﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRegionInfo.
	/// </summary>
	partial class frmRegionInfo : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public FCGrid gridDeletedBreakdown;
		public FCGrid gridBreakdown;
		public fecherFoundation.FCFrame framRegion;
		public FCGrid GridRegion;
		public FCGrid GridRegionAccounts;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddCode;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCode;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegionInfo));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.gridDeletedBreakdown = new fecherFoundation.FCGrid();
			this.gridBreakdown = new fecherFoundation.FCGrid();
			this.framRegion = new fecherFoundation.FCFrame();
			this.GridRegion = new fecherFoundation.FCGrid();
			this.GridRegionAccounts = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridDeletedBreakdown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridBreakdown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framRegion)).BeginInit();
			this.framRegion.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridRegion)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridRegionAccounts)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 578);
			this.BottomPanel.Size = new System.Drawing.Size(835, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.framRegion);
			this.ClientArea.Size = new System.Drawing.Size(835, 518);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(835, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(181, 30);
			this.HeaderText.Text = "Regional Setup";
			// 
			// Frame1
			// 
			this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Frame1.Controls.Add(this.gridDeletedBreakdown);
			this.Frame1.Controls.Add(this.gridBreakdown);
			this.Frame1.Location = new System.Drawing.Point(30, 337);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(664, 178);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Breakdown";
			// 
			// gridDeletedBreakdown
			// 
			this.gridDeletedBreakdown.AllowSelection = false;
			this.gridDeletedBreakdown.AllowUserToResizeColumns = false;
			this.gridDeletedBreakdown.AllowUserToResizeRows = false;
			this.gridDeletedBreakdown.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridDeletedBreakdown.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.BackColorBkg = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.BackColorFixed = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.BackColorSel = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridDeletedBreakdown.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridDeletedBreakdown.ColumnHeadersHeight = 30;
			this.gridDeletedBreakdown.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridDeletedBreakdown.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridDeletedBreakdown.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridDeletedBreakdown.DragIcon = null;
			this.gridDeletedBreakdown.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridDeletedBreakdown.FixedCols = 0;
			this.gridDeletedBreakdown.FixedRows = 0;
			this.gridDeletedBreakdown.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.FrozenCols = 0;
			this.gridDeletedBreakdown.GridColor = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.GridColorFixed = System.Drawing.Color.Empty;
			this.gridDeletedBreakdown.Location = new System.Drawing.Point(104, 13);
			this.gridDeletedBreakdown.Name = "gridDeletedBreakdown";
			this.gridDeletedBreakdown.ReadOnly = true;
			this.gridDeletedBreakdown.RowHeadersVisible = false;
			this.gridDeletedBreakdown.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridDeletedBreakdown.RowHeightMin = 0;
			this.gridDeletedBreakdown.Rows = 0;
			this.gridDeletedBreakdown.ScrollTipText = null;
			this.gridDeletedBreakdown.ShowColumnVisibilityMenu = false;
			this.gridDeletedBreakdown.Size = new System.Drawing.Size(5, 2);
			this.gridDeletedBreakdown.StandardTab = true;
			this.gridDeletedBreakdown.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridDeletedBreakdown.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridDeletedBreakdown.TabIndex = 1;
			this.gridDeletedBreakdown.Visible = false;
			// 
			// gridBreakdown
			// 
			this.gridBreakdown.AllowSelection = false;
			this.gridBreakdown.AllowUserToResizeColumns = false;
			this.gridBreakdown.AllowUserToResizeRows = false;
			this.gridBreakdown.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridBreakdown.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridBreakdown.BackColorBkg = System.Drawing.Color.Empty;
			this.gridBreakdown.BackColorFixed = System.Drawing.Color.Empty;
			this.gridBreakdown.BackColorSel = System.Drawing.Color.Empty;
			this.gridBreakdown.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridBreakdown.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.gridBreakdown.ColumnHeadersHeight = 30;
			this.gridBreakdown.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridBreakdown.DefaultCellStyle = dataGridViewCellStyle4;
			this.gridBreakdown.DragIcon = null;
			this.gridBreakdown.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridBreakdown.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridBreakdown.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridBreakdown.FrozenCols = 0;
			this.gridBreakdown.GridColor = System.Drawing.Color.Empty;
			this.gridBreakdown.GridColorFixed = System.Drawing.Color.Empty;
			this.gridBreakdown.Location = new System.Drawing.Point(21, 27);
			this.gridBreakdown.Name = "gridBreakdown";
			this.gridBreakdown.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridBreakdown.RowHeightMin = 0;
			this.gridBreakdown.Rows = 2;
			this.gridBreakdown.ScrollTipText = null;
			this.gridBreakdown.ShowColumnVisibilityMenu = false;
			this.gridBreakdown.Size = new System.Drawing.Size(606, 145);
			this.gridBreakdown.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridBreakdown.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.gridBreakdown.TabIndex = 0;
			this.gridBreakdown.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridBreakdown_ValidateEdit);
			// 
			// framRegion
			// 
			this.framRegion.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.framRegion.Controls.Add(this.GridRegion);
			this.framRegion.Controls.Add(this.GridRegionAccounts);
			this.framRegion.Location = new System.Drawing.Point(30, 30);
			this.framRegion.Name = "framRegion";
			this.framRegion.Size = new System.Drawing.Size(786, 290);
			this.framRegion.TabIndex = 0;
			// 
			// GridRegion
			// 
			this.GridRegion.AllowSelection = false;
			this.GridRegion.AllowUserToResizeColumns = false;
			this.GridRegion.AllowUserToResizeRows = false;
			this.GridRegion.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridRegion.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridRegion.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridRegion.BackColorBkg = System.Drawing.Color.Empty;
			this.GridRegion.BackColorFixed = System.Drawing.Color.Empty;
			this.GridRegion.BackColorSel = System.Drawing.Color.Empty;
			this.GridRegion.Cols = 4;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridRegion.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.GridRegion.ColumnHeadersHeight = 30;
			this.GridRegion.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridRegion.DefaultCellStyle = dataGridViewCellStyle6;
			this.GridRegion.DragIcon = null;
			this.GridRegion.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridRegion.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridRegion.ExtendLastCol = true;
			this.GridRegion.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridRegion.FrozenCols = 0;
			this.GridRegion.GridColor = System.Drawing.Color.Empty;
			this.GridRegion.GridColorFixed = System.Drawing.Color.Empty;
			this.GridRegion.Location = new System.Drawing.Point(20, 30);
			this.GridRegion.Name = "GridRegion";
			this.GridRegion.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridRegion.RowHeightMin = 0;
			this.GridRegion.Rows = 2;
			this.GridRegion.ScrollTipText = null;
			this.GridRegion.ShowColumnVisibilityMenu = false;
			this.GridRegion.Size = new System.Drawing.Size(744, 122);
			this.GridRegion.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridRegion.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridRegion.TabIndex = 0;
			// 
			// GridRegionAccounts
			// 
			this.GridRegionAccounts.AllowSelection = false;
			this.GridRegionAccounts.AllowUserToResizeColumns = false;
			this.GridRegionAccounts.AllowUserToResizeRows = false;
			this.GridRegionAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridRegionAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridRegionAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridRegionAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.GridRegionAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.GridRegionAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.GridRegionAccounts.Cols = 8;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridRegionAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.GridRegionAccounts.ColumnHeadersHeight = 30;
			this.GridRegionAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridRegionAccounts.DefaultCellStyle = dataGridViewCellStyle8;
			this.GridRegionAccounts.DragIcon = null;
			this.GridRegionAccounts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridRegionAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridRegionAccounts.ExtendLastCol = true;
			this.GridRegionAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridRegionAccounts.FrozenCols = 0;
			this.GridRegionAccounts.GridColor = System.Drawing.Color.Empty;
			this.GridRegionAccounts.GridColorFixed = System.Drawing.Color.Empty;
			this.GridRegionAccounts.Location = new System.Drawing.Point(20, 162);
			this.GridRegionAccounts.Name = "GridRegionAccounts";
			this.GridRegionAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridRegionAccounts.RowHeightMin = 0;
			this.GridRegionAccounts.Rows = 2;
			this.GridRegionAccounts.ScrollTipText = null;
			this.GridRegionAccounts.ShowColumnVisibilityMenu = false;
			this.GridRegionAccounts.Size = new System.Drawing.Size(744, 110);
			this.GridRegionAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridRegionAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridRegionAccounts.TabIndex = 1;
			this.GridRegionAccounts.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(GridRegionAccounts_EditingControlShowing);
			//this.GridRegionAccounts.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridRegionAccounts_KeyDownEdit);
			//this.GridRegionAccounts.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.GridRegionAccounts_KeyPressEdit);
			//this.GridRegionAccounts.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.GridRegionAccounts_KeyUpEdit);
			this.GridRegionAccounts.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridRegionAccounts_BeforeEdit);
			this.GridRegionAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridRegionAccounts_ValidateEdit);
			this.GridRegionAccounts.CurrentCellChanged += new System.EventHandler(this.GridRegionAccounts_RowColChange);
			this.GridRegionAccounts.Enter += new System.EventHandler(this.GridRegionAccounts_Enter);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddCode,
				this.mnuDeleteCode,
				this.mnuSepar2,
				this.mnuSave,
				this.mnuSaveExit,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddCode
			// 
			this.mnuAddCode.Index = 0;
			this.mnuAddCode.Name = "mnuAddCode";
			this.mnuAddCode.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
			this.mnuAddCode.Text = "Add Breakdown Code";
			this.mnuAddCode.Click += new System.EventHandler(this.mnuAddCode_Click);
			// 
			// mnuDeleteCode
			// 
			this.mnuDeleteCode.Index = 1;
			this.mnuDeleteCode.Name = "mnuDeleteCode";
			this.mnuDeleteCode.Shortcut = Wisej.Web.Shortcut.ShiftDelete;
			this.mnuDeleteCode.Text = "Delete Breakdown Code";
			this.mnuDeleteCode.Click += new System.EventHandler(this.mnuDeleteCode_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 2;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 4;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 5;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmRegionInfo
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(835, 686);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmRegionInfo";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Regional Setup";
			this.Load += new System.EventHandler(this.frmRegionInfo_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRegionInfo_KeyDown);
			this.Resize += new System.EventHandler(this.frmRegionInfo_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridDeletedBreakdown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridBreakdown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framRegion)).EndInit();
			this.framRegion.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridRegion)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridRegionAccounts)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
