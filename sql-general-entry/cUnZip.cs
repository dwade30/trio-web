//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;
using System.Runtime.InteropServices;

using System.IO;

namespace TWGNENTY
{
	public class cUnzip
	{

		//=========================================================

		// ======================================================================================

		// ======================================================================================

		public enum EUZMsgLevel
		{
			euzAllMessages = 0,
			euzPartialMessages = 1,
			euzNoMessages = 2
		};
		public enum EUZOverWriteResponse
		{
			euzDoNotOverwrite = 100,
			euzOverwriteThisFile = 102,
			euzOverwriteAllFiles = 103,
			euzOverwriteNone = 104
		};

		private string m_sZipFile = "";
		private string m_sUnzipFolder = "";

		private struct tZipContents
		{
			public string sName;
			public string sFolder;
			public int lSize;
			public int lPackedSize;
			public int lFactor;
			public string sMethod;
			public DateTime dDate;
			public int lCrc;
			public bool fEncryped;
			public bool fSelected;
		};
		private tZipContents[] m_tZipContents = null;
		private int m_iCount;
		private mUnzip.DCLIST m_tDCL = new mUnzip.DCLIST();

		public delegate void CancelEventHandler(string sMsg, ref bool bCancel);
		public event CancelEventHandler Cancel;
		public delegate void OverwritePromptEventHandler(string sFIle, ref EUZOverWriteResponse eResponse);
		public event OverwritePromptEventHandler OverwritePrompt;
		public delegate void PasswordRequestEventHandler(ref string sPassword, ref bool bCancel);
		public event PasswordRequestEventHandler Event_PasswordRequest;
		public delegate void ProgressEventHandler(int lCount, string sMsg);
		public event ProgressEventHandler Progress;

		public bool ExtractOnlyNewer
		{
			get
			{
					bool ExtractOnlyNewer = false;
				ExtractOnlyNewer = (m_tDCL.ExtractOnlyNewer!=0); // 1=extract only newer
				return ExtractOnlyNewer;
			}

			set
			{
				m_tDCL.ExtractOnlyNewer = Math.Abs((value ? -1 : 0)); // 1=extract only newer
			}
		}

		public bool SpaceToUnderScore
		{
			get
			{
					bool SpaceToUnderScore = false;
				SpaceToUnderScore = (m_tDCL.SpaceToUnderScore!=0); // 1=convert space to underscore
				return SpaceToUnderScore;
			}

			set
			{
				m_tDCL.SpaceToUnderScore = Math.Abs((value ? -1 : 0));
			}
		}

		public bool PromptToOverwrite
		{
			get
			{
					bool PromptToOverwrite = false;
				PromptToOverwrite = (m_tDCL.PromptToOverwrite!=0);
				return PromptToOverwrite;
			}

			set
			{
				m_tDCL.PromptToOverwrite = Math.Abs((value ? -1 : 0));
			}
		}

		public EUZMsgLevel MessageLevel
		{
			get
			{
					EUZMsgLevel MessageLevel = (EUZMsgLevel)0;
				MessageLevel = (EUZMsgLevel)m_tDCL.fQuiet;
				return MessageLevel;
			}

			set
			{
				m_tDCL.fQuiet = (int)value;
			}
		}

		public bool TestZip
		{
			get
			{
					bool TestZip = false;
				TestZip = (m_tDCL.ntflag!=0);
				return TestZip;
			}

			set
			{
				m_tDCL.ntflag = Math.Abs((value ? -1 : 0));
			}
		}

		public bool UseFolderNames
		{
			get
			{
					bool UseFolderNames = false;
				UseFolderNames = (m_tDCL.ndflag!=0);
				return UseFolderNames;
			}

			set
			{
				m_tDCL.ndflag = Math.Abs((value ? -1 : 0));
			}
		}

		public bool OverwriteExisting
		{
			get
			{
					bool OverwriteExisting = false;
				OverwriteExisting = (m_tDCL.noflag!=0);
				return OverwriteExisting;
			}

			set
			{
				m_tDCL.noflag = Math.Abs((value ? -1 : 0));
			}
		}

		public bool ConvertCRToCRLF
		{
			get
			{
					bool ConvertCRToCRLF = false;
				ConvertCRToCRLF = (m_tDCL.naflag!=0);
				return ConvertCRToCRLF;
			}

			set
			{
				m_tDCL.naflag = Math.Abs((value ? -1 : 0));
			}
		}

		// vbPorter upgrade warning: 'Return' As bool	OnWrite(int)
		public bool CaseSensitiveFileNames
		{
			get
			{
					bool CaseSensitiveFileNames = false;
				CaseSensitiveFileNames = Convert.ToBoolean(m_tDCL.C_flag);
				return CaseSensitiveFileNames;
			}

			set
			{
				m_tDCL.C_flag = Math.Abs((value ? -1 : 0));
			}
		}


		public void DirectoryListAddFile(string sFileName, string sFolder, DateTime dDate, int lSize, int lCrc, bool fEncrypted, int lFactor, string sMethod)
		{
			if ((sFileName!="\0") && sFileName.Length>0) {
				m_iCount += 1;
				Array.Resize(ref m_tZipContents, m_iCount + 1);

				m_tZipContents[m_iCount].sName = sFileName;
				m_tZipContents[m_iCount].sFolder = sFolder;
				m_tZipContents[m_iCount].dDate = dDate;
				m_tZipContents[m_iCount].lSize = lSize;
				m_tZipContents[m_iCount].lCrc = lCrc;
				m_tZipContents[m_iCount].lFactor = lFactor;
				m_tZipContents[m_iCount].sMethod = sMethod;
				m_tZipContents[m_iCount].fEncryped = fEncrypted;
				// Default to selected:
				m_tZipContents[m_iCount].fSelected = true;

			}
		}
		public void OverwriteRequest(string sFIle, ref EUZOverWriteResponse eResponse)
		{
			if (this.OverwritePrompt != null) this.OverwritePrompt(sFIle, ref eResponse);
		}
		public void ProgressReport(string sMsg)
		{
			if (this.Progress != null) this.Progress(1, sMsg);
		}
		public void Service(string sMsg, ref bool bCancel)
		{
			if (this.Cancel != null) this.Cancel(sMsg, ref bCancel);
		}

		public void PasswordRequest(ref string sPassword, ref bool bCancel)
		{
			if (this.Event_PasswordRequest != null) this.Event_PasswordRequest(ref sPassword, ref bCancel);
		}
		public int FileCount
		{
			get
			{
					int FileCount = 0;
				FileCount = m_iCount;
				return FileCount;
			}
		}
		public string Get_Filename(int lIndex)
		{
				string Filename = "";
			Filename = m_tZipContents[lIndex].sName;
			return Filename;
		}
		public string Get_FileDirectory(int lIndex)
		{
				string FileDirectory = "";
			FileDirectory = m_tZipContents[lIndex].sFolder;
			return FileDirectory;
		}
		public int Get_FileSize(int lIndex)
		{
				int FileSize = 0;
			FileSize = m_tZipContents[lIndex].lSize;
			return FileSize;
		}
		public int Get_FilePackedSize(int lIndex)
		{
				int FilePackedSize = 0;
			FilePackedSize = m_tZipContents[lIndex].lSize*m_tZipContents[lIndex].lFactor / 100;
			return FilePackedSize;
		}
		public int Get_FileCompressionRatio(int lIndex)
		{
				int FileCompressionRatio = 0;
			FileCompressionRatio = m_tZipContents[lIndex].lFactor;
			return FileCompressionRatio;
		}
		public DateTime Get_FileDate(int lIndex)
		{
				DateTime FileDate = System.DateTime.Now;
			FileDate = m_tZipContents[lIndex].dDate;
			return FileDate;
		}
		public int Get_FileCRC(int lIndex)
		{
				int FileCRC = 0;
			FileCRC = m_tZipContents[lIndex].lCrc;
			return FileCRC;
		}
		public string Get_FileCompressionMethod(int lIndex)
		{
				string FileCompressionMethod = "";
			FileCompressionMethod = m_tZipContents[lIndex].sMethod;
			return FileCompressionMethod;
		}
		public bool Get_FileEncrypted(int lIndex)
		{
				bool FileEncrypted = false;
			FileEncrypted = m_tZipContents[lIndex].fEncryped;
			return FileEncrypted;
		}
		public bool Get_FileSelected(int lIndex)
		{
				bool FileSelected = false;
			FileSelected = m_tZipContents[lIndex].fSelected;
			return FileSelected;
		}
		public void Set_FileSelected(int lIndex, bool bState)
		{
			m_tZipContents[lIndex].fSelected = bState;
		}
		public int Directory()
		{
			int Directory = 0;
			string []s = new string[0 + 1];
			m_tDCL.lpszZipFN = m_sZipFile;
			m_tDCL.lpszExtractDir = "\0";
			m_tDCL.nvflag = 1;
			mUnzip.VBUnzip(ref this, ref m_tDCL, 0, ref s, 0, ref s);
			return Directory;
		}

		public bool Unzip(ref FCLabel Label3, ref Form FormName, ref FCProgressBar pbrProgress, ref FCLabel lblPercent)
		{
			bool Unzip = false;
			
			string[] sInc = null;
			int iIncCount = 0;
			string[] s = null;
			int i;
			if (m_sZipFile!="") {
				if (m_iCount>0) {
					pbrProgress.Maximum = m_iCount;
					FormName.Refresh();

					for(i=1; i<=m_iCount; i++) {
						if (m_tZipContents[i].fSelected) {
							Label3.Text = Label3.Tag+" "+m_tZipContents[i].sName;
							FormName.Refresh();

							// rename the file
							if (ff.FileExists(modBackupReplace.strDriveLetter+":\\"+m_tZipContents[i].sName)) {
								if (Strings.UCase(Strings.Right(m_tZipContents[i].sName, 3))=="EXE" || Strings.LCase(Strings.Right(m_tZipContents[i].sName, 3))=="dll") {
									// gstrBackupPath = Left(CurDir, 1) & ":\TrioData\TrioMast"
									modGNBas.gstrBackupPath = modGlobalFunctions.gGlobalSettings.MasterPath;
								} else {
									modGNBas.gstrBackupPath = Environment.CurrentDirectory;
								}
								modGNBas.gstrBackupPath = (Strings.Right(modGNBas.gstrBackupPath, 1)!="\\" ? modGNBas.gstrBackupPath+"\\" : modGNBas.gstrBackupPath);
								// 
								ff.CopyFile(modBackupReplace.strDriveLetter+":\\"+m_tZipContents[i].sName, modGNBas.gstrBackupPath+m_tZipContents[i].sName+".old", true);
							}

							iIncCount += 1;
							Array.Resize(ref sInc, iIncCount + 1);
							sInc[iIncCount] = ReverseSlashes(m_tZipContents[i].sFolder, m_tZipContents[i].sName);
						}

						lblPercent.Text = Convert.ToString(Convert.ToInt16((pbrProgress/m_iCount)*100))+"%";
						if (pbrProgress!=pbrProgress.Maximum) pbrProgress.Value = pbrProgress.Value+1;
						FormName.Refresh();
					} // i
					if (iIncCount==m_iCount) {
						iIncCount = 0;
						sInc = new string[0 + 1];
					}
				}
				m_tDCL.lpszZipFN = m_sZipFile;
				m_tDCL.nvflag = 0;
				m_tDCL.lpszExtractDir = m_sUnzipFolder;
				Unzip = (mUnzip.VBUnzip(ref Me, ref m_tDCL, ref iIncCount, ref sInc, 0, ref s)!=0);
			}
			return Unzip;
		}

		private string ReverseSlashes(string sFolder, string sFIle)
		{
			string ReverseSlashes = "";
			string sOut = "";
			int iPos = 0, iLastPos = 0;

			if (sFolder.Length>0 && sFolder!="\0") {
				sOut = sFolder+"/"+sFIle;
				iLastPos = 1;
				do {
					iPos = Strings.InStr(iLastPos, sOut, "\\", CompareConstants.vbBinaryCompare);
					if (iPos!=0) {
						fecherFoundation.Strings.MidSet(ref sOut, iPos, "/", 1);
						iLastPos = iPos+1;
					}
				} while (iPos!=0);
				ReverseSlashes = sOut;
			} else {
				ReverseSlashes = sFIle;
			}
			return ReverseSlashes;
		}
		public string UnzipFolder
		{
			set
			{
				m_sUnzipFolder = value;
			}

			get
			{
					string UnzipFolder = "";
				UnzipFolder = m_sUnzipFolder;
				m_tDCL.lpszExtractDir = m_sUnzipFolder;
				return UnzipFolder;
			}
		}

		public string ZipFile
		{
			get
			{
					string ZipFile = "";
				ZipFile = m_sZipFile;
				return ZipFile;
			}

			set
			{
				m_sZipFile = value;
				m_iCount = 0;
				FCUtils.EraseSafe(m_tZipContents);
			}
		}
		// vbPorter upgrade warning: lMajor As int	OnWrite(byte)
		// vbPorter upgrade warning: lMinor As int	OnWrite(byte)
		// vbPorter upgrade warning: lRevision As int	OnWrite(byte)
		// vbPorter upgrade warning: sExtraInfo As string	OnWrite(FixedString)
		public void GetVersion(ref int lMajor, ref int lMinor, ref int lRevision, ref DateTime dDate, ref string sExtraInfo)
		{
			mUnzip.UZPVER tVer = new mUnzip.UZPVER();
			int iPos;

			// Set Version space

			tVer.structlen = Marshal.SizeOf(tVer);
			tVer.betalevel = (Strings.Space(9)+"\0");
			tVer.Date = (Strings.Space(19)+"\0");
			tVer.zlib = (Strings.Space(9)+"\0");


			// Get version
			mUnzip.UzpVersion2(ref tVer);
			iPos = Strings.InStr(new string(tVer.betalevel), "\0", CompareConstants.vbBinaryCompare);
			if (iPos>1) {
				tVer.betalevel = Strings.Left(new string(tVer.betalevel), iPos-1).ToCharArray();
			}
			sExtraInfo = new string(tVer.betalevel);

			// Date..
			Debug.WriteLine(tVer.Date);

			lMajor = tVer.windll.major;
			lMinor = tVer.windll.minor;
			lRevision = tVer.windll.patchlevel;

		}





	}
}