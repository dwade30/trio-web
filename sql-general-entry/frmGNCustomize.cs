﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using SharedApplication.CentralData;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmGNCustomize.
	/// </summary>
	public partial class frmGNCustomize : BaseForm
	{
		public frmGNCustomize()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtBulkMail = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtBulkMail.AddControlArrayElement(txtBulkMail_5, 5);
			this.txtBulkMail.AddControlArrayElement(txtBulkMail_4, 4);
			this.txtBulkMail.AddControlArrayElement(txtBulkMail_3, 3);
			this.txtBulkMail.AddControlArrayElement(txtBulkMail_2, 2);
			this.txtBulkMail.AddControlArrayElement(txtBulkMail_1, 1);
			//FC:FINAL:RPU:#1244 - Set FormatCaption property to false and Set the correct text
			Frame1.FormatCaption = false;
			Frame1.Text = "SFTP Site Information";
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGNCustomize InstancePtr
		{
			get
			{
				return (frmGNCustomize)Sys.GetInstance(typeof(frmGNCustomize));
			}
		}

		protected frmGNCustomize _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/26/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/26/2004              *
		// ********************************************************
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		string strPath = "";
		const int CNSTMUNITYPECITY = 0;
		const int CNSTMUNITYPETOWN = 1;
		const int CNSTMUNITYPEOTHER = 2;

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// Dim strPath             As String
				MDIParent.InstancePtr.CommonDialog1.Filter = "*.jpg||*.gif";
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//Fc:FINAL:SBE - upload file to server, and use uploaded path
				//MDIParent.InstancePtr.CommonDialog1.ShowSave();
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strPath = MDIParent.InstancePtr.CommonDialog1.FileName;
				txtTownSeal.Text = Strings.Trim(strPath);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				if (!MDIParent.InstancePtr.CommonDialog1.CancelError)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Browse Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void frmGNCustomize_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				LoadSettings();
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmGNCustomize_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupcmbCityTown();
		}

		private void frmGNCustomize_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			rsSettings = null;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
		}

		private void SetupcmbCityTown()
		{
			cmbCityTown.Clear();
			cmbCityTown.AddItem("City");
			cmbCityTown.ItemData(cmbCityTown.NewIndex, CNSTMUNITYPECITY);
			cmbCityTown.AddItem("Town");
			cmbCityTown.ItemData(cmbCityTown.NewIndex, CNSTMUNITYPETOWN);
			cmbCityTown.AddItem("Corporation or other entity");
			cmbCityTown.ItemData(cmbCityTown.NewIndex, CNSTMUNITYPEOTHER);
			cmbCityTown.SelectedIndex = 1;
			// default to town
		}

		private bool SaveSettings()
		{
			bool SaveSettings = false;
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int x;
				string strUpdate = "";
				string strSQL = "";
				clsDRWrapper clsSave = new clsDRWrapper();
				// this will save all of the settings for Collections
				SaveSettings = false;
				intError = 1;
				rsSettings.Edit();
				intError = 2;
                var city = Strings.UCase(cmbCityTown.Text);

                switch (city)
                {
                    case "CITY":
                        rsSettings.Set_Fields("CityTown", "City");

                        break;
                    case "TOWN":
                        rsSettings.Set_Fields("CityTown", "Town");

                        break;
                    default:
                        rsSettings.Set_Fields("CityTown", " ");

                        break;
                }
				modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(rsSettings.Get_Fields_String("citytown"));
				if (modGlobalConstants.Statics.gstrCityTown != string.Empty)
				{
					modTrioStart.Statics.strCityTown = modGlobalConstants.Statics.gstrCityTown + " of " + modTrioStart.Statics.MuninameToShow;
				}
				else
				{
					modTrioStart.Statics.strCityTown = modTrioStart.Statics.MuninameToShow;
				}
				modTrioStart.Statics.CityTown = modGlobalConstants.Statics.gstrCityTown;
				if (Strings.Trim(txtTownSeal.Text) != "")
				{
					if (modGlobalConstants.Statics.gstrTownSealPath != strPath)
					{
						if (Strings.Trim(SetTownSealFile_2(Strings.Trim(txtTownSeal.Text))) == string.Empty)
						{
							intError = 8;
							MessageBox.Show("Town Seal file not saved correctly.", "Town Seal File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							intError = 9;
							modGlobalFunctions.AddCYAEntry_26("GN", "Saved new Town Seal file.", (Strings.Trim(txtTownSeal.Text)));
						}
					}
				}
				intError = 20;
				rsSettings.Set_Fields("BulkMail1", txtBulkMail[1].Text);
				rsSettings.Set_Fields("BulkMail2", txtBulkMail[2].Text);
				rsSettings.Set_Fields("BulkMail3", txtBulkMail[3].Text);
				rsSettings.Set_Fields("BulkMail4", txtBulkMail[4].Text);
				rsSettings.Set_Fields("BulkMail5", txtBulkMail[5].Text);
				rsSettings.Set_Fields("FTPSiteUser", $"{Strings.Trim(txtFTPUser.Text)}");  // do not add '@trio' here...will be added at usage
				rsSettings.Set_Fields("FTPSitePassword", Strings.Trim(txtFTPPassword.Text));
				if (cmbYes.SelectedIndex == 0)
				{
					// rsSettings.Fields("UpdateNotification") = True
					modRegistry.SaveRegistryKey("UpdateNotification", FCConvert.ToString(true));
				}
				else
				{
					// rsSettings.Fields("UpdateNotification") = False
					modRegistry.SaveRegistryKey("UpdateNotification", FCConvert.ToString(false));
				}
				if (cmbCentralPartyCheckNo.SelectedIndex == 0)
				{
					rsSettings.Set_Fields("CentralPartyCheck", true);
				}
				else
				{
					rsSettings.Set_Fields("CentralPartyCheck", false);
				}
				rsSettings.Update();
				SaveSettings = true;
				MessageBox.Show("Save Successful.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveSettings;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Settings - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveSettings;
		}

		private string SetTownSealFile_2(string strPath)
		{
			return SetTownSealFile(ref strPath);
		}

		private string SetTownSealFile(ref string strPath)
		{
			string SetTownSealFile = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will accept a filepath, validate it, copy the file to the
				// data directory then return the new path
				FileInfo fl;
				if (File.Exists("Townseal.pic"))
                {
                    fl = new FileInfo("townseal.pic");
					fl.Attributes = FileAttributes.Normal;
				}
				if (Strings.Trim(strPath) != "")
				{
					if (File.Exists(strPath))
                    {
						//FC:FINAL:CHN - issue #1533: Change using save path to UserData folder.
						// File.Copy(strPath, Path.Combine(Application.MapPath("\\"), "TOWNSEAL.PIC"), true);
						File.Copy(strPath, Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.CurDir(), "TOWNSEAL.PIC"), true);
						// this will copy the file to the data directory and change the name to TownSeal. and use the extention from the original file
						// SetTownSealFile = Path.Combine(Application.MapPath("\\"), "TOWNSEAL.PIC");
						SetTownSealFile = Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.CurDir(), "TOWNSEAL.PIC");
                    }
                    else
					{
						MessageBox.Show("The path specified is not valid.  Town Seal file not saved." + "\r\n" + strPath, "Town Seal File Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return SetTownSealFile;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				SetTownSealFile = "";
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Settings", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SetTownSealFile;
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				// this will load the settings from the database
				rsSettings.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (rsSettings.EndOfFile() && rsSettings.BeginningOfFile())
				{
					rsSettings.AddNew();
					// add defaults....
					rsSettings.Update(true);
				}

				clsDRWrapper rsPayport = new clsDRWrapper();
				rsPayport.OpenRecordset("SELECT * FROM Settings WHERE SettingName = 'PayPortInterface'", "SystemSettings");
				if (!rsPayport.EndOfFile() || !rsPayport.BeginningOfFile())
				{
					if (rsPayport.Get_Fields_String("SettingValue").ToLower() == "true")
					{
						cmdConfigurePayportSettings.Visible = true;
					}
				}

				if (FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail1")) != "" || FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail2")) != "" || FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail3")) != "" || FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail4")) != "" || FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail5")) != "")
				{
					txtBulkMail[1].Text = FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail1"));
					txtBulkMail[2].Text = FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail2"));
					txtBulkMail[3].Text = FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail3"));
					txtBulkMail[4].Text = FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail4"));
					txtBulkMail[5].Text = FCConvert.ToString(rsSettings.Get_Fields_String("BulkMail5"));
				}
				else
				{
					modGlobalConstants.Statics.gstrCityTown = Strings.Trim(modGlobalConstants.Statics.gstrCityTown);
					if (Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "TOWN" || Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "CITY")
					{
						txtBulkMail[1].Text = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
					}
					else
					{
						txtBulkMail[1].Text = modGlobalConstants.Statics.MuniName;
					}
					txtBulkMail[2].Text = "Bulk Mail Number = ";
					txtBulkMail[3].Text = "";
					txtBulkMail[4].Text = "";
					txtBulkMail[5].Text = "";
				}
				if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("citytown")))) == "CITY")
				{
					cmbCityTown.SelectedIndex = CNSTMUNITYPECITY;
				}
				else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("citytown")))) == "TOWN")
				{
					cmbCityTown.SelectedIndex = CNSTMUNITYPETOWN;
				}
				else
				{
					cmbCityTown.SelectedIndex = CNSTMUNITYPEOTHER;
				}
				txtFTPUser.Text = FCConvert.ToString(rsSettings.Get_Fields_String("FTPSiteUser"));
				txtFTPPassword.Text = FCConvert.ToString(rsSettings.Get_Fields_String("FTPSitePassword"));
				if (FCConvert.CBool(modRegistry.GetRegistryKey_3("UpdateNotification", FCConvert.ToString(false))))
				{
					cmbYes.SelectedIndex = 0;
				}
				else
				{
					cmbYes.SelectedIndex = 1;
				}
				if (rsSettings.Get_Fields_Boolean("CentralPartyCheck") == true)
				{
					cmbCentralPartyCheckNo.SelectedIndex = 0;
				}
				else
				{
					cmbCentralPartyCheckNo.SelectedIndex = 1;
				}
				txtTownSeal.Text = modGlobalConstants.Statics.gstrTownSealPath;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Default Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveSettings())
			{
				this.Unload();
			}
		}

		private void cmdConfigurePayportSettings_Click(object sender, EventArgs e)
		{
			StaticSettings.GlobalCommandDispatcher.Send(new ConfigurePayportSettings());
		}
	}
}
