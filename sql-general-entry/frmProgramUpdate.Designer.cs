//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmProgramUpdate.
	/// </summary>
	partial class frmProgramUpdate : fecherFoundation.FCForm
	{
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCButton cmdDir;
		public fecherFoundation.FCProgressBar pbrProgress;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
				_InstancePtr = null;
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmProgramUpdate));
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.cmdDir = new fecherFoundation.FCButton();
			this.pbrProgress = new fecherFoundation.FCProgressBar();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.lblPercent = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			
			this.SuspendLayout();
			//
			// Drive1
			//
			this.Drive1.Name = "Drive1";
			this.Drive1.TabIndex = 0;
			this.Drive1.Location = new System.Drawing.Point(6, 105);
			this.Drive1.Size = new System.Drawing.Size(123, 22);
			this.Drive1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// cmdDir
			//
			this.cmdDir.Name = "cmdDir";
			this.cmdDir.TabIndex = 1;
			this.cmdDir.Location = new System.Drawing.Point(134, 105);
			this.cmdDir.Size = new System.Drawing.Size(60, 26);
			this.cmdDir.Text = "&Select";
			this.cmdDir.BackColor = System.Drawing.SystemColors.Control;
			this.cmdDir.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdDir.Click += new System.EventHandler(this.cmdDir_Click);
			//
			// pbrProgress
			//
			this.pbrProgress.Name = "pbrProgress";
			this.pbrProgress.TabIndex = 4;
			this.pbrProgress.Location = new System.Drawing.Point(21, 530);
			this.pbrProgress.Size = new System.Drawing.Size(397, 25);
			this.pbrProgress.Minimum = 0;
			this.pbrProgress.Maximum = 100;
			//
			// cmdQuit
			//
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.TabIndex = 2;
			this.cmdQuit.Location = new System.Drawing.Point(195, 105);
			this.cmdQuit.Size = new System.Drawing.Size(60, 26);
			this.cmdQuit.Text = "&Cancel";
			this.cmdQuit.BackColor = System.Drawing.SystemColors.Control;
			this.cmdQuit.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			//
			// lblPercent
			//
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.TabIndex = 5;
			this.lblPercent.Location = new System.Drawing.Point(424, 530);
			this.lblPercent.Size = new System.Drawing.Size(88, 28);
			this.lblPercent.Text = "";
			this.lblPercent.BackColor = System.Drawing.SystemColors.Control;
			this.lblPercent.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.lblPercent.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 3;
			this.Label2.Location = new System.Drawing.Point(0, 6);
			this.Label2.Size = new System.Drawing.Size(260, 69);
			this.Label2.Text = "Place the CD into your drive and select the drive letter for your CD-Rom in the box below.";
			this.Label2.BackColor = System.Drawing.SystemColors.Control;
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label2.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(229, 59);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// frmProgramUpdate
			//
			this.ClientSize = new System.Drawing.Size(262, 169);
			this.Controls.Add(this.Drive1);
			this.Controls.Add(this.cmdDir);
			this.Controls.Add(this.pbrProgress);
			this.Controls.Add(this.cmdQuit);
			this.Controls.Add(this.lblPercent);
			this.Controls.Add(this.Label2);
			//this.Controls.Add(this.vsElasticLight1);
			this.Name = "frmProgramUpdate";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmProgramUpdate.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Load += new System.EventHandler(this.frmProgramUpdate_Load);
			this.Text = "Program Update";
			this.Drive1.ResumeLayout(false);
			this.cmdDir.ResumeLayout(false);
			this.cmdQuit.ResumeLayout(false);
			this.lblPercent.ResumeLayout(false);
			this.Label2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}