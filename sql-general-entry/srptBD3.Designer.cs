﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD3.
	/// </summary>
	partial class srptBD3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBD3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFund = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAssets = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLiabilities = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFundBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkOOB = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFund)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAssets)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLiabilities)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFundBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFund,
				this.fldAssets,
				this.fldLiabilities,
				this.fldFundBalance,
				this.fldNet,
				this.chkOOB
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7
			});
			this.ReportHeader.Height = 0.5208333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Fund Balancing List";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.84375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 5.8125F;
			this.Line1.X1 = 0.84375F;
			this.Line1.X2 = 6.65625F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.84375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: center";
			this.Label2.Text = "Fund";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "text-align: right";
			this.Label3.Text = "Assets";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 0.96875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.75F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "Liabilities";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 0.96875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.84375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Fund Balance";
			this.Label5.Top = 0.3125F;
			this.Label5.Width = 0.96875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.96875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Net";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.96875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.09375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: center";
			this.Label7.Text = "OOB";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 0.5625F;
			// 
			// fldFund
			// 
			this.fldFund.Height = 0.1875F;
			this.fldFund.Left = 0.84375F;
			this.fldFund.Name = "fldFund";
			this.fldFund.Style = "font-size: 9pt; text-align: center";
			this.fldFund.Text = "Field1";
			this.fldFund.Top = 0F;
			this.fldFund.Width = 0.6875F;
			// 
			// fldAssets
			// 
			this.fldAssets.Height = 0.1875F;
			this.fldAssets.Left = 1.65625F;
			this.fldAssets.Name = "fldAssets";
			this.fldAssets.Style = "font-size: 9pt; text-align: right";
			this.fldAssets.Text = "Field1";
			this.fldAssets.Top = 0F;
			this.fldAssets.Width = 0.96875F;
			// 
			// fldLiabilities
			// 
			this.fldLiabilities.Height = 0.1875F;
			this.fldLiabilities.Left = 2.75F;
			this.fldLiabilities.Name = "fldLiabilities";
			this.fldLiabilities.Style = "font-size: 9pt; text-align: right";
			this.fldLiabilities.Text = "Field1";
			this.fldLiabilities.Top = 0F;
			this.fldLiabilities.Width = 0.96875F;
			// 
			// fldFundBalance
			// 
			this.fldFundBalance.Height = 0.1875F;
			this.fldFundBalance.Left = 3.84375F;
			this.fldFundBalance.Name = "fldFundBalance";
			this.fldFundBalance.Style = "font-size: 9pt; text-align: right";
			this.fldFundBalance.Text = "Field1";
			this.fldFundBalance.Top = 0F;
			this.fldFundBalance.Width = 0.96875F;
			// 
			// fldNet
			// 
			this.fldNet.Height = 0.1875F;
			this.fldNet.Left = 4.96875F;
			this.fldNet.Name = "fldNet";
			this.fldNet.Style = "font-size: 9pt; text-align: right";
			this.fldNet.Text = "Field1";
			this.fldNet.Top = 0F;
			this.fldNet.Width = 0.96875F;
			// 
			// chkOOB
			// 
			this.chkOOB.Height = 0.1875F;
			this.chkOOB.Left = 6.3125F;
			this.chkOOB.Name = "chkOOB";
			this.chkOOB.Style = "text-align: center";
			this.chkOOB.Text = null;
			this.chkOOB.Top = 0F;
			this.chkOOB.Width = 0.1875F;
			// 
			// srptBD3
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFund)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAssets)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLiabilities)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFundBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFund;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAssets;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLiabilities;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFundBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNet;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkOOB;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
