﻿using Global;
using SharedApplication;
using SharedApplication.Messaging;

namespace TWGNENTY
{
    public class ShowInterestedPartiesHandler : CommandHandler<ShowInterestedParties>
    {
        protected override void Handle(ShowInterestedParties command)
        {
            frmInterestedParties.InstancePtr.Init(command.Account, command.Module,command.AssociatedId,command.AllowEdit, command.IdType);
        }
    }
}