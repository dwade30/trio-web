﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPrinters.
	/// </summary>
	partial class frmPrinters : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdTestPage;
		public fecherFoundation.FCFrame framPrinterList;
		public fecherFoundation.FCButton cmdCancelPrinterList;
		public FCGrid GridPrinterList;
		public FCGrid GridPrinter;
		public fecherFoundation.FCButton cmdPrintTestPage;
		public fecherFoundation.FCLabel Label9;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrinters));
            this.framPrinterList = new fecherFoundation.FCFrame();
            this.cmdCancelPrinterList = new fecherFoundation.FCButton();
            this.GridPrinterList = new fecherFoundation.FCGrid();
            this.GridPrinter = new fecherFoundation.FCGrid();
            this.cmdPrintTestPage = new fecherFoundation.FCButton();
            this.Label9 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.label11 = new Wisej.Web.Label();
            this.textBox1 = new Wisej.Web.TextBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framPrinterList)).BeginInit();
            this.framPrinterList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrinterList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrinterList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTestPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 534);
            this.BottomPanel.Size = new System.Drawing.Size(556, 93);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.textBox1);
            this.ClientArea.Controls.Add(this.label11);
            this.ClientArea.Controls.Add(this.cmdPrintTestPage);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.GridPrinter);
            this.ClientArea.Controls.Add(this.framPrinterList);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(576, 627);
            this.ClientArea.Controls.SetChildIndex(this.framPrinterList, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridPrinter, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrintTestPage, 0);
            this.ClientArea.Controls.SetChildIndex(this.label11, 0);
            this.ClientArea.Controls.SetChildIndex(this.textBox1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(576, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(144, 28);
            this.HeaderText.Text = "Printer Setup";
            // 
            // framPrinterList
            // 
            this.framPrinterList.BackColor = System.Drawing.Color.White;
            this.framPrinterList.Controls.Add(this.cmdCancelPrinterList);
            this.framPrinterList.Controls.Add(this.GridPrinterList);
            this.framPrinterList.Location = new System.Drawing.Point(20, 41);
            this.framPrinterList.Name = "framPrinterList";
            this.framPrinterList.Size = new System.Drawing.Size(527, 366);
            this.framPrinterList.TabIndex = 2;
            this.framPrinterList.Text = "Choose Printer";
            this.framPrinterList.Visible = false;
            // 
            // cmdCancelPrinterList
            // 
            this.cmdCancelPrinterList.AppearanceKey = "actionButton";
            this.cmdCancelPrinterList.Location = new System.Drawing.Point(20, 307);
            this.cmdCancelPrinterList.Name = "cmdCancelPrinterList";
            this.cmdCancelPrinterList.Size = new System.Drawing.Size(94, 40);
            this.cmdCancelPrinterList.TabIndex = 1;
            this.cmdCancelPrinterList.Text = "Cancel";
            this.cmdCancelPrinterList.Click += new System.EventHandler(this.cmdCancelPrinterList_Click);
            // 
            // GridPrinterList
            // 
            this.GridPrinterList.FixedCols = 0;
            this.GridPrinterList.Location = new System.Drawing.Point(20, 30);
            this.GridPrinterList.Name = "GridPrinterList";
            this.GridPrinterList.RowHeadersVisible = false;
            this.GridPrinterList.Rows = 1;
            this.GridPrinterList.ShowFocusCell = false;
            this.GridPrinterList.Size = new System.Drawing.Size(489, 262);
            this.GridPrinterList.TabIndex = 2;
            this.GridPrinterList.DoubleClick += new System.EventHandler(this.GridPrinterList_DblClick);
            // 
            // GridPrinter
            // 
            this.GridPrinter.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.GridPrinter.Cols = 3;
            this.GridPrinter.ExtendLastCol = true;
            this.GridPrinter.Location = new System.Drawing.Point(23, 41);
            this.GridPrinter.Name = "GridPrinter";
            this.GridPrinter.Rows = 7;
            this.GridPrinter.ShowFocusCell = false;
            this.GridPrinter.Size = new System.Drawing.Size(524, 387);
            this.GridPrinter.StandardTab = false;
            this.GridPrinter.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridPrinter.TabIndex = 43;
            this.GridPrinter.DoubleClick += new System.EventHandler(this.GridPrinter_DblClick);
            // 
            // cmdPrintTestPage
            // 
            this.cmdPrintTestPage.AppearanceKey = "actionButton";
            this.cmdPrintTestPage.Location = new System.Drawing.Point(36, 494);
            this.cmdPrintTestPage.Name = "cmdPrintTestPage";
            this.cmdPrintTestPage.Size = new System.Drawing.Size(100, 40);
            this.cmdPrintTestPage.TabIndex = 1;
            this.cmdPrintTestPage.Text = "Test Page";
            this.cmdPrintTestPage.Click += new System.EventHandler(this.cmdPrintTestPage_Click);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 17);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(308, 19);
            this.Label9.TabIndex = 44;
            this.Label9.Text = "DOUBLE CLICK ON ENTRY TO EDIT";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 2;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(266, 18);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(90, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 449);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(148, 15);
            this.label11.TabIndex = 40;
            this.label11.Text = "PRINT ASSISTANT KEY";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(187, 434);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(329, 22);
            this.textBox1.TabIndex = 41;
            // 
            // frmPrinters
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(576, 668);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPrinters";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Printer Setup";
            this.Load += new System.EventHandler(this.frmPrinters_Load);
            this.Resize += new System.EventHandler(this.frmPrinters_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrinters_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framPrinterList)).EndInit();
            this.framPrinterList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrinterList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrinterList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTestPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
        private Label label11;
        private TextBox textBox1;
    }
}
