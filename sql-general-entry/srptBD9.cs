﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD9.
	/// </summary>
	public partial class srptBD9 : FCSectionReport
	{
		public srptBD9()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD9";
		}

		public static srptBD9 InstancePtr
		{
			get
			{
				return (srptBD9)Sys.GetInstance(typeof(srptBD9));
			}
		}

		protected srptBD9 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD9	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intType;
		// vbPorter upgrade warning: lngIssuedTotal As int	OnWrite(short, double)
		int lngIssuedTotal;
		// vbPorter upgrade warning: lngOutstandingTotal As int	OnWrite(short, double)
		int lngOutstandingTotal;
		// vbPorter upgrade warning: lngCashedTotal As int	OnWrite(short, double)
		int lngCashedTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intType += 1;
				if (intType <= 7)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			intType = 1;
			lngIssuedTotal = 0;
			lngOutstandingTotal = 0;
			lngCashedTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldType.Text = GetTypeDescription();
			rsInfo.OpenRecordset("SELECT COUNT(Type) as ItemCount FROM CheckRecMaster WHERE Type = '" + FCConvert.ToString(intType) + "' AND Status = '1'", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
				fldIssued.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("ItemCount")), "#,##0");
				// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
				lngIssuedTotal += FCConvert.ToInt32(Conversion.Val(rsInfo.Get_Fields("ItemCount")));
			}
			else
			{
				fldIssued.Text = Strings.Format("0", "#,##0");
			}
			rsInfo.OpenRecordset("SELECT COUNT(Type) as ItemCount FROM CheckRecMaster WHERE Type = '" + FCConvert.ToString(intType) + "' AND Status = '2'", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
				fldOutstanding.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("ItemCount")), "#,##0");
				// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
				lngOutstandingTotal += FCConvert.ToInt32(Conversion.Val(rsInfo.Get_Fields("ItemCount")));
			}
			else
			{
				fldOutstanding.Text = Strings.Format("0", "#,##0");
			}
			rsInfo.OpenRecordset("SELECT COUNT(Type) as ItemCount FROM CheckRecMaster WHERE Type = '" + FCConvert.ToString(intType) + "' AND Status = '3'", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
				fldCashed.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("ItemCount")), "#,##0");
				// TODO Get_Fields: Field [ItemCount] not found!! (maybe it is an alias?)
				lngCashedTotal += FCConvert.ToInt32(Conversion.Val(rsInfo.Get_Fields("ItemCount")));
			}
			else
			{
				fldCashed.Text = Strings.Format("0", "#,##0");
			}
			fldTotal.Text = Strings.Format(FCConvert.ToInt32(fldIssued.Text) + FCConvert.ToInt32(fldOutstanding.Text) + FCConvert.ToInt32(fldCashed.Text), "#,##0");
		}

		private string GetTypeDescription()
		{
			string GetTypeDescription = "";
			switch (intType)
			{
				case 1:
					{
						GetTypeDescription = "1 - Accounts Payable";
						break;
					}
				case 2:
					{
						GetTypeDescription = "2 - Payroll";
						break;
					}
				case 3:
					{
						GetTypeDescription = "3 - Deposit";
						break;
					}
				case 4:
					{
						GetTypeDescription = "4 - Returned Check";
						break;
					}
				case 5:
					{
						GetTypeDescription = "5 - Interest";
						break;
					}
				case 6:
					{
						GetTypeDescription = "6 - Other Credits";
						break;
					}
				case 7:
					{
						GetTypeDescription = "7 - Other Debits";
						break;
					}
			}
			//end switch
			return GetTypeDescription;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldIssuedTotal.Text = Strings.Format(lngIssuedTotal, "#,##0");
			fldOutstandingTotal.Text = Strings.Format(lngOutstandingTotal, "#,##0");
			fldCashedTotal.Text = Strings.Format(lngCashedTotal, "#,##0");
			fldTotalTotal.Text = Strings.Format(lngIssuedTotal + lngOutstandingTotal + lngCashedTotal, "#,##0");
		}

	
	}
}
