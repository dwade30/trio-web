//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmNewOpticalBackup.
	/// </summary>
	public partial class frmNewOpticalBackup : fecherFoundation.FCForm
	{
;

		public frmNewOpticalBackup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.optZip = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optCloseDisk = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optZip.AddControlArrayElement(optZip_1, 1 );
			this.optZip.AddControlArrayElement(optZip_0, 0 );
			this.optCloseDisk.AddControlArrayElement(optCloseDisk_1, 1 );
			this.optCloseDisk.AddControlArrayElement(optCloseDisk_0, 0 );

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================


		private cOpticalWriter tWriter;
		private cDriveWriteInfo tDInfo;
		private string strListOfFiles;
		private string strExtendedList;
		private string strDriveSavedTo;
		string strTempDirOrFile = ""; // store the directory or temp file to delete when done burning
		string strPathFileSavedTo = "";
		string strZipFileSavedTo = "";
		private bool boolBurnCompleted;
		private void frmNewOpticalBackup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			switch (KeyCode) {
				
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					mnuExit_Click();
					break;
				}
			} //end switch
		}

		private void frmNewOpticalBackup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNewOpticalBackup properties;
			//frmNewOpticalBackup.FillStyle	= 0;
			//frmNewOpticalBackup.ScaleWidth	= 5880;
			//frmNewOpticalBackup.ScaleHeight	= 4155;
			//frmNewOpticalBackup.LinkTopic	= "Form2";
			//frmNewOpticalBackup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			txtNewDirectory.Text = Strings.Format(DateTime.Today, "mmddyyyy");
			txtTempDir.Text = Application.StartupPath+"\\temp\\";

		}



		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}



		// vbPorter upgrade warning: strPath As object	OnWrite(string)
		// vbPorter upgrade warning: strFileToMake As object	OnWrite(string)
		// vbPorter upgrade warning: strListToZip As object	OnWrite(string)
		private bool CreateZipFile(ref object strPath, ref object strFileToMake, ref object strListToZip)
		{
			bool CreateZipFile = false;
			int x;
			string[] arListArray = null;
			string strExePath;
			string strDBPath;
			// vbPorter upgrade warning: zipreturn As abeError	OnWrite(AbaleZipLibrary.abeError)
			AbaleZipLibrary.abeError zipreturn;

			arListArray = Strings.Split(Convert.ToString(strListToZip), ",", -1, CompareConstants.vbTextCompare);
			strDBPath = Environment.CurrentDirectory+"\\";
			// strExePath = Left(CurDir, 1) & ":\triodata\triomast\"
			strExePath = modGlobalFunctions.gGlobalSettings.MasterPath;
			if (Strings.Right("\\"+strExePath, 1)!="\\") {
				strExePath += "\\";
			}
			AbaleZip1.PreservePaths = false;
			AbaleZip1.FilesToProcess = "";
			AbaleZip1.ProcessSubfolders = false;
			AbaleZip1.SpanMultipleDisks = AbaleZipLibrary.abeDiskSpanning.adsNever;
			for(x=0; x<=Information.UBound(arListArray, 1); x++) {


				//Application.DoEvents();
				AbaleZip1.ZipFilename = strPath.ToString()+strFileToMake+".zip";

				
				if (Strings.UCase(Strings.Right(arListArray[x], 3))=="EXE")
				{
					AbaleZip1.AddFilesToProcess(strExePath+arListArray[x]);
				}
				else if (Strings.UCase(Strings.Right(arListArray[x], 3))=="VB1")
				{
					AbaleZip1.AddFilesToProcess(strDBPath+arListArray[x]);
				}



				this.Refresh();
			}
			zipreturn = AbaleZip1.Zip();

			if (zipreturn!=AbaleZipLibrary.abeError.aerSuccess) {
				CreateZipFile = false;
			} else {
				CreateZipFile = true;
			}
			return CreateZipFile;
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			string strZipDir;

			strZipDir = frmPath.InstancePtr.Init(false, "Select the path to store the temp file(s) or create a new temp directory in.", false);
			if (strZipDir!=string.Empty) {
				txtTempDir.Text = strZipDir;
			}

		}



		private void EnableThisForm_2(bool boolEnable) { EnableThisForm(ref boolEnable); }
		private void EnableThisForm(ref bool boolEnable)
		{
			if (boolEnable) {
				// enable
				framZipOptions.Enabled = true;
				framSessionOptions.Enabled = true;
				framBurnOptions.Enabled = true;
				mnuBurn.Enabled = true;
				mnuExit.Enabled = true;
				// mnuAbort.Enabled = False
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			} else {
				// disable
				framZipOptions.Enabled = false;
				framSessionOptions.Enabled = false;
				framBurnOptions.Enabled = false;
				mnuBurn.Enabled = false;
				mnuExit.Enabled = false;
				// mnuAbort.Enabled = True
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			}
		}

		public void Init(ref string strFileList, ref string strDrive, ref string strVerboseList)
		{
			int x;
			bool boolGotDrive;
			cWritableOpticalDrives tDrives;
			try
			{	// On Error GoTo ErrorHandler
				// don't keep the gn database locked
				//Application.DoEvents();

				strListOfFiles = strFileList;
				strExtendedList = strVerboseList;
				strDriveSavedTo = strDrive;

				boolGotDrive = false;
				tDrives = new cWritableOpticalDrives();

				for(x=1; x<=tDrives.Count; x++) {
					tDInfo = tDrives.GetDriveInfoByIndex(x);
					if (Strings.LCase(Strings.Left(tDInfo.Volume, 1))==Strings.LCase(Strings.Left(strDrive, 1))) {
						boolGotDrive = true;
						break;
					}
				} // x

				if (!boolGotDrive) {
					MessageBox.Show("Error. "+Strings.UCase(Strings.Left(strDrive, 1))+" not a valid CD/DVD/BD writer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				// initialize some things
				LoadWriteSpeedCombo();


				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupCloseDisk", "GN", Convert.ToString(true)))) {
					optCloseDisk[0].Checked = true;
				} else {
					optCloseDisk[1].Checked = true;
				}

				if (modRegistry.GetRegistryKey("BackupZip", "GN", "Single")=="Single") {
					optZip[0].Checked = true;
				} else {
					optZip[1].Checked = true;
				}

				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupCopyToTemp", "GN", Convert.ToString(false)))) {
					chkCopyToLocal.CheckState = Wisej.Web.CheckState.Checked;
				} else {
					chkCopyToLocal.CheckState = Wisej.Web.CheckState.Unchecked;
				}

				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupDeleteTemp", "GN", Convert.ToString(true)))) {
					chkDelete.CheckState = Wisej.Web.CheckState.Checked;
				} else {
					chkDelete.CheckState = Wisej.Web.CheckState.Unchecked;
				}

				txtTempDir.Text = modRegistry.GetRegistryKey("BackupTempDir", "GN");

				if (FCConvert.CBool(modRegistry.GetRegistryKey("BackupNewDirectory", "GN", Convert.ToString(false)))) {
					chkNewDirectory.CheckState = Wisej.Web.CheckState.Checked;
				} else {
					chkNewDirectory.CheckState = Wisej.Web.CheckState.Unchecked;
				}



				modRegistry.SaveRegistryKey("BackupWriteSpeed", Convert.ToString(cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex)), "GN");

				this.Show(MDIParent.InstancePtr);
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


		private void LoadWriteSpeedCombo()
		{
			int intMaxWriteSpeed;
			int intSpeed;
			int intDefaultSpeed;
			int intDefaultIndex;

			intDefaultSpeed = (int)Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BackupWriteSpeed", "GN", Convert.ToString(0))));

			// Clear Combo
			cboWriteSpeed.Clear();

			// intSpeed = intMaxWriteSpeed
			intDefaultIndex = -1;
			int intMaxIndex;
			intMaxWriteSpeed = 0;
			intMaxIndex = -1;
			// If speed is not zero then
			// vbPorter upgrade warning: tSpeed As cOpticalWriteSpeed	OnWrite(Collection)
			cOpticalWriteSpeed tSpeed;
			if (tDInfo.WriteSpeeds.Count>0) {
				int x;
				for(x=1; x<=tDInfo.WriteSpeeds.Count; x++) {
					tSpeed = tDInfo.WriteSpeeds(x);
					cboWriteSpeed.AddItem(tSpeed.WriteSpeed);
					if (intMaxWriteSpeed<tSpeed.WriteSpeed) {
						intMaxWriteSpeed = tSpeed.WriteSpeed;
						intMaxIndex = cboWriteSpeed.NewIndex;
					}
					cboWriteSpeed.ItemData( cboWriteSpeed.NewIndex, tSpeed.WriteSpeed);
				} // x
			} else {
				// Some drives don't report speed
				cboWriteSpeed.AddItem("Default");
				cboWriteSpeed.ItemData( cboWriteSpeed.NewIndex, 0);
			}
			// Set to Max
			if (cboWriteSpeed.Items.Count>0) {
				if (intDefaultIndex>=0) {
					cboWriteSpeed.SelectedIndex = intDefaultIndex;
				} else {
					if (intMaxIndex>=0) {
						cboWriteSpeed.SelectedIndex = intMaxIndex;
					} else {
						cboWriteSpeed.SelectedIndex = 0;
					}
				}
			}

		}


		private void tWriter_BurnCompleted()
		{
			CompleteBurn();
		}

		private void CompleteBurn()
		{
			try
			{	// On Error GoTo ErrorHandler
				if (boolBurnCompleted) return;
				boolBurnCompleted = true;
				
				lblProgress.Text = "Burn Completed";
				lblProgress.Refresh();
				//Application.DoEvents();
				EnableThisForm_2(true);
				if (optZip[0].Checked==true) {
					// delete the temp zip file
					if ((Strings.UCase(Environment.CurrentDirectory+"\\")!=Strings.UCase(strTempDirOrFile)) && (Strings.UCase(Application.StartupPath+"\\")!=Strings.UCase(strTempDirOrFile)) && strTempDirOrFile!="" && strTempDirOrFile!="\\") {
						fso.DeleteFile(strTempDirOrFile, true);
					}
				} else {
					if ((chkDelete.CheckState==Wisej.Web.CheckState.Checked || chkDelete.CheckState==CheckState.Indeterminate) && (chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked || chkCopyToLocal.CheckState==CheckState.Indeterminate)) {
						if ((Strings.UCase(Environment.CurrentDirectory+"\\")!=Strings.UCase(strTempDirOrFile)) && (Strings.UCase(Application.StartupPath+"\\")!=Strings.UCase(strTempDirOrFile)) && strTempDirOrFile!="" && strTempDirOrFile!="\\" && Strings.UCase(strTempDirOrFile)!="WINDOWS\\" && Strings.UCase(strTempDirOrFile)!="C:\\") {
							// make sure we don't delete the data directory or the vbtrio directory
							fso.DeleteFile(strTempDirOrFile+"*.vb1", true);
							fso.DeleteFile(strTempDirOrFile+"*.exe", true);
						}
					}
				}
				rptBackup.InstancePtr.Init(ref optZip[0].Checked, ref strExtendedList, strDriveSavedTo+"\\"+strPathFileSavedTo, ref strZipFileSavedTo);
				tWriter.EjectMedia();
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				lblProgress.Text = "";
				lblProgress.Refresh();
				//Application.DoEvents();
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In BurnCompleted", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);

			}
		}

		private void tWriter_ErrorEvent(string strMessage)
		{
			EnableThisForm_2(true);
			lblProgress.Text = "";
			lblProgress.Refresh();
			//Application.DoEvents();
			MessageBox.Show(strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		private void tWriter_UpdateMessage(string strMessage)
		{
			lblProgress.Text = strMessage;
			lblProgress.Refresh();
			//Application.DoEvents();
		}


		private void Form_Unload(ref short Cancel)
		{
			modGNBas.clsGESecurity = new clsTrioSecurity();
			modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
			modGNBas.clsGESecurity.Init("GE", "systemsettings");
			modGlobalConstants.Statics.clsSecurityClass.Init("GE", "systemsettings", null, modGNBas.clsGESecurity.Get_UserID());
			// Call secRS.OpenRecordset("SELECT * FROM Security", "systemsettings")
			// Call secRS.FindFirstRecord("ID", clsGESecurity.Get_UserID)
			MDIParent.InstancePtr.Grid.Focus();
		}

		private void mnuBurn_Click(object sender, System.EventArgs e)
		{
			string strTempPath = "";
			
			string strZipFileName = "";
			Scripting.Drive dr;
			string[] arListArray = null;
			bool boolCopyFiles = false;
			string strDestPath = "";
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			string strSrcPath = "";
			bool boolLoadLastSession;
			double dblBytesLeft;
			cOpticalItemInfo tDir;
			int x;


			try
			{	// On Error GoTo ErrorHandler
				boolBurnCompleted = false;
				// save defaults
				if (optCloseDisk[0].Checked) {
					modRegistry.SaveRegistryKey("BackupCloseDisk", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupCloseDisk", Convert.ToString(false), "GN");
				}
				if (optZip[0].Checked) {
					modRegistry.SaveRegistryKey("BackupZip", "Single", "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupZip", "Multiple", "GN");
				}
				if (chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked) {
					modRegistry.SaveRegistryKey("BackupCopyToTemp", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupCopyToTemp", Convert.ToString(false), "GN");
				}
				if (chkDelete.CheckState==Wisej.Web.CheckState.Checked) {
					modRegistry.SaveRegistryKey("BackupDeleteTemp", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupDeleteTemp", Convert.ToString(false), "GN");
				}
				modRegistry.SaveRegistryKey("BackupTempDir", txtTempDir.Text, "GN");
				if (chkNewDirectory.CheckState==Wisej.Web.CheckState.Checked) {
					modRegistry.SaveRegistryKey("BackupNewDirectory", Convert.ToString(true), "GN");
				} else {
					modRegistry.SaveRegistryKey("BackupNewDirectory", Convert.ToString(false), "GN");
				}
				modRegistry.SaveRegistryKey("BackupWriteSpeed", Convert.ToString(cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex)), "GN");

				//Application.DoEvents();
				// secRS.DisconnectDatabase
				// rsVariables.DisconnectDatabase
				// secDB.DisconnectDatabase
				modGNBas.clsGESecurity = null;
				modGlobalConstants.Statics.clsSecurityClass = null;

				//Application.DoEvents();

				cOpticalWriteSessionInfo tSession = new cOpticalWriteSessionInfo();
				if (tWriter==null) {
					tWriter = new cOpticalWriter();
					tWriter.DriveInfo = tDInfo;
					tWriter.InitializeWriter();
				}
				tSession.VolumeName = "TRIOBackup";
				boolLoadLastSession = false;
				if (cboWriteSpeed.SelectedIndex>=0) {
					if (cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex)>0) {
						tSession.WriteSpeed = cboWriteSpeed.ItemData( cboWriteSpeed.SelectedIndex);
					}
				}

				if (optZip[0].Checked || chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked || chkCopyToLocal.CheckState==CheckState.Indeterminate) {
					if (txtTempDir.Text=="") {
						strTempPath = Application.StartupPath+"\\";
					} else {
						strTempPath = txtTempDir.Text;
						if (fso.FolderExists(txtTempDir.Text)) {
							strTempPath = txtTempDir.Text;
							if (Strings.Right(strTempPath, 1)!="\\") strTempPath += "\\";
						} else {
							intRes = MessageBox.Show("Temp directory "+strTempPath+" does not exist. Create it now?", "Path not found", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intRes==DialogResult.Yes) {
								fso.CreateFolder(Strings.Mid(strTempPath, 1, strTempPath.Length-1));
								// 20                        MsgBox "Could not create temp directory " & strTempPath & ". Operation cancelled."
								// Exit Sub
								// End If
							} else {
								MessageBox.Show("Operation cancelled.");
								return;
							}
						}
					}
				}


				if (chkNewDirectory.CheckState==Wisej.Web.CheckState.Checked) {
					if (Strings.Trim(txtNewDirectory.Text)==string.Empty) {
						strDestPath = "";
					} else {
						strDestPath = Strings.Trim(txtNewDirectory.Text);
					}
				} else {
					strDestPath = "";
				}

				if (optCloseDisk[0].Checked) {
					tSession.LeaveSessionOpen = false;
				} else {
					tSession.LeaveSessionOpen = true;
				}

				EnableThisForm_2(false);

				strTempDirOrFile = strTempPath;
				if (!tWriter.IsMediaReady()) {
					MessageBox.Show("Burnable media not found");
					return;
				}
				strPathFileSavedTo = strDestPath;
				strZipFileSavedTo = "";
				dblBytesLeft = tWriter.SpaceAvailable();

				if (optZip[0].Checked) {
					// burning a zip file
					strZipFileName = "tz"+Strings.Format(DateTime.Today, "mmddyy");
					strZipFileSavedTo = strZipFileName;
					strTempDirOrFile = strTempPath+strZipFileName+".zip";
					if (fso.FileExists(strTempPath+strZipFileName+".zip")) fso.DeleteFile(strTempPath+strZipFileName+".zip", true);
					lblProgress.Text = "Creating Zip file. Please Wait.";
					if (CreateZipFile(ref strTempPath, ref strZipFileName, ref strListOfFiles)) {
						// burn it

						if (fso.GetFile(strTempPath+strZipFileName+".zip").Size<dblBytesLeft) {
							if (strDestPath!="") {
								tDir = tSession.AddDir("", strDestPath);
								tDir.AddFile(strTempPath+"\\"+strZipFileName+".zip", strZipFileName+".zip");
							} else {
								tSession.AddFile(strTempPath+"\\"+strZipFileName+".zip", strZipFileName+".zip");
							}

							tWriter.Burn(ref tSession);
							CompleteBurn();
						} else {
							MessageBox.Show("There is not enough free space on the media to save the backup."+"\n"+"Please insert a new CD/DVD and try again", "Insufficient Room", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}

					} else {
						MessageBox.Show("Error.  Could not create zip file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}

				} else {
					// burning each file

					arListArray = Strings.Split(strListOfFiles, ",", -1, CompareConstants.vbTextCompare);
					boolCopyFiles = false;
					dr = fso.GetDrive(Strings.Left(Environment.CurrentDirectory, 1));
					if (dr.DriveType==DriveTypeConst.Remote) {
						// is a network drive so copy the files locally before burning
						if (chkCopyToLocal.CheckState==Wisej.Web.CheckState.Checked) boolCopyFiles = true;
					}

					if (boolCopyFiles) {
						strSrcPath = strTempPath;
						lblProgress.Text = "Copying files to temp directory";
						for(x=0; x<=Information.UBound(arListArray, 1); x++) {
							fso.CopyFile(arListArray[x], strTempPath, true);
						} // x
					} else {
						strSrcPath = Environment.CurrentDirectory+"\\";
					}
					if (strDestPath!="") {
						tDir = tSession.AddDir("", strDestPath);
						for(x=0; x<=Information.UBound(arListArray, 1); x++) {
							tDir.AddFile(strSrcPath+arListArray[x], arListArray[x]);
						} // x
					} else {
						for(x=0; x<=Information.UBound(arListArray, 1); x++) {
							tSession.AddFile(strSrcPath+arListArray[x], "");
						} // x
					}
					tWriter.Burn(ref tSession);
					CompleteBurn();
				}

				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				lblProgress.Text = "";
				EnableThisForm_2(true);
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In line "+Erl+" in mnuBurn.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

	}
}