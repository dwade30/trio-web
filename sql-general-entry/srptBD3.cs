﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD3.
	/// </summary>
	public partial class srptBD3 : FCSectionReport
	{
		public srptBD3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD3";
		}

		public static srptBD3 InstancePtr
		{
			get
			{
				return (srptBD3)Sys.GetInstance(typeof(srptBD3));
			}
		}

		protected srptBD3 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD3	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles ORDER BY Fund", "TWBD0000.vb1");
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			GetFundInfo_2(rsInfo.Get_Fields("Fund"));
		}

		private void GetFundInfo_2(string strFundToCheck)
		{
			GetFundInfo(ref strFundToCheck);
		}

		private void GetFundInfo(ref string strFundToCheck)
		{
			clsDRWrapper rsAssets = new clsDRWrapper();
			clsDRWrapper rsLiabilities = new clsDRWrapper();
			clsDRWrapper rsFundBalance = new clsDRWrapper();
			clsDRWrapper rsRangeInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curAssetsBalance As Decimal	OnWrite(double, short)
			Decimal curAssetsBalance = 0;
			// vbPorter upgrade warning: curLiabilitiesBalance As Decimal	OnWrite(double, short)
			Decimal curLiabilitiesBalance = 0;
			// vbPorter upgrade warning: curFundBalanceBalance As Decimal	OnWrite(double, short)
			Decimal curFundBalanceBalance = 0;
			fldFund.Visible = true;
			fldAssets.Visible = true;
			fldLiabilities.Visible = true;
			fldFundBalance.Visible = true;
			fldNet.Visible = true;
			chkOOB.Visible = true;
			rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'A'", "TWBD0000.vb1");
			if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				rsAssets.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND LedgerAccount >= '" + rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") + "' GROUP BY Fund", "TWBD0000.vb1");
				if (rsAssets.EndOfFile() != true && rsAssets.BeginningOfFile() != true)
				{
					// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					curAssetsBalance = FCConvert.ToDecimal(rsAssets.Get_Fields("OriginalBudgetTotal") + rsAssets.Get_Fields("BudgetAdjustmentsTotal") + rsAssets.Get_Fields("PostedDebitsTotal") + rsAssets.Get_Fields("EncumbActivityTotal") + rsAssets.Get_Fields("PostedCreditsTotal"));
				}
				else
				{
					curAssetsBalance = 0;
				}
			}
			rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'L'", "TWBD0000.vb1");
			if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				rsLiabilities.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND LedgerAccount >= '" + rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") + "' GROUP BY Fund", "TWBD0000.vb1");
				if (rsLiabilities.EndOfFile() != true && rsLiabilities.BeginningOfFile() != true)
				{
					// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					curLiabilitiesBalance = FCConvert.ToDecimal(rsLiabilities.Get_Fields("OriginalBudgetTotal") + rsLiabilities.Get_Fields("BudgetAdjustmentsTotal") + rsLiabilities.Get_Fields("PostedDebitsTotal") + rsLiabilities.Get_Fields("EncumbActivityTotal") + rsLiabilities.Get_Fields("PostedCreditsTotal"));
				}
				else
				{
					curLiabilitiesBalance = 0;
				}
			}
			rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'F'", "TWBD0000.vb1");
			if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				rsFundBalance.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND LedgerAccount >= '" + rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") + "' GROUP BY Fund", "TWBD0000.vb1");
				if (rsFundBalance.EndOfFile() != true && rsFundBalance.BeginningOfFile() != true)
				{
					// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					curFundBalanceBalance = FCConvert.ToDecimal(rsFundBalance.Get_Fields("OriginalBudgetTotal") + rsFundBalance.Get_Fields("BudgetAdjustmentsTotal") + rsFundBalance.Get_Fields("PostedDebitsTotal") + rsFundBalance.Get_Fields("EncumbActivityTotal") + rsFundBalance.Get_Fields("PostedCreditsTotal"));
				}
				else
				{
					curFundBalanceBalance = 0;
				}
			}
			fldFund.Text = strFundToCheck;
			fldAssets.Text = Strings.Format(curAssetsBalance, "#,##0.00");
			fldLiabilities.Text = Strings.Format(curLiabilitiesBalance * -1, "#,##0.00");
			fldFundBalance.Text = Strings.Format(curFundBalanceBalance * -1, "#,##0.00");
			fldNet.Text = Strings.Format(curAssetsBalance + curLiabilitiesBalance + curFundBalanceBalance, "#,##0.00");
			if (curAssetsBalance == ((curLiabilitiesBalance * -1) + (curFundBalanceBalance * -1)))
			{
				chkOOB.Checked = false;
			}
			else
			{
				chkOOB.Checked = true;
			}
			if (curAssetsBalance == 0 && curLiabilitiesBalance == 0 && curFundBalanceBalance == 0)
			{
				fldFund.Visible = false;
				fldAssets.Visible = false;
				fldLiabilities.Visible = false;
				fldFundBalance.Visible = false;
				fldNet.Visible = false;
				chkOOB.Visible = false;
			}
			rsAssets.Dispose();
			rsLiabilities.Dispose();
			rsFundBalance.Dispose();
			rsRangeInfo.Dispose();
		}

		
	}
}
