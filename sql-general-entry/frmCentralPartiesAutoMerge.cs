﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmCentralPartiesAutoMerge.
	/// </summary>
	public partial class frmCentralPartiesAutoMerge : BaseForm
	{
		public frmCentralPartiesAutoMerge()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCentralPartiesAutoMerge InstancePtr
		{
			get
			{
				return (frmCentralPartiesAutoMerge)Sys.GetInstance(typeof(frmCentralPartiesAutoMerge));
			}
		}

		protected frmCentralPartiesAutoMerge _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================

		private bool boolStop;
		private Dictionary<string, IEnumerable<string>> roadTypesDictionary = new Dictionary<string, IEnumerable<string>>();
		private Dictionary<string, string> roadPermutationsDictionary = new Dictionary<string, string>();

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU: #1258 - Execute Process with StartTask and set ShowLoader to false 
			this.ShowLoader = false;
			FCUtils.StartTask(this, () =>
            {
                Process();
                FCUtils.UnlockUserInterface();
            });
		}

		private void Process()
		{
			clsDRWrapper rsMatch = new clsDRWrapper();
			int lngDeletedParties = 0;
			int lngBookmark = 0;
			int lngMergeID = 0;
			string strStart;
			string strEnd;
			string strWhere = "";
			int lngRecordCount = 0;
			int lngCount;
			int intTemp = 0;
			cPartyUtil pu = new cPartyUtil();
			string strAddr1 = "";
			IEnumerable<cPartyMergeItem> partyMergeItemCollection = new List<cPartyMergeItem>();
			Dictionary<int, cPartyMergeItem> partyMergeDictionary = new Dictionary<int, cPartyMergeItem>();


			strStart = Strings.Trim(txtStart.Text);
			strEnd = Strings.Trim(txtEnd.Text);
			if (strStart != "" && strEnd != "")
			{
				if (Strings.CompareString(strStart, ">", strEnd))
				{
					MessageBox.Show("The range you specified is invalid", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}

			modCentralPartyMerge.AddPartyIndexes();
            modCentralPartyMerge.FixDuplicatePrimaryAddressIssue();

			partyMergeItemCollection = GetPartyMergeItemCollectionAndDictionary(strStart, strEnd, ref partyMergeDictionary);

			boolStop = false;
			cmdProcess.Enabled = false;
			cmdStop.Enabled = true;
			
			lngCount = 0;
			if (partyMergeItemCollection.Count() > 0)
			{
				lngRecordCount = partyMergeItemCollection.Count();
				prgProgress.Maximum = 100;
				prgProgress.Value = 0;
				prgProgress.Visible = true;
				lblProgress.Text = 0 + " of " + lngRecordCount;
				lblProgress.Visible = true;
				lngDeletedParties = 0;
				foreach (cPartyMergeItem originalParty in partyMergeItemCollection)
				{
					lngCount += 1;
					if (!originalParty.Deleted)
					{
						rsMatch.OpenRecordset("SELECT * FROM PartyAndAddressView WHERE PartyType = " + FCConvert.ToString(originalParty.PartyType) + " AND FullName = '" + modGlobalFunctions.EscapeQuotes(originalParty.FullName) + "' AND id > " + FCConvert.ToString(originalParty.ID), "CentralParties");
						if (!rsMatch.EndOfFile())
						{
							do
							{
								if (Convert.ToInt32(rsMatch.Get_Fields("ID")) != originalParty.ID)
								{
									cPartyMergeItem compareParty = partyMergeDictionary[rsMatch.Get_Fields("id")];
									if (!compareParty.Deleted)
									{
										if (AddressesMatch(originalParty.Address1, rsMatch.Get_Fields("address1")))
										{
											compareParty.Deleted = true;
											pu.ChangePartyID(rsMatch.Get_Fields("ID"), originalParty.ID);
											modCentralPartyMerge.RemovePartiesPrimaryAddress(rsMatch.Get_Fields("id"));
											modCentralPartyMerge.ChangePartyIDInCentralParties(originalParty.ID, rsMatch.Get_Fields("ID"));
											modCentralPartyMerge.RemovePartyFromCentralParties(rsMatch.Get_Fields("ID"));
											lngDeletedParties += 1;
										}
									}
								}

								if (boolStop)
								{
									boolStop = false;
									cmdStop.Enabled = false;
									cmdProcess.Enabled = true;
									prgProgress.Visible = false;
									lblProgress.Visible = false;
									MessageBox.Show(
										"Merge stopped" + "\r\n" + lngDeletedParties +
										" duplicates have been removed from your data", "Merge Stopped",
										MessageBoxButtons.OK, MessageBoxIcon.Information);
									return;
								}

								rsMatch.MoveNext();
							} while (rsMatch.EndOfFile() != true);
						}
					}
					if (boolStop)
					{
						boolStop = false;
						cmdStop.Enabled = false;
						cmdProcess.Enabled = true;
						prgProgress.Visible = false;
						lblProgress.Visible = false;
						MessageBox.Show("Merge stopped" + "\r\n" + lngDeletedParties + " duplicates have been removed from your data", "Merge Stopped", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}

					if (lngRecordCount == 0)
						intTemp = 100; 
					else
						intTemp = Convert.ToInt32((lngCount / Convert.ToDecimal(lngRecordCount)) * 100);

					if (intTemp > 100)
						intTemp = 100;

					prgProgress.Value = intTemp;
					lblProgress.Text = lngCount + " of " + lngRecordCount;
					lblProgress.Refresh();
				}
			}
			cmdProcess.Enabled = true;
			cmdStop.Enabled = false;
			prgProgress.Visible = false;
			lblProgress.Visible = false;
			MessageBox.Show("Merge Completed!  " + lngDeletedParties + " duplicates have been removed from your data.", "Merge Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdStop_Click(object sender, System.EventArgs e)
		{
			boolStop = true;
		}

		private void frmCentralPartiesAutoMerge_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private IEnumerable<cPartyMergeItem> GetPartyMergeItemCollectionAndDictionary(string strRangeStart, string strRangeEnd, ref Dictionary<int, cPartyMergeItem> partyMergeDictionary)
		{
			string strWhere = "";
			string strSQL;
			List<cPartyMergeItem> returnCollection = new List<cPartyMergeItem>();
			if (strRangeStart != "" || strRangeEnd != "")
			{
				if (strRangeStart != "")
				{
					if (strRangeEnd != "")
					{
						strWhere += " partyandaddressview.fullname between '" + strRangeStart + "' and '" + strRangeEnd + "z'";
					}
					else
					{
						strWhere += " partyandaddressview.fullname >= '" + strRangeStart + "'";
					}
				}
				else
				{
					strWhere += " partyandaddressview.FullName < '" + strRangeEnd + "z'";
				}
			}
			if (strWhere != "")
			{
				strWhere = " where " + strWhere;
			}
			strSQL = "SELECT PartyAndAddressView.ID, PartyAndAddressView.PartyType, PartyAndAddressView.FullName, PartyAndAddressView.Address1 FROM Parties INNER JOIN PartyAndAddressView ON Parties.ID = PartyAndAddressView.ID " + strWhere + " order by partyandaddressview.id";

			clsDRWrapper rsLoad = new/*AsNew*/ clsDRWrapper();

			rsLoad.OpenRecordset(strSQL, "CentralParties");
			while (!rsLoad.EndOfFile())
			{
				var partyMergeItem = new cPartyMergeItem();
				partyMergeItem.ID = Convert.ToInt32(rsLoad.Get_Fields("id"));
				partyMergeItem.FullName = FCConvert.ToString(rsLoad.Get_Fields("FullName"));
				partyMergeItem.Address1 = FCConvert.ToString(rsLoad.Get_Fields("address1"));
				partyMergeItem.PartyType = Convert.ToInt16(rsLoad.Get_Fields("PartyType"));
				partyMergeDictionary.Add(partyMergeItem.ID, partyMergeItem);
				returnCollection.Add(partyMergeItem);
				rsLoad.MoveNext();
			}
			return returnCollection;
		}

		private void frmCentralPartiesAutoMerge_Load(object sender, System.EventArgs e)
		{
			roadTypesDictionary = GetRoadTypesDictionary();
			roadPermutationsDictionary = BuildRoadPermutationsDictionary(ref roadTypesDictionary);
		}
		private void frmCentralPartiesAutoMerge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}

		private bool AddressesMatch(string strAddress, string strCompareAddress)
		{
			bool AddressesMatch = false;

			string strAddressTransform = strAddress.Trim().ToLower();
			strAddressTransform = strAddressTransform.Replace(".", "");

			string strCompareAddressTransform = strCompareAddress.Trim().ToLower();
			strCompareAddressTransform = strCompareAddressTransform.Replace(".", "");

			if (strAddressTransform == strCompareAddressTransform)
			{
				return true;
			}

			string strRoadType = GetRoadType(strAddressTransform);
			if (strRoadType != "")
			{
				string strCompareRoadType = GetRoadType(strCompareAddressTransform);
				if (strRoadType == strCompareRoadType)
				{
					if (AllButLastWordMatches(strAddressTransform, strCompareAddressTransform))
					{
						AddressesMatch = true;
					}
				}
			}
			return AddressesMatch;
		}

		private string GetRoadType(string strAddress)
		{
			string getRoadType = "";
			if (strAddress != "")
			{
				string strAddressTransform = "";
				string strRoad = "";

				strAddressTransform = strAddress.Trim().ToLower();
				strRoad = GetLastWord(strAddressTransform);
				if (strRoad != "")
				{
					if (roadPermutationsDictionary.ContainsKey(strRoad))
					{
						getRoadType = roadPermutationsDictionary[strRoad];
					}
				}
			}
			return getRoadType;
		}

		private string GetLastWord(string strPhrase)
		{
			if (strPhrase.Trim() == "")
			{
				return "";
			}
			string[] ary = strPhrase.Split(' ');
			return ary.LastOrDefault();
		}

		private bool AllButLastWordMatches(string strPhrase, string strComparePhrase)
		{
			string[] ary = strPhrase.Split(' ');
			string[] aryCompare = strComparePhrase.Split(' ');
			
			if (ary.Length != aryCompare.Length)
			{
				return false;
			}
			if (ary.Length == 0)
			{
				return true;
			}

			short x;
			for (x = 0; x < ary.Length - 1; x++)
			{
				if (ary[x] != aryCompare[x])
				{
					return false;
				}
			}
			return true;
		}

		private Dictionary<string, IEnumerable<string>> GetRoadTypesDictionary()
		{
			Dictionary<string, IEnumerable<string>> GetRoadTypesDictionary = new Dictionary<string, IEnumerable<string>>();
			Dictionary<string, IEnumerable<string>> rTypesDictionary = new Dictionary<string, IEnumerable<string>>();
			IEnumerable<string> permutationCollection = new List<string>();

			permutationCollection = GetPermutationCollectionForRoad();
			rTypesDictionary.Add("road", permutationCollection);

			permutationCollection = GetPermutationCollectionForAvenue();
			rTypesDictionary.Add("avenue", permutationCollection);

			permutationCollection = GetPermutationCollectionForStreet();
			rTypesDictionary.Add("street", permutationCollection);

			permutationCollection = GetPermutationCollectionForDrive();
			rTypesDictionary.Add("drive", permutationCollection);

			permutationCollection = GetPermutationCollectionForCircle();
			rTypesDictionary.Add("circle", permutationCollection);

			permutationCollection = GetPermutationCollectionForLane();
			rTypesDictionary.Add("lane", permutationCollection);

			permutationCollection = GetPermutationCollectionForBoulevard();
			rTypesDictionary.Add("boulevard", permutationCollection);

			permutationCollection = GetPermutationCollectionForExtension();
			rTypesDictionary.Add("extension", permutationCollection);

			permutationCollection = GetPermutationCollectionForJunction();
			rTypesDictionary.Add("junction", permutationCollection);

			permutationCollection = GetPermutationCollectionForCourt();
			rTypesDictionary.Add("court", permutationCollection);

			GetRoadTypesDictionary = rTypesDictionary;
			return GetRoadTypesDictionary;
		}

		private IEnumerable<string> GetPermutationCollectionForRoad()
		{
			List<string> roadsCollection = new List<string>();
			roadsCollection.Add("road");
			roadsCollection.Add("rd");
			roadsCollection.Add("raod");
			return roadsCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForAvenue()
		{
			List<string> aveCollection = new List<string>();
			aveCollection.Add("avenue");
			aveCollection.Add("ave");
			return aveCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForStreet()
		{
			List<string> streetCollection = new List<string>();
			streetCollection.Add("street");
			streetCollection.Add("st");
			return streetCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForDrive()
		{
			List<string> driveCollection = new List<string>();
			driveCollection.Add("drive");
			driveCollection.Add("dr");
			return driveCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForCircle()
		{
			List<string> circleCollection = new List<string>();
			circleCollection.Add("circle");
			circleCollection.Add("cir");
			circleCollection.Add("circ");
			return circleCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForLane()
		{
			List<string> laneCollection = new List<string>();
			laneCollection.Add("lane");
			laneCollection.Add("ln");
			return laneCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForBoulevard()
		{
			List<string> boulevardCollection = new List<string>();
			boulevardCollection.Add("boulevard");
			boulevardCollection.Add("boul");
			boulevardCollection.Add("blvd");
			boulevardCollection.Add("bl");
			return boulevardCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForCourt()
		{
			List<string> courtCollection = new List<string>();
			courtCollection.Add("court");
			courtCollection.Add("ct");
			return courtCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForExtension()
		{
			List<string> extCollection = new List<string>();
			extCollection.Add("extension");
			extCollection.Add("ext");
			return extCollection;
		}

		private IEnumerable<string> GetPermutationCollectionForJunction()
		{
			List<string> junctionCollection = new List<string>(); 
			junctionCollection.Add("junction");
			junctionCollection.Add("jct");
			junctionCollection.Add("junc");
			return junctionCollection;
		}

		private Dictionary<string, string> BuildRoadPermutationsDictionary(ref Dictionary<string, IEnumerable<string>> rTypesDictionary)
		{
			Dictionary<string, string> permutationsDictionary = new Dictionary<string, string>();

			IEnumerable<string> tempCollection = new List<string>();
			foreach (var entry in rTypesDictionary)
			{
				tempCollection = entry.Value;
				foreach (string description in tempCollection)
				{
					permutationsDictionary.Add(description, entry.Key);
				}
			}

			return permutationsDictionary;
		}
	}
}
