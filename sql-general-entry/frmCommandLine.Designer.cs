//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmCommandLine.
	/// </summary>
	partial class frmCommandLine : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbExample;
		public fecherFoundation.FCLabel lblExample;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optExample;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCFrame fraPkzip;
		public fecherFoundation.FCLine Line24;
		public fecherFoundation.FCLine Line23;
		public fecherFoundation.FCLine Line22;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLine Line12;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLine Line11;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLine Line8;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCFrame fraCopy;
		public fecherFoundation.FCLine Line30;
		public fecherFoundation.FCLine Line29;
		public fecherFoundation.FCLine Line28;
		public fecherFoundation.FCLine Line27;
		public fecherFoundation.FCLine Line26;
		public fecherFoundation.FCLine Line25;
		public fecherFoundation.FCLine Line6;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLine Line5;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLine Line4;
		public fecherFoundation.FCLine Line3;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCListBox lstResults;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCFrame fraDirectory;
		public fecherFoundation.FCLine Line18;
		public fecherFoundation.FCLine Line17;
		public fecherFoundation.FCLine Line16;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLine Line15;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLine Line14;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLine Line13;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCFrame fraUnzip;
		public fecherFoundation.FCLine Line21;
		public fecherFoundation.FCLine Line20;
		public fecherFoundation.FCLine Line19;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLine Line10;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLine Line9;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLine Line7;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCButton cmdExecute;
		public fecherFoundation.FCTextBox txtCommand;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbExample = new fecherFoundation.FCComboBox();
			this.lblExample = new fecherFoundation.FCLabel();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCommandLine));
			this.components = new System.ComponentModel.Container();
			//this.optExample = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.Command1 = new fecherFoundation.FCButton();
			this.fraPkzip = new fecherFoundation.FCFrame();
			this.Line24 = new fecherFoundation.FCLine();
			this.Line23 = new fecherFoundation.FCLine();
			this.Line22 = new fecherFoundation.FCLine();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Line12 = new fecherFoundation.FCLine();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Line11 = new fecherFoundation.FCLine();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Line8 = new fecherFoundation.FCLine();
			this.Label11 = new fecherFoundation.FCLabel();
			this.fraCopy = new fecherFoundation.FCFrame();
			this.Line30 = new fecherFoundation.FCLine();
			this.Line29 = new fecherFoundation.FCLine();
			this.Line28 = new fecherFoundation.FCLine();
			this.Line27 = new fecherFoundation.FCLine();
			this.Line26 = new fecherFoundation.FCLine();
			this.Line25 = new fecherFoundation.FCLine();
			this.Line6 = new fecherFoundation.FCLine();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Line5 = new fecherFoundation.FCLine();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Line4 = new fecherFoundation.FCLine();
			this.Line3 = new fecherFoundation.FCLine();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Line2 = new fecherFoundation.FCLine();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Line1 = new fecherFoundation.FCLine();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lstResults = new fecherFoundation.FCListBox();
			this.cmdDone = new fecherFoundation.FCButton();
			this.fraDirectory = new fecherFoundation.FCFrame();
			this.Line18 = new fecherFoundation.FCLine();
			this.Line17 = new fecherFoundation.FCLine();
			this.Line16 = new fecherFoundation.FCLine();
			this.Label24 = new fecherFoundation.FCLabel();
			this.Line15 = new fecherFoundation.FCLine();
			this.Label23 = new fecherFoundation.FCLabel();
			this.Line14 = new fecherFoundation.FCLine();
			this.Label22 = new fecherFoundation.FCLabel();
			this.Line13 = new fecherFoundation.FCLine();
			this.Label21 = new fecherFoundation.FCLabel();
			this.Label20 = new fecherFoundation.FCLabel();
			this.fraUnzip = new fecherFoundation.FCFrame();
			this.Line21 = new fecherFoundation.FCLine();
			this.Line20 = new fecherFoundation.FCLine();
			this.Line19 = new fecherFoundation.FCLine();
			this.Label19 = new fecherFoundation.FCLabel();
			this.Line10 = new fecherFoundation.FCLine();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Line9 = new fecherFoundation.FCLine();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Line7 = new fecherFoundation.FCLine();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.cmdExecute = new fecherFoundation.FCButton();
			this.txtCommand = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// cmbExample
			//
			this.cmbExample.Items.Add("Directory");
			this.cmbExample.Items.Add("PKUnzip");
			this.cmbExample.Items.Add("PKZip");
			this.cmbExample.Items.Add("Copying Files");
			this.cmbExample.Name = "cmbExample";
			this.cmbExample.Location = new System.Drawing.Point(396, 32);
			this.cmbExample.Size = new System.Drawing.Size(120, 40);
			this.cmbExample.SelectedIndexChanged += new System.EventHandler(this.cmbExample_SelectedIndexChanged);
			//
			// lblExample
			//
			this.lblExample.Name = "lblExample";
			this.lblExample.Text = "lblExample";
			this.lblExample.Location = new System.Drawing.Point(376, 32);
			this.lblExample.AutoSize = true;

			//((System.ComponentModel.ISupportInitialize)(this.optExample)).BeginInit();
			//
			// Command1
			//
			this.Command1.Name = "Command1";
			this.Command1.TabIndex = 2;
			this.Command1.Location = new System.Drawing.Point(309, 79);
			this.Command1.Size = new System.Drawing.Size(151, 30);
			this.Command1.Text = "Run in Current Directory";
			this.Command1.BackColor = System.Drawing.SystemColors.Control;
			this.Command1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			//
			// fraPkzip
			//
			this.fraPkzip.Controls.Add(this.Line24);
			this.fraPkzip.Controls.Add(this.Line23);
			this.fraPkzip.Controls.Add(this.Line22);
			this.fraPkzip.Controls.Add(this.Label17);
			this.fraPkzip.Controls.Add(this.Label16);
			this.fraPkzip.Controls.Add(this.Line12);
			this.fraPkzip.Controls.Add(this.Label15);
			this.fraPkzip.Controls.Add(this.Line11);
			this.fraPkzip.Controls.Add(this.Label14);
			this.fraPkzip.Controls.Add(this.Line8);
			this.fraPkzip.Controls.Add(this.Label11);
			this.fraPkzip.Name = "fraPkzip";
			this.fraPkzip.TabIndex = 18;
			this.fraPkzip.Location = new System.Drawing.Point(5, 207);
			this.fraPkzip.Size = new System.Drawing.Size(593, 317);
			this.fraPkzip.Text = "PKZip";
			this.fraPkzip.BackColor = System.Drawing.SystemColors.Control;
			this.fraPkzip.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Line24
			//
			this.Line24.Name = "Line24";
			this.Line24.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line24.X1 = 2296;
			this.Line24.Y1 = 1606;
			this.Line24.X2 = 3186;
			this.Line24.Y2 = 1606;
			this.Line24.Location = new System.Drawing.Point(2296, 1606);
			this.Line24.Size = new System.Drawing.Size(890, 1);
			//
			// Line23
			//
			this.Line23.Name = "Line23";
			this.Line23.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line23.X1 = 3495;
			this.Line23.Y1 = 1613;
			this.Line23.X2 = 6350;
			this.Line23.Y2 = 1613;
			this.Line23.Location = new System.Drawing.Point(3495, 1613);
			this.Line23.Size = new System.Drawing.Size(2855, 1);
			//
			// Line22
			//
			this.Line22.Name = "Line22";
			this.Line22.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line22.X1 = 1148;
			this.Line22.Y1 = 1606;
			this.Line22.X2 = 1815;
			this.Line22.Y2 = 1606;
			this.Line22.Location = new System.Drawing.Point(1148, 1606);
			this.Line22.Size = new System.Drawing.Size(667, 1);
			//
			// Label17
			//
			this.Label17.Name = "Label17";
			this.Label17.TabIndex = 23;
			this.Label17.Location = new System.Drawing.Point(25, 35);
			this.Label17.Size = new System.Drawing.Size(96, 27);
			this.Label17.Text = "Zipping Files";
			this.Label17.BackColor = System.Drawing.SystemColors.Control;
			this.Label17.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Italic), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label16
			//
			this.Label16.Name = "Label16";
			this.Label16.TabIndex = 22;
			this.Label16.Location = new System.Drawing.Point(75, 70);
			this.Label16.Size = new System.Drawing.Size(443, 40);
			this.Label16.Text = "PKZIP a:\\Trio.zip   c:\\Trio\\tsgngn55.vb1";
			this.Label16.BackColor = System.Drawing.SystemColors.Control;
			this.Label16.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label16.Font = new System.Drawing.Font("Tahoma", 14.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Line12
			//
			this.Line12.Name = "Line12";
			this.Line12.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line12.X1 = 1332;
			this.Line12.Y1 = 1558;
			this.Line12.X2 = 1222;
			this.Line12.Y2 = 1982;
			this.Line12.Location = new System.Drawing.Point(1222, 1558);
			this.Line12.Size = new System.Drawing.Size(110, 424);
			//
			// Label15
			//
			this.Label15.Name = "Label15";
			this.Label15.TabIndex = 21;
			this.Label15.Location = new System.Drawing.Point(25, 134);
			this.Label15.Size = new System.Drawing.Size(71, 52);
			this.Label15.Text = "the PKZip Command";
			this.Label15.BackColor = System.Drawing.SystemColors.Control;
			this.Label15.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Line11
			//
			this.Line11.Name = "Line11";
			this.Line11.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line11.X1 = 2816;
			this.Line11.Y1 = 1606;
			this.Line11.X2 = 2704;
			this.Line11.Y2 = 1982;
			this.Line11.Location = new System.Drawing.Point(2704, 1606);
			this.Line11.Size = new System.Drawing.Size(112, 376);
			//
			// Label14
			//
			this.Label14.Name = "Label14";
			this.Label14.TabIndex = 20;
			this.Label14.Location = new System.Drawing.Point(122, 134);
			this.Label14.Size = new System.Drawing.Size(179, 110);
			this.Label14.Text = "the Drive, Directory and Name of the Zip file to be created.                  eg. a:\\trio.zip";
			this.Label14.BackColor = System.Drawing.SystemColors.Control;
			this.Label14.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Line8
			//
			this.Line8.Name = "Line8";
			this.Line8.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line8.X1 = 5890;
			this.Line8.Y1 = 1606;
			this.Line8.X2 = 5925;
			this.Line8.Y2 = 2077;
			this.Line8.Location = new System.Drawing.Point(5890, 1606);
			this.Line8.Size = new System.Drawing.Size(35, 471);
			//
			// Label11
			//
			this.Label11.Name = "Label11";
			this.Label11.TabIndex = 19;
			this.Label11.Location = new System.Drawing.Point(367, 153);
			this.Label11.Size = new System.Drawing.Size(186, 107);
			this.Label11.Text = "the Drive,Directory and name(s) of the files to be zipped.                       eg. c:\\trioville\\twbd*.vb1";
			this.Label11.BackColor = System.Drawing.SystemColors.Control;
			this.Label11.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// fraCopy
			//
			this.fraCopy.Controls.Add(this.Line30);
			this.fraCopy.Controls.Add(this.Line29);
			this.fraCopy.Controls.Add(this.Line28);
			this.fraCopy.Controls.Add(this.Line27);
			this.fraCopy.Controls.Add(this.Line26);
			this.fraCopy.Controls.Add(this.Line25);
			this.fraCopy.Controls.Add(this.Line6);
			this.fraCopy.Controls.Add(this.Label9);
			this.fraCopy.Controls.Add(this.Label8);
			this.fraCopy.Controls.Add(this.Line5);
			this.fraCopy.Controls.Add(this.Label7);
			this.fraCopy.Controls.Add(this.Line4);
			this.fraCopy.Controls.Add(this.Line3);
			this.fraCopy.Controls.Add(this.Label6);
			this.fraCopy.Controls.Add(this.Label5);
			this.fraCopy.Controls.Add(this.Line2);
			this.fraCopy.Controls.Add(this.Label4);
			this.fraCopy.Controls.Add(this.Line1);
			this.fraCopy.Controls.Add(this.Label3);
			this.fraCopy.Controls.Add(this.Label2);
			this.fraCopy.Name = "fraCopy";
			this.fraCopy.Visible = false;
			this.fraCopy.TabIndex = 9;
			this.fraCopy.Location = new System.Drawing.Point(5, 209);
			this.fraCopy.Size = new System.Drawing.Size(593, 301);
			this.fraCopy.Text = "Copy";
			this.fraCopy.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line30
			//
			this.Line30.Name = "Line30";
			this.Line30.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line30.X1 = 6186;
			this.Line30.Y1 = 1606;
			this.Line30.X2 = 6704;
			this.Line30.Y2 = 1606;
			this.Line30.Location = new System.Drawing.Point(6186, 1606);
			this.Line30.Size = new System.Drawing.Size(518, 1);
			//
			// Line29
			//
			this.Line29.Name = "Line29";
			this.Line29.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line29.X1 = 5223;
			this.Line29.Y1 = 1653;
			this.Line29.X2 = 5741;
			this.Line29.Y2 = 1653;
			this.Line29.Location = new System.Drawing.Point(5223, 1653);
			this.Line29.Size = new System.Drawing.Size(518, 1);
			//
			// Line28
			//
			this.Line28.Name = "Line28";
			this.Line28.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line28.X1 = 1889;
			this.Line28.Y1 = 1558;
			this.Line28.X2 = 2149;
			this.Line28.Y2 = 1558;
			this.Line28.Location = new System.Drawing.Point(1889, 1558);
			this.Line28.Size = new System.Drawing.Size(260, 1);
			//
			// Line27
			//
			this.Line27.Name = "Line27";
			this.Line27.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line27.X1 = 2410;
			this.Line27.Y1 = 1558;
			this.Line27.X2 = 2924;
			this.Line27.Y2 = 1558;
			this.Line27.Location = new System.Drawing.Point(2410, 1558);
			this.Line27.Size = new System.Drawing.Size(514, 1);
			//
			// Line26
			//
			this.Line26.Name = "Line26";
			this.Line26.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line26.X1 = 3259;
			this.Line26.Y1 = 1653;
			this.Line26.X2 = 4889;
			this.Line26.Y2 = 1653;
			this.Line26.Location = new System.Drawing.Point(3259, 1653);
			this.Line26.Size = new System.Drawing.Size(1630, 1);
			//
			// Line25
			//
			this.Line25.Name = "Line25";
			this.Line25.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line25.X1 = 1110;
			this.Line25.Y1 = 1511;
			this.Line25.X2 = 1629;
			this.Line25.Y2 = 1511;
			this.Line25.Location = new System.Drawing.Point(1110, 1511);
			this.Line25.Size = new System.Drawing.Size(519, 1);
			//
			// Line6
			//
			this.Line6.Name = "Line6";
			this.Line6.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line6.X1 = 6557;
			this.Line6.Y1 = 1606;
			this.Line6.X2 = 7519;
			this.Line6.Y2 = 2455;
			this.Line6.Location = new System.Drawing.Point(6557, 1606);
			this.Line6.Size = new System.Drawing.Size(962, 849);
			//
			// Label9
			//
			this.Label9.Name = "Label9";
			this.Label9.TabIndex = 17;
			this.Label9.Location = new System.Drawing.Point(449, 175);
			this.Label9.Size = new System.Drawing.Size(126, 78);
			this.Label9.Text = "the Directory where the copied file is going to.";
			this.Label9.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label8
			//
			this.Label8.Name = "Label8";
			this.Label8.TabIndex = 16;
			this.Label8.Location = new System.Drawing.Point(320, 130);
			this.Label8.Size = new System.Drawing.Size(126, 78);
			this.Label8.Text = "the Drive letter where the copied file is going to.";
			this.Label8.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line5
			//
			this.Line5.Name = "Line5";
			this.Line5.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line5.X1 = 5482;
			this.Line5.Y1 = 1653;
			this.Line5.X2 = 5519;
			this.Line5.Y2 = 2030;
			this.Line5.Location = new System.Drawing.Point(5482, 1653);
			this.Line5.Size = new System.Drawing.Size(37, 377);
			//
			// Label7
			//
			this.Label7.Name = "Label7";
			this.Label7.TabIndex = 15;
			this.Label7.Location = new System.Drawing.Point(215, 235);
			this.Label7.Size = new System.Drawing.Size(144, 52);
			this.Label7.Text = "the  Name of the File to be copied.";
			this.Label7.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line4
			//
			this.Line4.Name = "Line4";
			this.Line4.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line4.X1 = 4334;
			this.Line4.Y1 = 1653;
			this.Line4.X2 = 4334;
			this.Line4.Y2 = 3493;
			this.Line4.Location = new System.Drawing.Point(4334, 1653);
			this.Line4.Size = new System.Drawing.Size(1, 1840);
			//
			// Line3
			//
			this.Line3.Name = "Line3";
			this.Line3.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line3.X1 = 2593;
			this.Line3.Y1 = 1558;
			this.Line3.X2 = 2593;
			this.Line3.Y2 = 1795;
			this.Line3.Location = new System.Drawing.Point(2593, 1558);
			this.Line3.Size = new System.Drawing.Size(1, 237);
			//
			// Label6
			//
			this.Label6.Name = "Label6";
			this.Label6.TabIndex = 14;
			this.Label6.Location = new System.Drawing.Point(142, 124);
			this.Label6.Size = new System.Drawing.Size(144, 81);
			this.Label6.Text = "the Directory where the file to be copied is located.";
			this.Label6.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label5
			//
			this.Label5.Name = "Label5";
			this.Label5.TabIndex = 13;
			this.Label5.Location = new System.Drawing.Point(37, 207);
			this.Label5.Size = new System.Drawing.Size(144, 81);
			this.Label5.Text = "the Drive letter where the file to be copied is located.";
			this.Label5.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line2
			//
			this.Line2.Name = "Line2";
			this.Line2.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line2.X1 = 2000;
			this.Line2.Y1 = 1558;
			this.Line2.X2 = 1557;
			this.Line2.Y2 = 2928;
			this.Line2.Location = new System.Drawing.Point(1557, 1558);
			this.Line2.Size = new System.Drawing.Size(443, 1370);
			//
			// Label4
			//
			this.Label4.Name = "Label4";
			this.Label4.TabIndex = 12;
			this.Label4.Location = new System.Drawing.Point(22, 130);
			this.Label4.Size = new System.Drawing.Size(71, 52);
			this.Label4.Text = "the Copy Command";
			this.Label4.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line1
			//
			this.Line1.Name = "Line1";
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.X1 = 1296;
			this.Line1.Y1 = 1511;
			this.Line1.X2 = 1148;
			this.Line1.Y2 = 2266;
			this.Line1.Location = new System.Drawing.Point(1148, 1511);
			this.Line1.Size = new System.Drawing.Size(148, 755);
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.TabIndex = 11;
			this.Label3.Location = new System.Drawing.Point(70, 67);
			this.Label3.Size = new System.Drawing.Size(368, 40);
			this.Label3.Text = "Copy c:\\Trio\\tsgngn55.vb1  a:\\Test";
			this.Label3.BackColor = System.Drawing.SystemColors.Control;
			this.Label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label3.Font = new System.Drawing.Font("Tahoma", 14.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 10;
			this.Label2.Location = new System.Drawing.Point(22, 32);
			this.Label2.Size = new System.Drawing.Size(96, 27);
			this.Label2.Text = "Copying Files";
			this.Label2.BackColor = System.Drawing.SystemColors.Control;
			this.Label2.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Italic), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lstResults
			//
			this.lstResults.Name = "lstResults";
			this.lstResults.Visible = false;
			this.lstResults.TabIndex = 37;
			this.lstResults.Location = new System.Drawing.Point(2, 205);
			this.lstResults.Size = new System.Drawing.Size(596, 279);
			this.lstResults.BackColor = System.Drawing.SystemColors.Window;
			//
			// cmdDone
			//
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.TabIndex = 3;
			this.cmdDone.Location = new System.Drawing.Point(483, 79);
			this.cmdDone.Size = new System.Drawing.Size(118, 30);
			this.cmdDone.Text = "Return to Menu";
			this.cmdDone.BackColor = System.Drawing.SystemColors.Control;
			this.cmdDone.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			//
			//
			//
			// fraDirectory
			//
			this.fraDirectory.Controls.Add(this.Line18);
			this.fraDirectory.Controls.Add(this.Line17);
			this.fraDirectory.Controls.Add(this.Line16);
			this.fraDirectory.Controls.Add(this.Label24);
			this.fraDirectory.Controls.Add(this.Line15);
			this.fraDirectory.Controls.Add(this.Label23);
			this.fraDirectory.Controls.Add(this.Line14);
			this.fraDirectory.Controls.Add(this.Label22);
			this.fraDirectory.Controls.Add(this.Line13);
			this.fraDirectory.Controls.Add(this.Label21);
			this.fraDirectory.Controls.Add(this.Label20);
			this.fraDirectory.Name = "fraDirectory";
			this.fraDirectory.Visible = false;
			this.fraDirectory.TabIndex = 30;
			this.fraDirectory.Location = new System.Drawing.Point(5, 209);
			this.fraDirectory.Size = new System.Drawing.Size(593, 317);
			this.fraDirectory.Text = "Directory Listing";
			this.fraDirectory.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line18
			//
			this.Line18.Name = "Line18";
			this.Line18.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line18.X1 = 3444;
			this.Line18.Y1 = 1606;
			this.Line18.X2 = 4851;
			this.Line18.Y2 = 1606;
			this.Line18.Location = new System.Drawing.Point(3444, 1606);
			this.Line18.Size = new System.Drawing.Size(1407, 1);
			//
			// Line17
			//
			this.Line17.Name = "Line17";
			this.Line17.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line17.X1 = 1110;
			this.Line17.Y1 = 1558;
			this.Line17.X2 = 1592;
			this.Line17.Y2 = 1558;
			this.Line17.Location = new System.Drawing.Point(1110, 1558);
			this.Line17.Size = new System.Drawing.Size(482, 1);
			//
			// Line16
			//
			this.Line16.Name = "Line16";
			this.Line16.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line16.X1 = 2000;
			this.Line16.Y1 = 1700;
			this.Line16.X2 = 2853;
			this.Line16.Y2 = 1700;
			this.Line16.Location = new System.Drawing.Point(2000, 1700);
			this.Line16.Size = new System.Drawing.Size(853, 1);
			//
			// Label24
			//
			this.Label24.Name = "Label24";
			this.Label24.TabIndex = 35;
			this.Label24.Location = new System.Drawing.Point(327, 143);
			this.Label24.Size = new System.Drawing.Size(191, 84);
			this.Label24.Text = "the Name of the file(s) you want information about.   eg.  DIR c:\\trio\\tsbd*.vb1";
			this.Label24.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line15
			//
			this.Line15.Name = "Line15";
			this.Line15.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line15.X1 = 4630;
			this.Line15.Y1 = 1606;
			this.Line15.X2 = 5223;
			this.Line15.Y2 = 2124;
			this.Line15.Location = new System.Drawing.Point(4630, 1606);
			this.Line15.Size = new System.Drawing.Size(593, 518);
			//
			// Label23
			//
			this.Label23.Name = "Label23";
			this.Label23.TabIndex = 34;
			this.Label23.Location = new System.Drawing.Point(97, 204);
			this.Label23.Size = new System.Drawing.Size(179, 78);
			this.Label23.Text = "the Drive and Directory that you want a file listing of.";
			this.Label23.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line14
			//
			this.Line14.Name = "Line14";
			this.Line14.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line14.X1 = 2593;
			this.Line14.Y1 = 1700;
			this.Line14.X2 = 2334;
			this.Line14.Y2 = 2880;
			this.Line14.Location = new System.Drawing.Point(2334, 1700);
			this.Line14.Size = new System.Drawing.Size(259, 1180);
			//
			// Label22
			//
			this.Label22.Name = "Label22";
			this.Label22.TabIndex = 33;
			this.Label22.Location = new System.Drawing.Point(17, 134);
			this.Label22.Size = new System.Drawing.Size(91, 52);
			this.Label22.Text = "the Directory command";
			this.Label22.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line13
			//
			this.Line13.Name = "Line13";
			this.Line13.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line13.X1 = 1332;
			this.Line13.Y1 = 1558;
			this.Line13.X2 = 1222;
			this.Line13.Y2 = 1982;
			this.Line13.Location = new System.Drawing.Point(1222, 1558);
			this.Line13.Size = new System.Drawing.Size(110, 424);
			//
			// Label21
			//
			this.Label21.Name = "Label21";
			this.Label21.TabIndex = 32;
			this.Label21.Location = new System.Drawing.Point(72, 70);
			this.Label21.Size = new System.Drawing.Size(383, 40);
			this.Label21.Text = "DIR c:\\Trio\\tsgngn55.vb1 ";
			this.Label21.BackColor = System.Drawing.SystemColors.Control;
			this.Label21.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label21.Font = new System.Drawing.Font("Tahoma", 14.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label20
			//
			this.Label20.Name = "Label20";
			this.Label20.TabIndex = 31;
			this.Label20.Location = new System.Drawing.Point(25, 35);
			this.Label20.Size = new System.Drawing.Size(184, 27);
			this.Label20.Text = "Listing Files in a Directory";
			this.Label20.BackColor = System.Drawing.SystemColors.Control;
			this.Label20.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Italic), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// fraUnzip
			//
			this.fraUnzip.Controls.Add(this.Line21);
			this.fraUnzip.Controls.Add(this.Line20);
			this.fraUnzip.Controls.Add(this.Line19);
			this.fraUnzip.Controls.Add(this.Label19);
			this.fraUnzip.Controls.Add(this.Line10);
			this.fraUnzip.Controls.Add(this.Label18);
			this.fraUnzip.Controls.Add(this.Line9);
			this.fraUnzip.Controls.Add(this.Label13);
			this.fraUnzip.Controls.Add(this.Line7);
			this.fraUnzip.Controls.Add(this.Label12);
			this.fraUnzip.Controls.Add(this.Label10);
			this.fraUnzip.Name = "fraUnzip";
			this.fraUnzip.Visible = false;
			this.fraUnzip.TabIndex = 24;
			this.fraUnzip.Location = new System.Drawing.Point(5, 209);
			this.fraUnzip.Size = new System.Drawing.Size(593, 317);
			this.fraUnzip.Text = "PKUnZip";
			this.fraUnzip.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line21
			//
			this.Line21.Name = "Line21";
			this.Line21.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line21.X1 = 1148;
			this.Line21.Y1 = 1558;
			this.Line21.X2 = 2037;
			this.Line21.Y2 = 1558;
			this.Line21.Location = new System.Drawing.Point(1148, 1558);
			this.Line21.Size = new System.Drawing.Size(889, 1);
			//
			// Line20
			//
			this.Line20.Name = "Line20";
			this.Line20.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line20.X1 = 5482;
			this.Line20.Y1 = 1606;
			this.Line20.X2 = 6222;
			this.Line20.Y2 = 1606;
			this.Line20.Location = new System.Drawing.Point(5482, 1606);
			this.Line20.Size = new System.Drawing.Size(740, 1);
			//
			// Line19
			//
			this.Line19.Name = "Line19";
			this.Line19.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line19.X1 = 2593;
			this.Line19.Y1 = 1606;
			this.Line19.X2 = 4963;
			this.Line19.Y2 = 1606;
			this.Line19.Location = new System.Drawing.Point(2593, 1606);
			this.Line19.Size = new System.Drawing.Size(2370, 1);
			//
			// Label19
			//
			this.Label19.Name = "Label19";
			this.Label19.TabIndex = 29;
			this.Label19.Location = new System.Drawing.Point(367, 153);
			this.Label19.Size = new System.Drawing.Size(186, 107);
			this.Label19.Text = "the Drive and Directory of the location where the unzipped files are to go.    eg. c:\\trioville";
			this.Label19.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line10
			//
			this.Line10.Name = "Line10";
			this.Line10.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line10.X1 = 5890;
			this.Line10.Y1 = 1606;
			this.Line10.X2 = 5925;
			this.Line10.Y2 = 2077;
			this.Line10.Location = new System.Drawing.Point(5890, 1606);
			this.Line10.Size = new System.Drawing.Size(35, 471);
			//
			// Label18
			//
			this.Label18.Name = "Label18";
			this.Label18.TabIndex = 28;
			this.Label18.Location = new System.Drawing.Point(122, 134);
			this.Label18.Size = new System.Drawing.Size(179, 110);
			this.Label18.Text = "the Drive, Directory and Name of the Zip file to be unZipped.                  eg. a:\\trioville\\twbd.zip";
			this.Label18.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line9
			//
			this.Line9.Name = "Line9";
			this.Line9.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line9.X1 = 2816;
			this.Line9.Y1 = 1606;
			this.Line9.X2 = 2704;
			this.Line9.Y2 = 1982;
			this.Line9.Location = new System.Drawing.Point(2704, 1606);
			this.Line9.Size = new System.Drawing.Size(112, 376);
			//
			// Label13
			//
			this.Label13.Name = "Label13";
			this.Label13.TabIndex = 27;
			this.Label13.Location = new System.Drawing.Point(25, 134);
			this.Label13.Size = new System.Drawing.Size(71, 52);
			this.Label13.Text = "the PKUnZip Command";
			this.Label13.BackColor = System.Drawing.SystemColors.Control;
			//
			// Line7
			//
			this.Line7.Name = "Line7";
			this.Line7.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line7.X1 = 1332;
			this.Line7.Y1 = 1558;
			this.Line7.X2 = 1222;
			this.Line7.Y2 = 1982;
			this.Line7.Location = new System.Drawing.Point(1222, 1558);
			this.Line7.Size = new System.Drawing.Size(110, 424);
			//
			// Label12
			//
			this.Label12.Name = "Label12";
			this.Label12.TabIndex = 26;
			this.Label12.Location = new System.Drawing.Point(72, 70);
			this.Label12.Size = new System.Drawing.Size(431, 40);
			this.Label12.Text = "PKUNZIP a:\\Test\\twdn.zip  c:\\Trio";
			this.Label12.BackColor = System.Drawing.SystemColors.Control;
			this.Label12.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.Label12.Font = new System.Drawing.Font("Tahoma", 14.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label10
			//
			this.Label10.Name = "Label10";
			this.Label10.TabIndex = 25;
			this.Label10.Location = new System.Drawing.Point(25, 35);
			this.Label10.Size = new System.Drawing.Size(114, 27);
			this.Label10.Text = "UnZipping Files";
			this.Label10.BackColor = System.Drawing.SystemColors.Control;
			this.Label10.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Italic), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// cmdExecute
			//
			this.cmdExecute.Name = "cmdExecute";
			this.cmdExecute.TabIndex = 1;
			this.cmdExecute.Location = new System.Drawing.Point(166, 79);
			this.cmdExecute.Size = new System.Drawing.Size(118, 30);
			this.cmdExecute.Text = "Execute";
			this.cmdExecute.BackColor = System.Drawing.SystemColors.Control;
			this.cmdExecute.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdExecute.Click += new System.EventHandler(this.cmdExecute_Click);
			//
			// txtCommand
			//
			this.txtCommand.Name = "txtCommand";
			this.txtCommand.TabIndex = 0;
			this.txtCommand.Location = new System.Drawing.Point(165, 34);
			this.txtCommand.Size = new System.Drawing.Size(436, 38);
			this.txtCommand.Text = "";
			this.txtCommand.BackColor = System.Drawing.SystemColors.Window;
			this.txtCommand.BorderStyle = Wisej.Web.BorderStyle.FixedSingle;
			this.txtCommand.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCommand_KeyDown);
			//
			// Label1
			//
			this.Label1.Name = "Label1";
			this.Label1.TabIndex = 8;
			this.Label1.Location = new System.Drawing.Point(5, 34);
			this.Label1.Size = new System.Drawing.Size(154, 27);
			this.Label1.Text = "Enter the Command:";
			this.Label1.BackColor = System.Drawing.SystemColors.Control;
			this.Label1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(257, 50);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "&File";
			//
			// mnuFileExit
			//
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "E&xit    (Esc)";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuFile});
			//
			// frmCommandLine
			//
			this.ClientSize = new System.Drawing.Size(610, 514);
			this.Controls.Add(this.Command1);
			this.Controls.Add(this.fraPkzip);
			this.Controls.Add(this.fraCopy);
			this.Controls.Add(this.lstResults);
			this.Controls.Add(this.cmdDone);
			this.Controls.Add(this.cmbExample);
			this.Controls.Add(this.lblExample);

			this.Controls.Add(this.fraDirectory);
			this.Controls.Add(this.fraUnzip);
			this.Controls.Add(this.cmdExecute);
			this.Controls.Add(this.txtCommand);
			this.Controls.Add(this.Label1);
			//this.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmCommandLine";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmCommandLine.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.WindowsDefaultLocation;
			this.Load += new System.EventHandler(this.frmCommandLine_Load);
			this.Text = "Command Line";
			//((System.ComponentModel.ISupportInitialize)(this.optExample)).EndInit();
			this.Command1.ResumeLayout(false);
			this.Label17.ResumeLayout(false);
			this.Label16.ResumeLayout(false);
			this.Label15.ResumeLayout(false);
			this.Label14.ResumeLayout(false);
			this.Label11.ResumeLayout(false);
			this.fraPkzip.ResumeLayout(false);
			this.Label3.ResumeLayout(false);
			this.Label2.ResumeLayout(false);
			this.fraCopy.ResumeLayout(false);
			this.cmdDone.ResumeLayout(false);
			this.Label21.ResumeLayout(false);
			this.Label20.ResumeLayout(false);
			this.fraDirectory.ResumeLayout(false);
			this.Label12.ResumeLayout(false);
			this.Label10.ResumeLayout(false);
			this.fraUnzip.ResumeLayout(false);
			this.cmdExecute.ResumeLayout(false);
			this.Label1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}