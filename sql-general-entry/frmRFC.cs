//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRFC.
	/// </summary>
	public partial class frmRFC : fecherFoundation.FCForm
	{
;

		public frmRFC()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.optType = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.optType.AddControlArrayElement(optType_2, 2 );
			this.optType.AddControlArrayElement(optType_1, 1 );
			this.optType.AddControlArrayElement(optType_0, 0 );

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		private string strModule = "";
		private string strSort = "";

		private void GetModule()
		{
			
			if (cboModule=="All Modules")
			{
				strModule = string.Empty;
			}
			else if (cboModule=="Real Estate")
			{
				strModule = "RE";
			}
			else if (cboModule=="Personal Property")
			{
				strModule = "PP";
			}
			else if (cboModule=="Tax Billing")
			{
				strModule = "BL";
			}
			else if (cboModule=="Real Estate Collections")
			{
				strModule = "CL";
			}
			else if (cboModule=="Personal Property Collections")
			{
				strModule = "CL";
			}
			else if (cboModule=="Clerk")
			{
				strModule = "CK";
			}
			else if (cboModule=="Voter Registration")
			{
				strModule = "VR";
			}
			else if (cboModule=="Code Enforcement")
			{
				strModule = "CE";
			}
			else if (cboModule=="Budgetary")
			{
				strModule = "BD";
			}
			else if (cboModule=="Cash Receipts")
			{
				strModule = "CR";
			}
			else if (cboModule=="Payroll")
			{
				strModule = "PY";
			}
			else if (cboModule=="Utility Billing")
			{
				strModule = "UT";
			}
			else if (cboModule=="Blue Book")
			{
				strModule = "RB";
			}
			else if (cboModule=="Motor Vehicle Registration")
			{
				strModule = "MV";
			}
			else if (cboModule=="Enhanced 911")
			{
				strModule = "E9";
			}
			else 
			{
				strModule = string.Empty;
			}
		}

		private void cboModule_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			GetModule();
			LoadGrid(strModule);
		}

		private void cboReleaseNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			GetModule();
			LoadGrid(strModule);
		}

		private void drvDrive_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			GetModule();
			LoadGrid(strModule);
		}

		private void frmRFC_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			switch (KeyCode) {
				
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					mnuExit_Click();
					break;
				}
			} //end switch
		}

		private void frmRFC_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRFC properties;
			//frmRFC.ScaleWidth	= 9045;
			//frmRFC.ScaleHeight	= 7290;
			//frmRFC.LinkTopic	= "Form1";
			//End Unmaped Properties

			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			LoadCombo();
			LoadModuleCombo();
			SetGridProperties();


		}

		private void frmRFC_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			// UNLOAD THIS FORM
			this.Unload();
		}
		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}


		private void optType_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			cboReleaseNumber.Visible = Index==1;
			drvDrive.Visible = Index==2;

			GetModule();
			LoadGrid(strModule);
		}
		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = optType.GetIndex((FCRadioButton)sender);
			optType_CheckedChanged(index, sender, e);
		}

		private bool LoadCombo()
		{
			bool LoadCombo = false;
			clsDRWrapper rsData = new clsDRWrapper();

			LoadCombo = false;
			cboReleaseNumber.AddItem("All Releases");
			if (rsData.OpenRecordset("Select distinct ReleaseNumber from tblRFC", modGNBas.DEFAULTDATABASE, , , , , , false)) {
				while (!rsData.EndOfFile()) {
					LoadCombo = true;
					cboReleaseNumber.AddItem(rsData.Get_Fields_String("ReleaseNumber"));

					rsData.MoveNext();
				}
			}
			rsData = null;
			return LoadCombo;
		}

		private void LoadModuleCombo()
		{
			cboModule.Clear();
			cboModule.AddItem("All Modules");
			cboModule.AddItem("Real Estate");
			cboModule.AddItem("Personal Property");
			cboModule.AddItem("Tax Billing");
			cboModule.AddItem("Real Estate Collections");
			cboModule.AddItem("Personal Property Collections");
			cboModule.AddItem("Clerk");
			cboModule.AddItem("Voter Registration");
			cboModule.AddItem("Code Enforcement");
			cboModule.AddItem("Budgetary");
			cboModule.AddItem("Cash Receipts");
			cboModule.AddItem("Payroll");
			cboModule.AddItem("Utility Billing");
			cboModule.AddItem("Blue Book");
			cboModule.AddItem("Motor Vehicle Registration");
			cboModule.AddItem("Enhanced 911");
		}

		private void SetGridProperties()
		{

			vsGrid.Cols = 4;
			vsGrid.Rows = 1;
			vsGrid.TextMatrix(0, 0, "Module");
			vsGrid.TextMatrix(0, 1, "Release Number");
			vsGrid.TextMatrix(0, 2, "RFC Number");
			vsGrid.TextMatrix(0, 3, "Description");



			vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);

		}

		private void ResizeGrid()
		{
			int GridWidth = 0;


			GridWidth = vsGrid.Width;
			vsGrid.ColWidth(0, Convert.ToInt32(0.118*GridWidth));
			vsGrid.ColWidth(1, Convert.ToInt32(0.176*GridWidth));
			vsGrid.ColWidth(2, Convert.ToInt32(0.176*GridWidth));
			vsGrid.ColWidth(3, Convert.ToInt32(0.5*GridWidth));

		}
		private void LoadGrid(string strModule)
		{
			try
			{	// On Error GoTo ErrorHandler

				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				string strReleaseNumber = "";

				vsGrid.Rows = 1;
				if (optType[0].Checked) {
					rsData.OpenRecordset("Select ReleaseNumber from GlobalVariables", modGNBas.DEFAULTDATABASE);
					if (rsData.EndOfFile()) {
						// THERE ARE NO RELEASE NUMBERS
						MessageBox.Show("There is no current release number in the GE database.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					} else {
						strReleaseNumber = Convert.ToString(rsData.Get_Fields_String("ReleaseNumber"));
						// THERE IS A RELEASE NUMBER
						if (strModule!=string.Empty) {
							rsData = null;
							rsData.OpenRecordset("Select * from tblRFC Where Module = '"+strModule+"' AND ReleaseNumber = '"+strReleaseNumber+"' Order by Module,ReleaseNumber,RFCNumber", modGNBas.DEFAULTDATABASE, , , , , , false);
						} else {
							rsData = null;
							rsData.OpenRecordset("Select * from tblRFC Where ReleaseNumber = '"+strReleaseNumber+"' Order by Module,ReleaseNumber,RFCNumber", modGNBas.DEFAULTDATABASE, , , , , , false);
						}
					}
				} else if (optType[1].Checked) {
					if (cboReleaseNumber.Text==string.Empty) {
						// THERE ARE NO RELEASE NUMBERS
						// MsgBox "A release number must be selected.", vbInformation + vbOKOnly, "TRIO Software"
						return;
					} else {
						strReleaseNumber = cboReleaseNumber.Text;
						// THERE IS A RELEASE NUMBER
						if (strReleaseNumber!="All Releases") {
							if (strModule!=string.Empty) {
								rsData = null;
								rsData.OpenRecordset("Select * from tblRFC Where Module = '"+strModule+"' AND ReleaseNumber = '"+strReleaseNumber+"' Order by Module,ReleaseNumber,RFCNumber", modGNBas.DEFAULTDATABASE);
							} else {
								rsData = null;
								rsData.OpenRecordset("Select * from tblRFC Where ReleaseNumber = '"+strReleaseNumber+"' Order by Module,ReleaseNumber,RFCNumber", modGNBas.DEFAULTDATABASE, , , , , , false);
							}
						} else {
							if (strModule!=string.Empty) {
								rsData = null;
								rsData.OpenRecordset("Select * from tblRFC Where Module = '"+strModule+"' Order by Module,ReleaseNumber,RFCNumber", modGNBas.DEFAULTDATABASE, , , , , , false);
							} else {
								rsData = null;
								rsData.OpenRecordset("Select * from tblRFC Order by Module,ReleaseNumber,RFCNumber", modGNBas.DEFAULTDATABASE, , , , , , false);
							}
						}
					}
				} else if (optType[2].Checked) {
					if (fso.FileExists(drvDrive.Text+"\\Install\\Datafiles\\Twrfc000.vb1")) {
						if (strModule!=string.Empty) {
							rsData = null;
							rsData.OpenRecordset("Select * from tblRFC Where Module = '"+strModule+"' Order by Module,ReleaseNumber,RFCNumber", drvDrive.Text+"\\Install\\Datafiles\\Twrfc000.vb1");
						} else {
							rsData = null;
							rsData.OpenRecordset("Select * from tblRFC Order by Module,ReleaseNumber,RFCNumber", drvDrive.Text+"\\Install\\Datafiles\\Twrfc000.vb1");
						}
					} else {
						MessageBox.Show("RFC database could not be found on CD drive "+drvDrive.Text+"\\Install\\Datafiles\\", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}

				if (!rsData.EndOfFile()) {
					vsGrid.Rows = rsData.RecordCount()+1;

					for(intCounter=1; intCounter<=rsData.RecordCount(); intCounter++) {
						vsGrid.TextMatrix(intCounter, 0, Convert.ToString(rsData.Get_Fields_String("Module")));
						vsGrid.TextMatrix(intCounter, 1, Convert.ToString(rsData.Get_Fields_String("ReleaseNumber")));
						// TODO Get_Fields: Field [RFCNumber] not found!! (maybe it is an alias?)
						vsGrid.TextMatrix(intCounter, 2, Convert.ToString(rsData.Get_Fields("RFCNumber")));
						vsGrid.TextMatrix(intCounter, 3, Convert.ToString(rsData.Get_Fields_String("Description")));


						rsData.MoveNext();
					}

					vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsGrid.Rows-1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}

				rsData = null;
				return;

			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show(Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description);
			}
		}

		private void vsGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsGrid.Rows<2) return;
			if (vsGrid.MouseRow==0) {
				// sort this grid
				vsGrid.Select(1, vsGrid.Col);
				if (strSort=="flexSortGenericAscending") {
					vsGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					strSort = "flexSortGenericDescending";
				} else {
					vsGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					strSort = "flexSortGenericAscending";
				}
			}
		}


		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbType.SelectedIndex == 0)
			{
				optType_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 1)
			{
				optType_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 2)
			{
				optType_CheckedChanged(sender, e);
			}
		}
	}
}
