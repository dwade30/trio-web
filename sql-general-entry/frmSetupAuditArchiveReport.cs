﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSetupAuditArchiveReport.
	/// </summary>
	public partial class frmSetupAuditArchiveReport : BaseForm
	{
		public frmSetupAuditArchiveReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupAuditArchiveReport InstancePtr
		{
			get
			{
				return (frmSetupAuditArchiveReport)Sys.GetInstance(typeof(frmSetupAuditArchiveReport));
			}
		}

		protected frmSetupAuditArchiveReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmSetupAuditArchiveReport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAuditArchiveReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAuditArchiveReport properties;
			//frmSetupAuditArchiveReport.FillStyle	= 0;
			//frmSetupAuditArchiveReport.ScaleWidth	= 9045;
			//frmSetupAuditArchiveReport.ScaleHeight	= 7245;
			//frmSetupAuditArchiveReport.LinkTopic	= "Form2";
			//frmSetupAuditArchiveReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadPartyIdCombo();
			LoadScreenCombo();
			LoadNameCombo();
		}

		private void frmSetupAuditArchiveReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptAuditArchiveReport.InstancePtr, showModal: true);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			rptAuditArchiveReport.InstancePtr.PrintReport(true);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void LoadPartyIdCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboPartyID.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT UserField1 FROM AuditChangesArchive ORDER BY UserField1", "SystemSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPartyID.AddItem(rsInfo.Get_Fields_String("UserField1"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadScreenCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboScreen.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT Location FROM AuditChangesArchive ORDER BY Location", "SystemSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboScreen.AddItem(rsInfo.Get_Fields_String("Location"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadNameCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboName.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT UserField2 FROM AuditChangesArchive ORDER BY UserField2", "SystemSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboName.AddItem(rsInfo.Get_Fields_String("UserField2"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void optDateAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = false;
			Label2.Enabled = false;
			txtLowDate.Enabled = false;
			txtHighDate.Enabled = false;
			txtLowDate.Text = "";
			txtHighDate.Text = "";
		}

		private void optDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			Label2.Enabled = true;
			txtLowDate.Enabled = true;
			txtHighDate.Enabled = true;
		}

		private void optPartyIdAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedPartyId.Enabled = false;
			cboPartyID.Enabled = false;
			cboPartyID.SelectedIndex = -1;
		}

		private void optPartyIdSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedPartyId.Enabled = true;
			cboPartyID.Enabled = true;
			if (cboPartyID.Items.Count > 0)
			{
				cboPartyID.SelectedIndex = 0;
			}
		}

		private void optScreenAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedScreen.Enabled = false;
			cboScreen.Enabled = false;
			cboScreen.SelectedIndex = -1;
		}

		private void optScreenSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedScreen.Enabled = true;
			cboScreen.Enabled = true;
			if (cboScreen.Items.Count > 0)
			{
				cboScreen.SelectedIndex = 0;
			}
		}

		private void optNameAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedName.Enabled = false;
			cboName.Enabled = false;
			cboName.SelectedIndex = -1;
		}

		private void optNameSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedName.Enabled = true;
			cboName.Enabled = true;
			if (cboName.Items.Count > 0)
			{
				cboName.SelectedIndex = 0;
			}
		}

		private void cmbPartyIdAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbPartyIdAll.Text == "All")
			{
				optPartyIdAll_CheckedChanged(sender, e);
			}
			else if (cmbPartyIdAll.Text == "Selected")
			{
				optPartyIdSelected_CheckedChanged(sender, e);
			}
		}

		private void cmbScreenAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbScreenAll.Text == "All")
			{
				optScreenAll_CheckedChanged(sender, e);
			}
			else if (cmbScreenAll.Text == "Selected")
			{
				optScreenSelected_CheckedChanged(sender, e);
			}
		}

		private void cmbDateAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbDateAll.Text == "All")
			{
				optDateAll_CheckedChanged(sender, e);
			}
			else if (cmbDateAll.Text == "Date Range")
			{
				optDateRange_CheckedChanged(sender, e);
			}
		}

		private void cmbNameSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbNameSelected.Text == "All")
			{
				optNameAll_CheckedChanged(sender, e);
			}
			else if (cmbNameSelected.Text == "Selected")
			{
				optNameSelected_CheckedChanged(sender, e);
			}
		}
	}
}
