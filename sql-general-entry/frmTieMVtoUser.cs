﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.DataBaseLayer;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmTieMVtoUser.
	/// </summary>
	public partial class frmTieMVtoUser : BaseForm
	{
		public frmTieMVtoUser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTieMVtoUser InstancePtr
		{
			get
			{
				return (frmTieMVtoUser)Sys.GetInstance(typeof(frmTieMVtoUser));
			}
		}

		protected frmTieMVtoUser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int lngUserID;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper dcTemp = new clsDRWrapper();
			clsDRWrapper clsTempSecurity = new clsDRWrapper();
			int x;
			int intNoneCount;
			// load list of all users
			clsTempSecurity.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "'", "ClientSettings");
			intNoneCount = 0;
			for (x = 1; x <= grid1.Rows - 1; x++)
			{
				lngUserID = 0;
				if (Strings.UCase(Strings.Trim(grid1.TextMatrix(x, 2))) == "NONE")
				{
					intNoneCount += 1;
				}
				clsTempSecurity.FindFirstRecord("username", Strings.Trim(grid1.TextMatrix(x, 2)));
				if (!clsTempSecurity.NoMatch)
				{
					lngUserID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTempSecurity.Get_Fields_Int32("id"))));
				}
				dcTemp.Execute("update operators set securityid = " + FCConvert.ToString(lngUserID) + " where code = '" + grid1.TextMatrix(x, 0) + "'", "systemsettings");
			}
			// x
			if (intNoneCount > 0)
			{
				MessageBox.Show("There are " + FCConvert.ToString(intNoneCount) + " MV operator(s) not tied to a userid", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			this.Unload();
		}

		private void frmTieMVtoUser_Activated(object sender, System.EventArgs e)
		{
			FillList1();
		}

		private void FillList1()
		{
			clsDRWrapper dcTemp = new clsDRWrapper();
			//DAO.Field fld = new DAO.Field();
			//TableDef tbl = new TableDef();
			//DAO.Database dbtemp;
			string strTemp;
			string strSeparator;
			clsDRWrapper clsTemp = new clsDRWrapper();
			// Set dbtemp = OpenDatabase("systemsettings", False, False, ";PWD=" & DATABASEPASSWORD)
			// For Each tbl In dbtemp.TableDefs
			// If UCase(tbl.Name) = "OPERATORS" Then
			// If Not FieldExists(tbl, "securityid") Then
			// Set fld = tbl.CreateField("SecurityID", dbLong)
			// Call tbl.Fields.Append(fld)
			// Exit For
			// Else
			// Exit For
			// End If
			// Exit For
			// End If
			// Next
			dcTemp.OpenRecordset("select * from operators", "systemsettings");
			clsTemp.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "'", "ClientSettings");
			strSeparator = "";
			strTemp = "";
			while (!clsTemp.EndOfFile())
			{
				strTemp += strSeparator + clsTemp.Get_Fields_String("UserName");
				strSeparator = "|";
				clsTemp.MoveNext();
			}
			strTemp += strSeparator + "None";
			grid1.Rows = 1;
			grid1.TextMatrix(0, 0, "MV OpID");
			grid1.TextMatrix(0, 1, "MV Operator");
			grid1.TextMatrix(0, 2, "User Name");
			grid1.ColComboList(2, strTemp);
			while (!dcTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				strTemp = FCConvert.ToString(dcTemp.Get_Fields("code"));
				strTemp += "\t";
				// If strTemp = vbNullString Then strTemp = "   "
				// strTemp = strTemp & "   " & dcTemp.Fields("name")
				strTemp += dcTemp.Get_Fields_String("name") + "\t";
				if (Conversion.Val(dcTemp.Get_Fields_Int32("securityid")) > 0)
				{
					clsTemp.FindFirstRecord("id", dcTemp.Get_Fields_Int32("securityid"));
					if (!clsTemp.EndOfFile())
					{
						if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("username"))) != string.Empty)
						{
							strTemp += clsTemp.Get_Fields_String("USERNAME");
						}
						else
						{
							strTemp += "None";
						}
					}
					else
					{
						strTemp += "None";
					}
				}
				else
				{
					strTemp += "None";
				}
				// List1.AddItem (strTemp)
				grid1.AddItem(strTemp);
				if (FCConvert.ToInt32(dcTemp.Get_Fields_Int32("securityid")) == lngUserID)
				{
					grid1.Row = grid1.Rows - 1;
				}
				dcTemp.MoveNext();
			}
		}

		private void frmTieMVtoUser_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTieMVtoUser properties;
			//frmTieMVtoUser.ScaleWidth	= 4080;
			//frmTieMVtoUser.ScaleHeight	= 2685;
			//frmTieMVtoUser.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = grid1.WidthOriginal;
			grid1.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.22));
			grid1.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.27));
			grid1.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.45));
		}

		private void frmTieMVtoUser_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}
	}
}
