﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Microsoft.VisualBasic.ApplicationServices;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWGNENTY
{
	public class modVersionCheck
	{
		public static bool InvalidVersion()
		{
			bool InvalidVersion = false;
			CFileVersionInfo MasterApp;
			CFileVersionInfo CurrentApp;
			string strTemp;
			string strExeLocation = "";
			int i;
			MasterApp = new CFileVersionInfo();
			strTemp = Application.MapPath("\\") + "\\";

			strTemp = StaticSettings.gGlobalSettings.MasterPath;
			if (strTemp != "" && Strings.Right("\\" + strTemp, 1) != "\\")
			{
				strTemp += "\\";
			}
			MasterApp.FullPathName = Strings.UCase(strTemp + (new WindowsFormsApplicationBase()).Info.AssemblyName + ".exe");
			if (!MasterApp.Available)
				return InvalidVersion;
			CurrentApp = new CFileVersionInfo();
			CurrentApp.FullPathName = Strings.UCase(FCFileSystem.Statics.UserDataFolder + "\\" + (new WindowsFormsApplicationBase()).Info.AssemblyName + ".exe");
			if (!CurrentApp.Available)
			{
				MessageBox.Show("Error in finding " + FCFileSystem.Statics.UserDataFolder + "\\" + (new WindowsFormsApplicationBase()).Info.AssemblyName + ".exe", "VersionCheck", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				InvalidVersion = true;
				return InvalidVersion;
			}
			if (MasterApp.FileVersion != CurrentApp.FileVersion)
			{
				// fso.CopyFile strTemp & "TrioMast\" & App.EXEName & ".exe", App.Path & "\" & "Master" & App.EXEName & ".exe"
				File.Copy(strTemp + App.EXEName + ".exe", FCFileSystem.Statics.UserDataFolder + "\\" + "Master" + App.EXEName + ".exe", true);
				Interaction.Shell(FCFileSystem.Statics.UserDataFolder + "\\" + "UpdateVersion.exe");
				InvalidVersion = true;
			}
			return InvalidVersion;
		}

		public class StaticVariables
		{
			//=========================================================
			public bool gboolInvalidVersion;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
