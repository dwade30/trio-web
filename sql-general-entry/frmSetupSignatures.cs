﻿using Global;
using fecherFoundation;
using Wisej.Web;
using System;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSetupSignatures.
	/// </summary>
	public partial class frmSetupSignatures : BaseForm
	{
		public frmSetupSignatures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupSignatures InstancePtr
		{
			get
			{
				return (frmSetupSignatures)Sys.GetInstance(typeof(frmSetupSignatures));
			}
		}

		protected frmSetupSignatures _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 4/11/2006
		// ********************************************************
		const int CNSTGRIDUSERCOLAUTOID = 0;
		const int CNSTGRIDUSERCOLNAME = 1;
		const int CNSTGRIDUSERCOLPASSWORD = 2;
		const int CNSTGRIDUSERCOLTYPE = 3;
		const int CNSTGRIDUSERCOLMODULE = 4;
		const int CNSTGRIDUSERCOLMODSPECIFIC = 5;
		const int CNSTGRIDUSERCOLMODSPECIFICID = 6;
		const int CNSTGRIDUSERCOLPICTURE = 7;
		private bool boolFullPrivileges;
		private string strNewPicture = string.Empty;

		public void Init(bool boolFullPermissions = false)
		{
			boolFullPrivileges = boolFullPermissions;
			this.Show(FormShowEnum.Modal);
		}

		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			ReShowUsers();
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strFilename = "";
				MDIParent.InstancePtr.CommonDialog1.Filter = "Image Files|*.bmp;*.gif;*.jpg;*.sig";
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = VBtoConverter.cdlOFNFileMustExist+VBtoConverter.cdlOFNExplorer+VBtoConverter.cdlOFNLongNames+VBtoConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//FC:FINAL:MSH - issue #1248: restore 'CancelError' property for correct work if 'Cancel' clicked
				MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFilename = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (Strings.Trim(strFilename) != string.Empty)
				{
					if (LoadSigPicture(strFilename))
					{
						strNewPicture = strFilename;
					}
				}
				return;
			}
			catch
			{
				// ErrorHandler:
				if (Information.Err().Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In Browse_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			DisableSigFrame(true);
			txtPassword.Text = "";
			txtConfirmPassword.Text = "";
		}

		private void DisableSigFrame(bool boolDisable)
		{
			if (boolDisable)
			{
				framMain.Enabled = true;
				framSignature.Enabled = false;
				cmdOk.Enabled = false;
				cmdCancel.Enabled = false;
				cmdBrowse.Enabled = false;
				cmdClear.Enabled = false;
				btnSave.Enabled = true;
				btnAddUser.Enabled = true;
				btnDelete.Enabled = true;
			}
			else
			{
				framMain.Enabled = false;
				framSignature.Enabled = true;
				cmdOk.Enabled = true;
				cmdCancel.Enabled = true;
				cmdBrowse.Enabled = true;
				cmdClear.Enabled = true;
				btnSave.Enabled = false;
				btnAddUser.Enabled = false;
				btnDelete.Enabled = false;
			}
            //FC:FINAL:SBE - #1250 - force activate of the form, to enable button shortcut
            this.Activate();
		}

		private void FillSigFrame(int lngRow)
		{
			//FileSystemObject fso = new FileSystemObject(); 
			string strTemp;
			// VBto upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
			int lngID = 0;
			// VBto upgrade warning: lngType As int	OnWriteFCConvert.ToDouble(
			int lngType = 0;
			string strMod = "";
			strNewPicture = "";
			txtPassword.Text = "";
			txtConfirmPassword.Text = "";
			strTemp = Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE));
			if (strTemp == string.Empty)
			{
				lngID = FCConvert.ToInt32(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID)));
				if (lngID > 0)
				{
					strMod = gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE);
					lngType = FCConvert.ToInt32(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)));
					strTemp = modSignatureFile.GetSigFileName(ref lngID, ref strMod, ref lngType);
					if (strTemp != string.Empty)
					{
						strTemp = Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, strTemp);
						if (File.Exists(strTemp))
						{
							if (!LoadSigPicture(strTemp))
							{
								File.Delete(strTemp);
							}
						}
						else
						{
							imgSignature.Image = null;
						}
					}
					else
					{
						imgSignature.Image = null;
					}
				}
				else
				{
					imgSignature.Image = null;
				}
			}
			else
			{
				if (File.Exists(strTemp))
				{
					if (!LoadSigPicture(strTemp))
					{
						File.Delete(strTemp);
					}
				}
				else
				{
					imgSignature.Image = null;
				}
			}
		}

		private bool LoadSigPicture(string strFilename)
		{
			bool LoadSigPicture = false;
			try
			{
				// On Error GoTo ErrorHandler
				double dblRatio;
				int lngWidth;
				int lngHeight;
				int lngMaxWidth;
				int lngMaxHeight;
				// VBto upgrade warning: lngNewHeight As int	OnWriteFCConvert.ToDouble(
				int lngNewHeight;
				// VBto upgrade warning: lngNewWidth As int	OnWriteFCConvert.ToDouble(
				int lngNewWidth = 0;
				LoadSigPicture = false;
				imgSignature.Image = FCUtils.LoadPicture(strFilename);
				lngMaxHeight = Shape1.Height;
				lngMaxWidth = Shape1.Width;
				dblRatio = FCConvert.ToDouble(imgSignature.Image.Height) / FCConvert.ToDouble(imgSignature.Image.Width);
				lngNewHeight = FCConvert.ToInt32(dblRatio * lngMaxWidth);
				if (lngNewHeight > lngMaxHeight)
				{
					imgSignature.Height = lngMaxHeight;
					lngNewWidth = FCConvert.ToInt32(lngMaxHeight / dblRatio);
					imgSignature.Width = lngNewWidth;
				}
				else
				{
					imgSignature.Width = lngMaxWidth;
					imgSignature.Height = lngNewHeight;
				}
				//FC:FINAL:RPU:#1248 - move the buttons to avoid overlapping if the image is too large
				if (imgSignature.Location.X + imgSignature.Width + 20 > fcPanel1.Location.X)
				{
					fcPanel1.Location = new System.Drawing.Point(imgSignature.Location.X + imgSignature.Width + 20, fcPanel1.Location.Y);
				}
				//FC:FINAL:MSH - issue #1248: recalculate location of the PictureBox for correct alignment
				imgSignature.Top += (fcPanel1.Top + fcPanel1.Height / 2) - (imgSignature.Top + imgSignature.Height / 2);
				LoadSigPicture = true;
				return LoadSigPicture;
			}
			catch
			{
				// ErrorHandler:
				if (Information.Err().Number == 481)
				{
					MessageBox.Show("You have selected an invalid image file for your signature.  Please try again.", "Invalid Signature File", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In LoadSigPicture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return LoadSigPicture;
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = gridUser.Row;
			if (Strings.Trim(txtPassword.Text) != string.Empty)
			{
				if (Strings.Trim(txtConfirmPassword.Text) != string.Empty)
				{
					if (txtPassword.Text != txtConfirmPassword.Text)
					{
						MessageBox.Show("The new password and the confirmation do not match", "Bad Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else
					{
						gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, Strings.Trim(txtPassword.Text));
					}
				}
				else
				{
					MessageBox.Show("You must confirm the password to change it", "No Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (strNewPicture != string.Empty)
			{
				gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, strNewPicture);
			}
			txtPassword.Text = "";
			txtConfirmPassword.Text = "";
			gridUser.RowData(lngRow, true);
			DisableSigFrame(true);
		}

		private void frmSetupSignatures_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = 0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSetupSignatures_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupSignatures.FillStyle	= 0;
			//frmSetupSignatures.ScaleWidth	= 5880;
			//frmSetupSignatures.ScaleHeight	= 3810;
			//frmSetupSignatures.KeyPreview	= -1  'True;
			//frmSetupSignatures.LinkTopic	= "Form2";
			//frmSetupSignatures.LockControls	= -1  'True;
			//frmSetupSignatures.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtConfirmPassword.IMEMode	= 3  'DISABLE;
			//txtConfirmPassword.PasswordChar	= "*";
			//txtPassword.IMEMode	= 3  'DISABLE;
			//txtPassword.PasswordChar	= "*";
			//lblConfirm.UseMnemonic	= 0   'False;
			//lblPassword.UseMnemonic	= 0   'False;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridUser();
			LoadGridUser();
			SetupcmbType();
			if (boolFullPrivileges)
			{
				gridUser.Editable = FCGrid.EditableSettings.flexEDKbd;
			}
		}

		private void ReShowUsers()
		{
			int lngRow;
			int lngType;
			int lngFirstVisible;
			lngType = cmbType.ItemData(cmbType.SelectedIndex);
			lngFirstVisible = 0;
			if (boolFullPrivileges)
			{
				switch (lngType)
				{
					case modSignatureFile.BUDGETARYSIG:
					case modSignatureFile.COLLECTORSIG:
					case modSignatureFile.TREASURERSIG:
					case modSignatureFile.UTILITYSIG:
						{
							btnAddUser.Visible = false;
							btnDelete.Visible = false;
							break;
						}
					default:
						{
							btnAddUser.Visible = true;
							btnDelete.Visible = true;
							break;
						}
				}
				//end switch
			}
			else
			{
				btnAddUser.Visible = false;
				btnDelete.Visible = false;
			}
			for (lngRow = 1; lngRow <= gridUser.Rows - 1; lngRow++)
			{
				if (Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)) == lngType)
				{
					gridUser.RowHidden(lngRow, false);
					if (lngFirstVisible < 1)
						lngFirstVisible = lngRow;
				}
				else
				{
					gridUser.RowHidden(lngRow, true);
				}
			}
			// lngRow
			if (lngFirstVisible > 0)
			{
				gridUser.Row = lngFirstVisible;
				FillSigFrame(lngFirstVisible);
			}
			else
			{
				gridUser.Row = 0;
				FillSigFrame(0);
			}
		}

		private void gridType_ComboCloseUp(int Row, int Col, bool FinishEdit)
		{
			//Application.DoEvents();
			ReShowUsers();
		}

		private void gridUser_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			gridUser.RowData(gridUser.Row, true);
		}

		private void gridUser_DblClick(object sender, System.EventArgs e)
		{
			// VBto upgrade warning: lngAutoID As int	OnWriteFCConvert.ToDouble(
			int lngAutoID;
			int lngReturn = 0;
			if (gridUser.Row < 1)
				return;
			lngAutoID = FCConvert.ToInt32(Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLAUTOID)));
			if (lngAutoID != 0)
			{
				// get password
				lngReturn = frmSignaturePassword.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLTYPE))), 0, false, lngAutoID);
				if (lngReturn != lngAutoID)
				{
					return;
				}
			}
			DisableSigFrame(false);
		}
		// Private Sub gridUser_EnterCell()
		// Currently this screws up the double click event because that wont fire when in edit mode
		// If gridUser.Row > 0 Then
		// Select Case Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLTYPE))
		// Case CLERKSIG, PAYROLLSIG
		// If boolFullPrivileges Then
		// gridUser.EditCell
		// End If
		// Case Else
		// End Select
		// End If
		// End Sub
		private void gridUser_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int lngRow;
			lngRow = gridUser.Row;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						DeleteUser();
						break;
					}
				case Keys.Insert:
					{
						break;
					}
				case Keys.F9:
					{
						if (e.Shift)
						{
							ResetPassword(lngRow);
						}
						break;
					}
			}
			//end switch
		}

		private void ResetPassword(int lngRow)
		{
			object lngResponse;
			clsDRWrapper rsSave = new clsDRWrapper();
			if (boolFullPrivileges && Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID)) == 0)
			{
				// if it hasn't even been saved yet, don't ask for the one day code
				gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, "3333");
				gridUser.RowData(lngRow, true);
				MessageBox.Show("Password Cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			lngResponse = 0;
			if (frmInput.InstancePtr.Init(ref lngResponse, "One Day Code", "Contact TRIO for the GN one day code to use this option", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
			{
				if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngResponse), "GN"))
				{
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, "3333");
					gridUser.RowData(lngRow, true);
					rsSave.Execute("update signatures set password = '����' where id = " + FCConvert.ToString(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID))), "SystemSettings");
				}
				else
				{
					MessageBox.Show("Code is not correct", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
		}

		private void gridUser_KeyDownEdit(object sender, Wisej.Web.KeyEventArgs e)
		{
			int lngRow;
			lngRow = gridUser.Row;
			switch (e.KeyCode)
			{
				case Keys.F9:
					{
						if (e.Shift)
						{
							ResetPassword(lngRow);
						}
						break;
					}
			}
			//end switch
		}

		private void gridUser_RowColChange(object sender, System.EventArgs e)
		{
			if (gridUser.Row > 0)
			{
				FillSigFrame(gridUser.Row);
			}
		}

		private void mnuAddUser_Click(object sender, System.EventArgs e)
		{
			AddUser();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			DeleteUser();
		}

		private void AddUser()
		{
			int lngRow = 0;
			int lngType = 0;
			string strMod = "";
			if (boolFullPrivileges)
			{
				if (btnAddUser.Visible)
				{
					// lngType = Val(gridType.TextMatrix(0, 0))
					lngType = cmbType.ItemData(cmbType.SelectedIndex);
					switch (lngType)
					{
						case modSignatureFile.BUDGETARYSIG:
							{
								strMod = "BD";
								break;
							}
						case modSignatureFile.COLLECTORSIG:
							{
								strMod = "CL";
								break;
							}
						case modSignatureFile.TREASURERSIG:
							{
								strMod = "CL";
								break;
							}
						case modSignatureFile.CLERKSIG:
							{
								strMod = "CK";
								break;
							}
						case modSignatureFile.PAYROLLSIG:
							{
								strMod = "PY";
								break;
							}
						case modSignatureFile.UTILITYSIG:
							{
								strMod = "UT";
								break;
							}
					}
					//end switch
					gridUser.Rows += 1;
					lngRow = gridUser.Rows - 1;
					gridUser.TopRow = lngRow;
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID, FCConvert.ToString(0));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFIC, "");
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFICID, FCConvert.ToString(0));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE, strMod);
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLNAME, "New Signature");
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, "");
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE, FCConvert.ToString(lngType));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
					gridUser.RowData(lngRow, true);
				}
			}
		}

		private void DeleteUser()
		{
			if (boolFullPrivileges)
			{
				if (gridUser.Row < 1)
					return;
				if (btnDelete.Visible)
				{
					if (Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLAUTOID)) > 0)
					{
						GridDelete.Rows += 1;
						GridDelete.TextMatrix(GridDelete.Rows - 1, 0, FCConvert.ToString(Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLAUTOID))));
					}
					gridUser.RemoveItem(gridUser.Row);
				}
			}
		}

		private void mnuExit_Click()
		{
			Close();
		}

		private void SetupcmbType()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// Dim strTemp As String
				// With gridType
				// .Rows = 1
				// strTemp = "#" & BUDGETARYSIG & ";Budgetary Checks|#" & CLERKSIG & ";Clerk Vitals|#" & PAYROLLSIG & ";Payroll Checks|#" & COLLECTORSIG & ";Tax Collector|"
				// strTemp = strTemp & "#" & TREASURERSIG & ";Tax Treasurer|#" & UTILITYSIG & ";Utility Lien Process"
				// .ColComboList(0) = strTemp
				// .TextMatrix(0, 0) = BUDGETARYSIG
				// ReShowUsers
				// End With
				cmbType.Clear();
				cmbType.AddItem("Budgetary Checks");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.BUDGETARYSIG);
				cmbType.AddItem("Clerk Vitals");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.CLERKSIG);
				cmbType.AddItem("Payroll Checks");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.PAYROLLSIG);
				cmbType.AddItem("Tax Collector");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.COLLECTORSIG);
				cmbType.AddItem("Tax Treasurer");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.TREASURERSIG);
				cmbType.AddItem("Utility Lien Process");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.UTILITYSIG);
				cmbType.SelectedIndex = 0;
				return;
			}
			catch
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In SetupcmbType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridUser()
		{
			gridUser.Cols = 8;
			gridUser.ColHidden(CNSTGRIDUSERCOLAUTOID, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLPASSWORD, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLTYPE, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLMODULE, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLMODSPECIFIC, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLMODSPECIFICID, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLPICTURE, true);
			gridUser.TextMatrix(0, CNSTGRIDUSERCOLNAME, "Name");
			gridUser.ColWidth(CNSTGRIDUSERCOLNAME, FCConvert.ToInt32(gridUser.WidthOriginal * 0.95));
		}
		// Private Sub ResizeGridType()
		// Dim GridHeight As Long
		// With gridType
		// GridHeight = .RowHeight(0) + 50
		// .Height = GridHeight
		// End With
		// End Sub
		private void LoadGridUser()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				gridUser.Rows = 1;
				rsLoad.OpenRecordset("Select * from signatures order by type", "SystemSettings");
				while (!rsLoad.EndOfFile())
				{
					gridUser.Rows += 1;
					lngRow = gridUser.Rows - 1;
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFIC, FCConvert.ToString(rsLoad.Get_Fields_String("modspecific")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFICID, FCConvert.ToString(rsLoad.Get_Fields_Int32("modspecificid")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE, FCConvert.ToString(rsLoad.Get_Fields_String("module")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_String("name")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsLoad.Get_Fields_String("password"))));
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE, FCConvert.ToString(rsLoad.Get_Fields("type")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
					gridUser.RowData(lngRow, false);
					gridUser.RowHidden(lngRow, true);
					rsLoad.MoveNext();
				}
				return;
			}
			catch
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In LoadGridUser", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				// VBto upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
				int lngID = 0;
				// VBto upgrade warning: lngType As int	OnWriteFCConvert.ToDouble(
				int lngType = 0;
				string strMod = "";
				string strFilename = "";
				string strSrc = "";
				//FileSystemObject fso = new FileSystemObject(); 
				SaveInfo = false;
				// handle deletions first
				for (lngRow = 0; lngRow <= GridDelete.Rows - 1; lngRow++)
				{
					modGlobalFunctions.AddCYAEntry_8("GN", "Deleted signature entry " + GridDelete.TextMatrix(lngRow, 0));
					rsSave.Execute("delete from signatures where id = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(lngRow, 0))), "SystemSettings");
				}
				// lngRow
				GridDelete.Rows = 0;
				rsSave.OpenRecordset("select * from signatures", "SystemSettings");
				for (lngRow = 1; lngRow <= gridUser.Rows - 1; lngRow++)
				{
					lngID = FCConvert.ToInt32(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID)));
					if (lngID == 0 || FCConvert.ToBoolean(gridUser.RowData(lngRow)))
					{
						if (lngID == 0)
						{
							rsSave.AddNew();
							// lngID = rsSave.Fields("id")
							gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID, FCConvert.ToString(lngID));
							rsSave.Set_Fields("Type", Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)));
							rsSave.Set_Fields("Module", gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE));
						}
						else
						{
							if (rsSave.FindFirstRecord("id", lngID))
							{
								rsSave.Edit();
							}
						}
						if (Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD)) != string.Empty)
						{
							rsSave.Set_Fields("password", modEncrypt.ToggleEncryptCode(Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD))));
						}
						rsSave.Set_Fields("name", gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLNAME));
						rsSave.Set_Fields("modspecific", gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFIC));
						rsSave.Set_Fields("modspecificid", Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFICID)));
						rsSave.Update();
						lngID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
						if (Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE)) != string.Empty)
						{
							strSrc = Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE));
							strMod = gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE);
							lngType = FCConvert.ToInt32(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)));
							strFilename = modSignatureFile.GetSigFileName(ref lngID, ref strMod, ref lngType);
							//FC:FINAL:MSH - i.issue #1588: wrong file path
							if (File.Exists(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFilename)))
							{
								File.Delete(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFilename));
							}
							//FC:FINAL:MSH - i.issue #1588: if file doesn't exist will be threw an exception
							//File.Copy(strSrc, Path.Combine(Application.MapPath("\\"), "UserData", strFilename), true);
							if (File.Exists(strSrc))
							{
								File.Copy(strSrc, Path.Combine(FCFileSystem.Statics.UserDataFolder, strFilename), true);
							}
							gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
						}
						gridUser.RowData(lngRow, false);
					}
				}
				// lngRow
				SaveInfo = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private void cmdClear_Click(object sender, EventArgs e)
		{
			imgSignature.Picture = null;
			strNewPicture = "Clear";
		}
	}
}
