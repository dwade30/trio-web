﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmInternalAudit.
	/// </summary>
	partial class frmInternalAudit : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkModule;
		public fecherFoundation.FCCheckBox chkModule_9;
		public fecherFoundation.FCCheckBox chkModule_0;
		public fecherFoundation.FCCheckBox chkModule_2;
		public fecherFoundation.FCCheckBox chkModule_7;
		public fecherFoundation.FCCheckBox chkModule_6;
		public fecherFoundation.FCCheckBox chkModule_8;
		public fecherFoundation.FCCheckBox chkModule_1;
		public fecherFoundation.FCCheckBox chkModule_5;
		public fecherFoundation.FCCheckBox chkModule_3;
		public fecherFoundation.FCCheckBox chkModule_4;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInternalAudit));
			this.chkModule_9 = new fecherFoundation.FCCheckBox();
			this.chkModule_0 = new fecherFoundation.FCCheckBox();
			this.chkModule_2 = new fecherFoundation.FCCheckBox();
			this.chkModule_7 = new fecherFoundation.FCCheckBox();
			this.chkModule_6 = new fecherFoundation.FCCheckBox();
			this.chkModule_8 = new fecherFoundation.FCCheckBox();
			this.chkModule_1 = new fecherFoundation.FCCheckBox();
			this.chkModule_5 = new fecherFoundation.FCCheckBox();
			this.chkModule_3 = new fecherFoundation.FCCheckBox();
			this.chkModule_4 = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 514);
			this.BottomPanel.Size = new System.Drawing.Size(301, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkModule_9);
			this.ClientArea.Controls.Add(this.chkModule_0);
			this.ClientArea.Controls.Add(this.chkModule_2);
			this.ClientArea.Controls.Add(this.chkModule_7);
			this.ClientArea.Controls.Add(this.chkModule_6);
			this.ClientArea.Controls.Add(this.chkModule_8);
			this.ClientArea.Controls.Add(this.chkModule_1);
			this.ClientArea.Controls.Add(this.chkModule_5);
			this.ClientArea.Controls.Add(this.chkModule_3);
			this.ClientArea.Controls.Add(this.chkModule_4);
			this.ClientArea.Size = new System.Drawing.Size(301, 454);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(301, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(157, 30);
			this.HeaderText.Text = "Internal Audit";
			// 
			// chkModule_9
			// 
			this.chkModule_9.Location = new System.Drawing.Point(30, 390);
			this.chkModule_9.Name = "chkModule_9";
			this.chkModule_9.Size = new System.Drawing.Size(181, 27);
			this.chkModule_9.TabIndex = 9;
			this.chkModule_9.Text = "Accounts Receivable";
			// 
			// chkModule_0
			// 
			this.chkModule_0.Location = new System.Drawing.Point(30, 30);
			this.chkModule_0.Name = "chkModule_0";
			this.chkModule_0.Size = new System.Drawing.Size(102, 27);
			this.chkModule_0.TabIndex = 0;
			this.chkModule_0.Text = "Budgetary";
			// 
			// chkModule_2
			// 
			this.chkModule_2.Location = new System.Drawing.Point(30, 110);
			this.chkModule_2.Name = "chkModule_2";
			this.chkModule_2.Size = new System.Drawing.Size(108, 27);
			this.chkModule_2.TabIndex = 2;
			this.chkModule_2.Text = "Collections";
			// 
			// chkModule_7
			// 
			this.chkModule_7.Location = new System.Drawing.Point(30, 310);
			this.chkModule_7.Name = "chkModule_7";
			this.chkModule_7.Size = new System.Drawing.Size(112, 27);
			this.chkModule_7.TabIndex = 7;
			this.chkModule_7.Text = "Real Estate";
			// 
			// chkModule_6
			// 
			this.chkModule_6.Location = new System.Drawing.Point(30, 270);
			this.chkModule_6.Name = "chkModule_6";
			this.chkModule_6.Size = new System.Drawing.Size(159, 27);
			this.chkModule_6.TabIndex = 6;
			this.chkModule_6.Text = "Personal Property";
			// 
			// chkModule_8
			// 
			this.chkModule_8.Location = new System.Drawing.Point(30, 350);
			this.chkModule_8.Name = "chkModule_8";
			this.chkModule_8.Size = new System.Drawing.Size(116, 27);
			this.chkModule_8.TabIndex = 8;
			this.chkModule_8.Text = "Utility Billing";
			// 
			// chkModule_1
			// 
			this.chkModule_1.Location = new System.Drawing.Point(30, 70);
			this.chkModule_1.Name = "chkModule_1";
			this.chkModule_1.Size = new System.Drawing.Size(134, 27);
			this.chkModule_1.TabIndex = 1;
			this.chkModule_1.Text = "Cash Receipts";
			// 
			// chkModule_5
			// 
			this.chkModule_5.Location = new System.Drawing.Point(30, 230);
			this.chkModule_5.Name = "chkModule_5";
			this.chkModule_5.Size = new System.Drawing.Size(128, 27);
			this.chkModule_5.TabIndex = 5;
			this.chkModule_5.Text = "Motor Vehicle";
			// 
			// chkModule_3
			// 
			this.chkModule_3.Location = new System.Drawing.Point(30, 150);
			this.chkModule_3.Name = "chkModule_3";
			this.chkModule_3.Size = new System.Drawing.Size(65, 27);
			this.chkModule_3.TabIndex = 3;
			this.chkModule_3.Text = "E911";
			// 
			// chkModule_4
			// 
			this.chkModule_4.Location = new System.Drawing.Point(30, 190);
			this.chkModule_4.Name = "chkModule_4";
			this.chkModule_4.Size = new System.Drawing.Size(121, 27);
			this.chkModule_4.TabIndex = 4;
			this.chkModule_4.Text = "Fixed Assets";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(92, 27);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(119, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmInternalAudit
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(301, 622);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmInternalAudit";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Internal Audit";
			this.Load += new System.EventHandler(this.frmInternalAudit_Load);
			this.Activated += new System.EventHandler(this.frmInternalAudit_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmInternalAudit_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkModule_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
