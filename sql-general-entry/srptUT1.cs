﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptUT1.
	/// </summary>
	public partial class srptUT1 : FCSectionReport
	{
		public srptUT1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptUT1";
		}

		public static srptUT1 InstancePtr
		{
			get
			{
				return (srptUT1)Sys.GetInstance(typeof(srptUT1));
			}
		}

		protected srptUT1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptUT1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curCLTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCLTotal;
		// vbPorter upgrade warning: curGLTotal As Decimal	OnWrite(short, double)
		Decimal curGLTotal;
		string strType = "";
		int lngNumberOfBills;
		string strOldType = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					CheckType2:
					;
					IncrementType();
					if (strType != "")
					{
						SetRecordSet();
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							eArgs.EOF = false;
						}
						else
						{
							goto CheckType2;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = strType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			strType = "WR";
			SetRecordSet();
			CheckType:
			;
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				IncrementType();
				if (strType != "")
				{
					SetRecordSet();
					goto CheckType;
				}
				else
				{
					this.Cancel();
					return;
				}
			}
			blnFirstRecord = true;
			curCLTotal = 0;
			curGLTotal = 0;
			lngNumberOfBills = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsInfo.EndOfFile())
			{
                using (clsDRWrapper rsRateInfo = new clsDRWrapper())
                {
                    rsRateInfo.OpenRecordset(
                        "SELECT * FROM RateKeys WHERE ID = " + rsInfo.Get_Fields_Int32("BillingRateKey"),
                        "TWUT0000.vb1");
                    if (rsRateInfo.EndOfFile() != true && rsRateInfo.BeginningOfFile() != true)
                    {
                        fldBillDate.Text = Strings.Format(rsRateInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
                    }
                    else
                    {
                        if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("BillingRateKey")) == 0)
                        {
                            fldBillDate.Text = "PRE PAY";
                        }
                        else
                        {
                            fldBillDate.Text = "UNKNOWN";
                        }
                    }

                    // TODO Get_Fields: Field [PrinOwedTotal] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [PrinPaidTotal] not found!! (maybe it is an alias?)
                    fldCL.Text =
                        Strings.Format((rsInfo.Get_Fields("PrinOwedTotal") - rsInfo.Get_Fields("PrinPaidTotal")),
                            "#,##0.00");
                    // TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
                    fldNumberOfBills.Text = Strings.Format(rsInfo.Get_Fields("NumberOfBills"), "#,##0");
                    // TODO Get_Fields: Field [PrinOwedTotal] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [PrinPaidTotal] not found!! (maybe it is an alias?)
                    curCLTotal +=
                        FCConvert.ToDecimal(rsInfo.Get_Fields("PrinOwedTotal") - rsInfo.Get_Fields("PrinPaidTotal"));
                    strOldType = strType;
                    // TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
                    lngNumberOfBills += rsInfo.Get_Fields_Int32("NumberOfBills");
                }
            }
		}

		private void SetRecordSet()
		{
			if (strType == "WR")
			{
				rsInfo.OpenRecordset("SELECT BillingRateKey, COUNT(BillingRateKey) as NumberOfBills, SUM(WPrinOwed) as PrinOwedTotal, SUM(WPrinPaid) as PrinPaidTotal, SUM(WTaxOwed) as TaxOwedTotal, SUM(WTaxPaid) as TaxPaidTotal, 0 as CostOwedTotal, 0 as CostPaidTotal, 0 as InterestOwedTotal, 0 as InterestPaidTotal FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE (Service = 'W' or Service = 'B') AND (BillStatus = 'B' OR BillingRateKey = 0) AND WLienRecordNumber = 0 AND TaxAcquired <> 1  GROUP BY BillingRateKey HAVING (SUM(WPrinOwed) - SUM(WPrinPaid)) + (SUM(WTaxOwed) - SUM(WTaxPaid)) <> 0 ORDER BY BillingRateKey", "TWUT0000.vb1");
			}
			else if (strType == "SR")
			{
				rsInfo.OpenRecordset("SELECT BillingRateKey, COUNT(BillingRateKey) as NumberOfBills, SUM(SPrinOwed) as PrinOwedTotal, SUM(SPrinPaid) as PrinPaidTotal, SUM(STaxOwed) as TaxOwedTotal, SUM(STaxPaid) as TaxPaidTotal, 0 as CostOwedTotal, 0 as CostPaidTotal, 0 as InterestOwedTotal, 0 as InterestPaidTotal FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE (Service = 'S' or Service = 'B') AND (BillStatus = 'B' OR BillingRateKey = 0) AND SLienRecordNumber = 0 AND TaxAcquired <> 1 GROUP BY BillingRateKey HAVING (SUM(SPrinOwed) - SUM(SPrinPaid)) + (SUM(STaxOwed) - SUM(STaxPaid)) <> 0 ORDER BY BillingRateKey", "TWUT0000.vb1");
			}
			else if (strType == "WL")
			{
				rsInfo.OpenRecordset("SELECT RateKey as BillingRateKey, COUNT(RateKey) as NumberOfBills, SUM(Principal) as PrinOwedTotal, SUM(PrinPaid) as PrinPaidTotal, SUM(Tax) as TaxOwedTotal, SUM(TaxPaid) as TaxPaidTotal, SUM(Costs) as CostOwedTotal, SUM(CostPaid) as CostPaidTotal, SUM(Interest) as InterestOwedTotal, SUM(PLIPaid) as InterestPaidTotal FROM ((Lien INNER JOIN Bill ON Lien.ID = Bill.WlienRecordNumber) INNER JOIN Master ON Bill.AccountKey = Master.ID) WHERE WCombinationLienKey = Bill.ID AND Water = 1 AND TaxAcquired <> 1  GROUP BY RateKey HAVING (SUM(Principal) - SUM(PrinPaid)) + (SUM(Tax)- SUM(TaxPaid)) + (SUM(Costs) - SUM(CostPaid)) + (SUM(Interest) - SUM(PLIPaid)) <> 0 ORDER BY RateKey", "TWUT0000.vb1");
			}
			else if (strType == "SL")
			{
				rsInfo.OpenRecordset("SELECT RateKey as BillingRateKey, COUNT(RateKey) as NumberOfBills, SUM(Principal) as PrinOwedTotal, SUM(PrinPaid) as PrinPaidTotal, SUM(Tax) as TaxOwedTotal, SUM(TaxPaid) as TaxPaidTotal, SUM(Costs) as CostOwedTotal, SUM(CostPaid) as CostPaidTotal, SUM(Interest) as InterestOwedTotal, SUM(PLIPaid) as InterestPaidTotal FROM ((Lien INNER JOIN Bill ON Lien.ID = Bill.SlienRecordNumber) INNER JOIN Master ON Bill.AccountKey = Master.ID) WHERE SCombinationLienKey = Bill.ID AND Water <> 1 AND TaxAcquired <> 1 GROUP BY RateKey HAVING (SUM(Principal) - SUM(PrinPaid)) + (SUM(Tax)- SUM(TaxPaid)) + (SUM(Costs) - SUM(CostPaid)) + (SUM(Interest) - SUM(PLIPaid)) <> 0 ORDER BY RateKey", "TWUT0000.vb1");
			}
		}

		private void IncrementType()
		{
			if (strType == "WR")
			{
				strType = "SR";
			}
			else if (strType == "SR")
			{
				strType = "WL";
			}
			else if (strType == "WL")
			{
				strType = "SL";
			}
			else
			{
				strType = "";
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsBudInfo = new clsDRWrapper();
			clsDRWrapper rsAcctInfo = new clsDRWrapper();
			int lngFund = 0;
			fldCLTotal.Text = Strings.Format(curCLTotal, "#,##0.00");
			fldNumberOfBillsTotal.Text = Strings.Format(lngNumberOfBills, "#,##0");
			rsAcctInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = '" + strOldType + "'", "TWBD0000.vb1");
			if (rsAcctInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Conversion.Val(rsAcctInfo.Get_Fields("Account")) != 0)
				{
					rsBudInfo.OpenRecordset("SELECT * FROM UtilityBilling", "TWUT0000.vb1");
					if (rsBudInfo.EndOfFile() != true && rsBudInfo.BeginningOfFile() != true)
					{
						if (Strings.Left(strOldType, 1) == "S")
						{
							lngFund = modBudgetaryAccounting.GetFundFromAccount_2(rsBudInfo.Get_Fields_String("SewerPrincipalAccount"));
						}
						else
						{
							lngFund = modBudgetaryAccounting.GetFundFromAccount_2(rsBudInfo.Get_Fields_String("WaterPrincipalAccount"));
						}
						if (lngFund == 0)
						{
							curGLTotal = 0;
							goto FillInRest;
						}
					}
					else
					{
						curGLTotal = 0;
						goto FillInRest;
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBudInfo.OpenRecordset("SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + Strings.Format(lngFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "' AND LedgerAccount = '" + rsAcctInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					if (rsBudInfo.EndOfFile() != true && rsBudInfo.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curGLTotal = FCConvert.ToDecimal(Conversion.Val(rsBudInfo.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsBudInfo.Get_Fields("BudgetAdjustmentsTotal")) + Conversion.Val(rsBudInfo.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsBudInfo.Get_Fields("EncumbActivityTotal")) + Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")));
					}
					else
					{
						curGLTotal = 0;
					}
				}
				else
				{
					curGLTotal = 0;
				}
			}
			else
			{
				curGLTotal = 0;
			}
			FillInRest:
			;
			fldGLTotal.Text = Strings.Format(curGLTotal, "#,##0.00");
			fldDiffTotal.Text = Strings.Format(curCLTotal - curGLTotal, "#,##0.00");
			if (curCLTotal != curGLTotal)
			{
				chkTotalFlag.Checked = true;
			}
			else
			{
				chkTotalFlag.Checked = false;
			}
			curCLTotal = 0;
			curGLTotal = 0;
			lngNumberOfBills = 0;
			rsBudInfo.Dispose();
			rsAcctInfo.Dispose();
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			if (strType == "WR")
			{
				fldBillType.Text = "Water Bills";
			}
			else if (strType == "SR")
			{
				fldBillType.Text = "Sewer Bills";
			}
			else if (strType == "WL")
			{
				fldBillType.Text = "Water Liens";
			}
			else if (strType == "SL")
			{
				fldBillType.Text = "Sewer Liens";
			}
		}

		

		private void srptUT1_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
