﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for rptVerticalAlignment.
	/// </summary>
	public partial class rptVerticalAlignment : BaseSectionReport
	{
		public rptVerticalAlignment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "rptVerticalTestAlignment";
		}

		public static rptVerticalAlignment InstancePtr
		{
			get
			{
				return (rptVerticalAlignment)Sys.GetInstance(typeof(rptVerticalAlignment));
			}
		}

		protected rptVerticalAlignment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVerticalAlignment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intNumber;
		bool blnSecondPage;
		public string strPrinterName = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnSecondPage)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			blnSecondPage = true;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (this.Document.Pages.Count > 1)
			{
				this.Document.Pages.RemoveAt(1);
			}
			//this.Document.Pages.Commit();
			//this.Refresh();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intNumber = 0;
			blnSecondPage = false;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			foreach (FCPrinter tempPrinter in FCGlobal.Printers)
			{
				// If Windows2000 Then
				// If tempFCGlobal.Printer.DeviceName = Mvr3Printer Then
				// rptMVPrintTest.FCGlobal.Printer.DeviceName = tempFCGlobal.Printer.DeviceName
				// Exit For
				// End If
				// Else
				if (tempPrinter.DeviceName == strPrinterName)
				{
					this.Document.Printer.PrinterName = tempPrinter.DeviceName;
					break;
				}
				// End If
			}
			//this.Printer.RenderMode = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			intNumber += 1;
			if (intNumber > 9)
			{
				intNumber = 0;
			}
			lblNumber.Text = Strings.StrDup(80, Strings.Trim(intNumber.ToString()));
		}

		public void SetPrinter(string strPrinter)
		{
			strPrinterName = strPrinter;
		}

		
	}
}
