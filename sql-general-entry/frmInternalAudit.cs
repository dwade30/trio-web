﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmInternalAudit.
	/// </summary>
	public partial class frmInternalAudit : BaseForm
	{
		public frmInternalAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkModule = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkModule.AddControlArrayElement(chkModule_9, 9);
			this.chkModule.AddControlArrayElement(chkModule_0, 0);
			this.chkModule.AddControlArrayElement(chkModule_2, 2);
			this.chkModule.AddControlArrayElement(chkModule_7, 7);
			this.chkModule.AddControlArrayElement(chkModule_6, 6);
			this.chkModule.AddControlArrayElement(chkModule_8, 8);
			this.chkModule.AddControlArrayElement(chkModule_1, 1);
			this.chkModule.AddControlArrayElement(chkModule_5, 5);
			this.chkModule.AddControlArrayElement(chkModule_3, 3);
			this.chkModule.AddControlArrayElement(chkModule_4, 4);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmInternalAudit InstancePtr
		{
			get
			{
				return (frmInternalAudit)Sys.GetInstance(typeof(frmInternalAudit));
			}
		}

		protected frmInternalAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void chkUT_Click()
		{
		}

		private void frmInternalAudit_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmInternalAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInternalAudit properties;
			//frmInternalAudit.FillStyle	= 0;
			//frmInternalAudit.ScaleWidth	= 5880;
			//frmInternalAudit.ScaleHeight	= 4140;
			//frmInternalAudit.LinkTopic	= "Form2";
			//frmInternalAudit.LockControls	= true;
			//frmInternalAudit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			chkModule[0].Text = "Accounts Receivable";
			chkModule[1].Text = "Budgetary";
			chkModule[2].Text = "Cash Receipts";
			chkModule[3].Text = "Collections";
			chkModule[4].Text = "E911";
			chkModule[5].Text = "Fixed Assets";
			chkModule[6].Text = "Motor Vehicle";
			chkModule[7].Text = "Personal Property";
			chkModule[8].Text = "Real Estate";
			chkModule[9].Text = "Utility Billing";
			if (modGlobalConstants.Statics.gboolAR)
			{
				chkModule[0].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[0].Visible = true;
			}
			else
			{
				chkModule[0].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[0].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				chkModule[1].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[1].Enabled = true;
			}
			else
			{
				chkModule[1].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[1].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolCR)
			{
				chkModule[2].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[2].Visible = true;
			}
			else
			{
				chkModule[2].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[2].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolCL)
			{
				chkModule[3].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[3].Visible = true;
			}
			else
			{
				chkModule[3].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[3].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolE9)
			{
				chkModule[4].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[4].Visible = true;
			}
			else
			{
				chkModule[4].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[4].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolFA)
			{
				chkModule[5].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[5].Visible = true;
			}
			else
			{
				chkModule[5].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[5].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolMV)
			{
				chkModule[6].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[6].Visible = true;
			}
			else
			{
				chkModule[6].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[6].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolPP)
			{
				chkModule[7].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[7].Visible = true;
			}
			else
			{
				chkModule[7].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[7].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolRE)
			{
				chkModule[8].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[8].Visible = true;
			}
			else
			{
				chkModule[8].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[8].Enabled = false;
			}
			if (modGlobalConstants.Statics.gboolUT)
			{
				chkModule[9].CheckState = Wisej.Web.CheckState.Checked;
				chkModule[9].Visible = true;
			}
			else
			{
				chkModule[9].CheckState = Wisej.Web.CheckState.Unchecked;
				chkModule[9].Enabled = false;
			}
		}

		private void frmInternalAudit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			bool blnSelection;
			int counter;
			blnSelection = false;
			for (counter = 0; counter <= 9; counter++)
			{
				if (chkModule[FCConvert.ToInt16(counter)].CheckState == Wisej.Web.CheckState.Checked)
				{
					blnSelection = true;
				}
			}
			if (blnSelection)
			{
				frmReportViewer.InstancePtr.Init(rptInternalAudit.InstancePtr, showModal: true);
				this.Hide();
			}
			else
			{
				MessageBox.Show("You must select at least one module before you may continue.", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}
	}
}
