﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmFixProcess.
	/// </summary>
	partial class frmFixProcess : BaseForm
	{
		public fecherFoundation.FCFileListBox File1;
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCDirListBox Dir1;
		public fecherFoundation.FCButton cmdRun;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCLabel lblInformation;
		public fecherFoundation.FCLabel lblScreenTitle;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFixProcess));
			this.File1 = new fecherFoundation.FCFileListBox();
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.Dir1 = new fecherFoundation.FCDirListBox();
			this.cmdRun = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.lblInformation = new fecherFoundation.FCLabel();
			this.lblScreenTitle = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRun)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 526);
			this.BottomPanel.Size = new System.Drawing.Size(522, 93);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.File1);
			this.ClientArea.Controls.Add(this.Drive1);
			this.ClientArea.Controls.Add(this.Dir1);
			this.ClientArea.Controls.Add(this.cmdRun);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.lblInformation);
			this.ClientArea.Controls.Add(this.lblScreenTitle);
			this.ClientArea.Size = new System.Drawing.Size(522, 466);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(522, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(140, 30);
			this.HeaderText.Text = "Fix Process";
			// 
			// File1
			// 
			this.File1.Location = new System.Drawing.Point(242, 130);
			this.File1.Name = "File1";
			this.File1.Size = new System.Drawing.Size(243, 275);
			this.File1.TabIndex = 5;
			this.File1.DoubleClick += new System.EventHandler(this.File1_DoubleClick);
			// 
			// Drive1
			// 
			this.Drive1.Location = new System.Drawing.Point(30, 130);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(178, 22);
			this.Drive1.TabIndex = 3;
			this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
			// 
			// Dir1
			// 
			this.Dir1.Location = new System.Drawing.Point(30, 170);
			this.Dir1.Name = "Dir1";
			this.Dir1.Size = new System.Drawing.Size(180, 233);
			this.Dir1.TabIndex = 4;
			this.Dir1.SelectedIndexChanged += new System.EventHandler(this.Dir1_SelectedIndexChanged);
			// 
			// cmdRun
			// 
			this.cmdRun.AppearanceKey = "actionButton";
			this.cmdRun.ForeColor = System.Drawing.Color.White;
			this.cmdRun.Location = new System.Drawing.Point(30, 420);
			this.cmdRun.Name = "cmdRun";
			this.cmdRun.Size = new System.Drawing.Size(88, 40);
			this.cmdRun.TabIndex = 6;
			this.cmdRun.Text = "Run";
			this.cmdRun.Click += new System.EventHandler(this.cmdRun_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(141, 420);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(108, 40);
			this.cmdCancel.TabIndex = 7;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 70);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(455, 46);
			this.lblInstructions.TabIndex = 2;
			this.lblInstructions.Text = "PLEASE SELECT THE PROGRAM YOU WISH TO RUN FROM THE LIST BELOW.  AFTER SELECTING T" + "HE FILE CLICK THE RUN BUTTON TO RUN THE PROGRAM";
			// 
			// lblInformation
			// 
			this.lblInformation.AutoSize = true;
			this.lblInformation.Location = new System.Drawing.Point(361, 30);
			this.lblInformation.Name = "lblInformation";
			this.lblInformation.Size = new System.Drawing.Size(4, 14);
			this.lblInformation.TabIndex = 1;
			// 
			// lblScreenTitle
			// 
			this.lblScreenTitle.Location = new System.Drawing.Point(30, 30);
			this.lblScreenTitle.Name = "lblScreenTitle";
			this.lblScreenTitle.Size = new System.Drawing.Size(308, 17);
			this.lblScreenTitle.TabIndex = 0;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.mnuSaveExit,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuProcessSave.Text = "Save";
			this.mnuProcessSave.Visible = false;
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Visible = false;
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			this.Seperator.Visible = false;
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmFixProcess
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(522, 619);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmFixProcess";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Fix Process";
			this.Load += new System.EventHandler(this.frmFixProcess_Load);
			this.Activated += new System.EventHandler(this.frmFixProcess_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFixProcess_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFixProcess_KeyPress);
			this.Resize += new System.EventHandler(this.frmFixProcess_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRun)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
