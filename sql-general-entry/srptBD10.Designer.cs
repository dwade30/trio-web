﻿namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD10.
	/// </summary>
	partial class srptBD10
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBD10));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstanding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstanding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldType,
				this.fldIssued,
				this.fldOutstanding,
				this.fldCashed,
				this.fldTotal
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6
			});
			this.GroupHeader1.Height = 0.4895833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldIssuedTotal,
				this.fldOutstandingTotal,
				this.fldCashedTotal,
				this.fldTotalTotal,
				this.Line2
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Check Rec Summary - Amount";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.65625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.46875F;
			this.Line1.Width = 6.03125F;
			this.Line1.X1 = 0.65625F;
			this.Line1.X2 = 6.6875F;
			this.Line1.Y1 = 0.46875F;
			this.Line1.Y2 = 0.46875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.65625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "";
			this.Label2.Text = "Type";
			this.Label2.Top = 0.28125F;
			this.Label2.Width = 1.5625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.34375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "text-align: right";
			this.Label3.Text = "Issued";
			this.Label3.Top = 0.28125F;
			this.Label3.Width = 1.03125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Text = "Outstanding";
			this.Label4.Top = 0.28125F;
			this.Label4.Width = 1.03125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.53125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Cashed";
			this.Label5.Top = 0.28125F;
			this.Label5.Width = 1.03125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Total";
			this.Label6.Top = 0.28125F;
			this.Label6.Width = 1.03125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.6875F;
			this.fldType.Name = "fldType";
			this.fldType.Text = "Field1";
			this.fldType.Top = 0F;
			this.fldType.Width = 1.5625F;
			// 
			// fldIssued
			// 
			this.fldIssued.Height = 0.1875F;
			this.fldIssued.Left = 2.34375F;
			this.fldIssued.Name = "fldIssued";
			this.fldIssued.Style = "text-align: right";
			this.fldIssued.Text = "Field1";
			this.fldIssued.Top = 0F;
			this.fldIssued.Width = 1.03125F;
			// 
			// fldOutstanding
			// 
			this.fldOutstanding.Height = 0.1875F;
			this.fldOutstanding.Left = 3.4375F;
			this.fldOutstanding.Name = "fldOutstanding";
			this.fldOutstanding.Style = "text-align: right";
			this.fldOutstanding.Text = "Field1";
			this.fldOutstanding.Top = 0F;
			this.fldOutstanding.Width = 1.03125F;
			// 
			// fldCashed
			// 
			this.fldCashed.Height = 0.1875F;
			this.fldCashed.Left = 4.53125F;
			this.fldCashed.Name = "fldCashed";
			this.fldCashed.Style = "text-align: right";
			this.fldCashed.Text = "Field1";
			this.fldCashed.Top = 0F;
			this.fldCashed.Width = 1.03125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 5.625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "text-align: right";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.03125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.6875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold";
			this.Field1.Text = "Totals:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 1.5625F;
			// 
			// fldIssuedTotal
			// 
			this.fldIssuedTotal.Height = 0.1875F;
			this.fldIssuedTotal.Left = 2.34375F;
			this.fldIssuedTotal.Name = "fldIssuedTotal";
			this.fldIssuedTotal.Style = "font-weight: bold; text-align: right";
			this.fldIssuedTotal.Text = "Field1";
			this.fldIssuedTotal.Top = 0.03125F;
			this.fldIssuedTotal.Width = 1.03125F;
			// 
			// fldOutstandingTotal
			// 
			this.fldOutstandingTotal.Height = 0.1875F;
			this.fldOutstandingTotal.Left = 3.4375F;
			this.fldOutstandingTotal.Name = "fldOutstandingTotal";
			this.fldOutstandingTotal.Style = "font-weight: bold; text-align: right";
			this.fldOutstandingTotal.Text = "Field1";
			this.fldOutstandingTotal.Top = 0.03125F;
			this.fldOutstandingTotal.Width = 1.03125F;
			// 
			// fldCashedTotal
			// 
			this.fldCashedTotal.Height = 0.1875F;
			this.fldCashedTotal.Left = 4.53125F;
			this.fldCashedTotal.Name = "fldCashedTotal";
			this.fldCashedTotal.Style = "font-weight: bold; text-align: right";
			this.fldCashedTotal.Text = "Field1";
			this.fldCashedTotal.Top = 0.03125F;
			this.fldCashedTotal.Width = 1.03125F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 5.625F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-weight: bold; text-align: right";
			this.fldTotalTotal.Text = "Field1";
			this.fldTotalTotal.Top = 0.03125F;
			this.fldTotalTotal.Width = 1.03125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.6875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 6F;
			this.Line2.X1 = 0.6875F;
			this.Line2.X2 = 6.6875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// srptBD10
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstanding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstanding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
