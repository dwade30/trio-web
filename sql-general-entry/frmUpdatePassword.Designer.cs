﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmUpdatePassword.
	/// </summary>
	partial class frmUpdatePassword : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtPassword2;
		public fecherFoundation.FCTextBox txtPassword1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.txtPassword2 = new fecherFoundation.FCTextBox();
			this.txtPassword1 = new fecherFoundation.FCTextBox();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.txtCurrentPassword = new fecherFoundation.FCTextBox();
			this.lblCurrentPassword = new fecherFoundation.FCLabel();
			this.lblPasswordStrngth = new fecherFoundation.FCLabel();
			this.txtPasswordStrengthIndicator = new fecherFoundation.FCTextBox();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 387);
			this.BottomPanel.Size = new System.Drawing.Size(576, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtPasswordStrengthIndicator);
			this.ClientArea.Controls.Add(this.lblPasswordStrngth);
			this.ClientArea.Controls.Add(this.txtCurrentPassword);
			this.ClientArea.Controls.Add(this.lblCurrentPassword);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Controls.Add(this.txtPassword2);
			this.ClientArea.Controls.Add(this.txtPassword1);
			this.ClientArea.Controls.Add(this.Label2_0);
			this.ClientArea.Controls.Add(this.Label2_1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Font = new System.Drawing.Font("default", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.ClientArea.Size = new System.Drawing.Size(576, 327);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(576, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Change Password";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(129, 251);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(88, 40);
			this.cmdCancel.TabIndex = 7;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.DialogResult = Wisej.Web.DialogResult.OK;
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(20, 251);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(91, 40);
			this.cmdOK.TabIndex = 6;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// txtPassword2
			// 
			this.txtPassword2.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword2.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword2.Location = new System.Drawing.Point(214, 188);
			this.txtPassword2.Name = "txtPassword2";
			this.txtPassword2.PasswordChar = '*';
			this.txtPassword2.Size = new System.Drawing.Size(157, 40);
			this.txtPassword2.TabIndex = 4;
			// 
			// txtPassword1
			// 
			this.txtPassword1.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword1.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword1.Location = new System.Drawing.Point(214, 128);
			this.txtPassword1.Name = "txtPassword1";
			this.txtPassword1.PasswordChar = '*';
			this.txtPassword1.Size = new System.Drawing.Size(157, 40);
			this.txtPassword1.TabIndex = 2;
			this.txtPassword1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPassword1_KeyPress);
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(20, 202);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(156, 19);
			this.Label2_0.TabIndex = 3;
			this.Label2_0.Text = "CONFIRM PASSWORD";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(20, 142);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(156, 19);
			this.Label2_1.TabIndex = 1;
			this.Label2_1.Text = "NEW PASSWORD";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(268, 17);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "YOU MUST TYPE YOUR PASSWORD TWICE";
			// 
			// txtCurrentPassword
			// 
			this.txtCurrentPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtCurrentPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtCurrentPassword.Location = new System.Drawing.Point(214, 68);
			this.txtCurrentPassword.Name = "txtCurrentPassword";
			this.txtCurrentPassword.PasswordChar = '*';
			this.txtCurrentPassword.Size = new System.Drawing.Size(157, 40);
			this.txtCurrentPassword.TabIndex = 1;
			// 
			// lblCurrentPassword
			// 
			this.lblCurrentPassword.Location = new System.Drawing.Point(20, 82);
			this.lblCurrentPassword.Name = "lblCurrentPassword";
			this.lblCurrentPassword.Size = new System.Drawing.Size(156, 19);
			this.lblCurrentPassword.TabIndex = 9;
			this.lblCurrentPassword.Text = "CURRENT PASSWORD";
			// 
			// lblPasswordStrngth
			// 
			this.lblPasswordStrngth.Alignment = Wisej.Web.HorizontalAlignment.Center;
			this.lblPasswordStrngth.Location = new System.Drawing.Point(401, 148);
			this.lblPasswordStrngth.Name = "lblPasswordStrngth";
			this.lblPasswordStrngth.Size = new System.Drawing.Size(156, 19);
			this.lblPasswordStrngth.TabIndex = 10;
			this.lblPasswordStrngth.Text = "PASSWORD STRENGTH";
			this.lblPasswordStrngth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtPasswordStrengthIndicator
			// 
			this.txtPasswordStrengthIndicator.BackColor = System.Drawing.Color.Red;
			this.txtPasswordStrengthIndicator.Location = new System.Drawing.Point(399, 173);
			this.txtPasswordStrengthIndicator.LockedOriginal = true;
			this.txtPasswordStrengthIndicator.Name = "txtPasswordStrengthIndicator";
			this.txtPasswordStrengthIndicator.ReadOnly = true;
			this.txtPasswordStrengthIndicator.Size = new System.Drawing.Size(157, 40);
			this.txtPasswordStrengthIndicator.TabIndex = 12;
			this.txtPasswordStrengthIndicator.TabStop = false;
			this.txtPasswordStrengthIndicator.Text = "BLANK";
			this.txtPasswordStrengthIndicator.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			// 
			// frmUpdatePassword
			// 
			this.AcceptButton = this.cmdOK;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(576, 387);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmUpdatePassword";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Change Password";
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUpdatePassword_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public FCTextBox txtCurrentPassword;
		public FCLabel lblCurrentPassword;
		public FCLabel lblPasswordStrngth;
		public FCTextBox txtPasswordStrengthIndicator;
	}
}
