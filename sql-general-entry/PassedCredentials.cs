﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWGNENTY
{
    internal class PassedCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}