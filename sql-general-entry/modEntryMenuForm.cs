﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

namespace TWGNENTY
{
	public class modEntryMenuForm
	{
		[DllImport("kernel32", EntryPoint = "GetDriveTypeA")]
		public static extern object GetDriveType(string nDrive);

		public static void EMMain()
		{
			try
			{
				modReplaceWorkFiles.EntryFlagFile(false, "", true);
				// testing
				if (!((modGlobalConstants.Statics.gstrEntryFlag == string.Empty) || (Convert.ToByte(Strings.Left(modGlobalConstants.Statics.gstrEntryFlag + " ", 1)[0]) == 0)))
				{
					MDIParent.InstancePtr.Init();
					return;
				}
				modGNBas.Statics.EntryMenuFlag = true;
            }
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Line " + Information.Erl() + " in EMMain", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Information.Err(ex).Clear();
			}
		}

		//public static void CheckDate()
		//{
		//	// uses the variables Cyear and Cmonth from code module 1 to determine if the dates are .... correct?
		//	bool DateFlag;
		//	object yr = null;
		//	int mo = 0;
		//	int Violation;
		//	DateFlag = false;
		//	if (FCConvert.ToInt32(yr) < Statics.CYear)
		//		Violation = 1;
		//	if (FCConvert.ToInt32(yr) > Statics.CYear)
		//	{
		//		DateFlag = true;
		//	}
		//	else if (FCConvert.ToInt32(yr) == Statics.CYear && mo >= Statics.CMonth)
		//	{
		//		DateFlag = true;
		//	}
		//}

		public static void EnableDisableMenuOption(bool boolState, int intRow)
		{
			//if (!boolState)
			//{
			//	MDIParent.InstancePtr.Grid.Select(intRow, 1);
			//	MDIParent.InstancePtr.Grid.CellForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			//	// MDIParent.BackColor
			//	MDIParent.InstancePtr.Grid.RowData(intRow, false);
			//}
			//else
			//{
			//	MDIParent.InstancePtr.Grid.Select(intRow, 1);
			//	MDIParent.InstancePtr.Grid.CellForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
			//	MDIParent.InstancePtr.Grid.RowData(intRow, true);
			//}
		}

		//public static void CheckAvailablePrograms()
		//{
		//	EnableDisableMenuOption(Statics.RealEstateAllowed, 1);
		//	EnableDisableMenuOption(Statics.PersonalPropertyAllowed, 2);
		//	EnableDisableMenuOption(Statics.BillingAllowed, 3);
		//	EnableDisableMenuOption(Statics.CollectionsAllowed, 4);
		//	EnableDisableMenuOption(Statics.CollectionsAllowed, 5);
		//	EnableDisableMenuOption(Statics.ClerkAllowed, 6);
		//	EnableDisableMenuOption(Statics.VoterRegAllowed, 7);
		//	EnableDisableMenuOption(Statics.CodeAllowed, 8);
		//	EnableDisableMenuOption(Statics.BudgetaryAllowed, 9);
		//	EnableDisableMenuOption(Statics.CashReceiptAllowed, 10);
		//	EnableDisableMenuOption(Statics.PayrollAllowed, 11);
		//	EnableDisableMenuOption(Statics.UtilitiesAllowed, 12);
		//	EnableDisableMenuOption(Statics.RedbookAllowed, 13);
		//	EnableDisableMenuOption(Statics.MotorVehicleAllowed, 14);
		//	EnableDisableMenuOption(Statics.FixedAssetAllowed, 16);
		//	EnableDisableMenuOption(Statics.AccountsReceivableAllowed, 17);
		//}

		public static void CheckExpirationDate()
		{
			// using the current date, this function assigns the boolean variables based on the program
			// expiration date
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSecurity = new clsDRWrapper();
				string strExpireMessage = "";
				string strExpired = "";
				bool boolTheyExist;
				string strPath;
				clsDRWrapper clsTemp = new clsDRWrapper();
				int intReturn;
				int intMessageIndex;
				BeginAgain:
				;
				strPath = Application.StartupPath;
				if (Strings.Right(strPath, 1) != "\\")
					strPath += "\\";
				strPath += "bin\\";
				for (intReturn = 0; intReturn <= 18; intReturn++)
				{
					Statics.strDisabledReason[intReturn] = "";
				}
				intMessageIndex = 1;
				if (rsSecurity.OpenRecordset("Select * from Modules", "systemsettings"))
				{
					if (!rsSecurity.EndOfFile())
					{
						/*? On Error Resume Next  */// Real Estate
						// RealEstateAllowed = CompareDates(LongDate(rsSecurity.GetData("REDate")), LongDate(Now), ">")
						try
						{
							Statics.RealEstateAllowed = (DateAndTime.DateDiff("d", (DateTime)rsSecurity.Get_Fields_DateTime("REDate"), DateTime.Now)) <= 0;
							if (DateTime.Today.Month == FCConvert.ToInt32(rsSecurity.Get_Fields_DateTime("REDate").Month) && DateTime.Today.Year == FCConvert.ToInt32(rsSecurity.Get_Fields_DateTime("REDate").Year))
							{
								if (FCConvert.ToBoolean(rsSecurity.GetData("RE")))
								{
									//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
									//strExpireMessage += "Real Estate:      " + rsSecurity.GetData("REDate") + "\r";
									strExpireMessage += "Real Estate:      " + rsSecurity.Get_Fields_DateTime("REDate").ToShortDateString() + "\r\n";
								}
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						try
						{
							if (!Statics.RealEstateAllowed)
							{
								if (FCConvert.ToBoolean(rsSecurity.GetData("RE")))
								{
									//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
									//strExpired += "Real Estate:      " + rsSecurity.Get_Fields_DateTime("REDate") + "\r";
									strExpired += "Real Estate:      " + rsSecurity.Get_Fields_DateTime("REDate").ToShortDateString() + "\r\n";
									Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("REDate"), "MM/dd/yyyy");
								}
								else
								{
									Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
								}
							}
							else
							{
								if (!File.Exists(strPath + "twre0000.dll"))
								{
									Statics.RealEstateAllowed = false;
									Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
								}
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							if (FCConvert.ToBoolean(rsSecurity.GetData("sk")))
							{
								if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("skDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("skDate")).Year)
								{
									strExpireMessage += "Real Estate Sketching:     " + rsSecurity.Get_Fields_DateTime("skDate").ToShortDateString() + "\r\n";
								}
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						try
						{
							if (FCConvert.ToBoolean(rsSecurity.GetData("sk")))
							{
								if (!modDateRoutines.CompareDates(
									modDateRoutines.LongDate(rsSecurity.GetData("skDate")),
									modDateRoutines.LongDate(DateTime.Now), ">"))
								{
									strExpired += "Real Estate Sketching:     " +
										              rsSecurity.Get_Fields_DateTime("skDATE").ToShortDateString() +
										              "\r\n";
								}
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						try
						{
							if (FCConvert.ToBoolean(rsSecurity.GetData("RH")))
							{
								if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("RHdate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("rHdate")).Year)
								{
									//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
									//strExpireMessage += "Real Estate Handheld:     " + rsSecurity.GetData("rHdate") + "\r";
									strExpireMessage += "Real Estate Handheld:     " + rsSecurity.Get_Fields_DateTime("rHdate").ToShortDateString() + "\r\n";
								}
								if (!modDateRoutines.CompareDates(modDateRoutines.LongDate(rsSecurity.GetData("rhdate")), modDateRoutines.LongDate(DateTime.Now), ">"))
								{
									//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
									//strExpired += "Real Estate Handheld:     " + rsSecurity.GetData("rhdate") + "\r";
									strExpired += "Real Estate Handheld:     " + rsSecurity.Get_Fields_DateTime("rhdate").ToShortDateString() + "\r\n";
								}
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						// Commercial
						Statics.CommercialAllowed = false;
						try
						{
							// Personal Property
							// PersonalPropertyAllowed = CompareDates(LongDate(rsSecurity.GetData("PPDate")), LongDate(Now), ">")
							Statics.PersonalPropertyAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.Get_Fields_DateTime("PPDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("PP")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("PPDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("PPDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("PP")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Personal Prop:     " + rsSecurity.GetData("PPDate") + "\r";
											strExpireMessage += "Personal Prop:     " + rsSecurity.Get_Fields_DateTime("PPDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.PersonalPropertyAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("PP")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Personal Prop:     " + rsSecurity.GetData("PPDate") + "\r";
											strExpired += "Personal Prop:     " + rsSecurity.Get_Fields_DateTime("PPDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("PPDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twpp0000.dll"))
										{
											Statics.PersonalPropertyAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						// Billing
						// BillingAllowed = CompareDates(LongDate(rsSecurity.GetData("BLDate")), LongDate(Now), ">")
						try
						{
							Statics.BillingAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("BLDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("BL")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("BLDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("BLDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("BL")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Billing:     " + rsSecurity.GetData("BLDate") + "\r";
											strExpireMessage += "Billing:     " + rsSecurity.Get_Fields_DateTime("BLDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.BillingAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("BL")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Billing:     " + rsSecurity.GetData("BLDate") + "\r";
											strExpired += "Billing:     " + rsSecurity.Get_Fields_DateTime("BLDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("BLDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twBL0000.dll"))
										{
											Statics.BillingAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Collections
							// CollectionsAllowed = CompareDates(LongDate(rsSecurity.GetData("CLDate")), LongDate(Now), ">")
							Statics.CollectionsAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("CLDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("CL")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("CLDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("CLDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CL")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Collections:     " + rsSecurity.GetData("CLDate") + "\r";
											strExpireMessage += "Collections:     " + rsSecurity.Get_Fields_DateTime("CLDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.CollectionsAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CL")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Collections:     " + rsSecurity.GetData("CLDate") + "\r";
											strExpired += "Collections:     " + rsSecurity.Get_Fields_DateTime("CLDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("CLDate"), "MM/dd/yyyy");
											intMessageIndex += 1;
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("REDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
											intMessageIndex += 1;
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twCL0000.dll"))
										{
											Statics.CollectionsAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
											intMessageIndex += 1;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Clerk
							// ClerkAllowed = CompareDates(LongDate(rsSecurity.GetData("CKDate")), LongDate(Now), ">")
							Statics.ClerkAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("CKDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("CK")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("CKDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("CKDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CK")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Clerk:                " + rsSecurity.GetData("CKDate") + "\r";
											strExpireMessage += "Clerk:                " + rsSecurity.Get_Fields_DateTime("CKDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.ClerkAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CK")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Clerk:                " + rsSecurity.GetData("CKDate") + "\r";
											strExpired += "Clerk:                " + rsSecurity.Get_Fields_DateTime("CKDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("CKDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twck0000.dll"))
										{
											Statics.ClerkAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Voter Registration
							// VoterRegAllowed = CompareDates(LongDate(rsSecurity.GetData("VRDate")), LongDate(Now), ">")
							Statics.VoterRegAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("VRDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("VR")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("VRDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("VRDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("VR")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Voter Reg:          " + rsSecurity.GetData("VRDate") + "\r";
											strExpireMessage += "Voter Reg:          " + rsSecurity.Get_Fields_DateTime("VRDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.VoterRegAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("VR")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Voter Reg:          " + rsSecurity.GetData("VRDate") + "\r";
											strExpired += "Voter Reg:          " + rsSecurity.Get_Fields_DateTime("VRDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("VRDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twvr0000.dll"))
										{
											Statics.VoterRegAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Code Enforcement
							// CodeAllowed = CompareDates(LongDate(rsSecurity.GetData("CEDate")), LongDate(Now), ">")
							Statics.CodeAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("CEDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("CE")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("CEDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("CEDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CE")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Code Enforcement:                " + rsSecurity.GetData("CEDate") + "\r";
											strExpireMessage += "Code Enforcement:                " + rsSecurity.Get_Fields_DateTime("CEDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.CodeAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CE")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Code Enforcement:                " + rsSecurity.GetData("CEDate") + "\r";
											strExpired += "Code Enforcement:                " + rsSecurity.Get_Fields_DateTime("CEDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("CEDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twce0000.dll"))
										{
											Statics.CodeAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Budgetary
							// BudgetaryAllowed = CompareDates(LongDate(rsSecurity.Fields("BDDate")), LongDate(Now), ">")
							Statics.BudgetaryAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.Get_Fields_DateTime("BDDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("BD")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("BDDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.Get_Fields_DateTime("BDDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.Get_Fields_Boolean("BD")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Budgetary:                " + rsSecurity.GetData("BDDate") + "\r";
											strExpireMessage += "Budgetary:                " + rsSecurity.Get_Fields_DateTime("BDDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.BudgetaryAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("BD")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Budgetary:                " + rsSecurity.GetData("BDDate") + "\r";
											strExpired += "Budgetary:                " + rsSecurity.Get_Fields_DateTime("BDDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("BDDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twbd0000.dll"))
										{
											Statics.BudgetaryAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Cash Receipting
							// CashReceiptAllowed = CompareDates(LongDate(rsSecurity.GetData("CRDate")), LongDate(Now), ">")
							Statics.CashReceiptAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("CRDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("CR")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("CRDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("CRDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CR")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Cash Receipting:     " + rsSecurity.GetData("CRDate") + "\r";
											strExpireMessage += "Cash Receipting:     " + rsSecurity.Get_Fields_DateTime("CRDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.CashReceiptAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("CR")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Cash Receipting:     " + rsSecurity.GetData("CRDate") + "\r";
											strExpired += "Cash Receipting:     " + rsSecurity.Get_Fields_DateTime("CRDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("CRDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twCR0000.dll"))
										{
											Statics.CashReceiptAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// 
							// Payroll
							// PayrollAllowed = CompareDates(LongDate(rsSecurity.GetData("PYDate")), LongDate(Now), ">")
							Statics.PayrollAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("PYDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("PY")))
							{
								if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("PYDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("PYDate")).Year)
								{
									if (FCConvert.ToBoolean(rsSecurity.GetData("PY")))
									{
										//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
										//strExpireMessage += "Payroll:                " + rsSecurity.GetData("PYDate") + "\r";
										strExpireMessage += "Payroll:                " + rsSecurity.Get_Fields_DateTime("PYDate").ToShortDateString() + "\r\n";
									}
								}
								if (!Statics.PayrollAllowed)
								{
									if (FCConvert.ToBoolean(rsSecurity.GetData("PY")))
									{
										//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
										//strExpired += "Payroll:                " + rsSecurity.GetData("PYDate") + "\r";
										strExpired += "Payroll:                " + rsSecurity.Get_Fields_DateTime("PYDate").ToShortDateString() + "\r\n";
										Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("PYDate"), "MM/dd/yyyy");
									}
									else
									{
										Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
									}
								}
								else
								{
									if (!File.Exists(strPath + "twpy0000.dll"))
									{
										Statics.PayrollAllowed = false;
										Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
									}
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Utility
							// UtilitiesAllowed = CompareDates(LongDate(rsSecurity.GetData("UTDate")), LongDate(Now), ">")
							Statics.UtilitiesAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("UTDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("UT")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("UTDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("UTDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("UT")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Utility Billing:                " + rsSecurity.GetData("UTDate") + "\r";
											strExpireMessage += "Utility Billing:                " + rsSecurity.Get_Fields_DateTime("UTDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.UtilitiesAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("UT")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Utility Billing:                " + rsSecurity.GetData("UTDate") + "\r";
											strExpired += "Utility Billing:                " + rsSecurity.Get_Fields_DateTime("UTDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("UTDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twUT0000.dll"))
										{
											Statics.UtilitiesAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// fixed asset
							Statics.FixedAssetAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("FADate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("FA")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("FADate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("FADate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("FA")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Fixed Asset:                " + rsSecurity.GetData("FADate") + "\r";
											strExpireMessage += "Fixed Asset:                " + rsSecurity.Get_Fields_DateTime("FADate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									if (!Statics.FixedAssetAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("FA")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Fixed Asset:                " + rsSecurity.GetData("FADate") + "\r";
											strExpired += "Fixed Asset:                " + rsSecurity.Get_Fields_DateTime("FADate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("FADate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twFA0000.dll"))
										{
											Statics.FixedAssetAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// accountsreceivable
							Statics.AccountsReceivableAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.GetData("ARDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("AR")))
							{
								if (DateTime.Today.Month == ((DateTime)rsSecurity.Get_Fields_DateTime("ARDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.Get_Fields_DateTime("ARDate")).Year)
								{
									if (FCConvert.ToBoolean(rsSecurity.Get_Fields_Boolean("AR")))
									{
										//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
										//strExpireMessage += "Accounts Receivable:         " + rsSecurity.Get_Fields_DateTime("ardate") + "\r";
										strExpireMessage += "Accounts Receivable:         " + rsSecurity.Get_Fields_DateTime("ardate").ToShortDateString() + "\r\n";
									}
								}
								if (!Statics.AccountsReceivableAllowed)
								{
									if (FCConvert.ToBoolean(rsSecurity.Get_Fields_Boolean("AR")))
									{
										//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
										//strExpired += "Accounts Receivable         " + rsSecurity.Get_Fields_DateTime("ARDate") + "\r";
										strExpired += "Accounts Receivable         " + rsSecurity.Get_Fields_DateTime("ARDate").ToShortDateString() + "\r\n";
										Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("ARDate"), "MM/dd/yyyy");
									}
									else
									{
										Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
									}
								}
								else
								{
									if (!File.Exists(strPath + "twar0000.dll"))
									{
										Statics.AccountsReceivableAllowed = false;
										Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
									}
								}
							}
							else
							{
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							// Motor Vehicle
							// MotorVehicleAllowed = CompareDates(LongDate(rsSecurity.GetData("MVDate")), LongDate(Now), ">")
							Statics.MotorVehicleAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.Get_Fields_DateTime("MVDate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
							if (FCConvert.ToBoolean(rsSecurity.GetData("MV")))
							{
								try
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.GetData("MVDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.GetData("MVDate")).Year)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("MV")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Motor Vehicle:      " + rsSecurity.GetData("MVDate") + "\r";
											strExpireMessage += "Motor Vehicle:      " + rsSecurity.Get_Fields_DateTime("MVDate").ToShortDateString() + "\r\n";
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
								try
								{
									Statics.RedbookAllowed = Statics.MotorVehicleAllowed;
									if (!Statics.MotorVehicleAllowed)
									{
										if (FCConvert.ToBoolean(rsSecurity.GetData("MV")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpired += "Motor Vehicle:      " + rsSecurity.GetData("MVDate") + "\r";
											strExpired += "Motor Vehicle:      " + rsSecurity.Get_Fields_DateTime("MVDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("MVDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									else
									{
										if (!File.Exists(strPath + "twmv0000.dll"))
										{
											Statics.MotorVehicleAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
										else
										{
											if (Conversion.Val(rsSecurity.Get_Fields_String("mv_statelevel")) == 0)
											{
												Statics.MotorVehicleAllowed = false;
												Statics.strDisabledReason[intMessageIndex] = "State level to low";
											}
										}
									}
								}
								//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
								catch
								{
								}
							}
							else
							{
								Statics.RedbookAllowed = false;
								Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						try
						{
							if (!Statics.RedbookAllowed)
							{
								Statics.RedbookAllowed = DateAndTime.DateDiff("d", FCConvert.ToDateTime(modDateRoutines.LongDate(rsSecurity.Get_Fields_DateTime("rbdate"))), FCConvert.ToDateTime(modDateRoutines.LongDate(DateTime.Now))) <= 0;
								// TODO Get_Fields: Check the table for the column [RB] and replace with corresponding Get_Field method
								if (FCConvert.ToBoolean(rsSecurity.Get_Fields("RB")))
								{
									if (DateTime.Today.Month == ((DateTime)rsSecurity.Get_Fields_DateTime("rbDate")).Month && DateTime.Today.Year == ((DateTime)rsSecurity.Get_Fields_DateTime("rbDate")).Year)
									{
										// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
										if (FCConvert.ToBoolean(rsSecurity.Get_Fields("rb")))
										{
											//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
											//strExpireMessage += "Blue Book:      " + rsSecurity.GetData("rbDate") + "\r";
											strExpireMessage += "Blue Book:      " + rsSecurity.Get_Fields_DateTime("rbDate").ToShortDateString() + "\r\n";
											Statics.strDisabledReason[intMessageIndex] = "Module expired: " + Strings.Format(rsSecurity.Get_Fields_DateTime("RBDate"), "MM/dd/yyyy");
										}
										else
										{
											Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
										}
									}
									if (!Statics.RedbookAllowed)
									{
										//FC:FINAL:MSH - issue #1535: get date in a short format and move caret to a new line
										//strExpired += "Blue Book:      " + rsSecurity.Get_Fields_DateTime("RBDate") + "\r\n";
										strExpired += "Blue Book:      " + rsSecurity.Get_Fields_DateTime("RBDate").ToShortDateString() + "\r\n";
									}
									else
									{
										if (!File.Exists(strPath + "twrb0000.dll"))
										{
											Statics.RedbookAllowed = false;
											Statics.strDisabledReason[intMessageIndex] = "Unable to locate program";
										}
									}
								}
								else
								{
									Statics.RedbookAllowed = false;
									Statics.strDisabledReason[intMessageIndex] = "Module not purchased";
								}
							}
							else
							{
								// strDisabledReason(intMessageIndex) = "Module not purchased"
							}
						}
						//FC:FINAl:SBE - add try with empty catch to have the same result as in original (On Error Resume Next)
						catch
						{
						}
						intMessageIndex += 1;
						
						intMessageIndex += 1;
					}
					Statics.RealEstateAllowed = Statics.RealEstateAllowed || Statics.BillingAllowed;
					Statics.PersonalPropertyAllowed = Statics.PersonalPropertyAllowed || Statics.BillingAllowed;
					if (Statics.gboolShowMessage)
					{
						Statics.gboolShowMessage = false;
						if (strExpireMessage == string.Empty)
						{
						}
						else
						{
							//FC:FINAL:MSH - issue #1535: move caret to a new line
							//strExpireMessage = "TRIO Software coming expiration (s)" + "\r" + "\r" + strExpireMessage;
							strExpireMessage = "TRIO Software coming expiration (s)" + "\r\n" + "\r\n" + strExpireMessage;
							MessageBox.Show(strExpireMessage, "TRIO Software Expirations", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						// If strExpired = vbNullString Then
						// 
						// Else
						// strExpired = "TRIO Software Expired Modules" & Chr(13) & Chr(13) & strExpired
						// MsgBox strExpired, vbInformation, "TRIO Software Expired Modules"
						// End If
					}
					rsSecurity = null;
					return;
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("General Entry has encountered a problem and will close." + "\r\n" + Information.Err(ex).Description, "Closing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				// If intReturn = vbYes Then
				// Set dbtemp = Nothing
				// RepairGNDatabase
				// GoTo BeginAgain
				// Else
				// MsgBox "General Entry is closing.", vbInformation, "Closing"
				Application.Exit();
				// End If
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		//public static void SecurityCheck()
		//{
		//	string Check;
		//	int X;
		//	int Total = 0;
		//	// vbPorter upgrade warning: Summ As short --> As int	OnWrite(int, double)
		//	short Summ;
		//	int Violation;
		//	Check = "";
		//	Check = Strings.Mid(new string(modGNWork.Statics.SEC.MUNI), 3, 1);
		//	Check += Strings.Mid(new string(modGNWork.Statics.SEC.MUNI), 12, 1);
		//	Check += Strings.Mid(new string(modGNWork.Statics.SEC.MUNI), 1, 1);
		//	Check += Strings.Mid(new string(modGNWork.Statics.SEC.MUNI), 10, 1);
		//	Check += Strings.Mid(new string(modGNWork.Statics.SEC.MUNI), 5, 1);
		//	X = 1;
		//	do
		//	{
		//		Total += Convert.ToByte(Strings.Mid(Check, X, 1)[0]);
		//		X += 1;
		//	}
		//	while (X < 6);
		//	if (Total != modGNWork.Statics.SEC.CTLM)
		//		Violation = 4;
		//	Summ = FCConvert.ToInt16(modGNWork.Statics.SEC.RSYEAR + modGNWork.Statics.SEC.RSMONTH);
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.SRYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.SRMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.CMYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.CMMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.PPYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.PPMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.BLYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.BLMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.CLYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.CLMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.VRYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.VRMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.CEYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.CEMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.BDYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.BDMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.CRYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.CRMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.PYYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.PYMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.UTYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.UTMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.MVYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.MVMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.TCYEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.TCMONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.E9YEAR)) + Conversion.Val(new string(modGNWork.Statics.SEC.E9MONTH)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.VRMAX)) + Conversion.Val(new string(modGNWork.Statics.SEC.MAXPP)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.CEMAX)) + Conversion.Val(new string(modGNWork.Statics.SEC.PYMAX)));
		//	Summ += FCConvert.ToInt16(Conversion.Val(new string(modGNWork.Statics.SEC.UTMAX)) + Conversion.Val(new string(modGNWork.Statics.SEC.MVMAX)) + Conversion.Val(new string(modGNWork.Statics.SEC.RBMAX)));
		//	Summ -= 1969;
		//	if (modGNWork.Statics.SEC.NETWORKFLAG[0] == 'Y')
		//		modGNWork.Statics.SEC.NETWORKFLAG2 = modGNWork.Statics.SEC.NETWORKFLAG;
		//	if (modGNWork.Statics.SEC.NETWORKFLAG2[0] == '0')
		//		modGNWork.Statics.SEC.NETWORKFLAG2 = modGNWork.Statics.SEC.NETWORKFLAG;
		//	if (modGNWork.Statics.SEC.NETWORKFLAG2[0] == 'Y' || modGNWork.Statics.SEC.NETWORKFLAG[0] == 'Y')
		//	{
		//		Summ += 1;
		//	}
		//	if (Summ != Conversion.Val(new string(modGNWork.Statics.SEC.SPRODSUM)))
		//		Violation = 5;
		//}

		public class StaticVariables
		{
            public bool RealEstateAllowed;
			public bool CommercialAllowed;
			public bool PersonalPropertyAllowed;
			public bool BillingAllowed;
			public bool CollectionsAllowed;
			public bool ClerkAllowed;
			public bool VoterRegAllowed;
			public bool CodeAllowed;
			public bool BudgetaryAllowed;
			public bool CashReceiptAllowed;
			public bool PayrollAllowed;
			public bool UtilitiesAllowed;
			public bool RedbookAllowed;
			public bool MotorVehicleAllowed;
			public bool FixedAssetAllowed;
			public bool AccountsReceivableAllowed;
			
            public bool gboolShowMessage;
		
			public bool PasswordChecked;
            public string[] strDisabledReason = new string[18 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
