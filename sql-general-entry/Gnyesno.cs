//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	public class modGNYesNo
	{

		//=========================================================
		public static string ans = "";

		public static void ClearSpecs()
		{
			int X;
			for(X=1; X<=10; X++) {
				modGNBas.SpecLength[X] = 0;
				modGNBas.SpecCase[X] = "";
				modGNBas.SpecKeyword[X] = "";
				modGNBas.SpecM1[X] = "";
				modGNBas.SpecM2[X] = "";
				modGNBas.SpecM3[X] = "";
				modGNBas.SpecM4[X] = "";
				modGNBas.SpecM5[X] = "";
				modGNBas.SpecM6[X] = "";
				modGNBas.SpecM7[X] = "";
				modGNBas.SpecM8[X] = "";
				modGNBas.SpecM9[X] = "";
				modGNBas.SpecM10[X] = "";
				modGNBas.SpecType[X] = "";
				modGNBas.SpecParam[X] = "";
				modGNBas.SpecLow[X] = Convert.ToString(0);
				modGNBas.SpecHigh[X] = Convert.ToString(0);
			} // X
			frmGNYesNo.lblm1.Caption = "";
			frmGNYesNo.lblm2.Caption = "";
			frmGNYesNo.lblm3.Caption = "";
			frmGNYesNo.lblM4.Caption = "";
			frmGNYesNo.lblM5.Caption = "";
			frmGNYesNo.lblM6.Caption = "";
			frmGNYesNo.lblM7.Caption = "";
			frmGNYesNo.lblM8.Caption = "";
			frmGNYesNo.lblM9.Caption = "";
			frmGNYesNo.lblM10.Caption = "";
		}

		public static void Get1_10(int fld)
		{
			modGNBas.RespLength = modGNBas.SpecLength[fld];
			modGNBas.RespCase = modGNBas.SpecCase[fld];
			modGNBas.RespKeyword = modGNBas.SpecKeyword[fld];
			modGNBas.RespM1 = modGNBas.SpecM1[fld];
			modGNBas.RespM2 = modGNBas.SpecM2[fld];
			modGNBas.RespM3 = modGNBas.SpecM3[fld];
			modGNBas.RespM4 = modGNBas.SpecM4[fld];
			modGNBas.RespM5 = modGNBas.SpecM5[fld];
			modGNBas.RespM6 = modGNBas.SpecM6[fld];
			modGNBas.RespM7 = modGNBas.SpecM7[fld];
			modGNBas.RespM8 = modGNBas.SpecM8[fld];
			modGNBas.RespM9 = modGNBas.SpecM9[fld];
			modGNBas.RespM10 = modGNBas.SpecM10[fld];
			modGNBas.RespType = modGNBas.SpecType[fld];
			modGNBas.RespParam = modGNBas.SpecParam[fld];
			modGNBas.RespLow = modGNBas.SpecLow[fld];
			modGNBas.RespHigh = modGNBas.SpecHigh[fld];
			if ((int)fld >1) {
				frmGNYesNo.cmdBack.Visible = true;
			} else {
				frmGNYesNo.cmdBack.Visible = false;
			}
		}

	}
}