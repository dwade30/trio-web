using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TWGNENTY
{

    [Table("tblRegion")]
    public partial class tblRegion
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string TownName { get; set; }

        [StringLength(50)]
        public string TownAbbreviation { get; set; }

        public int? TownNumber { get; set; }

        [StringLength(255)]
        public string ResCode { get; set; }

        [StringLength(255)]
        public string Fund { get; set; }

        [StringLength(255)]
        public string RECommitmentAccount { get; set; }

        [StringLength(255)]
        public string PPCommitmentAccount { get; set; }

        [StringLength(255)]
        public string RESupplementalAccount { get; set; }

        [StringLength(255)]
        public string PPSupplementalAccount { get; set; }
    }
}
