﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptAR1.
	/// </summary>
	public partial class srptAR1 : BaseSectionReport
	{
		public srptAR1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptAR1 InstancePtr
		{
			get
			{
				return (srptAR1)Sys.GetInstance(typeof(srptAR1));
			}
		}

		protected srptAR1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAR1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curCLTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCLTotal;
		// vbPorter upgrade warning: curGLTotal As Decimal	OnWrite(short, double)
		Decimal curGLTotal;
		int lngNumberOfBills;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT COUNT(BillType) as NumberOfBills, SUM(PrinOwed) as PrinOwedTotal, SUM(PrinPaid) as PrinPaidTotal, BillType FROM Bill WHERE BillStatus <> 'V' GROUP BY BillType HAVING SUM(PrinOwed) - SUM(PrinPaid) <> 0 ORDER BY BillType", "TWAR0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				// Me.Cancel
				//this.Stop();
				this.Cancel();
				return;
			}
			blnFirstRecord = true;
			curCLTotal = 0;
			curGLTotal = 0;
			lngNumberOfBills = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsBudInfo = new clsDRWrapper();
			//clsDRWrapper rsAcctInfo = new clsDRWrapper();
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
            try
            {
                // TODO Get_Fields: Check the table for the column [BillType] and replace with corresponding Get_Field method
                if (FCConvert.ToInt32(rsInfo.Get_Fields("BillType")) == 0)
                {
                    fldBillType.Text = "000 Pre-Payment";
                    rsDefaultInfo.OpenRecordset("SELECT * FROM Customize", "TWAR0000.vb1");
                    if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
                    {
                        if (Strings.Trim(rsDefaultInfo.Get_Fields_String("PrePayReceivableAccount") + "") != "")
                        {
                            rsBudInfo.OpenRecordset(
                                "SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = '" +
                                rsDefaultInfo.Get_Fields_String("PrePayReceivableAccount") + "'", "TWBD0000.vb1");
                            if (rsBudInfo.EndOfFile() != true && rsBudInfo.BeginningOfFile() != true)
                            {
                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curGLTotal = FCConvert.ToDecimal(
                                    Conversion.Val(rsBudInfo.Get_Fields("OriginalBudgetTotal")) +
                                    Conversion.Val(rsBudInfo.Get_Fields("BudgetAdjustmentsTotal")) +
                                    Conversion.Val(rsBudInfo.Get_Fields("PostedDebitsTotal")) +
                                    Conversion.Val(rsBudInfo.Get_Fields("EncumbActivityTotal")) +
                                    Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")));
                            }
                            else
                            {
                                curGLTotal = 0;
                            }
                        }
                        else
                        {
                            curGLTotal = 0;
                        }
                    }
                    else
                    {
                        curGLTotal = 0;
                    }
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [BillType] and replace with corresponding Get_Field method
                    rsTypeInfo.OpenRecordset(
                        "SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsInfo.Get_Fields("BillType"),
                        "TWAR0000.vb1");
                    if (rsTypeInfo.EndOfFile() != true && rsTypeInfo.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Check the table for the column [BillType] and replace with corresponding Get_Field method
                        fldBillType.Text = Strings.Format(rsInfo.Get_Fields("BillType"), "000") + " " +
                                           rsTypeInfo.Get_Fields_String("TypeTitle");
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [BillType] and replace with corresponding Get_Field method
                        fldBillType.Text = Strings.Format(rsInfo.Get_Fields("BillType"), "000") + " " + "UNKNOWN";
                    }

                    if (Strings.Trim(rsTypeInfo.Get_Fields_String("ReceivableAccount") + "") != "")
                    {
                        rsBudInfo.OpenRecordset(
                            "SELECT SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Account = '" +
                            rsTypeInfo.Get_Fields_String("ReceivableAccount") + "'", "TWBD0000.vb1");
                        if (rsBudInfo.EndOfFile() != true && rsBudInfo.BeginningOfFile() != true)
                        {
                            // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                            curGLTotal = FCConvert.ToDecimal(
                                Conversion.Val(rsBudInfo.Get_Fields("OriginalBudgetTotal")) +
                                Conversion.Val(rsBudInfo.Get_Fields("BudgetAdjustmentsTotal")) +
                                Conversion.Val(rsBudInfo.Get_Fields("PostedDebitsTotal")) +
                                Conversion.Val(rsBudInfo.Get_Fields("EncumbActivityTotal")) +
                                Conversion.Val(rsBudInfo.Get_Fields("PostedCreditsTotal")));
                        }
                        else
                        {
                            curGLTotal = 0;
                        }
                    }
                    else
                    {
                        curGLTotal = 0;
                    }
                }

                // TODO Get_Fields: Field [PrinOwedTotal] not found!! (maybe it is an alias?)
                // TODO Get_Fields: Field [PrinPaidTotal] not found!! (maybe it is an alias?)
                fldCL.Text = Strings.Format((rsInfo.Get_Fields("PrinOwedTotal") - rsInfo.Get_Fields("PrinPaidTotal")),
                    "#,##0.00");
                // TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
                fldNumberOfBills.Text = Strings.Format(rsInfo.Get_Fields("NumberOfBills"), "#,##0");
                // TODO Get_Fields: Field [PrinOwedTotal] not found!! (maybe it is an alias?)
                // TODO Get_Fields: Field [PrinPaidTotal] not found!! (maybe it is an alias?)
                curCLTotal += (rsInfo.Get_Fields("PrinOwedTotal") - rsInfo.Get_Fields("PrinPaidTotal"));
                // TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
                lngNumberOfBills += rsInfo.Get_Fields("NumberOfBills");
                fldGL.Text = Strings.Format(curGLTotal, "#,##0.00");
                fldDifference.Text = Strings.Format(curCLTotal - curGLTotal, "#,##0.00");
                if (Strings.Format(curCLTotal, "#,##0.00") != Strings.Format(curGLTotal, "#,##0.00"))
                {
                    chkFlag.Checked = true;
                }
                else
                {
                    chkFlag.Checked = false;
                }

                curCLTotal = 0;
                curGLTotal = 0;
            }
            finally
            {
                rsDefaultInfo.Dispose();
                rsTypeInfo.Dispose();
                rsBudInfo.Dispose();
            }
        }

		

		private void srptAR1_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
