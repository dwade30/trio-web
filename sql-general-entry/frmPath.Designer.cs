//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
    /// <summary>
    /// Summary description for frmPath.
    /// </summary>
    partial class frmPath : fecherFoundation.FCForm
    {
        public AxCDWriterXPLib.AxCDWriterXP CDWriterXP1;
        public fecherFoundation.FCButton cmdCancel;
        public fecherFoundation.FCFileListBox File1;
        public fecherFoundation.FCButton cmdSelect;
        public fecherFoundation.FCDirListBox Dir1;
        public fecherFoundation.FCDriveListBox Drive1;
        public fecherFoundation.FCLabel lblTitle;
        private Wisej.Web.MainMenu MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuFile;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuSepar;
        public fecherFoundation.FCToolStripMenuItem mnuExit;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

	 protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
                _InstancePtr = null;
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmPath));
            this.CDWriterXP1 = new AxCDWriterXPLib.AxCDWriterXP();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.File1 = new fecherFoundation.FCFileListBox();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.Dir1 = new fecherFoundation.FCDirListBox();
            this.Drive1 = new fecherFoundation.FCDriveListBox();
            this.lblTitle = new fecherFoundation.FCLabel();

            this.MainMenu1 = new Wisej.Web.MainMenu();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.SuspendLayout();
            //
            // CDWriterXP1
            //
            this.CDWriterXP1.Name = "CDWriterXP1";
            this.CDWriterXP1.Location = new System.Drawing.Point(314, 56);
            this.CDWriterXP1._StockProps = 0;
            this.CDWriterXP1.LicenseCode = "09692067199093982821B";
            //
            // cmdCancel
            //
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.Location = new System.Drawing.Point(299, 311);
            this.cmdCancel.Size = new System.Drawing.Size(92, 26);
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.BackColor = System.Drawing.SystemColors.Control;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            //
            // File1
            //
            this.File1.Name = "File1";
            this.File1.TabIndex = 3;
            this.File1.Location = new System.Drawing.Point(193, 85);
            this.File1.Size = new System.Drawing.Size(198, 204);
            this.File1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            //
            // cmdSelect
            //
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.TabIndex = 2;
            this.cmdSelect.Location = new System.Drawing.Point(12, 311);
            this.cmdSelect.Size = new System.Drawing.Size(92, 26);
            this.cmdSelect.Text = "&Select";
            this.cmdSelect.BackColor = System.Drawing.SystemColors.Control;
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            //
            // Dir1
            //
            this.Dir1.Name = "Dir1";
            this.Dir1.TabIndex = 1;
            this.Dir1.Location = new System.Drawing.Point(7, 85);
            this.Dir1.Size = new System.Drawing.Size(174, 200);
            this.Dir1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            this.Dir1.SelectedIndexChanged += new System.EventHandler(this.Dir1_SelectedIndexChanged);
            //
            // Drive1
            //
            this.Drive1.Name = "Drive1";
            this.Drive1.TabIndex = 0;
            this.Drive1.Location = new System.Drawing.Point(110, 55);
            this.Drive1.Size = new System.Drawing.Size(177, 22);
            this.Drive1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
            //
            // lblTitle
            //
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Location = new System.Drawing.Point(2, 24);
            this.lblTitle.Size = new System.Drawing.Size(395, 28);
            this.lblTitle.Text = "";
            this.lblTitle.BackColor = System.Drawing.SystemColors.Control;
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            //
            // vsElasticLight1
            //

            //this.vsElasticLight1.Location = new System.Drawing.Point(417, 54);
            //this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
            //
            // mnuFile
            //
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] { this.mnuProcess, this.mnuSepar, this.mnuExit });
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "&File";
            //
            // mnuProcess
            //
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "S&ave && Continue";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            //
            // mnuSepar
            //
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            //
            // mnuExit
            //
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "E&xit    (Esc)";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            //
            // MainMenu1
            //
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] { this.mnuFile });
            //
            // frmPath
            //
            this.ClientSize = new System.Drawing.Size(396, 286);
            this.Controls.Add(this.CDWriterXP1);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.File1);
            this.Controls.Add(this.cmdSelect);
            this.Controls.Add(this.Dir1);
            this.Controls.Add(this.Drive1);
            this.Controls.Add(this.lblTitle);
            //this.Controls.Add(this.vsElasticLight1);
            this.Menu = this.MainMenu1;
            this.Name = "frmPath";
            this.BackColor = System.Drawing.SystemColors.Control;
            this.MinimizeBox = true;
            this.MaximizeBox = true;
            this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
            //this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmPath.Icon")));
            this.StartPosition = Wisej.Web.FormStartPosition.WindowsDefaultLocation;
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPath_KeyDown);
            this.Load += new System.EventHandler(this.frmPath_Load);
            this.Text = "Select Backup / Restore Path";
            ((System.ComponentModel.ISupportInitialize)(this.CDWriterXP1)).EndInit();
            this.File1.ResumeLayout(false);
            this.Dir1.ResumeLayout(false);
            this.Drive1.ResumeLayout(false);
            this.lblTitle.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
            this.ResumeLayout(false);
        }
        #endregion
	}
}