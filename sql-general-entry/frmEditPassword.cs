﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmEditPassword.
	/// </summary>
	public partial class frmEditPassword : BaseForm
	{
		public frmEditPassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strReturnPassword;
		bool boolNullsOK;
		string strCurrentPassword;

		private void frmEditPassword_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditPassword_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditPassword properties;
			//frmEditPassword.ScaleWidth	= 3885;
			//frmEditPassword.ScaleHeight	= 2520;
			//frmEditPassword.LinkTopic	= "Form1";
			//frmEditPassword.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public string Init(ref string strOriginalPassword, bool boolAllowNullString = false)
		{
			string Init = "";
			Init = strOriginalPassword;
			strCurrentPassword = Strings.UCase(strOriginalPassword);
			boolNullsOK = boolAllowNullString;
			strReturnPassword = strOriginalPassword;
			txtCurrentPassword.Text = "";
			txtNew1.Text = "";
			txtNew2.Text = "";
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturnPassword;
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			txtCurrentPassword.Text = Strings.UCase(Strings.Trim(txtCurrentPassword.Text));
			if (txtCurrentPassword.Text != strCurrentPassword)
			{
				MessageBox.Show("The password is incorrect", "Incorrect password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			txtNew1.Text = Strings.Trim(txtNew1.Text);
			txtNew2.Text = Strings.Trim(txtNew2.Text);
			if (txtNew1.Text == txtNew2.Text)
			{
				if (!boolNullsOK && txtNew1.Text == string.Empty)
				{
					MessageBox.Show("The new password cannot be blank", "Blank Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				MessageBox.Show("The new password does not match the confirmation of the new password", "Discrepancy", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strReturnPassword = txtNew1.Text;
			this.Unload();
		}
	}
}
