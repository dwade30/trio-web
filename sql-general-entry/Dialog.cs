﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for dlgContactTrio.
	/// </summary>
	public partial class dlgContactTrio : BaseForm
	{
		public dlgContactTrio()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static dlgContactTrio InstancePtr
		{
			get
			{
				return (dlgContactTrio)Sys.GetInstance(typeof(dlgContactTrio));
			}
		}

		protected dlgContactTrio _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolOkayNow;

		private void cmdTakeCode_Click(object sender, System.EventArgs e)
		{
			CheckCode();
			if (!boolOkayNow)
			{
				MessageBox.Show("Invalid Code", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        //FC:FINAL:AM:#1882 - moved code to Load
        //private void dlgContactTrio_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        //{
        //	Keys KeyCode = e.KeyCode;
        //	int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
        //	if (Shift == 1)
        //	{
        //		if (KeyCode == Keys.F9)
        //		{
        //			lblEnterCode.Visible = true;
        //			txtCode.Visible = true;
        //			cmdTakeCode.Visible = true;
        //			OKButton.Visible = false;
        //		}
        //	}
        //}

        private void OKButton_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
		// vbPorter upgrade warning: intChances As short	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool GetCode(short intChances)
		{
			bool GetCode = false;
			GetCode = false;
			Label1.Text = "The current system date is older than the most recent recorded date TRIO was run. You have " + FCConvert.ToString(intChances) + " more times before the problem must be corrected.";
			this.Show(FCForm.FormShowEnum.Modal);
			if (boolOkayNow)
				GetCode = true;
			return GetCode;
		}

		private void CheckCode()
		{
			int lngCode;
			int lngDay;
			int lngMonth;
			int lngYear;
			int XORNumber;
			int lngTemp;
			// Dim clsTemp As New clsDRWrapper
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strMun;
			int lngUpper8bits;
			clsTemp.OpenRecordset("select * from globalvariables", "systemsettings");
			strMun = FCConvert.ToString(clsTemp.Get_Fields_String("muniname"));
			strMun = Strings.UCase(strMun);
			lngUpper8bits = Convert.ToByte(Strings.Left(strMun, 1)[0]);
			lngUpper8bits += (Convert.ToByte(Strings.Mid(strMun, 3)[0]) * 2);
			lngUpper8bits += (Convert.ToByte(Strings.Right(strMun, 1)[0]) * 3);
			lngUpper8bits *= 512;
			// shift left 16 bits
			lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtCode.Text)));
			lngDay = DateTime.Today.Day;
			lngMonth = DateTime.Today.Month;
			lngYear = DateTime.Today.Year - 2000;
			lngTemp = ((lngDay + 13 + lngYear) * 5) * 256;
			// add 13, add last two digits of the year, multiply by 5 then shift it 8 bits
			lngTemp += (Math.Abs(22 - lngYear) * lngMonth);
			lngTemp += lngUpper8bits;
			XORNumber = (113 * 512) + (127 * 256) + 251;
			// upper 8 xored by 127, lower by 251
			lngTemp = lngTemp ^ XORNumber;
			if (lngTemp == lngCode)
			{
				boolOkayNow = true;
				this.Unload();
			}
			else
			{
				boolOkayNow = false;
				this.Unload();
				return;
			}
		}

		private void dlgContactTrio_Load(object sender, System.EventArgs e)
		{
            //Begin Unmaped Properties
            //dlgContactTrio properties;
            //dlgContactTrio.ScaleWidth	= 4605;
            //dlgContactTrio.ScaleHeight	= 2310;
            //dlgContactTrio.LinkTopic	= "Form1";
            //End Unmaped Properties
            //FC:FINAL:AM:#1882 - moved code from KeyDown
            lblEnterCode.Visible = true;
            txtCode.Visible = true;
            cmdTakeCode.Visible = true;
            OKButton.Visible = false;
        }
	}
}
