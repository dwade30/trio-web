﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD6.
	/// </summary>
	public partial class srptBD6 : FCSectionReport
	{
		public srptBD6()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD6";
		}

		public static srptBD6 InstancePtr
		{
			get
			{
				return (srptBD6)Sys.GetInstance(typeof(srptBD6));
			}
		}

		protected srptBD6 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD6	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//clsDRWrapper rsRangeInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT Class, SUM(Amount) As TotalAmount FROM VendorTaxInfo GROUP BY Class ORDER BY Class", "TWBD0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				// Me.Cancel
				//this.Stop();
				this.Cancel();
				return;
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldCode.Text = GetCodeTitle_2(FCConvert.ToInt16(rsInfo.Get_Fields_String("Class")));
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
		}

		private string GetCodeTitle_2(short x)
		{
			return GetCodeTitle(ref x);
		}

		private string GetCodeTitle(ref short x)
		{
			string GetCodeTitle = "";
            using (clsDRWrapper rsCodeTitle = new clsDRWrapper())
            {
                rsCodeTitle.OpenRecordset(
                    "SELECT * FROM ClassCodes WHERE ClassKey = '" + Strings.Trim(Conversion.Str(x)) + "'",
                    "TWBD0000.vb1");
                if (rsCodeTitle.EndOfFile() != true && rsCodeTitle.BeginningOfFile() != true)
                {
                    GetCodeTitle = rsCodeTitle.Get_Fields_String("ClassKey") + " - " +
                                   Strings.Trim(FCConvert.ToString(rsCodeTitle.Get_Fields_String("ClassCode")));
                }
                else
                {
                    GetCodeTitle = Strings.Trim(Conversion.Str(x)) + " - UNKNOWN";
                }
            }

            return GetCodeTitle;
		}

		

		private void srptBD6_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
