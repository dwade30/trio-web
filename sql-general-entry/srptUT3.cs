﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptUT3.
	/// </summary>
	public partial class srptUT3 : FCSectionReport
	{
		public srptUT3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptUT3";
		}

		public static srptUT3 InstancePtr
		{
			get
			{
				return (srptUT3)Sys.GetInstance(typeof(srptUT3));
			}
		}

		protected srptUT3 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptUT3	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int lngSewerTotal;
		int lngWaterTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT DISTINCT Code FROM Category WHERE rTrim(isnull(LongDescription, '')) <> '' OR rTrim(isnull(ShortDescription, '')) <> '' ORDER BY Code", "TWUT0000.vb1");
			blnFirstRecord = true;
			lngWaterTotal = 0;
			lngSewerTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsCategoryInfo = new clsDRWrapper();
			clsDRWrapper rsSummaryInfo = new clsDRWrapper();
			// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
			rsCategoryInfo.OpenRecordset("SELECT * FROM Category WHERE Code = " + rsInfo.Get_Fields("Code"), "TWUT0000.vb1");
			if (rsCategoryInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCategory.Text = Strings.Format(rsInfo.Get_Fields("Code"), "00") + " " + rsCategoryInfo.Get_Fields_String("LongDescription");
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCategory.Text = Strings.Format(rsInfo.Get_Fields("Code"), "00") + " UNKNOWN";
			}
			// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
			rsSummaryInfo.OpenRecordset("SELECT COUNT(WaterCategory) as WaterTotal FROM Master WHERE WaterCategory = " + rsInfo.Get_Fields("Code"), "TWUT0000.vb1");
			if (rsSummaryInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Field [WaterTotal] not found!! (maybe it is an alias?)
				fldWaterAccounts.Text = Strings.Format(rsSummaryInfo.Get_Fields("WaterTotal"), "#,##0");
				// TODO Get_Fields: Field [WaterTotal] not found!! (maybe it is an alias?)
				lngWaterTotal += rsSummaryInfo.Get_Fields_Int32("WaterTotal");
			}
			else
			{
				fldWaterAccounts.Text = Strings.Format(0, "#,##0");
			}
			// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
			rsSummaryInfo.OpenRecordset("SELECT COUNT(SewerCategory) as SewerTotal FROM Master WHERE SewerCategory = " + rsInfo.Get_Fields("Code"), "TWUT0000.vb1");
			if (rsSummaryInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Field [SewerTotal] not found!! (maybe it is an alias?)
				fldSewerAccounts.Text = Strings.Format(rsSummaryInfo.Get_Fields("SewerTotal"), "#,##0");
				// TODO Get_Fields: Field [SewerTotal] not found!! (maybe it is an alias?)
				lngSewerTotal += rsSummaryInfo.Get_Fields_Int32("SewerTotal");
			}
			else
			{
				fldSewerAccounts.Text = Strings.Format(0, "#,##0");
			}
			rsSummaryInfo.Dispose();
			rsCategoryInfo.Dispose();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldSewerAccountsTotal.Text = Strings.Format(lngSewerTotal, "#,##0");
			fldWaterAccountsTotal.Text = Strings.Format(lngWaterTotal, "#,##0");
		}

		
	}
}
