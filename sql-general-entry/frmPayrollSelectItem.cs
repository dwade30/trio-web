﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmPayrollSelectItem.
	/// </summary>
	public partial class frmPayrollSelectItem : BaseForm
	{
		public frmPayrollSelectItem()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPayrollSelectItem InstancePtr
		{
			get
			{
				return (frmPayrollSelectItem)Sys.GetInstance(typeof(frmPayrollSelectItem));
			}
		}

		protected frmPayrollSelectItem _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		string strReturn;
		const int CNSTGRIDCOLDISPLAY = 0;
		const int CNSTGRIDCOLVALUE = 1;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPT = 4;
		// vbPorter upgrade warning: intType As short	OnWrite(string)
		public string Init(short intType)
		{
			string Init = "";
			Init = "";
			strReturn = "";
			if (!LoadGrid(ref intType))
			{
				return Init;
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private bool LoadGrid(ref short intType)
		{
			bool LoadGrid = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngRow;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				Grid.ColAlignment(CNSTGRIDCOLDISPLAY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				LoadGrid = false;
				Grid.Rows = 1;
				Grid.ColHidden(CNSTGRIDCOLVALUE, true);
				switch (intType)
				{
					case CNSTPYTYPEGROUP:
						{
							strSQL = "select GROUPID FROM TBLEMPLOYEEmaster where isnull(groupid , '') <> '' group by groupid order by groupid";
							Grid.TextMatrix(0, CNSTGRIDCOLDISPLAY, "Group");
							this.Text = "Group";
							break;
						}
					case CNSTPYTYPEIND:
						{
							Grid.TextMatrix(0, CNSTGRIDCOLDISPLAY, "Employee");
							strSQL = "select * from tblemployeemaster order by employeenumber";
							this.Text = "Employee";
							break;
						}
					case CNSTPYTYPESEQ:
						{
							Grid.TextMatrix(0, CNSTGRIDCOLDISPLAY, "Sequence");
							this.Text = "Sequence";
							strSQL = "select seqnumber from tblemployeemaster where seqnumber > 0 group by seqnumber order by seqnumber";
							break;
						}
					case CNSTPYTYPEDEPTDIV:
						{
							Grid.TextMatrix(0, CNSTGRIDCOLDISPLAY, "Department-Division");
							this.Text = "Department-Division";
							strSQL = "select deptdiv from tblemployeemaster where isnull(deptdiv ,'') <> '' group by deptdiv order by deptdiv";
							break;
						}
					case CNSTPYTYPEDEPT:
						{
							Grid.TextMatrix(0, CNSTGRIDCOLDISPLAY, "Department");
							this.Text = "Department";
							strSQL = "select department from tblemployeemaster where isnull(department,'') <> '' group by department order by department";
							break;
						}
				}
				//end switch
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (rsLoad.EndOfFile())
				{
					return LoadGrid;
				}
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					switch (intType)
					{
						case CNSTPYTYPEGROUP:
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLVALUE, FCConvert.ToString(rsLoad.Get_Fields_Int32("groupid")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLDISPLAY, FCConvert.ToString(rsLoad.Get_Fields_Int32("groupid")));
								break;
							}
						case CNSTPYTYPEIND:
							{
								// TODO Get_Fields: Field [employeenumber] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTGRIDCOLVALUE, FCConvert.ToString(rsLoad.Get_Fields("employeenumber")));
								// TODO Get_Fields: Field [employeenumber] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTGRIDCOLDISPLAY, rsLoad.Get_Fields("employeenumber") + "  " + rsLoad.Get_Fields_String("firstname") + " " + rsLoad.Get_Fields_String("lastname"));
								break;
							}
						case CNSTPYTYPESEQ:
							{
								// TODO Get_Fields: Field [seqnumber] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTGRIDCOLVALUE, FCConvert.ToString(rsLoad.Get_Fields("seqnumber")));
								// TODO Get_Fields: Field [seqnumber] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTGRIDCOLDISPLAY, FCConvert.ToString(rsLoad.Get_Fields("seqnumber")));
								break;
							}
						case CNSTPYTYPEDEPTDIV:
							{
								// TODO Get_Fields: Check the table for the column [deptdiv] and replace with corresponding Get_Field method
								Grid.TextMatrix(lngRow, CNSTGRIDCOLVALUE, FCConvert.ToString(rsLoad.Get_Fields("deptdiv")));
								// TODO Get_Fields: Check the table for the column [deptdiv] and replace with corresponding Get_Field method
								Grid.TextMatrix(lngRow, CNSTGRIDCOLDISPLAY, FCConvert.ToString(rsLoad.Get_Fields("deptdiv")));
								break;
							}
						case CNSTPYTYPEDEPT:
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLVALUE, FCConvert.ToString(rsLoad.Get_Fields_String("Department")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLDISPLAY, FCConvert.ToString(rsLoad.Get_Fields_String("department")));
								break;
							}
					}
					//end switch
					rsLoad.MoveNext();
				}
				LoadGrid = true;
				return LoadGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadGrid;
		}

		private void frmPayrollSelectItem_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPayrollSelectItem_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPayrollSelectItem properties;
			//frmPayrollSelectItem.FillStyle	= 0;
			//frmPayrollSelectItem.ScaleWidth	= 5880;
			//frmPayrollSelectItem.ScaleHeight	= 4275;
			//frmPayrollSelectItem.LinkTopic	= "Form2";
			//frmPayrollSelectItem.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			strReturn = Grid.TextMatrix(Grid.Row, CNSTGRIDCOLVALUE);
			this.Unload();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			strReturn = Grid.TextMatrix(Grid.Row, CNSTGRIDCOLVALUE);
			this.Unload();
		}
	}
}
