﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Authorization;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Enums;
using SharedDataAccess;
using TWGNENTY.Authentication;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	public partial class frmLogin : BaseForm
	{
		private AuthenticationService _authenticationService;
		private ITrioContextFactory _trioContextFactory;
		public frmLogin()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		public frmLogin(ITrioContextFactory passedContextFactory)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();

			_trioContextFactory = passedContextFactory;
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLogin InstancePtr
		{
			get
			{
				return (frmLogin)Sys.GetInstance(typeof(frmLogin));
			}
		}

		protected frmLogin _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private User tUser;
		SettingsInfo s = new SettingsInfo();
		WorkstationGroup[] wsgInfo;
		WorkstationGroup x = new WorkstationGroup();
		string strDataPath;
		private bool boolActivated;

		public User Login(string strCaption, bool boolIsHarrisStaff = false)
        {
			tUser = null;
			lblGroup.Text = strCaption;
			lblHarrisStaff.Visible = boolIsHarrisStaff;
			this.Show(FCForm.FormShowEnum.Modal);
			return tUser;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			//tUser = null;
			//this.Unload();
			Application.Exit();
		}

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:PB: - issue #2743 pressing enter while selecting user or database from droppeddownlist fires login event
            if (cboDataGroup.DroppedDown) return;

            if (txtUser.Text.Trim() == "")
            {
                MessageBox.Show("You didn't enter a user name", "No User", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtPassword.Text.Trim() == "")
            {
                MessageBox.Show("You didn't enter a password", "No Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var clientSettingsContext = _trioContextFactory.GetClientSettingsContext();
            _authenticationService = new AuthenticationService(new UserDataAccess(clientSettingsContext, StaticSettings.gGlobalSettings), _trioContextFactory.GetSystemSettingsContext());

            var result = _authenticationService.GetAuthenticatedUser(txtUser.Text.Trim(), Strings.Trim(txtPassword.Text));
            tUser = result.security;

            switch (result.result)
            {
                case AuthenticationResult.InactiveUser:
                    MessageBox.Show("You cant log in with an inactive user.", "Inactive User", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    break;
                case AuthenticationResult.LockedOut:
                    MessageBox.Show("This user is currently locked out.  You can either wait 10 minutes or have an admin unlock the user.", "Locked Out", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    break;
                case AuthenticationResult.InvalidCredentials:
                    MessageBox.Show("Invalid user / password combination", "Invalid Credentials", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    break;
                case AuthenticationResult.InvalidForSelectedEnvironment:
                    MessageBox.Show("Invalid user for the selected data group", "Invalid User For Selected Data Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                default:
                {
                    PasswordValidationService passwordValidationService = new PasswordValidationService();
                    if (passwordValidationService.IsValidPassword(txtPassword.Text.Trim(),
                                                                  new List<PasswordHistory>()) != PasswordValidationResult.GoodPassword)
                    {
                        MessageBox.Show("Your password does not meet minimum requirements.  You need to update your password before you can continue.", "Change Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (_authenticationService.ChangePasswordNow(tUser))
                        {
                            MessageBox.Show("You have successfully changed your password.", "Password Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("You must create a new password before you can continue", "Password Change Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            tUser = null;
                            return;
                        }
                    }

                    int daysUntilPasswordChange = _authenticationService.DaysUntilPasswordExpired(tUser);
                    if (tUser.Frequency > 0)
                    {
                        if (daysUntilPasswordChange == 0)
                        {
                            MessageBox.Show("Your password has expired.  You need to update your password before you can continue.", "Change Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (_authenticationService.ChangePasswordNow(tUser))
                            {
                                MessageBox.Show("You have successfully changed your password.", "Password Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("You must create a new password before you can continue", "Password Change Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                tUser = null;
                                return;
                            }
                        }
                        else if (daysUntilPasswordChange >= 1 && daysUntilPasswordChange <= 3)
                        {
                            DialogResult varResp;

                            varResp = MessageBox.Show("You have " + daysUntilPasswordChange + " day(s) left before you must change your password.  Would you like to change your password now?", "Change Password", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (varResp == DialogResult.Yes)
                            {
                                if (_authenticationService.ChangePasswordNow(tUser))
                                {
                                    MessageBox.Show("You have successfully changed your password.", "Password Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                    }

                    TWSharedLibrary.Variables.Statics.IntUserID = tUser.Id;
                    TWSharedLibrary.Variables.Statics.UserID = tUser.UserID;
                    TWSharedLibrary.Variables.Statics.UserName = tUser.UserName;

                    SetOwnerVariables();
                    this.Unload();

                    break;
                }
            }
        }

        private static void SetOwnerVariables()
        {
            if (!string.IsNullOrEmpty(TWSharedLibrary.Variables.Statics.UserID))
            {
                Variables.Statics.OwnerType = "User";
                Variables.Statics.OwnerID = TWSharedLibrary.Variables.Statics.IntUserID.ToString();
            }
        }

        private void cboDataGroup_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsLogin = new clsDRWrapper();
			strDataPath = wsgInfo[cboDataGroup.SelectedIndex].GlobalDataDirectory;
			_trioContextFactory.UpdateContextDetails(cboDataGroup.Text);
            //FC:FINAL:AM:#2105 - use the data directory for the reports
            TWSharedLibrary.Variables.Statics.ReportPath = System.IO.Path.Combine(strDataPath, "RPT");
            TWSharedLibrary.Variables.Statics.DataGroup = cboDataGroup.Text;
            FCFileSystem.Statics.UserDataFolder = strDataPath;
            if (Strings.Right(strDataPath, 1) != "\\")
			{
				strDataPath += "\\";
			}
			
			s.GlobalDataDirectory = strDataPath;
			rsLogin.DefaultGroup = "Live";
			if (rsLogin.MegaGroupsCount() > 0)
			{
				rsLogin.DefaultMegaGroup = wsgInfo[cboDataGroup.SelectedIndex].GroupName;
			}
			modAPIsConst.SetCurrentDirectoryWrp(ref strDataPath);
			modGlobalFunctions.LoadSQLConfig();
            rsLogin.DisposeOf();
		}
		
		private void frmLogin_Activated(object sender, System.EventArgs e)
		{
			if (!boolActivated)
			{
				boolActivated = true;
			}
		}

		private void frmLogin_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = FCConvert.ToInt32(e.KeyData) / 0x10000;
			switch (KeyCode)
			{
				//case Keys.F9:
				//	{
				//		if ((e.KeyData & Keys.Shift) == Keys.Shift)
				//		{
				//			if (Strings.UCase(txtPassword.Text) == Strings.UCase(modGlobalConstants.DATABASEPASSWORD))
				//			{
    //                            LoginSuperUser(cboDataGroup.Text);
				//			}
				//		}
				//		break;
				//	}
                case Keys.F8:
                {
                    if ((e.KeyData & Keys.Shift) == Keys.Shift)
                    {
                        if (!string.IsNullOrWhiteSpace(txtPassword.Text) && !string.IsNullOrWhiteSpace(txtUser.Text))
                        {
                            var trioWebApi = new cTRIOWebAPI();
                            var userName = txtUser.Text.Trim();
                            var password = txtPassword.Text;
                            var dataGroup = cboDataGroup.Text;
                            //var authenticateTask = trioWebApi.AuthenticateStaffAsync(userName, password);
                            //if (authenticateTask.Result)

							if (trioWebApi.AuthenticateSuperUser(userName, password))
                            {
                                LoginSuperUser(dataGroup);
                            }
                        }
                    }
                    break;
                }
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					this.Unload();
					break;
				}
			}
		}

        private void LoginSuperUser(string dataGroup)
        {
            tUser = new User();
            tUser.Id = -1;
            tUser.UserID = "SuperUser";
            tUser.UserName = "SuperUser";
            TWSharedLibrary.Variables.Statics.DataGroup = dataGroup;
            TWSharedLibrary.Variables.Statics.IntUserID = tUser.Id;
            TWSharedLibrary.Variables.Statics.UserID = tUser.UserID;
            TWSharedLibrary.Variables.Statics.UserName = tUser.UserName;

			//FC:FINAL:SBE - #4373 - save assistant key to settings. For superUser we don't have a record in Security table, but we can use the UserID (-1) as a key in Settings table
			SetOwnerVariables();
            this.Unload();
        }

        private void frmLogin_Load(object sender, System.EventArgs e)
		{

			int counter;

            lblVersion.Text = Application.ProductVersion;

			cboDataGroup.Clear();
			wsgInfo = s.GetAvailableSetups();
			for (counter = 0; counter <= Information.UBound(wsgInfo, 1); counter++)
			{
				cboDataGroup.AddItem(wsgInfo[counter].GroupName);
			}
			if (cboDataGroup.Items.Count > 0)
				cboDataGroup.SelectedIndex = 0;
		}
	}
}
