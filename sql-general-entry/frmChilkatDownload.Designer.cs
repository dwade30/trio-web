﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmChilkatDownload.
	/// </summary>
	partial class frmChilkatDownload : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbNew;
		public fecherFoundation.FCLabel lblNew;
		public fecherFoundation.FCButton cmdDownload;
		public fecherFoundation.FCFrame fraSpecificPrograms;
		public fecherFoundation.FCGrid vsUpdates;
		public fecherFoundation.FCListBox lstMessages;
		public fecherFoundation.FCRichTextBox txtProgramNotes;
		public fecherFoundation.FCGrid vsList;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public Chilkat.Ftp2 ChilkatFtp1;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCLabel lblNotifications;
		public fecherFoundation.FCLine Line1;
		public AbaleZipLibrary.AbaleZipClass AbaleZip1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNotes;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuDownloadUpdates;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbNew = new fecherFoundation.FCComboBox();
			this.lblNew = new fecherFoundation.FCLabel();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmChilkatDownload));
			this.components = new System.ComponentModel.Container();
			this.cmdDownload = new fecherFoundation.FCButton();
			this.fraSpecificPrograms = new fecherFoundation.FCFrame();
			this.vsUpdates = new fecherFoundation.FCGrid();
			this.lstMessages = new fecherFoundation.FCListBox();
			this.txtProgramNotes = new fecherFoundation.FCRichTextBox();
			this.vsList = new fecherFoundation.FCGrid();
			this.ProgressBar1 = new fecherFoundation.FCProgressBar();
			this.ChilkatFtp1 = new Chilkat.Ftp2();
			this.lblPercent = new fecherFoundation.FCLabel();
			this.lblNotifications = new fecherFoundation.FCLabel();
			this.Line1 = new fecherFoundation.FCLine();
			this.AbaleZip1 = new AbaleZipLibrary.AbaleZipClass();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintNotes = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDownloadUpdates = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//
			// cmbNew
			//
			this.cmbNew.Items.Add("All Programs With Updates");
			this.cmbNew.Items.Add("All Programs");
			this.cmbNew.Items.Add("Selected Programs");
			this.cmbNew.Name = "cmbNew";
			this.cmbNew.Location = new System.Drawing.Point(195, 42);
			this.cmbNew.Size = new System.Drawing.Size(120, 40);
			this.cmbNew.Text = "All Programs With Updates";
			this.cmbNew.SelectedIndexChanged += new System.EventHandler(this.cmbNew_SelectedIndexChanged);
			//
			// lblNew
			//
			this.lblNew.Name = "lblNew";
			this.lblNew.Text = "lblNew";
			this.lblNew.Location = new System.Drawing.Point(175, 42);
			this.lblNew.AutoSize = true;
			//
			// cmdDownload
			//
			this.cmdDownload.Name = "cmdDownload";
			this.cmdDownload.TabIndex = 8;
			this.cmdDownload.Location = new System.Drawing.Point(236, 178);
			this.cmdDownload.Size = new System.Drawing.Size(120, 26);
			this.cmdDownload.Text = "Download Updates";
			this.cmdDownload.BackColor = System.Drawing.SystemColors.Control;
			this.cmdDownload.Click += new System.EventHandler(this.cmdDownload_Click);
			//
			// fraSpecificPrograms
			//
			this.fraSpecificPrograms.Controls.Add(this.vsUpdates);
			this.fraSpecificPrograms.Name = "fraSpecificPrograms";
			this.fraSpecificPrograms.TabIndex = 6;
			this.fraSpecificPrograms.Location = new System.Drawing.Point(188, 58);
			this.fraSpecificPrograms.Size = new System.Drawing.Size(217, 117);
			this.fraSpecificPrograms.Text = "";
			this.fraSpecificPrograms.BackColor = System.Drawing.SystemColors.Control;
			//
			// vsUpdates
			//
			this.vsUpdates.Name = "vsUpdates";
			this.vsUpdates.Enabled = true;
			this.vsUpdates.TabIndex = 7;
			this.vsUpdates.Location = new System.Drawing.Point(12, 19);
			this.vsUpdates.Size = new System.Drawing.Size(194, 94);
			this.vsUpdates.Rows = 0;
			this.vsUpdates.Cols = 3;
			this.vsUpdates.FixedRows = 0;
			this.vsUpdates.FixedCols = 0;
			this.vsUpdates.ExtendLastCol = true;
			this.vsUpdates.ExplorerBar = 0;
			this.vsUpdates.TabBehavior = 0;
			this.vsUpdates.Editable = 0;
			this.vsUpdates.FrozenRows = 0;
			this.vsUpdates.FrozenCols = 0;
			this.vsUpdates.Click += new System.EventHandler(this.vsUpdates_ClickEvent);
			this.vsUpdates.KeyPress += new KeyPressEventHandler(this.vsUpdates_KeyPressEvent);
			//
			// lstMessages
			//
			this.lstMessages.Name = "lstMessages";
			this.lstMessages.Visible = false;
			this.lstMessages.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.lstMessages, "FTP Status Messages");
			this.lstMessages.Location = new System.Drawing.Point(100, 258);
			this.lstMessages.Size = new System.Drawing.Size(389, 32);
			this.lstMessages.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			//
			// txtProgramNotes
			//
			this.txtProgramNotes.Name = "txtProgramNotes";
			this.txtProgramNotes.Enabled = true;
			this.txtProgramNotes.TabIndex = 1;
			this.txtProgramNotes.Location = new System.Drawing.Point(0, 328);
			this.txtProgramNotes.Size = new System.Drawing.Size(590, 176);
			this.txtProgramNotes.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsList
			//
			this.vsList.Name = "vsList";
			this.vsList.Enabled = true;
			this.vsList.Visible = false;
			this.vsList.TabIndex = 3;
			this.vsList.Location = new System.Drawing.Point(110, 144);
			this.vsList.Size = new System.Drawing.Size(43, 66);
			this.vsList.Rows = 0;
			this.vsList.Cols = 1;
			this.vsList.FixedRows = 0;
			this.vsList.FixedCols = 0;
			this.vsList.ExtendLastCol = false;
			this.vsList.ExplorerBar = 0;
			this.vsList.TabBehavior = 0;
			this.vsList.Editable = 0;
			this.vsList.FrozenRows = 0;
			this.vsList.FrozenCols = 0;
			//
			// ProgressBar1
			//
			this.ProgressBar1.Name = "ProgressBar1";
			this.ProgressBar1.TabIndex = 9;
			this.ProgressBar1.Location = new System.Drawing.Point(152, 229);
			this.ProgressBar1.Size = new System.Drawing.Size(290, 26);
			this.ProgressBar1.Minimum = 0;
			this.ProgressBar1.Maximum = 100;
			this.ProgressBar1.Step = 1;
			//
			// ChilkatFtp1
			//
			//this.ChilkatFtp1.Location = new System.Drawing.Point(49, 68);
			//
			// lblPercent
			//
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.TabIndex = 11;
			this.lblPercent.Location = new System.Drawing.Point(7, 210);
			this.lblPercent.Size = new System.Drawing.Size(610, 16);
			this.lblPercent.Text = "";
			this.lblPercent.BackColor = System.Drawing.SystemColors.Control;
			this.lblPercent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// lblNotifications
			//
			this.lblNotifications.Name = "lblNotifications";
			this.lblNotifications.TabIndex = 10;
			this.lblNotifications.Location = new System.Drawing.Point(203, 304);
			this.lblNotifications.Size = new System.Drawing.Size(183, 18);
			this.lblNotifications.Text = "Program Notifications";
			this.lblNotifications.BackColor = System.Drawing.SystemColors.Control;
			this.lblNotifications.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// Line1
			//
			this.Line1.Name = "Line1";
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.X1 = 3105;
			this.Line1.Y1 = 4441;
			this.Line1.X2 = 5610;
			this.Line1.Y2 = 4441;
			this.Line1.Location = new System.Drawing.Point(3105, 4441);
			this.Line1.Size = new System.Drawing.Size(2505, 1);
			//
			// AbaleZip1
			//
			//this.AbaleZip1.Name = "AbaleZip1";
			//this.AbaleZip1.Location = new System.Drawing.Point(486, 46);
			this.AbaleZip1.BasePath = "";
			//this.AbaleZip1.CompressionLevel = 6;
			this.AbaleZip1.EncryptionPassword = "";
			this.AbaleZip1.RequiredFileAttributes = 0;
			//this.AbaleZip1.ExcludedFileAttributes = 24;
			this.AbaleZip1.FilesToProcess = "";
			this.AbaleZip1.FilesToExclude = "";
			//this.AbaleZip1.MinDateToProcess = 2;
			//this.AbaleZip1.MaxDateToProcess = 2958465;
			this.AbaleZip1.MinSizeToProcess = 0;
			this.AbaleZip1.MaxSizeToProcess = 0;
			this.AbaleZip1.SplitSize = 0;
			this.AbaleZip1.PreservePaths = true;
			this.AbaleZip1.ProcessSubfolders = false;
			this.AbaleZip1.SkipIfExisting = false;
			this.AbaleZip1.SkipIfNotExisting = false;
			this.AbaleZip1.SkipIfOlderDate = false;
			this.AbaleZip1.SkipIfOlderVersion = false;
			this.AbaleZip1.TempFolder = "";
			this.AbaleZip1.UseTempFile = true;
			this.AbaleZip1.UnzipToFolder = "";
			this.AbaleZip1.ZipFilename = "";
			this.AbaleZip1.SpanMultipleDisks = 0;
			//this.AbaleZip1.ExtraHeaders = 10;
			this.AbaleZip1.ZipOpenedFiles = false;
			this.AbaleZip1.SfxBinaryModule = "";
			this.AbaleZip1.DeleteZippedFiles = false;
			this.AbaleZip1.FirstDiskFreeSpace = 0;
			this.AbaleZip1.MinDiskFreeSpace = 0;
			//this.AbaleZip1.EventsToTrigger = 4194303;
			//this.AbaleZip1.CompressionMethod = 8;
			//
			// vsElasticLight1
			//
			//this.vsElasticLight1.Location = new System.Drawing.Point(596, 42);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintNotes,
				this.mnuSepar1,
				this.mnuDownloadUpdates,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			//
			// mnuPrintNotes
			//
			this.mnuPrintNotes.Name = "mnuPrintNotes";
			this.mnuPrintNotes.Text = "Print Program Notifications";
			this.mnuPrintNotes.Click += new System.EventHandler(this.mnuPrintNotes_Click);
			//
			// mnuSepar1
			//
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Visible = false;
			this.mnuSepar1.Text = "-";
			//
			// mnuDownloadUpdates
			//
			this.mnuDownloadUpdates.Name = "mnuDownloadUpdates";
			this.mnuDownloadUpdates.Enabled = false;
			this.mnuDownloadUpdates.Visible = false;
			this.mnuDownloadUpdates.Text = "Download Updates";
			this.mnuDownloadUpdates.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuDownloadUpdates.Click += new System.EventHandler(this.mnuDownloadUpdates_Click);
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			//
			// frmChilkatDownload
			//
			this.ClientSize = new System.Drawing.Size(627, 522);
			this.Controls.Add(this.cmbNew);
			this.ClientSize = new System.Drawing.Size(627, 522);
			this.Controls.Add(this.lblNew);
			this.Controls.Add(this.cmdDownload);
			this.Controls.Add(this.fraSpecificPrograms);
			this.Controls.Add(this.lstMessages);
			this.Controls.Add(this.txtProgramNotes);
			this.Controls.Add(this.vsList);
			this.Controls.Add(this.ProgressBar1);
			//this.Controls.Add(this.ChilkatFtp1);
			this.Controls.Add(this.lblPercent);
			this.Controls.Add(this.lblNotifications);
			this.Controls.Add(this.Line1);
			//this.Controls.Add(this.AbaleZip1);
			//this.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmChilkatDownload";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmChilkatDownload.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Activated += new System.EventHandler(this.frmChilkatDownload_Activated);
			this.Load += new System.EventHandler(this.frmChilkatDownload_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChilkatDownload_KeyPress);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Text = "Program Update";
			this.vsUpdates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsUpdates)).EndInit();
			this.fraSpecificPrograms.ResumeLayout(false);
			this.txtProgramNotes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtProgramNotes)).EndInit();
			this.vsList.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ChilkatFtp1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.AbaleZip1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
