//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Microsoft.VisualBasic.ApplicationServices;

namespace TWGNENTY
{
	public class mUnzip
	{

		//=========================================================

		// ======================================================================================
		// Name:     mUnzip
		// Author:   Steve McMahon (steve@vbaccelerator.com)
		// Date:     1 December 2000
		// 
		// Requires: Info-ZIP's Unzip32.DLL v5.40, renamed to vbuzip10.dll
		// cUnzip.cls
		// 
		// Copyright � 2000 Steve McMahon for vbAccelerator
		// --------------------------------------------------------------------------------------
		// Visit vbAccelerator - advanced free source code for VB programmers
		// http://vbaccelerator.com
		// --------------------------------------------------------------------------------------
		// 
		// Part of the implementation of cUnzip.cls, a class which gives a
		// simple interface to Info-ZIP's excellent, free unzipping library
		// (Unzip32.DLL).
		// 
		// This sample uses decompression code by the Info-ZIP group.  The
		// original Info-Zip sources are freely available from their website
		// at
		// http://www.cdrcom.com/pubs/infozip/
		// 
		// Please ensure you visit the site and read their free source licensing
		// information and requirements before using their code in your own
		// application.
		// 
		// ======================================================================================


		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref byte lpvDest, ref byte lpvSource, int cbCopy);
		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref byte lpvDest, ref Any lpvSource, int cbCopy);
		private static void CopyMemoryWrp(ref byte lpvDest, ref Any lpvSource, int cbCopy)
		{
			CopyMemory(ref lpvDest, ref lpvSource, cbCopy);
		}

		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref byte lpvDest, ref Any lpvSource, int cbCopy);
		private static void CopyMemoryWrp(ref byte lpvDest, ref Any lpvSource, int cbCopy)
		{
			CopyMemory(ref lpvDest, ref lpvSource, cbCopy);
		}


		// argv

		[StructLayout(LayoutKind.Sequential)]
		private struct UNZIPnames
		{
			public string []s /*?  = new string[1023 + 1] */;
		};

		// Callback large "string" (sic)

		private struct CBChar
		{
			public byte[] ch;
		};

		// Callback small "string" (sic)

		private struct CBCh
		{
			public byte[] ch;
		};

		// DCL structure

		public struct DCLIST
		{
			public int ExtractOnlyNewer; // 1 to extract only newer
			public int SpaceToUnderScore; // 1 to convert spaces to underscore
			public int PromptToOverwrite; // 1 if overwriting prompts required
			// vbPorter upgrade warning: fQuiet As int	OnWrite(EUZMsgLevel)
			public int fQuiet; // 0 = all messages, 1 = few messages, 2 = no messages
			public int ncflag; // write to stdout if 1
			public int ntflag; // test zip file
			public int nvflag; // verbose listing
			public int nUflag; // "update" (extract only newer/new files)
			public int nzflag; // display zip file comment
			public int ndflag; // all args are files/dir to be extracted
			public int noflag; // 1 if always overwrite files
			public int naflag; // 1 to do end-of-line translation
			public int nZIflag; // 1 to get zip info
			public int C_flag; // 1 to be case insensitive
			public int fPrivilege; // zip file name
			public string lpszZipFN; // directory to extract to.
			public string lpszExtractDir;
		};


		[StructLayout(LayoutKind.Sequential)]
		private struct USERFUNCTION
		{
			// Callbacks:
			public int lptrPrnt; // Pointer to application's print routine
			public int lptrSound; // Pointer to application's sound routine.  NULL if app doesn't use sound
			public int lptrReplace; // Pointer to application's replace routine.
			public int lptrPassword; // Pointer to application's password routine.
			public int lptrMessage; // Pointer to application's routine for
			// displaying information about specific files in the archive
			// used for listing the contents of the archive.
			public int lptrService; // callback function designed to be used for allowing the
			// app to process Windows messages, or cancelling the operation
			// as well as giving option of progress.  If this function returns
			// non-zero, it will terminate what it is doing.  It provides the app
			// with the name of the archive member it has just processed, as well
			// as the original size.

			// Values filled in after processing:
			public int lTotalSizeComp; // Value to be filled in for the compressed total size, excluding
			// the archive header and central directory list.
			public int lTotalSize; // Total size of all files in the archive
			public int lCompFactor; // Overall archive compression factor
			public int lNumMembers; // Total number of files in the archive
			public short cchComment; // Flag indicating whether comment in archive.
		};


		[StructLayout(LayoutKind.Sequential)]
		public struct ZIPVERSIONTYPE
		{
			// vbPorter upgrade warning: major As byte	OnRead(int)
			public byte major;
			// vbPorter upgrade warning: minor As byte	OnRead(int)
			public byte minor;
			// vbPorter upgrade warning: patchlevel As byte	OnRead(int)
			public byte patchlevel;
			public byte not_used;
		};


		[StructLayout(LayoutKind.Sequential)]
		public struct UZPVER
		{
			public int structlen; // Length of structure
			public int flag; // 0 is beta, 1 uses zlib
			// vbPorter upgrade warning: betalevel As FixedString	OnWrite(string)	OnRead(string)
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
			public string betalevel; // e.g "g BETA"
			// vbPorter upgrade warning: Date As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
			public string Date; // e.g. "4 Sep 95" (beta) or "4 September 1995"
			// vbPorter upgrade warning: zlib As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
			public string zlib; // e.g. "1.0.5 or NULL"
			public ZIPVERSIONTYPE Unzip;
			public ZIPVERSIONTYPE zipinfo;
			public ZIPVERSIONTYPE os2dll;
			public ZIPVERSIONTYPE windll;
		};


		[DllImport("vbuzip10.dll")]
		private static extern int Wiz_SingleEntryUnzip(int ifnc, ref UNZIPnames ifnv, int xfnc, ref UNZIPnames xfnv, ref DCLIST dcll, ref USERFUNCTION Userf);

		[DllImport("vbuzip10.dll")]
		public static extern void UzpVersion2(ref UZPVER uzpv);

		// Object for callbacks:
		private static cUnzip m_cUnzip;
		private static bool m_bCancel;

		// vbPorter upgrade warning: lPtr As int	OnWrite(void*)
		private static int plAddressOf(int lPtr)
		{
			int plAddressOf = 0;
			// VB Bug workaround fn
			plAddressOf = lPtr;
			return plAddressOf;
		}

		private delegate void Address_UnzipMessageCallBack(int ucsize, int csiz, int cfactor, int mo, int dy, int yr, int hh, int mm, byte c, ref CBCh fname, ref CBCh meth, int crc, byte fCrypt);
		private static Address_UnzipMessageCallBack Ptr_UnzipMessageCallBack = new Address_UnzipMessageCallBack(UnzipMessageCallBack);
		private static void UnzipMessageCallBack(int ucsize, int csiz, int cfactor, int mo, int dy, int yr, int hh, int mm, byte c, ref CBCh fname, ref CBCh meth, int crc, byte fCrypt)
		{
			string sFileName = "";
			string sFolder = "";
			// vbPorter upgrade warning: dDate As DateTime	OnWrite(DateTime, double)
			DateTime dDate;
			string sMethod = "";
			int iPos = 0;

			 /*? On Error Resume Next  */
try {

			// Add to unzip class:

			// Parse:
			sFileName = Encoding.Default.GetString(fname.ch);
			ParseFileFolder(ref sFileName, ref sFolder);
			dDate = DateAndTime.DateSerial(yr, mo, hh);
			dDate = dDate.AddDays(DateAndTime.TimeSerial(hh, mm, 0).ToOADate());
			sMethod = Encoding.Default.GetString(meth.ch);
			iPos = Strings.InStr(sMethod, "\0", CompareConstants.vbBinaryCompare);
			if (iPos>1) {
				sMethod = Strings.Left(sMethod, iPos-1);
			}

			Debug.WriteLine(fCrypt);
			m_cUnzip.DirectoryListAddFile(sFileName, sFolder, dDate, csiz, crc, ((fCrypt & 64)==64), cfactor, sMethod);


} catch { }
		}

		private delegate int Address_UnzipPrintCallback(ref CBChar fname, int X);
		private static Address_UnzipPrintCallback Ptr_UnzipPrintCallback = new Address_UnzipPrintCallback(UnzipPrintCallback);
		private static int UnzipPrintCallback(ref CBChar fname, int X)
		{
			int UnzipPrintCallback = 0;
			byte[] b = null;	// - "AutoDim"

			int iPos;
			string sFIle = "";
			 /*? On Error Resume Next  */
try {

			// Check we've got a message:
			if (X>1 && X<32000) {
				// If so, then get the readable portion of it:
				b = new byte[X + 1];
				CopyMemoryWrp(ref b[0], ref fname, X);
				// Convert to VB string:
				sFIle = Encoding.Default.GetString(b);

				// Fix up backslashes:
				ReplaceSection(ref sFIle, "/", "\\");

				// Tell the caller about it
				m_cUnzip.ProgressReport(sFIle);
			}
			UnzipPrintCallback = 0;
} catch { }
			return UnzipPrintCallback;
		}

		private delegate int Address_UnzipPasswordCallBack(ref CBCh pwd, int X, ref CBCh s2, ref CBCh Name);
		private static Address_UnzipPasswordCallBack Ptr_UnzipPasswordCallBack = new Address_UnzipPasswordCallBack(UnzipPasswordCallBack);
		private static int UnzipPasswordCallBack(ref CBCh pwd, int X, ref CBCh s2, ref CBCh Name)
		{
			int UnzipPasswordCallBack = 0;

			bool bCancel = false;
			string sPassword = "";
			byte[] b = null;
			int lSize;

			 /*? On Error Resume Next  */
try {

			// The default:
			UnzipPasswordCallBack = 1;

			if (m_bCancel) {
				return UnzipPasswordCallBack;
			}

			// Ask for password:
			m_cUnzip.PasswordRequest(ref sPassword, ref bCancel);

			sPassword = Strings.Trim(sPassword);

			// Cancel out if no useful password:
			if (bCancel || sPassword.Length==0) {
				m_bCancel = true;
				return UnzipPasswordCallBack;
			}

			// Put password into return parameter:
			lSize = sPassword.Length;
			if (lSize>254) {
				lSize = 254;
			}
			b = Encoding.Default.GetBytes(sPassword);
			CopyMemory(ref pwd.ch[0], ref b[0], lSize);

			// Ask UnZip to process it:
			UnzipPasswordCallBack = 0;

} catch { }
			return UnzipPasswordCallBack;
		}

		// vbPorter upgrade warning: 'Return' As int	OnWrite(EUZOverWriteResponse)
		private delegate int Address_UnzipReplaceCallback(ref CBChar fname);
		private static Address_UnzipReplaceCallback Ptr_UnzipReplaceCallback = new Address_UnzipReplaceCallback(UnzipReplaceCallback);
		private static int UnzipReplaceCallback(ref CBChar fname)
		{
			int UnzipReplaceCallback = 0;
			cUnzip.EUZOverWriteResponse eResponse;
			int iPos;
			string sFIle;

			 /*? On Error Resume Next  */
try {
			eResponse = cUnzip.EUZOverWriteResponse.euzDoNotOverwrite;

			// Extract the filename:
			sFIle = Encoding.Default.GetString(fname.ch);
			iPos = Strings.InStr(sFIle, "\0", CompareConstants.vbBinaryCompare);
			if (iPos>1) {
				sFIle = Strings.Left(sFIle, iPos-1);
			}

			// No backslashes:
			ReplaceSection(ref sFIle, "/", "\\");

			// Request the overwrite request:
			m_cUnzip.OverwriteRequest(sFIle, ref eResponse);

			// Return it to the zipping lib
			UnzipReplaceCallback = (int)eResponse;

} catch { }
			return UnzipReplaceCallback;
		}
		private delegate int Address_UnZipServiceCallback(ref CBChar mname, int X);
		private static Address_UnZipServiceCallback Ptr_UnZipServiceCallback = new Address_UnZipServiceCallback(UnZipServiceCallback);
		private static int UnZipServiceCallback(ref CBChar mname, int X)
		{
			int UnZipServiceCallback = 0;
			byte[] b = null;	// - "AutoDim"

			int iPos = 0;
			string sInfo = "";
			bool bCancel = false;

			// -- Always Put This In Callback Routines!
			 /*? On Error Resume Next  */
try {

			// Check we've got a message:
			if (X>1 && X<32000) {
				// If so, then get the readable portion of it:
				b = new byte[X + 1];
				CopyMemoryWrp(ref b[0], ref mname, X);
				// Convert to VB string:
				sInfo = Encoding.Default.GetString(b);
				iPos = Strings.InStr(sInfo, "\0", CompareConstants.vbBinaryCompare);
				if (iPos>0) {
					sInfo = Strings.Left(sInfo, iPos-1);
				}
				ReplaceSection(ref sInfo, "\\", "/");
				m_cUnzip.Service(sInfo, ref bCancel);
				if (bCancel) {
					UnZipServiceCallback = 1;
				} else {
					UnZipServiceCallback = 0;
				}
			}

} catch { }
			return UnZipServiceCallback;
		}



		private static void ParseFileFolder(ref string sFileName, ref string sFolder)
		{
			int iPos;
			int iLastPos;

			iPos = Strings.InStr(sFileName, "\0", CompareConstants.vbBinaryCompare);
			if (iPos!=0) {
				sFileName = Strings.Left(sFileName, iPos-1);
			}

			iLastPos = ReplaceSection(ref sFileName, "/", "\\");

			if (iLastPos>1) {
				sFolder = Strings.Left(sFileName, iLastPos-2);
				sFileName = Strings.Mid(sFileName, iLastPos);
			}

		}
		private static int ReplaceSection(ref string sString, string sToReplace, string sReplaceWith)
		{
			int ReplaceSection = 0;
			int iPos = 0;
			int iLastPos;
			iLastPos = 1;
			do {
				iPos = Strings.InStr(iLastPos, sString, "/", CompareConstants.vbBinaryCompare);
				if (iPos>1) {
					fecherFoundation.Strings.MidSet(ref sString, iPos, "\\", 1);
					iLastPos = iPos+1;
				}
			} while (!(iPos==0));
			ReplaceSection = iLastPos;

			return ReplaceSection;
		}

		// Main subroutine
		public static int VBUnzip(ref cUnzip cUnzipObject, ref DCLIST tDCL, ref int iIncCount, ref string[] sInc, ref int iExCount, ref string[] sExc)
		{
			int VBUnzip = 0;
			USERFUNCTION tUser = new USERFUNCTION();
			int lR;
			UNZIPnames tInc = new UNZIPnames();
			UNZIPnames tExc = new UNZIPnames();
			int i;

			try
			{	// On Error GoTo ErrorHandler

				m_cUnzip = cUnzipObject;
				// Set Callback addresses
				tUser.lptrPrnt = plAddressOf(UnzipPrintCallback);
				tUser.lptrSound = 0; // not supported
				tUser.lptrReplace = plAddressOf(UnzipReplaceCallback);
				tUser.lptrPassword = plAddressOf(UnzipPasswordCallBack);
				tUser.lptrMessage = plAddressOf(UnzipMessageCallBack);
				tUser.lptrService = plAddressOf(UnZipServiceCallback);

				// Set files to include/exclude:
				if (iIncCount>0) {
					for(i=1; i<=iIncCount; i++) {
						tInc.s[i-1] = sInc[i];
					} // i
					tInc.s[iIncCount] = "\0";
				} else {
					tInc.s[0] = "\0";
				}
				if (iExCount>0) {
					for(i=1; i<=iExCount; i++) {
						tExc.s[i-1] = sExc[i];
					} // i
					tExc.s[iExCount] = "\0";
				} else {
					tExc.s[0] = "\0";
				}
				m_bCancel = false;
				VBUnzip = Wiz_SingleEntryUnzip(iIncCount, ref tInc, iExCount, ref tExc, ref tDCL, ref tUser);

				// Debug.Print "--------------"
				// Debug.Print MYUSER.cchComment
				// Debug.Print MYUSER.TotalSizeComp
				// Debug.Print MYUSER.TotalSize
				// Debug.Print MYUSER.CompFactor
				// Debug.Print MYUSER.NumMembers
				// Debug.Print "--------------"

				return VBUnzip;

			}
			catch (Exception ex)
            {	// ErrorHandler:
				// vbPorter upgrade warning: sErr As int	OnWrite(string)
				int lErr, sErr;
				lErr = Information.Err(ex).Number; sErr = Convert.ToInt32(Information.Err(ex).Description);
				VBUnzip = -1;
				m_cUnzip = null;
				Information.Err(ex).Raise(lErr, null, sErr, null,null);
				return VBUnzip;

			}
		}

	}
}