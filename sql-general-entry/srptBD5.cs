﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for srptBD5.
	/// </summary>
	public partial class srptBD5 : FCSectionReport
	{
		public srptBD5()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptBD5";
		}

		public static srptBD5 InstancePtr
		{
			get
			{
				return (srptBD5)Sys.GetInstance(typeof(srptBD5));
			}
		}

		protected srptBD5 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBD5	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsRangeInfo = new clsDRWrapper())
            {
                rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'L'", "TWBD0000.vb1");
                if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
                {
                    // TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
                    rsInfo.OpenRecordset(
                        "SELECT Account, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE LedgerAccount >= '" +
                        rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") +
                        "' GROUP BY Account ORDER BY Account", "TWBD0000.vb1");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        // Me.Cancel
                        //this.Stop();
                        this.Cancel();
                        return;
                    }
                }
                else
                {
                    this.Cancel();
                    return;
                }
            }

            blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldAccount.Visible = true;
			fldBegBal.Visible = true;
			fldDebits.Visible = true;
			fldCredits.Visible = true;
			fldBalance.Visible = true;
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rsInfo.Get_Fields("Account"));
			// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
			fldBegBal.Text = Strings.Format((Conversion.Val(rsInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1, "#,##0.00");
			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
			fldDebits.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("PostedDebitsTotal")), "#,##0.00");
			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
			fldCredits.Text = Strings.Format((Conversion.Val(rsInfo.Get_Fields("PostedCreditsTotal")) * -1), "#,##0.00");
			// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
			fldBalance.Text = Strings.Format(((Conversion.Val(rsInfo.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsInfo.Get_Fields("BudgetAdjustmentsTotal"))) * -1) - Conversion.Val(rsInfo.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsInfo.Get_Fields("PostedCreditsTotal")), "#,##0.00");
			// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
			if (((Conversion.Val(rsInfo.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsInfo.Get_Fields("BudgetAdjustmentsTotal"))) * -1) - Conversion.Val(rsInfo.Get_Fields("PostedDebitsTotal")) - Conversion.Val(rsInfo.Get_Fields("PostedCreditsTotal")) == 0)
			{
				fldAccount.Visible = false;
				fldBegBal.Visible = false;
				fldDebits.Visible = false;
				fldCredits.Visible = false;
				fldBalance.Visible = false;
			}
		}

		

		private void srptBD5_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
