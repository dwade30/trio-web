//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;

using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	public class modProductUpdate
	{

		//=========================================================
		public static string UpdDisk;

		public static void UpdateProgramInformation()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			const int curOnErrorGoToLabel_ErrorHandler = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */

				clsDRWrapper rsUpdate = new clsDRWrapper();
				string MuniUC;
				// vbPorter upgrade warning: MuniChk As short --> As int	OnWrite(int, double)
				int MuniChk;
				int DateChk;
				int InMonth;
				int InYear;
				string TempDate;
				int Dig1 = 0;
				string Dig2 = "";
				
				Scripting.File tempFile;
				// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
				DialogResult intReturn;

				FCFileSystem.FileClose(1);
				// UpdDisk = UCase(IIf(GetRegistryKey("UpdateDisk") <> vbNullString, gstrReturn, "Empty"))
				UpdDisk = modGNBas.gstrFilePath;
				if (UpdDisk.Length>0) {
					UpdDisk = modGNBas.gstrFilePath;
				} else {
					UpdDisk = "";
				}

				if (UpdDisk=="Empty") {
					MessageBox.Show("You haven't chosen a drive to update from.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				if ((Strings.Right(UpdDisk, 1)!="/") && (Strings.Right(UpdDisk, 1)!="\\")) UpdDisk += "\\";


				if (!fso.DriveExists(Strings.Left(UpdDisk, 1))) {
					MessageBox.Show("The drive "+UpdDisk+" you have chosen is not found. Please choose another.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				if (!fso.FileExists(UpdDisk+"tsupdate.td1")) {
					MessageBox.Show("File "+UpdDisk+"Tsupdate.td1 not found.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;

				}


				tempFile = fso.GetFile(UpdDisk+"tsupdate.td1");


				if ((int)tempFile.Size<192) {
					intReturn = MessageBox.Show("File size of tsupdate.td1 appears to be too small."+"\n"+"Continue anyway?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intReturn==DialogResult.No) return;
				}

				if (DateTime.Today.Month==1 || DateTime.Today.Month==12) {
					switch (DateTime.Today.Month) {
						
						case 1:
						{
							if (!(((tempFile.DateLastModified.Month==1) && (DateTime.Today.Year==tempFile.DateLastModified.Year)) || ((tempFile.DateLastModified.Month==12) && (DateTime.Today.Year-tempFile.DateLastModified.Year==1)))) {
								MessageBox.Show("The file date is either newer than todays date or is more than a month old."+"\n"+"Please contact TRIO for a new update disk", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
							break;
						}
						case 12:
						{
							if (!(((tempFile.DateLastModified.Month==11) || (tempFile.DateLastModified.Month==12)) && (tempFile.DateLastModified.Year==DateTime.Today.Year))) {
								MessageBox.Show("The file date is either newer than todays date or is more than a month old."+"\n"+"Please contact TRIO for a new update disk", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
							break;
						}
					} //end switch
				} else {
					if (!(((tempFile.DateLastModified.Month==DateTime.Today.Month) || (DateTime.Today.Month-tempFile.DateLastModified.Month==1)) && (tempFile.DateLastModified.Year==DateTime.Today.Year))) {
						MessageBox.Show("The file date is either newer than todays date or is more than a month old."+"\n"+"Please contact TRIO for a new update disk", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}

				tempFile = null;
				FCFileSystem.FileOpen(, UpdDisk+"TsUpdate.td1", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(modTtreupd.UPD));
				FCFileSystem.FileGet(1, ref modTtreupd.UPD, -1 /*? 1 */);
				FCFileSystem.FileGet(1, ref modTtreupd.UP2, -1 /*? 2 */);
				FCFileSystem.FileClose(1);


				MuniUC = Strings.UCase(modGlobalConstants.MuniName);
				MuniChk = Convert.ToByte(Strings.Mid(MuniUC, 1, 1)[0]);
				MuniChk += Convert.ToByte(Strings.Mid(MuniUC, 1, 2)[0]);
				MuniChk -= Convert.ToByte(Strings.Mid(MuniUC, 1, 3)[0]);
				if (MuniChk!=modTtreupd.UPD.UCTLM) {
					MessageBox.Show("Municipal Check Digit Error:"+Convert.ToString(MuniChk)+Convert.ToString(modTtreupd.UPD.UCTLM), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				TempDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				InMonth = (int)Math.Round(Conversion.Val(Strings.Mid(TempDate, 1, 2)));
				InYear = (int)Math.Round(Conversion.Val(Strings.Mid(TempDate, 7, 4)));
				DateChk = InMonth*InYear-369;
				if (DateChk!=modTtreupd.UPD.UCTLD) {
					InMonth -= 1;
					if (InMonth==0) {
						InMonth = 12;
						InYear -= 1;
					}
					DateChk = InMonth*InYear-369;
					if (DateChk!=modTtreupd.UPD.UCTLD) {
						InMonth += 2;
						if (InMonth>12) {
							InMonth -= 12;
							InYear += 1;
						}
						DateChk = InMonth*InYear-369;
						if (DateChk!=modTtreupd.UPD.UCTLD) {
							MessageBox.Show("Date Check Digit Error: "+Convert.ToString(DateChk)+" / "+Convert.ToString(modTtreupd.UPD.UCTLD), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
				}

				MuniChk = (int)Math.Round(Conversion.Val(new string(modTtreupd.UPD.URSYEAR)));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.URSMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UCMYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UCMMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UPPYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UPPMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UBLYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UBLMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UCLYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UCLMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UVRYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UVRMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UCEYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UCEMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UBDYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UBDMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UCRYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UCRMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UPYYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UPYMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UUTYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UPD.UUTMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UPD.UMAXPP));
				MuniChk += Conversion.Val(new string(modTtreupd.UP2.U2MVYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UP2.U2MVMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UP2.U2CKYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UP2.U2CKMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UP2.U2TCYEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UP2.U2TCMONTH));
				MuniChk += Conversion.Val(new string(modTtreupd.UP2.U2E9YEAR));
				MuniChk -= Conversion.Val(new string(modTtreupd.UP2.U2E9MONTH));
				MuniChk += 69;
				if (MuniChk!=modTtreupd.UPD.UCTDTE) {
					MessageBox.Show("Module Check Digit Error:"+Convert.ToString(MuniChk)+Convert.ToString(modTtreupd.UPD.UCTDTE), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				rsUpdate.OpenRecordset("Select * from Modules", "systemsettings");

				if (!rsUpdate.EndOfFile()) {
					rsUpdate.Edit();
					if (Conversion.Val(new string(modTtreupd.UP2.U2MVYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("MV", 1);
						rsUpdate.SetData("MVDate", new string(modTtreupd.UP2.U2MVMONTH)+SetLastDay(modTtreupd.UP2.U2MVMONTH, modTtreupd.UP2.U2MVYEAR)+new string(modTtreupd.UP2.U2MVYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.URSYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("RE", 1);
						rsUpdate.SetData("REDate", new string(modTtreupd.UPD.URSMONTH)+SetLastDay(modTtreupd.UPD.URSMONTH, modTtreupd.UPD.URSYEAR)+new string(modTtreupd.UPD.URSYEAR));

						// USE THE RE DATE FOR REAL ESTATE PICTURES AND WIN SKETCH
						rsUpdate.SetData("RP", 1);
						rsUpdate.SetData("RPDate", new string(modTtreupd.UPD.URSMONTH)+SetLastDay(modTtreupd.UPD.URSMONTH, modTtreupd.UPD.URSYEAR)+new string(modTtreupd.UPD.URSYEAR));
						rsUpdate.SetData("WS", 1);
						rsUpdate.SetData("WSDate", new string(modTtreupd.UPD.URSMONTH)+SetLastDay(modTtreupd.UPD.URSMONTH, modTtreupd.UPD.URSYEAR)+new string(modTtreupd.UPD.URSYEAR));
					}

					 /*? On Error Resume Next  */
					if (Conversion.Val(new string(modTtreupd.UPD.UCMYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("CM", 1);
						rsUpdate.SetData("CMDate", new string(modTtreupd.UPD.UCMMONTH)+SetLastDay(modTtreupd.UPD.UCMMONTH, modTtreupd.UPD.UCMYEAR)+new string(modTtreupd.UPD.UCMYEAR));
					}
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
					if (Conversion.Val(new string(modTtreupd.UPD.UPYYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("PY", 1);
						rsUpdate.SetData("PYDate", new string(modTtreupd.UPD.UPYMONTH)+SetLastDay(modTtreupd.UPD.UPYMONTH, modTtreupd.UPD.UPYYEAR)+new string(modTtreupd.UPD.UPYYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UP2.U2TCYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("TC", 1);
						rsUpdate.SetData("TCDate", new string(modTtreupd.UP2.U2TCMONTH)+SetLastDay(modTtreupd.UP2.U2TCMONTH, modTtreupd.UP2.U2TCYEAR)+new string(modTtreupd.UP2.U2TCYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UP2.U2E9YEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("E9", 1);
						rsUpdate.SetData("E9Date", new string(modTtreupd.UP2.U2E9MONTH)+SetLastDay(modTtreupd.UP2.U2E9MONTH, modTtreupd.UP2.U2E9YEAR)+new string(modTtreupd.UP2.U2E9YEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UP2.U2CKYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("CK", 1);
						rsUpdate.SetData("CKDate", new string(modTtreupd.UP2.U2CKMONTH)+SetLastDay(modTtreupd.UP2.U2CKMONTH, modTtreupd.UP2.U2CKYEAR)+new string(modTtreupd.UP2.U2CKYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UPPYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("PP", 1);
						rsUpdate.SetData("PPDate", new string(modTtreupd.UPD.UPPMONTH)+SetLastDay(modTtreupd.UPD.UPPMONTH, modTtreupd.UPD.UPPYEAR)+new string(modTtreupd.UPD.UPPYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UMAXPP))>=0) {
						switch (Conversion.Val(new string(modTtreupd.UPD.UMAXPP))) {
							
							case 0:
							{
								rsUpdate.Set_Fields("pplevel", 0);

								break;
							}
							case 1:
							{
								rsUpdate.Set_Fields("pplevel", 1);
								break;
							}
							case 2:
							{
								rsUpdate.Set_Fields("pplevel", 2);
								break;
							}
							case 3:
							{
								rsUpdate.Set_Fields("pplevel", 3);
								break;
							}
						} //end switch
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UBLYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("BL", 1);
						rsUpdate.SetData("BLDate", new string(modTtreupd.UPD.UBLMONTH)+SetLastDay(modTtreupd.UPD.UBLMONTH, modTtreupd.UPD.UBLYEAR)+new string(modTtreupd.UPD.UBLYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UCLYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("CL", 1);
						rsUpdate.SetData("CLDate", new string(modTtreupd.UPD.UCLMONTH)+SetLastDay(modTtreupd.UPD.UCLMONTH, modTtreupd.UPD.UCLYEAR)+new string(modTtreupd.UPD.UCLYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UVRYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("VR", 1);
						rsUpdate.SetData("VRDate", new string(modTtreupd.UPD.UVRMONTH)+SetLastDay(modTtreupd.UPD.UVRMONTH, modTtreupd.UPD.UVRYEAR)+new string(modTtreupd.UPD.UVRYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UCEYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("CE", 1);
						rsUpdate.SetData("CEDate", new string(modTtreupd.UPD.UCEMONTH)+SetLastDay(modTtreupd.UPD.UCEMONTH, modTtreupd.UPD.UCEYEAR)+new string(modTtreupd.UPD.UCEYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UBDYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("BD", 1);
						rsUpdate.SetData("BDDate", new string(modTtreupd.UPD.UBDMONTH)+SetLastDay(modTtreupd.UPD.UBDMONTH, modTtreupd.UPD.UBDYEAR)+new string(modTtreupd.UPD.UBDYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UCRYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("CR", 1);
						rsUpdate.SetData("CRDate", new string(modTtreupd.UPD.UCRMONTH)+SetLastDay(modTtreupd.UPD.UCRMONTH, modTtreupd.UPD.UCRYEAR)+new string(modTtreupd.UPD.UCRYEAR));
					}
					if (Conversion.Val(new string(modTtreupd.UPD.UUTYEAR)) >= Convert.ToDouble(Strings.Right(Convert.ToString(DateTime.Today.Year), 4))) {
						rsUpdate.SetData("UT", 1);
						rsUpdate.SetData("UTDate", new string(modTtreupd.UPD.UUTMONTH)+SetLastDay(modTtreupd.UPD.UUTMONTH, modTtreupd.UPD.UUTYEAR)+new string(modTtreupd.UPD.UUTYEAR));
					}
					if (modTtreupd.UPD.UMVRAPIDRENEWAL=="Y" || modTtreupd.UPD.UMVRAPIDRENEWAL=="N") {
						rsUpdate.SetData("MVRR", modTtreupd.UPD.UMVRAPIDRENEWAL);
					}
					if (modTtreupd.UPD.UCRMOSES=="Y" || modTtreupd.UPD.UCRMOSES=="N") {
						rsUpdate.SetData("CRMoses", modTtreupd.UPD.UCRMOSES);
					}

					// new stuff 1/6/03
					if (Conversion.Val(new string(modTtreupd.UP2.U2MAXMV))>9) {
						Dig1 = (int)Math.Round(Conversion.Val(Strings.Mid(new string(modTtreupd.UP2.U2MAXMV), 1, 1)));
						Dig2 = Strings.Mid(new string(modTtreupd.UP2.U2MAXMV), 2, 1);
						if (Dig1==1 || Dig1==3 || Dig1==5) {
							rsUpdate.Set_Fields("rb", true);
						} else {
							rsUpdate.Set_Fields("rb", false);
						}

						if (Conversion.Val(Dig2)>0) {
							rsUpdate.Set_Fields("MV_STATELEVEL", Dig2);
						}
					}


					Dig1 = (int)Math.Round(Conversion.Val(new string(modTtreupd.UP2.U2MAXRB)));
					if (Dig1!=0) {
						if (Dig1>31) {
							rsUpdate.SetData("MV_RBLVL_TRAL", "Y");
							Dig1 -= 32;
						} else {
							rsUpdate.SetData("MV_RBLVL_TRAL", "N");
						}
						if (Dig1>15) {
							rsUpdate.SetData("MV_RBLVL_TKBD", "Y");
							Dig1 -= 16;
						} else {
							rsUpdate.SetData("MV_RBLVL_TKBD", "N");
						}
						if (Dig1>7) {
							rsUpdate.SetData("MV_RBLVL_RCVH", "Y");
							Dig1 -= 8;
						} else {
							rsUpdate.SetData("MV_RBLVL_RCVH", "N");
						}
						if (Dig1>3) {
							rsUpdate.SetData("MV_RBLVL_MTCY", "Y");
							Dig1 -= 4;
						} else {
							rsUpdate.SetData("MV_RBLVL_MTCY", "N");
						}
						if (Dig1>1) {
							rsUpdate.SetData("MV_RBLVL_HVTK", "Y");
							Dig1 -= 2;
						} else {
							rsUpdate.SetData("MV_RBLVL_HVTK", "N");
						}
						if (Dig1>0) {
							rsUpdate.SetData("MV_RBLVL_AUTO", "Y");
						} else {
							rsUpdate.SetData("MV_RBLVL_AUTO", "N");
						}
					}

					if (Strings.Trim(Convert.ToString(modTtreupd.UPD.UMAXACCT))!=string.Empty) {
						rsUpdate.Set_Fields("RE_MaxAccounts", modTtreupd.UPD.UMAXACCT);
					}

					rsUpdate.Update();
				}

				// Call rsUpdate.OpenRecordset("Select * from Wmv", "TWGN0000.vb1")

				// If Not rsUpdate.EndOfFile Then
				// rsUpdate.Edit
				// If Val(UP2.U2MAXMV) > 9 Then
				// Dig1 = Val(Mid$(UP2.U2MAXMV, 1, 1))
				// Dig2 = Mid$(UP2.U2MAXMV, 2, 1)
				// If Dig1 = 1 Or Dig1 = 3 Or Dig1 = 5 Then
				// Call rsUpdate.SetData("WMV_REDBOOK", "Y")
				// Else
				// Call rsUpdate.SetData("WMV_REDBOOK", "N")
				// End If
				// If Dig1 = 2 Or Dig1 = 3 Then
				// Call rsUpdate.SetData("WMV_EXCISETAX", "Y")
				// Else
				// Call rsUpdate.SetData("WMV_EXCISETAX", "N")
				// End If
				// If Dig1 = 4 Or Dig1 = 5 Then
				// Call rsUpdate.SetData("WMV_ELECREG", "Y")
				// Else
				// Call rsUpdate.SetData("WMV_ELECREG", "N")
				// End If
				// If Val(Dig2) > 0 Then
				// Call rsUpdate.SetData("WMV_STATELEVEL", Dig2)
				// End If
				// End If
				// 
				// Dig1 = Val(UP2.U2MAXRB)
				// If Dig1 <> 0 Then
				// If Dig1 > 31 Then
				// Call rsUpdate.SetData("WMV_RBLVL_TRAL", "Y")
				// Dig1 = Dig1 - 32
				// Else
				// Call rsUpdate.SetData("WMV_RBLVL_TRAL", "N")
				// End If
				// If Dig1 > 15 Then
				// Call rsUpdate.SetData("WMV_RBLVL_TKBD", "Y")
				// Dig1 = Dig1 - 16
				// Else
				// Call rsUpdate.SetData("WMV_RBLVL_TKBD", "N")
				// End If
				// If Dig1 > 7 Then
				// Call rsUpdate.SetData("WMV_RBLVL_RCVH", "Y")
				// Dig1 = Dig1 - 8
				// Else
				// Call rsUpdate.SetData("WMV_RBLVL_RCVH", "N")
				// End If
				// If Dig1 > 3 Then
				// Call rsUpdate.SetData("WMV_RBLVL_MTCY", "Y")
				// Dig1 = Dig1 - 4
				// Else
				// Call rsUpdate.SetData("WMV_RBLVL_MTCY", "N")
				// End If
				// If Dig1 > 1 Then
				// Call rsUpdate.SetData("WMV_RBLVL_HVTK", "Y")
				// Dig1 = Dig1 - 2
				// Else
				// Call rsUpdate.SetData("WMV_RBLVL_HVTK", "N")
				// End If
				// If Dig1 > 0 Then
				// Call rsUpdate.SetData("WMV_RBLVL_AUTO", "Y")
				// Else
				// Call rsUpdate.SetData("WMV_RBLVL_AUTO", "N")
				// End If
				// End If
				// rsUpdate.Update
				// End If

				// Call rsUpdate.OpenRecordset("Select * from WRE", "TWGN0000.vb1")
				// With rsUpdate
				// If Not .EndOfFile Then
				// .Edit
				// .Fields("WMaxAcct") = UPD.UMAXACCT
				// 
				// .Update
				// End If
				// End With

				// Call rsUpdate.OpenRecordset("select * from wpp", "twgn0000.vb1")
				// With rsUpdate
				// If Not .EndOfFile Then
				// .Edit
				// Select Case Val(UPD.UMAXPP)
				// Case 0
				// rsUpdate.Fields("maxaccounts") = 0 'unlimited
				// Case 1
				// rsUpdate.Fields("maxaccounts") = 50
				// Case 2
				// rsUpdate.Fields("maxaccounts") = 300
				// Case 3
				// rsUpdate.Fields("maxaccounts") = 1000
				// End Select
				// End If
				// End With

				MessageBox.Show("Update of system information was successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;

			ErrorHandler: ;
				if (Information.Err(ex).Number==71) {
					MessageBox.Show("Disk "+UpdDisk+" is not ready. Set Update Disk.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				} else {
					MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"In UpdateProgramInformation", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}

			}
			catch
			{
				switch(vOnErrorGoToLabel) {
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
				}
			}
		}

		// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(FixedString)
		// vbPorter upgrade warning: intYear As short --> As int	OnWrite(FixedString)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string SetLastDay(int intMonth, int intYear)
		{
			string SetLastDay = "";
			SetLastDay = "/"+Convert.ToString(DateAndTime.DateAdd("d", -1, DateAndTime.DateAdd("m", 1, DateTime.Parse(Convert.ToString(intMonth)+"/01/"+Convert.ToString(intYear)))).Day)+"/";
			return SetLastDay;
		}

		public static void UpdateWindowsProgramInformation()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			const int curOnErrorGoToLabel_ErrorHandler = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				Scripting.File fl;

				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */

				clsDRWrapper rsUpdate = new clsDRWrapper();
				string MuniUC;
				int MuniChk;
				int DateChk;
				int InMonth;
				int InYear;
				string TempDate = "";
				int Dig1;
				string Dig2 = "";
				
				Scripting.File tempFile;
				int intReturn;
				clsDRWrapper rsNew = new clsDRWrapper();
				clsDRWrapper rsOld = new clsDRWrapper();
				clsDRWrapper rsShortNew = new clsDRWrapper();
				clsDRWrapper rsShortOld = new clsDRWrapper();
				string strTemp;

				frmWait.InstancePtr.Unload();
				UpdDisk = modGNBas.gstrFilePath;
				if (UpdDisk.Length>0) {
					UpdDisk = modGNBas.gstrFilePath;
				} else {
					UpdDisk = "";
				}

				if (UpdDisk=="Empty") {
					MessageBox.Show("You haven't chosen a drive to update from.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				if ((Strings.Right(UpdDisk, 1)!="/") && (Strings.Right(UpdDisk, 1)!="\\")) UpdDisk += "\\";
				if (!fso.DriveExists(Strings.Left(UpdDisk, 1))) {
					MessageBox.Show("The drive "+UpdDisk+" you have chosen is not found. Please choose another.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				if (!fso.FileExists(UpdDisk+"TWUPDATE.VB1")) {
					MessageBox.Show("File "+UpdDisk+"TWUPDATE.VB1 not found.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				fso.CopyFile("Twgn0000.vb1", "TWGNUpdateBackup.vb1", true);
				if ((UpdDisk!=string.Empty) && (UpdDisk!=Environment.CurrentDirectory) && (UpdDisk!=Environment.CurrentDirectory+"\\")) {
					fso.CopyFile(UpdDisk+"twupdate.vb1", "twupdate.vb1", true);
				}

				UpdDisk = "";
				fl = fso.GetFile(UpdDisk+"twupdate.vb1");

				fl.Attributes = FileAttributes.Normal;
				fl = null;

				rsNew.MegaGroup = "";
				rsNew.GroupName = "";
				// rsNew.Path = UpdDisk
				// strTemp = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & CurDir & "\twupdate.vb1;User Id=admin;Password=TRIO2001;"
				strTemp = "Driver={Microsoft Access Driver (*.mdb)};Dbq="+Environment.CurrentDirectory+"\\twupdate.vb1;Uid=admin;Pwd=TRIO2001;";
				rsNew.AddConnection(strTemp, "TWUPDATE.VB1", dbConnectionTypes_ODBC);
				rsNew.OpenRecordset("select * from globalvariables", "twupdate.vb1");
				strTemp = Convert.ToString(rsNew.Get_Fields_String("mUNINAME"));

				rsNew.OpenRecordset("Select * from Modules", "TWUPDATE.VB1");

				// MuniUC = CheckTownData(False, False, MuniName)
				MuniUC = Strings.Trim(modCheckDigits.ReturnTownEncryption(ref modGlobalConstants.MuniName));
				if (MuniUC!=Convert.ToString(rsNew.Get_Fields_String("TOWNCHECK"))) {
					if (Strings.InStr(1, Strings.UCase(modGlobalConstants.MuniName), Strings.UCase(strTemp), CompareConstants.vbTextCompare)<1) {
						MessageBox.Show("Municipal Check Digit Error: "+rsNew.Get_Fields_String("TOWNCHECK"), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					} else {

						MuniUC = modCheckDigits.ReturnTownEncryption(ref strTemp);
						if (MuniUC!=Convert.ToString(rsNew.Get_Fields_String("towncheck"))) {
							MessageBox.Show("Municipal Check Digit Error: "+rsNew.Get_Fields_String("TOWNCHECK"), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
				}

				MuniChk = modCheckDigits.CalculateCheckDigits03(true, true, UpdDisk);
				if (MuniChk != Convert.ToInt32(rsNew.Get_Fields_String("CheckDigits"))) {
					if (MuniChk-modCheckDigits.CCD03REMAXAccounts!=rsNew.Get_Fields_String("CheckDigits")) {
						MessageBox.Show("Module Check Digit Error: "+Convert.ToString(MuniChk)+rsNew.Get_Fields_String("CheckDigits"), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}


				rsOld.OpenRecordset("Select * from Modules", "systemsettings");

				rsOld.Edit();
				SetModuleInformation(ref rsNew, ref rsOld, "MV");
				SetModuleInformation(ref rsNew, ref rsOld, "RE");

				// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
				if (Convert.ToBoolean(rsNew.Get_Fields("RE"))) {
					if (Convert.ToBoolean(rsNew.Get_Fields_Boolean("CM"))) {
						rsOld.Set_Fields("CM", true);
						if (Convert.ToString(rsNew.Get_Fields_DateTime("CMDate"))==string.Empty || !Information.IsDate(rsNew.Get_Fields_DateTime("CMDate"))) {
							rsOld.Set_Fields("CMDate", 0);
						} else {
							rsOld.Set_Fields("CMDate", rsNew.Get_Fields_DateTime("CMDate"));
						}
						if (!fecherFoundation.FCUtils.IsNull(rsNew.Get_Fields_Int16("CMLevel"))) {
							rsOld.Set_Fields("CMLevel", Convert.ToString(Conversion.Val(Convert.ToString(rsNew.Get_Fields_Int16("CMLevel")))));
						}
					} else if (!fecherFoundation.FCUtils.IsNull(rsNew.Get_Fields_Boolean("CM"))) {
						rsOld.Set_Fields("CM", false);
						rsOld.Set_Fields("CMDate", 0);
					}

					if (Convert.ToBoolean(rsNew.Get_Fields_Boolean("RH"))) {
						rsOld.Set_Fields("RH", true);
						if (Convert.ToString(rsNew.Get_Fields_DateTime("RhDate"))==string.Empty || !Information.IsDate(rsNew.Get_Fields_DateTime("RhDate"))) {
							rsOld.Set_Fields("RhDate", 0);
						} else {
							rsOld.Set_Fields("RhDate", rsNew.Get_Fields_DateTime("RhDate"));
						}
					} else if (!fecherFoundation.FCUtils.IsNull(rsNew.Get_Fields_Boolean("RH")) && Information.IsDate(rsNew.Get_Fields_DateTime("RhDATE"))) {
						rsOld.Set_Fields("RH", false);
						rsOld.Set_Fields("RHDate", 0);
					}

					if (Convert.ToBoolean(rsNew.Get_Fields_Boolean("SK"))) {
						rsOld.Set_Fields("SK", true);
						if (Convert.ToString(rsNew.Get_Fields_DateTime("SKDate"))==string.Empty || !Information.IsDate(rsNew.Get_Fields_DateTime("SKDate"))) {
							rsOld.Set_Fields("SKDate", 0);
						} else {
							rsOld.Set_Fields("SKDate", rsNew.Get_Fields_DateTime("SKDate"));
						}
					} else if (!fecherFoundation.FCUtils.IsNull(rsNew.Get_Fields_Boolean("SK")) && Information.IsDate(rsNew.Get_Fields_DateTime("SKDATE"))) {
						rsOld.Set_Fields("SK", false);
						rsOld.Set_Fields("SKDate", 0);
					}


					if (!fecherFoundation.FCUtils.IsNull(rsNew.Get_Fields_Int32("RE_MaxAccounts"))) {
						if (!(Convert.ToString(rsNew.Get_Fields_Int32("RE_MaxAccounts"))==string.Empty)) {
							rsOld.Set_Fields("RE_MaxAccounts", Convert.ToString(Conversion.Val(Convert.ToString(rsNew.Get_Fields_Int32("RE_MaxAccounts")))));
						}
					}

					// ElseIf Not IsNull(rsNew.Fields("RE")) Then
					// .Fields("CM") = False
					// .Fields("CMDate") = 0
					// .Fields("RP") = False
					// .Fields("RPDate") = 0
					// .Fields("SK") = False
					// .Fields("SKDate") = 0
					// .Fields("SKLevel") = -1
					// .Fields("RE_MaxAccounts") = 1

				}

				SetModuleInformation(ref rsNew, ref rsOld, "PY");
				if (Convert.ToBoolean(rsNew.Get_Fields_Boolean("PY"))) {
					 /*? On Error Resume Next  */
					if (!fecherFoundation.FCUtils.IsNull(rsNew.Get_Fields_Int16("TimeClockLevel"))) {
						rsOld.Set_Fields("TimeClockLevel", Convert.ToString(Conversion.Val(Convert.ToString(rsNew.Get_Fields_Int16("TimeClockLevel")))));
					}
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				}
				SetModuleInformation(ref rsNew, ref rsOld, "TC");
				SetModuleInformation(ref rsNew, ref rsOld, "E9");
				SetModuleInformation(ref rsNew, ref rsOld, "CK");
				SetModuleInformation(ref rsNew, ref rsOld, "PP");
				SetModuleInformation(ref rsNew, ref rsOld, "BL");
				SetModuleInformation(ref rsNew, ref rsOld, "XF");
				SetModuleInformation(ref rsNew, ref rsOld, "CL");
				SetModuleInformation(ref rsNew, ref rsOld, "VR");
				SetModuleInformation(ref rsNew, ref rsOld, "CE");
				SetModuleInformation(ref rsNew, ref rsOld, "BD");
				SetModuleInformation(ref rsNew, ref rsOld, "CR");
				SetModuleInformation(ref rsNew, ref rsOld, "UT");
				SetModuleInformation(ref rsNew, ref rsOld, "RB");
				SetModuleInformation(ref rsNew, ref rsOld, "HR");
				SetModuleInformation(ref rsNew, ref rsOld, "RH");
				SetModuleInformation(ref rsNew, ref rsOld, "SK");
				SetModuleInformation(ref rsNew, ref rsOld, "MS");
				SetModuleInformation(ref rsNew, ref rsOld, "RR");
				SetModuleInformation(ref rsNew, ref rsOld, "FA");
				SetModuleInformation(ref rsNew, ref rsOld, "TS");
				SetModuleInformation(ref rsNew, ref rsOld, "RX");
				SetModuleInformation(ref rsNew, ref rsOld, "AR");
				SetESPInformation(ref rsNew, ref rsOld);

				if (Convert.ToString(rsNew.Get_Fields_String("MV_STATELEVEL"))!=string.Empty) rsOld.Set_Fields("MV_STATELEVEL", rsNew.Get_Fields_String("MV_STATELEVEL"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_RBLVL_TRAL"))!=string.Empty) rsOld.Set_Fields("MV_RBLVL_TRAL", rsNew.Get_Fields_String("MV_RBLVL_TRAL"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_RBLVL_TKBD"))!=string.Empty) rsOld.Set_Fields("MV_RBLVL_TKBD", rsNew.Get_Fields_String("MV_RBLVL_TKBD"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_RBLVL_RCVH"))!=string.Empty) rsOld.Set_Fields("MV_RBLVL_RCVH", rsNew.Get_Fields_String("MV_RBLVL_RCVH"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_RBLVL_MTCY"))!=string.Empty) rsOld.Set_Fields("MV_RBLVL_MTCY", rsNew.Get_Fields_String("MV_RBLVL_MTCY"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_RBLVL_HVTK"))!=string.Empty) rsOld.Set_Fields("MV_RBLVL_HVTK", rsNew.Get_Fields_String("MV_RBLVL_HVTK"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_RBLVL_AUTO"))!=string.Empty) rsOld.Set_Fields("MV_RBLVL_AUTO", rsNew.Get_Fields_String("MV_RBLVL_AUTO"));
				if (Convert.ToString(rsNew.Get_Fields_String("MV_Rentals"))!=string.Empty) rsOld.Set_Fields("MV_Rentals", rsNew.Get_Fields_String("MV_Rentals"));
				rsOld.Update();

				frmWait.InstancePtr.Unload();
				MessageBox.Show("Update of system information was successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;

			ErrorHandler: ;
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number==71) {
					MessageBox.Show("Disk "+UpdDisk+" is not ready. Set Update Disk.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				} else {
					MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"In UpdateProgramInformation", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}

			}
			catch
			{
				switch(vOnErrorGoToLabel) {
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
				}
			}
		}

		public static void SetESPInformation(ref clsDRWrapper rsCopyNew, ref clsDRWrapper rsCopyOld)
		{
			if (!(rsCopyNew.Fields("ESPService")==string.Empty)) {
				if (rsCopyNew.Fields("ESPService")) {
					rsCopyOld.Set_Fields("ESPService", true);
					if (rsCopyNew.Fields("ESPDate")==string.Empty) {
						rsCopyOld.Set_Fields("ESPDate", 0);
					} else {
						rsCopyOld.Set_Fields("ESPDate", rsCopyNew.Fields("ESPDate"));
					}
				} else if (Information.IsDate(rsCopyNew.Fields("ESPDate"))) {
					rsCopyOld.Set_Fields("ESPService", false);
					rsCopyOld.Set_Fields("ESPDate", 0);
				}
			}
		}

		public static void SetModuleInformation(ref clsDRWrapper rsCopyNew, ref clsDRWrapper rsCopyOld, string strModule)
		{
			if (!((rsCopyNew.Fields(strModule))==string.Empty)) {
				if (rsCopyNew.Fields(strModule)) {
					rsCopyOld.Set_Fields(strModule, true);
					if (rsCopyNew.Fields(strModule+"Date")==string.Empty) {
						rsCopyOld.Set_Fields(strModule+"Date", 0);
					} else {
						rsCopyOld.Set_Fields(strModule+"Date",rsCopyNew.Fields(strModule+"Date"));
					}
					if (Strings.UCase(strModule)!="MS" && Strings.UCase(strModule)!="RR") {
						if (!fecherFoundation.FCUtils.IsNull(rsCopyNew.Fields(strModule+"Level"))) {
							if (rsCopyNew.Fields(strModule+"Level")!=string.Empty) {
								rsCopyOld.Set_Fields(strModule+"Level",Conversion.Val(rsCopyNew.Fields(strModule+"Level")));
							}
						}
					}
					if (Strings.UCase(strModule)=="BL") {
						if (!fecherFoundation.FCUtils.IsNull(rsCopyNew.Fields("RE_MaxAccounts"))) {
							if (!(rsCopyNew.Fields("RE_MaxAccounts")==string.Empty)) {
								rsCopyOld.Set_Fields("RE_MaxAccounts", Conversion.Val(rsCopyNew.Fields("RE_MaxAccounts")));
							}
						}
					}
					// If UCase(strModule) = "TS" Then
					// If Not IsNull(rsCopyNew.Fields("TaxServiceAuto")) Then
					// rsCopyOld.Fields("TaxServiceAuto") = rsCopyNew.Fields("TaxServiceAuto")
					// End If
					// End If
				} else if (Information.IsDate(rsCopyNew.Fields(strModule+"DATE"))) {
					rsCopyOld.Set_Fields(strModule, false);
					rsCopyOld.Set_Fields(strModule+"Date", 0);
					if (Strings.UCase(strModule)!="MS" && Strings.UCase(strModule)!="RR") {
						rsCopyOld.Set_Fields(strModule+"Level", -1);
					}
				}
			}
		}

	}
}
