﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmDeleteUnusedParties.
	/// </summary>
	public partial class frmDeleteUnusedParties : BaseForm
	{
		public frmDeleteUnusedParties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private bool boolStop;

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE:#4048 - execute delete process in the background to be able to access cmdStop
            FCUtils.StartTask(this, () =>
            {
                DeleteUnusedParties();
                FCUtils.UnlockUserInterface();
            });
		}

		private void cmdStop_Click(object sender, System.EventArgs e)
		{
			boolStop = true;
		}

		private void frmDeleteUnusedParties_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void DeleteUnusedParties()
		{
			string strStart;
			string strEnd;
			string strWhere = "";
			string strSQL = "";
			if (MessageBox.Show("This will permanently delete any parties that are not being used." + "\r\n" + "Are you sure you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
			{
				return;
			}
			cmdProcess.Enabled = false;
			lblProgress.Text = "";
			lblDeleted.Text = "";
			prgProgress.Value = 0;
			strStart = Strings.Trim(txtStart.Text);
			strEnd = Strings.Trim(txtEnd.Text);
			if (strStart != "" && strEnd != "")
			{
				if (Strings.CompareString(strStart, ">", strEnd))
				{
					MessageBox.Show("The range you specified is invalid", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			prgProgress.Visible = true;
			lblProgress.Visible = true;
			lblDeleted.Visible = true;
			if (strStart != "" || strEnd != "")
			{
				strWhere = "";
				if (strStart != "")
				{
					if (strEnd != "")
					{
						strWhere += " fullname between '" + strStart + "' and '" + strEnd + "z'";
					}
					else
					{
						strWhere += " fullname >= '" + strStart + "'";
					}
				}
				else
				{
					strWhere += " FullName < '" + strEnd + "z'";
				}
				strSQL = "Select id from partynameview where " + strWhere + " order by id";
			}
			else
			{
				strSQL = "select id from parties order by id";
			}
			boolStop = false;
			cPartyUtil pu = new cPartyUtil();
			try
			{
				// On Error GoTo ErrorHandler
				cPartyController pc = new cPartyController();
				clsDRWrapper rsLoad = new clsDRWrapper();
				// Dim bWait As New frmBusy
				int lngCount;
				int lngChecked;
				int lngRecordCount;
				// bWait.Message = "Deleting Parties"
				// bWait.StartBusy
				// bWait.Top = 1
				// bWait.Show , Me
				lngCount = 0;
				lngChecked = 0;
				rsLoad.OpenRecordset(strSQL, "CentralParties");
				lngRecordCount = rsLoad.RecordCount();
				while (!rsLoad.EndOfFile() && !boolStop)
				{
					lngChecked += 1;
					lblProgress.Text = "Checked " + FCConvert.ToString(lngChecked) + " of " + FCConvert.ToString(lngRecordCount) + " Parties";
					prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(lngChecked) / lngRecordCount) * 100);
					lblProgress.Refresh();
					prgProgress.Refresh();
					//Application.DoEvents();
					if (!pu.IsPartyReferenced(rsLoad.Get_Fields_Int32("id")))
					{
						pc.DeleteParty(rsLoad.Get_Fields_Int32("id"));
						lngCount += 1;
						lblDeleted.Text = "Deleted " + FCConvert.ToString(lngCount);
						lblDeleted.Refresh();
						//Application.DoEvents();
					}
					// bWait.Message = "Checked " & lngChecked & " Parties" & vbNewLine & "Deleted " & lngCount
					rsLoad.MoveNext();
				}
				// bWait.StopBusy
				// Unload bWait
				// Set bWait = Nothing
				MessageBox.Show(FCConvert.ToString(lngCount) + " unused parties deleted", "Parties Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				lblProgress.Text = "";
				lblDeleted.Text = "";
				prgProgress.Value = 0;
				prgProgress.Visible = false;
				lblProgress.Visible = false;
				lblDeleted.Visible = false;
				cmdProcess.Enabled = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				cmdProcess.Enabled = true;
				// bWait.StopBusy
				// Unload bWait
				// Set bWait = Nothing
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE:#4048 - execute delete process in the background to be able to access cmdStop
            FCUtils.StartTask(this, () =>
            {
                DeleteUnusedParties();
                FCUtils.UnlockUserInterface();
            });
        }

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void frmDeleteUnusedParties_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeleteUnusedParties properties;
			//frmDeleteUnusedParties.ScaleWidth	= 6015;
			//frmDeleteUnusedParties.ScaleHeight	= 3375;
			//frmDeleteUnusedParties.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
