﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for MDIParent.
	/// </summary>
	partial class MDIParent : BaseForm
	{
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public Wisej.Web.ColorDialog dlg1;
		public Chilkat.Ftp2 ChilkatFtp1;
		public fecherFoundation.FCPictureBox imgArchive;
		public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.StatusBar StatusBar1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel2;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel3;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel4;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCGrid Grid;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuExportBlueBook;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuFileOptionsMenuColor;
		public fecherFoundation.FCToolStripMenuItem mnuFileOptionsMenuFont;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize8;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize9;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize10;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize11;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize12;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize14;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize16;
		public fecherFoundation.FCToolStripMenuItem mnuOptionsFontSize18;
		public fecherFoundation.FCToolStripMenuItem mdiseparator2;
		public fecherFoundation.FCToolStripMenuItem mnuChangePassword;
		public fecherFoundation.FCToolStripMenuItem mnuFileCentralParties;
		public fecherFoundation.FCToolStripMenuItem mnuEliminateDuplicateParties;
		public fecherFoundation.FCToolStripMenuItem mnuEliminateDuplicatePartiesManualMerge;
		public fecherFoundation.FCToolStripMenuItem mnuEliminateDuplicatePartiesAutoMerge;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteUnusedParties;
		public fecherFoundation.FCToolStripMenuItem Optionsseparator1;
		public fecherFoundation.FCToolStripMenuItem mnuFExit;
		public fecherFoundation.FCToolStripMenuItem mnuCYA;
		public fecherFoundation.FCToolStripMenuItem mnuForms;
		public fecherFoundation.FCToolStripMenuItem mnuPrintForm;
		public fecherFoundation.FCToolStripMenuItem mnuOMaxForms;
		public fecherFoundation.FCToolStripMenuItem mnuHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRedbookHelp;
		public fecherFoundation.FCToolStripMenuItem mnuBudgetaryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCashReceiptsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuClerkHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCodeEnforcementHelp;
		public fecherFoundation.FCToolStripMenuItem mnuEnhanced911Help;
		public fecherFoundation.FCToolStripMenuItem mnuFixedAssetsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuGeneralEntryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuMotorVehicleRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPayrollHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPersonalPropertyHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRealEstateHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxCollectionsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuUtilityBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuVoterRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuHelpSeparator;
		public fecherFoundation.FCToolStripMenuItem mnuHAbout;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuStartWebex;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MDIParent));
			this.components = new System.ComponentModel.Container();
			this.Picture1 = new fecherFoundation.FCPictureBox();
			this.CommonDialog1 = new FCCommonDialog();
			this.dlg1 = new Wisej.Web.ColorDialog();
			this.ChilkatFtp1 = new Chilkat.Ftp2();
			this.imgArchive = new fecherFoundation.FCPictureBox();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.StatusBar1 = new Wisej.Web.StatusBar();
			this.StatusBar1_Panel1 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel2 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel3 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel4 = new Wisej.Web.StatusBarPanel();
			this.ImageList1 = new Wisej.Web.ImageList();
			this.Grid = new fecherFoundation.FCGrid();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExportBlueBook = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileOptionsMenuColor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileOptionsMenuFont = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize8 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize9 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize10 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize11 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize12 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize14 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize16 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptionsFontSize18 = new fecherFoundation.FCToolStripMenuItem();
			this.mdiseparator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuChangePassword = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileCentralParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEliminateDuplicateParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEliminateDuplicatePartiesManualMerge = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEliminateDuplicatePartiesAutoMerge = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteUnusedParties = new fecherFoundation.FCToolStripMenuItem();
			this.Optionsseparator1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCYA = new fecherFoundation.FCToolStripMenuItem();
			this.mnuForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintForm = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOMaxForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRedbookHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBudgetaryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCashReceiptsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClerkHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCodeEnforcementHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEnhanced911Help = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFixedAssetsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGeneralEntryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMotorVehicleRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayrollHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPersonalPropertyHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRealEstateHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxCollectionsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUtilityBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuVoterRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelpSeparator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHAbout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuStartWebex = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//
			// Picture1
			//
			//this.Picture1.Controls.Add(this.ChilkatFtp1);
			this.Picture1.Controls.Add(this.imgArchive);
			this.Picture1.Controls.Add(this.Image1);
			this.Picture1.Name = "Picture1";
			this.Picture1.TabIndex = 2;
			this.Picture1.Location = new System.Drawing.Point(168, 24);
			this.Picture1.Size = new System.Drawing.Size(599, 447);
			this.Picture1.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.Picture1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255)));
			this.Picture1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left));
			//
			// CommonDialog1
			//
			//
			// dlg1
			//
			//
			// ChilkatFtp1
			//
			//this.ChilkatFtp1.Location = new System.Drawing.Point(0, 0);
			//
			// imgArchive
			//
			this.imgArchive.Name = "imgArchive";
			this.imgArchive.Visible = false;
			this.imgArchive.Location = new System.Drawing.Point(21, 183);
			this.imgArchive.Size = new System.Drawing.Size(395, 116);
			this.imgArchive.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.imgArchive.BackColor = System.Drawing.SystemColors.Control;
			this.imgArchive.Image = ((System.Drawing.Image)(resources.GetObject("imgArchive.Image")));
			this.imgArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			//
			// Image1
			//
			this.Image1.Name = "Image1";
			this.Image1.Location = new System.Drawing.Point(0, 4);
			this.Image1.Size = new System.Drawing.Size(432, 418);
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.Image1.BackColor = System.Drawing.SystemColors.Control;
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			//
			// StatusBar1
			//
			this.StatusBar1.Panels.AddRange(new Wisej.Web.StatusBarPanel[] {
				this.StatusBar1_Panel1,
				this.StatusBar1_Panel2,
				this.StatusBar1_Panel3,
				this.StatusBar1_Panel4
			});
			this.StatusBar1.Name = "StatusBar1";
			this.StatusBar1.TabIndex = 0;
			this.StatusBar1.Location = new System.Drawing.Point(0, 471);
			this.StatusBar1.Size = new System.Drawing.Size(599, 19);
			this.StatusBar1.ShowPanels = true;
			this.StatusBar1.SizingGrip = false;
			//this.StatusBar1.Font = new System.Drawing.Font("MS Sans Serif", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Panel1
			//
			this.StatusBar1_Panel1.Text = "TRIO Software Corporation";
			this.StatusBar1_Panel1.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel1.Width = 166;
			this.StatusBar1_Panel1.MinWidth = 142;
			//
			// Panel2
			//
			this.StatusBar1_Panel2.Text = "";
			this.StatusBar1_Panel2.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel2.BorderStyle = Wisej.Web.StatusBarPanelBorderStyle.None;
			this.StatusBar1_Panel2.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Spring;
			this.StatusBar1_Panel2.Width = 274;
			//
			// Panel3
			//
			this.StatusBar1_Panel3.Text = "";
			this.StatusBar1_Panel3.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel3.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel3.Width = 68;
			this.StatusBar1_Panel3.MinWidth = 67;
			//
			// Panel4
			//
			this.StatusBar1_Panel4.Text = "";
			this.StatusBar1_Panel4.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel4.Width = 67;
			this.StatusBar1_Panel4.MinWidth = 67;
			//
			// ImageList1
			//
			this.ImageList1.ImageSize = new System.Drawing.Size(17, 19);
			//this.ImageList1.ColorDepth = Wisej.Web.ColorDepth.Depth8Bit;
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
			this.ImageList1.Images.SetKeyName(0, "Clear");
			this.ImageList1.Images.SetKeyName(1, "Left");
			this.ImageList1.Images.SetKeyName(2, "Right");
			//
			// Grid
			//
			this.Grid.Name = "Grid";
			this.Grid.Enabled = true;
			this.Grid.TabIndex = 1;
			this.Grid.Location = new System.Drawing.Point(0, 24);
			this.Grid.Size = new System.Drawing.Size(168, 447);
			this.Grid.Rows = 20;
			this.Grid.Cols = 4;
			this.Grid.FixedRows = 1;
			this.Grid.FixedCols = 0;
			this.Grid.ExtendLastCol = false;
			this.Grid.ExplorerBar = 0;
			this.Grid.TabBehavior = 0;
			this.Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			this.Grid.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
			this.Grid.Enter += new System.EventHandler(this.Grid_Enter);
			this.Grid.KeyPress += new KeyPressEventHandler(this.Grid_KeyPressEvent);
			this.Grid.CellMouseDown += new DataGridViewCellMouseEventHandler(this.Grid_MouseDownEvent);
			this.Grid.CellMouseMove += new DataGridViewCellMouseEventHandler(this.Grid_MouseMoveEvent);
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuExportBlueBook,
				this.mnuOptions,
				this.mnuFileCentralParties,
				this.mnuEliminateDuplicateParties,
				this.mnuDeleteUnusedParties,
				this.Optionsseparator1,
				this.mnuFExit,
				this.mnuCYA
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuExportBlueBook
			//
			this.mnuExportBlueBook.Name = "mnuExportBlueBook";
			this.mnuExportBlueBook.Visible = false;
			this.mnuExportBlueBook.Text = "Export Blue Book";
			this.mnuExportBlueBook.Click += new System.EventHandler(this.mnuExportBlueBook_Click);
			//
			// mnuOptions
			//
			this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileOptionsMenuColor,
				this.mnuFileOptionsMenuFont,
				this.mdiseparator2,
				this.mnuChangePassword
			});
			this.mnuOptions.Name = "mnuOptions";
			this.mnuOptions.Text = "Options";
			//
			// mnuFileOptionsMenuColor
			//
			this.mnuFileOptionsMenuColor.Name = "mnuFileOptionsMenuColor";
			this.mnuFileOptionsMenuColor.Text = "Set Menu Color";
			this.mnuFileOptionsMenuColor.Click += new System.EventHandler(this.mnuFileOptionsMenuColor_Click);
			//
			// mnuFileOptionsMenuFont
			//
			this.mnuFileOptionsMenuFont.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuOptionsFontSize8,
				this.mnuOptionsFontSize9,
				this.mnuOptionsFontSize10,
				this.mnuOptionsFontSize11,
				this.mnuOptionsFontSize12,
				this.mnuOptionsFontSize14,
				this.mnuOptionsFontSize16,
				this.mnuOptionsFontSize18
			});
			this.mnuFileOptionsMenuFont.Name = "mnuFileOptionsMenuFont";
			this.mnuFileOptionsMenuFont.Visible = false;
			this.mnuFileOptionsMenuFont.Text = "Set Menu Font";
			//
			// mnuOptionsFontSize8
			//
			this.mnuOptionsFontSize8.Name = "mnuOptionsFontSize8";
			this.mnuOptionsFontSize8.Text = "8";
			this.mnuOptionsFontSize8.Click += new System.EventHandler(this.mnuOptionsFontSize8_Click);
			//
			// mnuOptionsFontSize9
			//
			this.mnuOptionsFontSize9.Name = "mnuOptionsFontSize9";
			this.mnuOptionsFontSize9.Text = "9";
			this.mnuOptionsFontSize9.Click += new System.EventHandler(this.mnuOptionsFontSize9_Click);
			//
			// mnuOptionsFontSize10
			//
			this.mnuOptionsFontSize10.Name = "mnuOptionsFontSize10";
			this.mnuOptionsFontSize10.Text = "10";
			this.mnuOptionsFontSize10.Click += new System.EventHandler(this.mnuOptionsFontSize10_Click);
			//
			// mnuOptionsFontSize11
			//
			this.mnuOptionsFontSize11.Name = "mnuOptionsFontSize11";
			this.mnuOptionsFontSize11.Text = "11";
			this.mnuOptionsFontSize11.Click += new System.EventHandler(this.mnuOptionsFontSize11_Click);
			//
			// mnuOptionsFontSize12
			//
			this.mnuOptionsFontSize12.Name = "mnuOptionsFontSize12";
			this.mnuOptionsFontSize12.Text = "12";
			this.mnuOptionsFontSize12.Click += new System.EventHandler(this.mnuOptionsFontSize12_Click);
			//
			// mnuOptionsFontSize14
			//
			this.mnuOptionsFontSize14.Name = "mnuOptionsFontSize14";
			this.mnuOptionsFontSize14.Text = "14";
			this.mnuOptionsFontSize14.Click += new System.EventHandler(this.mnuOptionsFontSize14_Click);
			//
			// mnuOptionsFontSize16
			//
			this.mnuOptionsFontSize16.Name = "mnuOptionsFontSize16";
			this.mnuOptionsFontSize16.Text = "16";
			this.mnuOptionsFontSize16.Click += new System.EventHandler(this.mnuOptionsFontSize16_Click);
			//
			// mnuOptionsFontSize18
			//
			this.mnuOptionsFontSize18.Name = "mnuOptionsFontSize18";
			this.mnuOptionsFontSize18.Text = "18";
			this.mnuOptionsFontSize18.Click += new System.EventHandler(this.mnuOptionsFontSize18_Click);
			//
			// mdiseparator2
			//
			this.mdiseparator2.Name = "mdiseparator2";
			this.mdiseparator2.Text = "-";
			//
			// mnuChangePassword
			//
			this.mnuChangePassword.Name = "mnuChangePassword";
			this.mnuChangePassword.Text = "Change User Password";
			this.mnuChangePassword.Click += new System.EventHandler(this.mnuChangePassword_Click);
			//
			// mnuFileCentralParties
			//
			this.mnuFileCentralParties.Name = "mnuFileCentralParties";
			this.mnuFileCentralParties.Text = "Central Parties";
			this.mnuFileCentralParties.Click += new System.EventHandler(this.mnuFileCentralParties_Click);
			//
			// mnuEliminateDuplicateParties
			//
			this.mnuEliminateDuplicateParties.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuEliminateDuplicatePartiesManualMerge,
				this.mnuEliminateDuplicatePartiesAutoMerge
			});
			this.mnuEliminateDuplicateParties.Name = "mnuEliminateDuplicateParties";
			this.mnuEliminateDuplicateParties.Text = "Eliminate Duplicate Parties";
			//
			// mnuEliminateDuplicatePartiesManualMerge
			//
			this.mnuEliminateDuplicatePartiesManualMerge.Name = "mnuEliminateDuplicatePartiesManualMerge";
			this.mnuEliminateDuplicatePartiesManualMerge.Text = "Manual Merge";
			this.mnuEliminateDuplicatePartiesManualMerge.Click += new System.EventHandler(this.mnuEliminateDuplicatePartiesManualMerge_Click);
			//
			// mnuEliminateDuplicatePartiesAutoMerge
			//
			this.mnuEliminateDuplicatePartiesAutoMerge.Name = "mnuEliminateDuplicatePartiesAutoMerge";
			this.mnuEliminateDuplicatePartiesAutoMerge.Text = "Automatic Merge";
			this.mnuEliminateDuplicatePartiesAutoMerge.Click += new System.EventHandler(this.mnuEliminateDuplicatePartiesAutoMerge_Click);
			//
			// mnuDeleteUnusedParties
			//
			this.mnuDeleteUnusedParties.Name = "mnuDeleteUnusedParties";
			this.mnuDeleteUnusedParties.Text = "Delete Unused Parties";
			this.mnuDeleteUnusedParties.Click += new System.EventHandler(this.mnuDeleteUnusedParties_Click);
			//
			// Optionsseparator1
			//
			this.Optionsseparator1.Name = "Optionsseparator1";
			this.Optionsseparator1.Text = "-";
			//
			// mnuFExit
			//
			this.mnuFExit.Name = "mnuFExit";
			this.mnuFExit.Text = "Exit";
			this.mnuFExit.Click += new System.EventHandler(this.mnuFExit_Click);
			//
			// mnuCYA
			//
			this.mnuCYA.Name = "mnuCYA";
			this.mnuCYA.Visible = false;
			this.mnuCYA.Text = "CYA";
			this.mnuCYA.Shortcut = Wisej.Web.Shortcut.CtrlY;
			this.mnuCYA.Click += new System.EventHandler(this.mnuCYA_Click);
			//
			// mnuForms
			//
			this.mnuForms.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintForm,
				this.mnuOMaxForms
			});
			this.mnuForms.Name = "mnuForms";
			this.mnuForms.Text = "Forms";
			//
			// mnuPrintForm
			//
			this.mnuPrintForm.Name = "mnuPrintForm";
			this.mnuPrintForm.Text = "Print Form(s)";
			this.mnuPrintForm.Click += new System.EventHandler(this.mnuPrintForm_Click);
			//
			// mnuOMaxForms
			//
			this.mnuOMaxForms.Name = "mnuOMaxForms";
			this.mnuOMaxForms.Text = "Maximize Forms";
			this.mnuOMaxForms.Click += new System.EventHandler(this.mnuOMaxForms_Click);
			//
			// mnuHelp
			//
			this.mnuHelp.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRedbookHelp,
				this.mnuBudgetaryHelp,
				this.mnuCashReceiptsHelp,
				this.mnuClerkHelp,
				this.mnuCodeEnforcementHelp,
				this.mnuEnhanced911Help,
				this.mnuFixedAssetsHelp,
				this.mnuGeneralEntryHelp,
				this.mnuMotorVehicleRegistrationHelp,
				this.mnuPayrollHelp,
				this.mnuPersonalPropertyHelp,
				this.mnuRealEstateHelp,
				this.mnuTaxBillingHelp,
				this.mnuTaxCollectionsHelp,
				this.mnuUtilityBillingHelp,
				this.mnuVoterRegistrationHelp,
				this.mnuHelpSeparator,
				this.mnuHAbout,
				this.mnuSepar3,
				this.mnuStartWebex
			});
			this.mnuHelp.Name = "mnuHelp";
			this.mnuHelp.Text = "Help";
			//
			// mnuRedbookHelp
			//
			this.mnuRedbookHelp.Name = "mnuRedbookHelp";
			this.mnuRedbookHelp.Text = "Blue Book";
			this.mnuRedbookHelp.Click += new System.EventHandler(this.mnuRedbookHelp_Click);
			//
			// mnuBudgetaryHelp
			//
			this.mnuBudgetaryHelp.Name = "mnuBudgetaryHelp";
			this.mnuBudgetaryHelp.Text = "Budgetary";
			this.mnuBudgetaryHelp.Click += new System.EventHandler(this.mnuBudgetaryHelp_Click);
			//
			// mnuCashReceiptsHelp
			//
			this.mnuCashReceiptsHelp.Name = "mnuCashReceiptsHelp";
			this.mnuCashReceiptsHelp.Text = "Cash Receipts";
			this.mnuCashReceiptsHelp.Click += new System.EventHandler(this.mnuCashReceiptsHelp_Click);
			//
			// mnuClerkHelp
			//
			this.mnuClerkHelp.Name = "mnuClerkHelp";
			this.mnuClerkHelp.Text = "Clerk";
			this.mnuClerkHelp.Click += new System.EventHandler(this.mnuClerkHelp_Click);
			//
			// mnuCodeEnforcementHelp
			//
			this.mnuCodeEnforcementHelp.Name = "mnuCodeEnforcementHelp";
			this.mnuCodeEnforcementHelp.Text = "Code Enforcement";
			this.mnuCodeEnforcementHelp.Click += new System.EventHandler(this.mnuCodeEnforcementHelp_Click);
			//
			// mnuEnhanced911Help
			//
			this.mnuEnhanced911Help.Name = "mnuEnhanced911Help";
			this.mnuEnhanced911Help.Text = "Enhanced 911";
			this.mnuEnhanced911Help.Click += new System.EventHandler(this.mnuEnhanced911Help_Click);
			//
			// mnuFixedAssetsHelp
			//
			this.mnuFixedAssetsHelp.Name = "mnuFixedAssetsHelp";
			this.mnuFixedAssetsHelp.Text = "Fixed Assets";
			this.mnuFixedAssetsHelp.Click += new System.EventHandler(this.mnuFixedAssetsHelp_Click);
			//
			// mnuGeneralEntryHelp
			//
			this.mnuGeneralEntryHelp.Name = "mnuGeneralEntryHelp";
			this.mnuGeneralEntryHelp.Text = "General Entry";
			this.mnuGeneralEntryHelp.Click += new System.EventHandler(this.mnuGeneralEntryHelp_Click);
			//
			// mnuMotorVehicleRegistrationHelp
			//
			this.mnuMotorVehicleRegistrationHelp.Name = "mnuMotorVehicleRegistrationHelp";
			this.mnuMotorVehicleRegistrationHelp.Text = "Motor Vehicle Registration";
			this.mnuMotorVehicleRegistrationHelp.Click += new System.EventHandler(this.mnuMotorVehicleRegistrationHelp_Click);
			//
			// mnuPayrollHelp
			//
			this.mnuPayrollHelp.Name = "mnuPayrollHelp";
			this.mnuPayrollHelp.Text = "Payroll";
			this.mnuPayrollHelp.Click += new System.EventHandler(this.mnuPayrollHelp_Click);
			//
			// mnuPersonalPropertyHelp
			//
			this.mnuPersonalPropertyHelp.Name = "mnuPersonalPropertyHelp";
			this.mnuPersonalPropertyHelp.Text = "Personal Property";
			this.mnuPersonalPropertyHelp.Click += new System.EventHandler(this.mnuPersonalPropertyHelp_Click);
			//
			// mnuRealEstateHelp
			//
			this.mnuRealEstateHelp.Name = "mnuRealEstateHelp";
			this.mnuRealEstateHelp.Text = "Real Estate";
			this.mnuRealEstateHelp.Click += new System.EventHandler(this.mnuRealEstateHelp_Click);
			//
			// mnuTaxBillingHelp
			//
			this.mnuTaxBillingHelp.Name = "mnuTaxBillingHelp";
			this.mnuTaxBillingHelp.Text = "Tax Billing";
			this.mnuTaxBillingHelp.Click += new System.EventHandler(this.mnuTaxBillingHelp_Click);
			//
			// mnuTaxCollectionsHelp
			//
			this.mnuTaxCollectionsHelp.Name = "mnuTaxCollectionsHelp";
			this.mnuTaxCollectionsHelp.Text = "Tax Collections";
			this.mnuTaxCollectionsHelp.Click += new System.EventHandler(this.mnuTaxCollectionsHelp_Click);
			//
			// mnuUtilityBillingHelp
			//
			this.mnuUtilityBillingHelp.Name = "mnuUtilityBillingHelp";
			this.mnuUtilityBillingHelp.Text = "Utility Billing";
			this.mnuUtilityBillingHelp.Click += new System.EventHandler(this.mnuUtilityBillingHelp_Click);
			//
			// mnuVoterRegistrationHelp
			//
			this.mnuVoterRegistrationHelp.Name = "mnuVoterRegistrationHelp";
			this.mnuVoterRegistrationHelp.Text = "Voter Registration";
			this.mnuVoterRegistrationHelp.Click += new System.EventHandler(this.mnuVoterRegistrationHelp_Click);
			//
			// mnuHelpSeparator
			//
			this.mnuHelpSeparator.Name = "mnuHelpSeparator";
			this.mnuHelpSeparator.Text = "-";
			//
			// mnuHAbout
			//
			this.mnuHAbout.Name = "mnuHAbout";
			this.mnuHAbout.Text = "About";
			this.mnuHAbout.Click += new System.EventHandler(this.mnuHAbout_Click);
			//
			// mnuSepar3
			//
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			//
			// mnuStartWebex
			//
			this.mnuStartWebex.Name = "mnuStartWebex";
			this.mnuStartWebex.Text = "Start WebEx Support Session";
			this.mnuStartWebex.Click += new System.EventHandler(this.mnuStartWebex_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile,
				this.mnuForms,
				this.mnuHelp
			});
			//
			// MDIParent
			//
			this.ClientSize = new System.Drawing.Size(599, 466);
			this.Controls.Add(this.Picture1);
			this.Controls.Add(this.StatusBar1);
			this.Controls.Add(this.Grid);
			this.Menu = this.MainMenu1;
			this.IsMdiContainer = true;
			this.Name = "MDIParent";
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("MDIParent.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.WindowState = Wisej.Web.FormWindowState.Maximized;
			this.Activated += new System.EventHandler(this.MDIParent_Activated);
			//this.Load += new System.EventHandler(this.MDIParent_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_QueryUnload);
			this.Text = "TRIO Software Corporation    (General Entry)";
			this.Picture1.ResumeLayout(false);
			this.Grid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
