//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.IO;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmRFC.
	/// </summary>
	partial class frmRFC : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optType;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboModule;
		public fecherFoundation.FCDriveListBox drvDrive;
		public fecherFoundation.FCComboBox cboReleaseNumber;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLine Line1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbType = new fecherFoundation.FCComboBox();
			this.lblType = new fecherFoundation.FCLabel();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmRFC));
			this.components = new System.ComponentModel.Container();
			//this.optType = new System.Collections.Generic.List<fecherFoundation.FCRadioButton>();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cboModule = new fecherFoundation.FCComboBox();
			this.drvDrive = new fecherFoundation.FCDriveListBox();
			this.cboReleaseNumber = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Line1 = new fecherFoundation.FCLine();
			
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// cmbType
			//
			this.cmbType.Items.Add("Current Release");
			this.cmbType.Items.Add("Prior Releases");
			this.cmbType.Items.Add("Compact Disk");
			this.cmbType.Name = "cmbType";
			this.cmbType.Location = new System.Drawing.Point(21, 22);
			this.cmbType.Size = new System.Drawing.Size(120, 40);
			this.cmbType.Text = "Current Release";
			this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
			//
			// lblType
			//
			this.lblType.Name = "lblType";
			this.lblType.Text = "lblType";
			this.lblType.Location = new System.Drawing.Point(1, 22);
			this.lblType.AutoSize = true;
			//((System.ComponentModel.ISupportInitialize)(this.optType)).BeginInit();
			//
			// vsGrid
			//
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.Enabled = true;
			this.vsGrid.TabIndex = 6;
			this.vsGrid.Location = new System.Drawing.Point(25, 209);
			this.vsGrid.Size = new System.Drawing.Size(555, 289);
			this.vsGrid.Rows = 50;
			this.vsGrid.Cols = 10;
			this.vsGrid.FixedRows = 1;
			this.vsGrid.FixedCols = 0;
			this.vsGrid.ExtendLastCol = true;
			this.vsGrid.ExplorerBar = 0;
			this.vsGrid.TabBehavior = 0;
			this.vsGrid.Editable = 0;
			this.vsGrid.FrozenRows = 0;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.Click += new System.EventHandler(this.vsGrid_ClickEvent);
			//
			// Frame1
			//
			this.Frame1.Controls.Add(this.cboModule);
			this.Frame1.Controls.Add(this.cmbType);
			this.Frame1.Controls.Add(this.lblType);

			this.Frame1.Controls.Add(this.drvDrive);
			this.Frame1.Controls.Add(this.cboReleaseNumber);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.Line1);
			this.Frame1.Name = "Frame1";
			this.Frame1.TabIndex = 0;
			this.Frame1.Location = new System.Drawing.Point(24, 42);
			this.Frame1.Size = new System.Drawing.Size(557, 135);
			this.Frame1.Text = "Read RFC's ";
			this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// cboModule
			//
			this.cboModule.Name = "cboModule";
			this.cboModule.TabIndex = 8;
			this.cboModule.Location = new System.Drawing.Point(183, 98);
			this.cboModule.Size = new System.Drawing.Size(248, 22);
			this.cboModule.Text = "";
			this.cboModule.BackColor = System.Drawing.SystemColors.Window;
			this.cboModule.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboModule.SelectedIndexChanged += new System.EventHandler(this.cboModule_SelectedIndexChanged);
			//
			// drvDrive
			//
			this.drvDrive.Name = "drvDrive";
			this.drvDrive.Visible = false;
			this.drvDrive.TabIndex = 5;
			this.drvDrive.Location = new System.Drawing.Point(183, 65);
			this.drvDrive.Size = new System.Drawing.Size(248, 22);
			this.drvDrive.FlatStyle = Wisej.Web.FlatStyle.Flat;
			this.drvDrive.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.drvDrive.SelectedIndexChanged += new System.EventHandler(this.drvDrive_SelectedIndexChanged);
			//
			// cboReleaseNumber
			//
			this.cboReleaseNumber.Name = "cboReleaseNumber";
			this.cboReleaseNumber.Visible = false;
			this.cboReleaseNumber.TabIndex = 4;
			this.cboReleaseNumber.Location = new System.Drawing.Point(183, 38);
			this.cboReleaseNumber.Size = new System.Drawing.Size(248, 22);
			this.cboReleaseNumber.Text = "";
			this.cboReleaseNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cboReleaseNumber.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboReleaseNumber.SelectedIndexChanged += new System.EventHandler(this.cboReleaseNumber_SelectedIndexChanged);
			//
			// Label1
			//
			this.Label1.Name = "Label1";
			this.Label1.TabIndex = 7;
			this.Label1.Location = new System.Drawing.Point(37, 103);
			this.Label1.Size = new System.Drawing.Size(106, 17);
			this.Label1.Text = "Module Name";
			this.Label1.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.Label1.FlatStyle = Wisej.Web.FlatStyle.Flat;
			this.Label1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Line1
			//
			this.Line1.Name = "Line1";
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.X1 = 0;
			this.Line1.Y1 = 1359;
			this.Line1.X2 = 8252;
			this.Line1.Y2 = 1359;
			this.Line1.Location = new System.Drawing.Point(0, 1359);
			this.Line1.Size = new System.Drawing.Size(8252, 1);
			//
			// vsElasticLight1
			//
			
			//this.vsElasticLight1.Location = new System.Drawing.Point(611, 30);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "&File";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "E&xit    (Esc)";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuFile});
			//
			// frmRFC
			//
			this.ClientSize = new System.Drawing.Size(610, 491);
			this.Controls.Add(this.vsGrid);
			this.Controls.Add(this.Frame1);
			//this.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmRFC";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmRFC.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.WindowsDefaultLocation;
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRFC_KeyDown);
			this.Load += new System.EventHandler(this.frmRFC_Load);
			this.Resize += new System.EventHandler(this.frmRFC_Resize);
			this.Text = "View RFC's";
			//((System.ComponentModel.ISupportInitialize)(this.optType)).EndInit();
			this.vsGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			this.cboModule.ResumeLayout(false);
			this.drvDrive.ResumeLayout(false);
			this.cboReleaseNumber.ResumeLayout(false);
			this.Label1.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}