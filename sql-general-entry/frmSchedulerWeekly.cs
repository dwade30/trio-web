﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmSchedulerWeekly.
	/// </summary>
	public partial class frmSchedulerWeekly : BaseForm
	{
		public frmSchedulerWeekly()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSchedulerWeekly InstancePtr
		{
			get
			{
				return (frmSchedulerWeekly)Sys.GetInstance(typeof(frmSchedulerWeekly));
			}
		}

		protected frmSchedulerWeekly _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           10/22/03
		// This form will be used to enter and view tasks scheduled for
		// a certain week
		// ********************************************************
		// vbPorter upgrade warning: datStartDate As Variant --> As DateTime
		DateTime datStartDate, datEndDate;
		int MondayCol;
		int TuesdayCol;
		int WednesdayCol;
		int ThursdayCol;
		int FridayCol;
		int intStartRow;
		int intStartCol;
		string strSwapText = "";
		string strSwapData = "";
		int lngSwapColor;
		int lngTaskKey;
		DateTime datTaskDate;
		bool blnAllowEdit;
		string[,] strNotes = new string[100 + 1, 5 + 1];

		private void frmSchedulerWeekly_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			blnAllowEdit = true;
			if (!modSecurity2.ValidPermissions(this, modSecurity2.CNSTCALENDAR))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Unload();
				return;
			}
			if (lngTaskKey != -1)
			{
				frmTaskInfo.InstancePtr.Init(lngTaskKey, datTaskDate);
				lngTaskKey = -1;
				LoadTasks();
			}
			vsWeek.AutoSize(0, vsWeek.Cols - 1, false, 100);
			this.Refresh();
		}

		private void frmSchedulerWeekly_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSchedulerWeekly properties;
			//frmSchedulerWeekly.FillStyle	= 0;
			//frmSchedulerWeekly.ScaleWidth	= 9045;
			//frmSchedulerWeekly.ScaleHeight	= 7440;
			//frmSchedulerWeekly.LinkTopic	= "Form2";
			//frmSchedulerWeekly.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			DateTime datCurrentDate;
			MondayCol = 0;
			TuesdayCol = 1;
			WednesdayCol = 2;
			ThursdayCol = 3;
			FridayCol = 4;
			vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 0, vsWeek.Rows - 1, vsWeek.Cols - 1, "0");
			for (counter = 0; counter <= 3; counter++)
			{
				vsWeek.ColWidth(counter, FCConvert.ToInt32(vsWeek.WidthOriginal / 5.0));
			}
			datCurrentDate = datStartDate;
			vsWeek.TextMatrix(0, MondayCol, Strings.Format(datCurrentDate, "ddd, MMM d"));
			datCurrentDate = DateAndTime.DateAdd("d", 1, datCurrentDate);
			vsWeek.TextMatrix(0, TuesdayCol, Strings.Format(datCurrentDate, "ddd, MMM d"));
			datCurrentDate = DateAndTime.DateAdd("d", 1, datCurrentDate);
			vsWeek.TextMatrix(0, WednesdayCol, Strings.Format(datCurrentDate, "ddd, MMM d"));
			datCurrentDate = DateAndTime.DateAdd("d", 1, datCurrentDate);
			vsWeek.TextMatrix(0, ThursdayCol, Strings.Format(datCurrentDate, "ddd, MMM d"));
			datCurrentDate = DateAndTime.DateAdd("d", 1, datCurrentDate);
			vsWeek.TextMatrix(0, FridayCol, Strings.Format(datCurrentDate, "ddd, MMM d"));
			vsWeek.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsWeek.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			LoadTasks();
			//Application.DoEvents();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSchedulerWeekly_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSchedulerWeekly_Resize(object sender, System.EventArgs e)
		{
			vsWeek.ColWidth(MondayCol, FCConvert.ToInt32(vsWeek.WidthOriginal * 0.1996615));
			vsWeek.ColWidth(TuesdayCol, FCConvert.ToInt32(vsWeek.WidthOriginal * 0.1996615));
			vsWeek.ColWidth(WednesdayCol, FCConvert.ToInt32(vsWeek.WidthOriginal * 0.1996615));
			vsWeek.ColWidth(ThursdayCol, FCConvert.ToInt32(vsWeek.WidthOriginal * 0.1996615));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		public void Init(ref DateTime datStart, ref DateTime datEnd, int lngKey = -1, DateTime? tempdatTask = null)//= DateTime.FromOADate(0))
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdatTask == null)
			{
				tempdatTask = DateTime.FromOADate(0);
			}
			DateTime datTask = tempdatTask.Value;
			//Application.DoEvents();
			intStartCol = -1;
			intStartRow = -1;
			datStartDate = datStart;
			datEndDate = datEnd;
			lngTaskKey = lngKey;
			datTaskDate = datTask;
			this.Show(FCForm.FormShowEnum.Modal);
			LoadTasks();
		}

		private void LoadTasks()
		{
			clsDRWrapper rsTasks = new clsDRWrapper();
			int counter;
			DateTime datCurrentDate;
			int intRowCounter = 0;
			clsDRWrapper rsOperators = new clsDRWrapper();
			int counter2;
			for (counter = 0; counter <= 100; counter++)
			{
				for (counter2 = 0; counter2 <= 5; counter2++)
				{
					strNotes[counter, counter2] = "";
				}
			}
			vsWeek.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			datCurrentDate = datStartDate;
			// rsOperators.OpenRecordset "SELECT * FROM Operators"
			for (counter = 0; counter <= 4; counter++)
			{
				if (frmScheduler.InstancePtr.chkViewAll.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsTasks.OpenRecordset("SELECT * FROM Tasks WHERE TaskDate = '" + FCConvert.ToString(datCurrentDate) + "' ORDER BY RowOrder", "SystemSettings");
				}
				else
				{
					rsTasks.OpenRecordset("SELECT * FROM Tasks WHERE TaskPerson = '" + modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID() + "' AND TaskDate = '" + FCConvert.ToString(datCurrentDate) + "' ORDER BY RowOrder", "SystemSettings");
				}
				if (rsTasks.EndOfFile() != true && rsTasks.BeginningOfFile() != true)
				{
					intRowCounter = 1;
					do
					{
						if (frmScheduler.InstancePtr.chkViewAll.CheckState == Wisej.Web.CheckState.Checked)
						{
							vsWeek.TextMatrix(intRowCounter, counter, rsTasks.Get_Fields_String("TaskPerson") + " - " + rsTasks.Get_Fields_String("TaskDescription"));
						}
						else
						{
							vsWeek.TextMatrix(intRowCounter, counter, FCConvert.ToString(rsTasks.Get_Fields_String("TaskDescription")));
						}
						strNotes[intRowCounter, counter] = FCConvert.ToString(rsTasks.Get_Fields_String("TaskNotes"));
						vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, intRowCounter, counter, rsTasks.Get_Fields_Int32("ID"));
						intRowCounter += 1;
						rsTasks.MoveNext();
					}
					while (rsTasks.EndOfFile() != true);
				}
				datCurrentDate = DateAndTime.DateAdd("d", 1, datCurrentDate);
			}
			vsWeek.AutoSize(0, vsWeek.Cols - 1, false, 100);
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			clsDRWrapper rsTaskInfo = new clsDRWrapper();
			for (counter = 0; counter <= 4; counter++)
			{
				for (counter2 = 1; counter2 <= vsWeek.Rows - 1; counter2++)
				{
					if (Strings.Trim(vsWeek.TextMatrix(counter2, counter)) != "")
					{
						rsTaskInfo.OpenRecordset("SELECT * FROM Tasks WHERE ID = " + FCConvert.ToString(Conversion.Val(vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, counter2, counter))), "SystemSettings");
						if (rsTaskInfo.EndOfFile() != true && rsTaskInfo.BeginningOfFile() != true)
						{
							rsTaskInfo.Edit();
							rsTaskInfo.Set_Fields("RowOrder", counter2);
							rsTaskInfo.Update();
						}
					}
				}
			}
			mnuProcessQuit_Click();
		}

		private void vsWeek_DblClick(object sender, System.EventArgs e)
		{
			if (blnAllowEdit)
			{
				frmTaskInfo.InstancePtr.Init(FCConvert.ToInt32(vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, vsWeek.Row, vsWeek.Col)), DateAndTime.DateAdd("d", vsWeek.Col, datStartDate));
				LoadTasks();
			}
			else
			{
				MessageBox.Show("You do not have permission to be able to add or edit tasks.", "Invalid Permissions", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsWeek_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				intStartCol = vsWeek.MouseCol;
				intStartRow = vsWeek.MouseRow;
				lngSwapColor = FCConvert.ToInt32(vsWeek.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intStartRow, intStartCol));
				strSwapText = vsWeek.TextMatrix(vsWeek.MouseRow, vsWeek.MouseCol);
				strSwapData = FCConvert.ToString(vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, intStartRow, intStartCol));
			}
		}

		private void vsWeek_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsWeek[e.ColumnIndex, e.RowIndex];
            if (vsWeek.GetFlexRowIndex(e.RowIndex) > 0)
			{
				//ToolTip1.SetToolTip(vsWeek, strNotes[vsWeek.MouseRow, vsWeek.MouseCol]);
				cell.ToolTipText = strNotes[vsWeek.GetFlexRowIndex(e.RowIndex), vsWeek.GetFlexColIndex(e.ColumnIndex)];
			}
			else
			{
                //ToolTip1.SetToolTip(vsWeek, "");
                cell.ToolTipText = "";
			}
		}

		private void vsWeek_MouseUpEvent(object sender, MouseEventArgs e)
		{
			if (intStartCol != -1 && intStartRow != -1)
			{
				if (intStartCol == vsWeek.MouseCol)
				{
					if (vsWeek.MouseRow > 0 && intStartRow != vsWeek.MouseRow)
					{
						vsWeek.TextMatrix(intStartRow, intStartCol, vsWeek.TextMatrix(vsWeek.MouseRow, vsWeek.MouseCol));
						vsWeek.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intStartRow, intStartCol, vsWeek.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsWeek.MouseRow, vsWeek.MouseCol));
						vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, intStartRow, intStartCol, vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, vsWeek.MouseRow, vsWeek.MouseCol));
						vsWeek.TextMatrix(vsWeek.MouseRow, vsWeek.MouseCol, strSwapText);
						vsWeek.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsWeek.MouseRow, vsWeek.MouseCol, lngSwapColor);
						vsWeek.Cell(FCGrid.CellPropertySettings.flexcpData, vsWeek.MouseRow, vsWeek.MouseCol, strSwapData);
					}
					else
					{
						intStartRow = -1;
						intStartCol = -1;
					}
				}
				else
				{
					intStartRow = -1;
					intStartCol = -1;
				}
			}
			vsWeek.AutoSize(0, vsWeek.Cols - 1, false, 100);
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			bool blnAllowChange;
			blnAllowChange = false;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity2.CNSTEDITCALENDARALL:
						{
							if (strPerm == "N")
							{
								// do nothing
							}
							else
							{
								blnAllowChange = true;
							}
							break;
						}
					case modSecurity2.CNSTEDITCALENDAR:
						{
							if (strPerm == "N")
							{
								// do nothing
							}
							else
							{
								blnAllowChange = true;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
			blnAllowEdit = blnAllowChange;
		}
	}
}
