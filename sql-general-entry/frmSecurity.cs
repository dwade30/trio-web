﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess;
using System;
using System.Linq;
using SharedApplication.ClientSettings.Models;
using TWGNENTY.Authentication;
using TWSharedLibrary;
using Wisej.Web;

namespace TWGNENTY
{
    /// <summary>
    /// Summary description for frmSecurity.
    /// </summary>
    public partial class frmSecurity : BaseForm
    {
        private AuthenticationService _authenticationService;
        private UserDataAccess _userDataAccess;
        private int currentUserId = 0;
        public frmSecurity()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmSecurity(AuthenticationService passedAuthenticationService, UserDataAccess passedUserDataAccess)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();

            _authenticationService = passedAuthenticationService;
            _userDataAccess = passedUserDataAccess;
        }

        private void InitializeComponentEx()
        {
            lbl1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lbl1.AddControlArrayElement(lbl1_1, 1);
            lbl1.AddControlArrayElement(lbl1_2, 2);
            lbl1.AddControlArrayElement(lbl1_3, 3);
            lbl1.AddControlArrayElement(lbl1_0, 0);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmSecurity InstancePtr
        {
            get
            {
                return (frmSecurity)Sys.GetInstance(typeof(frmSecurity));
            }
        }

        protected frmSecurity _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        string strSQL = "";
        bool bolUseSecurity;
        int lngID;
        public bool GoodPassword;
        public string NewPassword = string.Empty;
        private clsDRWrapper rsSecurity = new clsDRWrapper();
        string strApplicationName = "";
        bool boolFillingCombo;
        private clsDRWrapper secRS = new clsDRWrapper();
        //FC:FINAL:IPI - #1266 - when modules permission is changed, must refresh the modules status on MainForm
        private bool isSaved = false;

        private void LoadPayrollGrid()
        {
            int intCounter;
            clsDRWrapper rsData = new clsDRWrapper();
            vsPayroll.Rows = 1;
            vsPayroll.ColWidth(0, 1000);
            vsPayroll.ColWidth(1, 2000);
            //vsPayroll.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsPayroll.TextMatrix(0, 0, "Dept/Div");
            vsPayroll.TextMatrix(0, 1, "Password");
            intCounter = 1;
            rsData.OpenRecordset("Select * from tblPayrollDeptDiv Order by DeptDiv", "systemsettings");
            while (!rsData.EndOfFile())
            {
                vsPayroll.Rows += 1;
                // TODO Get_Fields: Check the table for the column [DeptDiv] and replace with corresponding Get_Field method
                vsPayroll.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("DeptDiv")));
                vsPayroll.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Password")));
                rsData.MoveNext();
                intCounter += 1;
            }
            if (vsPayroll.Rows > 1)
            {
                vsPayroll.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, vsPayroll.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            }
            vsPayroll.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
        }

        private void SavePayrollGrid()
        {
            int intCounter;
            clsDRWrapper rsData = new clsDRWrapper();
            fraPayroll.Visible = false;
            rsData.Execute("Delete from tblPayrollDeptDiv", "systemsettings");
            rsData.OpenRecordset("Select * from tblPayrollDeptDiv", "systemsettings");
            for (intCounter = 1; intCounter <= vsPayroll.Rows - 1; intCounter++)
            {
                rsData.AddNew();
                rsData.Set_Fields("DeptDiv", vsPayroll.TextMatrix(intCounter, 0));
                rsData.Set_Fields("Password", vsPayroll.TextMatrix(intCounter, 1));
                rsData.Update();
            }
        }

        private void cmbModule_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            fraPayroll.Visible = false;

            if (boolFillingCombo || cmbModule.SelectedIndex == -1)
                return;
            if (currentUserId == 0)
            {
                MessageBox.Show("You must first choose a user from the list above or save the information for the user you are adding", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                if (Strings.UCase(cmbModule.Text) == "GENERAL ENTRY")
                {
                    strApplicationName = "GE";
                }
                else if (Strings.UCase(cmbModule.Text) == "REAL ESTATE")
                {
                    strApplicationName = "RE";
                }
                else if (Strings.UCase(cmbModule.Text) == "PERSONAL PROPERTY")
                {
                    strApplicationName = "PP";
                }
                else if (Strings.UCase(cmbModule.Text) == "TAX BILLING")
                {
                    strApplicationName = "BL";
                }
                else if (Strings.UCase(cmbModule.Text) == "COLLECTIONS")
                {
                    strApplicationName = "CL";
                }
                else if (Strings.UCase(cmbModule.Text) == "CLERK")
                {
                    strApplicationName = "CK";
                }
                else if (Strings.UCase(cmbModule.Text) == "VOTER REGISTRATION")
                {
                    strApplicationName = "VR";
                }
                else if (Strings.UCase(cmbModule.Text) == "CODE ENFORCEMENT")
                {
                    strApplicationName = "CE";
                }
                else if (Strings.UCase(cmbModule.Text) == "BUDGETARY")
                {
                    strApplicationName = "BD";
                }
                else if (Strings.UCase(cmbModule.Text) == "CASH RECEIPTING")
                {
                    strApplicationName = "CR";
                }
                else if (Strings.UCase(cmbModule.Text) == "PAYROLL")
                {
                    strApplicationName = "PY";
                    LoadPayrollGrid();
                    fraPayroll.Visible = true;
                    fraPayroll.ZOrder(ZOrderConstants.BringToFront);
                }
                else if (Strings.UCase(cmbModule.Text) == "CENTRAL PARTIES")
                {
                    strApplicationName = "CP";
                }
                else if (Strings.UCase(cmbModule.Text) == "UTILITY BILLING")
                {
                    strApplicationName = "UT";
                }
                else if (Strings.UCase(cmbModule.Text) == "MOTOR VEHICLE")
                {
                    strApplicationName = "MV";
                }
                else if (Strings.UCase(cmbModule.Text) == "FIXED ASSETS")
                {
                    strApplicationName = "FA";
                }
                else if (Strings.UCase(cmbModule.Text) == "ACCOUNTS RECEIVABLE")
                {
                    strApplicationName = "AR";
                }

                if (Strings.Trim(txtOPID.Text) != "" && Strings.Trim(txtUserID.Text) != "" && Strings.Trim(txtName.Text) != "")
                {
                    GridProperties();
                    NewSetupGrid(ref strApplicationName);
                    fraSecurity.Visible = true;
                    vs1.TextMatrix(0, 1, FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1)));
                    vs1.TextMatrix(0, 2, FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 2)));
                    vs1.TextMatrix(0, 3, FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 3)));
                    fraInfo.Visible = false;
                }
                else
                {
                    MessageBox.Show("Operator ID, User ID and User Name Must Be Filled Out First.", "Current Data Only", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                mnuFile_Click(null, null);
            }
        }

        private void cmdCancel_Click(object sender, System.EventArgs e)
        {
            fraSecurity.Visible = false;
            fraInfo.Visible = true;
        }

        private void FillModuleCombo()
        {
            boolFillingCombo = true;
            cmbModule.Clear();
            modEntryMenuForm.CheckExpirationDate();
            cmbModule.AddItem("General Entry");
            cmbModule.AddItem("Central Parties");
            if (modEntryMenuForm.Statics.RealEstateAllowed)
            {
                cmbModule.AddItem("Real Estate");
            }
            if (modEntryMenuForm.Statics.PersonalPropertyAllowed)
            {
                cmbModule.AddItem("Personal Property");
            }
            if (modEntryMenuForm.Statics.BillingAllowed)
            {
                cmbModule.AddItem("Tax Billing");
            }
            if (modEntryMenuForm.Statics.CollectionsAllowed)
            {
                cmbModule.AddItem("Collections");
            }
            if (modEntryMenuForm.Statics.ClerkAllowed)
            {
                cmbModule.AddItem("Clerk");
            }
            if (modEntryMenuForm.Statics.CodeAllowed)
            {
                cmbModule.AddItem("Code Enforcement");
            }
            if (modEntryMenuForm.Statics.BudgetaryAllowed)
            {
                cmbModule.AddItem("Budgetary");
            }
            if (modEntryMenuForm.Statics.CashReceiptAllowed)
            {
                cmbModule.AddItem("Cash Receipting");
            }
            if (modEntryMenuForm.Statics.PayrollAllowed)
            {
                cmbModule.AddItem("Payroll");
            }
            else
            {
                cmdPayrollSecurity.Visible = false;
            }
            if (modEntryMenuForm.Statics.UtilitiesAllowed)
            {
                cmbModule.AddItem("Utility Billing");
            }
            if (modEntryMenuForm.Statics.MotorVehicleAllowed)
            {
                cmbModule.AddItem("Motor Vehicle");
            }
            if (modEntryMenuForm.Statics.FixedAssetAllowed)
            {
                cmbModule.AddItem("Fixed Assets");
            }
            if (modEntryMenuForm.Statics.AccountsReceivableAllowed)
            {
                cmbModule.AddItem("Accounts Receivable");
            }
            if (modEntryMenuForm.Statics.VoterRegAllowed)
            {
                cmbModule.AddItem("Voter Registration");
            }

            cmbModule.SelectedIndex = -1;
            boolFillingCombo = false;
        }

        private void cmdDelete_Click()
        {
            if (currentUserId == 0)
            {
                MessageBox.Show("User must be selected.", "Trio Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            secRS.OpenRecordset("select count(*) as thecount from permissionstable where modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PASSWORDSETUP) + " and permission = 'F'", "systemsettings");
            var userIdToUse = currentUserId;
            if (FCConvert.ToInt32(secRS.GetData("thecount")) == 1)
            {
                secRS.OpenRecordset("select userid from permissionstable where modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PASSWORDSETUP) + " and permission = 'F'", "systemsettings");
                
                if (FCConvert.ToInt32(secRS.GetData("UserID")) == userIdToUse)
                {
                    MessageBox.Show("You cannot delete the last user with password setup permissions." + "\r\n" + "Give another user this right before deleting this user.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            secRS.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "'", "ClientSettings");
            if (secRS.EndOfFile() != true && secRS.BeginningOfFile() != true)
            {
                secRS.MoveLast();
                secRS.MoveFirst();
                secRS.FindFirstRecord("ID", userIdToUse);
                if (secRS.NoMatch == false)
                {
                    secRS.Delete();
                    secRS.Update();
                    modGNBas.Statics.secDB.Execute("delete from permissionstable where userid = " + userIdToUse, "systemsettings");
                }
            }
            FillList();
            ClearScreen();
            currentUserId = 0;
        }

        private void cmdSave_Click()
        {
	        bool newUser = false; 
			
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            if (fraSecurity.Visible)
            {
                cmdSaveOptions_Click();
            }
            if (txtOPID.Text == "")
            {
                MessageBox.Show("Operator ID must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtOPID.Focus();
                goto Done;
            }
            txtUserID.Text = txtOPID.Text;

            if (txtName.Text == "")
            {
                MessageBox.Show("User Name must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtName.Focus();
                goto Done;
            }
            if (currentUserId == 0)
            {
                if (txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Password is empty. You must select a password", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Focus();
                    goto Done;
                }
            }

            var user = new User();
            var userIdToUse = currentUserId;
            if (userIdToUse > 0)
            {
                user = _authenticationService.GetUserByID(userIdToUse);
            }
            else
            {
	            if (_userDataAccess.DuplicateUserIdExists(txtUserID.Text.Trim()))
	            {
		            MessageBox.Show("A user already exists with this user id." + "\r\n" + "Please enter an unused user id to continue.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
		            goto Done;
	            }

                user.LockedOut = false;
                user.Inactive = false;
                user.FailedAttempts = 0;
                user.UserIdentifier = Guid.NewGuid();
                user.ClientIdentifier = StaticSettings.gGlobalSettings.ClientIdentifier;
                newUser = true;
            }

            if (txtPassword.Text.Trim() != "")
            {
                PasswordValidationService passwordValidationService = new PasswordValidationService();
                var result = passwordValidationService.IsValidPassword(txtPassword.Text, _authenticationService.GetPreviousPasswords(user.Id));

                if (result == PasswordValidationResult.CommonPassword)
                {
                    MessageBox.Show("This password is too common to use.", "Common Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Text = "";
                    txtPasswordConfirm.Text = "";
                    txtPassword.Focus();
                    goto Done;
                }
                else if (result == PasswordValidationResult.PreviousPassword)
                {
                    MessageBox.Show("You have already used this password.", "Previously Used Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Text = "";
                    txtPasswordConfirm.Text = "";
                    txtPassword.Focus();
                    goto Done;
                }
                else if (result == PasswordValidationResult.DoesNotMeetMinimumRequirements)
                {
                    MessageBox.Show("This password does not meet the minimum requirements.  The password length must be at least 8 characters.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Text = "";
                    txtPasswordConfirm.Text = "";
                    txtPassword.Focus();
                    goto Done;
                }
            }

            if (txtPasswordConfirm.Text != txtPassword.Text)
            {
                MessageBox.Show("The password and password confirmation boxes are not the same." + "\r\n" + "Please re-enter the password and its confirmation.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                goto Done;
            }
            txtPassword.Text = Strings.Trim(txtPassword.Text + "");

            user.UserID = txtUserID.Text;
            user.OPID = txtOPID.Text;
            user.UserName = txtName.Text;
            user.Frequency = txtFrequency.Text.ToIntegerValue();
            user.UpdateDate = DateTime.Now;
            user.CanUpdateSite = chkAllowSiteUpdate.Checked;

            if (Strings.Trim(txtPassword.Text) != "")
            {
                user.DateChanged = DateTime.Now;
                var result = _authenticationService.GetPasswordHashAndSalt(txtPassword.Text);
                user.Password = result.password;
                user.Salt = result.salt;
            }

            _userDataAccess.SaveUser(user);

            if (newUser)
            {
				BuildPermissionsTable(user.Id);
            }

            currentUserId = user.Id;
            int intThisID;
            int x;
            intThisID = user.Id;
            FillList();
            for (x = 1; x <= lstUsers.Items.Count - 1; x++)
            {
                if (lstUsers.ItemData(x) == intThisID)
                {
                    lstUsers.SelectedIndex = x;
                    MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    goto Done;
                }
            }
            isSaved = true;
            MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
        Done:
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
        }

        private void cmdSaveOptions_Click(object sender, System.EventArgs e)
        {
            string strOptions = "";
            int intRow;
            clsDRWrapper cdcTemp = new clsDRWrapper();
            txtUserID.Text = txtOPID.Text;
            if (txtUserID.Text != string.Empty)
            {
                if (fraPayroll.Visible)
                    SavePayrollGrid();
                cdcTemp.Execute("delete  FROM permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = '" + strApplicationName + "'", "systemsettings");
            }
            else
            {
                MessageBox.Show("There is no current UserID. You must pick a user in order to save permissions.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            lngID = currentUserId;
            bool boolCollections;
            boolCollections = true;

            for (intRow = 1; intRow <= vs1.Rows - 1; intRow++)
            {
                if (vs1.TextMatrix(intRow, 1) != "")
                {
                    if (Conversion.Val(vs1.TextMatrix(intRow, 2)) == -1)
                    {
                        strOptions = "F";
                    }
                    else if (Conversion.Val(vs1.TextMatrix(intRow, 3)) == -1)
                    {
                        strOptions = "P";
                    }
                    else if (Conversion.Val(vs1.TextMatrix(intRow, 4)) == -1)
                    {
                        strOptions = "N";
                    }
                    else
                    {
                        strOptions = "N";
                        vs1.TextMatrix(intRow, 4, FCConvert.ToString(-1));
                    }
                }

                if (vs1.TextMatrix(intRow, 5) == "1" && strApplicationName != "GE")
                {
                    if (strApplicationName == "RE")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.REALESTATE), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.REALESTATE) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "MV")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.MOTORVEHICLE), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.MOTORVEHICLE) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "PP")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PERSONALPROPERTY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.PERSONALPROPERTY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "BL")
                    {
                        cdcTemp.Execute("delete from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.BILLINGENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.BILLINGENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "UT")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.UTILITYBILLING), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.UTILITYBILLING) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "CL")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.COLLECTIONSENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.COLLECTIONSENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "CK")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.CLERKENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.CLERKENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "VR")
                    {
                        cdcTemp.Execute("delete  from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.VOTERREGISTRATION), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.VOTERREGISTRATION) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "CE")
                    {
                        cdcTemp.Execute("delete from permissionStable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.CODEENFORCEMENT), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.CODEENFORCEMENT) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "BD")
                    {
                        cdcTemp.Execute("delete  from permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.BUDGETARYSYSTEM), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.BUDGETARYSYSTEM) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "CR")
                    {
                        cdcTemp.Execute("delete  from permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.CASHRECEIPTS), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.CASHRECEIPTS) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "PY")
                    {
                        cdcTemp.Execute("delete  from permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PAYROLLENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.PAYROLLENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "TC")
                    {
                        cdcTemp.Execute("delete  from permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.TAXRATECALCENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.TAXRATECALCENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "FA")
                    {
                        cdcTemp.Execute("delete  from permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.FIXEDASSETENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.FIXEDASSETENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                    else if (strApplicationName == "AR")
                    {
                        cdcTemp.Execute("delete  from permissionstable where userid = " + FCConvert.ToString(lstUsers.ItemData(lstUsers.SelectedIndex)) + " and modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.CNSTACCOUNTSRECEIVABLEENTRY), "systemsettings");
                        cdcTemp.Execute("insert into permissionstable(userid,modulename,functionid,permission) values(" + FCConvert.ToString(lngID) + ",'GE'," + FCConvert.ToString(modSecurity2.CNSTACCOUNTSRECEIVABLEENTRY) + ",'" + strOptions + "')", "systemsettings");
                    }
                }
                if (strApplicationName == "GE")
                {
                    switch (FCConvert.ToInt32(vs1.TextMatrix(intRow, 5)))
                    {
                        case modSecurity2.REALESTATE:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'RE'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.MOTORVEHICLE:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'MV'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.PERSONALPROPERTY:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'PP'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.BILLINGENTRY:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'BL'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.UTILITYBILLING:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'UT'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.CLERKENTRY:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'CK'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.CODEENFORCEMENT:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'CE'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.BUDGETARYSYSTEM:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'BD'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.CASHRECEIPTS:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'CR'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.PAYROLLENTRY:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'PY'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.TAXRATECALCENTRY:
                            {
                                break;
                            }
                        case modSecurity2.FIXEDASSETENTRY:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'FA'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.CNSTACCOUNTSRECEIVABLEENTRY:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'AR'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.REDBOOK:
                            {
                                if (strOptions == "N")
                                {
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'RB'", "systemsettings");
                                }
                                break;
                            }
                        case modSecurity2.COLLECTIONSENTRY:
                            {
                                // re collections
                                if (!boolCollections && strOptions == "N")
                                {
                                    // ppcollections is also none
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'CL'", "systemsettings");
                                }
                                if (strOptions == "N")
                                {
                                    boolCollections = false;
                                }
                                break;
                            }
                        case modSecurity2.PPCOLLECTIONS:
                            {
                                // pp collections
                                if (!boolCollections && strOptions == "N")
                                {
                                    // recollections is also none
                                    cdcTemp.Execute("update permissionstable set permission = 'N' where userid =" + FCConvert.ToString(lngID) + " and modulename = 'CL'", "systemsettings");
                                }
                                if (strOptions == "N")
                                {
                                    boolCollections = false;
                                }
                                break;
                            }
                    }
                    //end switch
                }
                cdcTemp.OpenRecordset("select * from permissionstable where userid = " + FCConvert.ToString(lngID) + " and modulename = '" + strApplicationName + "' and functionid = " + vs1.TextMatrix(intRow, 5), "systemsettings");
                if (cdcTemp.RecordCount() == 0)
                {
                    // need this if to eliminate repeats, although they will all be equal
                    cdcTemp.Execute("insert into permissionstable (userid,modulename,functionid,permission) values (" + FCConvert.ToString(lngID) + ",'" + strApplicationName + "'," + vs1.TextMatrix(intRow, 5) + ",'" + strOptions + "')", "systemsettings");
                }
            }

            isSaved = true;
            MessageBox.Show("Save Complete.  Updated security settings will take effect the next time the user logs into the system.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            modGNBas.Statics.boolChangedWithoutSave = false;
            fraSecurity.Visible = false;
            fraInfo.Visible = true;
            txtT.Text = "";
        }

        public void cmdSaveOptions_Click()
        {
            cmdSaveOptions_Click(cmdSaveOptions, new System.EventArgs());
        }

        private void frmSecurity_Activated(object sender, System.EventArgs e)
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            if (modGlobalRoutines.FormExist(this))
                return;
            FillList();
            clsTemp.OpenRecordset("select * from globalvariables", "systemsettings");
            modGlobalFunctions.SetFixedSize(this, FCConvert.ToInt16(true));
        }

        private void frmSecurity_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        private void frmSecurity_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Escape)
            {
                if (fraSecurity.Visible == true)
                {
                    fraSecurity.Visible = false;
                    // Frame1.Visible = True
                    fraInfo.Visible = true;
                    // cmdMV.Visible = True
                }
                else
                {
                    KeyAscii = (Keys)0;
                    frmSecurity.InstancePtr.Unload();
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmSecurity_Load(object sender, System.EventArgs e)
        {
            FillModuleCombo();
            modGlobalFunctions.SetFixedSize(this);
            modGlobalFunctions.SetTRIOColors(this);
            if (!StaticSettings.gGlobalSettings.IsHostedSite)
            {
                lblUpdateSite.Visible = true;
                chkAllowSiteUpdate.Visible = true;
            }
            else
            {
                lblUpdateSite.Visible = false;
                chkAllowSiteUpdate.Visible = false;
            }
            if (modEntryMenuForm.Statics.PayrollAllowed)
            {
                cmdPayrollSecurity.Visible = true;
            }
            else
            {
                cmdPayrollSecurity.Visible = false;
            }
            vs1.EditMode = DataGridViewEditMode.EditOnF2;
            isSaved = false;
        }

        private void frmSecurity_Resize(object sender, System.EventArgs e)
        {
            ResizeVs1();
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            modGNBas.Statics.clsGESecurity.Init("GE", "systemsettings");
            modGlobalConstants.Statics.clsSecurityClass.Init("GE", "systemsettings", null, modGNBas.Statics.clsGESecurity.Get_UserID());
            rsSecurity = null;

            if (isSaved)
            {
                MDIParent.InstancePtr.MainCaptions();
                App.MainForm.RefreshModules();
            }
        }

        private void lstUsers_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string strResponse = "";

            if (Strings.UCase(Strings.Mid(lstUsers.Items[lstUsers.SelectedIndex].Text, 1, 3)) != "ADD")
            {
                if (fraSecurity.Visible == true)
                {
                    if (modGNBas.Statics.boolChangedWithoutSave && (lstUsers.SelectedIndex != Conversion.Val(lstUsers.Tag)))
                    {
                        strResponse = FCConvert.ToString(MessageBox.Show("You have changed permission for this user without saving." + "\r\n" + "Do you want to save this information before loading the new user?", " ", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                        if (strResponse == DialogResult.Yes.ToString())
                        {
                            cmdSaveOptions_Click();
                        }
                    }
                }
                lngID = lstUsers.ItemData(lstUsers.SelectedIndex);
                FillScreen();
                if (fraSecurity.Visible)
                {
                    FillUserInfo(ref strApplicationName, ref lngID);
                }
                lstUsers.Tag = lstUsers.SelectedIndex;
            }
            else
            {
                if (fraSecurity.Visible == true)
                {
                    lstUsers.SelectedIndex = FCConvert.ToInt32(lstUsers.Tag);
                    return;
                }
                ClearScreen();
                lngID = 0;
                currentUserId = 0;
                txtOPID.Focus();
            }
        }

        private void FillScreen()
        {
            User user = _authenticationService.GetUserByID(lngID);
            if (user != null)
            {
                currentUserId = user.Id;
                txtOPID.Text = user.OPID;
                txtUserID.Text = user.UserID;
                txtName.Text = user.UserName;
                txtFrequency.Text = user.Frequency.ToString();
                txtPassword.Text = "";
                txtPasswordConfirm.Text = "";
                chkAllowSiteUpdate.Checked = user.CanUpdateSite ?? false;

                if (user.LockedOut ?? false)
                {
                    btnUnlockUser.Visible = true;
                }
                else
                {
                    btnUnlockUser.Visible = false;
                }
            }
        }

        private void ClearScreen()
        {
            currentUserId = 0;
            txtOPID.Text = "";
            txtUserID.Text = "";
            txtName.Text = "";
            txtFrequency.Text = "";
            txtPassword.Text = "";
            txtPasswordConfirm.Text = "";
            btnUnlockUser.Visible = false;
            chkAllowSiteUpdate.Checked = false;
        }

        private void FillList()
        {
            lstUsers.Clear();
            lstUsers.AddItem("Add a New User");

            var users = _userDataAccess.GetUsers();

            lstUsers.Columns.Add("");
            lstUsers.Columns[0].Width = lstUsers.Width / 2 - 10;
            lstUsers.Columns[1].Width = lstUsers.Width / 2 - 10;
            lstUsers.GridLineStyle = GridLineStyle.None;
            if (users.Count() > 0)
            {
                foreach (var user in users)
                {
                    lstUsers.AddItem(user.UserName + "\t" + user.UserID);
                    lstUsers.ItemData(lstUsers.NewIndex, user.Id);
                    secRS.MoveNext();
                }
            }
            if (txtOPID.Text != string.Empty)
            {
                lstUsers.SelectedIndex = lstUsers.Items.Count - 1;
            }
        }

        private void mnuAddPayrollDeptDiv_Click(object sender, System.EventArgs e)
        {
            vsPayroll.Rows += 1;
            vsPayroll.Select(vsPayroll.Rows - 1, 0);
            vsPayroll.Focus();
            vsPayroll.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsPayroll.Row, 0, vsPayroll.Row, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void mnuDeleteInfo_Click(object sender, System.EventArgs e)
        {
            cmdDelete_Click();
        }

        private void mnuDeletePayrollDeptDiv_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (vsPayroll.Row > 0)
                {
                    if (vsPayroll.Row <= vsPayroll.Rows - 1)
                    {
                        vsPayroll.RemoveItem(vsPayroll.Row);
                    }
                }
                else
                {
                    MessageBox.Show("Grid row to delete must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            catch
            {
            }
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Unload();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuFile_Click(object sender, System.EventArgs e)
        {
            cmdAddPayrollDeptDiv.Visible = cmbModule.Text == "Payroll";
            cmdDeletePayrollDeptDiv.Visible = cmbModule.Text == "Payroll";
        }

        private void mnuPayrollSecurity_Click(object sender, System.EventArgs e)
        {
            frmPayrollSecurity.InstancePtr.Show(FormShowEnum.Modal);
        }

        private void mnuSaveExit_Click(object sender, System.EventArgs e)
        {
            mnuSaveInfo_Click();
            mnuExit_Click();
        }

        private void mnuSaveInfo_Click(object sender, System.EventArgs e)
        {
            cmdSave_Click();
        }

        public void mnuSaveInfo_Click()
        {
            mnuSaveInfo_Click(mnuSaveInfo, new System.EventArgs());
        }

        private void vs1_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            int intCounter = 0;
            clsDRWrapper clstempdc = new clsDRWrapper();
            int intParentRow = 0;
            int x;
            bool boolTemp = false;
            bool boolExit;
            boolExit = false;
            modGNBas.Statics.boolChangedWithoutSave = true;
            if (strApplicationName == "GE")
            {
                if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, 5)) == modSecurity2.PASSWORDSETUP)
                {
                    if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2) != FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                    {
                        clstempdc.OpenRecordset("select * from permissionstable where modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PASSWORDSETUP) + " and permission = 'F'", "systemsettings");
                        if (clstempdc.RecordCount() < 2)
                        {
                            if (FCConvert.ToInt32(clstempdc.GetData("userid")) == lngID)
                            {
                                // it is!
                                MessageBox.Show("This is the last person with password setup permission." + "\r\n" + "You must give someone else this permission before taking it away from this user.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                return;
                            }
                        }
                    }
                }
                else if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, 5)) == modSecurity2.SYSTEMMAINT)
                {
                    if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                    {
                        clstempdc.OpenRecordset("select * from permissionstable where modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.SYSTEMMAINT) + " and permission = 'F' or permission = 'P'", "systemsettings");
                        if (clstempdc.RecordCount() < 2)
                        {
                            if (clstempdc.RecordCount() < 1)
                            {
                                MessageBox.Show("You must have at least one person with permission to enter System Maintenenace", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                boolExit = true;
                                // TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
                            }
                            else if (FCConvert.ToInt32(clstempdc.Get_Fields("userid")) == lngID)
                            {
                                MessageBox.Show("This is the last person with permission to enter system maintenance" + "\r\n" + "You must give someone else this permission before taking it away from this user.", "Last User", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                boolExit = true;
                            }
                        }
                        clstempdc.OpenRecordset("select * from permissionstable where modulename = 'GE' and functionid = " + FCConvert.ToString(modSecurity2.PASSWORDSETUP) + " and permission = 'F'", "systemsettings");
                        if (clstempdc.RecordCount() < 2)
                        {
                            boolTemp = false;
                            if (clstempdc.RecordCount() < 1)
                            {
                                MessageBox.Show("You must have at least one person with permission to enter Password Setup", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                boolTemp = true;
                                // TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
                            }
                            else if (FCConvert.ToInt32(clstempdc.Get_Fields("userid")) == lngID)
                            {
                                boolTemp = true;
                                MessageBox.Show("This is the last person with permission to enter Password Setup", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            if (boolTemp)
                            {
                                for (x = 1; x <= vs1.Rows - 1; x++)
                                {
                                    if (FCConvert.ToDouble(vs1.TextMatrix(x, 5)) == modSecurity2.PASSWORDSETUP)
                                    {
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, x, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, x, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, x, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                        return;
                                    }
                                }
                                // x
                            }
                        }
                        if (boolExit || boolTemp)
                        {
                            return;
                        }
                    }
                }
            }
            switch (vs1.Col)
            {
                case 2:
                    {
                        if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                        {
                            if (vs1.IsSubtotal(vs1.Row))
                            {
                                intCounter = vs1.Row + 1;
                                if (intCounter < vs1.Rows)
                                {
                                    while (!vs1.IsSubtotal(intCounter))
                                    {
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                        intCounter += 1;
                                        if (intCounter == vs1.Rows)
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                intParentRow = GetParentRow(vs1.Row);
                                SetParentRow(intParentRow);
                            }
                            CheckAllOccurences(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 5)), 2, vs1.Row);
                        }
                        else
                        {
                            vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                        }
                        break;
                    }
                case 3:
                    {
                        // Exit Sub
                        if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 3) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                        {
                            CheckAllOccurences(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 5)), 3, vs1.Row);
                        }
                        else
                        {
                            vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                        }
                        break;
                    }
                case 4:
                    {
                        if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                        {
                            if (vs1.IsSubtotal(vs1.Row))
                            {
                                intCounter = vs1.Row + 1;
                                if (intCounter < vs1.Rows)
                                {
                                    while (!vs1.IsSubtotal(intCounter))
                                    {
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 4, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                        intCounter += 1;
                                        if (intCounter == vs1.Rows)
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                intParentRow = GetParentRow(vs1.Row);
                                SetParentRow(intParentRow);
                            }
                            CheckAllOccurences(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 5)), 4, vs1.Row);
                            if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, 5)) == 1 && strApplicationName != "GE")
                            {
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, 4, vs1.Rows - 1, 4, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, 2, vs1.Rows - 1, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, 3, vs1.Rows - 1, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                            }
                        }
                        else
                        {
                            vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, vs1.Row, 4, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                        }
                        break;
                    }
            }
        }

        private short GetParentRow(int intCurrentRow)
        {
            short GetParentRow = 0;
            int counter;
            GetParentRow = -1;
            for (counter = intCurrentRow - 1; counter >= 0; counter--)
            {
                if (vs1.IsSubtotal(counter))
                {
                    GetParentRow = FCConvert.ToInt16(counter);
                    break;
                }
            }
            return GetParentRow;
        }

        private void SetParentRow(int intStartRow)
        {
            int counter;
            bool blnYes;
            bool blnNo;
            blnYes = false;
            blnNo = false;
            for (counter = FCConvert.ToInt32(intStartRow + 1); counter <= vs1.Rows - 1; counter++)
            {
                if (!vs1.IsSubtotal(counter))
                {
                    if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, counter, 2) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                    {
                        blnYes = true;
                    }
                    else if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, counter, 4) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                    {
                        blnNo = true;
                    }
                }
                else
                {
                    break;
                }
            }
            if (blnYes == true && blnNo == true)
            {
                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 4, FCGrid.CellCheckedSettings.flexUnchecked);
            }
            else if (blnYes)
            {
                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 4, FCGrid.CellCheckedSettings.flexUnchecked);
            }
            else
            {
                if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 2) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                {
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intStartRow, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                }
            }
        }

        private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpData, vs1.Row, vs1.Col)) == "NoView")
            {
                vs1.EditText = "";
            }
        }

        private void vs1_DblClick(object sender, System.EventArgs e)
        {
            if (vs1.MouseRow == 0)
            {
                switch (vs1.MouseCol)
                {
                    case 2:
                        {
                            if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol)) == 2)
                            {
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol, vs1.Rows - 1, vs1.MouseCol, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, 3, vs1.Rows - 1, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                            }
                            else if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol)) == 1)
                            {
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol, vs1.Rows - 1, vs1.MouseCol, FCGrid.CellCheckedSettings.flexUnchecked);
                            }
                            break;
                        }
                    case 3:
                        {
                            break;
                        }
                    case 4:
                        {
                            if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol)) == 2)
                            {
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol, vs1.Rows - 1, vs1.MouseCol, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, 2, vs1.Rows - 1, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                            }
                            else if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol)) == 1)
                            {
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, 1, vs1.MouseCol, vs1.Rows - 1, vs1.MouseCol, FCGrid.CellCheckedSettings.flexUnchecked);
                            }
                            break;
                        }
                }
            }
        }

        private void vs1_RowColChange(object sender, System.EventArgs e)
        {
            if (FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpData, vs1.Row, vs1.Col)) == "NoView")
            {
                vs1.Editable = FCGrid.EditableSettings.flexEDNone;
                vs1.EditText = FCConvert.ToString(0);
                vs1.TextMatrix(vs1.Row, vs1.Col, FCConvert.ToString(0));
            }
            else
            {
                vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            }
        }

        private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int intCounter;

            int row = vs1.GetFlexRowIndex(e.RowIndex);
            int col = vs1.GetFlexColIndex(e.ColumnIndex);

            if (vs1.EditText == "0")
            {
                if (vs1.TextMatrix(row, 1) != "")
                {
                    if (col == 2)
                    {
                        vs1.TextMatrix(row, 3, "");
                        vs1.TextMatrix(row, 4, "");
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, row, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, row, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                    }
                    else if (col == 3)
                    {
                        if (vs1.IsSubtotal(row))
                        {
                            vs1.TextMatrix(row, 2, "");
                            vs1.TextMatrix(row, 4, "");
                            vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, row, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                            vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, row, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else if (col == 4)
                    {
                        vs1.TextMatrix(row, 2, "");
                        vs1.TextMatrix(row, 3, "");
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, row, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, row, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                    }
                }
            }
        }

        private void GridProperties()
        {
            int xxx;
            for (xxx = 1; xxx <= vs1.Rows - 1; xxx++)
            {
                vs1.RowHidden(xxx, false);
            }

            vs1.Clear();
            float sngScreen;
            sngScreen = 1;
            if (FCGlobal.Screen.Width < 10000)
            {
                sngScreen = 1;
            }
            else if (FCGlobal.Screen.Width < 13000)
            {
                sngScreen = 1.3f;
            }
            else if (FCGlobal.Screen.Width < 16000)
            {
                sngScreen = 1.6f;
            }
            else
            {
                sngScreen = 1.9f;
            }

            vs1.ColDataType(2, FCGrid.DataTypeSettings.flexDTBoolean);
            vs1.ColDataType(3, FCGrid.DataTypeSettings.flexDTBoolean);
            vs1.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
            vs1.ColDataType(5, FCGrid.DataTypeSettings.flexDTLong);
            vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, true);
            vs1.TextMatrix(0, 1, "Security Option");
            vs1.TextMatrix(0, 2, "Full");
            vs1.TextMatrix(0, 3, "Partial");
            vs1.TextMatrix(0, 4, "None");
            vs1.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
            vs1.ColHidden(5, true);
            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
        }

        private void NewSetupGrid(ref string strModName)
        {
            cSecuritySetup tSecSet = new cSecuritySetup();
            vs1.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;
            FCCollection tColl = new FCCollection();
            tColl = tSecSet.GetPermissionSetup(strModName);
            cSecuritySetupItem tOb;
            cSecuritySetupItem tItem;
            int intRcount = 0;
            int intCRow = 0;
            int x;
            vs1.Rows = 1;
            string strTemp = "";
            if (!(tColl == null))
            {
                intRcount = tColl.Count;
                vs1.Rows = intRcount + 1;
                intCRow = 1;
                for (x = 1; x <= tColl.Count; x++)
                {
                    tOb = tColl[x];
                    tItem = tOb;
                    if (tItem.ChildFunction == string.Empty)
                    {
                        strTemp = tItem.ParentFunction;
                        vs1.TextMatrix(intCRow, 1, strTemp);
                        vs1.IsSubtotal(intCRow, true);
                        vs1.IsCollapsed(intCRow, FCGrid.CollapsedSettings.flexOutlineExpanded);
                        vs1.RowOutlineLevel(intCRow, 0);
                    }
                    else
                    {
                        vs1.TextMatrix(intCRow, 1, "    " + tItem.ChildFunction);
                        vs1.IsSubtotal(intCRow, false);
                        vs1.IsCollapsed(intCRow, FCGrid.CollapsedSettings.flexOutlineExpanded);
                        vs1.RowOutlineLevel(intCRow, 1);
                    }
                    if (tItem.UseViewOnly == false)
                    {
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCRow, 3, intCRow, 3, Information.RGB(200, 200, 200));
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCRow, 3, FCGrid.CellCheckedSettings.flexNoCheckbox);
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpData, intCRow, 3, "NoView");
                    }
                    else
                    {
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpData, intCRow, 3, "UseView");
                    }
                    vs1.TextMatrix(intCRow, 5, FCConvert.ToString(tItem.FunctionID));
                    intCRow += 1;
                }
                // x
            }
            else
            {
                vs1.Rows = 1;
                intCRow = 1;
            }
            FillUserInfo(ref strModName, ref lngID);
            modColorScheme.ColorGrid(vs1);
        }

        private void FillUserInfo(ref string strModName, ref int lngID)
        {
            int intloopcounter;
            int lngFId = 0;
            int lngIDNum = 0;
            modGNBas.Statics.boolChangedWithoutSave = false;
            for (intloopcounter = 1; intloopcounter <= vs1.Rows - 1; intloopcounter++)
            {
                lngFId = FCConvert.ToInt32(vs1.TextMatrix(intloopcounter, 5));
                if (lngFId == 1 && Strings.UCase(strModName) != "GE")
                {
                    if (strModName == "RE")
                    {
                        lngIDNum = modSecurity2.REALESTATE;
                    }
                    else if (strModName == "MV")
                    {
                        lngIDNum = modSecurity2.MOTORVEHICLE;
                    }
                    else if (strModName == "PP")
                    {
                        lngIDNum = modSecurity2.PERSONALPROPERTY;
                    }
                    else if (strModName == "BL")
                    {
                        lngIDNum = modSecurity2.BILLINGENTRY;
                    }
                    else if (strModName == "UT")
                    {
                        lngIDNum = modSecurity2.UTILITYBILLING;
                    }
                    else if (strModName == "CL")
                    {
                        lngIDNum = modSecurity2.COLLECTIONSENTRY;
                    }
                    else if (strModName == "CK")
                    {
                        lngIDNum = modSecurity2.CLERKENTRY;
                    }
                    else if (strModName == "VR")
                    {
                        lngIDNum = modSecurity2.VOTERREGISTRATION;
                    }
                    else if (strModName == "CE")
                    {
                        lngIDNum = modSecurity2.CODEENFORCEMENT;
                    }
                    else if (strModName == "BD")
                    {
                        lngIDNum = modSecurity2.BUDGETARYSYSTEM;
                    }
                    else if (strModName == "CR")
                    {
                        lngIDNum = modSecurity2.CASHRECEIPTS;
                    }
                    else if (strModName == "PY")
                    {
                        lngIDNum = modSecurity2.PAYROLLENTRY;
                    }
                    else if (strModName == "FA")
                    {
                        lngIDNum = modSecurity2.FIXEDASSETENTRY;
                    }
                    else if (strModName == "AR")
                    {
                        lngIDNum = modSecurity2.CNSTACCOUNTSRECEIVABLEENTRY;
                    }
                    rsSecurity.OpenRecordset("select * from permissionstable where modulename = 'GE' and userid = " + FCConvert.ToString(lngID) + " and functionid = " + FCConvert.ToString(lngIDNum), "systemsettings");
                }
                else
                {
                    rsSecurity.OpenRecordset("Select * from permissionstable where modulename = '" + strModName + "' and userid = " + FCConvert.ToString(lngID) + " and functionid = " + FCConvert.ToString(lngFId), "systemsettings");
                }
                if (!rsSecurity.EndOfFile())
                {
                    string vbPorterVar = FCConvert.ToString(rsSecurity.GetData("permission"));
                    if (vbPorterVar == "F")
                    {
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                    }
                    else if (vbPorterVar == "P")
                    {
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                    }
                    else if (vbPorterVar == "N")
                    {
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 4, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                    }
                }
                else
                {
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intloopcounter, 4, Wisej.Web.CheckState.Checked);
                }
            }
        }

        private void CheckAllOccurences(int lngFuncID, int intWhatValue, int intRowNum)
        {
            int intCounter;
            int intPlaceHolder = 0;
            int intColChkd = 0;
            int intParentRow = 0;
            for (intCounter = 1; intCounter <= vs1.Rows - 1; intCounter++)
            {
                if (intCounter != intRowNum)
                {
                    if (FCConvert.ToDouble(vs1.TextMatrix(intCounter, 5)) == lngFuncID)
                    {
                        if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, intWhatValue) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                            return;

                        switch (intWhatValue)
                        {
                            case 2:
                                {
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 2, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                    break;
                                }
                            case 3:
                                {
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 3, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 4, FCGrid.CellCheckedSettings.flexUnchecked);
                                    break;
                                }
                            case 4:
                                {
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 2, FCGrid.CellCheckedSettings.flexUnchecked);
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 3, FCGrid.CellCheckedSettings.flexUnchecked);
                                    vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intCounter, 4, FCConvert.ToInt32(Wisej.Web.CheckState.Checked));
                                    intParentRow = GetParentRow(intCounter);
                                    SetParentRow(intParentRow);
                                    break;
                                }
                        }

                        if (vs1.IsSubtotal(intRowNum))
                        {
                            intPlaceHolder = intRowNum + 1;
                            if (intPlaceHolder == vs1.Rows)
                                return;
                            while (!vs1.IsSubtotal(intPlaceHolder))
                            {
                                if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intPlaceHolder, 2) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                                {
                                    intColChkd = 2;
                                }
                                else if (vs1.Cell(FCGrid.CellPropertySettings.flexcpChecked, intPlaceHolder, 3) == FCConvert.ToInt32(Wisej.Web.CheckState.Checked))
                                {
                                    intColChkd = 3;
                                }
                                else
                                {
                                    intColChkd = 4;
                                }
                                CheckAllOccurences(FCConvert.ToInt32(vs1.TextMatrix(intPlaceHolder, 5)), intColChkd, intPlaceHolder);
                                intPlaceHolder += 1;
                                if (intPlaceHolder == vs1.Rows)
                                    return;
                            }
                        }
                    }
                }
            }
        }

        private void ResizeVs1()
        {
            int GridWidth = 0;
            GridWidth = vs1.WidthOriginal;
            vs1.ColWidth(0, 60);
            vs1.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.62));
            vs1.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.11));
            vs1.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.11));
            vs1.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.11));
        }

        private void vsPayroll_KeyDownEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                vsPayroll.Rows += 1;
                vsPayroll.Select(vsPayroll.Rows - 1, 0);
            }
        }

        private void vsPayroll_KeyDownEdit(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                vsPayroll.Rows += 1;
                vsPayroll.Select(vsPayroll.Rows - 1, 0);
            }
        }

        private void btnUnlockUser_Click(object sender, EventArgs e)
        {
            var userIdToUse = currentUserId;
            var user = _authenticationService.GetUserByID(userIdToUse);
            if (_authenticationService.UnlockUser(user))
            {
                btnUnlockUser.Visible = false;
                MessageBox.Show("You have successfully unlocked this user.", "User Unlocked", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BuildPermissionsTable(int id)
        {
	        BuildPermissionsByModule("GE", id);
			BuildPermissionsByModule("CP", id);

			if (modEntryMenuForm.Statics.RealEstateAllowed)
			{
				BuildPermissionsByModule("RE", id);
			}
			if (modEntryMenuForm.Statics.PersonalPropertyAllowed)
			{
				BuildPermissionsByModule("PP", id);
			}
			if (modEntryMenuForm.Statics.BillingAllowed)
			{
				BuildPermissionsByModule("BL", id);
			}
			if (modEntryMenuForm.Statics.CollectionsAllowed)
			{
				BuildPermissionsByModule("CL", id);
			}
			if (modEntryMenuForm.Statics.ClerkAllowed)
			{
				BuildPermissionsByModule("CK", id);
			}
			if (modEntryMenuForm.Statics.CodeAllowed)
			{
				BuildPermissionsByModule("CE", id);
			}
			if (modEntryMenuForm.Statics.BudgetaryAllowed)
			{
				BuildPermissionsByModule("BD", id);
			}
			if (modEntryMenuForm.Statics.CashReceiptAllowed)
			{
				BuildPermissionsByModule("CR", id);
			}
			if (modEntryMenuForm.Statics.PayrollAllowed)
			{
				BuildPermissionsByModule("PY", id);
			}
			if (modEntryMenuForm.Statics.UtilitiesAllowed)
			{
				BuildPermissionsByModule("UT", id);
			}
			if (modEntryMenuForm.Statics.PersonalPropertyAllowed)
			{
				BuildPermissionsByModule("MV", id);
			}
			if (modEntryMenuForm.Statics.BillingAllowed)
			{
				BuildPermissionsByModule("FA", id);
			}
			if (modEntryMenuForm.Statics.CollectionsAllowed)
			{
				BuildPermissionsByModule("AR", id);
			}
        }



        private void BuildPermissionsByModule(string module, int id)
        {
	        clsDRWrapper cdcTemp = new clsDRWrapper();
	        cSecuritySetup tSecSet = new cSecuritySetup();

	        FCCollection tColl = new FCCollection();

	        tColl = tSecSet.GetPermissionSetup(module);

	        cSecuritySetupItem tItem = new cSecuritySetupItem();
	        
			if (tColl != null)
	        {
		        for (int x = 1; x < tColl.Count; x++)
		        {
			        var tOb = tColl[x];
			        tItem = tOb;

			        cdcTemp.Execute(
				        "insert into permissionstable (userid,modulename,functionid,permission) values (" + id +
				        ",'" + tItem.ModuleName + "'," + tItem.FunctionID + ",'N')", "systemsettings");
		        }
	        }
        }
	}
}
