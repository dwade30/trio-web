﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using AbaleZipLibrary;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWGNENTY
{
    /// <summary>
    /// Summary description for frmChilkatDownload.
    /// </summary>
    public partial class frmChilkatDownload : fecherFoundation.FCForm
    {
        public frmChilkatDownload()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmChilkatDownload InstancePtr
        {
            get
            {
                return (frmChilkatDownload)Sys.GetInstance(typeof(frmChilkatDownload));
            }
        }

        protected frmChilkatDownload _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By   Dave Wade
        // Date         7/24/03
        // This form will be used to perform automatic uploads to
        // our FTP site.  This can be used if we need to get a database
        // and the customer does not have PCAnywhere and does not have Email
        // ********************************************************
        string downloadfile;
        string strFTPSiteAddress = "";
        string strFTPSiteUser = "";
        string strFTPSitePassword = "";
        int FileCol;
        int SelectCol;
        int DescriptionCol;
        string[] strFolders = null;
        string[] strFiles = null;
        int intFolders;
        int intFiles;
        bool blnAutoCheck;
        public bool blnFromUpdateReminder;
        private string[] strZFiles = null;
        private bool boolDownloadError;
        private string strFTPUserToUse = string.Empty;
        private string strFTPUpdateFolderToUse = string.Empty;
        // Dim WithEvents temp As BACKUPRESTORE
        private void ChilkatFtp1_BeginDownloadFile(string Path, ref int skip)
        {
            ProgressBar1.Value = 0;
        }

        private void ChilkatFtp1_GetProgress(int pctDone)
        {
            ProgressBar1.Value = pctDone;
        }

        private void cmdDownload_Click(object sender, System.EventArgs e)
        {
            int counter;
            bool blnFilesSelected;
            int intTotalFiles;
            int intCurrentlyDownloadedFiles;
            string strDestinationFolder = "";
            string strDataDest;
            bool boolEXEs;
            try
            {
                // On Error GoTo ErrorHandler
                cmdDownload.Enabled = false;
                if (vsUpdates.Rows == 0)
                {
                    /*? 4 */
                    return;
                }
                boolEXEs = false;
                blnFilesSelected = false;
                // 8    If optSelected = True Then
                intTotalFiles = 0;
                for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
                {
                    if (vsUpdates.TextMatrix(counter, SelectCol) == FCConvert.ToString(true))
                    {
                        blnFilesSelected = true;
                        /*? 18 */
                        var desc = Strings.UCase(vsUpdates.TextMatrix(counter, DescriptionCol));

                        if (desc == "GENERAL ENTRY")
                        {
                            intTotalFiles += 1;
                        }
                        else if (desc != "BLUE BOOK DATA")
                        {
                            intTotalFiles += 1;
                        }
                        else
                        {
                            intTotalFiles += 1;
                        }
                    }
                }
                // Else
                // 20        intTotalFiles = vsUpdates.Rows * 3
                // 22        blnFilesSelected = True
                // End If
                if (!blnFilesSelected)
                {
                    MessageBox.Show("You must select at least one update before you may proceed", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    /*? 28 */
                    return;
                }
                intCurrentlyDownloadedFiles = 1;
                lblPercent.Text = "Downloading files:  " + FCConvert.ToString(intCurrentlyDownloadedFiles) + " of " + FCConvert.ToString(intTotalFiles);
                // 34    strDestinationFolder = Left(CurDir, 1) & ":\TRIOData\TRIOMast"
                // strDataDest = CurDir
                if (Strings.Right(Environment.CurrentDirectory, 1) != "\\")
                {
                    strDestinationFolder = Environment.CurrentDirectory + "\\temp";
                }
                else
                {
                    strDestinationFolder = Environment.CurrentDirectory + "temp";
                }
                strDataDest = strDestinationFolder;
                boolDownloadError = false;
                for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
                {
                    if (vsUpdates.TextMatrix(counter, SelectCol) == FCConvert.ToString(true))
                    {
                        // download exe file
                        if (Strings.UCase(vsUpdates.TextMatrix(counter, DescriptionCol)) == "BLUE BOOK DATA")
                        {
                            MessageBox.Show("Before continuing you must make sure that everyone is out of Motor Vehicle and Blue Book", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            DLFile_20(ReturnProperCase_2(vsUpdates.TextMatrix(counter, FileCol) + ".zip"), strDataDest, vsUpdates.TextMatrix(counter, FileCol) + ".zip");
                            intCurrentlyDownloadedFiles += 1;
                            lblPercent.Text = "Downloading files:  " + FCConvert.ToString(intCurrentlyDownloadedFiles) + " of " + FCConvert.ToString(intTotalFiles);
                        }
                        else if (Strings.UCase(vsUpdates.TextMatrix(counter, DescriptionCol)) == "PAYROLL TAX TABLE UPDATE")
                        {
                            DLFile_20(ReturnProperCase_2(vsUpdates.TextMatrix(counter, FileCol) + ".zip"), strDataDest, vsUpdates.TextMatrix(counter, FileCol) + ".zip");
                            intCurrentlyDownloadedFiles += 1;
                            lblPercent.Text = "Downloading files:  " + FCConvert.ToString(intCurrentlyDownloadedFiles) + " of " + FCConvert.ToString(intTotalFiles);
                        }
                        else
                        {
                            if (DLFile_20(ReturnProperCase_2(vsUpdates.TextMatrix(counter, FileCol) + ".zip"), strDestinationFolder, vsUpdates.TextMatrix(counter, FileCol) + ".zip"))
                            {
                                intCurrentlyDownloadedFiles += 1;
                                lblPercent.Text = "Downloading files:  " + FCConvert.ToString(intCurrentlyDownloadedFiles) + " of " + FCConvert.ToString(intTotalFiles);
                                boolEXEs = true;
                            }
                            else
                            {
                                boolDownloadError = true;
                            }
                            // download hlp file
                            // 46                    If CheckIfFileExists(vsUpdates.TextMatrix(counter, FileCol) & ".hlp") Then
                            // 
                            // 48                        DLFile ReturnProperCase(vsUpdates.TextMatrix(counter, FileCol) & ".hlp"), strDestinationFolder, vsUpdates.TextMatrix(counter, FileCol) & ".hlp"
                            // End If
                            // 50                    intCurrentlyDownloadedFiles = intCurrentlyDownloadedFiles + 1
                            // 52                    lblPercent = "Downloading files:  " & intCurrentlyDownloadedFiles & " of " & intTotalFiles
                            // 
                            // download cnt file
                            // 54                    If CheckIfFileExists(vsUpdates.TextMatrix(counter, FileCol) & ".cnt") Then
                            // 
                            // 56                        DLFile ReturnProperCase(vsUpdates.TextMatrix(counter, FileCol) & ".cnt"), strDestinationFolder, vsUpdates.TextMatrix(counter, FileCol) & ".cnt"
                            // End If
                            // 58                    intCurrentlyDownloadedFiles = intCurrentlyDownloadedFiles + 1
                            // 60                    lblPercent = "Downloading files:  " & intCurrentlyDownloadedFiles & " of " & intTotalFiles
                            // If UCase(vsUpdates.TextMatrix(counter, DescriptionCol)) = "GENERAL ENTRY" Then
                            // If CheckIfFileExists("TWPerm.vb1") Then
                            // DLFile "TWPerm.vb1", strDataDest, "TWPerm.vb1"
                            // End If
                            // intCurrentlyDownloadedFiles = intCurrentlyDownloadedFiles + 1
                            // lblPercent.Caption = "Downloading files: " & intCurrentlyDownloadedFiles & " of " & intTotalFiles
                            // End If
                        }
                    }
                }
                lblPercent.Text = "";
                ProgressBar1.Value = 0;
                cmdDownload.Enabled = true;
                if (!boolDownloadError)
                {
                    if (boolEXEs)
                    {
                        MessageBox.Show("Program update has completed.  Please have all users exit TRIO at this time." + "\r\n" + "One user should access each updated program before all users are allowed back into the system.", "Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Program update has completed", "Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("One or more files did not update successfully" + "\r\n" + "Please try again", "Update not successful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                // 68    Unload Me
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                cmdDownload.Enabled = true;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In cmdDownload " + "line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void cmdDownload_Click()
        {
            cmdDownload_Click(cmdDownload, new System.EventArgs());
        }

        private void frmChilkatDownload_Activated(object sender, System.EventArgs e)
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_Err_Connect = 1;
            const int curOnErrorGoToLabel_ErrorHandler = 2;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                int counter;
                clsDRWrapper rsModules = new clsDRWrapper();
                bool boolHasBilling = false;
                bool success;
                bool boolRetried;
                if (modGlobalRoutines.FormExist(this))
                {
                    return;
                }
                boolRetried = false;
                this.Refresh();
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Connecting to FTP Site");
                ChilkatFtp1.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
            TryConnect:
                ;
                vOnErrorGoToLabel = curOnErrorGoToLabel_Err_Connect;
                /* On Error GoTo Err_Connect */
                ChilkatFtp1.Hostname = strFTPSiteAddress;
                ChilkatFtp1.Username = strFTPSiteUser;
                ChilkatFtp1.Password = strFTPSitePassword;
                // ChilkatFtp1.Passive = 1
                success = ChilkatFtp1.Connect();
                if (!success)
                {
                    // error
                    if (!boolRetried)
                    {
                        boolRetried = true;
                        strFTPSitePassword = "trio";
                        goto TryConnect;
                    }
                    MessageBox.Show("Could not connect to ftp site." + "\r\n" + ChilkatFtp1.LastErrorText, "No Connection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.Unload();
                    return;
                }
                // 8    FTP1.Connect strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21
                vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                /* On Error GoTo ErrorHandler */// 10    FTP1.LCD = Left(CurDir, 1) & ":\TRIOData\TRIOMast"
                                                // If Right(CurDir, 1) <> "\" Then
                                                // 
                                                // 10    FTP1.LCD = CurDir & "\temp"
                                                // Else
                                                // FTP1.LCD = CurDir & "temp"
                                                // End If
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building Update List", true, 28);
                RefreshSocket();
                // 16    vsUpdates.Rows = vsUpdates.Rows + 1
                // 18    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol) = "TWGNENTY"
                // If Not blnAutoCheck Or Not blnFromUpdateReminder Then
                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = False
                // Else
                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = UpdateModule("GN")
                // End If
                // 22    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol) = "General Entry"
                if (intFiles > 0)
                {
                    boolHasBilling = false;
                    rsModules.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
                    if (rsModules.BeginningOfFile() != true && rsModules.EndOfFile() != true)
                    {
                        for (counter = 0; counter <= 27; counter++)
                        {
                            //Application.DoEvents();
                            frmWait.InstancePtr.prgProgress.Value = counter + 1;
                            if (Strings.UCase(CreateModuleString(ref counter)) == "GN")
                            {
                                if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                {
                                    vsUpdates.Rows += 1;
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                    if (!blnAutoCheck)
                                    {
                                        // Or Not blnFromUpdateReminder
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                    }
                                    else
                                    {
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                    }
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                }
                                /*? 46 */
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "PYTAXUPDATE")
                            {
                                if (FCConvert.ToBoolean(rsModules.Get_Fields_Boolean("PY")))
                                {
                                    if (CheckIfFileExists_2("TWTaxUpd.zip"))
                                    {
                                        vsUpdates.Rows += 1;
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TWTaxUPD");
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2("PYTaxUpdate"));
                                    }
                                }
                                /*? 60 */
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "REMVR")
                            {
                                // TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
                                if (FCConvert.ToBoolean(rsModules.Get_Fields("RE")))
                                {
                                    if (CheckIfFileExists_2("TWMVRXLS.zip"))
                                    {
                                        vsUpdates.Rows += 1;
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TWMVRXLS");
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2("REMVR"));
                                    }
                                }
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "RXUPLOAD")
                            {
                                // If rsModules.Fields("RX") Then
                                // If CheckIfFileExists("TWRXUpload.zip") Then
                                // vsUpdates.Rows = vsUpdates.Rows + 1
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol) = "TWRXUpload"
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = False
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol) = CreateModuleDescription("RXUpload")
                                // End If
                                // End If
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "EPAY")
                            {
                                /*? 74 */
                            }
                            else if (rsModules.Get_Fields(CreateModuleString(ref counter)))
                            {
                                if (Information.IsDate(rsModules.Get_Fields(CreateModuleString(ref counter) + "Date")))
                                {
                                    if (DateAndTime.DateDiff("d", (DateTime)rsModules.Get_Fields(CreateModuleString(ref counter) + "Date"), DateTime.Today) <= 0)
                                    {
                                        if (Strings.UCase(CreateModuleString(ref counter)) == "BL")
                                            boolHasBilling = true;
                                        if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                        {
                                            vsUpdates.Rows += 1;
                                            vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                            if (!blnAutoCheck)
                                            {
                                                // Or Not blnFromUpdateReminder
                                                vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                            }
                                            else
                                            {
                                                vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                            }
                                            vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                        }
                                    }
                                    else
                                    {
                                        if ((Strings.UCase(CreateModuleString(ref counter)) == "RE") || (Strings.UCase(CreateModuleString(ref counter)) == "PP"))
                                        {
                                            if (boolHasBilling)
                                            {
                                                if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                                {
                                                    vsUpdates.Rows += 1;
                                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                                    if (!blnAutoCheck)
                                                    {
                                                        // Or Not blnFromUpdateReminder
                                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                                    }
                                                    else
                                                    {
                                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                                    }
                                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if ((Strings.UCase(CreateModuleString(ref counter)) == "RE") || (Strings.UCase(CreateModuleString(ref counter)) == "PP"))
                                    {
                                        if (boolHasBilling)
                                        {
                                            if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                            {
                                                vsUpdates.Rows += 1;
                                                vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                                if (!blnAutoCheck)
                                                {
                                                    // Or Not blnFromUpdateReminder
                                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                                }
                                                else
                                                {
                                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                                }
                                                vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                            }
                                        }
                                    }
                                    else
                                    {
                                    }
                                }
                                // If UCase(CreateModuleString(counter)) = "RB" Then
                                // If CheckIfFileExists("TWRB0000.zip") Then
                                // vsUpdates.Rows = vsUpdates.Rows + 1
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol) = "TWRB0000"
                                // If Not blnAutoCheck Or Not blnFromUpdateReminder Then
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = False
                                // Else
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = UpdateModule("RB")
                                // End If
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol) = CreateModuleDescription("RB") & " Data"
                                // End If
                                // End If
                                /*? 91 */
                            }
                            else if ((Strings.UCase(CreateModuleString(ref counter)) == "PP" || Strings.UCase(CreateModuleString(ref counter)) == "RE") && boolHasBilling)
                            {
                                if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                {
                                    vsUpdates.Rows += 1;
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                    if (!blnAutoCheck)
                                    {
                                        // Or Not blnFromUpdateReminder
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                    }
                                    else
                                    {
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                    }
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                }
                            }
                        }
                    }
                }
                if (blnAutoCheck)
                {
                    ShowProgramNotes();
                }
                frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Maximum;
                frmWait.InstancePtr.Unload();
                return;
            }
            catch (Exception ex)
            {
            Err_Connect:
                ;
                if (Strings.Left(Information.Err(ex).Description + "     ", 5) == "12014" && strFTPSitePassword != "trio")
                {
                    strFTPSitePassword = "trio";
                    //TODO SBE
                    //goto TryConnect;
                }
                frmWait.InstancePtr.Unload();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show("Connect failed: " + "\r\n" + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                AddStatusMsg_2("Connect failed: " + Information.Err(ex).Description);
                return;
            ErrorHandler:
                ;
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In form_activate line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                //            switch (vOnErrorGoToLabel) {
                //	default:
                //	case curOnErrorGoToLabel_Default:
                //		// ...
                //		break;
                //	case curOnErrorGoToLabel_Err_Connect:
                //		//? goto Err_Connect;
                //		break;
                //	case curOnErrorGoToLabel_ErrorHandler:
                //		//? goto ErrorHandler;
                //		break;
                //}
            }
        }

        private void frmChilkatDownload_Load(object sender, System.EventArgs e)
        {
            clsDRWrapper rsDefaults = new clsDRWrapper();

            try
            {
                //Scripting.Folder fd;
                bool boolUseTestSite;
                boolUseTestSite = false;

                if (!Directory.Exists("temp"))
                {
                    Directory.CreateDirectory("temp");

                    //fd.Attributes = FileAttributes.Normal;
                }

                strFTPUserToUse = "TRIOUpdatesSQL";
                strFTPUpdateFolderToUse = "TRIOUpdatesSQL";

                if (StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
                {
                    if (MessageBox.Show("Would you like to use the test update folder?", "Use Test Site?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        strFTPUserToUse = "TRIOUpdatesSQLTest";
                        boolUseTestSite = true;
                        strFTPUpdateFolderToUse = "TRIOUpdatesSQLTest";
                    }
                }

                FileCol = 0;
                SelectCol = 1;
                DescriptionCol = 2;
                /*? 8 */
                vsUpdates.ColHidden(FileCol, true);
                vsUpdates.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
                vsUpdates.ColWidth(SelectCol, FCConvert.ToInt32(vsUpdates.Width * 0.2));
                rsDefaults.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");

                if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
                {
	                strFTPSiteAddress = StaticSettings.gGlobalSettings.SFTPSiteAddress;
                    strFTPSiteUser = strFTPUserToUse;
                    strFTPSitePassword = "zfbuf9AB";
                }

                lstMessages.Clear();

                if (modTrioStart.UpdateProgramVersionsFile(boolUseTestSite))
                {
                    blnAutoCheck = true;
                }
                else
                {
                    MessageBox.Show("Unable to find current version information on products.");
                    blnAutoCheck = false;
                }

                // Set temp = New BACKUPRESTORE
                modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
                modGlobalFunctions.SetTRIOColors(this);

                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In form_load line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsDefaults.DisposeOf();
            }
        }

        private void frmChilkatDownload_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                this.Unload();
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (ChilkatFtp1.IsConnected)
                ChilkatFtp1.Disconnect();
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            //MDIParent.InstancePtr.Focus();
        }

        private void mnuDownloadUpdates_Click(object sender, System.EventArgs e)
        {
            cmdDownload_Click();
        }
        // Private Sub FTP1_TransferComplete(ByVal RemoteFileName As String, ByVal LocalFileName As String, ByVal Download As Boolean)
        // If Len(RemoteFileName) > 3 Then
        // If UCase(Right(RemoteFileName, 3)) = "ZIP" Then
        // If Not UnZipFile(LocalFileName) Then
        // boolDownloadError = True
        // End If
        // End If
        // End If
        // End Sub
        // Private Sub TransferProgress(ByVal Bytes As Long, ByVal TotalBytes As Long)
        // Dim dblPerc As Double
        // Dim lngPerc As Long
        // On Error GoTo ErrorHandler
        //
        // If TotalBytes <> 0 Then
        // 2    dblPerc = Bytes / TotalBytes
        // Else
        // dblPerc = 0
        // End If
        // 4    lngPerc = dblPerc * 100
        // 6    If lngPerc > 100 Then lngPerc = 100
        //
        // 8    ProgressBar1.Value = lngPerc
        // Me.Refresh
        // Exit Sub
        // ErrorHandler:
        // MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In FTP1_TransferProgress line " & Erl, vbCritical, "Error"
        // End Sub
        // Private Sub FTP1_TransferStarting(ByVal RemoteFileName As String, ByVal LocalFileName As String, ByVal Download As Boolean, ByVal TotalBytes As Long, Cancel As Boolean)
        // On Error GoTo ErrorHandler
        //
        // 2    ProgressBar1.Value = 0
        // ProgressBar1.Max = TotalBytes
        // 4    ProgressBar1.Max = 100
        //
        // Exit Sub
        // ErrorHandler:
        // MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In transferstarting line " & Erl, vbCritical, "Error"
        // End Sub
        private void mnuPrintNotes_Click(object sender, System.EventArgs e)
        {
            //TODO - add missing report
            //frmReportViewer.InstancePtr.Init(ref rptProgramNotes);
        }

        private void mnuProcessQuit_Click()
        {
            this.Unload();
        }

        private bool UnZipFile(string strFileToUnzip)
        {
            bool UnZipFile = false;
            AbaleZipLibrary.abeError zipreturn;
            bool boolEXEs;
            bool boolVB1s;
            bool boolErr;
            bool boolSpecial = false;
            string strFileNames;
            int intCounter;
            string strTempDir = "";
            //Scripting.File fNewFile;
            // vbPorter upgrade warning: zipResponse As abeError	OnWrite(AbaleZipLibrary.abeError)
            AbaleZipLibrary.abeError zipResponse;
            string strSpecialFileNames;
            clsDRWrapper rsArchive = new clsDRWrapper();
            bool boolBluebookDataUpdate = false;
            string strBlueBookData = "";
            string strDataDir;
            frmBusy fBusy = null;
            // On Error GoTo ErrorHandler
            UnZipFile = false;
            AbaleZip1.FilesToProcess = "";
            boolErr = false;
            boolEXEs = false;
            boolVB1s = false;
            strFileNames = "";
            strSpecialFileNames = "";
            string strSpecialSep;
            strSpecialSep = "";
            if (!pOpen(strFileToUnzip))
            {
                return UnZipFile;
            }
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    strDataDir = Environment.CurrentDirectory;
                    if (Strings.Right(strDataDir, 1) != "\\")
                    {
                        strDataDir += "\\";
                    }
                    // lblPercent.Caption = "Unzipping " & strFileToUnzip
                    fBusy = new frmBusy();
                    fBusy.Message = "Unzipping " + strFileToUnzip;
                    fBusy.StartBusy();
                    for (intCounter = 0; intCounter <= Information.UBound(strZFiles, 1); intCounter++)
                    {
                        //Application.DoEvents();
                        if (Strings.Trim(strZFiles[intCounter]) != string.Empty)
                        {
                            if (Strings.UCase(Strings.Right(strZFiles[intCounter], 3)) != "BAK" && Strings.UCase(Strings.Right(strZFiles[intCounter], 3)) != ("VB1" + Strings.LCase(Strings.Right("    " + strZFiles[intCounter], 4))) != ("json" + Strings.UCase(Strings.Right(strZFiles[intCounter], 3)) != "XLS"))
                            {
                                // strTempDir = Left(CurDir, 1) & ":\TrioData\TrioMast"
                                strTempDir = StaticSettings.gGlobalSettings.MasterPath;
                            }
                            else
                            {
                                strTempDir = Environment.CurrentDirectory;
                            }
                            strTempDir = (Strings.Right(strTempDir, 1) != "\\" ? strTempDir + "\\" : strTempDir);
                            if (Strings.UCase(Strings.Right(strZFiles[intCounter], 3)) != "BAK" && Strings.UCase(Strings.Right(strZFiles[intCounter], 3)) != "XLS" && Strings.LCase(strZFiles[intCounter]) != "harrisepayments.exe" && Strings.LCase(Strings.Right("    " + strZFiles[intCounter], 4)) != "json")
                            {
                                boolEXEs = true;
                            }
                            else if (Strings.LCase(strZFiles[intCounter]) == "harrisepayments.exe")
                            {
                                boolSpecial = true;
                                strSpecialFileNames += strSpecialSep + strZFiles[intCounter];
                                strSpecialSep = ",";
                            }
                            else
                            {
                                boolVB1s = true;
                                if (Strings.UCase(Strings.Right(strZFiles[intCounter], 3)) == "BAK")
                                {
                                    strFileNames += strZFiles[intCounter] + ",";
                                }
                                if (Strings.LCase(Strings.Right("    " + strZFiles[intCounter], 4)) == "json")
                                {
                                    if (Strings.LCase(Strings.Left(strZFiles[intCounter] + "        ", 8)) == "bluebook")
                                    {
                                        //boolBluebookDataUpdate = true;
                                        //strBlueBookData = strZFiles[intCounter];
                                    }
                                }
                            }
                            AbaleZip1.AddFilesToProcess(strZFiles[intCounter]);
                            //ff = new FCFileSystemObject();
                            if (!Directory.Exists(strTempDir))
                            {
                                Directory.CreateDirectory(strTempDir);
                            }
                            // If ff.FileExists(strTempDir & strZFiles(intCounter)) And UCase(Right(strZFiles(intCounter), 3)) = "BAK" Then
                            // Set fNewFile = ff.GetFile(strTempDir & strZFiles(intCounter))
                            // Call fNewFile.Copy(strTempDir & strZFiles(intCounter) & ".old")
                            // End If
                        }
                    }
                    // intCounter
                    if (strFileNames != string.Empty)
                    {
                        strFileNames = Strings.Mid(strFileNames, 1, strFileNames.Length - 1);
                        strFileNames = Strings.Mid(strFileNames, 1, 255);
                    }
                    if (AbaleZip1.FilesToProcess != string.Empty)
                    {
                        // there is at least one file to unzip
                        // exes first
                        modGlobalFunctions.AddCYAEntry_8("GN", "Performed Update", strFileNames);
                        AbaleZip1.BasePath = "";
                        if (boolEXEs)
                        {
                            // strTempDir = Left(CurDir, 1) & ":\TrioData\TrioMast"
                            strTempDir = StaticSettings.gGlobalSettings.MasterPath;
                            AbaleZip1.UnzipToFolder = strTempDir;
                            AbaleZip1.FilesToExclude = "*.bak";
                            AbaleZip1.FilesToExclude = "*.xls";
                            AbaleZip1.FilesToExclude = "*.json";
                            AbaleZip1.FilesToExclude = "HarrisEpayments.exe";
                            zipResponse = AbaleZip1.Unzip();
                            if (zipResponse != AbaleZipLibrary.abeError.aerSuccess)
                            {
                                fBusy.StopBusy();
                                MessageBox.Show("Error saving file(s).");
                                boolErr = true;
                            }
                            else
                            {
                            }
                        }
                        // then database files
                        if (boolVB1s)
                        {
                            boolErr = false;
                            AbaleZip1.FilesToExclude = "";
                            AbaleZip1.AddFilesToExclude("*.exe");
                            AbaleZip1.AddFilesToExclude("*.cnt");
                            AbaleZip1.AddFilesToExclude("*.hlp");
                            AbaleZip1.AddFilesToExclude("TWRE0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWPP0000.vb1");
                            AbaleZip1.AddFilesToExclude("twCK0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWPY0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWBD0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWBL0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWGN0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWUT0000.vb1");
                            AbaleZip1.AddFilesToExclude("TWCR0000.vb1");
                            // strTempDir = temp.GetBackupPath(rsArchive.ConnectionInformation("SystemSettings"))
                            strTempDir = Environment.CurrentDirectory;
                            AbaleZip1.UnzipToFolder = strTempDir;
                            zipResponse = AbaleZip1.Unzip();
                            if (zipResponse != AbaleZipLibrary.abeError.aerSuccess)
                            {
                                fBusy.StopBusy();
                                MessageBox.Show("Error saving data file(s).");
                                boolErr = true;
                            }
                            else
                            {
                                // Dim strBackupPath As String
                                // 
                                // Set temp = New BACKUPRESTORE
                                // strBackupPath = strTempDir & "\" & strFileNames
                                // 
                                // Call temp.Restore(rsArchive.ConnectionInformation("SystemSettings"), rsArchive.GetFullDBName("BlueBook"), strBackupPath, "", 0)
                                //if (boolBluebookDataUpdate)
                                //{
                                //    cBlueBookExportImport bbex = new cBlueBookExportImport();
                                //    // Set fBusy = New frmBusy
                                //    fBusy.Message = "Importing Blue Book data";
                                //    // fBusy.StartBusy
                                //    // fBusy.Show
                                //    bbex.ImportBlueBook(strDataDir + strBlueBookData);
                                //    // fBusy.StopBusy
                                //    // Set fBusy = Nothing
                                //}
                            }
                        }
                        if (boolSpecial)
                        {
                            AbaleZip1.FilesToProcess = "";
                            AbaleZip1.FilesToExclude = "";
                            string[] strAry = null;
                            strAry = Strings.Split(strSpecialFileNames, ",", -1, CompareConstants.vbBinaryCompare);
                            int x;
                            for (x = 0; x <= Information.UBound(strAry, 1); x++)
                            {
                                //Application.DoEvents();
                                AbaleZip1.AddFilesToProcess(strAry[x]);
                            }
                            // x
                            strTempDir = Environment.CurrentDirectory;
                            AbaleZip1.UnzipToFolder = strTempDir;
                            zipResponse = AbaleZip1.Unzip();
                            if (zipResponse != AbaleZipLibrary.abeError.aerSuccess)
                            {
                                fBusy.StopBusy();
                                MessageBox.Show("Error saving file(s).");
                                boolErr = true;
                            }
                        }
                    }
                    UnZipFile = true;
                    fBusy.StopBusy();
                    fBusy.Unload();
                    /*- fBusy = null; */
                    //return UnZipFile;
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    if (!(fBusy == null))
                    {
                        fBusy.StopBusy();
                        fBusy.Unload();
                        /*- fBusy = null; */
                    }
                    int lngerrornum;
                    string strErrmess;
                    strErrmess = Information.Err(ex).Description;
                    lngerrornum = Information.Err(ex).Number;
                    Information.Err(ex).Raise(lngerrornum, null, strErrmess, null, null);
                }
                FCUtils.UnlockUserInterface();
            });
            fBusy.Show(FormShowEnum.Modal);
            return UnZipFile;
        }

        private void mnuSaveExit_Click()
        {
        }

        private void optAll_CheckedChanged(object sender, System.EventArgs e)
        {
            int counter;
            try
            {
                // On Error GoTo ErrorHandler
                fraSpecificPrograms.Enabled = false;
                if (vsUpdates.Rows > 0)
                {
                    for (counter = 0; counter <= vsUpdates.Rows - 1; counter++)
                    {
                        vsUpdates.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In optAll_Click line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void optNew_CheckedChanged(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fraSpecificPrograms.Enabled = true;
                SelectUpdates();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In OptNew_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void optSelected_CheckedChanged(object sender, System.EventArgs e)
        {
            fraSpecificPrograms.Enabled = true;
        }

        private bool DLFile_20(string FileName, string downloadpath, string downloadas)
        {
            return DLFile(ref FileName, ref downloadpath, ref downloadas);
        }

        private bool DLFile(ref string FileName, ref string downloadpath, ref string downloadas)
        {
            bool DLFile = false;
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_ErrorHandler = 1;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
            /* On Error GoTo ErrorHandler */
            StartOver:
                ;
                downloadfile = downloadpath + "\\" + downloadas;
                if (FCFileSystem.Dir(downloadpath + "\\" + downloadas, 0) != "")
                {
                    // If UCase(downloadas) = "TWRB0000.VB1" Then
                    // Dim fs As New FCFileSystemObject
                    // If Not fs.FileExists("TWRBbackup.vb1") Then
                    // fs.CopyFile "twrb0000.vb1", "TWRBBackup.vb1"
                    // End If
                    // End If
                    FCFileSystem.Kill(downloadpath + "\\" + downloadas);
                }
                // 8    FTP1.GetFile Filename, downloadpath, downloadas
                bool success;
                ChilkatFtp1.AutoGetSizeForProgress = true;
                success = ChilkatFtp1.GetFile(FileName, downloadfile);
                if (!success)
                {
                    MessageBox.Show(ChilkatFtp1.LastErrorText);
                    return DLFile;
                }
                if (downloadfile.Length > 3)
                {
                    if (Strings.UCase(Strings.Right(downloadfile, 3)) == "ZIP")
                    {
                        if (!UnZipFile(downloadfile))
                        {
                            boolDownloadError = true;
                        }
                    }
                }
                DLFile = true;
                return DLFile;
            }
            catch (Exception ex)
            {
            ErrorHandler:
                ;
                if (Information.Erl() == 8 && Information.Err(ex).Number == -2147217408)
                {
                TryConnect:
                    ;
                    /*? On Error Resume Next  */// FTP1.Connect strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21
                    ChilkatFtp1.Hostname = strFTPSiteAddress;
                    ChilkatFtp1.Username = strFTPSiteUser;
                    ChilkatFtp1.Password = strFTPSitePassword;
                    ChilkatFtp1.Connect();
                    if (Information.Err(ex).Number != 0)
                    {
                        // give up
                        if (Strings.Left(Information.Err(ex).Description + "     ", 5) == "12014" && strFTPSitePassword != "trio")
                        {
                            strFTPSitePassword = "trio";
                            goto TryConnect;
                        }
                        MessageBox.Show("Connection Failed in DLFile" + "\r\n" + "Cannot download " + FileName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return DLFile;
                    }
                    vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                    /* On Error GoTo ErrorHandler *///TODO SBE
                                                    //goto StartOver;
                }
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In DLFile " + "line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                //            switch (vOnErrorGoToLabel) {
                //	default:
                //	case curOnErrorGoToLabel_Default:
                //		// ...
                //		break;
                //	case curOnErrorGoToLabel_ErrorHandler:
                //		//? goto ErrorHandler;
                //		break;
                //	//case curOnErrorGoToLabel_ErrorHandler:
                //	//	//? goto ErrorHandler;
                //	//	break;
                //}
            }
            return DLFile;
        }

        private string CreateModuleString(ref int intRow, short intStartRow = 0)
        {
            string CreateModuleString = "";
            if (intRow == 0 + intStartRow)
            {
                CreateModuleString = "AR";
            }
            else if (intRow == 1 + intStartRow)
            {
                CreateModuleString = "RB";
            }
            else if (intRow == 2 + intStartRow)
            {
                CreateModuleString = "BL";
            }
            else if (intRow == 3 + intStartRow)
            {
                CreateModuleString = "BD";
            }
            else if (intRow == 4 + intStartRow)
            {
                CreateModuleString = "CR";
                // else if (intRow == 5 + intStartRow
                // CreateModuleString = "EPAY"
            }
            else if (intRow == 5 + intStartRow)
            {
                CreateModuleString = "CK";
            }
            else if (intRow == 6 + intStartRow)
            {
                CreateModuleString = "CE";
            }
            else if (intRow == 7 + intStartRow)
            {
                CreateModuleString = "CL";
                //break;
            }
            else if (intRow == 8 + intStartRow)
            {
                CreateModuleString = "E9";
                //break;
            }
            else if (intRow == 9 + intStartRow)
            {
                CreateModuleString = "FA";
                //break;
            }
            else if (intRow == 10 + intStartRow)
            {
                CreateModuleString = "GN";
                //break;
            }
            else if (intRow == 11 + intStartRow)
            {
                CreateModuleString = "HR";
                //break;
            }
            else if (intRow == 12 + intStartRow)
            {
                CreateModuleString = "IV";
                //break;
            }
            else if (intRow == 13 + intStartRow)
            {
                CreateModuleString = "RE";
                //break;
            }
            else if (intRow == 14 + intStartRow)
            {
                CreateModuleString = "REMVR";
                //break;
            }
            else if (intRow == 15 + intStartRow)
            {
                CreateModuleString = "CM";
                //break;
            }
            else if (intRow == 16 + intStartRow)
            {
                CreateModuleString = "RH";
                //break;
            }
            else if (intRow == 17 + intStartRow)
            {
                CreateModuleString = "SK";
                //break;
            }
            else if (intRow == 18 + intStartRow)
            {
                CreateModuleString = "XF";
                //break;
            }
            else if (intRow == 19 + intStartRow)
            {
                CreateModuleString = "MV";
                //break;
            }
            else if (intRow == 20 + intStartRow)
            {
                CreateModuleString = "RR";
                //break;
            }
            else if (intRow == 21 + intStartRow)
            {
                CreateModuleString = "PP";
                //break;
            }
            else if (intRow == 22 + intStartRow)
            {
                CreateModuleString = "PY";
                //break;
            }
            else if (intRow == 23 + intStartRow)
            {
                CreateModuleString = "PYTaxUpdate";
                //break;
            }
            else if (intRow == 24 + intStartRow)
            {
                CreateModuleString = "TS";
                //break;
            }
            else if (intRow == 25 + intStartRow)
            {
                CreateModuleString = "UT";
                //break;
            }
            else if (intRow == 26 + intStartRow)
            {
                CreateModuleString = "VR";
                //break;
            }
            else if (intRow == 27 + intStartRow)
            {
                CreateModuleString = "RXUpload";
                //break;
            }
            return CreateModuleString;
        }

        private string CreateModuleDescription_2(string strCode)
        {
            return CreateModuleDescription(ref strCode);
        }

        private string CreateModuleDescription(ref string strCode)
        {
            string CreateModuleDescription = "";
            switch (strCode)
            {
                case "AR":
                    CreateModuleDescription = "Accounts Receivable";

                    break;
                case "RB":
                    CreateModuleDescription = "Blue Book";

                    break;
                case "BL":
                    CreateModuleDescription = "Billing";

                    break;
                case "BD":
                    CreateModuleDescription = "Budgetary";

                    break;
                case "CR":
                    CreateModuleDescription = "Cash Receipts";

                    break;
                case "EPAY":
                    CreateModuleDescription = "E-Payment Interface";

                    break;
                case "CK":
                    CreateModuleDescription = "Clerk";

                    break;
                case "CE":
                    CreateModuleDescription = "Code Enforcement";

                    break;
                case "CL":
                    CreateModuleDescription = "Collections";

                    break;
                case "E9":
                    CreateModuleDescription = "E911";

                    break;
                case "FA":
                    CreateModuleDescription = "Fixed Assets";

                    break;
                case "GN":
                    CreateModuleDescription = "General Entry";

                    break;
                case "HR":
                    CreateModuleDescription = "Human Resources";

                    break;
                case "IV":
                    CreateModuleDescription = "Inventory";

                    break;
                case "RE":
                    CreateModuleDescription = "Real Estate";

                    break;
                case "REMVR":
                    CreateModuleDescription = "Real Estate MVR Excel File";

                    break;
                case "RH":
                    CreateModuleDescription = "Real Estate Handheld";

                    break;
                case "SK":
                    CreateModuleDescription = "Real Estate Sketching";

                    break;
                case "XF":
                    CreateModuleDescription = "Real Estate Transfer";

                    break;
                case "MV":
                    CreateModuleDescription = "Motor Vehicle";

                    break;
                case "RR":
                    CreateModuleDescription = "MV Rapid Renewal";

                    break;
                case "PP":
                    CreateModuleDescription = "Personal Property";

                    break;
                case "PY":
                    CreateModuleDescription = "Payroll";

                    break;
                case "PYTaxUpdate":
                    CreateModuleDescription = "Payroll Tax Table Update";

                    break;
                case "TS":
                    CreateModuleDescription = "Tax Service";

                    break;
                case "UT":
                    CreateModuleDescription = "Utility Billing";

                    break;
                case "VR":
                    CreateModuleDescription = "Voter Registration";

                    break;
                case "CM":
                    CreateModuleDescription = "Commercial Assessment";

                    break;
                case "RXUpload":
                    CreateModuleDescription = "Real Estate Info Extract Upload";

                    break;
            }
            return CreateModuleDescription;
        }

        private bool RefreshSocket()
        {
            bool RefreshSocket = false;
            //TODO SBE
            //Collection loDirCollection = new Collection();
            //DevPowerFTP.dpFTPListItem loDirCollectionEntry = new DevPowerFTP.dpFTPListItem();
            int intFilesAndFolders;
            strFolders = new string[0 + 1];
            strFiles = new string[0 + 1];
            try
            {
                // On Error GoTo ErrorHandler
                intFolders = 0;
                intFiles = 0;
                intFilesAndFolders = 0;
                if (!(ChilkatFtp1.IsConnected))
                    return RefreshSocket;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                intFilesAndFolders = ChilkatFtp1.NumFilesAndDirs;
                if (intFilesAndFolders < 0)
                {
                    MessageBox.Show(ChilkatFtp1.LastErrorText);
                    return RefreshSocket;
                }
                int i;
                for (i = 0; i <= intFilesAndFolders - 1; i++)
                {
                    //if (ChilkatFtp1.GetIsDirectory(i)==1) {
                    if (ChilkatFtp1.GetIsDirectory(i))
                    {
                        Array.Resize(ref strFolders, intFolders + 1);
                        intFolders += 1;
                        strFolders[intFolders - 1] = ChilkatFtp1.GetFilename(i);
                    }
                    else
                    {
                        Array.Resize(ref strFiles, intFiles + 1);
                        intFiles += 1;
                        strFiles[intFiles - 1] = ChilkatFtp1.GetFilename(i);
                    }
                }
                // i
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                return RefreshSocket;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In RefreshSocket line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return RefreshSocket;
        }

        private bool CheckIfUpdateFolderExists()
        {
            bool CheckIfUpdateFolderExists = false;
            int counter;
            try
            {
                // On Error GoTo ErrorHandler
                CheckIfUpdateFolderExists = false;
                RefreshSocket();
                if (intFolders > 0)
                {
                    for (counter = 0; counter <= intFolders - 1; counter++)
                    {
                        // If strFolders(counter) = "TRIOUpdatesSQL" Then
                        if (strFolders[counter] == strFTPUpdateFolderToUse)
                        {
                            CheckIfUpdateFolderExists = true;
                            break;
                        }
                    }
                }
                return CheckIfUpdateFolderExists;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CheckIfUpdateFolderExists line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CheckIfUpdateFolderExists;
        }

        private bool CheckIfFileExists_2(string strFilename)
        {
            return CheckIfFileExists(ref strFilename);
        }

        private bool CheckIfFileExists(ref string strFilename)
        {
            bool CheckIfFileExists = false;
            int counter;
            try
            {
                // On Error GoTo ErrorHandler
                CheckIfFileExists = false;
                if (intFiles > 0)
                {
                    for (counter = 0; counter <= intFiles - 1; counter++)
                    {
                        if (Strings.UCase(strFiles[counter]) == Strings.UCase(strFilename))
                        {
                            CheckIfFileExists = true;
                            break;
                        }
                    }
                }
                return CheckIfFileExists;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CheckIfFileExists line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CheckIfFileExists;
        }

        private void vsUpdates_ClickEvent(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (vsUpdates.Row >= 0)
                {
                    if (vsUpdates.TextMatrix(vsUpdates.Row, SelectCol) == FCConvert.ToString(false))
                    {
                        vsUpdates.TextMatrix(vsUpdates.Row, SelectCol, FCConvert.ToString(true));
                    }
                    else
                    {
                        vsUpdates.TextMatrix(vsUpdates.Row, SelectCol, FCConvert.ToString(false));
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsUpdates_Click line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void vsUpdates_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (vsUpdates.Row >= 0)
                {
                    if (e.KeyChar == 32)
                    {
                        e.KeyChar = '\0';
                        if (vsUpdates.TextMatrix(vsUpdates.Row, SelectCol) == FCConvert.ToString(false))
                        {
                            vsUpdates.TextMatrix(vsUpdates.Row, SelectCol, FCConvert.ToString(true));
                        }
                        else
                        {
                            vsUpdates.TextMatrix(vsUpdates.Row, SelectCol, FCConvert.ToString(false));
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In vsUpdates_KeyPress line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private string ReturnProperCase_2(string strFilename)
        {
            return ReturnProperCase(ref strFilename);
        }

        private string ReturnProperCase(ref string strFilename)
        {
            string ReturnProperCase = "";
            int counter;
            try
            {
                // On Error GoTo ErrorHandler
                ReturnProperCase = strFilename;
                if (intFiles > 0)
                {
                    for (counter = 0; counter <= intFiles - 1; counter++)
                    {
                        if (Strings.UCase(strFiles[counter]) == Strings.UCase(strFilename))
                        {
                            ReturnProperCase = strFiles[counter];
                            break;
                        }
                    }
                }
                return ReturnProperCase;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ReturnProperCase Line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return ReturnProperCase;
        }

        private void FTP1_ServerResponse(string Message)
        {
            AddStatusMsg(ref Message);
        }

        private void AddStatusMsg_2(string Message)
        {
            AddStatusMsg(ref Message);
        }

        private void AddStatusMsg(ref string Message)
        {
            try
            {
                // On Error GoTo ErrorHandler
                lstMessages.AddItem(Message);
                lstMessages.SetSelected(lstMessages.ListCount - 1, true);
                lstMessages.Refresh();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AddStatusMsg line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private bool UpdateModule_2(string strModule)
        {
            return UpdateModule(ref strModule);
        }

        private bool UpdateModule(ref string strModule)
        {
            bool UpdateModule = false;
            string[] strInfo = null;
            //FCFileSystemObject fs = new FCFileSystemObject();
            StreamReader tsInfo;
            string strFileInfo = "";
            CFileVersionInfo filVersionInfo = new CFileVersionInfo();
            clsDRWrapper rsModuleInfo = new clsDRWrapper();
            string strCurVersion = "";
            string[] strVersionInfo = null;
            string strNewVersion = "";
            string strMaster;
            try
            {
                // On Error GoTo ErrorHandler
                UpdateModule = false;
                strMaster = StaticSettings.gGlobalSettings.MasterPath;
                if (strMaster != "" && Strings.Right("\\" + strMaster, 1) != "\\")
                {
                    strMaster += "\\";
                }
                rsModuleInfo.OpenRecordset("SELECT * FROM Modules", "systemsettings");
                // loop through the file to see if any of the programs we have in TRIOMast do not match the current version on the FTP site
                tsInfo = File.OpenText("ProgramVersions.csv");
                //, IOMode.ForReading, false, Tristate.TristateUseDefault);
                while (tsInfo.EndOfStream == false)
                {
                    strFileInfo = tsInfo.ReadLine();
                    if (Strings.Left(strFileInfo, 2) == strModule)
                    {
                        strInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
                        if (strInfo[0] == "GN")
                        {
                            // 18                If fs.FileExists(Left(CurDir, 1) & ":\TRIOData\TRIOMast\TWGNENTY.exe") Then
                            if (File.Exists(strMaster + "TWGNENTY.exe"))
                            {
                                // 20                    filVersionInfo.FullPathName = Left(CurDir, 1) & ":\TRIOData\TRIOMast\TWGNENTY.exe"
                                filVersionInfo.FullPathName = strMaster + "TWGNENTY.exe";
                                if (filVersionInfo.Available)
                                {
                                    strCurVersion = filVersionInfo.FileVersion + ".0.0";
                                    strVersionInfo = Strings.Split(strCurVersion, ".", -1, CompareConstants.vbTextCompare);
                                    strCurVersion = Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[0])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[1])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[2])), "0000");
                                    strVersionInfo = Strings.Split(strInfo[1], ".", -1, CompareConstants.vbTextCompare);
                                    strNewVersion = Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[0])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[1])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[2])), "0000");
                                    if (string.Compare(strCurVersion, strNewVersion) < 0)
                                    {
                                        UpdateModule = true;
                                        break;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                UpdateModule = true;
                                break;
                            }
                        }
                        else
                        {
                            if ((rsModuleInfo.Get_Fields(strInfo[0]) && rsModuleInfo.Get_Fields(strInfo[0] + "Date") > DateTime.Today.ToOADate()) || ((strInfo[0] == "RE" || strInfo[0] == "PP") && rsModuleInfo.Get_Fields_Boolean("BL") && rsModuleInfo.Get_Fields_DateTime("BLDate").ToOADate() > DateTime.Today.ToOADate()))
                            {
                                // 38                    If fs.FileExists(Left(CurDir, 1) & ":\TRIOData\TRIOMast\TW" & strInfo(0) & "0000.exe") Then
                                if (File.Exists(strMaster + "TW" + strInfo[0] + "0000.exe"))
                                {
                                    // 40                        filVersionInfo.FullPathName = Left(CurDir, 1) & ":\TRIOData\TRIOMast\TW" & strInfo(0) & "0000.exe"
                                    filVersionInfo.FullPathName = strMaster + "TW" + strInfo[0] + "0000.exe";
                                    if (filVersionInfo.Available)
                                    {
                                        strCurVersion = filVersionInfo.FileVersion + ".0.0";
                                        strVersionInfo = Strings.Split(strCurVersion, ".", -1, CompareConstants.vbTextCompare);
                                        strCurVersion = Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[0])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[1])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[2])), "0000");
                                        strVersionInfo = Strings.Split(strInfo[1], ".", -1, CompareConstants.vbTextCompare);
                                        /*? 52 */
                                        strNewVersion = Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[0])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[1])), "0000") + "." + Strings.Format(Conversion.Val(Strings.Trim(strVersionInfo[2])), "0000");
                                        if (string.Compare(strCurVersion, strNewVersion) < 0)
                                        {
                                            UpdateModule = true;
                                            break;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    UpdateModule = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            EndCheck:
                ;
                return UpdateModule;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In UpdateModule Line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return UpdateModule;
        }

        private void ShowProgramNotes()
        {
            dynamic[] ulInfo = null;
            // - "AutoDim"
            string[] strInfo = null;
            //FCFileSystemObject fs = new FCFileSystemObject();
            StreamReader tsInfo;
            clsDRWrapper rsModuleInfo = new clsDRWrapper();
            string strFileInfo = "";
            int counter;
            modTrioStart.Statics.intArrayCounter = -1;
            ulInfo = new object[0 + 1];
            txtProgramNotes.Text = "";
            txtProgramNotes.Font = new Font("Tahoma", txtProgramNotes.Font.Size);
            txtProgramNotes.Font = new Font(txtProgramNotes.Font.Name, 9);
            rsModuleInfo.OpenRecordset("SELECT * FROM Modules", "systemsettings");
            // loop through the file to see if any of the programs we have in TRIOMast do not match the current version on the FTP site
            tsInfo = File.OpenText("ProgramVersions.csv");
            //, IOMode.ForReading, false, Tristate.TristateUseDefault);
            while (tsInfo.EndOfStream == false)
            {
                strFileInfo = tsInfo.ReadLine();
                if (Strings.Trim(strFileInfo) != "")
                {
                    strInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
                    if (strInfo[0] == "GN")
                    {
                        if (Information.UBound(strInfo, 1) >= 2)
                        {
                            if (Strings.Trim(strInfo[2]) != "")
                            {
                                txtProgramNotes.Text = txtProgramNotes.Text + "General Entry";
                                if (modTrioStart.Statics.intArrayCounter == -1)
                                {
                                    modTrioStart.Statics.intArrayCounter = 0;
                                    ulInfo[modTrioStart.Statics.intArrayCounter].intStart = txtProgramNotes.Text.Length - "General Entry".Length;
                                    ulInfo[modTrioStart.Statics.intArrayCounter].intLength = "General Entry".Length;
                                }
                                else
                                {
                                    modTrioStart.Statics.intArrayCounter += 1;
                                    Array.Resize(ref ulInfo, modTrioStart.Statics.intArrayCounter + 1);
                                    ulInfo[modTrioStart.Statics.intArrayCounter].intStart = txtProgramNotes.Text.Length - "General Entry".Length;
                                    ulInfo[modTrioStart.Statics.intArrayCounter].intLength = "General Entry".Length;
                                }
                                for (counter = 2; counter <= Information.UBound(strInfo, 1); counter++)
                                {
                                    if (Strings.Trim(strInfo[counter]) != "")
                                    {
                                        txtProgramNotes.Text = txtProgramNotes.Text + "\r\n";
                                        txtProgramNotes.Text = txtProgramNotes.Text + "   -- " + Strings.Trim(strInfo[counter]) + "\r\n";
                                    }
                                }
                                txtProgramNotes.Text = txtProgramNotes.Text + "\r\n";
                            }
                        }
                    }
                    else if (strInfo[0] == "Notes")
                    {
                        if (Strings.Trim(strInfo[2]) != "")
                        {
                            txtProgramNotes.Text = txtProgramNotes.Text + "General Information";
                            if (modTrioStart.Statics.intArrayCounter == -1)
                            {
                                modTrioStart.Statics.intArrayCounter = 0;
                                ulInfo[modTrioStart.Statics.intArrayCounter].intStart = txtProgramNotes.Text.Length - "General Information".Length;
                                ulInfo[modTrioStart.Statics.intArrayCounter].intLength = "General Information".Length;
                            }
                            else
                            {
                                modTrioStart.Statics.intArrayCounter += 1;
                                Array.Resize(ref ulInfo, modTrioStart.Statics.intArrayCounter + 1);
                                ulInfo[modTrioStart.Statics.intArrayCounter].intStart = txtProgramNotes.Text.Length - "General Information".Length;
                                ulInfo[modTrioStart.Statics.intArrayCounter].intLength = "General Information".Length;
                            }
                            for (counter = 2; counter <= Information.UBound(strInfo, 1); counter++)
                            {
                                if (Strings.Trim(strInfo[counter]) != "")
                                {
                                    txtProgramNotes.Text = txtProgramNotes.Text + "\r\n";
                                    txtProgramNotes.Text = txtProgramNotes.Text + "   -- " + Strings.Trim(strInfo[counter]) + "\r\n";
                                }
                            }
                            txtProgramNotes.Text = txtProgramNotes.Text + "\r\n";
                        }
                    }
                    else
                    {
                        if (rsModuleInfo.Get_Fields(strInfo[0]) && rsModuleInfo.Get_Fields(strInfo[0] + "Date") > DateTime.Today.ToOADate())
                        {
                            if (Information.UBound(strInfo, 1) >= 2)
                            {
                                if (Strings.Trim(strInfo[2]) != "")
                                {
                                    txtProgramNotes.Text = txtProgramNotes.Text + CreateModuleDescription(ref strInfo[0]);
                                    if (modTrioStart.Statics.intArrayCounter == -1)
                                    {
                                        modTrioStart.Statics.intArrayCounter = 0;
                                        ulInfo[modTrioStart.Statics.intArrayCounter].intStart = txtProgramNotes.Text.Length - CreateModuleDescription(ref strInfo[0]).Length;
                                        ulInfo[modTrioStart.Statics.intArrayCounter].intLength = CreateModuleDescription(ref strInfo[0]).Length;
                                    }
                                    else
                                    {
                                        modTrioStart.Statics.intArrayCounter += 1;
                                        Array.Resize(ref ulInfo, modTrioStart.Statics.intArrayCounter + 1);
                                        ulInfo[modTrioStart.Statics.intArrayCounter].intStart = txtProgramNotes.Text.Length - CreateModuleDescription(ref strInfo[0]).Length;
                                        ulInfo[modTrioStart.Statics.intArrayCounter].intLength = CreateModuleDescription(ref strInfo[0]).Length;
                                    }
                                    for (counter = 2; counter <= Information.UBound(strInfo, 1); counter++)
                                    {
                                        if (Strings.Trim(strInfo[counter]) != "")
                                        {
                                            txtProgramNotes.Text = txtProgramNotes.Text + "\r\n";
                                            txtProgramNotes.Text = txtProgramNotes.Text + "   -- " + Strings.Trim(strInfo[counter]) + "\r\n";
                                        }
                                    }
                                    txtProgramNotes.Text = txtProgramNotes.Text + "\r\n";
                                }
                            }
                        }
                    }
                }
            }
            if (modTrioStart.Statics.intArrayCounter != -1)
            {
                for (counter = 0; counter <= modTrioStart.Statics.intArrayCounter; counter++)
                {
                    txtProgramNotes.SelStart = ulInfo[counter].intStart;
                    txtProgramNotes.SelLength = ulInfo[counter].intLength;
                    //txtProgramNotes.SelUnderline = true;
                    //txtProgramNotes.SelBold = true;
                }
            }
            txtProgramNotes.SelStart = 1;
            txtProgramNotes.SelLength = 0;
        EndCheck:
            ;
        }

        private bool pOpen(string sFIle)
        {
            bool pOpen = false;
            int i;
            string sIcon = "";
            ListViewItem itmX;
            //AbaleZipLibrary.AbaleZipItemsClass abObj = new AbaleZipLibrary.AbaleZipItemsClass();
            AbaleZipLibrary.AbaleZipItemsClass abObj = null;
            // vbPorter upgrade warning: abFormat As abeContentsFormat	OnWrite(AbaleZipLibrary.abeContentsFormat)
            AbaleZipLibrary.abeContentsFormat abFormat;
            // vbPorter upgrade warning: ZipErr As abeError	OnWrite(AbaleZipLibrary.abeError)
            AbaleZipLibrary.abeError ZipErr;
            string strTemp = "";
            try
            {
                // On Error GoTo ErrorHandler
                pOpen = false;
                FCUtils.EraseSafe(strZFiles);
                AbaleZip1.ZipFilename = sFIle;
                abFormat = AbaleZipLibrary.abeContentsFormat.acfCollection;
                ZipErr = AbaleZip1.GetZipContents(abObj, abFormat);
                if (ZipErr != AbaleZipLibrary.abeError.aerSuccess)
                {
                    // unsuccessful
                    strTemp = "Unknown Error";
                    switch (ZipErr)
                    {
                        case AbaleZipLibrary.abeError.aerBusy:
                            strTemp = "Busy";

                            break;
                        case AbaleZipLibrary.abeError.aerCannotAccessArray:
                            strTemp = "Cannot Access Array";

                            break;
                        case AbaleZipLibrary.abeError.aerCannotUpdateAndSpan:
                            strTemp = "Cannot Update and Span";

                            break;
                        case AbaleZipLibrary.abeError.aerCannotUpdateSpanned:
                            strTemp = "Cannot Update Spanned";

                            break;
                        case AbaleZipLibrary.abeError.aerCreateTempFile:
                            strTemp = "Create Temp File";

                            break;
                        case AbaleZipLibrary.abeError.aerDiskNotEmptyAbort:
                            strTemp = "Disk Not Empty Abort";

                            break;
                        case AbaleZipLibrary.abeError.aerEmptyZipFile:
                            strTemp = "Empty Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerEndOfZipFile:
                            strTemp = "End of Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerFilesSkipped:
                            strTemp = "Files Skipped";

                            break;
                        case AbaleZipLibrary.abeError.aerInsertDiskAbort:
                            strTemp = "Insert Disk Abort";

                            break;
                        case AbaleZipLibrary.abeError.aerInternalError:
                            strTemp = "Internal Error";

                            break;
                        case AbaleZipLibrary.abeError.aerInvalidArrayDimensions:
                            strTemp = "Invalid Array Dimensions";

                            break;
                        case AbaleZipLibrary.abeError.aerInvalidArrayType:
                            strTemp = "Invalid Array Type";

                            break;
                        case AbaleZipLibrary.abeError.aerInvalidSfxProperty:
                            strTemp = "Invalid Sfx Property";

                            break;
                        case AbaleZipLibrary.abeError.aerMemory:
                            strTemp = "Memory Error";

                            break;
                        case AbaleZipLibrary.abeError.aerMoveTempFile:
                            strTemp = "Move Temp File";

                            break;
                        case AbaleZipLibrary.abeError.aerNotAZipFile:
                            strTemp = "Not a Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerNothingToDo:
                            strTemp = "Nothing to Do";

                            break;
                        case AbaleZipLibrary.abeError.aerNotLicensed:
                            strTemp = "Not Licensed";

                            break;
                        case AbaleZipLibrary.abeError.aerOpenZipFile:
                            strTemp = "Open Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerProcessStarted:
                            strTemp = "Process Started";

                            break;
                        case AbaleZipLibrary.abeError.aerReadSfxBinary:
                            strTemp = "Read Sfx Binary";

                            break;
                        case AbaleZipLibrary.abeError.aerReadZipFile:
                            strTemp = "Read Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerRemoveWithoutTemp:
                            strTemp = "Remove Without Temp";

                            break;
                        case AbaleZipLibrary.abeError.aerSeekInZipFile:
                            strTemp = "Seek In Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerSfxBinaryNotFound:
                            strTemp = "Sfx Binary Not Found";

                            break;
                        case AbaleZipLibrary.abeError.aerSplitSizeTooSmall:
                            strTemp = "Split Size Too Small";

                            break;
                        case AbaleZipLibrary.abeError.aerSuccess:
                            strTemp = "No Error";

                            break;
                        case AbaleZipLibrary.abeError.aerUninitializedArray:
                            strTemp = "Uninitialized Array";

                            break;
                        case AbaleZipLibrary.abeError.aerUninitializedString:
                            strTemp = "Uninitialized String";

                            break;
                        case AbaleZipLibrary.abeError.aerUnsupportedDataType:
                            strTemp = "Unsupported Data Type";

                            break;
                        case AbaleZipLibrary.abeError.aerUserAbort:
                            strTemp = "User Abort";

                            break;
                        case AbaleZipLibrary.abeError.aerWarnings:
                            strTemp = "Warnings";

                            break;
                        case AbaleZipLibrary.abeError.aerWriteTempZipFile:
                            strTemp = "Write Temp Zip File";

                            break;
                        case AbaleZipLibrary.abeError.aerWriteZipFile:
                            strTemp = "Write Zip File";

                            break;
                    }
                    strTemp = AbaleZip1.GetErrorDescription(AbaleZipLibrary.abeValueType.avtError, FCConvert.ToInt32(ZipErr));
                    if (ZipErr == AbaleZipLibrary.abeError.aerInsertDiskAbort)
                    {
                        strTemp = "Corrupt file or the wrong file could have been chosen.";
                    }
                    strTemp = "";
                    string tFile = "";
                    tFile = Path.GetFileName(sFIle);
                    if (tFile.Length >= 12)
                    {
                        var filePath = Strings.UCase(tFile);

                        switch (filePath)
                        {
                            case "TWRE0000.ZIP":
                                strTemp = "Real Estate";

                                break;
                            case "TWCR0000.ZIP":
                                strTemp = "Cash Receipting";

                                break;
                            case "TWCL0000.ZIP":
                                strTemp = "Collections";

                                break;
                            case "TWCK0000.ZIP":
                                strTemp = "Clerk";

                                break;
                            case "TWPY0000.ZIP":
                                strTemp = "Payroll";

                                break;
                            case "TWBL0000.ZIP":
                                strTemp = "Billing";

                                break;
                            case "TWRB0000.ZIP":
                                strTemp = "Redbook";

                                break;
                            case "TWMV0000.ZIP":
                                strTemp = "Motor Vehicle";

                                break;
                            case "TWVR0000.ZIP":
                                strTemp = "Voter Registration";

                                break;
                            case "TWFA0000.ZIP":
                                strTemp = "Fixed Assets";

                                break;
                            case "TWPP0000.ZIP":
                                strTemp = "Personal Property";

                                break;
                            case "TWUT0000.ZIP":
                                strTemp = "Utility Billing";

                                break;
                            case "TWBD0000.ZIP":
                                strTemp = "Budgetary";

                                break;
                            case "TWE90000.ZIP":
                                strTemp = "E-911";

                                break;
                            case "TWGN0000.ZIP":
                                strTemp = "General Entry";

                                break;
                            case "TWTAXUPD.ZIP":
                                strTemp = "Payroll Tax Table";

                                break;
                            case "TWUPDVER.ZIP":
                                strTemp = "Update Version";

                                break;
                            case "TWXF0000.ZIP":
                                strTemp = "Real Estate Transfer";

                                break;
                            case "TWAR0000.ZIP":
                                strTemp = "Accounts Receivable";

                                break;
                        }
                        strTemp = "Error updating " + strTemp + ". The downloaded file may be corrupt or the existing file could not be overwritten";
                    }
                    else
                    {
                        strTemp = "Error in saving downloaded file. It may be corrupt or the old file couldn't be overwritten";
                    }
                    MessageBox.Show(strTemp, null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Unload Me
                    return pOpen;
                }
                // Display it in the ListView:
                for (i = 1; i <= abObj.Count; i++)
                {
                    // sIcon = AddIconToImageList(m_cUnzip.Filename(i), ilsIcons16, "DEFAULT")
                    // sFIle = m_cUnzip.Filename(i)
                    sFIle = abObj.get_Item(i).Filename;
                    Array.Resize(ref strZFiles, i + 1);
                    strZFiles[i - 1] = sFIle;
                }
                // i
                pOpen = true;
                return pOpen;
            }
            catch
            {
                // ErrorHandler:
            }
            return pOpen;
        }

        private void SelectUpdates()
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_Err_Connect = 1;
            const int curOnErrorGoToLabel_ErrorHandler = 2;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                int counter;
                clsDRWrapper rsModules = new clsDRWrapper();
                bool boolHasBilling = false;
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Connecting to FTP Site");
            TryConnect:
                ;
                vOnErrorGoToLabel = curOnErrorGoToLabel_Err_Connect;
                /* On Error GoTo Err_Connect */// 8  '  FTP1.Connect strFTPSiteAddress, strFTPSiteUser, strFTPSitePassword, 21
                ChilkatFtp1.Hostname = strFTPSiteAddress;
                ChilkatFtp1.Username = strFTPSiteUser;
                ChilkatFtp1.Password = strFTPSitePassword;
                ChilkatFtp1.Connect();
                vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                /* On Error GoTo ErrorHandler */
                string strLocalDir = "";
                // 10    FTP1.LCD = Left(CurDir, 1) & ":\TRIOData\TRIOMast"
                if (Strings.Right(Environment.CurrentDirectory, 1) != "\\")
                {
                    strLocalDir = Environment.CurrentDirectory + "\\temp";
                }
                else
                {
                    strLocalDir = Environment.CurrentDirectory + "temp";
                }
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building Update List", true, 28);
                RefreshSocket();
                // 16    vsUpdates.Rows = vsUpdates.Rows + 1
                // 18    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol) = "TWGNENTY"
                // If Not blnAutoCheck Or Not blnFromUpdateReminder Then
                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = False
                // Else
                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = UpdateModule("GN")
                // End If
                // 22    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol) = "General Entry"
                if (intFiles > 0)
                {
                    vsUpdates.Rows = 0;
                    boolHasBilling = false;
                    rsModules.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
                    if (rsModules.BeginningOfFile() != true && rsModules.EndOfFile() != true)
                    {
                        for (counter = 0; counter <= 26; counter++)
                        {
                            frmWait.InstancePtr.prgProgress.Value = counter + 1;
                            //Application.DoEvents();
                            if (Strings.UCase(CreateModuleString(ref counter)) == "GN")
                            {
                                if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                {
                                    vsUpdates.Rows += 1;
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                    /*? 39 */
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                }
                                /*? 46 */
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "PYTAXUPDATE")
                            {
                                if (FCConvert.ToBoolean(rsModules.Get_Fields_Boolean("PY")))
                                {
                                    if (CheckIfFileExists_2("TWTaxUpd.zip"))
                                    {
                                        vsUpdates.Rows += 1;
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TWTaxUPD");
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2("PYTaxUpdate"));
                                    }
                                }
                                /*? 60 */
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "REMVR")
                            {
                                // TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
                                if (FCConvert.ToBoolean(rsModules.Get_Fields("RE")))
                                {
                                    if (CheckIfFileExists_2("TWMVRXLS.zip"))
                                    {
                                        vsUpdates.Rows += 1;
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TWMVRXLS");
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                        vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2("REMVR"));
                                    }
                                }
                                // ElseIf UCase(CreateModuleString(counter)) = "RXUPLOAD" Then
                                // If rsModules.Fields("RX") Then
                                // If CheckIfFileExists("TWRXUpload.zip") Then
                                // vsUpdates.Rows = vsUpdates.Rows + 1
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol) = "TWRXUpload"
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = False
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol) = CreateModuleDescription("RXUpload")
                                // End If
                                // End If
                            }
                            else if (Strings.UCase(CreateModuleString(ref counter)) == "EPAY")
                            {
                                if (CheckIfFileExists_2(""))
                                {
                                    vsUpdates.Rows += 1;
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "HarrisEPayments.exe");
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(false));
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2("EPAY"));
                                }
                                /*? 74 */
                            }
                            else if (rsModules.Get_Fields(CreateModuleString(ref counter)))
                            {
                                if (Information.IsDate(rsModules.Get_Fields(CreateModuleString(ref counter) + "Date")))
                                {
                                    if (DateAndTime.DateDiff("d", (DateTime)rsModules.Get_Fields(CreateModuleString(ref counter) + "Date"), DateTime.Today) <= 0 || ((Strings.UCase(CreateModuleString(ref counter)) == "PP" || Strings.UCase(CreateModuleString(ref counter)) == "RE") && boolHasBilling))
                                    {
                                        if (Strings.UCase(CreateModuleString(ref counter)) == "BL")
                                            boolHasBilling = true;
                                        if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                        {
                                            vsUpdates.Rows += 1;
                                            vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                            /*? 84 */
                                            vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                            vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                        }
                                    }
                                }
                                // If UCase(CreateModuleString(counter)) = "RB" Then
                                // If CheckIfFileExists("TWRB0000.zip") Then
                                // vsUpdates.Rows = vsUpdates.Rows + 1
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol) = "TWRB0000"
                                // If Not blnAutoCheck Or Not blnFromUpdateReminder Then
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = False
                                // Else
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol) = UpdateModule("RB")
                                // End If
                                // vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol) = CreateModuleDescription("RB") & " Data"
                                // End If
                                // End If
                                /*? 91 */
                            }
                            else if ((Strings.UCase(CreateModuleString(ref counter)) == "PP" || Strings.UCase(CreateModuleString(ref counter)) == "RE") && boolHasBilling)
                            {
                                if (CheckIfFileExists_2("TW" + CreateModuleString(ref counter) + "0000.zip"))
                                {
                                    vsUpdates.Rows += 1;
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, FileCol, "TW" + CreateModuleString(ref counter) + "0000");
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, SelectCol, FCConvert.ToString(UpdateModule_2(CreateModuleString(ref counter))));
                                    vsUpdates.TextMatrix(vsUpdates.Rows - 1, DescriptionCol, CreateModuleDescription_2(CreateModuleString(ref counter)));
                                }
                            }
                        }
                    }
                }
                ShowProgramNotes();
                frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Maximum;
                frmWait.InstancePtr.Unload();
                return;
            }
            catch (Exception ex)
            {
            Err_Connect:
                ;
                if (Strings.Left(Information.Err(ex).Description + "     ", 5) == "12014" && strFTPSitePassword != "trio")
                {
                    strFTPSitePassword = "trio";
                    //TODO SBE
                    //goto TryConnect;
                }
                frmWait.InstancePtr.Unload();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show("Connect failed: " + "\r\n" + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                AddStatusMsg_2("Connect failed: " + Information.Err(ex).Description);
                return;
            ErrorHandler:
                ;
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SelectUpdates line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                switch (vOnErrorGoToLabel)
                {
                    default:
                    case curOnErrorGoToLabel_Default:
                        // ...
                        break;
                    case curOnErrorGoToLabel_Err_Connect:
                        //? goto Err_Connect;
                        break;
                    case curOnErrorGoToLabel_ErrorHandler:
                        //? goto ErrorHandler;
                        break;
                }
            }
        }

        private void cmbNew_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbNew.Text == "All Programs")
            {
                optAll_CheckedChanged(sender, e);
            }
            else if (cmbNew.Text == "All Programs With Updates")
            {
                optNew_CheckedChanged(sender, e);
            }
            else if (cmbNew.Text == "Selected Programs")
            {
                optSelected_CheckedChanged(sender, e);
            }
        }
    }
}
