﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWGNENTY
{
	public class modEncrypt
	{
		//=========================================================
		public static string ToggleEncryptCode(string strText = "")
		{
			string ToggleEncryptCode = "";
			int intCounter;
			string strNewString = "";
			strText = Strings.UCase(strText);
			if (strText != "")
			{
				for (intCounter = strText.Length; intCounter >= 1; intCounter--)
				{
					strNewString += FCConvert.ToString(Convert.ToChar(Strings.Mid(strText, intCounter, 1)[0] ^ 240));
				}
			}
			ToggleEncryptCode = strNewString;
			return ToggleEncryptCode;
		}
	}
}
