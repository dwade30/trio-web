﻿using Global;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Messaging;

namespace TWGNENTY
{
    public class SelectValidAccountHandler : CommandHandler<SelectValidAccount,string>
    {
        protected override string Handle(SelectValidAccount command)
        {
            if (string.IsNullOrWhiteSpace(modAccountTitle.Statics.Exp))
            {
                modValidateAccount.SetBDFormats();
            }

            return frmLoadValidAccounts.InstancePtr.Init(command.Account,command.AccountType);
        }
    }
}