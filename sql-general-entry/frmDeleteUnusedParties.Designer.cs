﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmDeleteUnusedParties.
	/// </summary>
	partial class frmDeleteUnusedParties : BaseForm
	{
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCButton cmdStop;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCProgressBar prgProgress;
		public fecherFoundation.FCLabel lblDeleted;
		public fecherFoundation.FCLabel lblProgress;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblRange;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeleteUnusedParties));
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cmdStop = new fecherFoundation.FCButton();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.prgProgress = new fecherFoundation.FCProgressBar();
			this.lblDeleted = new fecherFoundation.FCLabel();
			this.lblProgress = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblRange = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdStop)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 436);
			this.BottomPanel.Size = new System.Drawing.Size(415, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.cmdStop);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.prgProgress);
			this.ClientArea.Controls.Add(this.lblDeleted);
			this.ClientArea.Controls.Add(this.lblProgress);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Size = new System.Drawing.Size(415, 376);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(415, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(258, 30);
			this.HeaderText.Text = "Delete Unused Parties";
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.ForeColor = System.Drawing.Color.White;
			this.cmdProcess.Location = new System.Drawing.Point(30, 221);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(95, 48);
			this.cmdProcess.TabIndex = 7;
			this.cmdProcess.Text = "Delete";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// cmdStop
			// 
			this.cmdStop.AppearanceKey = "actionButton";
			this.cmdStop.ForeColor = System.Drawing.Color.White;
			this.cmdStop.Location = new System.Drawing.Point(145, 221);
			this.cmdStop.Name = "cmdStop";
			this.cmdStop.Size = new System.Drawing.Size(79, 48);
			this.cmdStop.TabIndex = 8;
			this.cmdStop.Text = "Stop";
			this.cmdStop.Click += new System.EventHandler(this.cmdStop_Click);
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(30, 58);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(138, 40);
			this.txtStart.TabIndex = 4;
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(249, 58);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(138, 40);
			this.txtEnd.TabIndex = 6;
			// 
			// prgProgress
			// 
			this.prgProgress.Location = new System.Drawing.Point(30, 181);
			this.prgProgress.Name = "prgProgress";
			this.prgProgress.Size = new System.Drawing.Size(357, 20);
			this.prgProgress.TabIndex = 1;
			this.prgProgress.Visible = false;
			// 
			// lblDeleted
			// 
			this.lblDeleted.Location = new System.Drawing.Point(30, 146);
			this.lblDeleted.Name = "lblDeleted";
			this.lblDeleted.Size = new System.Drawing.Size(362, 23);
			this.lblDeleted.TabIndex = 2;
			this.lblDeleted.Visible = false;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(30, 117);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(190, 23);
			this.lblProgress.TabIndex = 0;
			this.lblProgress.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(186, 72);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(46, 21);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblRange
			// 
			this.lblRange.Location = new System.Drawing.Point(30, 30);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(362, 17);
			this.lblRange.TabIndex = 3;
			this.lblRange.Text = "ENTER A NAME RANGE TO LIMIT THE NAMES SEARCHED";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuDelete,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 0;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmDeleteUnusedParties
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(415, 357);
			this.FillColor = 0;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDeleteUnusedParties";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Delete Unused Parties";
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeleteUnusedParties_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdStop)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
