﻿namespace TWGNENTY.UpdateScheduler
{
	partial class frmScheduleUpdate
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblScheduledUpdateInfo = new fecherFoundation.FCLabel();
			this.dtpScheduledDate = new fecherFoundation.FCDateTimePicker();
			this.fraExistingUpdate = new fecherFoundation.FCFrame();
			this.btnCancelUpdate = new fecherFoundation.FCButton();
			this.btnRescheduleUpdate = new fecherFoundation.FCButton();
			this.fraScheduleUpdate = new fecherFoundation.FCFrame();
			this.btnCreate = new fecherFoundation.FCButton();
			this.cboScheduledTime = new fecherFoundation.FCComboBox();
			this.btnCancelSchedule = new fecherFoundation.FCButton();
			this.btnSave = new fecherFoundation.FCButton();
			this.lblCurrentVersion = new fecherFoundation.FCLabel();
			this.lblLatestVersion = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraExistingUpdate)).BeginInit();
			this.fraExistingUpdate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnCancelUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnRescheduleUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraScheduleUpdate)).BeginInit();
			this.fraScheduleUpdate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnCreate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnCancelSchedule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 226);
			this.BottomPanel.Size = new System.Drawing.Size(681, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblLatestVersion);
			this.ClientArea.Controls.Add(this.lblCurrentVersion);
			this.ClientArea.Controls.Add(this.fraExistingUpdate);
			this.ClientArea.Controls.Add(this.fraScheduleUpdate);
			this.ClientArea.Size = new System.Drawing.Size(701, 351);
			this.ClientArea.Controls.SetChildIndex(this.fraScheduleUpdate, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraExistingUpdate, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCurrentVersion, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblLatestVersion, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(701, 60);
			// 
			// lblScheduledUpdateInfo
			// 
			this.lblScheduledUpdateInfo.Location = new System.Drawing.Point(13, 21);
			this.lblScheduledUpdateInfo.Name = "lblScheduledUpdateInfo";
			this.lblScheduledUpdateInfo.Size = new System.Drawing.Size(406, 19);
			this.lblScheduledUpdateInfo.TabIndex = 1001;
			this.lblScheduledUpdateInfo.Text = "FCLABEL1";
			this.lblScheduledUpdateInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dtpScheduledDate
			// 
			this.dtpScheduledDate.AutoSize = false;
			this.dtpScheduledDate.Location = new System.Drawing.Point(31, 35);
			this.dtpScheduledDate.Name = "dtpScheduledDate";
			this.dtpScheduledDate.Size = new System.Drawing.Size(211, 32);
			this.dtpScheduledDate.TabIndex = 1002;
			// 
			// fraExistingUpdate
			// 
			this.fraExistingUpdate.Controls.Add(this.btnCancelUpdate);
			this.fraExistingUpdate.Controls.Add(this.btnRescheduleUpdate);
			this.fraExistingUpdate.Controls.Add(this.lblScheduledUpdateInfo);
			this.fraExistingUpdate.Location = new System.Drawing.Point(135, 94);
			this.fraExistingUpdate.Name = "fraExistingUpdate";
			this.fraExistingUpdate.Size = new System.Drawing.Size(430, 113);
			this.fraExistingUpdate.TabIndex = 1003;
			// 
			// btnCancelUpdate
			// 
			this.btnCancelUpdate.AppearanceKey = "actionButton";
			this.btnCancelUpdate.Location = new System.Drawing.Point(227, 60);
			this.btnCancelUpdate.Name = "btnCancelUpdate";
			this.btnCancelUpdate.Size = new System.Drawing.Size(149, 33);
			this.btnCancelUpdate.TabIndex = 1003;
			this.btnCancelUpdate.Text = "Cancel Update";
			this.btnCancelUpdate.Click += new System.EventHandler(this.btnCancelUpdate_Click);
			// 
			// btnRescheduleUpdate
			// 
			this.btnRescheduleUpdate.AppearanceKey = "actionButton";
			this.btnRescheduleUpdate.Location = new System.Drawing.Point(55, 60);
			this.btnRescheduleUpdate.Name = "btnRescheduleUpdate";
			this.btnRescheduleUpdate.Size = new System.Drawing.Size(149, 33);
			this.btnRescheduleUpdate.TabIndex = 1002;
			this.btnRescheduleUpdate.Text = "Reschedule";
			this.btnRescheduleUpdate.Click += new System.EventHandler(this.btnRescheduleUpdate_Click);
			// 
			// fraScheduleUpdate
			// 
			this.fraScheduleUpdate.Controls.Add(this.btnCreate);
			this.fraScheduleUpdate.Controls.Add(this.cboScheduledTime);
			this.fraScheduleUpdate.Controls.Add(this.btnCancelSchedule);
			this.fraScheduleUpdate.Controls.Add(this.btnSave);
			this.fraScheduleUpdate.Controls.Add(this.dtpScheduledDate);
			this.fraScheduleUpdate.Location = new System.Drawing.Point(135, 83);
			this.fraScheduleUpdate.Name = "fraScheduleUpdate";
			this.fraScheduleUpdate.Size = new System.Drawing.Size(430, 143);
			this.fraScheduleUpdate.TabIndex = 1004;
			this.fraScheduleUpdate.Text = "Schedule Update";
			// 
			// btnCreate
			// 
			this.btnCreate.AppearanceKey = "actionButton";
			this.btnCreate.Location = new System.Drawing.Point(166, 86);
			this.btnCreate.Name = "btnCreate";
			this.btnCreate.Size = new System.Drawing.Size(99, 33);
			this.btnCreate.TabIndex = 1007;
			this.btnCreate.Text = "Save";
			this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
			// 
			// cboScheduledTime
			// 
			this.cboScheduledTime.Location = new System.Drawing.Point(265, 35);
			this.cboScheduledTime.Name = "cboScheduledTime";
			this.cboScheduledTime.Size = new System.Drawing.Size(135, 32);
			this.cboScheduledTime.TabIndex = 1006;
			// 
			// btnCancelSchedule
			// 
			this.btnCancelSchedule.AppearanceKey = "actionButton";
			this.btnCancelSchedule.Location = new System.Drawing.Point(227, 86);
			this.btnCancelSchedule.Name = "btnCancelSchedule";
			this.btnCancelSchedule.Size = new System.Drawing.Size(99, 33);
			this.btnCancelSchedule.TabIndex = 1005;
			this.btnCancelSchedule.Text = "Cancel";
			this.btnCancelSchedule.Click += new System.EventHandler(this.btnCancelSchedule_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.btnSave.AppearanceKey = "actionButton";
			this.btnSave.Location = new System.Drawing.Point(105, 86);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(99, 33);
			this.btnSave.TabIndex = 1004;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// lblCurrentVersion
			// 
			this.lblCurrentVersion.Location = new System.Drawing.Point(135, 10);
			this.lblCurrentVersion.Name = "lblCurrentVersion";
			this.lblCurrentVersion.Size = new System.Drawing.Size(235, 19);
			this.lblCurrentVersion.TabIndex = 1005;
			this.lblCurrentVersion.Text = "FCLABEL1";
			this.lblCurrentVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblLatestVersion
			// 
			this.lblLatestVersion.Location = new System.Drawing.Point(135, 40);
			this.lblLatestVersion.Name = "lblLatestVersion";
			this.lblLatestVersion.Size = new System.Drawing.Size(235, 19);
			this.lblLatestVersion.TabIndex = 1006;
			this.lblLatestVersion.Text = "FCLABEL1";
			this.lblLatestVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frmScheduleUpdate
			// 
			this.ClientSize = new System.Drawing.Size(701, 411);
			this.Name = "frmScheduleUpdate";
			this.Text = "Schedule Update";
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraExistingUpdate)).EndInit();
			this.fraExistingUpdate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnCancelUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnRescheduleUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraScheduleUpdate)).EndInit();
			this.fraScheduleUpdate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnCreate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnCancelSchedule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private fecherFoundation.FCFrame fraScheduleUpdate;
		private fecherFoundation.FCComboBox cboScheduledTime;
		private fecherFoundation.FCButton btnCancelSchedule;
		private fecherFoundation.FCButton btnSave;
		private fecherFoundation.FCDateTimePicker dtpScheduledDate;
		private fecherFoundation.FCFrame fraExistingUpdate;
		private fecherFoundation.FCButton btnCancelUpdate;
		private fecherFoundation.FCButton btnRescheduleUpdate;
		private fecherFoundation.FCLabel lblScheduledUpdateInfo;
		private fecherFoundation.FCButton btnCreate;
		private fecherFoundation.FCLabel lblLatestVersion;
		private fecherFoundation.FCLabel lblCurrentVersion;
	}
}