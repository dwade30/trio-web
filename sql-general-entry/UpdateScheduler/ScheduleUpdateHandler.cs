﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.ScheduleUpdate;
using SharedApplication.Messaging;
using Wisej.Web;
using MessageBox = System.Windows.Forms.MessageBox;
using MessageBoxButtons = System.Windows.Forms.MessageBoxButtons;
using MessageBoxIcon = System.Windows.Forms.MessageBoxIcon;

namespace TWGNENTY.UpdateScheduler
{
	public class ScheduleUpdateHandler : CommandHandler<ScheduleUpdate, bool>
	{
		private IModalView<IScheduleUpdateViewModel> scheduleUpdateView;
		public ScheduleUpdateHandler(IModalView<IScheduleUpdateViewModel> scheduleUpdateView)
		{
			this.scheduleUpdateView = scheduleUpdateView;
		}
		protected override bool Handle(ScheduleUpdate command)
		{
			if (scheduleUpdateView.ViewModel.IsNewVersionAvailable())
			{
				scheduleUpdateView.ShowModal();
				return true;
			}
			else
			{
				return false;
			}
			
		}
	}
}
