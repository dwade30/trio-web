﻿using System;
using Global;
using SharedApplication;
using SharedApplication.ClientSettings.ScheduleUpdate;
using Wisej.Web;

namespace TWGNENTY.UpdateScheduler
{
	public partial class frmScheduleUpdate : BaseForm, IModalView<IScheduleUpdateViewModel>
	{
		public frmScheduleUpdate()
		{
			InitializeComponent();
		}

		public frmScheduleUpdate(IScheduleUpdateViewModel viewModel) : this()
		{
			ViewModel = viewModel;

			lblCurrentVersion.Text = "Current Version - " + viewModel.CurrentVersion;
			lblLatestVersion.Text = "Latest Version - " + viewModel.LatestVersion;

			LoadTimeCombo();
			ShowCorrectFrame();
		}

		private void LoadTimeCombo()
		{
			string amPm = "AM";

			cboScheduledTime.Items.Clear();

			cboScheduledTime.Items.Add("12:00 " + amPm);
			cboScheduledTime.Items.Add("12:30 " + amPm);

			for (int hourCounter = 1; hourCounter < 12; hourCounter++)
			{
				cboScheduledTime.Items.Add(hourCounter + ":00 " + amPm);
				cboScheduledTime.Items.Add(hourCounter + ":30 " + amPm);
			}
			amPm = "PM";
			cboScheduledTime.Items.Add("12:00 " + amPm);
			cboScheduledTime.Items.Add("12:30 " + amPm);

			for (int hourCounter = 1; hourCounter < 12; hourCounter++)
			{
				cboScheduledTime.Items.Add(hourCounter + ":00 " + amPm);
				cboScheduledTime.Items.Add(hourCounter + ":30 " + amPm);
			}
		}

		private void ShowCorrectFrame()
		{
			if (ViewModel.UpdateScheduled)
			{
				ShowExistingUpdate();
			}
			else
			{
				ShowScheduleUpdate();
			}
		}

		private void ShowExistingUpdate()
		{
			fraExistingUpdate.Visible = true;
			fraScheduleUpdate.Visible = false;

			UpdateExistingUpdateInfo();
		}

		private void ShowScheduleUpdate()
		{
			fraExistingUpdate.Visible = false;
			fraScheduleUpdate.Visible = true;

			if (ViewModel.UpdateScheduled)
			{
				dtpScheduledDate.Text = ViewModel.UpcomingScheduledUpdate.ScheduledDateTime.ToLongDateString();
				cboScheduledTime.Text = ViewModel.UpcomingScheduledUpdate.ScheduledDateTime.ToShortTimeString();

				btnCancelSchedule.Visible = true;
				btnSave.Visible = true;
				btnCreate.Visible = false;
			}
			else
			{
				dtpScheduledDate.Text = DateTime.Today.AddDays(1).ToLongDateString();
				cboScheduledTime.Text = "12:00 AM";
				btnCreate.Visible = true;
				btnCancelSchedule.Visible = false;
				btnSave.Visible = false;
			}
		}

		private void UpdateExistingUpdateInfo()
		{
			lblScheduledUpdateInfo.Text = "Update scheduled to run on " +
			                              ViewModel.UpcomingScheduledUpdate.ScheduledDateTime.ToString(
				                              "MM/dd/yyyy hh:mm tt");
		}

		public IScheduleUpdateViewModel ViewModel { get; set; }

		public void ShowModal()
		{
			this.Show(FormShowEnum.Modal);
		}

		private void btnRescheduleUpdate_Click(object sender, EventArgs e)
		{
			ShowScheduleUpdate();
		}

		private void btnCancelUpdate_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you wish to cancel the update?", "Cancel Update?",
				MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				if (ViewModel.CancelScheduledUpdate())
				{
					ShowScheduleUpdate();
				}
				else
				{
					MessageBox.Show("Unable to cancel the update.", "Error Encountered", MessageBoxButtons.OK,
						MessageBoxIcon.Error);
				}
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			ScheduleUpdate();
		}

		private void ScheduleUpdate()
		{
			DateTime dateToCheck;
			if (DateTime.TryParse(dtpScheduledDate.Text + " " + cboScheduledTime.Text, out dateToCheck))
			{
				if (dateToCheck > DateTime.Now)
				{
					if (ViewModel.SaveScheduledUpdate(dateToCheck))
					{
						ShowExistingUpdate();
					}
					else
					{
						MessageBox.Show("Unable to schedule the update.", "Error Encountered", MessageBoxButtons.OK,
							MessageBoxIcon.Error);
					}
				}
				else
				{
					MessageBox.Show("Unable to schedule the update.  The scheduled update time must be set to some time in the future.", "Invalid Scheduled Time", MessageBoxButtons.OK,
						MessageBoxIcon.Error);
				}
			}
			else
			{
				MessageBox.Show("Unable to schedule the update.  The scheduled update time is invalid.", "Invalid Scheduled Time", MessageBoxButtons.OK,
					MessageBoxIcon.Error);
			}
		}
		
		private void btnCancelSchedule_Click(object sender, EventArgs e)
		{
			ShowExistingUpdate();
		}

		private void btnCreate_Click(object sender, EventArgs e)
		{
			ScheduleUpdate();
		}
	}
}

