﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmScheduler.
	/// </summary>
	partial class frmScheduler : BaseForm
	{
		public fecherFoundation.FCCheckBox chkViewAll;
		public fecherFoundation.FCComboBox cboYear;
		public fecherFoundation.FCComboBox cboMonth;
		public fecherFoundation.FCFrame fraEvent;
		public fecherFoundation.FCButton cmdRequestDate;
		public fecherFoundation.FCTextBox txtEventTitle;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public Global.T2KDateBox txtEventDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraDeleteEvent;
		public fecherFoundation.FCButton cmdDeleteDate;
		public fecherFoundation.FCButton cmdDeleteCancel;
		public fecherFoundation.FCButton cmdDeleteOK;
		public Global.T2KDateBox txtDeleteEventDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraSelectTown;
		public fecherFoundation.FCTextBox txtCriteria;
		public fecherFoundation.FCButton cmdSearchOK;
		public fecherFoundation.FCButton cmdSearchCancel;
		public fecherFoundation.FCFrame fraResults;
		public fecherFoundation.FCGrid vsResults;
		public fecherFoundation.FCGrid vsCalender;
		public fecherFoundation.FCPictureBox imgForward;
		public fecherFoundation.FCPictureBox imgBack;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrevious;
		public fecherFoundation.FCToolStripMenuItem mnuFileNext;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileAddEvent;
		public fecherFoundation.FCToolStripMenuItem mnuFileDeleteEvent;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScheduler));
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.chkViewAll = new fecherFoundation.FCCheckBox();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.cboMonth = new fecherFoundation.FCComboBox();
			this.fraEvent = new fecherFoundation.FCFrame();
			this.cmdRequestDate = new fecherFoundation.FCButton();
			this.txtEventTitle = new fecherFoundation.FCTextBox();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.txtEventDate = new Global.T2KDateBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraDeleteEvent = new fecherFoundation.FCFrame();
			this.cmdDeleteDate = new fecherFoundation.FCButton();
			this.cmdDeleteCancel = new fecherFoundation.FCButton();
			this.cmdDeleteOK = new fecherFoundation.FCButton();
			this.txtDeleteEventDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraSelectTown = new fecherFoundation.FCFrame();
			this.txtCriteria = new fecherFoundation.FCTextBox();
			this.cmdSearchOK = new fecherFoundation.FCButton();
			this.cmdSearchCancel = new fecherFoundation.FCButton();
			this.fraResults = new fecherFoundation.FCFrame();
			this.vsResults = new fecherFoundation.FCGrid();
			this.vsCalender = new fecherFoundation.FCGrid();
			this.imgForward = new fecherFoundation.FCPictureBox();
			this.imgBack = new fecherFoundation.FCPictureBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrevious = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNext = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileAddEvent = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDeleteEvent = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileDeleteEvent = new fecherFoundation.FCButton();
			this.cmdFileAddEvent = new fecherFoundation.FCButton();
			this.cmdFileNext = new fecherFoundation.FCButton();
			this.cmdFilePrevious = new fecherFoundation.FCButton();
			this.fcButton1 = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkViewAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraEvent)).BeginInit();
			this.fraEvent.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDeleteEvent)).BeginInit();
			this.fraDeleteEvent.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeleteEventDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectTown)).BeginInit();
			this.fraSelectTown.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraResults)).BeginInit();
			this.fraResults.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsResults)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsCalender)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgForward)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgBack)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteEvent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAddEvent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileNext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 719);
			this.BottomPanel.Size = new System.Drawing.Size(807, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraResults);
			this.ClientArea.Controls.Add(this.chkViewAll);
			this.ClientArea.Controls.Add(this.fraEvent);
			this.ClientArea.Controls.Add(this.cboYear);
			this.ClientArea.Controls.Add(this.cboMonth);
			this.ClientArea.Controls.Add(this.fraDeleteEvent);
			this.ClientArea.Controls.Add(this.fraSelectTown);
			this.ClientArea.Controls.Add(this.vsCalender);
			this.ClientArea.Controls.Add(this.imgForward);
			this.ClientArea.Controls.Add(this.imgBack);
			this.ClientArea.Size = new System.Drawing.Size(807, 659);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.fcButton1);
			this.TopPanel.Controls.Add(this.cmdFilePrevious);
			this.TopPanel.Controls.Add(this.cmdFileNext);
			this.TopPanel.Controls.Add(this.cmdFileAddEvent);
			this.TopPanel.Controls.Add(this.cmdFileDeleteEvent);
			this.TopPanel.Size = new System.Drawing.Size(807, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDeleteEvent, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileAddEvent, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileNext, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrevious, 0);
			this.TopPanel.Controls.SetChildIndex(this.fcButton1, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(112, 30);
			this.HeaderText.Text = "Calendar";
			// 
			// chkViewAll
			// 
			this.chkViewAll.Location = new System.Drawing.Point(30, 30);
			this.chkViewAll.Name = "chkViewAll";
			this.chkViewAll.Size = new System.Drawing.Size(140, 27);
			this.chkViewAll.TabIndex = 0;
			this.chkViewAll.Text = "View All Events";
			this.chkViewAll.CheckedChanged += new System.EventHandler(this.chkViewAll_CheckedChanged);
			// 
			// cboYear
			// 
			this.cboYear.AutoSize = false;
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboYear.FormattingEnabled = true;
			this.cboYear.Location = new System.Drawing.Point(570, 30);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(92, 40);
			this.cboYear.TabIndex = 2;
			this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
			// 
			// cboMonth
			// 
			this.cboMonth.AutoSize = false;
			this.cboMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboMonth.FormattingEnabled = true;
			this.cboMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboMonth.Location = new System.Drawing.Point(322, 30);
			this.cboMonth.Name = "cboMonth";
			this.cboMonth.Size = new System.Drawing.Size(234, 40);
			this.cboMonth.TabIndex = 1;
			this.cboMonth.SelectedIndexChanged += new System.EventHandler(this.cboMonth_SelectedIndexChanged);
			// 
			// fraEvent
			// 
			this.fraEvent.BackColor = System.Drawing.Color.White;
			this.fraEvent.Controls.Add(this.cmdRequestDate);
			this.fraEvent.Controls.Add(this.txtEventTitle);
			this.fraEvent.Controls.Add(this.cmdOK);
			this.fraEvent.Controls.Add(this.cmdCancel);
			this.fraEvent.Controls.Add(this.txtEventDate);
			this.fraEvent.Controls.Add(this.Label3);
			this.fraEvent.Controls.Add(this.Label1);
			this.fraEvent.Location = new System.Drawing.Point(30, 90);
			this.fraEvent.Name = "fraEvent";
			this.fraEvent.Size = new System.Drawing.Size(342, 211);
			this.fraEvent.TabIndex = 11;
			this.fraEvent.Text = "Enter Event Info";
			this.fraEvent.Visible = false;
			// 
			// cmdRequestDate
			// 
			this.cmdRequestDate.AppearanceKey = "imageButton";
			this.cmdRequestDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdRequestDate.Location = new System.Drawing.Point(270, 30);
			this.cmdRequestDate.Name = "cmdRequestDate";
			this.cmdRequestDate.Size = new System.Drawing.Size(41, 40);
			this.cmdRequestDate.TabIndex = 15;
			this.cmdRequestDate.Click += new System.EventHandler(this.cmdRequestDate_Click);
			// 
			// txtEventTitle
			// 
			this.txtEventTitle.AutoSize = false;
			this.txtEventTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtEventTitle.LinkItem = null;
			this.txtEventTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEventTitle.LinkTopic = null;
			this.txtEventTitle.Location = new System.Drawing.Point(148, 90);
			this.txtEventTitle.Name = "txtEventTitle";
			this.txtEventTitle.Size = new System.Drawing.Size(163, 40);
			this.txtEventTitle.TabIndex = 14;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(20, 157);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(88, 40);
			this.cmdOK.TabIndex = 13;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(114, 157);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(86, 40);
			this.cmdCancel.TabIndex = 12;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// txtEventDate
			// 
			this.txtEventDate.Location = new System.Drawing.Point(148, 30);
			this.txtEventDate.Mask = "##/##/####";
			this.txtEventDate.MaxLength = 10;
			this.txtEventDate.Name = "txtEventDate";
			this.txtEventDate.Size = new System.Drawing.Size(115, 40);
			this.txtEventDate.TabIndex = 16;
			this.txtEventDate.Text = "  /  /";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(97, 16);
			this.Label3.TabIndex = 18;
			this.Label3.Text = "EVENT DATE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 104);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(97, 16);
			this.Label1.TabIndex = 17;
			this.Label1.Text = "EVENT TITLE";
			// 
			// fraDeleteEvent
			// 
			this.fraDeleteEvent.BackColor = System.Drawing.Color.White;
			this.fraDeleteEvent.Controls.Add(this.cmdDeleteDate);
			this.fraDeleteEvent.Controls.Add(this.cmdDeleteCancel);
			this.fraDeleteEvent.Controls.Add(this.cmdDeleteOK);
			this.fraDeleteEvent.Controls.Add(this.txtDeleteEventDate);
			this.fraDeleteEvent.Controls.Add(this.Label2);
			this.fraDeleteEvent.Location = new System.Drawing.Point(30, 90);
			this.fraDeleteEvent.Name = "fraDeleteEvent";
			this.fraDeleteEvent.Size = new System.Drawing.Size(329, 153);
			this.fraDeleteEvent.TabIndex = 5;
			this.fraDeleteEvent.Text = "Delete Event Info";
			this.fraDeleteEvent.Visible = false;
			// 
			// cmdDeleteDate
			// 
			this.cmdDeleteDate.AppearanceKey = "imageButton";
			this.cmdDeleteDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdDeleteDate.Location = new System.Drawing.Point(267, 30);
			this.cmdDeleteDate.Name = "cmdDeleteDate";
			this.cmdDeleteDate.Size = new System.Drawing.Size(44, 40);
			this.cmdDeleteDate.TabIndex = 8;
			this.cmdDeleteDate.Click += new System.EventHandler(this.cmdDeleteDate_Click);
			// 
			// cmdDeleteCancel
			// 
			this.cmdDeleteCancel.AppearanceKey = "actionButton";
			this.cmdDeleteCancel.ForeColor = System.Drawing.Color.White;
			this.cmdDeleteCancel.Location = new System.Drawing.Point(123, 81);
			this.cmdDeleteCancel.Name = "cmdDeleteCancel";
			this.cmdDeleteCancel.Size = new System.Drawing.Size(78, 40);
			this.cmdDeleteCancel.TabIndex = 7;
			this.cmdDeleteCancel.Text = "Cancel";
			this.cmdDeleteCancel.Click += new System.EventHandler(this.cmdDeleteCancel_Click);
			// 
			// cmdDeleteOK
			// 
			this.cmdDeleteOK.AppearanceKey = "actionButton";
			this.cmdDeleteOK.ForeColor = System.Drawing.Color.White;
			this.cmdDeleteOK.Location = new System.Drawing.Point(34, 81);
			this.cmdDeleteOK.Name = "cmdDeleteOK";
			this.cmdDeleteOK.Size = new System.Drawing.Size(78, 40);
			this.cmdDeleteOK.TabIndex = 6;
			this.cmdDeleteOK.Text = "OK";
			this.cmdDeleteOK.Click += new System.EventHandler(this.cmdDeleteOK_Click);
			// 
			// txtDeleteEventDate
			// 
			this.txtDeleteEventDate.Location = new System.Drawing.Point(147, 30);
			this.txtDeleteEventDate.Mask = "##/##/####";
			this.txtDeleteEventDate.MaxLength = 10;
			this.txtDeleteEventDate.Name = "txtDeleteEventDate";
			this.txtDeleteEventDate.Size = new System.Drawing.Size(115, 40);
			this.txtDeleteEventDate.TabIndex = 9;
			this.txtDeleteEventDate.Text = "  /  /";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(121, 16);
			this.Label2.TabIndex = 10;
			this.Label2.Text = "EVENT DATE";
			// 
			// fraSelectTown
			// 
            this.fraSelectTown.BackColor = Color.White;
            this.fraSelectTown.Controls.Add(this.txtCriteria);
			this.fraSelectTown.Controls.Add(this.cmdSearchOK);
			this.fraSelectTown.Controls.Add(this.cmdSearchCancel);
			this.fraSelectTown.Location = new System.Drawing.Point(30, 90);
			this.fraSelectTown.Name = "fraSelectTown";
			this.fraSelectTown.Size = new System.Drawing.Size(239, 135);
			this.fraSelectTown.TabIndex = 2;
			this.fraSelectTown.Text = "Enter Search Criteria";
			this.fraSelectTown.Visible = false;
			// 
			// txtCriteria
			// 
			this.txtCriteria.AutoSize = false;
			this.txtCriteria.BackColor = System.Drawing.SystemColors.Window;
			this.txtCriteria.LinkItem = null;
			this.txtCriteria.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCriteria.LinkTopic = null;
			this.txtCriteria.Location = new System.Drawing.Point(20, 30);
			this.txtCriteria.Name = "txtCriteria";
			this.txtCriteria.Size = new System.Drawing.Size(185, 40);
			this.txtCriteria.TabIndex = 22;
			this.txtCriteria.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCriteria_KeyDown);
			// 
			// cmdSearchOK
			// 
			this.cmdSearchOK.AppearanceKey = "actionButton";
			this.cmdSearchOK.ForeColor = System.Drawing.Color.White;
			this.cmdSearchOK.Location = new System.Drawing.Point(20, 80);
			this.cmdSearchOK.Name = "cmdSearchOK";
			this.cmdSearchOK.Size = new System.Drawing.Size(78, 40);
			this.cmdSearchOK.TabIndex = 4;
			this.cmdSearchOK.Text = "OK";
			this.cmdSearchOK.Click += new System.EventHandler(this.cmdSearchOK_Click);
			// 
			// cmdSearchCancel
			// 
			this.cmdSearchCancel.AppearanceKey = "actionButton";
			this.cmdSearchCancel.ForeColor = System.Drawing.Color.White;
			this.cmdSearchCancel.Location = new System.Drawing.Point(109, 80);
			this.cmdSearchCancel.Name = "cmdSearchCancel";
			this.cmdSearchCancel.Size = new System.Drawing.Size(78, 40);
			this.cmdSearchCancel.TabIndex = 3;
			this.cmdSearchCancel.Text = "Cancel";
			this.cmdSearchCancel.Click += new System.EventHandler(this.cmdSearchCancel_Click);
			// 
			// fraResults
			// 
			this.fraResults.BackColor = System.Drawing.Color.White;
			this.fraResults.Controls.Add(this.vsResults);
			this.fraResults.Location = new System.Drawing.Point(30, 90);
			this.fraResults.Name = "fraResults";
			this.fraResults.Size = new System.Drawing.Size(621, 321);
			this.fraResults.TabIndex = 0;
			this.fraResults.Text = "Search Results";
			this.fraResults.Visible = false;
			// 
			// vsResults
			// 
			this.vsResults.AllowSelection = false;
			this.vsResults.AllowUserToResizeColumns = false;
			this.vsResults.AllowUserToResizeRows = false;
			this.vsResults.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsResults.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsResults.BackColorBkg = System.Drawing.Color.Empty;
			this.vsResults.BackColorFixed = System.Drawing.Color.Empty;
			this.vsResults.BackColorSel = System.Drawing.Color.Empty;
			this.vsResults.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsResults.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsResults.ColumnHeadersHeight = 30;
			this.vsResults.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsResults.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsResults.DragIcon = null;
			this.vsResults.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsResults.ExtendLastCol = true;
			this.vsResults.FixedCols = 0;
			this.vsResults.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsResults.FrozenCols = 0;
			this.vsResults.GridColor = System.Drawing.Color.Empty;
			this.vsResults.GridColorFixed = System.Drawing.Color.Empty;
			this.vsResults.Location = new System.Drawing.Point(20, 30);
			this.vsResults.Name = "vsResults";
			this.vsResults.ReadOnly = true;
			this.vsResults.RowHeadersVisible = false;
			this.vsResults.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsResults.RowHeightMin = 0;
			this.vsResults.Rows = 1;
			this.vsResults.ScrollTipText = null;
			this.vsResults.ShowColumnVisibilityMenu = false;
			this.vsResults.ShowFocusCell = false;
			this.vsResults.Size = new System.Drawing.Size(470, 265);
			this.vsResults.StandardTab = true;
			this.vsResults.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsResults.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsResults.TabIndex = 1;
			this.vsResults.KeyDown += new Wisej.Web.KeyEventHandler(this.vsResults_KeyDownEvent);
			this.vsResults.DoubleClick += new System.EventHandler(this.vsResults_DblClick);
			// 
			// vsCalender
			// 
			this.vsCalender.AllowSelection = false;
			this.vsCalender.AllowUserToResizeColumns = false;
			this.vsCalender.AllowUserToResizeRows = false;
			this.vsCalender.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsCalender.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCalender.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCalender.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCalender.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCalender.BackColorSel = System.Drawing.Color.Empty;
			this.vsCalender.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Both;
			this.vsCalender.Cols = 7;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCalender.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsCalender.ColumnHeadersHeight = 30;
			this.vsCalender.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCalender.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsCalender.DragIcon = null;
			this.vsCalender.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCalender.ExtendLastCol = true;
			this.vsCalender.FixedCols = 0;
			this.vsCalender.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCalender.FrozenCols = 0;
			this.vsCalender.GridColor = System.Drawing.Color.Empty;
			this.vsCalender.GridColorFixed = System.Drawing.Color.Empty;
			this.vsCalender.Location = new System.Drawing.Point(30, 90);
			this.vsCalender.Name = "vsCalender";
			this.vsCalender.ReadOnly = true;
			this.vsCalender.RowHeadersVisible = false;
			this.vsCalender.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCalender.RowHeightMin = 0;
			this.vsCalender.Rows = 25;
			this.vsCalender.ScrollTipText = null;
			this.vsCalender.ShowColumnVisibilityMenu = false;
			this.vsCalender.ShowFocusCell = false;
			this.vsCalender.Size = new System.Drawing.Size(756, 566);
			this.vsCalender.StandardTab = true;
			this.vsCalender.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsCalender.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsCalender.TabIndex = 3;
			this.vsCalender.DoubleClick += new System.EventHandler(this.vsCalender_DblClick);
			// 
			// imgForward
			// 
			this.imgForward.AllowDrop = true;
			this.imgForward.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgForward.DrawStyle = ((short)(0));
			this.imgForward.DrawWidth = ((short)(1));
			this.imgForward.FillStyle = ((short)(1));
			this.imgForward.FontTransparent = true;
			this.imgForward.ImageSource = "icon-arrow-forward";
			this.imgForward.Location = new System.Drawing.Point(678, 30);
			this.imgForward.Name = "imgForward";
			this.imgForward.Size = new System.Drawing.Size(35, 33);
			this.imgForward.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgForward.TabIndex = 24;
			this.imgForward.Click += new System.EventHandler(this.imgForward_Click);
			// 
			// imgBack
			// 
			this.imgBack.AllowDrop = true;
			this.imgBack.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgBack.DrawStyle = ((short)(0));
			this.imgBack.DrawWidth = ((short)(1));
			this.imgBack.FillStyle = ((short)(1));
			this.imgBack.FontTransparent = true;
			this.imgBack.ImageSource = "icon-arrow-back";
			this.imgBack.Location = new System.Drawing.Point(275, 30);
			this.imgBack.Name = "imgBack";
			this.imgBack.Size = new System.Drawing.Size(35, 33);
			this.imgBack.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgBack.TabIndex = 25;
			this.imgBack.Click += new System.EventHandler(this.imgBack_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSearch,
				this.mnuFileSeperator2,
				this.mnuFilePrevious,
				this.mnuFileNext,
				this.mnuSeperator2,
				this.mnuFileAddEvent,
				this.mnuFileDeleteEvent,
				this.mnuFileSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSearch
			// 
			this.mnuFileSearch.Index = 0;
			this.mnuFileSearch.Name = "mnuFileSearch";
			this.mnuFileSearch.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuFileSearch.Text = "Search ";
			this.mnuFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFilePrevious
			// 
			this.mnuFilePrevious.Index = 2;
			this.mnuFilePrevious.Name = "mnuFilePrevious";
			this.mnuFilePrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFilePrevious.Text = "Previous Month";
			this.mnuFilePrevious.Click += new System.EventHandler(this.mnuFilePrevious_Click);
			// 
			// mnuFileNext
			// 
			this.mnuFileNext.Index = 3;
			this.mnuFileNext.Name = "mnuFileNext";
			this.mnuFileNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileNext.Text = "Next Month";
			this.mnuFileNext.Click += new System.EventHandler(this.mnuFileNext_Click);
			// 
			// mnuSeperator2
			// 
			this.mnuSeperator2.Index = 4;
			this.mnuSeperator2.Name = "mnuSeperator2";
			this.mnuSeperator2.Text = "-";
			// 
			// mnuFileAddEvent
			// 
			this.mnuFileAddEvent.Index = 5;
			this.mnuFileAddEvent.Name = "mnuFileAddEvent";
			this.mnuFileAddEvent.Shortcut = Wisej.Web.Shortcut.CtrlA;
			this.mnuFileAddEvent.Text = "Add Event";
			this.mnuFileAddEvent.Click += new System.EventHandler(this.mnuFileAddEvent_Click);
			// 
			// mnuFileDeleteEvent
			// 
			this.mnuFileDeleteEvent.Index = 6;
			this.mnuFileDeleteEvent.Name = "mnuFileDeleteEvent";
			this.mnuFileDeleteEvent.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuFileDeleteEvent.Text = "Delete Event";
			this.mnuFileDeleteEvent.Click += new System.EventHandler(this.mnuFileDeleteEvent_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 7;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 8;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFileDeleteEvent
			// 
			this.cmdFileDeleteEvent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDeleteEvent.AppearanceKey = "toolbarButton";
			this.cmdFileDeleteEvent.Location = new System.Drawing.Point(666, 29);
			this.cmdFileDeleteEvent.Name = "cmdFileDeleteEvent";
			this.cmdFileDeleteEvent.Size = new System.Drawing.Size(100, 24);
			this.cmdFileDeleteEvent.TabIndex = 1;
			this.cmdFileDeleteEvent.Text = "Delete Event";
			this.cmdFileDeleteEvent.Click += new System.EventHandler(this.mnuFileDeleteEvent_Click);
			// 
			// cmdFileAddEvent
			// 
			this.cmdFileAddEvent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileAddEvent.AppearanceKey = "toolbarButton";
			this.cmdFileAddEvent.Location = new System.Drawing.Point(571, 29);
			this.cmdFileAddEvent.Name = "cmdFileAddEvent";
			this.cmdFileAddEvent.Size = new System.Drawing.Size(88, 24);
			this.cmdFileAddEvent.TabIndex = 2;
			this.cmdFileAddEvent.Text = "Add Event";
			this.cmdFileAddEvent.Click += new System.EventHandler(this.mnuFileAddEvent_Click);
			// 
			// cmdFileNext
			// 
			this.cmdFileNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileNext.AppearanceKey = "toolbarButton";
			this.cmdFileNext.Location = new System.Drawing.Point(470, 29);
			this.cmdFileNext.Name = "cmdFileNext";
			this.cmdFileNext.Size = new System.Drawing.Size(95, 24);
			this.cmdFileNext.TabIndex = 3;
			this.cmdFileNext.Text = "Next Month";
			this.cmdFileNext.Click += new System.EventHandler(this.mnuFileNext_Click);
			// 
			// cmdFilePrevious
			// 
			this.cmdFilePrevious.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrevious.AppearanceKey = "toolbarButton";
			this.cmdFilePrevious.Location = new System.Drawing.Point(348, 29);
			this.cmdFilePrevious.Name = "cmdFilePrevious";
			this.cmdFilePrevious.Size = new System.Drawing.Size(116, 24);
			this.cmdFilePrevious.TabIndex = 4;
			this.cmdFilePrevious.Text = "Previous Month";
			this.cmdFilePrevious.Click += new System.EventHandler(this.mnuFilePrevious_Click);
			// 
			// fcButton1
			// 
			this.fcButton1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.fcButton1.AppearanceKey = "toolbarButton";
            this.fcButton1.ImageSource = "button-search";
            this.fcButton1.Location = new System.Drawing.Point(275, 29);
			this.fcButton1.Name = "fcButton1";
            this.fcButton1.Shortcut = Shortcut.F6;
            this.fcButton1.Size = new System.Drawing.Size(81, 24);
			this.fcButton1.TabIndex = 5;
			this.fcButton1.Text = "Search";
            this.fcButton1.Click += this.mnuFileSearch_Click;
			// 
			// frmScheduler
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(807, 827);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmScheduler";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Calendar";
			this.Load += new System.EventHandler(this.frmScheduler_Load);
			this.Activated += new System.EventHandler(this.frmScheduler_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmScheduler_KeyPress);
			this.Resize += new System.EventHandler(this.frmScheduler_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkViewAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraEvent)).EndInit();
			this.fraEvent.ResumeLayout(false);
			this.fraEvent.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDeleteEvent)).EndInit();
			this.fraDeleteEvent.ResumeLayout(false);
			this.fraDeleteEvent.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeleteEventDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectTown)).EndInit();
			this.fraSelectTown.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraResults)).EndInit();
			this.fraResults.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsResults)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsCalender)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgForward)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgBack)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteEvent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAddEvent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileNext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton fcButton1;
		private FCButton cmdFilePrevious;
		private FCButton cmdFileNext;
		private FCButton cmdFileAddEvent;
		private FCButton cmdFileDeleteEvent;
	}
}
