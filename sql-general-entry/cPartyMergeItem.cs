﻿namespace TWGNENTY
{
	public class cPartyMergeItem
	{
		public bool Deleted { get; set; } = false;
		public int ID { get; set; } = 0;
		public string FullName { get; set; } = "";
		public string Address1 { get; set; } = "";
		public int PartyType { get; set; } = 0;
	}
}