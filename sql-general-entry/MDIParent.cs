﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using System;
using TWSharedLibrary;
using TWSharedLibrary.PrinterUtility;
using Wisej.Core;
using Wisej.Web;

namespace TWGNENTY
{
    public class MDIParent// : BaseForm
    {
        public fecherFoundation.FCCommonDialog CommonDialog1;
        public Chilkat.Ftp2 ChilkatFtp1;
        public MDIParent()
        {
            //
            // Required for Windows Form Designer support
            //
            //InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;

            CommonDialog1 = new FCCommonDialog();
            ChilkatFtp1 = new Chilkat.Ftp2();
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static MDIParent InstancePtr
        {
            get
            {
                return (MDIParent)Sys.GetInstance(typeof(MDIParent));
            }
        }

        protected MDIParent _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        private int intCount;
        private int intExit;
        private bool boolThisFormLoaded;
        private bool blnShowReminders;
        private bool mnuEliminateDuplicatePartiesEnabled;
        private bool mnuDeleteUnusedPartiesEnabled;
        const int CNSTMAINMENUID = 0;
        const int CNSTSystemMENUID = 1;
        const int CNSTCustomMENUID = 2;
        const int CNSTAuditMENUID = 3;

        private void ClearLabels()
        {

        }

        private void ClearMenuItems()
        {

        }

        //FC:FINAL:IPI - #1266 - when modules permission is changed in frmSecurity, must refresh the modules status on MainForm
        //private void MainCaptions()
        public void MainCaptions()
        {

            #region OldCode
            //ClearLabels();
            //ClearMenuItems();
            //CurRow = 1;
            // These numbers are permanent they are ID's not order numbers.  Change the order of the call to
            // AddMenuItem to rearrange the menu
            //AddMenuItem_726(CurRow, 1, "Real Estate", modSecurity2.REALESTATE, "", "", modEntryMenuForm.Statics.RealEstateAllowed);
            //AddMenuItem_726(CurRow, 2, "Personal Property", modSecurity2.PERSONALPROPERTY, "", "", modEntryMenuForm.Statics.PersonalPropertyAllowed);
            //AddMenuItem_726(CurRow, 3, "Tax Billing", modSecurity2.BILLINGENTRY, "", "", modEntryMenuForm.Statics.BillingAllowed);
            //AddMenuItem_726(CurRow, 4, "Real Estate Collections", modSecurity2.COLLECTIONSENTRY, "", "", modEntryMenuForm.Statics.CollectionsAllowed);
            //AddMenuItem_726(CurRow, 5, "Personal Property Collections", modSecurity2.PPCOLLECTIONS, "", "", modEntryMenuForm.Statics.CollectionsAllowed);
            //AddMenuItem_726(CurRow, 6, "Clerk", modSecurity2.CLERKENTRY, "", "", modEntryMenuForm.Statics.ClerkAllowed);
            //// Call AddMenuItem(CurRow, 7, "Voter Registration", VOTERREGISTRATION, "", "", VoterRegAllowed)
            //AddMenuItem_726(CurRow, 8, "Code Enforcement", modSecurity2.CODEENFORCEMENT, "", "", modEntryMenuForm.Statics.CodeAllowed);
            //AddMenuItem_726(CurRow, 9, "Budgetary System", modSecurity2.BUDGETARYSYSTEM, "", "", modEntryMenuForm.Statics.BudgetaryAllowed);
            //AddMenuItem_726(CurRow, 10, "Cash Receipting", modSecurity2.CASHRECEIPTS, "", "", modEntryMenuForm.Statics.CashReceiptAllowed);
            //AddMenuItem_726(CurRow, 11, "Payroll", modSecurity2.PAYROLLENTRY, "", "", modEntryMenuForm.Statics.PayrollAllowed);
            //AddMenuItem_726(CurRow, 12, "Utility Billing", modSecurity2.UTILITYBILLING, "", "", modEntryMenuForm.Statics.UtilitiesAllowed);
            //AddMenuItem_726(CurRow, 13, "Blue Book", modSecurity2.REDBOOK, "", "", modEntryMenuForm.Statics.RedbookAllowed);
            //AddMenuItem_726(CurRow, 14, "Motor Vehicle Registration", modSecurity2.MOTORVEHICLE, "", "", modEntryMenuForm.Statics.MotorVehicleAllowed);
            //AddMenuItem_726(CurRow, 15, "Enhanced 911", modSecurity2.E911ENTRY, "", "", modEntryMenuForm.Statics.E911);
            //AddMenuItem_726(CurRow, 16, "Fixed Assets", modSecurity2.FIXEDASSETENTRY, "", "", modEntryMenuForm.Statics.FixedAssetAllowed);
            //AddMenuItem_726(CurRow, 17, "Accounts Receivable", modSecurity2.CNSTACCOUNTSRECEIVABLEENTRY, "", "", modEntryMenuForm.Statics.AccountsReceivableAllowed);

            //if (!modSecurity2.ValidPermissions(this, modSecurity2.PASSWORDSETUP, false))
            //{
            //	// makes sure if they can edit passwords that then can get into system maintenance
            //	lngFCode = modSecurity2.SYSTEMMAINT;
            //}
            //AddMenuItem_1887(CurRow, 18, "System Maintenance", lngFCode, "M", true);
            //AddMenuItem_240(CurRow, 19, "Exit", 0, "X");
            //AutoReSizeMenu();
            //return MainCaptions;
            #endregion

            //FC:FINAL:IPI - #1297 - set the active/ inactive modules
            Tuple<string, int, bool>[] permissions = new[] { Tuple.Create("re", modSecurity2.REALESTATE, modEntryMenuForm.Statics.RealEstateAllowed),
                                                      Tuple.Create("pp", modSecurity2.PERSONALPROPERTY, modEntryMenuForm.Statics.PersonalPropertyAllowed),
                                                      Tuple.Create("bl", modSecurity2.BILLINGENTRY, modEntryMenuForm.Statics.BillingAllowed),
                                                      Tuple.Create("cl1", modSecurity2.COLLECTIONSENTRY, modEntryMenuForm.Statics.CollectionsAllowed),
                                                      Tuple.Create("cl0", modSecurity2.PPCOLLECTIONS, modEntryMenuForm.Statics.CollectionsAllowed),
                                                      Tuple.Create("ck", modSecurity2.CLERKENTRY, modEntryMenuForm.Statics.ClerkAllowed),
                                                      Tuple.Create("ce", modSecurity2.CODEENFORCEMENT, modEntryMenuForm.Statics.CodeAllowed),
                                                      Tuple.Create("bd", modSecurity2.BUDGETARYSYSTEM, modEntryMenuForm.Statics.BudgetaryAllowed),
                                                      Tuple.Create("cr", modSecurity2.CASHRECEIPTS, modEntryMenuForm.Statics.CashReceiptAllowed),
                                                      Tuple.Create("py", modSecurity2.PAYROLLENTRY, modEntryMenuForm.Statics.PayrollAllowed),
                                                      Tuple.Create("ut", modSecurity2.UTILITYBILLING, modEntryMenuForm.Statics.UtilitiesAllowed),
                                                      Tuple.Create("bb", modSecurity2.REDBOOK, modEntryMenuForm.Statics.RedbookAllowed),
                                                      Tuple.Create("mv", modSecurity2.MOTORVEHICLE, modEntryMenuForm.Statics.MotorVehicleAllowed),
                                                      //Tuple.Create("me911", modSecurity2.E911ENTRY, modEntryMenuForm.Statics.E911),
                                                      Tuple.Create("fa", modSecurity2.FIXEDASSETENTRY, modEntryMenuForm.Statics.FixedAssetAllowed),
                                                      Tuple.Create("ar", modSecurity2.CNSTACCOUNTSRECEIVABLEENTRY, modEntryMenuForm.Statics.AccountsReceivableAllowed)
                                            };

            foreach (Tuple<string, int, bool> perm in permissions)
            {
                App.MainForm.Modules[perm.Item1].IsActive = perm.Item3 && modSecurity2.ValidPermissions(this, perm.Item2, false);
            }

            bool systemMaintenancePermission = false;
            if (!modSecurity2.ValidPermissions(this, modSecurity2.PASSWORDSETUP, false))
            {
                // makes sure if they can edit passwords that then can get into system maintenance
                systemMaintenancePermission = modSecurity2.ValidPermissions(this, modSecurity2.SYSTEMMAINT, false);
            }
            else
            {
                // if lngFCode = 0, AddMenuItem executes modEntryMenuForm.EnableDisableMenuOption(boolAdditionalPerm, intRow); where boolAdditionalPerm = 0
                systemMaintenancePermission = true;
            }
            App.MainForm.ConfigureSettingsContextMenu(systemMaintenancePermission, mnuEliminateDuplicatePartiesEnabled, mnuDeleteUnusedPartiesEnabled);
        }

        public object CustomSettingsCaptions()
        {
            object CustomSettingsCaptions = null;
            int CurRow;
            string strTemp = "";
            int lngFCode;

            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem_78(CurRow, 1, "Customize", modSecurity2.CNSTCUSTOMIZE);
            AddMenuItem_726(CurRow, 2, "Printer Setup", modSecurity2.CNSTSECURITYPRINTERSETUP, "", "Select default printers and print test pages");
            AddMenuItem_726(CurRow, 3, "Password Setup", 0, "", "Change password and/or edit security settings");
            AddMenuItem_78(CurRow, 4, "Signatures", modSecurity2.CNSTSECURITYSIGNATURES);
            AddMenuItem_78(CurRow, 5, "Edit Operators", modSecurity2.EDITOPERATORS);
            if (modRegionalTown.IsRegionalTown())
            {
                AddMenuItem_726(CurRow, 6, "Regional Setup", 0, "", "Edit settings for systems set up as a region");
            }
            AddMenuItem_240(CurRow, 7, "Return to Sys. Maintenance", 0, "X");
            AutoReSizeMenu();
            return CustomSettingsCaptions;
        }

        public object SystemCaptions()
        {
            object SystemCaptions = null;
            int CurRow;
            string strTemp = "";
            int lngFCode;

            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem_726(CurRow, 1, "Custom Settings", 0, "", "");
            AddMenuItem_726(CurRow, 2, "Check Database Structure", 0, "", "Run a diagnostic routine to check that the databases structure is up to date");
            AddMenuItem_726(CurRow, 3, "Process Update Disk", 0, "", "Run a TRIO Windows update CD");
            AddMenuItem_726(CurRow, 4, "Process DLL Disk", 0, "", "Update DLLs from a TRIO Windows update CD");
            AddMenuItem_699(CurRow, 5, "Process Expiration Update", "", "Update expiration dates");
            AddMenuItem_726(CurRow, 6, "Program Information", 0, "", "View expiration dates and other TRIO program information");
            AddMenuItem_78(CurRow, 7, "Backup", modSecurity2.BACKUPRESTORE);
            AddMenuItem_78(CurRow, 8, "Restore", modSecurity2.BACKUPRESTORE);
            // Call AddMenuItem(CurRow, 10, "Backup and Restore", BACKUPRESTORE, "", "Backup or restore databases or programs")
            AddMenuItem_726(CurRow, 10, "Upload Files", modSecurity2.UploadFiles, "", "Send files to the TRIO FTP site");
            AddMenuItem_726(CurRow, 11, "Program Update", modSecurity2.PROGRAMUPDATE, "", "Update TRIO programs from the TRIO FTP site");
            AddMenuItem_78(CurRow, 12, "Audits", 0);
            AddMenuItem_726(CurRow, 13, "E-Mail", modSecurity2.CNSTEMAIL, "", "Send messages and attachments by E-mail");
            AddMenuItem_24(CurRow, 14, "Process Fix Program");
            AddMenuItem_78(CurRow, 15, "Calendar", modSecurity2.CNSTVIEWCALENDAR);
            AddMenuItem_240(CurRow, 16, "Return to Main", 0, "X");
            AutoReSizeMenu();
            return SystemCaptions;
        }

        public object AuditCaptions()
        {
            object AuditCaptions = null;
            int CurRow;
            string strTemp = "";
            int lngFCode;

            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem_78(CurRow, 1, "Internal Audit", modSecurity2.INTERNALAUDIT);
            AddMenuItem_726(CurRow, 2, "Audit Changes", modSecurity2.CNSTAUDITCHANGES, "", "");
            AddMenuItem_726(CurRow, 3, "Audit Changes Archive", modSecurity2.CNSTAUDITCHANGESARCHIVE, "", "");
            AddMenuItem_726(CurRow, 4, "Purge Audit Changes", modSecurity2.CNSTPURGEAUDITCHANGES, "", "");
            AddMenuItem_240(CurRow, 5, "Return to Sys. Maintenance", 0, "X");
            AutoReSizeMenu();
            return AuditCaptions;
        }

        private void AutoReSizeMenu()
        {
            int intCounter;
            // vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
            int FullRowHeight;
            // this will store the height of the labels with text
            int intLabels;
            intLabels = 20;

        }
        // vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(	OnRead(string)
        private void AddMenuItem_24(int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", bool boolAdditionalPerm = true)
        {
            AddMenuItem(intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip, boolAdditionalPerm);
        }

        private void AddMenuItem_78(int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", bool boolAdditionalPerm = true)
        {
            AddMenuItem(intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip, boolAdditionalPerm);
        }

        private void AddMenuItem_240(int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", bool boolAdditionalPerm = true)
        {
            AddMenuItem(intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip, boolAdditionalPerm);
        }

        private void AddMenuItem_699(int intRow, short intCode, string strCaption, string strForceOption = "", string strToolTip = "", bool boolAdditionalPerm = true)
        {
            AddMenuItem(intRow, ref intCode, ref strCaption, 0, strForceOption, strToolTip, boolAdditionalPerm);
        }

        private void AddMenuItem_726(int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", bool boolAdditionalPerm = true)
        {
            AddMenuItem(intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip, boolAdditionalPerm);
        }

        private void AddMenuItem(int intRow, ref short intCode, ref string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", bool boolAdditionalPerm = true)
        {
            // vbPorter upgrade warning: strOption As string	OnWrite(string, short)
            string strOption;
            strOption = strForceOption;
            if (strOption == string.Empty)
            {
                if (intRow < 10)
                {
                    strOption = FCConvert.ToString(intRow);
                }
                else
                {
                    switch (intRow)
                    {
                        case 10:
                            {
                                strOption = "A";
                                break;
                            }
                        case 11:
                            {
                                strOption = "B";
                                break;
                            }
                        case 12:
                            {
                                strOption = "C";
                                break;
                            }
                        case 13:
                            {
                                strOption = "D";
                                break;
                            }
                        case 14:
                            {
                                strOption = "E";
                                break;
                            }
                        case 15:
                            {
                                strOption = "F";
                                break;
                            }
                        case 16:
                            {
                                strOption = "G";
                                break;
                            }
                        case 17:
                            {
                                strOption = "H";
                                break;
                            }
                        case 18:
                            {
                                strOption = "I";
                                break;
                            }
                        case 19:
                            {
                                strOption = "J";
                                break;
                            }
                        case 20:
                            {
                                strOption = "K";
                                break;
                            }
                        case 21:
                            {
                                strOption = "L";
                                break;
                            }
                        case 22:
                            {
                                strOption = "M";
                                break;
                            }
                    }
                    //end switch
                }
            }
            strOption += ". " + strCaption;

            if (lngFCode != 0)
            {
                modEntryMenuForm.EnableDisableMenuOption(boolAdditionalPerm && modSecurity2.ValidPermissions(this, lngFCode, false), intRow);
            }
            else
            {
                modEntryMenuForm.EnableDisableMenuOption(boolAdditionalPerm, intRow);
            }
            intRow += 1;
        }
        // vbPorter upgrade warning: intMenu As short	OnWriteFCConvert.ToInt32(
        private void SetMenuOptions_2(short intMenu)
        {
            SetMenuOptions(ref intMenu);
        }

        private void SetMenuOptions(ref short intMenu)
        {
            modGNBas.CloseChildForms();
            //Grid.Tag = (System.Object)(intMenu);
            switch (intMenu)
            {
                case CNSTMAINMENUID:
                    {
                        modEntryMenuForm.CheckExpirationDate();
                        MainCaptions();
                        // vbPorter upgrade warning: intCounter As Variant --> As int
                        int intCounter;

                        break;
                    }
                case CNSTSystemMENUID:
                    {
                        SystemCaptions();
                        break;
                    }
                case CNSTCustomMENUID:
                    {
                        CustomSettingsCaptions();
                        break;
                    }
                case CNSTAuditMENUID:
                    {
                        AuditCaptions();
                        break;
                    }
            }
            //end switch
        }

        public void Init()
        {

            try
            {
                blnShowReminders = true;
                modGlobalFunctions.LoadTRIOColors();

                string strReturn = "";
                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);
                if (strReturn == string.Empty)
                    strReturn = "False";
                if (strReturn != "TRUE" && strReturn != "True")
                {
                    strReturn = "False";
                }
                modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
                //mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
                modGNBas.Statics.clsGESecurity = new clsTrioSecurity();
                modGNBas.Statics.clsGESecurity.Init("GE", "systemsettings", null, null, true);
                modGlobalConstants.Statics.clsSecurityClass.Init("GE", "systemsettings", null, modGNBas.Statics.clsGESecurity.Get_UserID());
                //FC:FINAL:IPI - menus Enabled flag set in ReEnableMenus is used in ConfigureSettingsContextMenu
                ReEnableMenus();
                SetMenuOptions_2(CNSTMAINMENUID);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + " occurred in MDIForm Load in Line " + Information.Erl(), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
        }

        public object ReEnableMenus()
        {
            object ReEnableMenus = null;
            //FC:FINAL:IPI - menus Enabled flag set in ReEnableMenus is used in ConfigureSettingsContextMenu
            if (Strings.LCase(modGNBas.Statics.clsGESecurity.CheckOtherPermissions(modCentralParties.CNSTCentralPartiesEdit, "CP")) == "f")
            {
                mnuEliminateDuplicatePartiesEnabled = true;
                mnuDeleteUnusedPartiesEnabled = true;
            }
            else
            {
                mnuEliminateDuplicatePartiesEnabled = false;
                mnuDeleteUnusedPartiesEnabled = false;
                //mnuEliminateDuplicateParties.Visible = false;
                //mnuDeleteUnusedParties.Visible = false;
            }
            return ReEnableMenus;
        }

        public void UpdateDatesOneMonth(bool boolCheckDigits = false)
        {
            string strReturn = "";
            // vbPorter upgrade warning: intCheck As short --> As int	OnWrite(int, double)
            short intCheck = 0;
            int ZZ;
            string strTemp = "";
            // validate that the user has TRIO Permissions for this function
            if (!boolCheckDigits)
            {
                strReturn = Interaction.InputBox("This routine must be used only at the request of a TRIO Software representative. Enter TRIO Software Code to extend expiration dates one month", "");
            }
            else
            {
                strReturn = "98";
            }
            // THIS 98 CODE WAS CREATED SO THAT THE UPDATE CHECK DIGITS FUNCTION CAN
            // BE RUN WITHOUT THE USE OF SYSTEM MAINTENANCE. WE DO NOT WANT THE USER
            // TO ENTER THIS AT ANY TIME BECAUSE THEY CAN CHANGE THEIR DATES MANUALLY
            // AND THEN RUN THIS AND THE CHECK DIGITS WILL BE RECALCULATED AND ALL WILL
            // BE FINE. BE CAUTIOUS WHEN USING THIS FUNCTIONALITY.
            if (strReturn == "98")
            {
                strTemp = "98";
                strReturn = Interaction.InputBox("This routine must be used only at the request of a TRIO Software representative....Enter TRIO Software Check Digit Code", "");
            }
            if (strReturn.Length < 5)
                return;
            for (ZZ = 1; ZZ <= 3; ZZ++)
            {
                intCheck += FCConvert.ToInt16(Convert.ToByte(Strings.Mid(strReturn, ZZ, 1)[0]) / ZZ);
            }
            intCheck += FCConvert.ToInt16(Conversion.Val(Strings.Mid(strReturn, 4, strReturn.Length - 3)));
            if (intCheck != 269 - Conversion.Val(Strings.Mid(Strings.Format(DateTime.Today, "MM/dd/yy"), 1, 2)) + Conversion.Val(Strings.Mid(Strings.Format(DateTime.Today, "MM/dd/yy"), 4, 2)))
            {
                MessageBox.Show("This is not a valid code", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            // make a copy of their table WRE, WPP, WMV, and Modules
            /*? On Error Resume Next  */

            object FieldName;
            clsDRWrapper rsCopy = new clsDRWrapper();

            try
            {
                rsCopy.OpenRecordset("Select * from MODULES", "systemsettings");
                int x;
                string strFieldName = "";
                if (strTemp == "98")
                {
                    modCheckDigits.UpdateCheckDigits();
                    MessageBox.Show("TRIO will need to be restarted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    string strDate = "";
                    var rsSecurity = new clsDRWrapper();

                    try
                    {
                        if (rsSecurity.OpenRecordset("Select * from Modules", "systemsettings"))
                        {
                            if (rsSecurity.EndOfFile()) return;

                            rsSecurity.Edit();
                            for (x = 0; x <= rsSecurity.FieldsCount - 1; x++)
                            {
                                strFieldName = rsSecurity.Get_FieldsIndexName(x);
                                // For Each fld In rsSecurity.AllFields
                                if (Strings.Right("    " + strFieldName, 4) == "Date")
                                {
                                    if ((Conversion.Val(Strings.Left(modDateRoutines.LongDate(rsSecurity.Get_Fields(strFieldName)), 2)) <= DateTime.Today.Month) && (Conversion.Val(Strings.Mid(modDateRoutines.LongDate(rsSecurity.Get_Fields(strFieldName)), 7, 4)) == DateTime.Today.Year) || (Conversion.Val(Strings.Mid(modDateRoutines.LongDate(rsSecurity.Get_Fields(strFieldName)), 7, 4)) < DateTime.Today.Year))
                                    {
                                        strDate = FCConvert.ToString(rsSecurity.Get_Fields(strFieldName));
                                        //FC:FINAL:AM:#1876 - check for date
                                        if (Information.IsDate(strDate))
                                        {
                                            rsSecurity.SetData(strFieldName, DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(strDate)));
                                        }
                                    }
                                }
                            }
                            rsSecurity.Update();
                            modCheckDigits.UpdateCheckDigits();
                            MessageBox.Show("Update completed successfully. You must restart TRIO for the changes to take effect.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    finally
                    {
                        rsSecurity.DisposeOf();
                    }
                }
            }
            finally
            {
                rsCopy.DisposeOf();
            }
        }

        public static void FixProcess()
        {
            frmBusy bWait = new frmBusy();
            FCUtils.StartTask(bWait, () =>
            {
                try
                {                    
                    bWait.Message = "Checking Dates";
                    bWait.StartBusy();
                    cSystemWebAPIController swac = new cSystemWebAPIController();
                    if (!swac.UpdateSubscriptionsFromAPI(modGlobalConstants.Statics.MuniName))
                    {
                        bWait.StopBusy();
                        bWait.Unload();
                        if (!swac.LastErrorMessage.IsNullOrWhiteSpace())
                        {
                            MessageBox.Show( swac.LastErrorMessage);
                        }
                        else
                        {
                            MessageBox.Show("Unable to process expiration information");
                        }
                    }
                    else
                    {
                        bWait.StopBusy();
                        bWait.Unload();
                        MessageBox.Show("Expiration dates have been updated.  You must log out and back in for the changes to take effect.", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }                   
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    bWait.StopBusy();
                    bWait.Unload();
                    if (Information.Err(ex).Number != 32755)
                    {
                        MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
                FCUtils.UnlockUserInterface();
            });
            bWait.Show(FCForm.FormShowEnum.Modal);
        }

        public class StaticVariables
        {
            /// </summary>
            /// Summary description for MDIParent.
            /// <summary>
            public int R = 0, c;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }

        [WebMethod]
        public static void SetTheMachineId(string machineId)
        { 
            MessageBox.Show("machine id is " + machineId, "MachineId", MessageBoxButtons.OK, MessageBoxIcon.None);
            if (!machineId.IsNullOrWhiteSpace())
            {
                StaticSettings.gGlobalSettings.MachineOwnerID = machineId;
                PrinterUtility.LoadPrintingSettings();
            }
        }
    }
}
