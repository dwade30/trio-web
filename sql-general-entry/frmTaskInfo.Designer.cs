﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWGNENTY
{
	/// <summary>
	/// Summary description for frmTaskInfo.
	/// </summary>
	partial class frmTaskInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbYes;
		public fecherFoundation.FCLabel lblYes;
		public fecherFoundation.FCFrame fraDescription;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCFrame fraEditStaff;
		public fecherFoundation.FCComboBox cboUser;
		public fecherFoundation.FCFrame fraInstallers;
		public fecherFoundation.FCGrid vsInstallers;
		public fecherFoundation.FCFrame fraReminder;
		public fecherFoundation.FCFrame fraReminderYes;
		public fecherFoundation.FCComboBox cboReminderTime;
		public fecherFoundation.FCLabel lblCreatedByName;
		public fecherFoundation.FCLine linCreatedBy;
		public fecherFoundation.FCLabel lblCreatedByLabel;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator3;
		public fecherFoundation.FCToolStripMenuItem mnuFileChangeDate;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
        public fecherFoundation.FCFrame fraOther;
        public fecherFoundation.FCRichTextBox rtfNotes;

        protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaskInfo));
            this.cmbYes = new fecherFoundation.FCComboBox();
            this.lblYes = new fecherFoundation.FCLabel();
            this.fraDescription = new fecherFoundation.FCFrame();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.fraEditStaff = new fecherFoundation.FCFrame();
            this.cboUser = new fecherFoundation.FCComboBox();
            this.fraInstallers = new fecherFoundation.FCFrame();
            this.vsInstallers = new fecherFoundation.FCGrid();
            this.fraReminder = new fecherFoundation.FCFrame();
            this.cboReminderTime = new fecherFoundation.FCComboBox();
            this.fraReminderYes = new fecherFoundation.FCFrame();
            this.lblCreatedByName = new fecherFoundation.FCLabel();
            this.linCreatedBy = new fecherFoundation.FCLine();
            this.lblCreatedByLabel = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileChangeDate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.cmdFileChangeDate = new fecherFoundation.FCButton();
            this.cmdFileDelete = new fecherFoundation.FCButton();
            this.fraOther = new fecherFoundation.FCFrame();
            this.rtfNotes = new fecherFoundation.FCRichTextBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDescription)).BeginInit();
            this.fraDescription.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEditStaff)).BeginInit();
            this.fraEditStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInstallers)).BeginInit();
            this.fraInstallers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsInstallers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReminder)).BeginInit();
            this.fraReminder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReminderYes)).BeginInit();
            this.fraReminderYes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChangeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOther)).BeginInit();
            this.fraOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtfNotes)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(648, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraDescription);
            this.ClientArea.Controls.Add(this.fraOther);
            this.ClientArea.Controls.Add(this.fraEditStaff);
            this.ClientArea.Controls.Add(this.fraInstallers);
            this.ClientArea.Controls.Add(this.fraReminder);
            this.ClientArea.Controls.Add(this.lblCreatedByName);
            this.ClientArea.Controls.Add(this.linCreatedBy);
            this.ClientArea.Controls.Add(this.lblCreatedByLabel);
            this.ClientArea.Size = new System.Drawing.Size(648, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileChangeDate);
            this.TopPanel.Controls.Add(this.cmdFileDelete);
            this.TopPanel.Size = new System.Drawing.Size(648, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileChangeDate, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(363, 30);
            this.HeaderText.Text = "Enter Calendar Item Information";
            // 
            // cmbYes
            // 
            this.cmbYes.AutoSize = false;
            this.cmbYes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYes.FormattingEnabled = true;
            this.cmbYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbYes.Location = new System.Drawing.Point(96, 0);
            this.cmbYes.Name = "cmbYes";
            this.cmbYes.Size = new System.Drawing.Size(118, 40);
            this.cmbYes.TabIndex = 1;
            this.cmbYes.Text = "No";
            this.cmbYes.SelectedIndexChanged += new System.EventHandler(this.cmbYes_SelectedIndexChanged);
            // 
            // lblYes
            // 
            this.lblYes.AutoSize = true;
            this.lblYes.Location = new System.Drawing.Point(2, 14);
            this.lblYes.Name = "lblYes";
            this.lblYes.Size = new System.Drawing.Size(73, 15);
            this.lblYes.TabIndex = 0;
            this.lblYes.Text = "REMINDER";
            // 
            // fraDescription
            // 
            this.fraDescription.Controls.Add(this.txtDescription);
            this.fraDescription.Location = new System.Drawing.Point(30, 202);
            this.fraDescription.Name = "fraDescription";
            this.fraDescription.Size = new System.Drawing.Size(570, 90);
            this.fraDescription.TabIndex = 4;
            this.fraDescription.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(20, 30);
            this.txtDescription.MaxLength = 50;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(523, 40);
            this.txtDescription.TabIndex = 0;
            // 
            // fraEditStaff
            // 
            this.fraEditStaff.Controls.Add(this.cboUser);
            this.fraEditStaff.Location = new System.Drawing.Point(30, 30);
            this.fraEditStaff.Name = "fraEditStaff";
            this.fraEditStaff.Size = new System.Drawing.Size(289, 86);
            this.fraEditStaff.TabIndex = 0;
            this.fraEditStaff.Text = "User";
            // 
            // cboUser
            // 
            this.cboUser.AutoSize = false;
            this.cboUser.BackColor = System.Drawing.SystemColors.Window;
            this.cboUser.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboUser.FormattingEnabled = true;
            this.cboUser.Location = new System.Drawing.Point(20, 30);
            this.cboUser.Name = "cboUser";
            this.cboUser.Size = new System.Drawing.Size(248, 40);
            this.cboUser.TabIndex = 0;
            this.cboUser.Text = "Combo1";
            // 
            // fraInstallers
            // 
            this.fraInstallers.Controls.Add(this.vsInstallers);
            this.fraInstallers.Location = new System.Drawing.Point(30, 30);
            this.fraInstallers.Name = "fraInstallers";
            this.fraInstallers.Size = new System.Drawing.Size(244, 161);
            this.fraInstallers.TabIndex = 1;
            this.fraInstallers.Text = "User";
            // 
            // vsInstallers
            // 
            this.vsInstallers.AllowSelection = false;
            this.vsInstallers.AllowUserToResizeColumns = false;
            this.vsInstallers.AllowUserToResizeRows = false;
            this.vsInstallers.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsInstallers.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsInstallers.BackColorBkg = System.Drawing.Color.Empty;
            this.vsInstallers.BackColorFixed = System.Drawing.Color.Empty;
            this.vsInstallers.BackColorSel = System.Drawing.Color.Empty;
            this.vsInstallers.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsInstallers.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsInstallers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsInstallers.ColumnHeadersHeight = 30;
            this.vsInstallers.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.vsInstallers.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsInstallers.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsInstallers.DragIcon = null;
            this.vsInstallers.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsInstallers.ExtendLastCol = true;
            this.vsInstallers.FixedCols = 0;
            this.vsInstallers.FixedRows = 0;
            this.vsInstallers.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsInstallers.FrozenCols = 0;
            this.vsInstallers.GridColor = System.Drawing.Color.Empty;
            this.vsInstallers.GridColorFixed = System.Drawing.Color.Empty;
            this.vsInstallers.Location = new System.Drawing.Point(20, 30);
            this.vsInstallers.Name = "vsInstallers";
            this.vsInstallers.OutlineCol = 0;
            this.vsInstallers.ReadOnly = true;
            this.vsInstallers.RowHeadersVisible = false;
            this.vsInstallers.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsInstallers.RowHeightMin = 0;
            this.vsInstallers.Rows = 0;
            this.vsInstallers.ScrollTipText = null;
            this.vsInstallers.ShowColumnVisibilityMenu = false;
            this.vsInstallers.ShowFocusCell = false;
            this.vsInstallers.Size = new System.Drawing.Size(205, 124);
            this.vsInstallers.StandardTab = true;
            this.vsInstallers.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsInstallers.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsInstallers.TabIndex = 0;
            this.vsInstallers.KeyDown += new Wisej.Web.KeyEventHandler(this.vsInstallers_KeyDownEvent);
            this.vsInstallers.Click += new System.EventHandler(this.vsInstallers_ClickEvent);
            // 
            // fraReminder
            // 
            this.fraReminder.Controls.Add(this.cboReminderTime);
            this.fraReminder.Controls.Add(this.fraReminderYes);
            this.fraReminder.Location = new System.Drawing.Point(30, 432);
            this.fraReminder.Name = "fraReminder";
            this.fraReminder.Size = new System.Drawing.Size(570, 88);
            this.fraReminder.TabIndex = 5;
            this.fraReminder.Text = "Reminder";
            // 
            // cboReminderTime
            // 
            this.cboReminderTime.AutoSize = false;
            this.cboReminderTime.BackColor = System.Drawing.SystemColors.Window;
            this.cboReminderTime.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReminderTime.Enabled = false;
            this.cboReminderTime.FormattingEnabled = true;
            this.cboReminderTime.Items.AddRange(new object[] {
            "Day of",
            "1 Day Before",
            "1 Week Before"});
            this.cboReminderTime.Location = new System.Drawing.Point(274, 30);
            this.cboReminderTime.Name = "cboReminderTime";
            this.cboReminderTime.Size = new System.Drawing.Size(271, 40);
            this.cboReminderTime.TabIndex = 0;
            // 
            // fraReminderYes
            // 
            this.fraReminderYes.AppearanceKey = "groupBoxNoBorders";
            this.fraReminderYes.Controls.Add(this.cmbYes);
            this.fraReminderYes.Controls.Add(this.lblYes);
            this.fraReminderYes.Location = new System.Drawing.Point(30, 30);
            this.fraReminderYes.Name = "fraReminderYes";
            this.fraReminderYes.Size = new System.Drawing.Size(228, 50);
            this.fraReminderYes.TabIndex = 1;
            // 
            // lblCreatedByName
            // 
            this.lblCreatedByName.Location = new System.Drawing.Point(479, 30);
            this.lblCreatedByName.Name = "lblCreatedByName";
            this.lblCreatedByName.Size = new System.Drawing.Size(141, 17);
            this.lblCreatedByName.TabIndex = 3;
            this.lblCreatedByName.Text = "LABEL1";
            this.lblCreatedByName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // linCreatedBy
            // 
            this.linCreatedBy.BorderColor = System.Drawing.SystemColors.WindowText;
            this.linCreatedBy.BorderWidth = ((short)(1));
            this.linCreatedBy.LineWidth = 0;
            this.linCreatedBy.Location = new System.Drawing.Point(304, 80);
            this.linCreatedBy.Name = "linCreatedBy";
            this.linCreatedBy.Size = new System.Drawing.Size(156, 1);
            this.linCreatedBy.Visible = false;
            this.linCreatedBy.X1 = 7080F;
            this.linCreatedBy.X2 = 8640F;
            this.linCreatedBy.Y1 = 420F;
            this.linCreatedBy.Y2 = 420F;
            // 
            // lblCreatedByLabel
            // 
            this.lblCreatedByLabel.Location = new System.Drawing.Point(360, 30);
            this.lblCreatedByLabel.Name = "lblCreatedByLabel";
            this.lblCreatedByLabel.Size = new System.Drawing.Size(108, 17);
            this.lblCreatedByLabel.TabIndex = 2;
            this.lblCreatedByLabel.Text = "CREATED BY";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileDelete,
            this.mnuFileSeperator3,
            this.mnuFileChangeDate,
            this.mnuFileSeperator,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileDelete
            // 
            this.mnuFileDelete.Index = 0;
            this.mnuFileDelete.Name = "mnuFileDelete";
            this.mnuFileDelete.Text = "Delete";
            this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // mnuFileSeperator3
            // 
            this.mnuFileSeperator3.Index = 1;
            this.mnuFileSeperator3.Name = "mnuFileSeperator3";
            this.mnuFileSeperator3.Text = "-";
            // 
            // mnuFileChangeDate
            // 
            this.mnuFileChangeDate.Enabled = false;
            this.mnuFileChangeDate.Index = 2;
            this.mnuFileChangeDate.Name = "mnuFileChangeDate";
            this.mnuFileChangeDate.Text = "Change Task Date";
            this.mnuFileChangeDate.Click += new System.EventHandler(this.mnuFileChangeDate_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 3;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 4;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 5;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 6;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(253, 26);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(142, 48);
            this.cmdProcessSave.TabIndex = 0;
            this.cmdProcessSave.Text = "Save & Exit";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // cmdFileChangeDate
            // 
            this.cmdFileChangeDate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileChangeDate.AppearanceKey = "toolbarButton";
            this.cmdFileChangeDate.Enabled = false;
            this.cmdFileChangeDate.Location = new System.Drawing.Point(416, 26);
            this.cmdFileChangeDate.Name = "cmdFileChangeDate";
            this.cmdFileChangeDate.Size = new System.Drawing.Size(127, 24);
            this.cmdFileChangeDate.TabIndex = 1;
            this.cmdFileChangeDate.Text = "Change Task Date";
            this.cmdFileChangeDate.Click += new System.EventHandler(this.mnuFileChangeDate_Click);
            // 
            // cmdFileDelete
            // 
            this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileDelete.AppearanceKey = "toolbarButton";
            this.cmdFileDelete.Location = new System.Drawing.Point(548, 26);
            this.cmdFileDelete.Name = "cmdFileDelete";
            this.cmdFileDelete.Size = new System.Drawing.Size(52, 24);
            this.cmdFileDelete.TabIndex = 2;
            this.cmdFileDelete.Text = "Delete";
            this.cmdFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // fraOther
            // 
            this.fraOther.Controls.Add(this.rtfNotes);
            this.fraOther.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fraOther.Location = new System.Drawing.Point(30, 304);
            this.fraOther.Name = "fraOther";
            this.fraOther.Size = new System.Drawing.Size(570, 112);
            this.fraOther.TabIndex = 7;
            this.fraOther.Text = "Notes";
            // 
            // rtfNotes
            // 
            this.rtfNotes.Location = new System.Drawing.Point(20, 25);
            this.rtfNotes.Multiline = true;
            this.rtfNotes.Name = "rtfNotes";
            this.rtfNotes.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.rtfNotes.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.rtfNotes.SelTabCount = null;
            this.rtfNotes.Size = new System.Drawing.Size(523, 69);
            this.rtfNotes.TabIndex = 8;
            // 
            // frmTaskInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(648, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmTaskInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Enter Calendar Item Information";
            this.Load += new System.EventHandler(this.frmTaskInfo_Load);
            this.Activated += new System.EventHandler(this.frmTaskInfo_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaskInfo_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDescription)).EndInit();
            this.fraDescription.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraEditStaff)).EndInit();
            this.fraEditStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraInstallers)).EndInit();
            this.fraInstallers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsInstallers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReminder)).EndInit();
            this.fraReminder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraReminderYes)).EndInit();
            this.fraReminderYes.ResumeLayout(false);
            this.fraReminderYes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChangeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOther)).EndInit();
            this.fraOther.ResumeLayout(false);
            this.fraOther.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtfNotes)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
        private FCButton cmdFileDelete;
        private FCButton cmdFileChangeDate;
    }
}
