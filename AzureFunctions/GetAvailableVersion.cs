using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AzureFunctions
{
    public static class GetAvailableVersion
    {
        [FunctionName("GetAvailableVersion")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            BlobServiceClient blobServiceClient = new BlobServiceClient(Environment.GetEnvironmentVariable("StorageConnectionString"));
            BlobContainerClient container = blobServiceClient.GetBlobContainerClient("triowebupdates");

            string version = "";

            foreach (BlobItem blobItem in container.GetBlobs().OrderBy(x => x.Name))
            {
	            version = blobItem.Name.Substring(0, blobItem.Name.Length - 4);
            }
            
            return version != null
                ? (ActionResult)new OkObjectResult($"{version}")
                : new BadRequestObjectResult("No updates found on site.");
        }
    }
}
