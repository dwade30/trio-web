//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public partial class frmPayrollDistribution : BaseForm
	{
		public frmPayrollDistribution()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            //FC:FINAL:BSE #2558 Allow only numbers
            this.txtTax.AllowOnlyNumericInput();
            this.txtDed.AllowOnlyNumericInput();
            chkDeductions = new System.Collections.Generic.List<FCCheckBox>();
            chkDeductions.AddControlArrayElement(chkDeductions_0, 0);
            chkDeductions.AddControlArrayElement(chkDeductions_1, 1);
            chkMatches = new System.Collections.Generic.List<FCCheckBox>();
            chkMatches.AddControlArrayElement(chkMatches_0, 0);
            chkMatches.AddControlArrayElement(chkMatches_1, 1);
            chkMatches.AddControlArrayElement(chkMatches_2, 2);
            chkDD = new System.Collections.Generic.List<FCCheckBox>();
            chkDD.AddControlArrayElement(chkDD_0, 0);
            chkDD.AddControlArrayElement(chkDD_1, 1);
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPayrollDistribution InstancePtr
		{
			get
			{
				return (frmPayrollDistribution)Sys.GetInstance(typeof(frmPayrollDistribution));
			}
		}

		protected frmPayrollDistribution _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: DAN C. SOLTESZ
		// DATE:       May 31,2001
		// MODIFIED ON: Aug 13, 2001
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		private bool boolUseProjects;
		private bool boolSkipResize;
		private bool boolScrollBars;
		private clsDRWrapper rsDistribution = new clsDRWrapper();
		private clsDRWrapper rsDistribution2 = new clsDRWrapper();
		private clsDRWrapper rsPayCatType = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int gintRow;
		// vbPorter upgrade warning: intInitialDedUpTop As int	OnWrite(string)
		private int intInitialDedUpTop;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		private DialogResult intDataChanged;
		private bool boolMSGShown;
		private bool NotValidData;
		private int intDataEntry;
		private bool boolBiWeeklyTown;
		private bool boolDoNotExit;
		private int intHoldCol;
		private int intHoldRow;
		// vbPorter upgrade warning: intRowChange As int	OnWriteFCConvert.ToInt32(
		private int intRowChange;
		private bool TotalFlag;
		private int intNewCount;
		private bool NewFlag;
		private bool boolFromAccount;
		private bool boolFormLoading;
		private double dblTaxColTotals;
		// vbPorter upgrade warning: intChildID As int	OnRead(string)
		private int intChildID;
		private bool boolChild;
		private int intAID;
		private int intTaxID;
		// vbPorter upgrade warning: intResponse As int	OnWrite(DialogResult)
		private DialogResult intResponse;
		private string strEmployeeDeptDiv = "";
		private int lngAccountRow;
		private bool boolFicaExempt;
		private bool boolUnempExempt;
		private bool boolCancel;
		private bool boolMsgAlreadyShown;
		private bool boolFromDataEntry;
		private bool boolCantEdit;
		private bool boolActive;
		const int CNSTGRIDDISTMCOLID = 0;
		const int CNSTGRIDDISTMCOLPOSITION = 1;
		const int CNSTGRIDDISTMCOLSTATUS = 2;
		const int CNSTGRIDDISTMCOLREPORTTYPE = 3;
		const int CNSTGRIDDISTMCOLHDC = 4;
		const int CNSTGRIDDISTMCOLFEDCOMPAMOUNT = 5;
		const int CNSTGRIDCOLDISTM = 5;
		const int CNSTGRIDCOLNUMBER = 0;
		const int CNSTGRIDCOLCODE = 1;
		const int CNSTGRIDCOLACCOUNT = 2;
		const int CNSTGRIDCOLTAXTYPE = 4;
		const int CNSTGRIDCOLNUMWEEKS = 13;
		const int CNSTGRIDCOLWEEKSWITHHELD = 14;
		const int CNSTGRIDCOLDEFAULTHOURS = 15;
		const int CNSTGRIDCOLHOURSWEEK = 16;
		const int CNSTGRIDCOLGROSS = 17;
		const int cnstgridcolID = 18;
		const int CNSTGRIDCOLCHANGES = 19;
		const int CNSTGRIDCOLPERIODTAXES = 20;
		const int CNSTGRIDCOLPERIODDEDS = 21;
		const int CNSTGRIDCOLEXISTING = 22;
		const int CNSTGRIDCOLPROJECT = 24;
		const int CNSTGRIDCOLCONTRACT = 23;
		const int CNSTGRIDCOLCONTRACTACCOUNTLIST = 25;
		private bool boolPrefillAccount;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private clsGridAccount clsGA = new clsGridAccount();

		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void SetupContractCol()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			rsTemp.OpenRecordset("select * from contracts where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by ID", "twpy0000.vb1");
			if (rsTemp.EndOfFile())
			{
				Grid.ColHidden(CNSTGRIDCOLCONTRACT, true);
			}
			else
			{
				strTemp = "#0;None|";
				while (!rsTemp.EndOfFile())
				{
					strTemp += "#" + rsTemp.Get_Fields("contractid") + ";" + rsTemp.Get_Fields("Description") + "|";
					rsTemp.MoveNext();
				}
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				Grid.ColComboList(CNSTGRIDCOLCONTRACT, strTemp);
				Grid.ColHidden(CNSTGRIDCOLCONTRACT, false);
			}
		}

		private void frmPayrollDistribution_Activated(object sender, System.EventArgs e)
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				lblEmployeeNumber.Text = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				if (modGlobalVariables.Statics.gboolDataEntryOpen && intDataChanged != 0)
				{
					intResponse = MessageBox.Show("Data has not been saved on Distribution Screen. Select Yes to Save data and refresh screen, select No to ignore changes and refresh screen, or select Cancel to continue without saving or refreshing.", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					switch (intResponse)
					{
						case DialogResult.Yes:
							{
								mnuSave_Click();
								RefreshScreen();
								break;
							}
						case DialogResult.No:
							{
								RefreshScreen();
								break;
							}
						case DialogResult.Cancel:
							{
								break;
							}
					}
				}
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;

				if (modGlobalVariables.Statics.gboolDataEntry)
				{
					if (Grid.Rows >= 3)
					{
						Grid.Select(2, CNSTGRIDCOLHOURSWEEK);
					}
				}
				//FC:FINAL:DDU:#2711 - show both buttons if form is oppened from frmEmployeeListing
				if (frmEmployeeListing.InstancePtr.FromDataEntry)
				{
                    cmdSave.Enabled = true;
                    cmdSave.Visible = true;
					cmdSave.Click += mnuSave_Click;
					cmdSaveExit.Text = "Save & Exit";
					cmdSaveExit.Size = new Size(130, 48);
					cmdSaveExit.Click -= mnuSave_Click;
					cmdSaveExit.Click += mnuSaveExit_Click;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void RefreshScreen()
		{
			LoadData();
		}

		private void frmPayrollDistribution_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyDown";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();

				if (fecherFoundation.Strings.UCase(this.ActiveControl.GetName()) == "GRID")
				{
					if (Grid.Col == CNSTGRIDCOLACCOUNT && Grid.Row > 0)
					{
						modNewAccountBox.CheckFormKeyDown(Grid, Grid.Row, Grid.Col, KeyCode, Shift, Grid.EditSelStart, Grid.EditText, Grid.EditSelLength);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayrollDistribution_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed, if so close active from
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Reload()
		{
			int counter;
			boolCantEdit = false;
			clsGA.GRID7Light = Grid;
			clsGA.DefaultAccountType = "E";
			clsGA.OnlyAllowDefaultType = false;
			clsGA.AccountCol = CNSTGRIDCOLACCOUNT;
			clsGA.Validation = true;
			clsGA.AllowSplits = modRegionalTown.IsRegionalTown();
			//vsElasticLight1.Enabled = true;
			if (modGlobalVariables.Statics.gboolDataEntry)
			{
				boolFromDataEntry = true;
				modCoreysSweeterCode.Statics.gstrEditList += modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + ",";
			}
			else
			{
				boolFromDataEntry = false;
			}
			boolFormLoading = true;
			modGlobalRoutines.CheckCurrentEmployee();
			// Call GetFormats
			// how many new rows were added.
			intNewCount = 0;
			TotalFlag = true;
			boolUnempExempt = false;
			boolFicaExempt = false;
			// matthew 5/18/2005 Call ID 68979
			// boolDataEntry = gboolDataEntry
			if (modGlobalVariables.Statics.gboolDataEntry)
			{
				intDataEntry = 1;
			}
			else
			{
				intDataEntry = 0;
			}
			// was a new row added
			NewFlag = false;
			// open the forms global database connection
			rsDistribution.DefaultDB = "TWPY0000.vb1";
			rsDistribution2.DefaultDB = "TWPY0000.vb1";
			// set the size of the form
			modGlobalFunctions.SetFixedSize(this, 0);
			intDataChanged = 0;
			// find all data for current employee
			SetGridProperties();
			LoadData();
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select DeptDiv from tblEmployeeMaster where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				if (!modGlobalVariables.Statics.gboolBudgetary)
					strEmployeeDeptDiv = "E __-__-__-__";
			}
			else
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					modAccountTitle.SetAccountFormats();
					strEmployeeDeptDiv = "E " + rsData.Get_Fields("DeptDiv") + "-" + Strings.StrDup(FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)), "_") + "-" + Strings.StrDup(FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)), "_");
				}
			}
			rsData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PayFreqWeekly")))
			{
				boolBiWeeklyTown = false;
			}
			else if (rsData.Get_Fields_Boolean("PayFreqBiWeekly"))
			{
				boolBiWeeklyTown = true;
			}
			modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
			boolFormLoading = false;
			lblAccountDesc.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblAccountDesc.Font = FCUtils.FontChangeBold(lblAccountDesc.Font, true);
			if (modGlobalVariables.Statics.gboolDataEntry)
			{
				if (Grid.Rows > 3)
					Grid.Col = cnstgridcolID;
			}
			// Bug Call ID 5739
			clsDRWrapper rsExecute = new clsDRWrapper();
			if (modGlobalConstants.Statics.gboolBD)
			{
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (rsData.EndOfFile())
				{
					mnuLongAccountScreen.Enabled = false;
				}
			}
			else
			{
				mnuLongAccountScreen.Enabled = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				if (!boolFromDataEntry)
				{
					boolCantEdit = true;
					cmdSave.Enabled = false;
					cmdSaveExit.Enabled = false;
					Grid.Editable = FCGrid.EditableSettings.flexEDNone;
					txtDed.Enabled = false;
					txtTax.Enabled = false;
					chkDD[0].Enabled = false;
					chkDD[1].Enabled = false;
					chkDeductions[0].Enabled = false;
					chkDeductions[1].Enabled = false;
					chkMatches[0].Enabled = false;
					chkMatches[1].Enabled = false;
					chkMatches[2].Enabled = false;
					chkVacation.Enabled = false;
				}
				cmdDeleteLine.Enabled = false;
				mnuMultAdd.Enabled = false;
				cmdNewLine.Enabled = false;
				cmdUpdateBaseRates.Enabled = false;
				clsGA.GRID7Light = null;
			}
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
			FillControlInformationClass();
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
		}

		private void frmPayrollDistribution_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modCoreysSweeterCode.Statics.gstrEditList = "";
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "PENOBSCOT COUNTY")
				{
					cmdEmployeeSchedule.Visible = false;
					menusepar5.Visible = false;
				}
				if (FCConvert.CBool(modRegistry.GetRegistryKey("PrefillAccounts", "PY", "True")))
				{
					boolPrefillAccount = true;
				}
				else
				{
					boolPrefillAccount = false;
				}
				Reload();

                mnuFile_Click(null, EventArgs.Empty);
            }
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayrollDistribution_Resize(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Resize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// make grid look pretty if user decides to resize
				if (boolFormLoading)
				{
				}
				else
				{
					if (intDataEntry == 1)
					{
						SetDataEntryGrid();
					}
					else if (intDataEntry == 0)
					{
						SetDistributionGrid();
					}
					else if (intDataEntry == 2)
					{
						SetLongAccountGrid();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Form_Resize()
		{
			frmPayrollDistribution_Resize(this, new System.EventArgs());
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				if (intDataChanged == (DialogResult)2 && !boolCantEdit)
				{
					e.Cancel = true;
					return;
				}
				if (boolFromDataEntry)
				{
					modGlobalVariables.Statics.gboolDataEntry = false;
					// frmEmployeeListing.Show , MDIParent
				}
				else
				{
					modGlobalVariables.Statics.gboolDataEntry = false;
					//MDIParent.InstancePtr.Show();
				}
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetupProjectCol()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			strTemp = "#0;None|";
			rsTemp.OpenRecordset("select * from projectmaster order by longdescription", "twbd0000.vb1");
			while (!rsTemp.EndOfFile())
			{
				strTemp += "#" + rsTemp.Get_Fields("id") + ";" + rsTemp.Get_Fields("LONGDESCRIPTION") + "\t" + rsTemp.Get_Fields("projectcode") + "|";
				rsTemp.MoveNext();
			}
			strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			Grid.ColComboList(CNSTGRIDCOLPROJECT, strTemp);
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				Grid.Cols = 26;
                Grid.FixedRows = 2;
                Grid.ColFormat(11, "#0.0000");
				Grid.ColFormat(12, "0.00");
				Grid.ColFormat(CNSTGRIDCOLDEFAULTHOURS, "#0.00");
				Grid.ColFormat(CNSTGRIDCOLHOURSWEEK, "#0.00");
				Grid.ColFormat(CNSTGRIDCOLGROSS, "##0.00");
				GridTotals.ColFormat(1, "#0.00");
				GridTotals.ColFormat(2, "##0.00");
                //FC:FINAL:SBE - #i2485 - align numeric cols to right. Cell Formatting is not triggered each time, and alignemnt based on cell data is not ok
                GridTotals.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridTotals.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridTotals.TextMatrix(0, 0, "Totals (Pay)");
				GridTotals.TextMatrix(1, 0, "Non Pay");
				GridTotals.TextMatrix(2, 0, "Ded Totals");
				GridTotals.TextMatrix(2, 1, "Ded Totals");
				GridTotals.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				//FC:FINAL:SBE - #i2485 - don't need to merge row. In original Merge row will check cell contents before merge
				//GridTotals.MergeRow(1, true);
				//GridTotals.MergeRow(2, true);
				Grid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				Grid.MergeRow(0, true);
				Grid.TextMatrix(1, 0, "#");
				Grid.TextMatrix(1, 1, "Code");
				Grid.TextMatrix(1, 2, "Acct");
				Grid.TextMatrix(1, 3, "Cat");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.TextMatrix(1, CNSTGRIDCOLTAXTYPE, "Tax");
				Grid.TextMatrix(1, 5, "M");
				Grid.TextMatrix(1, 6, "E");
				Grid.TextMatrix(1, 7, "U");
				Grid.TextMatrix(1, 8, "W");
				Grid.TextMatrix(1, 9, "W/C");
				Grid.TextMatrix(1, 10, "CD");      
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                Grid.TextMatrix(1, 11, "Base Rate");
                //FC:FINAL:BSE #2558 set allowed keys for client side check 
                Grid.ColAllowedKeys(11, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.TextMatrix(1, 12, "Mult");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.MergeRow(0, true, CNSTGRIDCOLNUMWEEKS, CNSTGRIDCOLWEEKSWITHHELD);
                Grid.TextMatrix(0, CNSTGRIDCOLNUMWEEKS, "Tax Periods");
				//Grid.TextMatrix(0, CNSTGRIDCOLWEEKSWITHHELD, Grid.TextMatrix(0, CNSTGRIDCOLNUMWEEKS));
				Grid.TextMatrix(1, CNSTGRIDCOLNUMWEEKS, "Calc");
                //FC:FINAL:BSE #2558 set allowed keys for client side check 
                Grid.ColAllowedKeys(CNSTGRIDCOLNUMWEEKS, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(CNSTGRIDCOLNUMWEEKS, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.TextMatrix(1, CNSTGRIDCOLWEEKSWITHHELD, "Whld");
                //FC:FINAL:BSE #2558 set allowed keys for client side check 
                Grid.ColAllowedKeys(CNSTGRIDCOLWEEKSWITHHELD, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(CNSTGRIDCOLWEEKSWITHHELD, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.TextMatrix(1, CNSTGRIDCOLDEFAULTHOURS, "Def");
                //FC:FINAL:BSE #2558 set allowed keys for client side check 
                Grid.ColAllowedKeys(CNSTGRIDCOLDEFAULTHOURS, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(CNSTGRIDCOLDEFAULTHOURS, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.TextMatrix(1, CNSTGRIDCOLHOURSWEEK, "This Wk");
                //FC:FINAL:BSE #2558 set allowed keys for client side check 
                Grid.ColAllowedKeys(CNSTGRIDCOLHOURSWEEK, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(CNSTGRIDCOLHOURSWEEK, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.TextMatrix(1, CNSTGRIDCOLGROSS, "Gross");
                //FC:FINAL:BSE #2555 align header
                Grid.ColAlignment(CNSTGRIDCOLGROSS, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.MergeRow(0, true, CNSTGRIDCOLDEFAULTHOURS, CNSTGRIDCOLHOURSWEEK);
                Grid.TextMatrix(0, CNSTGRIDCOLDEFAULTHOURS, "Hours");
				//Grid.TextMatrix(0, CNSTGRIDCOLHOURSWEEK, "Hours");
                Grid.MergeRow(0, true, 10, 11);
                Grid.TextMatrix(0, 10, "Pay");
				Grid.TextMatrix(0, 11, "Pay");
				// .TextMatrix(0, CNSTGRIDCOLNUMWEEKS) = "Tax"
				Grid.TextMatrix(0, 1, "                                                                              " + "                                                                                          ");
                Grid.MergeRow(0, true, 5, 9);
                Grid.TextMatrix(0, 5, "Include For");
				//Grid.TextMatrix(0, 6, "Include For");
				//Grid.TextMatrix(0, 7, "Include For");
				//Grid.TextMatrix(0, 8, "Include For");
				//Grid.TextMatrix(0, 9, "Include For");
				Grid.TextMatrix(1, CNSTGRIDCOLPROJECT, "Project");
				Grid.TextMatrix(1, CNSTGRIDCOLCONTRACT, "Contract");
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strTemp = "";
				rsTemp.OpenRecordset("select * from contracts where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by ID", "twpy0000.vb1");
				if (rsTemp.EndOfFile())
				{
					Grid.ColHidden(CNSTGRIDCOLCONTRACT, true);
				}
				else
				{
					strTemp = "#0;None|";
					while (!rsTemp.EndOfFile())
					{
						strTemp += "#" + rsTemp.Get_Fields("ID") + ";" + rsTemp.Get_Fields("Description") + "|";
						rsTemp.MoveNext();
					}
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					Grid.ColComboList(CNSTGRIDCOLCONTRACT, strTemp);
					Grid.ColHidden(CNSTGRIDCOLCONTRACT, false);
				}
				boolUseProjects = false;
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsTemp.OpenRecordset("select * from budgetary", "twbd0000.vb1");
					if (!rsTemp.EndOfFile())
					{
						if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("useprojectnumber"))) == "Y")
						{
							Grid.ColHidden(CNSTGRIDCOLPROJECT, false);
							boolUseProjects = true;
							SetupProjectCol();
						}
						else
						{
							Grid.ColHidden(CNSTGRIDCOLPROJECT, true);
						}
					}
					else
					{
						Grid.ColHidden(CNSTGRIDCOLPROJECT, true);
					}
				}
				else
				{
					Grid.ColHidden(CNSTGRIDCOLPROJECT, true);
				}
				//Grid.FixedRows = 2;
				//Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, CNSTGRIDCOLGROSS, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				// ******************************************************************************************
				// this would be if we were to have a new distribution code type that was just weeks 1-4 and
				// not 5. matthew 10/26/2004
				if (boolBiWeeklyTown)
				{
					Grid.ColComboList(1, "#1;N\tNot Used|#2;W\tWeekly|#3;O\tOne Week Only|#14;1\tWeek 1 Only|#15;2\tWeek 2 Only|#16;3\tWeek 3 Only|#17;4\tWeek 4 Only|#13;T\tWeek 1&2 Not 3|#4;S1      -Separate Check(W)|#5;S2      -Separate Check(W)|#6;S3      -Separate Check(W)|#7;S4      -Separate Check(W)|#8;S5      -Separate Check(W)|#9;S6      -Separate Check(W)|#10;S7      -Separate Check(W)|#11;S8      -Separate Check(W)|#12;S9      -Separate Check(W)");
				}
				else
				{
					Grid.ColComboList(1, "#1;N\tNot Used|#2;W\tWeekly|#3;O\tOne Week Only|#14;1\tWeek 1 Only|#15;2\tWeek 2 Only|#16;3\tWeek 3 Only|#17;4\tWeek 4 Only|#13;T\tWeek 1-4 Not 5|#4;S1      -Separate Check(W)|#5;S2      -Separate Check(W)|#6;S3      -Separate Check(W)|#7;S4      -Separate Check(W)|#8;S5      -Separate Check(W)|#9;S6      -Separate Check(W)|#10;S7      -Separate Check(W)|#11;S8      -Separate Check(W)|#12;S9      -Separate Check(W)");
					// .ColComboList(1, "#1;N         -Not Used|#2;W         -Weekly|#3;1         -One Week Only|#13;5         -Week 1-4 Not 5|#4;S1      -Separate Check(W)|#5;S2      -Separate Check(W)|#6;S3      -Separate Check(W)|#7;S4      -Separate Check(W)|#8;S5      -Separate Check(W)|#9;S6      -Separate Check(W)|#10;S7      -Separate Check(W)|#11;S8      -Separate Check(W)|#12;S9      -Separate Check(W)"
				}
				Grid.ColDataType(6, FCGrid.DataTypeSettings.flexDTBoolean);
				Grid.ColDataType(8, FCGrid.DataTypeSettings.flexDTBoolean);
				Grid.ColHidden(8, true);
				Grid.ColComboList(3, string.Empty);
				Grid.ColData(3, string.Empty);
				rsDistribution.OpenRecordset("SELECT * FROM tblPayCategories order by categorynumber", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					rsDistribution.MoveLast();
					rsDistribution.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblPayCategories table
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsDistribution.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							Grid.ColComboList(3, Grid.ColComboList(3) + "#" + rsDistribution.Get_Fields("ID") + ";" + rsDistribution.Get_Fields_Int32("CategoryNumber") + "\t" + rsDistribution.Get_Fields("Description") + " (" + rsDistribution.Get_Fields("Type") + ")");
						}
						else
						{
							Grid.ColComboList(3, Grid.ColComboList(3) + "|#" + rsDistribution.Get_Fields("ID") + ";" + rsDistribution.Get_Fields_Int32("CategoryNumber") + "\t" + rsDistribution.Get_Fields("Description") + "(" + rsDistribution.Get_Fields("Type") + ")");
						}
						Grid.ColData(3, FCConvert.ToString(Grid.ColData(3)) + rsDistribution.Get_Fields("ID") + "," + rsDistribution.Get_Fields_String("description") + " (" + rsDistribution.Get_Fields("type") + ");");
						rsDistribution.MoveNext();
					}
					if (FCConvert.ToString(Grid.ColData(3)) != string.Empty)
					{
						Grid.ColData(3, Strings.Mid(FCConvert.ToString(Grid.ColData(3)), 1, FCConvert.ToString(Grid.ColData(3)).Length - 1));
					}
				}
				// Dynamicly load column 4 but clear each time so we don't have duplicates
				Grid.ColComboList(CNSTGRIDCOLTAXTYPE, string.Empty);
				rsDistribution.OpenRecordset("SELECT * FROM tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					rsDistribution.MoveLast();
					rsDistribution.MoveFirst();
					// Makes this column work like a combo box
					// This fills the box with the values from the tblTaxStatusCodes table
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsDistribution.RecordCount()); intCounter++)
					{
						if (FCConvert.ToString(rsDistribution.Get_Fields("TaxStatusCode")) == "T")
						{
							intTaxID = FCConvert.ToInt16(rsDistribution.Get_Fields("ID"));
						}
						if (intCounter == 1)
						{
							Grid.ColComboList(CNSTGRIDCOLTAXTYPE, Grid.ColComboList(CNSTGRIDCOLTAXTYPE) + "#" + rsDistribution.Get_Fields("ID") + ";" + rsDistribution.Get_Fields("TaxStatusCode") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Description"))) != "" ? "\t" + rsDistribution.Get_Fields("Description") : ""));
						}
						else
						{
							Grid.ColComboList(CNSTGRIDCOLTAXTYPE, Grid.ColComboList(CNSTGRIDCOLTAXTYPE) + "|#" + rsDistribution.Get_Fields("ID") + ";" + rsDistribution.Get_Fields("TaxStatusCode") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Description"))) != "" ? "\t" + rsDistribution.Get_Fields("Description") : ""));
						}
						rsDistribution.MoveNext();
					}
				}
				Grid.ColComboList(10, string.Empty);
				rsDistribution.OpenRecordset("SELECT * FROM tblPayCodes order by PayCode");
				if (!rsDistribution.EndOfFile())
				{
					rsDistribution.MoveLast();
					rsDistribution.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblPayCategories table
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsDistribution.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							Grid.ColComboList(10, Grid.ColComboList(10) + "#" + rsDistribution.Get_Fields("ID") + ";" + rsDistribution.Get_Fields_String("PayCode") + "\t" + rsDistribution.Get_Fields("Description"));
							// we need to remember the ID so that when we add a new row we
							// can set the combo index to the ID which is the first element
							// in the combo box
							intAID = FCConvert.ToInt16(rsDistribution.Get_Fields("ID"));
						}
						else
						{
							Grid.ColComboList(10, Grid.ColComboList(10) + "|#" + rsDistribution.Get_Fields("ID") + ";" + rsDistribution.Get_Fields_String("PayCode") + "\t" + rsDistribution.Get_Fields("Description"));
						}
						rsDistribution.MoveNext();
					}
				}
				if (modGlobalVariables.Statics.gboolDataEntry)
				{
					SetDataEntryGrid();
				}
				else
				{
					SetDistributionGrid();
				}
                //FC:FINAL:AM:#4088 - set tooltips
                Grid.Columns[CNSTGRIDCOLTAXTYPE - 1].HeaderCell.ToolTipText = "Tax Status";
                Grid.Columns[5 - 1].HeaderCell.ToolTipText = "MainePERS";
                Grid.Columns[6 - 1].HeaderCell.ToolTipText = "Earned Income Credit";
                Grid.Columns[7 - 1].HeaderCell.ToolTipText = "Unemployment";
                Grid.Columns[9 - 1].HeaderCell.ToolTipText = "Workers Comp";
                Grid.Columns[10 - 1].HeaderCell.ToolTipText = "Pay Code";
                Grid.Columns[12 - 1].HeaderCell.ToolTipText = "Determined by Cat";
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetLongAccountGrid()
		{
			modGlobalVariables.Statics.gboolDataEntry = true;
			//FC:FINAL:DDU:#i2076 - designer verify this part
			//if ((Grid.RowHeight(0) * Grid.Rows + 100) < (this.HeightOriginal * 0.5 - 550))
			//{
			//	// .Height = (.RowHeight(0) * .Rows) - 100
			//	boolScrollBars = false;
			//}
			//else
			//{
			//	boolScrollBars = true;
			//}
			//Grid.HeightOriginal = FCConvert.ToInt32((this.HeightOriginal * 0.5) - 550);
			Grid.ColWidth(0, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(1, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			if (boolScrollBars)
			{
				Grid.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.198));
			}
			else
			{
				Grid.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.253));
			}
			Grid.ColHidden(CNSTGRIDCOLCONTRACTACCOUNTLIST, true);
			Grid.ColWidth(3, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			// .ColWidth(CNSTGRIDCOLTAXTYPE) = .Width * 0.05
			Grid.ColHidden(CNSTGRIDCOLTAXTYPE, true);
			Grid.ColWidth(5, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(7, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(8, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(9, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColWidth(10, FCConvert.ToInt32(Grid.WidthOriginal * 0.02));
            Grid.ColWidth(11, FCConvert.ToInt32(Grid.WidthOriginal * 0.12));
			Grid.ColWidth(12, FCConvert.ToInt32(Grid.WidthOriginal * 0.07));
			Grid.ColWidth(CNSTGRIDCOLNUMWEEKS, FCConvert.ToInt32(Grid.WidthOriginal * 0.1));
            Grid.ColWidth(CNSTGRIDCOLWEEKSWITHHELD, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
            Grid.ColWidth(CNSTGRIDCOLDEFAULTHOURS, FCConvert.ToInt32(Grid.WidthOriginal * 0.07));
            Grid.ColWidth(CNSTGRIDCOLHOURSWEEK, FCConvert.ToInt32(Grid.WidthOriginal * 0.08));
            Grid.ColWidth(CNSTGRIDCOLGROSS, FCConvert.ToInt32(Grid.WidthOriginal * 0.12));
            Grid.ColWidth(cnstgridcolID, 0);
			Grid.ColWidth(CNSTGRIDCOLCHANGES, 0);
			Grid.ColWidth(CNSTGRIDCOLPERIODTAXES, 0);
			Grid.ColWidth(CNSTGRIDCOLPERIODDEDS, 0);
			Grid.ColWidth(CNSTGRIDCOLEXISTING, 0);
			Grid.ColWidth(CNSTGRIDCOLCONTRACT, FCConvert.ToInt32(Grid.WidthOriginal * 0.25));
			Grid.ColHidden(CNSTGRIDCOLTAXTYPE, true);
			Grid.ColHidden(5, true);
			Grid.ColHidden(6, true);
			Grid.ColHidden(7, true);
			Grid.ColHidden(8, true);
			Grid.ColHidden(9, true);
			Grid.ColHidden(10, true);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 2, 0, Color.Blue);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, true);
			// disable grid columns
			if (Grid.Rows > 2)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, Grid.Rows - 1, 11, Color.White);
			}
			if (Grid.Rows > 3)
			{
				Grid.Row = 2;
				Grid.Col = CNSTGRIDCOLNUMWEEKS;
			}
			// Line1.Visible = True
			// Line1.BorderWidth = 2
			// Line1.X1 = Grid.Left - 100
			// Line1.X2 = Grid.Width '- 300
			// Line1.Y1 = (Grid.RowHeight(0) * 2) - 20
			// Line1.Y2 = (Grid.RowHeight(0) * 2) - 20
			// Line1.ZOrder 0
			//FC:FINAL:DDU:#i2076 - designer verify this part
			//GridTotals.TopOriginal = Grid.TopOriginal + Grid.HeightOriginal + 150;
			//GridTotals.WidthOriginal = Grid.ColWidth(CNSTGRIDCOLDEFAULTHOURS) + Grid.ColWidth(CNSTGRIDCOLHOURSWEEK) + Grid.ColWidth(CNSTGRIDCOLGROSS) + 600;
			GridTotals.ColWidth(0, GridTotals.WidthOriginal - (Grid.ColWidth(CNSTGRIDCOLHOURSWEEK) + Grid.ColWidth(CNSTGRIDCOLGROSS) + 50));
			GridTotals.ColWidth(1, Grid.ColWidth(CNSTGRIDCOLHOURSWEEK));
			GridTotals.ColWidth(2, Grid.ColWidth(CNSTGRIDCOLGROSS));
			//FC:FINAL:DDU:#i2074 - hide row instead of set it's height to 0
			GridTotals.RowHidden(2, true);
			//GridTotals.RowHeight(2, 0);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 2, 0, Color.Black);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, false);
			//if (boolScrollBars)
			//{
			//	GridTotals.LeftOriginal = (Grid.LeftOriginal + Grid.WidthOriginal) - GridTotals.WidthOriginal - 200;
			//}
			//else
			//{
			//	GridTotals.LeftOriginal = (Grid.LeftOriginal + Grid.WidthOriginal) - GridTotals.WidthOriginal;
			//}
		}

		public void SetDataEntryGrid()
		{
			modGlobalVariables.Statics.gboolDataEntry = true;
			string strCombo = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			string strHDC = "";
			string strFedComp = "";
			// *********************************************************************************************************
			// load the distribution combo with the choices in the msrs screen
			strCombo = "#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMNOMSRS) + ";N";
			strCombo += "|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMDEFAULT) + ";Y";
			GridDistM.Rows = 0;
			GridDistM.Rows += 1;
			GridDistM.TextMatrix(0, CNSTGRIDDISTMCOLID, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMNOMSRS));
			GridDistM.TextMatrix(0, CNSTGRIDDISTMCOLPOSITION, "N");
			GridDistM.Rows += 1;
			GridDistM.TextMatrix(1, CNSTGRIDDISTMCOLID, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMDEFAULT));
			GridDistM.TextMatrix(1, CNSTGRIDDISTMCOLPOSITION, "Y");
			clsLoad.OpenRecordset("select * FROM tblmsrsdistributiondetail where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by ID", "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				GridDistM.Rows += 1;
				lngRow = GridDistM.Rows - 1;
				if (Conversion.Val(clsLoad.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD)
				{
					modCustomReport.Statics.strReportType = "PLD";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD)
				{
					modCustomReport.Statics.strReportType = "School PLD";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER)
				{
					modCustomReport.Statics.strReportType = "Teacher";
				}
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLREPORTTYPE, modCustomReport.Statics.strReportType);
				if (Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract")) == modCoreysSweeterCode.CNSTMSRSCONTRACT)
				{
					strHDC = "Contract";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract")) == modCoreysSweeterCode.CNSTMSRSDAILY)
				{
					strHDC = "Daily";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract")) == modCoreysSweeterCode.CNSTMSRSHOURLY)
				{
					strHDC = "Hourly";
				}
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLHDC, strHDC);
				if (Conversion.Val(clsLoad.Get_Fields("fedcompamount")) > 0)
				{
					strFedComp = "\t" + "Fed Comp Amt " + clsLoad.Get_Fields("fedcompamount");
					GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLFEDCOMPAMOUNT, "Fed Comp Amt " + clsLoad.Get_Fields("fedcompamount"));
				}
				else
				{
					strFedComp = "";
				}
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLPOSITION, FCConvert.ToString(clsLoad.Get_Fields("position")));
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLSTATUS, FCConvert.ToString(clsLoad.Get_Fields("status")));
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLID, FCConvert.ToString(clsLoad.Get_Fields("ID")));
				strCombo += "|#" + clsLoad.Get_Fields("ID") + ";" + clsLoad.Get_Fields("position") + "\t" + clsLoad.Get_Fields("status") + "\t" + modCustomReport.Statics.strReportType + "\t" + strHDC + strFedComp;
				clsLoad.MoveNext();
			}
			Grid.ColComboList(CNSTGRIDCOLDISTM, strCombo);
			// *********************************************************************************************************
			//FC:FINAL:DDU:#i2076 - designer verify this part
			//if ((Grid.RowHeight(0) * Grid.Rows + 100) < (this.HeightOriginal * 0.5 - 550))
			//{
			//	// .Height = (.RowHeight(0) * .Rows) - 100
			//	boolScrollBars = false;
			//}
			//else
			//{
			//	boolScrollBars = true;
			//}
			//Grid.HeightOriginal = FCConvert.ToInt32((this.HeightOriginal * 0.5) - 550);
			Grid.ColWidth(0, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(1, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			//if (boolScrollBars)
			//{
			//	Grid.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.198));
			//}
			//else
			//{
			//	Grid.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.226));
			//}
			Grid.ColWidth(3, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			Grid.ColWidth(CNSTGRIDCOLTAXTYPE, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColWidth(5, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(7, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(8, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(9, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColWidth(10, FCConvert.ToInt32(Grid.WidthOriginal * 0.045));
			Grid.ColWidth(11, FCConvert.ToInt32(Grid.WidthOriginal * 0.15));
            Grid.ColWidth(12, FCConvert.ToInt32(Grid.WidthOriginal * 0.07));
            Grid.ColWidth(CNSTGRIDCOLNUMWEEKS, FCConvert.ToInt32(Grid.WidthOriginal * 0.1));
			Grid.ColWidth(CNSTGRIDCOLWEEKSWITHHELD, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColWidth(CNSTGRIDCOLDEFAULTHOURS, FCConvert.ToInt32(Grid.WidthOriginal * 0.07));
			Grid.ColWidth(CNSTGRIDCOLHOURSWEEK, FCConvert.ToInt32(Grid.WidthOriginal * 0.08));
			Grid.ColWidth(CNSTGRIDCOLGROSS, FCConvert.ToInt32(Grid.WidthOriginal * 0.12));
			Grid.ColWidth(cnstgridcolID, 0);
			Grid.ColWidth(CNSTGRIDCOLCHANGES, 0);
			Grid.ColWidth(CNSTGRIDCOLPERIODTAXES, 0);
			Grid.ColWidth(CNSTGRIDCOLPERIODDEDS, 0);
			Grid.ColWidth(CNSTGRIDCOLEXISTING, 0);
			Grid.ColWidth(CNSTGRIDCOLCONTRACT, FCConvert.ToInt32(Grid.WidthOriginal * 0.25));
			Grid.ColHidden(CNSTGRIDCOLPROJECT, true);
			Grid.ColHidden(CNSTGRIDCOLCONTRACTACCOUNTLIST, true);
			// no scrolling inside cells
			GridTotals.ColWidth(0, FCConvert.ToInt32(Grid.WidthOriginal * 0.165));
			GridTotals.ColWidth(1, FCConvert.ToInt32(Grid.WidthOriginal * 0.1));
			GridTotals.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.095));
			Grid.ColHidden(CNSTGRIDCOLTAXTYPE, true);
			Grid.ColHidden(5, true);
			Grid.ColHidden(6, true);
			Grid.ColHidden(7, true);
			Grid.ColHidden(8, true);
			Grid.ColHidden(9, true);
			Grid.ColHidden(10, true);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 2, 0, Color.Blue);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, true);
			// disable grid columns
			Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 0, Grid.Rows - 1, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, CNSTGRIDCOLCONTRACT, Grid.Rows - 1, CNSTGRIDCOLCONTRACT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			int intWeekChoice = 0;
			bool boolLineActive = false;
			for (lngRow = 2; lngRow <= Grid.Rows - 1; lngRow++)
			{
				// check each row
				boolLineActive = true;
				intWeekChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, 1))));
				if (!boolActive)
				{
					// gray out everything
					boolLineActive = false;
				}
				else
				{
					if (intWeekChoice != 1)
					{
						switch (modGlobalVariables.Statics.gintWeekNumber)
						{
							case 1:
								{
									if (intWeekChoice > 14 && intWeekChoice <= 17)
									{
										boolLineActive = false;
									}
									break;
								}
							case 2:
								{
									if (intWeekChoice == 14 || (intWeekChoice >= 16 && intWeekChoice <= 17))
									{
										boolLineActive = false;
									}
									break;
								}
							case 3:
								{
									if ((intWeekChoice >= 14 && intWeekChoice <= 15) || (intWeekChoice == 17))
									{
										boolLineActive = false;
									}
									else if (intWeekChoice == 13 && boolBiWeeklyTown)
									{
										boolLineActive = false;
									}
									break;
								}
							case 4:
								{
									if (intWeekChoice >= 14 && intWeekChoice <= 16)
									{
										boolLineActive = false;
									}
									break;
								}
							case 5:
								{
									if ((intWeekChoice >= 14 && intWeekChoice <= 17) || (!boolBiWeeklyTown && intWeekChoice == 13))
									{
										boolLineActive = false;
									}
									break;
								}
						}
						//end switch
					}
					else
					{
						boolLineActive = false;
					}
				}
				if (boolLineActive)
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLDEFAULTHOURS, Color.White);
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLHOURSWEEK, Color.White);
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLNUMWEEKS, Color.White);
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLWEEKSWITHHELD, Color.White);
				}
				else
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLDEFAULTHOURS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLHOURSWEEK, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLNUMWEEKS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDCOLWEEKSWITHHELD, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			// lngRow
			if (Grid.Rows > 3)
			{
				Grid.Row = 2;
				Grid.Col = CNSTGRIDCOLNUMWEEKS;
			}
			// Line1.Visible = True
			// Line1.BorderWidth = 2
			// Line1.X1 = Grid.Left - 100
			// Line1.X2 = Grid.Width '- 300
			// Line1.Y1 = (Grid.RowHeight(0) * 2) - 20
			// Line1.Y2 = (Grid.RowHeight(0) * 2) - 20
			// Line1.ZOrder 0
			//FC:FINAL:DDU:#i2076 - designer verify this part
			//GridTotals.TopOriginal = Grid.TopOriginal + Grid.HeightOriginal + 150;
			//GridTotals.WidthOriginal = Grid.ColWidth(CNSTGRIDCOLDEFAULTHOURS) + Grid.ColWidth(CNSTGRIDCOLHOURSWEEK) + Grid.ColWidth(CNSTGRIDCOLGROSS) + 600;
			GridTotals.ColWidth(0, GridTotals.WidthOriginal - (Grid.ColWidth(CNSTGRIDCOLHOURSWEEK) + Grid.ColWidth(CNSTGRIDCOLGROSS) + 50));
			GridTotals.ColWidth(1, Grid.ColWidth(CNSTGRIDCOLHOURSWEEK));
			GridTotals.ColWidth(2, Grid.ColWidth(CNSTGRIDCOLGROSS));
			//FC:FINAL:DDU:#i2074 - hide row instead of set it's height to 0
			GridTotals.RowHidden(2, true);
			//GridTotals.RowHeight(2, 0);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 2, 0, Color.Black);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, false);
			// GridTotals.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 2, 0) = 10
			//if (boolScrollBars)
			//{
			//	GridTotals.LeftOriginal = (Grid.LeftOriginal + Grid.WidthOriginal) - GridTotals.WidthOriginal - 200;
			//}
			//else
			//{
			//	GridTotals.LeftOriginal = (Grid.LeftOriginal + Grid.WidthOriginal) - GridTotals.WidthOriginal;
			//}
			clsGA.GRID7Light = null;
			// GridTotals.Height = 580
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				bool boolEUChanged;
				int intTemp;
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsRecNumber = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsPayCategory = new clsDRWrapper();
				int intCol;
				int lngID = 0;
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strTemp = "";
				// 
				// If clsSecurityClass.Check_Permissions(EMPLOYEEEDITSAVE) <> "F" Then
				// If Not boolFromDataEntry Or Not gboolDataEntry Then
				// boolCantEdit = True
				// mnuSave.Enabled = False
				// mnuSaveExit.Enabled = False
				// Grid.Editable = flexEDNone
				// txtDed.Enabled = False
				// txtTax.Enabled = False
				// chkDD(0).Enabled = False
				// chkDD(1).Enabled = False
				// chkDeductions(0).Enabled = False
				// chkDeductions(1).Enabled = False
				// chkMatches(0).Enabled = False
				// chkMatches(1).Enabled = False
				// chkMatches(2).Enabled = False
				// chkVacation.Enabled = False
				// End If
				// End If
				boolMsgAlreadyShown = false;
				boolMSGShown = false;
				rsMaster.OpenRecordset("SELECT * FROM tblEmployeeMaster where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				rsPayCategory.OpenRecordset("SELECT tblTaxStatusCodes.TaxStatusCode, tblPayCategories.ID as CategoryNumber FROM tblPayCategories INNER JOIN tblTaxStatusCodes ON tblPayCategories.TaxStatus = tblTaxStatusCodes.ID");
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsMaster.Get_Fields("status"))) == "ACTIVE")
				{
					boolActive = true;
				}
				else
				{
					boolActive = false;
				}
				boolEUChanged = false;
				rsDistribution.OpenRecordset("SELECT tblPayCategories.Multi, tblPayCategories.Type, tblPayrollDistribution.* FROM tblPayrollDistribution LEFT JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' ORDER BY RecordNumber", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					rsDistribution.MoveLast();
					rsDistribution.MoveFirst();
					rsDistribution2.OpenRecordset("SELECT tblPayCodes.Rate, tblPayrollDistribution.EmployeeNumber, tblPayCodes.PayCode, tblEmployeeMaster.WorkCompCode FROM (tblPayrollDistribution INNER JOIN tblPayCodes ON tblPayrollDistribution.CD = tblPayCodes.ID) INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE tblPayrollDistribution.EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by RecordNumber", "TWPY0000.vb1");
					if (!rsDistribution2.EndOfFile())
					{
						rsDistribution2.MoveLast();
						rsDistribution2.MoveFirst();
						Grid.Rows = 2;
						for (intCounter = 2; intCounter <= (rsDistribution.RecordCount() + 1); intCounter++)
						{
							Grid.AddItem("", Grid.Rows);
							if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Int32("RecordNumber") + " ")) != intCounter - 1)
							{
								// Call rsRecNumber.Execute("Update tblPayrollDistribution SET RecordNumber = " & intCounter - 1 & " where EmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' and id = " & rsDistribution.Fields("id"), "Twpy0000.vb1")
								intDataChanged += 1;
								// Change the row number in the first column to indicate that this record has been edited
								Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLCHANGES, Grid.TextMatrix(intCounter, CNSTGRIDCOLCHANGES) + "..");
							}
							Grid.TextMatrix(intCounter, CNSTGRIDCOLPROJECT, FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields("project"))));
							// Grid.TextMatrix(intCounter, 0) = Trim(rsDistribution.Fields("RecordNumber") & " ")
							Grid.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter - 1));
							Grid.TextMatrix(intCounter, 1, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("AccountCode") + " "))));
							Grid.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields("AccountNumber") + " "));
							Grid.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields("Cat") + " "));
							Grid.TextMatrix(intCounter, CNSTGRIDCOLCONTRACT, FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields("contractid"))));
							lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDistribution.Get_Fields("contractid"))));
							if (lngID > 0)
							{
								rsTemp.OpenRecordset("select * from contractaccounts where contractid = " + FCConvert.ToString(lngID) + " order by account", "twpy0000.vb1");
								if (rsTemp.EndOfFile())
								{
									Grid.TextMatrix(intCounter, CNSTGRIDCOLCONTRACTACCOUNTLIST, "");
								}
								else
								{
									strTemp = "";
									while (!rsTemp.EndOfFile())
									{
										strTemp += rsTemp.Get_Fields("account") + "|";
										rsTemp.MoveNext();
									}
									if (strTemp != string.Empty)
									{
										strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
									}
									Grid.TextMatrix(intCounter, CNSTGRIDCOLCONTRACTACCOUNTLIST, strTemp);
								}
							}
							rsPayCategory.FindFirstRecord("CategoryNumber", fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Cat"))));
							if (rsPayCategory.EndOfFile())
							{
								// Grid.TextMatrix(intCounter, 4) = Trim(rsDistribution.Fields("TaxCode") & " ")
								Grid.TextMatrix(intCounter, CNSTGRIDCOLTAXTYPE, string.Empty);
							}
							else
							{
								Grid.TextMatrix(intCounter, CNSTGRIDCOLTAXTYPE, fecherFoundation.Strings.Trim(rsPayCategory.Get_Fields("TaxStatusCode") + " "));
							}
							Grid.TextMatrix(intCounter, CNSTGRIDCOLDISTM, FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields_Int32("MSRSID"))));
							
							// MATTHEW 10/6/2004 CALL ID 54367
							if (boolUnempExempt)
							{
								if (Strings.Left(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("DistU") + " "), 1) == "Y")
								{
									if (boolMsgAlreadyShown == false)
									{
										// MsgBox "Unemployment Exempt is selected on the employee's master screen. Distribution records must have the Include for Unemployment set to an 'N'", vbInformation + vbOKOnly, "TRIO Software"
										MessageBox.Show("Unemployment Exempt is selected on the employee's MainePERS/Unemployment screen or Employee Master screen. Distribution records must have the Include for Unemployment set to an 'N'", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
										boolMsgAlreadyShown = true;
									}
									if (Strings.Left(Grid.TextMatrix(intCounter, CNSTGRIDCOLCHANGES) + " ", 1) != ".")
									{
										Grid.TextMatrix(intCounter, CNSTGRIDCOLCHANGES, Grid.TextMatrix(intCounter, CNSTGRIDCOLCHANGES) + "..");
									}
									// Grid.TextMatrix(intCounter, CNSTGRIDCOLCHANGES) = Grid.TextMatrix(intCounter, CNSTGRIDCOLCHANGES) & ".."
									Grid.TextMatrix(intCounter, 7, "N");
									boolEUChanged = true;
								}
								else
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 7, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
									if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("DistU") + " ")) == 0)
									{
										Grid.TextMatrix(intCounter, 7, "N");
									}
									else
									{
										Grid.TextMatrix(intCounter, 7, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("DistU") + " "));
									}
								}
							}
							else
							{
								// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 7) = vbWhite
								Grid.TextMatrix(intCounter, 7, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("DistU") + " "));
							}
							Grid.TextMatrix(intCounter, 8, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("WC") + " ")));
							Grid.TextMatrix(intCounter, 9, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("WorkComp") + " ")));
							if (!rsDistribution2.EndOfFile())
							{
								if (FCConvert.ToString(rsDistribution2.Get_Fields_String("PayCode")) == "0")
								{
									Grid.TextMatrix(intCounter, 11, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Decimal("BaseRate") + " "));
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 11, Color.White);
								}
								else if (FCConvert.ToString(rsDistribution2.Get_Fields_String("PayCode")) == "998" || FCConvert.ToString(rsDistribution2.Get_Fields_String("PayCode")) == "999")
								{
									Grid.TextMatrix(intCounter, 11, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Decimal("BaseRate") + " "));
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
								else
								{
									Grid.TextMatrix(intCounter, 11, fecherFoundation.Strings.Trim(rsDistribution2.Get_Fields("Rate") + " "));
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
							}
							else
							{
								Grid.TextMatrix(intCounter, 11, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Decimal("BaseRate") + " "));
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 11, Color.White);
							}
							Grid.TextMatrix(intCounter, 10, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields("CD") + " "));
							Grid.TextMatrix(intCounter, 12, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("Multi") + " "));
							Grid.TextMatrix(intCounter, CNSTGRIDCOLNUMWEEKS, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Int32("NumberWeeks") + " "));
							Grid.TextMatrix(intCounter, CNSTGRIDCOLWEEKSWITHHELD, FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields_Int16("WeeksTaxWithheld"))));
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type"))) == "Dollars")
							{
								if (!boolCantEdit)
								{
									Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								}
								else
								{
									Grid.Editable = FCGrid.EditableSettings.flexEDNone;
								}
								//Grid.Editable = !boolCantEdit;
								// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 14) = TRIOCOLORGRAYBACKGROUND
								// changed on 1/28/04 for Caribou as they want a different amount for their default
								// and this week even if the amount type is dollars
								// Grid.TextMatrix(intCounter, 14) = 1
								Grid.TextMatrix(intCounter, CNSTGRIDCOLDEFAULTHOURS, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("DefaultHours") + " "));
								// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 15) = TRIOCOLORGRAYBACKGROUND
								// changed on 1/28/04 for Caribou as they want a different amount for their default
								// and this week even if the amount type is dollars
								// Grid.TextMatrix(intCounter, 15) = 1
								Grid.TextMatrix(intCounter, CNSTGRIDCOLHOURSWEEK, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + " "));
							}
							else
							{
								Grid.TextMatrix(intCounter, CNSTGRIDCOLDEFAULTHOURS, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("DefaultHours") + " "));
								Grid.TextMatrix(intCounter, CNSTGRIDCOLHOURSWEEK, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + " "));
							}
							// ********************************************************************************************************
							if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "DOLLARS")
							{
								if (!boolCantEdit)
								{
									Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								}
								else
								{
									Grid.Editable = FCGrid.EditableSettings.flexEDNone;
								}
								//Grid.Editable = !boolCantEdit;
								// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 14) = TRIOCOLORGRAYBACKGROUND
								// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 15) = TRIOCOLORGRAYBACKGROUND
								// changed on 1/28/04 for Caribou as they want a different amount for their default
								// and this week even if the amount type is dollars
								// Grid.TextMatrix(intCounter, 14) = "1"
								// Grid.TextMatrix(intCounter, 15) = "1"
								Grid.TextMatrix(intCounter, CNSTGRIDCOLDEFAULTHOURS, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("DefaultHours") + " "));
								Grid.TextMatrix(intCounter, CNSTGRIDCOLHOURSWEEK, fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + " "));
							}
							else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "Non-Pay (Hours)")
							{
								for (intCol = 5; intCol <= CNSTGRIDCOLDEFAULTHOURS; intCol++)
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
							}
							else
							{
								if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))), 1)) == "P")
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEFAULTHOURS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLHOURSWEEK, Color.White);
								}
								else
								{
									if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))), 1)) != "N")
									{
										Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEFAULTHOURS, Color.White);
									}
									else
									{
										Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEFAULTHOURS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
									}
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLHOURSWEEK, Color.White);
								}
							}
							// ********************************************************************************************************
							Grid.TextMatrix(intCounter, CNSTGRIDCOLGROSS, FCConvert.ToString((Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Decimal("BaseRate") + " ")) * Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("Factor") + " "))) * Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + " "))));
							Grid.TextMatrix(intCounter, cnstgridcolID, FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields("ID") + " ")));
							if (!rsMaster.EndOfFile())
							{
								Grid.TextMatrix(intCounter, CNSTGRIDCOLPERIODTAXES, fecherFoundation.Strings.Trim(rsMaster.Get_Fields_Double("PeriodTaxes") + " "));
								Grid.TextMatrix(intCounter, CNSTGRIDCOLPERIODDEDS, fecherFoundation.Strings.Trim(rsMaster.Get_Fields_Double("PeriodDeds") + " "));
								txtTax.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("PeriodTaxes") + " ");
								txtDed.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("PeriodDeds") + " ");
								// matthew 10/23/2005
								chkDeductions[0].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("DeductionPercent")) ? CheckState.Checked : CheckState.Unchecked);
								chkDeductions[1].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("DeductionDollars")) ? CheckState.Checked : CheckState.Unchecked);
								chkMatches[0].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("MatchPercentGross")) ? CheckState.Checked : CheckState.Unchecked);
								chkMatches[1].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("MatchPercentDeduction")) ? CheckState.Checked : CheckState.Unchecked);
								chkMatches[2].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("MatchDollars")) ? CheckState.Checked : CheckState.Unchecked);
								chkVacation.CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("S1CheckAsP1Vacation")) ? CheckState.Checked : CheckState.Unchecked);
								// CALL ID 82295 MATTHEW 11/22/2005
								// chkDD = IIf(rsMaster.Fields("S1CheckAsP1DirectDeposits"), 1, 0)
								chkDD[0].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("DDPercent")) ? CheckState.Checked : CheckState.Unchecked);
								chkDD[1].CheckState = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("DDDollars")) ? CheckState.Checked : CheckState.Unchecked);
							}
							rsDistribution2.MoveNext();
							rsDistribution.MoveNext();
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 12, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLGROSS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							if (FCConvert.ToDouble(Grid.TextMatrix(intCounter, 1)) == 1)
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, intCounter, Grid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
						}
						intInitialDedUpTop = FCConvert.ToInt32(txtDed.Text);
					}
				}
				else
				{
					//App.DoEvents();
					Grid.Rows = 2;
					GridTotals.TextMatrix(0, 1, "");
					GridTotals.TextMatrix(0, 2, "");
					GridTotals.TextMatrix(1, 2, "");
					GridTotals.TextMatrix(2, 2, "");
					txtDed.Text = FCConvert.ToString(1);
					txtTax.Text = FCConvert.ToString(1);
				}
                //FC:FINAL:AM:#2555 - set the correct row
                if (Grid.Rows > 2)
                {
                    Grid.Row = 2;
                }
                //FC:FINAL:AM:#4088 - set the Row/Col as in VB6
                else
                {
                    Grid.Row = 1;                    
                }
                Grid.Col = 13;
				Grid_RowColChange(null, null);
				// data is load so now find out what the totals are
				CalculateGridTotals(Grid.Row, Grid.Row);
				// data is loaded so now find out what totals are for tax and ded
				//Grid_AfterEdit(Grid.Row, CNSTGRIDCOLNUMWEEKS);
				Grid_AfterEdit(new object(), new DataGridViewCellEventArgs(Grid.Row, CNSTGRIDCOLNUMWEEKS));
				// if we didn't use all the grid space then adjust to make look nice
				if (!boolEUChanged)
				{
					intDataChanged = 0;
				}
				if (modGlobalVariables.Statics.gboolDataEntry)
				{
					SetDataEntryGrid();
				}
				else
				{
					SetDistributionGrid();
				}
				SetupContractCol();
				if (Grid.Rows > 3)
					Grid.Select(2, 1);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "GRID_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCol;
				if (Strings.Right(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, CNSTGRIDCOLCHANGES)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCHANGES, Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCHANGES) + "..");
				}
				// if there was a change then we want to make sure we update the totals
				CalculateGridTotals(Grid.Row, Grid.Col);
				if (Grid.Col == 1)
				{
					if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 1))), 1)) == "P")
					{
						if (!boolCantEdit)
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						//Grid.Editable = !boolCantEdit;
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLDEFAULTHOURS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						Grid.TextMatrix(Grid.Row, CNSTGRIDCOLDEFAULTHOURS, "1");
						CalculateGridTotals(Grid.Row, Grid.Col);
					}
					else
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLDEFAULTHOURS, Color.White);
					}
				}
				if (Grid.Col == 1 || Grid.Col == 3 || NewFlag == true)
				{
					rsDistribution.OpenRecordset("SELECT * FROM tblPayCategories where ID = " + FCConvert.ToString(Conversion.Val(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, Grid.Row, 3))), "TWPY0000.vb1");
					if (!rsDistribution.EndOfFile())
					{
						if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) != "Non-Pay (Hours)")
						{
							for (intCol = 5; intCol <= CNSTGRIDCOLDEFAULTHOURS; intCol++)
							{
								if (intCol != 12 && intCol != 7)
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, intCol, Color.White);
								}
							}
							if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpValue, Grid.Row, CNSTGRIDCOLHOURSWEEK)) < 0)
							{
								MessageBox.Show("Distribution record has a negative hours and the type is NOT Non-Pay.");
								return;
							}
						}
						else
						{
							Grid.TextMatrix(Grid.Row, 11, FCConvert.ToString(0));
							Grid.TextMatrix(Grid.Row, CNSTGRIDCOLDEFAULTHOURS, FCConvert.ToString(0));
						}
						rsDistribution.MoveLast();
						rsDistribution.MoveFirst();
						if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "DOLLARS")
						{
							if (!boolCantEdit)
							{
								Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								Grid.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							//Grid.Editable = !boolCantEdit;
							if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 1))), 1)) != "N")
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLHOURSWEEK, Color.White);
							}
						}
						else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "Non-Pay (Hours)")
						{
							for (intCol = 5; intCol <= CNSTGRIDCOLDEFAULTHOURS; intCol++)
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, intCol, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
							if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 1))), 1)) != "N")
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLHOURSWEEK, Color.White);
							}
						}
						else
						{
							if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 1))), 1)) == "P")
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLDEFAULTHOURS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLHOURSWEEK, Color.White);
							}
							else
							{
								if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 1))), 1)) != "N")
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLDEFAULTHOURS, Color.White);
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLHOURSWEEK, Color.White);
								}
								else
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLDEFAULTHOURS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, CNSTGRIDCOLHOURSWEEK, Color.White);
							}
						}
						// get the default values for this pay category and fill the grid
						Grid.TextMatrix(Grid.Row, 12, FCConvert.ToString(rsDistribution.Get_Fields_Double("Multi")));
						Grid.Cell(FCGrid.CellPropertySettings.flexcpText, Grid.Row, CNSTGRIDCOLTAXTYPE, rsDistribution.Get_Fields_Int32("TaxStatus"));
						// matthew 5/05/2005
						// ***********************************************************************************************
						// comments above showed that I commented out this line on 5/5 but I cannot find any call
						// that would back this up. I'll comment it back in via call id 73087 until something else
						// shows that we shouldn't be doing this. Matthew 7/19/2005
						// COREY remembered the reason. This code always changed dist m when you changed any column in this
						// grid. If the default is N and you have manually changed it to 09901, then changed the freq to W
						// and tabbed off that column it would fill distm with the default again. The only time we want this
						// to change is on a new record and then the pay category changes. Matthew 7/19/2005
						if (Grid.Col == 3)
						{
							Grid.TextMatrix(Grid.Row, 5, FCConvert.ToString((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields_Boolean("MSR")))) == "TRUE" ? modCoreysSweeterCode.CNSTDISTMDEFAULT : (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields_Boolean("MSR")))) == "FALSE" ? modCoreysSweeterCode.CNSTDISTMNOMSRS : FCConvert.ToInt32(rsDistribution.Get_Fields_Boolean("MSR"))))));
						}

						if (boolUnempExempt)
						{
						}
						else
						{
							Grid.TextMatrix(Grid.Row, 7, (FCConvert.ToBoolean(rsDistribution.Get_Fields("Unemployment")) ? "Y" : "N"));
						}
						Grid.TextMatrix(Grid.Row, 9, (FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("WorkersComp")) ? "Y" : "N"));
						rsDistribution.MoveNext();
					}
				}
				NewFlag = false;
				if (Grid.Col == 10)
				{
					if (Grid.TextMatrix(Grid.Row, 10) != string.Empty)
					{
						// make sure we have the right rate displayed based on CD
						if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 10)) != 998 && FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 10)) != 999)
						{
							rsDistribution.OpenRecordset("Select * From tblPayCodes where ID =" + Grid.TextMatrix(Grid.Row, 10), "TWPY0000.vb1");
							if (!rsDistribution.EndOfFile())
							{
								rsDistribution.MoveLast();
								rsDistribution.MoveFirst();
								Grid.TextMatrix(Grid.Row, 11, FCConvert.ToString(rsDistribution.Get_Fields("Rate")));
							}
						}
						if (Strings.Left(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col)), 1) == "0")
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, 11, Color.White);
						}
						else
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
					}
				}
				// If Col = 5 Then GRID.TextMatrix(Row, Col) = UCase(Left(Trim(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col)), 1)) & Mid(Trim(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col)), 2, Len(Trim(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col))) - 1)
				if (Grid.Col == 7)
					Grid.TextMatrix(Grid.Row, Grid.Col, fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col))), 1)) + Strings.Mid(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col))), 2, fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col))).Length - 1));
				if (Grid.Col == 9)
				{
					Grid.TextMatrix(Grid.Row, Grid.Col, fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col))), 1)) + Strings.Mid(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col))), 2, fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col))).Length - 1));
					Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Grid.Row, Grid.Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				if (Grid.Col == 1)
				{
					if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 1))), 1)) == "N")
					{
						for (intCol = 2; intCol <= CNSTGRIDCOLHOURSWEEK; intCol++)
						{
							// If intCol <> 12 Then
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, intCol, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							// End If
						}
						// intCol
					}
					else
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, 2, Grid.Row, 3, Color.White);
						if (!boolUnempExempt)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, 7, Color.White);
						}
					}
				}
				intDataChanged += 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: Row As int	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: Col As int	OnWriteFCConvert.ToInt32(
		private void CalculateGridTotals(int Row, int Col)
		{
			// just find totals
			clsDRWrapper rsDistribution = new clsDRWrapper();
			// Call rsDistribution.OpenRecordset("SELECT * FROM tblPayCategories where ID = " & Val(Grid.TextMatrix(Row, 3)), "TWPY0000.vb1")
			rsDistribution.OpenRecordset("SELECT * FROM tblPayCategories", "TWPY0000.vb1");
			// Call rsDistribution.FindFirstRecord("ID", Val(Grid.TextMatrix(Row, 3)))
			rsDistribution.FindFirst("ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Row, 3))));
			if (Row > 1)
			{
				if (rsDistribution.NoMatch)
				{
					Grid.TextMatrix(Row, CNSTGRIDCOLGROSS, FCConvert.ToString(0));
				}
				else
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type"))) == "Non-Pay (Hours)")
					{
						Grid.TextMatrix(Row, CNSTGRIDCOLGROSS, FCConvert.ToString(0));
					}
					else
					{
						Grid.TextMatrix(Row, CNSTGRIDCOLGROSS, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Row, 11)) * Conversion.Val(Grid.TextMatrix(Row, 12)) * Conversion.Val(Grid.TextMatrix(Row, CNSTGRIDCOLHOURSWEEK))));
					}
				}
			}
			GridTotals.TextMatrix(0, 1, "0");
			GridTotals.TextMatrix(0, 2, "0");
			GridTotals.TextMatrix(1, 1, "0");
			GridTotals.TextMatrix(1, 2, "0");
			for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
			{
				// Call rsDistribution.FindFirstRecord("ID", Val(Grid.TextMatrix(intCounter, 3)))
				rsDistribution.FindFirst("ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, 3))));
				if (rsDistribution.NoMatch)
				{
				}
				else
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type"))) == "Hours")
					{
						GridTotals.TextMatrix(0, 1, FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(0, 1)) + Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLHOURSWEEK))));
					}
					else if (Strings.Right(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type"))), 7) == "(Hours)")
					{
						// THEN THIS IS A NON PAY
						GridTotals.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(1, 1)) + Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLHOURSWEEK))));
					}
					GridTotals.TextMatrix(0, 2, FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(0, 2)) + FCConvert.ToDouble(Strings.Format(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLGROSS)), "0.00"))));
					GridTotals.TextMatrix(1, 2, string.Empty);
				}
			}
			TotalFlag = false;
		}

		private void Grid_AfterMoveRow(object sender, AfterMoveRowEventArgs e)
		{
			// must renumber rows
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 2; x <= (Grid.Rows - 1); x++)
			{
				Grid.TextMatrix(x, CNSTGRIDCOLNUMBER, FCConvert.ToString(x - 1));
			}
			// x
			intDataChanged = (DialogResult)1;
		}

		private void Grid_ChangeEdit(object sender, System.EventArgs e)
		{
			int lngRow;
			string strTemp = "";
			if (Grid.Col == CNSTGRIDCOLACCOUNT)
			{
				lblAccountDesc.Text = "Account:  " + modAccountTitle.ReturnAccountDescription(Grid.EditText);
			}
		}

		private void Grid_ComboCloseUp(object sender, EventArgs e)
		{
			string strTemp = "";
			int lngID = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (Grid.Row > 1)
			{
				if (Grid.Col == CNSTGRIDCOLCONTRACT)
				{
					// lngID = Val(Grid.TextMatrix(Row, CNSTGRIDCOLCONTRACT))
					lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.ComboData(Grid.ComboIndex))));
					rsTemp.OpenRecordset("select * from contractaccounts where contractid = " + FCConvert.ToString(lngID) + " order by account", "twpy0000.vb1");
					if (rsTemp.EndOfFile())
					{
						Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCONTRACTACCOUNTLIST, "");
					}
					else
					{
						strTemp = "";
						while (!rsTemp.EndOfFile())
						{
							strTemp += rsTemp.Get_Fields("account") + "|";
							rsTemp.MoveNext();
						}
						if (strTemp != string.Empty)
						{
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						}
						Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCONTRACTACCOUNTLIST, strTemp);
					}
				}
			}
		}

		private void Grid_Enter(object sender, System.EventArgs e)
		{
			if (Grid.Col == 2)
			{
				if (Grid.Row == lngAccountRow)
				{
					Grid.Col = 3;
				}
			}
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// make tab key work like right arrow and left arrow
			if (KeyCode == (Keys)9)
				KeyCode = (Keys)39;
			// If KeyCode = 13 And Grid.Col = 2 Then
			// Grid.EditText = True
			// Exit Sub
			// End If
			if (KeyCode == (Keys)13)
				KeyCode = (Keys)39;
			if (KeyCode == Keys.Home)
				txtTax.Focus();
			modGridKeyMove.MoveGridProsition(ref Grid, 1, CNSTGRIDCOLHOURSWEEK, KeyCode);
			if (KeyCode == Keys.Home)
				txtTax.Focus();
		}

		private void Grid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			modGridKeyMove.MoveGridProsition(ref Grid, 1, CNSTGRIDCOLHOURSWEEK, KeyCode);
		}

		private void Grid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			// If Col = 2 Then Call CheckAccountKeyPress(Grid, Row, Col, KeyAscii)
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 8)
				return;
			switch (Grid.Col)
			{
				case 10:
				case 11:
				case 12:
				case CNSTGRIDCOLNUMWEEKS:
				case CNSTGRIDCOLWEEKSWITHHELD:
				case CNSTGRIDCOLDEFAULTHOURS:
				case CNSTGRIDCOLHOURSWEEK:
				case CNSTGRIDCOLGROSS:
					{
						if (Grid.Col == CNSTGRIDCOLHOURSWEEK)
						{
							// allow negatives for non pay pay cat types
							if (KeyAscii == 45)
							{
								rsPayCatType.OpenRecordset("SELECT * FROM tblPayCategories where ID = " + FCConvert.ToString(Conversion.Val(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, Grid.Row, 3))), "TWPY0000.vb1");
								if (!rsPayCatType.EndOfFile())
								{
									if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsPayCatType.Get_Fields("Type")))) != "Non-Pay (Hours)")
									{
										KeyAscii = 0;
										e.KeyChar = Strings.Chr(KeyAscii);
									}
									return;
								}
							}
						}
						if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 46)
						{
							KeyAscii = 0;
						}
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		// Private Sub GRID_KeyUpEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
		// If Col = 2 Then
		// Call CheckAccountKeyCode(Grid, Row, Col, KeyCode, 0)
		// KeyCode = 0
		// End If
		// End Sub
		private void Grid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			/*? On Error Resume Next  */// MsgBox lngCCol
			int lngCRow;
			int lngCCol;
			string strTemp = "";
			int lngRow = 0;
			string[] strAry = null;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp;
            lngCRow = Grid.GetFlexRowIndex(e.RowIndex);
            lngCCol = Grid.GetFlexColIndex(e.ColumnIndex);
            if (Grid.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = Grid[e.ColumnIndex, e.RowIndex];
                //if (lngCRow == 1 && lngCCol == CNSTGRIDCOLTAXTYPE)
                //{
                //    cell.ToolTipText = "Tax Status";
                //}
                //else if (lngCRow == 1 && lngCCol == 5)
                //{
                //    cell.ToolTipText = "MainePERS";
                //}
                //else if (lngCRow == 1 && lngCCol == 6)
                //{
                //    cell.ToolTipText = "Earned Income Credit";
                //}
                //else if (lngCRow == 1 && lngCCol == 7)
                //{
                //    cell.ToolTipText = "Unemployment";
                //}
                //else if (lngCRow == 1 && lngCCol == 9)
                //{
                //    cell.ToolTipText = "Workers Comp";
                //}
                //else if (lngCRow == 1 && lngCCol == 12)
                //{
                //    cell.ToolTipText = "Determined by Cat";
                //}
                if (lngCCol == CNSTGRIDCOLDISTM)
                {
                    // If InStr(GRID.TextMatrix(lngCRow, lngCCol), "-") Then
                    // GRID.ToolTipText = "Format: Y/N/#### - ## - # (Position Code - Status Code - Grant Funded) / (Dist M: " & GRID.TextMatrix(lngCRow, lngCCol) & ")"
                    // Else
                    // GRID.ToolTipText = "Y/N/#### - ## - # (Position Code - Status Code - Grant Funded)"
                    // End If
                    if (lngCCol == CNSTGRIDCOLDISTM)
                    {
                        if (lngCRow > 0)
                        {
                            lngRow = GridDistM.FindRow(FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngCRow, CNSTGRIDCOLDISTM)))), 0, CNSTGRIDDISTMCOLID);
                            if (lngRow >= 0)
                            {
                                strTemp = GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLPOSITION);
                                if (Conversion.Val(Grid.TextMatrix(lngCRow, CNSTGRIDCOLDISTM)) != modCoreysSweeterCode.CNSTDISTMDEFAULT && Conversion.Val(Grid.TextMatrix(lngCRow, CNSTGRIDCOLDISTM)) != modCoreysSweeterCode.CNSTDISTMNOMSRS)
                                {
                                    strTemp += "  " + GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLSTATUS);
                                    strTemp += "  " + GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLREPORTTYPE);
                                    strTemp += "  " + GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLHDC);
                                    strTemp += "  " + GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLFEDCOMPAMOUNT);
                                }
                            }
                            else
                            {
                                strTemp = "";
                            }
                        }
                    }
                    cell.ToolTipText = strTemp;
                }
                else if (lngCCol == 7)
                {
                    cell.ToolTipText = "Y/N/S/####";
                }
                else if (lngCCol == 9)
                {
                    cell.ToolTipText = "Y/N/#### (Workers Comp Code)";
                }
                else if (lngCCol == 10)
                {
                    cell.ToolTipText = "Pay Code";
                }
                else if (lngCCol == 3)
                {
                    strAry = Strings.Split(FCConvert.ToString(Grid.ColData(3)), ";", -1, CompareConstants.vbTextCompare);
                    if (Information.UBound(strAry, 1) >= 0)
                    {
                        for (intTemp = 0; intTemp <= (Information.UBound(strAry, 1)); intTemp++)
                        {
                            if (Conversion.Val(strAry[intTemp]) == Conversion.Val(Grid.TextMatrix(lngCRow, 3)))
                            {
                                strTemp = strAry[intTemp];
                                strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
                                if (Information.UBound(strAry, 1) > 0)
                                {
                                    cell.ToolTipText = strAry[1];
                                }
                                break;
                            }
                        }
                        // intTemp
                    }
                }
            }
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			intRowChange = Grid.Row;
			if (Grid.Col == CNSTGRIDCOLACCOUNT && Grid.Row > 1)
			{
				Grid.ColComboList(CNSTGRIDCOLACCOUNT, Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCONTRACTACCOUNTLIST));
			}
			if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, Grid.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND && !boolCantEdit)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				if (modValidateAccount.AccountValidate(Grid.TextMatrix(Grid.Row, 2)))
				{
					lblAccountDesc.Text = "Account:  " + modDavesSweetCode.GetAccountDescription(Grid.TextMatrix(Grid.Row, 2));
				}
				else
				{
					lblAccountDesc.Text = "";
				}
			}
			else
			{
				lblAccountDesc.Text = "";
			}
			// *********************************************************************************************
			// DONE FOR CALL ID 64441
			// MATTHEW 3/1/2005
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
			{
				if (intCounter == Grid.Row)
				{
					for (intCol = 1; intCol <= (Grid.Cols - 1); intCol++)
					{
						if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
						}
						else
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol, 0xFFFFC0);
						}
					}
				}
				else
				{
					for (intCol = 1; intCol <= (Grid.Cols - 1); intCol++)
					{
						if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
						}
						else
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol, Color.White);
						}
					}
				}
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLTAXTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 12, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			// *********************************************************************************************
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "GRID_ValidateEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				NotValidData = false;
				if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, Grid.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					e.Cancel = true;
					return;
				}
				// If Col = 2 Then
				// Cancel = CheckAccountValidate(Grid, Row, Col, Cancel)
				// Exit Sub
				// End If
				// make sure that there is something selected in combo box
				switch (Grid.Col)
				{
				// make sure there is something in every box
					case 2:
					case 3:
					case CNSTGRIDCOLTAXTYPE:
						{
							if (Grid.TextMatrix(Grid.Row, Grid.Col) == string.Empty && Grid.EditText == string.Empty)
							{
								intDataChanged = (DialogResult)1;
								MessageBox.Show("Please Enter Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								e.Cancel = true;
								NotValidData = true;
							}
							break;
						}
					case 9:
						{
							if (Grid.EditText.Length > 5)
							{
								MessageBox.Show("Code Cannot Be More Than 5 Digits", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								e.Cancel = true;
								NotValidData = true;
							}
							break;
						}
					case CNSTGRIDCOLHOURSWEEK:
						{
							if (Strings.Right(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, 3)), 9) == "(Dollars)" && Conversion.Val(Grid.EditText) > 1)
							{
								MessageBox.Show("Enter 0 or 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								e.Cancel = true;
								NotValidData = true;
							}
							break;
						}
				}
				//end switch
				if (Grid.Col == 5)
				{
					if (fecherFoundation.Strings.Trim(Grid.EditText).Length == 1)
					{
						if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.EditText)) == "Y")
						{
						}
						else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.EditText)) == "N")
						{
						}
						else
						{
							MessageBox.Show("MainePERS must be in for the format of a Y or N if a single character.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							NotValidData = true;
						}
					}
				}
				if (Grid.Col == 7)
				{
					if (Strings.Left(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.EditText)), 1) == "Y")
					{
					}
					else if (Strings.Left(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.EditText)), 1) == "N")
					{
						if (fecherFoundation.Strings.Trim(Grid.EditText).Length > 1)
						{
							MessageBox.Show("Unemployment flag of N cannot have a code.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							NotValidData = true;
						}
					}
					else if (Strings.Left(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.EditText)), 1) == "S")
					{
					}
					else
					{
						// MATTHEW 6/3/2004
						if (Information.IsNumeric(Strings.Left(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.EditText)), 1)))
						{
						}
						else
						{
							MessageBox.Show("Unemployment flag must be in for the format of a Y, N or S if a single character.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							NotValidData = true;
						}
					}
					Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Grid.Row, Grid.Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				if (Grid.Col == 9)
				{
					if (fecherFoundation.Strings.Trim(Grid.EditText).Length > 0)
					{
						// Grid.TextMatrix(Row, Col) = UCase(Left(Trim(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col)), 1)) & Mid(Trim(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col)), 2, Len(Trim(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col))) - 1)
						// Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, Col) = flexAlignLeftCenter
						if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(Grid.EditText), 1)) == "Y")
						{
							if (fecherFoundation.Strings.Trim(Grid.EditText).Length > 1)
							{
								if (Information.IsNumeric(Strings.Mid(fecherFoundation.Strings.Trim(Grid.EditText), 2, fecherFoundation.Strings.Trim(Grid.EditText).Length - 1)))
								{
								}
								else
								{
									MessageBox.Show("W/C must be in for the format Y/N ####", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
									e.Cancel = true;
									NotValidData = true;
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(Grid.EditText), 1)) == "N")
						{
							if (fecherFoundation.Strings.Trim(Grid.EditText).Length > 1)
							{
								if (Information.IsNumeric(Strings.Mid(fecherFoundation.Strings.Trim(Grid.EditText), 2, fecherFoundation.Strings.Trim(Grid.EditText).Length - 1)))
								{
								}
								else
								{
									MessageBox.Show("W/C must be in for the format Y/N ####", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
									e.Cancel = true;
									NotValidData = true;
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(Grid.EditText), 1)) == "S")
						{
						}
						else
						{
							if (Information.IsNumeric(fecherFoundation.Strings.Trim(Grid.EditText)))
							{
							}
							else
							{
								MessageBox.Show("W/C must be in for the format Y/N ####", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								e.Cancel = true;
								NotValidData = true;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void GridTotals_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridTotals[e.ColumnIndex, e.RowIndex];
            if (GridTotals.GetFlexColIndex(e.ColumnIndex) == 1)
			{
				//ToolTip1.SetToolTip(GridTotals, "This Week Totals");
				cell.ToolTipText = "This Week Totals";
			}
			else
			{
				//ToolTip1.SetToolTip(GridTotals, "Gross Totals");
				cell.ToolTipText = "Gross Totals";
			}
		}

		private void mnuDistributionScreen_Click(object sender, System.EventArgs e)
		{
			// Call SetDistributionGrid
			intDataEntry = 0;
			clsGA.GRID7Light = Grid;
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				boolCantEdit = true;
				cmdSave.Enabled = false;
				cmdSaveExit.Enabled = false;
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				txtDed.Enabled = false;
				txtTax.Enabled = false;
				chkDD[0].Enabled = false;
				chkDD[1].Enabled = false;
				chkDeductions[0].Enabled = false;
				chkDeductions[1].Enabled = false;
				chkMatches[0].Enabled = false;
				chkMatches[1].Enabled = false;
				chkMatches[2].Enabled = false;
				chkVacation.Enabled = false;
			}
			Form_Resize();
		}

		public void SetDistributionGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			string strCombo = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strReportType = "";
			string strHDC = "";
			string strFedComp = "";
			int lngRow;
			modGlobalVariables.Statics.gboolDataEntry = false;
			// load the distribution combo with the choices in the msrs screen
			strCombo = "#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMNOMSRS) + ";N";
			strCombo += "|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMDEFAULT) + ";Y";
			GridDistM.Rows = 0;
			GridDistM.Rows += 1;
			GridDistM.TextMatrix(0, CNSTGRIDDISTMCOLID, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMNOMSRS));
			GridDistM.TextMatrix(0, CNSTGRIDDISTMCOLPOSITION, "N");
			GridDistM.Rows += 1;
			GridDistM.TextMatrix(1, CNSTGRIDDISTMCOLID, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMDEFAULT));
			GridDistM.TextMatrix(1, CNSTGRIDDISTMCOLPOSITION, "Y");
			Grid.ColHidden(CNSTGRIDCOLCONTRACTACCOUNTLIST, true);
			clsLoad.OpenRecordset("select * FROM tblmsrsdistributiondetail where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by ID", "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				GridDistM.Rows += 1;
				lngRow = GridDistM.Rows - 1;
				if (Conversion.Val(clsLoad.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD)
				{
					strReportType = "PLD";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD)
				{
					strReportType = "School PLD";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER)
				{
					strReportType = "Teacher";
				}
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLREPORTTYPE, strReportType);
				if (Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract")) == modCoreysSweeterCode.CNSTMSRSCONTRACT)
				{
					strHDC = "Contract";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract")) == modCoreysSweeterCode.CNSTMSRSDAILY)
				{
					strHDC = "Daily";
				}
				else if (Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract")) == modCoreysSweeterCode.CNSTMSRSHOURLY)
				{
					strHDC = "Hourly";
				}
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLHDC, strHDC);
				if (Conversion.Val(clsLoad.Get_Fields("fedcompamount")) > 0)
				{
					strFedComp = "\t" + "Fed Comp Amt " + clsLoad.Get_Fields("fedcompamount");
					GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLFEDCOMPAMOUNT, "Fed Comp Amt " + clsLoad.Get_Fields("fedcompamount"));
				}
				else
				{
					strFedComp = "";
				}
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLPOSITION, FCConvert.ToString(clsLoad.Get_Fields("position")));
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLSTATUS, FCConvert.ToString(clsLoad.Get_Fields("status")));
				GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLID, FCConvert.ToString(clsLoad.Get_Fields("ID")));
				strCombo += "|#" + clsLoad.Get_Fields("ID") + ";" + clsLoad.Get_Fields("position") + "\t" + clsLoad.Get_Fields("status") + "\t" + strReportType + "\t" + strHDC + strFedComp;
				clsLoad.MoveNext();
			}
			Grid.ColComboList(CNSTGRIDCOLDISTM, strCombo);
			// .ColHidden(CNSTGRIDCOLTAXTYPE) = False
			Grid.ColHidden(CNSTGRIDCOLTAXTYPE, true);
			Grid.ColHidden(CNSTGRIDCOLDISTM, false);
			Grid.ColHidden(6, true);
			Grid.ColHidden(7, false);
			Grid.ColHidden(8, false);
			Grid.ColHidden(9, false);
			Grid.ColHidden(10, false);
			if (boolUseProjects)
			{
				Grid.ColHidden(CNSTGRIDCOLPROJECT, false);
			}
			//FC:FINAL:DDU:#i2076 - designer verify this part
			//if ((Grid.RowHeight(0) * Grid.Rows + 100) < (this.HeightOriginal * 0.5 - 550))
			//{
			//	// .Height = (.RowHeight(0) * .Rows) - 100
			//	boolScrollBars = false;
			//}
			//else
			//{
			//	boolScrollBars = true;
			//}
			// .Height = (Me.Height * 0.5) - 550
			Grid.ColWidth(0, FCConvert.ToInt32(Grid.WidthOriginal * 0.03));
			Grid.ColWidth(1, FCConvert.ToInt32(Grid.WidthOriginal * 0.041));
			if (boolScrollBars)
			{
				Grid.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.141));
				Grid.ColWidth(CNSTGRIDCOLGROSS, FCConvert.ToInt32(Grid.WidthOriginal * 0.08));
			}
			else
			{
				Grid.ColWidth(2, FCConvert.ToInt32(Grid.WidthOriginal * 0.18));
				Grid.ColWidth(CNSTGRIDCOLGROSS, FCConvert.ToInt32(Grid.WidthOriginal * 0.08));
			}
			Grid.ColWidth(3, FCConvert.ToInt32(Grid.WidthOriginal * 0.046));
			Grid.ColWidth(CNSTGRIDCOLTAXTYPE, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColHidden(CNSTGRIDCOLTAXTYPE, true);
			Grid.ColWidth(CNSTGRIDCOLDISTM, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			Grid.ColWidth(7, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			Grid.ColWidth(8, Grid.WidthOriginal * 0);
			Grid.ColWidth(9, FCConvert.ToInt32(Grid.WidthOriginal * 0.06));
			Grid.ColWidth(10, FCConvert.ToInt32(Grid.WidthOriginal * 0.04));
			Grid.ColWidth(11, FCConvert.ToInt32(Grid.WidthOriginal * 0.1));
			Grid.ColWidth(12, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColWidth(CNSTGRIDCOLNUMWEEKS, FCConvert.ToInt32(Grid.WidthOriginal * 0.1));
			Grid.ColWidth(CNSTGRIDCOLWEEKSWITHHELD, FCConvert.ToInt32(Grid.WidthOriginal * 0.05));
			Grid.ColWidth(CNSTGRIDCOLDEFAULTHOURS, FCConvert.ToInt32(Grid.WidthOriginal * 0.058));
			Grid.ColWidth(CNSTGRIDCOLHOURSWEEK, FCConvert.ToInt32(Grid.WidthOriginal * 0.068));
			Grid.ColWidth(cnstgridcolID, 0);
			Grid.ColWidth(CNSTGRIDCOLCHANGES, 0);
			Grid.ColWidth(CNSTGRIDCOLPERIODTAXES, 0);
			Grid.ColWidth(CNSTGRIDCOLPERIODDEDS, 0);
			Grid.ColWidth(CNSTGRIDCOLEXISTING, 0);
			Grid.ColWidth(CNSTGRIDCOLPROJECT, FCConvert.ToInt32(Grid.WidthOriginal * 0.2));
			Grid.ColWidth(CNSTGRIDCOLCONTRACT, FCConvert.ToInt32(Grid.WidthOriginal * 0.25));
			// disable grid columns
			if (Grid.Rows > 2)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, Grid.Rows - 1, 10, Color.White);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, CNSTGRIDCOLHOURSWEEK, Grid.Rows - 1, CNSTGRIDCOLHOURSWEEK, Color.White);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 2, 5, Grid.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			if (!boolFormLoading)
			{
				for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
				{
					if (Strings.Left(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 10)), 1) == "0")
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 11, Color.White);
					}
					else
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			if (Grid.Rows > 3)
			{
				Grid.Row = 2;
				Grid.Col = 1;
			}
			clsDRWrapper rsMaster = new clsDRWrapper();
			rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
			if (rsMaster.EndOfFile())
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 6, Grid.Rows - 1, 6, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7, Grid.Rows - 1, 7, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				boolUnempExempt = true;
				boolFicaExempt = true;
			}
			else
			{
				if (Grid.Rows >= 3)
				{
					if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("FicaExempt")))
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 6, Grid.Rows - 1, 6, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						// boolUnempExempt = True
						boolFicaExempt = true;
					}
					else
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 6, Grid.Rows - 1, 6, Color.White);
						// boolUnempExempt = False
						boolFicaExempt = false;
					}
					// matthew call id 79666
					// If rsMaster.Fields("UnemploymentExempt") Then
					// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7, Grid.Rows - 1, 7) = TRIOCOLORGRAYBACKGROUND
					// boolUnempExempt = True
					// boolFICAExempt = True
					// Else
					// If Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7) <> TRIOCOLORGRAYBACKGROUND Then
					// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7, Grid.Rows - 1, 7) = vbWhite
					// End If
					// boolUnempExempt = False
					// boolFICAExempt = False
					// End If
				}
			}
			// NOW CHECK STUTA
			// matthew call id 79666
			// If boolUnempExempt Then
			rsMaster.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
			if (!rsMaster.EndOfFile())
			{
				if (FCConvert.ToString(rsMaster.Get_Fields("Unemployment")) == "N")
				{
					boolUnempExempt = true;
				}
				else
				{
					boolUnempExempt = false;
				}
			}
			// End If
			if (boolUnempExempt)
			{
				if (Grid.Rows > 3)
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7, Grid.Rows - 1, 7, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			else
			{
				if (Grid.Rows > 3)
				{
					if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						if (Grid.Rows > 3)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 7, Grid.Rows - 1, 7, Color.White);
						}
					}
				}
			}
			int intCol;
			clsDRWrapper rsData = new clsDRWrapper();
			for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
			{
				rsData.OpenRecordset("SELECT * FROM tblPayCategories where CategoryNumber = " + FCConvert.ToString(Conversion.Val(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3))), "TWPY0000.vb1");
				if (!rsData.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Type")))) != "Non-Pay (Hours)")
					{
						for (intCol = 5; intCol <= CNSTGRIDCOLDEFAULTHOURS; intCol++)
						{
							if (intCol != 12 && intCol != 11 && intCol != 7)
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol, Color.White);
							}
						}
					}
					else
					{
						for (intCol = CNSTGRIDCOLTAXTYPE; intCol <= CNSTGRIDCOLDEFAULTHOURS; intCol++)
						{
							if (intCol != 12)
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCol, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
						}
					}
				}
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLTAXTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			// Line1.Visible = True
			// Line1.BorderWidth = 2
			// Line1.X1 = Grid.Left - 100
			// Line1.X2 = Grid.Width '- 300
			// Line1.Y1 = (Grid.RowHeight(0) * 2) - 20
			// Line1.Y2 = (Grid.RowHeight(0) * 2) - 20
			// Line1.ZOrder 0
			//FC:FINAL:DDU:#i2076 - designer verify this part
			//GridTotals.TopOriginal = Grid.TopOriginal + Grid.HeightOriginal + 150;
			//GridTotals.WidthOriginal = Grid.ColWidth(CNSTGRIDCOLDEFAULTHOURS) + Grid.ColWidth(CNSTGRIDCOLHOURSWEEK) + Grid.ColWidth(CNSTGRIDCOLGROSS) + 800;
			GridTotals.ColWidth(0, GridTotals.WidthOriginal - (Grid.ColWidth(CNSTGRIDCOLHOURSWEEK) + Grid.ColWidth(CNSTGRIDCOLGROSS) + 50));
			GridTotals.ColWidth(1, Grid.ColWidth(CNSTGRIDCOLHOURSWEEK));
			GridTotals.ColWidth(2, Grid.ColWidth(CNSTGRIDCOLGROSS));
			//FC:FINAL:DDU:#i2074 - hide row instead of set it's height to 0
			GridTotals.RowHidden(2, true);
			//GridTotals.RowHeight(2, 0);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 2, 0, Color.Black);
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, false);
			//if (boolScrollBars)
			//{
			//	GridTotals.LeftOriginal = (Grid.LeftOriginal + Grid.WidthOriginal) - GridTotals.WidthOriginal - 200;
			//}
			//else
			//{
			//	GridTotals.LeftOriginal = (Grid.LeftOriginal + Grid.WidthOriginal) - GridTotals.WidthOriginal;
			//}
			for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
			{
				if (Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLCODE)) == 1)
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, intCounter, Grid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, intCounter, 3, Color.White);
				}
			}
			// intCounter
		}

		private void mnuEmployeeSchedule_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngID;
			// vbPorter upgrade warning: lngReturn As int	OnWrite(DialogResult)
			DialogResult lngReturn = 0;
			lngID = 0;
			rsLoad.OpenRecordset("select scheduleid from tblemployeemaster where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("scheduleid"))));
			}
			if (lngID > 0)
			{
				if (intDataChanged > 0 && !boolCantEdit)
				{
					lngReturn = MessageBox.Show("Data has been changed" + "\r\n" + "Save changes first?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
					if (lngReturn == DialogResult.Cancel)
						return;
					if (lngReturn == DialogResult.Yes)
					{
						if (!SaveData())
						{
							return;
						}
					}
				}
				if (boolFromDataEntry)
				{
					if (frmEmployeeSchedule.InstancePtr.Init(ref modGlobalVariables.Statics.gdatCurrentWeekEndingDate, ref modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber))
					{
						// must refresh screen
						RefreshScreen();
					}
				}
				else
				{
					// must pick a date
					DateTime dtReturn;
					dtReturn = frmDatePicker.InstancePtr.Init("Choose a date for week of", DateTime.Today.Year - 1, DateTime.Today.Year + 1, modGlobalVariables.Statics.gdatCurrentWeekEndingDate);
					if (dtReturn.ToOADate() != 0)
					{
						frmEmployeeSchedule.InstancePtr.Unload();
						if (frmEmployeeSchedule.InstancePtr.Init(ref dtReturn, ref modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber))
						{
							if (modSchedule.GetWeekBeginningDate(dtReturn).ToOADate() == modSchedule.GetWeekBeginningDate(modGlobalVariables.Statics.gdatCurrentWeekEndingDate).ToOADate())
							{
								RefreshScreen();
							}
						}
					}
				}
			}
			else
			{
				MessageBox.Show("This employee has not been assigned to a schedule type" + "\r\n" + "Cannot Continue", "No Schedule", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			cmdNewLine.Enabled = !modGlobalVariables.Statics.gboolDataEntry && !boolCantEdit;
			cmdDeleteLine.Enabled = !modGlobalVariables.Statics.gboolDataEntry && !boolCantEdit;
		}

		private void mnuLongAccountScreen_Click(object sender, System.EventArgs e)
		{
			intDataEntry = 2;
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				// If Not boolFromDataEntry Then
				boolCantEdit = true;
				cmdSave.Enabled = false;
				cmdSaveExit.Enabled = false;
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				txtDed.Enabled = false;
				txtTax.Enabled = false;
				chkDD[0].Enabled = false;
				chkDD[1].Enabled = false;
				chkDeductions[0].Enabled = false;
				chkDeductions[1].Enabled = false;
				chkMatches[0].Enabled = false;
				chkMatches[1].Enabled = false;
				chkMatches[2].Enabled = false;
				chkVacation.Enabled = false;
				// End If
			}
			SetLongAccountGrid();
		}

		private void mnuOpenDataEntryScreen_Click(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			this.Grid.Col = CNSTGRIDCOLNUMWEEKS;
			//App.DoEvents();
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				if (boolFromDataEntry)
				{
					boolCantEdit = false;
					cmdSave.Enabled = true;
					cmdSaveExit.Enabled = true;
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					txtDed.Enabled = true;
					txtTax.Enabled = true;
					chkDD[0].Enabled = true;
					chkDD[1].Enabled = true;
					chkDeductions[0].Enabled = true;
					chkDeductions[1].Enabled = true;
					chkMatches[0].Enabled = true;
					chkMatches[1].Enabled = true;
					chkMatches[2].Enabled = true;
					chkVacation.Enabled = true;
				}
			}
			SetDataEntryGrid();
			intDataEntry = 1;
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				Grid.Select(0, 0);
				SaveChanges();
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(frmEmployeeSearch.InstancePtr)))
					frmEmployeeSearch.InstancePtr.Unload();
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				Reload();
				modGlobalRoutines.SetEmployeeCaption(lblEmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				LoadData();
				if (intDataEntry == 1)
				{
					if (Grid.Rows > 2)
					{
						Grid.Row = 2;
						Grid.Col = CNSTGRIDCOLHOURSWEEK;
					}
				}
			}
		}

		private void mnuCaption1_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(1));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = FCConvert.ToBoolean(0);
			LoadData();
			lblEmployeeNumber.Text = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
		}

		private void mnuCaption2_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(2));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption3_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(3));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (3 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption4_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(4));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (4 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption5_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(5));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (5 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption6_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(6));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (6 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption7_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(7));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (7 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption8_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(8));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (8 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption9_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(9));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadData();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (9 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (Grid.Row < 0)
					return;
				if (Grid.Col < 0)
					return;
				string TempRow;
				clsDRWrapper rsData = new clsDRWrapper();
				TempRow = Grid.TextMatrix(Grid.Row, cnstgridcolID);
				// check the history table and see if there was any distributions for this employee
				// Call rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "' and DistAutoID = " & Val(TempRow))
				// If rsData.EndOfFile Then
				// Else
				// MsgBox "History was created for this employee and distribution number with a pay date of " & rsData.Fields("PayDate") & "." & Chr(13) & "Distribution record cannot be deleted.", vbCritical + vbOKOnly, "TRIO Software"
				// Exit Sub
				// End If
				int intRecordNumber = 0;
				if (MessageBox.Show("This action will delete record #" + FCConvert.ToString(Grid.Row - 1) + ". Continue?", "Payroll Distribution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					int intColNumber = 0;
					intColNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, 0))));
					if (Grid.TextMatrix(Grid.Row, CNSTGRIDCOLEXISTING) == "1")
					{
						Grid.RemoveItem(Grid.Row);
						for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
						{
							if (Conversion.Val(Grid.TextMatrix(intCounter, 0)) > intColNumber)
							{
								Grid.TextMatrix(intCounter, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, 0)) - 1));
							}
						}
						return;
					}
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					// Dave 12/14/2006---------------------------------------------------
					// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
					modAuditReporting.RemoveGridRow_8("Grid", intTotalNumberOfControls - 1, Grid.Row, clsControlInfo);
					// We then add a change record saying the row was deleted
					clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(Grid.Row) + " from Payroll Distribution");
					// -------------------------------------------------------------------
					Grid.RemoveItem(Grid.Row);
					// after we delete we have to adjust row numbers
					for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
					{
						if (Conversion.Val(Grid.TextMatrix(intCounter, 0)) > intColNumber)
						{
							Grid.TextMatrix(intCounter, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, 0)) - 1));
						}
					}
					rsDistribution.Execute("Delete from tblPayrollDistribution where ID = " + TempRow, "Payroll");
					rsDistribution.OpenRecordset("Select * From tblPayrollDistribution where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' Order by RecordNumber", "TWPY0000.vb1");
					if (!rsDistribution.EndOfFile())
					{
						rsDistribution.MoveLast();
						rsDistribution.MoveFirst();
						intRecordNumber = FCConvert.ToInt16(rsDistribution.Get_Fields_Int32("RecordNumber"));
						for (intCounter = 1; intCounter <= (rsDistribution.RecordCount()); intCounter++)
						{
							if (rsDistribution.Get_Fields_Int32("RecordNumber") > intRecordNumber)
							{
								rsDistribution.Edit();
								rsDistribution.SetData("RecordNumber", rsDistribution.Get_Fields_Int32("RecordNumber") - 1);
								rsDistribution.Update();
							}
							rsDistribution.MoveNext();
							if (!rsDistribution.EndOfFile())
								intRecordNumber = FCConvert.ToInt16(rsDistribution.Get_Fields_Int32("RecordNumber"));
						}
					}
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					LoadData();
					MessageBox.Show("Record Deleted successfully.", "Payroll Distribution", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuMultAdd_Click(object sender, System.EventArgs e)
		{
			frmMultiPay.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			modMultiPay.SetMultiPayCaptions(this);
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsData = new clsDRWrapper();
				intNewCount += 1;
				// add a new line to grid and initialize to default values
				Grid.AddItem("", Grid.Rows);
				Grid.TextMatrix(Grid.Rows - 1, 0, FCConvert.ToString(Grid.Rows - 2));
				Grid.Cell(FCGrid.CellPropertySettings.flexcpText, Grid.Rows - 1, 1, 2);
				if (boolPrefillAccount)
				{
					Grid.TextMatrix(Grid.Rows - 1, 2, strEmployeeDeptDiv);
				}
				// Call ID 77428 Matthew 9/15/2005
				// .Cell(FCGrid.CellPropertySettings.flexcpText, .Rows - 1, 3) = 1
				rsDistribution.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsDistribution.Get_Fields("Include")))
					{
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDISTM, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMDEFAULT));
					}
					else
					{
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDISTM, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMNOMSRS));
					}
				}
				else
				{
					Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDISTM, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTMNOMSRS));
				}
				rsDistribution.OpenRecordset("Select * from tblTaxStatusCodes where TaxStatusCode ='T'", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpText, Grid.Rows - 1, CNSTGRIDCOLTAXTYPE, rsDistribution.Get_Fields("ID"));
				}
				rsDistribution.OpenRecordset("Select * from tblPayCodes where PayCode = '0'", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					Grid.Cell(FCGrid.CellPropertySettings.flexcpText, Grid.Rows - 1, 10, rsDistribution.Get_Fields("ID"));
				}
				if (boolFicaExempt)
				{
				}
				else
				{
					Grid.TextMatrix(Grid.Rows - 1, 6, "True");
				}
				if (boolUnempExempt)
				{
				}
				else
				{
					Grid.TextMatrix(Grid.Rows - 1, 7, "True");
				}
				Grid.TextMatrix(Grid.Rows - 1, 8, "True");
				rsDistribution.OpenRecordset("Select WorkCompCode from tblEmployeeMaster where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					rsDistribution.MoveLast();
					rsDistribution.MoveFirst();
					Grid.TextMatrix(Grid.Rows - 1, 9, FCConvert.ToString(rsDistribution.Get_Fields_String("WorkCompCode")));
					// w/c get from master
				}
				Grid.TextMatrix(Grid.Rows - 1, 11, "0");
				Grid.TextMatrix(Grid.Rows - 1, 12, "1");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLNUMWEEKS, "0");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLWEEKSWITHHELD, "0");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDEFAULTHOURS, "0");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLHOURSWEEK, "0");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLGROSS, "0");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLCHANGES, ".");
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLPERIODTAXES, txtTax.Text);
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLPERIODDEDS, txtDed.Text);
				Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLEXISTING, "1");
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Rows - 1, 12, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Rows - 1, CNSTGRIDCOLGROSS, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				Grid.Select(-1, 9);
				NewFlag = true;
				// set to true so we will lock the right col's on new
				Grid_AfterEdit(null, null);
				//Grid_AfterEdit(Grid.Row, Grid.Col);
				//Grid_AfterEdit(Grid.Row, CNSTGRIDCOLNUMWEEKS);
				if (modGlobalVariables.Statics.gboolDataEntry)
				{
					SetDataEntryGrid();
				}
				else
				{
					SetDistributionGrid();
				}
				intNewCount -= 1;
				intDataChanged += 1;
				// set focus to this new row
				this.Grid.Select(Grid.Rows - 1, 1);
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Payroll Distribution");
				// ------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				ResumeCode:
				;
				// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
				int intCount;
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsMisc = new clsDRWrapper();
				clsDRWrapper rsVacSick = new clsDRWrapper();
				clsDRWrapper rsPayCat = new clsDRWrapper();
				clsDRWrapper rsDD = new clsDRWrapper();
				int lngRow = 0;
				int x;
				NotValidData = false;
				Grid.Select(0, 0);
				Grid.Focus();
				boolDoNotExit = true;
				if (!CheckTaxPer())
				{
					return SaveData;
				}
				if (intInitialDedUpTop != FCConvert.ToDouble(txtDed.Text))
				{
					// CHECK TO SEE IF THIS PERSON HAS ANY DIRECT DEPOSITS THAT ARE OF A DOLLAR AMOUNT
					rsDD.OpenRecordset("Select * from tblDirectDeposit where EmployeeNumber  ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
					while (!rsDD.EndOfFile())
					{
						if (FCConvert.ToString(rsDD.Get_Fields("Type")) == "Dollars")
						{
							// WARN THAT THIS AMOUNT WILL BE MULTIPLIED BY THE DEDUCTION NUMBER
							// If MsgBox("The Deduction Multiplier for this employee will be multiplied by the amount of deposits that are of type Dollars. Verify that this will result in correct deposit amounts. Continue?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
							if (MessageBox.Show("The dollar type direct deposit amounts will be multiplied by the deduction multiplier (DED's). Verify that this will result in the correct direct deposit amounts. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								goto NextRow;
							}
							else
							{
								boolDoNotExit = true;
								return SaveData;
							}
						}
						rsDD.MoveNext();
					}
				}
				NextRow:
				;
				if (modGlobalVariables.Statics.gboolCheckNegBalances)
				{
					// COMPARE WITH THE BALA
					rsVacSick.OpenRecordset("SELECT tblAssociateCodes.PayCatID, tblVacationSick.UsedBalance, tblVacationSick.EmployeeNumber,tblVacationSick.Selected FROM tblAssociateCodes INNER JOIN tblVacationSick ON (tblAssociateCodes.CodeType = tblVacationSick.TypeID) order by tblvacationsick.selected desc");
					rsPayCat.OpenRecordset("Select * from tblPayCategories");
					for (intCount = 2; intCount <= (Grid.Rows - 1); intCount++)
					{
						if (FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 3)) == string.Empty)
						{
							MessageBox.Show("Invalid Category entry.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							Grid.Select(intCount, 3);
							return SaveData;
						}
						if (rsPayCat.FindFirstRecord("ID", Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 3)))
						{
							if (Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(rsPayCat.Get_Fields("Type"))), 7) != "Non-Pay")
							{
								// CALL ID 81747 MATTHEW 11/22/2005
								// THIS FUNCTION WILL CHECK TO SEE IF THEY HAVE THIS PAY CATEGORY SETUP IN THE VACATION SICK TABLE
								if (rsVacSick.FindFirst("Employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and PayCatID = " + FCConvert.ToString(Conversion.Val(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 3)))))
								{
									// If rsVacSick.FindFirstRecord2("EmployeeNumber,PayCatID", gtypeCurrentEmployee.EmployeeNumber & "," & Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 3), ",") Then
									if (rsVacSick.Get_Fields_Double("UsedBalance") < FCConvert.ToDouble(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, CNSTGRIDCOLHOURSWEEK)) && FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, CNSTGRIDCOLHOURSWEEK)) != 0)
									{
										if (MessageBox.Show("Employee: " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + " has a balance of  " + rsVacSick.Get_Fields_Double("UsedBalance") + "  for the pay category " + Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCount, 3) + "." + "\r" + "\r" + "Your distribution line is requesting to pay  " + Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, CNSTGRIDCOLHOURSWEEK) + "  which would create a negative balance. " + "\r" + "\r" + "These balances do NOT include any time that may be accrued during this pay run. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
										{
										}
										else
										{
											Grid.Select(intCount, CNSTGRIDCOLHOURSWEEK);
											return SaveData;
										}
									}
								}
							}
						}
					}
				}
				if (Conversion.Val(this.txtDed.Text) > 52)
				{
					MessageBox.Show("Deduction multiplier cannot be greater then 52 weeks.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtDed.Focus();
					txtDed.SelectionStart = 0;
					txtDed.SelectionLength = txtDed.Text.Length;
					NotValidData = true;
				}
				else if (Conversion.Val(txtTax.Text) > 52)
				{
					MessageBox.Show("Tax multiplier cannot be greater then 52 weeks.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtTax.Focus();
					txtTax.SelectionStart = 0;
					txtTax.SelectionLength = txtTax.Text.Length;
					NotValidData = true;
				}
				else if (Conversion.Val(txtTax.Text) != Conversion.Val(txtTax.Text))
				{
					MessageBox.Show("Tax multiplier must be a whole number", "Invalid Multiplier", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					NotValidData = true;
				}
				else if (Conversion.Val(txtDed.Text) != Conversion.Val(txtDed.Text))
				{
					MessageBox.Show("Deduction multiplier must be a whole number", "Invalid Multiplier", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					NotValidData = true;
				}
				// vbPorter upgrade warning: intValid As int	OnWrite(bool)
				int intValid = 0;
				string strEditText = "";
				int intBreakdownCode = 0;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCount = 2; intCount <= (Grid.Rows - 1); intCount++)
				{
					if (modRegionalTown.IsRegionalTown())
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2))).Length >= 3 && Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2))), 1) == "E")
						{
							if (FCConvert.ToDouble(Strings.Mid(fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2))), 3, 1)) == 0)
							{
								// this is a multitown number so check it three times
								intBreakdownCode = 0;
								rsData.OpenRecordset("Select * from DeptDivTitles where Department = '1" + Strings.Mid(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2)), 4, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) - 1)) + "'", "TWBD0000.vb1");
								if (!rsData.EndOfFile())
								{
									if (FCConvert.ToInt32(rsData.Get_Fields("BreakDownCode")) != 0)
									{
										intBreakdownCode = FCConvert.ToInt16(rsData.Get_Fields("BreakDownCode"));
									}
								}
								if (intBreakdownCode == 0)
								{
									MessageBox.Show("Invalid Account Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
									Grid.Select(intCount, 2);
									return SaveData;
								}
								else
								{
									rsData.OpenRecordset("Select * from RegionalTownBreakDown where Code = " + FCConvert.ToString(intBreakdownCode) + " Order by TownNumber", "CentralDate");
									while (!rsData.EndOfFile())
									{
										strEditText = "E " + rsData.Get_Fields("TownNumber") + Strings.Right(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2)), FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2)).Length - 3);
										intValid = FCConvert.ToInt16(modValidateAccount.AccountValidate(strEditText));
										if (intValid == 0)
										{
											MessageBox.Show("Invalid Account Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
											Grid.Select(intCount, 2);
											return SaveData;
										}
										rsData.MoveNext();
									}
								}
							}
							else
							{
								strEditText = FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2));
								intValid = FCConvert.ToInt16(modValidateAccount.AccountValidate(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2)));
								if (intValid == 0)
								{
									MessageBox.Show("Invalid Account Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
									Grid.Select(intCount, 2);
									return SaveData;
								}
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							Grid.Select(intCount, 2);
							return SaveData;
						}
					}
					else
					{
						if (modValidateAccount.AccountValidate(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, 2)))
						{
						}
						else
						{
							MessageBox.Show("Invalid Account Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							Grid.Select(intCount, 2);
							return SaveData;
						}
					}
				}
				if (NotValidData == true)
					return SaveData;
				this.Focus();
				rsDistribution.OpenRecordset("Select * from tblPayrollDistribution where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' ORDER BY RecordNumber", "TWPY0000.vb1");
				if (!rsDistribution.EndOfFile())
				{
					rsDistribution.MoveLast();
					rsDistribution.MoveFirst();
				}
				else
				{
					goto here;
				}
				here:
				;
				rsDistribution2.OpenRecordset("Select WorkCompCode from tblEmployeeMaster where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				if (!rsDistribution2.EndOfFile())
				{
					rsDistribution2.MoveLast();
					rsDistribution2.MoveFirst();
					rsMisc.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
					// Dave 12/14/2006--------------------------------------------
					// Set New Information so we can compare
					for (x = 0; x <= intTotalNumberOfControls - 1; x++)
					{
						clsControlInfo[x].FillNewValue(this);
					}
					// Thsi function compares old and new values and creates change records for any differences
					modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
					// This function takes all the change records and writes them into the AuditChanges table in the database
					clsReportChanges.SaveToAuditChangesTable("Payroll Distribution", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
					// Reset all information pertianing to changes and start again
					intTotalNumberOfControls = 0;
					clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
					clsReportChanges.Reset();
					// Initialize all the control and old data values
					FillControlInformationClass();
					FillControlInformationClassFromGrid();
					for (x = 0; x <= intTotalNumberOfControls - 1; x++)
					{
						clsControlInfo[x].FillOldValue(this);
					}
					// ----------------------------------------------------------------
					// put data into the grid for the selected employee
					Grid.Select(0, 0);
					for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
					{
						// If Right(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 18), 1) = "." Then
						// Call rsDistribution.FindFirst("RecordNumber = " & intCounter - 1)
						if (!rsDistribution.FindFirst("id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, cnstgridcolID)))))
						{
							// rsDistribution.FindFirstRecord "RecordNumber", Grid.TextMatrix(intCounter, 0)
							// If rsDistribution.NoMatch Then
							// corey 12/28/2005 Allow it to save a Not Used Code
							// if user added a new record to grid then add a record to database also
							// If Left(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1), 1) = "N" Then
							// GoTo NextRecord
							// Else
							if (Conversion.Val(Grid.TextMatrix(intCounter, cnstgridcolID)) == 0)
							{
								rsDistribution.AddNew();
								rsDistribution.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
								rsDistribution.SetData("RecordNumber", intCounter - 1);
							}
							else
							{
								goto NextRecord;
								// record exists but obviously there was an error loading it
							}
							// End If
						}
						else
						{
							// just saving data that was there on load
							// If Left(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1), 1) = "N" Then
							// rsDistribution.Delete
							// GoTo NextRecord
							// Else
							rsDistribution.Edit();
							// End If
						}
						rsDistribution.SetData("AccountCode", Grid.TextMatrix(intCounter, 1));
						rsDistribution.SetData("RecordNumber", intCounter - 1);
						if (fecherFoundation.Strings.Trim(Strings.Mid(Grid.TextMatrix(intCounter, 2), 3, 1)) == string.Empty || fecherFoundation.Strings.Trim(Strings.Mid(Grid.TextMatrix(intCounter, 2), 3, 1)) == "_")
						{
							// If Trim(Mid(GRID.TextMatrix(intCounter, 2), 2, 1)) = vbNullString Or Trim(Mid(GRID.TextMatrix(intCounter, 2), 2, 1)) = "_" Then
							rsDistribution.SetData("AccountNumber", "M 0        ");
						}
						else
						{
							rsDistribution.SetData("AccountNumber", Grid.TextMatrix(intCounter, 2));
						}
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3))) == string.Empty)
						{
							MessageBox.Show("Missing Category for row " + FCConvert.ToString(intCounter - 1), "Bad Category", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							Grid.Select(intCounter, 3);
							return SaveData;
						}
						else
						{
							rsDistribution.SetData("Cat", Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3));
						}
						// rsDistribution.SetData "TaxCode", Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 4)  'GRID.ComboItem"
						// ******************************************************************************************
						// MATTHEW 05/18/2005
						// ******************************************************************************************
						// ******************************************************************************************
						// MATTHEW 11/15/2005 call id 81689
						// If intDataEntry <> 1 Then
						// ******************************************************************************************
						// find the info in the distm grid by the ID
						lngRow = GridDistM.FindRow(FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLDISTM)))), 0, CNSTGRIDDISTMCOLID);
						if (lngRow >= 0)
						{
							// rsDistribution.SetData "MSRS", SplitMSRS(GRID.TextMatrix(intCounter, 5), 1)
							// rsDistribution.SetData "StatusCode", SplitMSRS(GRID.TextMatrix(intCounter, 5), 2)
							// rsDistribution.SetData "GrantFunded", SplitMSRS(GRID.TextMatrix(intCounter, 5), 3)
							rsDistribution.Set_Fields("msrs", GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLPOSITION));
							rsDistribution.Set_Fields("statuscode", GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLSTATUS));
							if (fecherFoundation.Strings.Trim(GridDistM.TextMatrix(lngRow, CNSTGRIDDISTMCOLFEDCOMPAMOUNT)) != string.Empty)
							{
								rsDistribution.Set_Fields("grantfunded", 1);
							}
							else
							{
								rsDistribution.Set_Fields("grantfunded", 0);
							}
							rsDistribution.Set_Fields("msrsid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLDISTM))));
						}
						else
						{
							rsDistribution.Set_Fields("MSRS", "N");
							rsDistribution.Set_Fields("StatusCode", "");
							rsDistribution.Set_Fields("GrantFunded", 0);
							rsDistribution.Set_Fields("msrsid", modCoreysSweeterCode.CNSTDISTMNOMSRS);
						}
						// End If
						rsDistribution.SetData("DistU", (Grid.TextMatrix(intCounter, 7) == string.Empty ? "0" : Grid.TextMatrix(intCounter, 7)));
						rsDistribution.SetData("WC", Grid.TextMatrix(intCounter, 8));
						if (FCConvert.ToString(rsDistribution2.Get_Fields_String("WorkCompCode")) == Grid.TextMatrix(intCounter, 9))
						{
							rsDistribution.SetData("WorkComp", "0");
						}
						else
						{
							rsDistribution.SetData("WorkComp", Grid.TextMatrix(intCounter, 9) + " ");
						}
						rsDistribution.SetData("CD", Grid.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 10));
						rsDistribution.SetData("BaseRate", Conversion.Val(Grid.TextMatrix(intCounter, 11)));
						rsDistribution.SetData("Factor", Grid.TextMatrix(intCounter, 12));
						rsDistribution.SetData("NumberWeeks", Grid.TextMatrix(intCounter, CNSTGRIDCOLNUMWEEKS));
						rsDistribution.Set_Fields("WeeksTaxWithheld", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLWEEKSWITHHELD))));
						rsDistribution.Set_Fields("project", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLPROJECT))));
						rsDistribution.SetData("DefaultHours", Strings.Format(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLDEFAULTHOURS)), "######0.00"));
						rsDistribution.SetData("HoursWeek", Strings.Format(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLHOURSWEEK)), "######0.00"));
						rsDistribution.SetData("Gross", Grid.TextMatrix(intCounter, CNSTGRIDCOLGROSS));
						rsDistribution.Set_Fields("ContractID", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intCounter, CNSTGRIDCOLCONTRACT))));
						Grid.TextMatrix(intCounter, CNSTGRIDCOLEXISTING, "0");
						rsDistribution.Update();
						Grid.TextMatrix(intCounter, cnstgridcolID, FCConvert.ToString(rsDistribution.Get_Fields("id")));
						// End If
						NextRecord:
						;
					}
				}
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				if (rsMaster.EndOfFile())
				{
					rsMaster.AddNew();
				}
				else
				{
					rsMaster.Edit();
				}
				rsMaster.Set_Fields("PeriodTaxes", FCConvert.ToString(Conversion.Val(txtTax.Text)));
				rsMaster.Set_Fields("PeriodDeds", FCConvert.ToString(Conversion.Val(txtDed.Text)));
				// rsMaster.Fields("S1CheckAsP1Deductions") = chkDeductions
				rsMaster.Set_Fields("DeductionPercent", chkDeductions[0].CheckState);
				rsMaster.Set_Fields("DeductionDollars", chkDeductions[1].CheckState);
				rsMaster.Set_Fields("DDPercent", chkDD[0].CheckState);
				rsMaster.Set_Fields("DDDollars", chkDD[1].CheckState);
				rsMaster.Set_Fields("MatchPercentGross", chkMatches[0].CheckState);
				rsMaster.Set_Fields("MatchPercentDeduction", chkMatches[1].CheckState);
				rsMaster.Set_Fields("MatchDollars", chkMatches[2].CheckState);
				// If chkDeductions Then
				// AddCYAEntry "PY", "Save of S1 Check As P1 Deductions", "EmployeeNumber = " & gtypeCurrentEmployee.EMPLOYEENUMBER, "PayDate = " & CStr(gdatCurrentPayDate)
				// End If
				rsMaster.Set_Fields("S1CheckAsP1Vacation", chkVacation.CheckState);
				if (chkVacation.CheckState == CheckState.Checked)
					modGlobalFunctions.AddCYAEntry_6("PY", "Save of S1 Check As P1 Vacation", "EmployeeNumber = " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, "PayDate = " + modGlobalVariables.Statics.gdatCurrentPayDate.ToString());
				// rsMaster.Fields("S1CheckAsP1Matches") = chkMatches
				// If chkMatches Then AddCYAEntry "PY", "Save of S1 Check As P1 Matches", "EmployeeNumber = " & gtypeCurrentEmployee.EMPLOYEENUMBER, "PayDate = " & CStr(gdatCurrentPayDate)
				// rsMaster.Fields("S1CheckAsP1DirectDeposits") = chkDD
				// If chkDD Then AddCYAEntry "PY", "Save of S1 Check As P1 DDs", "EmployeeNumber = " & gtypeCurrentEmployee.EMPLOYEENUMBER, "PayDate = " & CStr(gdatCurrentPayDate)
				rsMaster.Set_Fields("dataentrydone", true);
				rsMaster.Update();
				boolDoNotExit = false;
				intDataChanged = 0;
				modGlobalRoutines.UpdatePayrollStepTable("DataEntry", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				if (modGlobalVariables.Statics.gboolDataEntry)
				{
				}
				else
				{
					MessageBox.Show("Save was successful", "Payroll Distribution", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				LoadData();
				// CHECK TO SEE IF THE CALCULATION HAS ALREADY BEEN RUN
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Calculation", "Calculation", true))
				{
					// CHECK TO SEE IF THE VERIFY AND ACCEPT HAS BEEN RUN
					if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "VerifyAccept", true))
					{
						// ACCEPT HAS BEEN DONE SO DO NOT CLEAR OUT THE CALCULATION FLAG
					}
					else
					{
						// CLEAR OUT THE CALCULATION FIELD
						modGlobalRoutines.UpdatePayrollStepTable("EditTotals");
					}
				}
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuSave_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(cmdSave, new System.EventArgs());
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, int, string)
		private string SplitMSRS(string strTemp, int intMSRS)
		{
			string SplitMSRS = null;
			// vbPorter upgrade warning: aryTemp As object	OnWrite(string())
			string[] aryTemp;
			aryTemp = Strings.Split(strTemp, "-", -1, CompareConstants.vbBinaryCompare);
			if (Information.UBound(aryTemp, 1) > 0)
			{
				// there is a status code
				if (intMSRS == 1)
				{
					SplitMSRS = (aryTemp)[0];
				}
				else if (intMSRS == 2)
				{
					SplitMSRS = (aryTemp)[1];
				}
				else
				{
					if (Information.UBound(aryTemp, 1) > 1)
					{
						SplitMSRS = (aryTemp)[2];
					}
					else
					{
						SplitMSRS = "0";
					}
				}
			}
			else
			{
				// return everything
				if (intMSRS == 1)
				{
					SplitMSRS = fecherFoundation.Strings.Trim(strTemp);
				}
				else if (intMSRS == 2)
				{
					SplitMSRS = string.Empty;
				}
				else
				{
					SplitMSRS = "0";
				}
			}
			SplitMSRS = fecherFoundation.Strings.Trim(FCConvert.ToString(SplitMSRS));
			return SplitMSRS;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuSaveExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolFromDataEntry)
				{
					mnuSave_Click();
					if (!boolDoNotExit)
					{
						mnuExit_Click();
						frmEmployeeListing.InstancePtr.Show(App.MainForm);
					}
				}
				else
				{
					mnuSave_Click();
					if (!boolDoNotExit)
						mnuExit_Click();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuUpdateBaseRate_Click(object sender, System.EventArgs e)
		{
			if (!modGlobalVariables.Statics.gboolDataEntry)
			{
				frmDistributionBaseRateUpdate.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				MessageBox.Show("Base rate can only be changed from the distribution screen.", "VQ Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuViewValidAccounts_Click()
		{
			// vbPorter upgrade warning: intGridRow As int	OnWriteFCConvert.ToInt32(
			int intGridRow = 0;
			if (!modGlobalVariables.Statics.gboolDataEntry)
			{
				intGridRow = Grid.Row;
				// frmValidAccounts.Show 1, MDIParent
				modGlobalVariables.Statics.gstrSelectedAccount = frmLoadValidAccounts.InstancePtr.Init(Grid.TextMatrix(intGridRow, 2));
				if (modGlobalVariables.Statics.gstrSelectedAccount != string.Empty)
				{
					if (intGridRow > 1)
					{
						Grid.TextMatrix(intGridRow, 2, modGlobalVariables.Statics.gstrSelectedAccount);
						Grid.Select(intGridRow, 3);
					}
					else
					{
						MessageBox.Show("Cursor must be in row that you wish account number to be intered in.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
		}

		private void txtDed_TextChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
			{
				Grid.TextMatrix(intCounter, CNSTGRIDCOLPERIODDEDS, txtDed.Text);
			}
			GridTotals.TextMatrix(2, 2, txtDed.Text);
		}

		private void txtDed_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDed_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtDed.Text.Length > 2)
			{
				MessageBox.Show("Maximum Length Of 2", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtDed.SelectionStart = 0;
				txtDed.SelectionLength = txtDed.Text.Length;
				e.Cancel = true;
			}
			intDataChanged += 1;
		}

		private void txtTax_TextChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			dblTaxColTotals = 0;
			for (intCounter = 2; intCounter <= (Grid.Rows - 1); intCounter++)
			{
				Grid.TextMatrix(intCounter, CNSTGRIDCOLPERIODTAXES, txtTax.Text);
			}
		}

		private void txtTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtTax.Text.Length > 3)
			{
				MessageBox.Show("Maximum Length Of 3", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
				e.Cancel = true;
			}
			intDataChanged += 1;
		}

		private bool CheckTaxPer()
		{
			bool CheckTaxPer = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int[] LastTaxPer = new int[11 + 1];
				int[] LastWeeksPer = new int[11 + 1];
				int intDefault;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				int intLast;
				int intCheck = 0;
				double dblGross = 0;
				int intWeek = 0;
				int intDefaultWeeks;
				for (x = 0; x <= 11; x++)
				{
					LastTaxPer[x] = 0;
					LastWeeksPer[x] = 0;
				}
				// x
				CheckTaxPer = false;
				intDefault = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTax.Text)));
				// default
				if (intDefault == 0)
					intDefault = 0;
				intDefaultWeeks = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTax.Text)));
				for (x = 2; x <= (Grid.Rows - 1); x++)
				{
					dblGross = Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLGROSS));
					if (dblGross != 0)
					{
						// if there is any gross
						intWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, 1))));
						if ((intWeek == 2) || (intWeek == 3))
						{
							if (LastTaxPer[0] == 0)
							{
								if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLNUMWEEKS)) == 0)
								{
									LastTaxPer[0] = intDefault;
								}
								else
								{
									LastTaxPer[0] = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLNUMWEEKS))));
								}
							}
							else
							{
								intCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLNUMWEEKS))));
								if (intCheck == 0)
									intCheck = intDefault;
								if (intCheck != LastTaxPer[0])
								{
									MessageBox.Show("Number of tax periods must be the same if on the same check", "Multiple Tax Periods", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckTaxPer;
								}
							}
							if (LastWeeksPer[0] == 0)
							{
								if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLWEEKSWITHHELD)) == 0)
								{
									LastWeeksPer[0] = intDefaultWeeks;
								}
								else
								{
									LastWeeksPer[0] = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLWEEKSWITHHELD))));
								}
							}
							else
							{
								intCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLWEEKSWITHHELD))));
								if (intCheck == 0)
									intCheck = intDefaultWeeks;
								if (intCheck != LastWeeksPer[0])
								{
									MessageBox.Show("Numer of tax periods withheld must be the same if on the same check", "Multiple Tax Periods Withheld", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckTaxPer;
								}
							}
						}
						else if (intWeek >= 4 && intWeek <= 12)
						{
							if (LastTaxPer[intWeek - 3] == 0)
							{
								if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLNUMWEEKS)) == 0)
								{
									LastTaxPer[intWeek - 3] = intDefault;
								}
								else
								{
									LastTaxPer[intWeek - 3] = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLNUMWEEKS))));
								}
							}
							else
							{
								intCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLNUMWEEKS))));
								if (intCheck == 0)
									intCheck = intDefault;
								if (intCheck != LastTaxPer[intWeek - 3])
								{
									MessageBox.Show("Number of tax periods must be the same if on the same check", "Multiple Tax Periods", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckTaxPer;
								}
							}
							if (LastWeeksPer[intWeek - 3] == 0)
							{
								if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLWEEKSWITHHELD)) == 0)
								{
									LastWeeksPer[intWeek - 3] = intDefaultWeeks;
								}
								else
								{
									LastWeeksPer[intWeek - 3] = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLWEEKSWITHHELD))));
								}
							}
							else
							{
								intCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLWEEKSWITHHELD))));
								if (intCheck == 0)
									intCheck = intDefaultWeeks;
								if (intCheck != LastWeeksPer[intWeek - 3])
								{
									MessageBox.Show("Number of tax periods withheld must be the same if on the same check", "Multiple Tax Periods Withheld", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckTaxPer;
								}
							}
						}
						else
						{
						}
					}
				}
				// x
				CheckTaxPer = true;
				return CheckTaxPer;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckTaxPer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckTaxPer;
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtTax";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Taxes";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtDed";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Deds";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkDD";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Direct Deposit Percent";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkDD";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 1;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Direct Deposit Dollars";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkDeductions";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Deductions Percent";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkDeductions";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 1;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Deductions Dollars";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkMatches";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Matches Percent";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkMatches";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 1;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Matches % D";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkMatches";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 2;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Matches Dollars";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkVacation";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Vacation / Sick / Other";
			intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			for (intRows = 2; intRows <= (Grid.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (Grid.Cols - 1); intCols++)
				{
					if (intCols != 0 && intCols != CNSTGRIDCOLTAXTYPE && intCols != 6 && intCols != 12 && intCols != CNSTGRIDCOLGROSS && intCols != cnstgridcolID && intCols != CNSTGRIDCOLCHANGES && intCols != CNSTGRIDCOLPERIODTAXES && intCols != CNSTGRIDCOLPERIODDEDS && intCols != CNSTGRIDCOLEXISTING)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "Grid";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + Grid.TextMatrix(1, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}
	}
}
