﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cEmployeeCheck
	{
		//=========================================================
		private int lngIDNumber;
		// vbPorter upgrade warning: strEmployeeNumber As object	OnWrite(string)
		private object strEmployeeNumber;
		private DateTime dtPayDate;
		private DateTime dtCheckDate;
		private int lngCheckNumber;
		private cGenericCollection colPayItems = new cGenericCollection();
		private cGenericCollection colDeductions = new cGenericCollection();
		private cGenericCollection colBenefits = new cGenericCollection();
		private cGenericCollection colDeposits = new cGenericCollection();
		private double dblCheckAmount;
		private double dblSavingsAmount;
		private double dblCheckingAmount;
		private Dictionary<object, object> dEmployeeTaxes = new Dictionary<object, object>();
		private double dblNetPay;
		private cGenericCollection colEarnedTotals = new cGenericCollection();
		private cPayRun currentPayRun = new cPayRun();
		private string strDeptDiv = string.Empty;
		private string strEmployeeName = string.Empty;
		private double dblBasePayRate;
		private double dblYTDGross;
		private double dblYTDNet;
		private double dblYTDDeductions;
		private double dblBenefitsTotal;
		private double dblYTDBenefitsTotal;
		private string strCheckMessage = string.Empty;
		private string strSSN = string.Empty;
		private double dblCurrentGross;
		private cEmployee checkEmployee = new cEmployee();
		private bool boolNonNegotiable;
		private bool boolSelected;

		public bool Selected
		{
			set
			{
				boolSelected = value;
			}
			get
			{
				bool Selected = false;
				Selected = boolSelected;
				return Selected;
			}
		}

		public bool NonNegotiable
		{
			set
			{
				boolNonNegotiable = value;
			}
			get
			{
				bool NonNegotiable = false;
				NonNegotiable = boolNonNegotiable;
				return NonNegotiable;
			}
		}

		public cEmployee Employee
		{
			set
			{
				checkEmployee = value;
			}
			get
			{
				cEmployee Employee = null;
				Employee = checkEmployee;
				return Employee;
			}
		}

		public double CurrentGross
		{
			set
			{
				dblCurrentGross = value;
			}
			get
			{
				double CurrentGross = 0;
				CurrentGross = dblCurrentGross;
				return CurrentGross;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public string CheckMessage
		{
			set
			{
				strCheckMessage = value;
			}
			get
			{
				string CheckMessage = "";
				CheckMessage = strCheckMessage;
				return CheckMessage;
			}
		}

		public double YearToDateBenefitsTotal
		{
			set
			{
				dblYTDBenefitsTotal = value;
			}
			get
			{
				double YearToDateBenefitsTotal = 0;
				YearToDateBenefitsTotal = dblYTDBenefitsTotal;
				return YearToDateBenefitsTotal;
			}
		}

		public double BenefitsTotal
		{
			set
			{
				dblBenefitsTotal = value;
			}
			get
			{
				double BenefitsTotal = 0;
				BenefitsTotal = dblBenefitsTotal;
				return BenefitsTotal;
			}
		}

		public double YearToDateDeductions
		{
			set
			{
				dblYTDDeductions = value;
			}
			get
			{
				double YearToDateDeductions = 0;
				YearToDateDeductions = dblYTDDeductions;
				return YearToDateDeductions;
			}
		}

		public double YearToDateNet
		{
			set
			{
				dblYTDNet = value;
			}
			get
			{
				double YearToDateNet = 0;
				YearToDateNet = dblYTDNet;
				return YearToDateNet;
			}
		}

		public double YearToDateGross
		{
			set
			{
				dblYTDGross = value;
			}
			get
			{
				double YearToDateGross = 0;
				YearToDateGross = dblYTDGross;
				return YearToDateGross;
			}
		}

		public double BasePayrate
		{
			set
			{
				dblBasePayRate = value;
			}
			get
			{
				double BasePayrate = 0;
				BasePayrate = dblBasePayRate;
				return BasePayrate;
			}
		}

		public string EmployeeName
		{
			set
			{
				strEmployeeName = value;
			}
			get
			{
				string EmployeeName = "";
				EmployeeName = strEmployeeName;
				return EmployeeName;
			}
		}

		public string DepartmentDivision
		{
			set
			{
				strDeptDiv = value;
			}
			get
			{
				string DepartmentDivision = "";
				DepartmentDivision = strDeptDiv;
				return DepartmentDivision;
			}
		}

		public cPayRun PayRun
		{
			get
			{
				cPayRun PayRun = null;
				PayRun = currentPayRun;
				return PayRun;
			}
		}

		public cGenericCollection EarnedTime
		{
			get
			{
				cGenericCollection EarnedTime = null;
				EarnedTime = colEarnedTotals;
				return EarnedTime;
			}
		}

		public double Net
		{
			set
			{
				dblNetPay = value;
			}
			get
			{
				double Net = 0;
				Net = dblNetPay;
				return Net;
			}
		}

		public Dictionary<object, object> EmployeeTaxes
		{
			get
			{
				Dictionary<object, object> EmployeeTaxes = null;
				EmployeeTaxes = dEmployeeTaxes;
				return EmployeeTaxes;
			}
		}

		public int ID
		{
			set
			{
				lngIDNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngIDNumber;
				return ID;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = FCConvert.ToString(strEmployeeNumber);
				return EmployeeNumber;
			}
		}

		public DateTime PayDate
		{
			set
			{
				dtPayDate = value;
			}
			get
			{
				DateTime PayDate = System.DateTime.Now;
				PayDate = dtPayDate;
				return PayDate;
			}
		}

		public DateTime CheckDate
		{
			set
			{
				dtCheckDate = value;
			}
			get
			{
				DateTime CheckDate = System.DateTime.Now;
				CheckDate = dtCheckDate;
				return CheckDate;
			}
		}

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}

		public cGenericCollection PayItems
		{
			get
			{
				cGenericCollection PayItems = null;
				PayItems = colPayItems;
				return PayItems;
			}
		}

		public cGenericCollection DEDUCTIONS
		{
			get
			{
				cGenericCollection DEDUCTIONS = null;
				DEDUCTIONS = colDeductions;
				return DEDUCTIONS;
			}
		}

		public cGenericCollection Benefits
		{
			get
			{
				cGenericCollection Benefits = null;
				Benefits = colBenefits;
				return Benefits;
			}
		}

		public cGenericCollection Deposits
		{
			get
			{
				cGenericCollection Deposits = null;
				Deposits = colDeposits;
				return Deposits;
			}
		}

		public double CheckAmount
		{
			set
			{
				dblCheckAmount = value;
			}
			get
			{
				double CheckAmount = 0;
				CheckAmount = dblCheckAmount;
				return CheckAmount;
			}
		}

		public double SavingsAmount
		{
			set
			{
				dblSavingsAmount = value;
			}
			get
			{
				double SavingsAmount = 0;
				SavingsAmount = dblSavingsAmount;
				return SavingsAmount;
			}
		}

		public double CheckingAmount
		{
			set
			{
				dblCheckingAmount = value;
			}
			get
			{
				double CheckingAmount = 0;
				CheckingAmount = dblCheckingAmount;
				return CheckingAmount;
			}
		}
	}
}
