//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPrintParameters.
	/// </summary>
	partial class frmPrintParameters
	{
		public fecherFoundation.FCComboBox cmbPage;
		public fecherFoundation.FCFrame fraDeptDiv;
		public fecherFoundation.FCCheckBox chkDeptDiv;
		public fecherFoundation.FCFrame frmPeriod;
		public fecherFoundation.FCComboBox cboReportPeriod;
		public fecherFoundation.FCFrame frmType;
		public fecherFoundation.FCComboBox cboVacSickTypes;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbPage = new fecherFoundation.FCComboBox();
            this.fraDeptDiv = new fecherFoundation.FCFrame();
            this.chkDeptDiv = new fecherFoundation.FCCheckBox();
            this.frmPeriod = new fecherFoundation.FCFrame();
            this.cboReportPeriod = new fecherFoundation.FCComboBox();
            this.frmType = new fecherFoundation.FCFrame();
            this.cboVacSickTypes = new fecherFoundation.FCComboBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptDiv)).BeginInit();
            this.fraDeptDiv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPeriod)).BeginInit();
            this.frmPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmType)).BeginInit();
            this.frmType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 390);
            this.BottomPanel.Size = new System.Drawing.Size(339, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraDeptDiv);
            this.ClientArea.Controls.Add(this.frmPeriod);
            this.ClientArea.Controls.Add(this.frmType);
            this.ClientArea.Controls.Add(this.cmbPage);
            this.ClientArea.Size = new System.Drawing.Size(339, 330);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(339, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(220, 30);
            this.HeaderText.Text = "Report Parameters";
            // 
            // cmbPage
            // 
            this.cmbPage.AutoSize = false;
            this.cmbPage.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPage.FormattingEnabled = true;
            this.cmbPage.Items.AddRange(new object[] {
            "Print on same page",
            "Print on separate page"});
            this.cmbPage.Location = new System.Drawing.Point(30, 30);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(281, 40);
            this.cmbPage.TabIndex = 8;
            // 
            // fraDeptDiv
            // 
            this.fraDeptDiv.Controls.Add(this.chkDeptDiv);
            this.fraDeptDiv.Location = new System.Drawing.Point(30, 250);
            this.fraDeptDiv.Name = "fraDeptDiv";
            this.fraDeptDiv.Size = new System.Drawing.Size(281, 72);
            this.fraDeptDiv.TabIndex = 7;
            this.fraDeptDiv.Text = "Department / Division";
            this.fraDeptDiv.Visible = false;
            // 
            // chkDeptDiv
            // 
            this.chkDeptDiv.Location = new System.Drawing.Point(20, 30);
            this.chkDeptDiv.Name = "chkDeptDiv";
            this.chkDeptDiv.Size = new System.Drawing.Size(248, 27);
            this.chkDeptDiv.TabIndex = 8;
            this.chkDeptDiv.Text = "New page every new Dept/Div";
            // 
            // frmPeriod
            // 
            this.frmPeriod.Controls.Add(this.cboReportPeriod);
            this.frmPeriod.Location = new System.Drawing.Point(30, 140);
            this.frmPeriod.Name = "frmPeriod";
            this.frmPeriod.Size = new System.Drawing.Size(281, 90);
            this.frmPeriod.TabIndex = 4;
            this.frmPeriod.Text = "Report Period";
            // 
            // cboReportPeriod
            // 
            this.cboReportPeriod.AutoSize = false;
            this.cboReportPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cboReportPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReportPeriod.FormattingEnabled = true;
            this.cboReportPeriod.Items.AddRange(new object[] {
            "Weekly",
            "Monthly",
            "Quarterly",
            "Yearly"});
            this.cboReportPeriod.Location = new System.Drawing.Point(20, 30);
            this.cboReportPeriod.Name = "cboReportPeriod";
            this.cboReportPeriod.Size = new System.Drawing.Size(241, 40);
            this.cboReportPeriod.TabIndex = 5;
            // 
            // frmType
            // 
            this.frmType.Controls.Add(this.cboVacSickTypes);
            this.frmType.Location = new System.Drawing.Point(30, 30);
            this.frmType.Name = "frmType";
            this.frmType.Size = new System.Drawing.Size(281, 90);
            this.frmType.TabIndex = 3;
            this.frmType.Text = "Report Type";
            // 
            // cboVacSickTypes
            // 
            this.cboVacSickTypes.AutoSize = false;
            this.cboVacSickTypes.BackColor = System.Drawing.SystemColors.Window;
            this.cboVacSickTypes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboVacSickTypes.FormattingEnabled = true;
            this.cboVacSickTypes.Location = new System.Drawing.Point(20, 30);
            this.cboVacSickTypes.Name = "cboVacSickTypes";
            this.cboVacSickTypes.Size = new System.Drawing.Size(241, 40);
            this.cboVacSickTypes.TabIndex = 6;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Process";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(118, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // frmPrintParameters
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(339, 498);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrintParameters";
            this.Text = "Report Parameters";
            this.Load += new System.EventHandler(this.frmPrintParameters_Load);
            this.Activated += new System.EventHandler(this.frmPrintParameters_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintParameters_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintParameters_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptDiv)).EndInit();
            this.fraDeptDiv.ResumeLayout(false);
            this.fraDeptDiv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPeriod)).EndInit();
            this.frmPeriod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frmType)).EndInit();
            this.frmType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}