﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMMAUnemploymentReport.
	/// </summary>
	partial class rptMMAUnemploymentReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMMAUnemploymentReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldReportNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldQuarter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPreparer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTelephoneNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFederalID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStateID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMemberCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeasonalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFirst = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSecond = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblThird = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSeasonal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldNature = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSocialSecurity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFem = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonseasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotalFemOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalFemTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalFemThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalAllOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalAllTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalAllThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalNonSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotalSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.lblSummaryFirst = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummarySecond = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryThird = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.lblPageSummary1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageSummary2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageSummary3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageSummary4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPageTotalFemOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalFemTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalFemThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalAllOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalAllTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalAllThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalNonSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPageTotalSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReportNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuarter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreparer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTelephoneNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMemberCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSecond)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThird)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSocialSecurity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonseasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalFemOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalFemTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalFemThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalAllOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalAllTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalAllThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalNonSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummarySecond)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryThird)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalFemOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalFemTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalFemThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalAllOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalAllTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalAllThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalNonSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldNature,
				this.fldSocialSecurity,
				this.fldFirst,
				this.fldMI,
				this.fldLast,
				this.fldFem,
				this.fldOne,
				this.fldTwo,
				this.fldThree,
				this.fldNonseasonal,
				this.fldSeasonal,
				this.Line15,
				this.Line16,
				this.Line17,
				this.Line18,
				this.Line19,
				this.Line20,
				this.Line21,
				this.Line22,
				this.Line28,
				this.Line29
			});
			this.Detail.Height = 0.2916667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label13,
				this.Line1,
				this.Label14,
				this.fldEmployer,
				this.Label15,
				this.fldReportNumber,
				this.Label16,
				this.fldQuarter,
				this.Label17,
				this.fldPreparer,
				this.Label18,
				this.fldTelephoneNumber,
				this.Label19,
				this.fldFederalID,
				this.Label20,
				this.fldStateID,
				this.Label21,
				this.fldMemberCode,
				this.Label22,
				this.fldSeasonalCode,
				this.Shape1,
				this.Label23,
				this.Shape2,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label36,
				this.Label37,
				this.Label38,
				this.Label39,
				this.Label40,
				this.Label41,
				this.lblFirst,
				this.lblSecond,
				this.lblThird,
				this.Label45,
				this.Label46,
				this.lblSeasonal,
				this.Label48,
				this.Label58,
				this.Line4,
				this.Line5,
				this.Line6,
				this.Label4,
				this.Label3,
				this.Line3,
				this.Field1,
				this.Line7,
				this.Line8,
				this.Line9,
				this.Line10,
				this.Line11,
				this.Line12,
				this.Line13,
				this.Line14,
				this.Line26,
				this.Line27
			});
			this.PageHeader.Height = 2.385417F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			//
			// 
			this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape5,
				this.Shape4,
				this.Shape3,
				this.lblPageSummary1,
				this.lblPageSummary2,
				this.lblPageSummary3,
				this.lblPageSummary4,
				this.fldPageTotalFemOne,
				this.fldPageTotalFemTwo,
				this.fldPageTotalFemThree,
				this.fldPageTotalAllOne,
				this.fldPageTotalAllTwo,
				this.fldPageTotalAllThree,
				this.fldPageTotalNonSeasonal,
				this.fldPageTotalSeasonal,
				this.Line23,
				this.Line24,
				this.Line25,
				this.Label59
			});
			this.PageFooter.Height = 0.9583333F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label54,
				this.Label55,
				this.Label56,
				this.Label57,
				this.fldGrandTotalFemOne,
				this.fldGrandTotalFemTwo,
				this.fldGrandTotalFemThree,
				this.fldGrandTotalAllOne,
				this.fldGrandTotalAllTwo,
				this.fldGrandTotalAllThree,
				this.fldGrandTotalNonSeasonal,
				this.fldGrandTotalSeasonal,
				this.lblSummary,
				this.Label60,
				this.Line30,
				this.Line31,
				this.Line32,
				this.Shape6,
				this.Line33,
				this.Shape7,
				this.lblSummaryFirst,
				this.lblSummarySecond,
				this.lblSummaryThird
			});
			this.GroupFooter1.Height = 0.9895833F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label1.Text = "Maine Municipal Association Unemployment Compensation Fund";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.71875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label9.Text = "Nature of ";
			this.Label9.Top = 2.03125F;
			this.Label9.Width = 0.625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.6875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "Social";
			this.Label10.Top = 2.03125F;
			this.Label10.Width = 0.96875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.03125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label11.Text = "Employee\'s Name";
			this.Label11.Top = 1.90625F;
			this.Label11.Width = 1.59375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.34375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label13.Text = "Fem";
			this.Label13.Top = 2.1875F;
			this.Label13.Width = 0.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 2.375F;
			this.Line1.Width = 7.71875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.71875F;
			this.Line1.Y1 = 2.375F;
			this.Line1.Y2 = 2.375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label14.Text = "Employer:";
			this.Label14.Top = 0.4375F;
			this.Label14.Width = 0.5625F;
			// 
			// fldEmployer
			// 
			this.fldEmployer.Height = 0.19F;
			this.fldEmployer.Left = 0.625F;
			this.fldEmployer.Name = "fldEmployer";
			this.fldEmployer.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.fldEmployer.Text = "Field1";
			this.fldEmployer.Top = 0.4375F;
			this.fldEmployer.Width = 2.375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label15.Text = "Report No.";
			this.Label15.Top = 0.625F;
			this.Label15.Width = 0.8125F;
			// 
			// fldReportNumber
			// 
			this.fldReportNumber.Height = 0.19F;
			this.fldReportNumber.Left = 0.90625F;
			this.fldReportNumber.Name = "fldReportNumber";
			this.fldReportNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldReportNumber.Text = "Field1";
			this.fldReportNumber.Top = 0.625F;
			this.fldReportNumber.Width = 2.15625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label16.Text = "Quarter";
			this.Label16.Top = 0.78125F;
			this.Label16.Width = 0.8125F;
			// 
			// fldQuarter
			// 
			this.fldQuarter.Height = 0.19F;
			this.fldQuarter.Left = 0.90625F;
			this.fldQuarter.Name = "fldQuarter";
			this.fldQuarter.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldQuarter.Text = "Field2";
			this.fldQuarter.Top = 0.78125F;
			this.fldQuarter.Width = 2.15625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label17.Text = "Preparer";
			this.Label17.Top = 0.9375F;
			this.Label17.Width = 0.8125F;
			// 
			// fldPreparer
			// 
			this.fldPreparer.Height = 0.19F;
			this.fldPreparer.Left = 0.90625F;
			this.fldPreparer.Name = "fldPreparer";
			this.fldPreparer.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldPreparer.Text = "Field3";
			this.fldPreparer.Top = 0.9375F;
			this.fldPreparer.Width = 2.15625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label18.Text = "Telephone No.";
			this.Label18.Top = 1.09375F;
			this.Label18.Width = 0.8125F;
			// 
			// fldTelephoneNumber
			// 
			this.fldTelephoneNumber.Height = 0.19F;
			this.fldTelephoneNumber.Left = 0.90625F;
			this.fldTelephoneNumber.Name = "fldTelephoneNumber";
			this.fldTelephoneNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldTelephoneNumber.Text = "Field4";
			this.fldTelephoneNumber.Top = 1.09375F;
			this.fldTelephoneNumber.Width = 2.15625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.03125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label19.Text = "Federal ID";
			this.Label19.Top = 1.40625F;
			this.Label19.Width = 0.75F;
			// 
			// fldFederalID
			// 
			this.fldFederalID.Height = 0.19F;
			this.fldFederalID.Left = 2.78125F;
			this.fldFederalID.Name = "fldFederalID";
			this.fldFederalID.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldFederalID.Text = "Field5";
			this.fldFederalID.Top = 1.40625F;
			this.fldFederalID.Width = 1.0625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.03125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label20.Text = "State ID";
			this.Label20.Top = 1.25F;
			this.Label20.Width = 0.75F;
			// 
			// fldStateID
			// 
			this.fldStateID.Height = 0.19F;
			this.fldStateID.Left = 2.78125F;
			this.fldStateID.Name = "fldStateID";
			this.fldStateID.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldStateID.Text = "Field6";
			this.fldStateID.Top = 1.25F;
			this.fldStateID.Width = 1.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label21.Text = "Member Code";
			this.Label21.Top = 1.25F;
			this.Label21.Width = 0.875F;
			// 
			// fldMemberCode
			// 
			this.fldMemberCode.Height = 0.19F;
			this.fldMemberCode.Left = 0.90625F;
			this.fldMemberCode.Name = "fldMemberCode";
			this.fldMemberCode.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldMemberCode.Text = "Field7";
			this.fldMemberCode.Top = 1.25F;
			this.fldMemberCode.Width = 1.0625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label22.Text = "Seasonal Code";
			this.Label22.Top = 1.40625F;
			this.Label22.Width = 0.90625F;
			// 
			// fldSeasonalCode
			// 
			this.fldSeasonalCode.Height = 0.19F;
			this.fldSeasonalCode.Left = 0.90625F;
			this.fldSeasonalCode.Name = "fldSeasonalCode";
			this.fldSeasonalCode.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldSeasonalCode.Text = "Field8";
			this.fldSeasonalCode.Top = 1.40625F;
			this.fldSeasonalCode.Width = 1.0625F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.71875F;
			this.Shape1.Left = 3.875F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.6875F;
			this.Shape1.Width = 2.34375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 5.21875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label23.Text = "For MMA Use ONLY";
			this.Label23.Top = 0.53125F;
			this.Label23.Width = 1.25F;
			// 
			// Shape2
			// 
			this.Shape2.Height = 1F;
			this.Shape2.Left = 6.21875F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 0.6875F;
			this.Shape2.Width = 1.375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.90625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label24.Text = "INITIAL RECEIPT";
			this.Label24.Top = 0.71875F;
			this.Label24.Width = 1.40625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 3.90625F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label25.Text = "Received @ MMA";
			this.Label25.Top = 0.875F;
			this.Label25.Width = 1.0625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.90625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label26.Text = "Partial Entry";
			this.Label26.Top = 1.03125F;
			this.Label26.Width = 1.0625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 3.90625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label27.Text = "Entry Complete";
			this.Label27.Top = 1.1875F;
			this.Label27.Width = 1.0625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 5.03125F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label28.Text = "___/___/______   ___";
			this.Label28.Top = 0.875F;
			this.Label28.Width = 1.15625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.19F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.03125F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label29.Text = "___/___/______   ___";
			this.Label29.Top = 1.03125F;
			this.Label29.Width = 1.15625F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.19F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.03125F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label30.Text = "___/___/______   ___";
			this.Label30.Top = 1.1875F;
			this.Label30.Width = 1.15625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 6.28125F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label31.Text = "INCOMPLETE";
			this.Label31.Top = 0.71875F;
			this.Label31.Width = 1F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 6.28125F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label32.Text = "Check needs:";
			this.Label32.Top = 0.875F;
			this.Label32.Width = 1F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 6.25F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label33.Text = "__ Soc Sec No";
			this.Label33.Top = 1.03125F;
			this.Label33.Width = 1.28125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 6.25F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label34.Text = "__ Nature Bus";
			this.Label34.Top = 1.1875F;
			this.Label34.Width = 1.28125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.19F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 6.25F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label35.Text = "__ Wage(s)";
			this.Label35.Top = 1.34375F;
			this.Label35.Width = 1.28125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.19F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 6.25F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label36.Text = "__ Other_________";
			this.Label36.Top = 1.5F;
			this.Label36.Width = 1.28125F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.19F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 1.6875F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.Label37.Text = "First";
			this.Label37.Top = 2.1875F;
			this.Label37.Width = 0.84375F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.19F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0.6875F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label38.Text = "Security No";
			this.Label38.Top = 2.1875F;
			this.Label38.Width = 0.96875F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.19F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label39.Text = "Business";
			this.Label39.Top = 2.1875F;
			this.Label39.Width = 0.625F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.19F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 2.53125F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label40.Text = "MI";
			this.Label40.Top = 2.1875F;
			this.Label40.Width = 0.25F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.19F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 2.8125F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.Label41.Text = "Last";
			this.Label41.Top = 2.1875F;
			this.Label41.Width = 1.53125F;
			// 
			// lblFirst
			// 
			this.lblFirst.Height = 0.19F;
			this.lblFirst.HyperLink = null;
			this.lblFirst.Left = 4.75F;
			this.lblFirst.Name = "lblFirst";
			this.lblFirst.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblFirst.Text = "1st";
			this.lblFirst.Top = 2.1875F;
			this.lblFirst.Width = 0.375F;
			// 
			// lblSecond
			// 
			this.lblSecond.Height = 0.19F;
			this.lblSecond.HyperLink = null;
			this.lblSecond.Left = 5.125F;
			this.lblSecond.Name = "lblSecond";
			this.lblSecond.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblSecond.Text = "2nd";
			this.lblSecond.Top = 2.1875F;
			this.lblSecond.Width = 0.375F;
			// 
			// lblThird
			// 
			this.lblThird.Height = 0.19F;
			this.lblThird.HyperLink = null;
			this.lblThird.Left = 5.5F;
			this.lblThird.Name = "lblThird";
			this.lblThird.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblThird.Text = "3rd";
			this.lblThird.Top = 2.1875F;
			this.lblThird.Width = 0.375F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.40625F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 4.75F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.Label45.Text = "Check if employed in pay period incl 12th of the month";
			this.Label45.Top = 1.71875F;
			this.Label45.Width = 1.15625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.19F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 5.9375F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label46.Text = "Nonseasonal";
			this.Label46.Top = 2.1875F;
			this.Label46.Width = 0.8125F;
			// 
			// lblSeasonal
			// 
			this.lblSeasonal.Height = 0.19F;
			this.lblSeasonal.HyperLink = null;
			this.lblSeasonal.Left = 6.84375F;
			this.lblSeasonal.Name = "lblSeasonal";
			this.lblSeasonal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblSeasonal.Text = "Seasonal";
			this.lblSeasonal.Top = 2.1875F;
			this.lblSeasonal.Width = 0.84375F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.19F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 6.34375F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Label48.Text = "Gross Wages";
			this.Label48.Top = 1.90625F;
			this.Label48.Width = 0.8125F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 0.125F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-align: left; ddo-" + "char-set: 1";
			this.Label58.Text = "Employer\'s Unemployment Compensation Wage and Recapitulation Report";
			this.Label58.Top = 0.1875F;
			this.Label58.Width = 5.84375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0.625F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.71875F;
			this.Line4.Width = 0F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 0F;
			this.Line4.Y1 = 2.34375F;
			this.Line4.Y2 = 1.71875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.71875F;
			this.Line5.Width = 7.71875F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.71875F;
			this.Line5.Y1 = 1.71875F;
			this.Line5.Y2 = 1.71875F;
			// 
			// Line6
			// 
			this.Line6.Height = 0.625F;
			this.Line6.Left = 7.71875F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.71875F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 7.71875F;
			this.Line6.X2 = 7.71875F;
			this.Line6.Y1 = 2.34375F;
			this.Line6.Y2 = 1.71875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.28125F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.75F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0.125F;
			this.Label3.Width = 0.84375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.03125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.125F;
			this.Line3.Width = 7.71875F;
			this.Line3.X1 = 0.03125F;
			this.Line3.X2 = 7.75F;
			this.Line3.Y1 = 0.125F;
			this.Line3.Y2 = 0.125F;
			// 
			// Field1
			// 
			this.Field1.DataField = "MaxPageCount";
			this.Field1.Height = 0.19F;
			this.Field1.Left = 7.59375F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Field1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
			this.Field1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.Field1.Text = "MaxPageCounter";
			this.Field1.Top = 0.125F;
			this.Field1.Width = 0.15625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0.65625F;
			this.Line7.Left = 0.65625F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 1.71875F;
			this.Line7.Width = 0F;
			this.Line7.X1 = 0.65625F;
			this.Line7.X2 = 0.65625F;
			this.Line7.Y1 = 2.375F;
			this.Line7.Y2 = 1.71875F;
			// 
			// Line8
			// 
			this.Line8.Height = 0.65625F;
			this.Line8.Left = 1.65625F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.71875F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 1.65625F;
			this.Line8.X2 = 1.65625F;
			this.Line8.Y1 = 2.375F;
			this.Line8.Y2 = 1.71875F;
			// 
			// Line9
			// 
			this.Line9.Height = 0.21875F;
			this.Line9.Left = 2.53125F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 2.15625F;
			this.Line9.Width = 0F;
			this.Line9.X1 = 2.53125F;
			this.Line9.X2 = 2.53125F;
			this.Line9.Y1 = 2.375F;
			this.Line9.Y2 = 2.15625F;
			// 
			// Line10
			// 
			this.Line10.Height = 0.21875F;
			this.Line10.Left = 2.78125F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 2.15625F;
			this.Line10.Width = 0F;
			this.Line10.X1 = 2.78125F;
			this.Line10.X2 = 2.78125F;
			this.Line10.Y1 = 2.375F;
			this.Line10.Y2 = 2.15625F;
			// 
			// Line11
			// 
			this.Line11.Height = 0.65625F;
			this.Line11.Left = 4.34375F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 1.71875F;
			this.Line11.Width = 0F;
			this.Line11.X1 = 4.34375F;
			this.Line11.X2 = 4.34375F;
			this.Line11.Y1 = 2.375F;
			this.Line11.Y2 = 1.71875F;
			// 
			// Line12
			// 
			this.Line12.Height = 0.65625F;
			this.Line12.Left = 4.71875F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 1.71875F;
			this.Line12.Width = 0F;
			this.Line12.X1 = 4.71875F;
			this.Line12.X2 = 4.71875F;
			this.Line12.Y1 = 2.375F;
			this.Line12.Y2 = 1.71875F;
			// 
			// Line13
			// 
			this.Line13.Height = 0.65625F;
			this.Line13.Left = 5.90625F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 1.71875F;
			this.Line13.Width = 0F;
			this.Line13.X1 = 5.90625F;
			this.Line13.X2 = 5.90625F;
			this.Line13.Y1 = 2.375F;
			this.Line13.Y2 = 1.71875F;
			// 
			// Line14
			// 
			this.Line14.Height = 0.21875F;
			this.Line14.Left = 6.78125F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 2.15625F;
			this.Line14.Width = 0F;
			this.Line14.X1 = 6.78125F;
			this.Line14.X2 = 6.78125F;
			this.Line14.Y1 = 2.375F;
			this.Line14.Y2 = 2.15625F;
			// 
			// Line26
			// 
			this.Line26.Height = 0.21875F;
			this.Line26.Left = 5.125F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 2.15625F;
			this.Line26.Width = 0F;
			this.Line26.X1 = 5.125F;
			this.Line26.X2 = 5.125F;
			this.Line26.Y1 = 2.375F;
			this.Line26.Y2 = 2.15625F;
			// 
			// Line27
			// 
			this.Line27.Height = 0.21875F;
			this.Line27.Left = 5.5F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 2.15625F;
			this.Line27.Width = 0F;
			this.Line27.X1 = 5.5F;
			this.Line27.X2 = 5.5F;
			this.Line27.Y1 = 2.375F;
			this.Line27.Y2 = 2.15625F;
			// 
			// fldNature
			// 
			this.fldNature.Height = 0.1875F;
			this.fldNature.Left = 0F;
			this.fldNature.Name = "fldNature";
			this.fldNature.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldNature.Text = "Field1";
			this.fldNature.Top = 0.0625F;
			this.fldNature.Width = 0.625F;
			// 
			// fldSocialSecurity
			// 
			this.fldSocialSecurity.Height = 0.1875F;
			this.fldSocialSecurity.Left = 0.6875F;
			this.fldSocialSecurity.Name = "fldSocialSecurity";
			this.fldSocialSecurity.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldSocialSecurity.Text = "Field1";
			this.fldSocialSecurity.Top = 0.0625F;
			this.fldSocialSecurity.Width = 0.96875F;
			// 
			// fldFirst
			// 
			this.fldFirst.Height = 0.1875F;
			this.fldFirst.Left = 1.6875F;
			this.fldFirst.Name = "fldFirst";
			this.fldFirst.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldFirst.Text = "Field2";
			this.fldFirst.Top = 0.0625F;
			this.fldFirst.Width = 0.84375F;
			// 
			// fldMI
			// 
			this.fldMI.Height = 0.1875F;
			this.fldMI.Left = 2.53125F;
			this.fldMI.Name = "fldMI";
			this.fldMI.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldMI.Text = "Field2";
			this.fldMI.Top = 0.0625F;
			this.fldMI.Width = 0.25F;
			// 
			// fldLast
			// 
			this.fldLast.Height = 0.1875F;
			this.fldLast.Left = 2.8125F;
			this.fldLast.Name = "fldLast";
			this.fldLast.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldLast.Text = "Field2";
			this.fldLast.Top = 0.0625F;
			this.fldLast.Width = 1.53125F;
			// 
			// fldFem
			// 
			this.fldFem.Height = 0.1875F;
			this.fldFem.Left = 4.34375F;
			this.fldFem.Name = "fldFem";
			this.fldFem.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldFem.Text = "Field2";
			this.fldFem.Top = 0.0625F;
			this.fldFem.Width = 0.375F;
			// 
			// fldOne
			// 
			this.fldOne.Height = 0.1875F;
			this.fldOne.Left = 4.75F;
			this.fldOne.Name = "fldOne";
			this.fldOne.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldOne.Text = "Field2";
			this.fldOne.Top = 0.0625F;
			this.fldOne.Width = 0.375F;
			// 
			// fldTwo
			// 
			this.fldTwo.Height = 0.1875F;
			this.fldTwo.Left = 5.125F;
			this.fldTwo.Name = "fldTwo";
			this.fldTwo.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldTwo.Text = "Field3";
			this.fldTwo.Top = 0.0625F;
			this.fldTwo.Width = 0.375F;
			// 
			// fldThree
			// 
			this.fldThree.Height = 0.1875F;
			this.fldThree.Left = 5.5F;
			this.fldThree.Name = "fldThree";
			this.fldThree.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldThree.Text = "Field4";
			this.fldThree.Top = 0.0625F;
			this.fldThree.Width = 0.375F;
			// 
			// fldNonseasonal
			// 
			this.fldNonseasonal.Height = 0.1875F;
			this.fldNonseasonal.Left = 5.9375F;
			this.fldNonseasonal.Name = "fldNonseasonal";
			this.fldNonseasonal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldNonseasonal.Text = "Field4";
			this.fldNonseasonal.Top = 0.0625F;
			this.fldNonseasonal.Width = 0.8125F;
			// 
			// fldSeasonal
			// 
			this.fldSeasonal.Height = 0.1875F;
			this.fldSeasonal.Left = 6.84375F;
			this.fldSeasonal.Name = "fldSeasonal";
			this.fldSeasonal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSeasonal.Text = "Field4";
			this.fldSeasonal.Top = 0.0625F;
			this.fldSeasonal.Width = 0.84375F;
			// 
			// Line15
			// 
			this.Line15.Height = 0.28125F;
			this.Line15.Left = 0.65625F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 0F;
			this.Line15.Width = 0F;
			this.Line15.X1 = 0.65625F;
			this.Line15.X2 = 0.65625F;
			this.Line15.Y1 = 0.28125F;
			this.Line15.Y2 = 0F;
			// 
			// Line16
			// 
			this.Line16.Height = 0.28125F;
			this.Line16.Left = 1.65625F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 0F;
			this.Line16.Width = 0F;
			this.Line16.X1 = 1.65625F;
			this.Line16.X2 = 1.65625F;
			this.Line16.Y1 = 0.28125F;
			this.Line16.Y2 = 0F;
			// 
			// Line17
			// 
			this.Line17.Height = 0.28125F;
			this.Line17.Left = 2.53125F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 0F;
			this.Line17.Width = 0F;
			this.Line17.X1 = 2.53125F;
			this.Line17.X2 = 2.53125F;
			this.Line17.Y1 = 0.28125F;
			this.Line17.Y2 = 0F;
			// 
			// Line18
			// 
			this.Line18.Height = 0.28125F;
			this.Line18.Left = 2.78125F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 0F;
			this.Line18.Width = 0F;
			this.Line18.X1 = 2.78125F;
			this.Line18.X2 = 2.78125F;
			this.Line18.Y1 = 0.28125F;
			this.Line18.Y2 = 0F;
			// 
			// Line19
			// 
			this.Line19.Height = 0.28125F;
			this.Line19.Left = 4.34375F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 0F;
			this.Line19.Width = 0F;
			this.Line19.X1 = 4.34375F;
			this.Line19.X2 = 4.34375F;
			this.Line19.Y1 = 0.28125F;
			this.Line19.Y2 = 0F;
			// 
			// Line20
			// 
			this.Line20.Height = 0.28125F;
			this.Line20.Left = 4.71875F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 0F;
			this.Line20.Width = 0F;
			this.Line20.X1 = 4.71875F;
			this.Line20.X2 = 4.71875F;
			this.Line20.Y1 = 0.28125F;
			this.Line20.Y2 = 0F;
			// 
			// Line21
			// 
			this.Line21.Height = 0.28125F;
			this.Line21.Left = 5.90625F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 0F;
			this.Line21.Width = 0F;
			this.Line21.X1 = 5.90625F;
			this.Line21.X2 = 5.90625F;
			this.Line21.Y1 = 0.28125F;
			this.Line21.Y2 = 0F;
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 0F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 0.28125F;
			this.Line22.Width = 7.71875F;
			this.Line22.X1 = 0F;
			this.Line22.X2 = 7.71875F;
			this.Line22.Y1 = 0.28125F;
			this.Line22.Y2 = 0.28125F;
			// 
			// Line28
			// 
			this.Line28.Height = 0.28125F;
			this.Line28.Left = 5.125F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 0F;
			this.Line28.Width = 0F;
			this.Line28.X1 = 5.125F;
			this.Line28.X2 = 5.125F;
			this.Line28.Y1 = 0.28125F;
			this.Line28.Y2 = 0F;
			// 
			// Line29
			// 
			this.Line29.Height = 0.28125F;
			this.Line29.Left = 5.5F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 0F;
			this.Line29.Width = 0F;
			this.Line29.X1 = 5.5F;
			this.Line29.X2 = 5.5F;
			this.Line29.Y1 = 0.28125F;
			this.Line29.Y2 = 0F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.19F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 0.21875F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label54.Text = "# FEMALES CHECKED PER MONTH";
			this.Label54.Top = 0.53125F;
			this.Label54.Width = 2.15625F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.19F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 0.21875F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label55.Text = "# EMPLOYEES CHECKED PER MONTH";
			this.Label55.Top = 0.75F;
			this.Label55.Width = 2.15625F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.19F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 4.46875F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label56.Text = "NONSEASONAL WAGES";
			this.Label56.Top = 0.53125F;
			this.Label56.Width = 1.5F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.19F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 4.46875F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label57.Text = "SEASONAL WAGES";
			this.Label57.Top = 0.75F;
			this.Label57.Width = 1.5F;
			// 
			// fldGrandTotalFemOne
			// 
			this.fldGrandTotalFemOne.Height = 0.19F;
			this.fldGrandTotalFemOne.Left = 2.4375F;
			this.fldGrandTotalFemOne.Name = "fldGrandTotalFemOne";
			this.fldGrandTotalFemOne.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldGrandTotalFemOne.Text = "Field2";
			this.fldGrandTotalFemOne.Top = 0.53125F;
			this.fldGrandTotalFemOne.Width = 0.375F;
			// 
			// fldGrandTotalFemTwo
			// 
			this.fldGrandTotalFemTwo.Height = 0.19F;
			this.fldGrandTotalFemTwo.Left = 2.8125F;
			this.fldGrandTotalFemTwo.Name = "fldGrandTotalFemTwo";
			this.fldGrandTotalFemTwo.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldGrandTotalFemTwo.Text = "Field3";
			this.fldGrandTotalFemTwo.Top = 0.53125F;
			this.fldGrandTotalFemTwo.Width = 0.375F;
			// 
			// fldGrandTotalFemThree
			// 
			this.fldGrandTotalFemThree.Height = 0.19F;
			this.fldGrandTotalFemThree.Left = 3.1875F;
			this.fldGrandTotalFemThree.Name = "fldGrandTotalFemThree";
			this.fldGrandTotalFemThree.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldGrandTotalFemThree.Text = "Field4";
			this.fldGrandTotalFemThree.Top = 0.53125F;
			this.fldGrandTotalFemThree.Width = 0.375F;
			// 
			// fldGrandTotalAllOne
			// 
			this.fldGrandTotalAllOne.Height = 0.19F;
			this.fldGrandTotalAllOne.Left = 2.4375F;
			this.fldGrandTotalAllOne.Name = "fldGrandTotalAllOne";
			this.fldGrandTotalAllOne.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldGrandTotalAllOne.Text = "Field5";
			this.fldGrandTotalAllOne.Top = 0.75F;
			this.fldGrandTotalAllOne.Width = 0.375F;
			// 
			// fldGrandTotalAllTwo
			// 
			this.fldGrandTotalAllTwo.Height = 0.19F;
			this.fldGrandTotalAllTwo.Left = 2.8125F;
			this.fldGrandTotalAllTwo.Name = "fldGrandTotalAllTwo";
			this.fldGrandTotalAllTwo.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldGrandTotalAllTwo.Text = "Field6";
			this.fldGrandTotalAllTwo.Top = 0.75F;
			this.fldGrandTotalAllTwo.Width = 0.375F;
			// 
			// fldGrandTotalAllThree
			// 
			this.fldGrandTotalAllThree.Height = 0.19F;
			this.fldGrandTotalAllThree.Left = 3.1875F;
			this.fldGrandTotalAllThree.Name = "fldGrandTotalAllThree";
			this.fldGrandTotalAllThree.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldGrandTotalAllThree.Text = "Field7";
			this.fldGrandTotalAllThree.Top = 0.75F;
			this.fldGrandTotalAllThree.Width = 0.375F;
			// 
			// fldGrandTotalNonSeasonal
			// 
			this.fldGrandTotalNonSeasonal.Height = 0.19F;
			this.fldGrandTotalNonSeasonal.Left = 6F;
			this.fldGrandTotalNonSeasonal.Name = "fldGrandTotalNonSeasonal";
			this.fldGrandTotalNonSeasonal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldGrandTotalNonSeasonal.Text = null;
			this.fldGrandTotalNonSeasonal.Top = 0.53125F;
			this.fldGrandTotalNonSeasonal.Width = 0.8125F;
			// 
			// fldGrandTotalSeasonal
			// 
			this.fldGrandTotalSeasonal.Height = 0.19F;
			this.fldGrandTotalSeasonal.Left = 6F;
			this.fldGrandTotalSeasonal.Name = "fldGrandTotalSeasonal";
			this.fldGrandTotalSeasonal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldGrandTotalSeasonal.Text = "Field4";
			this.fldGrandTotalSeasonal.Top = 0.75F;
			this.fldGrandTotalSeasonal.Width = 0.8125F;
			// 
			// lblSummary
			// 
			this.lblSummary.Height = 0.19F;
			this.lblSummary.HyperLink = null;
			this.lblSummary.Left = 0.21875F;
			this.lblSummary.Name = "lblSummary";
			this.lblSummary.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblSummary.Text = null;
			this.lblSummary.Top = 0.09375F;
			this.lblSummary.Width = 3.65625F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.19F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 0.21875F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label60.Text = "GRAND TOTALS:";
			this.Label60.Top = 0.34375F;
			this.Label60.Width = 1.15625F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 2.4375F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 0.71875F;
			this.Line30.Width = 1.125F;
			this.Line30.X1 = 2.4375F;
			this.Line30.X2 = 3.5625F;
			this.Line30.Y1 = 0.71875F;
			this.Line30.Y2 = 0.71875F;
			// 
			// Line31
			// 
			this.Line31.Height = 0.375F;
			this.Line31.Left = 2.8125F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 0.53125F;
			this.Line31.Width = 0F;
			this.Line31.X1 = 2.8125F;
			this.Line31.X2 = 2.8125F;
			this.Line31.Y1 = 0.53125F;
			this.Line31.Y2 = 0.90625F;
			// 
			// Line32
			// 
			this.Line32.Height = 0.375F;
			this.Line32.Left = 3.1875F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 0.53125F;
			this.Line32.Width = 0F;
			this.Line32.X1 = 3.1875F;
			this.Line32.X2 = 3.1875F;
			this.Line32.Y1 = 0.53125F;
			this.Line32.Y2 = 0.90625F;
			// 
			// Shape6
			// 
			this.Shape6.Height = 0.375F;
			this.Shape6.Left = 2.4375F;
			this.Shape6.Name = "Shape6";
			this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape6.Top = 0.53125F;
			this.Shape6.Width = 1.125F;
			// 
			// Line33
			// 
			this.Line33.Height = 0F;
			this.Line33.Left = 6F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 0.71875F;
			this.Line33.Width = 0.8125F;
			this.Line33.X1 = 6F;
			this.Line33.X2 = 6.8125F;
			this.Line33.Y1 = 0.71875F;
			this.Line33.Y2 = 0.71875F;
			// 
			// Shape7
			// 
			this.Shape7.Height = 0.375F;
			this.Shape7.Left = 6F;
			this.Shape7.Name = "Shape7";
			this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape7.Top = 0.53125F;
			this.Shape7.Width = 0.8125F;
			// 
			// lblSummaryFirst
			// 
			this.lblSummaryFirst.Height = 0.19F;
			this.lblSummaryFirst.HyperLink = null;
			this.lblSummaryFirst.Left = 2.4375F;
			this.lblSummaryFirst.Name = "lblSummaryFirst";
			this.lblSummaryFirst.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblSummaryFirst.Text = "1st";
			this.lblSummaryFirst.Top = 0.375F;
			this.lblSummaryFirst.Width = 0.375F;
			// 
			// lblSummarySecond
			// 
			this.lblSummarySecond.Height = 0.19F;
			this.lblSummarySecond.HyperLink = null;
			this.lblSummarySecond.Left = 2.8125F;
			this.lblSummarySecond.Name = "lblSummarySecond";
			this.lblSummarySecond.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblSummarySecond.Text = "2nd";
			this.lblSummarySecond.Top = 0.375F;
			this.lblSummarySecond.Width = 0.375F;
			// 
			// lblSummaryThird
			// 
			this.lblSummaryThird.Height = 0.19F;
			this.lblSummaryThird.HyperLink = null;
			this.lblSummaryThird.Left = 3.1875F;
			this.lblSummaryThird.Name = "lblSummaryThird";
			this.lblSummaryThird.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.lblSummaryThird.Text = "3rd";
			this.lblSummaryThird.Top = 0.375F;
			this.lblSummaryThird.Width = 0.375F;
			// 
			// Shape5
			// 
			this.Shape5.Height = 0.19F;
			this.Shape5.Left = 6.875F;
			this.Shape5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 0.5F;
			this.Shape5.Width = 0.8125F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.19F;
			this.Shape4.Left = 5.96875F;
			this.Shape4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 0.34375F;
			this.Shape4.Width = 0.84375F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.3125F;
			this.Shape3.Left = 4.71875F;
			this.Shape3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.03125F;
			this.Shape3.Width = 1.125F;
			// 
			// lblPageSummary1
			// 
			this.lblPageSummary1.Height = 0.19F;
			this.lblPageSummary1.HyperLink = null;
			this.lblPageSummary1.Left = 1.5F;
			this.lblPageSummary1.Name = "lblPageSummary1";
			this.lblPageSummary1.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblPageSummary1.Text = "Total # of FEMALES checked each month this page";
			this.lblPageSummary1.Top = 0.1875F;
			this.lblPageSummary1.Width = 3.1875F;
			// 
			// lblPageSummary2
			// 
			this.lblPageSummary2.Height = 0.19F;
			this.lblPageSummary2.HyperLink = null;
			this.lblPageSummary2.Left = 1.5F;
			this.lblPageSummary2.Name = "lblPageSummary2";
			this.lblPageSummary2.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblPageSummary2.Text = "Total # of EMPLOYEES checked each month this page";
			this.lblPageSummary2.Top = 0.03125F;
			this.lblPageSummary2.Width = 3.1875F;
			// 
			// lblPageSummary3
			// 
			this.lblPageSummary3.Height = 0.19F;
			this.lblPageSummary3.HyperLink = null;
			this.lblPageSummary3.Left = 1.5F;
			this.lblPageSummary3.Name = "lblPageSummary3";
			this.lblPageSummary3.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblPageSummary3.Text = "Total NONSEASONAL wages this page:";
			this.lblPageSummary3.Top = 0.34375F;
			this.lblPageSummary3.Width = 3.1875F;
			// 
			// lblPageSummary4
			// 
			this.lblPageSummary4.Height = 0.19F;
			this.lblPageSummary4.HyperLink = null;
			this.lblPageSummary4.Left = 1.5F;
			this.lblPageSummary4.Name = "lblPageSummary4";
			this.lblPageSummary4.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblPageSummary4.Text = "Total SEASONAL wages this page:";
			this.lblPageSummary4.Top = 0.5F;
			this.lblPageSummary4.Width = 3.1875F;
			// 
			// fldPageTotalFemOne
			// 
			this.fldPageTotalFemOne.Height = 0.19F;
			this.fldPageTotalFemOne.Left = 4.71875F;
			this.fldPageTotalFemOne.Name = "fldPageTotalFemOne";
			this.fldPageTotalFemOne.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPageTotalFemOne.Text = "Field2";
			this.fldPageTotalFemOne.Top = 0.1875F;
			this.fldPageTotalFemOne.Width = 0.375F;
			// 
			// fldPageTotalFemTwo
			// 
			this.fldPageTotalFemTwo.Height = 0.19F;
			this.fldPageTotalFemTwo.Left = 5.09375F;
			this.fldPageTotalFemTwo.Name = "fldPageTotalFemTwo";
			this.fldPageTotalFemTwo.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPageTotalFemTwo.Text = "Field3";
			this.fldPageTotalFemTwo.Top = 0.1875F;
			this.fldPageTotalFemTwo.Width = 0.375F;
			// 
			// fldPageTotalFemThree
			// 
			this.fldPageTotalFemThree.Height = 0.19F;
			this.fldPageTotalFemThree.Left = 5.46875F;
			this.fldPageTotalFemThree.Name = "fldPageTotalFemThree";
			this.fldPageTotalFemThree.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPageTotalFemThree.Text = "Field4";
			this.fldPageTotalFemThree.Top = 0.1875F;
			this.fldPageTotalFemThree.Width = 0.375F;
			// 
			// fldPageTotalAllOne
			// 
			this.fldPageTotalAllOne.Height = 0.19F;
			this.fldPageTotalAllOne.Left = 4.71875F;
			this.fldPageTotalAllOne.Name = "fldPageTotalAllOne";
			this.fldPageTotalAllOne.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPageTotalAllOne.Text = "Field5";
			this.fldPageTotalAllOne.Top = 0.03125F;
			this.fldPageTotalAllOne.Width = 0.375F;
			// 
			// fldPageTotalAllTwo
			// 
			this.fldPageTotalAllTwo.Height = 0.19F;
			this.fldPageTotalAllTwo.Left = 5.09375F;
			this.fldPageTotalAllTwo.Name = "fldPageTotalAllTwo";
			this.fldPageTotalAllTwo.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPageTotalAllTwo.Text = "Field6";
			this.fldPageTotalAllTwo.Top = 0.03125F;
			this.fldPageTotalAllTwo.Width = 0.375F;
			// 
			// fldPageTotalAllThree
			// 
			this.fldPageTotalAllThree.Height = 0.19F;
			this.fldPageTotalAllThree.Left = 5.46875F;
			this.fldPageTotalAllThree.Name = "fldPageTotalAllThree";
			this.fldPageTotalAllThree.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPageTotalAllThree.Text = "Field7";
			this.fldPageTotalAllThree.Top = 0.03125F;
			this.fldPageTotalAllThree.Width = 0.375F;
			// 
			// fldPageTotalNonSeasonal
			// 
			this.fldPageTotalNonSeasonal.Height = 0.19F;
			this.fldPageTotalNonSeasonal.Left = 6F;
			this.fldPageTotalNonSeasonal.Name = "fldPageTotalNonSeasonal";
			this.fldPageTotalNonSeasonal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldPageTotalNonSeasonal.Text = null;
			this.fldPageTotalNonSeasonal.Top = 0.34375F;
			this.fldPageTotalNonSeasonal.Width = 0.8125F;
			// 
			// fldPageTotalSeasonal
			// 
			this.fldPageTotalSeasonal.Height = 0.19F;
			this.fldPageTotalSeasonal.Left = 6.875F;
			this.fldPageTotalSeasonal.Name = "fldPageTotalSeasonal";
			this.fldPageTotalSeasonal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldPageTotalSeasonal.Text = "Field4";
			this.fldPageTotalSeasonal.Top = 0.5F;
			this.fldPageTotalSeasonal.Width = 0.8125F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 4.71875F;
			this.Line23.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 0.1875F;
			this.Line23.Width = 1.125F;
			this.Line23.X1 = 5.84375F;
			this.Line23.X2 = 4.71875F;
			this.Line23.Y1 = 0.1875F;
			this.Line23.Y2 = 0.1875F;
			// 
			// Line24
			// 
			this.Line24.Height = 0.3125F;
			this.Line24.Left = 5.09375F;
			this.Line24.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 0F;
			this.Line24.Width = 0F;
			this.Line24.X1 = 5.09375F;
			this.Line24.X2 = 5.09375F;
			this.Line24.Y1 = 0.3125F;
			this.Line24.Y2 = 0F;
			// 
			// Line25
			// 
			this.Line25.Height = 0.3125F;
			this.Line25.Left = 5.46875F;
			this.Line25.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 0F;
			this.Line25.Width = 0F;
			this.Line25.X1 = 5.46875F;
			this.Line25.X2 = 5.46875F;
			this.Line25.Y1 = 0.3125F;
			this.Line25.Y2 = 0F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.19F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 0.03125F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label59.Text = "Please return to:    MMA Unemployment Comp. Fund, P.O. Box 9109, Augusta, ME 0433" + "2-9109";
			this.Label59.Top = 0.78125F;
			this.Label59.Width = 5.71875F;
			// 
			// rptMMAUnemploymentReport
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.75F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReportNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuarter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreparer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTelephoneNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMemberCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSecond)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThird)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSocialSecurity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonseasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalFemOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalFemTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalFemThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalAllOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalAllTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalAllThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalNonSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummarySecond)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryThird)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageSummary4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalFemOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalFemTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalFemThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalAllOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalAllTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalAllThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalNonSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPageTotalSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNature;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSocialSecurity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFem;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonseasonal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmployer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReportNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuarter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPreparer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTelephoneNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFederalID;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateID;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMemberCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeasonalCode;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFirst;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSecond;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblThird;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageSummary2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageSummary3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageSummary4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalFemOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalFemTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalFemThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalAllOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalAllTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalAllThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalNonSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPageTotalSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalFemOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalFemTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalFemThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalAllOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalAllTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalAllThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalNonSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryFirst;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummarySecond;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryThird;
	}
}
