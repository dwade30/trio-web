//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNewCustomDistReport.
	/// </summary>
	public partial class rptNewCustomDistReport : BaseSectionReport
	{
		public rptNewCustomDistReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Distribution Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewCustomDistReport InstancePtr
		{
			get
			{
				return (rptNewCustomDistReport)Sys.GetInstance(typeof(rptNewCustomDistReport));
			}
		}

		protected rptNewCustomDistReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
				clsDistPay?.Dispose();
				clsEmp?.Dispose();
                clsReport = null;
                clsDistPay = null;
                clsEmp = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewCustomDistReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolPayCatFirst;
		clsDRWrapper clsReport = new clsDRWrapper();
		clsDRWrapper clsDistPay = new clsDRWrapper();
		clsDRWrapper clsEmp = new clsDRWrapper();
		bool boolFirstTime;
		double dblGroup1Hours;
		double dblGroup1Gross;
		int intGroup1Employees;
		double dblGroup2Hours;
		double dblGroup2Gross;
		int intGroup2Employees;
		double dblTotHours;
		double dblTotGross;
		int intTotEmployees;
		bool boolBySubGroup;
		bool boolOnlySummary;
		bool boolPaidDept;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		public void Init(bool boolPayCat, string strEmpNo = "", DateTime? tempdtStart = null, DateTime? tempdtEnd = null, string strAcctStart = "", string strAcctEnd = "", string strgroup = "", string strDist = "", bool boolSubGroup = true, bool boolDetail = true, bool boolSummary = false, bool boolPayDept = false)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtStart == null)
			{
				tempdtStart = DateTime.FromOADate(0);
			}
			DateTime dtStart = tempdtStart.Value;
			if (tempdtEnd == null)
			{
				tempdtEnd = DateTime.FromOADate(0);
			}
			DateTime dtEnd = tempdtEnd.Value;

			string strSQL = "";
			string strWhere;
			string strOrderBy = "";
			int intDeptDivStart;
			// vbPorter upgrade warning: intDeptDivLength As int	OnWriteFCConvert.ToDouble(
			int intDeptDivLength;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			intDeptDivStart = 3;
			intDeptDivLength = FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1);
			boolPaidDept = boolPayDept;
			boolOnlySummary = (boolSummary && !boolDetail);
			boolPayCatFirst = boolPayCat;
			// strWhere = " where checkvoid = 0 and checknumber > 0 and distributionrecord = 1"
			strWhere = " where checkvoid = 0 and distributionrecord = 1";
			if (strEmpNo != string.Empty)
			{
				strWhere += " and tblcheckdetail.employeenumber = '" + strEmpNo + "' ";
			}
			if (dtStart.ToOADate() != 0)
			{
				if (dtEnd.ToOADate() != 0)
				{
					strWhere += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' ";
				}
				else
				{
					strWhere += " and paydate = '" + FCConvert.ToString(dtStart) + "' ";
				}
			}
			if (strAcctStart != string.Empty)
			{
				if (strAcctEnd != string.Empty)
				{
					strWhere += " and distaccountnumber between '" + strAcctStart + "' and '" + strAcctEnd + "' ";
				}
				else
				{
					strWhere += " and distaccountnumber = '" + strAcctStart + "' ";
				}
			}
			if (boolPaidDept)
			{
				strWhere += " and (left(distaccountnumber & ' ',1) = 'E' or left(distaccountnumber & ' ',1) = 'G') ";
			}
			if (strgroup != string.Empty)
			{
				strWhere += " and groupid = '" + strgroup + "' ";
			}
			if (strDist != string.Empty)
			{
				strWhere += " " + strDist;
			}
			boolBySubGroup = boolSubGroup;
			if (boolSubGroup)
			{
				// If Not boolOnlySummary Then
				if (!boolPaidDept)
				{
					strSQL = "select tblcheckdetail.employeenumber,deptdiv,distpaycategory,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " + strWhere;
					strSQL += " group by distpaycategory,deptdiv,tblcheckdetail.employeenumber ";
					if (boolPayCatFirst)
					{
						strOrderBy = " order by distpaycategory,deptdiv,tblcheckdetail.employeenumber ";
					}
					else
					{
						strOrderBy = " order by deptdiv,distpaycategory,tblcheckdetail.employeenumber ";
					}
				}
				else
				{
					strSQL = "select tblcheckdetail.employeenumber,mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + ") as deptdiv,distpaycategory,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " + strWhere;
					strSQL += " group by distpaycategory,mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),tblcheckdetail.employeenumber ";
					if (boolPayCatFirst)
					{
						strOrderBy = " order by distpaycategory,mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),tblcheckdetail.employeenumber ";
					}
					else
					{
						strOrderBy = " order by mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),distpaycategory,tblcheckdetail.employeenumber ";
					}
				}
				// Else
				// strSQL = "select deptdiv,distpaycategory,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " & strWhere
				// strSQL = strSQL & " group by distpaycategory,deptdiv "
				// If boolPayCatFirst Then
				// strOrderBy = " order by distpaycategory,deptdiv "
				// Else
				// strOrderBy = " order by deptdiv,distpaycategory "
				// End If
				// End If
			}
			else if (boolPayCat)
			{
				// If Not boolOnlySummary Then
				strSQL = "select tblcheckdetail.employeenumber,distpaycategory,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " + strWhere;
				strSQL += " group by distpaycategory,tblcheckdetail.employeenumber,tblemployeemaster.lastname,tblemployeemaster.firstname ";
				strOrderBy = " order by Distpaycategory,tblemployeemaster.lastname,tblemployeemaster.firstname ";
				// Else
				// strSQL = "select distpaycategory,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " & strWhere
				// strSQL = strSQL & " group by distpaycategory "
				// strOrderBy = " order by Distpaycategory "
				// End If
			}
			else if (!boolPaidDept)
			{
				// If Not boolOnlySummary Then
				strSQL = "select tblcheckdetail.employeenumber,deptdiv,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " + strWhere;
				strSQL += " group by deptdiv,tblcheckdetail.employeenumber,tblemployeemaster.lastname,tblemployeemaster.firstname ";
				strOrderBy = " order by deptdiv,tblemployeemaster.lastname,tblemployeemaster.firstname ";
				// Else
				// strSQL = "select deptdiv,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " & strWhere
				// strSQL = strSQL & " group by deptdiv "
				// strOrderBy = " order by deptdiv "
				// End If
			}
			else
			{
				strSQL = "select tblcheckdetail.employeenumber,mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + ") as deptdiv,sum(disthours) as tothours,sum(distgrosspay) as totgrosspay from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) " + strWhere;
				strSQL += " group by mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),tblcheckdetail.employeenumber,tblemployeemaster.lastname,tblemployeemaster.firstname ";
				strOrderBy = " order by mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),tblemployeemaster.lastname,tblemployeemaster.firstname ";
			}
			strSQL += strOrderBy;
			clsReport.OpenRecordset(strSQL, "twpy0000.vb1");
			if (clsReport.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			clsDistPay.OpenRecordset("select * from TBLpaycategories order by ID", "twpy0000.vb1");
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "CustomDistributionReport");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader1");
			this.Fields.Add("grpHeader2");
			this.Fields.Add("Group1DField");
			this.Fields.Add("Group1TField");
			if (boolBySubGroup)
			{
				this.Fields.Add("Group2DField");
				this.Fields.Add("Group2TField");
			}
			else
			{
				txtGroup2.DataField = "";
				GroupHeader2.Visible = false;
				GroupFooter2.Visible = false;
				// lblGroup1Gross.Visible = True
				// lblGroup1Hours.Visible = True
				// lblHours.Visible = True
				// lblGrossPay.Visible = True
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			while (!clsReport.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(clsReport.Get_Fields("EmployeeNumber")))
				{
					clsReport.MoveNext();
				}
				else
				{
					break;
				}
			}
			eArgs.EOF = clsReport.EndOfFile();
			if (!eArgs.EOF)
			{
				if (!boolFirstTime)
				{
					clsReport.MoveNext();
				}
				boolFirstTime = false;
				if (!clsReport.EndOfFile())
				{
					if (boolPayCatFirst)
					{
						this.Fields["grpHeader1"].Value = clsReport.Get_Fields("distpaycategory");
						if (boolBySubGroup)
						{
							this.Fields["grpHeader2"].Value = clsReport.Get_Fields("distpaycategory") + "|" + clsReport.Get_Fields("deptdiv");
							this.Fields["group2DField"].Value = clsReport.Get_Fields("deptdiv");
						}
						if (clsDistPay.FindFirstRecord("ID", Conversion.Val(this.Fields["grpHeader1"].Value)))
						{
							// Me.Fields("Group1DField") = GroupHeader1.GroupValue & " " & clsDistPay.Fields("description")
							this.Fields["Group1DField"].Value = clsDistPay.Get_Fields_Int32("CategoryNumber") + " " + clsDistPay.Get_Fields("Description");
						}
						else
						{
							this.Fields["Group1DField"].Value = this.Fields["grpHeader1"].Value;
						}
					}
					else
					{
						this.Fields["grpHeader1"].Value = clsReport.Get_Fields("deptdiv");
						this.Fields["Group1DField"].Value = this.Fields["grpHeader1"].Value;
						if (boolBySubGroup)
						{
							this.Fields["grpHeader2"].Value = clsReport.Get_Fields("Deptdiv") + "|" + clsReport.Get_Fields("distpaycategory");
							if (clsDistPay.FindFirstRecord("ID", Conversion.Val(clsReport.Get_Fields("distpaycategory"))))
							{
								// Me.Fields("Group2DField") = clsReport.Fields("distpaycategory") & " " & clsDistPay.Fields("description")
								this.Fields["Group2DField"].Value = clsDistPay.Get_Fields_Int32("CategoryNumber") + " " + clsDistPay.Get_Fields("Description");
							}
							else
							{
								this.Fields["Group2DField"].Value = clsReport.Get_Fields("distpaycategory");
							}
						}
					}
					if (boolBySubGroup)
					{
						this.Fields["Group2tfield"].Value = this.Fields["group2dfield"].Value + " Total";
						this.Fields["group1tfield"].Value = this.Fields["group1dfield"].Value + " Total";
					}
					else
					{
						this.Fields["group1tfield"].Value = "Total";
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			clsEmp.OpenRecordset("select * from tblemployeemaster order by employeenumber", "twpy0000.vb1");
			boolFirstTime = true;
			dblGroup1Gross = 0;
			dblGroup1Hours = 0;
			intGroup1Employees = 0;
			dblGroup2Gross = 0;
			dblGroup2Hours = 0;
			intGroup2Employees = 0;
			dblTotGross = 0;
			dblTotHours = 0;
			intTotEmployees = 0;
			if (boolOnlySummary)
			{
				Detail.Visible = false;
				Detail.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblGross = 0;
			double dblHours = 0;
			if (!clsReport.EndOfFile())
			{
				if (!boolOnlySummary)
				{
					Detail.Visible = true;
				}
				dblGross = clsReport.Get_Fields("totgrosspay");
				txtGrossPay.Text = Strings.Format(dblGross, "#,###,##0.00");
				dblHours = Conversion.Val(clsReport.Get_Fields("tothours"));
				txtHours.Text = Strings.Format(dblHours, "#,###,##0.00");
				dblGroup1Gross += dblGross;
				dblGroup1Hours += dblHours;
				dblGroup2Gross += dblGross;
				dblGroup2Hours += dblHours;
				dblTotGross += dblGross;
				dblTotHours += dblHours;
				intGroup1Employees += 1;
				intGroup2Employees += 1;
				intTotEmployees += 1;
				if (!boolOnlySummary)
				{
					txtEmpNo.Text = clsReport.Get_Fields("employeenumber");
					if (clsEmp.FindFirstRecord("employeenumber", clsReport.Get_Fields("employeenumber")))
					{
						txtEmployeeName.Text = clsEmp.Get_Fields_String("FirstName") + " " + clsEmp.Get_Fields_String("LastName") + " " + clsEmp.Get_Fields("desig");
					}
					else
					{
						txtEmployeeName.Text = "";
					}
				}
			}
			else
			{
				txtEmpNo.Text = "";
				txtGrossPay.Text = "";
				txtHours.Text = "";
				txtEmployeeName.Text = "";
				Detail.Visible = false;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtGroup1Hours.Text = Strings.Format(dblGroup1Hours, "#,###,##0.00");
			txtGroup1GrossPay.Text = Strings.Format(dblGroup1Gross, "#,###,##0.00");
			fldGroup1Employees.Text = Strings.Format(intGroup1Employees, "#,##0");
			intGroup1Employees = 0;
			dblGroup1Hours = 0;
			dblGroup1Gross = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			txtGroup2Hours.Text = Strings.Format(dblGroup2Hours, "#,###,##0.00");
			txtGroup2GrossPay.Text = Strings.Format(dblGroup2Gross, "#,###,##0.00");
			fldGroup2Employees.Text = Strings.Format(intGroup2Employees, "#,##0");
			intGroup2Employees = 0;
			dblGroup2Hours = 0;
			dblGroup2Gross = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// If boolPayCatFirst Then
			// If clsDistPay.FindFirstRecord("ID", Val(GroupHeader1.GroupValue)) Then
			// txtGroup1.Text = GroupHeader1.GroupValue & " " & clsDistPay.Fields("description")
			// Else
			// txtGroup1.Text = GroupHeader1.GroupValue
			// End If
			// Else
			// txtGroup1.Text = GroupHeader1.GroupValue
			// End If
			// lblGroup1Label.Caption = txtGroup1.Text & " Total"
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// Dim strAry() As String
			// strAry = Split(GroupHeader2.GroupValue, "|", , vbTextCompare)
			// If boolPayCatFirst Then
			// txtGroup2.Text = strAry(1)
			// Else
			// If clsDistPay.FindFirstRecord("ID", Val(strAry(1))) Then
			// txtGroup2.Text = strAry(1) & " " & clsDistPay.Fields("description")
			// Else
			// txtGroup2.Text = strAry(1)
			// End If
			// End If
			// lblGroup2Label.Caption = txtGroup2.Text & " Total"
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotGross.Text = Strings.Format(dblTotGross, "#,###,##0.00");
			txtTotHours.Text = Strings.Format(dblTotHours, "#,###,##0.00");
			fldTotEmployees.Text = Strings.Format(intTotEmployees, "#,##0");
		}

		

	}
}
