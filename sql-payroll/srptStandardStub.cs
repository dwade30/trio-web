﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptStandardStub.
	/// </summary>
	public partial class srptStandardStub : FCSectionReport
	{
		public srptStandardStub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptStandardStub InstancePtr
		{
			get
			{
				return (srptStandardStub)Sys.GetInstance(typeof(srptStandardStub));
			}
		}

		protected srptStandardStub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptStandardStub	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolShowPayPeriod;
		private cEmployeeCheck eCheck;
		private cEarnedTimeService earnService = new cEarnedTimeService();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			eCheck = (this.ParentReport as rptEmailPayrollCheck).GetCheckByIndex(this.UserData.ToString().ToIntegerValue());
            boolShowPayPeriod = (this.ParentReport as rptEmailPayrollCheck).PrintPayPeriod;

			int x;
			if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais")
			{
				Detail.Height = 5220 / 1440F;
				Detail.CanShrink = false;
				for (x = 1; x <= this.Detail.Controls.Count - 1; x++)
				{
					if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "TEXT" || fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "BOLD")
					{
						if (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdate")
						{
							txtDate.Left = 5400 / 1440F;
						}
						else if (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtcheckno")
						{
						}
						else
						{
							this.Detail.Controls[x].Top += 200 / 1440F;
						}
					}
				}
				// x
				lblPayRate.Visible = false;
				txtPayRate.Visible = false;
				lblDeptDiv.Visible = false;
				txtDeptDiv.Visible = false;
				// txtEmployeeNo.Top = txtDeptDiv.Top
				txtEmployeeNo.Left = 3400 / 1440F;
				txtEmployeeNo.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				txtEmployeeNo.Top = txtName.Top;
				// txtName.Top = txtEmployeeNo.Top
				txtName.Left = 6120 / 1440F;
			}
		}

		private void ClearFields()
		{
			int x;
			for (x = 1; x <= 9; x++)
			{
				(Detail.Controls["txtPayDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtHours" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtPayAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblDeposit" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblDepositAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				Detail.Controls["lblDeposit" + x].Visible = false;
				Detail.Controls["lblDepositAmount" + x].Visible = false;
			}
			// x
			lblCode1.Text = "";
			lblCode2.Text = "";
			lblCode3.Text = "";
			lblCode1Balance.Text = "";
			lblCode2Balance.Text = "";
			lblCode3Balance.Text = "";
			txtVacationBalance.Text = "";
			txtSickBalance.Text = "";
			txtOtherBalance.Text = "";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// Fill textboxes from grid
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			bool boolPrintTest = false;
			// vbPorter upgrade warning: tempTax As cTax	OnWrite(object)
			cTax tempTax;
			int intMaxPayLines;
			int intCurrentLine = 0;
			int intMaxDeductionLines;
			double dblTotalHours = 0;
			double dblTotalPayAmount = 0;
			int intMaxDepositLines;
			int lngVacID = 0;
			int lngSickID;
			cVacSickItem earnItem;
			int intMaxVacSickLines;
			intMaxPayLines = 9;
			intMaxDeductionLines = 9;
			intMaxDepositLines = 9;
			intMaxVacSickLines = 3;
			ClearFields();
			if (eCheck == null)
				return;
			if (!boolPrintTest)
			{
				Line1.Visible = false;
				cPayRun pRun;
				pRun = eCheck.PayRun;
				if (!boolShowPayPeriod)
				{
					lblCheckMessage.Text = eCheck.CheckMessage;
				}
				else
				{
					if (!(pRun == null))
					{
						if (Information.IsDate(pRun.PeriodStart) && Information.IsDate(pRun.PeriodEnd))
						{
							lblCheckMessage.Text = "Pay Period: " + Strings.Format(pRun.PeriodStart, "MM/dd/yyyy") + " to " + Strings.Format(pRun.PeriodEnd, "MM/dd/yyyy");
						}
					}
				}
				lngVacID = earnService.GetVacationID();
				lngSickID = earnService.GetSickID();
				intCurrentLine = 0;
				cPayStubItem payItem;
				double dblPayExcess = 0;
				double dblExcessHours = 0;
				double dblFicaMed = 0;
				dblTotalHours = 0;
				dblTotalPayAmount = 0;
				eCheck.PayItems.MoveFirst();
				while (eCheck.PayItems.IsCurrent())
				{
					payItem = (cPayStubItem)eCheck.PayItems.GetCurrentItem();
					intCurrentLine += 1;
					dblTotalPayAmount += payItem.Amount;
					if (intCurrentLine < intMaxPayLines)
					{
						(Detail.Controls["txtPayDesc" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = payItem.Description;
						(Detail.Controls["txtPayAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(payItem.Amount, "0.00");
						if (!payItem.IsDollars)
						{
							(Detail.Controls["txtHours" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(payItem.Hours, "0.00");
							dblTotalHours += payItem.Hours;
							if (payItem.Hours < 0)
							{
							}
							else
							{
							}
						}
						else
						{
						}
					}
					else
					{
						dblPayExcess += payItem.Amount;
						if (!payItem.IsDollars)
						{
							dblExcessHours += payItem.Hours;
							dblTotalHours += payItem.Hours;
							(Detail.Controls["txtHours" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblExcessHours, "0.00");
						}
						(Detail.Controls["txtPayAmount" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblPayExcess, "0.00");
						if (intCurrentLine == intMaxPayLines)
						{
							(Detail.Controls["txtPayDesc" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = payItem.Description;
						}
						else
						{
							(Detail.Controls["txtPayDesc" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "Other";
						}
					}
					eCheck.PayItems.MoveNext();
				}
				cPayStubItem dedItem;
				double dblDedExcess = 0;
				double dblDedYTDExcess = 0;
				double dblTotalDeductions = 0;
				eCheck.DEDUCTIONS.MoveFirst();
				intCurrentLine = 0;
				while (eCheck.DEDUCTIONS.IsCurrent())
				{
					dedItem = (cPayStubItem)eCheck.DEDUCTIONS.GetCurrentItem();
					intCurrentLine += 1;
					dblTotalDeductions += dedItem.Amount;
					if (intCurrentLine < intMaxDeductionLines)
					{
						(Detail.Controls["txtDedDesc" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = dedItem.Description;
						(Detail.Controls["txtDedAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dedItem.Amount, "0.00");
						(Detail.Controls["txtDedYTD" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dedItem.total, "0.00");
					}
					else
					{
						dblDedExcess += dedItem.Amount;
						dblDedYTDExcess += dedItem.total;
						(Detail.Controls["txtDedAmount" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblDedExcess, "0.00");
						(Detail.Controls["txtDedYTD" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblDedYTDExcess, "0.00");
						if (intCurrentLine == intMaxDeductionLines)
						{
							(Detail.Controls["txtDedDesc" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = dedItem.Description;
						}
						else
						{
							(Detail.Controls["txtDedDesc" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "Other";
						}
					}
					eCheck.DEDUCTIONS.MoveNext();
				}
				eCheck.Deposits.MoveFirst();
				intCurrentLine = 0;
				cCheckDeposit depItem;
				double dblExcessDeposit = 0;
				while (eCheck.Deposits.IsCurrent())
				{
					depItem = (cCheckDeposit)eCheck.Deposits.GetCurrentItem();
					intCurrentLine += 1;
					if (intCurrentLine < intMaxDepositLines)
					{
						(Detail.Controls["lblDeposit" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = depItem.Bank;
						(Detail.Controls["lblDepositAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(depItem.Amount, "#,###,##0.00");
						Detail.Controls["lblDeposit" + intCurrentLine].Visible = true;
						Detail.Controls["lblDepositAmount" + intCurrentLine].Visible = true;
					}
					else
					{
						dblExcessDeposit += depItem.Amount;
						Detail.Controls["lblDeposit" + intMaxDepositLines].Visible = true;
						Detail.Controls["lblDepositAmount" + intMaxDepositLines].Visible = true;
						if (intCurrentLine == intMaxDepositLines)
						{
							(Detail.Controls["lblDeposit" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = depItem.Bank;
						}
						else
						{
							(Detail.Controls["lblDeposit" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = FCConvert.ToString(depItem.Bank == "Other");
						}
						(Detail.Controls["lblDepositAmount" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblExcessDeposit, "#,###,##0.00");
					}
					eCheck.Deposits.MoveNext();
				}
				// With rptLaserCheck1.GridMisc
				dblDirectDepChk = eCheck.CheckingAmount;
				dblDirectDepSav = eCheck.SavingsAmount;
				dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
				txtDate.Text = "DATE " + Strings.Format(eCheck.PayRun.PayDate, "MM/dd/yyyy");
				txtCurrentGross.Text = Strings.Format(eCheck.CurrentGross, "0.00");
				dblFicaMed = 0;
				tempTax = (cTax)eCheck.EmployeeTaxes["federal"];
				txtCurrentFed.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["fica"];
				dblFicaMed = tempTax.Tax;
				tempTax = (cTax)eCheck.EmployeeTaxes["medicare"];
				dblFicaMed += tempTax.Tax;
				txtCurrentFica.Text = Strings.Format(dblFicaMed, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["state"];
				txtCurrentState.Text = Strings.Format(tempTax.Tax, "0.00");
				txtCurrentDeductions.Text = Strings.Format(dblTotalDeductions, "0.00");
				txtYTDDeds.Text = Strings.Format(eCheck.YearToDateDeductions, "0.00");
				txtCurrentNet.Text = FCConvert.ToString(eCheck.Net);
				eCheck.EarnedTime.MoveFirst();
				intCurrentLine = 0;
				double dblOtherEarned = 0;
				dblOtherEarned = 0;
				while (eCheck.EarnedTime.IsCurrent())
				{
					earnItem = (cVacSickItem)eCheck.EarnedTime.GetCurrentItem();
					if (earnItem.TypeID == lngVacID)
					{
						txtVacationBalance.Text = Strings.Format(earnItem.Balance, "0.00");
					}
					else if (earnItem.TypeID == lngSickID)
					{
						txtSickBalance.Text = Strings.Format(earnItem.Balance, "0.00");
					}
					else
					{
						intCurrentLine += 1;
						if (intCurrentLine <= intMaxVacSickLines)
						{
							(Detail.Controls["lblCode" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = earnItem.Name;
							(Detail.Controls["lblCode" + intCurrentLine + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(earnItem.Balance, "0.00");
						}
						else
						{
							dblOtherEarned += earnItem.Balance;
							txtOtherBalance.Text = Strings.Format(dblOtherEarned, "0.00");
						}
					}
					eCheck.EarnedTime.MoveNext();
				}
				txtYTDGross.Text = Strings.Format(eCheck.YearToDateGross, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfederal"];
				txtYTDFed.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfica"];
				txtYTDFICA.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdstate"];
				txtYTDState.Text = Strings.Format(tempTax.Tax, "0.00");
				txtYTDNet.Text = Strings.Format(eCheck.YearToDateNet, "0.00");
				txtTotalHours.Text = Strings.Format(dblTotalHours, "0.00");
				txtTotalAmount.Text = Strings.Format(dblTotalPayAmount, "0.00");
				txtEMatchCurrent.Text = Strings.Format(eCheck.BenefitsTotal, "0.00");
				txtEmatchYTD.Text = Strings.Format(eCheck.YearToDateBenefitsTotal, "0.00");
				txtEmployeeNo.Text = eCheck.EmployeeNumber;
				txtName.Text = eCheck.EmployeeName;
				txtCheckNo.Text = FCConvert.ToString(eCheck.CheckNumber);
				if (dblDirectDeposit > 0)
				{
					// If LCase(MuniName) <> "rangeley" Then
					txtDirectDeposit.Visible = true;
					txtDirectDepositLabel.Visible = true;
					txtChkAmount.Visible = true;
					lblCheckAmount.Visible = true;
					txtDirectDepChkLabel.Visible = true;
					txtDirectDepositSavLabel.Visible = true;
					txtDirectDepSav.Visible = true;
					txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
					txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
					// End If
					txtChkAmount.Text = Strings.Format(eCheck.CheckAmount, "0.00");
				}
				else
				{
					txtDirectDeposit.Visible = false;
					txtDirectDepositLabel.Visible = false;
					txtChkAmount.Visible = false;
					lblCheckAmount.Visible = false;
					txtChkAmount.Text = Strings.Format(eCheck.CheckAmount, "0.00");
				}
				if (eCheck.BasePayrate == 0)
				{
					lblPayRate.Visible = false;
					txtPayRate.Visible = false;
				}
				else
				{
					lblPayRate.Visible = true;
					txtPayRate.Visible = true;
					txtPayRate.Text = Strings.Format(eCheck.BasePayrate, "0.00");
				}
				if (fecherFoundation.Strings.Trim(eCheck.DepartmentDivision) == string.Empty)
				{
					lblDeptDiv.Visible = false;
					txtDeptDiv.Visible = false;
				}
				else
				{
					lblDeptDiv.Visible = true;
					txtDeptDiv.Visible = true;
					txtDeptDiv.Text = fecherFoundation.Strings.Trim(eCheck.DepartmentDivision);
				}
				// End With
			}
			else
			{
				Line1.Visible = true;
			}
		}
	}
}
