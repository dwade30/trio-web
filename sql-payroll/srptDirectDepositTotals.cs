//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDirectDepositTotals.
	/// </summary>
	public partial class srptDirectDepositTotals : FCSectionReport
	{
		public srptDirectDepositTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptDirectDepositTotals InstancePtr
		{
			get
			{
				return (srptDirectDepositTotals)Sys.GetInstance(typeof(srptDirectDepositTotals));
			}
		}

		protected srptDirectDepositTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBankInfo?.Dispose();
                rsBankInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptDirectDepositTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		int intFinalCount;
		clsDRWrapper rsBankInfo = new clsDRWrapper();
		bool boolFromTempTable;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsBankInfo.MoveNext();
				eArgs.EOF = rsBankInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolFromTempTable = FCConvert.CBool(this.UserData);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			intFinalCount = 0;
			curFinalTotal = 0;
			if (!boolFromTempTable)
			{
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
				{
					rsBankInfo.OpenRecordset("SELECT tblBanks.RecordNumber as RecordNumber, tblBanks.Name as Name, SUM(tblCheckDetail.DDAmount) AS TotalAmount, COUNT(tblCheckDetail.DDAmount) AS TotalCount FROM (tblBanks INNER JOIN tblCheckDetail ON tblBanks.ID = convert(int, tblCheckDetail.DDBankNumber)) WHERE tblCheckDetail.BankRecord = 1 AND tblCheckDetail.PayDate = '" + FCConvert.ToString(rptDirectDeposit.InstancePtr.datPayDate) + "' GROUP BY tblBanks.RecordNumber, tblBanks.Name ORDER BY tblBanks.RecordNumber");
				}
				else
				{
					rsBankInfo.OpenRecordset("SELECT tblBanks.RecordNumber as RecordNumber, tblBanks.Name as Name, SUM(tblCheckDetail.DDAmount) AS TotalAmount, COUNT(tblCheckDetail.DDAmount) AS TotalCount FROM (tblBanks INNER JOIN tblCheckDetail ON tblBanks.ID = convert(int, tblCheckDetail.DDBankNumber)) WHERE tblCheckDetail.BankRecord = 1 AND tblCheckDetail.PayDate = '" + FCConvert.ToString(rptDirectDeposit.InstancePtr.datPayDate) + "' AND tblCheckDetail.PayRunID = " + FCConvert.ToString(rptDirectDeposit.InstancePtr.intPayRunID) + " GROUP BY tblBanks.RecordNumber, tblBanks.Name ORDER BY tblBanks.RecordNumber");
				}
			}
			else
			{
				rsBankInfo.OpenRecordset("SELECT tblBanks.RecordNumber as RecordNumber, tblBanks.Name as Name, SUM(tblTempPayProcess.DDAmount) AS TotalAmount, COUNT(tblTempPayProcess.DDAmount) AS TotalCount FROM (tblBanks INNER JOIN tblTempPayProcess ON tblBanks.ID = convert(int,tblTempPayProcess.DDBankNumber)) WHERE tblTempPayProcess.BankRecord = 1 AND tblTempPayProcess.PayDate = '" + FCConvert.ToString(rptDirectDeposit.InstancePtr.datPayDate) + "' AND tblTempPayProcess.PayRunID = " + FCConvert.ToString(rptDirectDeposit.InstancePtr.intPayRunID) + " GROUP BY tblBanks.RecordNumber, tblBanks.Name ORDER BY tblBanks.RecordNumber");
			}
			if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldBank As object	OnWrite(string)
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			fldBank.Text = Strings.Format(rsBankInfo.Get_Fields_Int32("RecordNumber"), "00") + " " + rsBankInfo.Get_Fields_String("Name");
			fldAmount.Text = Strings.Format(rsBankInfo.Get_Fields("TotalAmount"), "#,##0.00");
			fldCount.Text = FCConvert.ToString(rsBankInfo.Get_Fields("TotalCount"));
			curFinalTotal += FCConvert.ToDecimal(rsBankInfo.Get_Fields("TotalAmount"));
			intFinalCount += FCConvert.ToDecimal(rsBankInfo.Get_Fields("TotalCount"));
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalCount As object	OnWrite
			fldTotalAmount.Text = Strings.Format(curFinalTotal, "#,##0.00");
			fldTotalCount.Text = intFinalCount.ToString();
		}

		
	}
}
