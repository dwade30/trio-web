//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Labels";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND frmCustomLabels.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As int	OnWriteFCConvert.ToInt32(
		float intLabelWidth;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
        private SSNViewType ssnViewPermission = SSNViewType.None;
        public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "CustomLabels");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int intRow;
			int intCol;
			int intControl;
			int intType = 0;
			while (!rsData.EndOfFile())
			{
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			// 
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
			{
				intType = FCConvert.ToInt32(Math.Round(Conversion.Val(Detail.Controls[intControl].Tag)));

                switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intType]))
				{
					case modCustomReport.GRIDDATE:
						{
							if (Information.IsDate(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intType])).ToStringES())))
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intType])).ToStringES()), "MM/dd/yyyy");
							}
							else
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							}
							break;
						}
					default:
						{
                            var strData = rsData.Get_Fields_String(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intType])).ToStringES());
                            var strNameOfField = modCustomReport.Statics.strFields[intType].Trim();
                            if (strNameOfField.ToLower() == "ssn" || strNameOfField.ToLower() == "socialsecuritynumber")
                            {
                                if (ssnViewPermission == SSNViewType.Masked)
                                {
                                    strData = "***-**-" + strData.Right(4);
                                }
                                else if (ssnViewPermission == SSNViewType.None)
                                {
                                    strData = "***-**-****";
                                }
                            }
                            (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
							break;
						}
				}
				//end switch
			}			
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobalVariables.DEFAULTDATABASE);
			clsPrintLabel labLabel = new clsPrintLabel();
			int intIndex;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			int intLabelType;
			intLabelType = frmCustomLabels.InstancePtr.cmbLabelType.ItemData(frmCustomLabels.InstancePtr.cmbLabelType.SelectedIndex);
			intIndex = labLabel.Get_IndexFromID(intLabelType);
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			modGlobalConstants.Statics.PrintWidth = 8.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
			intLabelWidth = labLabel.Get_LabelWidth(intIndex);
			if (labLabel.Get_TopMargin(intIndex) > 0)
			{
				this.PageSettings.Margins.Top = labLabel.Get_TopMargin(intIndex);
			}
			if (labLabel.Get_LeftMargin(intIndex) > 0)
			{
				this.PageSettings.Margins.Left = labLabel.Get_LeftMargin(intIndex);
			}
			if (labLabel.Get_RightMargin(intIndex) > 0)
			{
				this.PageSettings.Margins.Right = labLabel.Get_RightMargin(intIndex);
			}
			if (labLabel.Get_PageWidth(intIndex) > 0)
			{
				this.PageSettings.PaperWidth = labLabel.Get_PageWidth(intIndex);
			}
			else
			{
				this.PageSettings.PaperWidth = 8.5F;
			}
			if (labLabel.Get_PageHeight(intIndex) > 0)
			{
				PageSettings.PaperHeight = labLabel.Get_PageHeight(intIndex);
			}
			modGlobalConstants.Statics.PrintWidth = FCConvert.ToSingle(this.PageSettings.PaperWidth - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right);
			intLabelWidth = labLabel.Get_LabelWidth(intIndex);
			Detail.Height = labLabel.Get_LabelHeight(intIndex) + labLabel.Get_VerticalSpace(intIndex);
			Detail.ColumnCount = labLabel.Get_LabelsWide(intIndex);
			Detail.ColumnSpacing = labLabel.Get_HorizontalSpace(intIndex);
			
			CreateDataFields();
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
        }

		private void CreateHeaderFields()
		{
			int intControlNumber = 0;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
			double dblSizeRatio;
			//dblSizeRatio = (frmCustomLabels.InstancePtr.Line2.X2 - frmCustomLabels.InstancePtr.Line2.X1) / 1440;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 0; intRow <= (frmCustomLabels.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomLabels.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					intControlNumber += 1;
					NewField = PageHeader.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtHeader" + FCConvert.ToString(intControlNumber));
					//NewField.Name = "txtHeader" + FCConvert.ToString(intControlNumber);
					NewField.Top = (intRow * frmCustomLabels.InstancePtr.vsLayout.RowHeight(0)) / 1440F + 1000 / 1440F;
					//NewField.Left = (frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / dblSizeRatio) + 50;
					if (intCol > 0)
					{
						if (frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) == frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol - 1))
						{
							NewField.Text = string.Empty;
						}
						else
						{
							NewField.Text = frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
						}
					}
					else
					{
						NewField.Text = frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					//NewField.Width = FCConvert.ToSingle(frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol) / dblSizeRatio);
					NewField.WordWrap = false;
					//PageHeader.Controls.Add(NewField);
				}
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
			double dblSizeRatio;
			bool boolSkip = false;
			//dblSizeRatio = (frmCustomLabels.InstancePtr.Line2.X2 - frmCustomLabels.InstancePtr.Line2.X1) / 1440;
			// line2 is invisible and is 1440 wide before resizing
			// this lets us know how much we have been resized
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 1; intRow <= (frmCustomLabels.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomLabels.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					boolSkip = false;
					//FC:FINAL:DDU:#i2458 - check if null or empty not just empty
					if (!FCUtils.IsNull(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))
						&& !FCUtils.IsEmpty(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))
					{
						if (intCol > 0)
						{
							if (Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))
							{
								boolSkip = true;
							}
							else if (Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) < 0)
							{
								boolSkip = true;
							}
						}
						if (!boolSkip && Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) >= 0)
						{
							NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol));
							//NewField.Name = "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol);
							NewField.Top = ((intRow - 1) * 240) / 1440F;
							//NewField.Left = frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / dblSizeRatio;
							//NewField.Width = FCConvert.ToSingle(frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol) / dblSizeRatio);
							NewField.Height = 240 / 1440F;
							NewField.Text = string.Empty;
							NewField.WordWrap = false;
							NewField.CanGrow = false;
							NewField.CanShrink = false;
							NewField.Tag = Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol));
							//Detail.Controls.Add(NewField);
						}
						else
						{
							//(NewField as GrapeCity.ActiveReports.SectionReportModel.TextBox).Width += FCConvert.ToSingle(frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol) / dblSizeRatio);
						}
						if (NewField.Left >= intLabelWidth)
						{
							NewField.Visible = false;
						}
						else if (NewField.Left + NewField.Width > intLabelWidth)
						{
							NewField.Width = intLabelWidth - NewField.Left;
						}
					}
				}
				// intCol
			}
			// intRow
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			
		}

		
	}
}
