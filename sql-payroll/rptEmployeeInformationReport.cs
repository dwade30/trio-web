﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeInformationReport.
	/// </summary>
	public partial class rptEmployeeInformationReport : BaseSectionReport
	{
		public rptEmployeeInformationReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Employee Information Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptEmployeeInformationReport InstancePtr
		{
			get
			{
				return (rptEmployeeInformationReport)Sys.GetInstance(typeof(rptEmployeeInformationReport));
			}
		}

		protected rptEmployeeInformationReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsProcessing?.Dispose();
                rsProcessing = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEmployeeInformationReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cEmployeeService employeeService = new cEmployeeService();
		private object intEmployeeReport;
		private int intCounter;
		private int intpage;
		private string[] strEmployeeNumbers = null;
		private bool boolRange;
		private clsDRWrapper rsProcessing = new clsDRWrapper();
		//private clsDRWrapper rsNumbers = new clsDRWrapper();
		private bool boolEmpty;
		private bool boolDeductions;
		private bool boolEmpMatch;
		private bool boolDirectDeposit;
		private bool boolPayTotals;
		private bool boolVacSick;
		private bool boolMSRS;
		private bool boolDistribtuion;
		private cGenericCollection employeeList;
       

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				Next1:
				;
				// are there any more employee to process
				if (boolEmpty)
				{
					eArgs.EOF = true;
					return;
				}
				if (intCounter > Information.UBound(strEmployeeNumbers, 1))
				{
					eArgs.EOF = true;
					return;
				}
				modGlobalVariables.Statics.gstrEmployeeToGet = strEmployeeNumbers[intCounter];
				intCounter += 1;
				eArgs.EOF = false;
				lblHeader.Text = lblHeader.Tag + "  -  " + modGlobalVariables.Statics.gstrEmployeeToGet;
				// is that employee really there, make sure else go look for next one
				rsProcessing.OpenRecordset("Select * From tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gstrEmployeeToGet + "'", "TWPY0000.vb1");
				if (rsProcessing.EndOfFile())
					goto Next1;
				
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
        
        private void Detail_Format(object sender, System.EventArgs e)
        {
            srptEmployeeMaster.Report = new rptEmployeeMaster();
            srptEmployeeMaster.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            if (boolDirectDeposit)
            {
                subDirectDeposit.Report = new srptDirectDeposit();
                subDirectDeposit.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            if (boolPayTotals)
            {
                srptEmployeePayTotals.Report = new srptEmployeePayTotals();
                srptEmployeePayTotals.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            else
            {
                this.Shape4.Visible = false;
                this.Label33.Visible = false;
                this.Label34.Visible = false;
                this.Label35.Visible = false;
                this.Label36.Visible = false;
                this.Label37.Visible = false;
                this.Label43.Visible = false;
                this.Label44.Visible = false;
            }
            if (boolDirectDeposit)
            {
                subDirectDeposit.Report = new srptDirectDeposit();
                subDirectDeposit.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            if (boolVacSick)
            {
                srptVacSick.Report = new srptEmpVacSick();
                srptVacSick.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            if (boolEmpMatch)
            {
                srptEmployeeMatch.Report = new srptEmployeeMatch();
                srptEmployeeMatch.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            if (boolDeductions)
            {
                srptEmployeeDeductions.Report = new srptEmployeeDeductions();
                srptEmployeeDeductions.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            if (boolDistribtuion)
            {
                SubDistribution.Report = new srptDistribution();
                SubDistribution.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
            if (boolMSRS)
            {
                subMSRS.Report = new srptMSRS();
                subMSRS.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
            }
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_ReportEnd";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolEmpty)
				{
				}
				else
				{
					srptEmployeeMaster.Report.Unload();
					srptEmployeeMaster.Report = null;
					if (boolDirectDeposit)
					{
						subDirectDeposit.Report.Unload();
					}
					subDirectDeposit.Report = null;
					if (boolEmpMatch)
					{
						srptEmployeeMatch.Report.Unload();
					}
					srptEmployeeMatch.Report = null;
					if (boolDeductions)
					{
						srptEmployeeDeductions.Report.Unload();
					}
					srptEmployeeDeductions.Report = null;
					if (boolPayTotals)
					{
						srptEmployeePayTotals.Report.Unload();
					}
					srptEmployeePayTotals.Report = null;
					if (boolDistribtuion)
					{
						SubDistribution.Report.Unload();
					}
					SubDistribution.Report = null;
					if (boolVacSick)
					{
						srptVacSick.Report.Unload();
					}
					srptVacSick.Report = null;
					if (boolMSRS)
					{
						subMSRS.Report.Unload();
					}
					subMSRS.Report = null;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_ReportStart";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCounter = 0;
                using (clsDRWrapper rsData = new clsDRWrapper())
                {
                    // Dim strSQL As String
                    string strOrderBy = "";
                    //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                    rsProcessing.DefaultDB = "TWPY0000.vb1";

                    int lngUserID;
                    lngUserID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                    if (employeeList == null)
                    {
                        rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                        switch (rsData.Get_Fields_Int32("reportsequence"))
                        {
                            case 0:
                            {
                                // employee name
                                strOrderBy = "lastname,firstname ";
                                break;
                            }
                            case 1:
                            {
                                // employee number
                                strOrderBy = "employeenumber ";
                                break;
                            }
                            case 2:
                            {
                                // sequence
                                strOrderBy = "seqnumber,lastname,firstname ";
                                break;
                            }
                            case 3:
                            {
                                // dept/div
                                strOrderBy = "DEPTDIV,lastname,firstname ";
                                break;
                            }
                        }

                        //end switch
                        employeeList = employeeService.GetEmployeesPermissable(lngUserID, strOrderBy, false);
                    }

                    if (employeeList.ItemCount() < 1)
                    {
                        boolEmpty = true;
                    }
                    else
                    {
                        boolEmpty = false;
                        strEmployeeNumbers = new string[employeeList.ItemCount() - 1 + 1];
                        employeeList.MoveFirst();
                        cEmployee tempEmp;
                        intCounter = 0;
                        while (employeeList.IsCurrent())
                        {
                            //Application.DoEvents();
                            tempEmp = (cEmployee) employeeList.GetCurrentItem();
                            strEmployeeNumbers[intCounter] = tempEmp.EmployeeNumber;
                            intCounter += 1;
                            employeeList.MoveNext();
                        }
                    }

                    intCounter = 0;
                    intpage = 0;
                    lblHeader.Tag = "EMPLOYEE DATA";
                }
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		public void Init(int intOptions, cGenericCollection empList)
		{
			// 1 is direct deposit
			// 2 is deductions
			// 4 is employer match
			// 8 is pay totals
			// 16 is vac sick
			// 32 is msrs
			employeeList = empList;
			boolMSRS = false;
			boolVacSick = false;
			boolPayTotals = false;
			boolEmpMatch = false;
			boolDeductions = false;
			boolDirectDeposit = false;
			if (intOptions >= 64)
			{
				boolDistribtuion = true;
				intOptions -= 64;
			}
			if (intOptions >= 32)
			{
				boolMSRS = true;
				intOptions -= 32;
			}
			if (intOptions >= 16)
			{
				boolVacSick = true;
				intOptions -= 16;
			}
			if (intOptions >= 8)
			{
				boolPayTotals = true;
				intOptions -= 8;
			}
			if (intOptions >= 4)
			{
				boolEmpMatch = true;
				intOptions -= 4;
			}
			if (intOptions >= 2)
			{
				boolDeductions = true;
				intOptions -= 2;
			}
			if (intOptions >= 1)
			{
				boolDirectDeposit = true;
				intOptions = 0;
			}
            //FC:FINAL:SBE - #2519 - show progress dialog during report load, to avoid ThreadAbortException
            frmReportViewer.InstancePtr.Init(this, boolAllowEmail: false, showProgressDialog: true);
		}

		
	}
}
