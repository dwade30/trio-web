﻿//Fecher vbPorter - Version 1.0.0.59
using Global;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cChecksToEmailView
	{
		//=========================================================
		private cGenericCollection checksToEmail = new cGenericCollection();
		private Dictionary<int, object> ChecksDictionary = new Dictionary<int, object>();

		public delegate void CancelledEventHandler();

		public event CancelledEventHandler Cancelled;

		public delegate void SelectionsCommittedEventHandler();

		public event SelectionsCommittedEventHandler SelectionsCommitted;

		public Dictionary<int, object> SelectedChecks
		{
			get
			{
				Dictionary<int, object> SelectedChecks = null;
				SelectedChecks = ChecksDictionary;
				return SelectedChecks;
			}
		}

		public cGenericCollection Checks
		{
			get
			{
				cGenericCollection Checks = null;
				Checks = checksToEmail;
				return Checks;
			}
		}

		public void Commit(ref Dictionary<int, object> checksToUse)
		{
			ChecksDictionary = checksToUse;
			if (this.SelectionsCommitted != null)
				this.SelectionsCommitted();
		}

		public void Cancel()
		{
			if (this.Cancelled != null)
				this.Cancelled();
		}
	}
}
