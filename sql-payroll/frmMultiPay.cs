//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmMultiPay : BaseForm
	{
		public frmMultiPay()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMultiPay InstancePtr
		{
			get
			{
				return (frmMultiPay)Sys.GetInstance(typeof(frmMultiPay));
			}
		}

		protected frmMultiPay _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       August 7,2001
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		private int intCounter;
		private int intDataChanged;

		private void frmMultiPay_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmMultiPay_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMultiPay properties;
			//frmMultiPay.ScaleWidth	= 5880;
			//frmMultiPay.ScaleHeight	= 3930;
			//frmMultiPay.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, FCConvert.ToInt16(true));
				// open the forms global database connection
				// Set dbDeductions = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				SetGridProperties();
				LoadData();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmMultiPay_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				vsMultiPay.Rows = 1;
				rsData.OpenRecordset("Select * from tblMultiPay where ParentID = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "' order by ChildID", "Twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					vsMultiPay.Rows = rsData.RecordCount() + 1;
					vsMultiPay.FixedRows = 1;
					for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
					{
						vsMultiPay.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ChildID")));
						vsMultiPay.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Caption")));
						vsMultiPay.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields("ID")));
						vsMultiPay.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields_Boolean("UseParentDeductions")));
						rsData.MoveNext();
					}
					vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					vsMultiPay.Rows = 2;
					vsMultiPay.FixedRows = 1;
					vsMultiPay.TextMatrix(1, 0, FCConvert.ToString(1));
					vsMultiPay.TextMatrix(1, 1, "Master");
					vsMultiPay.TextMatrix(1, 2, modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID);
					vsMultiPay.TextMatrix(1, 3, FCConvert.ToString(true));
					rsData.Execute("Insert into tblMultiPay (ParentID,ChildID,ChildEmployeeID,Caption,UseParentDeductions) VALUES (" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + ",1," + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + ",'Master',1)", "Twpy0000.vb1");
					rsData.OpenRecordset("SELECT max(ID) as NewID from tblMultiPay", "Twpy0000.vb1");
					vsMultiPay.TextMatrix(1, 2, FCConvert.ToString(rsData.Get_Fields("NewID")));
				}
				vsMultiPay.Row = 0;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						// Call cmdSave_Click
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmMultiPay_Resize(object sender, System.EventArgs e)
		{
			vsMultiPay.ColWidth(0, FCConvert.ToInt32(vsMultiPay.WidthOriginal * 0.05));
			vsMultiPay.ColWidth(1, FCConvert.ToInt32(vsMultiPay.WidthOriginal * 0.6));
			vsMultiPay.ColWidth(2, FCConvert.ToInt32(vsMultiPay.WidthOriginal * 0.05));
			vsMultiPay.ColWidth(3, FCConvert.ToInt32(vsMultiPay.WidthOriginal * 0.2));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsMultiPay.Cols = 4;
				vsMultiPay.FixedCols = 1;
				vsMultiPay.ColHidden(2, true);
				modGlobalRoutines.CenterGridCaptions(ref vsMultiPay);
				// set column 0 properties
				vsMultiPay.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMultiPay.ColWidth(0, 400);
				// set column 1 properties
				vsMultiPay.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftBottom);
				vsMultiPay.ColWidth(1, 3000);
				vsMultiPay.TextMatrix(0, 1, "Caption");
				// set column 2 properties
				vsMultiPay.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMultiPay.ColWidth(2, 500);
				vsMultiPay.TextMatrix(0, 2, "");
				// set column 3 properties
				vsMultiPay.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMultiPay.ColWidth(3, 1500);
				vsMultiPay.TextMatrix(0, 3, "Use Master Ded");
				vsMultiPay.ColDataType(3, FCGrid.DataTypeSettings.flexDTBoolean);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			vsMultiPay.AddItem(FCConvert.ToString(vsMultiPay.Rows));
			vsMultiPay.Select(vsMultiPay.Rows - 1, 1);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			if (MessageBox.Show("This will remove Multi-Pay #" + vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMultiPay.Row, 0) + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				rsData.Execute("Delete from tblMultiPay where ID = " + vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMultiPay.Row, 2), "Twpy0000.vb1");
				vsMultiPay.RemoveItem();
				for (intCounter = 1; intCounter <= (vsMultiPay.Rows - 1); intCounter++)
				{
					vsMultiPay.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
				}
				mnuSave_Click();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			int intCount;
			int intMaxLength = 0;
			// vbPorter upgrade warning: strTemp As string	OnWrite(int, string, double)
			string strTemp = "";
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsChildID = new clsDRWrapper();
			vsMultiPay.Select(0, 0);
			rsData.OpenRecordset("Select * from tblMultiPay where ParentID = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "' order by ChildID", "Twpy0000.vb1");
			for (intCounter = 1; intCounter <= (vsMultiPay.Rows - 1); intCounter++)
			{
				rsData.FindFirstRecord("ID", Conversion.Val(vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)));
				// If rsData.Fields("ParentID") <> rsData.Fields("ChildEmployeeID") Then
				if (rsData.NoMatch)
				{
					rsData.AddNew();
					rsChildID.OpenRecordset("SELECT tblFieldLengths.EmployeeNumber, tblMultiPay.ParentID, tblMultiPay.ChildID, tblMultiPay.Text, tblMultiPay.ChildEmployeeID" + " FROM tblFieldLengths, tblMultiPay where ParentID = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "' order by ChildEmployeeID DESC", "Twpy0000.vb1");
					// get the max employee length
					intMaxLength = FCConvert.ToInt16(rsChildID.Get_Fields("EmployeeNumber"));
					if (FCConvert.ToString(rsChildID.Get_Fields_Int32("ChildEmployeeID")).Length < intMaxLength)
					{
						// this is the first child
						NewID:
						;
						for (intCount = 1; intCount <= intMaxLength; intCount++)
						{
							if (intCount == 1)
							{
								strTemp = FCConvert.ToString(9);
							}
							else
							{
								strTemp += "0";
							}
						}
					}
					else
					{
						if (FCConvert.ToDouble(Strings.Left(FCConvert.ToString(rsChildID.Get_Fields_Int32("ChildEmployeeID")), 1)) != 9)
						{
							for (intCount = 1; intCount <= intMaxLength; intCount++)
							{
								if (intCount == 1)
								{
									strTemp = FCConvert.ToString(9);
								}
								else
								{
									strTemp += "0";
								}
							}
						}
						else
						{
							strTemp = FCConvert.ToString((Conversion.Val(rsChildID.Get_Fields_Int32("ChildEmployeeID")) + 1));
						}
					}
					rsChildID.OpenRecordset("Select Max(EmployeeNumber) as MaxNum from tblEmployeeMaster", "Twpy0000.vb1");
					if (!rsChildID.EndOfFile())
					{
						if (rsChildID.Get_Fields("MaxNum") < Conversion.Val(strTemp))
						{
							rsData.SetData("ChildEmployeeID", Conversion.Val(strTemp));
						}
						else
						{
							rsData.SetData("ChildEmployeeID", Conversion.Val(rsChildID.Get_Fields("MaxNum")) + 1);
						}
					}
					else
					{
						return;
					}
					rsData.SetData("ParentID", modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID);
					rsData.SetData("ChildID", intCounter);
					rsData.SetData("Caption", vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1));
					//rsData.SetData("UseParentDeductions", vsMultiPay.CellChecked);
					rsData.Update();
					rsChildID.OpenRecordset("SELECT max(ID) as NewID from tblMultiPay", "Twpy0000.vb1");
					vsMultiPay.TextMatrix(intCounter, 2, FCConvert.ToString(rsChildID.Get_Fields("NewID")));
				}
				else
				{
					rsData.Edit();
					rsData.SetData("ParentID", modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID);
					rsData.SetData("ChildID", intCounter);
					rsData.SetData("Caption", vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1));
					rsData.SetData("UseParentDeductions", (vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3) == true ? true : false));
					rsData.Update();
				}
				// End If
			}
			MessageBox.Show("Save was successful", "Payroll", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			Close();
		}

		private void vsMultiPay_RowColChange(object sender, System.EventArgs e)
		{
			if (vsMultiPay.Row != 1)
			{
				vsMultiPay.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsMultiPay.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			//vsMultiPay.Editable = vsMultiPay.Row!=1;
		}

		private void vsMultiPay_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 1; intCounter <= (vsMultiPay.Rows - 1); intCounter++)
			{
				if (intCounter != vsMultiPay.Row)
				{
					if (FCConvert.ToString(vsMultiPay.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, vsMultiPay.Col)) == vsMultiPay.EditText)
					{
						MessageBox.Show("Cannot have Duplicate descriptions for Mulit Pay's", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
					}
				}
			}
		}
	}
}
