//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmProcessEditTotals.
	/// </summary>
	partial class frmProcessEditTotals
	{
		public fecherFoundation.FCTextBox txtHours;
		public fecherFoundation.FCTextBox txtDollars;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCLabel lblTotals;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuCalculate;
		public fecherFoundation.FCToolStripMenuItem mnuSummary;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrintScreen;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtHours = new fecherFoundation.FCTextBox();
			this.txtDollars = new fecherFoundation.FCTextBox();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.lblTotals = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCalculate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSummary = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintScreen = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdCalculate = new fecherFoundation.FCButton();
			this.cmdShowBreakdown = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdShowBreakdown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdCalculate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 460);
			this.BottomPanel.Size = new System.Drawing.Size(678, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsGrid);
			this.ClientArea.Controls.Add(this.txtHours);
			this.ClientArea.Controls.Add(this.txtDollars);
			this.ClientArea.Controls.Add(this.lblTotals);
			this.ClientArea.Size = new System.Drawing.Size(678, 400);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdShowBreakdown);
			this.TopPanel.Size = new System.Drawing.Size(678, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdShowBreakdown, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(128, 30);
			this.HeaderText.Text = "Edit Totals";
			// 
			// txtHours
			// 
			this.txtHours.BackColor = System.Drawing.SystemColors.Menu;
			this.txtHours.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.txtHours.Location = new System.Drawing.Point(143, 270);
			this.txtHours.LockedOriginal = true;
			this.txtHours.Name = "txtHours";
			this.txtHours.ReadOnly = true;
			this.txtHours.Size = new System.Drawing.Size(120, 40);
			this.txtHours.TabIndex = 1;
			this.txtHours.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtHours.Visible = false;
			// 
			// txtDollars
			// 
			this.txtDollars.BackColor = System.Drawing.SystemColors.Menu;
			this.txtDollars.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.txtDollars.Location = new System.Drawing.Point(283, 270);
			this.txtDollars.LockedOriginal = true;
			this.txtDollars.Name = "txtDollars";
			this.txtDollars.ReadOnly = true;
			this.txtDollars.Size = new System.Drawing.Size(120, 40);
			this.txtDollars.TabIndex = 2;
			this.txtDollars.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtDollars.Visible = false;
			// 
			// vsGrid
			// 
			this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsGrid.Cols = 10;
			this.vsGrid.Location = new System.Drawing.Point(30, 30);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.Rows = 50;
			this.vsGrid.Size = new System.Drawing.Size(620, 347);
			this.vsGrid.TabIndex = 3;
			// 
			// lblTotals
			// 
			this.lblTotals.Location = new System.Drawing.Point(30, 284);
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Size = new System.Drawing.Size(56, 16);
			this.lblTotals.TabIndex = 3;
			this.lblTotals.Text = "TOTALS";
			this.lblTotals.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuCalculate,
            this.mnuSummary,
            this.mnuSP1,
            this.mnuPrintScreen,
            this.mnuSP2,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuCalculate
			// 
			this.mnuCalculate.Index = 0;
			this.mnuCalculate.Name = "mnuCalculate";
			this.mnuCalculate.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuCalculate.Text = "Calculate                  ";
			this.mnuCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
			// 
			// mnuSummary
			// 
			this.mnuSummary.Index = 1;
			this.mnuSummary.Name = "mnuSummary";
			this.mnuSummary.Shortcut = Wisej.Web.Shortcut.F9;
			this.mnuSummary.Text = "Show Breakdown";
			this.mnuSummary.Click += new System.EventHandler(this.mnuSummary_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 2;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuPrintScreen
			// 
			this.mnuPrintScreen.Index = 3;
			this.mnuPrintScreen.Name = "mnuPrintScreen";
			this.mnuPrintScreen.Text = "Print Screen";
			this.mnuPrintScreen.Click += new System.EventHandler(this.mnuPrintScreen_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 4;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 5;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdCalculate
			// 
			this.cmdCalculate.AppearanceKey = "acceptButton";
			this.cmdCalculate.Location = new System.Drawing.Point(266, 30);
			this.cmdCalculate.Name = "cmdCalculate";
			this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCalculate.Size = new System.Drawing.Size(118, 48);
			this.cmdCalculate.TabIndex = 1;
			this.cmdCalculate.Text = "Calculate";
			this.cmdCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
			// 
			// cmdShowBreakdown
			// 
			this.cmdShowBreakdown.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdShowBreakdown.Location = new System.Drawing.Point(422, 29);
			this.cmdShowBreakdown.Name = "cmdShowBreakdown";
			this.cmdShowBreakdown.Shortcut = Wisej.Web.Shortcut.F9;
			this.cmdShowBreakdown.Size = new System.Drawing.Size(128, 24);
			this.cmdShowBreakdown.TabIndex = 1;
			this.cmdShowBreakdown.Text = "Show Breakdown";
			this.cmdShowBreakdown.Click += new System.EventHandler(this.mnuSummary_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Location = new System.Drawing.Point(556, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(94, 24);
			this.cmdPrint.TabIndex = 2;
			this.cmdPrint.Text = "Print Screen";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrintScreen_Click);
			// 
			// frmProcessEditTotals
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(678, 568);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmProcessEditTotals";
			this.Text = "Edit Totals";
			this.Load += new System.EventHandler(this.frmProcessEditTotals_Load);
			this.Activated += new System.EventHandler(this.frmProcessEditTotals_Activated);
			this.Resize += new System.EventHandler(this.frmProcessEditTotals_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProcessEditTotals_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdShowBreakdown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdCalculate;
		private FCButton cmdShowBreakdown;
		private FCButton cmdPrint;
	}
}