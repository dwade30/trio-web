//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2C.
	/// </summary>
	public partial class rptW2C : BaseSectionReport
	{
		public rptW2C()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W2C";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2C InstancePtr
		{
			get
			{
				return (rptW2C)Sys.GetInstance(typeof(rptW2C));
			}
		}

		protected rptW2C _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2C	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double dblLaserLineAdjustment;
		// vbPorter upgrade warning: dblHorizAdjust As object	OnWrite(double, object)
		object dblHorizAdjust;
		clsDRWrapper rsReport = new clsDRWrapper();
		bool boolPrintTest;
		clsW2C clsW2CSummary;
		int intRecCount;

		public void Init(int lngYear, clsW2C W2CClass, bool modalDialog, bool boolW2 = true, double dblAdjustment = 0, double dblHorizAdjustment = 0, bool boolTestPrint = false)
		{
			string strSQL = "";
			string[] straddr = new string[4 + 1];
			int X = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsW2CSummary = W2CClass;
				boolPrintTest = boolTestPrint;
				straddr[1] = "";
				straddr[2] = "";
				straddr[3] = "";
				straddr[4] = "";
				dblLaserLineAdjustment = dblAdjustment;
				dblHorizAdjust = 120 * dblHorizAdjustment;
				if (!boolPrintTest)
				{
					strSQL = "select * from tblw2master";
					rsReport.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsReport.EndOfFile())
					{
						X = 1;
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields("employersname"))) != string.Empty)
						{
							straddr[X] = FCConvert.ToString(rsReport.Get_Fields("employersname"));
							X += 1;
						}
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields("address1"))) != string.Empty)
						{
							straddr[X] = FCConvert.ToString(rsReport.Get_Fields("address1"));
							X += 1;
						}
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields("address2"))) != string.Empty)
						{
							straddr[X] = FCConvert.ToString(rsReport.Get_Fields("address2"));
							X += 1;
						}
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields("city"))) != string.Empty || FCConvert.ToString(rsReport.Get_Fields("state")) != string.Empty)
						{
							straddr[X] = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields("city") + "  " + rsReport.Get_Fields("state")) + " " + rsReport.Get_Fields_String("zip"));
							X += 1;
						}
						txtEmployerName.Text = straddr[1];
						txtEmployerAddress1.Text = straddr[2];
						txtEmployerAddress2.Text = straddr[3];
						txtEmployerAddress3.Text = straddr[4];
						txtEmployersFedEIN.Text = rsReport.Get_Fields_String("employersfederalid");
					}
					else
					{
						txtEmployerAddress1.Text = "";
						txtEmployerAddress2.Text = "";
						txtEmployerAddress3.Text = "";
						txtEmployerName.Text = "";
						txtEmployersFedEIN.Text = "";
					}
					strSQL = "select * from w2c where taxyear = " + FCConvert.ToString(lngYear) + " and [current] = 1 order by orderno";
					rsReport.OpenRecordset(strSQL, "twpy0000.vb1");
					if (rsReport.EndOfFile())
					{
						MessageBox.Show("No W-2C information found for year " + FCConvert.ToString(lngYear), "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (boolW2)
					{
						txtW2Type.Text = "";
					}
					else
					{
						txtW2Type.Text = "C";
					}
					txtYear.Text = FCConvert.ToString(lngYear);
				}
				else
				{
					txtEmployerName.Text = "Employer Name";
					txtEmployerAddress1.Text = "Employer Addr 1";
					txtEmployerAddress2.Text = "Employer Addr 2";
					txtEmployerAddress3.Text = "Employer Addr 3";
					txtEmployersFedEIN.Text = "Fed EIN";
					intRecCount = 0;
					txtYear.Text = "9999";
					txtW2Type.Text = "C";
				}
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, false, showModal: modalDialog);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolPrintTest)
			{
				eArgs.EOF = rsReport.EndOfFile();
			}
			else
			{
				eArgs.EOF = intRecCount > 0;
				intRecCount += 1;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			object ControlName;
			// If Not boolPrintTest Then
			dblLaserLineAdjustment = modGlobalRoutines.SetW2LineAdjustment(this);
			dblHorizAdjust = modGlobalRoutines.SetW2HorizAdjustment(this);
			// End If
			// dblLaserLineAdjustment = dblLaserLineAdjustment + 3.5
			// 
			// For Each ControlName In Me.Detail.Controls
			// ControlName.Top = ControlName.Top + (200 * dblLaserLineAdjustment)
			// If boolMove Then
			// ControlName.Top = ControlName.Top + 200
			// End If
			// ControlName.Left = ControlName.Left + (dblHorizAdjust)
			// Next
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolPrintTest)
			{
				if (!rsReport.EndOfFile())
				{
					string[] straddr = new string[3 + 1];
					int X;
					string strTemp = "";
					for (X = 1; X <= 3; X++)
					{
						straddr[X] = "";
					}
					// X
					X = 1;
					if (FCConvert.ToString(rsReport.Get_Fields("address1")) != string.Empty)
					{
						straddr[X] = FCConvert.ToString(rsReport.Get_Fields("address1"));
						X += 1;
					}
					if (FCConvert.ToString(rsReport.Get_Fields("address2")) != string.Empty)
					{
						straddr[X] = FCConvert.ToString(rsReport.Get_Fields("address2"));
						X += 1;
					}
					strTemp = fecherFoundation.Strings.Trim(rsReport.Get_Fields("city") + " " + rsReport.Get_Fields("state"));
					strTemp = fecherFoundation.Strings.Trim(strTemp + " " + rsReport.Get_Fields_String("zip"));
					straddr[X] = strTemp;
					txtAddress1.Text = straddr[1];
					txtAddress2.Text = straddr[2];
					txtAddress3.Text = straddr[3];
					txtFirstName.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields("firstname") + " " + fecherFoundation.Strings.Trim(Strings.Left(rsReport.Get_Fields("middlename") + " ", 1)));
					txtLastName.Text = rsReport.Get_Fields_String("lastname");
					txtDesig.Text = rsReport.Get_Fields_String("desig");
					txtSSN.Text = rsReport.Get_Fields_String("ssn");
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("correctingssnorname")))
					{
						txtSSNNameCheck.Visible = true;
					}
					else
					{
						txtSSNNameCheck.Visible = false;
					}
					strTemp = fecherFoundation.Strings.Trim(rsReport.Get_Fields("prevfirstname") + " " + rsReport.Get_Fields("prevmiddlename"));
					strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strTemp + " " + rsReport.Get_Fields("prevlastname")) + " " + rsReport.Get_Fields("desig"));
					txtIncorrectName.Text = strTemp;
					txtIncorrectSSN.Text = rsReport.Get_Fields_String("prevssn");
					if (Conversion.Val(rsReport.Get_Fields("prevfederalWage")) != 0 || Conversion.Val(rsReport.Get_Fields("FEDERALWAGE")) != 0)
					{
						txtPrevWages.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("Prevfederalwage")), "0.00");
						txtCurrWages.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("federalwage")), "0.00");
						clsW2CSummary.TotalFederalWage += Conversion.Val(rsReport.Get_Fields("federalwage"));
						clsW2CSummary.PrevTotalFedWage += Conversion.Val(rsReport.Get_Fields("prevfederalwage"));
					}
					else
					{
						txtPrevWages.Text = "";
						txtCurrWages.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("ficawage")) != 0 || Conversion.Val(rsReport.Get_Fields("prevficawage")) != 0)
					{
						txtPrevSSWages.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevficawage")), "0.00");
						txtCurrSSWages.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("ficawage")), "0.00");
						clsW2CSummary.CurrFicaWages += Conversion.Val(rsReport.Get_Fields("ficawage"));
						clsW2CSummary.PrevFicaWages += Conversion.Val(rsReport.Get_Fields("prevficawage"));
					}
					else
					{
						txtPrevSSWages.Text = "";
						txtCurrSSWages.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("medicarewage")) != 0 || Conversion.Val(rsReport.Get_Fields("prevmedicarewage")) != 0)
					{
						txtPrevMedicare.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevmedicarewage")), "0.00");
						txtCurrMedicare.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("medicarewage")), "0.00");
						clsW2CSummary.CurrMedWages += Conversion.Val(rsReport.Get_Fields("medicarewage"));
						clsW2CSummary.PrevMedWages += Conversion.Val(rsReport.Get_Fields("prevmedicarewage"));
					}
					else
					{
						txtPrevMedicare.Text = "";
						txtCurrMedicare.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("sstips")) != 0 || Conversion.Val(rsReport.Get_Fields("prevsstips")) != 0)
					{
						txtPrevSSTips.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevsstips")), "0.00");
						txtCurrSSTips.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("sstips")), "0.00");
						clsW2CSummary.CurrSSTips += Conversion.Val(rsReport.Get_Fields("sstips"));
						clsW2CSummary.PrevSSTips += Conversion.Val(rsReport.Get_Fields("prevsstips"));
					}
					else
					{
						txtPrevSSTips.Text = "";
						txtCurrSSTips.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("nonqualifiedamounts")) != 0 || Conversion.Val(rsReport.Get_Fields("prevnonqualifiedamounts")) != 0)
					{
						txtPrevNonQualifiedPlans.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevnonqualifiedamounts")), "0.00");
						txtCurrNonQualifiedPlans.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("nonqualifiedamounts")), "0.00");
						clsW2CSummary.CurrNonQualified += Conversion.Val(rsReport.Get_Fields("nonqualifiedamounts"));
						clsW2CSummary.PrevNonQualified += Conversion.Val(rsReport.Get_Fields("prevnonqualifiedamounts"));
					}
					else
					{
						txtPrevNonQualifiedPlans.Text = "";
						txtCurrNonQualifiedPlans.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("federaltax")) != 0 || Conversion.Val(rsReport.Get_Fields("prevfederaltax")) != 0)
					{
						txtPrevFedTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevfederaltax")), "0.00");
						txtCurrFedTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("federaltax")), "0.00");
						clsW2CSummary.CurrFedTax += Conversion.Val(rsReport.Get_Fields("federaltax"));
						clsW2CSummary.PrevFedTax += Conversion.Val(rsReport.Get_Fields("prevfederaltax"));
					}
					else
					{
						txtPrevFedTax.Text = "";
						txtCurrFedTax.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("ficatax")) != 0 || Conversion.Val(rsReport.Get_Fields("prevficatax")) != 0)
					{
						txtPrevSSTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevficatax")), "0.00");
						txtCurrSSTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("ficatax")), "0.00");
						clsW2CSummary.CurrFicaTax += Conversion.Val(rsReport.Get_Fields("ficatax"));
						clsW2CSummary.PrevFicaTax += Conversion.Val(rsReport.Get_Fields("prevficatax"));
					}
					else
					{
						txtPrevSSTax.Text = "";
						txtCurrSSTax.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("medicaretax")) != 0 || Conversion.Val(rsReport.Get_Fields("prevmedicaretax")) != 0)
					{
						txtPrevMedTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevmedicaretax")), "0.00");
						txtCurrMedTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("medicaretax")), "0.00");
						clsW2CSummary.CurrMedTax += Conversion.Val(rsReport.Get_Fields("medicaretax"));
						clsW2CSummary.PrevMedTax += Conversion.Val(rsReport.Get_Fields("prevmedicaretax"));
					}
					else
					{
						txtPrevMedTax.Text = "";
						txtCurrMedTax.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("allocatedtips")) != 0 || Conversion.Val(rsReport.Get_Fields("prevallocatedtips")) != 0)
					{
						txtPrevAllocatedTips.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevallocatedtips")), "0.00");
						txtCurrAllocatedTips.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("allocatedtips")), "0.00");
						clsW2CSummary.CurrAllocatedTips += Conversion.Val(rsReport.Get_Fields("allocatedtips"));
						clsW2CSummary.PrevAllocatedTips += Conversion.Val(rsReport.Get_Fields("prevallocatedtips"));
					}
					else
					{
						txtPrevAllocatedTips.Text = "";
						txtCurrAllocatedTips.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("dependentcare")) != 0 || Conversion.Val(rsReport.Get_Fields("prevdependentcare")) != 0)
					{
						txtPrevDependentCare.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevdependentcare")), "0.00");
						txtCurrDependentCare.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("dependentcare")), "0.00");
						clsW2CSummary.CurrDependentCare += Conversion.Val(rsReport.Get_Fields("dependentcare"));
						clsW2CSummary.PrevDependentCare += Conversion.Val(rsReport.Get_Fields("prevdependentcare"));
					}
					else
					{
						txtPrevDependentCare.Text = "";
						txtCurrDependentCare.Text = "";
					}
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("w2retirement")))
					{
						txtCurrRetirementPlan.Text = "X";
					}
					else
					{
						txtCurrRetirementPlan.Text = "";
					}
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("prevw2retirement")))
					{
						txtPrevRetirementPlan.Text = "X";
					}
					else
					{
						txtPrevRetirementPlan.Text = "";
					}
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("w2statutoryemployee")))
					{
						txtCurrStatutory.Text = "X";
					}
					else
					{
						txtCurrStatutory.Text = "";
					}
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("prevw2statutoryemployee")))
					{
						txtPrevStatutory.Text = "X";
					}
					else
					{
						txtPrevStatutory.Text = "";
					}
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("w2deferred")))
					{
						txtCurrThirdParty.Text = "X";
					}
					else
					{
						txtCurrThirdParty.Text = "";
					}
					if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("prevw2deferred")))
					{
						txtPrevThirdParty.Text = "X";
					}
					else
					{
						txtPrevThirdParty.Text = "";
					}
					txtPrevStateName.Text = rsReport.Get_Fields_String("prevstatename");
					txtCurrStateName.Text = rsReport.Get_Fields_String("statename");
					txtPrevStateID.Text = rsReport.Get_Fields_String("prevstateid");
					txtCurrStateID.Text = rsReport.Get_Fields_String("stateid");
					// clsW2CSummary.IncorrectStateID = .Fields("stateid")
					if (Conversion.Val(rsReport.Get_Fields("prevstatewage")) != 0 || Conversion.Val(rsReport.Get_Fields("statewage")) != 0)
					{
						txtPrevStateWages.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevstatewage")), "0.00");
						txtCurrStateWages.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("statewage")), "0.00");
						clsW2CSummary.CurrStateWage += Conversion.Val(rsReport.Get_Fields("statewage"));
						clsW2CSummary.PrevStateWage += Conversion.Val(rsReport.Get_Fields("prevstatewage"));
					}
					else
					{
						txtPrevStateWages.Text = "";
						txtCurrStateWages.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("prevstatetax")) != 0 || Conversion.Val(rsReport.Get_Fields("statetax")) != 0)
					{
						txtPrevStateTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevstatetax")), "0.00");
						txtCurrStateTax.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("statetax")), "0.00");
						clsW2CSummary.CurrStateTax += Conversion.Val(rsReport.Get_Fields("statetax"));
						clsW2CSummary.PrevStateTax += Conversion.Val(rsReport.Get_Fields("prevstatetax"));
					}
					else
					{
						txtPrevStateTax.Text = "";
						txtCurrStateTax.Text = "";
					}
					
					txtPrevStateName2.Text = FCConvert.ToString(rsReport.Get_Fields("prevstatename2"));
					txtCurrStateName2.Text = FCConvert.ToString(rsReport.Get_Fields("statename2"));
					txtPrevStateID2.Text = FCConvert.ToString(rsReport.Get_Fields("prevstateid2"));
					txtCurrStateID2.Text = FCConvert.ToString(rsReport.Get_Fields("stateid2"));
					if (Conversion.Val(rsReport.Get_Fields("prevstatewage2")) != 0 || Conversion.Val(rsReport.Get_Fields("statewage2")) != 0)
					{
						txtPrevStateWages2.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevstatewage2")), "0.00");
						txtCurrStateWages2.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("statewage2")), "0.00");
						clsW2CSummary.CurrStateWage += Conversion.Val(rsReport.Get_Fields("statewage2"));
						clsW2CSummary.PrevStateWage += Conversion.Val(rsReport.Get_Fields("prevstatewage2"));
					}
					else
					{
						txtPrevStateWages2.Text = "";
						txtCurrStateWages2.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields("prevstatetax2")) != 0 || Conversion.Val(rsReport.Get_Fields("statetax2")) != 0)
					{
						txtPrevStateTax2.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("prevstatetax2")), "0.00");
						txtCurrStateTax2.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("statetax2")), "0.00");
						clsW2CSummary.CurrStateTax += Conversion.Val(rsReport.Get_Fields("statetax2"));
						clsW2CSummary.PrevStateTax += Conversion.Val(rsReport.Get_Fields("prevstatetax2"));
					}
					else
					{
						txtPrevStateTax2.Text = "";
						txtCurrStateTax2.Text = "";
					}
					
					txtPrev12A.Text = "";
					txtPrev12ACode.Text = "";
					txtPrev12B.Text = "";
					txtPrev12BCode.Text = "";
					txtPrev12C.Text = "";
					txtPrev12CCode.Text = "";
					txtPrev12D.Text = "";
					txtPrev12DCode.Text = "";
					txtCurr12A.Text = "";
					txtCurr12ACode.Text = "";
					txtCurr12B.Text = "";
					txtCurr12BCode.Text = "";
					txtCurr12C.Text = "";
					txtCurr12CCode.Text = "";
					txtCurr12D.Text = "";
					txtCurr12DCode.Text = "";
					// clsW2CSummary.CurrBox12 = 0
					// clsW2CSummary.PrevBox12 = 0
					clsDRWrapper rsTemp = new clsDRWrapper();
					rsTemp.OpenRecordset("select * from w2cbox1214 where box12 = 1 and recordid = " + rsReport.Get_Fields("ID") + " order by line", "twpy0000.vb1");
					while (!rsTemp.EndOfFile())
					{
						// If Trim(rsTemp.Fields("code")) <> vbNullString Or Trim(rsTemp.Fields("prevcode")) <> vbNullString Then
						if (Conversion.Val(rsTemp.Get_Fields("line")) >= 1 && Conversion.Val(rsTemp.Get_Fields("line")) < 5)
						{
							switch (rsTemp.Get_Fields_Int32("line"))
							{
								case 1:
									{
										txtPrev12ACode.Text = FCConvert.ToString(rsTemp.Get_Fields("prevcode"));
										txtCurr12ACode.Text = FCConvert.ToString(rsTemp.Get_Fields("code"));
										txtPrev12A.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
										txtCurr12A.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
										clsW2CSummary.CurrBox12 += Conversion.Val(rsTemp.Get_Fields("amount"));
										clsW2CSummary.PrevBox12 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
										break;
									}
								case 2:
									{
										txtPrev12BCode.Text = FCConvert.ToString(rsTemp.Get_Fields("prevcode"));
										txtCurr12BCode.Text = FCConvert.ToString(rsTemp.Get_Fields("code"));
										txtPrev12B.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
										txtCurr12B.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
										clsW2CSummary.CurrBox12 += Conversion.Val(rsTemp.Get_Fields("amount"));
										clsW2CSummary.PrevBox12 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
										break;
									}
								case 3:
									{
										txtPrev12CCode.Text = FCConvert.ToString(rsTemp.Get_Fields("prevcode"));
										txtCurr12CCode.Text = FCConvert.ToString(rsTemp.Get_Fields("code"));
										txtPrev12C.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
										txtCurr12C.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
										clsW2CSummary.CurrBox12 += Conversion.Val(rsTemp.Get_Fields("amount"));
										clsW2CSummary.PrevBox12 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
										break;
									}
								case 4:
									{
										txtPrev12DCode.Text = rsTemp.Get_Fields_String("prevcode");
										txtCurr12DCode.Text = rsTemp.Get_Fields_String("code");
										txtPrev12D.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
										txtCurr12D.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
										clsW2CSummary.CurrBox12 += Conversion.Val(rsTemp.Get_Fields("amount"));
										clsW2CSummary.PrevBox12 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
										break;
									}
							}
							//end switch
						}
						// End If
						rsTemp.MoveNext();
					}
					txtPrev14A.Text = "";
					txtPrev14B.Text = "";
					txtCurr14A.Text = "";
					txtCurr14B.Text = "";
					// clsW2CSummary.CurrBox14 = 0
					// clsW2CSummary.PrevBox14 = 0
					rsTemp.OpenRecordset("select * from w2cbox1214 where box14 = 1 and recordid = " + rsReport.Get_Fields("ID") + " order by line", "twpy0000.vb1");
					while (!rsTemp.EndOfFile())
					{
						if (Conversion.Val(rsTemp.Get_Fields("line")) >= 1 && Conversion.Val(rsTemp.Get_Fields("line")) < 4)
						{
							if (Conversion.Val(rsTemp.Get_Fields("line")) == 1)
							{
								txtPrev14A.Text = rsTemp.Get_Fields_String("prevcode") + " " + Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
								txtCurr14A.Text = rsTemp.Get_Fields_String("code") + " " + Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
								clsW2CSummary.CurrBox14 += Conversion.Val(rsTemp.Get_Fields("amount"));
								clsW2CSummary.PrevBox14 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
							}
							else if (Conversion.Val(rsTemp.Get_Fields("line")) == 2)
							{
								txtPrev14B.Text = rsTemp.Get_Fields_String("prevcode") + " " + Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
								txtCurr14B.Text = rsTemp.Get_Fields_String("code") + " " + Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
								clsW2CSummary.CurrBox14 += Conversion.Val(rsTemp.Get_Fields("amount"));
								clsW2CSummary.PrevBox14 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
							}
							else if (Conversion.Val(rsTemp.Get_Fields("line")) == 3)
							{
								txtPrev14C.Text = rsTemp.Get_Fields_String("prevcode") + " " + Strings.Format(Conversion.Val(rsTemp.Get_Fields("prevamount")), "0.00");
								txtCurr14C.Text = rsTemp.Get_Fields_String("code") + " " + Strings.Format(Conversion.Val(rsTemp.Get_Fields("amount")), "0.00");
								clsW2CSummary.CurrBox14 += Conversion.Val(rsTemp.Get_Fields("amount"));
								clsW2CSummary.PrevBox14 += Conversion.Val(rsTemp.Get_Fields("prevamount"));
							}
						}
						rsTemp.MoveNext();
					}
					rsReport.MoveNext();
				}
			}
			else
			{
				txtAddress1.Text = "Address 1";
				txtAddress2.Text = "Address 2";
				txtAddress3.Text = "Address 3";
				txtFirstName.Text = "FirstName ";
				txtLastName.Text = "LastName";
				txtDesig.Text = "Sr.";
				txtSSN.Text = "000-00-0000";
				txtSSNNameCheck.Visible = true;
				txtIncorrectName.Text = "Previous Name";
				txtIncorrectSSN.Text = "Previous SSN";
				txtPrevWages.Text = "000.00";
				txtCurrWages.Text = "000.00";
				txtPrevMedicare.Text = "000.00";
				txtCurrMedicare.Text = "000.00";
				txtPrevNonQualifiedPlans.Text = "000.00";
				txtCurrNonQualifiedPlans.Text = "000.00";
				txtPrevFedTax.Text = "000.00";
				txtCurrFedTax.Text = "000.00";
				txtPrevSSTax.Text = "000.00";
				txtCurrSSTax.Text = "000.00";
				txtPrevMedTax.Text = "000.00";
				txtCurrMedTax.Text = "000.00";
				txtPrevAllocatedTips.Text = "000.00";
				txtCurrAllocatedTips.Text = "000.00";
				txtPrevDependentCare.Text = "000.00";
				txtCurrDependentCare.Text = "000.00";
				txtCurrRetirementPlan.Text = "X";
				txtPrevRetirementPlan.Text = "X";
				txtCurrStatutory.Text = "X";
				txtPrevStatutory.Text = "X";
				txtCurrThirdParty.Text = "X";
				txtPrevThirdParty.Text = "X";
				txtPrevStateName.Text = "XX";
				txtCurrStateName.Text = "XX";
				txtPrevStateID.Text = "State ID";
				txtCurrStateID.Text = "State ID";
				txtPrevStateWages.Text = "000.00";
				txtCurrStateWages.Text = "000.00";
				txtPrevStateTax.Text = "000.00";
				txtCurrStateTax.Text = "000.00";
				txtPrevStateName2.Text = "State";
				txtCurrStateName2.Text = "State";
				txtPrevStateID2.Text = "State ID";
				txtCurrStateID2.Text = "State ID";
				txtPrevStateWages2.Text = "000.00";
				txtCurrStateWages2.Text = "000.00";
				txtPrevStateTax2.Text = "000.00";
				txtCurrStateTax2.Text = "000.00";
				txtPrev12A.Text = "000.00";
				txtPrev12ACode.Text = "X";
				txtPrev12B.Text = "000.00";
				txtPrev12BCode.Text = "X";
				txtPrev12C.Text = "000.00";
				txtPrev12CCode.Text = "X";
				txtPrev12D.Text = "000.00";
				txtPrev12DCode.Text = "X";
				txtCurr12A.Text = "000.00";
				txtCurr12ACode.Text = "X";
				txtCurr12B.Text = "000.00";
				txtCurr12BCode.Text = "X";
				txtCurr12C.Text = "000.00";
				txtCurr12CCode.Text = "X";
				txtCurr12D.Text = "000.00";
				txtCurr12DCode.Text = "X";
				txtPrev14A.Text = "X 000.00";
				txtPrev14B.Text = "X 000.00";
				txtCurr14A.Text = "X 000.00";
				txtCurr14B.Text = "X 000.00";
			}
		}
	}
}
