﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayrollAccountingChargesReversal.
	/// </summary>
	partial class rptPayrollAccountingChargesReversal
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPayrollAccountingChargesReversal));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader4 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter4 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckReturn = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.EmployeeBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldEmployeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Subreport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblBadAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.EmployeeBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBadAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAmount,
				this.fldAccount
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblPayDate,
				this.Label8,
				this.Line2,
				this.Label9,
				this.Label10,
				this.lblCheckReturn
			});
			this.PageHeader.Height = 0.9791667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader4
			// 
			this.GroupHeader4.Height = 0F;
			this.GroupHeader4.Name = "GroupHeader4";
			// 
			// GroupFooter4
			//
			// 
			this.GroupFooter4.Format += new System.EventHandler(this.GroupFooter4_Format);
			this.GroupFooter4.CanShrink = true;
			this.GroupFooter4.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBadAccount
			});
			this.GroupFooter4.Name = "GroupFooter4";
			// 
			// GroupHeader3
			// 
			this.GroupHeader3.Height = 0F;
			this.GroupHeader3.Name = "GroupHeader3";
			// 
			// GroupFooter3
			//
			// 
			this.GroupFooter3.Format += new System.EventHandler(this.GroupFooter3_Format);
			this.GroupFooter3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label13,
				this.Subreport1
			});
			this.GroupFooter3.Height = 0.46875F;
			this.GroupFooter3.Name = "GroupFooter3";
			this.GroupFooter3.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label12,
				this.Line5,
				this.fldFinalTotal
			});
			this.GroupFooter1.Height = 0.40625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			//
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.EmployeeBinder,
				this.fldEmployee
			});
			this.GroupHeader2.DataField = "EmployeeBinder";
			this.GroupHeader2.Height = 0.1875F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			//
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label11,
				this.Line4,
				this.fldEmployeeTotal
			});
			this.GroupFooter2.Height = 0.21875F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Payroll Accounting Charges";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Height = 0.21875F;
			this.lblPayDate.HyperLink = null;
			this.lblPayDate.Left = 1.5F;
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblPayDate.Text = null;
			this.lblPayDate.Top = 0.21875F;
			this.lblPayDate.Width = 4.6875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Employee";
			this.Label8.Top = 0.78125F;
			this.Label8.Width = 0.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.96875F;
			this.Line2.Width = 7.46875F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.46875F;
			this.Line2.Y1 = 0.96875F;
			this.Line2.Y2 = 0.96875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.03125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label9.Text = "Amount";
			this.Label9.Top = 0.78125F;
			this.Label9.Width = 0.96875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4.15625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label10.Text = "Charge Account";
			this.Label10.Top = 0.78125F;
			this.Label10.Width = 1.375F;
			// 
			// lblCheckReturn
			// 
			this.lblCheckReturn.Height = 0.21875F;
			this.lblCheckReturn.HyperLink = null;
			this.lblCheckReturn.Left = 1.5F;
			this.lblCheckReturn.Name = "lblCheckReturn";
			this.lblCheckReturn.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblCheckReturn.Text = null;
			this.lblCheckReturn.Top = 0.4375F;
			this.lblCheckReturn.Width = 4.6875F;
			// 
			// EmployeeBinder
			// 
			this.EmployeeBinder.DataField = "EmployeeBinder";
			this.EmployeeBinder.Height = 0.125F;
			this.EmployeeBinder.Left = 6.78125F;
			this.EmployeeBinder.Name = "EmployeeBinder";
			this.EmployeeBinder.Text = "EmployeeBinder";
			this.EmployeeBinder.Top = 0.03125F;
			this.EmployeeBinder.Visible = false;
			this.EmployeeBinder.Width = 0.59375F;
			// 
			// fldEmployee
			// 
			this.fldEmployee.Height = 0.1875F;
			this.fldEmployee.Left = 0F;
			this.fldEmployee.Name = "fldEmployee";
			this.fldEmployee.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldEmployee.Text = "Field1";
			this.fldEmployee.Top = 0F;
			this.fldEmployee.Width = 3.15625F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 3F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.96875F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 4.15625F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 3.09375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.8125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label11.Text = "Total";
			this.Label11.Top = 0.03125F;
			this.Label11.Width = 1.15625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 1.78125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0F;
			this.Line4.Width = 2.21875F;
			this.Line4.X1 = 4F;
			this.Line4.X2 = 1.78125F;
			this.Line4.Y1 = 0F;
			this.Line4.Y2 = 0F;
			// 
			// fldEmployeeTotal
			// 
			this.fldEmployeeTotal.Height = 0.1875F;
			this.fldEmployeeTotal.Left = 3.03125F;
			this.fldEmployeeTotal.Name = "fldEmployeeTotal";
			this.fldEmployeeTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldEmployeeTotal.Text = "Field1";
			this.fldEmployeeTotal.Top = 0.03125F;
			this.fldEmployeeTotal.Width = 0.96875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.8125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label12.Text = "Final Total";
			this.Label12.Top = 0.21875F;
			this.Label12.Width = 1.15625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 1.78125F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.1875F;
			this.Line5.Width = 2.21875F;
			this.Line5.X1 = 4F;
			this.Line5.X2 = 1.78125F;
			this.Line5.Y1 = 0.1875F;
			this.Line5.Y2 = 0.1875F;
			// 
			// fldFinalTotal
			// 
			this.fldFinalTotal.Height = 0.1875F;
			this.fldFinalTotal.Left = 3.03125F;
			this.fldFinalTotal.Name = "fldFinalTotal";
			this.fldFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalTotal.Text = "Field1";
			this.fldFinalTotal.Top = 0.21875F;
			this.fldFinalTotal.Width = 0.96875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.21875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.40625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label13.Text = "Summary";
			this.Label13.Top = 0F;
			this.Label13.Width = 4.6875F;
			// 
			// Subreport1
			// 
			this.Subreport1.CloseBorder = false;
			this.Subreport1.Height = 0.19F;
			this.Subreport1.Left = 0F;
			this.Subreport1.Name = "Subreport1";
			this.Subreport1.Report = null;
			this.Subreport1.Top = 0.3125F;
			this.Subreport1.Width = 7.5F;
			// 
			// lblBadAccount
			// 
			this.lblBadAccount.Height = 0.21875F;
			this.lblBadAccount.HyperLink = null;
			this.lblBadAccount.Left = 2.96875F;
			this.lblBadAccount.Name = "lblBadAccount";
			this.lblBadAccount.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblBadAccount.Text = "** - Bad Account";
			this.lblBadAccount.Top = 0F;
			this.lblBadAccount.Width = 1.5625F;
			// 
			// rptPayrollAccountingChargesReversal
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader4);
			this.Sections.Add(this.GroupHeader3);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter3);
			this.Sections.Add(this.GroupFooter4);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.EmployeeBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBadAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckReturn;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBadAccount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport Subreport1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox EmployeeBinder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmployeeTotal;
	}
}
