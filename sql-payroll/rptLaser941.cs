//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptLaser941.
	/// </summary>
	public partial class rptLaser941 : BaseSectionReport
	{
		public rptLaser941()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "941";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLaser941 InstancePtr
		{
			get
			{
				return (rptLaser941)Sys.GetInstance(typeof(rptLaser941));
			}
		}

		protected rptLaser941 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLaser941	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intQuarterUsed;
		int lngYearUsed;
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolSemiWeekly;
		double dblDepositsMade;
		string strSequence = "";
		bool boolForceScheduleB;
		private cFed941ViewContext viewContext;

		public cFed941ViewContext ReportContext
		{
			get
			{
				cFed941ViewContext ReportContext = null;
				ReportContext = viewContext;
				return ReportContext;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strTemp = "";
                // vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
                DateTime dtStart = DateTime.FromOADate(0);
                DateTime dtEnd = DateTime.FromOADate(0);
                string strSQL = "";
                int lngLine1 = 0;
                double dblLine2 = 0;
                double dblTotalIncomeTaxWithheld = 0;
                // vbPorter upgrade warning: dblLine5aC1 As double	OnWrite(string)
                double dblLine5aC1 = 0;
                double dblLine5aC2 = 0;
                double dblLine5bC1 = 0;
                double dblLine5bC2 = 0;
                double dblLine5aiC1 = 0;
                double dblLine5aiC2 = 0;
                double dblLine5aiiC1 = 0;
                double dblLine5aiiC2 = 0;

                // vbPorter upgrade warning: dblLine5cC1 As double	OnWrite(string)
                double dblLine5cC1 = 0;
                double dblLine5cC2 = 0;
                double dblTotSSandMedicareTaxes = 0;
                // vbPorter upgrade warning: dblFractionsOfCents As double	OnWrite(string)
                double dblFractionsOfCents = 0;
                double dblSickPayAdjustment = 0;
                double dblTipsAndInsAdjustment = 0;
                double dblIncomeTaxAdjustment = 0;
                double dblSSandMedicareAdjustment = 0;
                double dblTotalAdjustments = 0;
                double dblLine10 = 0;
                double dblTotTaxAfterAdjustment = 0;
                double dblEICPayments = 0;
                double dblTotTaxesAfterEIC = 0;
                double dblTotalDeposits = 0;
                double dblBalanceDue = 0;
                double dblLine17a = 0;
                double dblLine17b = 0;
                double dblLine17c = 0;
                int intMonth = 0;
                string[] strAry = null;
                string strWhere;
                string strSQL2 = "";
                string strSQL3 = "";
                // vbPorter upgrade warning: dtMarchStart As DateTime	OnWrite(string)
                DateTime dtMarchStart;
                // vbPorter upgrade warning: dtMarchEnd As DateTime	OnWrite(string)
                DateTime dtMarchEnd;
                double dblSumTaxWH;
                double dblCobraPayments = 0;
                int lngCobraIndividuals = 0;
                // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
                int x;
                // vbPorter upgrade warning: dblW2Fica As double	OnWrite(int, string)
                double dblW2Fica;
                // vbPorter upgrade warning: dblW2Med As double	OnWrite(int, string)
                double dblW2Med;
                double dblW2Sum;
                double dblBox1Matches;
                string strSQLShouldve = "";
                object lngFirstExemptEmps;
                // 6a
                object lngExemptEmpts;
                // 6b
                object dblExemptWages;
                // 6c
                double dblExemptTax = 0;
                // 6d
                object lngNumMarch;
                // 12c
                object dblExemptMarch;
                // 12d
                object dblExemptTaxMarch;
                // 12e

                double dblQualifiedSickLeaveWages; // 5ai
                double dblQualifiedFamilyLeaveWages; //5aii
                double dblNonrefundableCreditSFLeaveWages; //11b
                double dblNonRefundableEmplRetentionCredit; //11c
                double dblTotalAfterAdjAndCredits; //12
                double dblRefundableCreditForLeaveWages; //13c
                double dblRefundableEmplRetentionCredit; //13d
                double dblTotDepositsDeferralsRefundableCredits; //13e
                double dblTotAdvancesFromForm7200; //13f
                double dblTotDepositsDeferralsRefCreditsLessAdvances; //13g
                double dblQualifiedExpensesAllocableToSickLeaveWages; //19
                double dblQualifiedExpensesAllocableToFamilyLeaveWages; //20
                double dblQualifiedWagesForEmplRetentionCredit; //21
                double dblQualifiedExpensesAllocableToLine21Wages; //22
                double dblCreditFromForm5884C; //23
                
                txtName.Text = EWRRecord.EmployerName;
                txtName2.Text = txtName.Text;
                txtName3.Text = txtName.Text;

                txtAddress.Text = EWRRecord.EmployerAddress;
                if (Conversion.Val(EWRRecord.TransmitterPhone) > 0)
                {
                    strTemp = Strings.Format(EWRRecord.TransmitterPhone, "0000000000");
                    strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" +
                              Strings.Mid(strTemp, 7);
                    txtTransmitterPhone.Text = strTemp;
                }

                txtTransmitterName.Text = EWRRecord.TransmitterName;
                txtTransmitterTitle.Text = EWRRecord.TransmitterTitle;
                txtCity.Text = EWRRecord.EmployerCity;
                txtState.Text = EWRRecord.EmployerState;
                txtZip.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerZip + " " + EWRRecord.EmployerZip4);
                for (x = 1; x <= (EWRRecord.FederalEmployerID.Length); x++)
                {
                    (Detail.Controls["txtEINBox" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        Strings.Mid(EWRRecord.FederalEmployerID, x, 1);
                }

                // x
                txtEmployerID.Text = EWRRecord.FederalEmployerID;
                txtEmployerID2.Text = EWRRecord.FederalEmployerID;
                strTemp = EWRRecord.FederalEmployerID;
                string strMatchBox1List;
                strMatchBox1List = modCoreysSweeterCode.GetBox1Matches();
                clsLoad.OpenRecordset(
                    "select * from tblFed941 where quarter = " + FCConvert.ToString(intQuarterUsed) +
                    " and reportyear = " + FCConvert.ToString(lngYearUsed), "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    dblIncomeTaxAdjustment =
                        Conversion.Val(clsLoad.Get_Fields_Double("CurrentYearIncomeTaxWithholdingAdjustment"));
                    dblSSandMedicareAdjustment =
                        Conversion.Val(clsLoad.Get_Fields_Double("PriorQuarterSSAndMedicareAdjustment"));
                    dblSickPayAdjustment = Conversion.Val(clsLoad.Get_Fields_Double("SickPayAdjustment"));
                    dblTipsAndInsAdjustment =
                        Conversion.Val(clsLoad.Get_Fields_Double("TipsAndLifeInsuranceAdjustment"));
                    dblDepositsMade = Conversion.Val(clsLoad.Get_Fields_Double("Deposits"));
                    dblCobraPayments = Conversion.Val(clsLoad.Get_Fields_Double("CobraPayments"));
                    lngCobraIndividuals =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("CobraIndividuals"))));

                    dblQualifiedSickLeaveWages = Conversion.Val(clsLoad.Get_Fields_Double("QualifiedSickLeaveWages"));
                    dblQualifiedFamilyLeaveWages =
                        Conversion.Val(clsLoad.Get_Fields_Double("QualifiedFamilyLeaveWages"));
                    dblNonrefundableCreditSFLeaveWages =
                        Conversion.Val(clsLoad.Get_Fields_Double("NonRefundableCreditforLeaveWages"));
                    dblNonRefundableEmplRetentionCredit =
                        Conversion.Val(clsLoad.Get_Fields_Double("NonRefundableEmplRetentionCredit"));
                    dblRefundableCreditForLeaveWages =
                        Conversion.Val(clsLoad.Get_Fields_Double("RefundableCreditforLeaveWages"));
                    dblRefundableEmplRetentionCredit =
                        Conversion.Val(clsLoad.Get_Fields_Double("RefundableEmplRetentionCredit"));
                    dblTotAdvancesFromForm7200 = Conversion.Val(clsLoad.Get_Fields_Double("TotalAdvancesFromForm7200"));
                    dblQualifiedExpensesAllocableToSickLeaveWages =
                        Conversion.Val(clsLoad.Get_Fields_Double("QualifiedExpensesAllocableToSickLeaveWages"));
                    dblQualifiedExpensesAllocableToFamilyLeaveWages =
                        Conversion.Val(clsLoad.Get_Fields_Double("QualifiedExpensesAllocableToFamilyLeaveWages"));
                    dblQualifiedWagesForEmplRetentionCredit =
                        Conversion.Val(clsLoad.Get_Fields_Double("QualifiedWagesForEmplRetentionCredit"));
                    dblQualifiedExpensesAllocableToLine21Wages =
                        Conversion.Val(clsLoad.Get_Fields_Double("QualifiedExpensesAllocableToLine21Wages"));
                    dblCreditFromForm5884C = Conversion.Val(clsLoad.Get_Fields_Double("CreditFromForm5884C"));
                }
                else
                {
                    dblIncomeTaxAdjustment = 0;
                    dblSickPayAdjustment = 0;
                    dblSSandMedicareAdjustment = 0;
                    dblTipsAndInsAdjustment = 0;
                    dblCobraPayments = 0;
                    lngCobraIndividuals = 0;
                    dblQualifiedSickLeaveWages = 0;
                    dblQualifiedFamilyLeaveWages = 0;
                    dblNonrefundableCreditSFLeaveWages = 0;
                    dblNonRefundableEmplRetentionCredit = 0;
                    dblRefundableCreditForLeaveWages = 0;
                    dblRefundableEmplRetentionCredit = 0;
                    dblTotDepositsDeferralsRefundableCredits = 0;
                    dblTotAdvancesFromForm7200 = 0;
                    dblTotDepositsDeferralsRefCreditsLessAdvances = 0;
                    dblQualifiedExpensesAllocableToSickLeaveWages = 0;
                    dblQualifiedExpensesAllocableToFamilyLeaveWages = 0;
                    dblQualifiedWagesForEmplRetentionCredit = 0;
                    dblQualifiedExpensesAllocableToLine21Wages = 0;
                    dblCreditFromForm5884C = 0;
                }

                modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStart, ref dtEnd, intQuarterUsed, lngYearUsed);
                (Detail.Controls["txtQuarter" + intQuarterUsed] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                    .Text = "X";
                txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                dtMarchStart =
                    FCConvert.ToDateTime(Strings.Format(frmPrint941.InstancePtr.txtStartDate[0].Text, "MM/dd/yyyy"));
                dtMarchEnd =
                    FCConvert.ToDateTime(Strings.Format(frmPrint941.InstancePtr.txtEndDate[0].Text, "MM/dd/yyyy"));
                strWhere = "";
                // now look at data to get the rest
                if (strSequence == string.Empty || strSequence == "-1")
                {
                    strSQL =
                        "SELECT SUM(eicamount) as toteic, Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, sum(EmployerFicaTax) as SumOfEmployerFICATaxWH ,sum(EmployerMedicareTax) as SumOfEMployerMedicareTaxWH, Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail  Where ( checkvoid = 0) and  PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1";
                    strSQL2 =
                        "select count(employeenumber) as numemployees from (select employeenumber from tblcheckdetail where (checkvoid = 0) and  PayDate >= '" +
                        FCConvert.ToString(dtMarchStart) + "' And PayDate <= '" + FCConvert.ToString(dtMarchEnd) +
                        "' And TotalRecord = 1 group by employeenumber) as temp";
                    strSQL3 =
                        "SELECT Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, sum(EmployerFicaTAx) as SumOfEmployerFICATaxWH ,sum(EmployerMedicareTax) as SumOfEmployerMedicareTaxWH, Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail  Where  w2adjustment = 0 and ( checkvoid = 0) and  PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1";

                    strSQLShouldve =
                        "SELECT FICATaxGross,MedicareTaxGross FROM tblCheckDetail  Where  w2adjustment = 0 and ( checkvoid = 0) and  PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1";
                }
                else
                {
                    strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
                    if (Information.UBound(strAry, 1) > 0)
                    {
                        strWhere = " seqnumber between " + strAry[0] + " and " + strAry[1];
                    }
                    else
                    {
                        strWhere = " seqnumber = " + strAry[0];
                    }

                    strSQL =
                        "SELECT SUM(eicamount) as toteic, Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(LocalTaxGross) AS SumOfLocalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH,sum(EmployerFicaTax) as SumOfEmployerFicaTaxWH,sum(EmployerMedicareTax) as SumOfEmployerMedicareTaxWH , Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where (checkvoid = 0) and PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1 and " + strWhere;
                    strSQL2 =
                        "select count(tblcheckdetail.employeenumber) as numemployees from (select tblcheckdetail.employeenumber from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where (checkvoid = 0) and PayDate >= '" +
                        FCConvert.ToString(dtMarchStart) + "' And PayDate <= '" + FCConvert.ToString(dtMarchEnd) +
                        "' And TotalRecord = 1 and " + strWhere + " group by tblcheckdetail.employeenumber) as temp";
                    strSQL3 =
                        "SELECT Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, SUM(EmployerFicaTax) as SumOfEmployerFicaTaxWH ,sum(EmployerMedicareTax) as SumOfEmployerMedicareTaxWH, Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where w2adjustment = 0 and (checkvoid = 0) and PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1 and " + strWhere;
                    strSQLShouldve =
                        "SELECT FICATaxGross, MedicareTaxGross FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where w2adjustment = 0 and (checkvoid = 0) and PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1 and " + strWhere;
                }


                clsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    lngLine1 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("numemployees"))));
                }

                dblBox1Matches =
                    modCoreysSweeterCode.GetQTDBox1Matches(strMatchBox1List, "", dtEnd, DateTime.FromOADate(0));
                dblW2Fica = 0;
                dblW2Med = 0;
                dblW2Sum = 0;
                double dblShouldveFica;
                double dblShouldveMed;
                dblShouldveFica = 0;
                dblShouldveMed = 0;
                clsLoad.OpenRecordset(strSQLShouldve, "twpy0000.vb1");
                while (!clsLoad.EndOfFile())
                {
                    dblShouldveFica += FCConvert.ToDouble(Strings.Format(
                        FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ficataxgross")), "0.00")) *
                        0.062, "0.00"));
                    dblShouldveFica += FCConvert.ToDouble(Strings.Format(
                        FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ficataxgross")), "0.00")) *
                        0.062, "0.00"));
                    dblShouldveMed += FCConvert.ToDouble(Strings.Format(
                        FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("medicaretaxgross")),
                            "0.00")) * 0.0145, "0.00"));
                    dblShouldveMed += FCConvert.ToDouble(Strings.Format(
                        FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("medicaretaxgross")),
                            "0.00")) * 0.0145, "0.00"));
                    clsLoad.MoveNext();
                }

                clsLoad.OpenRecordset(strSQL3, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    dblW2Fica = FCConvert.ToDouble(
                        Strings.Format(Conversion.Val(clsLoad.Get_Fields("sumofficataxgross")), "0.00"));
                    dblW2Med = FCConvert.ToDouble(
                        Strings.Format(Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxgross")), "0.00"));
                }

                if (intQuarterUsed == 2 && lngYearUsed == 2010)
                {
                    txt12c.Visible = true;
                    txt12d.Visible = true;
                    txt12e.Visible = true;
                }
                else
                {
                    txt12c.Visible = false;
                    txt12d.Visible = false;
                    txt12e.Visible = false;
                }

                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    dblLine2 = Conversion.Val(clsLoad.Get_Fields("SumOfFederalTaxGross"));
                    dblLine2 += dblBox1Matches;
                    dblTotalIncomeTaxWithheld = Conversion.Val(clsLoad.Get_Fields("sumoffederaltaxwh"));
                    dblTotalIncomeTaxWithheld = dblTotalIncomeTaxWithheld;
                    dblLine5aC1 =
                        (FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("SumOfFicaTaxGross")),
                            "0.00")) - dblQualifiedSickLeaveWages - dblQualifiedFamilyLeaveWages);
                    dblLine5aC2 = Conversion.Val(Strings.Format(dblLine5aC1 * 0.124, "0.00"));
                    dblLine5bC1 = 0;
                    // tips
                    dblLine5bC2 = Conversion.Val(Strings.Format(dblLine5bC1 * 0.124, "0.00"));
                    dblLine5cC1 =
                        FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxgross")),
                            "0.00"));
                    dblLine5cC2 = Conversion.Val(Strings.Format(dblLine5cC1 * 0.029, "0.00"));

                    dblLine5aiC1 = dblQualifiedSickLeaveWages;
                    dblLine5aiC2 = Math.Round(dblQualifiedSickLeaveWages * 0.062, 2, MidpointRounding.AwayFromZero);
                    dblLine5aiiC1 = dblQualifiedFamilyLeaveWages;
                    dblLine5aiiC2 = Math.Round(dblQualifiedFamilyLeaveWages * 0.062, 2, MidpointRounding.AwayFromZero);

                    dblW2Sum = (Conversion.Val(Strings.Format(dblW2Fica * 0.124, "0.00"))) +
                               (Conversion.Val(Strings.Format(dblW2Med * 0.029, "0.00")));
                    dblTotSSandMedicareTaxes =
                        Conversion.Val(Strings.Format(
                            dblLine5cC2 + dblLine5aC2 + dblLine5bC2 + dblLine5aiC2 + dblLine5aiiC2, "0.00"));
                    dblSumTaxWH = Conversion.Val(clsLoad.Get_Fields("sumofficataxwh")) +
                                  Conversion.Val(clsLoad.Get_Fields("sumofemployerficataxwh")) +
                                  Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxwh")) +
                                  Conversion.Val(clsLoad.Get_Fields("sumofemployermedicaretaxwh"));
                    dblFractionsOfCents =
                        FCConvert.ToDouble(Strings.Format(((dblShouldveFica + dblShouldveMed)) - dblW2Sum, "0.00"));
                    dblTotalAdjustments = dblFractionsOfCents + dblSickPayAdjustment + dblTipsAndInsAdjustment;
                    dblLine10 = dblTotSSandMedicareTaxes + dblTotalAdjustments;
                    dblTotTaxAfterAdjustment =
                        Conversion.Val(Strings.Format(dblLine10 + dblTotalIncomeTaxWithheld - dblExemptTax, "0.00"));
                    dblEICPayments = Conversion.Val(clsLoad.Get_Fields("toteic"));
                    dblTotTaxesAfterEIC = Conversion.Val(dblTotTaxAfterAdjustment - dblEICPayments);
                    dblTotalAfterAdjAndCredits = dblTotTaxAfterAdjustment -
                                                 (dblNonrefundableCreditSFLeaveWages +
                                                  dblNonRefundableEmplRetentionCredit);
                    dblTotalDeposits = dblDepositsMade;

                    dblTotDepositsDeferralsRefundableCredits = dblTotalDeposits + 
                                                               dblRefundableCreditForLeaveWages +
                                                               dblRefundableEmplRetentionCredit;
                    dblTotDepositsDeferralsRefCreditsLessAdvances =
                        dblTotDepositsDeferralsRefundableCredits - dblTotAdvancesFromForm7200;

                    double dblCobraPlusDeposits = 0;
                    dblCobraPlusDeposits = dblCobraPayments + dblTotalDeposits;
                    if (dblTotDepositsDeferralsRefCreditsLessAdvances > dblTotalAfterAdjAndCredits)
                    {
                        dblBalanceDue =
                            Conversion.Val(Strings.Format(
                                dblTotalAfterAdjAndCredits - dblTotDepositsDeferralsRefCreditsLessAdvances, "0.00"));
                    }
                    else
                    {
                        dblBalanceDue =
                            Conversion.Val(Strings.Format(
                                dblTotDepositsDeferralsRefCreditsLessAdvances - dblTotalAfterAdjAndCredits, "0.00"));
                    }

                    txtLine1.Text = Strings.Format(lngLine1, "#,##0");
                    txtLine2.Text = Strings.Format(dblLine2, "#,###,###,##0.00");
                    txtLine3.Text = Strings.Format(dblTotalIncomeTaxWithheld, "#,###,###,##0.00");
                    txtLine5aC1.Text = Strings.Format(dblLine5aC1, "#,###,##0.00");
                    txtLine5aC2.Text = Strings.Format(dblLine5aC2, "#,###,##0.00");
                    txtLine5aiC1.Text = dblLine5aiC1.FormatAsCurrencyNoSymbol();
                    txtLine5aiC2.Text = dblLine5aiC2.FormatAsCurrencyNoSymbol();
                    txtLine5aiiC1.Text = dblLine5aiiC1.FormatAsCurrencyNoSymbol();
                    txtLine5aiiC2.Text = dblLine5aiiC2.FormatAsCurrencyNoSymbol();
                    txtLine5bC1.Text = Strings.Format(dblLine5bC1, "#,###,##0.00");
                    txtLine5bC2.Text = Strings.Format(dblLine5bC2, "#,###,##0.00");
                    txtLine5cC1.Text = Strings.Format(dblLine5cC1, "#,###,##0.00");
                    txtLine5cC2.Text = Strings.Format(dblLine5cC2, "#,###,##0.00");
                    txtLine5d.Text = Strings.Format(dblTotSSandMedicareTaxes, "#,###,##0.00");
                    txtLine6.Text = Strings.Format(dblTotSSandMedicareTaxes + dblTotalIncomeTaxWithheld - dblExemptTax,
                        "#,###,###,##0.00");
                    if (dblFractionsOfCents >= 0)
                    {
                        txtFractionsOfCents.Text = Strings.Format(dblFractionsOfCents, "#,###,##0.00");
                    }
                    else
                    {
                        txtFractionsOfCents.Text = "(" + Strings.Format(-dblFractionsOfCents, "#,###,##0.00") + ")";
                    }

                    if (dblSickPayAdjustment >= 0)
                    {
                        txtSickPayAdjustment.Text = Strings.Format(dblSickPayAdjustment, "#,###,##0.00");
                    }
                    else
                    {
                        txtSickPayAdjustment.Text = "(" + Strings.Format(-dblSickPayAdjustment, "#,###,##0.00") + ")";
                    }

                    if (dblSSandMedicareAdjustment >= 0)
                    {
                        txtSSandMedicareAdjustment.Text = Strings.Format(dblSSandMedicareAdjustment, "#,###,##0.00");
                    }
                    else
                    {
                        txtSSandMedicareAdjustment.Text =
                            "(" + Strings.Format(-dblSSandMedicareAdjustment, "#,###,##0.00") + ")";
                    }

                    if (dblTipsAndInsAdjustment >= 0)
                    {
                        txtTipsAdjustment.Text = Strings.Format(dblTipsAndInsAdjustment, "#,###,##0.00");
                    }
                    else
                    {
                        txtTipsAdjustment.Text = "(" + Strings.Format(-dblTipsAndInsAdjustment, "#,###,##0.00") + ")";
                    }

                    if (dblIncomeTaxAdjustment >= 0)
                    {
                        txtIncomeTaxAdjustment.Text = Strings.Format(dblIncomeTaxAdjustment, "#,###,##0.00");
                    }
                    else
                    {
                        txtIncomeTaxAdjustment.Text =
                            "(" + Strings.Format(-dblIncomeTaxAdjustment, "#,###,##0.00") + ")";
                    }

                    txtNonRefCreditSFLeaveWages.Text =
                        dblNonrefundableCreditSFLeaveWages.FormatAsCurrencyNoSymbol(); // 11b
                    txtNonrefEmplRetentionCredit.Text =
                        dblNonRefundableEmplRetentionCredit.FormatAsCurrencyNoSymbol(); //11c
                    txtTotNonRefundableCredits.Text =
                        (dblNonrefundableCreditSFLeaveWages + dblNonRefundableEmplRetentionCredit)
                        .FormatAsCurrencyNoSymbol(); //11d
                    txtRefCreditForLeaveWages.Text = dblRefundableCreditForLeaveWages.FormatAsCurrencyNoSymbol(); //13c
                    txtRefEmplRententionCredit.Text = dblRefundableEmplRetentionCredit.FormatAsCurrencyNoSymbol(); //13d
                    txtTotDepDefRefCredits.Text =
                        dblTotDepositsDeferralsRefundableCredits.FormatAsCurrencyNoSymbol(); //13e
                    txtTotAdvancesForm7200.Text = dblTotAdvancesFromForm7200.FormatAsCurrencyNoSymbol(); //13f
                    txtTotDepDefRefCreditsLessAdv.Text =
                        dblTotDepositsDeferralsRefCreditsLessAdvances.FormatAsCurrencyNoSymbol(); //13g

                    txtTotalAdjustments.Text = Strings.Format(dblTotalAdjustments, "#,###,##0.00");
                    txtTotTaxAfterAdjustment.Text = Strings.Format(dblTotTaxAfterAdjustment, "#,###,##0.00");
                    txtTotalAfterCredits.Text = Strings.Format(dblTotalAfterAdjAndCredits, "#,###,##0.00");
                    txtTotalDeposits.Text = Strings.Format(dblTotalDeposits, "#,###,##0.00");
                    txtCobraIndividuals.Text = Strings.Format(lngCobraIndividuals, "#,###,##0.00");
                    txtCobraPayments.Text = Strings.Format(dblCobraPayments, "#,###,##0.00");
                    txtCobraPlusDeposits.Text = Strings.Format(dblCobraPlusDeposits, "#,###,##0.00");

                    if (dblTotDepositsDeferralsRefCreditsLessAdvances > dblTotalAfterAdjAndCredits)
                    {
                        txtBalanceDue.Text = "";
                        txtOverpayment.Text = Strings.Format(dblBalanceDue, "#,###,##0.00");
                        if (MessageBox.Show(
                            "Do you want the overpayment to be refunded?" + "\r\n" +
                            "Responding no will apply it to the next return.", "Overpayment",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            txtOverPaymentRefund.Text = "X";
                            txtOverPaymentReturn.Text = "";
                        }
                        else
                        {
                            txtOverPaymentRefund.Text = "";
                            txtOverPaymentReturn.Text = "X";
                        }
                    }
                    else if (dblTotDepositsDeferralsRefCreditsLessAdvances > dblTotalAfterAdjAndCredits)
                    {
                        txtBalanceDue.Text = Strings.Format(dblBalanceDue, "#,###,##0.00");
                        txtOverpayment.Text = "";
                    }
                    else
                    {
                        txtOverpayment.Text = "";
                        txtBalanceDue.Text = "0.00";
                    }

                    if (dblTotalAfterAdjAndCredits >= 2500 || (boolSemiWeekly && boolForceScheduleB))
                    {
                        txtCheckLess2500.Text = "";
                        if (!boolSemiWeekly)
                        {
                            txtCheckMonthly.Text = "X";
                            txtCheckSemiWeekly.Text = "";
                            intMonth = (intQuarterUsed * 3) - 2;
                            dtStart = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" +
                                                           FCConvert.ToString(lngYearUsed));
                            dtEnd = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStart));
                            dblLine17a =
                                modCoreysSweeterCode.GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere, true);
                            dtStart = FCConvert.ToDateTime(FCConvert.ToString(intMonth + 1) + "/1/" +
                                                           FCConvert.ToString(lngYearUsed));
                            dtEnd = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStart));
                            dblLine17b =
                                modCoreysSweeterCode.GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere, true);
                            dtStart = FCConvert.ToDateTime(FCConvert.ToString(intMonth + 2) + "/1/" +
                                                           FCConvert.ToString(lngYearUsed));
                            dtEnd = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStart));
                            dblLine17c =
                                modCoreysSweeterCode.GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere, true);
                            txtLine17a.Text = Strings.Format(dblLine17a, "#,###,##0.00");
                            txtLine17b.Text = Strings.Format(dblLine17b, "#,###,##0.00");
                            txtLine17c.Text = Strings.Format(dblLine17c, "#,###,##0.00");
                            txtLine17d.Text = Strings.Format(dblLine17a + dblLine17b + dblLine17c, "#,###,###,##0.00");
                            ReportFooter.Visible = false;
                        }
                        else
                        {
                            txtCheckMonthly.Text = "";
                            txtCheckSemiWeekly.Text = "X";
                            // will have to fill out schedule B
                            SubReport1.Report = new srpt941ScheduleB();
                            if (strWhere != string.Empty)
                            {
                                SubReport1.Report.UserData =
                                    FCConvert.ToString(intQuarterUsed) + "," + FCConvert.ToString(lngYearUsed) + "," +
                                    strWhere;
                            }
                            else
                            {
                                SubReport1.Report.UserData =
                                    FCConvert.ToString(intQuarterUsed) + "," + FCConvert.ToString(lngYearUsed);
                            }
                        }

                        if (dblTotalAfterAdjAndCredits < 2500)
                        {
                            txtCheckSemiWeekly.Text = "";
                            txtCheckMonthly.Text = "";
                            txtCheckLess2500.Text = "X";
                        }
                    }
                    else
                    {
                        // dont fill out anything if less than 2500
                        txtCheckMonthly.Text = "";
                        txtCheckSemiWeekly.Text = "";
                        txtCheckLess2500.Text = "X";
                        ReportFooter.Visible = false;
                    }

                    txtQualfiedExpAllocToSickLeaveWages.Text =
                        dblQualifiedExpensesAllocableToSickLeaveWages.FormatAsCurrencyNoSymbol(); //19
                    txtQualifiedExpAllocToFamilyLeaveWages.Text =
                        dblQualifiedExpensesAllocableToFamilyLeaveWages.FormatAsCurrencyNoSymbol(); //20
                    txtQualifiedWagesForEmployeeRetentionCredit.Text =
                        dblQualifiedWagesForEmplRetentionCredit.FormatAsCurrencyNoSymbol(); //21
                    txtQualifiedExpAllocToLine21.Text =
                        dblQualifiedExpensesAllocableToLine21Wages.FormatAsCurrencyNoSymbol(); //22
                    txtCreditFromForm5884C.Text = dblCreditFromForm5884C.FormatAsCurrencyNoSymbol(); //23
                }
            }
        }

		public void Init(ref cFed941ViewContext theView, ref bool boolUseSemiWeekly, ref double dblDeposits, bool modalDialog, List<string> batchReports = null)
		{
			viewContext = theView;
			strSequence = theView.Sequence;
			intQuarterUsed = theView.ReportQuarter;
			lngYearUsed = theView.ReportYear;
			boolSemiWeekly = boolUseSemiWeekly;
			dblDepositsMade = dblDeposits;
			LoadInfo();
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// Me.PrintReport False
			}
			else
			{
				// Me.Show vbModal, MDIParent
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "941", showModal: modalDialog);
			}
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                    EWRRecord.TransmitterName = FCConvert.ToString(clsLoad.Get_Fields("transmittername"));
                    boolForceScheduleB = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ForceScheduleB"));
                }
                else
                {
                    boolForceScheduleB = false;
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                    EWRRecord.TransmitterName = "";
                }
            }
        }

		
	}
}
