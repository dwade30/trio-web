//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCheckReturn : BaseForm
	{
		public frmCheckReturn()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCheckReturn InstancePtr
		{
			get
			{
				return (frmCheckReturn)Sys.GetInstance(typeof(frmCheckReturn));
			}
		}

		protected frmCheckReturn _InstancePtr = null;
		//=========================================================
		private bool boolTAGrid;
		// vbPorter upgrade warning: lngCheckNumber As int	OnWrite(string)
		public int lngCheckNumber;
		private string strEmployeeNumber = "";
		// vbPorter upgrade warning: lngPayrunID As int	OnWrite(string)
		private int lngPayrunID;
		// vbPorter upgrade warning: pdatCheckDate As DateTime	OnWrite(string)
		private DateTime pdatCheckDate;
		public int lngJournalNumber;
		public int lngPeriodNumber;
		private bool pboolSelect;
		private bool boolGridSort;
		private bool blnNoRecords;

		public void Init(int lngPeriod)
		{
			lngPeriodNumber = lngPeriod;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmCheckReturn_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			if (blnNoRecords == true)
			{
				Close();
			}
		}

		private void frmCheckReturn_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (vsData.TextMatrix(0, 1) == "Ded Number")
			{
			}
			else
			{
				if (KeyAscii == Keys.Escape)
				{
					if (MessageBox.Show("Check return will not be completed. Continue to exit?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						KeyAscii = (Keys)0;
						Close();
					}
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCheckReturn_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolTAGrid = false;
				// SET THE SIZE OF THE FORM TO THE DEFAULT
				modGlobalFunctions.SetFixedSize(this, 0);
				lblCaption.Visible = false;
				Label1.Visible = false;
				SetGridParameters();
				LoadData();
				mnuExit.Visible = true;
				mnuSP1.Visible = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadData()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			DateTime dtTemp1;
			DateTime dtTemp2;
			DateTime dtDate;
			dtTemp1 = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(DateTime.Today);
			dtTemp1 = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp1);
			dtTemp1 = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtTemp1);
			dtDate = dtTemp1;
			blnNoRecords = false;
			// WE DO NOT WANT VOIDED CHECKS TO SHOW SO THEY CANNOT BE VOIDED A SECOND TIME.
			// .OpenRecordset "Select Distinct PayDate, PayRunID, EmployeeNumber, EmployeeName, CheckNumber from tblCheckDetail where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "' AND CheckNumber <> 0 Order by CheckNumber Desc", "TWPY0000.vb1"
			rsData.OpenRecordset("Select Distinct PayDate, PayRunID, EmployeeNumber, EmployeeName, CheckNumber from tblCheckDetail where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND CheckNumber <> 0 AND CheckVoid = 0 and TotalRecord = 1 and paydate >= '" + FCConvert.ToString(dtDate) + "' Order by PayDate Desc", "TWPY0000.vb1");
			vsData.Rows = rsData.RecordCount() + 1;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				if (FCConvert.ToInt32(rsData.Get_Fields("CheckNumber")) == 0)
				{
				}
				else
				{
					vsData.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
					vsData.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployeeName")));
					vsData.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields("CheckNumber")));
					vsData.TextMatrix(intCounter, 4, FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate")));
					vsData.TextMatrix(intCounter, 5, FCConvert.ToString(rsData.Get_Fields("PayRunID")));
					rsData.MoveNext();
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
			}
			if (intCounter == 1)
			{
				MessageBox.Show("This record has no data");
				blnNoRecords = true;
				return;
			}
		}

		private void LoadTAGrid(int lngCheckNumber, ref DateTime datReversalPayDate, ref int intReversalPayRunID)
		{
			// 1) = "Ded Number"
			// 2) = "Description"
			// 3) = "DeductionID"
			// 4) = "DeductionAccountNumber"
			// 5) = "Recipient"
			// 6) = "RecipientID"
			// 7) = "Category"
			// 8) = "CategoryID"
			// 9) = "Amount"
			// 10) = "Apply"
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsDedData = new clsDRWrapper();
			clsDRWrapper rsCategories = new clsDRWrapper();
			clsDRWrapper rsRecipients = new clsDRWrapper();
			clsDRWrapper rsAccounts = new clsDRWrapper();
			string strText = "";
            //FC:FINAL:SBE - #i2247 - exception is thrown from client side. Use BeginUpdate and EndUpdate to disable automatic update of datagridview during data load
            vsData.BeginUpdate();
            rsRecipients.OpenRecordset("Select * from tblRecipients Order By ID", "TWPY0000.vb1");
			rsCategories.OpenRecordset("Select * from tblCategories Order By ID", "TWPY0000.vb1");
			rsAccounts.OpenRecordset("Select * from tblPayrollAccounts Order By ID", "TWPY0000.vb1");
			vsData.Rows += 5;
			// deal with the first 5 hard coded categories
			// .OpenRecordset "Select * from tblCheckDetail where EmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' AND CheckNumber = " & lngCheckNumber & " AND TotalRecord = 1", "TWPY0000.vb1"
			rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND TotalRecord = 1 and paydate = '" + FCConvert.ToString(datReversalPayDate) + "' and payrunid = " + FCConvert.ToString(intReversalPayRunID), "TWPY0000.vb1");
			if (!rsData.EndOfFile())
				modGlobalVariables.Statics.gintUseGroupMultiFund = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int32("MultiFundNumber"))));
			for (intCounter = 1; intCounter <= 5; intCounter++)
			{
				vsData.TextMatrix(intCounter, 1, string.Empty);
				vsData.TextMatrix(intCounter, 2, string.Empty);
				vsData.TextMatrix(intCounter, 3, FCConvert.ToString(0));
				rsAccounts.FindFirstRecord("CategoryID", intCounter);
				if (rsAccounts.NoMatch)
				{
					vsData.TextMatrix(intCounter, 4, FCConvert.ToString(0));
				}
				else
				{
					// vsData.TextMatrix(intCounter, 4) = rsAccounts.Fields("Account")
					// ***********************************************************
					// MATTHEW MULTI CALL ID 79810 10/27/2005
					strText = FCConvert.ToString(rsAccounts.Get_Fields("Account"));
					if (strText.Length > 5 && modGlobalVariables.Statics.gintUseGroupMultiFund > 0)
					{
						// corey 01/03/06 Need to format Fund
						if (Strings.Left(fecherFoundation.Strings.Trim(strText), 2) == "**")
						{
							Strings.MidSet(ref strText, 5, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
						}
						else
						{
							Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
						}
					}
					vsData.TextMatrix(intCounter, 4, strText);
					// ***********************************************************
				}
				// FEDERAL = 1, FICA = 2, MEDICARE = 3, STATE = 4, LOCAL = 5
				rsCategories.FindFirstRecord("ID", intCounter);
				if (rsCategories.NoMatch)
				{
					vsData.TextMatrix(intCounter, 7, string.Empty);
					vsData.TextMatrix(intCounter, 8, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(intCounter, 7, FCConvert.ToString(rsCategories.Get_Fields("Description")));
					vsData.TextMatrix(intCounter, 8, FCConvert.ToString(rsCategories.Get_Fields("ID")));
					rsRecipients.FindFirstRecord("ID", rsCategories.Get_Fields("Include"));
					if (rsRecipients.NoMatch)
					{
						vsData.TextMatrix(intCounter, 5, string.Empty);
						vsData.TextMatrix(intCounter, 6, FCConvert.ToString(0));
					}
					else
					{
						vsData.TextMatrix(intCounter, 5, FCConvert.ToString(rsRecipients.Get_Fields_String("Name")));
						vsData.TextMatrix(intCounter, 6, FCConvert.ToString(rsRecipients.Get_Fields("ID")));
					}
				}
				if (intCounter == 1)
				{
					vsData.TextMatrix(intCounter, 9, FCConvert.ToString((rsData.Get_Fields("FederalTaxWH") * -1)));
				}
				else if (intCounter == 2)
				{
					// vsData.TextMatrix(intCounter, 9) = (.Fields("FICATaxWH") * 2) * -1
					vsData.TextMatrix(intCounter, 9, FCConvert.ToString((rsData.Get_Fields("ficataxwh") + rsData.Get_Fields("employerficatax")) * -1));
				}
				else if (intCounter == 3)
				{
					vsData.TextMatrix(intCounter, 9, FCConvert.ToString((rsData.Get_Fields("MedicareTaxWH") + rsData.Get_Fields_Double("EmployerMedicareTax")) * -1));
				}
				else if (intCounter == 4)
				{
					vsData.TextMatrix(intCounter, 9, FCConvert.ToString((rsData.Get_Fields("StateTaxWH") * -1)));
				}

				vsData.TextMatrix(intCounter, 10, FCConvert.ToString(0));
			}
			// remove rows that have zero as a balance
			for (intCounter = (vsData.Rows - 1); intCounter >= 1; intCounter--)
			{
				if (Conversion.Val(vsData.TextMatrix(intCounter, 9)) == 0)
				{
					vsData.RemoveItem(intCounter);
				}
				else if (vsData.TextMatrix(intCounter, 5) == "")
				{
					vsData.RemoveItem(intCounter);
				}
			}
			// deal with deduction records
			// .OpenRecordset "Select * from tblCheckDetail where EmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' AND CheckNumber = " & lngCheckNumber & " AND deductionrecord = 1", "TWPY0000.vb1"
			rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND deductionrecord = 1 and paydate = '" + FCConvert.ToString(datReversalPayDate) + "' and payrunid = " + FCConvert.ToString(intReversalPayRunID), "TWPY0000.vb1");
			vsData.Rows += rsData.RecordCount();
			for (intCounter = (vsData.Rows - rsData.RecordCount()); intCounter <= (vsData.Rows - 1); intCounter++)
			{
				rsCategories.OpenRecordset("Select * from tblCategories where Code = " + rsData.Get_Fields_Int32("DedDeductionNumber"), "TWPY0000.vb1");
				if (rsCategories.EndOfFile())
				{
				}
				else
				{
					rsDedData.OpenRecordset("Select * from tblDeductionSetup where ID = " + rsData.Get_Fields_Int32("DedDeductionNumber"), "TWPY0000.vb1");
					if (!rsDedData.EndOfFile())
					{
						vsData.TextMatrix(intCounter, 1, FCConvert.ToString(rsDedData.Get_Fields_Int32("DeductionNumber")));
						vsData.TextMatrix(intCounter, 2, FCConvert.ToString(rsDedData.Get_Fields("Description")));
						vsData.TextMatrix(intCounter, 3, FCConvert.ToString(rsDedData.Get_Fields("ID")));
						if (modGlobalVariables.Statics.gintUseGroupMultiFund == 0)
						{
							vsData.TextMatrix(intCounter, 4, FCConvert.ToString(rsDedData.Get_Fields_String("AccountID")));
						}
						else
						{
							// ***********************************************************
							// MATTHEW MULTI CALL ID 79810 10/27/2005
							strText = FCConvert.ToString(rsDedData.Get_Fields_String("AccountID"));
							if (strText.Length > 5)
							{
								// corey 01/03/2006 Need To format fund
								if (Strings.Left(fecherFoundation.Strings.Trim(strText), 2) == "**")
								{
									Strings.MidSet(ref strText, 5, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
								}
								else
								{
									Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
								}
							}
							vsData.TextMatrix(intCounter, 4, strText);
							// ***********************************************************
						}
					}
					vsData.TextMatrix(intCounter, 7, string.Empty);
					vsData.TextMatrix(intCounter, 8, FCConvert.ToString(rsCategories.Get_Fields("ID")));
					rsRecipients.FindFirstRecord("ID", rsCategories.Get_Fields("Include"));
					if (rsRecipients.NoMatch)
					{
						vsData.TextMatrix(intCounter, 5, string.Empty);
						vsData.TextMatrix(intCounter, 6, FCConvert.ToString(0));
					}
					else
					{
						vsData.TextMatrix(intCounter, 5, FCConvert.ToString(rsRecipients.Get_Fields_String("Name")));
						vsData.TextMatrix(intCounter, 6, FCConvert.ToString(rsRecipients.Get_Fields("ID")));
					}
					vsData.TextMatrix(intCounter, 9, FCConvert.ToString((FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("DedAmount"), "0.00")) * -1)));
					vsData.TextMatrix(intCounter, 10, FCConvert.ToString(0));
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 0, intCounter, 8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				rsData.MoveNext();
			}
			// remove rows that have zero as a balance
			for (intCounter = (vsData.Rows - 1); intCounter >= 1; intCounter--)
			{
				if (Conversion.Val(vsData.TextMatrix(intCounter, 9)) == 0)
				{
					vsData.RemoveItem(intCounter);
				}
			}
			// deal with Match Records
			// .OpenRecordset "Select * from tblCheckDetail where EmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' AND CheckNumber = " & lngCheckNumber & " AND matchrecord = 1", "TWPY0000.vb1"
			rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND matchrecord = 1 and paydate = '" + FCConvert.ToString(datReversalPayDate) + "' and payrunid = " + FCConvert.ToString(intReversalPayRunID), "TWPY0000.vb1");
			vsData.Rows += rsData.RecordCount();
			for (intCounter = (vsData.Rows - rsData.RecordCount()); intCounter <= (vsData.Rows - 1); intCounter++)
			{
				rsCategories.OpenRecordset("Select * from tblCategories where Code = " + rsData.Get_Fields_Int32("MatchDeductionNumber"), "TWPY0000.vb1");
				if (rsCategories.EndOfFile())
				{
				}
				else
				{
					rsDedData.OpenRecordset("Select * from tblDeductionSetup where ID = " + rsData.Get_Fields_Int32("MatchDeductionNumber"), "TWPY0000.vb1");
					if (!rsDedData.EndOfFile())
					{
						vsData.TextMatrix(intCounter, 1, FCConvert.ToString(rsDedData.Get_Fields_Int32("DeductionNumber")));
						vsData.TextMatrix(intCounter, 2, FCConvert.ToString(rsDedData.Get_Fields("Description")));
						vsData.TextMatrix(intCounter, 3, FCConvert.ToString(rsDedData.Get_Fields("ID")));
						if (modGlobalVariables.Statics.gintUseGroupMultiFund == 0)
						{
							vsData.TextMatrix(intCounter, 4, FCConvert.ToString(rsDedData.Get_Fields_String("AccountID")));
						}
						else
						{
							// ***********************************************************
							// MATTHEW MULTI CALL ID 79810 10/27/2005
							strText = FCConvert.ToString(rsDedData.Get_Fields_String("AccountID"));
							if (strText.Length > 5)
							{
								if (Strings.Left(fecherFoundation.Strings.Trim(strText), 2) == "**")
								{
									// Corey 01/03/2006 Need to format Fund
									Strings.MidSet(ref strText, 5, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
								}
								else
								{
									Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
								}
							}
							vsData.TextMatrix(intCounter, 4, strText);
							// ***********************************************************
						}
					}
					vsData.TextMatrix(intCounter, 7, string.Empty);
					vsData.TextMatrix(intCounter, 8, FCConvert.ToString(rsCategories.Get_Fields("ID")));
					rsRecipients.FindFirstRecord("ID", rsCategories.Get_Fields("Include"));
					if (rsRecipients.NoMatch)
					{
						vsData.TextMatrix(intCounter, 5, string.Empty);
						vsData.TextMatrix(intCounter, 6, FCConvert.ToString(0));
					}
					else
					{
						vsData.TextMatrix(intCounter, 5, FCConvert.ToString(rsRecipients.Get_Fields_String("Name")));
						vsData.TextMatrix(intCounter, 6, FCConvert.ToString(rsRecipients.Get_Fields("ID")));
					}
					vsData.TextMatrix(intCounter, 9, FCConvert.ToString((FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("MatchAmount"), "0.00")) * -1)));
					vsData.TextMatrix(intCounter, 10, FCConvert.ToString(0));
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 0, intCounter, 8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				rsData.MoveNext();
			}
			// remove rows that have zero as a balance
			for (intCounter = (vsData.Rows - 1); intCounter >= 1; intCounter--)
			{
				if (Conversion.Val(vsData.TextMatrix(intCounter, 9)) == 0)
				{
					vsData.RemoveItem(intCounter);
				}
				// SET THE LAST COLUMN TO ALWAYS BE BLANK SO THAT IT FORCES THE USER TO ENTER A DECISION
				if (intCounter <= vsData.Rows - 1)
					vsData.TextMatrix(intCounter, 10, FCConvert.ToString(0));
			}

            //FC:FINAL:SBE - #i2247 - exception is thrown from client side. Use BeginUpdate and EndUpdate to disable automatic update of datagridview during data load
            vsData.EndUpdate();
        }

		private void SetGridParameters()
		{
			//FC:FINAL:DDU:#i2025 - add enough cols to grid
			vsData.Cols = 6;
			vsData.TextMatrix(0, 1, "Employee Number");
			vsData.TextMatrix(0, 2, "Employee Name");
			vsData.TextMatrix(0, 3, "Check Number");
			vsData.TextMatrix(0, 4, "Pay Date");
			vsData.TextMatrix(0, 5, "Pay Run ID");
			//FC:FINAL:DDU:#2737 - aligned columns headers with columns data
			vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.Editable = FCGrid.EditableSettings.flexEDNone;
		}

		private void frmCheckReturn_Resize(object sender, System.EventArgs e)
		{
			if (boolTAGrid)
			{
				// 1) = "Ded Number"
				// 2) = "Description"
				// 3) = "DeductionID"
				// 4) = "DeductionAccountNumber"
				// 5) = "Recipient"
				// 6) = "RecipientID"
				// 7) = "Category"
				// 8) = "CategoryID"
				// 9) = "Amount"
				// 10) = "Apply"
				vsData.Cols = 11;
				vsData.ColWidth(0, 0);
				vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
				vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.25));
				vsData.ColWidth(3, 0);
				vsData.ColWidth(4, 0);
				vsData.ColWidth(5, FCConvert.ToInt32(vsData.WidthOriginal * 0.25));
				vsData.ColWidth(6, 0);
				vsData.ColWidth(7, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
				vsData.ColWidth(8, 0);
				vsData.ColWidth(9, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
				vsData.ColWidth(10, FCConvert.ToInt32(vsData.WidthOriginal * 0.07));
				// .ColDataType(10) = flexDTBoolean
				//vsData.RowHeight(0) += 100;
			}
			else
			{
				vsData.Cols = 6;
				vsData.ColWidth(0, 0);
				vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.19));
				vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.38));
				vsData.ColWidth(3, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
				vsData.ColWidth(4, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
				vsData.ColWidth(5, FCConvert.ToInt32(vsData.WidthOriginal * 0.11));
			}
		}

		private void mnuAll_Click(object sender, System.EventArgs e)
		{
			if (vsData.Cols > 8)
			{
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				for (intCounter = 1; intCounter <= (this.vsData.Rows - 1); intCounter++)
				{
					if (pboolSelect)
					{
						vsData.TextMatrix(intCounter, 10, FCConvert.ToString(1));
					}
					else
					{
						vsData.TextMatrix(intCounter, 10, FCConvert.ToString(2));
					}
				}
				pboolSelect = !pboolSelect;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			if (boolTAGrid)
			{
				if (MessageBox.Show("Reduction amounts will not be saved. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Close();
				}
			}
			else
			{
				if (MessageBox.Show("Check return will not be completed. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Close();
					return;
				}
			}
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsExecute = new clsDRWrapper();
			string strSQL = "";
			int[] lngAry = null;
			double[] dblMSRSAry = new double[3 + 1];
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x = 0;
			bool boolMSRS = false;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			double dblSum = 0;
			// vbPorter upgrade warning: dblDed As double	OnWrite(int, string, double)
			double[] dblDed = new double[3 + 1];
			if (vsData.Rows == 1)
			{
				Close();
				return;
			}
			if (boolTAGrid)
			{
				vsData.Select(0, 0);
				for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
				{
					if (FCConvert.ToDouble(vsData.TextMatrix(intCounter, 10)) == 0)
					{
						MessageBox.Show("Apply option must be selected before process can continue.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsData.Select(intCounter, 10);
						return;
					}
				}
				// get list of msrs deductions including insurance premiums and employer contributions
				x = 0;
				rsData.OpenRecordset("select * from tbldefaultdeductions", "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					while (!rsData.EndOfFile())
					{
						x += 1;
						Array.Resize(ref lngAry, x + 1);
						lngAry[x] = FCConvert.ToInt32(rsData.Get_Fields("deductionid"));
						rsData.MoveNext();
					}
				}
				else
				{
					// make sure it has at least one element so it won't cause an error
					lngAry = new int[1 + 1];
					lngAry[1] = -1;
					// a deduction that can't exist
				}
				// now get the percentage of msrs allocations by report type
				// Call rsData.OpenRecordset("select sum(distgrosspay) as sumGross from tblcheckdetail where distributionrecord = 1 and reporttype = " & CNSTMSRSREPORTTYPETEACHER & " and employeenumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' and checknumber = " & lngCheckNumber, "twpy0000.vb1")
				rsData.OpenRecordset("select sum(distgrosspay) as sumGross from tblcheckdetail where distributionrecord = 1 and reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER) + " and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and checknumber = " + FCConvert.ToString(lngCheckNumber) + " and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID), "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					dblMSRSAry[3] = Conversion.Val(rsData.Get_Fields("sumgross"));
				}
				else
				{
					dblMSRSAry[3] = 0;
				}
				// Call rsData.OpenRecordset("select sum(distgrosspay) as sumGross from tblcheckdetail where distributionrecord = 1 and reporttype = " & CNSTMSRSREPORTTYPESCHOOLPLD & " and employeenumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' and checknumber = " & lngCheckNumber, "twpy0000.vb1")
				rsData.OpenRecordset("select sum(distgrosspay) as sumGross from tblcheckdetail where distributionrecord = 1 and reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD) + " and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and checknumber = " + FCConvert.ToString(lngCheckNumber) + " and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID), "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					dblMSRSAry[2] = Conversion.Val(rsData.Get_Fields("sumgross"));
				}
				else
				{
					dblMSRSAry[2] = 0;
				}
				// Call rsData.OpenRecordset("select sum(distgrosspay) as sumGross from tblcheckdetail where distributionrecord = 1 and reporttype = " & CNSTMSRSREPORTTYPEPLD & " and employeenumber = '" & gtypeCurrentEmployee.EmployeeNumber & "' and checknumber = " & lngCheckNumber, "twpy0000.vb1")
				rsData.OpenRecordset("select sum(distgrosspay) as sumGross from tblcheckdetail where distributionrecord = 1 and reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD) + " and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and checknumber = " + FCConvert.ToString(lngCheckNumber) + " and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID), "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					dblMSRSAry[1] = Conversion.Val(rsData.Get_Fields("sumgross"));
				}
				else
				{
					dblMSRSAry[1] = 0;
				}
				dblSum = dblMSRSAry[1] + dblMSRSAry[2] + dblMSRSAry[3];
				if (dblMSRSAry[3] > 0)
				{
					dblTemp = FCConvert.ToDouble(Strings.Format(dblMSRSAry[3] / dblSum, "0.00"));
					dblMSRSAry[3] = dblTemp;
				}
				if (dblMSRSAry[2] > 0)
				{
					if (dblMSRSAry[1] > 0)
					{
						dblTemp = FCConvert.ToDouble(Strings.Format(dblMSRSAry[2] / dblSum, "0.00"));
						dblMSRSAry[2] = dblTemp;
						dblMSRSAry[1] = 1 - dblMSRSAry[3] - dblMSRSAry[2];
					}
					else
					{
						dblMSRSAry[2] = 1 - dblMSRSAry[3];
					}
				}
				else
				{
					dblMSRSAry[1] = 1 - dblMSRSAry[3] - dblMSRSAry[2];
				}
			}
			rsData.OpenRecordset("Select * from tblTAReduction where ID = 0", "TWPY0000.vb1");
			if (boolTAGrid)
			{
				// FILL THE TABLE TBLTAREDUCTION WITH ALL CHECKED ITEMS FROM THE GRID
				for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
				{
					if (FCConvert.ToDouble(vsData.TextMatrix(intCounter, 10)) == 1)
					{
						// they said to include this record so add it to the table
						if (Conversion.Val(vsData.TextMatrix(intCounter, 9)) == 0)
						{
						}
						else
						{
							boolMSRS = false;
							for (x = 1; x <= (Information.UBound(lngAry, 1)); x++)
							{
								if (Conversion.Val(vsData.TextMatrix(intCounter, 3)) == lngAry[x])
								{
									boolMSRS = true;
									break;
								}
							}
							// x
							if (!boolMSRS)
							{
								strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
								strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
								strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
								strSQL += "," + vsData.TextMatrix(intCounter, 6);
								strSQL += "," + vsData.TextMatrix(intCounter, 8);
								strSQL += "," + vsData.TextMatrix(intCounter, 3);
								strSQL += "," + vsData.TextMatrix(intCounter, 9);
								strSQL += "," + FCConvert.ToString(lngCheckNumber);
								strSQL += ",'" + vsData.TextMatrix(intCounter, 4) + "'";
								strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
								strSQL += ")";
								rsExecute.Execute(strSQL, "twpy0000.vb1");
								rsData.AddNew();
								rsData.Set_Fields("UserID", modGlobalVariables.Statics.gstrUser);
								rsData.Set_Fields("tblTAReductionDate", DateTime.Today);
								rsData.Set_Fields("tblTAReductionTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsData.Set_Fields("RecipientID", vsData.TextMatrix(intCounter, 6));
								rsData.Set_Fields("CategoryID", vsData.TextMatrix(intCounter, 8));
								rsData.Set_Fields("DeductionID", vsData.TextMatrix(intCounter, 3));
								rsData.Set_Fields("Amount", vsData.TextMatrix(intCounter, 9));
								// * -1
								rsData.Set_Fields("CheckNumber", lngCheckNumber);
								rsData.Set_Fields("DeductionAccountNumber", vsData.TextMatrix(intCounter, 4));
								rsData.Set_Fields("reporttype", modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
								rsData.Update();
							}
							else
							{
								// going to have to split the deduction up between report types
								dblSum = Conversion.Val(vsData.TextMatrix(intCounter, 9));
								dblDed[1] = 0;
								dblDed[2] = 0;
								dblDed[3] = 0;
								if (dblMSRSAry[3] != 0)
								{
									dblDed[3] = FCConvert.ToDouble(Strings.Format(dblSum * dblMSRSAry[3], "0.00"));
								}
								if (dblMSRSAry[2] != 0)
								{
									if (dblMSRSAry[1] != 0)
									{
										dblDed[2] = FCConvert.ToDouble(Strings.Format(dblSum * dblMSRSAry[2], "0.00"));
										dblDed[1] = dblSum - dblDed[3] - dblDed[2];
									}
									else
									{
										dblDed[2] = dblSum - dblDed[3];
									}
								}
								else
								{
									dblDed[1] = dblSum - dblDed[3];
								}
								for (x = 1; x <= 3; x++)
								{
									if (dblDed[x] != 0)
									{
										strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
										strSQL += "'" + FCConvert.ToString(pdatCheckDate) + "'";
										strSQL += "," + FCConvert.ToString(lngPayrunID);
										strSQL += "," + vsData.TextMatrix(intCounter, 6);
										strSQL += "," + vsData.TextMatrix(intCounter, 8);
										strSQL += "," + vsData.TextMatrix(intCounter, 3);
										strSQL += "," + vsData.TextMatrix(intCounter, 9);
										strSQL += "," + FCConvert.ToString(lngCheckNumber);
										strSQL += ",'" + vsData.TextMatrix(intCounter, 4) + "'";
										switch (x)
										{
											case 1:
												{
													strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD);
													break;
												}
											case 2:
												{
													strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD);
													break;
												}
											case 3:
												{
													strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
													break;
												}
										}
										//end switch
										strSQL += ")";
										rsExecute.Execute(strSQL, "twpy0000.vb1");
										rsData.AddNew();
										rsData.Set_Fields("userid", modGlobalVariables.Statics.gstrUser);
										rsData.Set_Fields("tblTAReductiondate", DateTime.Today);
										rsData.Set_Fields("tblTAReductiontime", fecherFoundation.DateAndTime.TimeOfDay);
										rsData.Set_Fields("recipientid", vsData.TextMatrix(intCounter, 6));
										rsData.Set_Fields("categoryid", vsData.TextMatrix(intCounter, 8));
										rsData.Set_Fields("deductionid", vsData.TextMatrix(intCounter, 3));
										rsData.Set_Fields("amount", dblDed[x]);
										rsData.Set_Fields("checknumber", lngCheckNumber);
										rsData.Set_Fields("deductionaccountnumber", vsData.TextMatrix(intCounter, 4));
										switch (x)
										{
											case 1:
												{
													rsData.Set_Fields("reporttype", modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD);
													break;
												}
											case 2:
												{
													rsData.Set_Fields("reporttype", modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD);
													break;
												}
											case 3:
												{
													rsData.Set_Fields("reporttype", modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
													break;
												}
										}
										//end switch
										rsData.Update();
									}
								}
								// x
							}
						}
					}
				}
				MessageBox.Show("Check Return completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
			else
			{
				vsData_DblClick(null, null);
			}
		}

		private void vsData_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsData.MouseRow == 0)
			{
				if (boolGridSort)
				{
					vsData.Sort = (FCGrid.SortSettings)1;
				}
				else
				{
					vsData.Sort = (FCGrid.SortSettings)2;
				}
				boolGridSort = !boolGridSort;
			}
		}

		private void vsData_DblClick(object sender, System.EventArgs e)
		{
			DateTime pdatTempCheckDate;
			DateTime pdatTempPayDate;
			bool boolSameMonth;
			bool boolSameQuarter;
			bool boolSameFiscalYear = false;
			bool boolSameCalanderYear = false;
			string strWhereClause = "";
			double pdblTotalPay;
			string strBDtoUse = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsCheckReturn = new clsDRWrapper();
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modAccountTitle.SetAccountFormats();
				if (boolTAGrid)
				{
				}
				else
				{
					if (vsData.MouseRow > 0)
					{
						// then the user had chosen a valid row in the grid
						pdatCheckDate = FCConvert.ToDateTime(vsData.TextMatrix(vsData.MouseRow, 4));
						strEmployeeNumber = vsData.TextMatrix(vsData.MouseRow, 1);
						lngCheckNumber = FCConvert.ToInt32(vsData.TextMatrix(vsData.MouseRow, 3));
						lngPayrunID = FCConvert.ToInt32(vsData.TextMatrix(vsData.MouseRow, 5));
						if (MessageBox.Show("Are you sure you wish to return check number " + FCConvert.ToString(lngCheckNumber) + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return;
						}
						// this will give me the first day of the fiscal year for the check
						pdatTempCheckDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(pdatCheckDate);
						// this will give me the first day of the fiscal year for the Pay Date
						pdatTempPayDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
						if (fecherFoundation.DateAndTime.DateDiff("d", pdatTempCheckDate, pdatTempPayDate) == 0)
						{
							// this is the same fiscal year
							boolSameFiscalYear = true;
						}
						else
						{
							boolSameFiscalYear = false;
                            rsTemp.OpenRecordset("select * from standardaccounts where code = 'AP'", "twbd0000.vb1");
                            if (!rsTemp.EndOfFile())
                            {
                                if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Account")))) != 0)
                                {
                                    // do nothing
                                }
                                else
                                {
                                    MessageBox.Show("Before you may return a check from a previous fiscal year you must set up your Town EOY Accounts Payable Account in Budgetary System > File Maintenance > G/L Ctrl / Stand Accts.", "Invalid Town EOY Accounts Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                    return;
                                }
                            }
							//if (modValidateAccount.Statics.gboolSchoolAccounts)
							//{
							//	rsTemp.OpenRecordset("select * from standardaccounts where code = 'SAP'", "twbd0000.vb1");
							//	if (!rsTemp.EndOfFile())
							//	{
							//		if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Account")))) != 0)
							//		{
							//			// do nothing
							//		}
							//		else
							//		{
							//			MessageBox.Show("Before you may return a check from a previous fiscal year you must set up your School EOY Accounts Payable Account in Budgetary System > File Maintenance > G/L Ctrl / Stand Accts.", "Invalid School EOY Accounts Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							//			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							//			return;
							//		}
							//	}
							//}
						}
						if (fecherFoundation.DateAndTime.DateDiff("yyyy", pdatCheckDate, modGlobalVariables.Statics.gdatCurrentPayDate) == 0)
						{
							// this is the same calander year
							boolSameCalanderYear = true;
						}
						else
						{
							boolSameCalanderYear = false;
						}
						if (fecherFoundation.DateAndTime.DateDiff("m", pdatCheckDate, modGlobalVariables.Statics.gdatCurrentPayDate) == 0)
						{
							// this is the same month
							boolSameMonth = true;
						}
						else
						{
							boolSameMonth = false;
						}
						if (fecherFoundation.DateAndTime.DateDiff("q", pdatCheckDate, modGlobalVariables.Statics.gdatCurrentPayDate) == 0)
						{
							// this is the same quarter
							boolSameQuarter = true;
						}
						else
						{
							boolSameQuarter = false;
						}
						strWhereClause = "Where EmployeeNumber = '" + strEmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " ";
						// NOW UPDATE THE EMPLOYERS MATCH RECORDS WITH THE CORRECTION
						clsDRWrapper rsData = new clsDRWrapper();
						clsDRWrapper rsNew = new clsDRWrapper();
						strBDtoUse = "";
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!boolSameFiscalYear)
							{
								rsData.OpenRecordset("select top 1 * from archives where ArchiveType = 'Archive' order by archiveid desc", "SystemSettings");
								strBDtoUse = "Archive_" + rsData.Get_Fields("archiveID");
							}
						}
						// NOW UPDATE THE EMPLOYEE VACATION / SICK RECORDS WITH THE CORRECTION
						// MATTHEW 3/16/04
						// Call rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" & strEmployeeNumber & "' AND CheckNumber = " & lngCheckNumber & " AND VSRecord = 1", "TWPY0000.vb1")
						rsData.OpenRecordset("Select * from tblCheckDetail " + strWhereClause + "AND VSRecord = 1", "TWPY0000.vb1");
						while (!rsData.EndOfFile())
						{
							rsNew.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' AND TypeID = " + rsData.Get_Fields_Int32("VSTypeID") + " AND selected = 1", "TWPY0000.vb1");
							if (!rsNew.EndOfFile())
							{
								rsNew.Edit();
								if (boolSameFiscalYear)
								{
									rsNew.Set_Fields("UsedFiscal", rsNew.Get_Fields_Double("UsedFiscal") - rsData.Get_Fields_Double("VSUsed"));
									if (rsNew.Get_Fields_Double("UsedFiscal") < 0)
										rsNew.Set_Fields("UsedFiscal", 0);
								}
								if (boolSameCalanderYear)
								{
									rsNew.Set_Fields("UsedCalendar", rsNew.Get_Fields_Double("UsedCalendar") - rsData.Get_Fields_Double("VSUsed"));
									if (rsNew.Get_Fields_Double("UsedCalendar") < 0)
										rsNew.Set_Fields("UsedCalendar", 0);
								}
								// Matthew change by request for Topsham Matthew 3/9/2004
								// rsNew.Fields("UsedBalance") = rsNew.Fields("UsedBalance") - rsData.Fields("VSUsed")
								rsNew.Set_Fields("UsedBalance", rsNew.Get_Fields_Double("UsedBalance") + rsData.Get_Fields_Double("VSUsed"));
								if (rsNew.Get_Fields_Double("UsedBalance") < 0)
									rsNew.Set_Fields("UsedBalance", 0);
								if (boolSameFiscalYear)
								{
									rsNew.Set_Fields("AccruedFiscal", rsNew.Get_Fields_Double("AccruedFiscal") - rsData.Get_Fields_Double("VSAccrued"));
									if (rsNew.Get_Fields_Double("AccruedFiscal") < 0)
										rsNew.Set_Fields("AccruedFiscal", 0);
								}
								if (boolSameCalanderYear)
								{
									rsNew.Set_Fields("AccruedCalendar", rsNew.Get_Fields_Double("AccruedCalendar") - rsData.Get_Fields_Double("VSAccrued"));
									if (rsNew.Get_Fields_Double("AccruedCalendar") < 0)
										rsNew.Set_Fields("AccruedCalendar", 0);
								}
								// MATTHEW CHANGE BY REQUEST FOR TOPSHAM MATTHEW 3/9/2004
								// THIS WAS NEVER IN THE CODE.
								rsNew.Set_Fields("UsedBalance", rsNew.Get_Fields_Double("UsedBalance") - rsData.Get_Fields_Double("VSAccrued"));
								if (rsNew.Get_Fields_Double("UsedBalance") < 0)
									rsNew.Set_Fields("UsedBalance", 0);
								rsNew.Update();
							}
							rsData.MoveNext();
						}
						// NOW ADJUST THE TAX WITH HELD AND AND TOTAL GROSS FIELDS
						double dblTotal;
						// NOW VOID ALL OF THE RECORDS IN CHECKDETAIL WITH THIS CHECK
						rsNew.Execute("update tblcheckdetail set CheckVoid = 1 where bankrecord = 1 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID), "twpy0000.vb1");
						rsNew.Execute("Update tblCheckDetail Set CheckVoid = 1  where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " ", "TWPY0000.vb1");
						// DAVE'S CALL TO PAYROLL ACCOUNTING CHARGES REPORT AND JOURNAL NUMBER
						// Create Reversing Journal for Budgetary
						if (modGlobalConstants.Statics.gboolBD)
						{
							GetJournalNumber(!boolSameFiscalYear, ref strBDtoUse);
						}
						modGlobalVariables.Statics.gintUseGroupMultiFund = 0;
						if (modGlobalConstants.Statics.gboolBD)
						{
							// Void Check out of Budgetary Check Rec Table
							clsDRWrapper rsCheckRec = new clsDRWrapper();
							if (strBDtoUse != string.Empty)
							{
								rsCheckRec.GroupName = strBDtoUse;
							}
							rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Type = '2' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND CHECKDATE = '" + FCConvert.ToString(pdatCheckDate) + "'", "TWBD0000.vb1");
							if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
							{
								rsCheckRec.Edit();
								rsCheckRec.Set_Fields("Status", "V");
								if (!boolSameFiscalYear)
								{
									rsCheckRec.Set_Fields("transferred", false);
								}
								rsCheckRec.Update();
							}
						}
						// Show Report With Reversed Charges
						rptPayrollAccountingChargesReversal.InstancePtr.Init(ref strEmployeeNumber, ref pdatCheckDate, ref lngPayrunID, lngCheckNumber, !boolSameFiscalYear);
						// NOW WE NEED TO FIX ALL OF THE TRUST AND AGENCY CHECKS THAT WERE PRINTED
						boolTAGrid = true;
						SetTAGridParameters();
						// If Not rsData.EndOfFile Then Call LoadTAGrid(lngCheckNumber)
						LoadTAGrid(lngCheckNumber, ref pdatCheckDate, ref lngPayrunID);
						// CREATE A COPY OF THE RECORDS TO BE DELETED
						// Call rsData.Execute("INSERT INTO tblCheckReturn SELECT tblCheckDetail.* From tblCheckDetail WHERE EmployeeNumber = '" & strEmployeeNumber & "' AND CheckNumber = " & lngCheckNumber & " and paydate = '" & pdatCheckDate & "' and payrunid = " & lngPayRunID & " ", "twpy0000.vb1")
						rsData.OpenRecordset("SELECT * From tblCheckDetail WHERE EmployeeNumber = '" + strEmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID));
						rsCheckReturn.OpenRecordset("SELECT * FROM tblCheckReturn");
						if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
						{
							do
							{
								rsCheckReturn.AddNew();
								for (intCounter = 0; intCounter <= (rsData.FieldsCount - 1); intCounter++)
								{
									if (rsData.Get_FieldsIndexName(intCounter) == "ID")
									{
										// dop nothing
									}
									else
									{
										rsCheckReturn.Set_Fields(rsData.Get_FieldsIndexName(intCounter), rsData.Get_FieldsIndexValue(intCounter));
									}
								}
								rsCheckReturn.Update();
								rsData.MoveNext();
							}
							while (rsData.EndOfFile() != true);
						}
						// NOW DELETE THOSE RECORDS FROM THE CHECK DETAIL TABLE
						// ADDED THIS PIECE IN SO THAT THE CHECK HISTORY REPORT WILL SHOW VOIDED
						// CHECKS. I'M CONCERNED THAT WHERE THESE RECORDS WERE BEING REMOVED BUT
						// ARE NOW NOT, IN SOME CHASES WHERE I SUM UP MANY RECORDS I MAY NOW HAVE
						// BEEN LOOKING FOR WHERE CheckVoid = 0 AND I WILL NEED TO DO THIS NOW
						// WHY I'M LEAVING THIS HERE FOR NOW. MATTHEW 3/24/2004
						// Call rsData.Execute("Delete from tblCheckDetail where EmployeeNumber = '" & strEmployeeNumber & "' AND CheckNumber = " & lngCheckNumber)
						rsData.Execute("Update tblCheckDetail Set CheckVoid = 1 where EmployeeNumber = '" + strEmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID), "twpy0000.vb1");
						rsData.OpenRecordset("Select * from tblCheckDetail where TotalRecord = 1 AND EmployeeNumber = '" + strEmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " and paydate = '" + FCConvert.ToString(pdatCheckDate) + "' and payrunid = " + FCConvert.ToString(lngPayrunID), "twpy0000.vb1");
						clsDRWrapper rsCheckHistory = new clsDRWrapper();
						rsCheckHistory.OpenRecordset("Select * from tblCheckHistory where CheckNumber = 1");
						rsCheckHistory.AddNew();
						rsCheckHistory.Set_Fields("CheckNumber", rsData.Get_Fields("CheckNumber"));
						rsCheckHistory.Set_Fields("ReplacedByCheckNumber", rsData.Get_Fields("CheckNumber"));
						rsCheckHistory.Set_Fields("EMPLOYEENUMBER", rsData.Get_Fields("EmployeeNumber"));
						rsCheckHistory.Set_Fields("EMPLOYEENAME", rsData.Get_Fields_String("EmployeeName"));
						rsCheckHistory.Set_Fields("Amount", rsData.Get_Fields_Decimal("NetPay"));
						rsCheckHistory.Set_Fields("CheckDate", rsData.Get_Fields_DateTime("PayDate"));
						rsCheckHistory.Set_Fields("CheckRun", rsData.Get_Fields("PayRunID"));
						rsCheckHistory.Set_Fields("CheckType", "R");
						rsCheckHistory.Set_Fields("Status", "V");
						rsCheckHistory.Set_Fields("ReportNumber", 0);
						rsCheckHistory.Set_Fields("WarrantNumber", FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Int32("WarrantNumber"))));
						rsCheckHistory.Update();
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 32755)
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetTAGridParameters()
		{
			vsData.Rows = 1;
			vsData.Cols = 11;
			vsData.TextMatrix(0, 1, "Ded Number");
			vsData.TextMatrix(0, 2, "Description");
			vsData.TextMatrix(0, 3, "DeductionID");
			vsData.TextMatrix(0, 4, "Deduction Account");
			vsData.TextMatrix(0, 5, "Recipient");
			vsData.TextMatrix(0, 6, "RecipientID");
			vsData.TextMatrix(0, 7, "Category");
			vsData.TextMatrix(0, 8, "CategoryID");
			vsData.TextMatrix(0, 9, "Amount");
			vsData.TextMatrix(0, 10, "Apply");
			vsData.ColHidden(3, true);
			vsData.ColHidden(6, true);
			vsData.ColHidden(8, true);
			// .ColDataType(10) = flexDTBoolean
			vsData.ColWidth(0, 0);
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.25));
			vsData.ColWidth(3, 0);
			vsData.ColWidth(4, 0);
			vsData.ColWidth(5, FCConvert.ToInt32(vsData.WidthOriginal * 0.25));
			vsData.ColWidth(6, 0);
			vsData.ColWidth(7, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
			vsData.ColWidth(8, 0);
			vsData.ColWidth(9, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
			vsData.ColWidth(10, FCConvert.ToInt32(vsData.WidthOriginal * 0.07));
			//App.DoEvents();
			vsData.ColComboList(10, "#0; |#1;Yes|#2;No");
			//App.DoEvents();
			//vsData.RowHeight(0) += 100;
			//FC:FINAL:DDU:#2737 - aligned columns headers with columns data
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 10, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsData.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//vsData.Editable = FCGrid.EditableSettings.flexEDNone;
			lblCaption.Text = "Employee Number:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "    Check Number:  " + FCConvert.ToString(lngCheckNumber);
			lblCaption.Visible = true;
			Label1.Visible = true;
			mnuExit.Visible = false;
			mnuSP1.Visible = false;
			boolTAGrid = true;
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            vsData.SetColReadOnly(0,true);
            vsData.SetColReadOnly(1, true);
            vsData.SetColReadOnly(2, true);
            vsData.SetColReadOnly(3, true);
            vsData.SetColReadOnly(4, true);
            vsData.SetColReadOnly(5, true);
            vsData.SetColReadOnly(6, true);
            vsData.SetColReadOnly(7, true);
            vsData.SetColReadOnly(8, true);
            vsData.SetColReadOnly(9, true);
            //vsData.EditCell();
        }

		private void GetJournalNumber(bool boolPreviousFiscal, ref string strBD)
		{
			//modBudgetaryAccounting.FundType[] AccountStrut;
			int intArrayID = 0;
			string strAccount = "";
			// vbPorter upgrade warning: dblValue As double	OnWrite(double, string, int)
			double dblValue = 0;
			clsDRWrapper rspayrollaccounts = new clsDRWrapper();
			// Dim rsSchoolAccounts As New clsDRWrapper
			clsDRWrapper rsData = new clsDRWrapper();
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			string strText = "";
			bool boolSeparateAccounts;
			string strBDtoUse;
			strBDtoUse = strBD;
			boolSeparateAccounts = false;
			// NOW ADD THE DATA TO BUDGETARY
			// ONLY WANT EXPENSE AND DEPRECIATION
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
                modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				intArrayID = 0;
				modAccountTitle.SetAccountFormats();
				rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" + strEmployeeNumber + "' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND TotalRecord = 1", "TWPY0000.vb1");
				if (!rsData.EndOfFile())
					modGlobalVariables.Statics.gintUseGroupMultiFund = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int32("MultiFundNumber"))));
				// DISTRIBUTION RECORDS
				rsData.OpenRecordset("Select * from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND distributionrecord = 1 order by DistAccountNumber", modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber")))
						{
							dblValue -= FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("DistGrossPay"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								// ***********************************************************
								// not sure why this was commented out. Matthew 11/18/03
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                                // ***********************************************************
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("DistGrossPay") * -1, "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						if (Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) < intArrayID)
						{
                            
                            Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						}
						else
						{
							if (modGlobalVariables.Statics.AccountStrut[intArrayID].Account != "")
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
							}
						}
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                        modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						intArrayID += 1;
					}
				}
				// NOW PASS THE TAX INFORMATION
				if (!boolSeparateAccounts)
				{
					rsData.OpenRecordset("Select 0 as fromschool,Sum(FederalTaxWH) as TotalFedTaxWH, Sum(StateTaxWH) as TotalStateTaxWH, Sum(FICATaxWH) as TotalFICATaxWH,sum(EmployerFicaTax) as TotalEmployerFicaTax,sum(EmployerMedicareTax) as TotalEmployerMedicareTax, Sum(MedicareTaxWH) as TotalMedTaxWH from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND TotalRecord = 1 group by fromschool,multifundnumber", modGlobalVariables.DEFAULTDATABASE);
				}
				else
				{
					rsData.OpenRecordset("Select fromschool,Sum(FederalTaxWH) as TotalFedTaxWH, Sum(StateTaxWH) as TotalStateTaxWH, Sum(FICATaxWH) as TotalFICATaxWH,sum(EmployerFicaTax) as TotalEmployerFicaTax,sum(EmployerMedicareTax) as TotalEmployerMedicareTax, Sum(MedicareTaxWH) as TotalMedTaxWH from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND TotalRecord = 1 group by fromschool,multifundnumber", modGlobalVariables.DEFAULTDATABASE);
				}
				rspayrollaccounts.OpenRecordset("Select * from tblPayrollAccounts order by id", modGlobalVariables.DEFAULTDATABASE);
				// Call rsSchoolAccounts.OpenRecordset("select * from payrollschoolaccounts order by id", "twpy0000.vb1")
				if (rspayrollaccounts.EndOfFile())
				{
				}
				else
				{
					// Federal Tax WH
					while (!rsData.EndOfFile())
					{
						// If Not rsData.Fields("fromschool") Then
						if (Conversion.Val(rsData.Get_Fields("TotalFedTaxWH")) == 0)
						{
						}
						else
						{
							rspayrollaccounts.FindFirstRecord("CategoryID", 1);
							if (rspayrollaccounts.NoMatch)
							{
							}
							else
							{
								// ***********************************************************
								// MATTHEW MULTI CALL ID 79810 10/27/2005
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								strText = FCConvert.ToString(rspayrollaccounts.Get_Fields("Account"));
								if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
								{
									// corey 01/03/2006 Need to format fund
									Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
                                    modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
								}
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalFedTaxWH"), "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
						}
						if (Conversion.Val(rsData.Get_Fields("TotalStateTaxWH")) == 0)
						{
						}
						else
						{
							rspayrollaccounts.FindFirstRecord("CategoryID", 4);
							if (rspayrollaccounts.NoMatch)
							{
							}
							else
							{
								// ***********************************************************
								// MATTHEW MULTI CALL ID 79810 10/27/2005
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								strText = FCConvert.ToString(rspayrollaccounts.Get_Fields("Account"));
								if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
								{
									// corey 1/03/2005
									Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
                                    modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
								}
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalStateTaxWH"), "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
						}
						if (Conversion.Val(rsData.Get_Fields("TotalFICATaxWH")) + Conversion.Val(rsData.Get_Fields("TotalEMployerFicaTax")) == 0)
						{
						}
						else
						{
							rspayrollaccounts.FindFirstRecord("CategoryID", 2);
							if (rspayrollaccounts.NoMatch)
							{
							}
							else
							{
								// ***********************************************************
								// MATTHEW MULTI CALL ID 79810 10/27/2005
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								strText = FCConvert.ToString(rspayrollaccounts.Get_Fields("Account"));
								if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
								{
									// Corey 01/03/2006 Need to format fund
									Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
                                    modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
								}
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalFICATaxWH") + rsData.Get_Fields("TotalEmployerFicaTax"), "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
						}
						if (Conversion.Val(rsData.Get_Fields("TotalMedTaxWH")) == 0 && Conversion.Val(rsData.Get_Fields("TotalEmployerMedicareTax")) == 0)
						{
						}
						else
						{
							rspayrollaccounts.FindFirstRecord("CategoryID", 3);
							if (rspayrollaccounts.NoMatch)
							{
							}
							else
							{
								// ***********************************************************
								// MATTHEW MULTI CALL ID 79810 10/27/2005
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								strText = FCConvert.ToString(rspayrollaccounts.Get_Fields("Account"));
								if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
								{
									// corey 01/03/2006   Need to format fund
									Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
                                    modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
								}
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalMedTaxWH") + Conversion.Val(rsData.Get_Fields("totalEmployerMedicareTax")), "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
						}
						
						rsData.MoveNext();
					}
				}
				rsData.Execute("delete from tblTempTax", "twpy0000.vb1");
				modGlobalRoutines.SetupTempTaxTable(pdatCheckDate, lngPayrunID, "tblCheckDetail", lngCheckNumber);
				rsData.OpenRecordset("Select * from tblTempTax Order by DeptDiv", modGlobalVariables.DEFAULTDATABASE);
				while (!rsData.EndOfFile())
				{
					if (Conversion.Val(rsData.Get_Fields_Double("EmployerMedicareTax")) == 0)
					{
						// this record is FICA
						if (Conversion.Val(rsData.Get_Fields("employerFICATax")) == 0)
						{
						}
						else
						{
							Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
							modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
							modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
							modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
							// AccountStrut(intArrayID).Amount = Format(rsData.Fields("FICATax") * -1, "0.00")
							modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("EmployerFICATax") * -1, "0.00"));
							modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
							intArrayID += 1;
						}
					}
					else
					{
						// this record is Medicare
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
						modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields_Double("EmployerMedicareTax") * -1, "0.00"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						intArrayID += 1;
					}
					rsData.MoveNext();
				}


				dblValue = 0;
				rsData.OpenRecordset("Select * from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND deductionrecord = 1 order by DistAccountNumber", modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber")))
						{
							dblValue -= FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("DedAmount"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("DedAmount") * -1, "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                        modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						intArrayID += 1;
					}
				}
				// EMPLOYERS MATCH RECORDS DEBITS
				dblValue = 0;
				rsData.OpenRecordset("Select * from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND matchrecord = 1 order by DistAccountNumber", modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber")))
						{
							dblValue -= FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("MatchAmount"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("MatchAmount") * -1, "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                        modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						intArrayID += 1;
					}
				}
				// EMPLOYERS MATCH RECORDS CREDITS
				dblValue = 0;
				rsData.OpenRecordset("Select * from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND matchrecord = 1 order by DistAccountNumber", modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("MatchGLAccountNumber"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("MatchGLAccountNumber")))
						{
							dblValue -= FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("MatchAmount"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("MatchAmount") * -1, "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("MatchGLAccountNumber"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                        modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						intArrayID += 1;
					}
				}
				
				string strOVAccount = "";
				string strOVSCHAccount = "";
				strOVAccount = "";
				strOVSCHAccount = "";
				if (boolPreviousFiscal)
				{
					clsDRWrapper rsTemp = new clsDRWrapper();
					rsTemp.OpenRecordset("select * from standardaccounts where code = 'AP'", "twbd0000.vb1");
					if (!rsTemp.EndOfFile())
					{
						strOVAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
					}
					rsTemp.OpenRecordset("select * from standardaccounts where code = 'SAP'", "twbd0000.vb1");
					if (!rsTemp.EndOfFile())
					{
						strOVSCHAccount = FCConvert.ToString(rsTemp.Get_Fields("account"));
					}
				}
				// matthew 10/20/2004
				if (modGlobalConstants.Statics.gboolBD)
				{
					// this sets up the cash accounts for the funds if the user has BD
					modGlobalVariables.Statics.ftFundArray = modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);
				}
				else
				{
					// create a cash entry into the account M CASH for these accounts
					modGlobalVariables.Statics.ftFundArray = modGlobalRoutines.CashAccountForM(ref modGlobalVariables.Statics.AccountStrut);
				}

				if (strOVAccount == string.Empty)
				{
					if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(DateTime.Today, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false, 0, true))
					{
					}
				}
				else
				{
					modBudgetaryAccounting.CalcCashFundsFromOverride(ref strOVAccount, ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(DateTime.Today, "MM/dd/yyyy") + " PY", "PY", false);
				}
				
				lngJournalNumber = modBudgetaryAccounting.AddToJournal(ref modGlobalVariables.Statics.AccountStrut, "PY", 0, FCConvert.ToDateTime(Strings.Format(pdatCheckDate, "MM/dd/yyyy")), Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " - Void " + FCConvert.ToString(lngCheckNumber), lngPeriodNumber, false, -1, strBDtoUse);
			}
		}		
	}
}
