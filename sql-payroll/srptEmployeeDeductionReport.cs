//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeeDeductionReport.
	/// </summary>
	public partial class srptEmployeeDeductionReport : FCSectionReport
	{
		public srptEmployeeDeductionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptEmployeeDeductionReport InstancePtr
		{
			get
			{
				return (srptEmployeeDeductionReport)Sys.GetInstance(typeof(srptEmployeeDeductionReport));
			}
		}

		protected srptEmployeeDeductionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsDeductions?.Dispose();
				clsLoad?.Dispose();
                clsDeductions = null;
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptEmployeeDeductionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: boolHistorical As bool	OnWrite(bool, string)
		bool boolHistorical;
		bool boolMatchRecords;
		string strTitle = "";
		int lngDedCount;
		int lngTotEmployees;
		double dblTotalCurrent;
		double dblTotalMTD;
		double dblTotalQTD;
		double dblTotalFiscal;
		double dblTotalCalendar;
		double dblTotalLTD;
		double dblDedCurrent;
		double dblDedMTD;
		double dblDedQTD;
		double dblDedFiscal;
		double dblDedCalendar;
		double dblDedLTD;
		clsDRWrapper clsDeductions = new clsDRWrapper();
		clsDRWrapper clsLoad = new clsDRWrapper();
		

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
			if (!eArgs.EOF)
			{
				this.Fields["grpHeader"].Value = clsLoad.Get_Fields("deddeductionnumber");
				if (!clsLoad.EndOfFile())
				{
					// If boolMatchRecords Then
					// strTitle = clsLoad.Fields("deddeductionnumber") & " - " & clsLoad.Fields("description")
					// Else
					// If clsDeductions.FindFirstRecord("ID", Val(clsLoad.Fields("deddeductionnumber"))) Then
					if (clsDeductions.FindFirst("ID = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("deddeductionnumber")))))
					{
						strTitle = clsDeductions.Get_Fields("deductionnumber") + "-" + clsDeductions.Get_Fields_String("description");
						if (Conversion.Val(clsLoad.Get_Fields("deddeductionnumber")) == 4)
						{
							strTitle = strTitle;
						}
					}
					// End If
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			// vbPorter upgrade warning: arSQL As string()	OnRead(string, bool)
			string[] arSQL = null;
			int X;
			int Y;
			bool boolSkipPage;
			dblTotalCurrent = 0;
			dblTotalMTD = 0;
			dblTotalQTD = 0;
			dblTotalFiscal = 0;
			dblTotalCalendar = 0;
			dblTotalLTD = 0;
			dblDedCurrent = 0;
			dblDedMTD = 0;
			dblDedQTD = 0;
			dblDedFiscal = 0;
			dblDedCalendar = 0;
			dblDedLTD = 0;
			lngDedCount = 0;
			lngTotEmployees = 0;
			clsLoad.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
			modGlobalVariables.Statics.gintSequenceBy = clsLoad.Get_Fields_Int32("ReportSequence");
			switch (modGlobalVariables.Statics.gintSequenceBy)
			{
				case 0:
					{
						// employee name
						break;
					}
				case 1:
					{
						// employee number
						break;
					}
				case 2:
					{
						// sequence
						lblOrder.Text = "Seq #";
						break;
					}
				case 3:
					{
						// dept/div
						lblOrder.Text = "Dept/Div";
						break;
					}
			}
			//end switch
			boolSkipPage = false;
			if (clsLoad.Get_Fields_Boolean("SkipPageDed"))
				boolSkipPage = true;
			arSQL = Strings.Split(FCConvert.ToString(this.UserData), "@", -1, CompareConstants.vbTextCompare);
			strSQL = arSQL[0];
			boolHistorical = false;
			if (Information.UBound(arSQL, 1) > 0)
			{
				boolHistorical = FCConvert.CBool(arSQL[1]);
			}
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields("ThisIsMatchRecord")))
				{
					// add more space since making the employee match label visible brings the top of the visible
					// report closer to the stuff above it
					for (X = 0; X <= this.GroupHeader1.Controls.Count - 1; X++)
					{
						if (FCConvert.ToString(this.GroupHeader1.Controls[X].Tag) == "headerfields")
						{
							if (Strings.Left(this.GroupHeader1.Controls[X].Name + "    ", 4) == "Line")
							{
								(this.GroupHeader1.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 += 240 / 1440F;
								(this.GroupHeader1.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 += 240 / 1440F;
							}
							else
							{
								this.GroupHeader1.Controls[X].Top += 240 / 1440F;
							}
						}
					}
					// X
					txtEmployerMatchTitle.Visible = true;
					Line2.Visible = true;
					Line3.Visible = true;
					boolMatchRecords = true;
					Label8.Visible = false;
					Label9.Visible = false;
					txtLast.Visible = false;
					Label7.Visible = false;
					txtLTD.Visible = false;
					txtDedLTD.Visible = false;
					txtTotalLTD.Visible = false;
				}
				else
				{
					boolMatchRecords = false;
				}
			}
			else
			{
				this.Close();
				return;
			}
			if (boolSkipPage)
				this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			if (boolHistorical)
			{
				// don't track ltd if not from the table
				// Label7.Visible = False
				// txtLTD.Visible = False
				// txtDedLTD.Visible = False
				// txtTotalLTD.Visible = False
				// don't track hit limit if not current data
				Label8.Visible = false;
				Label9.Visible = false;
				txtLast.Visible = false;
			}
			// get the deduction information
			clsDeductions.OpenRecordset("select * from tblDeductionSetup", "twpy0000.vb1");
			// initialize it with a bogus variable
			//FC:FINAL:DDU:#i2426 - groupvalue make issues, commented it out to work as in original
			//this.Fields["grpHeader"].Value = "0";
			// If boolMatchRecords Then
			// strTitle = clsLoad.Fields("deddeductionnumber") & " - " & clsLoad.Fields("description")
			// Else
			// If clsDeductions.FindFirstRecord("ID", Val(clsLoad.Fields("deddeductionnumber"))) Then
			if (clsDeductions.FindFirst("ID = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("deddeductionnumber")))))
			{
				strTitle = clsDeductions.Get_Fields("deductionnumber") + "-" + clsDeductions.Get_Fields_String("description");
			}
			// End If
			// If gboolSamePage Then
			// Me.GroupHeader1.NewPage = ddNPNone
			// Else
			// Me.GroupHeader1.NewPage = ddNPBefore
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblCurrent As double	OnWrite(string)
			double dblCurrent = 0;
			// vbPorter upgrade warning: dblMTD As double	OnWrite(string)
			double dblMTD = 0;
			// vbPorter upgrade warning: dblQTD As double	OnWrite(string)
			double dblQTD = 0;
			// vbPorter upgrade warning: dblFiscal As double	OnWrite(string)
			double dblFiscal = 0;
			// vbPorter upgrade warning: dblCalendar As double	OnWrite(string)
			double dblCalendar = 0;
			// vbPorter upgrade warning: dblLTD As double	OnWrite(string)
			double dblLTD = 0;
			switch (modGlobalVariables.Statics.gintSequenceBy)
			{
				case 0:
					{
						// employee name
						// lblOrder.Caption = "Emp #"
						// txtEmployeeNumber.Text = .Fields("employeenumber")
						break;
					}
				case 1:
					{
						// employee number
						// lblOrder.Caption = "Emp #"
						// txtEmployeeNumber.Text = .Fields("employeenumber")
						break;
					}
				case 2:
					{
						// sequence
						lblOrder.Text = "Seq";
						txtEmployeeNumber.Text = FCConvert.ToString(clsLoad.Get_Fields("SeqNumber"));
						break;
					}
				case 3:
					{
						// dept/div
						lblOrder.Text = "Dept/Div";
						txtEmployeeNumber.Text = FCConvert.ToString(clsLoad.Get_Fields("DeptDiv"));
						break;
					}
			}
			//end switch
			txtName.Text = clsLoad.Get_Fields("employeenumber") + "   " + fecherFoundation.Strings.Trim(clsLoad.Get_Fields("lastname") + "," + clsLoad.Get_Fields("firstname") + " " + clsLoad.Get_Fields("middlename") + " " + clsLoad.Get_Fields("desig"));
			dblCurrent = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("curdeduction")), "0.00"));
			dblQTD = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("qtddeduction")), "0.00"));
			dblMTD = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("mtddeduction")), "0.00"));
			dblFiscal = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ytdfiscaldeduction")), "0.00"));
			dblCalendar = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ytdcalendardeduction")), "0.00"));
			dblLTD = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ltddeduction")), "0.00"));
			txtCurrent.Text = Strings.Format(dblCurrent, "0.00");
			txtMTD.Text = Strings.Format(dblMTD, "0.00");
			txtQTD.Text = Strings.Format(dblQTD, "0.00");
			txtFiscal.Text = Strings.Format(dblFiscal, "0.00");
			txtCalendar.Text = Strings.Format(dblCalendar, "0.00");
			txtLTD.Text = Strings.Format(dblLTD, "0.00");
			if (!FCConvert.ToBoolean(clsLoad.Get_Fields("ThisIsMatchRecord")))
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields("HistoricalRecord")))
				{
					// If Val(.Fields("limitreached")) < 0 Then
					// txtLast.Text = "*"
					// Else
					// txtLast.Text = ""
					// End If
				}
				else
				{
					if (Conversion.Val(clsLoad.Get_Fields("limit")) > 0)
					{
						if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("limittype"))) == "LIFE")
						{
							if (dblLTD >= Conversion.Val(clsLoad.Get_Fields("limit")))
							{
								txtLast.Text = "*";
							}
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("limittype"))) == "FISCAL")
						{
							if (dblFiscal >= Conversion.Val(clsLoad.Get_Fields("limit")))
							{
								txtLast.Text = "*";
							}
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("limittype"))) == "QUARTER")
						{
							if (dblQTD >= Conversion.Val(clsLoad.Get_Fields("limit")))
							{
								txtLast.Text = "*";
							}
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("limittype"))) == "MONTH")
						{
							if (dblMTD >= Conversion.Val(clsLoad.Get_Fields("limit")))
							{
								txtLast.Text = "*";
							}
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("limittype"))) == "PERIOD")
						{
							if (dblCurrent >= Conversion.Val(clsLoad.Get_Fields("limit")))
							{
								txtLast.Text = "*";
							}
						}
					}
					else
					{
						txtLast.Text = "";
					}
				}
			}
			else
			{
				txtLast.Text = "";
			}
			dblTotalCurrent += dblCurrent;
			dblTotalMTD += dblMTD;
			dblTotalQTD += dblQTD;
			dblTotalFiscal += dblFiscal;
			dblTotalCalendar += dblCalendar;
			dblTotalLTD += dblLTD;
			dblDedCurrent += dblCurrent;
			dblDedMTD += dblMTD;
			dblDedQTD += dblQTD;
			dblDedFiscal += dblFiscal;
			dblDedCalendar += dblCalendar;
			dblDedLTD += dblLTD;
			lngDedCount += 1;
			lngTotEmployees += 1;
			clsLoad.MoveNext();
			// If Not .EndOfFile Then
			// If boolMatchRecords Then
			// strTitle = clsLoad.Fields("deddeductionnumber") & " - " & clsLoad.Fields("description")
			// Else
			// If clsDeductions.FindFirstRecord("ID", clsLoad.Fields("deddeductionnumber")) Then
			// strTitle = clsDeductions.Fields("deductionnumber") & "-" & clsDeductions.Fields("description")
			// End If
			// End If
			// End If
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtNumEmployees.Text = "Employees: " + FCConvert.ToString(lngDedCount);
			txtDedCurrent.Text = Strings.Format(dblDedCurrent, "0.00");
			txtDedMTD.Text = Strings.Format(dblDedMTD, "0.00");
			txtDedQTD.Text = Strings.Format(dblDedQTD, "0.00");
			txtDedFiscal.Text = Strings.Format(dblDedFiscal, "0.00");
			txtDedCalendar.Text = Strings.Format(dblDedCalendar, "0.00");
			txtDedLTD.Text = Strings.Format(dblDedLTD, "0.00");
			lngDedCount = 0;
			dblDedCurrent = 0;
			dblDedQTD = 0;
			dblDedMTD = 0;
			dblDedFiscal = 0;
			dblDedCalendar = 0;
			dblDedLTD = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtDeductionHeading.Text = strTitle;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalCurrent.Text = Strings.Format(dblTotalCurrent, "0.00");
			txtTotalCalendar.Text = Strings.Format(dblTotalCalendar, "0.00");
			txtTotalFiscal.Text = Strings.Format(dblTotalFiscal, "0.00");
			txtTotalLTD.Text = Strings.Format(dblTotalLTD, "0.00");
			txtTotalMTD.Text = Strings.Format(dblTotalMTD, "0.00");
			txtTotalQTD.Text = Strings.Format(dblTotalQTD, "0.00");
			txtNumberOfEmployees.Text = "Number Employees: " + FCConvert.ToString(lngTotEmployees);
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			txtDeductionHeading.Text = strTitle;
		}

	

		private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
