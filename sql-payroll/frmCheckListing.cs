﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCheckListing : BaseForm
	{
		public frmCheckListing()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.cmbType.Text = "Check Listing Regular";
            this.cmbReportOn.Text = "Single Employee";
            this.cmbSort.Text = "Name";
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCheckListing InstancePtr
		{
			get
			{
				return (frmCheckListing)Sys.GetInstance(typeof(frmCheckListing));
			}
		}

		protected frmCheckListing _InstancePtr = null;
		//=========================================================
		private void chkVoid_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!(chkVoid.CheckState == Wisej.Web.CheckState.Checked))
			{
				chkVoidOnly.CheckState = Wisej.Web.CheckState.Unchecked;
				chkVoidOnly.Enabled = false;
			}
			else
			{
				chkVoidOnly.Enabled = true;
			}
		}

		private void frmCheckListing_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCheckListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCheckListing properties;
			//frmCheckListing.ScaleWidth	= 7485;
			//frmCheckListing.ScaleHeight	= 5625;
			//frmCheckListing.LinkTopic	= "Form1";
			//frmCheckListing.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			string strTemp;
			strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("PYCheckDetailShowAsGross")));
			if (strTemp != string.Empty)
			{
				if (FCConvert.CBool(strTemp))
				{
					chkGrossWages.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			bool boolGW = false;
			if (chkGrossWages.CheckState == Wisej.Web.CheckState.Checked)
			{
				modRegistry.SaveRegistryKey("PYCheckDetailShowAsGross", "True");
				boolGW = true;
			}
			else
			{
				modRegistry.SaveRegistryKey("PYCheckDetailShowAsGross", "False");
				boolGW = false;
			}
			if (FCConvert.ToBoolean(GetSQL()))
			{
				//FC:FINAL:AM:#4075 - don't show the loading window
                //frmWait.InstancePtr.Init("Loading Report", false, 100);
				if (cmbType.Text == "Check Listing Regular")
				{
					// this is the regular report
					// rptCheckListingRegular.Show , MDIParent
					// frmReportViewer.Init rptCheckListingRegular
					rptCheckListingRegular.InstancePtr.Init(ref boolGW);
				}
				else if (cmbType.Text == "Check Listing Tax Breakdown")
				{
					// rptCheckListingTax.Show , MDIParent
					// frmReportViewer.Init rptCheckListingTax
					rptCheckListingTax.InstancePtr.Init(ref boolGW);
				}
				else if (cmbType.Text == "TA Check Detail")
				{
					// rptCheckListingTax.Show , MDIParent
					frmReportViewer.InstancePtr.Init(rptTACheckDetail.InstancePtr, boolAllowEmail: true, strAttachmentName: "TACheckDetail");
				}
				else if (cmbType.Text == "Account Number Detail")
				{
					// rptAccountTotalsReport.Show , MDIParent
					// rptNewAccountTotalsReport.Show , MDIParent
					frmReportViewer.InstancePtr.Init(rptNewAccountTotalsReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "AccountTotals");
					//frmWait.InstancePtr.Unload();
				}
				else
				{
					// rptCheckListingDistribution.Show , MDIParent
					frmReportViewer.InstancePtr.Init(rptCheckListingDistribution.InstancePtr, boolAllowEmail: true, strAttachmentName: "CheckListing");
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object GetSQL()
		{
			object GetSQL = null;
			int intQuarter;
			if (cmbType.Text == "Check Listing Regular" || cmbType.Text == "Check Listing Tax Breakdown")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT distinct tblcheckdetail.employeenumber,checknumber,paydate,payrunid, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName,tblemployeemaster.middlename,tblemployeemaster.desig FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE convert(int, isnull(tblCheckDetail.CheckNumber, 0)) <> 0 AND tblCheckDetail.EmployeeNumber <> '' ";
			}
			else if (cmbType.Text == "TA Check Detail")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT tblCheckDetail.* FROM tblCheckDetail WHERE TrustRecord = 1 ";
			}
			else if (cmbType.Text == "Account Number Detail")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT tblCheckDetail.DistAccountNumber, tblCheckDetail.EmployeeNumber, tblCheckDetail.DistPayCategory, Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay, Sum(tblCheckDetail.DistHours) AS SumOfDistHours From tblCheckDetail Where tblCheckDetail.distributionrecord = 1 ";
			}
			else
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT DISTINCT tblCheckDetail.*, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName,tblemployeemaster.middlename,tblemployeemaster.desig FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE convert(int, isnull(tblCheckDetail.CheckNumber, 0)) <> 0 AND tblCheckDetail.EmployeeNumber <> '' ";
			}
			GetSQL = false;
			modGlobalVariables.Statics.gstrCheckListingSort = string.Empty;
			if (cmbReportOn.Text == "Single Employee")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("You must first enter an Employee Number to continue.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeNumber.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeNumber.Text + "'";
					modGlobalVariables.Statics.gstrCheckListingSort = "Employee Number";
				}
			}
			else if (cmbReportOn.Text == "Month")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("Pay Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND Month(tblCheckDetail.PayDate) = " + txtEmployeeNumber.Text + " AND YEAR(TBLCHECKDETAIL.PAYDATE) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year);
					modGlobalVariables.Statics.gstrCheckListingSort = "Month of Pay Date: " + txtEmployeeNumber.Text;
				}
			}
			else if (cmbReportOn.Text == "Quarter")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("Pay Date Quarter must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					switch (txtEmployeeNumber.Text)
					{
						case "1":
							{
								modGlobalVariables.Statics.gstrCheckListingSQL += " AND ((Month(tblCheckDetail.PayDate) = 1 AND Year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ") OR (Month(tblCheckDetail.PayDate) = 2 AND Year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ") OR (Month(tblCheckDetail.PayDate) = 3 AND Year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "))";
								break;
							}
						case "2":
							{
								modGlobalVariables.Statics.gstrCheckListingSQL += " AND ((Month(tblCheckDetail.PayDate) = 4 OR Month(tblCheckDetail.PayDate) = 5 OR Month(tblCheckDetail.PayDate) = 6)) and year (paydate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year);
								break;
							}
						case "3":
							{
								modGlobalVariables.Statics.gstrCheckListingSQL += " AND ((Month(tblCheckDetail.PayDate) = 7 OR Month(tblCheckDetail.PayDate) = 8 OR Month(tblCheckDetail.PayDate) = 9)) and year (paydate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year);
								break;
							}
						case "4":
							{
								modGlobalVariables.Statics.gstrCheckListingSQL += " AND ((Month(tblCheckDetail.PayDate) = 10 OR Month(tblCheckDetail.PayDate) = 11 OR Month(tblCheckDetail.PayDate) = 12)) and year (paydate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year);
								break;
							}
					}
					//end switch
					modGlobalVariables.Statics.gstrCheckListingSort = "Quarter of Pay Date: " + txtEmployeeNumber.Text + " of " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year);
				}
			}
			else if (cmbReportOn.Text == "Year")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("Pay Date Year must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND Year(tblCheckDetail.PayDate) = " + txtEmployeeNumber.Text;
					modGlobalVariables.Statics.gstrCheckListingSort = "Year of Pay Date: " + txtEmployeeNumber.Text;
				}
			}
			else if (cmbReportOn.Text == "Date Range")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					modGlobalVariables.Statics.gstrCheckListingSort = "Pay Dates Between: " + txtStartDate.Text + " - " + txtEndDate.Text;
				}
			}
			else if (cmbReportOn.Text == "All Files")
			{
			}
			else if (cmbReportOn.Text == "Emp/Date Range")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeNumber.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeNumber.Text + "'";
					modGlobalVariables.Statics.gstrCheckListingSort += "Employee Number: " + txtEmployeeNumber.Text;
				}
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					modGlobalVariables.Statics.gstrCheckListingSort += " AND Pay Dates Between: " + txtStartDate.Text + " - " + txtEndDate.Text;
				}
			}
			else if (cmbReportOn.Text == "Specific Check")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("You must first enter a Check Number to continue.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeNumber.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.CheckNumber = " + txtEmployeeNumber.Text;
					modGlobalVariables.Statics.gstrCheckListingSort += "Check Number: " + txtEmployeeNumber.Text;
				}
			}
			else
			{
			}
			if (cmbType.Text == "TA Check Detail")
			{
				if (chkVoid.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " ";
					// AND CheckVoid = 1 "
					if (chkVoidOnly.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " and checkvoid = 1 ";
					}
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND checkvoid = 0 ";
				}
				modGlobalVariables.Statics.gstrCheckListingSQL += " Order by CheckNumber, paydate ";
			}
			else if (cmbType.Text == "Account Number Detail")
			{
				if (chkVoid.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " ";
					// AND CheckVoid = 1 "
					if (chkVoidOnly.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " and checkvoid = 1 ";
					}
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND checkvoid = 0 ";
				}
				modGlobalVariables.Statics.gstrCheckListingSQL += " AND DistAccountNumber <> ''";
				modGlobalVariables.Statics.gstrCheckListingSQL += " GROUP BY tblCheckDetail.DistAccountNumber, tblCheckDetail.EmployeeNumber, tblCheckDetail.DistPayCategory";
			}
			else
			{
				if (chkVoid.CheckState == CheckState.Checked)
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " ";
					if (chkVoidOnly.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " and checkvoid = 1 ";
					}
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND CheckVoid = 0 ";
				}
				if (cmbType.Text == "Employee Distribution Breakdown")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND distributionrecord = 1 ";
					if (cmbSort.Text == "Name")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,middlename,desig, PayDate,DistPayCategory";
						modGlobalVariables.Statics.gstrCheckListingSort = "- - Employee Number - -";
						modGlobalVariables.Statics.gstrCheckListingReportGroup = "EmployeeNumber";
					}
					else if (cmbSort.Text == "Check")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by CheckNumber,PayDate,LastName,FirstName,middlename,desig, DistPayCategory";
						modGlobalVariables.Statics.gstrCheckListingSort = "- - Check Number - -";
						modGlobalVariables.Statics.gstrCheckListingReportGroup = "CheckNumber";
					}
					else if (cmbSort.Text == "Pay Date")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by PayDate,LastName,FirstName,middlename,desig, DistPayCategory";
						modGlobalVariables.Statics.gstrCheckListingSort = "- - PayDate - -";
						modGlobalVariables.Statics.gstrCheckListingReportGroup = "PayDate";
					}
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND TotalRecord = 1 ";
					if (cmbSort.Text == "Name")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,middlename,desig,paydate";
						modGlobalVariables.Statics.gstrCheckListingSort = "- - Employee Number - -";
						modGlobalVariables.Statics.gstrCheckListingReportGroup = "EmployeeNumber";
					}
					else if (cmbSort.Text == "Check")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by CheckNumber,LastName,FirstName,middlename,desig";
						modGlobalVariables.Statics.gstrCheckListingSort = "- - Check Number - -";
						modGlobalVariables.Statics.gstrCheckListingReportGroup = "CheckNumber";
					}
					else if (cmbSort.Text == "Pay Date")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by PayDate,LastName,FirstName,middlename,desig";
						modGlobalVariables.Statics.gstrCheckListingSort = "- - PayDate - -";
						modGlobalVariables.Statics.gstrCheckListingReportGroup = "PayDate";
					}
				}
			}
			GetSQL = true;
			return GetSQL;
		}

		private void optReportOn_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
            //FC:FINAL:AM:#i2115 - use the text of the combo instead of the index
            //txtEmployeeNumber.Visible = (Index == 0 || Index == 1 || Index == 2 || Index == 3 || Index == 7 || Index == 6);
            //txtStartDate.Visible = (Index == 4 || Index == 6);
            //txtEndDate.Visible = (Index == 4 || Index == 6);
            //lblCaption.Visible = Index != 5;
            txtEmployeeNumber.Visible = (cmbReportOn.Text != "Date Range" && cmbReportOn.Text != "All Files");
            txtStartDate.Visible = (cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Emp/Date Range");
            txtEndDate.Visible = (cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Emp/Date Range");
            lblCaption.Visible = cmbReportOn.Text != "All Files";
            lblDash.Visible = txtStartDate.Visible;
			txtEmployeeNumber.Text = string.Empty;
			switch (cmbReportOn.Text)
			{
				case "Single Employee":
					{
						lblCaption.Text = "Employee Number";
						break;
					}
				case "Month":
					{
						lblCaption.Text = "Month of Pay Date";
						break;
					}
				case "Quarter":
					{
						lblCaption.Text = "Qtr. of Pay Date";
						break;
					}
				case "Year":
					{
						lblCaption.Text = "Year of Pay Date";
						break;
					}
				case "Date Range":
					{
						lblCaption.Text = "Pay Date Range";
						break;
					}
				//case 5:
				//	{
				//		break;
				//	}
				case "Emp/Date Range":
					{
						lblCaption.Text = "Emp / Date Range";
						break;
					}
				case "Specific Check":
					{
						// specific check
						lblCaption.Text = "Check Number";
						break;
					}
			}
			//end switch
		}

		private void optReportOn_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReportOn.SelectedIndex;
			optReportOn_CheckedChanged(index, sender, e);
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index > 1)
			{
				chkGrossWages.Visible = false;
			}
			else
			{
				chkGrossWages.Visible = true;
			}
            if (Index == 3)
            {
                cmbReportOn.Items.Clear();
                cmbReportOn.Items.AddRange(new object[] {
                    "Date Range",
                    "Specific Check",
                    "All Files" });
                cmbReportOn.Text = "Specific Check";
            }
            else
            {
                string tmp = cmbReportOn.Text;
                string temp = txtEmployeeNumber.Text;
                cmbReportOn.Items.Clear();
                this.cmbReportOn.Items.AddRange(new object[] {
                    "Single Employee",
                    "Emp/Date Range",
                    "Month",
                    "Quarter",
                    "Year",
                    "Date Range",
                    "Specific Check",
                    "All Files"});
                //FC:FINAL:BSE:#4074 item should not change, unless inapplicable to the type of report
                if (cmbReportOn.Items.Contains(tmp))
                {
                    cmbReportOn.Text = tmp;
                }
                else
                {
                    cmbReportOn.Text = "Single Employee";
                }
                //FC:FINAL:BSE:#4071 employee number field is cleared after checked changed 
                if (txtEmployeeNumber.Text == "")
                {
                    txtEmployeeNumber.Text = temp;
                }
            }
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

		//FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtEndDate_TextChanged(object sender, System.EventArgs e)
		//{
		//	if ((txtEndDate.Text.Length == 2 || txtEndDate.Text.Length == 5) && Strings.Right(txtEndDate.Text, 1) != "/")
		//	{
		//		txtEndDate.Text = txtEndDate.Text + "/";
		//		txtEndDate.SelectionStart = txtEndDate.Text.Length;
		//		txtEndDate.SelectionLength = txtEndDate.Text.Length;
		//	}
		//}

		//private void txtEndDate_Enter(object sender, System.EventArgs e)
		//{
		//	txtEndDate.SelectionStart = 0;
		//	txtEndDate.SelectionLength = txtEndDate.Text.Length;
		//}

		//private void txtStartDate_TextChanged(object sender, System.EventArgs e)
		//{
		//	if ((txtStartDate.Text.Length == 2 || txtStartDate.Text.Length == 5) && Strings.Right(txtStartDate.Text, 1) != "/")
		//	{
		//		txtStartDate.Text = txtStartDate.Text + "/";
		//		txtStartDate.SelectionStart = txtStartDate.Text.Length;
		//		txtStartDate.SelectionLength = txtStartDate.Text.Length;
		//	}
		//}

		//private void txtStartDate_Enter(object sender, System.EventArgs e)
		//{
		//	txtStartDate.SelectionStart = 0;
		//	txtStartDate.SelectionLength = txtStartDate.Text.Length;
		//}       
    }
}
