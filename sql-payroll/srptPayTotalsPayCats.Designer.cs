﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptPayTotalsPayCats.
	/// </summary>
	partial class srptPayTotalsPayCats
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptPayTotalsPayCats));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDesc,
				this.txtCurrent,
				this.txtMTD,
				this.txtQTD,
				this.txtFYTD,
				this.txtCYTD
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// txtDesc
			// 
			this.txtDesc.Height = 0.1666667F;
			this.txtDesc.Left = 0.02083333F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Text = "Field1";
			this.txtDesc.Top = 0F;
			this.txtDesc.Width = 2.229167F;
			// 
			// txtCurrent
			// 
			this.txtCurrent.Height = 0.1666667F;
			this.txtCurrent.Left = 2.375F;
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.Style = "text-align: right";
			this.txtCurrent.Text = "Field2";
			this.txtCurrent.Top = 0F;
			this.txtCurrent.Width = 0.9166667F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.1666667F;
			this.txtMTD.Left = 3.375F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.Style = "text-align: right";
			this.txtMTD.Text = "Field3";
			this.txtMTD.Top = 0F;
			this.txtMTD.Width = 0.9166667F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.1666667F;
			this.txtQTD.Left = 4.375F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "text-align: right";
			this.txtQTD.Text = "Field4";
			this.txtQTD.Top = 0F;
			this.txtQTD.Width = 0.9166667F;
			// 
			// txtFYTD
			// 
			this.txtFYTD.Height = 0.1666667F;
			this.txtFYTD.Left = 5.375F;
			this.txtFYTD.Name = "txtFYTD";
			this.txtFYTD.Style = "text-align: right";
			this.txtFYTD.Text = "Field5";
			this.txtFYTD.Top = 0F;
			this.txtFYTD.Width = 0.9166667F;
			// 
			// txtCYTD
			// 
			this.txtCYTD.Height = 0.1666667F;
			this.txtCYTD.Left = 6.375F;
			this.txtCYTD.Name = "txtCYTD";
			this.txtCYTD.Style = "text-align: right";
			this.txtCYTD.Text = "Field6";
			this.txtCYTD.Top = 0F;
			this.txtCYTD.Width = 0.9166667F;
			// 
			// srptPayTotalsPayCats
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTD;
	}
}
