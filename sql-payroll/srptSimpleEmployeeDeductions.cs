//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptSimpleEmployeeDeductions.
	/// </summary>
	public partial class srptSimpleEmployeeDeductions : FCSectionReport
	{
		public srptSimpleEmployeeDeductions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "sub Employee Deduction Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptSimpleEmployeeDeductions InstancePtr
		{
			get
			{
				return (srptSimpleEmployeeDeductions)Sys.GetInstance(typeof(srptSimpleEmployeeDeductions));
			}
		}

		protected srptSimpleEmployeeDeductions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsDeduction?.Dispose();
				rsData?.Dispose();
                rsDeduction = null;
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptSimpleEmployeeDeductions	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsDeduction = new clsDRWrapper();
		double dblAmount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			dblAmount = modCoreysSweeterCode.GetCYTDDeductionTotal(rsData.Get_Fields("deductionCODE"), rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtAmount.Text = Strings.Format(dblAmount, "#,##0.00");
			txtTotalAmount.Text = Strings.Format(Conversion.Val(txtTotalAmount.Text) + dblAmount, "#,##0.00");
			txtDedCode.Text = string.Empty;
			if (rsDeduction.FindFirstRecord("ID", rsData.Get_Fields_Int32("DeductionCode")))
			{
				txtDedCode.Text = rsDeduction.Get_Fields_String("Description");
			}
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsData.OpenRecordset("SELECT tblEmployeeDeductions.EmployeeNumber, tblEmployeeDeductions.DeductionCode From tblEmployeeDeductions GROUP BY tblEmployeeDeductions.EmployeeNumber, tblEmployeeDeductions.DeductionCode Having EmployeeNumber = '" + this.UserData + "' ORDER BY tblEmployeeDeductions.EmployeeNumber, tblEmployeeDeductions.DeductionCode", "TWPY0000.vb1");
			rsDeduction.OpenRecordset("Select * from tblDeductionSetup");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtTotalAmount.Text = Strings.Format(txtTotalAmount.Text, "#,##0.00");
		}

		
	}
}
