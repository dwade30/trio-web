//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmPayrollProcessSetup : BaseForm
	{
		public frmPayrollProcessSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPayrollProcessSetup InstancePtr
		{
			get
			{
				return (frmPayrollProcessSetup)Sys.GetInstance(typeof(frmPayrollProcessSetup));
			}
		}

		protected frmPayrollProcessSetup _InstancePtr = null;
		//=========================================================
		private bool boolSaveOK;
		private int intCalendar;
		// vbPorter upgrade warning: aryFirstWeekOf As object	OnWrite
		private object[,] aryFirstWeekOf = new object[13 + 1, 13 + 1];
		private bool boolAllowChange;
		private clsHistory clsHistoryClass = new clsHistory();
		DateTime datSelectedDate;

		private void cmdPeriodStartCalendar_Click(object sender, System.EventArgs e)
		{
			if (Information.IsDate(t2kPeriodStartDate.Text))
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, Convert.ToDateTime(t2kPeriodStartDate.Text));
			}
			else
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.FromOADate(0));
			}
			if (datSelectedDate.ToOADate() != 0)
			{
				t2kPeriodStartDate.Text = Strings.Format(datSelectedDate, "MM/dd/yyyy");
			}
		}

		private void cboFirstWeek_DropDown(object sender, System.EventArgs e)
		{
			if (!boolAllowChange)
			{
				if (MessageBox.Show("Changing this setting will affect this pay run. Continue", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGlobalFunctions.AddCYAEntry_6("PY", "First Week Of setting changed in Check Process Setup by: " + modGlobalVariables.Statics.gstrUser + " On: " + DateTime.Today.ToShortDateString());
					boolAllowChange = true;
				}
			}
		}

		private void cboManualCheck_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.cboWeekNumber.Text == this.cboNumberPayWeeks.Text)
			{
				chkGroupID.CheckState = Wisej.Web.CheckState.Unchecked;
				fraTrustAgency.Visible = true;
				//yes
				if (!cmbNo.Items.Contains("Month T&A checks have been run."))
				{
					cmbNo.Items.Add("Month T&A checks have been run.");
				}
				//no
				if (!cmbNo.Items.Contains("Month T&A checks have NOT been run."))
				{
					cmbNo.Items.Add("Month T&A checks have NOT been run.");
				}
				lblWarning.Visible = true;
				//grouponly
				if (cmbNo.Items.Contains("Run T&A checks for group X only"))
				{
					cmbNo.Items.Remove("Run T&A checks for group X only");
				}
				//allemployees
				if (cmbNo.Items.Contains("Run T&A checks for ALL employees"))
				{
					cmbNo.Items.Remove("Run T&A checks for ALL employees");
				}
				//weeklyrecipients
				if (cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
				{
					cmbNo.Items.Remove("Run T&A checks for weekly recipients only");
				}
				cmbNo.Text = "Month T&A checks have NOT been run.";
			}
			else
			{
				fraTrustAgency.Visible = false;
				if (cmbNo.Items.Contains("Month T&A checks have been run."))
				{
					cmbNo.Items.Remove("Month T&A checks have been run.");
				}
				if (cmbNo.Items.Contains("Month T&A checks have NOT been run."))
				{
					cmbNo.Items.Remove("Month T&A checks have NOT been run.");
				}
				if (!cmbNo.Items.Contains("Run T&A checks for group X only"))
				{
					cmbNo.Items.Add("Run T&A checks for group X only");
				}
				if (!cmbNo.Items.Contains("Run T&A checks for ALL employees"))
				{
					cmbNo.Items.Add("Run T&A checks for ALL employees");
				}
				if (!cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
				{
					cmbNo.Items.Add("Run T&A checks for weekly recipients only");
				}
			}
		}

		private void cboNumberPayWeeks_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.cboWeekNumber.Text == this.cboNumberPayWeeks.Text)
			{
				chkGroupID.CheckState = Wisej.Web.CheckState.Unchecked;
				fraTrustAgency.Visible = true;
				lblWarning.Visible = true;
				//yes
				if (!cmbNo.Items.Contains("Month T&A checks have been run."))
				{
					cmbNo.Items.Add("Month T&A checks have been run.");
				}
				//no
				if (!cmbNo.Items.Contains("Month T&A checks have NOT been run."))
				{
					cmbNo.Items.Add("Month T&A checks have NOT been run.");
				}
				lblWarning.Visible = true;
				//grouponly
				if (cmbNo.Items.Contains("Run T&A checks for group X only"))
				{
					cmbNo.Items.Remove("Run T&A checks for group X only");
				}
				//allemployees
				if (cmbNo.Items.Contains("Run T&A checks for ALL employees"))
				{
					cmbNo.Items.Remove("Run T&A checks for ALL employees");
				}
				//weeklyrecipients
				if (cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
				{
					cmbNo.Items.Remove("Run T&A checks for weekly recipients only");
				}
				cmbNo.Text = "Month T&A checks have NOT been run.";
			}
			else
			{
				fraTrustAgency.Visible = false;
				//yes
				if (cmbNo.Items.Contains("Month T&A checks have been run."))
				{
					cmbNo.Items.Remove("Month T&A checks have been run.");
				}
				//no
				if (cmbNo.Items.Contains("Month T&A checks have NOT been run."))
				{
					cmbNo.Items.Remove("Month T&A checks have NOT been run.");
				}
				lblWarning.Visible = true;
				//grouponly
				if (!cmbNo.Items.Contains("Run T&A checks for group X only"))
				{
					cmbNo.Items.Add("Run T&A checks for group X only");
				}
				//allemployees
				if (!cmbNo.Items.Contains("Run T&A checks for ALL employees"))
				{
					cmbNo.Items.Add("Run T&A checks for ALL employees");
				}
				//weeklyrecipients
				if (!cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
				{
					cmbNo.Items.Add("Run T&A checks for weekly recipients only");
				}
			}
		}

		private void cboWeekNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intFirstMonthOfSetting = 0;
				lblFirstWeek.Visible = FCConvert.ToDouble(cboWeekNumber.Text) == 1;
				cboFirstWeek.Visible = FCConvert.ToDouble(cboWeekNumber.Text) == 1;
				// CHECK TO SEE WHAT TYPE O
				if (FCConvert.ToDouble(cboWeekNumber.Text) == 1)
				{
					intFirstMonthOfSetting = FCConvert.ToInt32(aryFirstWeekOf[GetNumberOfMonth(lblWeekMonth.Text), GetNumberOfMonth(txtFirstFiscalMonth.Text)]) - 1;
					cboFirstWeek.SelectedIndex = intFirstMonthOfSetting;
				}
				if (this.cboWeekNumber.Text == this.cboNumberPayWeeks.Text)
				{
					chkGroupID.CheckState = Wisej.Web.CheckState.Unchecked;
					fraTrustAgency.Visible = true;
					lblWarning.Visible = true;
					//yes
					if (!cmbNo.Items.Contains("Month T&A checks have been run."))
					{
						cmbNo.Items.Add("Month T&A checks have been run.");
					}
					//no
					if (!cmbNo.Items.Contains("Month T&A checks have NOT been run."))
					{
						cmbNo.Items.Add("Month T&A checks have NOT been run.");
					}
					lblWarning.Visible = true;
					//grouponly
					if (cmbNo.Items.Contains("Run T&A checks for group X only"))
					{
						cmbNo.Items.Remove("Run T&A checks for group X only");
					}
					//allemployees
					if (cmbNo.Items.Contains("Run T&A checks for ALL employees"))
					{
						cmbNo.Items.Remove("Run T&A checks for ALL employees");
					}
					//weeklyrecipients
					if (cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
					{
						cmbNo.Items.Remove("Run T&A checks for weekly recipients only");
					}
					cmbNo.Text = "Month T&A checks have NOT been run.";
				}
				else
				{
					fraTrustAgency.Visible = false;
					//yes
					if (cmbNo.Items.Contains("Month T&A checks have been run."))
					{
						cmbNo.Items.Remove("Month T&A checks have been run.");
					}
					//no
					if (cmbNo.Items.Contains("Month T&A checks have NOT been run."))
					{
						cmbNo.Items.Remove("Month T&A checks have NOT been run.");
					}
					lblWarning.Visible = true;
					//grouponly
					if (!cmbNo.Items.Contains("Run T&A checks for group X only"))
					{
						cmbNo.Items.Add("Run T&A checks for group X only");
					}
					//allemployees
					if (!cmbNo.Items.Contains("Run T&A checks for ALL employees"))
					{
						cmbNo.Items.Add("Run T&A checks for ALL employees");
					}
					//weeklyrecipients
					if (!cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
					{
						cmbNo.Items.Add("Run T&A checks for weekly recipients only");
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r" + "Error setting up for Pay Run. Please Call TRIO.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkGroupID_CheckedChanged(object sender, System.EventArgs e)
		{
			txtGroup.Visible = 0 != (chkGroupID.CheckState);
			if (txtGroup.Visible)
			{
				cboManualCheck.SelectedIndex = 0;
				fraTrustAgency.Visible = true;
				lblWarning.Visible = false;
				//yes
				if (cmbNo.Items.Contains("Month T&A checks have been run."))
				{
					cmbNo.Items.Remove("Month T&A checks have been run.");
				}
				//no
				if (cmbNo.Items.Contains("Month T&A checks have NOT been run."))
				{
					cmbNo.Items.Remove("Month T&A checks have NOT been run.");
				}
				lblWarning.Visible = true;
				//grouponly
				if (!cmbNo.Items.Contains("Run T&A checks for group X only"))
				{
					cmbNo.Items.Add("Run T&A checks for group X only");
				}
				//allemployees
				if (!cmbNo.Items.Contains("Run T&A checks for ALL employees"))
				{
					cmbNo.Items.Add("Run T&A checks for ALL employees");
				}
				//weeklyrecipients
				if (!cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
				{
					cmbNo.Items.Add("Run T&A checks for weekly recipients only");
				}
				cmbNo.Text = "Run T&A checks for group X only";
				lblFundNumber.Visible = true;
				txtFundNumber.Visible = true;
				clsDRWrapper rsData = new clsDRWrapper();
				// Call rsData.OpenRecordset("Select * from tblPayrollAccounts where Code = 'CC'")
				// If Not rsData.EndOfFile Then
				// txtFundNumber.Text = Mid(rsData.Fields("Account"), 3, Val(Mid(Ledger, 1, 2)))
				// Else
				// txtFundNumber.Text = 0
				// End If
				rsData.OpenRecordset("select * from tblpayrollprocesssetup where groupid = 1 and grouptext = '1' ", "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					txtFundNumber.Text = FCConvert.ToString(Conversion.Val(rsData.Get_Fields("usegroupmultifund")));
				}
				else
				{
					rsData.OpenRecordset("select top 1 * from tblpayrollsteps where groupid = 1 and grouptext = '1' order by id desc", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						txtFundNumber.Text = FCConvert.ToString(Conversion.Val(rsData.Get_Fields("usegroupmultifund")));
					}
					else
					{
						txtFundNumber.Text = FCConvert.ToString(0);
					}
				}
				txtGroup.Text = FCConvert.ToString(1);
				txtGroup.Visible = true;
				txtGroup.Focus();
				txtGroup.SelectionStart = 0;
				txtGroup.SelectionLength = 1;
			}
			else
			{
				chkGroupID.CheckState = Wisej.Web.CheckState.Unchecked;
				fraTrustAgency.Visible = true;
				lblWarning.Visible = true;
				//yes
				if (!cmbNo.Items.Contains("Month T&A checks have been run."))
				{
					cmbNo.Items.Add("Month T&A checks have been run.");
				}
				//no
				if (!cmbNo.Items.Contains("Month T&A checks have NOT been run."))
				{
					cmbNo.Items.Add("Month T&A checks have NOT been run.");
				}
				lblWarning.Visible = true;
				//grouponly
				if (cmbNo.Items.Contains("Run T&A checks for group X only"))
				{
					cmbNo.Items.Remove("Run T&A checks for group X only");
				}
				//allemployees
				if (cmbNo.Items.Contains("Run T&A checks for ALL employees"))
				{
					cmbNo.Items.Remove("Run T&A checks for ALL employees");
				}
				//weeklyrecipients
				if (cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
				{
					cmbNo.Items.Remove("Run T&A checks for weekly recipients only");
				}
				cmbNo.Text = "Month T&A checks have NOT been run.";
				lblFundNumber.Visible = false;
				txtFundNumber.Visible = false;
				txtFundNumber.Text = FCConvert.ToString(0);
				if (cboWeekNumber.Text == cboNumberPayWeeks.Text)
				{
				}
				else
				{
					fraTrustAgency.Visible = false;
				}
				// optYes.Visible = False
				// optNo.Visible = False
				// optGroupOnly.Visible = True
				// optAllEmployees.Visible = True
				// optWeeklyRecipients.Visible = True
				// optGroupOnly.Value = False
			}
		}

		private void cmdCalendar1_Click(object sender, System.EventArgs e)
		{
			if (Information.IsDate(mebWeekEndDate.Text))
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, Convert.ToDateTime(mebWeekEndDate.Text));
			}
			else
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.FromOADate(0));
			}
			if (datSelectedDate.ToOADate() != 0)
			{
				mebWeekEndDate.Text = Strings.Format(datSelectedDate, "MM/dd/yyyy");
			}
		}

		private void cmdCalendar2_Click(object sender, System.EventArgs e)
		{
			if (Information.IsDate(mebPayDate.Text))
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, Convert.ToDateTime(mebPayDate.Text));
			}
			else
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.FromOADate(0));
			}
			if (datSelectedDate.ToOADate() != 0)
			{
				mebPayDate.Text = Strings.Format(datSelectedDate, "MM/dd/yyyy");
			}
		}

		private void frmPayrollProcessSetup_Activated(object sender, System.EventArgs e)
		{
		}
		
		private void frmPayrollProcessSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPayrollProcessSetup_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsData = new clsDRWrapper();
				int intFirstMonthOfSetting;
				// txtFirstFiscalMonth.Locked = gboolBudgetary
				txtFirstFiscalMonth.ReadOnly = true;
				// gboolBudgetary
				rsData.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					MessageBox.Show("Customize information has yet to be saved.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					chkBiWeekly.Visible = true;
				}
				else
				{
					chkBiWeekly.Visible = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("WeeklyPR"));
				}
				clsDRWrapper rsLengths = new clsDRWrapper();
				bool boolWeekly = false;
				bool boolPRBiWeekly = false;
				chkBiWeeklyDeds.Visible = false;
				rsLengths.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (!rsLengths.EndOfFile())
				{
					boolWeekly = (FCConvert.ToBoolean(rsLengths.Get_Fields_Boolean("PayFreqWeekly")) ? true : false);
				}
				if (boolWeekly)
				{
					rsLengths.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
					if (!rsLengths.EndOfFile())
					{
						boolPRBiWeekly = (FCConvert.ToBoolean(rsLengths.Get_Fields_Boolean("WeeklyPR")) ? true : false);
					}
					if (boolPRBiWeekly)
					{
					}
					else
					{
						chkBiWeeklyDeds.Visible = true;
					}
				}
				// CALL ID 83864 MATTHEW 12/16/2005
				if (boolWeekly)
				{
					cboWeekNumber.Clear();
					cboNumberPayWeeks.Clear();
					cboWeekNumber.AddItem("1");
					cboWeekNumber.AddItem("2");
					cboWeekNumber.AddItem("3");
					cboWeekNumber.AddItem("4");
					cboWeekNumber.AddItem("5");
					cboNumberPayWeeks.AddItem("1");
					cboNumberPayWeeks.AddItem("2");
					cboNumberPayWeeks.AddItem("3");
					cboNumberPayWeeks.AddItem("4");
					cboNumberPayWeeks.AddItem("5");
				}
				else
				{
					cboWeekNumber.Clear();
					cboNumberPayWeeks.Clear();
					cboWeekNumber.AddItem("1");
					cboWeekNumber.AddItem("2");
					cboWeekNumber.AddItem("3");
					cboNumberPayWeeks.AddItem("1");
					cboNumberPayWeeks.AddItem("2");
					cboNumberPayWeeks.AddItem("3");
				}
                //FC:FINAL:AM:#2702 - initialize array before ShowData
                SetFirstMonthArray();
                ShowData();
				//SetFirstMonthArray();
				boolAllowChange = false;
				if (this.cboWeekNumber.Text == this.cboNumberPayWeeks.Text)
				{
					chkGroupID.CheckState = Wisej.Web.CheckState.Unchecked;
					fraTrustAgency.Visible = true;
					lblWarning.Visible = true;
					//yes
					if (!cmbNo.Items.Contains("Month T&A checks have been run."))
					{
						cmbNo.Items.Add("Month T&A checks have been run.");
					}
					//no
					if (!cmbNo.Items.Contains("Month T&A checks have NOT been run."))
					{
						cmbNo.Items.Add("Month T&A checks have NOT been run.");
					}
					lblWarning.Visible = true;
					//grouponly
					if (cmbNo.Items.Contains("Run T&A checks for group X only"))
					{
						cmbNo.Items.Remove("Run T&A checks for group X only");
					}
					//allemployees
					if (cmbNo.Items.Contains("Run T&A checks for ALL employees"))
					{
						cmbNo.Items.Remove("Run T&A checks for ALL employees");
					}
					//weeklyrecipients
					if (cmbNo.Items.Contains("Run T&A checks for weekly recipients only"))
					{
						cmbNo.Items.Remove("Run T&A checks for weekly recipients only");
					}
					cmbNo.Text = "Month T&A checks have NOT been run.";
				}
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				clsHistoryClass.SetGridIDColumn("PY");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetFirstMonthArray()
		{
			// Week Number Of - First Fiscal Month
			// January-January
			aryFirstWeekOf[1, 1] = 5;
			aryFirstWeekOf[1, 2] = 4;
			aryFirstWeekOf[1, 3] = 4;
			aryFirstWeekOf[1, 4] = 4;
			aryFirstWeekOf[1, 5] = 4;
			aryFirstWeekOf[1, 6] = 4;
			aryFirstWeekOf[1, 7] = 4;
			aryFirstWeekOf[1, 8] = 4;
			aryFirstWeekOf[1, 9] = 4;
			aryFirstWeekOf[1, 10] = 4;
			aryFirstWeekOf[1, 11] = 4;
			aryFirstWeekOf[1, 12] = 4;
			// FEBRUARY
			aryFirstWeekOf[2, 1] = 1;
			aryFirstWeekOf[2, 2] = 3;
			aryFirstWeekOf[2, 3] = 1;
			aryFirstWeekOf[2, 4] = 1;
			aryFirstWeekOf[2, 5] = 1;
			aryFirstWeekOf[2, 6] = 1;
			aryFirstWeekOf[2, 7] = 1;
			aryFirstWeekOf[2, 8] = 1;
			aryFirstWeekOf[2, 9] = 1;
			aryFirstWeekOf[2, 10] = 1;
			aryFirstWeekOf[2, 11] = 1;
			aryFirstWeekOf[2, 12] = 1;
			// MARCH
			aryFirstWeekOf[3, 1] = 1;
			aryFirstWeekOf[3, 2] = 1;
			aryFirstWeekOf[3, 3] = 3;
			aryFirstWeekOf[3, 4] = 1;
			aryFirstWeekOf[3, 5] = 1;
			aryFirstWeekOf[3, 6] = 1;
			aryFirstWeekOf[3, 7] = 1;
			aryFirstWeekOf[3, 8] = 1;
			aryFirstWeekOf[3, 9] = 1;
			aryFirstWeekOf[3, 10] = 1;
			aryFirstWeekOf[3, 11] = 1;
			aryFirstWeekOf[3, 12] = 1;
			// APRIL
			aryFirstWeekOf[4, 1] = 2;
			aryFirstWeekOf[4, 2] = 2;
			aryFirstWeekOf[4, 3] = 2;
			aryFirstWeekOf[4, 4] = 6;
			aryFirstWeekOf[4, 5] = 2;
			aryFirstWeekOf[4, 6] = 2;
			aryFirstWeekOf[4, 7] = 2;
			aryFirstWeekOf[4, 8] = 2;
			aryFirstWeekOf[4, 9] = 2;
			aryFirstWeekOf[4, 10] = 2;
			aryFirstWeekOf[4, 11] = 2;
			aryFirstWeekOf[4, 12] = 2;
			// MAY
			aryFirstWeekOf[5, 1] = 1;
			aryFirstWeekOf[5, 2] = 1;
			aryFirstWeekOf[5, 3] = 1;
			aryFirstWeekOf[5, 4] = 1;
			aryFirstWeekOf[5, 5] = 3;
			aryFirstWeekOf[5, 6] = 1;
			aryFirstWeekOf[5, 7] = 1;
			aryFirstWeekOf[5, 8] = 1;
			aryFirstWeekOf[5, 9] = 1;
			aryFirstWeekOf[5, 10] = 1;
			aryFirstWeekOf[5, 11] = 1;
			aryFirstWeekOf[5, 12] = 1;
			// JUNE
			aryFirstWeekOf[6, 1] = 1;
			aryFirstWeekOf[6, 2] = 1;
			aryFirstWeekOf[6, 3] = 1;
			aryFirstWeekOf[6, 4] = 1;
			aryFirstWeekOf[6, 5] = 1;
			aryFirstWeekOf[6, 6] = 3;
			aryFirstWeekOf[6, 7] = 1;
			aryFirstWeekOf[6, 8] = 1;
			aryFirstWeekOf[6, 9] = 1;
			aryFirstWeekOf[6, 10] = 1;
			aryFirstWeekOf[6, 11] = 1;
			aryFirstWeekOf[6, 12] = 1;
			// JULY
			aryFirstWeekOf[7, 1] = 2;
			aryFirstWeekOf[7, 2] = 2;
			aryFirstWeekOf[7, 3] = 2;
			aryFirstWeekOf[7, 4] = 2;
			aryFirstWeekOf[7, 5] = 2;
			aryFirstWeekOf[7, 6] = 2;
			aryFirstWeekOf[7, 7] = 6;
			aryFirstWeekOf[7, 8] = 2;
			aryFirstWeekOf[7, 9] = 2;
			aryFirstWeekOf[7, 10] = 2;
			aryFirstWeekOf[7, 11] = 2;
			aryFirstWeekOf[7, 12] = 2;
			// AUGUST
			aryFirstWeekOf[8, 1] = 1;
			aryFirstWeekOf[8, 2] = 1;
			aryFirstWeekOf[8, 3] = 1;
			aryFirstWeekOf[8, 4] = 1;
			aryFirstWeekOf[8, 5] = 1;
			aryFirstWeekOf[8, 6] = 1;
			aryFirstWeekOf[8, 7] = 1;
			aryFirstWeekOf[8, 8] = 3;
			aryFirstWeekOf[8, 9] = 1;
			aryFirstWeekOf[8, 10] = 1;
			aryFirstWeekOf[8, 11] = 1;
			aryFirstWeekOf[8, 12] = 1;
			// SEPTEMBER
			aryFirstWeekOf[9, 1] = 1;
			aryFirstWeekOf[9, 2] = 1;
			aryFirstWeekOf[9, 3] = 1;
			aryFirstWeekOf[9, 4] = 1;
			aryFirstWeekOf[9, 5] = 1;
			aryFirstWeekOf[9, 6] = 1;
			aryFirstWeekOf[9, 7] = 1;
			aryFirstWeekOf[9, 8] = 1;
			aryFirstWeekOf[9, 9] = 3;
			aryFirstWeekOf[9, 10] = 1;
			aryFirstWeekOf[9, 11] = 1;
			aryFirstWeekOf[9, 12] = 1;
			// OCTOBER
			aryFirstWeekOf[10, 1] = 2;
			aryFirstWeekOf[10, 2] = 2;
			aryFirstWeekOf[10, 3] = 2;
			aryFirstWeekOf[10, 4] = 2;
			aryFirstWeekOf[10, 5] = 2;
			aryFirstWeekOf[10, 6] = 2;
			aryFirstWeekOf[10, 7] = 2;
			aryFirstWeekOf[10, 8] = 2;
			aryFirstWeekOf[10, 9] = 2;
			aryFirstWeekOf[10, 10] = 6;
			aryFirstWeekOf[10, 11] = 2;
			aryFirstWeekOf[10, 12] = 2;
			// NOVEMBER
			aryFirstWeekOf[11, 1] = 1;
			aryFirstWeekOf[11, 2] = 1;
			aryFirstWeekOf[11, 3] = 1;
			aryFirstWeekOf[11, 4] = 1;
			aryFirstWeekOf[11, 5] = 1;
			aryFirstWeekOf[11, 6] = 1;
			aryFirstWeekOf[11, 7] = 1;
			aryFirstWeekOf[11, 8] = 1;
			aryFirstWeekOf[11, 9] = 1;
			aryFirstWeekOf[11, 10] = 1;
			aryFirstWeekOf[11, 11] = 3;
			aryFirstWeekOf[11, 12] = 1;
			// DECEMBER
			aryFirstWeekOf[12, 1] = 1;
			aryFirstWeekOf[12, 2] = 1;
			aryFirstWeekOf[12, 3] = 1;
			aryFirstWeekOf[12, 4] = 1;
			aryFirstWeekOf[12, 5] = 1;
			aryFirstWeekOf[12, 6] = 1;
			aryFirstWeekOf[12, 7] = 1;
			aryFirstWeekOf[12, 8] = 1;
			aryFirstWeekOf[12, 9] = 1;
			aryFirstWeekOf[12, 10] = 1;
			aryFirstWeekOf[12, 11] = 1;
			aryFirstWeekOf[12, 12] = 3;
		}
		// vbPorter upgrade warning: intMonthID As int	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public object GetMonth(int intMonthID)
		{
			object GetMonth = null;
			switch (intMonthID)
			{
				case 1:
					{
						GetMonth = "January";
						break;
					}
				case 2:
					{
						GetMonth = "February";
						break;
					}
				case 3:
					{
						GetMonth = "March";
						break;
					}
				case 4:
					{
						GetMonth = "April";
						break;
					}
				case 5:
					{
						GetMonth = "May";
						break;
					}
				case 6:
					{
						GetMonth = "June";
						break;
					}
				case 7:
					{
						GetMonth = "July";
						break;
					}
				case 8:
					{
						GetMonth = "August";
						break;
					}
				case 9:
					{
						GetMonth = "September";
						break;
					}
				case 10:
					{
						GetMonth = "October";
						break;
					}
				case 11:
					{
						GetMonth = "November";
						break;
					}
				case 12:
					{
						GetMonth = "December";
						break;
					}
				default:
					{
						GetMonth = "January";
						break;
					}
			}
			//end switch
			return GetMonth;
		}
		// vbPorter upgrade warning: strMonth As string	OnWrite(string, VB.TextBox)
		// vbPorter upgrade warning: 'Return' As object	OnWrite
		public int GetNumberOfMonth(string strMonth)
		{
			int GetNumberOfMonth = 0;
			if (fecherFoundation.Strings.UCase(strMonth) == "JANUARY")
			{
				GetNumberOfMonth = 1;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "FEBRUARY")
			{
				GetNumberOfMonth = 2;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "MARCH")
			{
				GetNumberOfMonth = 3;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "APRIL")
			{
				GetNumberOfMonth = 4;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "MAY")
			{
				GetNumberOfMonth = 5;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "JUNE")
			{
				GetNumberOfMonth = 6;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "JULY")
			{
				GetNumberOfMonth = 7;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "AUGUST")
			{
				GetNumberOfMonth = 8;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "SEPTEMBER")
			{
				GetNumberOfMonth = 9;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "OCTOBER")
			{
				GetNumberOfMonth = 10;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "NOVEMBER")
			{
				GetNumberOfMonth = 11;
			}
			else if (fecherFoundation.Strings.UCase(strMonth) == "DECEMBER")
			{
				GetNumberOfMonth = 12;
			}
			else
			{
				GetNumberOfMonth = 1;
			}
			return GetNumberOfMonth;
		}

		private void ShowData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblPayrollProcessSetup");
				if (!rsData.EndOfFile())
				{
					if (FCConvert.ToInt32(rsData.Get_Fields("WeekNumber")) != 0)
					{
						for (intCounter = 0; intCounter <= cboWeekNumber.Items.Count - 1; intCounter++)
						{
							if (cboWeekNumber.Items[intCounter].ToString() == FCConvert.ToString(rsData.Get_Fields("WeekNumber")))
							{
								cboWeekNumber.SelectedIndex = intCounter;
							}
						}
					}

                    if (String.IsNullOrEmpty(rsData.Get_Fields_String("FirstWeekOf")))
                    {
                    }
                    else
                    {
                        cboFirstWeek.Text = FCConvert.ToString(rsData.Get_Fields_String("FirstWeekOf"));
                    }
					if (Information.IsDate(rsData.Get_Fields("PeriodStartDate")))
					{
						if (Convert.ToDateTime(rsData.Get_Fields("periodStartDate")).ToOADate() != 0)
						{
							t2kPeriodStartDate.Text = Strings.Format(rsData.Get_Fields("periodstartdate"), "MM/dd/yyyy");
						}
					}
					mebWeekEndDate.Text = Strings.Format(rsData.Get_Fields("WeekEndDate"), "MM/dd/yyyy");
					txtPayRun.Text = FCConvert.ToString(Conversion.Val(rsData.Get_Fields("PayRunID")));
					mebPayDate.Text = Strings.Format(rsData.Get_Fields("PayDate"), "MM/dd/yyyy");
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("NumberOfPayWeeks")) != 0)
					{
						for (intCounter = 0; intCounter <= cboNumberPayWeeks.Items.Count - 1; intCounter++)
						{
							if (cboNumberPayWeeks.Items[intCounter].ToString() == FCConvert.ToString(rsData.Get_Fields_Int32("NumberOfPayWeeks")))
							{
								cboNumberPayWeeks.SelectedIndex = intCounter;
							}
						}
					}
					if (fecherFoundation.Strings.Trim(mebPayDate.Text) != string.Empty)
					{
						lblMonth.Text = "Number of pay weeks in " + FCConvert.ToString(GetMonth(FCConvert.ToDateTime(mebPayDate.Text).Month));
					}
					lblWeekMonth.Text = FCConvert.ToString(GetMonth(FCConvert.ToDateTime(mebPayDate.Text).Month));
					chkBiWeekly.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PayBiWeeklys")) ? CheckState.Checked : CheckState.Unchecked);
					chkBiWeeklyDeds.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("BiWeeklyDeds")) ? CheckState.Checked : CheckState.Unchecked);
					// chkGroupID.Value = IIf(.Fields("GroupID"), 1, 0)
					// txtGroup.Visible = chkGroupID.Value
					// txtGroup = .Fields("GroupText")
				}
				else
				{
					// this indicates that a pay run has never been run.
					txtPayRun.Text = FCConvert.ToString(1);
				}
				cboManualCheck.Clear();
				cboManualCheck.AddItem("         NONE");
				cboManualCheck.ItemData(0, 0);
				rsData.OpenRecordset("Select * from tblEmployeeMaster where Status = 'Active' or Status = 'Hold 1 Week' order by EmployeeNumber");
				if (!rsData.EndOfFile())
				{
					for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length > 6)
						{
							cboManualCheck.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))) + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LastName"))));
						}
						else
						{
							cboManualCheck.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))) + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LastName"))));
						}
						cboManualCheck.ItemData(cboManualCheck.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
						rsData.MoveNext();
					}
				}
				modGlobalRoutines.ShowFirstFiscalMonth(ref txtFirstFiscalMonth);
				cboManualCheck.SelectedIndex = 0;
				clsHistoryClass.Init = this;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void Label10_Click()
		{
		}

		private void mebPayDate_Change(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(mebPayDate.Text) == string.Empty)
			{
			}
			else if (!Information.IsDate(mebPayDate.Text))
			{
			}
			else
			{
				this.lblMonth.Text = "Number of pay weeks in " + FCConvert.ToString(GetMonth(FCConvert.ToDateTime(mebPayDate.Text).Month));
				this.lblWeekMonth.Text = FCConvert.ToString(GetMonth(FCConvert.ToDateTime(mebPayDate.Text).Month));
			}
		}

		private void mebPayDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// intFirstMonthOfSetting = (aryFirstWeekOf(GetNumberOfMonth(lblWeekMonth.Text), GetNumberOfMonth(txtFirstFiscalMonth))) - 1
			// cboFirstWeek.ListIndex = intFirstMonthOfSetting
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                clsDRWrapper rsData = new clsDRWrapper();
                clsDRWrapper rsPrevRunPayrollSteps = new clsDRWrapper();
                clsDRWrapper rsPrevRunPayrollProcessingSetup = new clsDRWrapper();
                cPayRun pRun = new cPayRun();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                boolSaveOK = false;
                if (Information.IsDate(t2kPeriodStartDate.Text))
                {
                    pRun.PeriodStart = FCConvert.ToDateTime(t2kPeriodStartDate.Text);
                }

                if (!Information.IsDate(this.mebWeekEndDate.Text))
                {
                    MessageBox.Show("A valid Week Ending Date must be specified.", "TRIO Software",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.mebWeekEndDate.Focus();
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    return;
                }
                else
                {
                    pRun.PeriodEnd = FCConvert.ToDateTime(mebWeekEndDate.Text);
                }

                if (!Information.IsDate(mebPayDate.Text) || Conversion.Val(txtPayRun.Text) < 1)
                {
                    MessageBox.Show("Invalid pay date or pay run.", "TRIO Software", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    return;
                }
                else
                {
                    pRun.PayDate = FCConvert.ToDateTime(mebPayDate.Text);
                }

                if (string.Compare(this.cboWeekNumber.Text, this.cboNumberPayWeeks.Text) > 0)
                {
                    MessageBox.Show("Week number cannot be greater than number of pay weeks", "TRIO Software",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    return;
                }

                if (chkGroupID.CheckState == CheckState.Checked)
                {
                    if (!Information.IsNumeric(txtGroup.Text))
                    {
                        MessageBox.Show("Group ID must be numeric.", "TRIO Software", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        txtGroup.SelectionStart = 0;
                        txtGroup.SelectionLength = txtGroup.Text.Length;
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        return;
                    }
                }

                // NOW CHECK TO SEE IF THIS IS THE FIRST WEEK AND BE SURE THAT THEY
                // HAVE SELECTED THE FIRST WEEK OF OPTION
                rsPrevRunPayrollSteps.OpenRecordset("select * from tblpayrollsteps", "twpy0000.vb1");
                rsPrevRunPayrollSteps.OpenRecordset("select * from tblpayrollprocesssetup", "twpy0000.vb1");
                clsDRWrapper rsLengths = new clsDRWrapper();
                rsLengths.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
                if (!rsLengths.EndOfFile())
                {
                    if (FCConvert.ToBoolean(rsLengths.Get_Fields_Boolean("PayFreqWeekly")))
                    {
                        if (FCConvert.ToDateTime(mebPayDate.Text).Day <= 7)
                        {
                            // make sure that the first week is selected
                            if (Conversion.Val(cboWeekNumber.Text) != 1)
                            {
                                if (MessageBox.Show(
                                        "You have selected a date that is within the first 7 days of a month. " + "\r" +
                                        "\r" + "Do you want to continue with NOT selecting Week Number One?",
                                        "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                    DialogResult.Yes)
                                {
                                }
                                else
                                {
                                    cboWeekNumber.Focus();
                                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                    return;
                                }
                            }
                        }
                    }

                    if (FCConvert.ToBoolean(rsLengths.Get_Fields_Boolean("PayFreqBiWeekly")))
                    {
                        if (FCConvert.ToDateTime(mebPayDate.Text).Day <= 14)
                        {
                            // make sure that the first week is selected
                            if (FCConvert.ToDouble(cboWeekNumber.Text) != 1)
                            {
                                if (MessageBox.Show(
                                        "You have selected a date that is within the first 14 days of a Bi-weekly month. " +
                                        "\r" + "\r" + "Do you want to continue with NOT selecting Week Number One?",
                                        "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                    DialogResult.Yes)
                                {
                                }
                                else
                                {
                                    cboWeekNumber.Focus();
                                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                    return;
                                }
                            }
                        }
                    }
                }

                if (MessageBox.Show("This will prepare payroll files for the new pay calculation. Continue?",
                    "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // CHECK TO MAKE SURE THE USER WANTS TO PRINT A SINGLE MANUAL CHECK
                    if (cboManualCheck.SelectedIndex > 0)
                    {
                        if (MessageBox.Show("This will process a single employee. Continue?", "TRIO Software",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            // PROCESS A MANUAL CHECK
                            rsData.Execute(
                                "Update tblEmployeeMaster Set Status = 'Hold 1 Week' where Status = 'Active'",
                                "TWPY0000.vb1");
                            rsData.Execute(
                                "Update tblEmployeeMaster Set Status = 'Active' Where ID = " +
                                FCConvert.ToString(cboManualCheck.ItemData(cboManualCheck.SelectedIndex)),
                                "TWPY0000.vb1");
                        }
                        else
                        {
                            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                            return;
                        }
                    }
                    else
                    {
                        // THEN THIS ISN'T A MANUAL CHECK SEE IF THERE IS A GROUP
                        if (chkGroupID.CheckState == CheckState.Checked)
                        {
                            if (MessageBox.Show(
                                    "This will pay ALL employees with a Group ID of " + txtGroup.Text + ". Continue?",
                                    "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                DialogResult.Yes)
                            {
                                // PROCESS A THIS GROUP
                                rsData.Execute(
                                    "Update tblEmployeeMaster Set Status = 'Hold 1 Week' where Status = 'Active'",
                                    "TWPY0000.vb1");
                                rsData.Execute(
                                    "Update tblEmployeeMaster Set Status = 'Active' Where GroupID = '" + txtGroup.Text +
                                    "' and status = 'Hold 1 Week'", "TWPY0000.vb1");
                            }
                            else
                            {
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                return;
                            }
                        }
                    }

                    // CHECK TO SEE IF THERE HAS BEEN A PAY RUN ALREADY WITH THIS PAY DATE
                    rsData.OpenRecordset(
                        "Select max(PayRunID) as MaxPayRunID from tblCheckDetail where PayDate = '" + mebPayDate.Text +
                        "'", "TWPY0000.vb1");
                    if (rsData.EndOfFile())
                    {
                        txtPayRun.Text = FCConvert.ToString(1);
                    }
                    else
                    {
                        if (FCConvert.ToDouble(txtPayRun.Text) == Conversion.Val(rsData.Get_Fields("MaxPayRunID")) + 1)
                        {
                        }
                        else
                        {
                            if (Conversion.Val(rsData.Get_Fields("MaxPayRunID")) + 1 == 1)
                            {
                                txtPayRun.Text =
                                    FCConvert.ToString(Conversion.Val(rsData.Get_Fields("MaxPayRunID")) + 1);
                            }
                            else
                            {
                                if (MessageBox.Show(
                                        "There has already been a pay run numbered: " +
                                        rsData.Get_Fields("MaxPayRunID") + ". Continue with Pay Run Number: " +
                                        FCConvert.ToString(Conversion.Val(rsData.Get_Fields("MaxPayRunID")) + 1) + ".",
                                        "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                    DialogResult.Yes)
                                {
                                    txtPayRun.Text =
                                        FCConvert.ToString(Conversion.Val(rsData.Get_Fields("MaxPayRunID")) + 1);
                                }
                                else
                                {
                                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                    return;
                                }
                            }
                        }
                    }

                    int intWeekNumber = 0;
                    clsDRWrapper rsWeek = new clsDRWrapper();
                    intWeekNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cboWeekNumber.Text)));
                    // *****************************************************************************************
                    // MAKE SURE THAT THE WEEK NUMBER WAS THE SAME AS THE WEEK NUMBER FOR THE PAY RUN 1
                    // ON THIS PAY DATE.
                    if (FCConvert.ToDouble(txtPayRun.Text) != 1)
                    {
                        rsWeek.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + mebPayDate.Text +
                                             "' AND PayRunID =1");
                        if (rsWeek.EndOfFile())
                        {
                        }
                        else
                        {
                            if (FCConvert.ToInt32(rsWeek.Get_Fields("WeekNumber")) != intWeekNumber)
                            {
                                MessageBox.Show(
                                    "Week Number for Pay Run ID #1 was: " + rsWeek.Get_Fields("WeekNumber") + "." +
                                    "\r" + "All pay runs for this pay date must be this same week number.",
                                    "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                return;
                            }
                        }
                    }

                    // *****************************************************************************************
                    // MATTHEW 12/15/2004
                    // NEED TO MAKE SURE THERE WASN'T ANOTHER WEEK THAT WAS SETUP WITH THE SAME
                    // MONTH AND IT HAD A WEEK NUMBER SETUP AS WEEK NUMBER ONE WHERE THERE AS THERE WAS
                    // A WEEK NUMBER ONE ALREADY DONE.
                    // Call rsWeek.OpenRecordset("Select * from tblPayrollSteps where month(PayDate) = " & Month(mebPayDate.Text) & " AND WeekNumber = " & intWeekNumber)
                    rsWeek.OpenRecordset("Select * from tblPayrollSteps where month(PayDate) = " +
                                         FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Month) +
                                         " AND WeekNumber = " + FCConvert.ToString(intWeekNumber) +
                                         " and Year(paydate) = " +
                                         FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Year));
                    if (rsWeek.EndOfFile())
                    {
                    }
                    else
                    {
                        while (!rsWeek.EndOfFile())
                        {
                            if (FCConvert.ToDouble(txtPayRun.Text) == 1 && mebPayDate.Text !=
                                FCConvert.ToString(rsWeek.Get_Fields_DateTime("PayDate")))
                            {
                                rsData.OpenRecordset(
                                    "Select PayDate from tblCheckDetail where PayDate = '" +
                                    rsWeek.Get_Fields_DateTime("PayDate") + "'", "TWPY0000.vb1");
                                if (!rsData.EndOfFile())
                                {
                                    MessageBox.Show(
                                        "There has already been a week number: " + FCConvert.ToString(intWeekNumber) +
                                        " for the month of " +
                                        FCConvert.ToString(GetMonth(FCConvert.ToDateTime(mebPayDate.Text).Month)) +
                                        " on " + rsWeek.Get_Fields_DateTime("PayDate") + "\r" +
                                        "A different week number must be selected.", "TRIO Software",
                                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                    return;
                                }
                            }

                            rsWeek.MoveNext();
                        }
                    }

                    // *****************************************************************************************
                    // save info first so we can reverse this payroll if need be
                    if (!rsPrevRunPayrollSteps.EndOfFile())
                    {
                        rsPrevRunPayrollSteps.Edit();
                        rsPrevRunPayrollSteps.Set_Fields("numberofpayweeks",
                            rsPrevRunPayrollProcessingSetup.Get_Fields("numberofpayweeks"));
                        rsPrevRunPayrollSteps.Set_Fields("FirstWeekOf",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_String("FirstWeekOf"));
                        rsPrevRunPayrollSteps.Set_Fields("WeekEndDate",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_DateTime("WeekEndDate"));
                        if (Information.IsDate(rsPrevRunPayrollProcessingSetup.Get_Fields("periodstartdate")))
                        {
                            rsPrevRunPayrollSteps.Set_Fields("tblpayrollsteps.PeriodStartDate",
                                rsPrevRunPayrollProcessingSetup.Get_Fields("tblpayrollprocesssetup.periodstartdate"));
                        }

                        rsPrevRunPayrollSteps.Set_Fields("NumberOfPayWeeks",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_Int32("NumberOfPayWeeks"));
                        rsPrevRunPayrollSteps.Set_Fields("LastUpdated",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_DateTime("LastUpdated"));
                        rsPrevRunPayrollSteps.Set_Fields("UpdatedBy",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_String("UpdatedBy"));
                        rsPrevRunPayrollSteps.Set_Fields("MonthOf",
                            FCConvert.ToString(
                                Conversion.Val(rsPrevRunPayrollProcessingSetup.Get_Fields_Int16("MonthOf"))));
                        rsPrevRunPayrollSteps.Set_Fields("PayBiWeeklys",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_Boolean("PayBiWeeklys"));
                        rsPrevRunPayrollSteps.Set_Fields("ManualcheckEmpID",
                            rsPrevRunPayrollProcessingSetup.Get_Fields("ManualcheckEmpID"));
                        rsPrevRunPayrollSteps.Set_Fields("GroupID",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_Boolean("GroupID"));
                        rsPrevRunPayrollSteps.Set_Fields("GroupText",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_String("GroupText"));
                        rsPrevRunPayrollSteps.Set_Fields("TAChecksPrinted",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_String("TAChecksPrinted"));
                        rsPrevRunPayrollSteps.Set_Fields("BiWeeklyDeds",
                            rsPrevRunPayrollProcessingSetup.Get_Fields_Boolean("BiWeeklyDeds"));
                        rsPrevRunPayrollSteps.Set_Fields("UseGroupMultiFund",
                            FCConvert.ToInt32(rsPrevRunPayrollProcessingSetup.Get_Fields_Int16("UseGroupMultiFund")) ==
                            1);
                        rsPrevRunPayrollSteps.Update();
                    }

                    rsData.OpenRecordset("Select * from tblPayrollProcessSetup");
                    if (rsData.EndOfFile())
                    {
                        rsData.AddNew();
                    }
                    else
                    {
                        rsData.Edit();
                    }

                    rsData.Set_Fields("UseGroupMultiFund", modGlobalVariables.Statics.gintUseGroupMultiFund);
                    modGlobalFunctions.AddCYAEntry_6("PY", "GroupMultiFund",
                        FCConvert.ToString(modGlobalVariables.Statics.gintUseGroupMultiFund),
                        modGlobalVariables.Statics.gstrUser);
                    pRun.PayRunID =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(txtPayRun.Text))));
                    rsData.Set_Fields("PayRunID",
                        FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtPayRun.Text))));
                    modGlobalFunctions.AddCYAEntry_6("PY", "PayRunID",
                        FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtPayRun.Text))),
                        modGlobalVariables.Statics.gstrUser);
                    rsData.Set_Fields("ManualCheckEmpID", cboManualCheck.ItemData(cboManualCheck.SelectedIndex));
                    modGlobalFunctions.AddCYAEntry_6("PY", "ManualCheckEmpID",
                        FCConvert.ToString(cboManualCheck.ItemData(cboManualCheck.SelectedIndex)),
                        modGlobalVariables.Statics.gstrUser);
                    rsData.Set_Fields("WeekNumber", FCConvert.ToString(Conversion.Val(cboWeekNumber.Text)));
                    modGlobalFunctions.AddCYAEntry_6("PY", "WeekNumber", cboWeekNumber.Text,
                        modGlobalVariables.Statics.gstrUser);
                    if (Conversion.Val(cboWeekNumber.Text) != 1)
                    {
                        rsData.Set_Fields("FirstWeekOf", string.Empty);
                        modGlobalFunctions.AddCYAEntry_6("PY", "FirstWeekOf", "", modGlobalVariables.Statics.gstrUser);
                    }
                    else
                    {
                        rsData.Set_Fields("FirstWeekOf", cboFirstWeek.Text);
                        modGlobalFunctions.AddCYAEntry_6("PY", "FirstWeekOf", cboFirstWeek.Text,
                            modGlobalVariables.Statics.gstrUser);
                    }

                    if (t2kPeriodStartDate.Text == string.Empty)
                    {
                        rsData.Set_Fields("PeriodStartDate", null);
                    }
                    else if (Information.IsDate(t2kPeriodStartDate.Text))
                    {
                        rsData.Set_Fields("PeriodStartDate", t2kPeriodStartDate.Text);
                    }
                    else
                    {
                        rsData.Set_Fields("PeriodStartDate", null);
                    }

                    if (mebWeekEndDate.Text == string.Empty)
                    {
                        rsData.Set_Fields("WeekEndDate", null);
                        modGlobalFunctions.AddCYAEntry_6("PY", "WeekEndDate", "", modGlobalVariables.Statics.gstrUser);
                    }
                    else if (Information.IsDate(mebWeekEndDate.Text))
                    {
                        rsData.Set_Fields("WeekEndDate", mebWeekEndDate.Text);
                        modGlobalFunctions.AddCYAEntry_6("PY", "WeekEndDate",
                            FCConvert.ToString(rsData.Get_Fields_DateTime("WeekEndDate")),
                            modGlobalVariables.Statics.gstrUser);
                    }
                    else
                    {
                        rsData.Set_Fields("WeekEndDate", null);
                        modGlobalFunctions.AddCYAEntry_6("PY", "WeekEndDate", "", modGlobalVariables.Statics.gstrUser);
                    }

                    if (mebPayDate.Text == string.Empty)
                    {
                        MessageBox.Show("Invalid Pay Date", "TRIO Software", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        boolSaveOK = false;
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        return;
                    }
                    else if (Information.IsDate(mebPayDate.Text))
                    {
                        rsData.Set_Fields("PayDate", mebPayDate.Text);
                        modGlobalFunctions.AddCYAEntry_6("PY", "PayDate", mebPayDate.Text,
                            modGlobalVariables.Statics.gstrUser);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Pay Date", "TRIO Software", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        boolSaveOK = false;
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        return;
                    }

                    rsData.Set_Fields("NumberOfPayWeeks", FCConvert.ToString(Conversion.Val(cboNumberPayWeeks.Text)));
                    modGlobalFunctions.AddCYAEntry_6("PY", "NumberOfPayWeeks", cboNumberPayWeeks.Text,
                        modGlobalVariables.Statics.gstrUser);
                    rsData.Set_Fields("UpdatedBy", modGlobalRoutines.FixQuote(modGlobalVariables.Statics.gstrUser));
                    if (Information.IsDate(mebPayDate.Text))
                    {
                        rsData.Set_Fields("MonthOf", FCConvert.ToDateTime(mebPayDate.Text).Month);
                        modGlobalFunctions.AddCYAEntry_6("PY", "MonthOf",
                            FCConvert.ToString(rsData.Get_Fields_Int16("MonthOf")),
                            modGlobalVariables.Statics.gstrUser);
                    }
                    else
                    {
                        rsData.Set_Fields("MonthOf", 1);
                        modGlobalFunctions.AddCYAEntry_6("PY", "MonthOf", FCConvert.ToString(1),
                            modGlobalVariables.Statics.gstrUser);
                    }

                    rsData.Set_Fields("PayBiWeeklys", chkBiWeekly.CheckState);
                    modGlobalFunctions.AddCYAEntry_6("PY", "PayBiWeeklys", chkBiWeekly.CheckState.ToString(),
                        modGlobalVariables.Statics.gstrUser);
                    if (chkBiWeeklyDeds.Visible)
                    {
                        rsData.Set_Fields("BiWeeklyDeds", chkBiWeeklyDeds.CheckState);
                        modGlobalFunctions.AddCYAEntry_6("PY", "BiWeeklyDeds", chkBiWeeklyDeds.CheckState.ToString(),
                            modGlobalVariables.Statics.gstrUser);
                    }
                    else
                    {
                        // 04/12/2005
                        // .Fields("BiWeeklyDeds") = False
                        rsData.Set_Fields("BiWeeklyDeds", chkBiWeekly.CheckState);
                        modGlobalFunctions.AddCYAEntry_6("PY", "BiWeeklyDeds", chkBiWeeklyDeds.CheckState.ToString(),
                            modGlobalVariables.Statics.gstrUser);
                    }

                    rsData.Set_Fields("GroupID", chkGroupID.CheckState);
                    modGlobalFunctions.AddCYAEntry_6("PY", "GroupID", chkGroupID.CheckState.ToString(),
                        modGlobalVariables.Statics.gstrUser);
                    rsData.Set_Fields("GroupText", txtGroup.Text);
                    modGlobalFunctions.AddCYAEntry_6("PY", "GroupText", txtGroup.Text,
                        modGlobalVariables.Statics.gstrUser);
                    if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(cboManualCheck.Text)) == "NONE")
                    {
                        if (chkGroupID.CheckState == Wisej.Web.CheckState.Unchecked)
                        {
                            if (Conversion.Val(fecherFoundation.Strings.Trim(txtPayRun.Text)) == 1)
                            {
                                // this is the first pay run
                                rsData.Set_Fields("TAChecksPrinted", "FULL");
                                modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "FULL",
                                    modGlobalVariables.Statics.gstrUser);
                            }
                            else
                            {
                                // this is pay run 2 >
                                if (cmbNo.Text == "Month T&A checks have been run.")
                                {
                                    rsData.Set_Fields("TAChecksPrinted", "YES");
                                    modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "YES",
                                        modGlobalVariables.Statics.gstrUser);
                                }
                                else if (cmbNo.Text == "Month T&A checks have NOT been run.")
                                {
                                    // MATTHEW CALL ID 70703 5/17/2005
                                    // .Fields("TAChecksPrinted") = "NO"
                                    rsData.Set_Fields("TAChecksPrinted", "FULL");
                                    modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "NO",
                                        modGlobalVariables.Statics.gstrUser);
                                }
                                else
                                {
                                    rsData.Set_Fields("TAChecksPrinted", "FULL");
                                    modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "FULL",
                                        modGlobalVariables.Statics.gstrUser);
                                }
                            }
                        }
                        else
                        {
                            // this is a group run
                            if (cmbNo.Text == "Run T&A checks for group X only")
                            {
                                rsData.Set_Fields("TAChecksPrinted", "GROUP");
                                modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "GROUP",
                                    modGlobalVariables.Statics.gstrUser);
                            }
                            else if (cmbNo.Text == "Run T&A checks for ALL employees")
                            {
                                rsData.Set_Fields("TAChecksPrinted", "ALL");
                                modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "ALL",
                                    modGlobalVariables.Statics.gstrUser);
                            }
                            else if (cmbNo.Text == "Run T&A checks for weekly recipients only")
                            {
                                rsData.Set_Fields("TAChecksPrinted", "WEEKLY");
                                modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "WEEKLY",
                                    modGlobalVariables.Statics.gstrUser);
                            }
                        }
                    }
                    else
                    {
                        // this is manual check run
                        if (Conversion.Val(fecherFoundation.Strings.Trim(txtPayRun.Text)) == 1)
                        {
                            // this is a full pay run
                            rsData.Set_Fields("TAChecksPrinted", "FULL");
                            modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "FULL",
                                modGlobalVariables.Statics.gstrUser);
                        }
                        else
                        {
                            if (this.cboWeekNumber.Text == this.cboNumberPayWeeks.Text)
                            {
                                if (cmbNo.Text == "Month T&A checks have been run.")
                                {
                                    rsData.Set_Fields("TAChecksPrinted", "YES");
                                    modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "YES",
                                        modGlobalVariables.Statics.gstrUser);
                                }
                                else if (cmbNo.Text == "Month T&A checks have NOT been run.")
                                {
                                    // MATTHEW CALL ID 70703 5/17/2005
                                    // .Fields("TAChecksPrinted") = "NO"
                                    rsData.Set_Fields("TAChecksPrinted", "FULL");
                                    modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "NO",
                                        modGlobalVariables.Statics.gstrUser);
                                }
                            }
                            else
                            {
                                // WE WANT THIS SO THAT IT GIVES US THE FILTER OF ALL EMPLOYEES THAT
                                // HAVE A STATUS OF ACTIVE
                                rsData.Set_Fields("TAChecksPrinted", "YES");
                                modGlobalFunctions.AddCYAEntry_6("PY", "TAChecksPrinted", "YES",
                                    modGlobalVariables.Statics.gstrUser);
                            }
                        }
                    }

                    rsData.Update();
                    modGlobalVariables.Statics.gdatCurrentPayDate = FCConvert.ToDateTime(mebPayDate.Text);
                    if (Information.IsDate(mebWeekEndDate.Text))
                    {
                        modGlobalVariables.Statics.gdatCurrentWeekEndingDate =
                            FCConvert.ToDateTime(mebWeekEndDate.Text);
                    }
                    else
                    {
                        modGlobalVariables.Statics.gdatCurrentWeekEndingDate =
                            modGlobalVariables.Statics.gdatCurrentPayDate;
                    }

                    modGlobalVariables.Statics.gintCurrentPayRun = FCConvert.ToInt32(txtPayRun.Text);
                    modGlobalVariables.Statics.gintWeekNumber =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(cboWeekNumber.Text)));
                    // *****************************************************************************************
                    // CLEAR OUT THE VACATION SICK COLUMNS FOR THIS PERSON.
                    string strFirstWeekOf = "";
                    clsDRWrapper rsTemp = new clsDRWrapper();
                    rsTemp.OpenRecordset("Select * from tblPayrollProcessSetup", "Twpy0000.vb1");
                    if (!rsTemp.EndOfFile())
                    {
                        strFirstWeekOf =
                            fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("FirstWeekOf")));
                    }

                    if (FCConvert.ToDouble(txtPayRun.Text) == 1)
                    {
                        // MAKE SURE THAT THIS IS WEEK 1
                        if (fecherFoundation.Strings.UCase(strFirstWeekOf) == "FISCAL YEAR")
                        {
                            // CHANGE TO ONLY TWO STATEMENTS
                            rsTemp.Execute("Update tblVacationSick SET AccruedFiscal = 0,UsedFiscal = 0",
                                "TWPY0000.vb1");
                        }
                        else if (fecherFoundation.Strings.UCase(strFirstWeekOf) == "CALENDAR YEAR")
                        {
                            // CHANGE TO ONLY TWO STATEMENTS
                            rsTemp.Execute("Update tblVacationSick SET AccruedCalendar = 0,UsedCalendar = 0",
                                "TWPY0000.vb1");
                        }
                        else if (fecherFoundation.Strings.UCase(strFirstWeekOf) == "BOTH FISCAL AND CALENDAR")
                        {
                            // CHANGE TO ONLY TWO STATEMENTS
                            rsTemp.Execute(
                                "Update tblVacationSick SET AccruedCalendar = 0,UsedCalendar = 0,AccruedFiscal = 0,UsedFiscal = 0",
                                "TWPY0000.vb1");
                        }
                        else if (fecherFoundation.Strings.UCase(strFirstWeekOf) == "BOTH QUARTER AND FISCAL")
                        {
                            rsTemp.Execute("Update tblVacationSick SET AccruedFiscal = 0,UsedFiscal = 0",
                                "TWPY0000.vb1");
                        }
                        else
                        {
                        }
                    }

                    rsTemp.Execute("update tblemployeemaster set dataentrydone = 0", "twpy0000.vb1");
                    // ************************************************************************************************
                    // CLEAR OUT THE CURRENT COLUMNS FOR THE NEW CALCULATION
                    rsTemp.Execute("Update tblVacationSick SET AccruedCurrent = 0,UsedCurrent = 0", "TWPY0000.vb1");
                    // ************************************************************************************************
                    ClearVacSickBalances();
                    modGlobalRoutines.UpdatePayrollStepTable("FileSetup", pRun: pRun);
                    // check schedules
                    CheckSchedules();
                    boolSaveOK = true;
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    clsHistoryClass.Compare();
                    MessageBox.Show("Process completed successfully.", "TRIO Software", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    Close();
                }

                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                boolSaveOK = false;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " +
                                fecherFoundation.Information.Err(ex).Description);
            }
            finally
            {
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private bool CheckSchedules()
		{
			bool CheckSchedules = false;
            var originalPointer = FCGlobal.Screen.MousePointer;
            try
            {
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                this.ShowWait("Checking schedules");
				//this.UpdateWait("Checking schedules");
                clsScheduleWeek tWeek;
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strSQL;
                int lngLastSchedule;
                clsPayTotalsByCat tTot;
                lngLastSchedule = 0; 
                strSQL =
                    "Select employeenumber,scheduleid from tblemployeemaster where STATus = 'Active' and scheduleid > 0 order by scheduleid,employeenumber";
                rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                while (!rsLoad.EndOfFile())
                {
                    frmWait.InstancePtr.lblMessage.Text =
                        "Checking schedule for employee " + rsLoad.Get_Fields("employeenumber");
                    frmWait.InstancePtr.Refresh();
                    tWeek = new clsScheduleWeek();
                    tWeek.EmployeeNumber = FCConvert.ToString(rsLoad.Get_Fields("employeenumber"));
                    tWeek.LoadEmployeeWeek(modGlobalVariables.Statics.gdatCurrentWeekEndingDate);
                    tWeek.LoadSettings();
                    tWeek.InitPayTots();
                    tWeek.ReCalcWeek();
                    tTot = tWeek.GetPayTotals();
                    UpdateDistributionFromSchedule(tTot, rsLoad.Get_Fields("employeenumber"));
                    rsLoad.MoveNext();
                }

                //frmWait.InstancePtr.Unload();
                this.EndWait();
                return true;
            }
            catch (Exception ex)
            {
                this.EndWait();
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckSchedules", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				this.EndWait();
                FCGlobal.Screen.MousePointer = originalPointer;
            }
			return false;
		}

		private void UpdateDistributionFromSchedule(clsPayTotalsByCat tTot, string strEmployeeNumber)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				int lngCat = 0;
				string defaultWorkComp = "";
				string defaultMSRS = "";
				string defaultStatus = "";
				string defaultWC = "";
				double dblBaseRate = 0;
				string defaultDistU = "";
				int lngLastNum = 0;
				bool boolFound = false;
				string strAccount = "";
				string dAccount = "";
				clsPayCatTotal tBreak;
				clsPayCatTotal tCat;
				bool boolSoFar = false;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				int Y;
				if (!(tTot == null))
				{
					// must update the distribution screen
					clsDRWrapper rsPayCat = new clsDRWrapper();
					clsDRWrapper rsSave = new clsDRWrapper();
					rsPayCat.OpenRecordset("select * from tblpaycategories order by ID", "twpy0000.vb1");
					rsSave.Execute("update tblpayrolldistribution set hoursweek = 0 where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
					rsSave.OpenRecordset("Select * from tblpayrolldistribution where employeenumber = '" + strEmployeeNumber + "' order by recordnumber", "twpy0000.vb1");
					lngLastNum = 0;
					if (!rsSave.EndOfFile())
					{
						defaultWorkComp = FCConvert.ToString(rsSave.Get_Fields("workcomp"));
						defaultMSRS = FCConvert.ToString(rsSave.Get_Fields("msrs"));
						defaultStatus = FCConvert.ToString(rsSave.Get_Fields("statuscode"));
						defaultWC = FCConvert.ToString(rsSave.Get_Fields("wc"));
						dblBaseRate = Conversion.Val(rsSave.Get_Fields("baserate"));
						defaultDistU = FCConvert.ToString(rsSave.Get_Fields("distu"));
						rsSave.MoveLast();
						lngLastNum = FCConvert.ToInt32(rsSave.Get_Fields("recordnumber"));
					}
					for (lngRow = 1; lngRow <= tTot.MaxCat; lngRow++)
					{
						//App.DoEvents();
						lngCat = lngRow;
						if (!(tTot.Get_CategoryTotals(lngCat) == null))
						{
							if (Conversion.Val(tTot.Get_CategoryTotals(lngCat).AmountPaid) != 0)
							{
								tCat = tTot.Get_CategoryTotals(lngCat);
								boolFound = false;
								if (tCat.BreakDownCount < 1)
								{
									if (rsSave.FindFirstRecord("cat", lngCat))
									{
										rsSave.Edit();
										rsSave.Set_Fields("hoursweek", FCConvert.ToString(Conversion.Val(tTot.Get_CategoryTotals(lngCat).AmountPaid)));
										rsSave.Set_Fields("gross", Strings.Format(FCConvert.ToString(Conversion.Val(tTot.Get_CategoryTotals(lngCat).AmountPaid) * Conversion.Val(rsSave.Get_Fields("factor")) * Conversion.Val(rsSave.Get_Fields("baserate"))), "0.00"));
										rsSave.Update();
									}
									else
									{
										// must make new one
										rsSave.AddNew();
										rsSave.Set_Fields("employeenumber", strEmployeeNumber);
										rsSave.Set_Fields("cat", lngCat);
										lngLastNum += 1;
										rsSave.Set_Fields("recordnumber", lngLastNum);
										if (rsPayCat.FindFirstRecord("ID", lngCat))
										{
											rsSave.Set_Fields("factor", FCConvert.ToString(Conversion.Val(rsPayCat.Get_Fields("multi"))));
											rsSave.Set_Fields("taxcode", rsPayCat.Get_Fields("taxstatus"));
											rsSave.Set_Fields("wc", rsPayCat.Get_Fields_Boolean("workerscomp"));
											rsSave.Set_Fields("accountcode", 2);
											rsSave.Set_Fields("CD", 1);
											rsSave.Set_Fields("numberweeks", 0);
											rsSave.Set_Fields("defaulthours", 0);
											rsSave.Set_Fields("statuscode", "");
											rsSave.Set_Fields("grantfunded", 0);
											rsSave.Set_Fields("msrsid", -3);
											rsSave.Set_Fields("project", 0);
											rsSave.Set_Fields("weekstaxwithheld", 0);
											rsSave.Set_Fields("contractid", 0);
											rsSave.Set_Fields("workcomp", defaultWorkComp);
											rsSave.Set_Fields("msrs", defaultMSRS);
											rsSave.Set_Fields("statuscode", defaultStatus);
											rsSave.Set_Fields("wc", defaultWC);
											rsSave.Set_Fields("baserate", dblBaseRate);
											rsSave.Set_Fields("distu", defaultDistU);
											rsSave.Set_Fields("hoursweek", FCConvert.ToString(Conversion.Val(tTot.Get_CategoryTotals(lngCat).AmountPaid)));
											rsSave.Set_Fields("gross", Strings.Format(FCConvert.ToString(Conversion.Val(tTot.Get_CategoryTotals(lngCat).AmountPaid) * Conversion.Val(rsSave.Get_Fields("factor")) * Conversion.Val(rsSave.Get_Fields("baserate"))), "0.00"));
											if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsPayCat.Get_Fields("type"))) == "DOLLARS")
											{
												rsSave.Set_Fields("baserate", 1);
												rsSave.Set_Fields("gross", FCConvert.ToString(Conversion.Val(tTot.Get_CategoryTotals(lngCat).AmountPaid)));
											}
											else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsPayCat.Get_Fields("type"))) == "NON-PAY (HOURS)")
											{
												rsSave.Set_Fields("baserate", 0);
												rsSave.Set_Fields("gross", 0);
											}
											MessageBox.Show("Created new distribution line for employee " + strEmployeeNumber + " pay category " + rsPayCat.Get_Fields_String("description") + "\r\n" + "Check to make sure all entries for this line are correct", "New Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										}
										rsSave.Update();
									}
								}
								else
								{
									for (Y = 1; Y <= tCat.BreakDownCount; Y++)
									{
										//App.DoEvents();
										tBreak = tCat.Get_BreakdownByIndex(Y);
										if (!(tBreak == null))
										{
											strAccount = tBreak.Account;
											boolFound = false;
											if (rsSave.FindFirstRecord("cat", lngCat))
											{
												if (!(fecherFoundation.Strings.Trim(strAccount) == ""))
												{
													dAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("accountnumber")));
													if (dAccount.Length > 0)
													{
														boolSoFar = true;
														if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
														{
															for (x = 3; x <= (strAccount.Length); x++)
															{
																if (dAccount.Length >= x)
																{
																	if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
																	{
																		boolSoFar = false;
																	}
																}
																else
																{
																	boolSoFar = false;
																}
															}
															// x
															if (boolSoFar)
															{
																boolFound = true;
															}
															else
															{
																while (rsSave.FindNextRecord("cat", lngCat) && !boolFound)
																{
																	//App.DoEvents();
																	dAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("accountnumber")));
																	if (dAccount.Length > 0)
																	{
																		boolSoFar = true;
																		if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
																		{
																			for (x = 3; x <= (strAccount.Length); x++)
																			{
																				if (dAccount.Length >= x)
																				{
																					if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
																					{
																						boolSoFar = false;
																					}
																				}
																				else
																				{
																					boolSoFar = false;
																				}
																			}
																			// x
																			if (boolSoFar)
																			{
																				boolFound = true;
																				break;
																			}
																		}
																	}
																}
															}
														}
													}
												}
												else
												{
													boolFound = true;
												}
											}
											if (boolFound)
											{
												rsSave.Edit();
												rsSave.Set_Fields("hoursweek", FCConvert.ToString(Conversion.Val(tBreak.AmountPaid)));
												rsSave.Set_Fields("gross", Strings.Format(FCConvert.ToString(Conversion.Val(tBreak.AmountPaid) * Conversion.Val(rsSave.Get_Fields("factor")) * Conversion.Val(rsSave.Get_Fields("baserate"))), "0.00"));
												rsSave.Update();
											}
											if (!boolFound)
											{
												// must make new one
												rsSave.AddNew();
												rsSave.Set_Fields("employeenumber", strEmployeeNumber);
												rsSave.Set_Fields("cat", lngCat);
												lngLastNum += 1;
												rsSave.Set_Fields("recordnumber", lngLastNum);
												if (rsPayCat.FindFirstRecord("ID", lngCat))
												{
													rsSave.Set_Fields("factor", FCConvert.ToString(Conversion.Val(rsPayCat.Get_Fields("multi"))));
													rsSave.Set_Fields("taxcode", rsPayCat.Get_Fields("taxstatus"));
													rsSave.Set_Fields("wc", rsPayCat.Get_Fields_Boolean("workerscomp"));
													rsSave.Set_Fields("accountcode", 2);
													rsSave.Set_Fields("CD", 1);
													rsSave.Set_Fields("numberweeks", 0);
													rsSave.Set_Fields("defaulthours", 0);
													rsSave.Set_Fields("statuscode", "");
													rsSave.Set_Fields("grantfunded", 0);
													rsSave.Set_Fields("msrsid", -3);
													rsSave.Set_Fields("project", 0);
													rsSave.Set_Fields("weekstaxwithheld", 0);
													rsSave.Set_Fields("contractid", 0);
													rsSave.Set_Fields("workcomp", defaultWorkComp);
													rsSave.Set_Fields("msrs", defaultMSRS);
													rsSave.Set_Fields("statuscode", defaultStatus);
													rsSave.Set_Fields("wc", defaultWC);
													rsSave.Set_Fields("baserate", dblBaseRate);
													rsSave.Set_Fields("distu", defaultDistU);
													rsSave.Set_Fields("hoursweek", FCConvert.ToString(Conversion.Val(tBreak.AmountPaid)));
													rsSave.Set_Fields("gross", Strings.Format(FCConvert.ToString(Conversion.Val(tBreak.AmountPaid) * Conversion.Val(rsSave.Get_Fields("factor")) * Conversion.Val(rsSave.Get_Fields("baserate"))), "0.00"));
													if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsPayCat.Get_Fields("type"))) == "DOLLARS")
													{
														rsSave.Set_Fields("baserate", 1);
														rsSave.Set_Fields("gross", FCConvert.ToString(Conversion.Val(tBreak.AmountPaid)));
													}
													else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsPayCat.Get_Fields("type"))) == "NON-PAY (HOURS)")
													{
														rsSave.Set_Fields("baserate", 0);
														rsSave.Set_Fields("gross", 0);
													}
													MessageBox.Show("Created new distribution line for employee " + strEmployeeNumber + " pay category " + rsPayCat.Get_Fields_String("description") + "\r\n" + "Check to make sure all entries for this line are correct", "New Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
												}
											}
										}
									}
									// Y
								}
							}
						}
					}
					// lngRow
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateDistributionFromSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			// CHECK TO SEE IF THINGS WERE COMPLETED
			clsDRWrapper rsData = new clsDRWrapper();
			FCFileSystem fso = new FCFileSystem();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!Information.IsDate(mebPayDate.Text))
				{
					MessageBox.Show("Invalid Pay Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					mebPayDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateDiff("m", FCConvert.ToDateTime(mebPayDate.Text), DateTime.Today) < -1 || fecherFoundation.DateAndTime.DateDiff("m", FCConvert.ToDateTime(mebPayDate.Text), DateTime.Today) > 1)
				{
					MessageBox.Show("Pay date cannot be greater than one month.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					mebPayDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(mebPayDate.Text), DateTime.Today) < -7 || fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(mebPayDate.Text), DateTime.Today) > 7)
				{
					if (MessageBox.Show("Pay date is not within a week of the current date. Continue with current pay date?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("PY", "Continued with pay date not within 7 days of current date.", "PayDate: " + mebPayDate.Text);
					}
					else
					{
						mebPayDate.Focus();
						return;
					}
				}
				rsData.OpenRecordset("select top 1 paydate from tblpayrollsteps WHERE paydate <> '" + mebPayDate.Text + "'", "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string, DateTime)
					DateTime dtTemp;
					dtTemp = FCConvert.ToDateTime(FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Month) + "/1/" + FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Year));
					dtTemp = fecherFoundation.DateAndTime.DateAdd("m", 1, dtTemp);
					dtTemp = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp);
					rsData.OpenRecordset("select * from tblpayrollsteps where paydate between '" + FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Month) + "/1/" + FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Year) + "' and '" + FCConvert.ToString(dtTemp) + "' and paydate <> '" + mebPayDate.Text + "' order by WEEKNUMBER DESC", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						if (Conversion.Val(cboWeekNumber.Text) == Conversion.Val(rsData.Get_Fields("weeknumber")))
						{
							// it's okay if they didn't do anything yet
							if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("verifyaccept")))
							{
								// let's replace it
								rsData.Delete();
							}
							else
							{
								MessageBox.Show("The week number you have chosen is lower than or equal to one previously used for this month", "Invalid Week Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								boolSaveOK = false;
								return;
							}
						}
						else if (Conversion.Val(cboWeekNumber.Text) <= Conversion.Val(rsData.Get_Fields("weeknumber")))
						{
							MessageBox.Show("The week number you have chosen is lower than or equal to one previously used for this month", "Invalid Week Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolSaveOK = false;
							return;
						}
						else if (Conversion.Val(cboWeekNumber.Text) > Conversion.Val(rsData.Get_Fields("weeknumber")) + 1)
						{
							MessageBox.Show("The week number you have chosen is out of sequence.", "Invalid Week Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolSaveOK = false;
							return;
						}
					}
					else
					{
						if (Conversion.Val(cboWeekNumber.Text) > 1)
						{
							MessageBox.Show("The first pay run of the month cannot have a week number greater than 1", "Invalid Week Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolSaveOK = false;
							return;
						}
					}
					rsData.OpenRecordset("select * from tblpayrollsteps where weeknumber < " + FCConvert.ToString(Conversion.Val(cboWeekNumber.Text)) + " and paydate > '" + mebPayDate.Text + "' and paydate between '" + FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Month) + "/1/" + FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Year) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("d", -1, fecherFoundation.DateAndTime.DateAdd("m", 1, FCConvert.ToDateTime(FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Month) + "/1/" + FCConvert.ToString(FCConvert.ToDateTime(mebPayDate.Text).Year))))) + "' ", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						MessageBox.Show("There is already a paydate greater than this one for an earlier pay week", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						boolSaveOK = false;
						return;
					}
				}
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("FileSetup")))
					{
						// THIS IS THE FIRST TIME SO LET IT SAVE
					}
					else if (!FCConvert.ToBoolean(rsData.Get_Fields("DataEntry")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("EditTotals")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Calculation")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("VerifyAccept")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Checks")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("TrustAgency")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("CheckRegister")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PaySummary")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DeductionReports")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("BankList")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AccountingSummary")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DEForms")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("VacationReport")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("SickReport")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("OtherCodeTypes")) || !FCConvert.ToBoolean(rsData.Get_Fields("Warrant")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AuditReport")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AuditClear")) || !FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ResetTempData")))
					{
						goto ShowNotComplete;
					}
					else
					{
						// check to see if this the first week
						if (FCConvert.ToDouble(cboWeekNumber.Text) == 1)
						{
							// check to see if this a month/quarter/year
							if (cboFirstWeek.Text == "Regular Month")
							{
								if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("MTDProcess")))
								{
									goto ShowNotComplete;
								}
							}
							else if (cboFirstWeek.Text == "Quarter" || cboFirstWeek.Text == "Both Quarter And FISCAL")
							{
								if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("QTDProcess")))
								{
									goto ShowNotComplete;
								}
							}
							else if (cboFirstWeek.Text == "CALENDAR Year" || cboFirstWeek.Text == "Both FISCAL And CALENDAR")
							{
								if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("YTDCalendarProcess")))
								{
									goto ShowNotComplete;
								}
							}
						}
					}
				}
				FinishSave:
				;
				if (chkGroupID.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsData.OpenRecordset("Select * from tblPayrollAccounts where Code = 'CC'");
					if (!rsData.EndOfFile())
					{
						if (txtFundNumber.Text == Strings.Mid(FCConvert.ToString(rsData.Get_Fields("Account")), 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2))))
						{
							modGlobalVariables.Statics.gintUseGroupMultiFund = 0;
						}
						else
						{
							modGlobalVariables.Statics.gintUseGroupMultiFund = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFundNumber.Text)));
						}
					}
					else
					{
						modGlobalVariables.Statics.gintUseGroupMultiFund = 0;
					}
				}
				else
				{
					modGlobalVariables.Statics.gintUseGroupMultiFund = 0;
				}
				mnuSave_Click();
				if (!boolSaveOK)
					return;
				mnuExit_Click();
				return;
				ShowNotComplete:
				;
				// If MsgBox("Verify / Accept has been run but not all reports have been completed for the current pay date. " & Chr(13) & Chr(13) & "It is recommended that you finish running reports before the next pay run." & Chr(13) & Chr(13) & "Continue with File Setup?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
				if (MessageBox.Show("This process will end the prior pay run and start a new one. " + "\r" + "\r" + "It is recommended that you verify that all reports and checks have been printed before you continue." + "\r" + "\r" + "Continue with File Setup?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					goto FinishSave;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MnuSaveExit_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearVacSickBalances()
		{
			// MATTHEW 1/4/05
			// WE ARE CURRENTLY NOT DEALING WITH THE FACT THAT AT THE START OF A NEW YEAR YOU WILL LOOSE ANYTHING OVER A CERTAIN AMOUNT
			string strTemp;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsOtherData = new clsDRWrapper();
			int intMonth = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int intCleared = 0;
			rsData.OpenRecordset("Select * from tblCodes");
			while (!rsData.EndOfFile())
			{
				//App.DoEvents();
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Cleared"))) == "Never" || rsData.Get_Fields_String("Cleared") == "6")
				{
				}
				else
				{
					intCleared = 0;
					if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Cleared")))) == "anniversary")
					{
						intCleared = 1;
					}
					else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Cleared")))) == "anniversary date")
					{
						intCleared = 5;
					}
					else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Cleared")))) == "1st wk fiscal yr")
					{
						intCleared = 3;
					}
					if (intCleared == 0)
					{
						intCleared = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("cleared"))))));
					}
					if (intCleared == 1)
					{
						// #1;Anniversary
						// Call rsOtherData.OpenRecordset("SELECT tblEmployeeMaster.DateAnniversary, tblCodes.*, tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID")
						rsOtherData.OpenRecordset("SELECT tblEmployeeMaster.DateAnniversary, tblvacationsick.id, tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON (tblVacationSick.CodeID = tblCodes.ID) where tblcodes.cleared = '1'");
						while (!rsOtherData.EndOfFile())
						{
							if (Information.IsDate(rsOtherData.Get_Fields_String("DateAnniversary")))
							{
								if (Conversion.Val(Strings.Left(FCConvert.ToString(rsOtherData.Get_Fields_String("DateAnniversary")), 2)) == FCConvert.ToDateTime(mebPayDate.Text).Month && FCConvert.ToDouble(cboWeekNumber.Text) == 1 && FCConvert.ToDouble(txtPayRun.Text) == 1)
								{
									// clear balance
									rsTemp.Execute("update tblvacationsick set usedbalance = 0 where id = " + rsOtherData.Get_Fields("id"), "Payroll");
								}
							}
							rsOtherData.MoveNext();
						}
					}
					else if (intCleared == 5)
					{
						// #5 Anniversary date
						rsOtherData.OpenRecordset("SELECT tblEmployeeMaster.DateAnniversary, tblvacationsick.id , tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON (tblVacationSick.CodeID = tblCodes.ID) where tblcodes.cleared = '5'");
						while (!rsOtherData.EndOfFile())
						{
							//App.DoEvents();
							if (Information.IsDate(rsOtherData.Get_Fields_String("DateAnniversary")))
							{
								if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(rsOtherData.Get_Fields_String("DateAnniversary") + "/" + FCConvert.ToString(DateTime.Today.Year)), FCConvert.ToDateTime(mebPayDate.Text)) >= 0 && Conversion.Val(txtPayRun.Text) == 1)
								{
									rsTemp.OpenRecordset("select * from tblpayrollsteps where paydate <> '" + mebPayDate.Text + "' order by paydate desc", "twpy0000.vb1");
									if (!rsTemp.EndOfFile())
									{
										rsTemp.MoveFirst();
										if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(rsOtherData.Get_Fields_String("DateAnniversary") + "/" + FCConvert.ToString(DateTime.Today.Year)), (DateTime)rsTemp.Get_Fields("paydate")) < 0)
										{
											rsTemp.Execute("update tblvacationsick set usedbalance = 0 where id = " + rsOtherData.Get_Fields("id"), "Payroll");
										}
									}
								}
							}
							rsOtherData.MoveNext();
						}
					}
					else if (intCleared == 2)
					{
						// #2;1st Wk Cal Yr
						if (FCConvert.ToDateTime(mebPayDate.Text).Month == 1 && FCConvert.ToDouble(cboWeekNumber.Text) == 1 && FCConvert.ToDouble(txtPayRun.Text) == 1)
						{
							// clear the balance
							// Call rsOtherData.OpenRecordset("SELECT tblEmployeeMaster.DateAnniversary, tblCodes.*, tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID WHERE tblVacationSick.TypeID=" & rsData.Fields("CodeType") & " AND tblVacationSick.CodeID=" & rsData.Fields("ID"))
							rsOtherData.OpenRecordset("SELECT tblvacationsick.id,  tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID WHERE tblVacationSick.TypeID=" + rsData.Get_Fields_Int32("CodeType") + " AND tblVacationSick.CodeID=" + rsData.Get_Fields("ID") + " AND tblcodes.cleared = '2'");
							while (!rsOtherData.EndOfFile())
							{
								// clear balance
								//App.DoEvents();
								rsTemp.Execute("update tblvacationsick set usedbalance = 0 where id = " + rsOtherData.Get_Fields("id"), "Payroll");
								rsOtherData.MoveNext();
							}
						}
					}
					else if (intCleared == 3)
					{
						// #3;1st Wk Fiscal Yr
						if (modGlobalVariables.Statics.gboolBudgetary)
						{
							rsOtherData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
							if (rsOtherData.EndOfFile())
							{
								intMonth = 1;
							}
							else
							{
								intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOtherData.Get_Fields_String("FiscalStart"))));
							}
						}
						else
						{
							rsOtherData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
							if (rsOtherData.EndOfFile())
							{
								intMonth = 1;
							}
							else
							{
								intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOtherData.Get_Fields_Int16("FirstFiscalMonth"))));
							}
						}
						if (FCConvert.ToDateTime(mebPayDate.Text).Month == intMonth && FCConvert.ToDouble(cboWeekNumber.Text) == 1 && FCConvert.ToDouble(txtPayRun.Text) == 1)
						{
							// clear the balance
							// Call rsOtherData.Execute("UPDATE (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID SET tblVacationSick.UsedBalance = 0 WHERE tblcodes.cleared = '3' and (((tblVacationSick.TypeID)=" & rsData.Fields("CodeType") & ") AND ((tblVacationSick.CodeID)=" & rsData.Fields("ID") & "))", "Payroll")
							rsOtherData.OpenRecordset("SELECT tblEmployeeMaster.DateAnniversary, tblvacationsick.id, tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID WHERE tblVacationSick.TypeID=" + rsData.Get_Fields_Int32("CodeType") + " AND tblVacationSick.CodeID=" + rsData.Get_Fields("ID") + " AND tblcodes.cleared = '3'");
							while (!rsOtherData.EndOfFile())
							{
								rsTemp.Execute("update tblvacationsick set usedbalance = 0 where id = " + rsOtherData.Get_Fields("id"), "Payroll");
								rsOtherData.MoveNext();
							}
						}
					}
					else if (intCleared == 4)
					{
						// #4;January 1
						if (FCConvert.ToDateTime(mebPayDate.Text).Month == 1 && FCConvert.ToDateTime(mebPayDate.Text).Day == 1 && FCConvert.ToDouble(txtPayRun.Text) == 1)
						{
							rsOtherData.OpenRecordset("SELECT tblEmployeeMaster.DateAnniversary, tblCodes.*, tblVacationSick.UsedBalance, tblEmployeeMaster.EmployeeNumber FROM (tblEmployeeMaster INNER JOIN tblVacationSick ON tblEmployeeMaster.EmployeeNumber = tblVacationSick.EmployeeNumber) INNER JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID WHERE tblVacationSick.TypeID=" + rsData.Get_Fields_Int32("CodeType") + " AND tblVacationSick.CodeID=" + rsData.Get_Fields("ID") + " AND tblcodes.cleared = '4'");
							while (!rsOtherData.EndOfFile())
							{
								//App.DoEvents();
								rsOtherData.Edit();
								rsOtherData.Set_Fields("UsedBalance", 0);
								rsOtherData.Update();
								rsOtherData.MoveNext();
							}
						}
					}
				}
				rsData.MoveNext();
			}
		}

		private void txtGroup_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (chkGroupID.CheckState == Wisej.Web.CheckState.Checked && Conversion.Val(txtGroup.Text) > 0)
			{
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("select * from tblpayrollprocesssetup where groupid = 1 and grouptext = '" + FCConvert.ToString(Conversion.Val(txtGroup.Text)) + "'", "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					txtFundNumber.Text = FCConvert.ToString(Conversion.Val(rsData.Get_Fields("usegroupmultifund")));
				}
				else
				{
					rsData.OpenRecordset("select top 1 * from tblpayrollsteps where groupid = 1 and grouptext = '" + FCConvert.ToString(Conversion.Val(txtGroup.Text)) + "' order by id desc", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						txtFundNumber.Text = FCConvert.ToString(Conversion.Val(rsData.Get_Fields("usegroupmultifund")));
					}
				}
			}
		}
	}
}
