//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Narrow2Up.
	/// </summary>
	public partial class rptW2Narrow2Up : FCSectionReport
	{
		public rptW2Narrow2Up()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W2 Narrow 2 Up";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2Narrow2Up InstancePtr
		{
			get
			{
				return (rptW2Narrow2Up)Sys.GetInstance(typeof(rptW2Narrow2Up));
			}
		}

		protected rptW2Narrow2Up _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2Narrow2Up	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsW2Master = new clsDRWrapper();
		clsDRWrapper rsW2Deductions = new clsDRWrapper();
		clsDRWrapper rsW2Matches = new clsDRWrapper();
		double dblLaserLineAdjustment;
		object ControlName;
		clsDRWrapper rsbox12 = new clsDRWrapper();
		clsDRWrapper rsBox14 = new clsDRWrapper();
		clsDRWrapper rsBox10 = new clsDRWrapper();
		private bool boolPrintTest;
		private double dblHorizAdjust;

		private struct BoxEntries
		{
			public string Code;
			public double Amount;
			public bool ThirdParty;
		};

		private BoxEntries[] Box1214 = null;
		private BoxEntries[] aryBox10 = null;
		private BoxEntries[] aryBox14 = null;
		private int lngYearToUse;

		public void Init(bool boolTestPrint = false, double dblAdjustment = 0, double dblHorizAdjustment = 0)
		{
			boolPrintTest = boolTestPrint;
			dblLaserLineAdjustment = dblAdjustment;
			dblHorizAdjust = dblHorizAdjustment;
			//if (this.Document.Printer.PrintDialog)
			//{
				//modPrinterFunctions.PrintXsForAlignment("The X should be at the top of the form", 10, 1, this.Document.Printer.PrinterName);
				frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, 1, boolAllowEmail: false, showModal:true);
				if (!boolPrintTest)
				{
					rptW3Laser.InstancePtr.Init();
					if (lngYearToUse > 0)
					{
						frmW3ME.InstancePtr.Init(lngYearToUse, false);
					}
				}
			//}
			//else
			//{
			//	this.Close();
			//}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: txtEmployersName As object	OnWrite(string)
			// vbPorter upgrade warning: txtOther As object	OnWrite(string)
			// vbPorter upgrade warning: txtOther2 As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12a As object	OnWrite(string)
			// vbPorter upgrade warning: txt12aAmount As object	OnWrite(string)
			// vbPorter upgrade warning: txt12b As object	OnWrite(string)
			// vbPorter upgrade warning: txt12bAmount As object	OnWrite(string)
			// vbPorter upgrade warning: txt12c As object	OnWrite(string)
			// vbPorter upgrade warning: txt12cAmount As object	OnWrite(string)
			// vbPorter upgrade warning: txt12d As object	OnWrite(string)
			// vbPorter upgrade warning: txt12dAmount As object	OnWrite(string)
			// vbPorter upgrade warning: txt12a2 As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12a2Amount As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12b2 As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12b2Amount As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12c2 As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12c2Amount As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12d2 As object	OnWrite(string, object)
			// vbPorter upgrade warning: txt12d2Amount As object	OnWrite(string, object)
			// vbPorter upgrade warning: boolFound As object	OnWrite(bool)
			// vbPorter upgrade warning: boolThirdParty As object	OnWrite(bool)
			if (!boolPrintTest)
			{
				double dblTemp = 0;
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					if (rsW2Master.EndOfFile())
					{
					}
					else
					{
						rsW2Deductions.OpenRecordset("Select * from tblW2Deductions where Code <> '' AND EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
						// Call rsW2Matches.OpenRecordset("SELECT * From tblW2Matches WHERE tblW2Matches.DeductionNumber Not In (Select DeductionNumber from tblW2Deductions where Code <> '' AND EmployeeNumber ='" & rsData.Fields("EmployeeNumber") & "') And EmployeeNumber = '" & rsData.Fields("EmployeeNumber") & "'", "TWPY0000.vb1")
						// MATTHEW 12/29/2004
						// Call rsW2Matches.OpenRecordset("SELECT * From tblW2Matches where Code <> '' AND EmployeeNumber = '" & rsData.Fields("EmployeeNumber") & "'", "TWPY0000.vb1")
						txtFederalEIN.Text = rsW2Master.Get_Fields_String("EmployersFederalID");
						txtFederalEIN2.Text = rsW2Master.Get_Fields_String("EmployersFederalID");
						txtEmployersName.Text = rsW2Master.Get_Fields_String("EmployersName") + "\r";
						txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address1") + "\r";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("Address2"))) != string.Empty)
						{
							txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address2") + "\r";
						}
						txtEmployersName.Text = txtEmployersName.Text + fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("City"))) + ", " + rsW2Master.Get_Fields("State") + " " + rsW2Master.Get_Fields("Zip");
						txtEmployersName2.Text = txtEmployersName.Text;
						txtState.Text = rsW2Master.Get_Fields_String("StateCode");
						txtState2.Text = rsW2Master.Get_Fields_String("StateCode");
						txtEmployersStateEIN.Text = rsW2Master.Get_Fields_String("EmployersStateID");
						txtEmployersStateEIN2.Text = rsW2Master.Get_Fields_String("EmployersStateID");
					}
					txtSSN.Text = rsData.Get_Fields_String("SSN");
					txtSSN2.Text = rsData.Get_Fields_String("SSN");
					txtPen.Visible = rsData.Get_Fields_Boolean("W2Retirement");
					txtPen2.Visible = rsData.Get_Fields_Boolean("W2Retirement");
					txtStatutoryEmployee.Visible = rsData.Get_Fields_Boolean("W2StatutoryEmployee");
					txtStatutoryEmployee2.Visible = rsData.Get_Fields_Boolean("W2StatutoryEmployee");
					// txtEmployeesName.Text = rsData.Fields("EmployeeName") & Chr(13) & Chr(13)
					txtEmployeesName.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsData.Get_Fields("firstname") + " " + Strings.Left(rsData.Get_Fields("middlename") + " ", 1)) + " " + fecherFoundation.Strings.Trim(rsData.Get_Fields("lastname") + " " + rsData.Get_Fields("desig"))) + "\r" + "\r";
					txtEmployeesName.Text = txtEmployeesName.Text + rsData.Get_Fields_String("Address") + "\r";
					txtEmployeesName.Text = txtEmployeesName.Text + rsData.Get_Fields_String("CityStateZip");
					// txtEmployeesName2 = txtEmployeesName
					txtEmployeesName2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("address"))) + "\r\n" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CityStateZip")));
					txtFirstAndMiddle.Text = rsData.Get_Fields_String("FirstName") + " ";
					txtFirstAndMiddle.Text = fecherFoundation.Strings.Trim(txtFirstAndMiddle.Text + Strings.Left(rsData.Get_Fields("middlename") + " ", 1));
					txtLastName.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields("lastname") + " " + rsData.Get_Fields("desig"));
					// SET THE WAGES AND TAXES
					txtTotalWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FederalWage"));
					txtTotalWages2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FederalWage"));
					txtFICAWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FICAWage"));
					txtFICAWages2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FICAWage"));
					txtMedWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("MedicareWage"));
					txtMedWages2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("MedicareWage"));
					txtFederalTax.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FederalTax"));
					txtFederalTax2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FederalTax"));
					txtFICATax.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FICATax"));
					txtFICATax2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FICATax"));
					txtMedTaxes.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("MedicareTax"));
					txtMedTaxes2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("MedicareTax"));
					// PER RON AND KPORT 12/08/2004
					txtStateWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("StateWage"));
					txtStateWages2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("StateWage"));
					txtStateTax.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("StateTax"));
					txtStateTax2.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("StateTax"));
					// SET THE BOX 12 AND BOX 14 INFORMATION
					txtOther.Text = string.Empty;
					txtOther2.Text = string.Empty;
					txt12a.Text = string.Empty;
					txt12aAmount.Text = string.Empty;
					txt12b.Text = string.Empty;
					txt12bAmount.Text = string.Empty;
					txt12c.Text = string.Empty;
					txt12cAmount.Text = string.Empty;
					txt12d.Text = string.Empty;
					txt12dAmount.Text = string.Empty;
					txt12a2.Text = string.Empty;
					txt12a2Amount.Text = string.Empty;
					txt12b2.Text = string.Empty;
					txt12b2Amount.Text = string.Empty;
					txt12c2.Text = string.Empty;
					txt12c2Amount.Text = string.Empty;
					txt12d2.Text = string.Empty;
					txt12d2Amount.Text = string.Empty;
					rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box12 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					rsW2Deductions.MoveLast();
					rsW2Deductions.MoveFirst();
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1 + 1];
					for (int intArrayCounter = 0; intArrayCounter <= rsW2Deductions.RecordCount() - 1; intArrayCounter++)
					{
						Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
						Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
						Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
						rsW2Deductions.MoveNext();
					}
					rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  TBLW2MATCHES.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' and tblw2box12and14.Box12 = 1 GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code ");
					rsW2Matches.MoveLast();
					rsW2Matches.MoveFirst();
					// ReDim Preserve Box1214(UBound(Box1214) + rsW2Matches.RecordCount)
					bool boolFound;
					while (!rsW2Matches.EndOfFile())
					{
						boolFound = false;
						for (int intloop = 0; intloop <= Information.UBound(Box1214, 1); intloop++)
						{
							if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("code")))
							{
								boolFound = true;
								Box1214[intloop].Amount += rsW2Matches.Get_Fields("sumofcytdamount");
								break;
							}
						}
						// intloop
						if (!FCConvert.ToBoolean(boolFound))
						{
							Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + 1 + 1);
							Box1214[Information.UBound(Box1214)].Amount = rsW2Matches.Get_Fields("sumofcytdamount");
							Box1214[Information.UBound(Box1214)].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
							Box1214[Information.UBound(Box1214)].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("thirdpartysave"));
						}
						rsW2Matches.MoveNext();
					}
					// now show box 12 info
					intCounter = 1;
					bool boolThirdParty = false;
					for (int intArrayCounter = 0; intArrayCounter <= Information.UBound(Box1214, 1); intArrayCounter++)
					{
						if (!FCConvert.ToBoolean(boolThirdParty))
						{
							if (Box1214[intArrayCounter].ThirdParty)
							{
								boolThirdParty = true;
							}
						}
					}
					for (int intArrayCounter = 0; intArrayCounter <= Information.UBound(Box1214, 1); intArrayCounter++)
					{
						if (intCounter > 4)
							break;
						if (Box1214[intArrayCounter].Amount == 0)
							goto SkipEntry;
						if (Box1214[intArrayCounter].ThirdParty)
						{
							modGlobalVariables.Statics.W3Information.dblThirdPartySick = FCConvert.ToDouble(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
						}
						if (intCounter == 1)
						{
							txt12a.Text = Box1214[intArrayCounter].Code;
							txt12aAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 2)
						{
							txt12b.Text = Box1214[intArrayCounter].Code;
							txt12bAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 3)
						{
							txt12c.Text = Box1214[intArrayCounter].Code;
							txt12cAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 4)
						{
							txt12d.Text = Box1214[intArrayCounter].Code;
							txt12dAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						SkipEntry:
						;
					}
					// now fill box 14
					Box1214 = new BoxEntries[0 + 1];
					rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box14 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					rsW2Deductions.MoveLast();
					rsW2Deductions.MoveFirst();
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1 + 1];
					for (int intArrayCounter = 0; intArrayCounter <= rsW2Deductions.RecordCount() - 1; intArrayCounter++)
					{
						Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
						Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
						Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
						rsW2Deductions.MoveNext();
					}
					rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave,tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box14 = 1 and tblw2matches.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave,tblW2Matches.Code ");
					rsW2Matches.MoveLast();
					rsW2Matches.MoveFirst();
					Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + rsW2Matches.RecordCount() + 1);
					while (!rsW2Matches.EndOfFile())
					{
						boolFound = false;
						for (int intloop = 0; intloop <= Information.UBound(Box1214, 1); intloop++)
						{
							if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("Code")))
							{
								Box1214[intloop].Amount += rsW2Matches.Get_Fields("SumOfCYTDAmount");
								boolFound = true;
								break;
							}
							else if (Box1214[intloop].Code == "")
							{
								Box1214[intloop].Amount = rsW2Matches.Get_Fields("sumofcytdamount");
								Box1214[intloop].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
								Box1214[intloop].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("ThirdPartySave"));
								boolFound = true;
								break;
							}
						}
						// intloop
						rsW2Matches.MoveNext();
					}
					intCounter = 1;
					boolThirdParty = false;
					for (int intArrayCounter = 0; intArrayCounter <= Information.UBound(Box1214, 1); intArrayCounter++)
					{
						if (!FCConvert.ToBoolean(boolThirdParty))
						{
							if (Box1214[intArrayCounter].ThirdParty)
							{
								boolThirdParty = true;
							}
						}
					}
					for (int intArrayCounter = 0; intArrayCounter <= Information.UBound(Box1214, 1); intArrayCounter++)
					{
						if (intCounter > 4)
							break;
						if (Box1214[intArrayCounter].Amount == 0)
							goto SkipMatchEntry;
						if (Box1214[intArrayCounter].ThirdParty)
						{
							modGlobalVariables.Statics.W3Information.dblThirdPartySick = FCConvert.ToDouble(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
						}
						if (intCounter == 1)
						{
							txtOther.Text = Box1214[intArrayCounter].Code + " ";
							txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else
						{
							txtOther.Text = txtOther.Text + "\r" + Box1214[intArrayCounter].Code + " ";
							txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						SkipMatchEntry:
						;
					}
					txt12a2.Text = txt12a.Text;
					txt12a2Amount.Text = txt12aAmount.Text;
					txt12b2.Text = txt12b.Text;
					txt12b2Amount.Text = txt12bAmount.Text;
					txt12c2.Text = txt12c.Text;
					txt12c2Amount.Text = txt12cAmount.Text;
					txt12d2.Text = txt12d.Text;
					txt12d2Amount.Text = txt12dAmount.Text;
					txtOther2.Text = txtOther.Text;
					// NOW FILL BOX 10
					dblTemp = 0;
					rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1  AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					if (!rsBox10.EndOfFile())
					{
						dblTemp = Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
					}
					rsBox10.OpenRecordset("SELECT Sum(tblW2matches.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2matches ON tblW2Box12And14.DeductionNumber = tblW2matches.DeductionNumber Where tblW2Box12And14.Box10 = 1  AND tblW2matches.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					if (!rsBox10.EndOfFile())
					{
						dblTemp += Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
					}
					txtDependentCare.Text = Strings.Format(dblTemp, "0.00");
					txtDependentCare2.Text = Strings.Format(dblTemp, "0.00");
					rsData.MoveNext();
					eArgs.EOF = false;
				}
			}
			else
			{
				txt12a.Text = "X";
				txt12a2.Text = "X";
				txt12aAmount.Text = "999.99";
				txt12a2Amount.Text = "999.99";
				txt12b.Text = "X";
				txt12b2.Text = "X";
				txt12bAmount.Text = "999.99";
				txt12b2Amount.Text = "999.99";
				txt12c.Text = "X";
				txt12c2.Text = "X";
				txt12cAmount.Text = "999.99";
				txt12c2Amount.Text = "999.99";
				txt12d.Text = "X";
				txt12d2.Text = "X";
				txt12dAmount.Text = "999.99";
				txt12d2Amount.Text = "999.99";
				txtDependentCare.Text = "999.99";
				txtDependentCare2.Text = "999.99";
				txtEmployeesName.Text = "First Middle Last" + "\r\n" + "Address" + "\r\n" + "City, ST Zip";
				txtEmployeesName2.Text = "Address " + "\r\n" + "City, ST Zip";
				txtLastName.Text = "Last";
				txtFirstAndMiddle.Text = "First Middle";
				txtEmployersName.Text = "Employer";
				txtEmployersName2.Text = "Employer";
				txtEmployersStateEIN.Text = "XXXX";
				txtEmployersStateEIN2.Text = "XXXX";
				txtFederalEIN.Text = "XXXX";
				txtFederalEIN2.Text = "XXXX";
				txtFederalTax.Text = "999.99";
				txtFederalTax2.Text = "999.99";
				txtFICATax.Text = "999.99";
				txtFICATax2.Text = "999.99";
				txtFICAWages.Text = "999.99";
				txtFICAWages2.Text = "999.99";
				txtMedTaxes.Text = "999.99";
				txtMedTaxes2.Text = "999.99";
				txtMedWages.Text = "999.99";
				txtMedWages2.Text = "999.99";
				txtSSN.Text = "999-99-9999";
				txtSSN2.Text = "999-99-9999";
				txtState.Text = "99";
				txtState2.Text = "99";
				txtStateTax.Text = "999.99";
				txtStateTax2.Text = "999.99";
				txtStateWages.Text = "999.99";
				txtStateWages2.Text = "999.99";
				txtTotalWages.Text = "999.99";
				txtTotalWages2.Text = "999.99";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for(cnt=0; cnt<=this.Toolbar.Tools.Count-1; cnt++) {
			//	if ("Print..."==this.Toolbar.Tools(cnt).Caption) {
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//} // cnt
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 11F;
            //this.Printer.RenderMode = 1;
            if (!boolPrintTest)
            {
                dblLaserLineAdjustment = modGlobalRoutines.SetW2LineAdjustment(this);
				dblHorizAdjust = FCConvert.ToDouble(modGlobalRoutines.SetW2HorizAdjustment(this));
            }
            else
            {
                foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
                {
                    ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
                    ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
                }
            }
            dblLaserLineAdjustment += 2;
			if (!boolPrintTest)
			{
				if (FCConvert.ToDouble(modGlobalVariables.Statics.gstrPrintAllW2s) == 0)
				{
					string strOrder = "";
					switch (modCoreysSweeterCode.GetReportSequence())
					{
						case 0:
							{
								strOrder = "Lastname,firstname";
								break;
							}
						case 1:
							{
								strOrder = "Employeenumber";
								break;
							}
						case 2:
							{
								strOrder = "Seqnumber,lastname,firstname";
								break;
							}
						case 3:
							{
								strOrder = "DeptDiv,lastname,firstname";
								break;
							}
					}
					//end switch
					rsData.OpenRecordset("Select * from tblW2EditTable order by " + strOrder, "TWPY0000.vb1");
				}
				else
				{
					rsData.OpenRecordset("Select * from tblW2EditTable Where EmployeeNumber = '" + modGlobalVariables.Statics.gstrPrintAllW2s + "'", "TWPY0000.vb1");
				}
				if (!rsData.EndOfFile())
				{
					lngYearToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("year"))));
				}
				rsW2Master.OpenRecordset("Select * from tblW2Master", "TWPY0000.vb1");
				rsbox12.OpenRecordset("Select * from tblW2Box12AND14 Where Box12 = 1 ");
				rsBox14.OpenRecordset("Select * from tblW2Box12AND14 Where Box14 = 1");
			}
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			//foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			//{
			//	ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
			//	ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
			//}
		}
	}
}
