﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmDirectDeposit.
	/// </summary>
	partial class frmDirectDeposit
	{
		public FCGrid VSDeposit;
		public fecherFoundation.FCTextBox txtMinCheck;
		public fecherFoundation.FCFrame fraWarning;
		public fecherFoundation.FCLabel lblWarning;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddBank;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuline;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuBanks;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.VSDeposit = new fecherFoundation.FCGrid();
			this.txtMinCheck = new fecherFoundation.FCTextBox();
			this.fraWarning = new fecherFoundation.FCFrame();
			this.lblWarning = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblEmployeeNumber = new fecherFoundation.FCLabel();
			this.mnuBanks = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddBank = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuline = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSelectEmployee = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdBanks = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.VSDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWarning)).BeginInit();
			this.fraWarning.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBanks)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 592);
			this.BottomPanel.Size = new System.Drawing.Size(746, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.VSDeposit);
			this.ClientArea.Controls.Add(this.txtMinCheck);
			this.ClientArea.Controls.Add(this.fraWarning);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblEmployeeNumber);
			this.ClientArea.Size = new System.Drawing.Size(766, 606);
			this.ClientArea.Controls.SetChildIndex(this.lblEmployeeNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraWarning, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMinCheck, 0);
			this.ClientArea.Controls.SetChildIndex(this.VSDeposit, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdBanks);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdSelectEmployee);
			this.TopPanel.Size = new System.Drawing.Size(766, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSelectEmployee, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdBanks, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.AutoSize = false;
			this.HeaderText.Size = new System.Drawing.Size(308, 28);
			this.HeaderText.Text = "Direct Deposit Breakdown";
			// 
			// VSDeposit
			// 
			this.VSDeposit.AllowDrag = true;
			this.VSDeposit.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.VSDeposit.Cols = 10;
			this.VSDeposit.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.VSDeposit.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
			this.VSDeposit.ExtendLastCol = true;
			this.VSDeposit.Location = new System.Drawing.Point(30, 204);
			this.VSDeposit.Name = "VSDeposit";
			this.VSDeposit.ReadOnly = false;
			this.VSDeposit.Rows = 50;
			this.VSDeposit.Size = new System.Drawing.Size(708, 388);
			this.VSDeposit.TabIndex = 5;
			this.VSDeposit.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.VSDeposit_KeyDownEdit);
			this.VSDeposit.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.VSDeposit_KeyPressEdit);
			this.VSDeposit.ComboDropDown += new System.EventHandler(this.VSDeposit_ComboDropDown);
			this.VSDeposit.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.VSDeposit_AfterEdit);
			this.VSDeposit.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.VSDeposit_ValidateEdit);
			this.VSDeposit.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.VSDeposit_ClickEvent);
			this.VSDeposit.KeyDown += new Wisej.Web.KeyEventHandler(this.VSDeposit_KeyDownEvent);
			// 
			// txtMinCheck
			// 
			this.txtMinCheck.BackColor = System.Drawing.SystemColors.Window;
			this.txtMinCheck.Location = new System.Drawing.Point(250, 115);
			this.txtMinCheck.Name = "txtMinCheck";
			this.txtMinCheck.Size = new System.Drawing.Size(117, 40);
			this.txtMinCheck.TabIndex = 3;
			this.txtMinCheck.Text = "0.00";
			this.txtMinCheck.Validating += new System.ComponentModel.CancelEventHandler(this.txtMinCheck_Validating);
			// 
			// fraWarning
			// 
			this.fraWarning.Controls.Add(this.lblWarning);
			this.fraWarning.Location = new System.Drawing.Point(30, 30);
			this.fraWarning.Name = "fraWarning";
			this.fraWarning.Size = new System.Drawing.Size(708, 65);
			this.fraWarning.TabIndex = 1001;
			this.fraWarning.Text = "Warning!!!!";
			// 
			// lblWarning
			// 
			this.lblWarning.Location = new System.Drawing.Point(20, 30);
			this.lblWarning.Name = "lblWarning";
			this.lblWarning.Size = new System.Drawing.Size(667, 15);
			this.lblWarning.TabIndex = 1;
			this.lblWarning.Text = "THIS EMPLOYEE HAS CHOSEN A CHECK TYPE OF REGULAR. THESE DIRECT DEPOSIT ENTRIES WI" +
    "LL NOT BE APPLIED";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 129);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(179, 15);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "MINIMUM CHECK AMOUNT";
			// 
			// lblEmployeeNumber
			// 
			this.lblEmployeeNumber.Location = new System.Drawing.Point(30, 175);
			this.lblEmployeeNumber.Name = "lblEmployeeNumber";
			this.lblEmployeeNumber.Size = new System.Drawing.Size(708, 15);
			this.lblEmployeeNumber.TabIndex = 2;
			this.lblEmployeeNumber.Tag = "Employee #";
			this.lblEmployeeNumber.Text = "EMPLOYEE #";
			// 
			// mnuBanks
			// 
			this.mnuBanks.Index = -1;
			this.mnuBanks.Name = "mnuBanks";
			this.mnuBanks.Text = "Modify / Update Banks";
			this.mnuBanks.Click += new System.EventHandler(this.mnuBanks_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddBank,
            this.mnuDelete,
            this.mnuline,
            this.mnuSelectEmployee,
            this.mnuSP3,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuAddBank
			// 
			this.mnuAddBank.Index = 0;
			this.mnuAddBank.Name = "mnuAddBank";
			this.mnuAddBank.Text = "New";
			this.mnuAddBank.Click += new System.EventHandler(this.mnuAddBank_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuline
			// 
			this.mnuline.Index = 2;
			this.mnuline.Name = "mnuline";
			this.mnuline.Text = "-";
			// 
			// mnuSelectEmployee
			// 
			this.mnuSelectEmployee.Index = 3;
			this.mnuSelectEmployee.Name = "mnuSelectEmployee";
			this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuSelectEmployee.Text = "Select Employee";
			this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
			// 
			// mnuSP3
			// 
			this.mnuSP3.Index = 4;
			this.mnuSP3.Name = "mnuSP3";
			this.mnuSP3.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 5;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                            ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 6;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit                ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 7;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 8;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.Location = new System.Drawing.Point(494, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(50, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.mnuAddBank_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Location = new System.Drawing.Point(550, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(60, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdSelectEmployee
			// 
			this.cmdSelectEmployee.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSelectEmployee.Location = new System.Drawing.Point(616, 29);
			this.cmdSelectEmployee.Name = "cmdSelectEmployee";
			this.cmdSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdSelectEmployee.Size = new System.Drawing.Size(122, 24);
			this.cmdSelectEmployee.TabIndex = 3;
			this.cmdSelectEmployee.Text = "Select Employee";
			this.cmdSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(312, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(82, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdBanks
			// 
			this.cmdBanks.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdBanks.Location = new System.Drawing.Point(344, 29);
			this.cmdBanks.Name = "cmdBanks";
			this.cmdBanks.Size = new System.Drawing.Size(144, 24);
			this.cmdBanks.TabIndex = 4;
			this.cmdBanks.Text = "Modify/Update Banks";
			this.cmdBanks.Click += new System.EventHandler(this.mnuBanks_Click);
			// 
			// frmDirectDeposit
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(766, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmDirectDeposit";
			this.Text = "Direct Deposit Breakdown";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmDirectDeposit_Load);
			this.Activated += new System.EventHandler(this.frmDirectDeposit_Activated);
			this.Resize += new System.EventHandler(this.frmDirectDeposit_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDirectDeposit_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VSDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWarning)).EndInit();
			this.fraWarning.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBanks)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdNew;
        private FCButton cmdSelectEmployee;
        private FCButton cmdDelete;
        private FCButton cmdSave;
		private FCButton cmdBanks;
	}
}
