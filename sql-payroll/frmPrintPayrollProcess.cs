//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmPrintPayrollProcess : BaseForm
	{
		public frmPrintPayrollProcess()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintPayrollProcess InstancePtr
		{
			get
			{
				return (frmPrintPayrollProcess)Sys.GetInstance(typeof(frmPrintPayrollProcess));
			}
		}

		protected frmPrintPayrollProcess _InstancePtr = null;
		//=========================================================
		private string strEmployeeNumber = "";
		private int intPrintSequence;
		private clsDRWrapper rsDefault = new clsDRWrapper();
		private bool pboolSelect;
		private string strASC = string.Empty;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;
		private bool boolNetZero;

		private void frmPrintPayrollProcess_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			if (modGlobalVariables.Statics.gstrPrintProcessDetailReport == string.Empty)
			{
				Close();
			}
		}

		public void Init(bool boolZeroNet)
		{
			boolNetZero = boolZeroNet;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 2; intCounter <= (this.vsData.Rows - 1); intCounter++)
			{
				if (pboolSelect)
				{
					vsData.TextMatrix(intCounter, 0, FCConvert.ToString(true));
				}
				else
				{
					vsData.TextMatrix(intCounter, 0, FCConvert.ToString(false));
				}
			}
			pboolSelect = !pboolSelect;
		}

		private void frmPrintPayrollProcess_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsDefault.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
				if (rsDefault.EndOfFile())
				{
					intPrintSequence = 0;
				}
				else
				{
					// Employee Name = 0
					// Employee Number = 1
					// Sequence Number = 2
					// Department / Div = 3
					intPrintSequence = FCConvert.ToInt16(rsDefault.Get_Fields_Int32("ReportSequence"));
				}
				strASC = "ASC";
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				LoadGrid();
				pboolSelect = true;
				if (modGlobalVariables.Statics.gstrPrintProcessDetailReport == string.Empty)
				{
					mnuPrintSummary_Click();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPrintPayrollProcess_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmPrintPayrollProcess_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsTotals = new clsDRWrapper();
			int intSick = 0;
			int intVac = 0;
			int intOther;
			string strFind;
			strFind = "";
			int lngID;
			lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			rsData.OpenRecordset("select * from payrollpermissions where UserID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
			while (!rsData.EndOfFile())
			{
				switch (rsData.Get_Fields_Int32("type"))
				{
					case CNSTPYTYPEDEPTDIV:
						{
							strFind += "not deptdiv = '" + rsData.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPEGROUP:
						{
							strFind += "not groupid = '" + rsData.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPEIND:
						{
							strFind += "not tbltemppayprocess.employeenumber = '" + rsData.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPESEQ:
						{
							strFind += "not seqnumber = " + FCConvert.ToString(Conversion.Val(rsData.Get_Fields("val"))) + " and ";
							break;
						}
					case CNSTPYTYPEDEPARTMENT:
						{
							strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsData.Get_Fields("val"))) + " and ";
							break;
						}
				}
				//end switch
				rsData.MoveNext();
			}
			vsData.Rows = 2;
			vsData.Cols = 19;
			vsData.FixedCols = 0;
			vsData.FixedRows = 2;
			vsData.TextMatrix(0, 0, " ");
			vsData.TextMatrix(0, 1, " ");
			vsData.TextMatrix(1, 1, " # ");
			vsData.TextMatrix(0, 2, " ");
			vsData.TextMatrix(1, 2, "Employee Name");
			vsData.TextMatrix(0, 3, " ");
			vsData.TextMatrix(1, 3, "Gross");
			vsData.TextMatrix(0, 4, "- - - - - - - - - - - - - - - - - - - -");
			vsData.TextMatrix(1, 4, "Federal");
			vsData.TextMatrix(0, 5, "- - - - - - T a x e s");
			vsData.TextMatrix(1, 5, "FICA");
			vsData.TextMatrix(0, 6, "- - - - - - - - - - - - - - - - - - - -");
			vsData.TextMatrix(1, 6, "Medicare");
			vsData.TextMatrix(0, 7, "- - - - - - - - - - - - - - - - - - - -");
			vsData.TextMatrix(1, 7, "State");
			vsData.TextMatrix(0, 8, " ");
			vsData.TextMatrix(1, 8, "Deductions");
			vsData.TextMatrix(0, 9, " ");
			vsData.TextMatrix(1, 9, "Net Pay");
			vsData.TextMatrix(0, 10, " ");
			vsData.TextMatrix(1, 10, "Emp Match");
			vsData.TextMatrix(0, 11, " ");
			vsData.TextMatrix(1, 11, "Hours");
			vsData.TextMatrix(0, 12, "- - - - - - - - - - - - - -");
			vsData.TextMatrix(1, 12, "Accrued");
			vsData.TextMatrix(0, 13, "S i c k - - - - - - - - ");
			vsData.TextMatrix(1, 13, "Used");
			vsData.TextMatrix(0, 14, "- - - - - - - - - - - - - -");
			vsData.TextMatrix(1, 14, "Accrued");
			vsData.TextMatrix(0, 15, "V a c a t i o n - - - - - -");
			vsData.TextMatrix(1, 15, "Used");
			vsData.TextMatrix(0, 16, "- - - - - - - - - - - - - -");
			vsData.TextMatrix(1, 16, "Accrued");
			vsData.TextMatrix(0, 17, "O t h e r - - - - - - - - -");
			vsData.TextMatrix(1, 17, "Used");
			vsData.TextMatrix(1, 18, "MasterRecord");
			vsData.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
            vsData.MergeRow(0, true);
            vsData.MergeRow(1, true);
            vsData.ColHidden(18, true);
			vsData.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			vsData.FrozenCols = 4;
			if (intPrintSequence == 0)
			{
				// Employee Name = 0
				rsData.OpenRecordset("SELECT tblTempPayProcess.EmployeeNumber as TempEmployeeMaster, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, * FROM tblTempPayProcess INNER JOIN tblEmployeeMaster ON tblTempPayProcess.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where " + strFind + " (((tblTempPayProcess.TotalRecord) = 1)) ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName " + strASC, "TWPY0000.vb1");
			}
			else if (intPrintSequence == 1)
			{
				// Employee Number = 1
				rsData.OpenRecordset("Select tblTempPayProcess.EmployeeNumber as TempEmployeeMaster, * from tblTempPayProcess inner join tblemployeemaster on (tbltemppayprocess.employeenumber = tblemployeemaster.employeenumber) Where " + strFind + " TotalRecord = 1 Order by tbltemppayprocess.EmployeeNumber " + strASC, "TWPY0000.vb1");
			}
			else if (intPrintSequence == 2)
			{
				// Sequence Number = 2
				rsData.OpenRecordset("SELECT tblTempPayProcess.EmployeeNumber as TempEmployeeMaster, tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.EmployeeNumber, * FROM tblTempPayProcess INNER JOIN tblEmployeeMaster ON tblTempPayProcess.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where " + strFind + " (((tblTempPayProcess.TotalRecord) = 1)) ORDER BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.EmployeeNumber " + strASC, "TWPY0000.vb1");
			}
			else if (intPrintSequence == 3)
			{
				// Department / Div = 3
				rsData.OpenRecordset("SELECT tblTempPayProcess.EmployeeNumber as TempEmployeeMaster, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.EmployeeNumber, * FROM tblTempPayProcess INNER JOIN tblEmployeeMaster ON tblTempPayProcess.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where " + strFind + " (((tblTempPayProcess.TotalRecord) = 1)) ORDER BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.EmployeeNumber " + strASC, "TWPY0000.vb1");
			}
			for (intCounter = 2; intCounter <= (rsData.RecordCount() + 1); intCounter++)
			{
				vsData.Rows += 1;
				vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(false));
				vsData.TextMatrix(vsData.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("TempEmployeeMaster")));
				vsData.TextMatrix(vsData.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployeeName")));
				vsData.TextMatrix(vsData.Rows - 1, 3, Strings.Format(rsData.Get_Fields("GrossPay"), "0.00"));
				vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(rsData.Get_Fields("FederalTaxWH"), "0.00"));
				vsData.TextMatrix(vsData.Rows - 1, 5, Strings.Format(rsData.Get_Fields("FICATaxWH"), "0.00"));
				vsData.TextMatrix(vsData.Rows - 1, 6, Strings.Format(rsData.Get_Fields("MedicareTaxWH"), "0.00"));
				vsData.TextMatrix(vsData.Rows - 1, 7, Strings.Format(rsData.Get_Fields("StateTaxWH"), "0.00"));
				// DEDUCTION AMOUNT SUMS
				rsTotals.OpenRecordset("Select sum(DedAmount) as DedTotal from tblTempPayProcess where EmployeeNumber = '" + rsData.Get_Fields("TempEmployeeMaster") + "' and MasterRecord = '" + rsData.Get_Fields_String("MasterRecord") + "' AND deductionrecord = 1", "TWPY0000.vb1");
				if (rsTotals.EndOfFile())
				{
					vsData.TextMatrix(vsData.Rows - 1, 8, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 8, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("DedTotal"))), "0.00"));
				}
				// NET PAY SUMS
				vsData.TextMatrix(vsData.Rows - 1, 9, Strings.Format(rsData.Get_Fields_Decimal("NetPay"), "0.00"));
				// EMPLOYERS MATCH AMOUNT SUMS
				rsTotals.OpenRecordset("Select sum(MatchAmount) as MatchTotal from tblTempPayProcess where EmployeeNumber = '" + rsData.Get_Fields("TempEmployeeMaster") + "' and MasterRecord = '" + rsData.Get_Fields_String("MasterRecord") + "' AND matchrecord = 1", "TWPY0000.vb1");
				if (rsTotals.EndOfFile())
				{
					vsData.TextMatrix(vsData.Rows - 1, 10, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 10, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("MatchTotal"))), "0.00"));
				}
				rsTotals.OpenRecordset("SELECT Sum(tblTempPayProcess.DistHours) AS SumOfDistHours FROM tblTempPayProcess INNER JOIN tblPayCategories ON tblTempPayProcess.DistPayCategory = tblPayCategories.ID GROUP BY tblTempPayProcess.EmployeeNumber, tblTempPayProcess.MasterRecord, tblTempPayProcess.DistributionRecord, tblPayCategories.Type HAVING (((tblTempPayProcess.EmployeeNumber)='" + rsData.Get_Fields("TempEmployeeMaster") + "') AND ((tblTempPayProcess.MasterRecord)='" + rsData.Get_Fields_String("MasterRecord") + "') AND ((tblTempPayProcess.DistributionRecord)=1) AND ((tblPayCategories.Type)<> 'Dollars'))", "TWPY0000.vb1");
				if (rsTotals.EndOfFile())
				{
					vsData.TextMatrix(vsData.Rows - 1, 11, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 11, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("SumOfDistHours"))), "0.00"));
				}
				rsTotals.OpenRecordset("Select * from tblCodeTypes", "TWPY0000.vb1");
				if (!rsTotals.EndOfFile())
				{
					rsTotals.FindFirstRecord("Description", "Sick");
					if (rsTotals.NoMatch)
					{
						intSick = 0;
					}
					else
					{
						intSick = FCConvert.ToInt16(rsTotals.Get_Fields("ID"));
					}
					rsTotals.FindFirstRecord("Description", "Vacation");
					if (rsTotals.NoMatch)
					{
						intVac = 0;
					}
					else
					{
						intVac = FCConvert.ToInt16(rsTotals.Get_Fields("ID"));
					}
				}
				else
				{
					intSick = 0;
					intVac = 0;
				}
				// SICK
				rsTotals.OpenRecordset("Select sum(VSAccrued) as AccruedTotal, sum(VSUsed) as UsedTotal from tblTempPayProcess where EmployeeNumber = '" + rsData.Get_Fields("TempEmployeeMaster") + "' and MasterRecord = '" + rsData.Get_Fields_String("MasterRecord") + "' AND VSRecord = 1 and VSTypeID = " + FCConvert.ToString(intSick), "TWPY0000.vb1");
				if (rsTotals.EndOfFile())
				{
					vsData.TextMatrix(vsData.Rows - 1, 12, FCConvert.ToString(0));
					vsData.TextMatrix(vsData.Rows - 1, 13, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 12, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("AccruedTotal"))), "0.0000"));
					vsData.TextMatrix(vsData.Rows - 1, 13, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("UsedTotal"))), "0.00"));
				}
				// VAC
				rsTotals.OpenRecordset("Select sum(VSAccrued) as AccruedTotal, sum(VSUsed) as UsedTotal from tblTempPayProcess where EmployeeNumber = '" + rsData.Get_Fields("TempEmployeeMaster") + "' and MasterRecord = '" + rsData.Get_Fields_String("MasterRecord") + "' AND VSRecord = 1 AND VSTypeID = " + FCConvert.ToString(intVac), "TWPY0000.vb1");
				if (rsTotals.EndOfFile())
				{
					vsData.TextMatrix(vsData.Rows - 1, 14, FCConvert.ToString(0));
					vsData.TextMatrix(vsData.Rows - 1, 15, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 14, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("AccruedTotal"))), "0.0000"));
					vsData.TextMatrix(vsData.Rows - 1, 15, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("UsedTotal"))), "0.00"));
				}
				// OTHER
				rsTotals.OpenRecordset("Select sum(VSAccrued) as AccruedTotal, sum(VSUsed) as UsedTotal from tblTempPayProcess where EmployeeNumber = '" + rsData.Get_Fields("TempEmployeeMaster") + "' and MasterRecord = '" + rsData.Get_Fields_String("MasterRecord") + "' AND VSRecord = 1 AND VSTypeID <> " + FCConvert.ToString(intVac) + " AND VSTypeID <> " + FCConvert.ToString(intSick), "TWPY0000.vb1");
				if (rsTotals.EndOfFile())
				{
					vsData.TextMatrix(vsData.Rows - 1, 16, FCConvert.ToString(0));
					vsData.TextMatrix(vsData.Rows - 1, 17, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 16, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("AccruedTotal"))), "0.0000"));
					vsData.TextMatrix(vsData.Rows - 1, 17, Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("UsedTotal"))), "0.00"));
				}
				vsData.TextMatrix(vsData.Rows - 1, 18, rsData.Get_Fields_String("MasterRecord"));
				rsData.MoveNext();
			}
			if (vsData.Rows > 2)
			{
				vsData.Select(2, 0);
				vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsData.EditCell();
				vsData.TextMatrix(2, 0, FCConvert.ToString(false));
			}
		}

		private void frmPrintPayrollProcess_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(0, FCConvert.ToInt32(vsData.WidthOriginal * 0.05));
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.27));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrintDetail_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			modGlobalVariables.Statics.gboolCancelSelected = false;
			for (intCounter = 2; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				if (FCConvert.CBool(vsData.TextMatrix(intCounter, 0)) == true)
				{
					// GET THE CHECK TYPE P0=PRIMARY P1=SECOND CHECK..... AND EMPLOYEE NUMBER
					// TO SHOW. THIS VARIABLE WILL BE USED IN THE DETAIL REPORT.
					modGlobalVariables.Statics.gstrPrintProcessDetailReport += "^" + vsData.TextMatrix(intCounter, 18) + vsData.TextMatrix(intCounter, 1);
				}
			}
			Close();
		}

		private void mnuPrintSummary_Click()
		{
			// rptPrintProcessSummary.Show , MDIParent
			// frmReportViewer.Init rptPrintProcessSummary, , vbModal, , , , , , , , True, "PrintProcessSummary"
			rptPrintProcessSummary.InstancePtr.Init(this.Modal, boolNetZero);
		}

		private void vsData_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsData.MouseRow == 0 || vsData.MouseRow == 1)
			{
				if (vsData.MouseCol == 0)
				{
					// vbPorter upgrade warning: boolValue As bool	OnWrite(string)
					bool boolValue = false;
					// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
					int intCounter;
					for (intCounter = 2; intCounter <= (vsData.Rows - 1); intCounter++)
					{
						if (intCounter == 2)
						{
							boolValue = FCConvert.CBool(vsData.TextMatrix(intCounter, 0));
						}
						vsData.TextMatrix(intCounter, 0, FCConvert.ToString(!boolValue));
					}
				}
			}
		}
	}
}
