﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cTax
	{
		//=========================================================
		private string strName = string.Empty;
		private double dblWithheld;
		private double dblTaxable;

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public double Tax
		{
			set
			{
				dblWithheld = value;
			}
			get
			{
				double Tax = 0;
				Tax = dblWithheld;
				return Tax;
			}
		}

		public double Taxable
		{
			set
			{
				dblTaxable = value;
			}
			get
			{
				double Taxable = 0;
				Taxable = dblTaxable;
				return Taxable;
			}
		}
	}
}
