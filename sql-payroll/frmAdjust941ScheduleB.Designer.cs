//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmAdjust941ScheduleB.
	/// </summary>
	partial class frmAdjust941ScheduleB
	{
		public fecherFoundation.FCGrid vsBanks;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.vsBanks = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 514);
            this.BottomPanel.Size = new System.Drawing.Size(877, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsBanks);
            this.ClientArea.Size = new System.Drawing.Size(877, 454);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdRefresh);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(877, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRefresh, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(338, 30);
            this.HeaderText.Text = "Verify Schedule B Information";
            // 
            // vsBanks
            // 
            this.vsBanks.AllowSelection = false;
            this.vsBanks.AllowUserToResizeColumns = false;
            this.vsBanks.AllowUserToResizeRows = false;
            this.vsBanks.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBanks.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsBanks.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsBanks.BackColorBkg = System.Drawing.Color.Empty;
            this.vsBanks.BackColorFixed = System.Drawing.Color.Empty;
            this.vsBanks.BackColorSel = System.Drawing.Color.Empty;
            this.vsBanks.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsBanks.Cols = 10;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsBanks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsBanks.ColumnHeadersHeight = 30;
            this.vsBanks.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsBanks.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsBanks.DragIcon = null;
            this.vsBanks.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsBanks.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsBanks.ExtendLastCol = true;
            this.vsBanks.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsBanks.FrozenCols = 0;
            this.vsBanks.GridColor = System.Drawing.Color.Empty;
            this.vsBanks.GridColorFixed = System.Drawing.Color.Empty;
            this.vsBanks.Location = new System.Drawing.Point(30, 30);
            this.vsBanks.Name = "vsBanks";
            this.vsBanks.OutlineCol = 0;
            this.vsBanks.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsBanks.RowHeightMin = 0;
            this.vsBanks.Rows = 50;
            this.vsBanks.ScrollTipText = null;
            this.vsBanks.ShowColumnVisibilityMenu = false;
            this.vsBanks.Size = new System.Drawing.Size(817, 400);
            this.vsBanks.StandardTab = true;
            this.vsBanks.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsBanks.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsBanks.TabIndex = 0;
            this.vsBanks.CurrentCellChanged += new System.EventHandler(this.vsBanks_RowColChange);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(796, 26);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(45, 24);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Visible = false;
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(669, 26);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Visible = false;
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(618, 26);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Visible = false;
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRefresh.AppearanceKey = "toolbarButton";
            this.cmdRefresh.Location = new System.Drawing.Point(730, 26);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 24);
            this.cmdRefresh.TabIndex = 4;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSP3,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSave.Text = "Save                            ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit without showing report";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(389, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(80, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Save";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmAdjust941ScheduleB
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(877, 622);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmAdjust941ScheduleB";
            this.Text = "Verify Schedule B Information";
            this.Activated += new System.EventHandler(this.frmAdjust941ScheduleB_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAdjust941ScheduleB_KeyPress);
            this.Resize += new System.EventHandler(this.frmAdjust941ScheduleB_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}