//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmPrintParameters : BaseForm
	{
		public frmPrintParameters()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintParameters InstancePtr
		{
			get
			{
				return (frmPrintParameters)Sys.GetInstance(typeof(frmPrintParameters));
			}
		}

		protected frmPrintParameters _InstancePtr = null;
		//=========================================================
		private void cmdPrint_Click()
		{
			if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEDEDUCTIONS")
			{
				modGlobalVariables.Statics.gboolSamePage = cmbPage.Text == "Print on same page";
				modCustomReport.Statics.strCustomTitle = "Employee Deductions Report";
				frmCustomReport.InstancePtr.Show(App.MainForm);
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "EMPLOYEEDEDUCTIONS");
			}
			else if (modCustomReport.Statics.strPreSetReport == "PAYSUMMARY")
			{
				frmCustomReport.InstancePtr.Show(App.MainForm);
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "PAYSUMMARY");
			}
			else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEVACATION")
			{
				if (cboVacSickTypes.SelectedIndex < 0)
				{
					MessageBox.Show("Type of report must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					modGlobalVariables.Statics.gstrVacType = cboVacSickTypes.Text;
					modCustomReport.Statics.strCustomTitle = "Payroll " + modGlobalVariables.Statics.gstrVacType + " Report";
				}
				// frmCustomReport.Show
				// Call SetFormFieldCaptions(frmCustomReport, "EMPLOYEEVACATION")
				if (fraDeptDiv.Visible && chkDeptDiv.CheckState == Wisej.Web.CheckState.Checked)
				{
					modCoreysSweeterCode.PrintVacSickReports(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.gstrVacType)), cboReportPeriod.SelectedIndex, true);
				}
				else
				{
					modCoreysSweeterCode.PrintVacSickReports(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.gstrVacType)), cboReportPeriod.SelectedIndex);
				}
			}
			else
			{
			}
			Close();
		}

		private void frmPrintParameters_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmPrintParameters_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
				return;
			}
		}

		private void frmPrintParameters_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			// If KeyAscii = 27 Then Unload Me
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPrintParameters_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintParameters properties;
			//frmPrintParameters.ScaleWidth	= 3885;
			//frmPrintParameters.ScaleHeight	= 2250;
			//frmPrintParameters.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEDEDUCTIONS")
				{
					cmbPage.Visible = true;
					cboVacSickTypes.Visible = false;
					cboReportPeriod.Visible = false;
					fraDeptDiv.Visible = false;
				}
				else if (modCustomReport.Statics.strPreSetReport == "PAYSUMMARY")
				{
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEVACATION")
				{
					cmbPage.Visible = false;
					cboVacSickTypes.Visible = true;
					frmPeriod.Visible = true;
					fraDeptDiv.Visible = false;
					cboReportPeriod.Clear();
					cboReportPeriod.AddItem("Current Statistics");
					cboReportPeriod.AddItem("Historical Data");
					cboReportPeriod.SelectedIndex = 0;
					if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						frmPeriod.Text = "Report Layout";
						cboReportPeriod.Visible = true;
						cboReportPeriod.Enabled = true;
					}
					else
					{
						cboReportPeriod.Visible = false;
						frmPeriod.Visible = false;
					}
					// If gstrMQYProcessing = "WEEKLY" Then
					// cboReportPeriod.ListIndex = 0
					// ElseIf gstrMQYProcessing = "MONTHLY" Then
					// cboReportPeriod.ListIndex = 1
					// ElseIf gstrMQYProcessing = "QUARTERLY" Then
					// cboReportPeriod.ListIndex = 2
					// ElseIf gstrMQYProcessing = "YEARLY" Then
					// cboReportPeriod.ListIndex = 3
					// ElseIf gstrMQYProcessing = "NONE" Then
					// cboReportPeriod.ListIndex = 4
					// cboReportPeriod.Enabled = True
					// End If
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.OpenRecordset("Select * from tblCodeTypes");
					while (!rsData.EndOfFile())
					{
						cboVacSickTypes.AddItem(FCConvert.ToString(rsData.Get_Fields("Description")));
						cboVacSickTypes.ItemData(this.cboVacSickTypes.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
						rsData.MoveNext();
					}
					cboVacSickTypes.AddItem("All Types");
					cboVacSickTypes.ItemData(this.cboVacSickTypes.NewIndex, -1000);
					cboVacSickTypes.SelectedIndex = 0;
					rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						if (FCConvert.ToInt32(rsData.Get_Fields("reportsequence")) == 3)
						{
							// dept div
							fraDeptDiv.Visible = true;
						}
					}
					rsData = null;
				}
				else
				{
				}
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 2);
				modGlobalFunctions.SetTRIOColors(this);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEVACATION")
			{
				if (cboVacSickTypes.SelectedIndex >= 0)
				{
					modGlobalVariables.Statics.gstrVacType = FCConvert.ToString(cboVacSickTypes.ItemData(cboVacSickTypes.SelectedIndex));
					// If cboReportPeriod.ListIndex = 1 Then
					// historical
					modCoreysSweeterCode.PrintVacSickReports(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.gstrVacType)), cboReportPeriod.SelectedIndex, chkDeptDiv.CheckState == Wisej.Web.CheckState.Checked);
					// Else
					// current info
					// End If
					// gboolPrintAllTypes = chkPrintAll
					// 
					// If gboolPrintAllTypes Then
					// strCustomTitle = "Employee Vacation / Sick / Other Report"
					// Else
					// strCustomTitle = "Employee " & cboVacSickTypes & " Report"
					// End If
					// strCustomSQL = "SELECT tblCodes.*, tblVacationSick.*, tblEmployeeMaster.*, tblCodes.Code FROM ((tblVacationSick LEFT JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID) LEFT JOIN tblCodeTypes ON tblVacationSick.TypeID = tblCodeTypes.ID) INNER JOIN tblEmployeeMaster ON tblVacationSick.EmployeeNumber = tblEmployeeMaster.EmployeeNumber"
					// strCustomSQL = strCustomSQL & " order by tblcodes.Code"
					// If gtypeFullSetReports.boolFullSet Then
					// rptEmployeeVacationSick.PrintReport (False)
					// Unload Me
					// ElseIf gstrVacType = -1000 Then
					// all types
					// must run for all types
					// Call clsLoad.OpenRecordset("select * from tblcodetypes", "twpy0000.vb1")
					// Do While Not clsLoad.EndOfFile
					// strCustomSQL = "SELECT tblCodes.*, tblVacationSick.*, tblEmployeeMaster.*, tblCodes.Code FROM ((tblVacationSick LEFT JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID) LEFT JOIN tblCodeTypes ON tblVacationSick.TypeID = tblCodeTypes.ID) INNER JOIN tblEmployeeMaster ON tblVacationSick.EmployeeNumber = tblEmployeeMaster.EmployeeNumber"
					// strCustomSQL = strCustomSQL & " where typeid = " & clsLoad.Fields("ID") & " order by tblcodes.Code"
					// strCustomTitle = "Employee " & clsLoad.Fields("description") & " Report"
					// rptEmployeeVacationSick.Show vbModal, MDIParent
					// clsLoad.MoveNext
					// Loop
					// 
					// Unload Me
					// Else
					// frmCustomReport.Show
					// Call SetFormFieldCaptions(frmCustomReport, "EMPLOYEEVACATION")
					// frmCustomReport.cmdPrint_Click
					// Unload frmCustomReport
					// End If
					// strPreSetReport = "EMPLOYEEVACATION"
				}
				else
				{
					MessageBox.Show("Invalid Vac / Sick Type selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEDEDUCTIONS")
			{
				frmReportViewer.InstancePtr.Init(rptEmployeeDeductionReport.InstancePtr, showModal: this.Modal);
				//rptEmployeeDeductionReport.InstancePtr.Show(App.MainForm);
			}
		}
	}
}
