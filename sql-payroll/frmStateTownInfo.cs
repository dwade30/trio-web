//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmStateTownInfo : BaseForm
	{
		public frmStateTownInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmStateTownInfo InstancePtr
		{
			get
			{
				return (frmStateTownInfo)Sys.GetInstance(typeof(frmStateTownInfo));
			}
		}

		protected frmStateTownInfo _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsState = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		const int NEWRECORD = 1;
		const int MODIFYRECORD = 2;
		const int STATESTATUSCOL = 3;
		const int TOWNSTATUSCOL = 3;
		const int COUNTYSTATUSCOL = 2;

		private void frmStateTownInfo_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmStateTownInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				switch (SSTab1.SelectedIndex)
				{
					case 0:
						{
							if (StateGrid.Col != 2)
								KeyAscii = KeyAscii - 32;
							break;
						}
					case 1:
						{
							if (TownGrid.Col != 2)
								KeyAscii = KeyAscii - 32;
							break;
						}
					case 2:
						{
							if (CountyGrid.Col != 2)
								KeyAscii = KeyAscii - 32;
							break;
						}
				}
				//end switch
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmStateTownInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStateTownInfo properties;
			//frmStateTownInfo.ScaleWidth	= 9075;
			//frmStateTownInfo.ScaleHeight	= 7245;
			//frmStateTownInfo.LinkTopic	= "Form1";
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			LoadStates();
			LoadTowns();
			// only want this run once
			TownGrid.ColComboList(2, FCConvert.ToString(LoadGridCellAsCombo("Select * from DefaultCounties", "Name", "ID")));
			LoadCounties();
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			clsHistoryClass.SetGridIDColumn("PY", "CountyGrid", 0);
			clsHistoryClass.SetGridIDColumn("PY", "StateGrid", 0);
			clsHistoryClass.SetGridIDColumn("PY", "TownGrid", 0);
		}

		private void frmStateTownInfo_Resize(object sender, System.EventArgs e)
		{
			TownGrid.ColWidth(1, FCConvert.ToInt32(TownGrid.WidthOriginal * 0.4));
			TownGrid.ColWidth(2, FCConvert.ToInt32(TownGrid.WidthOriginal * 0.4));
			CountyGrid.ColWidth(1, FCConvert.ToInt32(CountyGrid.WidthOriginal * 0.4));
			StateGrid.ColWidth(1, FCConvert.ToInt32(StateGrid.WidthOriginal * 0.25));
			StateGrid.ColWidth(2, FCConvert.ToInt32(StateGrid.WidthOriginal * 0.7));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						DeleteState();
						break;
					}
				case 1:
					{
						DeleteTown();
						break;
					}
				case 2:
					{
						DeleteCounty();
						break;
					}
			}
			//end switch
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						NewState();
						break;
					}
				case 1:
					{
						NewTown();
						break;
					}
				case 2:
					{
						NewCounty();
						break;
					}
			}
			//end switch
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						SaveState();
						break;
					}
				case 1:
					{
						SaveTown();
						break;
					}
				case 2:
					{
						SaveCounties();
						break;
					}
			}
			//end switch
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						if (SaveState() == false)
							return;
						break;
					}
				case 1:
					{
						if (SaveTown() == false)
							return;
						break;
					}
				case 2:
					{
						if (SaveCounties() == false)
							return;
						break;
					}
			}
			//end switch
			mnuExit_Click();
		}

		private void SSTab1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						StateGrid.Focus();
						break;
					}
				case 1:
					{
						TownGrid.Focus();
						break;
					}
				case 2:
					{
						CountyGrid.Focus();
						break;
					}
			}
			//end switch
		}

		private void StateGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			if (FCConvert.ToDouble(StateGrid.TextMatrix(StateGrid.Row, 3)) != 1)
				StateGrid.TextMatrix(StateGrid.Row, 3, FCConvert.ToString(2));
		}

		private void NewState()
		{
			StateGrid.AddItem("", StateGrid.Rows);
			StateGrid.TextMatrix(StateGrid.Rows - 1, 3, FCConvert.ToString(1));
			StateGrid.TopRow = StateGrid.Rows - 1;
			StateGrid.Row = StateGrid.Rows - 1;
			StateGrid.Col = 1;
		}

		private void LoadStates()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from States ORDER BY State", modGlobalVariables.DEFAULTDATABASE);
			StateGrid.Rows = rsData.RecordCount() + 1;
			StateGrid.Cols = 4;
			StateGrid.TextMatrix(0, 0, "ID");
			StateGrid.TextMatrix(0, 1, "State");
			StateGrid.TextMatrix(0, 2, "Description");
			StateGrid.ColHidden(0, true);
			StateGrid.ColHidden(3, true);
			StateGrid.ColEditMask(1, "LL");
			//StateGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			StateGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, StateGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			StateGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
			StateGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				StateGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
				StateGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("State")));
				StateGrid.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields("Description")));
				StateGrid.TextMatrix(intCounter, 3, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			StateGrid.TopRow = 1;
			StateGrid.Row = 1;
			StateGrid.Col = 1;
			clsHistoryClass.Init = this;
		}

		private void DeleteState()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (StateGrid.Row > 1)
				{
					if (MessageBox.Show("Are you sure you wish to delete this State?   " + StateGrid.TextMatrix(StateGrid.Row, 1) + " - " + StateGrid.TextMatrix(StateGrid.Row, 2), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
						clsHistoryClass.AddAuditHistoryDeleteEntry(ref StateGrid);
						if (FCConvert.ToDouble(StateGrid.TextMatrix(StateGrid.Row, 3)) == 1)
						{
							StateGrid.RemoveItem(StateGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from States where ID = " + StateGrid.TextMatrix(StateGrid.Row, 0), modGlobalVariables.DEFAULTDATABASE);
							StateGrid.RemoveItem(StateGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveState()
		{
			bool SaveState = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				StateGrid.Row = 0;
				for (intCounter = 1; intCounter <= (StateGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 1)).Length != 2 && fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 2)).Length > 0)
					{
						MessageBox.Show("States must have a two digit identifier.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						StateGrid.Row = intCounter;
						StateGrid.TopRow = intCounter;
						StateGrid.Col = 1;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveState;
					}
					else if (fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 2)).Length <= 0 && fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 1)).Length == 2)
					{
						MessageBox.Show("States must have a description.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						StateGrid.Row = intCounter;
						StateGrid.TopRow = intCounter;
						StateGrid.Col = 2;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveState;
					}
					if (FCConvert.ToDouble(StateGrid.TextMatrix(intCounter, STATESTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into States (State,Description) VALUES ('" + StateGrid.TextMatrix(intCounter, 1) + "','" + StateGrid.TextMatrix(intCounter, 2) + "')", modGlobalVariables.DEFAULTDATABASE);
						StateGrid.TextMatrix(intCounter, 3, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(StateGrid.TextMatrix(intCounter, STATESTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update States Set State = '" + fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 1)) + "', Description = '" + fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 2)) + "' where ID = " + StateGrid.TextMatrix(intCounter, 0), modGlobalVariables.DEFAULTDATABASE);
						StateGrid.TextMatrix(intCounter, 3, FCConvert.ToString(0));
					}
				}
				LoadStates();
				SaveState = true;
				clsHistoryClass.Compare();
				MessageBox.Show("Save of State information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveState;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveState;
		}
		// *********************************************
		private void TownGrid_ComboDropDown(object sender, System.EventArgs e)
		{
			if (FCConvert.ToDouble(TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL)) != 1)
				TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL, FCConvert.ToString(2));
		}

		private void TownGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			if (FCConvert.ToDouble(TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL)) != 1)
				TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL, FCConvert.ToString(2));
		}

		private void NewTown()
		{
			TownGrid.AddItem("", TownGrid.Rows);
			TownGrid.TextMatrix(TownGrid.Rows - 1, TOWNSTATUSCOL, FCConvert.ToString(1));
			TownGrid.TopRow = TownGrid.Rows - 1;
			TownGrid.Row = TownGrid.Rows - 1;
			TownGrid.Col = 1;
		}

		private void LoadTowns()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsCounty = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultTowns ORDER BY Name", modGlobalVariables.DEFAULTDATABASE);
			rsCounty.OpenRecordset("Select * from DefaultCounties ORDER BY Name", modGlobalVariables.DEFAULTDATABASE);
			TownGrid.Rows = rsData.RecordCount() + 1;
			TownGrid.Cols = 4;
			TownGrid.TextMatrix(0, 0, "ID");
			TownGrid.TextMatrix(0, 1, "Town");
			TownGrid.TextMatrix(0, 2, "County");
			TownGrid.ColHidden(0, true);
			TownGrid.ColHidden(TOWNSTATUSCOL, true);
			//TownGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			TownGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, TownGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			TownGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
			TownGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				TownGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
				TownGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				rsCounty.FindFirstRecord("ID", Conversion.Val(rsData.Get_Fields("County")));
				if (rsCounty.NoMatch)
				{
					TownGrid.TextMatrix(intCounter, 2, string.Empty);
				}
				else
				{
					TownGrid.TextMatrix(intCounter, 2, FCConvert.ToString(rsCounty.Get_Fields_String("Name")));
				}
				TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			TownGrid.TopRow = 1;
			TownGrid.Row = 1;
			TownGrid.Col = 1;
			clsHistoryClass.Init = this;
		}

		private void DeleteTown()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (TownGrid.Row > 1)
				{
					if (MessageBox.Show("Are you sure you wish to delete this Town?   " + TownGrid.TextMatrix(TownGrid.Row, 1) + " - " + TownGrid.TextMatrix(TownGrid.Row, 2), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						clsHistoryClass.AddAuditHistoryDeleteEntry(ref TownGrid);
						if (FCConvert.ToDouble(TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL)) == 1)
						{
							TownGrid.RemoveItem(TownGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from DefaultTowns where ID = " + TownGrid.TextMatrix(TownGrid.Row, 0), modGlobalVariables.DEFAULTDATABASE);
							TownGrid.RemoveItem(TownGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveTown()
		{
			bool SaveTown = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				TownGrid.Row = 0;
				for (intCounter = 1; intCounter <= (TownGrid.Rows - 1); intCounter++)
				{
					// If Len(Trim(TownGrid.TextMatrix(intCounter, 1))) <= 0 Then
					// MsgBox "Town name must be entered.", vbInformation + vbOKOnly, "TRIO Software"
					// TownGrid.Row = intCounter
					// TownGrid.TopRow = intCounter
					// TownGrid.Col = 1
					// Screen.MousePointer = vbDefault
					// Exit Function
					// End If
					if (FCConvert.ToDouble(TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DefaultTowns (Name,County) VALUES ('" + TownGrid.TextMatrix(intCounter, 1) + "'," + FCConvert.ToString(Conversion.Val(TownGrid.TextMatrix(intCounter, 2))) + ")", modGlobalVariables.DEFAULTDATABASE);
						TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DefaultTowns Set Name = '" + fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 1)) + "', County = " + FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 2)))) + " where ID = " + TownGrid.TextMatrix(intCounter, 0), modGlobalVariables.DEFAULTDATABASE);
						TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL, FCConvert.ToString(0));
					}
				}
				LoadTowns();
				SaveTown = true;
				clsHistoryClass.Compare();
				MessageBox.Show("Save of Town information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveTown;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveTown;
		}
		// *********************************************
		private void CountyGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			if (FCConvert.ToDouble(CountyGrid.TextMatrix(CountyGrid.Row, COUNTYSTATUSCOL)) != 1)
				CountyGrid.TextMatrix(CountyGrid.Row, COUNTYSTATUSCOL, FCConvert.ToString(2));
		}

        private void CountyGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= CountyGrid_KeyPressEdit;
            e.Control.KeyPress += CountyGrid_KeyPressEdit;
        }

        private void TownGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= TownGrid_KeyPressEdit;
            e.Control.KeyPress += TownGrid_KeyPressEdit;
        }

        private void StateGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= StateGrid_KeyPressEdit;
            e.Control.KeyPress += StateGrid_KeyPressEdit;
        }

        private void NewCounty()
		{
			CountyGrid.AddItem("", CountyGrid.Rows);
			CountyGrid.TextMatrix(CountyGrid.Rows - 1, COUNTYSTATUSCOL, FCConvert.ToString(1));
			CountyGrid.TopRow = CountyGrid.Rows - 1;
			CountyGrid.Row = CountyGrid.Rows - 1;
			CountyGrid.Col = 1;
		}

		private void LoadCounties()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultCounties ORDER BY Name", modGlobalVariables.DEFAULTDATABASE);
			CountyGrid.Rows = rsData.RecordCount() + 1;
			CountyGrid.Cols = 3;
			CountyGrid.TextMatrix(0, 0, "ID");
			CountyGrid.TextMatrix(0, 1, "County");
			CountyGrid.ColHidden(0, true);
			CountyGrid.ColHidden(COUNTYSTATUSCOL, true);
			//CountyGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			CountyGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, CountyGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			CountyGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1, true);
			CountyGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				CountyGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
				CountyGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			CountyGrid.TopRow = 1;
			CountyGrid.Row = 1;
			CountyGrid.Col = 1;
			clsHistoryClass.Init = this;
		}

		private void DeleteCounty()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (CountyGrid.Row > 1)
				{
					if (MessageBox.Show("Are you sure you wish to delete this County?   " + CountyGrid.TextMatrix(CountyGrid.Row, 1), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						clsHistoryClass.AddAuditHistoryDeleteEntry(ref CountyGrid);
						if (FCConvert.ToDouble(CountyGrid.TextMatrix(CountyGrid.Row, COUNTYSTATUSCOL)) == 1)
						{
							CountyGrid.RemoveItem(CountyGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from DefaultCounties where ID = " + CountyGrid.TextMatrix(CountyGrid.Row, 0), modGlobalVariables.DEFAULTDATABASE);
							CountyGrid.RemoveItem(CountyGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveCounties()
		{
			bool SaveCounties = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				CountyGrid.Row = 0;
				for (intCounter = 1; intCounter <= (CountyGrid.Rows - 1); intCounter++)
				{
					// If Len(Trim(CountyGrid.TextMatrix(intCounter, 1))) <= 0 Then
					// MsgBox "County name must be entered.", vbInformation + vbOKOnly, "TRIO Software"
					// CountyGrid.Row = intCounter
					// CountyGrid.TopRow = intCounter
					// CountyGrid.Col = 1
					// Screen.MousePointer = vbDefault
					// Exit Function
					// End If
					if (FCConvert.ToDouble(CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DefaultCounties (Name) VALUES ('" + CountyGrid.TextMatrix(intCounter, 1) + "')", modGlobalVariables.DEFAULTDATABASE);
						CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DefaultCounties Set Name = '" + fecherFoundation.Strings.Trim(CountyGrid.TextMatrix(intCounter, 1)) + "' where ID = " + CountyGrid.TextMatrix(intCounter, 0), modGlobalVariables.DEFAULTDATABASE);
						CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL, FCConvert.ToString(0));
					}
				}
				LoadCounties();
				TownGrid.ColComboList(2, FCConvert.ToString(LoadGridCellAsCombo("Select * from DefaultCounties", "Name", "ID")));
				SaveCounties = true;
				clsHistoryClass.Compare();
				MessageBox.Show("Save of County information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveCounties;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveCounties;
		}
		// *************************************************
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object LoadGridCellAsCombo(string strSQL, string FieldName = "", string IDField = "")
		{
			object LoadGridCellAsCombo = null;
			int intCounter;
			clsDRWrapper rsCombo = new clsDRWrapper();
			rsCombo.OpenRecordset(strSQL, modGlobalVariables.DEFAULTDATABASE);
			while (!rsCombo.EndOfFile())
			{
				LoadGridCellAsCombo = LoadGridCellAsCombo + "|";
				LoadGridCellAsCombo = LoadGridCellAsCombo + "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
				rsCombo.MoveNext();
			}
			return LoadGridCellAsCombo;
		}
	}
}
