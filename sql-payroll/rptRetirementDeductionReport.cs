//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptRetirementDeductionReport.
	/// </summary>
	public partial class rptRetirementDeductionReport : BaseSectionReport
	{
		public rptRetirementDeductionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Deduction Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptRetirementDeductionReport InstancePtr
		{
			get
			{
				return (rptRetirementDeductionReport)Sys.GetInstance(typeof(rptRetirementDeductionReport));
			}
		}

		protected rptRetirementDeductionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployee?.Dispose();
				rsData?.Dispose();
				rsDeductions?.Dispose();
				rsSum?.Dispose();
                rsData = null;
                rsEmployee = null;
                rsDeductions = null;
                rsSum = null;
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRetirementDeductionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsDeductions = new clsDRWrapper();
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsEmployee = new clsDRWrapper();
		string strEmployeeNumber = "";
		double dblTotalAmount;
		string strDeductionNumber = "";
		bool boolDifferentEmployee;
		int intpage;
		string strCurrentValue = "";
		string strCurrentCompareValue = "";
		string strDisplayName = "";
		double dblGrandTotal;
		bool boolShowGrandTotal;
		bool boolShowReportTotal;
		double dblReportTotal;
		int intSpacer;
		string strWhere = "";
		clsDRWrapper rsSum = new clsDRWrapper();
		double dblField1Total;
		double dblField2Total;
		double dblField3Total;
		double dblField4Total;
		double dblHoursTotal;
		double dblSalaryTotal;
		double dblSubTotalTotal;
		string strHeader = "";
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
        private SSNViewType ssnViewPermission = SSNViewType.None;

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			NextRecord:
			;
			while (!rsData.EndOfFile())
			{
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			txtField1.Text = "0.00";
			txtField2.Text = "0.00";
			txtField3.Text = "0.00";
			txtField4.Text = "0.00";
			txtHours.Text = "0.00";
			txtSalary.Text = "0.00";
			txtSubTotal.Text = "0.00";
			double dblSubtotal;
			dblSubtotal = 0;
			txtName.Text = GetEmployeeDescription(rsData.Get_Fields("EmployeeNumber"));
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 3) != string.Empty)
			{
				rsSum.OpenRecordset("Select sum(DedAmount) as TotalDedAmount from tblCheckDetail Where checkvoid = 0 AND EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' AND DedDeductionNumber = " + frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 3) + " AND deductionrecord = 1" + modGlobalVariables.Statics.gstrCheckListingWhere);
				if (!rsSum.EndOfFile())
				{
					txtField1.Text = Strings.Format(rsSum.Get_Fields("TotalDedAmount"), "0.00");
					if (txtField1.Text == string.Empty)
					{
						txtField1.Text = "0.00";
					}
					else
					{
						dblField1Total += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("TotalDedAmount"), "0.00"));
						dblSubtotal += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("totaldedamount"), "0.00"));
					}
				}
			}
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 4) != string.Empty)
			{
				rsSum.OpenRecordset("Select sum(DedAmount) as TotalDedAmount from tblCheckDetail Where checkvoid = 0 AND EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' AND DedDeductionNumber = " + frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 4) + " AND deductionrecord = 1" + modGlobalVariables.Statics.gstrCheckListingWhere);
				if (!rsSum.EndOfFile())
				{
					txtField2.Text = Strings.Format(rsSum.Get_Fields("TotalDedAmount"), "0.00");
				}
				if (txtField2.Text == string.Empty)
				{
					txtField2.Text = "0.00";
				}
				else
				{
					dblField2Total += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("TotalDedAmount"), "0.00"));
					dblSubtotal += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("totaldedamount"), "0.00"));
				}
			}
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 5) != string.Empty)
			{
				rsSum.OpenRecordset("Select sum(DedAmount) as TotalDedAmount from tblCheckDetail Where checkvoid = 0 AND EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' AND DedDeductionNumber = " + frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 5) + " AND deductionrecord = 1" + modGlobalVariables.Statics.gstrCheckListingWhere);
				// Call rsSum.OpenRecordset("Select sum(MatchAmount) as TotalMatchAmount from tblCheckDetail Where EmployeeNumber = '" & rsData.Fields("EmployeeNumber") & "' AND MatchDeductionNumber = " & frmCustomRetirementDeductions.vsData.TextMatrix(1, 5) & " AND matchrecord = 1" & gstrCheckListingWhere)
				if (!rsSum.EndOfFile())
				{
					txtField3.Text = Strings.Format(rsSum.Get_Fields("TotalDedAmount"), "0.00");
					// txtField3 = Format(rsSum.Fields("TotalMatchAmount"), "0.00")
				}
				if (txtField3.Text == string.Empty)
				{
					txtField3.Text = "0.00";
				}
				else
				{
					dblField3Total += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("TotalDedAmount"), "0.00"));
					// dblField3Total = dblField3Total + Format(rsSum.Fields("TotalMatchAmount"), "0.00")
					dblSubtotal += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("totaldedamount"), "0.00"));
				}
			}
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 6) != string.Empty)
			{
				rsSum.OpenRecordset("Select sum(MatchAmount) as TotalMatchAmount from tblCheckDetail Where checkvoid = 0 AND EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' AND MatchDeductionNumber = " + frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 6) + " AND matchrecord = 1" + modGlobalVariables.Statics.gstrCheckListingWhere);
				if (!rsSum.EndOfFile())
				{
					txtField4.Text = Strings.Format(rsSum.Get_Fields("TotalMatchAmount"), "0.00");
				}
				if (txtField4.Text == string.Empty)
				{
					txtField4.Text = "0.00";
				}
				else
				{
					dblField4Total += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("TotalMatchAmount"), "0.00"));
					dblSubtotal += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("totalmatchamount"), "0.00"));
				}
			}
			dblSubTotalTotal += dblSubtotal;
			txtSubTotal.Text = Strings.Format(dblSubtotal, "#,###,###,##0.00");
			rsSum.OpenRecordset("Select sum(DistGrossPay) as TotalAmount, sum(DistHours) as TotalHours from tblCheckDetail Where checkvoid = 0 AND EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' AND distributionrecord = 1" + modGlobalVariables.Statics.gstrCheckListingWhere);
			if (!rsSum.EndOfFile())
			{
				txtHours.Text = Strings.Format(rsSum.Get_Fields("TotalHours"), "0.00");
				txtSalary.Text = Strings.Format(rsSum.Get_Fields("TotalAmount"), "0.00");
				if (txtHours.Text == string.Empty)
				{
					txtHours.Text = "0.00";
				}
				else
				{
					dblHoursTotal += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("TotalHours"), "0.00"));
				}
				if (txtSalary.Text == string.Empty)
				{
					txtSalary.Text = "0.00";
				}
				else
				{
					dblSalaryTotal += FCConvert.ToDouble(Strings.Format(rsSum.Get_Fields("TotalAmount"), "0.00"));
				}
			}
			if (!rsData.EndOfFile())
				rsData.MoveNext();
			if (txtField1.Text == "0.00" && txtField2.Text == "0.00" && txtField3.Text == "0.00" && txtField4.Text == "0.00" && txtHours.Text == "0.00" && txtSalary.Text == "0.00")
				goto NextRecord;
			eArgs.EOF = false;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetDeductionDescription(int lngDeductionNumber)
		{
			object GetDeductionDescription = null;
			rsDeductions.FindFirstRecord("ID", lngDeductionNumber);
			if (rsDeductions.NoMatch)
			{
				GetDeductionDescription = string.Empty;
			}
			else
			{
				GetDeductionDescription = rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + rsDeductions.Get_Fields("Description");
			}
			return GetDeductionDescription;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private string GetEmployeeDescription(string strEmployeeNumber)
		{
			string GetEmployeeDescription = null;
			rsEmployee.FindFirstRecord("EmployeeNumber", strEmployeeNumber);
			if (rsEmployee.NoMatch)
			{
				GetEmployeeDescription = string.Empty;
			}
			else
            {

                var strSSN = rsEmployee.Get_Fields_String("SSN");
                if (ssnViewPermission == SSNViewType.Masked)
                {
                    strSSN = "***-**-" + strSSN.Right(4);
                }
                else if (ssnViewPermission == SSNViewType.None)
                {
                    strSSN = "***-**-****";
                }
                GetEmployeeDescription = FCConvert.ToString(rsEmployee.Get_Fields("EmployeeNumber")) + Strings.StrDup(10 - FCConvert.ToString(rsEmployee.Get_Fields("EmployeeNumber")).Length, " ") + strSSN + Strings.StrDup(15 - strSSN.Length, " ") + rsEmployee.Get_Fields_String("LastName") + ", " + rsEmployee.Get_Fields_String("FirstName") + " " + rsEmployee.Get_Fields_String("MiddleName");
			}
			return GetEmployeeDescription;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolDifferentEmployee = true;
			boolShowReportTotal = true;
			intSpacer = 1;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			this.Name = "Custom Retirement Deduction Report";
			rsData.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, "TWPY0000.vb1");
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
			rsEmployee.OpenRecordset("Select * from tblEmployeeMaster", "TWPY0000.vb1");
			dblSubTotalTotal = 0;
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			frmCustomRetirementDeductions.InstancePtr.vsData.Select(0, 0);
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 3) != string.Empty)
			{
				strHeader = Strings.Right(FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 3)), FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 3)).Length - 5);
				if (strHeader.Length >= 15)
				{
					lblField1.Text = Strings.Left(strHeader, 12);
				}
				else
				{
					lblField1.Text = strHeader;
				}
			}
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 4) != string.Empty)
			{
				strHeader = Strings.Right(FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 4)), FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 4)).Length - 5);
				if (strHeader.Length >= 15)
				{
					lblField2.Text = Strings.Left(strHeader, 12);
				}
				else
				{
					lblField2.Text = strHeader;
				}
			}
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 5) != string.Empty)
			{
				strHeader = Strings.Right(FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 5)), FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 5)).Length - 5);
				if (strHeader.Length >= 15)
				{
					lblField3.Text = Strings.Left(strHeader, 12);
				}
				else
				{
					lblField3.Text = strHeader;
				}
			}
			if (frmCustomRetirementDeductions.InstancePtr.vsData.TextMatrix(1, 6) != string.Empty)
			{
				strHeader = Strings.Right(FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 6)), FCConvert.ToString(frmCustomRetirementDeductions.InstancePtr.vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, 6)).Length - 5);
				if (strHeader.Length >= 15)
				{
					lblField4.Text = Strings.Left(strHeader, 12);
				}
				else
				{
					lblField4.Text = strHeader;
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtField1Total.Text = Strings.Format(dblField1Total, "#,###,##0.00");
			txtField2Total.Text = Strings.Format(dblField2Total, "#,###,##0.00");
			txtField3Total.Text = Strings.Format(dblField3Total, "#,###,##0.00");
			txtField4Total.Text = Strings.Format(dblField4Total, "#,###,##0.00");
			txtHoursTotal.Text = Strings.Format(dblHoursTotal, "#,###,##0.00");
			txtSalaryTotal.Text = Strings.Format(dblSalaryTotal, "#,###,##0.00");
			txtSubtotalTotal.Text = Strings.Format(dblSubTotalTotal, "#,###,###,##0.00");
		}

		
	}
}
