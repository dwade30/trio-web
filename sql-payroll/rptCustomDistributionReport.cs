//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomDistributionReport.
	/// </summary>
	public partial class rptCustomDistributionReport : BaseSectionReport
	{
		public rptCustomDistributionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Distribution Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomDistributionReport InstancePtr
		{
			get
			{
				return (rptCustomDistributionReport)Sys.GetInstance(typeof(rptCustomDistributionReport));
			}
		}

		protected rptCustomDistributionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsDistributions?.Dispose();
                rsData = null;
                rsDistributions = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomDistributionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsDistributions = new clsDRWrapper();
		clsDRWrapper rsData = new clsDRWrapper();
		string strEmployeeNumber = "";
		double dblTotalAmount;
		string strDistributionNumber = "";
		bool boolDifferentEmployee;
		int intpage;
		string strCurrentValue = "";
		string strCurrentCompareValue = "";
		string strDisplayName = "";
		double dblGrandTotal;
		double dblGrandTotalHours;
		bool boolShowGrandTotal;
		bool boolShowReportTotal;
		double dblReportTotal;
		int intSpacer;
		double dblHours;
		double dblReportTotalHours;
		bool boolSubGroup;
		bool boolShowRate;
		// vbPorter upgrade warning: boolShowDetail As bool	OnWrite(bool, CheckState)
		bool boolShowDetail;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		public void Init(bool boolUseSubGroup = false, bool boolShowPayRate = false)
		{
			boolSubGroup = boolUseSubGroup;
			boolShowRate = boolShowPayRate;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			if (boolUseSubGroup)
			{
				boolShowDetail = true;
			}
			else
			{
				boolShowDetail = 0 != frmCustomDistributionReports.InstancePtr.chkShowDetails.CheckState;
			}
			if (boolShowRate)
			{
				txtPayRate.Visible = true;
				txtPayRateCaption.Visible = true;
				txtGroup.Width -= txtPayRateCaption.Width;
				txtField1.Width -= txtPayRate.Width;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "CustomDistribution");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			NextRecord:
			;
			Detail.Visible = true;
			lblAdjustment.Text = "";
			txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Regular);
			txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Regular);
			txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Regular);
			while (!rsData.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				if (modGlobalVariables.Statics.gboolSummaryDistCustomReport)
				{
					if (boolShowDetail)
					{
						if (!boolSubGroup || modGlobalVariables.Statics.gstrDistributionSummaryType != "Distribution")
						{
							txtField1.Text = GetDistributionDescription(strDistributionNumber);
						}
						else
						{
							txtField1.Text = strDisplayName;
						}
						txtField2.Text = Strings.Format(dblHours, "0.00");
						txtField3.Text = Strings.Format(dblTotalAmount, "0.00");
						txtField4.Text = "";
						txtPayRate.Text = "";
					}
					else
					{
						txtField1.Text = "";
						txtField2.Text = "";
						txtField3.Text = "";
						txtField4.Text = "";
						txtPayRate.Text = "";
						Detail.Visible = false;
					}
					eArgs.EOF = false;
					modGlobalVariables.Statics.gboolSummaryDistCustomReport = false;
					return;
				}
				else
				{
					if (boolShowGrandTotal)
					{
						boolShowGrandTotal = false;
						txtField1.Text = "Total";
						txtField2.Text = Strings.Format(dblGrandTotalHours, "#,##0.00");
						txtField3.Text = Strings.Format(dblGrandTotal, "#,##0.00");
						txtField4.Text = "";
						txtPayRate.Text = "";
						txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Bold);
						txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
						txtField3.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
						dblGrandTotal = 0;
						dblGrandTotalHours = 0;
						eArgs.EOF = false;
						return;
					}
					else
					{
						if (boolShowReportTotal)
						{
							if (intSpacer < 3)
							{
								txtField1.Text = "";
								txtField2.Text = "";
								txtField3.Text = "";
								txtField4.Text = "";
								txtPayRate.Text = "";
								intSpacer += 1;
								eArgs.EOF = false;
								return;
							}
							else
							{
								boolShowReportTotal = false;
								txtField1.Text = "Report Total";
								txtField2.Text = Strings.Format(dblReportTotalHours, "#,##0.00");
								txtField3.Text = Strings.Format(dblReportTotal, "#,##0.00");
								txtField4.Text = "";
								txtPayRate.Text = "";
								txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Bold);
								txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
								txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Bold);
								dblGrandTotal = 0;
								eArgs.EOF = false;
								return;
							}
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
			}
			if (modGlobalVariables.Statics.gstrDistributionSummaryType == "ID" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Name")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
				// ElseIf gstrDistributionSummaryType = "Name" Then
				// strCurrentValue = rsData.Fields("firstname") & " " & rsData.Fields("lastname")
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory"));
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "PayDate")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate"));
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary")
			{
				strCurrentValue = FCConvert.ToString(GetDeptDiv(rsData.Get_Fields_String("DistAccountNumber")));
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Account")
			{
				strCurrentValue = rsData.Get_Fields_String("DistAccountNumber");
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
			}
			if (modGlobalVariables.Statics.gstrDistributionSummaryType == "ID" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Name")
			{
				strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory"));
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
			{
				if (!boolSubGroup)
				{
					strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
				}
				else
				{
					strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
				}
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "PayDate")
			{
				strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory"));
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Account")
			{
				strCurrentCompareValue = rsData.Get_Fields_Int32("DistPayCategory") + "   " + rsData.Get_Fields_String("DistAccountNumber");
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary")
			{
				strCurrentCompareValue = rsData.Get_Fields_Int32("DistPayCategory") + "   " + FCConvert.ToString(GetDeptDiv(rsData.Get_Fields_String("DistAccountNumber")));
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
			{
				strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory"));
			}
			if (strEmployeeNumber != strCurrentValue)
			{
				if (modGlobalVariables.Statics.gboolSummaryDistCustomReport && !boolDifferentEmployee)
				{
					if (modGlobalVariables.Statics.gstrDistributionSummaryType == "ID" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Name" || modGlobalVariables.Statics.gstrDistributionSummaryType == "PayDate" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Account" || modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary" || modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
					{
						if (boolShowDetail)
						{
							txtField1.Text = GetDistributionDescription(strDistributionNumber);
							txtField2.Text = Strings.Format(dblHours, "#,##0.00");
							txtField3.Text = Strings.Format(dblTotalAmount, "#,##0.00");
							txtField4.Text = "";
							txtPayRate.Text = "";
						}
						else
						{
							txtField1.Text = "";
							txtField2.Text = "";
							txtField3.Text = "";
							txtField4.Text = "";
							txtPayRate.Text = "";
							Detail.Visible = false;
						}
					}
					else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
					{
						if (boolShowDetail)
						{
							txtField1.Text = strDisplayName;
							txtField2.Text = Strings.Format(dblHours, "#,##0.00");
							txtField3.Text = Strings.Format(dblTotalAmount, "#,##0.00");
							txtField4.Text = "";
							txtPayRate.Text = "";
						}
						else
						{
							txtField1.Text = "";
							txtField2.Text = "";
							txtField3.Text = "";
							txtField4.Text = "";
							Detail.Visible = false;
						}
					}
					dblHours = 0;
					dblTotalAmount = 0;
					boolDifferentEmployee = true;
					strDistributionNumber = strCurrentCompareValue;
					if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution" && boolSubGroup)
					{
						strDisplayName = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length > 5)
						{
							strDisplayName = rsData.Get_Fields("EmployeeNumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
						}
						else
						{
							strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
						}
					}
					eArgs.EOF = false;
					return;
				}
				else
				{
					if (boolShowGrandTotal)
					{
						boolShowGrandTotal = false;
						txtField1.Text = "Total";
						txtField2.Text = Strings.Format(dblGrandTotalHours, "#,##0.00");
						txtField3.Text = Strings.Format(dblGrandTotal, "#,##0.00");
						txtField4.Text = "";
						txtPayRate.Text = "";
						txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Bold);
						txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
						txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Bold);
						dblGrandTotal = 0;
						dblGrandTotalHours = 0;
						eArgs.EOF = false;
						return;
					}
					else
					{
						boolShowGrandTotal = true;
						boolDifferentEmployee = false;
						if (modGlobalVariables.Statics.gstrDistributionSummaryType == "ID" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Name")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
							this.Fields["grpHeader"].Value = rsData.Get_Fields("EmployeeNumber");
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblCheckDetail.EmployeeNumber"))).Length >= 6)
							{
								this.txtGroup.Text = rsData.Get_Fields("EmployeeNumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
							}
							else
							{
								this.txtGroup.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
							}
							// ElseIf gstrDistributionSummaryType = "Name" Then
							// strEmployeeNumber = rsData.Fields("firstname") & " " & rsData.Fields("lastname")
							// Me.GroupHeader1.DataField = rsData.Fields("firstname") & " " & rsData.Fields("lastname")
							// If Len(Trim(rsData.Fields("firstname") & " " & rsData.Fields("lastname"))) >= 6 Then
							// Me.txtGroup = rsData.Fields("FirstName") & " " & rsData.Fields("LastName")
							// Else
							// Me.txtGroup = rsData.Fields("FirstName") & " " & rsData.Fields("LastName")
							// End If
						}
						else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory"));
							this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory"));
							this.txtGroup.Text = GetDistributionDescription(FCConvert.ToString(Conversion.Val(strEmployeeNumber)));
						}
						else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "PayDate")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate"));
							this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate"));
							this.txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
						}
						else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
							this.Fields["grpHeader"].Value = rsData.Get_Fields("DeptDiv");
							this.txtGroup.Text = rsData.Get_Fields_String("DeptDiv");
						}
						else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Account")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
							this.Fields["grpHeader"].Value = rsData.Get_Fields_String("DistAccountNumber");
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length >= 6)
							{
								// Me.txtGroup = rsData.Fields("tblCheckDetail.EmployeeNumber") & rsData.Fields("FirstName") & " " & rsData.Fields("LastName") & "   " & rsData.Fields("DistAccountNumber")
								this.txtGroup.Text = rsData.Get_Fields_String("DistAccountNumber");
							}
							else
							{
								// Me.txtGroup = rsData.Fields("tblCheckDetail.EmployeeNumber") & String(6 - Len(Trim(rsData.Fields("tblCheckDetail.EmployeeNumber"))), " ") & rsData.Fields("FirstName") & " " & rsData.Fields("LastName") & "   " & rsData.Fields("DistAccountNumber")
								this.txtGroup.Text = rsData.Get_Fields_String("DistAccountNumber");
							}
						}
						else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary")
						{
							strEmployeeNumber = FCConvert.ToString(GetDeptDiv(rsData.Get_Fields_String("DistAccountNumber")));
							this.Fields["grpHeader"].Value = FCConvert.ToString(GetDeptDiv(rsData.Get_Fields_String("DistAccountNumber")));
							this.txtGroup.Text = FCConvert.ToString(GetDeptDiv(rsData.Get_Fields_String("DistAccountNumber"))) + "  " + modDavesSweetCode.GetAccountDescription(rsData.Get_Fields_String("DistAccountNumber"));
						}
						Detail.Visible = false;
						eArgs.EOF = false;
						return;
					}
				}
			}
			else
			{
				if (modGlobalVariables.Statics.gboolSummaryDistCustomReport)
				{
					if (strDistributionNumber == strCurrentCompareValue)
					{
						dblHours += Conversion.Val(rsData.Get_Fields("DistHours"));
						dblTotalAmount += Conversion.Val(rsData.Get_Fields("DistGrossPay"));
						dblGrandTotal += Conversion.Val(rsData.Get_Fields("DistGrossPay"));
						dblReportTotal += Conversion.Val(rsData.Get_Fields("DistGrossPay"));
						dblGrandTotalHours += Conversion.Val(rsData.Get_Fields("DistHours"));
						dblReportTotalHours += Conversion.Val(rsData.Get_Fields("DistHours"));
						if (!rsData.EndOfFile())
							rsData.MoveNext();
						goto NextRecord;
					}
					else
					{
						if (strDistributionNumber == "")
						{
							// this is the first time
							strDistributionNumber = strCurrentCompareValue;
							if (modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary")
							{
								strDisplayName = rsData.Get_Fields_String("DistAccountNumber") + "  " + modDavesSweetCode.GetAccountDescription(rsData.Get_Fields_String("DistAccountNumber"));
							}
							else if (boolSubGroup && modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
							{
								strDisplayName = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
							}
							else
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length > 5)
								{
									strDisplayName = rsData.Get_Fields("EmployeeNumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
								}
								else
								{
									strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
								}
							}
							goto NextRecord;
						}
						else
						{
							if (modGlobalVariables.Statics.gstrDistributionSummaryType == "ID" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Name" || modGlobalVariables.Statics.gstrDistributionSummaryType == "PayDate" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Account" || modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary" || modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
							{
								if (boolShowDetail)
								{
									txtField1.Text = GetDistributionDescription(strDistributionNumber);
									txtField2.Text = Strings.Format(dblHours, "#,##0.00");
									txtField3.Text = Strings.Format(dblTotalAmount, "#,##0.00");
									txtField4.Text = "";
									txtPayRate.Text = "";
								}
								else
								{
									txtField1.Text = "";
									txtField2.Text = "";
									txtField3.Text = "";
									txtField4.Text = "";
									txtPayRate.Text = "";
									Detail.Visible = false;
								}
							}
							else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
							{
								if (boolShowDetail)
								{
									txtField1.Text = strDisplayName;
									txtField2.Text = Strings.Format(dblHours, "#,##0.00");
									txtField3.Text = Strings.Format(dblTotalAmount, "#,##0.00");
									txtField4.Text = "";
									txtPayRate.Text = "";
								}
								else
								{
									txtField1.Text = "";
									txtField2.Text = "";
									txtField3.Text = "";
									txtField4.Text = "";
									txtPayRate.Text = "";
									Detail.Visible = false;
								}
							}
							dblTotalAmount = 0;
							dblHours = 0;
							strDistributionNumber = strCurrentCompareValue;
							if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution" && boolSubGroup)
							{
								strDisplayName = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
							}
							else
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length < 7)
								{
									strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
								}
								else
								{
									strDisplayName = rsData.Get_Fields("employeenumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
								}
							}
							eArgs.EOF = false;
							return;
						}
					}
				}
			}
			if (modGlobalVariables.Statics.gstrDistributionSummaryType == "ID" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Name" || modGlobalVariables.Statics.gstrDistributionSummaryType == "Account" || modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
			{
				txtField1.Text = GetDistributionDescription(FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory")));
				if (fecherFoundation.Strings.Trim(txtField1.Text).Length > 19)
				{
					txtField1.Text = Strings.Left(txtField1.Text, 19);
				}
				else
				{
					txtField1.Text = txtField1.Text + Strings.StrDup(20 - fecherFoundation.Strings.Trim(txtField1.Text).Length, " ");
				}
				if (modGlobalVariables.Statics.gstrDistributionSummaryType == "DeptDiv")
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length < 7)
					{
						strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
					}
					else
					{
						strDisplayName = rsData.Get_Fields("EmployeeNumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
					}
					txtField1.Text = txtField1.Text + "  " + strDisplayName;
				}
				// txtPayRate.Text = Format(Val(rsData.Fields("basepayrate")), "#,##0.00")
				txtPayRate.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("distpayrate")), "#,##0.00");
				txtField2.Text = Strings.Format(rsData.Get_Fields("DistHours"), "#,##0.0");
				txtField3.Text = Strings.Format(rsData.Get_Fields("DistGrossPay"), "#,##0.00");
				txtField4.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("adjustRECORD")))
				{
					lblAdjustment.Text = "Adj";
				}
				else
				{
					lblAdjustment.Text = "";
				}
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary")
			{
				txtField1.Text = GetDistributionDescription(FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory")));
				if (fecherFoundation.Strings.Trim(txtField1.Text).Length > 19)
				{
					txtField1.Text = Strings.Left(txtField1.Text, 19);
				}
				else
				{
					txtField1.Text = txtField1.Text + Strings.StrDup(20 - fecherFoundation.Strings.Trim(txtField1.Text).Length, " ");
				}
				txtField2.Text = Strings.Format(rsData.Get_Fields("DistHours"), "#,##0.0");
				txtField3.Text = Strings.Format(rsData.Get_Fields("DistGrossPay"), "#,##0.00");
				txtField4.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("adjustRECORD")))
				{
					lblAdjustment.Text = "Adj";
				}
				else
				{
					lblAdjustment.Text = "";
				}
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Distribution")
			{
				if (!boolSubGroup)
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length < 7)
					{
						txtField1.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
					}
					else
					{
						txtField1.Text = rsData.Get_Fields("EmployeeNumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
					}
				}
				else
				{
					txtField1.Text = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
				}
				txtField2.Text = Strings.Format(rsData.Get_Fields("DistHours"), "#,##0.0");
				txtField3.Text = Strings.Format(rsData.Get_Fields("DistGrossPay"), "#,##0.00");
				txtField4.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
				// txtPayRate.Text = Format(Val(rsData.Fields("basepayrate")), "#,##0.00")
				txtPayRate.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("distpayrate")), "#,##0.00");
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("adjustRECORD")))
				{
					lblAdjustment.Text = "Adj";
				}
				else
				{
					lblAdjustment.Text = "";
				}
			}
			else if (modGlobalVariables.Statics.gstrDistributionSummaryType == "PayDate")
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length < 7)
				{
					txtField1.Text = FCConvert.ToString(GetDistributionDescription(FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory")))) + " ----- " + rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
				}
				else
				{
					txtField1.Text = FCConvert.ToString(GetDistributionDescription(FCConvert.ToString(rsData.Get_Fields_Int32("DistPayCategory")))) + " ----- " + rsData.Get_Fields("EmployeeNumber") + " " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
				}
				txtField2.Text = Strings.Format(rsData.Get_Fields("DistHours"), "#,##0.0");
				txtField3.Text = Strings.Format(rsData.Get_Fields("DistGrossPay"), "#,##0.00");
				txtField4.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
				// txtPayRate.Text = Format(Val(rsData.Fields("basepayrate")), "#,##0.00")
				txtPayRate.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("distpayrate")), "#,##0.00");
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("adjustRECORD")))
				{
					lblAdjustment.Text = "Adj";
				}
				else
				{
					lblAdjustment.Text = "";
				}
			}
			dblGrandTotalHours += Conversion.Val(rsData.Get_Fields("DistHours"));
			dblGrandTotal += Conversion.Val(rsData.Get_Fields("DistGrossPay"));
			dblReportTotal += Conversion.Val(rsData.Get_Fields("DistGrossPay"));
			dblReportTotalHours += Conversion.Val(rsData.Get_Fields("DistHours"));
			if (!rsData.EndOfFile())
				rsData.MoveNext();
			eArgs.EOF = false;
		}
		// vbPorter upgrade warning: lngDistributionNumber As string	OnWrite(string, double)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private string GetDistributionDescription(string lngDistributionNumber)
		{
			string GetDistributionDescription = null;
			if (modGlobalVariables.Statics.gstrDistributionSummaryType == "Account" || modGlobalVariables.Statics.gstrDistributionSummaryType == "AccountSummary")
			{
				// vbPorter upgrade warning: strTemp As object	OnWrite(string())
				string[] strTemp = null;
				strTemp = Strings.Split(lngDistributionNumber, " ", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strTemp, 1) < 1)
				{
					if (Information.IsNumeric(lngDistributionNumber))
					{
						rsDistributions.FindFirstRecord("ID", lngDistributionNumber);
						if (rsDistributions.NoMatch)
						{
							GetDistributionDescription = string.Empty;
						}
						else
						{
							GetDistributionDescription = rsDistributions.Get_Fields_Int32("CategoryNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("CategoryNumber"))).Length, " ") + rsDistributions.Get_Fields("Description");
						}
					}
					else
					{
						GetDistributionDescription = string.Empty;
					}
				}
				else
				{
					rsDistributions.FindFirstRecord("ID", FCConvert.ToInt32(strTemp[0]));
					if (rsDistributions.NoMatch)
					{
						GetDistributionDescription = string.Empty;
					}
					else
					{
						GetDistributionDescription = rsDistributions.Get_Fields_Int32("CategoryNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("CategoryNumber"))).Length, " ") + rsDistributions.Get_Fields("Description");
					}
				}
			}
			else
			{
				rsDistributions.FindFirstRecord("ID", Conversion.Val(lngDistributionNumber));
				if (rsDistributions.NoMatch)
				{
					GetDistributionDescription = string.Empty;
				}
				else
				{
					GetDistributionDescription = rsDistributions.Get_Fields_Int32("CategoryNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("CategoryNumber"))).Length, " ") + rsDistributions.Get_Fields("Description");
				}
			}
			return GetDistributionDescription;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolDifferentEmployee = true;
			boolShowReportTotal = true;
			intSpacer = 1;
			rsData.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, "TWPY0000.vb1");
			rsDistributions.OpenRecordset("Select * from tblPayCategories", "TWPY0000.vb1");
			if (modGlobalVariables.Statics.gboolSummaryDistCustomReport)
				Field3.Visible = false;
			// boolShowDetail = frmCustomDistributionReports.chkShowDetails
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetDeptDiv(string strDistributionNumber)
		{
			object GetDeptDiv = null;
			if (Strings.Left(strDistributionNumber, 1) == "E")
			{
				GetDeptDiv = Strings.Left(strDistributionNumber, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)) + 3);
				GetDeptDiv = GetDeptDiv + Strings.Mid(strDistributionNumber, 6, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
			}
			else
			{
			}
			return GetDeptDiv;
		}

		

		private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
