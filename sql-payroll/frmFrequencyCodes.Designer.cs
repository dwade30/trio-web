//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmFrequencyCodes.
	/// </summary>
	partial class frmFrequencyCodes
	{
		public FCGrid vsFrequencyCodes;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuEnable;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsFrequencyCodes = new fecherFoundation.FCGrid();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdRefresh = new fecherFoundation.FCButton();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEnable = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdEnable = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsFrequencyCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEnable)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 367);
			this.BottomPanel.Size = new System.Drawing.Size(638, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsFrequencyCodes);
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Size = new System.Drawing.Size(638, 307);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdEnable);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdRefresh);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(638, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdRefresh, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEnable, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(206, 30);
			this.HeaderText.Text = "Frequency Codes";
			// 
			// vsFrequencyCodes
			// 
			this.vsFrequencyCodes.AllowSelection = false;
			this.vsFrequencyCodes.AllowUserToResizeColumns = false;
			this.vsFrequencyCodes.AllowUserToResizeRows = false;
			this.vsFrequencyCodes.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsFrequencyCodes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsFrequencyCodes.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.BackColorBkg = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.BackColorFixed = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.BackColorSel = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsFrequencyCodes.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsFrequencyCodes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsFrequencyCodes.ColumnHeadersHeight = 30;
			this.vsFrequencyCodes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsFrequencyCodes.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsFrequencyCodes.DragIcon = null;
			this.vsFrequencyCodes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsFrequencyCodes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsFrequencyCodes.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.FrozenCols = 0;
			this.vsFrequencyCodes.GridColor = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.GridColorFixed = System.Drawing.Color.Empty;
			this.vsFrequencyCodes.Location = new System.Drawing.Point(30, 30);
			this.vsFrequencyCodes.Name = "vsFrequencyCodes";
			this.vsFrequencyCodes.OutlineCol = 0;
			this.vsFrequencyCodes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsFrequencyCodes.RowHeightMin = 0;
			this.vsFrequencyCodes.Rows = 50;
			this.vsFrequencyCodes.ScrollTipText = null;
			this.vsFrequencyCodes.ShowColumnVisibilityMenu = false;
			this.vsFrequencyCodes.Size = new System.Drawing.Size(580, 268);
			this.vsFrequencyCodes.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsFrequencyCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsFrequencyCodes.TabIndex = 0;
			this.vsFrequencyCodes.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsFrequencyCodes_KeyDownEdit);
			this.vsFrequencyCodes.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsFrequencyCodes_AfterEdit);
			this.vsFrequencyCodes.CurrentCellChanged += new System.EventHandler(this.vsFrequencyCodes_RowColChange);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Enabled = false;
			this.cmdPrint.Location = new System.Drawing.Point(504, 81);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(50, 24);
			this.cmdPrint.TabIndex = 5;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Enabled = false;
			this.cmdSave.Location = new System.Drawing.Point(271, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Enabled = false;
			this.cmdDelete.Location = new System.Drawing.Point(316, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(58, 24);
			this.cmdDelete.TabIndex = 3;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Enabled = false;
			this.cmdNew.Location = new System.Drawing.Point(262, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(48, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdRefresh
			// 
			this.cmdRefresh.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdRefresh.AppearanceKey = "toolbarButton";
			this.cmdRefresh.Enabled = false;
			this.cmdRefresh.Location = new System.Drawing.Point(380, 29);
			this.cmdRefresh.Name = "cmdRefresh";
			this.cmdRefresh.Size = new System.Drawing.Size(66, 24);
			this.cmdRefresh.TabIndex = 4;
			this.cmdRefresh.Text = "Refresh";
			this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP1,
            this.mnuRefresh,
            this.mnuEnable,
            this.mnuSP2,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP3,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP4,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 2;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuRefresh
			// 
			this.mnuRefresh.Index = 3;
			this.mnuRefresh.Name = "mnuRefresh";
			this.mnuRefresh.Text = "Refresh";
			this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
			// 
			// mnuEnable
			// 
			this.mnuEnable.Index = 4;
			this.mnuEnable.Name = "mnuEnable";
			this.mnuEnable.Text = "Enable Grid for Editing";
			this.mnuEnable.Click += new System.EventHandler(this.mnuEnable_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 5;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Enabled = false;
			this.mnuPrint.Index = 6;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Enabled = false;
			this.mnuPrintPreview.Index = 7;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print/Preview";
			// 
			// mnuSP3
			// 
			this.mnuSP3.Index = 8;
			this.mnuSP3.Name = "mnuSP3";
			this.mnuSP3.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 9;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                           ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 10;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit               ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP4
			// 
			this.mnuSP4.Index = 11;
			this.mnuSP4.Name = "mnuSP4";
			this.mnuSP4.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 12;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdEnable
			// 
			this.cmdEnable.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEnable.AppearanceKey = "toolbarButton";
			this.cmdEnable.Location = new System.Drawing.Point(452, 29);
			this.cmdEnable.Name = "cmdEnable";
			this.cmdEnable.Size = new System.Drawing.Size(158, 24);
			this.cmdEnable.TabIndex = 6;
			this.cmdEnable.Text = "Enable Grid for Editing";
			this.cmdEnable.Click += new System.EventHandler(this.mnuEnable_Click);
			// 
			// frmFrequencyCodes
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(638, 475);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmFrequencyCodes";
			this.Text = "Frequency Codes";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmFrequencyCodes_Load);
			this.Activated += new System.EventHandler(this.frmFrequencyCodes_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFrequencyCodes_KeyPress);
			this.Resize += new System.EventHandler(this.frmFrequencyCodes_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsFrequencyCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEnable)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdEnable;
	}
}