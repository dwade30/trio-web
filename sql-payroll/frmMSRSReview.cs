//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmMSRSReview : BaseForm
	{
		public frmMSRSReview()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMSRSReview InstancePtr
		{
			get
			{
				return (frmMSRSReview)Sys.GetInstance(typeof(frmMSRSReview));
			}
		}

		protected frmMSRSReview _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 02/08/2006
		// ********************************************************
		// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string)
		DateTime dtStartDate;
		DateTime dtEndDate;
		const int cnstgridcolID = 1;
		const int CNSTGridColName = 0;
		const int CNSTGRIDCOLEMPLOYEENUMBER = 2;
		const int CNSTGRIDCOLCONTRIBUTION = 3;
		const int CNSTGRIDCOLEARNABLECOMP = 4;
		const int CNSTGRIDCOLPLD = 5;
		const int CNSTGRIDCOLSCHOOLPLD = 6;
		const int CNSTGRIDCOLTEACHER = 7;
		const int CNSTGRIDCOLOTHER = 8;
		const int CNSTGRIDCOLREPORT = 9;
		const int CNSTGRIDCOLPOSITION = 10;
		const int CNSTGRIDCOLSTATUS = 11;
		const int CNSTGRIDCOLPARENTROW = 12;
		const int CNSTGRIDCOLFEDCOMPAMOUNT = 13;
		const int CNSTGRIDCOLFEDCOMPDOLLARPERCENT = 14;

		public void Init(ref DateTime dtPayDate)
		{
			dtEndDate = dtPayDate;
			dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(dtEndDate.Month) + "/1/" + FCConvert.ToString(dtEndDate.Year));
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmMSRSReview_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMSRSReview_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMSRSReview properties;
			//frmMSRSReview.FillStyle	= 0;
			//frmMSRSReview.ScaleWidth	= 10560;
			//frmMSRSReview.ScaleHeight	= 7485;
			//frmMSRSReview.LinkTopic	= "Form2";
			//frmMSRSReview.LockControls	= -1  'True;
			//frmMSRSReview.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//this.WidthOriginal = MDIParent.InstancePtr.WidthOriginal - 50;
			this.LeftOriginal = 0;
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void frmMSRSReview_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int x;
			double dblPld;
			double dblSchoolPLD;
			double dblTeacher;
			double dblTemp;
			if (Grid.Row < 1)
				return;
			if (Grid.RowOutlineLevel(Grid.Row) == 1)
			{
				Grid.RowData(Grid.Row, true);
				// recalc totals for the employee
				RecalcEmployeeTotals(FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLPARENTROW))));
			}
		}
		// vbPorter upgrade warning: lngParentRow As int	OnWriteFCConvert.ToDouble(
		private void RecalcEmployeeTotals(int lngParentRow)
		{
			int lngRow;
			double dblPld;
			double dblSchoolPLD;
			double dblTeacher;
			double dblOther = 0;
			double dblTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngParentRow == Grid.Rows - 1 || lngParentRow < 1)
					return;
				lngRow = lngParentRow + 1;
				dblPld = 0;
				dblSchoolPLD = 0;
				dblTeacher = 0;
				do
				{
					dblTemp = 0;
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPARENTROW)) == lngParentRow)
					{
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPLD)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLPLD));
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHOOLPLD)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHOOLPLD));
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTEACHER)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLTEACHER));
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER));
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPLD, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHOOLPLD, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTEACHER, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, "");
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT)) == modCoreysSweeterCode.CNSTMSRSFEDCOMPPERCENT)
						{
							if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT)) > 100)
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT, FCConvert.ToString(100));
							}
						}
						else
						{
							if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT)) > dblTemp)
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT, FCConvert.ToString(dblTemp));
							}
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT)) == modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD)
						{
							dblPld += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLPLD, Strings.Format(dblTemp, "#,###,##0.00"));
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT)) == modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD)
						{
							dblSchoolPLD += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHOOLPLD, Strings.Format(dblTemp, "#,###,##0.00"));
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT)) == modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER)
						{
							dblTeacher += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTEACHER, Strings.Format(dblTemp, "#,###,##0.00"));
						}
						else
						{
							dblOther += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, Strings.Format(dblTemp, "#,###,##0.00"));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLPOSITION, "N");
							Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS, "");
						}
						lngRow += 1;
					}
					else
					{
						// terminate do loop
						lngRow = Grid.Rows;
					}
				}
				while (!(lngRow >= Grid.Rows));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLPLD, Strings.Format(dblPld, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLSCHOOLPLD, Strings.Format(dblSchoolPLD, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLTEACHER, Strings.Format(dblTeacher, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLOTHER, Strings.Format(dblOther, "#,###,##0.00"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In RecalcEmployeeTotals", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Grid_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			if (e.NewRow == 0)
				return;
			if (Grid.RowOutlineLevel(e.NewRow) == 1)
			{
				switch (e.NewCol)
				{
					case CNSTGRIDCOLPOSITION:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							Grid.ColComboList(CNSTGRIDCOLPOSITION, FCConvert.ToString(Grid.RowData(FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLPARENTROW)))));
							break;
						}
					case CNSTGRIDCOLREPORT:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
					case CNSTGRIDCOLSTATUS:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
					case CNSTGRIDCOLFEDCOMPAMOUNT:
					case CNSTGRIDCOLFEDCOMPDOLLARPERCENT:
						{
							// If Grid.TextMatrix(NewRow, CNSTGRIDCOLREPORT) = CNSTMSRSREPORTTYPETEACHER Then
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							// Else
							// Grid.Editable = flexEDNone
							// End If
							break;
						}
					default:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			clsDRWrapper clsDetails = new clsDRWrapper();
			int lngParentRow = 0;
			double dblTemp = 0;
			string strList = "";
			int RetirementCode = 0;
			string strWhere;
			string strTemp = "";
			int lngRow;
			double dblTeacher = 0;
			double dblPld = 0;
			double dblSchoolPLD = 0;
			double dblOther = 0;
			clsDRWrapper clsCat = new clsDRWrapper();
			Grid.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;
			strSQL = "Select * from tblPayCategories";
			clsCat.OpenRecordset(strSQL, "twpy0000.vb1");
			strSQL = "select * from tblDefaultDeductions where type = 'R'";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (clsLoad.RecordCount() == 1)
				{
					RetirementCode = FCConvert.ToInt16(clsLoad.Get_Fields("deductionid"));
				}
				else
				{
					RetirementCode = 0;
				}
			}
			else
			{
				// indicate no codes
				RetirementCode = -1;
			}
			switch (RetirementCode)
			{
				case -1:
					{
						strTemp = " and deddeductionnumber = - 10 ";
						break;
					}
				case 0:
					{
						strTemp = " and (deddeductionnumber = " + clsLoad.Get_Fields("deductionid");
						clsLoad.MoveNext();
						while (!clsLoad.EndOfFile())
						{
							strTemp += " or deddeductionnumber = " + clsLoad.Get_Fields("deductionid");
							clsLoad.MoveNext();
						}
						strTemp += ")";
						break;
					}
				default:
					{
						strTemp = " and deddeductionnumber = " + FCConvert.ToString(RetirementCode) + " ";
						break;
					}
			}
			//end switch
			strWhere = strTemp;
			Grid.Rows = 1;
			// strSQL = "select * from tblmiscupdate inner join (tblemployeemaster inner join (select employeenumber from tblcheckdetail where paydate between '" & dtStartDate & "' and '" & dtEndDate & "' group by employeenumber) as tbl1 on (tbl1.employeenumber = tblemployeemaster.employeenumber)) on (tblemployeemaster.employeenumber = tblmiscupdate.employeenumber) where include order by lastname,firstname"
			strSQL = "select * from tblmiscupdate inner join (tblemployeemaster inner join (select employeenumber from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' group by employeenumber) as tbl1 on (tbl1.employeenumber = tblemployeemaster.employeenumber)) on (tblemployeemaster.employeenumber = tblmiscupdate.employeenumber) order by lastname,firstname";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngParentRow = Grid.Rows - 1;
				Grid.RowOutlineLevel(lngParentRow, 0);
				Grid.IsSubtotal(lngParentRow, true);
				Grid.TextMatrix(lngParentRow, CNSTGridColName, clsLoad.Get_Fields("Lastname") + ", " + clsLoad.Get_Fields("firstname"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLEMPLOYEENUMBER, FCConvert.ToString(clsLoad.Get_Fields("employeenumber")));
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngParentRow, 0, lngParentRow, Grid.Cols - 1, Information.RGB(200, 255, 200));
				strList = "N|";
				strSQL = "select position from tblmsrsdistributiondetail where employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "' group by position";
				clsDetails.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsDetails.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsDetails.Get_Fields("position"))) != string.Empty)
					{
						strList += clsDetails.Get_Fields("position") + "|";
					}
					clsDetails.MoveNext();
				}
				strList = Strings.Mid(strList, 1, strList.Length - 1);
				Grid.RowData(lngParentRow, strList);
				dblTemp = 0;
				strSQL = "select sum(dedamount) as dedsum from tblcheckdetail where employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "' and deductionrecord = 1 and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 " + strWhere;
				clsDetails.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!clsDetails.EndOfFile())
				{
					dblTemp = Conversion.Val(clsDetails.Get_Fields("dedsum"));
				}
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLCONTRIBUTION, Strings.Format(dblTemp, "#,###,##0.00"));
				// now add details
				dblPld = 0;
				dblSchoolPLD = 0;
				dblTeacher = 0;
				dblOther = 0;
				strSQL = "select * from tblcheckdetail where employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "' and checkvoid = 0 and (distributionrecord = 1 or msrsadjustrecord = 1) and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' order by paydate";
				clsDetails.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsDetails.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.RowOutlineLevel(lngRow, 1);
					Grid.IsSubtotal(lngRow, false);
					Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(clsDetails.Get_Fields("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS, FCConvert.ToString(clsDetails.Get_Fields("statuscode")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPOSITION, FCConvert.ToString(clsDetails.Get_Fields("distm")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPARENTROW, FCConvert.ToString(lngParentRow));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT, FCConvert.ToString(clsDetails.Get_Fields("fedcompamount")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT, FCConvert.ToString(Conversion.Val(clsDetails.Get_Fields("FEDCOMPDOLLARPERCENT"))));
					if (!(clsCat.EndOfFile() && clsCat.BeginningOfFile()))
					{
						if (clsCat.FindFirstRecord("ID", Conversion.Val(clsDetails.Get_Fields("distpaycategory"))))
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLEARNABLECOMP, FCConvert.ToString(clsCat.Get_Fields_String("description")));
						}
					}
					if (!FCConvert.ToBoolean(clsDetails.Get_Fields_Boolean("msrsadjustrecord")))
					{
						Grid.TextMatrix(lngRow, CNSTGridColName, FCConvert.ToString(clsDetails.Get_Fields("paydate")));
					}
					else
					{
						Grid.TextMatrix(lngRow, CNSTGridColName, clsDetails.Get_Fields("paydate") + " Adjustment");
					}
					if (Conversion.Val(clsDetails.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD)
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblPld += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPLD, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT, FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD));
					}
					else if (Conversion.Val(clsDetails.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD)
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblSchoolPLD += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHOOLPLD, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT, FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD));
					}
					else if (Conversion.Val(clsDetails.Get_Fields("reporttype")) == modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER)
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblTeacher += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTEACHER, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT, FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER));
					}
					else
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblOther += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT, FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE));
					}
					Grid.RowData(lngRow, false);
					clsDetails.MoveNext();
				}
				dblTemp = dblPld + dblSchoolPLD + dblTeacher + dblOther;
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLEARNABLECOMP, Strings.Format(dblTemp, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLPLD, Strings.Format(dblPld, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLSCHOOLPLD, Strings.Format(dblSchoolPLD, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLTEACHER, Strings.Format(dblTeacher, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLOTHER, Strings.Format(dblOther, "#,###,##0.00"));
				clsLoad.MoveNext();
			}
			Grid.Row = 0;
			Grid.Outline(0);
            modColorScheme.ColorGrid(Grid);
		}

		private void SetupGrid()
		{
			string strTemp;
			strTemp = "#0; |#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE) + ";None|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD) + ";PLD|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD) + ";School PLD|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER) + ";Teacher";
			strTemp = "#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE) + ";None|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD) + ";PLD|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD) + ";School PLD|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER) + ";Teacher";
			Grid.TextMatrix(0, CNSTGridColName, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLEMPLOYEENUMBER, "Emp No");
			Grid.TextMatrix(0, CNSTGRIDCOLEARNABLECOMP, "Amount");
			Grid.TextMatrix(0, CNSTGRIDCOLCONTRIBUTION, "Emp Cont");
			Grid.TextMatrix(0, CNSTGRIDCOLPLD, "PLD");
			Grid.TextMatrix(0, CNSTGRIDCOLSCHOOLPLD, "Sch PLD");
			Grid.TextMatrix(0, CNSTGRIDCOLTEACHER, "Teacher");
			Grid.TextMatrix(0, CNSTGRIDCOLOTHER, "Other");
			Grid.TextMatrix(0, CNSTGRIDCOLREPORT, "Report");
			Grid.TextMatrix(0, CNSTGRIDCOLPOSITION, "Position");
			Grid.TextMatrix(0, CNSTGRIDCOLSTATUS, "Status");
			Grid.TextMatrix(0, CNSTGRIDCOLFEDCOMPAMOUNT, "Fed Comp");
			Grid.TextMatrix(0, CNSTGRIDCOLFEDCOMPDOLLARPERCENT, "Dol / %");
			Grid.ColComboList(CNSTGRIDCOLREPORT, strTemp);
			Grid.ColComboList(CNSTGRIDCOLFEDCOMPDOLLARPERCENT, "#0;Dollar|#1;Percent");
			Grid.ColHidden(cnstgridcolID, true);
			Grid.ColHidden(CNSTGRIDCOLPARENTROW, true);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGridColName, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLEMPLOYEENUMBER, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCONTRIBUTION, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLEARNABLECOMP, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPLD, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSCHOOLPLD, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLTEACHER, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLOTHER, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLREPORT, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPOSITION, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFEDCOMPAMOUNT, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFEDCOMPDOLLARPERCENT, FCConvert.ToInt32(0.07 * GridWidth));
        }

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				SaveInfo = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (Grid.RowOutlineLevel(lngRow) == 1)
					{
						if (FCConvert.ToBoolean(Grid.RowData(lngRow)))
						{
							if (Conversion.Val(Grid.TextMatrix(lngRow, cnstgridcolID)) > 0)
							{
								clsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, cnstgridcolID))), "twpy0000.vb1");
								if (!clsSave.EndOfFile())
								{
									clsSave.Edit();
									clsSave.Set_Fields("reporttype", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORT))));
									clsSave.Set_Fields("distm", Grid.TextMatrix(lngRow, CNSTGRIDCOLPOSITION));
									clsSave.Set_Fields("statusCODE", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS))));
									clsSave.Set_Fields("fedcompamount", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT))));
									clsSave.Set_Fields("Fedcompdollarpercent", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT))));
									clsSave.Update();
								}
							}
							Grid.RowData(lngRow, false);
						}
					}
				}
				// lngRow
				SaveInfo = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("MainePERS Records Updated", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Save Info", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}
	}
}
