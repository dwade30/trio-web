//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmFullSetofReports.
	/// </summary>
	partial class frmFullSetofReports
	{
		public fecherFoundation.FCCheckBox chkPrintAll;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCPictureBox Image2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel lbl941Wage;
		public fecherFoundation.FCPictureBox imgClearAudit;
		public fecherFoundation.FCLabel lblClearAudit;
		public fecherFoundation.FCPictureBox imgReset;
		public fecherFoundation.FCLabel lblReset;
		public fecherFoundation.FCPictureBox imgFUTA;
		public fecherFoundation.FCPictureBox imgFUTAProcessing;
		public fecherFoundation.FCLabel lblFUTA;
		public fecherFoundation.FCPictureBox imgStateUnemployment;
		public fecherFoundation.FCPictureBox imgStateUnemploymentProcessing;
		public fecherFoundation.FCLabel lblStateUnemployment;
		public fecherFoundation.FCPictureBox imgNonMMA;
		public fecherFoundation.FCPictureBox imgNonMMAProcessing;
		public fecherFoundation.FCLabel lblNonMMA;
		public fecherFoundation.FCPictureBox imgMMA;
		public fecherFoundation.FCLabel lblFTD941;
		public fecherFoundation.FCLabel lblFTD940;
		public fecherFoundation.FCLabel lblAudit;
		public fecherFoundation.FCLabel lblMSRS;
		public fecherFoundation.FCPictureBox imgFTD941;
		public fecherFoundation.FCPictureBox imgFTD940;
		public fecherFoundation.FCPictureBox imgFTD941Processing;
		public fecherFoundation.FCPictureBox imgFTD940Processing;
		public fecherFoundation.FCPictureBox imgMMAProcessing;
		public fecherFoundation.FCLabel lblMMA;
		public fecherFoundation.FCPictureBox imgPayrollWarant;
		public fecherFoundation.FCPictureBox imgCheckRegister;
		public fecherFoundation.FCLabel lblTrust;
		public fecherFoundation.FCLabel lblPayTaxSummary;
		public fecherFoundation.FCLabel lblDeductionReport;
		public fecherFoundation.FCLabel lblAccountingEntry;
		public fecherFoundation.FCLabel lblDataEntryForms;
		public fecherFoundation.FCLabel lblDirectDeposit;
		public fecherFoundation.FCLabel lblVacation;
		public fecherFoundation.FCPictureBox imgTrust;
		public fecherFoundation.FCPictureBox imgPayTaxSummary;
		public fecherFoundation.FCPictureBox imgDeductionReport;
		public fecherFoundation.FCPictureBox imgAccountingEntry;
		public fecherFoundation.FCPictureBox imgDataEntryForms;
		public fecherFoundation.FCPictureBox imgDirectDeposit;
		public fecherFoundation.FCPictureBox imgVacation;
		public fecherFoundation.FCPictureBox imgTrustProcessing;
		public fecherFoundation.FCPictureBox imgPayTaxSummaryProcessing;
		public fecherFoundation.FCPictureBox imgDeductionReportProcessing;
		public fecherFoundation.FCPictureBox imgAccountingEntryProcessing;
		public fecherFoundation.FCPictureBox imgDirectDepositProcessing;
		public fecherFoundation.FCPictureBox imgDataEntryFormsProcessing;
		public fecherFoundation.FCPictureBox imgVacationProcessing;
		public fecherFoundation.FCPictureBox imgCheckRegisterProcessing;
		public fecherFoundation.FCLabel lblCheckRegister;
		public fecherFoundation.FCPictureBox imgPayrollWarrantProcessing;
		public fecherFoundation.FCLabel lblPayrollWarrant;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCPictureBox imgResetProcessing;
		public fecherFoundation.FCPictureBox imgClearAuditProcessing;
		public fecherFoundation.FCPictureBox imgAudit;
		public fecherFoundation.FCPictureBox imgAuditProcessing;
		public fecherFoundation.FCPictureBox img941Wage;
		public fecherFoundation.FCPictureBox imgMSRS;
		public fecherFoundation.FCPictureBox img941WageProcessing;
		public fecherFoundation.FCPictureBox imgMSRSProcessing;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.chkPrintAll = new fecherFoundation.FCCheckBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Image2 = new fecherFoundation.FCPictureBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.lbl941Wage = new fecherFoundation.FCLabel();
			this.imgClearAudit = new fecherFoundation.FCPictureBox();
			this.lblClearAudit = new fecherFoundation.FCLabel();
			this.imgReset = new fecherFoundation.FCPictureBox();
			this.lblReset = new fecherFoundation.FCLabel();
			this.imgFUTA = new fecherFoundation.FCPictureBox();
			this.imgFUTAProcessing = new fecherFoundation.FCPictureBox();
			this.lblFUTA = new fecherFoundation.FCLabel();
			this.imgStateUnemployment = new fecherFoundation.FCPictureBox();
			this.imgStateUnemploymentProcessing = new fecherFoundation.FCPictureBox();
			this.lblStateUnemployment = new fecherFoundation.FCLabel();
			this.imgNonMMA = new fecherFoundation.FCPictureBox();
			this.imgNonMMAProcessing = new fecherFoundation.FCPictureBox();
			this.lblNonMMA = new fecherFoundation.FCLabel();
			this.imgMMA = new fecherFoundation.FCPictureBox();
			this.lblFTD941 = new fecherFoundation.FCLabel();
			this.lblFTD940 = new fecherFoundation.FCLabel();
			this.lblAudit = new fecherFoundation.FCLabel();
			this.lblMSRS = new fecherFoundation.FCLabel();
			this.imgFTD941 = new fecherFoundation.FCPictureBox();
			this.imgFTD940 = new fecherFoundation.FCPictureBox();
			this.imgFTD941Processing = new fecherFoundation.FCPictureBox();
			this.imgFTD940Processing = new fecherFoundation.FCPictureBox();
			this.imgMMAProcessing = new fecherFoundation.FCPictureBox();
			this.lblMMA = new fecherFoundation.FCLabel();
			this.imgPayrollWarant = new fecherFoundation.FCPictureBox();
			this.imgCheckRegister = new fecherFoundation.FCPictureBox();
			this.lblTrust = new fecherFoundation.FCLabel();
			this.lblPayTaxSummary = new fecherFoundation.FCLabel();
			this.lblDeductionReport = new fecherFoundation.FCLabel();
			this.lblAccountingEntry = new fecherFoundation.FCLabel();
			this.lblDataEntryForms = new fecherFoundation.FCLabel();
			this.lblDirectDeposit = new fecherFoundation.FCLabel();
			this.lblVacation = new fecherFoundation.FCLabel();
			this.imgTrust = new fecherFoundation.FCPictureBox();
			this.imgPayTaxSummary = new fecherFoundation.FCPictureBox();
			this.imgDeductionReport = new fecherFoundation.FCPictureBox();
			this.imgAccountingEntry = new fecherFoundation.FCPictureBox();
			this.imgDataEntryForms = new fecherFoundation.FCPictureBox();
			this.imgDirectDeposit = new fecherFoundation.FCPictureBox();
			this.imgVacation = new fecherFoundation.FCPictureBox();
			this.imgTrustProcessing = new fecherFoundation.FCPictureBox();
			this.imgPayTaxSummaryProcessing = new fecherFoundation.FCPictureBox();
			this.imgDeductionReportProcessing = new fecherFoundation.FCPictureBox();
			this.imgAccountingEntryProcessing = new fecherFoundation.FCPictureBox();
			this.imgDirectDepositProcessing = new fecherFoundation.FCPictureBox();
			this.imgDataEntryFormsProcessing = new fecherFoundation.FCPictureBox();
			this.imgVacationProcessing = new fecherFoundation.FCPictureBox();
			this.imgCheckRegisterProcessing = new fecherFoundation.FCPictureBox();
			this.lblCheckRegister = new fecherFoundation.FCLabel();
			this.imgPayrollWarrantProcessing = new fecherFoundation.FCPictureBox();
			this.lblPayrollWarrant = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.imgResetProcessing = new fecherFoundation.FCPictureBox();
			this.imgClearAuditProcessing = new fecherFoundation.FCPictureBox();
			this.imgAudit = new fecherFoundation.FCPictureBox();
			this.imgAuditProcessing = new fecherFoundation.FCPictureBox();
			this.img941Wage = new fecherFoundation.FCPictureBox();
			this.imgMSRS = new fecherFoundation.FCPictureBox();
			this.img941WageProcessing = new fecherFoundation.FCPictureBox();
			this.imgMSRSProcessing = new fecherFoundation.FCPictureBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgClearAudit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFUTA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFUTAProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgStateUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgStateUnemploymentProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgNonMMA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgNonMMAProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMMA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD941)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD940)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD941Processing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD940Processing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMMAProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayrollWarant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCheckRegister)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTrust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayTaxSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDeductionReport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAccountingEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDataEntryForms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgVacation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTrustProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayTaxSummaryProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDeductionReportProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAccountingEntryProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDirectDepositProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDataEntryFormsProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgVacationProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCheckRegisterProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayrollWarrantProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgResetProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgClearAuditProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAudit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAuditProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.img941Wage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMSRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.img941WageProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMSRSProcessing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(820, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.chkPrintAll);
			this.ClientArea.Controls.Add(this.lbl941Wage);
			this.ClientArea.Controls.Add(this.imgClearAudit);
			this.ClientArea.Controls.Add(this.lblClearAudit);
			this.ClientArea.Controls.Add(this.imgReset);
			this.ClientArea.Controls.Add(this.lblReset);
			this.ClientArea.Controls.Add(this.imgFUTA);
			this.ClientArea.Controls.Add(this.imgFUTAProcessing);
			this.ClientArea.Controls.Add(this.lblFUTA);
			this.ClientArea.Controls.Add(this.imgStateUnemployment);
			this.ClientArea.Controls.Add(this.imgStateUnemploymentProcessing);
			this.ClientArea.Controls.Add(this.lblStateUnemployment);
			this.ClientArea.Controls.Add(this.imgNonMMA);
			this.ClientArea.Controls.Add(this.imgNonMMAProcessing);
			this.ClientArea.Controls.Add(this.lblNonMMA);
			this.ClientArea.Controls.Add(this.imgMMA);
			this.ClientArea.Controls.Add(this.lblFTD941);
			this.ClientArea.Controls.Add(this.lblFTD940);
			this.ClientArea.Controls.Add(this.lblAudit);
			this.ClientArea.Controls.Add(this.lblMSRS);
			this.ClientArea.Controls.Add(this.imgFTD941);
			this.ClientArea.Controls.Add(this.imgFTD940);
			this.ClientArea.Controls.Add(this.imgFTD941Processing);
			this.ClientArea.Controls.Add(this.imgFTD940Processing);
			this.ClientArea.Controls.Add(this.imgMMAProcessing);
			this.ClientArea.Controls.Add(this.lblMMA);
			this.ClientArea.Controls.Add(this.imgPayrollWarant);
			this.ClientArea.Controls.Add(this.imgCheckRegister);
			this.ClientArea.Controls.Add(this.lblTrust);
			this.ClientArea.Controls.Add(this.lblPayTaxSummary);
			this.ClientArea.Controls.Add(this.lblDeductionReport);
			this.ClientArea.Controls.Add(this.lblAccountingEntry);
			this.ClientArea.Controls.Add(this.lblDataEntryForms);
			this.ClientArea.Controls.Add(this.lblDirectDeposit);
			this.ClientArea.Controls.Add(this.lblVacation);
			this.ClientArea.Controls.Add(this.imgTrust);
			this.ClientArea.Controls.Add(this.imgPayTaxSummary);
			this.ClientArea.Controls.Add(this.imgDeductionReport);
			this.ClientArea.Controls.Add(this.imgAccountingEntry);
			this.ClientArea.Controls.Add(this.imgDataEntryForms);
			this.ClientArea.Controls.Add(this.imgDirectDeposit);
			this.ClientArea.Controls.Add(this.imgVacation);
			this.ClientArea.Controls.Add(this.imgTrustProcessing);
			this.ClientArea.Controls.Add(this.imgPayTaxSummaryProcessing);
			this.ClientArea.Controls.Add(this.imgDeductionReportProcessing);
			this.ClientArea.Controls.Add(this.imgAccountingEntryProcessing);
			this.ClientArea.Controls.Add(this.imgDirectDepositProcessing);
			this.ClientArea.Controls.Add(this.imgDataEntryFormsProcessing);
			this.ClientArea.Controls.Add(this.imgVacationProcessing);
			this.ClientArea.Controls.Add(this.imgCheckRegisterProcessing);
			this.ClientArea.Controls.Add(this.lblCheckRegister);
			this.ClientArea.Controls.Add(this.imgPayrollWarrantProcessing);
			this.ClientArea.Controls.Add(this.lblPayrollWarrant);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.imgResetProcessing);
			this.ClientArea.Controls.Add(this.imgClearAuditProcessing);
			this.ClientArea.Controls.Add(this.imgAudit);
			this.ClientArea.Controls.Add(this.imgAuditProcessing);
			this.ClientArea.Controls.Add(this.img941Wage);
			this.ClientArea.Controls.Add(this.imgMSRS);
			this.ClientArea.Controls.Add(this.img941WageProcessing);
			this.ClientArea.Controls.Add(this.imgMSRSProcessing);
			this.ClientArea.Size = new System.Drawing.Size(820, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Size = new System.Drawing.Size(820, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			// 
			// chkPrintAll
			// 
			this.chkPrintAll.Checked = true;
			this.chkPrintAll.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkPrintAll.Location = new System.Drawing.Point(320, 30);
			this.chkPrintAll.Name = "chkPrintAll";
			this.chkPrintAll.Size = new System.Drawing.Size(230, 27);
			this.chkPrintAll.TabIndex = 24;
			this.chkPrintAll.Text = "Print reports already printed";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.Image2);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Image1);
			this.Frame1.Location = new System.Drawing.Point(20, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(184, 117);
			this.Frame1.TabIndex = 20;
			this.Frame1.Text = "Key";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(82, 75);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(88, 18);
			this.Label4.TabIndex = 22;
			this.Label4.Text = "= COMPLETE";
			// 
			// Image2
			// 
			this.Image2.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image2.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.Image2.Location = new System.Drawing.Point(20, 68);
			this.Image2.Name = "Image2";
			this.Image2.Size = new System.Drawing.Size(28, 29);
			this.Image2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image2.TabIndex = 23;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(82, 37);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(88, 18);
			this.Label3.TabIndex = 21;
			this.Label3.Text = "= PROCESSING";
			// 
			// Image1
			// 
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.Image1.Location = new System.Drawing.Point(20, 30);
			this.Image1.Name = "Image1";
			this.Image1.Size = new System.Drawing.Size(28, 29);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 24;
			// 
			// lbl941Wage
			// 
			this.lbl941Wage.Location = new System.Drawing.Point(550, 269);
			this.lbl941Wage.Name = "lbl941Wage";
			this.lbl941Wage.Size = new System.Drawing.Size(200, 16);
			this.lbl941Wage.TabIndex = 23;
			this.lbl941Wage.Text = "STATE 941";
			// 
			// imgClearAudit
			// 
			this.imgClearAudit.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgClearAudit.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgClearAudit.Location = new System.Drawing.Point(502, 67);
			this.imgClearAudit.Name = "imgClearAudit";
			this.imgClearAudit.Size = new System.Drawing.Size(28, 29);
			this.imgClearAudit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgClearAudit.TabIndex = 25;
			this.imgClearAudit.Visible = false;
			// 
			// lblClearAudit
			// 
			this.lblClearAudit.Location = new System.Drawing.Point(550, 74);
			this.lblClearAudit.Name = "lblClearAudit";
			this.lblClearAudit.Size = new System.Drawing.Size(200, 16);
			this.lblClearAudit.TabIndex = 19;
			this.lblClearAudit.Text = "ARCHIVE AUDIT INFOMATION";
			// 
			// imgReset
			// 
			this.imgReset.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgReset.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgReset.Location = new System.Drawing.Point(502, 106);
			this.imgReset.Name = "imgReset";
			this.imgReset.Size = new System.Drawing.Size(28, 29);
			this.imgReset.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgReset.TabIndex = 26;
			this.imgReset.Visible = false;
			// 
			// lblReset
			// 
			this.lblReset.Location = new System.Drawing.Point(550, 113);
			this.lblReset.Name = "lblReset";
			this.lblReset.Size = new System.Drawing.Size(200, 16);
			this.lblReset.TabIndex = 18;
			this.lblReset.Text = "RESET TEMP / DIST";
			// 
			// imgFUTA
			// 
			this.imgFUTA.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFUTA.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgFUTA.Location = new System.Drawing.Point(502, 418);
			this.imgFUTA.Name = "imgFUTA";
			this.imgFUTA.Size = new System.Drawing.Size(28, 29);
			this.imgFUTA.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFUTA.TabIndex = 28;
			this.imgFUTA.Visible = false;
			// 
			// imgFUTAProcessing
			// 
			this.imgFUTAProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFUTAProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgFUTAProcessing.Location = new System.Drawing.Point(502, 418);
			this.imgFUTAProcessing.Name = "imgFUTAProcessing";
			this.imgFUTAProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgFUTAProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFUTAProcessing.TabIndex = 29;
			this.imgFUTAProcessing.Visible = false;
			// 
			// lblFUTA
			// 
			this.lblFUTA.Location = new System.Drawing.Point(550, 425);
			this.lblFUTA.Name = "lblFUTA";
			this.lblFUTA.Size = new System.Drawing.Size(200, 16);
			this.lblFUTA.TabIndex = 17;
			this.lblFUTA.Text = " FUTA (940)";
			// 
			// imgStateUnemployment
			// 
			this.imgStateUnemployment.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgStateUnemployment.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgStateUnemployment.Location = new System.Drawing.Point(502, 379);
			this.imgStateUnemployment.Name = "imgStateUnemployment";
			this.imgStateUnemployment.Size = new System.Drawing.Size(28, 29);
			this.imgStateUnemployment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgStateUnemployment.TabIndex = 30;
			this.imgStateUnemployment.Visible = false;
			// 
			// imgStateUnemploymentProcessing
			// 
			this.imgStateUnemploymentProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgStateUnemploymentProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgStateUnemploymentProcessing.Location = new System.Drawing.Point(502, 379);
			this.imgStateUnemploymentProcessing.Name = "imgStateUnemploymentProcessing";
			this.imgStateUnemploymentProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgStateUnemploymentProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgStateUnemploymentProcessing.TabIndex = 31;
			this.imgStateUnemploymentProcessing.Visible = false;
			// 
			// lblStateUnemployment
			// 
			this.lblStateUnemployment.Location = new System.Drawing.Point(550, 386);
			this.lblStateUnemployment.Name = "lblStateUnemployment";
			this.lblStateUnemployment.Size = new System.Drawing.Size(200, 16);
			this.lblStateUnemployment.TabIndex = 16;
			this.lblStateUnemployment.Text = "UC-1";
			// 
			// imgNonMMA
			// 
			this.imgNonMMA.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgNonMMA.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgNonMMA.Location = new System.Drawing.Point(502, 340);
			this.imgNonMMA.Name = "imgNonMMA";
			this.imgNonMMA.Size = new System.Drawing.Size(28, 29);
			this.imgNonMMA.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgNonMMA.TabIndex = 32;
			this.imgNonMMA.Visible = false;
			// 
			// imgNonMMAProcessing
			// 
			this.imgNonMMAProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgNonMMAProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgNonMMAProcessing.Location = new System.Drawing.Point(502, 340);
			this.imgNonMMAProcessing.Name = "imgNonMMAProcessing";
			this.imgNonMMAProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgNonMMAProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgNonMMAProcessing.TabIndex = 33;
			this.imgNonMMAProcessing.Visible = false;
			// 
			// lblNonMMA
			// 
			this.lblNonMMA.Location = new System.Drawing.Point(550, 347);
			this.lblNonMMA.Name = "lblNonMMA";
			this.lblNonMMA.Size = new System.Drawing.Size(200, 16);
			this.lblNonMMA.TabIndex = 15;
			this.lblNonMMA.Text = "NON-MMA UNEMPLOYMENT";
			// 
			// imgMMA
			// 
			this.imgMMA.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMMA.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgMMA.Location = new System.Drawing.Point(502, 301);
			this.imgMMA.Name = "imgMMA";
			this.imgMMA.Size = new System.Drawing.Size(28, 29);
			this.imgMMA.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMMA.TabIndex = 34;
			this.imgMMA.Visible = false;
			// 
			// lblFTD941
			// 
			this.lblFTD941.Location = new System.Drawing.Point(550, 152);
			this.lblFTD941.Name = "lblFTD941";
			this.lblFTD941.Size = new System.Drawing.Size(200, 16);
			this.lblFTD941.TabIndex = 14;
			this.lblFTD941.Text = "FTD (941) - FICA/MED/FED";
			// 
			// lblFTD940
			// 
			this.lblFTD940.Location = new System.Drawing.Point(550, 191);
			this.lblFTD940.Name = "lblFTD940";
			this.lblFTD940.Size = new System.Drawing.Size(200, 16);
			this.lblFTD940.TabIndex = 13;
			this.lblFTD940.Text = "FTD (940) - FUTA";
			// 
			// lblAudit
			// 
			this.lblAudit.Location = new System.Drawing.Point(276, 425);
			this.lblAudit.Name = "lblAudit";
			this.lblAudit.Size = new System.Drawing.Size(200, 16);
			this.lblAudit.TabIndex = 12;
			this.lblAudit.Text = "AUDIT REPORT";
			// 
			// lblMSRS
			// 
			this.lblMSRS.Location = new System.Drawing.Point(550, 230);
			this.lblMSRS.Name = "lblMSRS";
			this.lblMSRS.Size = new System.Drawing.Size(200, 16);
			this.lblMSRS.TabIndex = 11;
			this.lblMSRS.Text = "MAINEPERS";
			// 
			// imgFTD941
			// 
			this.imgFTD941.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFTD941.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgFTD941.Location = new System.Drawing.Point(502, 145);
			this.imgFTD941.Name = "imgFTD941";
			this.imgFTD941.Size = new System.Drawing.Size(28, 29);
			this.imgFTD941.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFTD941.TabIndex = 35;
			this.imgFTD941.Visible = false;
			// 
			// imgFTD940
			// 
			this.imgFTD940.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFTD940.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgFTD940.Location = new System.Drawing.Point(502, 184);
			this.imgFTD940.Name = "imgFTD940";
			this.imgFTD940.Size = new System.Drawing.Size(28, 29);
			this.imgFTD940.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFTD940.TabIndex = 36;
			this.imgFTD940.Visible = false;
			// 
			// imgFTD941Processing
			// 
			this.imgFTD941Processing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFTD941Processing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgFTD941Processing.Location = new System.Drawing.Point(502, 145);
			this.imgFTD941Processing.Name = "imgFTD941Processing";
			this.imgFTD941Processing.Size = new System.Drawing.Size(28, 29);
			this.imgFTD941Processing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFTD941Processing.TabIndex = 37;
			this.imgFTD941Processing.Visible = false;
			// 
			// imgFTD940Processing
			// 
			this.imgFTD940Processing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFTD940Processing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgFTD940Processing.Location = new System.Drawing.Point(502, 184);
			this.imgFTD940Processing.Name = "imgFTD940Processing";
			this.imgFTD940Processing.Size = new System.Drawing.Size(28, 29);
			this.imgFTD940Processing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFTD940Processing.TabIndex = 38;
			this.imgFTD940Processing.Visible = false;
			// 
			// imgMMAProcessing
			// 
			this.imgMMAProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMMAProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgMMAProcessing.Location = new System.Drawing.Point(502, 301);
			this.imgMMAProcessing.Name = "imgMMAProcessing";
			this.imgMMAProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgMMAProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMMAProcessing.TabIndex = 39;
			this.imgMMAProcessing.Visible = false;
			// 
			// lblMMA
			// 
			this.lblMMA.Location = new System.Drawing.Point(550, 308);
			this.lblMMA.Name = "lblMMA";
			this.lblMMA.Size = new System.Drawing.Size(200, 16);
			this.lblMMA.TabIndex = 10;
			this.lblMMA.Text = "MMA UNEMPLOYMENT";
			// 
			// imgPayrollWarant
			// 
			this.imgPayrollWarant.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgPayrollWarant.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgPayrollWarant.Location = new System.Drawing.Point(228, 379);
			this.imgPayrollWarant.Name = "imgPayrollWarant";
			this.imgPayrollWarant.Size = new System.Drawing.Size(28, 29);
			this.imgPayrollWarant.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgPayrollWarant.TabIndex = 40;
			this.imgPayrollWarant.Visible = false;
			// 
			// imgCheckRegister
			// 
			this.imgCheckRegister.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgCheckRegister.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgCheckRegister.Location = new System.Drawing.Point(228, 106);
			this.imgCheckRegister.Name = "imgCheckRegister";
			this.imgCheckRegister.Size = new System.Drawing.Size(28, 29);
			this.imgCheckRegister.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgCheckRegister.TabIndex = 41;
			this.imgCheckRegister.Visible = false;
			// 
			// lblTrust
			// 
			this.lblTrust.Location = new System.Drawing.Point(276, 74);
			this.lblTrust.Name = "lblTrust";
			this.lblTrust.Size = new System.Drawing.Size(157, 18);
			this.lblTrust.TabIndex = 9;
			this.lblTrust.Text = "TRUST & AGENCY REPORT";
			// 
			// lblPayTaxSummary
			// 
			this.lblPayTaxSummary.Location = new System.Drawing.Point(276, 153);
			this.lblPayTaxSummary.Name = "lblPayTaxSummary";
			this.lblPayTaxSummary.Size = new System.Drawing.Size(157, 18);
			this.lblPayTaxSummary.TabIndex = 8;
			this.lblPayTaxSummary.Text = "PAY & TAX SUMMARY";
			// 
			// lblDeductionReport
			// 
			this.lblDeductionReport.Location = new System.Drawing.Point(276, 191);
			this.lblDeductionReport.Name = "lblDeductionReport";
			this.lblDeductionReport.Size = new System.Drawing.Size(157, 18);
			this.lblDeductionReport.TabIndex = 7;
			this.lblDeductionReport.Text = "DEDUCTION REPORT";
			// 
			// lblAccountingEntry
			// 
			this.lblAccountingEntry.Location = new System.Drawing.Point(276, 269);
			this.lblAccountingEntry.Name = "lblAccountingEntry";
			this.lblAccountingEntry.Size = new System.Drawing.Size(200, 16);
			this.lblAccountingEntry.TabIndex = 6;
			this.lblAccountingEntry.Text = "PAYROLL ACCOUNTING CHARGES";
			// 
			// lblDataEntryForms
			// 
			this.lblDataEntryForms.Location = new System.Drawing.Point(276, 308);
			this.lblDataEntryForms.Name = "lblDataEntryForms";
			this.lblDataEntryForms.Size = new System.Drawing.Size(200, 16);
			this.lblDataEntryForms.TabIndex = 5;
			this.lblDataEntryForms.Text = "DATA ENTRY FORMS";
			// 
			// lblDirectDeposit
			// 
			this.lblDirectDeposit.Location = new System.Drawing.Point(276, 230);
			this.lblDirectDeposit.Name = "lblDirectDeposit";
			this.lblDirectDeposit.Size = new System.Drawing.Size(157, 18);
			this.lblDirectDeposit.TabIndex = 4;
			this.lblDirectDeposit.Text = "DIRECT DEPOSIT";
			// 
			// lblVacation
			// 
			this.lblVacation.Location = new System.Drawing.Point(276, 347);
			this.lblVacation.Name = "lblVacation";
			this.lblVacation.Size = new System.Drawing.Size(200, 16);
			this.lblVacation.TabIndex = 3;
			this.lblVacation.Text = "VACATION / SICK / OTHER";
			// 
			// imgTrust
			// 
			this.imgTrust.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgTrust.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgTrust.Location = new System.Drawing.Point(228, 67);
			this.imgTrust.Name = "imgTrust";
			this.imgTrust.Size = new System.Drawing.Size(28, 29);
			this.imgTrust.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgTrust.TabIndex = 42;
			this.imgTrust.Visible = false;
			// 
			// imgPayTaxSummary
			// 
			this.imgPayTaxSummary.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgPayTaxSummary.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgPayTaxSummary.Location = new System.Drawing.Point(228, 145);
			this.imgPayTaxSummary.Name = "imgPayTaxSummary";
			this.imgPayTaxSummary.Size = new System.Drawing.Size(28, 29);
			this.imgPayTaxSummary.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgPayTaxSummary.TabIndex = 43;
			this.imgPayTaxSummary.Visible = false;
			// 
			// imgDeductionReport
			// 
			this.imgDeductionReport.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDeductionReport.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgDeductionReport.Location = new System.Drawing.Point(228, 184);
			this.imgDeductionReport.Name = "imgDeductionReport";
			this.imgDeductionReport.Size = new System.Drawing.Size(28, 29);
			this.imgDeductionReport.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDeductionReport.TabIndex = 44;
			this.imgDeductionReport.Visible = false;
			// 
			// imgAccountingEntry
			// 
			this.imgAccountingEntry.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgAccountingEntry.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgAccountingEntry.Location = new System.Drawing.Point(228, 262);
			this.imgAccountingEntry.Name = "imgAccountingEntry";
			this.imgAccountingEntry.Size = new System.Drawing.Size(28, 29);
			this.imgAccountingEntry.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgAccountingEntry.TabIndex = 45;
			this.imgAccountingEntry.Visible = false;
			// 
			// imgDataEntryForms
			// 
			this.imgDataEntryForms.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDataEntryForms.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgDataEntryForms.Location = new System.Drawing.Point(228, 301);
			this.imgDataEntryForms.Name = "imgDataEntryForms";
			this.imgDataEntryForms.Size = new System.Drawing.Size(28, 29);
			this.imgDataEntryForms.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDataEntryForms.TabIndex = 46;
			this.imgDataEntryForms.Visible = false;
			// 
			// imgDirectDeposit
			// 
			this.imgDirectDeposit.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDirectDeposit.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgDirectDeposit.Location = new System.Drawing.Point(228, 223);
			this.imgDirectDeposit.Name = "imgDirectDeposit";
			this.imgDirectDeposit.Size = new System.Drawing.Size(28, 29);
			this.imgDirectDeposit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDirectDeposit.TabIndex = 47;
			this.imgDirectDeposit.Visible = false;
			// 
			// imgVacation
			// 
			this.imgVacation.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgVacation.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgVacation.Location = new System.Drawing.Point(228, 340);
			this.imgVacation.Name = "imgVacation";
			this.imgVacation.Size = new System.Drawing.Size(28, 29);
			this.imgVacation.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgVacation.TabIndex = 48;
			this.imgVacation.Visible = false;
			// 
			// imgTrustProcessing
			// 
			this.imgTrustProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgTrustProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgTrustProcessing.Location = new System.Drawing.Point(228, 67);
			this.imgTrustProcessing.Name = "imgTrustProcessing";
			this.imgTrustProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgTrustProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgTrustProcessing.TabIndex = 49;
			this.imgTrustProcessing.Visible = false;
			// 
			// imgPayTaxSummaryProcessing
			// 
			this.imgPayTaxSummaryProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgPayTaxSummaryProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgPayTaxSummaryProcessing.Location = new System.Drawing.Point(228, 145);
			this.imgPayTaxSummaryProcessing.Name = "imgPayTaxSummaryProcessing";
			this.imgPayTaxSummaryProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgPayTaxSummaryProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgPayTaxSummaryProcessing.TabIndex = 50;
			this.imgPayTaxSummaryProcessing.Visible = false;
			// 
			// imgDeductionReportProcessing
			// 
			this.imgDeductionReportProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDeductionReportProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgDeductionReportProcessing.Location = new System.Drawing.Point(228, 184);
			this.imgDeductionReportProcessing.Name = "imgDeductionReportProcessing";
			this.imgDeductionReportProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgDeductionReportProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDeductionReportProcessing.TabIndex = 51;
			this.imgDeductionReportProcessing.Visible = false;
			// 
			// imgAccountingEntryProcessing
			// 
			this.imgAccountingEntryProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgAccountingEntryProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgAccountingEntryProcessing.Location = new System.Drawing.Point(228, 262);
			this.imgAccountingEntryProcessing.Name = "imgAccountingEntryProcessing";
			this.imgAccountingEntryProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgAccountingEntryProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgAccountingEntryProcessing.TabIndex = 52;
			this.imgAccountingEntryProcessing.Visible = false;
			// 
			// imgDirectDepositProcessing
			// 
			this.imgDirectDepositProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDirectDepositProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgDirectDepositProcessing.Location = new System.Drawing.Point(228, 223);
			this.imgDirectDepositProcessing.Name = "imgDirectDepositProcessing";
			this.imgDirectDepositProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgDirectDepositProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDirectDepositProcessing.TabIndex = 53;
			this.imgDirectDepositProcessing.Visible = false;
			// 
			// imgDataEntryFormsProcessing
			// 
			this.imgDataEntryFormsProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDataEntryFormsProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgDataEntryFormsProcessing.Location = new System.Drawing.Point(228, 301);
			this.imgDataEntryFormsProcessing.Name = "imgDataEntryFormsProcessing";
			this.imgDataEntryFormsProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgDataEntryFormsProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDataEntryFormsProcessing.TabIndex = 54;
			this.imgDataEntryFormsProcessing.Visible = false;
			// 
			// imgVacationProcessing
			// 
			this.imgVacationProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgVacationProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgVacationProcessing.Location = new System.Drawing.Point(228, 340);
			this.imgVacationProcessing.Name = "imgVacationProcessing";
			this.imgVacationProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgVacationProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgVacationProcessing.TabIndex = 55;
			this.imgVacationProcessing.Visible = false;
			// 
			// imgCheckRegisterProcessing
			// 
			this.imgCheckRegisterProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgCheckRegisterProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgCheckRegisterProcessing.Location = new System.Drawing.Point(228, 106);
			this.imgCheckRegisterProcessing.Name = "imgCheckRegisterProcessing";
			this.imgCheckRegisterProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgCheckRegisterProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgCheckRegisterProcessing.TabIndex = 56;
			this.imgCheckRegisterProcessing.Visible = false;
			// 
			// lblCheckRegister
			// 
			this.lblCheckRegister.Location = new System.Drawing.Point(276, 113);
			this.lblCheckRegister.Name = "lblCheckRegister";
			this.lblCheckRegister.Size = new System.Drawing.Size(157, 18);
			this.lblCheckRegister.TabIndex = 2;
			this.lblCheckRegister.Text = "CHECK REGISTER";
			// 
			// imgPayrollWarrantProcessing
			// 
			this.imgPayrollWarrantProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgPayrollWarrantProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgPayrollWarrantProcessing.Location = new System.Drawing.Point(228, 379);
			this.imgPayrollWarrantProcessing.Name = "imgPayrollWarrantProcessing";
			this.imgPayrollWarrantProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgPayrollWarrantProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgPayrollWarrantProcessing.TabIndex = 57;
			this.imgPayrollWarrantProcessing.Visible = false;
			// 
			// lblPayrollWarrant
			// 
			this.lblPayrollWarrant.Location = new System.Drawing.Point(276, 386);
			this.lblPayrollWarrant.Name = "lblPayrollWarrant";
			this.lblPayrollWarrant.Size = new System.Drawing.Size(200, 16);
			this.lblPayrollWarrant.TabIndex = 1;
			this.lblPayrollWarrant.Text = "PAYROLL WARRANT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(228, 39);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(60, 16);
			this.Label2.TabIndex = 58;
			this.Label2.Text = "STATUS";
			// 
			// imgResetProcessing
			// 
			this.imgResetProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgResetProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgResetProcessing.Location = new System.Drawing.Point(502, 106);
			this.imgResetProcessing.Name = "imgResetProcessing";
			this.imgResetProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgResetProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgResetProcessing.TabIndex = 58;
			this.imgResetProcessing.Visible = false;
			// 
			// imgClearAuditProcessing
			// 
			this.imgClearAuditProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgClearAuditProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgClearAuditProcessing.Location = new System.Drawing.Point(502, 67);
			this.imgClearAuditProcessing.Name = "imgClearAuditProcessing";
			this.imgClearAuditProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgClearAuditProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgClearAuditProcessing.TabIndex = 59;
			this.imgClearAuditProcessing.Visible = false;
			// 
			// imgAudit
			// 
			this.imgAudit.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgAudit.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgAudit.Location = new System.Drawing.Point(228, 418);
			this.imgAudit.Name = "imgAudit";
			this.imgAudit.Size = new System.Drawing.Size(28, 29);
			this.imgAudit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgAudit.TabIndex = 60;
			this.imgAudit.Visible = false;
			// 
			// imgAuditProcessing
			// 
			this.imgAuditProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgAuditProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgAuditProcessing.Location = new System.Drawing.Point(228, 418);
			this.imgAuditProcessing.Name = "imgAuditProcessing";
			this.imgAuditProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgAuditProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgAuditProcessing.TabIndex = 61;
			this.imgAuditProcessing.Visible = false;
			// 
			// img941Wage
			// 
			this.img941Wage.BorderStyle = Wisej.Web.BorderStyle.None;
			this.img941Wage.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.img941Wage.Location = new System.Drawing.Point(502, 262);
			this.img941Wage.Name = "img941Wage";
			this.img941Wage.Size = new System.Drawing.Size(28, 29);
			this.img941Wage.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.img941Wage.TabIndex = 62;
			this.img941Wage.Visible = false;
			// 
			// imgMSRS
			// 
			this.imgMSRS.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMSRS.ImageSource = "icon - process complete - active?color=#7EDBE7";
			this.imgMSRS.Location = new System.Drawing.Point(502, 223);
			this.imgMSRS.Name = "imgMSRS";
			this.imgMSRS.Size = new System.Drawing.Size(28, 29);
			this.imgMSRS.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMSRS.TabIndex = 63;
			this.imgMSRS.Visible = false;
			// 
			// img941WageProcessing
			// 
			this.img941WageProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.img941WageProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.img941WageProcessing.Location = new System.Drawing.Point(502, 262);
			this.img941WageProcessing.Name = "img941WageProcessing";
			this.img941WageProcessing.Size = new System.Drawing.Size(28, 29);
			this.img941WageProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.img941WageProcessing.TabIndex = 64;
			this.img941WageProcessing.Visible = false;
			// 
			// imgMSRSProcessing
			// 
			this.imgMSRSProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMSRSProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
			this.imgMSRSProcessing.Location = new System.Drawing.Point(502, 223);
			this.imgMSRSProcessing.Name = "imgMSRSProcessing";
			this.imgMSRSProcessing.Size = new System.Drawing.Size(28, 29);
			this.imgMSRSProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMSRSProcessing.TabIndex = 65;
			this.imgMSRSProcessing.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 0;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 1;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Visible = false;
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(393, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(82, 48);
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.Location = new System.Drawing.Point(747, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(45, 24);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save";
			this.cmdSave.Visible = false;
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmFullSetofReports
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(820, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmFullSetofReports";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmFullSetofReports_Load);
			this.Activated += new System.EventHandler(this.frmFullSetofReports_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFullSetofReports_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgClearAudit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFUTA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFUTAProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgStateUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgStateUnemploymentProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgNonMMA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgNonMMAProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMMA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD941)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD940)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD941Processing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgFTD940Processing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMMAProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayrollWarant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCheckRegister)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTrust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayTaxSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDeductionReport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAccountingEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDataEntryForms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgVacation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTrustProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayTaxSummaryProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDeductionReportProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAccountingEntryProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDirectDepositProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDataEntryFormsProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgVacationProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCheckRegisterProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPayrollWarrantProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgResetProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgClearAuditProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAudit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgAuditProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.img941Wage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMSRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.img941WageProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMSRSProcessing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrint;
		private FCButton cmdSave;
	}
}