﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTACheckDetail.
	/// </summary>
	partial class rptTACheckDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTACheckDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblSort = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtDateField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVoid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayRun = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRecipient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeduction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblSort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateField)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRun)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecipient)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeduction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmount,
				this.txtDescription,
				this.txtDeduction
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblSort,
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime
			});
			this.PageHeader.Height = 0.6354167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape1,
				this.txtDateField,
				this.txtVoid,
				this.txtCheckNumber,
				this.txtPayRun,
				this.Field6,
				this.Field19,
				this.txtRecipient,
				this.Line15,
				this.Field2,
				this.Field4,
				this.Field5,
				this.Line12,
				this.Field18,
				this.Field22
			});
			this.GroupHeader1.Height = 0.875F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroupAmount,
				this.Line14,
				this.Field16
			});
			this.GroupFooter1.Height = 0.4583333F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblSort
			// 
			this.lblSort.Height = 0.2083333F;
			this.lblSort.HyperLink = null;
			this.lblSort.Left = 0.0625F;
			this.lblSort.Name = "lblSort";
			this.lblSort.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblSort.Text = null;
			this.lblSort.Top = 0.25F;
			this.lblSort.Width = 9.78125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL CHECK LISTING";
			this.Label1.Top = 0.04166667F;
			this.Label1.Width = 9.78125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.08333334F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.40625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.40625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape1.Height = 0.5333334F;
			this.Shape1.Left = 0F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0F;
			this.Shape1.Width = 9.933333F;
			// 
			// txtDateField
			// 
			this.txtDateField.Height = 0.2333333F;
			this.txtDateField.Left = 1.133333F;
			this.txtDateField.Name = "txtDateField";
			this.txtDateField.Style = "font-family: \'Tahoma\'";
			this.txtDateField.Text = "Date";
			this.txtDateField.Top = 0.2333333F;
			this.txtDateField.Width = 0.8F;
			// 
			// txtVoid
			// 
			this.txtVoid.Height = 0.2333333F;
			this.txtVoid.Left = 2.7F;
			this.txtVoid.Name = "txtVoid";
			this.txtVoid.Style = "font-family: \'Tahoma\'";
			this.txtVoid.Text = "Void";
			this.txtVoid.Top = 0.2333333F;
			this.txtVoid.Width = 0.4F;
			// 
			// txtCheckNumber
			// 
			this.txtCheckNumber.Height = 0.2333333F;
			this.txtCheckNumber.Left = 0.3F;
			this.txtCheckNumber.Name = "txtCheckNumber";
			this.txtCheckNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCheckNumber.Text = "Check #";
			this.txtCheckNumber.Top = 0.2333333F;
			this.txtCheckNumber.Width = 0.5666667F;
			// 
			// txtPayRun
			// 
			this.txtPayRun.Height = 0.2333333F;
			this.txtPayRun.Left = 2.2F;
			this.txtPayRun.Name = "txtPayRun";
			this.txtPayRun.Style = "font-family: \'Tahoma\'";
			this.txtPayRun.Text = "Pay Run";
			this.txtPayRun.Top = 0.2333333F;
			this.txtPayRun.Width = 0.4333333F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2F;
			this.Field6.Left = 4.7F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Field6.Text = "Amount";
			this.Field6.Top = 0.6333333F;
			this.Field6.Width = 0.6666667F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.2F;
			this.Field19.Left = 5.433333F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field19.Text = "Description";
			this.Field19.Top = 0.6333333F;
			this.Field19.Width = 2F;
			// 
			// txtRecipient
			// 
			this.txtRecipient.Height = 0.2F;
			this.txtRecipient.Left = 3.2F;
			this.txtRecipient.Name = "txtRecipient";
			this.txtRecipient.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtRecipient.Text = "Recipient";
			this.txtRecipient.Top = 0.2333333F;
			this.txtRecipient.Width = 6.633333F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 4.533333F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 0.8F;
			this.Line15.Width = 5.333333F;
			this.Line15.X1 = 4.533333F;
			this.Line15.X2 = 9.866667F;
			this.Line15.Y1 = 0.8F;
			this.Line15.Y2 = 0.8F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2333333F;
			this.Field2.Left = 1.066667F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Field2.Text = "Pay Date";
			this.Field2.Top = 0.03333334F;
			this.Field2.Width = 0.8333333F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2333333F;
			this.Field4.Left = 2.766667F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field4.Text = "Void";
			this.Field4.Top = 0.03333334F;
			this.Field4.Width = 0.4F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2333333F;
			this.Field5.Left = 0.3333333F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field5.Text = "Check #";
			this.Field5.Top = 0.03333334F;
			this.Field5.Width = 0.6666667F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0.2F;
			this.Line12.Width = 9.933333F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 9.933333F;
			this.Line12.Y1 = 0.2F;
			this.Line12.Y2 = 0.2F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.2333333F;
			this.Field18.Left = 2F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field18.Text = "Pay Run ";
			this.Field18.Top = 0.03333334F;
			this.Field18.Width = 0.6666667F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.2333333F;
			this.Field22.Left = 3.2F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field22.Text = "Recipient";
			this.Field22.Top = 0.03333334F;
			this.Field22.Width = 1.833333F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.2F;
			this.txtAmount.Left = 4.533333F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAmount.Text = "Amount";
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.8333333F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.2333333F;
			this.txtDescription.Left = 5.433333F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtDescription.Text = "Regular";
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 2.466667F;
			// 
			// txtDeduction
			// 
			this.txtDeduction.Height = 0.2333333F;
			this.txtDeduction.Left = 8F;
			this.txtDeduction.Name = "txtDeduction";
			this.txtDeduction.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtDeduction.Text = "Deduction";
			this.txtDeduction.Top = 0F;
			this.txtDeduction.Width = 1.9F;
			// 
			// txtGroupAmount
			// 
			this.txtGroupAmount.Height = 0.1666667F;
			this.txtGroupAmount.Left = 4.5F;
			this.txtGroupAmount.Name = "txtGroupAmount";
			this.txtGroupAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupAmount.Text = "Field15";
			this.txtGroupAmount.Top = 0.1F;
			this.txtGroupAmount.Width = 0.8666667F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 4.53125F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0.0625F;
			this.Line14.Width = 2.03125F;
			this.Line14.X1 = 4.53125F;
			this.Line14.X2 = 6.5625F;
			this.Line14.Y1 = 0.0625F;
			this.Line14.Y2 = 0.0625F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.2F;
			this.Field16.Left = 5.466667F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field16.Text = "Check Total";
			this.Field16.Top = 0.1F;
			this.Field16.Width = 1F;
			// 
			// rptTACheckDetail
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEndedAndCanceled += ActiveReport_ReportEndedAndCanceled;
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.9375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblSort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateField)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRun)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecipient)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeduction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeduction;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSort;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateField;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVoid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayRun;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRecipient;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
	}
}
