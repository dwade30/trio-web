//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmExportVacSick.
	/// </summary>
	partial class frmExportVacSick
	{
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCListBox lstTypes;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton btnBrowse;
		public fecherFoundation.FCTextBox txtExportpath;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCListBox lstSequence;
		public fecherFoundation.FCListBox lstDepartment;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.ListViewItem listViewItem1 = new Wisej.Web.ListViewItem("lstTypes");
            this.Frame3 = new fecherFoundation.FCFrame();
            this.lstTypes = new fecherFoundation.FCListBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.btnBrowse = new fecherFoundation.FCButton();
            this.txtExportpath = new fecherFoundation.FCTextBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.lstSequence = new fecherFoundation.FCListBox();
            this.lstDepartment = new fecherFoundation.FCListBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(584, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(584, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(584, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(334, 30);
            this.HeaderText.Text = "Export Vacation / Sick / Other";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.lstTypes);
            this.Frame3.Location = new System.Drawing.Point(30, 291);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(445, 402);
            this.Frame3.TabIndex = 6;
            this.Frame3.Text = "Types";
            // 
            // lstTypes
            // 
            this.lstTypes.BackColor = System.Drawing.SystemColors.Window;
            this.lstTypes.CheckBoxes = true;
            this.lstTypes.Items.AddRange(new Wisej.Web.ListViewItem[] {
            listViewItem1});
            this.lstTypes.Location = new System.Drawing.Point(20, 30);
            this.lstTypes.Name = "lstTypes";
            this.lstTypes.Size = new System.Drawing.Size(405, 342);
            this.lstTypes.Style = 1;
            this.lstTypes.TabIndex = 7;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.btnBrowse);
            this.Frame2.Controls.Add(this.txtExportpath);
            this.Frame2.Location = new System.Drawing.Point(30, 523);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(516, 90);
            this.Frame2.TabIndex = 3;
            this.Frame2.Text = "Export To";
            // 
            // btnBrowse
            // 
            this.btnBrowse.AppearanceKey = "actionButton";
            this.btnBrowse.Location = new System.Drawing.Point(396, 30);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(100, 40);
            this.btnBrowse.TabIndex = 5;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtExportpath
            // 
            this.txtExportpath.BackColor = System.Drawing.SystemColors.Window;
            this.txtExportpath.Location = new System.Drawing.Point(20, 30);
            this.txtExportpath.Name = "txtExportpath";
            this.txtExportpath.Size = new System.Drawing.Size(358, 40);
            this.txtExportpath.TabIndex = 4;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.lstSequence);
            this.Frame1.Controls.Add(this.lstDepartment);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(445, 241);
            this.Frame1.Text = "Employees";
            // 
            // lstSequence
            // 
            this.lstSequence.BackColor = System.Drawing.SystemColors.Window;
            this.lstSequence.CheckBoxes = true;
            this.lstSequence.Location = new System.Drawing.Point(307, 59);
            this.lstSequence.Name = "lstSequence";
            this.lstSequence.Size = new System.Drawing.Size(118, 162);
            this.lstSequence.Style = 1;
            this.lstSequence.TabIndex = 2;
            // 
            // lstDepartment
            // 
            this.lstDepartment.BackColor = System.Drawing.SystemColors.Window;
            this.lstDepartment.CheckBoxes = true;
            this.lstDepartment.Location = new System.Drawing.Point(20, 59);
            this.lstDepartment.Name = "lstDepartment";
            this.lstDepartment.Size = new System.Drawing.Size(267, 162);
            this.lstDepartment.Style = 1;
            this.lstDepartment.TabIndex = 1;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(307, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(84, 15);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "SEQUENCE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(84, 15);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "DEPARTMENT";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Export";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveExit
            // 
            this.cmdSaveExit.AppearanceKey = "acceptButton";
            this.cmdSaveExit.Location = new System.Drawing.Point(247, 30);
            this.cmdSaveExit.Name = "cmdSaveExit";
            this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveExit.Size = new System.Drawing.Size(94, 48);
            this.cmdSaveExit.Text = "Export";
            this.cmdSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmExportVacSick
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(584, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmExportVacSick";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Export Vacation / Sick / Other";
            this.Load += new System.EventHandler(this.frmExportVacSick_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExportVacSick_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSaveExit;
	}
}