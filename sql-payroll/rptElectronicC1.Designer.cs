﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptElectronicC1.
	/// </summary>
	partial class rptElectronicC1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptElectronicC1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtsubtitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubtitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.lblPage,
				this.txtTime,
				this.txtsubtitle
			});
			this.PageHeader.Height = 0.4375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtUGross,
				this.txtExcess,
				this.txtTaxable,
				this.txtStateGross,
				this.txtStateTax
			});
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.25F;
			this.txtCaption.Left = 2.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = "Electronic C1 File Contents";
			this.txtCaption.Top = 0F;
			this.txtCaption.Width = 3.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.75F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.3125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtsubtitle
			// 
			this.txtsubtitle.Height = 0.1875F;
			this.txtsubtitle.Left = 2.0625F;
			this.txtsubtitle.Name = "txtsubtitle";
			this.txtsubtitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtsubtitle.Text = null;
			this.txtsubtitle.Top = 0.25F;
			this.txtsubtitle.Width = 3.5625F;
			// 
			// txtUGross
			// 
			this.txtUGross.Height = 0.1875F;
			this.txtUGross.Left = 3.125F;
			this.txtUGross.Name = "txtUGross";
			this.txtUGross.Style = "font-weight: bold; text-align: right";
			this.txtUGross.Text = "U. Gross";
			this.txtUGross.Top = 0.0625F;
			this.txtUGross.Visible = false;
			this.txtUGross.Width = 0.8125F;
			// 
			// txtExcess
			// 
			this.txtExcess.Height = 0.1875F;
			this.txtExcess.Left = 4F;
			this.txtExcess.Name = "txtExcess";
			this.txtExcess.Style = "font-weight: bold; text-align: right";
			this.txtExcess.Text = "Excess";
			this.txtExcess.Top = 0.0625F;
			this.txtExcess.Visible = false;
			this.txtExcess.Width = 0.8125F;
			// 
			// txtTaxable
			// 
			this.txtTaxable.Height = 0.1875F;
			this.txtTaxable.Left = 4.875F;
			this.txtTaxable.Name = "txtTaxable";
			this.txtTaxable.Style = "font-weight: bold; text-align: right";
			this.txtTaxable.Text = "Taxable";
			this.txtTaxable.Top = 0.0625F;
			this.txtTaxable.Visible = false;
			this.txtTaxable.Width = 0.8125F;
			// 
			// txtStateGross
			// 
			this.txtStateGross.Height = 0.1875F;
			this.txtStateGross.Left = 5.75F;
			this.txtStateGross.Name = "txtStateGross";
			this.txtStateGross.Style = "font-weight: bold; text-align: right";
			this.txtStateGross.Text = "ST Gross";
			this.txtStateGross.Top = 0.0625F;
			this.txtStateGross.Visible = false;
			this.txtStateGross.Width = 0.8125F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.1875F;
			this.txtStateTax.Left = 6.625F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "font-weight: bold; text-align: right";
			this.txtStateTax.Text = "Tax WH";
			this.txtStateTax.Top = 0.0625F;
			this.txtStateTax.Visible = false;
			this.txtStateTax.Width = 0.8125F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0F;
			this.SubReport1.Width = 7.4375F;
			// 
			// rptElectronicC1
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubtitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsubtitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
