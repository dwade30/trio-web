//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEmailPayrollChecks : BaseForm
	{
		public frmEmailPayrollChecks()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		
		public static frmEmailPayrollChecks InstancePtr
		{
			get
			{
				return (frmEmailPayrollChecks)Sys.GetInstance(typeof(frmEmailPayrollChecks));
			}
		}

		protected frmEmailPayrollChecks _InstancePtr = null;

		private clsDRWrapper rsLoad = new clsDRWrapper();
		bool boolLoading;
		private cCheckService checkService = new cCheckService();
		private cEmployeeService employeeService = new cEmployeeService();
		private cSettingsController setCont = new cSettingsController();
		private cEmailPayrollChecksView theView = new cEmailPayrollChecksView();

		private void cmbPayDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				FillcmbPayRun();
			}
		}

		private void frmEmailPayrollChecks_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEmailPayrollChecks_Load(object sender, System.EventArgs e)
		{   
            cmbHTML.Items.Remove("HTML");
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
            rsLoad.OpenRecordset("select showpayperiodonstub from tblDefaultInformation", "Payroll");

            if (!rsLoad.EndOfFile())
            {
                theView.ShowPayPeriodOnStub = rsLoad.Get_Fields_Boolean("showpayperiodonstub");
            }

			rsLoad.OpenRecordset("select distinct paydate,payrunid from tblcheckdetail WHERE not isnull(checkvoid,0) = 1 order by paydate desc, payrunid", "Payroll");
			FillcmbPayDate();
			SetupCheckCombo();
			string strTemp;
			strTemp = setCont.GetSettingValue("PayrollCheckEmailFormat", "Payroll", "", "", "");
			if (fecherFoundation.Strings.LCase(strTemp) == "html")
			{
				cmbHTML.Text = "HTML";
			}
			else
			{
				cmbHTML.Text = "PDF attachment";
			}
		}

		private void FillcmbPayRun()
		{
			rsLoad.MoveFirst();
			cmbPayrun.Clear();
			string strPayDate = "";
			if (cmbPayDate.SelectedIndex >= 0)
			{
				strPayDate = cmbPayDate.Text;
				while (!rsLoad.EndOfFile())
				{
					if (strPayDate == Strings.Format(rsLoad.Get_Fields("paydate"), "MM/dd/yyyy"))
					{
						cmbPayrun.AddItem(rsLoad.Get_Fields_Int32("Payrunid").ToString());
					}
					rsLoad.MoveNext();
				}
			}
			if (cmbPayrun.Items.Count > 0)
			{
				cmbPayrun.SelectedIndex = 0;
			}
		}

		private void FillcmbPayDate()
		{
			rsLoad.MoveFirst();
			boolLoading = true;
			cmbPayDate.Clear();
			string strLastDate;
			strLastDate = "";
			while (!rsLoad.EndOfFile())
			{
				if (!(Strings.Format(rsLoad.Get_Fields("paydate"), "MM/dd/yyyy") == strLastDate))
				{
					strLastDate = Strings.Format(rsLoad.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy");
					cmbPayDate.AddItem(strLastDate);
					cmbPayDate.ItemData(cmbPayDate.ListCount - 1, rsLoad.Get_Fields_Int32("payrunid"));
				}
				else
				{
					cmbPayDate.ItemData(cmbPayDate.ListCount - 1, rsLoad.Get_Fields_Int32("payrunid"));
				}
				rsLoad.MoveNext();
			}
			if (cmbPayDate.Items.Count > 0)
			{
				cmbPayDate.SelectedIndex = 0;
				FillcmbPayRun();
			}
			boolLoading = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupCheckCombo()
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
            int intFormatID;
            string strTemp = "";
			int counter;
			rsCheckInfo.OpenRecordset("SELECT * FROM CustomChecks ORDER BY Description","Payroll");
			cboCheckSelection.Clear();
			while (!rsCheckInfo.EndOfFile())
			{
				cboCheckSelection.AddItem(Strings.Format(rsCheckInfo.Get_Fields("ID"), "00") + " - " + rsCheckInfo.Get_Fields("Description"));
				cboCheckSelection.ItemData(cboCheckSelection.NewIndex, FCConvert.ToInt32(rsCheckInfo.Get_Fields("ID")));
				rsCheckInfo.MoveNext();
			}
			intFormatID = 0;
			if (cboCheckSelection.Items.Count == 1)
			{
				intFormatID = cboCheckSelection.ItemData(0);
				cboCheckSelection.SelectedIndex = 0;
			}
			else
			{
				strTemp = setCont.GetSettingValue("PayrollEmailCheckFormat", "Payroll", "", "", "");
				if (strTemp != "")
				{
					intFormatID = FCConvert.ToInt32(strTemp);
				}
				if (intFormatID > 0)
				{
					for (counter = 0; counter <= cboCheckSelection.Items.Count - 1; counter++)
					{
						if (intFormatID == cboCheckSelection.ItemData(counter))
						{
							cboCheckSelection.SelectedIndex = counter;
							break;
						}
					}
				}
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbPayDate.SelectedIndex >= 0 && cmbPayrun.SelectedIndex >= 0)
			{
				if (cboCheckSelection.SelectedIndex >= 0)
				{
					EmailPayrollChecks();
				}
				else
				{
					MessageBox.Show("You must select a check format to continue", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("You must select a valid pay date and pay run to continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void EmailPayrollChecks()
		{
			try
			{
                string strExportDir = "";

				theView.ExportDirectory = strExportDir;
				if (cmbHTML.Text == "HTML")
				{
					theView.ExportFormat = "html";
				}
				else if (cmbHTML.Text == "PDF attachment")
				{
					theView.ExportFormat = "pdf";
				}
				else
				{
					theView.ExportFormat = "pdf";
				}
				theView.CheckFormat = cboCheckSelection.ItemData(cboCheckSelection.SelectedIndex);
				theView.PayDate = cmbPayDate.Text;
				theView.PayRun = cmbPayrun.Text.ToIntegerValue();
                theView.ChecksLoaded += TheView_ChecksLoaded;
                this.ShowLoader = true;
                theView.BuildChecksReport(strExportDir, this);
                return;
			}
			catch (Exception ex)
			{
                MessageBox.Show("Error " +ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void TheView_ChecksLoaded(object sender, EventArgs e)
        {
            this.ShowLoader = false;
        }

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
