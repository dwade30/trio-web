﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCheckListing.
	/// </summary>
	partial class frmCheckListing
	{
		public fecherFoundation.FCComboBox cmbReportOn;
		public fecherFoundation.FCLabel lblReportOn;
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCCheckBox chkVoidOnly;
		public fecherFoundation.FCCheckBox chkGrossWages;
		public fecherFoundation.FCFrame Frame2;
		public T2KDateBox txtEndDate;
		public T2KDateBox txtStartDate;
		public fecherFoundation.FCTextBox txtEmployeeNumber;
		public fecherFoundation.FCLabel lblDash;
		public fecherFoundation.FCLabel lblCaption;
		public fecherFoundation.FCCheckBox chkVoid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbReportOn = new fecherFoundation.FCComboBox();
            this.lblReportOn = new fecherFoundation.FCLabel();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.chkVoidOnly = new fecherFoundation.FCCheckBox();
            this.chkGrossWages = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtEndDate = new T2KDateBox();
            this.txtStartDate = new T2KDateBox();
            this.txtEmployeeNumber = new fecherFoundation.FCTextBox();
            this.lblDash = new fecherFoundation.FCLabel();
            this.lblCaption = new fecherFoundation.FCLabel();
            this.chkVoid = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoidOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrossWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 367);
            this.BottomPanel.Size = new System.Drawing.Size(851, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkVoidOnly);
            this.ClientArea.Controls.Add(this.chkGrossWages);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbReportOn);
            this.ClientArea.Controls.Add(this.lblReportOn);
            this.ClientArea.Controls.Add(this.chkVoid);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Size = new System.Drawing.Size(851, 307);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(851, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 30);
            this.HeaderText.Text = "Check Listing Reports";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbReportOn
            // 
            this.cmbReportOn.Items.AddRange(new object[] {
            "Single Employee",
            "Emp/Date Range",
            "Month",
            "Quarter",
            "Year",
            "Date Range",
            "Specific Check",
            "All Files"});
            this.cmbReportOn.Location = new System.Drawing.Point(196, 150);
            this.cmbReportOn.Name = "cmbReportOn";
            this.cmbReportOn.Size = new System.Drawing.Size(321, 40);
            this.cmbReportOn.TabIndex = 28;
            this.ToolTip1.SetToolTip(this.cmbReportOn, null);
            this.cmbReportOn.SelectedIndexChanged += new System.EventHandler(this.optReportOn_CheckedChanged);
            // 
            // lblReportOn
            // 
            this.lblReportOn.AutoSize = true;
            this.lblReportOn.Location = new System.Drawing.Point(30, 164);
            this.lblReportOn.Name = "lblReportOn";
            this.lblReportOn.Size = new System.Drawing.Size(81, 15);
            this.lblReportOn.TabIndex = 29;
            this.lblReportOn.Text = "REPORT ON";
            this.ToolTip1.SetToolTip(this.lblReportOn, null);
            // 
            // cmbSort
            // 
            this.cmbSort.Items.AddRange(new object[] {
            "Name",
            "Check",
            "Pay Date"});
            this.cmbSort.Location = new System.Drawing.Point(196, 90);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(321, 40);
            this.cmbSort.TabIndex = 30;
            this.ToolTip1.SetToolTip(this.cmbSort, null);
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(30, 104);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(108, 15);
            this.lblSort.TabIndex = 31;
            this.lblSort.Text = "LIST REPORT BY";
            this.ToolTip1.SetToolTip(this.lblSort, null);
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Check Listing Regular",
            "Check Listing Tax Breakdown",
            "Employee Distribution Breakdown",
            "TA Check Detail",
            "Account Number Detail"});
            this.cmbType.Location = new System.Drawing.Point(196, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(321, 40);
            this.cmbType.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.cmbType, null);
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.optType_CheckedChanged);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(115, 15);
            this.lblType.TabIndex = 33;
            this.lblType.Text = "TYPE OF REPORT";
            this.ToolTip1.SetToolTip(this.lblType, null);
            // 
            // chkVoidOnly
            // 
            this.chkVoidOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkVoidOnly.Enabled = false;
            this.chkVoidOnly.Location = new System.Drawing.Point(762, 30);
            this.chkVoidOnly.Name = "chkVoidOnly";
            this.chkVoidOnly.Size = new System.Drawing.Size(61, 27);
            this.chkVoidOnly.TabIndex = 8;
            this.chkVoidOnly.Text = "Only";
            this.ToolTip1.SetToolTip(this.chkVoidOnly, "Include only voided checks");
            // 
            // chkGrossWages
            // 
            this.chkGrossWages.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkGrossWages.Checked = true;
            this.chkGrossWages.Location = new System.Drawing.Point(547, 77);
            this.chkGrossWages.Name = "chkGrossWages";
            this.chkGrossWages.Size = new System.Drawing.Size(194, 27);
            this.chkGrossWages.TabIndex = 27;
            this.chkGrossWages.Text = "Show Wages as Gross";
            this.ToolTip1.SetToolTip(this.chkGrossWages, null);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtEndDate);
            this.Frame2.Controls.Add(this.txtStartDate);
            this.Frame2.Controls.Add(this.txtEmployeeNumber);
            this.Frame2.Controls.Add(this.lblDash);
            this.Frame2.Controls.Add(this.lblCaption);
            this.Frame2.Location = new System.Drawing.Point(30, 210);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(633, 90);
            this.Frame2.TabIndex = 16;
            this.Frame2.Text = "Parameters";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEndDate.Location = new System.Drawing.Point(493, 30);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(120, 40);
            this.txtEndDate.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.txtEndDate, null);
            this.txtEndDate.Visible = false;
            //this.txtEndDate.Enter += new System.EventHandler(this.txtEndDate_Enter);
            //this.txtEndDate.TextChanged += new System.EventHandler(this.txtEndDate_TextChanged);
            // 
            // txtStartDate
            // 
            this.txtStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtStartDate.Location = new System.Drawing.Point(329, 30);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(120, 40);
            this.txtStartDate.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.txtStartDate, null);
            this.txtStartDate.Visible = false;
            //this.txtStartDate.Enter += new System.EventHandler(this.txtStartDate_Enter);
            //this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_TextChanged);
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeNumber.Location = new System.Drawing.Point(202, 30);
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Size = new System.Drawing.Size(97, 40);
            this.txtEmployeeNumber.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtEmployeeNumber, null);
            // 
            // lblDash
            // 
            this.lblDash.Location = new System.Drawing.Point(469, 44);
            this.lblDash.Name = "lblDash";
            this.lblDash.Size = new System.Drawing.Size(4, 26);
            this.lblDash.TabIndex = 21;
            this.lblDash.Text = "-";
            this.ToolTip1.SetToolTip(this.lblDash, null);
            this.lblDash.Visible = false;
            // 
            // lblCaption
            // 
            this.lblCaption.Location = new System.Drawing.Point(20, 44);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(143, 25);
            this.lblCaption.TabIndex = 17;
            this.lblCaption.Text = "EMPLOYEE NUMBER";
            this.ToolTip1.SetToolTip(this.lblCaption, null);
            // 
            // chkVoid
            // 
            this.chkVoid.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkVoid.Location = new System.Drawing.Point(547, 30);
            this.chkVoid.Name = "chkVoid";
            this.chkVoid.Size = new System.Drawing.Size(195, 27);
            this.chkVoid.TabIndex = 7;
            this.chkVoid.Text = "Include Voided Checks";
            this.ToolTip1.SetToolTip(this.chkVoid, null);
            this.chkVoid.CheckedChanged += new System.EventHandler(this.chkVoid_CheckedChanged);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Print / Preview";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(318, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(158, 48);
            this.cmdSave.Text = "Print / Preview";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmCheckListing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(851, 475);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCheckListing";
            this.ShowInTaskbar = false;
            this.Text = "Check Listing Reports";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmCheckListing_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCheckListing_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoidOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrossWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkVoid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
    }
}
