//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public partial class frmPayCategories : BaseForm
	{
		public frmPayCategories()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPayCategories InstancePtr
		{
			get
			{
				return (frmPayCategories)Sys.GetInstance(typeof(frmPayCategories));
			}
		}

		protected frmPayCategories _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 30,2001
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbPayCategories     As DAO.Database
		private clsDRWrapper rsPayCategories = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		private DialogResult intDataChanged;
		private int intNumRowsLocked;
        clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsPayCategories.Row < 0)
					return;
				if (vsPayCategories.Col < 0)
					return;
				if (!modGlobalRoutines.ValidToDelete(modGlobalVariables.gEnumCodeValidation.PayCategory, vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, 20)))
					return;
				if (MessageBox.Show("This action will delete record #" + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, (int)CatGridCol.CategoryNumber) + ". Continue?", "Payroll Deduction Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// we don't want to remove this record from the table as there is still
					// a deduction number ??? be we want to clear it out.
					if (vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, (int)CatGridCol.CategoryNumber) < 6)
					{
						rsPayCategories.Execute("Update tblPayCategories set Multi = 1" + ",Type = 'Hours'" + ", TaxStatus = " + "2, MSR = " + "1, Unemployment = " + "1, ExemptCode1 = " + "0, ExemptType1 = 'B'" + ", ExemptCode2 = " + "0, ExemptType2 = 'B'" + ", ExemptCode3 = " + "0, ExemptType3 = 'B'" + ", ExemptCode4 = " + "0, ExemptType4 = 'B'" + ", ExemptCode5 = " + "0, ExemptType5 = 'B'" + ", LastUserID ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(modGlobalVariables.Statics.gstrUser)) + "' Where ID = " + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, 20), "Payroll");
					}

					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsPayCategories);
					intTotalNumberOfControls -= 1;
					// clear the edit symbol from the row number
					vsPayCategories.TextMatrix(vsPayCategories.Row, 0, Strings.Mid(vsPayCategories.TextMatrix(vsPayCategories.Row, 0), 1, (vsPayCategories.TextMatrix(vsPayCategories.Row, 0).Length < 2 ? 2 : vsPayCategories.TextMatrix(vsPayCategories.Row, 0).Length - 2)));
					// load the grid with the new data to show the 'clean out' of the current row
					LoadGrid();
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void NEWRECORD()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strNumber;
				// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
				int intCount;
				// vbPorter upgrade warning: aryData As object	OnWrite(string())
				string[] aryData = null;
				// vbPorter upgrade warning: aryData2 As object	OnWrite(string())
				string[] aryData2 = null;
				clsDRWrapper rsData = new clsDRWrapper();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
				int intRow = 0;
				// we need to ask which deduction number they want to add so if we currently only
				// have 5 records and they want to set #100 then we need to add records 6 - 100
				strNumber = Interaction.InputBox("Enter Pay Category Number to add.", "Payroll Pay Category Setup", FCConvert.ToString(FCConvert.ToInt32(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Rows - 1, (int)CatGridCol.CategoryNumber))) + 1));
				// strNumber = vsPayCategories.Rows - 2
				if (Conversion.Val(strNumber) > 0)
				{
					for (x = 3; x <= vsPayCategories.Rows - 1; x++)
					{
						if (Conversion.Val(vsPayCategories.TextMatrix(x, (int)CatGridCol.CategoryNumber)) == FCConvert.ToDouble(strNumber))
						{
							MessageBox.Show("This category has already been used", "Duplicate Category", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
					}
					// x
					vsPayCategories.Rows += 1;
					intRow = (vsPayCategories.Rows - 1);
					// increment the counter as to how many records are dirty
					intDataChanged += 1;
					// assign a row number to this new record
					vsPayCategories.TextMatrix(intRow, 0, vsPayCategories.TextMatrix(intRow, 0) + ".");
					// add the deduction number to the first visible column
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.CategoryNumber, FCConvert.ToString(Conversion.Val(strNumber)));
					vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, (int)CatGridCol.CategoryNumber, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// set defaults
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.Description, "");
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.Multi, FCConvert.ToString(1));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.Type, "Hours");
					aryData = Strings.Split(vsPayCategories.ColComboList((int)CatGridCol.TaxStatus), "|", -1, CompareConstants.vbBinaryCompare);
					for (intCount = 0; intCount <= Information.UBound(aryData, 1); intCount++)
					{
						aryData2 = Strings.Split(FCConvert.ToString(aryData[intCount]), ";", -1, CompareConstants.vbBinaryCompare);
						if (Strings.Left(FCConvert.ToString(aryData2[1]), 1) == "T")
						{
							vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intRow, (int)CatGridCol.TaxStatus, Strings.Mid(FCConvert.ToString(aryData2[0]), 2, FCConvert.ToString(aryData2[0]).Length));
							break;
						}
					}

					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.MSR, FCConvert.ToString(1));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.Unemployment, FCConvert.ToString(1));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.WorkersComp, FCConvert.ToString(1));
                    vsPayCategories.TextMatrix(intRow, (int) CatGridCol.WorkedHours, true);
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptCode1, FCConvert.ToString(0));
                    vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptType1, "B\tBoth");
                    vsPayCategories.TextMatrix(intRow,(int)CatGridCol.ExemptCode2, FCConvert.ToString(0));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptType2, "B\tBoth");
					vsPayCategories.TextMatrix(intRow,(int)CatGridCol.ExemptCode3, FCConvert.ToString(0));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptType3, "B\tBoth");
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptCode4, FCConvert.ToString(0));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptType4, "B\tBoth");
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptCode5, FCConvert.ToString(0));
					vsPayCategories.TextMatrix(intRow, (int)CatGridCol.ExemptType5, "B\tBoth");
					vsPayCategories.Select(vsPayCategories.Rows - 1, (int)CatGridCol.Description);
					vsPayCategories.Focus();
					vsPayCategories.LeftCol = (int)CatGridCol.CategoryNumber;
				}
				else
				{
					return;
				}
                
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Pay Category Setup");
				// ------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// show the deduction setup report
				// rptPayCategoriesetup.Show
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				SaveChanges();
				// load the grid of current data from the database
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private bool ValidSave()
		{
			bool ValidSave = false;
			int intCol;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 3; intCounter <= (vsPayCategories.Rows - 1); intCounter++)
			{
				// WOULD LIKE TO FIND A PROPERTY LIKE THE LIST OF THE REGULAR
				// COMBO SO THAT WE DON'T NEED TO MOVE THE CURSOR TO EACH RECORD
				// TO OBTAIN IT'S INDEX.
				if (Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.TaxStatus)) == 0)
				{
					MessageBox.Show("Tax type must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsPayCategories.Select(intCounter, (int)CatGridCol.TaxStatus);
					return ValidSave;
				}
				// ************************************************************************************************
                // IF WE DON'T CHECK TO MAKE SURE THAT THEY CANNOT HAVE THE 9999 CODE
				// ALONG WITH SOME OTHER CODE THEN WE RUN INTO THE CHANCE OF A DEDUCTION
				// BEING EXEMPT FROM A SINGLE DEDUCTION TWICE
				if (FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1)) == "9999")
				{
					if (FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2)) != "0" || FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3)) != "0" || FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4)) != "0" || FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5)) != "0")
					{
						MessageBox.Show("The exempt type of 9999 - ALL can only be placed in Exempt #1 with no other exemptions specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsPayCategories.Select(intCounter, (int)CatGridCol.ExemptCode1);
						return ValidSave;
					}
				}
				// NEED TO NOW CHECK TO SEE IF THERE ARE DUPLICATE CODES IN THE EXEMPT FROM 1-5
				// AS WITH THE 9999 WE CANNOT HAVE A SINGLE PAY CATEGORY ASSIGNED THE SAME
				// DEDUC
				if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1)) != 0)
				{
					if (vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1) || vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1) || vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1) || vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1))
					{
						MessageBox.Show("The exempt type of " + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1) + " can only be specified once for a single pay category.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsPayCategories.Select(intCounter, (int)CatGridCol.ExemptCode1);
						return ValidSave;
					}
				}
				if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2)) != 0)
				{
					if (vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2) || vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2) || vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2))
					{
						MessageBox.Show("The exempt type of " + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2) + " can only be specified once for a single pay category.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsPayCategories.Select(intCounter, (int)CatGridCol.ExemptCode2);
						return ValidSave;
					}
				}
				if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3)) != 0)
				{
					if (vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3) || vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3))
					{
						MessageBox.Show("The exempt type of " + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3) + " can only be specified once for a single pay category.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsPayCategories.Select(intCounter, (int)CatGridCol.ExemptCode3);
						return ValidSave;
					}
				}
				if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4)) != 0)
				{
					if (vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5) == vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4))
					{
						MessageBox.Show("The exempt type of " + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4) + " can only be specified once for a single pay category.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsPayCategories.Select(intCounter, (int)CatGridCol.ExemptCode4);
						return ValidSave;
					}
				}
				if (FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2)) == "9999" || FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3)) == "9999" || FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4)) == "9999" || FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5)) == "9999")
				{
					MessageBox.Show("The exempt type of 9999 - ALL can only be placed in Exempt #1 with no other exemptions specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					vsPayCategories.Select(intCounter, (int)CatGridCol.ExemptCode1);
					return ValidSave;
				}
				// ************************************************************************************************
			}
			ValidSave = true;
			return ValidSave;
		}

		private int convertit(bool boolValue)
		{
			int convertit = 0;
			if (boolValue)
			{
				convertit = 1;
			}
			else
			{
				convertit = 0;
			}
			return convertit;
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				vsPayCategories.Select(0, 0);
				if (!ValidSave())
					return;
                // Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					if (clsControlInfo[x].GridRow < vsPayCategories.Rows)
					{
						clsControlInfo[x].FillNewValue(this);
					}
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Pay Category Setup", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				for (intCounter = 1; intCounter <= (vsPayCategories.Rows - 1); intCounter++)
				{
					if (Strings.Right(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
					{
						// record has been edited so we need to update it
						if (NotValidData())
							goto ExitRoutine;

						string description = FCConvert.ToString(modGlobalRoutines.FixQuote(
							vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Description)));

						rsPayCategories.Execute("Update tblPayCategories set Description ='" + description + "', Multi = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Multi))) + ",Type = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Type)))) + "', TaxStatus = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.TaxStatus))) + ", MSR = " + FCConvert.ToString(convertit(FCConvert.ToBoolean(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.MSR)))) + ", Unemployment = " + FCConvert.ToString(convertit(FCConvert.ToBoolean(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Unemployment)))) + ", WorkersComp = " + FCConvert.ToString(convertit(FCConvert.ToBoolean(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.WorkersComp)))) + ", ExemptCode1 = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter,(int) CatGridCol.ExemptCode1))) + ", ExemptType1 = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType1))) + " ', ExemptCode2 = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2))) + ", ExemptType2 = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType2))) + " ', ExemptCode3 = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3))) + ", ExemptType3 = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType3))) + " ', ExemptCode4 = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4))) + ", ExemptType4 = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType4))) + " ', ExemptCode5 = " + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5))) + ", ExemptType5 = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType5))) + " ', LastUserID ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(modGlobalVariables.Statics.gstrUser)) + "' Where ID = " + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ID), "Payroll");
						// clear the edit symbol from the row number
						vsPayCategories.TextMatrix(intCounter, 0, Strings.Mid(vsPayCategories.TextMatrix(intCounter, 0), 1, vsPayCategories.TextMatrix(intCounter, 0).Length - 2));
						//goto NextRecord;
					}
					else if (Strings.Right(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
					{
						// record has been added so we need to save it
						if (NotValidData())
							goto ExitRoutine;
						rsPayCategories.Execute("Insert into tblPayCategories " + "(CategoryNumber,Description,Multi,Type,TaxStatus," + "MSR,Unemployment,WorkersComp,ExemptCode1,ExemptType1," + "ExemptCode2,ExemptType2,ExemptCode3,ExemptType3,ExemptCode4,ExemptType4,ExemptCode5,ExemptType5," + "LastUserID) VALUES (" + vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.CategoryNumber) + ",'" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Description))) + " '," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Multi))) + ",'" + fecherFoundation.Strings.Trim(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Type))) + "'," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.TaxStatus))) + "," + FCConvert.ToString((FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.MSR)) == string.Empty ? 0 : convertit(FCConvert.ToBoolean(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.MSR))))) + "," + FCConvert.ToString((FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.Unemployment)) == string.Empty ? 0 : convertit(FCConvert.ToBoolean(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7))))) + "," + FCConvert.ToString((FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.WorkersComp)) == string.Empty ? 0 : convertit(FCConvert.ToBoolean(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.WorkersComp))))) + "," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode1))) + ",'" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType1))) + " '," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode2))) + ",'" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType2))) + " '," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode3))) + ",'" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType3))) + " '," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode4))) + ",'" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType4))) + " '," + FCConvert.ToString(Conversion.Val(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, (int)CatGridCol.ExemptCode5))) + ",'" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, (int)CatGridCol.ExemptType5))) + " ','" + modGlobalVariables.Statics.gstrUser + "' )", "Payroll");
						// clear the edit symbol from the row number
						vsPayCategories.TextMatrix(intCounter, 0, Strings.Mid(vsPayCategories.TextMatrix(intCounter, 0), 1, vsPayCategories.TextMatrix(intCounter, 0).Length - 1));
						// get the new ID from the table for the record that was just added 
						rsPayCategories.OpenRecordset("Select Max(ID) as NewID from tblPayCategories", "TWPY0000.vb1");
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ID, FCConvert.ToString(rsPayCategories.Get_Fields("NewID")));
					}

                    var catID = vsPayCategories.TextMatrix(intCounter, (int) CatGridCol.ID).ToIntegerValue();
                    if (catID > 0)
                    {
                        rsPayCategories.OpenRecordset("select * from CategoryTags where PayCategory = " + catID,
                            "Payroll");
                        if (rsPayCategories.EndOfFile())
                        {
                            if (vsPayCategories.TextMatrix(intCounter, (int) CatGridCol.WorkedHours).ToBoolean())
                            {
                                rsPayCategories.AddNew();
                                rsPayCategories.Set_Fields("PayCategory", catID);
                                rsPayCategories.Set_Fields("Tag", "Worked Hours");
                                rsPayCategories.Update();
                            }
                        }
                        else
                        {
                            if (!vsPayCategories.TextMatrix(intCounter, (int) CatGridCol.WorkedHours).ToBoolean())
                            {
                                rsPayCategories.Delete();
                                rsPayCategories.Update();
                            }
                        }
                    }

                    NextRecord:
					;
				}
				// MAKE SURE THAT THEY ONLY HAVE CODE1 SET TO 9999 AND NOT CODE2-CODE5
				// IF THESE OTHER ONES ARE SET THEN THE DEDUCTION GROSS AMOUNT BECOMES NEGATIVE

				rsPayCategories.Execute("Update tblPayCategories Set ExemptCode2 = 0, ExemptCode3=0, ExemptCode4=0, ExemptCode5=0 where ExemptCode1 = 9999", "Payroll");
				clsHistoryClass.Compare("", 2);
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				return;
				ExitRoutine:
				;
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
            return NotValidData;
		}

		private void frmPayCategories_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
				// vsPayCategories.Select 0, 0
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayCategories_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (Shift == 1 && KeyCode == Keys.F12)
				{
					modGlobalVariables.Statics.gobjEditFormName = this;
					frmEditLockedForm.InstancePtr.Show();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayCategories_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPayCategories_Load(object sender, System.EventArgs e)
		{

			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int counter;
				// vsElasticLight1.Enabled = True
				// Debug.Print Me.Height
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				rsPayCategories.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				// Load the grid with data
				LoadGrid();
				// lock the first 5 rows
				// vsPayCategories.Editable = False
				intNumRowsLocked = 8;
				this.vsPayCategories.FrozenCols = 2;
				this.vsPayCategories.FrozenRows = 5;
				clsHistoryClass.SetGridIDColumn("PY", "vsPayCategories", (int)CatGridCol.ID);
                // This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCount;
				intCount = 1;
				vsPayCategories.Rows = 3;
				// get all of the records from the database

                var tagSet = new HashSet<int>();

                rsPayCategories.OpenRecordset(
                    "select * from CategoryTags where [Tag] = 'Worked Hours' order by PayCategory", "Payroll");
                while (!rsPayCategories.EndOfFile())
                {
                    tagSet.Add(rsPayCategories.Get_Fields_Int32("PayCategory"));
                    rsPayCategories.MoveNext();
                }
				rsPayCategories.OpenRecordset("Select * from tblPayCategories Order by CategoryNumber", "TWPY0000.vb1");
				if (!rsPayCategories.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsPayCategories.MoveLast();
					rsPayCategories.MoveFirst();
					// set the number of rows in the grid
					vsPayCategories.Rows = rsPayCategories.RecordCount() + 3;
					// settings to merge the grid headers
					vsPayCategories.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
					vsPayCategories.MergeRow(0, true);
					vsPayCategories.MergeRow(1, true);
					
                    vsPayCategories.TextMatrix(0, (int)CatGridCol.ExemptType2, "Exempt");
                    vsPayCategories.TextMatrix(0, (int)CatGridCol.ExemptCode3, "From");
                    for (intCounter = (int)CatGridCol.ExemptCode1; intCounter <= (int)CatGridCol.ExemptType5; intCounter += 2)
					{
						vsPayCategories.TextMatrix(1, intCounter, "#" + FCConvert.ToString(intCount));
						intCount += 1;
					}
					// fill the grid
					for (intCounter = 3; intCounter <= (rsPayCategories.RecordCount() + 2); intCounter++)
					{
						vsPayCategories.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.CategoryNumber, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Int32("CategoryNumber") + " "));
						vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, (int)CatGridCol.CategoryNumber, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.Description, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields("Description") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.Multi, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Double("Multi") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.Type, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields("Type") + " "));
						if (FCConvert.ToString(rsPayCategories.Get_Fields("Type")) == "Non-Pay (Hours)")
						{
							vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, (int)CatGridCol.Multi, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.TaxStatus, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Int32("TaxStatus") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.MSR, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Boolean("MSR") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.Unemployment, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields("Unemployment") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.WorkersComp, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Boolean("WorkersComp") + " "));
                        if (tagSet.Contains(rsPayCategories.Get_Fields_Int32("ID")))
                        {
                            vsPayCategories.TextMatrix(intCounter, (int) CatGridCol.WorkedHours, true);
                        }
                        else
                        {
                            vsPayCategories.TextMatrix(intCounter, (int) CatGridCol.WorkedHours, false);
                        }
                        vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptCode1, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields("ExemptCode1") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptType1, FixComboCellValue(fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_String("ExemptType1") + " ")));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptCode2, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields("ExemptCode2") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptType2, FixComboCellValue(fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_String("ExemptType2") + " ")));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptCode3, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Int32("ExemptCode3") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptType3, FixComboCellValue(fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_String("ExemptType3") + " ")));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptCode4, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Int32("ExemptCode4") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptType4, FixComboCellValue(fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_String("ExemptType4") + " ")));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptCode5, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_Int32("ExemptCode5") + " "));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ExemptType5, FixComboCellValue(fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields_String("ExemptType5") + " ")));
						vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.ID, fecherFoundation.Strings.Trim(rsPayCategories.Get_Fields("ID") + " "));
						rsPayCategories.MoveNext();
					}

					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
				vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, (int)CatGridCol.CategoryNumber, 7, (int)CatGridCol.Description, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsPayCategories.Select(3, (int)CatGridCol.Multi);
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

        //FC:FINAL:AM:#2573 - fix wrong value saved in the db
        public string FixComboCellValue(string text)
        {
            if (text.StartsWith("B"))
            {
                return "B\tBoth";
            }
            else
            {
                return text;
            }
        }

		public void LockGridCells(int intStart)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LockGridCells";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (intStart >= vsPayCategories.Rows - 1)
					return;
				vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 0, intStart + 1, (int)CatGridCol.Description, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				if (intStart < vsPayCategories.Rows - 1)
				{
					vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intStart + 3, (int)CatGridCol.Description, (vsPayCategories.Rows - 3), (int)CatGridCol.Description, Color.White);
				}
				intNumRowsLocked = intStart;
				vsPayCategories.Refresh();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                
                vsPayCategories.FixedRows = 3;
                vsPayCategories.Cols = 21;
				vsPayCategories.ColHidden(0, true);

				vsPayCategories.ColHidden((int)CatGridCol.ID, true);
				// align the captions
				modGlobalRoutines.CenterGridCaptions(ref vsPayCategories);
                // set column 0 properties
				vsPayCategories.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCategories.ColWidth(0, 400);
				
				vsPayCategories.ColAlignment((int)CatGridCol.CategoryNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCategories.ColWidth((int)CatGridCol.CategoryNumber, 300);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.CategoryNumber, "#");
				
				vsPayCategories.ColAlignment((int)CatGridCol.Description, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCategories.ColWidth((int)CatGridCol.Description, 1800);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.Description, "Description");
			
				vsPayCategories.ColAlignment((int)CatGridCol.Multi, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                //FC:FINAL:BSE #2571 column should allow only numbers
                vsPayCategories.ColAllowedKeys((int)CatGridCol.Multi, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                vsPayCategories.ColWidth((int)CatGridCol.Multi, 700);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.Multi, "Mult");

				vsPayCategories.ColAlignment((int)CatGridCol.Type, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCategories.ColWidth((int)CatGridCol.Type, 900);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.Type, "Type");
				vsPayCategories.ColComboList((int)CatGridCol.Type, "#1;Dollars|#2;Hours|#3;Non-Pay (Hours)");
				// set column 5 properties
				vsPayCategories.ColAlignment((int)CatGridCol.TaxStatus, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCategories.ColWidth((int)CatGridCol.TaxStatus, 600);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.TaxStatus, "Tax Status");
				rsPayCategories.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsPayCategories.EndOfFile())
				{
					rsPayCategories.MoveLast();
					rsPayCategories.MoveFirst();

					for (intCounter = 1; intCounter <= (rsPayCategories.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							vsPayCategories.ColComboList((int)CatGridCol.TaxStatus, vsPayCategories.ColComboList((int)CatGridCol.TaxStatus) + "#" + rsPayCategories.Get_Fields("ID") + ";" + rsPayCategories.Get_Fields("TaxStatusCode") + "\t" + " " + rsPayCategories.Get_Fields("Description"));
						}
						else if (rsPayCategories.Get_Fields("TaxStatusCode") == "T")
						{
							vsPayCategories.ColComboList((int)CatGridCol.TaxStatus, vsPayCategories.ColComboList((int)CatGridCol.TaxStatus) + "|#" + rsPayCategories.Get_Fields("ID") + ";" + rsPayCategories.Get_Fields("TaxStatusCode") + "\t" + " " + rsPayCategories.Get_Fields("Description"));
						}
						else if (intCounter > 1)
						{
							vsPayCategories.ColComboList((int)CatGridCol.TaxStatus, vsPayCategories.ColComboList((int)CatGridCol.TaxStatus) + "|#" + rsPayCategories.Get_Fields("ID") + ";" + rsPayCategories.Get_Fields("TaxStatusCode") + "\t" + " " + rsPayCategories.Get_Fields("Description") + " Exempt");
						}
						else
						{
							vsPayCategories.ColComboList((int)CatGridCol.TaxStatus, vsPayCategories.ColComboList((int)CatGridCol.TaxStatus) + "|#" + rsPayCategories.Get_Fields("ID") + ";" + rsPayCategories.Get_Fields("TaxStatusCode") + "\t" + " " + rsPayCategories.Get_Fields("Description"));
						}
						rsPayCategories.MoveNext();
					}
				}
                vsPayCategories.ColDataType((int)CatGridCol.MSR, FCGrid.DataTypeSettings.flexDTBoolean);
                vsPayCategories.ColAlignment((int)CatGridCol.MSR, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsPayCategories.ColWidth((int)CatGridCol.MSR, 200);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.MSR, "M");
                // set column 7 properties
                vsPayCategories.ColDataType((int)CatGridCol.Unemployment, FCGrid.DataTypeSettings.flexDTBoolean);
                vsPayCategories.ColAlignment((int)CatGridCol.Unemployment, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsPayCategories.ColWidth((int)CatGridCol.Unemployment, 200);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.Unemployment, "U");

                vsPayCategories.ColDataType((int)CatGridCol.WorkersComp, FCGrid.DataTypeSettings.flexDTBoolean);
                vsPayCategories.ColAlignment((int)CatGridCol.WorkersComp, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsPayCategories.ColWidth((int)CatGridCol.WorkersComp, 200);
				vsPayCategories.TextMatrix(2, (int)CatGridCol.WorkersComp, "W");

                vsPayCategories.ColDataType((int) CatGridCol.WorkedHours, FCGrid.DataTypeSettings.flexDTBoolean);
                vsPayCategories.ColAlignment((int) CatGridCol.WorkedHours,
                    FCGrid.AlignmentSettings.flexAlignCenterCenter);
                vsPayCategories.ColWidth((int)CatGridCol.WorkedHours,200);
                vsPayCategories.TextMatrix(1, (int) CatGridCol.WorkedHours, "Worked");
                vsPayCategories.TextMatrix(2, (int) CatGridCol.WorkedHours, "Hours");

				int intExemptCount;
				intExemptCount = 1;
				for (intCounter = (int)CatGridCol.ExemptCode1; intCounter <= (int)CatGridCol.ExemptType5; intCounter += 2)
				{
                    vsPayCategories.ColAlignment(intCounter, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsPayCategories.ColWidth(intCounter, 800);
					vsPayCategories.TextMatrix(2, intCounter, "Ded ");
					rsPayCategories.OpenRecordset("Select * from tblDeductionSetup order by DeductionNumber", "TWPY0000.vb1");
					if (!rsPayCategories.EndOfFile())
					{
						rsPayCategories.MoveLast();
						rsPayCategories.MoveFirst();
						// This forces this column to work as a combo box
						vsPayCategories.ColComboList(intCounter, vsPayCategories.ColComboList(intCounter) + "#0;0|#9999;9999\tALL");
						// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
						int intCounter2;
						for (intCounter2 = 1; intCounter2 <= (rsPayCategories.RecordCount()); intCounter2++)
						{
							// If intCounter2 = 1 Then
							// vsPayCategories.ColComboList(intCounter, vsPayCategories.ColComboList(intCounter) & "#" & rsPayCategories.Fields("ID") & ";" & rsPayCategories.Fields("DeductionNumber") & IIf(Trim(rsPayCategories.Fields("Description")) <> "", "            - " & rsPayCategories.Fields("Description"), "")
							// Else
							vsPayCategories.ColComboList(intCounter, vsPayCategories.ColComboList(intCounter) + "|#" + rsPayCategories.Get_Fields("ID") + ";" + rsPayCategories.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsPayCategories.Get_Fields("Description"))) != "" ? "\t" + rsPayCategories.Get_Fields("Description") : ""));
							// End If
							rsPayCategories.MoveNext();
						}
					}
                    vsPayCategories.ColAlignment(intCounter + 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsPayCategories.ColWidth(intCounter + 1, 800);
					vsPayCategories.TextMatrix(2, intCounter + 1, "Type");

                    vsPayCategories.ColComboList(intCounter + 1, "R\tEmployer|E\tEmployee|B\tBoth|XR\tExempt All But (Employer)|XE\tExempt All But (Employee)|XB\tExempt All But (Both)");
                    intExemptCount += 1;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayCategories_Resize(object sender, System.EventArgs e)
		{
			vsPayCategories.ColWidth(0, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.05));
			vsPayCategories.ColWidth((int)CatGridCol.CategoryNumber, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.05));
			vsPayCategories.ColWidth((int)CatGridCol.Description, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.25));
			vsPayCategories.ColWidth((int)CatGridCol.Multi, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.06));
			vsPayCategories.ColWidth((int)CatGridCol.Type, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.09));
			vsPayCategories.ColWidth((int)CatGridCol.TaxStatus, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.06));
			vsPayCategories.ColWidth((int)CatGridCol.MSR, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.05));
			vsPayCategories.ColWidth((int)CatGridCol.Unemployment, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.05));
			vsPayCategories.ColWidth((int)CatGridCol.WorkersComp, FCConvert.ToInt32(vsPayCategories.WidthOriginal * 0.05));
            vsPayCategories.ColWidth((int) CatGridCol.WorkedHours, (vsPayCategories.WidthOriginal * .05).ToInteger());
        }
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsPayCategories.Select(0, 0);
				SaveChanges();
				if (intDataChanged == (DialogResult)2)
				{
					e.Cancel = true;
					return;
				}
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			NEWRECORD();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void vsPayCategories_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCategories_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsPayCategories.TextMatrix(vsPayCategories.Row, 0, vsPayCategories.TextMatrix(vsPayCategories.Row, 0) + "..");
				}
				if (vsPayCategories.Col == (int)CatGridCol.Type)
				{
					if (vsPayCategories.TextMatrix(vsPayCategories.Row, (int)CatGridCol.Type) != "3")
					{
						vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCategories.Row, (int)CatGridCol.Multi, Color.White);
					}
					if (vsPayCategories.TextMatrix(vsPayCategories.Row, vsPayCategories.Col) == "1")
					{
						vsPayCategories.TextMatrix(vsPayCategories.Row, (int)CatGridCol.Multi, "1");
					}
					else if (vsPayCategories.TextMatrix(vsPayCategories.Row, (int)CatGridCol.Type) == "3")
					{
						// non-pay
						vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCategories.Row, (int)CatGridCol.Multi, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsPayCategories_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsPayCategories.Col == (int)CatGridCol.Description || vsPayCategories.Col == (int)CatGridCol.Multi)
			{
				if (KeyCode == (Keys)37)
				{
					// left key
					if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCategories.Row, (int)CatGridCol.Description)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						if (vsPayCategories.Row > 3)
						{
							vsPayCategories.Row -= 1;
							vsPayCategories.Col = (int)CatGridCol.ExemptType5;
							vsPayCategories.LeftCol = (int)CatGridCol.ExemptType5;
						}
					}
					else
					{
						if (vsPayCategories.Col == (int)CatGridCol.Description)
						{
							if (vsPayCategories.Row > 3)
							{
								vsPayCategories.Row -= 1;
								vsPayCategories.Col = (int)CatGridCol.ExemptType5;
								vsPayCategories.LeftCol = (int)CatGridCol.ExemptType5;
							}
						}
						else
						{
							vsPayCategories.Col = (int)CatGridCol.Description;
							vsPayCategories.LeftCol = (int)CatGridCol.CategoryNumber;
						}
					}
					KeyCode = 0;
					return;
				}
			}
			// end of grid with arrow keys
			if (vsPayCategories.Col == (int)CatGridCol.ExemptType5)
			{
				// right arrow key
				if (KeyCode == (Keys)39)
				{
					NewRow:
					;
					if (vsPayCategories.Row != vsPayCategories.Rows - 1)
					{
						vsPayCategories.Row += 1;
						if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCategories.Row, (int)CatGridCol.Multi)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							vsPayCategories.Col = (int)CatGridCol.Multi;
						}
						else
						{
							vsPayCategories.Col = (int)CatGridCol.Description;
						}
						vsPayCategories.LeftCol = (int)CatGridCol.CategoryNumber;
						KeyCode = 0;
					}
					else
					{
						NEWRECORD();
						KeyCode = 0;
					}
				}
			}
		}

		private void vsPayCategories_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCategories_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsPayCategories.Col == (int)CatGridCol.ExemptType5)
					{
						if (vsPayCategories.Row < vsPayCategories.Rows - 1)
						{
							vsPayCategories.Row += 1;
							vsPayCategories.Col = (int)CatGridCol.CategoryNumber;
						}
						else
						{
							// if there is no other row then create a new one
							NEWRECORD();
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
				// Call ShowHelpScreen(KeyCode)
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsPayCategories_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			try
			{
                //FC:FINAL:AM:#2576 - in VB6 KeyPressEdit is not fired for the TAB and ENTER keys
                if (e.KeyChar == 9 || e.KeyChar == 13)
                {
                    return;
                }
                // On Error GoTo CallErrorRoutine
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCategories_KeyPressEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int KeyAscii = Strings.Asc(e.KeyChar);
				if (vsPayCategories.Col == (int)CatGridCol.Multi && fecherFoundation.Strings.UCase(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, (int)CatGridCol.Type))) == "DOLLARS")
				{
					if (KeyAscii != FCConvert.ToInt32(Keys.D1))
					{
						MessageBox.Show("If type is dollars, only a 1 can be chosen.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        vsPayCategories.TextMatrix(vsPayCategories.Row, vsPayCategories.Col, FCConvert.ToString(1));
                        //KeyAscii = 0;
                        //e.KeyChar = Strings.Chr(KeyAscii);
                        return;
					}
				}
				if (vsPayCategories.Col == (int)CatGridCol.Multi)
				{
					// allow the entry of a decimal point
					if (KeyAscii == 46)
						return;
					if (KeyAscii == 8)
						return;
					// if the user is in the MULTI columns then do not
					// allow the entry of a character
					if (KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9))
					{
						KeyAscii = 0;
					}
				}
				e.KeyChar = Strings.Chr(KeyAscii);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsPayCategories_KeyUpEvent(object sender, KeyEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCategories_KeyUp";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// show the help screen according to which column the user is currently in
				// Call ShowHelpScreen(KeyCode)
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

        private void VsPayCategories_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(this.vsPayCategories_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(this.vsPayCategories_KeyDownEdit);
            }
        }

        private void vsPayCategories_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (vsPayCategories.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                int col = vsPayCategories.GetFlexColIndex(e.ColumnIndex);
                DataGridViewCell cell = vsPayCategories[e.ColumnIndex, e.RowIndex];
                switch (col)
                {
                    case (int)CatGridCol.ExemptType1:
                    case (int)CatGridCol.ExemptType2:
                    case (int)CatGridCol.ExemptType3:
                    case (int)CatGridCol.ExemptType4:
                    case (int)CatGridCol.ExemptType5:
                        {
                            cell.ToolTipText = "R  - Employer;E  - Employee;B  - Both;XR - Exempt All But (Employer);XE - Exempt All But (Employee);XB - Exempt All But (Both)";
                            return;
                        }
                    default:
                        {
                            break;
                        }
                }
                //end switch
                if (col == (int)CatGridCol.MSR)
                {
                    cell.ToolTipText = "Maine State Retirement System";
                    return;
                }
                if (col == (int)CatGridCol.Unemployment)
                {
                    cell.ToolTipText = "Unemployment";
                    return;
                }
                if (col == (int)CatGridCol.WorkersComp)
                {
                    cell.ToolTipText = "Workers Compensation";
                    return;
                }
                if (col == (int)CatGridCol.Multi)
                {
                    if (fecherFoundation.Strings.UCase(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, (int)CatGridCol.Type))) == "DOLLARS")
                    {
                        cell.ToolTipText = "If type is dollars, only a 1 can be chosen";
                    }
                    else
                    {
                        cell.ToolTipText = "";
                    }
                }
            }
        }

        private void vsPayCategories_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCategories_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// select all of the data in the new cell
				// vsPayCategories.EditCell
				// vsPayCategories.EditSelStart = 0
				// vsPayCategories.EditSelLength = Len(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, vsPayCategories.Col))
				if (vsPayCategories.Row < intNumRowsLocked)
				{
					if (vsPayCategories.Col != (int)CatGridCol.Description)
					{
						vsPayCategories.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						vsPayCategories.Editable = FCGrid.EditableSettings.flexEDNone;
					}
					
				}
				else
				{
					vsPayCategories.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				if (vsPayCategories.Editable == FCGrid.EditableSettings.flexEDKbdMouse && vsPayCategories.Col == (int)CatGridCol.Multi)
				{
					if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCategories.Row, (int)CatGridCol.Multi)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsPayCategories.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}

				if (FCConvert.ToInt32(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCategories.Row, vsPayCategories.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND && vsPayCategories.Col != (int)CatGridCol.Multi)
				{
					vsPayCategories.Select(vsPayCategories.Row, (int)CatGridCol.Multi);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowHelpScreen(int KeyCode)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowHelpScreen";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// this variable is used in the form frmHelp to determine what data to show
				modGlobalVariables.Statics.gstrHelpFieldName = string.Empty;

			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsPayCategories_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsPayCategories.Col == (int)CatGridCol.CategoryNumber)
			{
				for (intCounter = 3; intCounter <= (vsPayCategories.Rows - 1); intCounter++)
				{
					if (vsPayCategories.EditText == vsPayCategories.TextMatrix(intCounter, (int)CatGridCol.CategoryNumber) && intCounter != vsPayCategories.Row)
					{
						MessageBox.Show("Duplicate Numbers Not Allowed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						// vsPayCategories.EditText = vsPayCategories.TextMatrix(Row, Col)
						e.Cancel = true;
						return;
					}
				}
			}
            else if (vsPayCategories.Col == (int)CatGridCol.Multi && fecherFoundation.Strings.UCase(FCConvert.ToString(vsPayCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCategories.Row, (int)CatGridCol.Type))) == "DOLLARS")
            {
                if (vsPayCategories.EditText != "1")
                {
                    MessageBox.Show("If type is dollars, only a 1 can be chosen.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    vsPayCategories.EditText = "1";
                    vsPayCategories.TextMatrix(vsPayCategories.Row, vsPayCategories.Col, "1");
                    vsPayCategories[e.ColumnIndex, e.RowIndex].Update();
                    return;
                }
            }
        }
        // This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 3; intRows <= (vsPayCategories.Rows - 1); intRows++)
			{
				for (intCols = (int)CatGridCol.Description; intCols <= (int)CatGridCol.ExemptType5; intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "vsPayCategories";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + fecherFoundation.Strings.Trim(vsPayCategories.TextMatrix(0, intCols) + " " + vsPayCategories.TextMatrix(1, intCols) + " " + vsPayCategories.TextMatrix(2, intCols));
					intTotalNumberOfControls += 1;
				}
			}
		}

        private enum CatGridCol
        {
			Line = 0,
			CategoryNumber = 1,
			Description = 2,
			Multi = 3,
			Type = 4,
			TaxStatus = 5,
			MSR = 6,
			Unemployment = 7,
			WorkersComp = 8,
			WorkedHours = 9,
			ExemptCode1 = 10,
			ExemptType1 = 11,
			ExemptCode2 = 12,
			ExemptType2 = 13,
			ExemptCode3 = 14,
			ExemptType3 = 15,
			ExemptCode4 = 16,
			ExemptType4 = 17,
			ExemptCode5  =18,
			ExemptType5 = 19,
			ID = 20
        }
	}
}
