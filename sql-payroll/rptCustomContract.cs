//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomContract.
	/// </summary>
	public partial class rptCustomContract : BaseSectionReport
	{
		public rptCustomContract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Contract Setup";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomContract InstancePtr
		{
			get
			{
				return (rptCustomContract)Sys.GetInstance(typeof(rptCustomContract));
			}
		}

		protected rptCustomContract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomContract	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolOnlySummary;
		clsDRWrapper rsReport = new clsDRWrapper();
		clsContract clsConCT = new clsContract();
		bool boolPaidOnly;
		bool boolUnpaidOnly;
		bool boolCustomDist;
		bool boolRecordFound;
		// vbPorter upgrade warning: dtAsOf As DateTime	OnWrite(int, DateTime)
		private DateTime dtAsOf;

		public void Init(bool boolSummary, ref string strSQL, ref int intStatus, bool boolDist, DateTime dtAsOfDate/*= FCConvert.ToDateTime("12:00 AM")*/)
		{
			// status of 0 is all,1 is unpaid and 2 is paid
			boolCustomDist = boolDist;
			dtAsOf = DateTime.FromOADate(0);
			if (dtAsOfDate.ToOADate() != 0)
				dtAsOf = dtAsOfDate;
			boolRecordFound = false;
			if (intStatus == 0)
			{
				boolPaidOnly = false;
				boolUnpaidOnly = false;
			}
			else if (intStatus == 1)
			{
				boolPaidOnly = false;
				boolUnpaidOnly = true;
			}
			else
			{
				boolPaidOnly = true;
				boolUnpaidOnly = false;
			}
			boolOnlySummary = boolSummary;
			rsReport.OpenRecordset(strSQL, "twpy0000.vb1");
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolOnlySummary)
			{
				lblTitle.Text = "Custom Contract Summary";
				this.Name = "Contract Summary";
			}
			else if (!boolDist)
			{
				lblTitle.Text = "Custom Contract Detail";
				this.Name = "Contract Detail";
			}
			else
			{
				lblTitle.Text = "Custom Contract Distribution Detail";
				this.Name = "Contract Distribution Detail";
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", true, "", "TRIO Software", false, true, "Contracts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			TryAgain:
			;
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				clsConCT.LoadContract(FCConvert.ToInt32(Conversion.Val(rsReport.Get_Fields("contractid"))), null, dtAsOf);
				if (boolPaidOnly)
				{
					if (clsConCT.AmountPaid < clsConCT.OriginalAmount)
					{
						rsReport.MoveNext();
						goto TryAgain;
					}
				}
				else if (boolUnpaidOnly)
				{
					if (clsConCT.AmountPaid >= clsConCT.OriginalAmount)
					{
						rsReport.MoveNext();
						goto TryAgain;
					}
				}
				boolRecordFound = true;
			}
			if (rsReport.EndOfFile() && !boolRecordFound)
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//FC:FINAL:BSE:#i2416 Exception when unloading report viewer during FetchData event handler
				//this.Close();
				//frmReportViewer.InstancePtr.Unload();
				this.Cancel();
				return;
			}
		}

		private void RptCustomContract_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
			//FC:FINAL:BSE:#i2416 Unload the viewer only after report is finished
			frmReportViewer.InstancePtr.Unload();
		}


		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			if (!boolOnlySummary && !boolCustomDist)
			{
				SubReport1.Visible = true;
				SubReport1.Report = new srptContractAccounts();
			}
			else if (!boolCustomDist)
			{
				SubReport1.Visible = false;
			}
			else
			{
				SubReport2.Report = new srptContractDistribution();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				string strTemp = "";
				txtEmployeeNumber.Text = rsReport.Get_Fields_String("employeenumber");
				txtName.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields("lastname") + ", " + fecherFoundation.Strings.Trim(rsReport.Get_Fields("firstname") + " " + rsReport.Get_Fields("middlename")) + " " + rsReport.Get_Fields("desig"));
				txtContract.Text = rsReport.Get_Fields_String("contractid");
				txtDescription.Text = rsReport.Get_Fields_String("description");
				strTemp = "";
				if (Information.IsDate(rsReport.Get_Fields("startdate")))
				{
					if (Convert.ToDateTime(rsReport.Get_Fields("startdate")).ToOADate() != 0)
					{
						strTemp = Strings.Format(rsReport.Get_Fields("startdate"), "MM/dd/yyyy");
					}
				}
				txtStart.Text = strTemp;
				strTemp = "";
				if (Information.IsDate(rsReport.Get_Fields("enddate")))
				{
					if (Convert.ToDateTime(rsReport.Get_Fields("enddate")).ToOADate() != 0)
					{
						strTemp = Strings.Format(rsReport.Get_Fields("enddate"), "MM/dd/yyyy");
					}
				}
				txtEnd.Text = strTemp;
				txtAmount.Text = Strings.Format(clsConCT.OriginalAmount, "#,###,##0.00");
				txtPaid.Text = Strings.Format(clsConCT.AmountPaid, "#,###,##0.00");
				txtRemaining.Text = Strings.Format(clsConCT.OriginalAmount - clsConCT.AmountPaid, "#,###,##0.00");
				if (!boolOnlySummary && !boolCustomDist)
				{
					if (dtAsOf.ToOADate() == 0)
					{
						SubReport1.Report.UserData = clsConCT.ContractID;
					}
					else
					{
						SubReport1.Report.UserData = FCConvert.ToString(clsConCT.ContractID) + "," + FCConvert.ToString(dtAsOf);
					}
				}
				else if (boolCustomDist)
				{
					if (dtAsOf.ToOADate() == 0)
					{
						SubReport2.Report.UserData = clsConCT.ContractID;
					}
					else
					{
						SubReport2.Report.UserData = FCConvert.ToString(clsConCT.ContractID) + "," + Strings.Format(dtAsOf, "MM/dd/yyyy");
					}
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
