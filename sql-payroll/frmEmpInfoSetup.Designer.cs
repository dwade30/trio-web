﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmpInfoSetup.
	/// </summary>
	partial class frmEmpInfoSetup
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> Check1;
		public fecherFoundation.FCFrame fraBenefits;
		public FCGrid vsData;
		public fecherFoundation.FCCheckBox Check1_0;
		public fecherFoundation.FCCheckBox Check1_1;
		public fecherFoundation.FCCheckBox Check1_2;
		public fecherFoundation.FCCheckBox Check1_3;
		public fecherFoundation.FCCheckBox Check1_4;
		public fecherFoundation.FCCheckBox Check1_5;
		public fecherFoundation.FCCheckBox chkSinglePerson;
		public fecherFoundation.FCTextBox txtSinglePerson;
		public fecherFoundation.FCCheckBox chkStatus;
		public fecherFoundation.FCTextBox txtDeptDiv;
		public fecherFoundation.FCCheckBox chkDeptDiv;
		public fecherFoundation.FCTextBox txtSequence;
		public fecherFoundation.FCCheckBox chkSequence;
		public fecherFoundation.FCComboBox cboStatus;
		public fecherFoundation.FCCheckBox chkSimple;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddBenefitName;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteBenefit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.fraBenefits = new fecherFoundation.FCFrame();
            this.vsData = new fecherFoundation.FCGrid();
            this.Check1_0 = new fecherFoundation.FCCheckBox();
            this.Check1_1 = new fecherFoundation.FCCheckBox();
            this.Check1_2 = new fecherFoundation.FCCheckBox();
            this.Check1_3 = new fecherFoundation.FCCheckBox();
            this.Check1_4 = new fecherFoundation.FCCheckBox();
            this.Check1_5 = new fecherFoundation.FCCheckBox();
            this.chkSinglePerson = new fecherFoundation.FCCheckBox();
            this.txtSinglePerson = new fecherFoundation.FCTextBox();
            this.chkStatus = new fecherFoundation.FCCheckBox();
            this.txtDeptDiv = new fecherFoundation.FCTextBox();
            this.chkDeptDiv = new fecherFoundation.FCCheckBox();
            this.txtSequence = new fecherFoundation.FCTextBox();
            this.chkSequence = new fecherFoundation.FCCheckBox();
            this.cboStatus = new fecherFoundation.FCComboBox();
            this.chkSimple = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddBenefitName = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteBenefit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.cmdAddBenefitName = new fecherFoundation.FCButton();
            this.cmdDeleteBenefit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBenefits)).BeginInit();
            this.fraBenefits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSinglePerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSequence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSimple)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBenefitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBenefit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(698, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraBenefits);
            this.ClientArea.Controls.Add(this.Check1_0);
            this.ClientArea.Controls.Add(this.Check1_1);
            this.ClientArea.Controls.Add(this.Check1_2);
            this.ClientArea.Controls.Add(this.Check1_3);
            this.ClientArea.Controls.Add(this.Check1_4);
            this.ClientArea.Controls.Add(this.Check1_5);
            this.ClientArea.Controls.Add(this.chkSinglePerson);
            this.ClientArea.Controls.Add(this.txtSinglePerson);
            this.ClientArea.Controls.Add(this.chkStatus);
            this.ClientArea.Controls.Add(this.txtDeptDiv);
            this.ClientArea.Controls.Add(this.chkDeptDiv);
            this.ClientArea.Controls.Add(this.txtSequence);
            this.ClientArea.Controls.Add(this.chkSequence);
            this.ClientArea.Controls.Add(this.cboStatus);
            this.ClientArea.Controls.Add(this.chkSimple);
            this.ClientArea.Size = new System.Drawing.Size(698, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddBenefitName);
            this.TopPanel.Controls.Add(this.cmdDeleteBenefit);
            this.TopPanel.Size = new System.Drawing.Size(698, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteBenefit, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddBenefitName, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(333, 30);
            this.HeaderText.Text = "Employee Information Report";
            // 
            // fraBenefits
            // 
            this.fraBenefits.AppearanceKey = "groupBoxNoBorders";
            this.fraBenefits.Controls.Add(this.vsData);
            this.fraBenefits.Location = new System.Drawing.Point(200, 458);
            this.fraBenefits.Name = "fraBenefits";
            this.fraBenefits.Size = new System.Drawing.Size(447, 170);
            this.fraBenefits.TabIndex = 15;
            this.fraBenefits.Text = "Benefits";
            this.fraBenefits.Visible = false;
            // 
            // vsData
            // 
            this.vsData.Cols = 1;
            this.vsData.ColumnHeadersVisible = false;
            this.vsData.ExtendLastCol = true;
            this.vsData.FixedCols = 0;
            this.vsData.FixedRows = 0;
            this.vsData.Location = new System.Drawing.Point(0, 30);
            this.vsData.Name = "vsData";
            this.vsData.RowHeadersVisible = false;
            this.vsData.Rows = 0;
            this.vsData.Size = new System.Drawing.Size(447, 120);
            this.vsData.TabIndex = 16;
            // 
            // Check1_0
            // 
            this.Check1_0.Checked = true;
            this.Check1_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.Check1_0.Location = new System.Drawing.Point(30, 30);
            this.Check1_0.Name = "Check1_0";
            this.Check1_0.Size = new System.Drawing.Size(130, 27);
            this.Check1_0.TabIndex = 14;
            this.Check1_0.Text = "Direct Deposit";
            // 
            // Check1_1
            // 
            this.Check1_1.Checked = true;
            this.Check1_1.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.Check1_1.Location = new System.Drawing.Point(30, 77);
            this.Check1_1.Name = "Check1_1";
            this.Check1_1.Size = new System.Drawing.Size(187, 27);
            this.Check1_1.TabIndex = 13;
            this.Check1_1.Text = "Employee Deductions";
            // 
            // Check1_2
            // 
            this.Check1_2.Checked = true;
            this.Check1_2.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.Check1_2.Location = new System.Drawing.Point(30, 124);
            this.Check1_2.Name = "Check1_2";
            this.Check1_2.Size = new System.Drawing.Size(146, 27);
            this.Check1_2.TabIndex = 12;
            this.Check1_2.Text = "Employer Match";
            // 
            // Check1_3
            // 
            this.Check1_3.Checked = true;
            this.Check1_3.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.Check1_3.Location = new System.Drawing.Point(287, 30);
            this.Check1_3.Name = "Check1_3";
            this.Check1_3.Size = new System.Drawing.Size(105, 27);
            this.Check1_3.TabIndex = 11;
            this.Check1_3.Text = "Pay Totals";
            // 
            // Check1_4
            // 
            this.Check1_4.Checked = true;
            this.Check1_4.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.Check1_4.Location = new System.Drawing.Point(287, 77);
            this.Check1_4.Name = "Check1_4";
            this.Check1_4.Size = new System.Drawing.Size(190, 27);
            this.Check1_4.TabIndex = 10;
            this.Check1_4.Text = "Vacation / Sick / Other";
            // 
            // Check1_5
            // 
            this.Check1_5.Checked = true;
            this.Check1_5.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.Check1_5.Location = new System.Drawing.Point(287, 124);
            this.Check1_5.Name = "Check1_5";
            this.Check1_5.Size = new System.Drawing.Size(117, 27);
            this.Check1_5.TabIndex = 9;
            this.Check1_5.Text = "MainePERS";
            // 
            // chkSinglePerson
            // 
            this.chkSinglePerson.Location = new System.Drawing.Point(115, 178);
            this.chkSinglePerson.Name = "chkSinglePerson";
            this.chkSinglePerson.Size = new System.Drawing.Size(189, 27);
            this.chkSinglePerson.TabIndex = 8;
            this.chkSinglePerson.Text = "Print Single Employee";
            this.chkSinglePerson.CheckedChanged += new System.EventHandler(this.chkSinglePerson_CheckedChanged);
            // 
            // txtSinglePerson
            // 
            this.txtSinglePerson.BackColor = System.Drawing.SystemColors.Window;
            this.txtSinglePerson.Location = new System.Drawing.Point(361, 171);
            this.txtSinglePerson.Name = "txtSinglePerson";
            this.txtSinglePerson.Size = new System.Drawing.Size(182, 40);
            this.txtSinglePerson.TabIndex = 7;
            this.txtSinglePerson.Visible = false;
            // 
            // chkStatus
            // 
            this.chkStatus.Location = new System.Drawing.Point(115, 238);
            this.chkStatus.Name = "chkStatus";
            this.chkStatus.Size = new System.Drawing.Size(151, 27);
            this.chkStatus.TabIndex = 6;
            this.chkStatus.Text = "Employee Status";
            this.chkStatus.CheckedChanged += new System.EventHandler(this.chkStatus_CheckedChanged);
            // 
            // txtDeptDiv
            // 
            this.txtDeptDiv.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeptDiv.Location = new System.Drawing.Point(361, 291);
            this.txtDeptDiv.Name = "txtDeptDiv";
            this.txtDeptDiv.Size = new System.Drawing.Size(182, 40);
            this.txtDeptDiv.TabIndex = 5;
            this.txtDeptDiv.Visible = false;
            // 
            // chkDeptDiv
            // 
            this.chkDeptDiv.Location = new System.Drawing.Point(115, 298);
            this.chkDeptDiv.Name = "chkDeptDiv";
            this.chkDeptDiv.Size = new System.Drawing.Size(150, 27);
            this.chkDeptDiv.TabIndex = 4;
            this.chkDeptDiv.Text = "Department / Div";
            this.chkDeptDiv.CheckedChanged += new System.EventHandler(this.chkDeptDiv_CheckedChanged);
            // 
            // txtSequence
            // 
            this.txtSequence.BackColor = System.Drawing.SystemColors.Window;
            this.txtSequence.Location = new System.Drawing.Point(361, 351);
            this.txtSequence.Name = "txtSequence";
            this.txtSequence.Size = new System.Drawing.Size(182, 40);
            this.txtSequence.TabIndex = 3;
            this.txtSequence.Visible = false;
            // 
            // chkSequence
            // 
            this.chkSequence.Location = new System.Drawing.Point(115, 358);
            this.chkSequence.Name = "chkSequence";
            this.chkSequence.Size = new System.Drawing.Size(164, 27);
            this.chkSequence.TabIndex = 2;
            this.chkSequence.Text = "Sequence Number";
            this.chkSequence.CheckedChanged += new System.EventHandler(this.chkSequence_CheckedChanged);
            // 
            // cboStatus
            // 
            this.cboStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cboStatus.Items.AddRange(new object[] {
            "Active",
            "Terminated",
            "Suspended",
            "Hold 1 Week",
            "Retired",
            "Resigned"});
            this.cboStatus.Location = new System.Drawing.Point(361, 231);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(182, 40);
            this.cboStatus.TabIndex = 1;
            this.cboStatus.Visible = false;
            // 
            // chkSimple
            // 
            this.chkSimple.Location = new System.Drawing.Point(200, 411);
            this.chkSimple.Name = "chkSimple";
            this.chkSimple.Size = new System.Drawing.Size(334, 27);
            this.chkSimple.TabIndex = 16;
            this.chkSimple.Text = "Print Simple Employee Information Report";
            this.chkSimple.CheckedChanged += new System.EventHandler(this.chkSimple_CheckedChanged);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddBenefitName,
            this.mnuDeleteBenefit,
            this.mnuSP2,
            this.mnuContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuAddBenefitName
            // 
            this.mnuAddBenefitName.Enabled = false;
            this.mnuAddBenefitName.Index = 0;
            this.mnuAddBenefitName.Name = "mnuAddBenefitName";
            this.mnuAddBenefitName.Text = "Add Benefit Name";
            this.mnuAddBenefitName.Click += new System.EventHandler(this.mnuAddBenefitName_Click);
            // 
            // mnuDeleteBenefit
            // 
            this.mnuDeleteBenefit.Enabled = false;
            this.mnuDeleteBenefit.Index = 1;
            this.mnuDeleteBenefit.Name = "mnuDeleteBenefit";
            this.mnuDeleteBenefit.Text = "Delete Benefit";
            this.mnuDeleteBenefit.Click += new System.EventHandler(this.mnuDeleteBenefit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 2;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = 3;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Save & Continue";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 4;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(260, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(174, 48);
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
            // 
            // cmdAddBenefitName
            // 
            this.cmdAddBenefitName.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddBenefitName.Enabled = false;
            this.cmdAddBenefitName.Location = new System.Drawing.Point(430, 29);
            this.cmdAddBenefitName.Name = "cmdAddBenefitName";
            this.cmdAddBenefitName.Size = new System.Drawing.Size(129, 24);
            this.cmdAddBenefitName.TabIndex = 1;
            this.cmdAddBenefitName.Text = "Add Benefit Name";
            this.cmdAddBenefitName.Click += new System.EventHandler(this.mnuAddBenefitName_Click);
            // 
            // cmdDeleteBenefit
            // 
            this.cmdDeleteBenefit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteBenefit.Enabled = false;
            this.cmdDeleteBenefit.Location = new System.Drawing.Point(565, 29);
            this.cmdDeleteBenefit.Name = "cmdDeleteBenefit";
            this.cmdDeleteBenefit.Size = new System.Drawing.Size(105, 24);
            this.cmdDeleteBenefit.TabIndex = 2;
            this.cmdDeleteBenefit.Text = "Delete Benefit";
            this.cmdDeleteBenefit.Click += new System.EventHandler(this.mnuDeleteBenefit_Click);
            // 
            // frmEmpInfoSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(698, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmpInfoSetup";
            this.Text = "Employee Information Report";
            this.Load += new System.EventHandler(this.frmEmpInfoSetup_Load);
            this.Activated += new System.EventHandler(this.frmEmpInfoSetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmpInfoSetup_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBenefits)).EndInit();
            this.fraBenefits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSinglePerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSequence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSimple)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBenefitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBenefit)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
        private FCButton cmdAddBenefitName;
        private FCButton cmdDeleteBenefit;
    }
}
