//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptAuditArchiveReport.
	/// </summary>
	public partial class rptAuditArchiveReport : BaseSectionReport
	{
		public rptAuditArchiveReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Audit Archive Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptAuditArchiveReport InstancePtr
		{
			get
			{
				return (rptAuditArchiveReport)Sys.GetInstance(typeof(rptAuditArchiveReport));
			}
		}

		protected rptAuditArchiveReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployeeInfo?.Dispose();
				rsData?.Dispose();
                rsEmployeeInfo = null;
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditArchiveReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				while (!rsData.EndOfFile())
				{
					if (FCConvert.ToString(rsData.Get_Fields_String("UserField1")) != "")
					{
						if (!employeeDict.ContainsKey(rsData.Get_Fields_String("UserField1")))
						{
							rsData.MoveNext();
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					eArgs.EOF = false;
				}
				else
				{
					rsData.MoveNext();
					eArgs.EOF = rsData.EndOfFile();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			string strSQL;
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			blnFirstRecord = true;
			strSQL = "";
			if (frmSetupAuditArchiveReport.InstancePtr.cmbEmployeeAll.Text == "All")
			{
				// do nothing
			}
			else
			{
				strSQL += "UserField1 = '" + fecherFoundation.Strings.Trim(Strings.Left(frmSetupAuditArchiveReport.InstancePtr.cboEmployee.Text, Strings.InStr(1, frmSetupAuditArchiveReport.InstancePtr.cboEmployee.Text, " - ", CompareConstants.vbBinaryCompare))) + "' AND ";
			}
			if (frmSetupAuditArchiveReport.InstancePtr.cmbScreenAll.Text == "All")
			{
				// do nothing
			}
			else
			{
				strSQL += "Location = '" + frmSetupAuditArchiveReport.InstancePtr.cboScreen.Text + "' AND ";
			}
			if (frmSetupAuditArchiveReport.InstancePtr.cmbDateAll.Text == "All")
			{
				// do nothing
			}
			else if (frmSetupAuditArchiveReport.InstancePtr.cmbDateAll.Text == "Date Range")
			{
				strSQL += "(DateUpdated >= '" + frmSetupAuditArchiveReport.InstancePtr.txtLowDate.Text + "' AND DateUpdated <= '" + frmSetupAuditArchiveReport.InstancePtr.txtHighDate.Text + "') AND ";
			}
			else
			{
				// strSQL = strSQL & "(CDate(UserField2) = '" & CDate(.cboPayDate.Text) & "' AND UserField3 = '" & .cboPayRunID.Text & "') AND "
				strSQL += " (convert(datetime,UserField2) = '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmSetupAuditArchiveReport.InstancePtr.cboPayDate.Text)) + "' and UserField3 = '" + frmSetupAuditArchiveReport.InstancePtr.cboPayRunID.Text + "') and ";
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5);
				rsData.OpenRecordset("Select * from AuditChangesArchive WHERE " + strSQL + " ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1", modGlobalVariables.DEFAULTDATABASE);
			}
			else
			{
				rsData.OpenRecordset("Select * from AuditChangesArchive ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1", modGlobalVariables.DEFAULTDATABASE);
			}
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				rsEmployeeInfo.OpenRecordset("SELECT * FROM tblEmployeeMaster");
			}
			else
			{
				MessageBox.Show("No Info Found", null);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldPayDate As object	OnWrite(string)
			// If rsEmployeeInfo.FindFirstRecord("EmployeeNumber", rsData.Fields("UserField1")) Then
			if (rsEmployeeInfo.FindFirst("employeenumber = '" + modGlobalFunctions.EscapeQuotes(rsData.Get_Fields("userfield1")) + "'"))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) != "")
				{
					txtEmployee.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields_String("UserField1") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Desig"))));
				}
				else
				{
					txtEmployee.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields_String("UserField1") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Desig"))));
				}
			}
			else
			{
				txtEmployee.Text = rsData.Get_Fields_String("UserField1") + " UNKNOWN";
			}
			txtUser.Text = rsData.Get_Fields_String("UserID");
			txtDateTimeField.Text = FCConvert.ToString(rsData.Get_Fields_DateTime("DateUpdated")) + " " + FCConvert.ToString(rsData.Get_Fields("TimeUpdated"));
			fldLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Location")));
			fldDescription.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
			if (Information.IsDate(rsData.Get_Fields_String("UserField2")))
			{
				fldPayDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(rsData.Get_Fields_String("UserField2"))), "MM/dd/yyyy");
			}
			else
			{
				fldPayDate.Text = "";
			}
			fldPayRunID.Text = rsData.Get_Fields_String("UserField3");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
