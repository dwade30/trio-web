//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	public partial class frmMiscUpdate : BaseForm
	{
		public frmMiscUpdate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			this.cboRateOfPay.Items.Add("");
			this.cboRateOfPay.Items.Add("M = Multiply By");
			this.cboRateOfPay.Items.Add("D = Divided By");
			this.cboRateOfPay.Items.Add("");
			this.cboRateOfPay.Items.Add("0");
			this.cboRateOfPay.Items.Add("0");
			this.cboRateOfPay.Items.Add("");
			this.cboRateOfPay.Items.Add("M = Multiply By");
			this.cboRateOfPay.Items.Add("D = Divide By");
			this.Frame2.FormatCaption = false;
			this.Frame2.Text = "MainePERS";
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_9, 8);
			this.Label1.AddControlArrayElement(Label1_10, 9);
			this.Label1.AddControlArrayElement(Label1_11, 10);
			this.Label1.AddControlArrayElement(Label1_12, 11);
			this.Label1.AddControlArrayElement(Label1_14, 12);
			this.Label1.AddControlArrayElement(Label1_15, 13);
			this.Label1.AddControlArrayElement(Label1_16, 14);
			this.Label1.AddControlArrayElement(Label1_17, 15);
			this.Label1.AddControlArrayElement(Label1_18, 16);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMiscUpdate InstancePtr
		{
			get
			{
				return (frmMiscUpdate)Sys.GetInstance(typeof(frmMiscUpdate));
			}
		}

		protected frmMiscUpdate _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE and DAN C. SOLTESZ
		// DATE:       MAY 08,2001
		//
		// NOTES: Never design a screen to work this way again.
		//
		// USE A CONTROL ARRAY FOR ALL TYPES OF CONTROLS
		//
		//
		//
		// **************************************************
		// private local variables
		// Private dbMiscUpdate        As DAO.Database
		private clsDRWrapper rsMiscUpdate = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		private int intCounter;
		private int intDataChanged;
		private Control ControlName = new Control();
		private string strEmployeeNumber = "";
		// vbPorter upgrade warning: intWidth As int	OnWriteFCConvert.ToInt32(
		private int intWidth;
		private bool boolSaveOK;
		private bool FormLoading;
		private bool boolCantEdit;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		const int cnstgridcolID = 0;
		const int CNSTGRIDCOLLINE = 1;
		const int CNSTGRIDCOLDEFAULT = 2;
		const int CNSTGRIDCOLPOSITION = 3;
		const int CNSTGRIDCOLSTATUS = 4;
		const int CNSTGRIDCOLREPORTTYPE = 5;
		const int CNSTGRIDCOLFEDCOMPAMOUNT = 6;
		const int CNSTGRIDCOLFEDCOMPDOLLARPERCENT = 7;
		const int CNSTGRIDCOLWORKWEEKSPERYEAR = 8;
		const int CNSTGRIDCOLHOURLYDAILYCONTRACT = 9;
		const int CNSTGRIDCOLFULLTIMEEQUIVALENT = 10;
		const int CNSTGRIDCOLPARTICIPATION = 11;
		const int CNSTGRIDCOLSCHEDULE = 12;
		const int CNSTMSRSREPORTTYPEPLD = 0;
		const int CNSTMSRSREPORTTYPESCHOOLPLD = 1;
		const int CNSTMSRSREPORTTYPETEACHER = 2;
		// MAY ONLY NEED SCHOOL AND NONSCHOOL
		const int CNSTMSRSHOURLY = 0;
		const int CNSTMSRSDAILY = 1;
		const int CNSTMSRSCONTRACT = 2;
        private SSNViewType ssnViewPermission = SSNViewType.None;

        private void SetupGridPlanCode()
		{
			string strTemp = "";
			strTemp = "--" + "\t" + "None";
			strTemp += "|AC|AN|BC|1C|2C|3C|4C|1N|2N|3N|4N|11000|11001|31000|32000|61000|62000";
			gridPlanCode.ColComboList(0, strTemp);
		}

		private void SetupGridLifeInsuranceCode()
		{
			string strTemp = "";
			strTemp = " ";
			strTemp += "|B" + "\t" + "Basic for the employee only";
			strTemp += "|HA" + "\t" + "Basic and Dependent Plan A";
			strTemp += "|HB" + "\t" + "Basic and Dependent Plan B";
			strTemp += "|S1" + "\t" + "Basic and Supplemental 1";
			strTemp += "|S2" + "\t" + "Basic and Supplemental 2";
			strTemp += "|S3" + "\t" + "Basic and Supplemental 3";
			strTemp += "|F1A" + "\t" + "Basic, Supplemental 1 and Dependent Plan A";
			strTemp += "|F2A" + "\t" + "Basic, Supplemental 2 and Dependent Plan A";
			strTemp += "|F3A" + "\t" + "Basic, Supplemental 3 and Dependent Plan A";
			strTemp += "|F1B" + "\t" + "Basic, Supplemental 1 and Dependent Plan B";
			strTemp += "|F2B" + "\t" + "Basic, Supplemental 2 and Dependent Plan B";
			strTemp += "|F3B" + "\t" + "Basic, Supplemental 3 and Dependent Plan B";
			strTemp += "|OC" + "\t" + "Other Coverage";
			strTemp += "|I" + "\t" + "Ineligible";
			strTemp += "|R" + "\t" + "Refused Insurance Coverage";
			gridLifeInsuranceCode.ColComboList(0, strTemp);
		}

		private void SetupGridLifeInsScheduleCode()
		{
			string strTemp = "";
			strTemp = "";
			strTemp = "-" + "\t" + "None";
			strTemp += "|B" + "\t" + "26 Biweekly Teacher";
			strTemp += "|C" + "\t" + "24 Biweekly Teacher";
			strTemp += "|D" + "\t" + "23 Biweekly Teacher";
			strTemp += "|E" + "\t" + "22 Biweekly Teacher";
			strTemp += "|F" + "\t" + "21 Biweekly Teacher";
			strTemp += "|G" + "\t" + "20 Biweekly Teacher";
			strTemp += "|H" + "\t" + "18 Biweekly Teacher";
			strTemp += "|I" + "\t" + "Monthly, 12 Months Teacher";
			strTemp += "|K" + "\t" + "26 Biweekly PLD";
			strTemp += "|L" + "\t" + "24 Biweekly PLD";
			strTemp += "|M" + "\t" + "23 Biweekly PLD";
			strTemp += "|N" + "\t" + "22 Biweekly PLD";
			strTemp += "|O" + "\t" + "21 Biweekly PLD";
			strTemp += "|P" + "\t" + "20 Biweekly PLD";
			strTemp += "|Q" + "\t" + "18 Biweekly PLD";
			strTemp += "|R" + "\t" + "48 Weekly";
			strTemp += "|S" + "\t" + "46 Weekly";
			strTemp += "|T" + "\t" + "44 Weekly";
			strTemp += "|U" + "\t" + "43 Weekly";
			strTemp += "|V" + "\t" + "42 Weekly";
			strTemp += "|W" + "\t" + "38 Weekly";
			strTemp += "|X" + "\t" + "36 Weekly";
			strTemp += "|Y" + "\t" + "52 Weekly";
			strTemp += "|Z" + "\t" + "Monthly";
			gridLifeInsScheduleCode.ColComboList(0, strTemp);
		}

		private void ResizeGridLifeInsuranceCode()
		{
            //gridLifeInsuranceCode.Height = gridLifeInsuranceCode.RowHeight(0) + 60;
            gridLifeInsuranceCode.ColWidth(1, 0);
        }

		private void ResizeGridLifeInsScheduleCode()
		{
            //gridLifeInsScheduleCode.Height = gridLifeInsScheduleCode.RowHeight(0) + 60;
            gridLifeInsScheduleCode.ColWidth(1, 0);
        }

		private void ResizeGridPlanCode()
		{
            //gridPlanCode.Height = gridPlanCode.RowHeight(0) + 60;
            gridPlanCode.ColWidth(1, 0);
        }

		private void SetupCombos()
		{
			cboMonthlyHD.Clear();
			cboMonthlyHD.AddItem("M = Multiply By");
			cboMonthlyHD.AddItem("D = Divided By");
			cboRateOfPay.Clear();
			cboRateOfPay.AddItem("M = Multiply By");
			cboRateOfPay.AddItem("D = Divided By");
			SetupGridLifeInsuranceCode();
			SetupGridPlanCode();
			SetupGridLifeInsScheduleCode();
		}

		private void SetupGrid()
		{
			string strTemp = "";
			Grid.Rows = 1;
			Grid.Cols = 13;
			Grid.ColHidden(cnstgridcolID, true);
			Grid.ColHidden(CNSTGRIDCOLLINE, true);
			// DON'T KNOW IF we'll use the line number or not
			Grid.ColDataType(CNSTGRIDCOLDEFAULT, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.TextMatrix(0, CNSTGRIDCOLDEFAULT, "Default");
			Grid.TextMatrix(0, CNSTGRIDCOLPOSITION, "Position");
			Grid.TextMatrix(0, CNSTGRIDCOLSTATUS, "Status");
			strTemp = " " + "\t" + "None|11" + "\t" + "Full Time|12" + "\t" + "Part Time";
			strTemp += "|14" + "\t" + "Seasonal|15" + "\t" + "Seasonal Part Time";
			strTemp += "|17" + "\t" + "Project/Intermittent";
			strTemp += "|52" + "\t" + "Insurance Only Retired|53" + "\t" + "Retired Returned to Work";
			// strTemp = strTemp & "|61" & vbTab & "Special PST Substitute Teacher|64" & vbTab & "Special PST Adult Education After 9/1/78"
			strTemp += "|65" + "\t" + "Insurance Only - Active";
			// strTemp = strTemp & "|1A" & vbTab & "Regular Full-time Limited Period|1B" & vbTab & "Regular Part-time Limited Period"
			// strTemp = strTemp & "|21" & vbTab & "Special PST Full-time Permanent|22" & vbTab & "Special PST Part-time Permanent"
			// strTemp = strTemp & "|23" & vbTab & "Special PST Intermittent Permanent|24" & vbTab & "Special PST Full-time Seasonal"
			// strTemp = strTemp & "|25" & vbTab & "Special PST Part-time Seasonal|26" & vbTab & "Special PST Intermittent Seasonal"
			// strTemp = strTemp & "|27" & vbTab & "Special PST Full-time Project|28" & vbTab & "Special PST Part-time Project"
			// strTemp = strTemp & "|29" & vbTab & "Special PST Intermittent Project|2A" & vbTab & "Special PST Full-time Limited Period"
			// strTemp = strTemp & "|2B" & vbTab & "Special PST Part-time Limited Period|2C" & vbTab & "Special PST Intermittent Limited Period"
			// strTemp = strTemp & "|42|43"
			// strTemp = strTemp & "|52" & vbTab & "Regular Insurance Only Retired|53" & vbTab & "Regular Retired Returned to Work"
			// strTemp = strTemp & "|61" & vbTab & "Special PST Substitute Teacher|64" & vbTab & "Special PST Adult Education After 9/1/78"
			// strTemp = strTemp & "|65" & vbTab & "Regular Full-time Insurance Only|67" & vbTab & "Regular Part-time Insurance Only"
			Grid.ColComboList(CNSTGRIDCOLSTATUS, strTemp);
			Grid.ColComboList(CNSTGRIDCOLREPORTTYPE, "#1;PLD|#2;School PLD|#3;Teacher");
			Grid.TextMatrix(0, CNSTGRIDCOLREPORTTYPE, "Report");
			Grid.TextMatrix(0, CNSTGRIDCOLFEDCOMPAMOUNT, "Fed Comp Am");
            Grid.ColAlignment(CNSTGRIDCOLFEDCOMPAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			Grid.ColComboList(CNSTGRIDCOLFEDCOMPDOLLARPERCENT, "#0;Dollar|#1;Percent");
			Grid.TextMatrix(0, CNSTGRIDCOLFEDCOMPDOLLARPERCENT, "Dol / %");
			Grid.TextMatrix(0, CNSTGRIDCOLWORKWEEKSPERYEAR, "Wks/Yr");
			Grid.ColComboList(CNSTGRIDCOLHOURLYDAILYCONTRACT, "#0;Hourly|#1;Daily|#2;Contract");
			Grid.TextMatrix(0, CNSTGRIDCOLHOURLYDAILYCONTRACT, "H/D/C");
			Grid.TextMatrix(0, CNSTGRIDCOLFULLTIMEEQUIVALENT, "F Time Eq.");
			Grid.ColComboList(CNSTGRIDCOLPARTICIPATION, "A" + "\t" + "Active|N" + "\t" + "Ineligible|R" + "\t" + "Refused Membership|Y" + "\t" + "EmployerPaid");
			Grid.TextMatrix(0, CNSTGRIDCOLPARTICIPATION, "Part.");
			Grid.TextMatrix(0, CNSTGRIDCOLSCHEDULE, "Schedule");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLDEFAULT, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPOSITION, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSTATUS, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLREPORTTYPE, FCConvert.ToInt32(0.11 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFEDCOMPAMOUNT, FCConvert.ToInt32(0.13 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFEDCOMPDOLLARPERCENT, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLWORKWEEKSPERYEAR, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLHOURLYDAILYCONTRACT, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFULLTIMEEQUIVALENT, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPARTICIPATION, FCConvert.ToInt32(0.05 * GridWidth));
		}

		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void FillcmbPositionCategory()
		{
			cmbPositionCategory.Clear();
			cmbPositionCategory.AddItem("None");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYNONE);
			// .AddItem ("Firefighter/Law/Government")
			// .ItemData(.NewIndex) = CNSTMSRSPLANCATEGORYFIREFIGHTERLAWGOVERNMENT
			cmbPositionCategory.AddItem("Firefighter");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYFIREFIGHTER);
			cmbPositionCategory.AddItem("General Government");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYGENERALGOVERNMENT);
			cmbPositionCategory.AddItem("Law Enforcement");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYLAWENFORCEMENT);
			cmbPositionCategory.AddItem("Teacher");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYTEACHER);
			cmbPositionCategory.AddItem("Teacher (Before 7/1/93)");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYTEACHERBEFORE93);
			cmbPositionCategory.SelectedIndex = 0;
		}

		private void cmbPositionCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cmbPositionCategory_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "Employee position classification used to determine plan code to file electronically";
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			SaveChanges();
			e.Cancel = !boolSaveOK;
		}

		private void cboUnemployment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
			if (cboUnemployment.Text == "N")
			{
				txtNatureCode.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				txtNatureCode.Text = string.Empty;
			}
			else
			{
				txtNatureCode.BackColor = Color.White;
			}
			txtNatureCode.ReadOnly = cboUnemployment.Text == "N";
		}

		private void frmMiscUpdate_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridLifeInsuranceCode();
			ResizeGridPlanCode();
			ResizeGridLifeInsScheduleCode();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			intDataChanged += 1;
			Grid.RowData(Grid.Row, true);
		}

		private void Grid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = Grid[e.ColumnIndex, e.RowIndex];
            switch (Grid.GetFlexColIndex(e.ColumnIndex))
			{
				case 0:
					{
						break;
					}
				case 1:
					{
						break;
					}
				case 2:
					{
						//ToolTip1.SetToolTip(Grid, "Mark the single record that will be used as the default.");
						cell.ToolTipText = "Mark the single record that will be used as the default.";
						break;
					}
				case 3:
					{
						//ToolTip1.SetToolTip(Grid, "Identifies the job or position that the employee holds. (Police Officer, Principal)");
						cell.ToolTipText = "Identifies the job or position that the employee holds. (Police Officer, Principal)";
						break;
					}
				case 4:
					{
						//ToolTip1.SetToolTip(Grid, "Employment status of the employee for the payroll period being reported.");
						cell.ToolTipText = "Employment status of the employee for the payroll period being reported.";
						break;
					}
				case 5:
					{
						//ToolTip1.SetToolTip(Grid, "PLD, School PLD, Teacher");
						cell.ToolTipText = "PLD, School PLD, Teacher";
						break;
					}
				case 6:
					{
						//ToolTip1.SetToolTip(Grid, "Federal compensation amount");
						cell.ToolTipText = "Federal compensation amount";
						break;
					}
				case 7:
					{
						//ToolTip1.SetToolTip(Grid, "Dollars, Percent");
						cell.ToolTipText = "Dollars, Percent";
						break;
					}
				case 8:
					{
						//ToolTip1.SetToolTip(Grid, "Number of weeks per year");
						cell.ToolTipText = "Number of weeks per year";
						break;
					}
				case 9:
					{
						//ToolTip1.SetToolTip(Grid, "Hours, Daily, Contract");
						cell.ToolTipText = "Hours, Daily, Contract";
						break;
					}
				case 10:
					{
						//ToolTip1.SetToolTip(Grid, "Full Time Equivalent - Annual contracted amount or activity stipend");
						cell.ToolTipText = "Full Time Equivalent - Annual contracted amount or activity stipend";
						break;
					}
				case CNSTGRIDCOLPARTICIPATION:
					{
						//ToolTip1.SetToolTip(Grid, "Participation status in retirement plan");
						cell.ToolTipText = "Participation status in retirement plan";
						break;
					}
				case CNSTGRIDCOLSCHEDULE:
					{
						//ToolTip1.SetToolTip(Grid, "Contribution schedule number");
						cell.ToolTipText = "Contribution schedule number";
						break;
					}
			}
			//end switch
		}

		private void gridLifeInsuranceCode_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "";
		}

		private void gridPlanCode_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "";
		}

		private void gridLifeInsScheduleCode_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "";
		}

		private void mnuAddLine_Click(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			intDataChanged += 1;
			Grid.Rows += 1;
			// initialize the row
			lngRow = Grid.Rows - 1;
			Grid.RowData(lngRow, true);
			Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(0));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLDEFAULT, FCConvert.ToString(false));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTYPE, FCConvert.ToString(CNSTMSRSREPORTTYPEPLD));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT, FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSFEDCOMPDOLLAR));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLLINE, FCConvert.ToString(lngRow));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTYPE, FCConvert.ToString(CNSTMSRSREPORTTYPEPLD));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS, FCConvert.ToString(11));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLPARTICIPATION, "A");
			Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHEDULE, "000001");
			// Dave 12/14/2006---------------------------------------------------
			// Add change record for adding a row to the grid
			clsReportChanges.AddChange("Added Row to MSRS Distribution");
			// ------------------------------------------------------------------
		}

		private void mnuDeleteLine_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (Grid.Row < 1)
				return;
			// check to see if it is used
			if (Conversion.Val(Grid.TextMatrix(Grid.Row, cnstgridcolID)) > 0)
			{
				clsLoad.OpenRecordset("select * from tblpayrolldistribution where msrsid = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, cnstgridcolID))), "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					MessageBox.Show("This line is currently being used on the distribution screen" + "\r\n" + "You cannot delete it until the distribution screen is updated", "In Use", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (MessageBox.Show("Are you sure you want to delete this line?", "Delete Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
			}
			if (Conversion.Val(Grid.TextMatrix(Grid.Row, cnstgridcolID)) > 0)
			{
				intDataChanged += 1;
				GridDelete.AddItem(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, cnstgridcolID))));
			}
			// Dave 12/14/2006---------------------------------------------------
			// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
			modAuditReporting.RemoveGridRow_8("Grid", intTotalNumberOfControls - 1, Grid.Row, clsControlInfo);
			// We then add a change record saying the row was deleted
			clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(Grid.Row) + " from MSRS Distribution");
			// -------------------------------------------------------------------
			Grid.RemoveItem(Grid.Row);
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (!boolCantEdit)
			{
				this.chkInclude.Focus();
			}
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				SaveChanges();
				if (boolSaveOK)
				{
					frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					LoadData();
					ShowData();
					modGlobalRoutines.SetEmployeeTextBox(txtFullName, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				}
			}
		}

		private void cboMonthlyHD_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboMonthlyHD_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboMonthlyHD.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 100, 0);
			// cboMonthlyHD.Width = intwidth * 2.5
		}

		private void cboMonthlyHD_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "M = Multiply by" + "\r\n" + "D = Divide by";
		}

		private void cboMonthlyHD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cboMonthlyHD.Text == string.Empty)
			{
				cboMonthlyHD.SelectedIndex = 0;
				// Cancel = True
			}
		}

		private void cboRateOfPay_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboRateOfPay_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboRateOfPay.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 100, 0);
			// cboRateOfPay.Width = intwidth * 2.5
		}

		private void cboRateOfPay_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "M = Multiply by" + "\r\n" + "D = Divide by";
		}

		private void cboRateOfPay_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cboRateOfPay.Text == string.Empty)
			{
				cboRateOfPay.SelectedIndex = 0;
				// Cancel = True
			}
		}

		private void cboUnemployment_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "Y = Include in unemployment reports." + "\r\n" + "N = Do not include in reports" + "\r\n" + "S = Yes, include as seasonal";
		}
		//
		private void cboUnemployment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cboUnemployment.Text == string.Empty)
			{
				cboUnemployment.SelectedIndex = 0;
				// Cancel = True
			}
			else
			{
				if (cboUnemployment.Text == "N")
				{
					txtNatureCode.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					txtNatureCode.BackColor = Color.White;
				}
				txtNatureCode.ReadOnly = cboUnemployment.Text == "N";
			}
		}

		private void chkInclude_CheckedChanged(object sender, System.EventArgs e)
		{
			cmdAdd.Enabled = 0 != (chkInclude.CheckState);
			cmdDelete.Enabled = 0 != (chkInclude.CheckState);
			SetControls();
			if (chkInclude.CheckState != Wisej.Web.CheckState.Checked && !FormLoading)
			{
				// vbPorter upgrade warning: ControlName As Control	OnWrite(string)
				Control ControlName = new Control();
				foreach (Control ControlName_foreach in this.GetAllControls())
				{
					ControlName = ControlName_foreach;
					if (ControlName is FCTextBox)
					{
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCTextBox).Text = string.Empty;
						}
					}
					else if (ControlName is FCComboBox)
					{
						if ((ControlName as FCComboBox).DropDownStyle == ComboBoxStyle.DropDownList)
						{
							(ControlName as FCComboBox).ListIndex = 0;
						}
						else
						{
							(ControlName as FCComboBox).Text = string.Empty;
						}
					}
					else if (ControlName is FCCheckBox)
					{
					}
					else
					{
					}
					ControlName = null;
				}
				SetDefaultValues();
			}
			intDataChanged += 1;
		}

		private void SetControls()
		{
			// vbPorter upgrade warning: lngColor As int	OnWrite(Color, int)
			int lngColor = 0;
			if (chkInclude.CheckState == CheckState.Checked)
			{
				lngColor = ColorTranslator.ToOle(Color.White);
			}
			else
			{
				lngColor = modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND;
			}
			// txtLifeInsCode.Enabled = chkInclude
			gridLifeInsuranceCode.Enabled = 0 != chkInclude.CheckState;
			gridLifeInsScheduleCode.Enabled = 0 != chkInclude.CheckState;
			cboMonthlyHD.Enabled = 0 != (chkInclude.CheckState);
			txtMonthlyHDFactor.Enabled = 0 != (chkInclude.CheckState);
			cboRateOfPay.Enabled = 0 != (chkInclude.CheckState);
			txtRateOfPayFactor.Enabled = 0 != (chkInclude.CheckState);
			txtStatusCode.Enabled = 0 != (chkInclude.CheckState);
			txtPositionCode.Enabled = 0 != (chkInclude.CheckState);
			// txtPlanCode.Enabled = chkInclude
			gridPlanCode.Enabled = 0 != chkInclude.CheckState;
			txtExcess.Enabled = 0 != (chkInclude.CheckState);
			// txtPayPeriodCode.Enabled = chkInclude
			txtLifeInsLevel.Enabled = 0 != (chkInclude.CheckState);
			txtFederalComp.Enabled = 0 != (chkInclude.CheckState);
			txtFedCompAmount.Enabled = 0 != (chkInclude.CheckState);
			txtWorkWeekPerYear.Enabled = 0 != (chkInclude.CheckState);
			txtPayRateCode.Enabled = 0 != (chkInclude.CheckState);
			//FC:FINAL:DDU:#i2397 - set tabstop on hidden unused controls
			txtStatusCode.TabStop = false;
			txtPositionCode.TabStop = false;
			txtFederalComp.TabStop = false;
			txtFedCompAmount.TabStop = false;
			txtWorkWeekPerYear.TabStop = false;
			txtPayRateCode.TabStop = false;
			// txtNatureCode.Enabled = chkInclude
			cmbPositionCategory.Enabled = chkInclude.CheckState == Wisej.Web.CheckState.Checked;
			cmbPositionCategory.BackColor = ColorTranslator.FromOle(lngColor);
			// txtLifeInsCode.BackColor = lngColor
			gridLifeInsuranceCode.BackColor = ColorTranslator.FromOle(lngColor);
			gridLifeInsScheduleCode.BackColor = ColorTranslator.FromOle(lngColor);
			cboMonthlyHD.BackColor = ColorTranslator.FromOle(lngColor);
			txtMonthlyHDFactor.BackColor = ColorTranslator.FromOle(lngColor);
			cboRateOfPay.BackColor = ColorTranslator.FromOle(lngColor);
			txtRateOfPayFactor.BackColor = ColorTranslator.FromOle(lngColor);
			txtStatusCode.BackColor = ColorTranslator.FromOle(lngColor);
			txtPositionCode.BackColor = ColorTranslator.FromOle(lngColor);
			// txtPlanCode.BackColor = lngColor
			gridPlanCode.BackColor = ColorTranslator.FromOle(lngColor);
			txtExcess.BackColor = ColorTranslator.FromOle(lngColor);
			// txtPayPeriodCode.BackColor = lngColor
			txtLifeInsLevel.BackColor = ColorTranslator.FromOle(lngColor);
			txtFederalComp.BackColor = ColorTranslator.FromOle(lngColor);
			txtFedCompAmount.BackColor = ColorTranslator.FromOle(lngColor);
			txtWorkWeekPerYear.BackColor = ColorTranslator.FromOle(lngColor);
			txtPayRateCode.BackColor = ColorTranslator.FromOle(lngColor);
			// txtNatureCode.BackColor = lngColor
		}

		private void chkInclude_Enter(object sender, System.EventArgs e)
		{
			lblDescription.Text = "Checked = Yes" + "\r\n" + "Unchecked = No";
		}

		private void cmdDelete_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Delete the field length information from the database
				if (MessageBox.Show("This action will delete Direct Deposit infomation. Continue?", "Payroll Standard Length", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsMiscUpdate.Execute("Delete from tblMiscUpdate", "Payroll");
					cmdNew_Click();
					MessageBox.Show("Delete of record was successful", "Payroll Misc Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						SaveData();
					}
					else
					{
						boolSaveOK = true;
					}
				}
				else
				{
					boolSaveOK = true;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_CallErrorRoutine = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// clear out the controls butDo nothing to the database
				// vbPorter upgrade warning: ControlName As Control	OnWrite(string, int)
				Control ControlName = new Control();
				foreach (Control ControlName_foreach in this.GetAllControls())
				{
					ControlName = ControlName_foreach;
					if (ControlName is FCTextBox)
					{
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCTextBox).Text = string.Empty;
						}
					}
					else if (ControlName is FCComboBox)
					{
						if ((ControlName as FCComboBox).DropDownStyle == ComboBoxStyle.DropDownList)
						{
							(ControlName as FCComboBox).ListIndex = 0;
						}
						else
						{
							(ControlName as FCComboBox).Text = string.Empty;
						}
					}
					else if (ControlName is FCCheckBox)
					{
						(ControlName as FCCheckBox).Value = 0;
					}
					else
					{
						/*? On Error Resume Next  */// ControlName = vbNullString
						fecherFoundation.Information.Err().Clear();
					}
					ControlName = null;
				}
				// form is now dirty
				intDataChanged = 1;
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_CallErrorRoutine:
						//? goto CallErrorRoutine;
						break;
				}
				CallErrorRoutine:
				;
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadData()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridDelete.Rows = 0;
				SetupGrid();
				clsLoad.OpenRecordset("select * from tblmsrsdistributiondetail where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by ID", "twpy0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.RowData(lngRow, false);
					Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(clsLoad.Get_Fields("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDEFAULT, FCConvert.ToString(clsLoad.Get_Fields_Boolean("booldefault")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLINE, FCConvert.ToString(lngRow));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fedcompamount"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fedcompdollarpercent"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFULLTIMEEQUIVALENT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fulltimeequivalent"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLHOURLYDAILYCONTRACT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("HourlyDailyContract"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPOSITION, FCConvert.ToString(clsLoad.Get_Fields("position")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("reporttype"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS, FCConvert.ToString(clsLoad.Get_Fields("status")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLWORKWEEKSPERYEAR, FCConvert.ToString(clsLoad.Get_Fields("workweeksperyear")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPARTICIPATION, FCConvert.ToString(clsLoad.Get_Fields("participation")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHEDULE, FCConvert.ToString(clsLoad.Get_Fields("schedule")));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngRow;
			string strSQL = "";
			string strCols = "";
			int lngID = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				this.Grid.Select(0, 0);
				gridPlanCode.Row = -1;
				gridLifeInsuranceCode.Row = -1;
				gridLifeInsScheduleCode.Row = -1;
				//App.DoEvents();
				// do the distribution stuff first
				// get rid of the deleted ones
				for (x = 0; x <= (GridDelete.Rows - 1); x++)
				{
					clsSave.Execute("delete from tblMSRSDistributionDetail where ID = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(x, 0))), "twpy0000.vb1");
				}
				// x
				GridDelete.Rows = 0;
				// check for legit data
				if (!CheckData())
				{
					return SaveData;
				}
				int intMarked;
				intMarked = 0;
				// CHECK TO SEE IF THEIR IS A DEFAULT RECORD
				if (chkInclude.CheckState == Wisej.Web.CheckState.Checked)
				{
					for (x = 1; x <= (this.Grid.Rows - 1); x++)
					{
						if (FCConvert.CBool(Grid.TextMatrix(x, 2)))
							intMarked += 1;
					}
					if (intMarked == 0)
					{
						MessageBox.Show("At least one row must be marked as the default.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						boolSaveOK = false;
						return SaveData;
					}
					else if (intMarked > 1)
					{
						MessageBox.Show("Only one row can be marked as the default.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						boolSaveOK = false;
						return SaveData;
					}
				}
				// Call rsMiscUpdate.OpenRecordset("Select * from tblMiscUpdate Where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "'", "TWPY0000.vb1")
				// If rsMiscUpdate.EndOfFile Then
				// rsMiscUpdate.AddNew
				// Else
				// rsMiscUpdate.Edit
				// End If
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("MSRS Setup", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				// now take care of the rest
				lngRow = Grid.FindRow(true, 1);
				clsSave.OpenRecordset("select * from tblmsrsdistributiondetail where ID = -1", "twpy0000.vb1");
				while (lngRow > 0)
				{
					if (Conversion.Val(Grid.TextMatrix(lngRow, cnstgridcolID)) > 0)
					{
						clsSave.OpenRecordset("select * from tblmsrsdistributiondetail where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, cnstgridcolID))), "twpy0000.vb1");
						if (!clsSave.EndOfFile())
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
					}
					else
					{
						// adding a new one
						clsSave.AddNew();
					}
					clsSave.Set_Fields("employeenumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
					clsSave.Set_Fields("line", lngRow);
					clsSave.Set_Fields("position", fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLPOSITION)));
					if (FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLDEFAULT)))
					{
						clsSave.Set_Fields("boolDefault", true);
					}
					else
					{
						clsSave.Set_Fields("boolDefault", false);
					}
					clsSave.Set_Fields("status", fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS)));
					clsSave.Set_Fields("reporttype", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTYPE))));
					clsSave.Set_Fields("FedCompAmount", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT))));
					clsSave.Set_Fields("FedCompDollarPercent", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT))));
					clsSave.Set_Fields("workweeksperyear", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLWORKWEEKSPERYEAR))));
					clsSave.Set_Fields("HourlyDailyContract", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLHOURLYDAILYCONTRACT))));
					clsSave.Set_Fields("FullTimeEquivalent", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFULLTIMEEQUIVALENT))));
					if (fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLPARTICIPATION)) != string.Empty)
					{
						clsSave.Set_Fields("participation", fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLPARTICIPATION)));
					}
					else
					{
						clsSave.Set_Fields("participation", "A");
					}
					clsSave.Set_Fields("schedule", Grid.TextMatrix(lngRow, CNSTGRIDCOLSCHEDULE));
					clsSave.Update();
					lngID = FCConvert.ToInt32(clsSave.Get_Fields("ID"));
					Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(lngID));
					Grid.RowData(lngRow, false);
					if (lngRow < Grid.Rows - 1)
					{
						lngRow = Grid.FindRow(true, lngRow + 1);
					}
					else
					{
						lngRow = -1;
					}
				}
				clsSave.OpenRecordset("select * from tblmiscupdate where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				lngRow = Grid.FindRow(true, CNSTGRIDCOLDEFAULT);
				if (chkInclude.CheckState == Wisej.Web.CheckState.Checked)
				{
					// clsSave.Fields("LifeInsCode") = txtLifeInsCode.Text
					clsSave.Set_Fields("lifeinscode", fecherFoundation.Strings.Trim(gridLifeInsuranceCode.TextMatrix(0, 0)));
					clsSave.Set_Fields("payperiodscode", gridLifeInsScheduleCode.TextMatrix(0, 0));
					clsSave.Set_Fields("MonthlyFactor", txtMonthlyHDFactor.Text);
					clsSave.Set_Fields("PayFactor", txtRateOfPayFactor.Text);
					if (lngRow > 0)
					{
						clsSave.Set_Fields("StatusCode", Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATUS));
						clsSave.Set_Fields("PositionCode", Grid.TextMatrix(lngRow, CNSTGRIDCOLPOSITION));
						clsSave.Set_Fields("FederalComp", Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPDOLLARPERCENT));
						clsSave.Set_Fields("FedCompAmount", Grid.TextMatrix(lngRow, CNSTGRIDCOLFEDCOMPAMOUNT));
						clsSave.Set_Fields("WorkWeekPerYear", Grid.TextMatrix(lngRow, CNSTGRIDCOLWORKWEEKSPERYEAR));
					}
					// clsSave.Fields("PlanCode") = txtPlanCode.Text
					clsSave.Set_Fields("plancode", gridPlanCode.TextMatrix(0, 0));
					clsSave.Set_Fields("Excess", txtExcess.Text);
					// clsSave.Fields("PayPeriodsCode") = txtPayPeriodCode.Text
					clsSave.Set_Fields("LifeInsLevel", txtLifeInsLevel.Text);
					clsSave.Set_Fields("PayRateCode", txtPayRateCode.Text);
					clsSave.Set_Fields("MonthlyHD", cboMonthlyHD.Text);
					clsSave.Set_Fields("RateOfPay", cboRateOfPay.Text);
					clsSave.Set_Fields("MSRSJobCategory", cmbPositionCategory.ItemData(cmbPositionCategory.SelectedIndex));
				}
				clsSave.Set_Fields("unemployment", cboUnemployment.Text);
				clsSave.Set_Fields("NatureCode", txtNatureCode.Text);
				clsSave.Set_Fields("include", chkInclude.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				clsSave.Set_Fields("lastuserid", modGlobalConstants.Statics.gstrUserID);
				clsSave.Update();
				intDataChanged = 0;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				boolSaveOK = true;
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private bool CheckData()
		{
			bool CheckData = false;
			// make sure that the data is ok before saving
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CheckData = false;
				if (chkInclude.CheckState == Wisej.Web.CheckState.Checked)
				{
					// Select Case UCase(txtLifeInsCode.Text)
					if ((fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "B") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "HA") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "HB") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "S1") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "S2") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "S3") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "F1A") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "F2A") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "F3A") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "F1B") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "F2B") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "F3B") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "I") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "R") || (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(gridLifeInsuranceCode.TextMatrix(0, 0))) == "OC"))
					{
					}
					else
					{
						MessageBox.Show("Invalid Life Code", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// If txtLifeInsCode.Enabled Then txtLifeInsCode.SetFocus
						if (gridLifeInsuranceCode.Enabled)
						{
							gridLifeInsuranceCode.Focus();
						}
						boolSaveOK = false;
						return CheckData;
					}
					if (Conversion.Val(txtMonthlyHDFactor.Text) > 99.99)
					{
						MessageBox.Show("Invalid Monthly HD Factor Entry" + "\r\n" + "Range 0.0-99.99", "Invalid Factor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtMonthlyHDFactor.Focus();
						boolSaveOK = false;
						return CheckData;
					}
					if (fecherFoundation.Strings.Trim(txtMonthlyHDFactor.Text) == string.Empty)
					{
						txtMonthlyHDFactor.Text = FCConvert.ToString(0);
					}
					if (fecherFoundation.Strings.Trim(txtRateOfPayFactor.Text) == string.Empty)
					{
						txtRateOfPayFactor.Text = FCConvert.ToString(0);
					}
					if (Conversion.Val(txtRateOfPayFactor.Text) > 99.99)
					{
						MessageBox.Show("Invalid Pay Rate Factor" + "\r\n" + "Valid Range 0.0-99.99", "Invalid Factor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtRateOfPayFactor.Focus();
						boolSaveOK = false;
						return CheckData;
					}
					// Select Case (txtPlanCode.Text)
					// Case "--", "AC", "BC", "AN", "1C", "2C", "3C", "4C", "1N", "2N", "3N", "4N", "11000", "11001"
					// Case Else
					// Call MsgBox("Invalid Plan Code", vbExclamation, "Invalid Code")
					// txtPlanCode.SetFocus
					// boolSaveOK = False
					// Exit Function
					// End Select
					if (((txtExcess.Text) == "E") || ((txtExcess.Text) == "P") || ((txtExcess.Text) == "-"))
					{
					}
					else
					{
						MessageBox.Show("Invalid Excess Code", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtExcess.Focus();
						boolSaveOK = false;
						return CheckData;
					}
					if (!Information.IsNumeric(txtLifeInsLevel.Text))
					{
						MessageBox.Show("Invalid Life Ins Level Entry", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtLifeInsLevel.Focus();
						boolSaveOK = false;
						return CheckData;
					}
				}
				if ((this.cboUnemployment.Text == "Y" || cboUnemployment.Text == "S") && fecherFoundation.Strings.Trim(txtNatureCode.Text) == string.Empty)
				{
					MessageBox.Show("Nature code must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtNatureCode.Focus();
					boolSaveOK = false;
					return CheckData;
				}
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				if (chkInclude.CheckState == Wisej.Web.CheckState.Checked)
				{
					for (x = 1; x <= (Grid.Rows - 1); x++)
					{
						if (fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTGRIDCOLPOSITION)) == string.Empty)
						{
							MessageBox.Show("A position must be specified", "Missing Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolSaveOK = false;
							return CheckData;
						}
						if (fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTGRIDCOLSTATUS)) == string.Empty)
						{
							MessageBox.Show("A status must be specified", "Missing Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolSaveOK = false;
							return CheckData;
						}
					}
					// x
				}
				CheckData = true;
				return CheckData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckData;
		}
		// Private Sub cmdSave_Click()
		// On Error GoTo CallErrorRoutine
		// gstrCurrentRoutine = "cmdSave_Click"
		// Dim Mouse As clsMousePointer
		// Set Mouse = New clsMousePointer
		// GoTo ResumeCode
		// CallErrorRoutine:
		// Call SetErrorHandler
		// Exit Sub
		// ResumeCode:
		//
		// boolSaveOK = True
		// get all of the records from the database
		// Call rsMiscUpdate.OpenRecordset("Select * from tblMiscUpdate Where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "'", "TWPY0000.vb1")
		// If rsMiscUpdate.EndOfFile Then
		// rsMiscUpdate.AddNew
		// Else
		// rsMiscUpdate.Edit
		// End If
		//
		// If Me.chkInclude Then
		// Select Case (txtLifeInsCode.Text)
		// Case "B", "HA", "HB", "S1", "S2", "S3", "F1A", "F2A", "F3A", "F1B", "F2B", "F3B", "I", "R", "OC"
		// Case Else
		// Call MsgBox("Invalid Life Code", vbCritical, "Error")
		// If txtLifeInsCode.Enabled Then txtLifeInsCode.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End Select
		// If Val(txtMonthlyHDFactor.Text) > 99.99 Then
		// Call MsgBox("Invalid Monthly HD Factor Entry" & vbNewLine & "Range 0.0-99.99", vbCritical, "Error")
		// txtMonthlyHDFactor.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If txtMonthlyHDFactor.Text = vbNullString Then
		// txtMonthlyHDFactor.Text = 0
		// End If
		// If txtStatusCode.Text = vbNullString Then
		// txtStatusCode.Text = 0
		// End If
		// If txtRateOfPayFactor.Text = vbNullString Then
		// txtRateOfPayFactor.Text = 0
		// End If
		// If Val(txtRateOfPayFactor.Text) > 99.99 Then
		// Call MsgBox("Invalid Pay Rate Factor" & vbNewLine & "Range 0.0-99.99", vbCritical, "Error")
		// txtRateOfPayFactor.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If Mid(txtPositionCode.Text, 1, 1) <> "0" And Mid(txtPositionCode.Text, 1, 1) <> "Y" And Mid(txtPositionCode.Text, 1, 1) <> "N" Then
		// Call MsgBox("Invalid First Character" & vbNewLine & "Must Be 0,Y,N", vbCritical, "Error")
		// boolSaveOK = False
		// txtPositionCode.SetFocus
		// Exit Sub
		// End If
		// If Not IsNumeric(Mid(txtPositionCode.Text, 2, 4)) Or Len(txtPositionCode.Text) > 5 Then
		// Call MsgBox("Max Char Length 5" & vbNewLine & "Characters 2-5 Must Be Numeric", vbCritical, "Error")
		// txtPositionCode.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// Select Case (txtPlanCode.Text)
		// Case "--", "AC", "BC", "AN", "1C", "2C", "3C", "4C", "1N", "2N", "3N", "4N"
		// Case Else
		// Call MsgBox("Invalid Plan Code", vbCritical, "Error")
		// txtPlanCode.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End Select
		// Select Case (txtExcess.Text)
		// Case "E", "P", "-"
		// Case Else
		// Call MsgBox("Invalid Excess Code", vbCritical, "Error")
		// txtExcess.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End Select
		// If Not IsNumeric(txtLifeInsLevel.Text) Then
		// Call MsgBox("Invalid Life Ins Level Entry", vbCritical, "Error")
		// txtLifeInsLevel.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If Not IsNumeric(txtFedCompAmount.Text) Then
		// Call MsgBox("Must Be Numeric", vbCritical, "Error")
		// txtFedCompAmount.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// ElseIf Val(txtFedCompAmount.Text) < 0 Then
		// Call MsgBox("Can't Work Negative Hours", vbCritical, "Error")
		// txtFedCompAmount.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If Not IsNumeric(txtWorkWeekPerYear.Text) Then
		// Call MsgBox("Work Week Must Be Numeric", vbCritical, "Error")
		// txtWorkWeekPerYear.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// ElseIf Val(txtWorkWeekPerYear.Text) < 0 Then
		// Call MsgBox("Can't Work Negative Hours", vbCritical, "Error")
		// txtWorkWeekPerYear.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// ElseIf Val(txtWorkWeekPerYear.Text) > 52 Then
		// Call MsgBox("Can't Work More Then 52 Weeks", vbCritical, "Error")
		// txtWorkWeekPerYear.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If cboUnemployment = vbNullString Then
		// cboUnemployment.ListIndex = 0
		// cboUnemployment.SetFocus
		// Call MsgBox("Please Select Value From List", vbCritical, "Invalid")
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If cboUnemployment <> "N" Then
		// If Not IsNumeric(txtNatureCode.Text) Then
		// Call MsgBox("Must Be Numeric", vbCritical, "Error")
		// txtNatureCode.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// End If
		//
		// If cboMonthlyHD = vbNullString Then
		// cboMonthlyHD.ListIndex = 0
		// cboMonthlyHD.SetFocus
		// Call MsgBox("Please Select Value From List", vbCritical, "Invalid")
		// boolSaveOK = False
		// Exit Sub
		// End If
		// If cboRateOfPay = vbNullString Then
		// cboRateOfPay.ListIndex = 0
		// cboRateOfPay.SetFocus
		// Call MsgBox("Please Select Value From List", vbCritical, "Invalid")
		// boolSaveOK = False
		// Exit Sub
		// End If
		// End If
		//
		// If cboUnemployment = "Y" Or cboUnemployment = "S" Then
		// If Not IsNumeric(txtNatureCode.Text) Then
		// Call MsgBox("Must Be Numeric", vbCritical, "Error")
		// cboUnemployment = "N"
		// txtNatureCode.BackColor = BackColor
		// txtNatureCode.Locked = True
		// cboUnemployment.SetFocus
		// boolSaveOK = False
		// Exit Sub
		// End If
		// End If
		//
		// rsMiscUpdate.SetData "EmployeeNumber", gtypeCurrentEmployee.EMPLOYEENUMBER
		// rsMiscUpdate.SetData "Include", chkInclude
		// If chkInclude Then
		// rsMiscUpdate.SetData "LifeInsCode", txtLifeInsCode
		// rsMiscUpdate.SetData "MonthlyFactor", txtMonthlyHDFactor
		// rsMiscUpdate.SetData "PayFactor", txtRateOfPayFactor
		// rsMiscUpdate.SetData "StatusCode", txtStatusCode
		// rsMiscUpdate.SetData "PositionCode", txtPositionCode
		// rsMiscUpdate.SetData "PlanCode", txtPlanCode
		// rsMiscUpdate.SetData "Excess", txtExcess
		// rsMiscUpdate.SetData "PayPeriodsCode", txtPayPeriodCode
		// rsMiscUpdate.SetData "LifeInsLevel", txtLifeInsLevel
		// rsMiscUpdate.SetData "FederalComp", txtFederalComp
		// rsMiscUpdate.SetData "FedCompAmount", txtFedCompAmount
		// rsMiscUpdate.SetData "WorkWeekPerYear", txtWorkWeekPerYear
		// rsMiscUpdate.SetData "PayRateCode", txtPayRateCode
		// rsMiscUpdate.SetData "MonthlyHD", cboMonthlyHD
		// rsMiscUpdate.SetData "RateOfPay", cboRateOfPay
		// End If
		//
		// rsMiscUpdate.SetData "Unemployment", cboUnemployment
		// rsMiscUpdate.SetData "NatureCode", txtNatureCode
		// rsMiscUpdate.SetData "LastUserID", gstrUser
		// rsMiscUpdate.Update
		//
		// all changes have been updated
		// intDataChanged = 0
		// Call clsHistoryClass.Compare
		// MsgBox "Save was successful", vbInformation + vbOKOnly, "Payroll MSRS Update"
		// End Sub
		private void frmMiscUpdate_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				this.txtSSN.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				this.txtFullName.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				this.txtDateHire.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				FormLoading = true;
				//intWidth = cboMonthlyHD.WidthOriginal;
				string intTemp = "";
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
				{
					if (strEmployeeNumber == modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber)
						return;
					// need to make sure that the save happens to the correct employee
					intTemp = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = strEmployeeNumber;
					SaveChanges();
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intTemp;
					// Get the data from the database
					ShowData();
					if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
					{
						MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
						// fraEmployee.Text = fraEmployee.Tag
					}
					else
					{
						// fraEmployee.Text = fraEmployee.Tag & gtypeCurrentEmployee.EMPLOYEENUMBER & " Information"
						clsDRWrapper rsEmployee = new clsDRWrapper();
						rsEmployee.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
						if (!rsEmployee.EndOfFile())
						{
							txtFullName.Text = fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("FirstName") + " ") + " " + fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("LastName") + " ") + " " + fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("Desig") + " ");
							// txtDateHire = LongDate(Trim(rsEmployee.Fields("DateHire") & " "))
							txtDateHire.Text = Strings.Format(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields("DateHire"))), "MM/dd/yyyy");
                            if (ssnViewPermission == SSNViewType.Masked)
                            {
                                txtSSN.Text = "***-**-" + rsEmployee.Get_Fields_String("SSN").Right(4);
                            }
                            else if (ssnViewPermission == SSNViewType.None)
                            {
                                txtSSN.Text = "***-**-****";
                            }
                            else
                            {
                                txtSSN.Text = Strings.Format(fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("SSN") + " "), "000-00-0000");
                            }
                            
							// txtStatus = Trim(rsEmployee.Fields("Status") & " ")
						}
						rsEmployee = null;
						strEmployeeNumber = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
					}
					intDataChanged = 0;
				}
				SetControls();
				FormLoading = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmMiscUpdate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyDown";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (KeyCode == Keys.Return)
				{
					if (this.ActiveControl.GetName() == "txtNatureCode")
					{
						chkInclude.Focus();
					}
					else
					{
						Support.SendKeys("{TAB}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmMiscUpdate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (KeyAscii == Keys.Escape)
				{
					Close();
					//MDIParent.InstancePtr.Show();
					return;
				}
				if (this.ActiveControl.GetName() == this.txtDateHire.Name || this.ActiveControl.GetName() == this.txtSSN.Text)
				{
					KeyAscii = (Keys)0;
				}
				if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
				{
					intDataChanged = 1;
				}
				if (KeyAscii >= Keys.A && KeyAscii <= Keys.F11)
				{
					intDataChanged = 1;
				}
				if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
				{
					KeyAscii = KeyAscii - 32;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmMiscUpdate_Load(object sender, System.EventArgs e)
		{

			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolCantEdit = false;
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				SetupGrid();
				SetupCombos();
				FillcmbPositionCategory();
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClass();
                // -------------------------------------------------------------------
                // Get the data from the database
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewSocialSecurityNumbers.ToInteger()))
                {
                    case "F":
                        ssnViewPermission = SSNViewType.Full;
                        break;
                    case "P":
                        ssnViewPermission = SSNViewType.Masked;
                        break;
                    default:
                        ssnViewPermission = SSNViewType.None;
                        break;
                }
                LoadData();
				ShowData();
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// fraEmployee.Text = fraEmployee.Tag
				}
				else
				{
					// fraEmployee.Text = fraEmployee.Tag & gtypeCurrentEmployee.EMPLOYEENUMBER & " Information"
					clsDRWrapper rsEmployee = new clsDRWrapper();
					rsEmployee.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
					if (!rsEmployee.EndOfFile())
					{
						txtFullName.Text = fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("FirstName") + " ") + " " + fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("LastName") + " ") + " " + fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("Desig") + " ");
						// txtDateHire = LongDate(Trim(rsEmployee.Fields("DateHire") & " "))
						txtDateHire.Text = Strings.Format(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields("DateHire"))), "MM/dd/yyyy");						
                        if (ssnViewPermission == SSNViewType.Masked)
                        {
                            txtSSN.Text = "***-**-" + rsEmployee.Get_Fields_String("SSN").Right(4);
                        }
                        else if (ssnViewPermission == SSNViewType.None)
                        {
                            txtSSN.Text = "***-**-****";
                        }
                        else
                        {
                            txtSSN.Text = Strings.Format(fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("SSN") + " "), "000-00-0000");
                        }
                    }
					rsEmployee = null;
					strEmployeeNumber = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				}
				lblDescription.Font = FCUtils.FontChangeSize(lblDescription.Font, 8);
				intDataChanged = 0;
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					cmdAdd.Visible = false;
					mnuClear.Visible = false;
					cmdDelete.Visible = false;
					cmdSave.Visible = false;
					mnuSaveExit.Visible = false;
					Grid.Enabled = false;
					chkInclude.Enabled = false;
					cmbPositionCategory.Enabled = false;
					cboMonthlyHD.Enabled = false;
					cboRateOfPay.Enabled = false;
					cboUnemployment.Enabled = false;
					gridLifeInsScheduleCode.Enabled = false;
					gridLifeInsuranceCode.Enabled = false;
					gridPlanCode.Enabled = false;
					// txtDateHire.Enabled = False
					txtExcess.Enabled = false;
					txtFedCompAmount.Enabled = false;
					txtFederalComp.Enabled = false;
					txtLifeInsLevel.Enabled = false;
					txtMonthlyHDFactor.Enabled = false;
					txtNatureCode.Enabled = false;
					txtPayRateCode.Enabled = false;
					txtPositionCode.Enabled = false;
					txtRateOfPayFactor.Enabled = false;
					txtWorkWeekPerYear.Enabled = false;
				}
				clsHistoryClass.SetGridIDColumn("PY");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetDefaultValues()
		{
			cboMonthlyHD.SelectedIndex = 0;
			cboRateOfPay.SelectedIndex = 0;
			cmbPositionCategory.SelectedIndex = 0;
			// cboUnemployment.ListIndex = 1
			txtExcess.Text = "-";
			// txtPayPeriodCode = "-"
			txtFederalComp.Text = "D";
			txtRateOfPayFactor.Text = "1.00";
			txtMonthlyHDFactor.Text = "1.00";
			txtPayRateCode.Text = "H";
			txtFedCompAmount.Text = FCConvert.ToString(0);
			txtWorkWeekPerYear.Text = FCConvert.ToString(52);
			txtLifeInsLevel.Text = FCConvert.ToString(0);
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				// get all of the records from the database
				cmdNew_Click();
				SetDefaultValues();
				rsMiscUpdate.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				if (!rsMiscUpdate.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsMiscUpdate.Get_Fields("Include")))
					{
						chkInclude.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkInclude.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// txtLifeInsCode = rsMiscUpdate.Fields("LifeInsCode")
					gridLifeInsuranceCode.TextMatrix(0, 0, FCConvert.ToString(rsMiscUpdate.Get_Fields("lifeinscode")));
					gridLifeInsScheduleCode.TextMatrix(0, 0, FCConvert.ToString(rsMiscUpdate.Get_Fields("payperiodscode")));
					txtMonthlyHDFactor.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields("MonthlyFactor"));
					if (FCConvert.ToString(rsMiscUpdate.Get_Fields("MonthlyFactor")) == string.Empty)
						cboMonthlyHD.SelectedIndex = 0;
					txtRateOfPayFactor.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields("PayFactor"));
					txtStatusCode.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("StatusCode"));
					txtPositionCode.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("PositionCode"));
					// txtPlanCode = rsMiscUpdate.Fields("PlanCode")
					gridPlanCode.TextMatrix(0, 0, FCConvert.ToString(rsMiscUpdate.Get_Fields("plancode")));
					txtExcess.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("Excess"));
					// txtPayPeriodCode = rsMiscUpdate.Fields("PayPeriodsCode")
					txtLifeInsLevel.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("LifeInsLevel"));
					txtFederalComp.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("FederalComp"));
					txtFedCompAmount.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_Double("FedCompAmount"));
					txtWorkWeekPerYear.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_Int32("WorkWeekPerYear"));
					txtPayRateCode.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("PayRateCode"));
					cboUnemployment.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields("Unemployment"));
					if (cboUnemployment.Text == "N")
					{
						//txtNatureCode.BackColor = this.BackColor;
						txtNatureCode.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					else
					{
						txtNatureCode.BackColor = Color.White;
					}
					txtNatureCode.ReadOnly = cboUnemployment.Text == "N";
					// 
					txtNatureCode.Text = FCConvert.ToString(rsMiscUpdate.Get_Fields_String("NatureCode"));
					cboMonthlyHD.Text = fecherFoundation.Strings.Trim(rsMiscUpdate.Get_Fields_String("MonthlyHD") + " ");
					cboRateOfPay.Text = fecherFoundation.Strings.Trim(rsMiscUpdate.Get_Fields_String("RateOfPay") + " ");
					for (x = 0; x <= cmbPositionCategory.Items.Count - 1; x++)
					{
						if (cmbPositionCategory.ItemData(x) == Conversion.Val(rsMiscUpdate.Get_Fields_Int32("MSRSJobCategory")))
						{
							cmbPositionCategory.SelectedIndex = x;
							break;
						}
					}
					// x
				}
                //FC:FINAL:AM:#2587 - don't set to null the reader
                //rsMiscUpdate = null;
                rsMiscUpdate = new clsDRWrapper();
				intDataChanged = 0;
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// Call cmdSave_Click
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			// Call cmdSave_Click
			// If boolSaveOK Then cmdExit_Click
			if (SaveData())
			{
				cmdExit_Click();
			}
		}

		private void txtDateHire_TextChanged(object sender, System.EventArgs e)
		{
			// intDataChanged = intDataChanged + 1
		}

		private void txtExcess_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtExcess_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "E = Excess contributions" + "\r\n" + "P = Re/Purchase of time" + "\r\n" + "- = Neither";
		}

		private void txtExcess_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ((fecherFoundation.Strings.UCase(txtExcess.Text) == "E") || (fecherFoundation.Strings.UCase(txtExcess.Text) == "P") || (fecherFoundation.Strings.UCase(txtExcess.Text) == "-"))
			{
				e.Cancel = false;
				txtExcess.Text = fecherFoundation.Strings.UCase(txtExcess.Text);
			}
			else
			{
				// Cancel = True
				MessageBox.Show("Invalid Code", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtFedCompAmount_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "If a portion of earnable" + "\r\n" + "compensation is federal" + "\r\n" + "compensation, enter the amount" + "\r\n" + "here as a dollar amount or percentage.";
		}

		private void txtFederalComp_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtFederalComp_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "If a portion of earnable" + "\r\n" + "compensation is federal" + "\r\n" + "compensation, enter the code" + "\r\n" + "here to indicate the amount" + "\r\n" + "entered was D-Dollar amount" + "\r\n" + "or P-Percentage.";
		}

		private void txtFederalComp_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (FCConvert.ToDouble(fecherFoundation.Strings.UCase(FCConvert.ToString(FCConvert.ToInt32(KeyAscii)))) == 68 || FCConvert.ToDouble(fecherFoundation.Strings.UCase(FCConvert.ToString(FCConvert.ToInt32(KeyAscii)))) == 80)
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalComp_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If txtFederalComp.Text <> "D" Or txtFederalComp.Text <> "P" Then
			// Cancel = True
			// Call MsgBox("Invalid Entry, Enter D or P", vbCritical, "Error")
			// End If
		}

		private void txtFullName_TextChanged(object sender, System.EventArgs e)
		{
			// intDataChanged = intDataChanged + 1
		}

		private void txtLifeInsCode_Change()
		{
			intDataChanged += 1;
		}
		// Private Sub txtLifeInsCode_GotFocus()
		// ActiveControl.SelStart = 0
		// ActiveControl.SelLength = 20
		// lblDescription.Text = "B = Basic" & vbNewLine & "HA,HB = Basic + Dep A or Dep B" & vbNewLine
		// & "S1,S2,S3 = Basic + Supp 1/2/3" & vbNewLine & "F1A,F2A,F3A = Basic + Supp 1/2/3 And Dep A" & vbNewLine
		// & "F1B,F2B,F3B = Basic + Supp 1/2/3 And Dep B" & vbNewLine
		// & "I = Ineligible" & vbNewLine & "R = Refused" & vbNewLine & "OC = Other Coverage"
		// End Sub
		// Private Sub txtLifeInsCode_Validate(Cancel As Boolean)
		// Select Case (txtLifeInsCode.Text)
		// Case "B", "HA", "HB", "S1", "S2", "S3", "F1A", "F2A", "F3A", "F1B", "F2B", "F3B", "I", "R", "OC"
		// Cancel = False
		// Case Else
		// Cancel = True
		// Call MsgBox("Invalid Code", vbCritical, "Error")
		// txtLifeInsCode.SetFocus
		// End Select
		//
		// End Sub
		private void txtLifeInsLevel_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtLifeInsLevel_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Life insurance coverage based on total gross" + "\r\n" + "compensation in the previous calendar year.";
		}

		private void txtLifeInsLevel_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtLifeInsLevel.Text) && txtLifeInsLevel.Text != string.Empty)
			{
				// Cancel = True
				MessageBox.Show("Invalid Entry", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMonthlyHDFactor_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtMonthlyHDFactor_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Entered hours, ETC must be multiplied or divided to get" + "\r\n" + "to the units to be reported. Amount to be mult or div by.";
		}

		private void txtMonthlyHDFactor_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMonthlyHDFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMonthlyHDFactor.Text) > 99.99)
			{
				// Cancel = True
				MessageBox.Show("Invalid Entry" + "\r\n" + "Range 0.0-99.99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			if (txtMonthlyHDFactor.Text == string.Empty)
			{
				txtMonthlyHDFactor.Text = FCConvert.ToString(0);
			}
		}

		private void txtNatureCode_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}
		//
		private void txtNatureCode_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Nature code as required";
		}
		//
		private void txtNatureCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cboUnemployment.Text == "Y")
			{
				if (!Information.IsNumeric(txtNatureCode.Text))
				{
					MessageBox.Show("Must Be Numeric", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					cboUnemployment.Text = "N";
					txtNatureCode.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					txtNatureCode.ReadOnly = true;
					cboUnemployment.Focus();
				}
			}
		}

		private void txtPayPeriodCode_Change()
		{
			intDataChanged += 1;
		}

		private void txtPayPeriodCode_GotFocus()
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Number of payroll premium deductions per year" + "\r\n" + "\r\n" + "B-I for biweekly payrolls" + "\r\n" + "R-Z for weekly payrolls" + "\r\n" + "- = Neither";
		}

		private void txtPayPeriodCode_KeyPress(ref int KeyAscii)
		{
			// must be B-I or R-Z or -
			if (KeyAscii == 45)
				return;
			if (KeyAscii == 8)
				return;
			if (KeyAscii > 64 && KeyAscii < 91)
			{
			}
			else
			{
				if (((KeyAscii - 32) < 66 || (KeyAscii - 32) > 73) && ((KeyAscii - 32) < 82 || (KeyAscii - 32) > 90) && KeyAscii != 45)
				{
					KeyAscii = 0;
				}
				else
				{
					KeyAscii -= 32;
				}
			}
		}

		private void txtPayRateCode_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtPayRateCode_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Enter pay rate code that indicates how the monthly" + "\r\n" + "hours and rate of pay will be shown.";
		}

		private void txtPayRateCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.D || KeyAscii == Keys.H)
			{
			}
			else if (KeyAscii == Keys.NumPad4 || KeyAscii == Keys.NumPad8)
			{
				KeyAscii = KeyAscii - 32;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// Private Sub txtPlanCode_Change()
		// intDataChanged = intDataChanged + 1
		// End Sub
		//
		// Private Sub txtPlanCode_GotFocus()
		// ActiveControl.SelStart = 0
		// ActiveControl.SelLength = 20
		// lblDescription.Text = "-- = No plan code" & vbNewLine & "AC = Regular plan A" & vbNewLine &
		// "BC = Regular plan B" & vbNewLine & "AN = Regular plan A-1" & vbNewLine & "1C-4C = Spec plan 1 thru 4" &
		// vbNewLine & "1N-4N = Spec plan 1-A thru 4-A" & vbNewLine & "11000,110001 Teacher plan"
		// End Sub
		//
		// Private Sub txtPlanCode_Validate(Cancel As Boolean)
		// Select Case (txtPlanCode.Text)
		// Case "--", "AC", "BC", "AN", "1C", "2C", "3C", "4C", "1N", "2N", "3N", "4N", "11000", "11001"
		// Cancel = False
		// Case Else
		// Cancel = True
		// Call MsgBox("Invalid Code", vbCritical, "Invalid")
		// End Select
		// End Sub
		private void txtPositionCode_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtPositionCode_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Examples:" + "\r\n" + "Y0101 = Classroom Teacher" + "\r\n" + "Y0105 = Special Ed Teacher" + "\r\n" + "Y0206 = Ed Tech III" + "\r\n" + "09901 = Member with or W/O ins" + "\r\n" + "09904 = Police Officer";
		}

		private void txtPositionCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Strings.Mid(txtPositionCode.Text, 1, 1) != "0" && Strings.Mid(txtPositionCode.Text, 1, 1) != "Y" && Strings.Mid(txtPositionCode.Text, 1, 1) != "N")
			{
				// Cancel = True
				MessageBox.Show("Invalid First Character" + "\r\n" + "Must Be 0,Y,N", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			if (!Information.IsNumeric(Strings.Mid(txtPositionCode.Text, 2, 4)) || txtPositionCode.Text.Length > 5)
			{
				// Cancel = True
				MessageBox.Show("Max Char Length 5" + "\r\n" + "Characters 2-5 Must Be Numeric", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtRateOfPayFactor_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtRateOfPayFactor_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Rate of pay must be multiplied or divided by to get to " + "\r\n" + "units to be reported. Amount to be mult or div by.";
		}

		private void txtRateOfPayFactor_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtRateOfPayFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtRateOfPayFactor.Text) > 99.99)
			{
				// Cancel = True
				MessageBox.Show("Invalid Entry" + "\r\n" + "Range 0.0-99.99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			if (txtRateOfPayFactor.Text == string.Empty)
			{
				txtRateOfPayFactor.Text = FCConvert.ToString(0);
			}
		}

		private void txtSSN_TextChanged(object sender, System.EventArgs e)
		{
			// intDataChanged = intDataChanged + 1
		}

		private void txtStatus_Change()
		{
			// intDataChanged = intDataChanged + 1
		}

		private void txtStatusCode_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtStatusCode_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Code specified to indicate" + "\r\n" + "employee's status." + "\r\n" + "Examples:" + "\r\n" + "11 = Full time regular" + "\r\n" + "12 = Part time regular" + "\r\n" + "52 = Insurance only-retired" + "\r\n" + "65 = Full time-insurance only" + "\r\n" + "67 = Part time-insurance only";
		}

		private void txtStatusCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStatusCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtStatusCode.Text) > 99)
			{
				// Cancel = True
				MessageBox.Show("Invalid Entry" + "\r\n" + "Range 0-99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			if (txtStatusCode.Text == string.Empty)
			{
				txtStatusCode.Text = FCConvert.ToString(0);
			}
		}

		private void txtWorkWeekPerYear_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtWorkWeekPerYear_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 20;
			lblDescription.Text = "Enter number of weeks per year" + "\r\n" + "considered full time for this position";
		}

		private void txtWorkWeekPerYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtWorkWeekPerYear.Text))
			{
				// Cancel = True
				MessageBox.Show("Must Be Numeric", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else if (Conversion.Val(txtWorkWeekPerYear.Text) < 0)
			{
				// Cancel = True
				MessageBox.Show("Can't Work Negative Hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else if (Conversion.Val(txtWorkWeekPerYear.Text) > 52)
			{
				// Cancel = True
				MessageBox.Show("Can't Work More Then 52 Weeks", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkInclude";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Include in MSRS";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "gridLifeInsuranceCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Life Insurance Code";
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "gridPlanCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Plan Code";
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtExcess";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Excess / Purchase";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "gridLifeInsScheduleCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Life Insurance Schedule Code";
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtLifeInsLevel";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Life Insurance Level";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cmbPositionCategory";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Position Category";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboMonthlyHD";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Monthly H/D: M or D";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMonthlyHDFactor";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Monthly H/D: Factor";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboRateOfPay";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Rate Of Pay: M or D";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtRateOfPayFactor";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Rate Of Pay: Factor";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboUnemployment";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Include Y/N/S";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtNatureCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Nature Code";
			intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			for (intRows = 1; intRows <= (Grid.Rows - 1); intRows++)
			{
				for (intCols = 2; intCols <= (Grid.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "Grid";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + Grid.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
		}
	}
}
