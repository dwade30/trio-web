﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt941ScheduleB.
	/// </summary>
	partial class srpt941ScheduleB
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srpt941ScheduleB));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.txtMonth1Line1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Line31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Line31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Line31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEINBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQuarter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQuarter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQuarter3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQuarter4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearMillenium = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearCentury = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearDecade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearMillenium)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearCentury)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearDecade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Image1,
				this.txtMonth1Line1,
				this.txtMonth1Line2,
				this.txtMonth1Line3,
				this.txtMonth1Line4,
				this.txtMonth1Line5,
				this.txtMonth1Line6,
				this.txtMonth1Line7,
				this.txtMonth1Line8,
				this.txtMonth1Line9,
				this.txtMonth1Line10,
				this.txtMonth1Line11,
				this.txtMonth1Line12,
				this.txtMonth1Line13,
				this.txtMonth1Line14,
				this.txtMonth1Line15,
				this.txtMonth1Line16,
				this.txtMonth1Line17,
				this.txtMonth1Line18,
				this.txtMonth1Line19,
				this.txtMonth1Line20,
				this.txtMonth1Line21,
				this.txtMonth1Line22,
				this.txtMonth1Line23,
				this.txtMonth1Line24,
				this.txtMonth1Line25,
				this.txtMonth1Line26,
				this.txtMonth1Line27,
				this.txtMonth1Line28,
				this.txtMonth1Line29,
				this.txtMonth1Line30,
				this.txtMonth1Line31,
				this.txtMonth2Line1,
				this.txtMonth2Line2,
				this.txtMonth2Line3,
				this.txtMonth2Line4,
				this.txtMonth2Line5,
				this.txtMonth2Line6,
				this.txtMonth2Line7,
				this.txtMonth2Line8,
				this.txtMonth2Line9,
				this.txtMonth2Line10,
				this.txtMonth2Line11,
				this.txtMonth2Line12,
				this.txtMonth2Line13,
				this.txtMonth2Line14,
				this.txtMonth2Line15,
				this.txtMonth2Line16,
				this.txtMonth2Line17,
				this.txtMonth2Line18,
				this.txtMonth2Line19,
				this.txtMonth2Line20,
				this.txtMonth2Line21,
				this.txtMonth2Line22,
				this.txtMonth2Line23,
				this.txtMonth2Line24,
				this.txtMonth2Line25,
				this.txtMonth2Line26,
				this.txtMonth2Line27,
				this.txtMonth2Line28,
				this.txtMonth2Line29,
				this.txtMonth2Line30,
				this.txtMonth2Line31,
				this.txtMonth3Line1,
				this.txtMonth3Line2,
				this.txtMonth3Line3,
				this.txtMonth3Line4,
				this.txtMonth3Line5,
				this.txtMonth3Line6,
				this.txtMonth3Line7,
				this.txtMonth3Line8,
				this.txtMonth3Line9,
				this.txtMonth3Line10,
				this.txtMonth3Line11,
				this.txtMonth3Line12,
				this.txtMonth3Line13,
				this.txtMonth3Line14,
				this.txtMonth3Line15,
				this.txtMonth3Line16,
				this.txtMonth3Line17,
				this.txtMonth3Line18,
				this.txtMonth3Line19,
				this.txtMonth3Line20,
				this.txtMonth3Line21,
				this.txtMonth3Line22,
				this.txtMonth3Line23,
				this.txtMonth3Line24,
				this.txtMonth3Line25,
				this.txtMonth3Line26,
				this.txtMonth3Line27,
				this.txtMonth3Line28,
				this.txtMonth3Line29,
				this.txtMonth3Line30,
				this.txtMonth3Line31,
				this.txtMonth3Total,
				this.txtTotal,
				this.txtMonth2Total,
				this.txtMonth1Total,
				this.txtName,
				this.txtEINBox1,
				this.txtEINBox2,
				this.txtEINBox3,
				this.txtEINBox4,
				this.txtEINBox5,
				this.txtEINBox6,
				this.txtEINBox7,
				this.txtEINBox8,
				this.txtEINBox9,
				this.txtQuarter1,
				this.txtQuarter2,
				this.txtQuarter3,
				this.txtQuarter4,
				this.txtYearMillenium,
				this.txtYearCentury,
				this.txtYearDecade,
				this.txtYearYear
			});
			this.Detail.Height = 10.02083F;
			this.Detail.Name = "Detail";
			// 
			// Image1
			// 
			this.Image1.BackColor = System.Drawing.Color.White;
			this.Image1.Height = 10F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = ((System.IO.Stream)(resources.GetObject("Image1.ImageData")));
			this.Image1.Left = 0F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.Image1.Top = 0F;
			this.Image1.Width = 8F;
			// 
			// txtMonth1Line1
			// 
			this.txtMonth1Line1.Height = 0.1875F;
			this.txtMonth1Line1.Left = 0.5208333F;
			this.txtMonth1Line1.Name = "txtMonth1Line1";
			this.txtMonth1Line1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line1.Text = null;
			this.txtMonth1Line1.Top = 3.125F;
			this.txtMonth1Line1.Width = 1.125F;
			// 
			// txtMonth1Line2
			// 
			this.txtMonth1Line2.Height = 0.1875F;
			this.txtMonth1Line2.Left = 0.5208333F;
			this.txtMonth1Line2.Name = "txtMonth1Line2";
			this.txtMonth1Line2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line2.Text = null;
			this.txtMonth1Line2.Top = 3.354167F;
			this.txtMonth1Line2.Width = 1.125F;
			// 
			// txtMonth1Line3
			// 
			this.txtMonth1Line3.Height = 0.1875F;
			this.txtMonth1Line3.Left = 0.5208333F;
			this.txtMonth1Line3.Name = "txtMonth1Line3";
			this.txtMonth1Line3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line3.Text = null;
			this.txtMonth1Line3.Top = 3.576389F;
			this.txtMonth1Line3.Width = 1.125F;
			// 
			// txtMonth1Line4
			// 
			this.txtMonth1Line4.Height = 0.1875F;
			this.txtMonth1Line4.Left = 0.5208333F;
			this.txtMonth1Line4.Name = "txtMonth1Line4";
			this.txtMonth1Line4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; vertical-align: top";
			this.txtMonth1Line4.Text = null;
			this.txtMonth1Line4.Top = 3.8125F;
			this.txtMonth1Line4.Width = 1.125F;
			// 
			// txtMonth1Line5
			// 
			this.txtMonth1Line5.Height = 0.1875F;
			this.txtMonth1Line5.Left = 0.5208333F;
			this.txtMonth1Line5.Name = "txtMonth1Line5";
			this.txtMonth1Line5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line5.Text = null;
			this.txtMonth1Line5.Top = 4.048611F;
			this.txtMonth1Line5.Width = 1.125F;
			// 
			// txtMonth1Line6
			// 
			this.txtMonth1Line6.Height = 0.1875F;
			this.txtMonth1Line6.Left = 0.5208333F;
			this.txtMonth1Line6.Name = "txtMonth1Line6";
			this.txtMonth1Line6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line6.Text = null;
			this.txtMonth1Line6.Top = 4.284722F;
			this.txtMonth1Line6.Width = 1.125F;
			// 
			// txtMonth1Line7
			// 
			this.txtMonth1Line7.Height = 0.1875F;
			this.txtMonth1Line7.Left = 0.5208333F;
			this.txtMonth1Line7.Name = "txtMonth1Line7";
			this.txtMonth1Line7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line7.Text = null;
			this.txtMonth1Line7.Top = 4.520833F;
			this.txtMonth1Line7.Width = 1.125F;
			// 
			// txtMonth1Line8
			// 
			this.txtMonth1Line8.Height = 0.1875F;
			this.txtMonth1Line8.Left = 0.5208333F;
			this.txtMonth1Line8.Name = "txtMonth1Line8";
			this.txtMonth1Line8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line8.Text = null;
			this.txtMonth1Line8.Top = 4.767361F;
			this.txtMonth1Line8.Width = 1.125F;
			// 
			// txtMonth1Line9
			// 
			this.txtMonth1Line9.Height = 0.1875F;
			this.txtMonth1Line9.Left = 1.916667F;
			this.txtMonth1Line9.Name = "txtMonth1Line9";
			this.txtMonth1Line9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line9.Text = null;
			this.txtMonth1Line9.Top = 3.125F;
			this.txtMonth1Line9.Width = 1.125F;
			// 
			// txtMonth1Line10
			// 
			this.txtMonth1Line10.Height = 0.1875F;
			this.txtMonth1Line10.Left = 1.916667F;
			this.txtMonth1Line10.Name = "txtMonth1Line10";
			this.txtMonth1Line10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line10.Text = null;
			this.txtMonth1Line10.Top = 3.354167F;
			this.txtMonth1Line10.Width = 1.125F;
			// 
			// txtMonth1Line11
			// 
			this.txtMonth1Line11.Height = 0.1875F;
			this.txtMonth1Line11.Left = 1.916667F;
			this.txtMonth1Line11.Name = "txtMonth1Line11";
			this.txtMonth1Line11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line11.Text = null;
			this.txtMonth1Line11.Top = 3.576389F;
			this.txtMonth1Line11.Width = 1.125F;
			// 
			// txtMonth1Line12
			// 
			this.txtMonth1Line12.Height = 0.1875F;
			this.txtMonth1Line12.Left = 1.916667F;
			this.txtMonth1Line12.Name = "txtMonth1Line12";
			this.txtMonth1Line12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; vertical-align: top";
			this.txtMonth1Line12.Text = null;
			this.txtMonth1Line12.Top = 3.8125F;
			this.txtMonth1Line12.Width = 1.125F;
			// 
			// txtMonth1Line13
			// 
			this.txtMonth1Line13.Height = 0.1875F;
			this.txtMonth1Line13.Left = 1.916667F;
			this.txtMonth1Line13.Name = "txtMonth1Line13";
			this.txtMonth1Line13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line13.Text = null;
			this.txtMonth1Line13.Top = 4.048611F;
			this.txtMonth1Line13.Width = 1.125F;
			// 
			// txtMonth1Line14
			// 
			this.txtMonth1Line14.Height = 0.1875F;
			this.txtMonth1Line14.Left = 1.916667F;
			this.txtMonth1Line14.Name = "txtMonth1Line14";
			this.txtMonth1Line14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line14.Text = null;
			this.txtMonth1Line14.Top = 4.284722F;
			this.txtMonth1Line14.Width = 1.125F;
			// 
			// txtMonth1Line15
			// 
			this.txtMonth1Line15.Height = 0.1875F;
			this.txtMonth1Line15.Left = 1.916667F;
			this.txtMonth1Line15.Name = "txtMonth1Line15";
			this.txtMonth1Line15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line15.Text = null;
			this.txtMonth1Line15.Top = 4.520833F;
			this.txtMonth1Line15.Width = 1.125F;
			// 
			// txtMonth1Line16
			// 
			this.txtMonth1Line16.Height = 0.1875F;
			this.txtMonth1Line16.Left = 1.916667F;
			this.txtMonth1Line16.Name = "txtMonth1Line16";
			this.txtMonth1Line16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line16.Text = null;
			this.txtMonth1Line16.Top = 4.767361F;
			this.txtMonth1Line16.Width = 1.125F;
			// 
			// txtMonth1Line17
			// 
			this.txtMonth1Line17.Height = 0.1875F;
			this.txtMonth1Line17.Left = 3.291667F;
			this.txtMonth1Line17.Name = "txtMonth1Line17";
			this.txtMonth1Line17.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line17.Text = null;
			this.txtMonth1Line17.Top = 3.125F;
			this.txtMonth1Line17.Width = 1.125F;
			// 
			// txtMonth1Line18
			// 
			this.txtMonth1Line18.Height = 0.1875F;
			this.txtMonth1Line18.Left = 3.291667F;
			this.txtMonth1Line18.Name = "txtMonth1Line18";
			this.txtMonth1Line18.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line18.Text = null;
			this.txtMonth1Line18.Top = 3.354167F;
			this.txtMonth1Line18.Width = 1.125F;
			// 
			// txtMonth1Line19
			// 
			this.txtMonth1Line19.Height = 0.1875F;
			this.txtMonth1Line19.Left = 3.291667F;
			this.txtMonth1Line19.Name = "txtMonth1Line19";
			this.txtMonth1Line19.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line19.Text = null;
			this.txtMonth1Line19.Top = 3.576389F;
			this.txtMonth1Line19.Width = 1.125F;
			// 
			// txtMonth1Line20
			// 
			this.txtMonth1Line20.Height = 0.1875F;
			this.txtMonth1Line20.Left = 3.291667F;
			this.txtMonth1Line20.Name = "txtMonth1Line20";
			this.txtMonth1Line20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; vertical-align: top";
			this.txtMonth1Line20.Text = null;
			this.txtMonth1Line20.Top = 3.8125F;
			this.txtMonth1Line20.Width = 1.125F;
			// 
			// txtMonth1Line21
			// 
			this.txtMonth1Line21.Height = 0.1875F;
			this.txtMonth1Line21.Left = 3.291667F;
			this.txtMonth1Line21.Name = "txtMonth1Line21";
			this.txtMonth1Line21.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line21.Text = null;
			this.txtMonth1Line21.Top = 4.048611F;
			this.txtMonth1Line21.Width = 1.125F;
			// 
			// txtMonth1Line22
			// 
			this.txtMonth1Line22.Height = 0.1875F;
			this.txtMonth1Line22.Left = 3.291667F;
			this.txtMonth1Line22.Name = "txtMonth1Line22";
			this.txtMonth1Line22.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line22.Text = null;
			this.txtMonth1Line22.Top = 4.284722F;
			this.txtMonth1Line22.Width = 1.125F;
			// 
			// txtMonth1Line23
			// 
			this.txtMonth1Line23.Height = 0.1875F;
			this.txtMonth1Line23.Left = 3.291667F;
			this.txtMonth1Line23.Name = "txtMonth1Line23";
			this.txtMonth1Line23.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line23.Text = null;
			this.txtMonth1Line23.Top = 4.520833F;
			this.txtMonth1Line23.Width = 1.125F;
			// 
			// txtMonth1Line24
			// 
			this.txtMonth1Line24.Height = 0.1875F;
			this.txtMonth1Line24.Left = 3.291667F;
			this.txtMonth1Line24.Name = "txtMonth1Line24";
			this.txtMonth1Line24.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line24.Text = null;
			this.txtMonth1Line24.Top = 4.767361F;
			this.txtMonth1Line24.Width = 1.125F;
			// 
			// txtMonth1Line25
			// 
			this.txtMonth1Line25.Height = 0.1875F;
			this.txtMonth1Line25.Left = 4.6875F;
			this.txtMonth1Line25.Name = "txtMonth1Line25";
			this.txtMonth1Line25.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line25.Text = null;
			this.txtMonth1Line25.Top = 3.125F;
			this.txtMonth1Line25.Width = 1.125F;
			// 
			// txtMonth1Line26
			// 
			this.txtMonth1Line26.Height = 0.1875F;
			this.txtMonth1Line26.Left = 4.6875F;
			this.txtMonth1Line26.Name = "txtMonth1Line26";
			this.txtMonth1Line26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line26.Text = null;
			this.txtMonth1Line26.Top = 3.354167F;
			this.txtMonth1Line26.Width = 1.125F;
			// 
			// txtMonth1Line27
			// 
			this.txtMonth1Line27.Height = 0.1875F;
			this.txtMonth1Line27.Left = 4.6875F;
			this.txtMonth1Line27.Name = "txtMonth1Line27";
			this.txtMonth1Line27.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line27.Text = null;
			this.txtMonth1Line27.Top = 3.576389F;
			this.txtMonth1Line27.Width = 1.125F;
			// 
			// txtMonth1Line28
			// 
			this.txtMonth1Line28.Height = 0.1875F;
			this.txtMonth1Line28.Left = 4.6875F;
			this.txtMonth1Line28.Name = "txtMonth1Line28";
			this.txtMonth1Line28.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; vertical-align: top";
			this.txtMonth1Line28.Text = null;
			this.txtMonth1Line28.Top = 3.8125F;
			this.txtMonth1Line28.Width = 1.125F;
			// 
			// txtMonth1Line29
			// 
			this.txtMonth1Line29.Height = 0.1875F;
			this.txtMonth1Line29.Left = 4.6875F;
			this.txtMonth1Line29.Name = "txtMonth1Line29";
			this.txtMonth1Line29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line29.Text = null;
			this.txtMonth1Line29.Top = 4.048611F;
			this.txtMonth1Line29.Width = 1.125F;
			// 
			// txtMonth1Line30
			// 
			this.txtMonth1Line30.Height = 0.1875F;
			this.txtMonth1Line30.Left = 4.6875F;
			this.txtMonth1Line30.Name = "txtMonth1Line30";
			this.txtMonth1Line30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line30.Text = null;
			this.txtMonth1Line30.Top = 4.284722F;
			this.txtMonth1Line30.Width = 1.125F;
			// 
			// txtMonth1Line31
			// 
			this.txtMonth1Line31.Height = 0.1875F;
			this.txtMonth1Line31.Left = 4.6875F;
			this.txtMonth1Line31.Name = "txtMonth1Line31";
			this.txtMonth1Line31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Line31.Text = null;
			this.txtMonth1Line31.Top = 4.520833F;
			this.txtMonth1Line31.Width = 1.125F;
			// 
			// txtMonth2Line1
			// 
			this.txtMonth2Line1.Height = 0.1875F;
			this.txtMonth2Line1.Left = 0.5208333F;
			this.txtMonth2Line1.Name = "txtMonth2Line1";
			this.txtMonth2Line1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line1.Text = null;
			this.txtMonth2Line1.Top = 5.177083F;
			this.txtMonth2Line1.Width = 1.125F;
			// 
			// txtMonth2Line2
			// 
			this.txtMonth2Line2.Height = 0.1875F;
			this.txtMonth2Line2.Left = 0.5208333F;
			this.txtMonth2Line2.Name = "txtMonth2Line2";
			this.txtMonth2Line2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; vertical-align: top";
			this.txtMonth2Line2.Text = null;
			this.txtMonth2Line2.Top = 5.413195F;
			this.txtMonth2Line2.Width = 1.125F;
			// 
			// txtMonth2Line3
			// 
			this.txtMonth2Line3.Height = 0.1875F;
			this.txtMonth2Line3.Left = 0.5208333F;
			this.txtMonth2Line3.Name = "txtMonth2Line3";
			this.txtMonth2Line3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line3.Text = null;
			this.txtMonth2Line3.Top = 5.649305F;
			this.txtMonth2Line3.Width = 1.125F;
			// 
			// txtMonth2Line4
			// 
			this.txtMonth2Line4.Height = 0.1875F;
			this.txtMonth2Line4.Left = 0.5208333F;
			this.txtMonth2Line4.Name = "txtMonth2Line4";
			this.txtMonth2Line4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line4.Text = null;
			this.txtMonth2Line4.Top = 5.885417F;
			this.txtMonth2Line4.Width = 1.125F;
			// 
			// txtMonth2Line5
			// 
			this.txtMonth2Line5.Height = 0.1875F;
			this.txtMonth2Line5.Left = 0.5208333F;
			this.txtMonth2Line5.Name = "txtMonth2Line5";
			this.txtMonth2Line5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line5.Text = null;
			this.txtMonth2Line5.Top = 6.121528F;
			this.txtMonth2Line5.Width = 1.125F;
			// 
			// txtMonth2Line6
			// 
			this.txtMonth2Line6.Height = 0.1875F;
			this.txtMonth2Line6.Left = 0.5208333F;
			this.txtMonth2Line6.Name = "txtMonth2Line6";
			this.txtMonth2Line6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line6.Text = null;
			this.txtMonth2Line6.Top = 6.357639F;
			this.txtMonth2Line6.Width = 1.125F;
			// 
			// txtMonth2Line7
			// 
			this.txtMonth2Line7.Height = 0.1875F;
			this.txtMonth2Line7.Left = 0.5208333F;
			this.txtMonth2Line7.Name = "txtMonth2Line7";
			this.txtMonth2Line7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line7.Text = null;
			this.txtMonth2Line7.Top = 6.59375F;
			this.txtMonth2Line7.Width = 1.125F;
			// 
			// txtMonth2Line8
			// 
			this.txtMonth2Line8.Height = 0.1875F;
			this.txtMonth2Line8.Left = 0.5208333F;
			this.txtMonth2Line8.Name = "txtMonth2Line8";
			this.txtMonth2Line8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line8.Text = null;
			this.txtMonth2Line8.Top = 6.829861F;
			this.txtMonth2Line8.Width = 1.125F;
			// 
			// txtMonth2Line9
			// 
			this.txtMonth2Line9.Height = 0.1875F;
			this.txtMonth2Line9.Left = 1.916667F;
			this.txtMonth2Line9.Name = "txtMonth2Line9";
			this.txtMonth2Line9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line9.Text = null;
			this.txtMonth2Line9.Top = 5.177083F;
			this.txtMonth2Line9.Width = 1.125F;
			// 
			// txtMonth2Line10
			// 
			this.txtMonth2Line10.Height = 0.1875F;
			this.txtMonth2Line10.Left = 1.916667F;
			this.txtMonth2Line10.Name = "txtMonth2Line10";
			this.txtMonth2Line10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line10.Text = null;
			this.txtMonth2Line10.Top = 5.416667F;
			this.txtMonth2Line10.Width = 1.125F;
			// 
			// txtMonth2Line11
			// 
			this.txtMonth2Line11.Height = 0.1875F;
			this.txtMonth2Line11.Left = 1.916667F;
			this.txtMonth2Line11.Name = "txtMonth2Line11";
			this.txtMonth2Line11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line11.Text = null;
			this.txtMonth2Line11.Top = 5.649305F;
			this.txtMonth2Line11.Width = 1.125F;
			// 
			// txtMonth2Line12
			// 
			this.txtMonth2Line12.Height = 0.1875F;
			this.txtMonth2Line12.Left = 1.916667F;
			this.txtMonth2Line12.Name = "txtMonth2Line12";
			this.txtMonth2Line12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line12.Text = null;
			this.txtMonth2Line12.Top = 5.885417F;
			this.txtMonth2Line12.Width = 1.125F;
			// 
			// txtMonth2Line13
			// 
			this.txtMonth2Line13.Height = 0.1875F;
			this.txtMonth2Line13.Left = 1.916667F;
			this.txtMonth2Line13.Name = "txtMonth2Line13";
			this.txtMonth2Line13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line13.Text = null;
			this.txtMonth2Line13.Top = 6.121528F;
			this.txtMonth2Line13.Width = 1.125F;
			// 
			// txtMonth2Line14
			// 
			this.txtMonth2Line14.Height = 0.1875F;
			this.txtMonth2Line14.Left = 1.916667F;
			this.txtMonth2Line14.Name = "txtMonth2Line14";
			this.txtMonth2Line14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line14.Text = null;
			this.txtMonth2Line14.Top = 6.357639F;
			this.txtMonth2Line14.Width = 1.125F;
			// 
			// txtMonth2Line15
			// 
			this.txtMonth2Line15.Height = 0.1875F;
			this.txtMonth2Line15.Left = 1.916667F;
			this.txtMonth2Line15.Name = "txtMonth2Line15";
			this.txtMonth2Line15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line15.Text = null;
			this.txtMonth2Line15.Top = 6.59375F;
			this.txtMonth2Line15.Width = 1.125F;
			// 
			// txtMonth2Line16
			// 
			this.txtMonth2Line16.Height = 0.1875F;
			this.txtMonth2Line16.Left = 1.916667F;
			this.txtMonth2Line16.Name = "txtMonth2Line16";
			this.txtMonth2Line16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line16.Text = null;
			this.txtMonth2Line16.Top = 6.829861F;
			this.txtMonth2Line16.Width = 1.125F;
			// 
			// txtMonth2Line17
			// 
			this.txtMonth2Line17.Height = 0.1875F;
			this.txtMonth2Line17.Left = 3.291667F;
			this.txtMonth2Line17.Name = "txtMonth2Line17";
			this.txtMonth2Line17.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line17.Text = null;
			this.txtMonth2Line17.Top = 5.177083F;
			this.txtMonth2Line17.Width = 1.125F;
			// 
			// txtMonth2Line18
			// 
			this.txtMonth2Line18.Height = 0.1875F;
			this.txtMonth2Line18.Left = 3.291667F;
			this.txtMonth2Line18.Name = "txtMonth2Line18";
			this.txtMonth2Line18.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line18.Text = null;
			this.txtMonth2Line18.Top = 5.416667F;
			this.txtMonth2Line18.Width = 1.125F;
			// 
			// txtMonth2Line19
			// 
			this.txtMonth2Line19.Height = 0.1875F;
			this.txtMonth2Line19.Left = 3.291667F;
			this.txtMonth2Line19.Name = "txtMonth2Line19";
			this.txtMonth2Line19.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line19.Text = null;
			this.txtMonth2Line19.Top = 5.649305F;
			this.txtMonth2Line19.Width = 1.125F;
			// 
			// txtMonth2Line20
			// 
			this.txtMonth2Line20.Height = 0.1875F;
			this.txtMonth2Line20.Left = 3.291667F;
			this.txtMonth2Line20.Name = "txtMonth2Line20";
			this.txtMonth2Line20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line20.Text = null;
			this.txtMonth2Line20.Top = 5.885417F;
			this.txtMonth2Line20.Width = 1.125F;
			// 
			// txtMonth2Line21
			// 
			this.txtMonth2Line21.Height = 0.1875F;
			this.txtMonth2Line21.Left = 3.291667F;
			this.txtMonth2Line21.Name = "txtMonth2Line21";
			this.txtMonth2Line21.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line21.Text = null;
			this.txtMonth2Line21.Top = 6.121528F;
			this.txtMonth2Line21.Width = 1.125F;
			// 
			// txtMonth2Line22
			// 
			this.txtMonth2Line22.Height = 0.1875F;
			this.txtMonth2Line22.Left = 3.291667F;
			this.txtMonth2Line22.Name = "txtMonth2Line22";
			this.txtMonth2Line22.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line22.Text = null;
			this.txtMonth2Line22.Top = 6.357639F;
			this.txtMonth2Line22.Width = 1.125F;
			// 
			// txtMonth2Line23
			// 
			this.txtMonth2Line23.Height = 0.1875F;
			this.txtMonth2Line23.Left = 3.291667F;
			this.txtMonth2Line23.Name = "txtMonth2Line23";
			this.txtMonth2Line23.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line23.Text = null;
			this.txtMonth2Line23.Top = 6.59375F;
			this.txtMonth2Line23.Width = 1.125F;
			// 
			// txtMonth2Line24
			// 
			this.txtMonth2Line24.Height = 0.1875F;
			this.txtMonth2Line24.Left = 3.291667F;
			this.txtMonth2Line24.Name = "txtMonth2Line24";
			this.txtMonth2Line24.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line24.Text = null;
			this.txtMonth2Line24.Top = 6.829861F;
			this.txtMonth2Line24.Width = 1.125F;
			// 
			// txtMonth2Line25
			// 
			this.txtMonth2Line25.Height = 0.1875F;
			this.txtMonth2Line25.Left = 4.6875F;
			this.txtMonth2Line25.Name = "txtMonth2Line25";
			this.txtMonth2Line25.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line25.Text = null;
			this.txtMonth2Line25.Top = 5.177083F;
			this.txtMonth2Line25.Width = 1.125F;
			// 
			// txtMonth2Line26
			// 
			this.txtMonth2Line26.Height = 0.1875F;
			this.txtMonth2Line26.Left = 4.6875F;
			this.txtMonth2Line26.Name = "txtMonth2Line26";
			this.txtMonth2Line26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line26.Text = null;
			this.txtMonth2Line26.Top = 5.416667F;
			this.txtMonth2Line26.Width = 1.125F;
			// 
			// txtMonth2Line27
			// 
			this.txtMonth2Line27.Height = 0.1875F;
			this.txtMonth2Line27.Left = 4.6875F;
			this.txtMonth2Line27.Name = "txtMonth2Line27";
			this.txtMonth2Line27.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line27.Text = null;
			this.txtMonth2Line27.Top = 5.649305F;
			this.txtMonth2Line27.Width = 1.125F;
			// 
			// txtMonth2Line28
			// 
			this.txtMonth2Line28.Height = 0.1875F;
			this.txtMonth2Line28.Left = 4.6875F;
			this.txtMonth2Line28.Name = "txtMonth2Line28";
			this.txtMonth2Line28.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line28.Text = null;
			this.txtMonth2Line28.Top = 5.885417F;
			this.txtMonth2Line28.Width = 1.125F;
			// 
			// txtMonth2Line29
			// 
			this.txtMonth2Line29.Height = 0.1875F;
			this.txtMonth2Line29.Left = 4.6875F;
			this.txtMonth2Line29.Name = "txtMonth2Line29";
			this.txtMonth2Line29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line29.Text = null;
			this.txtMonth2Line29.Top = 6.121528F;
			this.txtMonth2Line29.Width = 1.125F;
			// 
			// txtMonth2Line30
			// 
			this.txtMonth2Line30.Height = 0.1875F;
			this.txtMonth2Line30.Left = 4.6875F;
			this.txtMonth2Line30.Name = "txtMonth2Line30";
			this.txtMonth2Line30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line30.Text = null;
			this.txtMonth2Line30.Top = 6.357639F;
			this.txtMonth2Line30.Width = 1.125F;
			// 
			// txtMonth2Line31
			// 
			this.txtMonth2Line31.Height = 0.1875F;
			this.txtMonth2Line31.Left = 4.6875F;
			this.txtMonth2Line31.Name = "txtMonth2Line31";
			this.txtMonth2Line31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Line31.Text = null;
			this.txtMonth2Line31.Top = 6.59375F;
			this.txtMonth2Line31.Width = 1.125F;
			// 
			// txtMonth3Line1
			// 
			this.txtMonth3Line1.Height = 0.1875F;
			this.txtMonth3Line1.Left = 0.5208333F;
			this.txtMonth3Line1.Name = "txtMonth3Line1";
			this.txtMonth3Line1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line1.Text = null;
			this.txtMonth3Line1.Top = 7.253472F;
			this.txtMonth3Line1.Width = 1.125F;
			// 
			// txtMonth3Line2
			// 
			this.txtMonth3Line2.Height = 0.1875F;
			this.txtMonth3Line2.Left = 0.5208333F;
			this.txtMonth3Line2.Name = "txtMonth3Line2";
			this.txtMonth3Line2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line2.Text = null;
			this.txtMonth3Line2.Top = 7.482639F;
			this.txtMonth3Line2.Width = 1.125F;
			// 
			// txtMonth3Line3
			// 
			this.txtMonth3Line3.Height = 0.1875F;
			this.txtMonth3Line3.Left = 0.5208333F;
			this.txtMonth3Line3.Name = "txtMonth3Line3";
			this.txtMonth3Line3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line3.Text = null;
			this.txtMonth3Line3.Top = 7.71875F;
			this.txtMonth3Line3.Width = 1.125F;
			// 
			// txtMonth3Line4
			// 
			this.txtMonth3Line4.Height = 0.1875F;
			this.txtMonth3Line4.Left = 0.5208333F;
			this.txtMonth3Line4.Name = "txtMonth3Line4";
			this.txtMonth3Line4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line4.Text = null;
			this.txtMonth3Line4.Top = 7.954861F;
			this.txtMonth3Line4.Width = 1.125F;
			// 
			// txtMonth3Line5
			// 
			this.txtMonth3Line5.Height = 0.1875F;
			this.txtMonth3Line5.Left = 0.5208333F;
			this.txtMonth3Line5.Name = "txtMonth3Line5";
			this.txtMonth3Line5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line5.Text = null;
			this.txtMonth3Line5.Top = 8.190972F;
			this.txtMonth3Line5.Width = 1.125F;
			// 
			// txtMonth3Line6
			// 
			this.txtMonth3Line6.Height = 0.1875F;
			this.txtMonth3Line6.Left = 0.5208333F;
			this.txtMonth3Line6.Name = "txtMonth3Line6";
			this.txtMonth3Line6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line6.Text = null;
			this.txtMonth3Line6.Top = 8.427083F;
			this.txtMonth3Line6.Width = 1.125F;
			// 
			// txtMonth3Line7
			// 
			this.txtMonth3Line7.Height = 0.1875F;
			this.txtMonth3Line7.Left = 0.5208333F;
			this.txtMonth3Line7.Name = "txtMonth3Line7";
			this.txtMonth3Line7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line7.Text = null;
			this.txtMonth3Line7.Top = 8.663195F;
			this.txtMonth3Line7.Width = 1.125F;
			// 
			// txtMonth3Line8
			// 
			this.txtMonth3Line8.Height = 0.1875F;
			this.txtMonth3Line8.Left = 0.5208333F;
			this.txtMonth3Line8.Name = "txtMonth3Line8";
			this.txtMonth3Line8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line8.Text = null;
			this.txtMonth3Line8.Top = 8.899305F;
			this.txtMonth3Line8.Width = 1.125F;
			// 
			// txtMonth3Line9
			// 
			this.txtMonth3Line9.Height = 0.1875F;
			this.txtMonth3Line9.Left = 1.916667F;
			this.txtMonth3Line9.Name = "txtMonth3Line9";
			this.txtMonth3Line9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line9.Text = null;
			this.txtMonth3Line9.Top = 7.253472F;
			this.txtMonth3Line9.Width = 1.125F;
			// 
			// txtMonth3Line10
			// 
			this.txtMonth3Line10.Height = 0.1875F;
			this.txtMonth3Line10.Left = 1.916667F;
			this.txtMonth3Line10.Name = "txtMonth3Line10";
			this.txtMonth3Line10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line10.Text = null;
			this.txtMonth3Line10.Top = 7.482639F;
			this.txtMonth3Line10.Width = 1.125F;
			// 
			// txtMonth3Line11
			// 
			this.txtMonth3Line11.Height = 0.1875F;
			this.txtMonth3Line11.Left = 1.916667F;
			this.txtMonth3Line11.Name = "txtMonth3Line11";
			this.txtMonth3Line11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line11.Text = null;
			this.txtMonth3Line11.Top = 7.71875F;
			this.txtMonth3Line11.Width = 1.125F;
			// 
			// txtMonth3Line12
			// 
			this.txtMonth3Line12.Height = 0.1875F;
			this.txtMonth3Line12.Left = 1.916667F;
			this.txtMonth3Line12.Name = "txtMonth3Line12";
			this.txtMonth3Line12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line12.Text = null;
			this.txtMonth3Line12.Top = 7.954861F;
			this.txtMonth3Line12.Width = 1.125F;
			// 
			// txtMonth3Line13
			// 
			this.txtMonth3Line13.Height = 0.1875F;
			this.txtMonth3Line13.Left = 1.916667F;
			this.txtMonth3Line13.Name = "txtMonth3Line13";
			this.txtMonth3Line13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line13.Text = null;
			this.txtMonth3Line13.Top = 8.190972F;
			this.txtMonth3Line13.Width = 1.125F;
			// 
			// txtMonth3Line14
			// 
			this.txtMonth3Line14.Height = 0.1875F;
			this.txtMonth3Line14.Left = 1.916667F;
			this.txtMonth3Line14.Name = "txtMonth3Line14";
			this.txtMonth3Line14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line14.Text = null;
			this.txtMonth3Line14.Top = 8.427083F;
			this.txtMonth3Line14.Width = 1.125F;
			// 
			// txtMonth3Line15
			// 
			this.txtMonth3Line15.Height = 0.1875F;
			this.txtMonth3Line15.Left = 1.916667F;
			this.txtMonth3Line15.Name = "txtMonth3Line15";
			this.txtMonth3Line15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line15.Text = null;
			this.txtMonth3Line15.Top = 8.663195F;
			this.txtMonth3Line15.Width = 1.125F;
			// 
			// txtMonth3Line16
			// 
			this.txtMonth3Line16.Height = 0.1875F;
			this.txtMonth3Line16.Left = 1.916667F;
			this.txtMonth3Line16.Name = "txtMonth3Line16";
			this.txtMonth3Line16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line16.Text = null;
			this.txtMonth3Line16.Top = 8.899305F;
			this.txtMonth3Line16.Width = 1.125F;
			// 
			// txtMonth3Line17
			// 
			this.txtMonth3Line17.Height = 0.1875F;
			this.txtMonth3Line17.Left = 3.291667F;
			this.txtMonth3Line17.Name = "txtMonth3Line17";
			this.txtMonth3Line17.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line17.Text = null;
			this.txtMonth3Line17.Top = 7.253472F;
			this.txtMonth3Line17.Width = 1.125F;
			// 
			// txtMonth3Line18
			// 
			this.txtMonth3Line18.Height = 0.1875F;
			this.txtMonth3Line18.Left = 3.291667F;
			this.txtMonth3Line18.Name = "txtMonth3Line18";
			this.txtMonth3Line18.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line18.Text = null;
			this.txtMonth3Line18.Top = 7.482639F;
			this.txtMonth3Line18.Width = 1.125F;
			// 
			// txtMonth3Line19
			// 
			this.txtMonth3Line19.Height = 0.1875F;
			this.txtMonth3Line19.Left = 3.291667F;
			this.txtMonth3Line19.Name = "txtMonth3Line19";
			this.txtMonth3Line19.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line19.Text = null;
			this.txtMonth3Line19.Top = 7.71875F;
			this.txtMonth3Line19.Width = 1.125F;
			// 
			// txtMonth3Line20
			// 
			this.txtMonth3Line20.Height = 0.1875F;
			this.txtMonth3Line20.Left = 3.291667F;
			this.txtMonth3Line20.Name = "txtMonth3Line20";
			this.txtMonth3Line20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line20.Text = null;
			this.txtMonth3Line20.Top = 7.954861F;
			this.txtMonth3Line20.Width = 1.125F;
			// 
			// txtMonth3Line21
			// 
			this.txtMonth3Line21.Height = 0.1875F;
			this.txtMonth3Line21.Left = 3.291667F;
			this.txtMonth3Line21.Name = "txtMonth3Line21";
			this.txtMonth3Line21.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line21.Text = null;
			this.txtMonth3Line21.Top = 8.190972F;
			this.txtMonth3Line21.Width = 1.125F;
			// 
			// txtMonth3Line22
			// 
			this.txtMonth3Line22.Height = 0.1875F;
			this.txtMonth3Line22.Left = 3.291667F;
			this.txtMonth3Line22.Name = "txtMonth3Line22";
			this.txtMonth3Line22.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line22.Text = null;
			this.txtMonth3Line22.Top = 8.427083F;
			this.txtMonth3Line22.Width = 1.125F;
			// 
			// txtMonth3Line23
			// 
			this.txtMonth3Line23.Height = 0.1875F;
			this.txtMonth3Line23.Left = 3.291667F;
			this.txtMonth3Line23.Name = "txtMonth3Line23";
			this.txtMonth3Line23.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line23.Text = null;
			this.txtMonth3Line23.Top = 8.663195F;
			this.txtMonth3Line23.Width = 1.125F;
			// 
			// txtMonth3Line24
			// 
			this.txtMonth3Line24.Height = 0.1875F;
			this.txtMonth3Line24.Left = 3.291667F;
			this.txtMonth3Line24.Name = "txtMonth3Line24";
			this.txtMonth3Line24.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line24.Text = null;
			this.txtMonth3Line24.Top = 8.899305F;
			this.txtMonth3Line24.Width = 1.125F;
			// 
			// txtMonth3Line25
			// 
			this.txtMonth3Line25.Height = 0.1875F;
			this.txtMonth3Line25.Left = 4.6875F;
			this.txtMonth3Line25.Name = "txtMonth3Line25";
			this.txtMonth3Line25.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line25.Text = null;
			this.txtMonth3Line25.Top = 7.253472F;
			this.txtMonth3Line25.Width = 1.125F;
			// 
			// txtMonth3Line26
			// 
			this.txtMonth3Line26.Height = 0.1875F;
			this.txtMonth3Line26.Left = 4.6875F;
			this.txtMonth3Line26.Name = "txtMonth3Line26";
			this.txtMonth3Line26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line26.Text = null;
			this.txtMonth3Line26.Top = 7.482639F;
			this.txtMonth3Line26.Width = 1.125F;
			// 
			// txtMonth3Line27
			// 
			this.txtMonth3Line27.Height = 0.1875F;
			this.txtMonth3Line27.Left = 4.6875F;
			this.txtMonth3Line27.Name = "txtMonth3Line27";
			this.txtMonth3Line27.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line27.Text = null;
			this.txtMonth3Line27.Top = 7.71875F;
			this.txtMonth3Line27.Width = 1.125F;
			// 
			// txtMonth3Line28
			// 
			this.txtMonth3Line28.Height = 0.1875F;
			this.txtMonth3Line28.Left = 4.6875F;
			this.txtMonth3Line28.Name = "txtMonth3Line28";
			this.txtMonth3Line28.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line28.Text = null;
			this.txtMonth3Line28.Top = 7.954861F;
			this.txtMonth3Line28.Width = 1.125F;
			// 
			// txtMonth3Line29
			// 
			this.txtMonth3Line29.Height = 0.1875F;
			this.txtMonth3Line29.Left = 4.6875F;
			this.txtMonth3Line29.Name = "txtMonth3Line29";
			this.txtMonth3Line29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line29.Text = null;
			this.txtMonth3Line29.Top = 8.190972F;
			this.txtMonth3Line29.Width = 1.125F;
			// 
			// txtMonth3Line30
			// 
			this.txtMonth3Line30.Height = 0.1875F;
			this.txtMonth3Line30.Left = 4.6875F;
			this.txtMonth3Line30.Name = "txtMonth3Line30";
			this.txtMonth3Line30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line30.Text = null;
			this.txtMonth3Line30.Top = 8.427083F;
			this.txtMonth3Line30.Width = 1.125F;
			// 
			// txtMonth3Line31
			// 
			this.txtMonth3Line31.Height = 0.1875F;
			this.txtMonth3Line31.Left = 4.6875F;
			this.txtMonth3Line31.Name = "txtMonth3Line31";
			this.txtMonth3Line31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Line31.Text = null;
			this.txtMonth3Line31.Top = 8.663195F;
			this.txtMonth3Line31.Width = 1.125F;
			// 
			// txtMonth3Total
			// 
			this.txtMonth3Total.Height = 0.2708333F;
			this.txtMonth3Total.Left = 6.125F;
			this.txtMonth3Total.Name = "txtMonth3Total";
			this.txtMonth3Total.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth3Total.Text = null;
			this.txtMonth3Total.Top = 7.416667F;
			this.txtMonth3Total.Width = 1.541667F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.2708333F;
			this.txtTotal.Left = 6.125F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 9.270833F;
			this.txtTotal.Width = 1.541667F;
			// 
			// txtMonth2Total
			// 
			this.txtMonth2Total.Height = 0.2708333F;
			this.txtMonth2Total.Left = 6.125F;
			this.txtMonth2Total.Name = "txtMonth2Total";
			this.txtMonth2Total.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth2Total.Text = null;
			this.txtMonth2Total.Top = 5.333333F;
			this.txtMonth2Total.Width = 1.541667F;
			// 
			// txtMonth1Total
			// 
			this.txtMonth1Total.Height = 0.2708333F;
			this.txtMonth1Total.Left = 6.125F;
			this.txtMonth1Total.Name = "txtMonth1Total";
			this.txtMonth1Total.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtMonth1Total.Text = null;
			this.txtMonth1Total.Top = 3.270833F;
			this.txtMonth1Total.Width = 1.541667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.21875F;
			this.txtName.Left = 1.604167F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txtName.Text = null;
			this.txtName.Top = 1.302083F;
			this.txtName.Width = 3.395833F;
			// 
			// txtEINBox1
			// 
			this.txtEINBox1.CanGrow = false;
			this.txtEINBox1.Height = 0.1666667F;
			this.txtEINBox1.Left = 1.861111F;
			this.txtEINBox1.Name = "txtEINBox1";
			this.txtEINBox1.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox1.Text = null;
			this.txtEINBox1.Top = 0.9722222F;
			this.txtEINBox1.Width = 0.1666667F;
			// 
			// txtEINBox2
			// 
			this.txtEINBox2.CanGrow = false;
			this.txtEINBox2.Height = 0.1666667F;
			this.txtEINBox2.Left = 2.208333F;
			this.txtEINBox2.Name = "txtEINBox2";
			this.txtEINBox2.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox2.Text = null;
			this.txtEINBox2.Top = 0.9722222F;
			this.txtEINBox2.Width = 0.1666667F;
			// 
			// txtEINBox3
			// 
			this.txtEINBox3.CanGrow = false;
			this.txtEINBox3.Height = 0.1666667F;
			this.txtEINBox3.Left = 2.736111F;
			this.txtEINBox3.Name = "txtEINBox3";
			this.txtEINBox3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox3.Text = null;
			this.txtEINBox3.Top = 0.9722222F;
			this.txtEINBox3.Width = 0.1666667F;
			// 
			// txtEINBox4
			// 
			this.txtEINBox4.CanGrow = false;
			this.txtEINBox4.Height = 0.1666667F;
			this.txtEINBox4.Left = 3.09375F;
			this.txtEINBox4.Name = "txtEINBox4";
			this.txtEINBox4.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox4.Text = null;
			this.txtEINBox4.Top = 0.9722222F;
			this.txtEINBox4.Width = 0.1666667F;
			// 
			// txtEINBox5
			// 
			this.txtEINBox5.CanGrow = false;
			this.txtEINBox5.Height = 0.1666667F;
			this.txtEINBox5.Left = 3.4375F;
			this.txtEINBox5.Name = "txtEINBox5";
			this.txtEINBox5.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox5.Text = null;
			this.txtEINBox5.Top = 0.9722222F;
			this.txtEINBox5.Width = 0.1666667F;
			// 
			// txtEINBox6
			// 
			this.txtEINBox6.CanGrow = false;
			this.txtEINBox6.Height = 0.1666667F;
			this.txtEINBox6.Left = 3.788194F;
			this.txtEINBox6.Name = "txtEINBox6";
			this.txtEINBox6.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox6.Text = null;
			this.txtEINBox6.Top = 0.9722222F;
			this.txtEINBox6.Width = 0.1666667F;
			// 
			// txtEINBox7
			// 
			this.txtEINBox7.CanGrow = false;
			this.txtEINBox7.Height = 0.1666667F;
			this.txtEINBox7.Left = 4.138889F;
			this.txtEINBox7.Name = "txtEINBox7";
			this.txtEINBox7.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox7.Text = null;
			this.txtEINBox7.Top = 0.9722222F;
			this.txtEINBox7.Width = 0.1666667F;
			// 
			// txtEINBox8
			// 
			this.txtEINBox8.CanGrow = false;
			this.txtEINBox8.Height = 0.1666667F;
			this.txtEINBox8.Left = 4.493055F;
			this.txtEINBox8.Name = "txtEINBox8";
			this.txtEINBox8.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox8.Text = null;
			this.txtEINBox8.Top = 0.9722222F;
			this.txtEINBox8.Width = 0.1666667F;
			// 
			// txtEINBox9
			// 
			this.txtEINBox9.CanGrow = false;
			this.txtEINBox9.Height = 0.1666667F;
			this.txtEINBox9.Left = 4.829861F;
			this.txtEINBox9.Name = "txtEINBox9";
			this.txtEINBox9.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEINBox9.Text = null;
			this.txtEINBox9.Top = 0.9722222F;
			this.txtEINBox9.Width = 0.1666667F;
			// 
			// txtQuarter1
			// 
			this.txtQuarter1.Height = 0.1666667F;
			this.txtQuarter1.Left = 5.583333F;
			this.txtQuarter1.Name = "txtQuarter1";
			this.txtQuarter1.Style = "font-size: 10pt; text-align: center";
			this.txtQuarter1.Text = null;
			this.txtQuarter1.Top = 1.201389F;
			this.txtQuarter1.Width = 0.1666667F;
			// 
			// txtQuarter2
			// 
			this.txtQuarter2.Height = 0.1666667F;
			this.txtQuarter2.Left = 5.583333F;
			this.txtQuarter2.Name = "txtQuarter2";
			this.txtQuarter2.Style = "font-size: 10pt; text-align: center";
			this.txtQuarter2.Text = null;
			this.txtQuarter2.Top = 1.434028F;
			this.txtQuarter2.Width = 0.1666667F;
			// 
			// txtQuarter3
			// 
			this.txtQuarter3.Height = 0.1666667F;
			this.txtQuarter3.Left = 5.583333F;
			this.txtQuarter3.Name = "txtQuarter3";
			this.txtQuarter3.Style = "font-size: 10pt; text-align: center";
			this.txtQuarter3.Text = null;
			this.txtQuarter3.Top = 1.666667F;
			this.txtQuarter3.Width = 0.1666667F;
			// 
			// txtQuarter4
			// 
			this.txtQuarter4.Height = 0.1666667F;
			this.txtQuarter4.Left = 5.583333F;
			this.txtQuarter4.Name = "txtQuarter4";
			this.txtQuarter4.Style = "font-size: 10pt; text-align: center";
			this.txtQuarter4.Text = null;
			this.txtQuarter4.Top = 1.90625F;
			this.txtQuarter4.Width = 0.1666667F;
			// 
			// txtYearMillenium
			// 
			this.txtYearMillenium.CanGrow = false;
			this.txtYearMillenium.Height = 0.1666667F;
			this.txtYearMillenium.Left = 1.861111F;
			this.txtYearMillenium.Name = "txtYearMillenium";
			this.txtYearMillenium.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtYearMillenium.Text = null;
			this.txtYearMillenium.Top = 1.635417F;
			this.txtYearMillenium.Width = 0.1666667F;
			// 
			// txtYearCentury
			// 
			this.txtYearCentury.CanGrow = false;
			this.txtYearCentury.Height = 0.1666667F;
			this.txtYearCentury.Left = 2.208333F;
			this.txtYearCentury.Name = "txtYearCentury";
			this.txtYearCentury.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtYearCentury.Text = null;
			this.txtYearCentury.Top = 1.635417F;
			this.txtYearCentury.Width = 0.1666667F;
			// 
			// txtYearDecade
			// 
			this.txtYearDecade.CanGrow = false;
			this.txtYearDecade.Height = 0.1666667F;
			this.txtYearDecade.Left = 2.5625F;
			this.txtYearDecade.Name = "txtYearDecade";
			this.txtYearDecade.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtYearDecade.Text = null;
			this.txtYearDecade.Top = 1.635417F;
			this.txtYearDecade.Width = 0.1666667F;
			// 
			// txtYearYear
			// 
			this.txtYearYear.CanGrow = false;
			this.txtYearYear.Height = 0.1666667F;
			this.txtYearYear.Left = 2.90625F;
			this.txtYearYear.Name = "txtYearYear";
			this.txtYearYear.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtYearYear.Text = null;
			this.txtYearYear.Top = 1.635417F;
			this.txtYearYear.Width = 0.1666667F;
			// 
			// srpt941ScheduleB
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.0625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Line31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Line31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Line31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEINBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarter4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearMillenium)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearCentury)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearDecade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		public GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Line31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Line31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Line31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Total;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearMillenium;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearCentury;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearDecade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearYear;
	}
}
