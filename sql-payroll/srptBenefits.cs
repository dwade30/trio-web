﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptBenefits.
	/// </summary>
	public partial class srptBenefits : FCSectionReport
	{
		public srptBenefits()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "sub Employee Deduction Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptBenefits InstancePtr
		{
			get
			{
				return (srptBenefits)Sys.GetInstance(typeof(srptBenefits));
			}
		}

		protected srptBenefits _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBenefits	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsData = new clsDRWrapper();
		

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			txtDedCode.Text = rsData.Get_Fields_String("Description");
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsData.OpenRecordset("SELECT * from tblSimpleEmployeeReport order by Description", "TWPY0000.vb1");
		}

		
	}
}
