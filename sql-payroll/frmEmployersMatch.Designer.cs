//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployersMatch.
	/// </summary>
	partial class frmEmployersMatch
	{
		public fecherFoundation.FCFrame Frame1;
		public FCGrid vsMatch;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCGrid vsTotals;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdPrint;
        public fecherFoundation.FCButton cmdTemporaryOverrides;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuDedSetup;
		public fecherFoundation.FCToolStripMenuItem mnuTaxStatusCodes;
		public fecherFoundation.FCToolStripMenuItem mnuTemporaryOverrides;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuMultPays;
		public fecherFoundation.FCToolStripMenuItem mnuMultAdd;
		public fecherFoundation.FCToolStripMenuItem mnuCaption1;
		public fecherFoundation.FCToolStripMenuItem mnuCaption2;
		public fecherFoundation.FCToolStripMenuItem mnuCaption3;
		public fecherFoundation.FCToolStripMenuItem mnuCaption4;
		public fecherFoundation.FCToolStripMenuItem mnuCaption5;
		public fecherFoundation.FCToolStripMenuItem mnuCaption6;
		public fecherFoundation.FCToolStripMenuItem mnuCaption7;
		public fecherFoundation.FCToolStripMenuItem mnuCaption8;
		public fecherFoundation.FCToolStripMenuItem mnuCaption9;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsMatch = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.vsTotals = new fecherFoundation.FCGrid();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdTemporaryOverrides = new fecherFoundation.FCButton();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.mnuDedSetup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTaxStatusCodes = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTemporaryOverrides = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMultPays = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMultAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption7 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption8 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption9 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSelectEmployee = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTemporaryOverrides)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(696, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Size = new System.Drawing.Size(696, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdRefresh);
            this.TopPanel.Controls.Add(this.cmdSelectEmployee);
            this.TopPanel.Controls.Add(this.cmdTemporaryOverrides);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(696, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdTemporaryOverrides, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelectEmployee, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRefresh, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(207, 30);
            this.HeaderText.Text = "Employer\'s Match";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsMatch);
            this.Frame1.Location = new System.Drawing.Point(30, 59);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(570, 302);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "Deductions";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // vsMatch
            // 
            this.vsMatch.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsMatch.Cols = 5;
            this.vsMatch.Location = new System.Drawing.Point(0, 30);
            this.vsMatch.Name = "vsMatch";
            this.vsMatch.Rows = 5;
            this.vsMatch.Size = new System.Drawing.Size(570, 272);
            this.vsMatch.StandardTab = false;
            this.vsMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsMatch.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.vsMatch, null);
            this.vsMatch.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsMatch_KeyDownEdit);
            this.vsMatch.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsMatch_KeyPressEdit);
            this.vsMatch.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.vsMatch_KeyUpEdit);
            this.vsMatch.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsMatch_AfterEdit);
            this.vsMatch.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsMatch_ChangeEdit);
            this.vsMatch.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsMatch_ValidateEdit);
            this.vsMatch.CurrentCellChanged += new System.EventHandler(this.vsMatch_RowColChange);
            this.vsMatch.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsMatch_MouseMoveEvent);
            this.vsMatch.Enter += new System.EventHandler(this.vsMatch_Enter);
            this.vsMatch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsMatch_KeyDownEvent);
            this.vsMatch.KeyUp += new Wisej.Web.KeyEventHandler(this.vsMatch_KeyUpEvent);
            // 
            // Frame2
            // 
            this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.vsTotals);
            this.Frame2.Location = new System.Drawing.Point(30, 381);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(570, 302);
            this.Frame2.TabIndex = 3;
            this.Frame2.Text = "Totals";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // vsTotals
            // 
            this.vsTotals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTotals.Cols = 14;
            this.vsTotals.Location = new System.Drawing.Point(0, 30);
            this.vsTotals.Name = "vsTotals";
            this.vsTotals.Rows = 5;
            this.vsTotals.Size = new System.Drawing.Size(570, 272);
            this.vsTotals.StandardTab = false;
            this.vsTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsTotals.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.vsTotals, null);
            this.vsTotals.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsTotals_KeyPressEdit);
            this.vsTotals.CurrentCellChanged += new System.EventHandler(this.vsTotals_BeforeRowColChange);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(100, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(112, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New Deduction";
            this.ToolTip1.SetToolTip(this.cmdNew, null);
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRefresh.Location = new System.Drawing.Point(174, 29);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 24);
            this.cmdRefresh.TabIndex = 6;
            this.cmdRefresh.Text = "Refresh";
            this.ToolTip1.SetToolTip(this.cmdRefresh, null);
            this.cmdRefresh.Visible = false;
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(218, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(128, 24);
            this.cmdDelete.TabIndex = 7;
            this.cmdDelete.Text = "Delete Deduction";
            this.ToolTip1.SetToolTip(this.cmdDelete, null);
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(309, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 8;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Enabled = false;
            this.cmdPrint.Location = new System.Drawing.Point(626, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(48, 24);
            this.cmdPrint.TabIndex = 9;
            this.cmdPrint.Text = "Print";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdTemporaryOverrides
            // 
            this.cmdTemporaryOverrides.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdTemporaryOverrides.Location = new System.Drawing.Point(480, 29);
            this.cmdTemporaryOverrides.Name = "cmdTemporaryOverrides";
            this.cmdTemporaryOverrides.Size = new System.Drawing.Size(140, 24);
            this.cmdTemporaryOverrides.TabIndex = 11;
            this.cmdTemporaryOverrides.Text = "Temporary Overrides";
            this.ToolTip1.SetToolTip(this.cmdTemporaryOverrides, null);
            this.cmdTemporaryOverrides.Click += new System.EventHandler(this.mnuTemporaryOverrides_Click);
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(30, 30);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(638, 15);
            this.lblEmployeeNumber.TabIndex = 1;
            this.lblEmployeeNumber.Tag = "Employee #";
            this.lblEmployeeNumber.Text = "EMPLOYEE #";
            this.ToolTip1.SetToolTip(this.lblEmployeeNumber, null);
            // 
            // mnuDedSetup
            // 
            this.mnuDedSetup.Index = 0;
            this.mnuDedSetup.Name = "mnuDedSetup";
            this.mnuDedSetup.Text = "Edit Deduction Setup";
            this.mnuDedSetup.Visible = false;
            this.mnuDedSetup.Click += new System.EventHandler(this.mnuDedSetup_Click);
            // 
            // mnuTaxStatusCodes
            // 
            this.mnuTaxStatusCodes.Index = 1;
            this.mnuTaxStatusCodes.Name = "mnuTaxStatusCodes";
            this.mnuTaxStatusCodes.Text = "Edit Tax Status Codes";
            this.mnuTaxStatusCodes.Visible = false;
            this.mnuTaxStatusCodes.Click += new System.EventHandler(this.mnuTaxStatusCodes_Click);
            // 
            // mnuTemporaryOverrides
            // 
            this.mnuTemporaryOverrides.Index = 2;
            this.mnuTemporaryOverrides.Name = "mnuTemporaryOverrides";
            this.mnuTemporaryOverrides.Text = "Temporary Overrides";
            this.mnuTemporaryOverrides.Visible = false;
            this.mnuTemporaryOverrides.Click += new System.EventHandler(this.mnuTemporaryOverrides_Click);
            // 
            // mnuMultPays
            // 
            this.mnuMultPays.Index = 3;
            this.mnuMultPays.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuMultAdd,
            this.mnuCaption1,
            this.mnuCaption2,
            this.mnuCaption3,
            this.mnuCaption4,
            this.mnuCaption5,
            this.mnuCaption6,
            this.mnuCaption7,
            this.mnuCaption8,
            this.mnuCaption9});
            this.mnuMultPays.Name = "mnuMultPays";
            this.mnuMultPays.Text = "Mult Pays";
            this.mnuMultPays.Visible = false;
            // 
            // mnuMultAdd
            // 
            this.mnuMultAdd.Enabled = false;
            this.mnuMultAdd.Index = 0;
            this.mnuMultAdd.Name = "mnuMultAdd";
            this.mnuMultAdd.Text = "Add/Edit Additional Pay Master";
            // 
            // mnuCaption1
            // 
            this.mnuCaption1.Index = 1;
            this.mnuCaption1.Name = "mnuCaption1";
            this.mnuCaption1.Text = "Caption1";
            this.mnuCaption1.Click += new System.EventHandler(this.mnuCaption1_Click);
            // 
            // mnuCaption2
            // 
            this.mnuCaption2.Index = 2;
            this.mnuCaption2.Name = "mnuCaption2";
            this.mnuCaption2.Text = "Caption2";
            this.mnuCaption2.Click += new System.EventHandler(this.mnuCaption2_Click);
            // 
            // mnuCaption3
            // 
            this.mnuCaption3.Index = 3;
            this.mnuCaption3.Name = "mnuCaption3";
            this.mnuCaption3.Text = "Caption3";
            this.mnuCaption3.Click += new System.EventHandler(this.mnuCaption3_Click);
            // 
            // mnuCaption4
            // 
            this.mnuCaption4.Index = 4;
            this.mnuCaption4.Name = "mnuCaption4";
            this.mnuCaption4.Text = "Caption4";
            this.mnuCaption4.Click += new System.EventHandler(this.mnuCaption4_Click);
            // 
            // mnuCaption5
            // 
            this.mnuCaption5.Index = 5;
            this.mnuCaption5.Name = "mnuCaption5";
            this.mnuCaption5.Text = "Caption5";
            this.mnuCaption5.Click += new System.EventHandler(this.mnuCaption5_Click);
            // 
            // mnuCaption6
            // 
            this.mnuCaption6.Index = 6;
            this.mnuCaption6.Name = "mnuCaption6";
            this.mnuCaption6.Text = "Caption6";
            this.mnuCaption6.Click += new System.EventHandler(this.mnuCaption6_Click);
            // 
            // mnuCaption7
            // 
            this.mnuCaption7.Index = 7;
            this.mnuCaption7.Name = "mnuCaption7";
            this.mnuCaption7.Text = "Caption7";
            this.mnuCaption7.Click += new System.EventHandler(this.mnuCaption7_Click);
            // 
            // mnuCaption8
            // 
            this.mnuCaption8.Index = 8;
            this.mnuCaption8.Name = "mnuCaption8";
            this.mnuCaption8.Text = "Caption8";
            this.mnuCaption8.Click += new System.EventHandler(this.mnuCaption8_Click);
            // 
            // mnuCaption9
            // 
            this.mnuCaption9.Index = 9;
            this.mnuCaption9.Name = "mnuCaption9";
            this.mnuCaption9.Text = "Caption9";
            this.mnuCaption9.Click += new System.EventHandler(this.mnuCaption9_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = -1;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New Deduction";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Deduction";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = -1;
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Visible = false;
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = -1;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP4
            // 
            this.mnuSP4.Index = -1;
            this.mnuSP4.Name = "mnuSP4";
            this.mnuSP4.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSelectEmployee
            // 
            this.cmdSelectEmployee.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelectEmployee.Location = new System.Drawing.Point(352, 29);
            this.cmdSelectEmployee.Name = "cmdSelectEmployee";
            this.cmdSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelectEmployee.Size = new System.Drawing.Size(122, 24);
            this.cmdSelectEmployee.TabIndex = 10;
            this.cmdSelectEmployee.Text = "Select Employee";
            this.cmdSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // frmEmployersMatch
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(696, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployersMatch";
            this.Text = "Employer\'s Match";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployersMatch_Load);
            this.Activated += new System.EventHandler(this.frmEmployersMatch_Activated);
            this.Resize += new System.EventHandler(this.frmEmployersMatch_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmployersMatch_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployersMatch_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTemporaryOverrides)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSelectEmployee;
	}
}