//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cPayrollBankController
	{
		//=========================================================
		public cPayrollBank GetBank(int lngID)
		{
			cPayrollBank GetBank = null;
			cPayrollBank tBank = new cPayrollBank();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tblBanks where ID = " + FCConvert.ToString(lngID), "payroll");
			if (!rsLoad.EndOfFile())
			{
				tBank.ACH = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ACHBank"));
				tBank.Address1 = FCConvert.ToString(rsLoad.Get_Fields_String("Address1"));
				tBank.Address2 = FCConvert.ToString(rsLoad.Get_Fields_String("Address2"));
				tBank.Address3 = FCConvert.ToString(rsLoad.Get_Fields("Address3"));
				tBank.BankDepositID = FCConvert.ToString(rsLoad.Get_Fields_String("BankDepositID"));
				tBank.CheckingAccount = FCConvert.ToString(rsLoad.Get_Fields_String("CheckingAccount"));
				tBank.City = FCConvert.ToString(rsLoad.Get_Fields_String("City"));
				tBank.EFT = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("EFT"));
				tBank.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				if (Information.IsDate(rsLoad.Get_Fields("Lastupdate")))
				{
					tBank.LastUpdate = (DateTime)rsLoad.Get_Fields_DateTime("LastUpdate");
				}
				tBank.LastUserID = FCConvert.ToString(rsLoad.Get_Fields("LastUserID"));
				tBank.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
				tBank.RecordNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("RecordNumber"));
				tBank.ShortDescription = FCConvert.ToString(rsLoad.Get_Fields("ShortDescription"));
				tBank.State = FCConvert.ToString(rsLoad.Get_Fields("State"));
				tBank.Zip = FCConvert.ToString(rsLoad.Get_Fields("Zip"));
			}
			GetBank = tBank;
			return GetBank;
		}
	}
}
