//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeVacationSick.
	/// </summary>
	public partial class rptEmployeeVacationSick : BaseSectionReport
	{
		public rptEmployeeVacationSick()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Vacation Sick Etc..";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptEmployeeVacationSick InstancePtr
		{
			get
			{
				return (rptEmployeeVacationSick)Sys.GetInstance(typeof(rptEmployeeVacationSick));
			}
		}

		protected rptEmployeeVacationSick _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsBal?.Dispose();
                rsData = null;
                rsBal = null;
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEmployeeVacationSick	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		//private clsDRWrapper rsTemp = new clsDRWrapper();
		private clsDRWrapper rsBal = new clsDRWrapper();
		private string strTemp = "";
		private int intEmployee;
		private double dblAccruedCurrent;
		private double dblAccruedFiscal;
		private double dblAccruedCalendar;
		private double dblUsedCurrent;
		private double dblUsedFiscal;
		private double dblUsedCalendar;
		private double dblBalance;
		private bool boolGroups;
		private string strDeptDiv = "";
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
        private clsDRWrapper rsMissingBal = new clsDRWrapper();
        private DateTime dtMaxDate;
        private DateTime dtStartDate;
        private int intVacSickToUse = 0;
        private bool boolIsYTD = false;
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			while (!rsData.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(rsData.Get_Fields("tblemployeemasteremployeenumber")))
				{
					rsData.MoveNext();
				}
				else
				{
                    if (!rsBal.FindFirstRecord("employeenumber",
                        rsData.Get_Fields_String("tblemployeemasteremployeenumber")))
                    {
                        rsMissingBal.OpenRecordset(
                            "select top 1 employeenumber, vsbalance as CurrBal,vsused,vsaccrued,vstypeid from tblcheckdetail where employeenumber = '" +
                            rsData.Get_Fields_String("tblemployeemasteremployeenumber") +
                            "' and vsrecord = 1 and checkvoid = 0 and vstypeid = " + intVacSickToUse.ToString() +
                            " and paydate between '" + dtStartDate.ToShortDateString() + "' and '" +
                            dtMaxDate.ToShortDateString() + "' order by paydate desc, payrunid desc ", "Payroll");
                        if (rsMissingBal.EndOfFile())
                        {
                            if (!boolIsYTD)
                            {
                                rsData.MoveNext();
                            }
                            else
                            {
                                rsMissingBal.OpenRecordset(
                                    "select employeenumber,accruedcalendar as vsaccrued, usedcalendar as vsused, usedbalance as CurrBal, typeid as vstypeid from tblvacationsick where employeenumber = '" +
                                    rsData.Get_Fields_String("tblemployeemasteremployeenumber") + "' and typeid = " +
                                    intVacSickToUse + " and selected = 1", "Payroll");
                                if (rsMissingBal.EndOfFile())
                                {
                                    rsData.MoveNext();
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }

                    }
                    else
                    {
                        break;
                    }
				}
			}
			eArgs.EOF = rsData.EndOfFile();
			if (!eArgs.EOF && boolGroups)
			{
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
				strDeptDiv = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// & "  " & Format(Time, "hh:mm tt")
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);

			modPrintToFile.SetPrintProperties(this);
		}

		public void Init(int intMQYCode, int intVacSickCode, DateTime dtEndDate, int intPayrun = 1, bool boolSeparatePages = false)
		{
			// 0 is weekly
			// 1 is monthly
			// 2 is quarterly
			// 3 is yearly
			// 4 is individual
			string strWhere = "";
			DateTime dtPayDate = DateTime.FromOADate(0);
			//DateTime dtStartDate = DateTime.FromOADate(0);
			string strCurrentWhere = "";
			string strFiscalWhere = "";
			string strCalendarWhere = "";
			string strSQLBalance;
			string strSQL;
			string strSQLCodes = "";
			string strSQLCurrent;
			string strSQLFiscal = "";
			string strSQLCalendar = "";
			string strOrderBy = "";
			DateTime dtReturndate = DateTime.FromOADate(0);

            dtMaxDate = dtEndDate;
            intVacSickToUse = intVacSickCode;

            rsData.OpenRecordset("select * from tblcodetypes where ID = " + FCConvert.ToString(intVacSickCode), "twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				txtTitle.Text = "Employee " + rsData.Get_Fields_String("description") + " Report";
			}
			else
			{
				txtTitle.Text = "Employee Unknown Code Type Report";
			}

            boolIsYTD = false;
			switch (intMQYCode)
			{
				case 0:
					{
						// weekly
						txtSecondTitle.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						strCurrentWhere = " Where paydate = '" + FCConvert.ToString(dtEndDate) + "' and payrunid = " + FCConvert.ToString(intPayrun) + " ";
						//dtStartDate = dtEndDate;
						//dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtEndDate);
						break;
					}
				case 1:
					{
						txtSecondTitle.Text = "MTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						strCurrentWhere = " where paydate between '" + FCConvert.ToString(dtEndDate.Month) + "/1/" + FCConvert.ToString(dtEndDate.Year) + "' and '" + FCConvert.ToString(dtEndDate) + "' ";
						//dtStartDate = dtEndDate;
						//dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtEndDate);
						break;
					}
				case 2:
					{
						txtSecondTitle.Text = "QTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						DateTime dtTempDate;
						dtTempDate = dtEndDate;
						modGlobalRoutines.GetDatesForAQuarter(ref dtStartDate, ref dtEndDate, dtTempDate);
						strCurrentWhere = " where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' ";
						//dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtEndDate);
						break;
					}
				case 3:
					{
						txtSecondTitle.Text = "YTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						strCurrentWhere = " where paydate between '" + "1/1/" + FCConvert.ToString(dtEndDate.Year) + "' and '" + FCConvert.ToString(dtEndDate) + "' ";
                        //dtStartDate = dtEndDate;
                        //dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtEndDate);
                        boolIsYTD = true;
                        break;
					}
			}
			dtStartDate = new DateTime(dtEndDate.Year,1,1);
			strSQLCurrent = "(select sum(vsused) as CurrUsed,sum(vsaccrued) as CurrAccrued,sum(vsbalance) as CurrBal,vstypeid,employeenumber from tblcheckdetail " + strCurrentWhere + " and VSRecord = 1 and checkvoid = 0 and vstypeid = " + FCConvert.ToString(intVacSickCode) + " group by employeenumber,vstypeid having (sum(vsused) <> 0 or sum(vsaccrued) <> 0 or sum(vsbalance) <> 0)) as CurVSQuery ";
			strSQLBalance = "select  vsbalance as CurrBal,vsused,vsaccrued,vstypeid,employeenumber from tblcheckdetail " + strCurrentWhere + " and VSRecord = 1 and checkvoid = 0 and vstypeid = " + FCConvert.ToString(intVacSickCode) + " order by employeenumber ,paydate desc ";
			rsBal.OpenRecordset(strSQLBalance, "twpy0000.vb1");
			// now open the recordset
			rsData.OpenRecordset("select reportsequence from tbldefaultinformation", "twpy0000.vb1");
			if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 0)
			{
				// name
				strOrderBy = " order by lastname,firstname,tblemployeemaster.employeenumber";
				this.GroupHeader1.Visible = false;
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 1)
			{
				// employee number
				strOrderBy = " order by tblemployeemaster.employeenumber ";
				this.GroupHeader1.Visible = false;
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 2)
			{
				// sequence
				strOrderBy = " order by seqnumber,lastname,firstname,tblemployeemaster.employeenumber ";
				this.GroupHeader1.Visible = false;
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 3)
			{
				// dept div
				boolGroups = true;
				strOrderBy = " order by deptdiv,lastname,firstname,tblemployeemaster.employeenumber";
				// hide the column headers since these are in the group header also
				// Me.PageHeader.Height = 825
				this.GroupHeader1.Visible = true;
				if (boolSeparatePages)
				{
					this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
				}
				else
				{
					this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
			}
			//strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strSQLCurrent + " on (tblemployeemaster.employeenumber = curvsquery.employeenumber) " + strOrderBy;
            strSQL =
                "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster left join " +
                strSQLCurrent + " on (tblemployeemaster.employeenumber = curvsquery.employeenumber) " + strOrderBy;

            rsData.OpenRecordset(strSQL, "twpy0000.vb1");
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No data found.");
				this.Close();
				return;
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName);
				// Me.PrintReport (False)
			}
			else
			{
				// frmReportViewer.Init Me, , 1
				modCoreysSweeterCode.CheckDefaultPrint(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "EmployeeVacationSick");
				// Me.Show vbModal, MDIParent
			}
			return;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MiddleName"))) == "")
			{
				txtEmployee.Text = rsData.Get_Fields("tblemployeemasteremployeenumber") + "  " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName") + " " + rsData.Get_Fields_String("Desig");
			}
			else
			{
				txtEmployee.Text = rsData.Get_Fields("tblemployeemasteremployeenumber") + "  " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName") + " " + rsData.Get_Fields_String("Desig");
			}
			intEmployee += 1;
			txtAccruedCurrent.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("CurrAccrued")), "0.00");
			dblAccruedCurrent += Conversion.Val(rsData.Get_Fields("CurrAccrued"));
			txtUsedCurrent.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("CurrUsed")), "0.00");
			dblUsedCurrent += Conversion.Val(rsData.Get_Fields("CurrUsed"));
            txtBalance.Text = "0.00";
            double dblBal = 0;
            if (rsBal.FindFirstRecord("employeenumber", rsData.Get_Fields("tblemployeemasteremployeenumber")))
			{
				
				dblBal = Conversion.Val(rsBal.Get_Fields("CurrBal"));
				dblBal += Conversion.Val(rsBal.Get_Fields("vsaccrued")) - Conversion.Val(rsBal.Get_Fields("vsused"));
				txtBalance.Text = Strings.Format(dblBal, "0.00");
				dblBalance += dblBal;
			}
			else
			{
                if (!rsMissingBal.EndOfFile())
                {
                    if (rsMissingBal.Get_Fields_String("employeenumber") ==
                        rsData.Get_Fields_String("tblemployeemasteremployeenumber"))
                    {

                        dblBal = rsMissingBal.Get_Fields_Double("CurrBal");
                        dblBal = dblBal + rsBal.Get_Fields_Double("vsaccrued") -rsBal.Get_Fields_Double("vsused");
                        txtBalance.Text = dblBal.ToString("##.00");
                        dblBalance = dblBalance + dblBal;
                    }
                }
                else if (boolIsYTD)
                 {
                     
                     dblBal = rsBal.Get_Fields_Double("currbal");
                     txtBalance.Text = dblBal.ToString("##.00");
                     txtAccruedCurrent.Text = rsBal.Get_Fields_Double("vsAccrued").ToString("##.00");
                     dblAccruedCurrent = dblAccruedCurrent + rsBal.Get_Fields_Double("vsAccrued");
                     txtUsedCurrent.Text = rsBal.Get_Fields_Double("vsUsed").ToString("##.00");
                     dblUsedCurrent = dblUsedCurrent + rsBal.Get_Fields_Double("vsUsed");
                 }

            }
			rsData.MoveNext();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtDeptDiv.Text = FCConvert.ToString(this.Fields["grpHeader"].Value);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtEmployeeTotal As object	OnWrite
			// vbPorter upgrade warning: txtAccruedCurrentTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtUsedCurrentTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtBalanceTotal As object	OnWrite(string)
			txtEmployeeTotal.Text = intEmployee.ToString();
			txtAccruedCurrentTotal.Text = Strings.Format(dblAccruedCurrent, "0.00");
			txtUsedCurrentTotal.Text = Strings.Format(dblUsedCurrent, "0.00");
			txtBalanceTotal.Text = Strings.Format(dblBalance, "0.00");
		}

		

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
