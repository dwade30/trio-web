//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomDeductionReport.
	/// </summary>
	public partial class rptCustomDeductionReport : BaseSectionReport
	{
		public rptCustomDeductionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Deduction Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomDeductionReport InstancePtr
		{
			get
			{
				return (rptCustomDeductionReport)Sys.GetInstance(typeof(rptCustomDeductionReport));
			}
		}

		protected rptCustomDeductionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsDeductions?.Dispose();
                rsData = null;
                rsDeductions = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomDeductionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsDeductions = new clsDRWrapper();
		clsDRWrapper rsData = new clsDRWrapper();
		string strEmployeeNumber = "";
		double dblTotalAmount;
		string strDeductionNumber = "";
		bool boolDifferentEmployee;
		int intpage;
		string strCurrentValue = "";
		string strCurrentCompareValue = "";
		string strDisplayName = "";
		double dblGrandTotal;
		bool boolShowGrandTotal;
		bool boolShowReportTotal;
		double dblReportTotal;
		int intSpacer;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			NextRecord:
			;
			Detail.Visible = true;
			txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Regular);
			txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Regular);
			txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Regular);
			// If strDeductionNumber = "5007" Then MsgBox "A"
			while (!rsData.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				if (modGlobalVariables.Statics.gboolSummaryDedCustomReport)
				{
					if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name" || modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
					{
						txtField1.Text = GetDeductionDescription(FCConvert.ToInt32(Conversion.Val(strDeductionNumber)));
						txtField2.Text = Strings.Format(dblTotalAmount, "#,##0.00");
						txtField3.Text = "";
					}
					else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
					{
						// Or gstrDeductionSummaryType = "PayDate" Then
						txtField1.Text = strDisplayName;
						txtField2.Text = Strings.Format(dblTotalAmount, "#,##0.00");
						txtField3.Text = "";
					}
					txtField2.Text = Strings.Format(dblTotalAmount, "0.00");
					txtField3.Text = "";
					eArgs.EOF = false;
					modGlobalVariables.Statics.gboolSummaryDedCustomReport = false;
					return;
				}
				else
				{
					if (boolShowGrandTotal)
					{
						boolShowGrandTotal = false;
						txtField1.Text = "Total";
						txtField2.Text = Strings.Format(dblGrandTotal, "#,##0.00");
						txtField3.Text = "";
						txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Bold);
						txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
						txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Bold);
						dblGrandTotal = 0;
						eArgs.EOF = false;
						return;
					}
					else
					{
						if (boolShowReportTotal)
						{
							if (intSpacer < 3)
							{
								txtField1.Text = "";
								txtField2.Text = "";
								txtField3.Text = "";
								intSpacer += 1;
								eArgs.EOF = false;
								return;
							}
							else
							{
								boolShowReportTotal = false;
								txtField1.Text = "Report Total";
								txtField2.Text = Strings.Format(dblReportTotal, "#,##0.00");
								txtField3.Text = "";
								txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Bold);
								txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
								txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Bold);
								dblGrandTotal = 0;
								eArgs.EOF = false;
								return;
							}
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
			}
			if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
			}
			else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields_Int32("DedDeductionNumber"));
			}
			else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate"));
			}
			if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name")
			{
				strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields_Int32("DedDeductionNumber"));
			}
			else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
			{
				strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
			}
			else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
			{
				strCurrentCompareValue = FCConvert.ToString(rsData.Get_Fields_Int32("DedDeductionNumber"));
				// rsData.Fields("EmployeeNumber")
			}
			if (strEmployeeNumber != strCurrentValue)
			{
				if (modGlobalVariables.Statics.gboolSummaryDedCustomReport && !boolDifferentEmployee)
				{
					if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name" || modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
					{
						txtField1.Text = GetDeductionDescription(FCConvert.ToInt32(strDeductionNumber));
						txtField2.Text = Strings.Format(dblTotalAmount, "#,##0.00");
						txtField3.Text = "";
					}
					else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
					{
						// Or gstrDeductionSummaryType = "PayDate" Then
						txtField1.Text = strDisplayName;
						txtField2.Text = Strings.Format(dblTotalAmount, "#,##0.00");
						txtField3.Text = "";
					}
					dblTotalAmount = 0;
					boolDifferentEmployee = true;
					strDeductionNumber = strCurrentCompareValue;
					strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
					eArgs.EOF = false;
					return;
				}
				else
				{
					if (boolShowGrandTotal)
					{
						boolShowGrandTotal = false;
						txtField1.Text = "Total";
						txtField2.Text = Strings.Format(dblGrandTotal, "#,##0.00");
						txtField3.Text = "";
						txtField1.Font = new System.Drawing.Font(txtField1.Font, System.Drawing.FontStyle.Bold);
						txtField2.Font = new System.Drawing.Font(txtField2.Font, System.Drawing.FontStyle.Bold);
						txtField3.Font = new System.Drawing.Font(txtField3.Font, System.Drawing.FontStyle.Bold);
						dblGrandTotal = 0;
						eArgs.EOF = false;
						return;
					}
					else
					{
						boolShowGrandTotal = true;
						boolDifferentEmployee = false;
						if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
							this.Fields["grpHeader"].Value = rsData.Get_Fields("EmployeeNumber");
							this.txtGroup.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
						}
						else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields_Int32("DedDeductionNumber"));
							this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_Int32("DedDeductionNumber"));
							this.txtGroup.Text = GetDeductionDescription(FCConvert.ToInt32(Conversion.Val(strEmployeeNumber)));
						}
						else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
						{
							strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate"));
							this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate"));
							this.txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
						}
						Detail.Visible = false;
						eArgs.EOF = false;
						return;
					}
				}
			}
			else
			{
				if (modGlobalVariables.Statics.gboolSummaryDedCustomReport)
				{
					if (strDeductionNumber == strCurrentCompareValue)
					{
						dblTotalAmount += rsData.Get_Fields("DedAmount");
						dblGrandTotal += rsData.Get_Fields("DedAmount");
						dblReportTotal += rsData.Get_Fields("DedAmount");
						if (!rsData.EndOfFile())
							rsData.MoveNext();
						goto NextRecord;
					}
					else
					{
						if (strDeductionNumber == "")
						{
							// this is the first time
							strDeductionNumber = strCurrentCompareValue;
							strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
							// If Not rsData.EndOfFile Then rsData.MoveNext
							goto NextRecord;
						}
						else
						{
							if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name" || modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
							{
								txtField1.Text = GetDeductionDescription(FCConvert.ToInt32(strDeductionNumber));
								txtField2.Text = Strings.Format(dblTotalAmount, "#,##0.00");
								txtField3.Text = "";
							}
							else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
							{
								// Or gstrDeductionSummaryType = "PayDate" Then
								txtField1.Text = strDisplayName;
								txtField2.Text = Strings.Format(dblTotalAmount, "#,##0.00");
								txtField3.Text = "";
							}
							dblTotalAmount = 0;
							strDeductionNumber = strCurrentCompareValue;
							strDisplayName = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
							eArgs.EOF = false;
							return;
						}
					}
				}
			}
			if (modGlobalVariables.Statics.gstrDeductionSummaryType == "ID" || modGlobalVariables.Statics.gstrDeductionSummaryType == "Name")
			{
				if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AdjustRecord")))
				{
					txtField1.Text = GetDeductionDescription(rsData.Get_Fields_Int32("DedDeductionNumber"));
				}
				else
				{
					txtField1.Text = FCConvert.ToString(GetDeductionDescription(rsData.Get_Fields_Int32("DedDeductionNumber"))) + "   (Adjustment Record)";
				}
				txtField2.Text = Strings.Format(rsData.Get_Fields("DedAmount"), "#,##0.00");
				txtField3.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
			}
			else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "Deduction")
			{
				if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AdjustRecord")))
				{
					txtField1.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
				}
				else
				{
					txtField1.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName") + "   (Adjustment Record)";
				}
				txtField2.Text = Strings.Format(rsData.Get_Fields("DedAmount"), "#,##0.00");
				txtField3.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
			}
			else if (modGlobalVariables.Statics.gstrDeductionSummaryType == "PayDate")
			{
				if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AdjustRecord")))
				{
					txtField1.Text = FCConvert.ToString(GetDeductionDescription(rsData.Get_Fields_Int32("DedDeductionNumber"))) + " ----- " + rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
				}
				else
				{
					txtField1.Text = FCConvert.ToString(GetDeductionDescription(rsData.Get_Fields_Int32("DedDeductionNumber"))) + " ----- " + rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName") + "   (Adjustment Record)";
				}
				txtField2.Text = Strings.Format(rsData.Get_Fields("DedAmount"), "#,##0.00");
				txtField3.Text = FCConvert.ToString(rsData.Get_Fields("PayDate"));
			}
			dblGrandTotal += rsData.Get_Fields("DedAmount");
			dblReportTotal += rsData.Get_Fields("DedAmount");
			if (!rsData.EndOfFile())
				rsData.MoveNext();
			eArgs.EOF = false;
		}
		// vbPorter upgrade warning: lngDeductionNumber As int	OnWrite(double, string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private string GetDeductionDescription(int lngDeductionNumber)
		{
			string GetDeductionDescription = "";
			rsDeductions.FindFirstRecord("ID", lngDeductionNumber);
			if (rsDeductions.NoMatch)
			{
				GetDeductionDescription = string.Empty;
			}
			else
			{
				GetDeductionDescription = rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + rsDeductions.Get_Fields("Description");
			}
			return GetDeductionDescription;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolDifferentEmployee = true;
			boolShowReportTotal = true;
			intSpacer = 1;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsData.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, "TWPY0000.vb1");
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
			if (modGlobalVariables.Statics.gstrShowCYTDDeductionReport == string.Empty)
			{
				// gstrDeductionSummaryType = "ID" Or gstrDeductionSummaryType = "Name" Or gstrDeductionSummaryType = "Deduction" Then
				lblCaption2.Text = "Calendar Year-to-Date";
			}
			else
			{
				// ElseIf gstrDeductionSummaryType = "PayDate" Then
				lblCaption2.Text = "Pay Date " + modGlobalVariables.Statics.gstrShowCYTDDeductionReport;
			}
			// Me.Printer.Orientation = ddOPortrait
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		

		private void rptCustomDeductionReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
