//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPrint941.
	/// </summary>
	partial class frmPrint941
	{
		public fecherFoundation.FCComboBox cmbSchedule;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public System.Collections.Generic.List<T2KDateBox> txtStartDate;
		public System.Collections.Generic.List<T2KDateBox> txtEndDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblFrom;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtCobraIndividuals;
		public fecherFoundation.FCTextBox txtCobraPayments;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtSSAndMedicare;
		public fecherFoundation.FCTextBox txtIncomeTax;
		public fecherFoundation.FCTextBox txtTipsAndInsurance;
		public fecherFoundation.FCTextBox txtSickPay;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkForceScheduleB;
		public fecherFoundation.FCTextBox txtDeposits;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtTransmitterName;
		public fecherFoundation.FCTextBox txttitle;
		public fecherFoundation.FCTextBox txtExtension;
		public Global.T2KPhoneNumberBox txtPhone;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtFederalID;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCTextBox txtStreetAddress;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCFrame fraDateInfo;
		public Global.T2KDateBox txtStartDate_0;
		public Global.T2KDateBox txtEndDate_0;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCLabel lblMonth;
		public fecherFoundation.FCLabel lblFrom_0;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbSchedule = new fecherFoundation.FCComboBox();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.txtCobraIndividuals = new fecherFoundation.FCTextBox();
            this.txtCobraPayments = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtSSAndMedicare = new fecherFoundation.FCTextBox();
            this.txtIncomeTax = new fecherFoundation.FCTextBox();
            this.txtTipsAndInsurance = new fecherFoundation.FCTextBox();
            this.txtSickPay = new fecherFoundation.FCTextBox();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.chkForceScheduleB = new fecherFoundation.FCCheckBox();
            this.txtDeposits = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtTransmitterName = new fecherFoundation.FCTextBox();
            this.txttitle = new fecherFoundation.FCTextBox();
            this.txtExtension = new fecherFoundation.FCTextBox();
            this.txtPhone = new Global.T2KPhoneNumberBox();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtFederalID = new fecherFoundation.FCTextBox();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.txtStreetAddress = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.fraDateInfo = new fecherFoundation.FCFrame();
            this.txtStartDate_0 = new Global.T2KDateBox();
            this.txtEndDate_0 = new Global.T2KDateBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.lblMonth = new fecherFoundation.FCLabel();
            this.lblFrom_0 = new fecherFoundation.FCLabel();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.fra941AdditionalInfo = new fecherFoundation.FCFrame();
            this.txtQualWagesEmplRetentionCredit = new fecherFoundation.FCTextBox();
            this.txtCreditFromForm5884C = new fecherFoundation.FCTextBox();
            this.txtQualExpensesWagesLine21 = new fecherFoundation.FCTextBox();
            this.txtQualExpensesFamilyLeaveWages = new fecherFoundation.FCTextBox();
            this.txtQualExpensesSickLeaveWages = new fecherFoundation.FCTextBox();
            this.txtTotalAdvancesFromForm7200 = new fecherFoundation.FCTextBox();
            this.txt13dRefundableEmplRetentionCredit = new fecherFoundation.FCTextBox();
            this.txt13cRefundableCreditForQualSFLeaveWages = new fecherFoundation.FCTextBox();
            this.txt11cNonRefCredit = new fecherFoundation.FCTextBox();
            this.txt11bNonRefCredit = new fecherFoundation.FCTextBox();
            this.txtQualFamilyLeaveWages = new fecherFoundation.FCTextBox();
            this.txtQualSickLeaveWages = new fecherFoundation.FCTextBox();
            this.lblCreditFromForm5884C = new fecherFoundation.FCLabel();
            this.lblQualExpensesWagesLine21 = new fecherFoundation.FCLabel();
            this.lblQualWagesEmplRetentionCredit = new fecherFoundation.FCLabel();
            this.lblQualExpensesFamilyLeaveWages = new fecherFoundation.FCLabel();
            this.lblQualExpensesSickLeaveWages = new fecherFoundation.FCLabel();
            this.lblTotalAdvancesFromForm7200 = new fecherFoundation.FCLabel();
            this.lbl13dRefundableEmplRetentionCredit = new fecherFoundation.FCLabel();
            this.lbl13cRefundableCreditForQualSFLeaveWages = new fecherFoundation.FCLabel();
            this.lbl11cNonRefCredit = new fecherFoundation.FCLabel();
            this.lbl11bNonRefCredit = new fecherFoundation.FCLabel();
            this.lblQualFamilyLeaveWages = new fecherFoundation.FCLabel();
            this.lblQualSickLeaveWages = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkForceScheduleB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateInfo)).BeginInit();
            this.fraDateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fra941AdditionalInfo)).BeginInit();
            this.fra941AdditionalInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 893);
            this.BottomPanel.Size = new System.Drawing.Size(824, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fra941AdditionalInfo);
            this.ClientArea.Controls.Add(this.Frame6);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraDateInfo);
            this.ClientArea.Size = new System.Drawing.Size(844, 628);
            this.ClientArea.Controls.SetChildIndex(this.fraDateInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame6, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fra941AdditionalInfo, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(844, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(44, 28);
            this.HeaderText.Text = "941";
            // 
            // cmbSchedule
            // 
            this.cmbSchedule.Items.AddRange(new object[] {
            "Semiweekly schedule depositor",
            "Monthly schedule depositor"});
            this.cmbSchedule.Location = new System.Drawing.Point(20, 30);
            this.cmbSchedule.Name = "cmbSchedule";
            this.cmbSchedule.Size = new System.Drawing.Size(336, 40);
            this.cmbSchedule.TabIndex = 6;
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.txtCobraIndividuals);
            this.Frame6.Controls.Add(this.txtCobraPayments);
            this.Frame6.Controls.Add(this.Label8);
            this.Frame6.Controls.Add(this.Label7);
            this.Frame6.Location = new System.Drawing.Point(390, 335);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(340, 108);
            this.Frame6.TabIndex = 48;
            this.Frame6.Text = "Cobra";
            // 
            // txtCobraIndividuals
            // 
            this.txtCobraIndividuals.BackColor = System.Drawing.SystemColors.Window;
            this.txtCobraIndividuals.Location = new System.Drawing.Point(180, 48);
            this.txtCobraIndividuals.Name = "txtCobraIndividuals";
            this.txtCobraIndividuals.Size = new System.Drawing.Size(140, 40);
            this.txtCobraIndividuals.TabIndex = 42;
            this.txtCobraIndividuals.Text = "0";
            this.txtCobraIndividuals.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCobraPayments
            // 
            this.txtCobraPayments.BackColor = System.Drawing.SystemColors.Window;
            this.txtCobraPayments.Location = new System.Drawing.Point(20, 48);
            this.txtCobraPayments.Name = "txtCobraPayments";
            this.txtCobraPayments.Size = new System.Drawing.Size(140, 40);
            this.txtCobraPayments.TabIndex = 41;
            this.txtCobraPayments.Text = "0";
            this.txtCobraPayments.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(180, 30);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(88, 16);
            this.Label8.TabIndex = 50;
            this.Label8.Text = "# INDIVIDUALS";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 30);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(107, 16);
            this.Label7.TabIndex = 49;
            this.Label7.Text = "COBRA PAYMENTS";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtSSAndMedicare);
            this.Frame4.Controls.Add(this.txtIncomeTax);
            this.Frame4.Controls.Add(this.txtTipsAndInsurance);
            this.Frame4.Controls.Add(this.txtSickPay);
            this.Frame4.Controls.Add(this.Label6);
            this.Frame4.Controls.Add(this.Label5);
            this.Frame4.Controls.Add(this.Label4);
            this.Frame4.Controls.Add(this.Label3);
            this.Frame4.Location = new System.Drawing.Point(30, 335);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(340, 108);
            this.Frame4.TabIndex = 37;
            this.Frame4.Text = "Tax Adjustments";
            // 
            // txtSSAndMedicare
            // 
            this.txtSSAndMedicare.BackColor = System.Drawing.SystemColors.Window;
            this.txtSSAndMedicare.Location = new System.Drawing.Point(453, 36);
            this.txtSSAndMedicare.Name = "txtSSAndMedicare";
            this.txtSSAndMedicare.Size = new System.Drawing.Size(104, 40);
            this.txtSSAndMedicare.TabIndex = 46;
            this.txtSSAndMedicare.Text = "0";
            this.txtSSAndMedicare.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtSSAndMedicare.Visible = false;
            // 
            // txtIncomeTax
            // 
            this.txtIncomeTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncomeTax.Location = new System.Drawing.Point(349, 36);
            this.txtIncomeTax.Name = "txtIncomeTax";
            this.txtIncomeTax.Size = new System.Drawing.Size(104, 40);
            this.txtIncomeTax.TabIndex = 44;
            this.txtIncomeTax.Text = "0";
            this.txtIncomeTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIncomeTax.Visible = false;
            // 
            // txtTipsAndInsurance
            // 
            this.txtTipsAndInsurance.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipsAndInsurance.Location = new System.Drawing.Point(180, 48);
            this.txtTipsAndInsurance.Name = "txtTipsAndInsurance";
            this.txtTipsAndInsurance.Size = new System.Drawing.Size(140, 40);
            this.txtTipsAndInsurance.TabIndex = 40;
            this.txtTipsAndInsurance.Text = "0";
            this.txtTipsAndInsurance.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtSickPay
            // 
            this.txtSickPay.BackColor = System.Drawing.SystemColors.Window;
            this.txtSickPay.Location = new System.Drawing.Point(20, 48);
            this.txtSickPay.Name = "txtSickPay";
            this.txtSickPay.Size = new System.Drawing.Size(140, 40);
            this.txtSickPay.TabIndex = 38;
            this.txtSickPay.Text = "0";
            this.txtSickPay.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(437, 18);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(137, 23);
            this.Label6.TabIndex = 47;
            this.Label6.Text = "PREV QTR SS + MEDICARE";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label6.Visible = false;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(349, 18);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(104, 23);
            this.Label5.TabIndex = 45;
            this.Label5.Text = "INCOME TAX WH";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label5.Visible = false;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(180, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(88, 16);
            this.Label4.TabIndex = 43;
            this.Label4.Text = "TIPS + LIFE INS";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 16);
            this.Label3.TabIndex = 39;
            this.Label3.Text = "SICK PAY";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.chkForceScheduleB);
            this.Frame5.Controls.Add(this.cmbSchedule);
            this.Frame5.Controls.Add(this.txtDeposits);
            this.Frame5.Controls.Add(this.Label2);
            this.Frame5.Location = new System.Drawing.Point(30, 157);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(700, 168);
            this.Frame5.TabIndex = 33;
            this.Frame5.Text = "Deposit Information";
            // 
            // chkForceScheduleB
            // 
            this.chkForceScheduleB.Location = new System.Drawing.Point(184, 114);
            this.chkForceScheduleB.Name = "chkForceScheduleB";
            this.chkForceScheduleB.Size = new System.Drawing.Size(341, 22);
            this.chkForceScheduleB.TabIndex = 5;
            this.chkForceScheduleB.Text = "Always show Schedule B (even when less than 2500)";
            // 
            // txtDeposits
            // 
            this.txtDeposits.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeposits.Location = new System.Drawing.Point(20, 108);
            this.txtDeposits.Name = "txtDeposits";
            this.txtDeposits.Size = new System.Drawing.Size(140, 40);
            this.txtDeposits.TabIndex = 4;
            this.txtDeposits.Text = "0";
            this.txtDeposits.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 80);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(535, 16);
            this.Label2.TabIndex = 34;
            this.Label2.Text = "TOTAL DEPOSITS FOR QUARTER, INCLUDING OVERPAYMENT APPLIED FROM A PRIOR QUARTER";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtTransmitterName);
            this.Frame2.Controls.Add(this.txttitle);
            this.Frame2.Controls.Add(this.txtExtension);
            this.Frame2.Controls.Add(this.txtPhone);
            this.Frame2.Controls.Add(this.Label1_9);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_7);
            this.Frame2.Location = new System.Drawing.Point(30, 653);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(778, 140);
            this.Frame2.TabIndex = 29;
            this.Frame2.Text = "Contact Information For Individual Responsible For Accuracy Of 941 Report";
            // 
            // txtTransmitterName
            // 
            this.txtTransmitterName.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransmitterName.Location = new System.Drawing.Point(88, 30);
            this.txtTransmitterName.MaxLength = 30;
            this.txtTransmitterName.Name = "txtTransmitterName";
            this.txtTransmitterName.Size = new System.Drawing.Size(230, 40);
            this.txtTransmitterName.TabIndex = 35;
            // 
            // txttitle
            // 
            this.txttitle.BackColor = System.Drawing.SystemColors.Window;
            this.txttitle.Location = new System.Drawing.Point(88, 80);
            this.txttitle.MaxLength = 30;
            this.txttitle.Name = "txttitle";
            this.txttitle.Size = new System.Drawing.Size(230, 40);
            this.txttitle.TabIndex = 12;
            // 
            // txtExtension
            // 
            this.txtExtension.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtension.Location = new System.Drawing.Point(702, 80);
            this.txtExtension.MaxLength = 4;
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(56, 40);
            this.txtExtension.TabIndex = 14;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(410, 80);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(140, 22);
            this.txtPhone.TabIndex = 13;
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(20, 44);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(37, 16);
            this.Label1_9.TabIndex = 36;
            this.Label1_9.Text = "NAME";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 94);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(37, 16);
            this.Label1_5.TabIndex = 32;
            this.Label1_5.Text = "TITLE";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(336, 94);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(41, 16);
            this.Label1_6.TabIndex = 31;
            this.Label1_6.Text = "PHONE";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(569, 94);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(100, 16);
            this.Label1_7.TabIndex = 30;
            this.Label1_7.Text = "EXT. OR MAILBOX";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtFederalID);
            this.Frame3.Controls.Add(this.Label1_8);
            this.Frame3.Location = new System.Drawing.Point(30, 803);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(362, 90);
            this.Frame3.TabIndex = 27;
            this.Frame3.Text = "Employer Id Number";
            // 
            // txtFederalID
            // 
            this.txtFederalID.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalID.Location = new System.Drawing.Point(184, 30);
            this.txtFederalID.MaxLength = 9;
            this.txtFederalID.Name = "txtFederalID";
            this.txtFederalID.Size = new System.Drawing.Size(158, 40);
            this.txtFederalID.TabIndex = 15;
            this.txtFederalID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalID_KeyPress);
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(20, 44);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(140, 16);
            this.Label1_8.TabIndex = 28;
            this.Label1_8.Text = "FEDERAL EMPLOYER ID";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtEmployerName);
            this.Frame1.Controls.Add(this.txtStreetAddress);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtZip4);
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Controls.Add(this.Label1_1);
            this.Frame1.Controls.Add(this.Label1_2);
            this.Frame1.Controls.Add(this.Label1_3);
            this.Frame1.Controls.Add(this.Label1_4);
            this.Frame1.Location = new System.Drawing.Point(30, 453);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(700, 190);
            this.Frame1.TabIndex = 21;
            this.Frame1.Text = "Employer Name & Address";
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.Location = new System.Drawing.Point(155, 30);
            this.txtEmployerName.MaxLength = 44;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(525, 40);
            this.txtEmployerName.TabIndex = 6;
            // 
            // txtStreetAddress
            // 
            this.txtStreetAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetAddress.Location = new System.Drawing.Point(155, 80);
            this.txtStreetAddress.MaxLength = 35;
            this.txtStreetAddress.Name = "txtStreetAddress";
            this.txtStreetAddress.Size = new System.Drawing.Size(525, 40);
            this.txtStreetAddress.TabIndex = 7;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(155, 130);
            this.txtCity.MaxLength = 20;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(159, 40);
            this.txtCity.TabIndex = 8;
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(388, 130);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(55, 40);
            this.txtState.TabIndex = 9;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(500, 130);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.TabIndex = 10;
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Location = new System.Drawing.Point(610, 130);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(70, 40);
            this.txtZip4.TabIndex = 11;
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 44);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(110, 16);
            this.Label1_0.TabIndex = 26;
            this.Label1_0.Text = "EMPLOYER";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 94);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(110, 16);
            this.Label1_1.TabIndex = 25;
            this.Label1_1.Text = "STREET ADDRESS";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 144);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(110, 16);
            this.Label1_2.TabIndex = 24;
            this.Label1_2.Text = "CITY";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(332, 144);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(41, 16);
            this.Label1_3.TabIndex = 23;
            this.Label1_3.Text = "STATE";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(463, 144);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(27, 16);
            this.Label1_4.TabIndex = 22;
            this.Label1_4.Text = "ZIP";
            // 
            // fraDateInfo
            // 
            this.fraDateInfo.Controls.Add(this.txtStartDate_0);
            this.fraDateInfo.Controls.Add(this.txtEndDate_0);
            this.fraDateInfo.Controls.Add(this.lblInstructions);
            this.fraDateInfo.Controls.Add(this.lblMonth);
            this.fraDateInfo.Controls.Add(this.lblFrom_0);
            this.fraDateInfo.Controls.Add(this.lblTo_0);
            this.fraDateInfo.Location = new System.Drawing.Point(30, 30);
            this.fraDateInfo.Name = "fraDateInfo";
            this.fraDateInfo.Size = new System.Drawing.Size(700, 117);
            this.fraDateInfo.TabIndex = 16;
            this.fraDateInfo.Text = "Date Information";
            // 
            // txtStartDate_0
            // 
            this.txtStartDate_0.Location = new System.Drawing.Point(189, 57);
            this.txtStartDate_0.MaxLength = 10;
            this.txtStartDate_0.Name = "txtStartDate_0";
            this.txtStartDate_0.Size = new System.Drawing.Size(115, 22);
            this.txtStartDate_0.TabIndex = 0;
            // 
            // txtEndDate_0
            // 
            this.txtEndDate_0.Location = new System.Drawing.Point(357, 57);
            this.txtEndDate_0.MaxLength = 10;
            this.txtEndDate_0.Name = "txtEndDate_0";
            this.txtEndDate_0.Size = new System.Drawing.Size(115, 22);
            this.txtEndDate_0.TabIndex = 1;
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(537, 16);
            this.lblInstructions.TabIndex = 20;
            this.lblInstructions.Text = "ENTER DATE RANGE TO COVER 12TH OF THE MONTH FOR THE MONTH OF MARCH";
            // 
            // lblMonth
            // 
            this.lblMonth.Location = new System.Drawing.Point(20, 72);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(80, 16);
            this.lblMonth.TabIndex = 19;
            this.lblMonth.Text = "MARCH";
            // 
            // lblFrom_0
            // 
            this.lblFrom_0.Location = new System.Drawing.Point(123, 72);
            this.lblFrom_0.Name = "lblFrom_0";
            this.lblFrom_0.Size = new System.Drawing.Size(45, 16);
            this.lblFrom_0.TabIndex = 18;
            this.lblFrom_0.Text = "FROM";
            // 
            // lblTo_0
            // 
            this.lblTo_0.Location = new System.Drawing.Point(317, 72);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(25, 20);
            this.lblTo_0.TabIndex = 17;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(305, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(178, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // fra941AdditionalInfo
            // 
            this.fra941AdditionalInfo.Controls.Add(this.txtQualWagesEmplRetentionCredit);
            this.fra941AdditionalInfo.Controls.Add(this.txtCreditFromForm5884C);
            this.fra941AdditionalInfo.Controls.Add(this.txtQualExpensesWagesLine21);
            this.fra941AdditionalInfo.Controls.Add(this.txtQualExpensesFamilyLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.txtQualExpensesSickLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.txtTotalAdvancesFromForm7200);
            this.fra941AdditionalInfo.Controls.Add(this.txt13dRefundableEmplRetentionCredit);
            this.fra941AdditionalInfo.Controls.Add(this.txt13cRefundableCreditForQualSFLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.txt11cNonRefCredit);
            this.fra941AdditionalInfo.Controls.Add(this.txt11bNonRefCredit);
            this.fra941AdditionalInfo.Controls.Add(this.txtQualFamilyLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.txtQualSickLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.lblCreditFromForm5884C);
            this.fra941AdditionalInfo.Controls.Add(this.lblQualExpensesWagesLine21);
            this.fra941AdditionalInfo.Controls.Add(this.lblQualWagesEmplRetentionCredit);
            this.fra941AdditionalInfo.Controls.Add(this.lblQualExpensesFamilyLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.lblQualExpensesSickLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.lblTotalAdvancesFromForm7200);
            this.fra941AdditionalInfo.Controls.Add(this.lbl13dRefundableEmplRetentionCredit);
            this.fra941AdditionalInfo.Controls.Add(this.lbl13cRefundableCreditForQualSFLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.lbl11cNonRefCredit);
            this.fra941AdditionalInfo.Controls.Add(this.lbl11bNonRefCredit);
            this.fra941AdditionalInfo.Controls.Add(this.lblQualFamilyLeaveWages);
            this.fra941AdditionalInfo.Controls.Add(this.lblQualSickLeaveWages);
            this.fra941AdditionalInfo.Location = new System.Drawing.Point(30, 24);
            this.fra941AdditionalInfo.Name = "fra941AdditionalInfo";
            this.fra941AdditionalInfo.Size = new System.Drawing.Size(778, 868);
            this.fra941AdditionalInfo.TabIndex = 1001;
            this.fra941AdditionalInfo.Text = "941 - 2020 Additional Information";
            this.fra941AdditionalInfo.Visible = false;
            // 
            // txtQualWagesEmplRetentionCredit
            // 
            this.txtQualWagesEmplRetentionCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txtQualWagesEmplRetentionCredit.Location = new System.Drawing.Point(569, 591);
            this.txtQualWagesEmplRetentionCredit.Name = "txtQualWagesEmplRetentionCredit";
            this.txtQualWagesEmplRetentionCredit.Size = new System.Drawing.Size(140, 40);
            this.txtQualWagesEmplRetentionCredit.TabIndex = 51;
            this.txtQualWagesEmplRetentionCredit.Text = "0";
            this.txtQualWagesEmplRetentionCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCreditFromForm5884C
            // 
            this.txtCreditFromForm5884C.BackColor = System.Drawing.SystemColors.Window;
            this.txtCreditFromForm5884C.Location = new System.Drawing.Point(569, 703);
            this.txtCreditFromForm5884C.Name = "txtCreditFromForm5884C";
            this.txtCreditFromForm5884C.Size = new System.Drawing.Size(140, 40);
            this.txtCreditFromForm5884C.TabIndex = 53;
            this.txtCreditFromForm5884C.Text = "0";
            this.txtCreditFromForm5884C.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtQualExpensesWagesLine21
            // 
            this.txtQualExpensesWagesLine21.BackColor = System.Drawing.SystemColors.Window;
            this.txtQualExpensesWagesLine21.Location = new System.Drawing.Point(569, 647);
            this.txtQualExpensesWagesLine21.Name = "txtQualExpensesWagesLine21";
            this.txtQualExpensesWagesLine21.Size = new System.Drawing.Size(140, 40);
            this.txtQualExpensesWagesLine21.TabIndex = 52;
            this.txtQualExpensesWagesLine21.Text = "0";
            this.txtQualExpensesWagesLine21.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtQualExpensesFamilyLeaveWages
            // 
            this.txtQualExpensesFamilyLeaveWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtQualExpensesFamilyLeaveWages.Location = new System.Drawing.Point(569, 535);
            this.txtQualExpensesFamilyLeaveWages.Name = "txtQualExpensesFamilyLeaveWages";
            this.txtQualExpensesFamilyLeaveWages.Size = new System.Drawing.Size(140, 40);
            this.txtQualExpensesFamilyLeaveWages.TabIndex = 50;
            this.txtQualExpensesFamilyLeaveWages.Text = "0";
            this.txtQualExpensesFamilyLeaveWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtQualExpensesSickLeaveWages
            // 
            this.txtQualExpensesSickLeaveWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtQualExpensesSickLeaveWages.Location = new System.Drawing.Point(569, 479);
            this.txtQualExpensesSickLeaveWages.Name = "txtQualExpensesSickLeaveWages";
            this.txtQualExpensesSickLeaveWages.Size = new System.Drawing.Size(140, 40);
            this.txtQualExpensesSickLeaveWages.TabIndex = 49;
            this.txtQualExpensesSickLeaveWages.Text = "0";
            this.txtQualExpensesSickLeaveWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtTotalAdvancesFromForm7200
            // 
            this.txtTotalAdvancesFromForm7200.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalAdvancesFromForm7200.Location = new System.Drawing.Point(569, 423);
            this.txtTotalAdvancesFromForm7200.Name = "txtTotalAdvancesFromForm7200";
            this.txtTotalAdvancesFromForm7200.Size = new System.Drawing.Size(140, 40);
            this.txtTotalAdvancesFromForm7200.TabIndex = 48;
            this.txtTotalAdvancesFromForm7200.Text = "0";
            this.txtTotalAdvancesFromForm7200.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txt13dRefundableEmplRetentionCredit
            // 
            this.txt13dRefundableEmplRetentionCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txt13dRefundableEmplRetentionCredit.Location = new System.Drawing.Point(569, 367);
            this.txt13dRefundableEmplRetentionCredit.Name = "txt13dRefundableEmplRetentionCredit";
            this.txt13dRefundableEmplRetentionCredit.Size = new System.Drawing.Size(140, 40);
            this.txt13dRefundableEmplRetentionCredit.TabIndex = 47;
            this.txt13dRefundableEmplRetentionCredit.Text = "0";
            this.txt13dRefundableEmplRetentionCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txt13cRefundableCreditForQualSFLeaveWages
            // 
            this.txt13cRefundableCreditForQualSFLeaveWages.BackColor = System.Drawing.SystemColors.Window;
            this.txt13cRefundableCreditForQualSFLeaveWages.Location = new System.Drawing.Point(569, 311);
            this.txt13cRefundableCreditForQualSFLeaveWages.Name = "txt13cRefundableCreditForQualSFLeaveWages";
            this.txt13cRefundableCreditForQualSFLeaveWages.Size = new System.Drawing.Size(140, 40);
            this.txt13cRefundableCreditForQualSFLeaveWages.TabIndex = 46;
            this.txt13cRefundableCreditForQualSFLeaveWages.Text = "0";
            this.txt13cRefundableCreditForQualSFLeaveWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txt11cNonRefCredit
            // 
            this.txt11cNonRefCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txt11cNonRefCredit.Location = new System.Drawing.Point(569, 199);
            this.txt11cNonRefCredit.Name = "txt11cNonRefCredit";
            this.txt11cNonRefCredit.Size = new System.Drawing.Size(140, 40);
            this.txt11cNonRefCredit.TabIndex = 44;
            this.txt11cNonRefCredit.Text = "0";
            this.txt11cNonRefCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txt11bNonRefCredit
            // 
            this.txt11bNonRefCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txt11bNonRefCredit.Location = new System.Drawing.Point(569, 143);
            this.txt11bNonRefCredit.Name = "txt11bNonRefCredit";
            this.txt11bNonRefCredit.Size = new System.Drawing.Size(140, 40);
            this.txt11bNonRefCredit.TabIndex = 43;
            this.txt11bNonRefCredit.Text = "0";
            this.txt11bNonRefCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtQualFamilyLeaveWages
            // 
            this.txtQualFamilyLeaveWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtQualFamilyLeaveWages.Location = new System.Drawing.Point(569, 87);
            this.txtQualFamilyLeaveWages.Name = "txtQualFamilyLeaveWages";
            this.txtQualFamilyLeaveWages.Size = new System.Drawing.Size(140, 40);
            this.txtQualFamilyLeaveWages.TabIndex = 42;
            this.txtQualFamilyLeaveWages.Text = "0";
            this.txtQualFamilyLeaveWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtQualSickLeaveWages
            // 
            this.txtQualSickLeaveWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtQualSickLeaveWages.Location = new System.Drawing.Point(569, 31);
            this.txtQualSickLeaveWages.Name = "txtQualSickLeaveWages";
            this.txtQualSickLeaveWages.Size = new System.Drawing.Size(140, 40);
            this.txtQualSickLeaveWages.TabIndex = 41;
            this.txtQualSickLeaveWages.Text = "0";
            this.txtQualSickLeaveWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // lblCreditFromForm5884C
            // 
            this.lblCreditFromForm5884C.Location = new System.Drawing.Point(38, 708);
            this.lblCreditFromForm5884C.Name = "lblCreditFromForm5884C";
            this.lblCreditFromForm5884C.Size = new System.Drawing.Size(496, 30);
            this.lblCreditFromForm5884C.TabIndex = 38;
            this.lblCreditFromForm5884C.Text = "23 CREDIT FROM FORM 5884-C, LINE 11, FOR THIS QUARTER";
            this.lblCreditFromForm5884C.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblQualExpensesWagesLine21
            // 
            this.lblQualExpensesWagesLine21.Location = new System.Drawing.Point(38, 652);
            this.lblQualExpensesWagesLine21.Name = "lblQualExpensesWagesLine21";
            this.lblQualExpensesWagesLine21.Size = new System.Drawing.Size(496, 30);
            this.lblQualExpensesWagesLine21.TabIndex = 37;
            this.lblQualExpensesWagesLine21.Text = "22 QUALIFIED HEALTH PLAN EXPENSES ALLOCABLE TO WAGES REPORTED ON LINE 21";
            this.lblQualExpensesWagesLine21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblQualWagesEmplRetentionCredit
            // 
            this.lblQualWagesEmplRetentionCredit.Location = new System.Drawing.Point(38, 596);
            this.lblQualWagesEmplRetentionCredit.Name = "lblQualWagesEmplRetentionCredit";
            this.lblQualWagesEmplRetentionCredit.Size = new System.Drawing.Size(496, 30);
            this.lblQualWagesEmplRetentionCredit.TabIndex = 36;
            this.lblQualWagesEmplRetentionCredit.Text = "21 QUALIFIED WAGES FOR THE EMPLOYEE RETENTION CREDIT";
            this.lblQualWagesEmplRetentionCredit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblQualExpensesFamilyLeaveWages
            // 
            this.lblQualExpensesFamilyLeaveWages.Location = new System.Drawing.Point(38, 540);
            this.lblQualExpensesFamilyLeaveWages.Name = "lblQualExpensesFamilyLeaveWages";
            this.lblQualExpensesFamilyLeaveWages.Size = new System.Drawing.Size(496, 30);
            this.lblQualExpensesFamilyLeaveWages.TabIndex = 35;
            this.lblQualExpensesFamilyLeaveWages.Text = "20 QUALIFIED HEALTH PLAN EXPENSES ALLOCABLE TO QUALIFIED FAMILY LEAVE WAGES";
            this.lblQualExpensesFamilyLeaveWages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblQualExpensesFamilyLeaveWages.WordWrap = true;
            // 
            // lblQualExpensesSickLeaveWages
            // 
            this.lblQualExpensesSickLeaveWages.Location = new System.Drawing.Point(38, 484);
            this.lblQualExpensesSickLeaveWages.Name = "lblQualExpensesSickLeaveWages";
            this.lblQualExpensesSickLeaveWages.Size = new System.Drawing.Size(496, 30);
            this.lblQualExpensesSickLeaveWages.TabIndex = 34;
            this.lblQualExpensesSickLeaveWages.Text = "19 QUALIFIED HEALTH PLAN EXPENSES ALLOCABLE TO QUALIFIED SICK LEAVE WAGES";
            this.lblQualExpensesSickLeaveWages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblQualExpensesSickLeaveWages.WordWrap = true;
            // 
            // lblTotalAdvancesFromForm7200
            // 
            this.lblTotalAdvancesFromForm7200.Location = new System.Drawing.Point(38, 428);
            this.lblTotalAdvancesFromForm7200.Name = "lblTotalAdvancesFromForm7200";
            this.lblTotalAdvancesFromForm7200.Size = new System.Drawing.Size(496, 30);
            this.lblTotalAdvancesFromForm7200.TabIndex = 33;
            this.lblTotalAdvancesFromForm7200.Text = "13F TOTAL ADVANCES RECEIVED FROM FILING FORM(S) 7200 FOR THE QUARTER ";
            this.lblTotalAdvancesFromForm7200.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTotalAdvancesFromForm7200.WordWrap = true;
            // 
            // lbl13dRefundableEmplRetentionCredit
            // 
            this.lbl13dRefundableEmplRetentionCredit.Location = new System.Drawing.Point(38, 372);
            this.lbl13dRefundableEmplRetentionCredit.Name = "lbl13dRefundableEmplRetentionCredit";
            this.lbl13dRefundableEmplRetentionCredit.Size = new System.Drawing.Size(496, 30);
            this.lbl13dRefundableEmplRetentionCredit.TabIndex = 32;
            this.lbl13dRefundableEmplRetentionCredit.Text = "13D REFUNDABLE PORTION OF EMPLOYEE RETENTION CREDIT FROM WORKSHEET 1";
            this.lbl13dRefundableEmplRetentionCredit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl13dRefundableEmplRetentionCredit.WordWrap = true;
            // 
            // lbl13cRefundableCreditForQualSFLeaveWages
            // 
            this.lbl13cRefundableCreditForQualSFLeaveWages.Location = new System.Drawing.Point(38, 316);
            this.lbl13cRefundableCreditForQualSFLeaveWages.Name = "lbl13cRefundableCreditForQualSFLeaveWages";
            this.lbl13cRefundableCreditForQualSFLeaveWages.Size = new System.Drawing.Size(496, 30);
            this.lbl13cRefundableCreditForQualSFLeaveWages.TabIndex = 31;
            this.lbl13cRefundableCreditForQualSFLeaveWages.Text = "13C REFUNDABLE PORTION OF CREDIT FOR QUALIFIED SICK AND FAMILY LEAVE WAGES FROM W" +
    "ORKSHEET 1";
            this.lbl13cRefundableCreditForQualSFLeaveWages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl13cRefundableCreditForQualSFLeaveWages.WordWrap = true;
            // 
            // lbl11cNonRefCredit
            // 
            this.lbl11cNonRefCredit.Location = new System.Drawing.Point(38, 204);
            this.lbl11cNonRefCredit.Name = "lbl11cNonRefCredit";
            this.lbl11cNonRefCredit.Size = new System.Drawing.Size(496, 30);
            this.lbl11cNonRefCredit.TabIndex = 29;
            this.lbl11cNonRefCredit.Text = "11C NONREFUNDABLE PORTION OF EMPLOYEE RETENTION CREDIT FROM WORKSHEET 1";
            this.lbl11cNonRefCredit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl11cNonRefCredit.WordWrap = true;
            // 
            // lbl11bNonRefCredit
            // 
            this.lbl11bNonRefCredit.Location = new System.Drawing.Point(38, 148);
            this.lbl11bNonRefCredit.Name = "lbl11bNonRefCredit";
            this.lbl11bNonRefCredit.Size = new System.Drawing.Size(496, 30);
            this.lbl11bNonRefCredit.TabIndex = 28;
            this.lbl11bNonRefCredit.Text = "11B NONREFUNDABLE PORTION OF CREDIT FOR QUALIFIED SICK AND FAMILY LEAVE WAGES FRO" +
    "M WORKSHEET 1 ";
            this.lbl11bNonRefCredit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl11bNonRefCredit.WordWrap = true;
            // 
            // lblQualFamilyLeaveWages
            // 
            this.lblQualFamilyLeaveWages.Location = new System.Drawing.Point(38, 92);
            this.lblQualFamilyLeaveWages.Name = "lblQualFamilyLeaveWages";
            this.lblQualFamilyLeaveWages.Size = new System.Drawing.Size(496, 30);
            this.lblQualFamilyLeaveWages.TabIndex = 27;
            this.lblQualFamilyLeaveWages.Text = "5A(II) QUALIFIED FAMILY LEAVE WAGES";
            this.lblQualFamilyLeaveWages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblQualFamilyLeaveWages.WordWrap = true;
            // 
            // lblQualSickLeaveWages
            // 
            this.lblQualSickLeaveWages.Location = new System.Drawing.Point(38, 36);
            this.lblQualSickLeaveWages.Name = "lblQualSickLeaveWages";
            this.lblQualSickLeaveWages.Size = new System.Drawing.Size(496, 30);
            this.lblQualSickLeaveWages.TabIndex = 26;
            this.lblQualSickLeaveWages.Text = "5A(I) QUALIFIED SICK LEAVE WAGES";
            this.lblQualSickLeaveWages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblQualSickLeaveWages.WordWrap = true;
            // 
            // frmPrint941
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(844, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrint941";
            this.Text = "941";
            this.Load += new System.EventHandler(this.frmPrint941_Load);
            this.Activated += new System.EventHandler(this.frmPrint941_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrint941_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkForceScheduleB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDateInfo)).EndInit();
            this.fraDateInfo.ResumeLayout(false);
            this.fraDateInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fra941AdditionalInfo)).EndInit();
            this.fra941AdditionalInfo.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		public FCFrame fra941AdditionalInfo;
		public FCTextBox txtQualWagesEmplRetentionCredit;
		public FCTextBox txtCreditFromForm5884C;
		public FCTextBox txtQualExpensesWagesLine21;
		public FCTextBox txtQualExpensesFamilyLeaveWages;
		public FCTextBox txtQualExpensesSickLeaveWages;
		public FCTextBox txtTotalAdvancesFromForm7200;
		public FCTextBox txt13dRefundableEmplRetentionCredit;
		public FCTextBox txt13cRefundableCreditForQualSFLeaveWages;
		public FCTextBox txt11cNonRefCredit;
		public FCTextBox txt11bNonRefCredit;
		public FCTextBox txtQualFamilyLeaveWages;
		public FCTextBox txtQualSickLeaveWages;
		public FCLabel lblCreditFromForm5884C;
		public FCLabel lblQualExpensesWagesLine21;
		public FCLabel lblQualWagesEmplRetentionCredit;
		public FCLabel lblQualExpensesFamilyLeaveWages;
		public FCLabel lblQualExpensesSickLeaveWages;
		public FCLabel lblTotalAdvancesFromForm7200;
		public FCLabel lbl13dRefundableEmplRetentionCredit;
		public FCLabel lbl13cRefundableCreditForQualSFLeaveWages;
		public FCLabel lbl11cNonRefCredit;
		public FCLabel lbl11bNonRefCredit;
		public FCLabel lblQualFamilyLeaveWages;
		public FCLabel lblQualSickLeaveWages;
	}
}
