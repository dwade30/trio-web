//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmTaxStatusCodes.
	/// </summary>
	partial class frmTaxStatusCodes
	{
		public FCGrid vsTaxStatusCodes;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdSave;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuEditable;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.vsTaxStatusCodes = new fecherFoundation.FCGrid();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdRefresh = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.mnuEditable = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsTaxStatusCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 417);
			this.BottomPanel.Size = new System.Drawing.Size(903, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsTaxStatusCodes);
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Controls.Add(this.cmdRefresh);
			this.ClientArea.Size = new System.Drawing.Size(923, 560);
			this.ClientArea.Controls.SetChildIndex(this.cmdRefresh, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsTaxStatusCodes, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(923, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(194, 28);
			this.HeaderText.Text = "Tax Status Codes";
			// 
			// vsTaxStatusCodes
			// 
			this.vsTaxStatusCodes.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsTaxStatusCodes.Cols = 10;
			this.vsTaxStatusCodes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsTaxStatusCodes.Location = new System.Drawing.Point(30, 30);
			this.vsTaxStatusCodes.Name = "vsTaxStatusCodes";
			this.vsTaxStatusCodes.ReadOnly = false;
			this.vsTaxStatusCodes.Rows = 50;
			this.vsTaxStatusCodes.Size = new System.Drawing.Size(882, 387);
			this.vsTaxStatusCodes.StandardTab = false;
			this.vsTaxStatusCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsTaxStatusCodes.TabIndex = 0;
			this.vsTaxStatusCodes.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsTaxStatusCodes_KeyDownEdit);
			this.vsTaxStatusCodes.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsTaxStatusCodes_AfterEdit);
			this.vsTaxStatusCodes.CurrentCellChanged += new System.EventHandler(this.vsTaxStatusCodes_RowColChange);
			this.vsTaxStatusCodes.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsTaxStatusCodes_MouseMoveEvent);
			this.vsTaxStatusCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsTaxStatusCodes_KeyDownEvent);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Location = new System.Drawing.Point(827, 46);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(45, 24);
			this.cmdPrint.TabIndex = 5;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdRefresh
			// 
			this.cmdRefresh.Location = new System.Drawing.Point(712, 52);
			this.cmdRefresh.Name = "cmdRefresh";
			this.cmdRefresh.Size = new System.Drawing.Size(60, 24);
			this.cmdRefresh.TabIndex = 4;
			this.cmdRefresh.Text = "Refresh";
			this.cmdRefresh.Visible = false;
			this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.Location = new System.Drawing.Point(777, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(52, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Visible = false;
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Location = new System.Drawing.Point(835, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(60, 24);
			this.cmdDelete.TabIndex = 3;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Visible = false;
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(423, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// mnuEditable
			// 
			this.mnuEditable.Index = 0;
			this.mnuEditable.Name = "mnuEditable";
			this.mnuEditable.Text = "Enable Grid for Editing";
			this.mnuEditable.Visible = false;
			this.mnuEditable.Click += new System.EventHandler(this.mnuEditable_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 1;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			this.mnuSP2.Visible = false;
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Enabled = false;
			this.mnuPrintPreview.Index = 2;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print/Preview";
			this.mnuPrintPreview.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditable,
            this.mnuSP2,
            this.mnuPrintPreview});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
			// 
			// mnuNew
			// 
			this.mnuNew.Index = -1;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = -1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = -1;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuRefresh
			// 
			this.mnuRefresh.Index = -1;
			this.mnuRefresh.Name = "mnuRefresh";
			this.mnuRefresh.Text = "Refresh";
			this.mnuRefresh.Visible = false;
			this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
			// 
			// mnuPrint
			// 
			this.mnuPrint.Enabled = false;
			this.mnuPrint.Index = -1;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Visible = false;
			// 
			// mnuSP3
			// 
			this.mnuSP3.Index = -1;
			this.mnuSP3.Name = "mnuSP3";
			this.mnuSP3.Text = "-";
			this.mnuSP3.Visible = false;
			// 
			// mnuSave
			// 
			this.mnuSave.Index = -1;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                                       ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = -1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit           ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP4
			// 
			this.mnuSP4.Index = -1;
			this.mnuSP4.Name = "mnuSP4";
			this.mnuSP4.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmTaxStatusCodes
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(923, 620);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTaxStatusCodes";
			this.Text = "Tax Status Codes";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmTaxStatusCodes_Load);
			this.Activated += new System.EventHandler(this.frmTaxStatusCodes_Activated);
			this.Resize += new System.EventHandler(this.frmTaxStatusCodes_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxStatusCodes_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsTaxStatusCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}