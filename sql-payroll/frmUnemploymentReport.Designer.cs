//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmUnemploymentReport.
	/// </summary>
	partial class frmUnemploymentReport
	{
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cmbStartDate;
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cmbEndDate;
		public System.Collections.Generic.List<T2KDateBox> txtStartDate;
		public System.Collections.Generic.List<T2KDateBox> txtEndDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMonth;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblFrom;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraDateInfo;
		public fecherFoundation.FCComboBox cmbStartDate_0;
		public fecherFoundation.FCComboBox cmbEndDate_0;
		public fecherFoundation.FCComboBox cmbStartDate_1;
		public fecherFoundation.FCComboBox cmbStartDate_2;
		public fecherFoundation.FCComboBox cmbEndDate_1;
		public fecherFoundation.FCComboBox cmbEndDate_2;
		public Global.T2KDateBox txtStartDate_0;
		public Global.T2KDateBox txtEndDate_0;
		public Global.T2KDateBox txtStartDate_1;
		public Global.T2KDateBox txtEndDate_1;
		public Global.T2KDateBox txtStartDate_2;
		public Global.T2KDateBox txtEndDate_2;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCLabel lblMonth_0;
		public fecherFoundation.FCLabel lblFrom_0;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCLabel lblMonth_1;
		public fecherFoundation.FCLabel lblFrom_1;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblMonth_2;
		public fecherFoundation.FCLabel lblFrom_2;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCFrame fraTownInfo;
		public fecherFoundation.FCTextBox txtReportNumber;
		public fecherFoundation.FCTextBox txtEmployer;
		public fecherFoundation.FCTextBox txtPreparer;
		public fecherFoundation.FCTextBox txtFederalID;
		public fecherFoundation.FCTextBox txtStateID;
		public fecherFoundation.FCTextBox txtMemberCode;
		public fecherFoundation.FCTextBox txtSeasonalCode;
		public Global.T2KPhoneNumberBox txtTelephone;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCFrame fraSequenceRange;
		public fecherFoundation.FCFrame fraSequenceSingleSelection;
		public fecherFoundation.FCComboBox cboSingleSequence;
		public fecherFoundation.FCComboBox cboSequenceRange;
		public fecherFoundation.FCFrame fraSequenceRangeSelection;
		public fecherFoundation.FCComboBox cboStartSequence;
		public fecherFoundation.FCComboBox cboEndSequence;
		public fecherFoundation.FCLabel lblTo_3;
		public fecherFoundation.FCComboBox cboSequence;
		public fecherFoundation.FCComboBox cboYear;
		public fecherFoundation.FCComboBox cboQuarter;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblQuarter;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraDateInfo = new fecherFoundation.FCFrame();
			this.cmbStartDate_0 = new fecherFoundation.FCComboBox();
			this.cmbEndDate_0 = new fecherFoundation.FCComboBox();
			this.cmbStartDate_1 = new fecherFoundation.FCComboBox();
			this.cmbStartDate_2 = new fecherFoundation.FCComboBox();
			this.cmbEndDate_1 = new fecherFoundation.FCComboBox();
			this.cmbEndDate_2 = new fecherFoundation.FCComboBox();
			this.txtStartDate_0 = new Global.T2KDateBox();
			this.txtEndDate_0 = new Global.T2KDateBox();
			this.txtStartDate_1 = new Global.T2KDateBox();
			this.txtEndDate_1 = new Global.T2KDateBox();
			this.txtStartDate_2 = new Global.T2KDateBox();
			this.txtEndDate_2 = new Global.T2KDateBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.lblMonth_0 = new fecherFoundation.FCLabel();
			this.lblFrom_0 = new fecherFoundation.FCLabel();
			this.lblTo_0 = new fecherFoundation.FCLabel();
			this.lblMonth_1 = new fecherFoundation.FCLabel();
			this.lblFrom_1 = new fecherFoundation.FCLabel();
			this.lblTo_1 = new fecherFoundation.FCLabel();
			this.lblMonth_2 = new fecherFoundation.FCLabel();
			this.lblFrom_2 = new fecherFoundation.FCLabel();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.fraTownInfo = new fecherFoundation.FCFrame();
			this.txtReportNumber = new fecherFoundation.FCTextBox();
			this.txtEmployer = new fecherFoundation.FCTextBox();
			this.txtPreparer = new fecherFoundation.FCTextBox();
			this.txtFederalID = new fecherFoundation.FCTextBox();
			this.txtStateID = new fecherFoundation.FCTextBox();
			this.txtMemberCode = new fecherFoundation.FCTextBox();
			this.txtSeasonalCode = new fecherFoundation.FCTextBox();
			this.txtTelephone = new Global.T2KPhoneNumberBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.fraSequenceRange = new fecherFoundation.FCFrame();
			this.fraSequenceSingleSelection = new fecherFoundation.FCFrame();
			this.cboSingleSequence = new fecherFoundation.FCComboBox();
			this.cboSequenceRange = new fecherFoundation.FCComboBox();
			this.fraSequenceRangeSelection = new fecherFoundation.FCFrame();
			this.cboStartSequence = new fecherFoundation.FCComboBox();
			this.cboEndSequence = new fecherFoundation.FCComboBox();
			this.lblTo_3 = new fecherFoundation.FCLabel();
			this.cboSequence = new fecherFoundation.FCComboBox();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.cboQuarter = new fecherFoundation.FCComboBox();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.lblQuarter = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdClearDates = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateInfo)).BeginInit();
			this.fraDateInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).BeginInit();
			this.fraTownInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSequenceRange)).BeginInit();
			this.fraSequenceRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSequenceSingleSelection)).BeginInit();
			this.fraSequenceSingleSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSequenceRangeSelection)).BeginInit();
			this.fraSequenceRangeSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearDates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrintPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 962);
			this.BottomPanel.Size = new System.Drawing.Size(652, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraDateInfo);
			this.ClientArea.Controls.Add(this.fraTownInfo);
			this.ClientArea.Controls.Add(this.fraSequenceRange);
			this.ClientArea.Controls.Add(this.cboSequence);
			this.ClientArea.Controls.Add(this.cboYear);
			this.ClientArea.Controls.Add(this.cboQuarter);
			this.ClientArea.Controls.Add(this.lblSequence);
			this.ClientArea.Controls.Add(this.lblYear);
			this.ClientArea.Controls.Add(this.lblQuarter);
			this.ClientArea.Size = new System.Drawing.Size(672, 628);
			this.ClientArea.Controls.SetChildIndex(this.lblQuarter, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblYear, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblSequence, 0);
			this.ClientArea.Controls.SetChildIndex(this.cboQuarter, 0);
			this.ClientArea.Controls.SetChildIndex(this.cboYear, 0);
			this.ClientArea.Controls.SetChildIndex(this.cboSequence, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSequenceRange, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraTownInfo, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraDateInfo, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdClearDates);
			this.TopPanel.Size = new System.Drawing.Size(672, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClearDates, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// fraDateInfo
			// 
			this.fraDateInfo.Controls.Add(this.cmbStartDate_0);
			this.fraDateInfo.Controls.Add(this.cmbEndDate_0);
			this.fraDateInfo.Controls.Add(this.cmbStartDate_1);
			this.fraDateInfo.Controls.Add(this.cmbStartDate_2);
			this.fraDateInfo.Controls.Add(this.cmbEndDate_1);
			this.fraDateInfo.Controls.Add(this.cmbEndDate_2);
			this.fraDateInfo.Controls.Add(this.txtStartDate_0);
			this.fraDateInfo.Controls.Add(this.txtEndDate_0);
			this.fraDateInfo.Controls.Add(this.txtStartDate_1);
			this.fraDateInfo.Controls.Add(this.txtEndDate_1);
			this.fraDateInfo.Controls.Add(this.txtStartDate_2);
			this.fraDateInfo.Controls.Add(this.txtEndDate_2);
			this.fraDateInfo.Controls.Add(this.lblInstructions);
			this.fraDateInfo.Controls.Add(this.lblMonth_0);
			this.fraDateInfo.Controls.Add(this.lblFrom_0);
			this.fraDateInfo.Controls.Add(this.lblTo_0);
			this.fraDateInfo.Controls.Add(this.lblMonth_1);
			this.fraDateInfo.Controls.Add(this.lblFrom_1);
			this.fraDateInfo.Controls.Add(this.lblTo_1);
			this.fraDateInfo.Controls.Add(this.lblMonth_2);
			this.fraDateInfo.Controls.Add(this.lblFrom_2);
			this.fraDateInfo.Controls.Add(this.lblTo_2);
			this.fraDateInfo.Location = new System.Drawing.Point(30, 105);
			this.fraDateInfo.Name = "fraDateInfo";
			this.fraDateInfo.Size = new System.Drawing.Size(596, 217);
			this.fraDateInfo.TabIndex = 37;
			this.fraDateInfo.Text = "Date Information";
			// 
			// cmbStartDate_0
			// 
			this.cmbStartDate_0.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStartDate_0.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
			this.cmbStartDate_0.Location = new System.Drawing.Point(166, 57);
			this.cmbStartDate_0.Name = "cmbStartDate_0";
			this.cmbStartDate_0.Size = new System.Drawing.Size(163, 40);
			this.cmbStartDate_0.TabIndex = 53;
			this.cmbStartDate_0.Text = "cmbStartDate";
			// 
			// cmbEndDate_0
			// 
			this.cmbEndDate_0.BackColor = System.Drawing.SystemColors.Window;
			this.cmbEndDate_0.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
			this.cmbEndDate_0.Location = new System.Drawing.Point(411, 57);
			this.cmbEndDate_0.Name = "cmbEndDate_0";
			this.cmbEndDate_0.Size = new System.Drawing.Size(163, 40);
			this.cmbEndDate_0.TabIndex = 52;
			this.cmbEndDate_0.Text = "cmbStartDate";
			// 
			// cmbStartDate_1
			// 
			this.cmbStartDate_1.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStartDate_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
			this.cmbStartDate_1.Location = new System.Drawing.Point(166, 107);
			this.cmbStartDate_1.Name = "cmbStartDate_1";
			this.cmbStartDate_1.Size = new System.Drawing.Size(163, 40);
			this.cmbStartDate_1.TabIndex = 51;
			this.cmbStartDate_1.Text = "cmbStartDate";
			// 
			// cmbStartDate_2
			// 
			this.cmbStartDate_2.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStartDate_2.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
			this.cmbStartDate_2.Location = new System.Drawing.Point(166, 157);
			this.cmbStartDate_2.Name = "cmbStartDate_2";
			this.cmbStartDate_2.Size = new System.Drawing.Size(163, 40);
			this.cmbStartDate_2.TabIndex = 50;
			this.cmbStartDate_2.Text = "cmbStartDate";
			// 
			// cmbEndDate_1
			// 
			this.cmbEndDate_1.BackColor = System.Drawing.SystemColors.Window;
			this.cmbEndDate_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
			this.cmbEndDate_1.Location = new System.Drawing.Point(411, 107);
			this.cmbEndDate_1.Name = "cmbEndDate_1";
			this.cmbEndDate_1.Size = new System.Drawing.Size(163, 40);
			this.cmbEndDate_1.TabIndex = 49;
			this.cmbEndDate_1.Text = "cmbStartDate";
			// 
			// cmbEndDate_2
			// 
			this.cmbEndDate_2.BackColor = System.Drawing.SystemColors.Window;
			this.cmbEndDate_2.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
			this.cmbEndDate_2.Location = new System.Drawing.Point(411, 157);
			this.cmbEndDate_2.Name = "cmbEndDate_2";
			this.cmbEndDate_2.Size = new System.Drawing.Size(163, 40);
			this.cmbEndDate_2.TabIndex = 48;
			this.cmbEndDate_2.Text = "cmbStartDate";
			// 
			// txtStartDate_0
			// 
			this.txtStartDate_0.Location = new System.Drawing.Point(166, 57);
			this.txtStartDate_0.MaxLength = 10;
			this.txtStartDate_0.Name = "txtStartDate_0";
			this.txtStartDate_0.Size = new System.Drawing.Size(115, 22);
			this.txtStartDate_0.TabIndex = 3;
			this.txtStartDate_0.Visible = false;
			// 
			// txtEndDate_0
			// 
			this.txtEndDate_0.Location = new System.Drawing.Point(411, 57);
			this.txtEndDate_0.MaxLength = 10;
			this.txtEndDate_0.Name = "txtEndDate_0";
			this.txtEndDate_0.Size = new System.Drawing.Size(163, 22);
			this.txtEndDate_0.TabIndex = 4;
			this.txtEndDate_0.Visible = false;
			// 
			// txtStartDate_1
			// 
			this.txtStartDate_1.Location = new System.Drawing.Point(166, 107);
			this.txtStartDate_1.MaxLength = 10;
			this.txtStartDate_1.Name = "txtStartDate_1";
			this.txtStartDate_1.Size = new System.Drawing.Size(115, 22);
			this.txtStartDate_1.TabIndex = 5;
			this.txtStartDate_1.Visible = false;
			// 
			// txtEndDate_1
			// 
			this.txtEndDate_1.Location = new System.Drawing.Point(410, 107);
			this.txtEndDate_1.MaxLength = 10;
			this.txtEndDate_1.Name = "txtEndDate_1";
			this.txtEndDate_1.Size = new System.Drawing.Size(163, 22);
			this.txtEndDate_1.TabIndex = 6;
			this.txtEndDate_1.Visible = false;
			// 
			// txtStartDate_2
			// 
			this.txtStartDate_2.Location = new System.Drawing.Point(166, 157);
			this.txtStartDate_2.MaxLength = 10;
			this.txtStartDate_2.Name = "txtStartDate_2";
			this.txtStartDate_2.Size = new System.Drawing.Size(115, 22);
			this.txtStartDate_2.TabIndex = 7;
			this.txtStartDate_2.Visible = false;
			// 
			// txtEndDate_2
			// 
			this.txtEndDate_2.Location = new System.Drawing.Point(410, 157);
			this.txtEndDate_2.MaxLength = 10;
			this.txtEndDate_2.Name = "txtEndDate_2";
			this.txtEndDate_2.Size = new System.Drawing.Size(163, 22);
			this.txtEndDate_2.TabIndex = 8;
			this.txtEndDate_2.Visible = false;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(450, 16);
			this.lblInstructions.TabIndex = 47;
			this.lblInstructions.Text = "ENTER DATE RANGES TO COVER 12TH OF THE MONTH FOR EACH MONTH";
			// 
			// lblMonth_0
			// 
			this.lblMonth_0.Location = new System.Drawing.Point(20, 71);
			this.lblMonth_0.Name = "lblMonth_0";
			this.lblMonth_0.Size = new System.Drawing.Size(60, 19);
			this.lblMonth_0.TabIndex = 46;
			this.lblMonth_0.Text = "MONTH 1";
			// 
			// lblFrom_0
			// 
			this.lblFrom_0.Location = new System.Drawing.Point(92, 71);
			this.lblFrom_0.Name = "lblFrom_0";
			this.lblFrom_0.Size = new System.Drawing.Size(41, 18);
			this.lblFrom_0.TabIndex = 45;
			this.lblFrom_0.Text = "FROM";
			this.lblFrom_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTo_0
			// 
			this.lblTo_0.Location = new System.Drawing.Point(360, 71);
			this.lblTo_0.Name = "lblTo_0";
			this.lblTo_0.Size = new System.Drawing.Size(17, 19);
			this.lblTo_0.TabIndex = 44;
			this.lblTo_0.Text = "TO";
			this.lblTo_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblMonth_1
			// 
			this.lblMonth_1.Location = new System.Drawing.Point(19, 121);
			this.lblMonth_1.Name = "lblMonth_1";
			this.lblMonth_1.Size = new System.Drawing.Size(60, 19);
			this.lblMonth_1.TabIndex = 43;
			this.lblMonth_1.Text = "MONTH 2";
			// 
			// lblFrom_1
			// 
			this.lblFrom_1.Location = new System.Drawing.Point(92, 121);
			this.lblFrom_1.Name = "lblFrom_1";
			this.lblFrom_1.Size = new System.Drawing.Size(41, 18);
			this.lblFrom_1.TabIndex = 42;
			this.lblFrom_1.Text = "FROM";
			this.lblFrom_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTo_1
			// 
			this.lblTo_1.Location = new System.Drawing.Point(360, 121);
			this.lblTo_1.Name = "lblTo_1";
			this.lblTo_1.Size = new System.Drawing.Size(17, 19);
			this.lblTo_1.TabIndex = 41;
			this.lblTo_1.Text = "TO";
			this.lblTo_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblMonth_2
			// 
			this.lblMonth_2.Location = new System.Drawing.Point(19, 171);
			this.lblMonth_2.Name = "lblMonth_2";
			this.lblMonth_2.Size = new System.Drawing.Size(60, 19);
			this.lblMonth_2.TabIndex = 40;
			this.lblMonth_2.Text = "MONTH 3";
			// 
			// lblFrom_2
			// 
			this.lblFrom_2.Location = new System.Drawing.Point(92, 171);
			this.lblFrom_2.Name = "lblFrom_2";
			this.lblFrom_2.Size = new System.Drawing.Size(41, 18);
			this.lblFrom_2.TabIndex = 39;
			this.lblFrom_2.Text = "FROM";
			this.lblFrom_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTo_2
			// 
			this.lblTo_2.Location = new System.Drawing.Point(360, 171);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(17, 19);
			this.lblTo_2.TabIndex = 38;
			this.lblTo_2.Text = "TO";
			this.lblTo_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraTownInfo
			// 
			this.fraTownInfo.Controls.Add(this.txtReportNumber);
			this.fraTownInfo.Controls.Add(this.txtEmployer);
			this.fraTownInfo.Controls.Add(this.txtPreparer);
			this.fraTownInfo.Controls.Add(this.txtFederalID);
			this.fraTownInfo.Controls.Add(this.txtStateID);
			this.fraTownInfo.Controls.Add(this.txtMemberCode);
			this.fraTownInfo.Controls.Add(this.txtSeasonalCode);
			this.fraTownInfo.Controls.Add(this.txtTelephone);
			this.fraTownInfo.Controls.Add(this.Label2);
			this.fraTownInfo.Controls.Add(this.Label3);
			this.fraTownInfo.Controls.Add(this.Label4);
			this.fraTownInfo.Controls.Add(this.Label5);
			this.fraTownInfo.Controls.Add(this.Label6);
			this.fraTownInfo.Controls.Add(this.Label7);
			this.fraTownInfo.Controls.Add(this.Label8);
			this.fraTownInfo.Controls.Add(this.Label9);
			this.fraTownInfo.Location = new System.Drawing.Point(30, 522);
			this.fraTownInfo.Name = "fraTownInfo";
			this.fraTownInfo.Size = new System.Drawing.Size(596, 440);
			this.fraTownInfo.TabIndex = 28;
			this.fraTownInfo.Text = "Employer Information";
			// 
			// txtReportNumber
			// 
			this.txtReportNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtReportNumber.Location = new System.Drawing.Point(151, 30);
			this.txtReportNumber.MaxLength = 10;
			this.txtReportNumber.Name = "txtReportNumber";
			this.txtReportNumber.Size = new System.Drawing.Size(140, 40);
			this.txtReportNumber.TabIndex = 13;
			this.txtReportNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReportNumber_KeyPress);
			// 
			// txtEmployer
			// 
			this.txtEmployer.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployer.Location = new System.Drawing.Point(151, 130);
			this.txtEmployer.Name = "txtEmployer";
			this.txtEmployer.Size = new System.Drawing.Size(423, 40);
			this.txtEmployer.TabIndex = 15;
			// 
			// txtPreparer
			// 
			this.txtPreparer.BackColor = System.Drawing.SystemColors.Window;
			this.txtPreparer.Location = new System.Drawing.Point(151, 181);
			this.txtPreparer.Name = "txtPreparer";
			this.txtPreparer.Size = new System.Drawing.Size(423, 40);
			this.txtPreparer.TabIndex = 16;
			// 
			// txtFederalID
			// 
			this.txtFederalID.BackColor = System.Drawing.SystemColors.Window;
			this.txtFederalID.Location = new System.Drawing.Point(151, 230);
			this.txtFederalID.Name = "txtFederalID";
			this.txtFederalID.Size = new System.Drawing.Size(423, 40);
			this.txtFederalID.TabIndex = 17;
			// 
			// txtStateID
			// 
			this.txtStateID.BackColor = System.Drawing.SystemColors.Window;
			this.txtStateID.Location = new System.Drawing.Point(151, 280);
			this.txtStateID.Name = "txtStateID";
			this.txtStateID.Size = new System.Drawing.Size(423, 40);
			this.txtStateID.TabIndex = 18;
			// 
			// txtMemberCode
			// 
			this.txtMemberCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtMemberCode.Location = new System.Drawing.Point(151, 330);
			this.txtMemberCode.Name = "txtMemberCode";
			this.txtMemberCode.Size = new System.Drawing.Size(423, 40);
			this.txtMemberCode.TabIndex = 19;
			// 
			// txtSeasonalCode
			// 
			this.txtSeasonalCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtSeasonalCode.Location = new System.Drawing.Point(151, 380);
			this.txtSeasonalCode.Name = "txtSeasonalCode";
			this.txtSeasonalCode.Size = new System.Drawing.Size(423, 40);
			this.txtSeasonalCode.TabIndex = 20;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Location = new System.Drawing.Point(151, 80);
			this.txtTelephone.MaxLength = 13;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Size = new System.Drawing.Size(140, 22);
			this.txtTelephone.TabIndex = 14;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 144);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(100, 16);
			this.Label2.TabIndex = 36;
			this.Label2.Text = "EMPLOYER";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(100, 16);
			this.Label3.TabIndex = 35;
			this.Label3.Text = "REPORT NO";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 195);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(100, 16);
			this.Label4.TabIndex = 34;
			this.Label4.Text = "PREPARER";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 94);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(100, 16);
			this.Label5.TabIndex = 33;
			this.Label5.Text = "TELEPHONE";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 244);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(100, 16);
			this.Label6.TabIndex = 32;
			this.Label6.Text = "FEDERAL ID#";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 294);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(100, 16);
			this.Label7.TabIndex = 31;
			this.Label7.Text = "STATE ID#";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(20, 344);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(100, 16);
			this.Label8.TabIndex = 30;
			this.Label8.Text = "MEMBER CODE";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 394);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(100, 16);
			this.Label9.TabIndex = 29;
			this.Label9.Text = "SEASONAL CODE";
			// 
			// fraSequenceRange
			// 
			this.fraSequenceRange.AppearanceKey = "groupBoxLeftBorder";
			this.fraSequenceRange.Controls.Add(this.fraSequenceSingleSelection);
			this.fraSequenceRange.Controls.Add(this.cboSequenceRange);
			this.fraSequenceRange.Controls.Add(this.fraSequenceRangeSelection);
			this.fraSequenceRange.Location = new System.Drawing.Point(30, 332);
			this.fraSequenceRange.Name = "fraSequenceRange";
			this.fraSequenceRange.Size = new System.Drawing.Size(376, 170);
			this.fraSequenceRange.TabIndex = 24;
			this.fraSequenceRange.Text = "Sequence Range";
			// 
			// fraSequenceSingleSelection
			// 
			this.fraSequenceSingleSelection.Controls.Add(this.cboSingleSequence);
			this.fraSequenceSingleSelection.Location = new System.Drawing.Point(20, 80);
			this.fraSequenceSingleSelection.Name = "fraSequenceSingleSelection";
			this.fraSequenceSingleSelection.Size = new System.Drawing.Size(209, 90);
			this.fraSequenceSingleSelection.TabIndex = 25;
			this.fraSequenceSingleSelection.Text = "Select Sequence ";
			this.fraSequenceSingleSelection.Visible = false;
			// 
			// cboSingleSequence
			// 
			this.cboSingleSequence.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleSequence.Location = new System.Drawing.Point(20, 30);
			this.cboSingleSequence.Name = "cboSingleSequence";
			this.cboSingleSequence.Size = new System.Drawing.Size(169, 40);
			this.cboSingleSequence.TabIndex = 12;
			// 
			// cboSequenceRange
			// 
			this.cboSequenceRange.BackColor = System.Drawing.SystemColors.Window;
			this.cboSequenceRange.Items.AddRange(new object[] {
            "All Sequences",
            "Single Sequence",
            "Range of Sequences"});
			this.cboSequenceRange.Location = new System.Drawing.Point(20, 30);
			this.cboSequenceRange.Name = "cboSequenceRange";
			this.cboSequenceRange.Size = new System.Drawing.Size(209, 40);
			this.cboSequenceRange.TabIndex = 9;
			this.cboSequenceRange.SelectedIndexChanged += new System.EventHandler(this.cboSequenceRange_SelectedIndexChanged);
			// 
			// fraSequenceRangeSelection
			// 
			this.fraSequenceRangeSelection.Controls.Add(this.cboStartSequence);
			this.fraSequenceRangeSelection.Controls.Add(this.cboEndSequence);
			this.fraSequenceRangeSelection.Controls.Add(this.lblTo_3);
			this.fraSequenceRangeSelection.Location = new System.Drawing.Point(20, 80);
			this.fraSequenceRangeSelection.Name = "fraSequenceRangeSelection";
			this.fraSequenceRangeSelection.Size = new System.Drawing.Size(335, 90);
			this.fraSequenceRangeSelection.TabIndex = 26;
			this.fraSequenceRangeSelection.Text = "Select Sequence Range";
			this.fraSequenceRangeSelection.Visible = false;
			// 
			// cboStartSequence
			// 
			this.cboStartSequence.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartSequence.Location = new System.Drawing.Point(20, 30);
			this.cboStartSequence.Name = "cboStartSequence";
			this.cboStartSequence.Size = new System.Drawing.Size(120, 40);
			this.cboStartSequence.TabIndex = 10;
			// 
			// cboEndSequence
			// 
			this.cboEndSequence.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndSequence.Location = new System.Drawing.Point(195, 30);
			this.cboEndSequence.Name = "cboEndSequence";
			this.cboEndSequence.Size = new System.Drawing.Size(120, 40);
			this.cboEndSequence.TabIndex = 11;
			// 
			// lblTo_3
			// 
			this.lblTo_3.Location = new System.Drawing.Point(156, 44);
			this.lblTo_3.Name = "lblTo_3";
			this.lblTo_3.Size = new System.Drawing.Size(23, 16);
			this.lblTo_3.TabIndex = 27;
			this.lblTo_3.Text = "TO";
			this.lblTo_3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cboSequence
			// 
			this.cboSequence.BackColor = System.Drawing.SystemColors.Window;
			this.cboSequence.Items.AddRange(new object[] {
            "Name",
            "Sequence #"});
			this.cboSequence.Location = new System.Drawing.Point(180, 55);
			this.cboSequence.Name = "cboSequence";
			this.cboSequence.Size = new System.Drawing.Size(183, 40);
			this.cboSequence.TabIndex = 1;
			// 
			// cboYear
			// 
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.Location = new System.Drawing.Point(383, 55);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(139, 40);
			this.cboYear.TabIndex = 2;
			this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
			// 
			// cboQuarter
			// 
			this.cboQuarter.BackColor = System.Drawing.SystemColors.Window;
			this.cboQuarter.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
			this.cboQuarter.Location = new System.Drawing.Point(30, 55);
			this.cboQuarter.Name = "cboQuarter";
			this.cboQuarter.Size = new System.Drawing.Size(130, 40);
			this.cboQuarter.SelectedIndexChanged += new System.EventHandler(this.cboQuarter_SelectedIndexChanged);
			// 
			// lblSequence
			// 
			this.lblSequence.Location = new System.Drawing.Point(180, 30);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(64, 15);
			this.lblSequence.TabIndex = 23;
			this.lblSequence.Text = "SEQUENCE";
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(383, 30);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(86, 15);
			this.lblYear.TabIndex = 22;
			this.lblYear.Text = "SELECT YEAR";
			// 
			// lblQuarter
			// 
			this.lblQuarter.Location = new System.Drawing.Point(30, 30);
			this.lblQuarter.Name = "lblQuarter";
			this.lblQuarter.Size = new System.Drawing.Size(110, 15);
			this.lblQuarter.TabIndex = 21;
			this.lblQuarter.Text = "SELECT QUARTER";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileClear,
            this.mnuFileSeperator,
            this.mnuFilePrint,
            this.mnuProcessPreview,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Name = "mnuFileClear";
			this.mnuFileClear.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuFileClear.Text = "Clear Dates";
			this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 2;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuProcessPreview
			// 
			this.mnuProcessPreview.Index = 3;
			this.mnuProcessPreview.Name = "mnuProcessPreview";
			this.mnuProcessPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessPreview.Text = "Print Preview";
			this.mnuProcessPreview.Click += new System.EventHandler(this.mnuProcessPreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdClearDates
			// 
			this.cmdClearDates.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClearDates.Location = new System.Drawing.Point(496, 29);
			this.cmdClearDates.Name = "cmdClearDates";
			this.cmdClearDates.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdClearDates.Size = new System.Drawing.Size(92, 24);
			this.cmdClearDates.TabIndex = 1;
			this.cmdClearDates.Text = "Clear Dates";
			this.cmdClearDates.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Location = new System.Drawing.Point(594, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(50, 24);
			this.cmdPrint.TabIndex = 2;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(188, 30);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.Size = new System.Drawing.Size(158, 48);
			this.cmdPrintPreview.Text = "Print / Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.mnuProcessPreview_Click);
			// 
			// frmUnemploymentReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(672, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUnemploymentReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmUnemploymentReport_Load);
			this.Activated += new System.EventHandler(this.frmUnemploymentReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUnemploymentReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateInfo)).EndInit();
			this.fraDateInfo.ResumeLayout(false);
			this.fraDateInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).EndInit();
			this.fraTownInfo.ResumeLayout(false);
			this.fraTownInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSequenceRange)).EndInit();
			this.fraSequenceRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSequenceSingleSelection)).EndInit();
			this.fraSequenceSingleSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSequenceRangeSelection)).EndInit();
			this.fraSequenceRangeSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdClearDates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrint;
		private FCButton cmdClearDates;
		private FCButton cmdPrintPreview;
	}
}
