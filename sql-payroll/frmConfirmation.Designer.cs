//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmConfirmation.
	/// </summary>
	partial class frmConfirmation
	{
		public fecherFoundation.FCGrid vsGrid;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsGrid = new fecherFoundation.FCGrid();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 352);
            this.BottomPanel.Size = new System.Drawing.Size(801, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsGrid);
            this.ClientArea.Size = new System.Drawing.Size(801, 292);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(801, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(330, 30);
            this.HeaderText.Text = "Payroll Process Confirmation";
            // 
            // vsGrid
            // 
            this.vsGrid.AllowSelection = false;
            this.vsGrid.AllowUserToResizeColumns = false;
            this.vsGrid.AllowUserToResizeRows = false;
            this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
            this.vsGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsGrid.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsGrid.ColumnHeadersHeight = 30;
            this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsGrid.DragIcon = null;
            this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.FrozenCols = 0;
            this.vsGrid.GridColor = System.Drawing.Color.Empty;
            this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.Location = new System.Drawing.Point(30, 30);
            this.vsGrid.Name = "vsGrid";
            this.vsGrid.OutlineCol = 0;
            this.vsGrid.ReadOnly = true;
            this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsGrid.RowHeightMin = 0;
            this.vsGrid.Rows = 50;
            this.vsGrid.ScrollTipText = null;
            this.vsGrid.ShowColumnVisibilityMenu = false;
            this.vsGrid.Size = new System.Drawing.Size(743, 251);
            this.vsGrid.StandardTab = true;
            this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsGrid.TabIndex = 0;
            // 
            // frmConfirmation
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(898, 688);
            this.FillColor = 0;
            this.Name = "frmConfirmation";
            this.Text = "Payroll Process Confirmation";
            this.Load += new System.EventHandler(this.frmConfirmation_Load);
            this.Resize += new System.EventHandler(this.frmConfirmation_Resize);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}