//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmLoadBack.
	/// </summary>
	partial class frmLoadBack
	{
		public Global.T2KDateBox T2KPayDate;
		public FCGrid Grid;
		public fecherFoundation.FCLabel lblEmployeeName;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblFiscalEnd;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblFiscalStart;
		public fecherFoundation.FCLabel lblFiscalYear;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuReload;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.T2KPayDate = new Global.T2KDateBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.lblEmployeeName = new fecherFoundation.FCLabel();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblFiscalEnd = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblFiscalStart = new fecherFoundation.FCLabel();
            this.lblFiscalYear = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuReload = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdReload = new fecherFoundation.FCButton();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KPayDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 499);
            this.BottomPanel.Size = new System.Drawing.Size(699, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.T2KPayDate);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.lblEmployeeName);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblFiscalEnd);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblFiscalStart);
            this.ClientArea.Controls.Add(this.lblFiscalYear);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(699, 439);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Controls.Add(this.cmdReload);
            this.TopPanel.Size = new System.Drawing.Size(699, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdReload, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(286, 30);
            this.HeaderText.Text = "Load of Back Information";
            // 
            // T2KPayDate
            // 
            this.T2KPayDate.Location = new System.Drawing.Point(194, 30);
            this.T2KPayDate.Mask = "00/00/0000";
            this.T2KPayDate.Name = "T2KPayDate";
            this.T2KPayDate.SelLength = 0;
            this.T2KPayDate.SelStart = 0;
            this.T2KPayDate.Size = new System.Drawing.Size(115, 40);
            this.T2KPayDate.TabIndex = 1;
            this.T2KPayDate.TextChanged += new System.EventHandler(this.T2KPayDate_Change);
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 9;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExtendLastCol = true;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 119);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 50;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.Size = new System.Drawing.Size(641, 302);
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 0;
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(214, 90);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(457, 15);
            this.lblEmployeeName.TabIndex = 9;
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(112, 91);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(82, 16);
            this.lblEmployeeNumber.TabIndex = 8;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 90);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(79, 15);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "EMPLOYEE";
            // 
            // lblFiscalEnd
            // 
            this.lblFiscalEnd.Location = new System.Drawing.Point(546, 44);
            this.lblFiscalEnd.Name = "lblFiscalEnd";
            this.lblFiscalEnd.Size = new System.Drawing.Size(77, 15);
            this.lblFiscalEnd.TabIndex = 6;
            this.lblFiscalEnd.Text = "99/99/2006";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(509, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(17, 15);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "TO";
            // 
            // lblFiscalStart
            // 
            this.lblFiscalStart.Location = new System.Drawing.Point(423, 44);
            this.lblFiscalStart.Name = "lblFiscalStart";
            this.lblFiscalStart.Size = new System.Drawing.Size(66, 15);
            this.lblFiscalStart.TabIndex = 4;
            this.lblFiscalStart.Text = "99/99/2006";
            // 
            // lblFiscalYear
            // 
            this.lblFiscalYear.Location = new System.Drawing.Point(327, 44);
            this.lblFiscalYear.Name = "lblFiscalYear";
            this.lblFiscalYear.Size = new System.Drawing.Size(75, 15);
            this.lblFiscalYear.TabIndex = 3;
            this.lblFiscalYear.Text = "FISCAL YEAR";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(96, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "AS OF PAY DATE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuReload,
            this.mnuSelectEmployee,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuReload
            // 
            this.mnuReload.Index = 0;
            this.mnuReload.Name = "mnuReload";
            this.mnuReload.Text = "Reload Totals";
            this.mnuReload.Click += new System.EventHandler(this.mnuReload_Click);
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = 1;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 2;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 5;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdReload
            // 
            this.cmdReload.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdReload.AppearanceKey = "toolbarButton";
            this.cmdReload.Location = new System.Drawing.Point(439, 29);
            this.cmdReload.Name = "cmdReload";
            this.cmdReload.Size = new System.Drawing.Size(104, 24);
            this.cmdReload.TabIndex = 1;
            this.cmdReload.Text = "Reload Totals";
            this.cmdReload.Click += new System.EventHandler(this.mnuReload_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.AppearanceKey = "toolbarButton";
            this.cmdSelect.Location = new System.Drawing.Point(549, 29);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelect.Size = new System.Drawing.Size(122, 24);
            this.cmdSelect.TabIndex = 2;
            this.cmdSelect.Text = "Select Employee";
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(302, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmLoadBack
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(699, 607);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmLoadBack";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Load of Back Information";
            this.Load += new System.EventHandler(this.frmLoadBack_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLoadBack_KeyDown);
            this.Resize += new System.EventHandler(this.frmLoadBack_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KPayDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSelect;
		private FCButton cmdReload;
		private FCButton cmdSave;
	}
}