﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptSimpleEmployeeDeductions.
	/// </summary>
	partial class srptSimpleEmployeeDeductions
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptSimpleEmployeeDeductions));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDedCode,
				this.txtAmount
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape3,
				this.Label21,
				this.Label22,
				this.Label23
			});
			this.ReportHeader.Height = 0.5F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalAmount,
				this.Label24,
				this.Line1
			});
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Shape3
			// 
			this.Shape3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape3.Height = 0.3F;
			this.Shape3.Left = 2.2F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.1666667F;
			this.Shape3.Width = 2.766667F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.2F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 3.233333F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 9pt; font-weight: bold";
			this.Label21.Text = "Deductions";
			this.Label21.Top = 0.1666667F;
			this.Label21.Width = 0.8F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1666667F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 2.266667F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label22.Text = "Deduction Name";
			this.Label22.Top = 0.3333333F;
			this.Label22.Width = 1.033333F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3.966667F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label23.Text = "CYTD Amount";
			this.Label23.Top = 0.3333333F;
			this.Label23.Width = 0.8666667F;
			// 
			// txtDedCode
			// 
			this.txtDedCode.Height = 0.2F;
			this.txtDedCode.Left = 2.266667F;
			this.txtDedCode.Name = "txtDedCode";
			this.txtDedCode.Style = "font-size: 8pt";
			this.txtDedCode.Text = null;
			this.txtDedCode.Top = 0F;
			this.txtDedCode.Width = 1.666667F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.2F;
			this.txtAmount.Left = 4F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.OutputFormat = resources.GetString("txtAmount.OutputFormat");
			this.txtAmount.Style = "font-size: 8pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.8333333F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.2F;
			this.txtTotalAmount.Left = 3.666667F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat");
			this.txtTotalAmount.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotalAmount.Text = null;
			this.txtTotalAmount.Top = 0F;
			this.txtTotalAmount.Width = 1.166667F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1666667F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.133333F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label24.Text = "Total";
			this.Label24.Top = 0.03333334F;
			this.Label24.Width = 0.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.1F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 1.8F;
			this.Line1.X1 = 3.1F;
			this.Line1.X2 = 4.9F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// srptSimpleEmployeeDeductions
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
