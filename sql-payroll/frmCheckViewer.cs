﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using System;
using System.IO;
using Wisej.Web;

namespace TWPY0000
{
    public partial class frmCheckViewer : BaseForm
    {
        public frmCheckViewer()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            toolBar1.ButtonClick += toolBar1_ButtonClick;


        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmCheckViewer InstancePtr
        {
            get
            {
                return (frmCheckViewer)Sys.GetInstance(typeof(frmCheckViewer));
            }
        }

        protected frmCheckViewer _InstancePtr = null;
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Corey Gray              *
        // DATE           :               10/02/2003              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               03/15/2006              *
        // ********************************************************
        bool boolShowPrintDialog;
        bool boolPrintDuplex;
        bool boolDuplexForcePairs;
        string strDuplexLabel;
        FCFileSystem fso = new FCFileSystem();
        bool boolUseFilename;
        int intCopies;
        bool pboolOverRideDuplex;
        int intFormModal;
        bool boolEmail;
        string strEmailAttachmentName;
        bool boolAutoPrintDuplex;
        bool boolCantChoosePages;
        // vbPorter upgrade warning: rptObj As object	OnWrite(rptLaserCheck1)
        public void Init(FCSectionReport rptObj = null, string strPrinter = "", int intModal = 0, bool boolDuplex = false, bool boolDuplexPairsMandatory = false, string strDuplexTitle = "Pages", bool boolLandscape = false, string strFileName = "", string strFileTitle = "TRIO Software", bool boolOverRideDuplex = false, bool boolAllowEmail = false, string strAttachmentName = "", bool boolDontAllowPagesChoice = false)
        {
            // if printing duplex and it is a normal type report, boolDuplexForcePairs should be false
            // if printing duplex and Front and Back pages must be paired the way they are in the report (such as property cards) set boolDuplexForcePairs = true
            // set the duplex title if pages is not a good description for what is printing (such as property cards)
            // strAttachment name is the name to call the attachment.  It will be created with a meaningless temp name.  This will be the filename the email receiver will see
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                boolCantChoosePages = boolDontAllowPagesChoice;
                boolAutoPrintDuplex = false;
                strEmailAttachmentName = strAttachmentName;
                intCopies = 1;
                boolEmail = boolAllowEmail;
                boolUseFilename = false;
                if (strFileName != "")
                {
                    // make sure that the file exists
                    if (FCFileSystem.FileExists(strFileName))
                    {
                        ARViewer21.ReportSource.Document.Load(strFileName);
                        boolUseFilename = true;
                        Text = strFileTitle;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    // load the report from the object passed in
                    Text = rptObj.Name;
                    ARViewer21.ReportSource = rptObj;
                }
                //ARViewer21.Zoom = -1;
                boolPrintDuplex = boolDuplex;
                pboolOverRideDuplex = boolOverRideDuplex;
                boolDuplexForcePairs = boolDuplexPairsMandatory;
                strDuplexLabel = strDuplexTitle;

                intFormModal = intModal;
                switch (intModal)
                {
                    case 0:
                        {
                            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
                            //this.Show(FCForm.FormShowEnum.Modeless);
                            Show(App.MainForm);
                            break;
                        }
                    case 1:
                        {
                            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
                            //this.Show(FCForm.FormShowEnum.Modal);
                            Show(App.MainForm);
                            break;
                        }
                }
                //end switch
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ARViewer21_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = 0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        private void frmCheckViewer_Activated(object sender, System.EventArgs e)
        {
            if (WindowState != FormWindowState.Maximized && WindowState != FormWindowState.Minimized)
            {
                HeightOriginal = HeightOriginal - 15;
            }

            toolBar1.Focus();
        }

        private void frmCheckViewer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        private void frmCheckViewer_Load(object sender, System.EventArgs e)
        {

            int cnt;
            int lngID = 0;
            WindowState = FormWindowState.Maximized;

            mnuEmail.Visible = boolEmail;
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                if (!boolUseFilename)
                {
                    ARViewer21.ReportSource.Unload();
                }

                MessageBox.Show("Remove checks from printer before continuing.", "TRIO Software", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Report Viewer Unload", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuEmailPDF_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                FCFileSystem fso = new FCFileSystem();
                string strList = "";
                string fileName = System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, Path.GetTempFileName());
                a.Export(ARViewer21.ReportSource.Document, fileName);
                if (!FCFileSystem.FileExists(fileName))
                {
                    MessageBox.Show("Could not create report file to send as attachment", "Cannot E-Mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    if (fecherFoundation.Strings.Trim(strEmailAttachmentName) != string.Empty)
                    {
                        strList = fileName + ";" + strEmailAttachmentName + ".pdf";
                    }
                    else
                    {
                        strList = fileName;
                    }
                    frmEMail.InstancePtr.Init(strList, boolDeleteOriginalAttachments: true, showAsModalForm: true);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuEmailPDF_Click");
            }
        }

        private void mnuEMailRTF_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                FCFileSystem fso = new FCFileSystem();
                string strList = "";
                string fileName = System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, Path.GetTempFileName());
                a.Export(ARViewer21.ReportSource.Document, fileName);
                if (!FCFileSystem.FileExists(fileName))
                {
                    MessageBox.Show("Could not create report file to send as attachment", "Cannot E-Mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    if (fecherFoundation.Strings.Trim(strEmailAttachmentName) != string.Empty)
                    {
                        strList = fileName + ";" + strEmailAttachmentName + ".rtf";
                    }
                    else
                    {
                        strList = fileName;
                    }
                    frmEMail.InstancePtr.Init(strList, boolDeleteOriginalAttachments: true, showAsModalForm: true);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuEmailRTF_Click");
            }
        }

        private void mnuExcel_Click(object sender, System.EventArgs e)
        {
            try
            {
                string strOldDir;
                string strFileName = "";
                string strFullName = "";
                string strPathName = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }

                GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                //string fileName = ARViewer21.ReportSource.Name + ".xls";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".xls";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".xls";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    // FC:FINAL: RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Excel export.", MsgBoxStyle.Critical, "Error");
            }
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuHTML_Click(object sender, System.EventArgs e)
        {
            try
            {
                string strFileName = "";
                DialogResult intResp = 0;
                string strOldDir;
                string strFullName = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }

                GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name);
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text);
                }
                //FC:FINAL:RPU:#1363 - Use the report variable
                //if (ARViewer21.ReportSource.Document.Pages.Count > 1)
                if (report.Document.Pages.Count > 1)
                {
                    DialogResult res = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
                    if (res == DialogResult.No)
                    {
                        a.MultiPage = true;
                    }
                }
                if (a.MultiPage)
                {
                    string zipFilePath = GetTempFileName(".zip");
                    //FC:FINAL:RPU:#1363 - Concatenate just extension
                    //string fileName = ARViewer21.ReportSource.Name + ".zip";
                    fileName += ".zip";
                    var zip = fecherFoundation.ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
                    //FC:FINAL:RPU:#1363 - Use the report variable
                    //for (int i = 0; i < ARViewer21.ReportSource.Document.Pages.Count; i++)
                    for (int i = 0; i < report.Document.Pages.Count; i++)
                    {
                        string currentPage = (i + 1).ToString();
                        //string htmlFileName = ARViewer21.ReportSource.Name + "_Page" + currentPage + ".html";
                        string htmlFileName = report.Name + "_Page" + currentPage + ".html";
                        string tempFilePath = GetTempFileName(".html");
                        //a.Export(ARViewer21.ReportSource.Document, tempFilePath, currentPage);
                        a.Export(report.Document, tempFilePath, currentPage);
                        zip.CreateEntryFromFile(tempFilePath, htmlFileName, System.IO.Compression.CompressionLevel.Optimal);
                    }
                    zip.Dispose();
                    //FC:FINAL:DSE use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(zipFilePath, fileName);
                    FCUtils.DownloadAndOpen("_blank", zipFilePath, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                else
                {
                    //FC:FINAL:RPU:#1363 - Concatenate just extension
                    //string fileName = ARViewer21.ReportSource.Name + ".html";
                    fileName += ".html";
                    string tempFilePath = GetTempFileName(".html");
                    //FC:FINAL:RPU:#1363 - Use the report variable
                    //a.Export(ARViewer21.ReportSource.Document, tempFilePath);
                    a.Export(report.Document, tempFilePath);
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", tempFilePath, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In HTML export", MsgBoxStyle.Critical, "Error");
            }
        }

        private string GetTempFileName(string extension)
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            //string tempFolder = Path.Combine(Path.GetTempPath(), "Wisej", applicationName, "Temp", "PDF");
            string tempFolder = Path.Combine(Application.StartupPath, "Temp", "ReportViewer");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }

        private void mnuPrint_Click(object sender, System.EventArgs e)
        {
            PrintNegotiable();
        }
        private void PrintNegotiable()
        {
            int intStartPage;
            int intEndPage;
            string strDuplex = "";
            int intStart;
            int intEnd;
            string strPrinter;
            string strOldPrinter = "";
            // vbPorter upgrade warning: intOrientation As int	OnWrite(DDActiveReportsViewer2.PrtOrientation)
            int intOrientation;
            int x;
            if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
            {
                MessageBox.Show("Report not done", "Report Loading", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            intOrientation = 1;
            intStart = 1;
            intEnd = -1;
            for (x = 0; x <= ARViewer21.ReportSource.Document.Pages.Count - 1; x++)
            {
                //FC:FINAL:AM:#2728 - Tag not supported; use list instead
                //if (fecherFoundation.Strings.UCase(ARViewer21.ReportSource.Document.Pages[x].Tag) == "INVALID CHECK")
                if (!rptLaserCheck1.InstancePtr.invalidCheckPages.Contains(ARViewer21.ReportSource.Document.Pages[x])) 
                    continue;

                intEnd = x;
                break;
            } // x
            
            switch (intEnd)
            {
                case 0:
                    MessageBox.Show("There are no negotiable checks to print", "No Negotiable Checks", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                case -1:
                    intEnd = ARViewer21.ReportSource.Document.Pages.Count;

                    break;
            }

            intStartPage = 1;
            intEndPage = intEnd;
            if (!boolCantChoosePages)
            {
                if (!frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
                {
                    return;
                }
            }
            // End If
            //strPrinter = ARViewer21.Printer.PrinterName;

            FCSectionReport report = new FCSectionReport();
            report.Name = "Checks";
            intStartPage--;

            for (int i = intStartPage; i < intEndPage; i++)
            {
                GrapeCity.ActiveReports.Document.Section.Page pg = (GrapeCity.ActiveReports.Document.Section.Page)ARViewer21.ReportSource.Document.Pages[i].Clone();
                report.Document.Pages.Add(pg);
            }

            report.ExportToPDFAndPrint();

        }

        public void mnuPrint_Click()
        {
            mnuPrint_Click(mnuPrint, new System.EventArgs());
        }

        private void mnuPrintNonNegotiable_Click(object sender, System.EventArgs e)
        {
            PrintNonNegotiable();
        }

        private void PrintNonNegotiable()
        {
            // vbPorter upgrade warning: intOrientation As int	OnWrite(DDActiveReportsViewer2.PrtOrientation)
            int intOrientation;
            int intStart;
            int intEnd;
            int x;
            int intStartPage;
            int intEndPage;
            string strPrinter;
            // Print the range of non-negotiable checks
            if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
            {
                MessageBox.Show("Report not done", "Report Loading", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            intOrientation = 1;
            intStart = 0;
            for (x = 0; x <= ARViewer21.ReportSource.Document.Pages.Count - 1; x++)
            {
                //FC:FINAL:AM:#2728 - Tag not supported; use list instead
                //if (fecherFoundation.Strings.UCase(ARViewer21.ReportSource.Document.Pages[x].Tag) == "INVALID CHECK")
                if (rptLaserCheck1.InstancePtr.invalidCheckPages.Contains(ARViewer21.ReportSource.Document.Pages[x]))
                {
                    intStart = x + 1;
                    break;
                }
            } // x
            if (intStart < 1)
            {
                MessageBox.Show("There are no non-negotiable checks to print", "No Non-Negotiable Checks", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            intStartPage = intStart;
            intEndPage = ARViewer21.ReportSource.Document.Pages.Count;
            if (!boolCantChoosePages)
            {
                if (!frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
                {
                    return;
                }
            }
            //strPrinter = ARViewer21.Printer.PrinterName;

            FCSectionReport report = new FCSectionReport();
            report.Name = "Checks";
            intStartPage--;
            for (int i = intStartPage; i < intEndPage; i++)
            {
                GrapeCity.ActiveReports.Document.Section.Page pg = (GrapeCity.ActiveReports.Document.Section.Page)ARViewer21.ReportSource.Document.Pages[i].Clone();
                report.Document.Pages.Add(pg);
            }
            //         if (intOrientation >= 0)
            //{
            //	FCGlobal.Printer.Orientation = FCConvert.ToInt16(intOrientation);
            //}
            //ARViewer21.Printer.Copies = 1;
            report.ExportToPDFAndPrint();
        }

        private void mnuRTF_Click(object sender, System.EventArgs e)
        {
            try
            {
                string strOldDir;
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                strOldDir = Application.StartupPath;
                Information.Err().Clear();

                GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                //string fileName = ARViewer21.ReportSource.Name + ".rtf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".rtf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In RTF export", MsgBoxStyle.Critical, "Error");
            }
        }

        private void mnuExportPDF_Click(object sender, System.EventArgs e)
        {
            try
            {
                string strOldDir;
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }

                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                //string fileName = ARViewer21.ReportSource.Name + ".pdf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                //a.Export(ARViewer21.ReportSource.Document, Path.Combine(Application.StartupPath,fileName));
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In PDF export", MsgBoxStyle.Critical, "Error");
            }
        }

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            var buttonClicked = e.Button;

            if (buttonClicked == toolBarButtonEmailPDF)
            {
                mnuEmailPDF_Click(buttonClicked, EventArgs.Empty);
            }
            else
            {
                if (buttonClicked == toolBarButtonEmailRTF)
                {
                    mnuEMailRTF_Click(buttonClicked, EventArgs.Empty);
                }
                else
                {
                    if (buttonClicked == toolBarButtonExportPDF)
                    {
                        mnuExportPDF_Click(buttonClicked, EventArgs.Empty);
                    }
                    else
                    {
                        if (buttonClicked == toolBarButtonExportRTF)
                        {
                            mnuRTF_Click(buttonClicked, EventArgs.Empty);
                        }
                        else
                        {
                            if (buttonClicked == toolBarButtonExportHTML)
                            {
                                mnuHTML_Click(buttonClicked, EventArgs.Empty);
                            }
                            else
                            {
                                if (buttonClicked == toolBarButtonExportExcel)
                                {
                                    mnuExcel_Click(buttonClicked, EventArgs.Empty);
                                }
                                else
                                {
                                    if (buttonClicked == toolBarButtonPrint)
                                    {
                                        PrintNegotiable();
                                    }
                                    else
                                    {
                                        if (buttonClicked == toolBarButtonPrintNonNegotiable)
                                        {
                                            PrintNonNegotiable();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
