//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBankInfoLabels.
	/// </summary>
	public partial class rptBankInfoLabels : BaseSectionReport
	{
		public rptBankInfoLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Bank Label Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBankInfoLabels InstancePtr
		{
			get
			{
				return (rptBankInfoLabels)Sys.GetInstance(typeof(rptBankInfoLabels));
			}
		}

		protected rptBankInfoLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBank?.Dispose();
                rsBank = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBankInfoLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       July 02,2001
		//
		// **************************************************
		// private local variables
		int intCounter;
		clsDRWrapper rsBank;
		string strSortField = "";
		string strCity;
		// vbPorter upgrade warning: ControlName As object	OnWrite(string)
		private void PrintLabel(GrapeCity.ActiveReports.SectionReportModel.TextBox ControlName)
		{
			ControlName.Text = fecherFoundation.Strings.Trim(rsBank.Get_Fields_String("Name") + " ") + "  " + Strings.Format(rsBank.Get_Fields("ID"), "0000") + "\r\n";
			ControlName.Text = FCConvert.ToString(ControlName.Text) + fecherFoundation.Strings.Trim(rsBank.Get_Fields_String("Address1") + " ") + "\r\n";
			if (fecherFoundation.Strings.Trim(rsBank.Get_Fields_String("Address2") + " ") != string.Empty)
			{
				ControlName.Text = FCConvert.ToString(ControlName.Text) + rsBank.Get_Fields_String("Address2") + "\r\n";
			}
			if (FCConvert.ToString(rsBank.Get_Fields("Address3")) != string.Empty)
			{
				ControlName.Text = FCConvert.ToString(ControlName.Text) + rsBank.Get_Fields("Address3") + "\r\n";
			}
			strCity = rsBank.Get_Fields_String("City") + ", " + rsBank.Get_Fields("State") + " " + rsBank.Get_Fields("Zip");
			if (fecherFoundation.Strings.Trim(strCity) == "")
			{
				ControlName.Text = FCConvert.ToString(ControlName.Text) + fecherFoundation.Strings.Trim(strCity);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsBank.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				txtLabel1.Text = string.Empty;
				txtLabel2.Text = string.Empty;
				txtLabel3.Text = string.Empty;
				PrintLabel(txtLabel1);
				if (!rsBank.EndOfFile())
					rsBank.MoveNext();
				if (frmPrintBankSetup.InstancePtr.cmbStyle.Text == "2 Across")
				{
					if (rsBank.EndOfFile())
						goto PrintLabel;
					PrintLabel(txtLabel3);
					if (!rsBank.EndOfFile())
						rsBank.MoveNext();
				}
				else if (frmPrintBankSetup.InstancePtr.cmbStyle.Text == "3 Across")
				{
					if (rsBank.EndOfFile())
						goto PrintLabel;
					PrintLabel(txtLabel2);
					if (!rsBank.EndOfFile())
						rsBank.MoveNext();
				}
				if (frmPrintBankSetup.InstancePtr.cmbStyle.Text == "3 Across")
				{
					if (rsBank.EndOfFile())
						goto PrintLabel;
					PrintLabel(txtLabel3);
					if (!rsBank.EndOfFile())
						rsBank.MoveNext();
				}
				PrintLabel:
				;
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "BankLabels");
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Vacation Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsBank = new clsDRWrapper();
				if (modGlobalVariables.Statics.gboolSortBankName)
				{
					strSortField = "Name";
				}
				else
				{
					strSortField = "RecordNumber";
				}
				if (modGlobalVariables.Statics.gstrBankStart != string.Empty)
				{
					if (modGlobalVariables.Statics.gboolSortBankName)
					{
						rsBank.OpenRecordset("Select * from tblBanks where Name >= '" + modGlobalVariables.Statics.gstrBankStart + "' and Name <= '" + modGlobalVariables.Statics.gstrBankEnd + "' order by " + strSortField, "TWPY0000.vb1");
					}
					else
					{
						rsBank.OpenRecordset("Select * from tblBanks where ID >= " + Strings.Format(FCConvert.ToInt16(FCConvert.ToDouble(modGlobalVariables.Statics.gstrBankStart)), "0000") + " and ID <= " + Strings.Format(modGlobalVariables.Statics.gstrBankEnd, "0000") + " order by " + strSortField, "TWPY0000.vb1");
					}
				}
				else
				{
					rsBank.OpenRecordset("Select * from tblBanks order by " + strSortField, "TWPY0000.vb1");
				}
				intCounter = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		
	}
}