﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptContract.
	/// </summary>
	partial class rptContract
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptContract));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtContract = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemaining = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContract)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemaining)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNumber,
				this.txtName,
				this.txtContract,
				this.txtDescription,
				this.txtStart,
				this.txtEnd,
				this.txtAmount,
				this.txtPaid,
				this.txtRemaining,
				this.SubReport1
			});
			this.Detail.Height = 0.25F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9
			});
			this.PageHeader.Height = 0.7604167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 3.28125F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "color: rgb(0,0,0); font-family: \'Tahoma\'; font-size" + ": 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Contract Setup";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.395833F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 3.208333F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 8.5F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 8.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 8.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.Label1.Text = "Paid";
			this.Label1.Top = 0.5625F;
			this.Label1.Width = 0.7395833F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 9.208333F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.Label2.Text = "Remaining";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 0.7395833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 7.666667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.Label3.Text = "Amount";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 0.7395833F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.895833F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.Label4.Text = "End";
			this.Label4.Top = 0.5625F;
			this.Label4.Width = 0.7395833F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.072917F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.Label5.Text = "Start";
			this.Label5.Top = 0.5625F;
			this.Label5.Width = 0.7395833F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 9pt; font-weight: bold";
			this.Label6.Text = "EMP";
			this.Label6.Top = 0.5625F;
			this.Label6.Width = 0.4270833F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.53125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 9pt; font-weight: bold";
			this.Label7.Text = "Name";
			this.Label7.Top = 0.5625F;
			this.Label7.Width = 2.489583F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.135417F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 9pt; font-weight: bold; text-align: right";
			this.Label8.Text = "#";
			this.Label8.Top = 0.5625F;
			this.Label8.Width = 0.4270833F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.614583F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 9pt; font-weight: bold";
			this.Label9.Text = "Description";
			this.Label9.Top = 0.5625F;
			this.Label9.Width = 2.364583F;
			// 
			// txtEmployeeNumber
			// 
			this.txtEmployeeNumber.Height = 0.1666667F;
			this.txtEmployeeNumber.Left = 0F;
			this.txtEmployeeNumber.Name = "txtEmployeeNumber";
			this.txtEmployeeNumber.Style = "font-size: 9pt";
			this.txtEmployeeNumber.Text = null;
			this.txtEmployeeNumber.Top = 0.07291666F;
			this.txtEmployeeNumber.Width = 0.4791667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.53125F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 9pt";
			this.txtName.Text = null;
			this.txtName.Top = 0.07291666F;
			this.txtName.Width = 2.5625F;
			// 
			// txtContract
			// 
			this.txtContract.Height = 0.1666667F;
			this.txtContract.Left = 3.104167F;
			this.txtContract.Name = "txtContract";
			this.txtContract.Style = "font-size: 9pt; text-align: right";
			this.txtContract.Text = null;
			this.txtContract.Top = 0.07291666F;
			this.txtContract.Width = 0.4583333F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1666667F;
			this.txtDescription.Left = 3.614583F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-size: 9pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.07291666F;
			this.txtDescription.Width = 2.427083F;
			// 
			// txtStart
			// 
			this.txtStart.Height = 0.1666667F;
			this.txtStart.Left = 6.072917F;
			this.txtStart.Name = "txtStart";
			this.txtStart.Style = "font-size: 9pt; text-align: right";
			this.txtStart.Text = null;
			this.txtStart.Top = 0.07291666F;
			this.txtStart.Width = 0.7395833F;
			// 
			// txtEnd
			// 
			this.txtEnd.Height = 0.1666667F;
			this.txtEnd.Left = 6.875F;
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Style = "font-size: 9pt; text-align: right";
			this.txtEnd.Text = null;
			this.txtEnd.Top = 0.07291666F;
			this.txtEnd.Width = 0.7604167F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1666667F;
			this.txtAmount.Left = 7.666667F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-size: 9pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0.07291666F;
			this.txtAmount.Width = 0.7395833F;
			// 
			// txtPaid
			// 
			this.txtPaid.Height = 0.1666667F;
			this.txtPaid.Left = 8.4375F;
			this.txtPaid.Name = "txtPaid";
			this.txtPaid.Style = "font-size: 9pt; text-align: right";
			this.txtPaid.Text = null;
			this.txtPaid.Top = 0.07291666F;
			this.txtPaid.Width = 0.7395833F;
			// 
			// txtRemaining
			// 
			this.txtRemaining.Height = 0.1666667F;
			this.txtRemaining.Left = 9.208333F;
			this.txtRemaining.Name = "txtRemaining";
			this.txtRemaining.Style = "font-size: 9pt; text-align: right";
			this.txtRemaining.Text = null;
			this.txtRemaining.Top = 0.07291666F;
			this.txtRemaining.Width = 0.7395833F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.04166667F;
			this.SubReport1.Left = 6.0625F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.2604167F;
			this.SubReport1.Width = 3.875F;
			// 
			// rptContract
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContract)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemaining)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContract;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemaining;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
