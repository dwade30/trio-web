﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cUC1BarCodeInfo
	{
		//=========================================================
		// vbPorter upgrade warning: strVendorCode As int	OnWrite(string)
		private int strVendorCode;
		private string strSoftwareVersion = string.Empty;
		// vbPorter upgrade warning: strMaineVendorCode As int	OnWrite(string)
		private int strMaineVendorCode;
		private string strStateCode = string.Empty;
		private string strYear = string.Empty;
		private string strFormCode = string.Empty;
		private string strFormVersionNumber = string.Empty;
		private DateTime dtStart;
		private DateTime dtEnd;
		private string strAccountID = string.Empty;
		private string strFedId = string.Empty;
		private string strName = string.Empty;
		private int intEmployeeCountMonth1;
		private int intEmployeeCountMonth2;
		private int intEmployeeCountMonth3;
		private int intFemaleCountMonth1;
		private int intFemaleCountMonth2;
		private int intFemaleCountMonth3;
		private double dblUCWages;
		private double dblExcessWages;
		private double dblTaxableWages;
		private double dblUCDue;
		private double dblCSSFAssess;
        private double dblUPAFAssess;
		private double dblTotalContCSSFUPAFDue;
		private string strPreparerEIN = string.Empty;
		private string strPayrollProcessor = string.Empty;
		// vbPorter upgrade warning: strCode As string	OnRead
		public string VendorCode
		{
			set
			{
				strVendorCode = FCConvert.ToInt32(value);
			}
			// vbPorter upgrade warning: 'Return' As string	OnWrite
			get
			{
				string VendorCode = "";
				VendorCode = FCConvert.ToString(strVendorCode);
				return VendorCode;
			}
		}

		public string SoftwareVersion
		{
			set
			{
				strSoftwareVersion = value;
			}
			get
			{
				string SoftwareVersion = "";
				SoftwareVersion = strSoftwareVersion;
				return SoftwareVersion;
			}
		}
		// vbPorter upgrade warning: strCode As string	OnRead
		public string MaineVendorCode
		{
			set
			{
				strMaineVendorCode = FCConvert.ToInt32(value);
			}
			// vbPorter upgrade warning: 'Return' As string	OnWrite
			get
			{
				string MaineVendorCode = "";
				MaineVendorCode = FCConvert.ToString(strMaineVendorCode);
				return MaineVendorCode;
			}
		}

		public string StateCode
		{
			set
			{
				strStateCode = value;
			}
			get
			{
				string StateCode = "";
				StateCode = strStateCode;
				return StateCode;
			}
		}

		public string ReportYear
		{
			set
			{
				strYear = value;
			}
			get
			{
				string ReportYear = "";
				ReportYear = strYear;
				return ReportYear;
			}
		}

		public string FormCode
		{
			set
			{
				strFormCode = value;
			}
			get
			{
				string FormCode = "";
				FormCode = strFormCode;
				return FormCode;
			}
		}

		public string FormVersionNumber
		{
			set
			{
				strFormVersionNumber = value;
			}
			get
			{
				string FormVersionNumber = "";
				FormVersionNumber = strFormVersionNumber;
				return FormVersionNumber;
			}
		}

		public DateTime StartDate
		{
			set
			{
				dtStart = value;
			}
			get
			{
				DateTime StartDate = System.DateTime.Now;
				StartDate = dtStart;
				return StartDate;
			}
		}

		public DateTime EndDate
		{
			set
			{
				dtEnd = value;
			}
			get
			{
				DateTime EndDate = System.DateTime.Now;
				EndDate = dtEnd;
				return EndDate;
			}
		}

		public string UCAccountID
		{
			set
			{
				strAccountID = value;
			}
			get
			{
				string UCAccountID = "";
				UCAccountID = strAccountID;
				return UCAccountID;
			}
		}

		public string FederalID
		{
			set
			{
				strFedId = value;
			}
			get
			{
				string FederalID = "";
				FederalID = strFedId;
				return FederalID;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public int EmployeeCountMonth1
		{
			set
			{
				intEmployeeCountMonth1 = value;
			}
			get
			{
				int EmployeeCountMonth1 = 0;
				EmployeeCountMonth1 = intEmployeeCountMonth1;
				return EmployeeCountMonth1;
			}
		}

		public int EmployeeCountMonth2
		{
			set
			{
				intEmployeeCountMonth2 = value;
			}
			get
			{
				int EmployeeCountMonth2 = 0;
				EmployeeCountMonth2 = intEmployeeCountMonth2;
				return EmployeeCountMonth2;
			}
		}

		public int EmployeeCountMonth3
		{
			set
			{
				intEmployeeCountMonth3 = value;
			}
			get
			{
				int EmployeeCountMonth3 = 0;
				EmployeeCountMonth3 = intEmployeeCountMonth3;
				return EmployeeCountMonth3;
			}
		}

		public int FemaleCountMonth1
		{
			set
			{
				intFemaleCountMonth1 = value;
			}
			get
			{
				int FemaleCountMonth1 = 0;
				FemaleCountMonth1 = intFemaleCountMonth1;
				return FemaleCountMonth1;
			}
		}

		public int FemaleCountMonth2
		{
			set
			{
				intFemaleCountMonth2 = value;
			}
			get
			{
				int FemaleCountMonth2 = 0;
				FemaleCountMonth2 = intFemaleCountMonth2;
				return FemaleCountMonth2;
			}
		}

		public int FemaleCountMonth3
		{
			set
			{
				intFemaleCountMonth3 = value;
			}
			get
			{
				int FemaleCountMonth3 = 0;
				FemaleCountMonth3 = intFemaleCountMonth3;
				return FemaleCountMonth3;
			}
		}

		public double UCWages
		{
			set
			{
				dblUCWages = value;
			}
			get
			{
				double UCWages = 0;
				UCWages = dblUCWages;
				return UCWages;
			}
		}

		public double ExcessWages
		{
			set
			{
				dblExcessWages = value;
			}
			get
			{
				double ExcessWages = 0;
				ExcessWages = dblExcessWages;
				return ExcessWages;
			}
		}

		public double TaxableWages
		{
			set
			{
				dblTaxableWages = value;
			}
			get
			{
				double TaxableWages = 0;
				TaxableWages = dblTaxableWages;
				return TaxableWages;
			}
		}

		public double UCDue
		{
			set
			{
				dblUCDue = value;
			}
			get
			{
				double UCDue = 0;
				UCDue = dblUCDue;
				return UCDue;
			}
		}

		public double CSSFAssess
		{
			set
			{
				dblCSSFAssess = value;
			}
			get
			{
				double CSSFAssess = 0;
				CSSFAssess = dblCSSFAssess;
				return CSSFAssess;
			}
		}

        public double UPAFAssess
        {
            set
            {
                dblUPAFAssess = value;
            }
            get
            {
                double UPAFAssess = 0;
                UPAFAssess = dblUPAFAssess;
                return UPAFAssess;
            }
        }


		public double TotalContCSSFUPAFDue
		{
			set
			{
				dblTotalContCSSFUPAFDue = value;
			}
			get
			{
				double TotalContCSSFUPAFDue = 0;
                TotalContCSSFUPAFDue = dblTotalContCSSFUPAFDue;
				return TotalContCSSFUPAFDue;
			}
		}

		public string PreparerEIN
		{
			set
			{
				strPreparerEIN = value;
			}
			get
			{
				string PreparerEIN = "";
				PreparerEIN = strPreparerEIN;
				return PreparerEIN;
			}
		}

		public string PayrollProcessor
		{
			set
			{
				strPayrollProcessor = value;
			}
			get
			{
				string PayrollProcessor = "";
				PayrollProcessor = strPayrollProcessor;
				return PayrollProcessor;
			}
		}

		public string Get2DBarCodeString()
		{
			string Get2DBarCodeString = "";
			string strReturn;
			string strTemp;
			strReturn = "T1" + "\r";
			strReturn += FCConvert.ToString(strVendorCode) + "\r";
			strReturn += strSoftwareVersion + "\r";
			strReturn += FCConvert.ToString(strMaineVendorCode) + "\r";
			strReturn += strStateCode + "\r";
			strReturn += strYear + "\r";
			strReturn += strFormCode + "\r";
			strReturn += strFormVersionNumber + "\r";
			strTemp = Strings.Right("00" + FCConvert.ToString(dtStart.Month), 2) + Strings.Right("00" + FCConvert.ToString(dtStart.Day), 2) + FCConvert.ToString(dtStart.Year);
			strReturn += strTemp + "\r";
			strTemp = Strings.Right("00" + FCConvert.ToString(dtEnd.Month), 2) + Strings.Right("00" + FCConvert.ToString(dtEnd.Day), 2) + FCConvert.ToString(dtEnd.Year);
			strReturn += strTemp + "\r";
			strReturn += strAccountID.Replace(" ", "").Replace("-", "") + "\r";
			strReturn += strFedId.Replace(" ", "").Replace("-", "") + "\r";
			strReturn += AddString(strName, 35);
			strReturn += AddNumLine(intEmployeeCountMonth1);
			strReturn += AddNumLine(intEmployeeCountMonth2);
			strReturn += AddNumLine(intEmployeeCountMonth3);
			strReturn += AddNumLine(intFemaleCountMonth1);
			strReturn += AddNumLine(intFemaleCountMonth2);
			strReturn += AddNumLine(intFemaleCountMonth3);
			strReturn += AddDouble(dblUCWages);
			strReturn += AddDouble(dblExcessWages);
			strReturn += AddDouble(dblTaxableWages);
			strReturn += AddDouble(dblUCDue);
			strReturn += AddDouble(dblCSSFAssess);
            strReturn += AddDouble(dblUPAFAssess);
			strReturn += AddDouble(dblTotalContCSSFUPAFDue);
			strReturn += AddString(strPreparerEIN);
			strReturn += AddString(strPayrollProcessor);
			strReturn += "*EOD*";
			Get2DBarCodeString = strReturn;
			return Get2DBarCodeString;
		}
		// vbPorter upgrade warning: intValue As object	OnWrite
		private string AddNumLine(object intValue)
		{
			string AddNumLine = "";
			if (FCConvert.ToInt32(intValue) != 0)
			{
				AddNumLine = intValue + "\r";
			}
			else
			{
				AddNumLine = "\r";
			}
			return AddNumLine;
		}

		private string AddString(string strValue, int maxChars = 0)
		{
			string AddString = "";
			if (fecherFoundation.Strings.Trim(strValue) != "")
			{
				if (maxChars == 0 || strValue.Length <= maxChars)
				{
					AddString = strValue + "\r";
				}
				else
				{
					AddString = Strings.Left(strValue, maxChars);
				}
			}
			else
			{
				AddString = "\r";
			}
			return AddString;
		}

		private string AddDouble(double dblValue)
		{
			string AddDouble = "";
			if (dblValue != 0)
			{
				AddDouble = Strings.Format(dblValue, "0.00") + "\r";
			}
			else
			{
				AddDouble = "\r";
			}
			return AddDouble;
		}

		public cUC1BarCodeInfo() : base()
		{
			strFormVersionNumber = "01";
			strStateCode = "ME";
			strVendorCode = FCConvert.ToInt32("1442");
			strFormCode = "MEUC1";
			strMaineVendorCode = FCConvert.ToInt32("15");
		}
	}
}
