//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptWarrant.
	/// </summary>
	public partial class rptWarrant : BaseSectionReport
	{
		public rptWarrant()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Payroll Warrant";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptWarrant InstancePtr
		{
			get
			{
				return (rptWarrant)Sys.GetInstance(typeof(rptWarrant));
			}
		}

		protected rptWarrant _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsCheckInfo?.Dispose();
				rsTACheckInfo?.Dispose();
				rsDDCheckInfo?.Dispose();
				rsAP?.Dispose();
				rsTotalTA?.Dispose();
                rsTACheckInfo = null;
                rsCheckInfo = null;
                rsDDCheckInfo = null;
                rsAP = null;
                rsTotalTA = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrant	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		clsDRWrapper rsTACheckInfo = new clsDRWrapper();
		clsDRWrapper rsDDCheckInfo = new clsDRWrapper();
		clsDRWrapper rsAP = new clsDRWrapper();
		clsDRWrapper rsTotalTA = new clsDRWrapper();
		bool blnFirstRecord;
		Decimal curTotalDDAmount;
		Decimal curTotalCheckAmount;
		int intTotalRegular;
		string strCheckType = "";
		bool blnACH;
		// vbPorter upgrade warning: dblIntoAP As double	OnWrite(string)
		double dblIntoAP;
		// vbPorter upgrade warning: dblFromAP As double	OnWrite(string)
		double dblFromAP;
		double dblTotalGrossPay;

		public void Init(string strPrinterName, bool modalDialog)
		{ 
			frmReportViewer.InstancePtr.Init(this, strPrinterName, FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
                CheckRecord(eArgs);
			}
			else
			{
				if (strCheckType == "C")
				{
					rsCheckInfo.MoveNext();
					if (rsCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "D";
						CheckRecord(eArgs);
					}
				}
				else if (strCheckType == "D")
				{
					rsDDCheckInfo.MoveNext();
					if (rsDDCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "T";
						CheckRecord(eArgs);
					}
				}
				else
				{
					rsTACheckInfo.MoveNext();
					if (rsTACheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

        private void CheckRecord(FetchEventArgs eArgs)
        {
            if (strCheckType == "C")
            {
                if (rsCheckInfo.EndOfFile() != true)
                {
                    eArgs.EOF = false;
                }
                else
                {
                    strCheckType = "D";
                    CheckRecord(eArgs);
                }
            }
            else if (strCheckType == "D")
            {
                if (rsDDCheckInfo.EndOfFile() != true)
                {
                    eArgs.EOF = false;
                }
                else
                {
                    strCheckType = "T";
                    CheckRecord(eArgs);
                }
            }
            else
            {
                if (rsTACheckInfo.EndOfFile() != true)
                {
                    eArgs.EOF = false;
                }
                else
                {
                    eArgs.EOF = true;
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			int lngWarrant = 0;
            using (clsDRWrapper rsACHInfo = new clsDRWrapper())
            {
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                PageCounter = 0;
                Label2.Text = modGlobalConstants.Statics.MuniName;
                Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
                strCheckType = "C";
                rsCheckInfo.OpenRecordset(
                    "SELECT * FROM tblCheckDetail WHERE checkvoid = 0 and TotalRecord = 1 AND PayDate = '" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") + " ORDER BY CheckNumber");
                rsTACheckInfo.OpenRecordset(
                    "SELECT SUM(TrustAmount) as TotalTAAmount, WarrantNumber, CheckNumber, PayDate, TrustRecipientID FROM tblCheckDetail WHERE checkvoid = 0 and TrustRecord = 1 AND PayDate = '" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") +
                    " GROUP BY WarrantNumber, CheckNumber, TrustRecipientID, PayDate ORDER BY CheckNumber");
                rsACHInfo.OpenRecordset("SELECT * FROM tblDefaultInformation");
                if (FCConvert.ToBoolean(rsACHInfo.Get_Fields_Boolean("ACHClearingHouse")))
                {
                    blnACH = true;
                    rsDDCheckInfo.OpenRecordset(
                        "SELECT SUM(DDAmount) as TotalDDAmount, WarrantNumber, CheckNumber, PayDate FROM tblCheckDetail WHERE checkvoid = 0 and BankRecord = 1 AND PayDate = '" +
                        frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " +
                        frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") +
                        " GROUP BY WarrantNumber, CheckNumber, PayDate ORDER BY CheckNumber");
                }
                else
                {
                    blnACH = false;
                    rsDDCheckInfo.OpenRecordset(
                        "SELECT SUM(DDAmount) as TotalDDAmount, WarrantNumber, CheckNumber, PayDate, DDBankNumber FROM tblCheckDetail WHERE checkvoid = 0 and BankRecord = 1 AND PayDate = '" +
                        frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " +
                        frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") +
                        " GROUP BY WarrantNumber, CheckNumber, DDBankNumber, PayDate ORDER BY CheckNumber");
                }

                if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
                {
                    lngWarrant =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(rsCheckInfo.Get_Fields_Int32("WarrantNumber"))));
                }
                else if (rsTACheckInfo.EndOfFile() != true && rsTACheckInfo.BeginningOfFile() != true)
                {
                    lngWarrant = FCConvert.ToInt32(rsTACheckInfo.Get_Fields_Int32("WarrantNumber"));
                }
                else if (rsDDCheckInfo.EndOfFile() != true && rsDDCheckInfo.BeginningOfFile() != true)
                {
                    lngWarrant = FCConvert.ToInt32(rsDDCheckInfo.Get_Fields_Int32("WarrantNumber"));
                }
                else
                {
                    lngWarrant = 0;
                }

                lblPayDate.Text = "Pay Date: " +
                                  Strings.Format(frmWarrant.InstancePtr.rsInfo.Get_Fields("PayDate"), "MM/dd/yyyy");
                if (lngWarrant != 0)
                {
                    // lblWarrant.Caption = "WARRANT:  " & Format(lngWarrant, "0000")
                    lblWarrant.Text = "WARRANT:  " + FCConvert.ToString(lngWarrant);
                }
                else
                {
                    lblWarrant.Text = "WARRANT:  UNKNOWN";
                }

                if (frmWarrant.InstancePtr.cmbReprint.Text == "Reprint Warrant")
                {
                    lblReprint.Visible = true;
                }
                else
                {
                    lblReprint.Visible = false;
                }

                int curTotal = 0;
                intTotalRegular = 0;
                blnFirstRecord = true;
                // 
                // INFORMATION FOR THE PUT INTO AP TABLE
                // 
                // THIS COULD BE A NEGATIVE (USUALLY ON THE LAST PAY RUN OF THE MONTH) BECAUSE FOR EACH
                // OF THE FIRST THREE PAY PERIODS FOR THAT MONTH THIS AMOUNT WILL BE POSITIVE AS YOU ARE
                // DEPOSITING MONEY INTO AP TO BE PAYED OUT THE LAST OF THE MONTH. IT WILL BE A NEGATIVE
                // THE LAST PAY DATE OF THE MONTH BECAUSE WE ARE TAKING OUT A LARGER SUM TO COVER ALL MONTH
                // AND WE ONLY DEPOSITED A LITTLE BIT THAT PAY DATE SO THIS WILL BE A NEGATIVE IN MOST CASES.
                // 
                // MATTHEW 02/24/2004
                // 
                double dblTotal;
                dblTotal = 0;
                rsAP.OpenRecordset(
                    "SELECT tblCheckDetail.MatchAmount From tblCheckDetail WHERE (((tblCheckDetail.PayDate)='" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "') AND tblCheckDetail.PayRunID =" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") +
                    " AND tblCheckDetail.MatchRecord=1) AND CheckVoid = 0 AND isnull(matchglaccountnumber, '') <> isnull(matchACCOUNTnumber, '')",
                    "TWPY0000.vb1");
                // Call rsAP.OpenRecordset("SELECT tblCheckDetail.MatchAmount From tblCheckDetail WHERE (((tblCheckDetail.PayDate)='" & frmWarrant.rsInfo.Fields("PayDate") & "') AND tblCheckDetail.PayRunID =" & frmWarrant.rsInfo.Fields("PayRunID") & " AND tblCheckDetail.MatchRecord=1) AND CheckVoid = 0 ", "TWPY0000.vb1")
                while (!rsAP.EndOfFile())
                {
                    dblTotal += FCConvert.ToDouble(rsAP.Get_Fields("MatchAmount"));
                    rsAP.MoveNext();
                }

                rsAP.OpenRecordset(
                    "SELECT Sum(([GrossPay]-[NetPay])+[EmployerFicaTax]+[EmployerMedicareTax]) AS TotalWH From tblCheckDetail WHERE tblCheckDetail.PayDate = '" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND tblCheckDetail.PayRunID =" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") +
                    " AND tblCheckDetail.TotalRecord = 1 AND CheckVoid = 0", "TWPY0000.vb1");
                rsTotalTA.OpenRecordset(
                    "SELECT Sum(tblCheckDetail.TrustAmount) AS SumOfTrustAmount From tblCheckDetail WHERE tblCheckDetail.PayDate = '" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND tblCheckDetail.PayRunID =" +
                    frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID") +
                    " AND tblCheckDetail.TrustRecord = 1 AND CheckVoid = 0", "TWPY0000.vb1");
                dblIntoAP = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsAP.Get_Fields("TotalWH")) + dblTotal,
                    "###,##0.00"));
                dblFromAP = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsTotalTA.Get_Fields("SumOfTrustAmount")),
                    "###,##0.00"));
                dblTotalGrossPay = 0;
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (frmWarrant.InstancePtr.cmbReprint.Text == "Initial Run")
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
				{
					modGlobalRoutines.UpdatePayrollStepTable("Warrant", "", "", frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate"), frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID"));
				}
				else
				{
					if (frmWarrant.InstancePtr.rsInfo.EndOfFile())
					{
						modDavesSweetCode.Statics.blnWarrantCompleted = true;
					}
					else
					{
						if (frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate").ToOADate() != modGlobalVariables.Statics.gdatCurrentPayDate.ToOADate() || FCConvert.ToInt32(frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID")) != modGlobalVariables.Statics.gintCurrentPayRun)
						{
							modGlobalRoutines.UpdatePayrollStepTable("Warrant", "", "", frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate"), frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID"));
						}
						else
						{
							modDavesSweetCode.Statics.blnWarrantCompleted = true;
						}
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strCheckType == "C")
			{
				ShowEmployeeCheckInfo();
			}
			else if (strCheckType == "T")
			{
				ShowTACheckInfo();
			}
			else
			{
				ShowDDCheckInfo();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalDD As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldListedCount As object	OnWrite
			// vbPorter upgrade warning: txtIntoAP As object	OnWrite(string)
			// vbPorter upgrade warning: txtFromAP As object	OnWrite(string)
			// vbPorter upgrade warning: txtTotalPayroll As object	OnWrite(string)
            using (clsDRWrapper rsSignatureInfo = new clsDRWrapper())
            {
                fldTotalDD.Text = Strings.Format(curTotalDDAmount, "#,##0.00");
                fldTotalCheckAmount.Text = Strings.Format(curTotalCheckAmount, "#,##0.00");
                fldTotalNetPay.Text = Strings.Format(curTotalDDAmount + curTotalCheckAmount, "#,##0.00");
                fldTotalGrossPay.Text = Strings.Format(dblTotalGrossPay, "###,###,##0.00");
                fldListedCount.Text = intTotalRegular.ToString();
                if (modGlobalVariables.Statics.gboolBudgetary)
                {
                    rsSignatureInfo.OpenRecordset("SELECT * FROM WarrantMessage", "TWBD0000.vb1");
                    if (rsSignatureInfo.EndOfFile() != true && rsSignatureInfo.BeginningOfFile() != true)
                    {
                        fldWarrantSignature1.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 1, 80);
                        fldWarrantSignature2.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 81, 80);
                        fldWarrantSignature3.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 161, 80);
                        fldWarrantSignature4.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 241, 80);
                        fldWarrantSignature5.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 321, 80);
                        fldWarrantSignature6.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 401, 80);
                        fldWarrantSignature7.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 481, 80);
                        fldWarrantSignature8.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 561, 80);
                        fldWarrantSignature9.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 641, 80);
                        fldWarrantSignature10.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 721, 80);
                        if (fecherFoundation.Strings.Trim(fldWarrantSignature1.Text) == "")
                        {
                            fldWarrantSignature1.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature2.Text) == "")
                        {
                            fldWarrantSignature2.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature3.Text) == "")
                        {
                            fldWarrantSignature3.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature4.Text) == "")
                        {
                            fldWarrantSignature4.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature5.Text) == "")
                        {
                            fldWarrantSignature5.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature6.Text) == "")
                        {
                            fldWarrantSignature6.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature7.Text) == "")
                        {
                            fldWarrantSignature7.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature8.Text) == "")
                        {
                            fldWarrantSignature8.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature9.Text) == "")
                        {
                            fldWarrantSignature9.Visible = false;
                        }

                        if (fecherFoundation.Strings.Trim(fldWarrantSignature10.Text) == "")
                        {
                            fldWarrantSignature10.Visible = false;
                        }
                    }
                    else
                    {
                        fldWarrantSignature1.Visible = false;
                        fldWarrantSignature2.Visible = false;
                        fldWarrantSignature3.Visible = false;
                        fldWarrantSignature4.Visible = false;
                        fldWarrantSignature5.Visible = false;
                        fldWarrantSignature6.Visible = false;
                        fldWarrantSignature7.Visible = false;
                        fldWarrantSignature8.Visible = false;
                        fldWarrantSignature9.Visible = false;
                        fldWarrantSignature10.Visible = false;
                    }
                }
                else
                {
                    fldWarrantSignature1.Visible = false;
                    fldWarrantSignature2.Visible = false;
                    fldWarrantSignature3.Visible = false;
                    fldWarrantSignature4.Visible = false;
                    fldWarrantSignature5.Visible = false;
                    fldWarrantSignature6.Visible = false;
                    fldWarrantSignature7.Visible = false;
                    fldWarrantSignature8.Visible = false;
                    fldWarrantSignature9.Visible = false;
                    fldWarrantSignature10.Visible = false;
                }

                txtIntoAP.Text = Strings.Format(dblIntoAP, "###,##0.00");
                txtFromAP.Text = "(" + Strings.Format(dblFromAP, "###,##0.00") + ")";
                // CHANGED FOR WATERBORO. MATTHEW 9/14/2004
                // txtTotalPayroll = Format(CDbl(fldTotalNetPay) + (CDbl(dblIntoAP) - CDbl(dblFromAP)), "###,##0.00")
                txtTotalPayroll.Text =
                    Strings.Format(
                        FCConvert.ToDouble(fldTotalCheckAmount.Text) +
                        (FCConvert.ToDouble(dblIntoAP) - FCConvert.ToDouble(dblFromAP)), "###,##0.00");
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ShowEmployeeCheckInfo()
		{
			// vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
			// vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: txtGrossPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldPayee As object	OnWrite(string)
			clsDRWrapper rsDirectDeposit = new clsDRWrapper();
			// vbPorter upgrade warning: curtempDDAmount As Decimal	OnWrite(Decimal, int)
			Decimal curtempDDAmount;
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				rsDirectDeposit.OpenRecordset("SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE MasterRecord = '" + rsCheckInfo.Get_Fields_String("MasterRecord") + "' AND EmployeeNumber = '" + rsCheckInfo.Get_Fields("EmployeeNumber") + "' AND BankRecord = 1 AND PayDate = '" + frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "'");
			}
			else
			{
				rsDirectDeposit.OpenRecordset("SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE MasterRecord = '" + rsCheckInfo.Get_Fields_String("MasterRecord") + "' AND EmployeeNumber = '" + rsCheckInfo.Get_Fields("EmployeeNumber") + "' AND BankRecord = 1 AND PayDate = '" + frmWarrant.InstancePtr.rsInfo.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + frmWarrant.InstancePtr.rsInfo.Get_Fields("PayRunID"));
			}
			if (rsDirectDeposit.EndOfFile() != true && rsDirectDeposit.BeginningOfFile() != true)
			{
				curtempDDAmount = FCConvert.ToDecimal(Conversion.Val(rsDirectDeposit.Get_Fields("TotalDDAmount")));
			}
			else
			{
				curtempDDAmount = 0;
			}
			intTotalRegular += 1;
			fldCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
			fldDirectDeposit.Text = Strings.Format(curtempDDAmount, "#,##0.00");
			fldCheckAmount.Text = Strings.Format(rsCheckInfo.Get_Fields_Decimal("NetPay") - curtempDDAmount, "#,##0.00");
			fldNetPay.Text = Strings.Format(Conversion.Val(rsCheckInfo.Get_Fields_Decimal("NetPay")), "#,##0.00");
			// MATTHEW 7/26/2005
			txtGrossPay.Text = Strings.Format(rsCheckInfo.Get_Fields("GrossPay"), "#,##0.00");
			curTotalDDAmount += curtempDDAmount;
			curTotalCheckAmount += FCConvert.ToDecimal(rsCheckInfo.Get_Fields_Decimal("NetPay") - curtempDDAmount);
			fldPayee.Text = rsCheckInfo.Get_Fields("EmployeeNumber") + "  " + rsCheckInfo.Get_Fields_String("EmployeeName");
			dblTotalGrossPay += rsCheckInfo.Get_Fields("GrossPay");
		}

		private void ShowTACheckInfo()
		{
			// vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
			// vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldPayee As object	OnWrite(string)
			// vbPorter upgrade warning: txtGrossPay As object	OnWrite(string)
            using (clsDRWrapper rsTARecipient = new clsDRWrapper())
            {


                if (!rsTACheckInfo.EndOfFile())
                {
                    rsTARecipient.OpenRecordset("SELECT * FROM tblRecipients WHERE ID = " +
                                                rsTACheckInfo.Get_Fields_Int32("TrustRecipientID"));
                    intTotalRegular += 1;
                    fldCheck.Text = FCConvert.ToString(rsTACheckInfo.Get_Fields("CheckNumber"));
                    fldDirectDeposit.Text = Strings.Format(0, "#,##0.00");
                    fldCheckAmount.Text = Strings.Format(rsTACheckInfo.Get_Fields("TotalTAAmount"), "#,##0.00");
                    fldNetPay.Text = Strings.Format(rsTACheckInfo.Get_Fields("TotalTAAmount"), "#,##0.00");
                    curTotalCheckAmount += FCConvert.ToDecimal(rsTACheckInfo.Get_Fields("TotalTAAmount"));
                    if (rsTARecipient.EndOfFile() != true && rsTARecipient.BeginningOfFile() != true)
                    {
                        fldPayee.Text = "T & A " + rsTARecipient.Get_Fields_Int32("RecptNumber") + "  " +
                                        rsTARecipient.Get_Fields_String("Name");
                    }
                    else
                    {
                        fldPayee.Text = "T & A UNKNOWN";
                    }

                    txtGrossPay.Text = "";
                }
            }
        }

		private void ShowDDCheckInfo()
		{
            // vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
            // vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
            // vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
            // vbPorter upgrade warning: fldPayee As object	OnWrite(string)
            using (clsDRWrapper rsDDBank = new clsDRWrapper())
            {
                if (blnACH)
                {
                    rsDDBank.OpenRecordset("SELECT * FROM tblBanks WHERE ACHBank = 1");
                }
                else
                {
                    rsDDBank.OpenRecordset("SELECT * FROM tblBanks WHERE ID = " +
                                           FCConvert.ToString(
                                               Conversion.Val(rsDDCheckInfo.Get_Fields("DDBankNumber"))));
                }

                intTotalRegular += 1;
                fldCheck.Text = rsDDCheckInfo.Get_Fields_String("CheckNumber");
                fldDirectDeposit.Text = Strings.Format(0, "#,##0.00");
                fldCheckAmount.Text = Strings.Format(rsDDCheckInfo.Get_Fields("TotalDDAmount"), "#,##0.00");
                fldNetPay.Text = Strings.Format(rsDDCheckInfo.Get_Fields("TotalDDAmount"), "#,##0.00");
                curTotalCheckAmount += FCConvert.ToDecimal(rsDDCheckInfo.Get_Fields("TotalDDAmount"));
                if (rsDDBank.EndOfFile() != true && rsDDBank.BeginningOfFile() != true)
                {
                    fldPayee.Text = "D / D " + rsDDBank.Get_Fields_Int32("RecordNumber") + "  " +
                                    rsDDBank.Get_Fields_String("Name");
                }
                else
                {
                    fldPayee.Text = "D / D UNKNOWN";
                }

                txtGrossPay.Text = "";
            }
        }

		
	}
}
