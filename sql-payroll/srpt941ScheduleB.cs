//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt941ScheduleB.
	/// </summary>
	public partial class srpt941ScheduleB : FCSectionReport
	{
		public srpt941ScheduleB()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "941";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srpt941ScheduleB InstancePtr
		{
			get
			{
				return (srpt941ScheduleB)Sys.GetInstance(typeof(srpt941ScheduleB));
			}
		}

		protected srpt941ScheduleB _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srpt941ScheduleB	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp;
			string[] strAry = null;
			int intQuarter;
			// vbPorter upgrade warning: lngYear As int	OnRead(string)
			int lngYear;
			DateTime dtStart;
			DateTime dtEnd;
			double dblTemp = 0;
			double[] dblMonthTotal = new double[3 + 1];
			double dblTotalTotal;
			int intMonth;
			int intCurMonth;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strSQL = "";
			
			string strWhere;
			int intRowNumber;
			string strActiveMonth = "";
			// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
			string strYear;
			LoadInfo();
			txtName.Text = EWRRecord.EmployerName;
			for (x = 1; x <= (EWRRecord.FederalEmployerID.Length); x++)
			{
				(Detail.Controls["txtEINBox" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Mid(EWRRecord.FederalEmployerID, x, 1);
			}
			// x
			dblMonthTotal[1] = 0;
			dblMonthTotal[2] = 0;
			dblMonthTotal[3] = 0;
			dblTotalTotal = 0;
			strTemp = FCConvert.ToString(this.UserData);
			strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
			intQuarter = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
			(Detail.Controls["txtQuarter" + intQuarter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
			strWhere = "";
			if (Information.UBound(strAry, 1) > 1)
			{
				strWhere = fecherFoundation.Strings.Trim(strAry[2]);
			}
			// Call GetDateRangeForYearQuarter(dtStart, dtEnd, intQuarter, lngYear)
			strYear = FCConvert.ToString(lngYear);
			txtYearMillenium.Text = Strings.Left(strYear, 1);
			txtYearCentury.Text = Strings.Mid(strYear, 2, 1);
			txtYearDecade.Text = Strings.Mid(strYear, 3, 1);
			txtYearYear.Text = Strings.Mid(strYear, 4, 1);
			intMonth = intQuarter * 3 - 2;
			// Call frmAdjust941ScheduleB.Init(dtStart, dtEnd, strWhere)
			// frmAdjust941ScheduleB.Show 1, MDIParent
			cFed941ViewContext viewContext = new cFed941ViewContext();
			DateTime dtDate;
			FCCollection collDetail = new FCCollection();
			double[,] MonthDetail = new double[3 + 1, 31 + 1];
			if (this.ParentReport is rptLaser941)
			{
				viewContext = (this.ParentReport as rptLaser941).ReportContext;
				txtTotal.Text = Strings.Format(viewContext.TotalLiability(), "#,###,###,##0.00");
			}
			else
			{
				txtTotal.Text = "0.00";
			}
			int intM;
			for (intM = 1; intM <= 3; intM++)
			{
				for (x = 1; x <= 31; x++)
				{
					MonthDetail[intM, x] = 0;
					(this.Detail.Controls["txtMonth" + intM + "Line" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
			for (intM = 1; intM <= 3; intM++)
			{
				dblTemp = 0;
				collDetail = viewContext.LiabilitiesForMonth(intM);
				foreach (cTaxAdj941 tDetail in collDetail)
				{
					MonthDetail[intM, Convert.ToDateTime(tDetail.EffectiveDate).Day] = MonthDetail[intM, FCConvert.ToDateTime(tDetail.EffectiveDate).Day] + tDetail.Amount;
					dblTemp += tDetail.Amount;
				}
				(this.Detail.Controls["txtMonth" + intM + "Total"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblTemp, "#,###,###,##0.00");
			}
			for (intM = 1; intM <= 3; intM++)
			{
				for (x = 1; x <= 31; x++)
				{
					if (MonthDetail[intM, x] > 0)
					{
						(this.Detail.Controls["txtMonth" + intM + "Line" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(MonthDetail[intM, x], "#,###,###,##0.00");
					}
				}
			}
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                // LOAD INFO INTO THE GRID TO DISPLAY ON THE REPORT
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    // FILL
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                }
            }
        }

	
	}
}
