﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDDStub1.
	/// </summary>
	public partial class srptDDStub1 : FCSectionReport
	{
		public srptDDStub1()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptDDStub1 InstancePtr
		{
			get
			{
				return (srptDDStub1)Sys.GetInstance(typeof(srptDDStub1));
			}
		}

		protected srptDDStub1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptDDStub1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
				if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais")
				{
					Detail.Height = 5220 / 1440F;
					Detail.CanShrink = false;
					// For x = 1 To Me.Detail.Controls.Count - 1
					// If UCase(Me.Detail.Controls[x].Tag) = "TEXT" Or UCase(Me.Detail.Controls[x].Tag) = "BOLD" Then
					// Select Case LCase(Me.Detail.Controls[x].Name)
					// 
					// 
					// Case Else
					// Me.Detail.Controls[x].Top = Me.Detail.Controls[x].Top + 240
					// End Select
					// End If
					// Next x
					txtCheckNo.Left = 9000 / 1440F;
					txtCheckNo.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					txtCheckNo.Top = 240 / 1440F;
					txtDate.Top = 240 / 1440F;
				}
                if (FCConvert.ToString(this.UserData) != string.Empty)
                {
                    int X;
                    if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HALLOWELL")
                    {
                    }
                    else
                    {
                        clsPrt.SetReportFontsByTag(this, "Text", FCConvert.ToString(this.UserData));
                        clsPrt.SetReportFontsByTag(this, "bold", FCConvert.ToString(this.UserData), true);
                    }
                }
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DDStub ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// fill textboxes from grid
			int X;
			string[] strAray = new string[4 + 1];
			double[] dblAray = new double[4 + 1];
			//clsDRWrapper rsTemp = new clsDRWrapper();
			double[] dblAdjAray = new double[3 + 1];
			string[] strAdjAray = new string[3 + 1];
			double dblState;
			double dblFed = 0;
			double dblFica = 0;
			double dblMed = 0;
			double dblAdjFed = 0;
			double dblAdjFica = 0;
			double dblAdjMed = 0;
			int lngRecip;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lblFed.Visible = false;
				lblState.Visible = false;
				lblMed.Visible = false;
				lblFica.Visible = false;
				txtFed.Visible = false;
				txtState.Visible = false;
				txtMed.Visible = false;
				txtFica.Visible = false;
				lblCheckMessage.Text = modGlobalVariables.Statics.gstrCheckMessage;
				txtChkAmount.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9);
				txtBankNumber.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 2);
				txtName.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 18);
				txtCheckNo.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12);
				txtDate.Text = Strings.Format(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 24), "MM/dd/yyyy");
				if (rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 27) == "T")
				{
					X = 0;
					lblFed.Text = "Fed";
					lblState.Text = "State";
					lblMed.Text = "Med";
					lblFica.Text = "Fica";
					strAray[1] = "Fed";
					strAray[2] = "Fica";
					strAray[3] = "Med";
					strAray[4] = "State";
					lblFed.Visible = true;
					lblState.Visible = true;
					lblMed.Visible = true;
					lblFica.Visible = true;
					txtFed.Visible = true;
					txtState.Visible = true;
					txtMed.Visible = true;
					txtFica.Visible = true;
					lblFedAdjust.Visible = false;
					lblFicaAdjust.Visible = false;
					lblMedAdjust.Visible = false;
					txtFedAdjust.Visible = false;
					txtFicaAdjust.Visible = false;
					txtMedAdjust.Visible = false;
					dblAray[1] = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 6));
					dblAray[2] = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 7));
					dblAray[3] = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 32));
					dblAray[4] = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 8));
					dblFed = dblAray[1];
					dblFica = dblAray[2];
					dblMed = dblAray[3];
					dblState = dblAray[4];
					dblAdjFed = 0;
					dblAdjFica = 0;
					dblAdjMed = 0;
					dblAdjAray[1] = 0;
					dblAdjAray[2] = 0;
					dblAdjAray[3] = 0;
					// If dblAray(1) <> 0 Or dblAray(2) <> 0 Or dblAray(3) <> 0 Then
					// fed,fica medicare
					// strAdjAray(1) = "Fed Adjust"
					// strAdjAray(2) = "Fica Adjust"
					// lngRecip = Mid(.TextMatrix(0, 2), 11)
					// strAdjAray(3) = "Med Adjust"
					// Call rsTemp.OpenRecordset("select sum(trustamount) as trustsum from tblcheckdetail where trustcategoryid = 1 and checkvoid = 0 and trustrecipientid = " & lngRecip & " and " & .TextMatrix(0, 45) & " and (adjustrecord or taadjustrecord)", "twpy0000.vb1")
					// If Not rsTemp.EndOfFile Then
					// dblAdjAray(1) = Val(rsTemp.Fields("trustsum"))
					// dblAdjFed = dblAdjAray(1)
					// End If
					// If dblAdjAray(1) <> 0 Then
					// dblAray(1) = dblAray(1) - dblAdjAray(1)
					// dblFed = dblAray(1)
					// End If
					// Call rsTemp.OpenRecordset("select sum(trustamount) as trustsum from tblcheckdetail where trustcategoryid = 2 and checkvoid = 0 and trustrecipientid = " & lngRecip & " and " & .TextMatrix(0, 45) & " and (adjustrecord or taadjustrecord)", "twpy0000.vb1")
					// If Not rsTemp.EndOfFile Then
					// dblAdjAray(2) = Val(rsTemp.Fields("trustsum"))
					// dblAdjFica = dblAdjAray(2)
					// End If
					// If dblAdjAray(2) <> 0 Then
					// dblAray(2) = dblAray(2) - dblAdjAray(2)
					// dblFica = dblAray(2)
					// End If
					// Call rsTemp.OpenRecordset("select sum(trustamount) as trustsum from tblcheckdetail where trustcategoryid = 3 and checkvoid = 0 and trustrecipientid = " & lngRecip & " and " & .TextMatrix(0, 45) & " and (adjustrecord or taadjustrecord)", "twpy0000.vb1")
					// If Not rsTemp.EndOfFile Then
					// dblAdjAray(3) = Val(rsTemp.Fields("trustsum"))
					// dblAdjMed = dblAdjAray(3)
					// End If
					// If dblAdjAray(3) <> 0 Then
					// dblAray(3) = dblAray(3) - dblAdjAray(3)
					// dblMed = dblAray(3)
					// End If
					// End If
					if (Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 8)) == 0)
					{
						strAray[4] = "";
						dblAray[4] = 0;
						txtState.Visible = false;
					}
					// If Val(.TextMatrix(0, 32)) = 0 Then
					if (dblMed == 0 && dblAdjMed == 0)
					{
						strAray[3] = strAray[4];
						strAray[4] = "";
						dblAray[3] = dblAray[4];
						dblAray[4] = 0;
						txtState.Visible = false;
						strAdjAray[3] = "";
						dblAdjAray[3] = 0;
					}
					// If Val(.TextMatrix(0, 7)) = 0 Then
					if (dblFica == 0 && dblAdjFica == 0)
					{
						strAray[2] = strAray[3];
						strAray[3] = strAray[4];
						strAray[4] = "";
						dblAray[2] = dblAray[3];
						dblAray[3] = dblAray[4];
						dblAray[4] = 0;
						txtState.Visible = false;
						strAdjAray[2] = strAdjAray[3];
						strAdjAray[3] = "";
						dblAdjAray[2] = dblAdjAray[3];
						dblAdjAray[3] = 0;
					}
					// If Val(.TextMatrix(0, 6)) = 0 Then
					if (dblFed == 0 && dblAdjFed == 0)
					{
						strAray[1] = strAray[2];
						strAray[2] = strAray[3];
						strAray[3] = strAray[4];
						strAray[4] = "";
						dblAray[1] = dblAray[2];
						dblAray[2] = dblAray[3];
						dblAray[3] = dblAray[4];
						dblAray[4] = 0;
						txtState.Visible = false;
						strAdjAray[1] = strAdjAray[2];
						strAdjAray[2] = strAdjAray[3];
						strAdjAray[3] = "";
						dblAdjAray[1] = dblAdjAray[2];
						dblAdjAray[2] = dblAdjAray[3];
						dblAdjAray[3] = 0;
					}
					lblFed.Text = strAray[1];
					lblFica.Text = strAray[2];
					lblMed.Text = strAray[3];
					lblState.Text = strAray[4];
					lblFedAdjust.Text = strAdjAray[1];
					lblFicaAdjust.Text = strAdjAray[2];
					lblMedAdjust.Text = strAdjAray[3];
					txtFed.Text = Strings.Format(dblAray[1], "0.00");
					txtFica.Text = Strings.Format(dblAray[2], "0.00");
					txtMed.Text = Strings.Format(dblAray[3], "0.00");
					txtState.Text = Strings.Format(dblAray[4], "0.00");
					txtFedAdjust.Text = Strings.Format(dblAdjAray[1], "0.00");
					txtFicaAdjust.Text = Strings.Format(dblAdjAray[2], "0.00");
					txtMedAdjust.Text = Strings.Format(dblAdjAray[3], "0.00");
					if (dblAray[4] == 0)
						txtState.Visible = false;
					if (dblAray[3] == 0 && dblAdjAray[3] == 0)
					{
						txtMed.Visible = false;
						txtMedAdjust.Visible = false;
					}
					else if (dblAdjAray[3] == 0)
					{
						txtMedAdjust.Visible = false;
						lblMedAdjust.Text = "";
					}
					if (dblAray[2] == 0 && dblAdjAray[2] == 0)
					{
						txtFica.Visible = false;
						txtFicaAdjust.Visible = false;
					}
					else if (dblAdjAray[2] == 0)
					{
						txtFicaAdjust.Visible = false;
						lblFicaAdjust.Text = "";
					}
					if (dblAray[1] == 0 && dblAdjAray[1] == 0)
					{
						txtFed.Visible = false;
						txtFedAdjust.Visible = false;
					}
					else if (dblAdjAray[1] == 0)
					{
						txtFedAdjust.Visible = false;
						lblFedAdjust.Text = "";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Direct Deposit Stub DetailFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
