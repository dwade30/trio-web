//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmPrintMaine941 : BaseForm
	{
		public frmPrintMaine941()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_8, 8);
			this.Label1.AddControlArrayElement(Label1_10, 9);
			this.Label1.AddControlArrayElement(Label1_11, 10);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintMaine941 InstancePtr
		{
			get
			{
				return (frmPrintMaine941)Sys.GetInstance(typeof(frmPrintMaine941));
			}
		}

		protected frmPrintMaine941 _InstancePtr = null;
		//=========================================================
		string strSequence;
		int lngYearCovered;
		int intQuarterCovered;
		int intLastMonthInQuarter;
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();

		public void Init(int intQCovered, int lngYCovered, string strSeq = "", bool boolNotElectronic = false)
		{
			strSequence = strSeq;
			lngYearCovered = lngYCovered;
			intQuarterCovered = intQCovered;
			switch (intQuarterCovered)
			{
				case 1:
					{
						intLastMonthInQuarter = 3;
						break;
					}
				case 2:
					{
						intLastMonthInQuarter = 6;
						break;
					}
				case 3:
					{
						intLastMonthInQuarter = 9;
						break;
					}
				case 4:
					{
						intLastMonthInQuarter = 12;
						break;
					}
			}
			//end switch
			// get payment info
			frmC1Payments.InstancePtr.Init(intLastMonthInQuarter, lngYearCovered, strSeq);
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmPrintMaine941_Load(object sender, System.EventArgs e)
		{
			int intTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, true);
			LoadInfo();
			ShowInfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
		// vbPorter upgrade warning: KeyAscii As int	OnWrite(Keys)
		private bool KeyIsNumber(int KeyAscii)
		{
			bool KeyIsNumber = false;
			KeyIsNumber = false;
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 27 || KeyAscii == 127 || KeyAscii == 8))
			{
				KeyIsNumber = true;
			}
			return KeyIsNumber;
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			strSQL = "select * from tblemployerinfo";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1, modCoreysSweeterCode.EWRReturnCityLen);
				EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")), 1, modCoreysSweeterCode.EWRReturnNameLen);
				EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
				EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
				EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
				EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
				EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
				EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
				EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
				EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				EWRRecord.TransmitterName = FCConvert.ToString(clsLoad.Get_Fields("transmittername"));
				EWRRecord.TransmitterEmail = FCConvert.ToString(clsLoad.Get_Fields("transmitteremail"));
			}
			else
			{
				EWRRecord.TransmitterName = "";
				EWRRecord.EmployerAddress = "";
				EWRRecord.EmployerCity = "";
				EWRRecord.EmployerName = "";
				EWRRecord.EmployerState = "ME";
				EWRRecord.EmployerStateCode = 23;
				EWRRecord.EmployerZip = "";
				EWRRecord.EmployerZip4 = "";
				EWRRecord.FederalEmployerID = "";
				EWRRecord.MRSWithholdingID = "";
				EWRRecord.StateUCAccount = "";
				EWRRecord.TransmitterExt = "";
				EWRRecord.TransmitterPhone = "0000000000";
				EWRRecord.TransmitterTitle = "";
				EWRRecord.TransmitterEmail = "";
			}
		}

		private void ShowInfo()
		{
			string strTemp = "";
			object strPhone = "";
			int x;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			txtCity.Text = EWRRecord.EmployerCity;
			txtEmployerName.Text = EWRRecord.EmployerName;
			txtExtension.Text = EWRRecord.TransmitterExt;
			txtMRSID.Text = EWRRecord.MRSWithholdingID;
			txtFederalID.Text = EWRRecord.FederalEmployerID;
			txtState.Text = EWRRecord.EmployerState;
			txtStreetAddress.Text = EWRRecord.EmployerAddress;
			txttitle.Text = EWRRecord.TransmitterTitle;
			txtZip.Text = EWRRecord.EmployerZip;
			txtZip4.Text = EWRRecord.EmployerZip4;
			txtEmail.Text = EWRRecord.TransmitterEmail;
			strPhone = EWRRecord.TransmitterPhone;
			strPhone = FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(strPhone), 10));
			txtPhone.Text = "(" + Strings.Mid(strPhone.ToStringES(), 1, 3) + ")" + Strings.Mid(strPhone.ToStringES(), 4, 3) + "-" + Strings.Mid(strPhone.ToStringES(), 7);
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			object strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				if (fecherFoundation.Strings.Trim(txtFederalID.Text).Length != modCoreysSweeterCode.EWRFederalIDLen)
				{
					MessageBox.Show("The federal ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRFederalIDLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtFederalID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtMRSID.Text).Length != modCoreysSweeterCode.EWRMRSWithholdingLen)
				{
					MessageBox.Show("The MRS Withholding Account must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRMRSWithholdingLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtMRSID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtEmployerName.Text) == string.Empty)
				{
					MessageBox.Show("You must provide an employer name", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmployerName.Focus();
					return SaveInfo;
				}
				EWRRecord.EmployerAddress = Strings.Mid(fecherFoundation.Strings.Trim(txtStreetAddress.Text), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				EWRRecord.EmployerCity = Strings.Mid(fecherFoundation.Strings.Trim(txtCity.Text), 1, modCoreysSweeterCode.EWRReturnCityLen);
				EWRRecord.EmployerName = Strings.Mid(fecherFoundation.Strings.Trim(txtEmployerName.Text), 1, modCoreysSweeterCode.EWRReturnNameLen);
				EWRRecord.EmployerState = Strings.Mid(fecherFoundation.Strings.Trim(txtState.Text), 1, 2);
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = fecherFoundation.Strings.Trim(txtZip.Text);
				EWRRecord.EmployerZip4 = fecherFoundation.Strings.Trim(txtZip4.Text);
				EWRRecord.FederalEmployerID = txtFederalID.Text;
				EWRRecord.MRSWithholdingID = txtMRSID.Text;
				EWRRecord.TransmitterExt = txtExtension.Text;
				EWRRecord.TransmitterEmail = txtEmail.Text;
				strTemp = "";
				for (x = 1; x <= (txtPhone.Text.Length); x++)
				{
					if (Information.IsNumeric(Strings.Mid(txtPhone.Text, x, 1)))
					{
						strTemp += Strings.Mid(txtPhone.Text, x, 1);
					}
				}
				// x
				EWRRecord.TransmitterPhone = FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(strTemp), 10));
				EWRRecord.TransmitterTitle = fecherFoundation.Strings.Trim(txttitle.Text);
				clsSave.OpenRecordset("select * from tblemployerinfo", "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("employername", EWRRecord.EmployerName);
				clsSave.Set_Fields("Address1", EWRRecord.EmployerAddress);
				clsSave.Set_Fields("City", EWRRecord.EmployerCity);
				clsSave.Set_Fields("State", EWRRecord.EmployerState);
				clsSave.Set_Fields("zip", EWRRecord.EmployerZip);
				clsSave.Set_Fields("zip4", EWRRecord.EmployerZip4);
				clsSave.Set_Fields("TransmitterTitle", EWRRecord.TransmitterTitle);
				clsSave.Set_Fields("transmitterextension", EWRRecord.TransmitterExt);
				clsSave.Set_Fields("transmitterphone", EWRRecord.TransmitterPhone);
				clsSave.Set_Fields("FederalEmployerID", EWRRecord.FederalEmployerID);
				clsSave.Set_Fields("MRSWithholdingID", EWRRecord.MRSWithholdingID);
				clsSave.Set_Fields("transmitteremail", EWRRecord.TransmitterEmail);
				clsSave.Update();
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptC1Laser.InstancePtr.Init(0, 0, ref intQuarterCovered, ref lngYearCovered, ref strSequence, true);
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			rptC1Laser.InstancePtr.Init(0, 0, ref intQuarterCovered, ref lngYearCovered, ref strSequence, true, 0, true);
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsState941 clsE941 = new clsState941();
			if (!SaveInfo())
			{
				return;
			}
			clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("electronicc1")))
				{
					clsE941.IncludeC1 = false;
					clsE941.QuarterCovered = intQuarterCovered;
					clsE941.Sequence = strSequence;
					clsE941.YearCovered = lngYearCovered;
					if (clsE941.CreateFile())
					{
                        //FC:FINAL:AM:#2754 - no need for messagebox
                        //MessageBox.Show("File rtnwage created in " + FCFileSystem.Statics.UserDataFolder, "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "rtnwage.txt"), "rtnwage.txt");
                        mnuExit_Click();
                    }
				}
				else
				{
					rptC1Laser.InstancePtr.Init(0, 0, ref intQuarterCovered, ref lngYearCovered, ref strSequence, true);
					mnuExit_Click();
				}
			}
		}

		private void txtMRSID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void CreateElectronicVersion()
		{
			clsState941 cls941 = new clsState941();
			cls941.IncludeC1 = false;
			cls941.QuarterCovered = intQuarterCovered;
			cls941.YearCovered = lngYearCovered;
			cls941.Sequence = strSequence;
		}
	}
}
