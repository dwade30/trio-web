﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmpVacSick.
	/// </summary>
	partial class srptEmpVacSick
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptEmpVacSick));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPER = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPER)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCategory,
				this.txtRate,
				this.txtAccruedCurrent,
				this.txtAccruedFiscal,
				this.txtAccruedCalendar,
				this.txtUsedCurrent,
				this.txtUsedFiscal,
				this.txtUsedCalendar,
				this.txtBalance,
				this.txtUM,
				this.txtPER
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape4,
				this.Label9,
				this.Label3,
				this.Label4,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.lblBalance,
				this.Label20,
				this.Label21,
				this.Label33
			});
			this.ReportHeader.Height = 0.5104167F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Shape4
			// 
			this.Shape4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape4.Height = 0.3125F;
			this.Shape4.Left = 0F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 0.15625F;
			this.Shape4.Width = 7.125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label9.Text = "Category";
			this.Label9.Top = 0.3125F;
			this.Label9.Width = 1.1875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.25F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label3.Text = "Rate";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.8125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label4.Text = "U/M";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 0.375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.1875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label12.Text = "PER";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.9375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label13.Text = "Current";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 0.5625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.5F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label14.Text = "Fiscal";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 0.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label15.Text = "Current";
			this.Label15.Top = 0.3125F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.3125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label16.Text = "Fiscal";
			this.Label16.Top = 0.3125F;
			this.Label16.Width = 0.625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.0625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label17.Text = "Calendar";
			this.Label17.Top = 0.3125F;
			this.Label17.Width = 0.5625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label18.Text = "Calendar";
			this.Label18.Top = 0.3125F;
			this.Label18.Width = 0.625F;
			// 
			// lblBalance
			// 
			this.lblBalance.Height = 0.1875F;
			this.lblBalance.HyperLink = null;
			this.lblBalance.Left = 6.5F;
			this.lblBalance.Name = "lblBalance";
			this.lblBalance.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblBalance.Text = "Balance";
			this.lblBalance.Top = 0.3125F;
			this.lblBalance.Width = 0.625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.9375F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label20.Text = "------------ Accrued  ------------";
			this.Label20.Top = 0.15625F;
			this.Label20.Width = 1.6875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label21.Text = "-------------- Used --------------";
			this.Label21.Top = 0.15625F;
			this.Label21.Width = 1.8125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 9pt; font-weight: bold";
			this.Label33.Text = "Vacation / Sick";
			this.Label33.Top = 0.15625F;
			this.Label33.Width = 1.625F;
			// 
			// txtCategory
			// 
			this.txtCategory.Height = 0.1875F;
			this.txtCategory.Left = 0F;
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Style = "font-size: 9pt";
			this.txtCategory.Text = null;
			this.txtCategory.Top = 0F;
			this.txtCategory.Width = 1.1875F;
			// 
			// txtRate
			// 
			this.txtRate.Height = 0.1875F;
			this.txtRate.Left = 1.25F;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "font-size: 9pt; text-align: right";
			this.txtRate.Text = null;
			this.txtRate.Top = 0F;
			this.txtRate.Width = 0.5625F;
			// 
			// txtAccruedCurrent
			// 
			this.txtAccruedCurrent.Height = 0.1875F;
			this.txtAccruedCurrent.Left = 2.9375F;
			this.txtAccruedCurrent.Name = "txtAccruedCurrent";
			this.txtAccruedCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedCurrent.Text = null;
			this.txtAccruedCurrent.Top = 0F;
			this.txtAccruedCurrent.Width = 0.5625F;
			// 
			// txtAccruedFiscal
			// 
			this.txtAccruedFiscal.Height = 0.1875F;
			this.txtAccruedFiscal.Left = 3.5F;
			this.txtAccruedFiscal.Name = "txtAccruedFiscal";
			this.txtAccruedFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedFiscal.Text = null;
			this.txtAccruedFiscal.Top = 0F;
			this.txtAccruedFiscal.Width = 0.5625F;
			// 
			// txtAccruedCalendar
			// 
			this.txtAccruedCalendar.Height = 0.1875F;
			this.txtAccruedCalendar.Left = 4.0625F;
			this.txtAccruedCalendar.Name = "txtAccruedCalendar";
			this.txtAccruedCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedCalendar.Text = null;
			this.txtAccruedCalendar.Top = 0F;
			this.txtAccruedCalendar.Width = 0.5625F;
			// 
			// txtUsedCurrent
			// 
			this.txtUsedCurrent.Height = 0.1875F;
			this.txtUsedCurrent.Left = 4.625F;
			this.txtUsedCurrent.Name = "txtUsedCurrent";
			this.txtUsedCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtUsedCurrent.Text = null;
			this.txtUsedCurrent.Top = 0F;
			this.txtUsedCurrent.Width = 0.5625F;
			// 
			// txtUsedFiscal
			// 
			this.txtUsedFiscal.Height = 0.1875F;
			this.txtUsedFiscal.Left = 5.25F;
			this.txtUsedFiscal.Name = "txtUsedFiscal";
			this.txtUsedFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtUsedFiscal.Text = null;
			this.txtUsedFiscal.Top = 0F;
			this.txtUsedFiscal.Width = 0.625F;
			// 
			// txtUsedCalendar
			// 
			this.txtUsedCalendar.Height = 0.1875F;
			this.txtUsedCalendar.Left = 5.875F;
			this.txtUsedCalendar.Name = "txtUsedCalendar";
			this.txtUsedCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtUsedCalendar.Text = null;
			this.txtUsedCalendar.Top = 0F;
			this.txtUsedCalendar.Width = 0.625F;
			// 
			// txtBalance
			// 
			this.txtBalance.Height = 0.1875F;
			this.txtBalance.Left = 6.5F;
			this.txtBalance.Name = "txtBalance";
			this.txtBalance.Style = "font-size: 9pt; text-align: right";
			this.txtBalance.Text = null;
			this.txtBalance.Top = 0F;
			this.txtBalance.Width = 0.625F;
			// 
			// txtUM
			// 
			this.txtUM.Height = 0.1875F;
			this.txtUM.Left = 1.8125F;
			this.txtUM.Name = "txtUM";
			this.txtUM.Style = "font-size: 9pt; text-align: center";
			this.txtUM.Text = null;
			this.txtUM.Top = 0F;
			this.txtUM.Width = 0.375F;
			// 
			// txtPER
			// 
			this.txtPER.Height = 0.1875F;
			this.txtPER.Left = 2.1875F;
			this.txtPER.Name = "txtPER";
			this.txtPER.Style = "font-size: 9pt; text-align: center";
			this.txtPER.Text = null;
			this.txtPER.Top = 0F;
			this.txtPER.Width = 0.75F;
			// 
			// srptEmpVacSick
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.020833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPER)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPER;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
