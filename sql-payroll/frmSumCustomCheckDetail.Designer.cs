//Fecher vbPorter - Version 1.0.0.59
using Wisej.Web;
using fecherFoundation.Extensions;
using Global;
using fecherFoundation;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmSumCustomCheckDetail.
	/// </summary>
	partial class frmSumCustomCheckDetail
	{
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCComboBox cmbReportOn;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdAll;
		public T2KDateBox txtDeptStartDate;
		public T2KDateBox txtDeptEndDate;
		public fecherFoundation.FCTextBox txtDept;
		public T2KDateBox txtEndDate;
		public T2KDateBox txtStartDate;
		public T2KDateBox txtEmpEndDate;
		public T2KDateBox txtEmpStartDate;
		public fecherFoundation.FCTextBox txtEmpSingle;
		public fecherFoundation.FCTextBox txtSingleEmployee;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblDash;
		public fecherFoundation.FCListBox lstTypes;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.cmbReportOn = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmdAll = new fecherFoundation.FCButton();
            this.txtDeptStartDate = new T2KDateBox();
            this.txtDeptEndDate = new T2KDateBox();
            this.txtDept = new fecherFoundation.FCTextBox();
            this.txtEndDate = new T2KDateBox();
            this.txtStartDate = new T2KDateBox();
            this.txtEmpEndDate = new T2KDateBox();
            this.txtEmpStartDate = new T2KDateBox();
            this.txtEmpSingle = new fecherFoundation.FCTextBox();
            this.txtSingleEmployee = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblDash = new fecherFoundation.FCLabel();
            this.lstTypes = new fecherFoundation.FCListBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(471, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lstTypes);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Size = new System.Drawing.Size(471, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(471, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(309, 30);
            this.HeaderText.Text = "Check Detail Summary Rpt";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbSort
            // 
            this.cmbSort.AutoSize = false;
            this.cmbSort.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSort.FormattingEnabled = true;
            this.cmbSort.Items.AddRange(new object[] {
            "Employee Number"});
            this.cmbSort.Location = new System.Drawing.Point(294, 30);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(121, 40);
            this.cmbSort.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.cmbSort, null);
            this.cmbSort.Visible = false;
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(181, 44);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(116, 15);
            this.lblSort.TabIndex = 1;
            this.lblSort.Text = "SORT REPORT BY";
            this.ToolTip1.SetToolTip(this.lblSort, null);
            this.lblSort.Visible = false;
            // 
            // cmbReportOn
            // 
            this.cmbReportOn.AutoSize = false;
            this.cmbReportOn.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReportOn.FormattingEnabled = true;
            this.cmbReportOn.Items.AddRange(new object[] {
            "Single Employee",
            "Emp / Date Range",
            "Date Range",
            "Dept / Date Range",
            "All Files"});
            this.cmbReportOn.Location = new System.Drawing.Point(20, 30);
            this.cmbReportOn.Name = "cmbReportOn";
            this.cmbReportOn.Size = new System.Drawing.Size(364, 40);
            this.cmbReportOn.TabIndex = 23;
            this.cmbReportOn.Text = "Single Employee";
            this.ToolTip1.SetToolTip(this.cmbReportOn, null);
            this.cmbReportOn.SelectedIndexChanged += new System.EventHandler(this.optReportOn_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmdAll);
            this.Frame1.Controls.Add(this.cmbReportOn);
            this.Frame1.Controls.Add(this.txtDeptStartDate);
            this.Frame1.Controls.Add(this.txtDeptEndDate);
            this.Frame1.Controls.Add(this.txtDept);
            this.Frame1.Controls.Add(this.txtEndDate);
            this.Frame1.Controls.Add(this.txtStartDate);
            this.Frame1.Controls.Add(this.txtEmpEndDate);
            this.Frame1.Controls.Add(this.txtEmpStartDate);
            this.Frame1.Controls.Add(this.txtEmpSingle);
            this.Frame1.Controls.Add(this.txtSingleEmployee);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.lblDash);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(404, 390);
            this.Frame1.TabIndex = 17;
            this.Frame1.Text = "Report Criteria:";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // cmdAll
            // 
            this.cmdAll.AppearanceKey = "actionButton";
            this.cmdAll.Location = new System.Drawing.Point(20, 330);
            this.cmdAll.Name = "cmdAll";
            this.cmdAll.Size = new System.Drawing.Size(115, 40);
            this.cmdAll.TabIndex = 22;
            this.cmdAll.Text = "Select All";
            this.ToolTip1.SetToolTip(this.cmdAll, null);
            this.cmdAll.Click += new System.EventHandler(this.cmdAll_Click);
            // 
            // txtDeptStartDate
            // 
            this.txtDeptStartDate.AutoSize = false;
            this.txtDeptStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeptStartDate.Location = new System.Drawing.Point(120, 270);
            this.txtDeptStartDate.Name = "txtDeptStartDate";
            this.txtDeptStartDate.Size = new System.Drawing.Size(120, 40);
            this.txtDeptStartDate.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtDeptStartDate, "Start Date");
            this.txtDeptStartDate.Visible = false;
            //this.txtDeptStartDate.Enter += new System.EventHandler(this.txtDeptStartDate_Enter);
            //this.txtDeptStartDate.TextChanged += new System.EventHandler(this.txtDeptStartDate_TextChanged);
            // 
            // txtDeptEndDate
            // 
            this.txtDeptEndDate.AutoSize = false;
            this.txtDeptEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeptEndDate.Location = new System.Drawing.Point(264, 270);
            this.txtDeptEndDate.Name = "txtDeptEndDate";
            this.txtDeptEndDate.Size = new System.Drawing.Size(120, 40);
            this.txtDeptEndDate.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtDeptEndDate, "End Date");
            this.txtDeptEndDate.Visible = false;
            //this.txtDeptEndDate.Enter += new System.EventHandler(this.txtDeptEndDate_Enter);
            //this.txtDeptEndDate.TextChanged += new System.EventHandler(this.txtDeptEndDate_TextChanged);
            // 
            // txtDept
            // 
            this.txtDept.AutoSize = false;
            this.txtDept.BackColor = System.Drawing.SystemColors.Window;
            this.txtDept.LinkItem = null;
            this.txtDept.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDept.LinkTopic = null;
            this.txtDept.Location = new System.Drawing.Point(20, 270);
            this.txtDept.Name = "txtDept";
            this.txtDept.Size = new System.Drawing.Size(80, 40);
            this.txtDept.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtDept, "Department Number");
            this.txtDept.Visible = false;
            // 
            // txtEndDate
            // 
            this.txtEndDate.AutoSize = false;
            this.txtEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEndDate.Location = new System.Drawing.Point(264, 210);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(120, 40);
            this.txtEndDate.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtEndDate, "End Date");
            this.txtEndDate.Visible = false;
            //this.txtEndDate.Enter += new System.EventHandler(this.txtEndDate_Enter);
            //this.txtEndDate.TextChanged += new System.EventHandler(this.txtEndDate_TextChanged);
            // 
            // txtStartDate
            // 
            this.txtStartDate.AutoSize = false;
            this.txtStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtStartDate.Location = new System.Drawing.Point(120, 210);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(120, 40);
            this.txtStartDate.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtStartDate, "Start Date");
            this.txtStartDate.Visible = false;
            //this.txtStartDate.Enter += new System.EventHandler(this.txtStartDate_Enter);
            //this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_TextChanged);
            // 
            // txtEmpEndDate
            // 
            this.txtEmpEndDate.AutoSize = false;
            this.txtEmpEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpEndDate.Location = new System.Drawing.Point(264, 150);
            this.txtEmpEndDate.Name = "txtEmpEndDate";
            this.txtEmpEndDate.Size = new System.Drawing.Size(120, 40);
            this.txtEmpEndDate.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtEmpEndDate, "End Date");
            this.txtEmpEndDate.Visible = false;
            //this.txtEmpEndDate.Enter += new System.EventHandler(this.txtEmpEndDate_Enter);
            //this.txtEmpEndDate.TextChanged += new System.EventHandler(this.txtEmpEndDate_TextChanged);
            // 
            // txtEmpStartDate
            // 
            this.txtEmpStartDate.AutoSize = false;
            this.txtEmpStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpStartDate.Location = new System.Drawing.Point(120, 150);
            this.txtEmpStartDate.Name = "txtEmpStartDate";
            this.txtEmpStartDate.Size = new System.Drawing.Size(120, 40);
            this.txtEmpStartDate.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtEmpStartDate, "Start Date");
            this.txtEmpStartDate.Visible = false;
            //this.txtEmpStartDate.Enter += new System.EventHandler(this.txtEmpStartDate_Enter);
            //this.txtEmpStartDate.TextChanged += new System.EventHandler(this.txtEmpStartDate_TextChanged);
            // 
            // txtEmpSingle
            // 
            this.txtEmpSingle.AutoSize = false;
            this.txtEmpSingle.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpSingle.LinkItem = null;
            this.txtEmpSingle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmpSingle.LinkTopic = null;
            this.txtEmpSingle.Location = new System.Drawing.Point(20, 150);
            this.txtEmpSingle.Name = "txtEmpSingle";
            this.txtEmpSingle.Size = new System.Drawing.Size(80, 40);
            this.txtEmpSingle.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtEmpSingle, "Employee Number");
            this.txtEmpSingle.Visible = false;
            // 
            // txtSingleEmployee
            // 
            this.txtSingleEmployee.AutoSize = false;
            this.txtSingleEmployee.BackColor = System.Drawing.SystemColors.Window;
            this.txtSingleEmployee.LinkItem = null;
            this.txtSingleEmployee.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSingleEmployee.LinkTopic = null;
            this.txtSingleEmployee.Location = new System.Drawing.Point(20, 90);
            this.txtSingleEmployee.Name = "txtSingleEmployee";
            this.txtSingleEmployee.Size = new System.Drawing.Size(80, 40);
            this.txtSingleEmployee.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtSingleEmployee, "Employee Number");
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(250, 284);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(4, 25);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "-";
            this.ToolTip1.SetToolTip(this.Label2, null);
            this.Label2.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(250, 224);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(4, 25);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "-";
            this.ToolTip1.SetToolTip(this.Label1, null);
            this.Label1.Visible = false;
            // 
            // lblDash
            // 
            this.lblDash.Location = new System.Drawing.Point(250, 164);
            this.lblDash.Name = "lblDash";
            this.lblDash.Size = new System.Drawing.Size(4, 25);
            this.lblDash.TabIndex = 19;
            this.lblDash.Text = "-";
            this.ToolTip1.SetToolTip(this.lblDash, null);
            this.lblDash.Visible = false;
            // 
            // lstTypes
            // 
            this.lstTypes.Appearance = 0;
            this.lstTypes.BackColor = System.Drawing.SystemColors.Window;
            this.lstTypes.CheckBoxes = true;
            this.lstTypes.GridLines = false;
            this.lstTypes.Location = new System.Drawing.Point(30, 440);
            this.lstTypes.MultiSelect = 0;
            this.lstTypes.Name = "lstTypes";
            this.lstTypes.Size = new System.Drawing.Size(404, 300);
            this.lstTypes.Sorted = false;
            this.lstTypes.Style = 1;
            this.lstTypes.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.lstTypes, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Print / Preview";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(147, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(158, 48);
            this.cmdPrint.TabIndex = 0;
            this.cmdPrint.Text = "Print / Preview";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmSumCustomCheckDetail
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(471, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmSumCustomCheckDetail";
            this.ShowInTaskbar = false;
            this.Text = "Check Detail Summary Rpt";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmSumCustomCheckDetail_Load);
            this.Activated += new System.EventHandler(this.frmSumCustomCheckDetail_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSumCustomCheckDetail_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
	}
}