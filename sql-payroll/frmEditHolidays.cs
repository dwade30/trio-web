﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEditHolidays : BaseForm
	{
		public frmEditHolidays()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditHolidays InstancePtr
		{
			get
			{
				return (frmEditHolidays)Sys.GetInstance(typeof(frmEditHolidays));
			}
		}

		protected frmEditHolidays _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		bool boolLoading;
		bool boolReturn;
		clsHoliday holReturn = new clsHoliday();

		public bool Init(ref clsHoliday thol)
		{
			bool Init = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Init = false;
				boolReturn = false;
				holReturn.CopyFrom(ref thol);
				this.Show(FCForm.FormShowEnum.Modal);
				if (boolReturn)
				{
					thol.CopyFrom(ref holReturn);
				}
				Init = boolReturn;
				return Init;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Init;
		}

		private void cmbDay_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				// Call RefillDay(cmbHolidayType.ItemData(cmbHolidayType.ListIndex), cmbDay.ListIndex + 1, cmbMonth.ListIndex + 1)
				clsHoliday thol = new clsHoliday();
				thol.MonthNum = cmbMonth.SelectedIndex + 1;
				thol.DayNum = cmbDay.SelectedIndex + 1;
				// thol.DayOfWeek = cmbDay.ListIndex + 1
				thol.DayOfWeek = cmbWeekDay.SelectedIndex + 1;
				thol.TypeofHoliday = (clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex);
				txtThisYear.Text = Strings.Format(thol.Get_DateFallsOn(DateTime.Today.Year), "MM/dd/yyyy");
			}
		}

		private void cmbHolidayType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				lblOf.Visible = true;
				lblLast.Visible = false;
				lblDay.Visible = false;
				if (cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex) == FCConvert.ToInt32(clsHoliday.HolidayType.SpecificDate))
				{
					RefillDay(clsHoliday.HolidayType.SpecificDate, cmbDay.SelectedIndex + 1, cmbMonth.SelectedIndex + 1);
					cmbWeekDay.Visible = false;
					cmbMonth.Visible = true;
					lblDay.Visible = true;
					cmbDay.Visible = true;
				}
				else if (cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex) == FCConvert.ToInt32(clsHoliday.HolidayType.NthWeekdayOfMonth))
				{
					cmbWeekDay.Visible = true;
					cmbMonth.Visible = true;
					lblDay.Visible = false;
					cmbDay.Visible = true;
					if (cmbDay.SelectedIndex < 5)
					{
						RefillDay(clsHoliday.HolidayType.NthWeekdayOfMonth, cmbDay.SelectedIndex + 1, cmbMonth.SelectedIndex + 1);
					}
					else
					{
						RefillDay(clsHoliday.HolidayType.NthWeekdayOfMonth, 1, cmbMonth.SelectedIndex + 1);
					}
				}
				else if (cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex) == FCConvert.ToInt32(clsHoliday.HolidayType.LastWeekdayOfMonth))
				{
					lblLast.Visible = true;
					cmbWeekDay.Visible = true;
					cmbMonth.Visible = true;
					lblDay.Visible = false;
					cmbDay.Visible = false;
				}
				else if (cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex) == FCConvert.ToInt32(clsHoliday.HolidayType.Easter))
				{
					lblOf.Visible = false;
					cmbWeekDay.Visible = false;
					lblDay.Visible = false;
					cmbDay.Visible = false;
					cmbMonth.Visible = false;
				}
				clsHoliday thol = new clsHoliday();
				thol.MonthNum = cmbMonth.SelectedIndex + 1;
				thol.DayNum = cmbDay.SelectedIndex + 1;
				// thol.DayOfWeek = cmbDay.ListIndex + 1
				thol.DayOfWeek = cmbWeekDay.SelectedIndex + 1;
				thol.TypeofHoliday = (clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex);
				txtThisYear.Text = Strings.Format(thol.Get_DateFallsOn(DateTime.Today.Year), "MM/dd/yyyy");
			}
		}

		public void cmbHolidayType_Click()
		{
			cmbHolidayType_SelectedIndexChanged(cmbHolidayType, new System.EventArgs());
		}

		private void cmbMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				RefillDay((clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex), cmbDay.SelectedIndex + 1, cmbMonth.SelectedIndex + 1);
				clsHoliday thol = new clsHoliday();
				thol.MonthNum = cmbMonth.SelectedIndex + 1;
				thol.DayNum = cmbDay.SelectedIndex + 1;
				// thol.DayOfWeek = cmbDay.ListIndex + 1
				thol.DayOfWeek = cmbWeekDay.SelectedIndex + 1;
				thol.TypeofHoliday = (clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex);
				txtThisYear.Text = Strings.Format(thol.Get_DateFallsOn(DateTime.Today.Year), "MM/dd/yyyy");
			}
		}

		private void cmbWeekDay_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				// Call RefillDay(cmbHolidayType.ItemData(cmbHolidayType.ListIndex), cmbDay.ListIndex + 1, cmbMonth.ListIndex + 1)
				clsHoliday thol = new clsHoliday();
				thol.MonthNum = cmbMonth.SelectedIndex + 1;
				thol.DayNum = cmbDay.SelectedIndex + 1;
				// thol.DayOfWeek = cmbDay.ListIndex + 1
				thol.DayOfWeek = cmbWeekDay.SelectedIndex + 1;
				thol.TypeofHoliday = (clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex);
				txtThisYear.Text = Strings.Format(thol.Get_DateFallsOn(DateTime.Today.Year), "MM/dd/yyyy");
			}
		}

		private void frmEditHolidays_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditHolidays_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditHolidays properties;
			//frmEditHolidays.FillStyle	= 0;
			//frmEditHolidays.ScaleWidth	= 5880;
			//frmEditHolidays.ScaleHeight	= 4140;
			//frmEditHolidays.LinkTopic	= "Form2";
			//frmEditHolidays.LockControls	= -1  'True;
			//frmEditHolidays.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoading = true;
			SetupControls();
			LoadInfo();
			boolLoading = false;
		}

		private void LoadInfo()
		{
			clsShiftTypeList ShiftList = new clsShiftTypeList();
			ShiftList.LoadTypes();
			ShiftList.MoveFirst();
			int intindex;
			int lngHIndex = 0;
			int lngSID;
			intindex = ShiftList.GetCurrentIndex();
			cmbShiftType.Clear();
			clsShiftType tShift;
			while (intindex >= 0)
			{
				tShift = ShiftList.GetCurrentShiftType();
				if (!(tShift == null))
				{
					cmbShiftType.AddItem(tShift.ShiftName);
					cmbShiftType.ItemData(cmbShiftType.NewIndex, tShift.ShiftID);
					if (tShift.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.Holiday))
					{
						lngHIndex = cmbShiftType.NewIndex;
					}
				}
				ShiftList.MoveNext();
				intindex = ShiftList.GetCurrentIndex();
			}
			if (holReturn.TypeofHoliday != clsHoliday.HolidayType.NotUsed)
			{
				this.Text = "Edit Holiday";
				txtName.Text = holReturn.HolidayName;
				int x;
				for (x = 0; x <= cmbHolidayType.Items.Count - 1; x++)
				{
					if (cmbHolidayType.ItemData(x) == FCConvert.ToInt32(holReturn.TypeofHoliday))
					{
						cmbHolidayType.SelectedIndex = x;
						break;
					}
				}
				// x
				if (holReturn.MonthNum > 0)
				{
					cmbMonth.SelectedIndex = holReturn.MonthNum - 1;
				}
				if (holReturn.DayNum > 0)
				{
					cmbDay.SelectedIndex = holReturn.DayNum - 1;
				}
				if (holReturn.DayOfWeek > 0)
				{
					cmbWeekDay.SelectedIndex = holReturn.DayOfWeek - 1;
				}
				if (holReturn.ShiftType > 0)
				{
					if (cmbShiftType.Items.Count > 0)
					{
						for (x = 0; x <= cmbShiftType.Items.Count - 1; x++)
						{
							if (cmbShiftType.ItemData(x) == holReturn.ShiftType)
							{
								cmbShiftType.SelectedIndex = x;
								break;
							}
						}
						// x
					}
				}
				else
				{
					if (lngHIndex > -1)
					{
						cmbShiftType.SelectedIndex = lngHIndex;
					}
					else if (cmbShiftType.Items.Count > 0)
					{
						cmbShiftType.SelectedIndex = 0;
					}
				}
				boolLoading = false;
				cmbHolidayType_Click();
			}
			else
			{
				this.Text = "Add Holiday";
				if (lngHIndex > -1)
				{
					cmbShiftType.SelectedIndex = lngHIndex;
				}
				else if (cmbShiftType.Items.Count > 0)
				{
					cmbShiftType.SelectedIndex = 0;
				}
			}
		}

		private void SetupControls()
		{
			string strTemp = "";
			cmbHolidayType.Clear();
			cmbHolidayType.AddItem("Specific Date");
			cmbHolidayType.ItemData(cmbHolidayType.NewIndex, FCConvert.ToInt32(clsHoliday.HolidayType.SpecificDate));
			cmbHolidayType.AddItem("Nth Weekday of Month");
			cmbHolidayType.ItemData(cmbHolidayType.NewIndex, FCConvert.ToInt32(clsHoliday.HolidayType.NthWeekdayOfMonth));
			cmbHolidayType.AddItem("Last Weekday of Month");
			cmbHolidayType.ItemData(cmbHolidayType.NewIndex, FCConvert.ToInt32(clsHoliday.HolidayType.LastWeekdayOfMonth));
			cmbHolidayType.AddItem("Easter");
			cmbHolidayType.ItemData(cmbHolidayType.NewIndex, FCConvert.ToInt32(clsHoliday.HolidayType.Easter));
			cmbHolidayType.SelectedIndex = 0;
			cmbWeekDay.Clear();
			cmbWeekDay.AddItem("Sunday");
			cmbWeekDay.AddItem("Monday");
			cmbWeekDay.AddItem("Tuesday");
			cmbWeekDay.AddItem("Wednesday");
			cmbWeekDay.AddItem("Thursday");
			cmbWeekDay.AddItem("Friday");
			cmbWeekDay.AddItem("Saturday");
			cmbWeekDay.SelectedIndex = 0;
			cmbMonth.Clear();
			cmbMonth.AddItem("January");
			cmbMonth.AddItem("February");
			cmbMonth.AddItem("March");
			cmbMonth.AddItem("April");
			cmbMonth.AddItem("May");
			cmbMonth.AddItem("June");
			cmbMonth.AddItem("July");
			cmbMonth.AddItem("August");
			cmbMonth.AddItem("September");
			cmbMonth.AddItem("October");
			cmbMonth.AddItem("November");
			cmbMonth.AddItem("December");
			cmbMonth.SelectedIndex = 0;
			RefillDay(clsHoliday.HolidayType.SpecificDate, 1, 1);
			clsHoliday thol = new clsHoliday();
			thol.MonthNum = (cmbMonth.SelectedIndex + 1);
			thol.DayNum = (cmbDay.SelectedIndex + 1);
			// thol.DayOfWeek = cmbDay.ListIndex + 1
			thol.DayOfWeek = cmbWeekDay.SelectedIndex + 1;
			thol.TypeofHoliday = (clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex);
			if (cmbShiftType.SelectedIndex >= 0)
			{
				thol.ShiftType = cmbShiftType.ItemData(cmbShiftType.SelectedIndex);
			}
			txtThisYear.Text = Strings.Format(thol.Get_DateFallsOn(DateTime.Today.Year), "MM/dd/yyyy");
		}
		// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
		private void RefillDay(clsHoliday.HolidayType intHType, int intDay, int intMonth)
		{
			int x;
			switch (intHType)
			{
				case clsHoliday.HolidayType.SpecificDate:
					{
						cmbDay.Clear();
						cmbDay.AddItem("1st");
						cmbDay.AddItem("2nd");
						cmbDay.AddItem("3rd");
						for (x = 4; x <= 20; x++)
						{
							cmbDay.AddItem(FCConvert.ToString(x) + "th");
						}
						// x
						cmbDay.AddItem("21st");
						cmbDay.AddItem("22nd");
						cmbDay.AddItem("23rd");
						cmbDay.AddItem("24th");
						cmbDay.AddItem("25th");
						cmbDay.AddItem("26th");
						cmbDay.AddItem("27th");
						cmbDay.AddItem("28th");
						x = 28;
						if (intMonth != 2)
						{
							for (x = 29; x <= 31; x++)
							{
								if (Information.IsDate(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(x) + "/" + FCConvert.ToString(DateTime.Today.Year)))
								{
									if (x < 31)
									{
										cmbDay.AddItem(FCConvert.ToString(x) + "th");
									}
									else
									{
										cmbDay.AddItem(FCConvert.ToString(x) + "st");
									}
								}
							}
							// x
						}
						if (intDay <= cmbDay.Items.Count)
						{
							cmbDay.SelectedIndex = intDay - 1;
						}
						else
						{
							cmbDay.SelectedIndex = 0;
						}
						break;
					}
				case clsHoliday.HolidayType.NthWeekdayOfMonth:
					{
						cmbDay.Clear();
						cmbDay.AddItem("1st");
						cmbDay.AddItem("2nd");
						cmbDay.AddItem("3rd");
						cmbDay.AddItem("4th");
						cmbDay.AddItem("5th");
						if (intDay <= cmbDay.Items.Count)
						{
							cmbDay.SelectedIndex = intDay - 1;
						}
						else
						{
							cmbDay.SelectedIndex = 0;
						}
						break;
					}
			}
			//end switch
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtName.Text) != string.Empty)
			{
				holReturn.TypeofHoliday = (clsHoliday.HolidayType)cmbHolidayType.ItemData(cmbHolidayType.SelectedIndex);
				holReturn.DayNum = (cmbDay.SelectedIndex + 1);
				holReturn.DayOfWeek = (cmbWeekDay.SelectedIndex + 1);
				holReturn.HolidayName = txtName.Text;
				holReturn.MonthNum = (cmbMonth.SelectedIndex + 1);
				if (cmbShiftType.SelectedIndex >= 0)
				{
					holReturn.ShiftType = cmbShiftType.ItemData(cmbShiftType.SelectedIndex);
				}
				boolReturn = true;
				Close();
			}
			else
			{
				MessageBox.Show("Holiday name cannot be blank", "Bad Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
