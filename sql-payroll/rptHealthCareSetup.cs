﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptHealthCareSetup.
	/// </summary>
	public partial class rptHealthCareSetup : BaseSectionReport
	{
		public rptHealthCareSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptHealthCareSetup InstancePtr
		{
			get
			{
				return (rptHealthCareSetup)Sys.GetInstance(typeof(rptHealthCareSetup));
			}
		}

		protected rptHealthCareSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				sEmployees?.Dispose();
                sEmployees = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptHealthCareSetup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cACASetupList lReportData;
		private int intpage;
		private cEmployeeService sEmployees = new cEmployeeService();

		public cACASetupList HealthCareReportObject
		{
			get
			{
				cACASetupList HealthCareReportObject = null;
				HealthCareReportObject = lReportData;
				return HealthCareReportObject;
			}
		}

		public void Init(ref cACASetupList lData)
		{
			lReportData = lData;
			lReportData.MoveFirst();
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !lReportData.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTitle.Text = "Employee Health Care Setup";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lReportData.IsCurrent())
			{
				cACAEmployeeSetup sData;
				cEmployee eEmp;
				sData = lReportData.GetCurrentDetail();
				if (!(sData == null))
				{
					txtEmployeeNumber.Text = sData.EmployeeNumber;
					eEmp = sEmployees.GetEmployeeByID(sData.EmployeeID);
					if (!(eEmp == null))
					{
						txtName.Text = eEmp.FullName;
					}
					else
					{
						txtName.Text = "";
					}
					lblCoverageDeclined.Visible = sData.CoverageDeclined;
					txtJanBox14.Text = sData.GetCoverageRequiredCodeForMonth(1);
					txtFebBox14.Text = sData.GetCoverageRequiredCodeForMonth(2);
					txtMarBox14.Text = sData.GetCoverageRequiredCodeForMonth(3);
					txtAprBox14.Text = sData.GetCoverageRequiredCodeForMonth(4);
					txtMayBox14.Text = sData.GetCoverageRequiredCodeForMonth(5);
					txtJunBox14.Text = sData.GetCoverageRequiredCodeForMonth(6);
					txtJulBox14.Text = sData.GetCoverageRequiredCodeForMonth(7);
					txtAugBox14.Text = sData.GetCoverageRequiredCodeForMonth(8);
					txtSepBox14.Text = sData.GetCoverageRequiredCodeForMonth(9);
					txtOctBox14.Text = sData.GetCoverageRequiredCodeForMonth(10);
					txtNovBox14.Text = sData.GetCoverageRequiredCodeForMonth(11);
					txtDecBox14.Text = sData.GetCoverageRequiredCodeForMonth(12);
					txtJanBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(1), "0.00");
					txtFebBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(2), "0.00");
					txtMarBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(3), "0.00");
					txtAprBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(4), "0.00");
					txtMayBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(5), "0.00");
					txtJunBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(6), "0.00");
					txtJulBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(7), "0.00");
					txtAugBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(8), "0.00");
					txtSepBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(9), "0.00");
					txtOctBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(10), "0.00");
					txtNovBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(11), "0.00");
					txtDecBox15.Text = Strings.Format(sData.GetEmployeeShareLowestPremiumForMonth(12), "0.00");
					txtJanBox16.Text = sData.GetSafeHarborCodeForMonth(1);
					txtFebBox16.Text = sData.GetSafeHarborCodeForMonth(2);
					txtMarBox16.Text = sData.GetSafeHarborCodeForMonth(3);
					txtAprBox16.Text = sData.GetSafeHarborCodeForMonth(4);
					txtMayBox16.Text = sData.GetSafeHarborCodeForMonth(5);
					txtJunBox16.Text = sData.GetSafeHarborCodeForMonth(6);
					txtJulBox16.Text = sData.GetSafeHarborCodeForMonth(7);
					txtAugBox16.Text = sData.GetSafeHarborCodeForMonth(8);
					txtSepBox16.Text = sData.GetSafeHarborCodeForMonth(9);
					txtOctBox16.Text = sData.GetSafeHarborCodeForMonth(10);
					txtNovBox16.Text = sData.GetSafeHarborCodeForMonth(11);
					txtDecBox16.Text = sData.GetSafeHarborCodeForMonth(12);
					if (sData.Dependents.Count > 0)
					{
						SubReport1.Report = new srptACAHealthCareDependents();
						(SubReport1.Report as srptACAHealthCareDependents).EmployeeSetup = sData;
					}
					else
					{
                        SubReport1.Report = null;
					}
				}
				lReportData.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}
