﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmGetScheduleTime : BaseForm
	{
		public frmGetScheduleTime()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGetScheduleTime InstancePtr
		{
			get
			{
				return (frmGetScheduleTime)Sys.GetInstance(typeof(frmGetScheduleTime));
			}
		}

		protected frmGetScheduleTime _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		string strReturn;

		public string Init(ref string strTime, bool boolHideOption = false)
		{
			string Init = "";
			strReturn = strTime;
			Init = strTime;
			if (boolHideOption)
			{
				cmbScheduled.Visible = false;
				//framScheduled.TopOriginal = optScheduled[1].TopOriginal;
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void frmGetScheduleTime_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetScheduleTime_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetScheduleTime properties;
			//frmGetScheduleTime.FillStyle	= 0;
			//frmGetScheduleTime.ScaleWidth	= 5880;
			//frmGetScheduleTime.ScaleHeight	= 4050;
			//frmGetScheduleTime.LinkTopic	= "Form2";
			//frmGetScheduleTime.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupControls();
			FillCmbAccount();
			LoadInfo();
		}

		private void SetupControls()
		{
			int x;
			cmbFromHour.Clear();
			for (x = 1; x <= 12; x++)
			{
				cmbFromHour.AddItem(x.ToString());
			}
			// x
			cmbFromHour.SelectedIndex = 0;
			cmbFromMinute.Clear();
			for (x = 0; x <= 59; x++)
			{
				cmbFromMinute.AddItem(Strings.Format(x, "00"));
			}
			// x
			cmbFromMinute.SelectedIndex = 0;
			cmbFromAMPM.Clear();
			cmbFromAMPM.AddItem("AM");
			cmbFromAMPM.AddItem("PM");
			cmbFromAMPM.SelectedIndex = 0;
			cmbToHour.Clear();
			for (x = 1; x <= 12; x++)
			{
				cmbToHour.AddItem(x.ToString());
			}
			// x
			cmbToHour.SelectedIndex = 0;
			cmbToMinute.Clear();
			for (x = 0; x <= 59; x++)
			{
				cmbToMinute.AddItem(Strings.Format(x, "00"));
			}
			// x
			cmbToMinute.SelectedIndex = 0;
			cmbToAMPM.Clear();
			cmbToAMPM.AddItem("AM");
			cmbToAMPM.AddItem("PM");
			cmbToAMPM.SelectedIndex = 0;
		}

		private string CreateFormat(string strType)
		{
			string CreateFormat = "";
			string strTemp = "";
			int counter = 0;
			if (strType == "E")
			{
				strTemp = modAccountTitle.Statics.Exp;
			}
			else if (strType == "R")
			{
				strTemp = modAccountTitle.Statics.Rev;
			}
			else if (strType == "G")
			{
				strTemp = modAccountTitle.Statics.Ledger;
			}
			else if (strType == "P")
			{
				strTemp = modNewAccountBox.Statics.PSegmentSize;
			}
			else if (strType == "V")
			{
				strTemp = modNewAccountBox.Statics.VSegmentSize;
			}
			else if (strType == "L")
			{
				strTemp = modNewAccountBox.Statics.LSegmentSize;
			}
			else
			{
				strTemp = "";
			}
			if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
				return CreateFormat;
			do
			{
				counter = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, 2))));
				strTemp = Strings.Right(strTemp, strTemp.Length - 2);
				if (counter > 0)
				{
					for (counter = counter; counter >= 1; counter--)
					{
						// If blnAllowLetters Then
						CreateFormat = CreateFormat + "A";
						// Else
						// CreateFormat = CreateFormat & "0"
						// End If
					}
					CreateFormat = CreateFormat + "-";
				}
			}
			while (strTemp != "");
			CreateFormat = Strings.Left(CreateFormat, CreateFormat.Length - 1);
			return CreateFormat;
		}

		private void FillCmbAccount()
		{
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				cmbAccount.AddItem(" ");
				cmbAccount.AddItem("E");
				cmbAccount.AddItem("G");
				cmbAccount.AddItem("R");
				
			}
			else
			{
				cmbAccount.AddItem(" ");
				cmbAccount.AddItem("M");
			}
		}

		private void gridAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 120)
			{
				KeyAscii -= 32;
			}
			else if (KeyAscii == 88 || KeyAscii == 8 || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
					KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void cmbAccount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (fecherFoundation.Strings.Trim(cmbAccount.Text) != "")
			{
				strTemp = CreateFormat(cmbAccount.Text);
				gridAccount.EditMask = strTemp;
				if (FCConvert.ToString(gridAccount.Tag) != fecherFoundation.Strings.Trim(cmbAccount.Text))
				{
					strTemp = Strings.Replace(strTemp, "A", "X", 1, -1, CompareConstants.vbTextCompare);
					gridAccount.TextMatrix(0, 0, strTemp);
				}
				gridAccount.Visible = true;
				gridAccount.Tag = (System.Object)(fecherFoundation.Strings.Trim(cmbAccount.Text));
			}
			else
			{
				gridAccount.TextMatrix(0, 0, "");
				gridAccount.Visible = false;
				gridAccount.Tag = (System.Object)("");
			}
		}

		private void LoadInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.UCase(strReturn) == "UNSCHEDULED")
				{
					framScheduled.Visible = false;
					cmbScheduled.Text = "Not Scheduled/Worked";
				}
				else
				{
					framScheduled.Visible = true;
					string[] strAry = null;
					string strTemp = "";
					strAry = Strings.Split(strReturn, ",", -1, CompareConstants.vbTextCompare);
					strTemp = strAry[0];
					if (FCConvert.ToDateTime(strTemp).Hour < 13)
					{
						cmbFromHour.Text = FCConvert.ToString(FCConvert.ToDateTime(strTemp).Hour);
						cmbFromAMPM.Text = "AM";
					}
					else
					{
						cmbFromHour.Text = FCConvert.ToString(FCConvert.ToDateTime(strTemp).Hour - 12);
						cmbFromAMPM.Text = "PM";
					}
					cmbFromMinute.Text = Strings.Format(FCConvert.ToDateTime(strTemp).Minute, "00");
					strTemp = strAry[1];
					if (FCConvert.ToDateTime(strTemp).Hour < 13)
					{
						if (FCConvert.ToDateTime(strTemp).Hour > 0)
						{
							cmbToHour.Text = FCConvert.ToString(FCConvert.ToDateTime(strTemp).Hour);
						}
						else
						{
							cmbToHour.Text = FCConvert.ToString(12);
						}
						cmbToAMPM.Text = "AM";
					}
					else
					{
						cmbToHour.Text = FCConvert.ToString(FCConvert.ToDateTime(strTemp).Hour - 12);
						cmbToAMPM.Text = "PM";
					}
					cmbToMinute.Text = Strings.Format(FCConvert.ToDateTime(strTemp).Minute, "00");
					txtLunchTime.Text = FCConvert.ToString(Conversion.Val(strAry[2]));
					strTemp = strAry[3];
					if (fecherFoundation.Strings.Trim(strTemp) != "")
					{
						cmbAccount.Text = Strings.Left(strTemp, 1);
						//App.DoEvents();
						gridAccount.TextMatrix(0, 0, Strings.Mid(strTemp, 3));
						gridAccount.Visible = true;
					}
					else
					{
						cmbAccount.Text = " ";
						gridAccount.TextMatrix(0, 0, "");
						gridAccount.Visible = false;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strFrom = "";
			string strTo = "";
			if (cmbScheduled.Text == "Scheduled/Worked")
			{
				strFrom = cmbFromHour.Text + ":" + cmbFromMinute.Text + " " + cmbFromAMPM.Text;
				strTo = cmbToHour.Text + ":" + cmbToMinute.Text + " " + cmbToAMPM.Text;
				strReturn = strFrom + "," + strTo + "," + FCConvert.ToString(Conversion.Val(txtLunchTime.Text));
				if (fecherFoundation.Strings.Trim(cmbAccount.Text) != "")
				{
					gridAccount.Col = -1;
					//App.DoEvents();
					strReturn += "," + cmbAccount.Text + " " + gridAccount.TextMatrix(0, 0);
				}
				else
				{
					strReturn += ",";
				}
			}
			else
			{
				strReturn = "Unscheduled";
			}
			Close();
		}

		private void optScheduled_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				framScheduled.Visible = true;
			}
			else
			{
				framScheduled.Visible = false;
			}
		}

		private void optScheduled_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbScheduled.SelectedIndex;
			optScheduled_CheckedChanged(index, sender, e);
		}
	}
}
