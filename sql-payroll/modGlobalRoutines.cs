//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Linq;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	public class modGlobalRoutines
	{
		//=========================================================
		public struct BLS3020
		{
			public int Report;
			public int WorksiteID;
			public int intMonth1;
			public int intMonth2;
			public int intMonth3;
			public double dblWages;
			public int intMonthID;
			public string strDescription;
			public string strWhere;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public BLS3020(int unusedParam)
			{
				this.Report = 0;
				this.WorksiteID = 0;
				this.intMonth1 = 0;
				this.intMonth2 = 0;
				this.intMonth3 = 0;
				this.dblWages = 0;
				this.intMonthID = 0;
				this.strDescription = String.Empty;
				this.strWhere = String.Empty;
			}
		};

		public struct CountedEmployees
		{
			public string strEmployeeNumber;
			public int intMonthApplied;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public CountedEmployees(int unusedParam)
			{
				this.strEmployeeNumber = String.Empty;
				this.intMonthApplied = 0;
			}
		};

		public struct FedStateFilingStatus
		{
			public string strData;
			public string boolType;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public FedStateFilingStatus(int unusedParam)
			{
				this.strData = String.Empty;
				this.boolType = String.Empty;
			}
		};

		public static object ForceFormToResize(ref FCForm FormName)
		{
			object ForceFormToResize = null;
			// If FormName.WindowState = vbMaximized Or FormName.WindowState = vbMinimized Then
			// Else
			// If FormName.Height > 15 Then
			// FormName.Height = FormName.Height - 15
			// End If
			// End If
			return ForceFormToResize;
		}
		// Public Function SetVerifyAdjustNeeded(Optional ByVal boolValue As Boolean = True, Optional ByVal boolReturn As Boolean = False) As Boolean
		// Dim rsData As New clsDRWrapper
		//
		// If boolReturn Then
		// Call rsData.OpenRecordset("Select * from tblFieldLengths")
		// If rsData.EndOfFile Then
		// SetVerifyAdjustNeeded = False
		// Else
		// SetVerifyAdjustNeeded = rsData.Fields("VerifyAdjustNeeded")
		// End If
		// Else
		// Call rsData.OpenRecordset("Select * from tblFieldLengths")
		// If rsData.EndOfFile Then
		// rsData.AddNew
		// Else
		// rsData.Edit
		// End If
		//
		// rsData.Fields("VerifyAdjustNeeded") = boolValue
		// rsData.Update
		//
		//
		// End If
		// End Function
		public static void ChangeFedStateTaxStatus()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				bool boolChangeMade;
				Statics.aryFedStateFilingStatus = new FedStateFilingStatus[1 + 1];
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}

		public static void MergeAdditionalW2Info()
		{
			clsDRWrapper rsOffline = new clsDRWrapper();
			clsDRWrapper rsLive = new clsDRWrapper();
			clsDRWrapper rsOfflineSetup = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			// ****************************************************************************************
			// DEDUCTIONS
			// ****************************************************************************************
			// CHECK TO MAKE SURE THE MERGE HAS NOT ALREADY BEEN DONE
			rsLive.OpenRecordset("Select * from tblW2Deductions Where DeductionNumber >=9000");
			if (rsLive.EndOfFile())
			{
				rsLive.OpenRecordset("Select * from tblW2Deductions");
				rsOffline.OpenRecordset("SELECT tblW2AdditionalInfoData.*, tblW2Box12And14.Box12, tblW2Box12And14.Box14 FROM tblW2AdditionalInfoData INNER JOIN tblW2Box12And14 ON tblW2AdditionalInfoData.AdditionalDeductionID = tblW2Box12And14.DeductionNumber Where Box12 = 1");
				rsOfflineSetup.OpenRecordset("SELECT * FROM tblW2AdditionalInfo");
				while (!rsOffline.EndOfFile())
				{
					// NOW CHECK TO SEE IF THERE IS A RECORD IN THE LIVE TABLE
					if (rsLive.FindFirstRecord2("EmployeeNumber,DeductionNumber", rsOffline.Get_Fields("EmployeeNumber") + "," + rsOffline.Get_Fields_Int32("AdditionalDeductionID"), ","))
					{
						rsLive.Edit();
					}
					else
					{
						rsLive.AddNew();
					}
					rsLive.Set_Fields("EmployeeNumber", rsOffline.Get_Fields("EmployeeNumber"));
					rsLive.Set_Fields("DeductionNumber", rsOffline.Get_Fields_Int32("AdditionalDeductionID"));
					rsLive.Set_Fields("CYTDAmount", rsOffline.Get_Fields("Amount"));
					if (rsOfflineSetup.FindFirstRecord("ID", rsOffline.Get_Fields_Int32("AdditionalDeductionID") - 9000))
					{
						rsLive.Set_Fields("ThirdPartySave", (FCConvert.ToBoolean(rsOfflineSetup.Get_Fields_Boolean("ThirdParty")) ? 1 : 0));
					}
					else
					{
						rsLive.Set_Fields("ThirdPartySave", false);
					}
					if (FCConvert.ToBoolean(rsOffline.Get_Fields_Boolean("Box12")))
					{
						rsLive.Set_Fields("Box12", 1);
					}
					else if (rsOffline.Get_Fields_Boolean("Box14"))
					{
						rsLive.Set_Fields("Box12", 0);
					}
					else
					{
						rsLive.Set_Fields("Box12", 1);
					}
					rsLive.Update();
					rsOffline.MoveNext();
				}
				// Call rsData.Execute("Update tblDefaultInformation SET W2MasterSaveDone = False", "TWPY0000.vb1")
				MessageBox.Show("Merge of additional W-2 BOX 12 information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("The merge of additional W-2 BOX 12 information has already been done. Cannot repeat process.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			// ****************************************************************************************
			// MATCHES
			// ****************************************************************************************
			// CHECK TO MAKE SURE THE MERGE HAS NOT ALREADY BEEN DONE
			rsLive.OpenRecordset("Select * from tblW2Matches Where DeductionNumber >=9000");
			if (rsLive.EndOfFile())
			{
				rsLive.OpenRecordset("Select * from tblW2Matches");
				rsOffline.OpenRecordset("SELECT tblW2AdditionalInfoData.*, tblW2Box12And14.Box12, tblW2Box12And14.Box14 FROM tblW2AdditionalInfoData INNER JOIN tblW2Box12And14 ON tblW2AdditionalInfoData.AdditionalDeductionID = tblW2Box12And14.DeductionNumber where Box14 = 1");
				rsOfflineSetup.OpenRecordset("SELECT * FROM tblW2AdditionalInfo");
				while (!rsOffline.EndOfFile())
				{
					// NOW CHECK TO SEE IF THERE IS A RECORD IN THE LIVE TABLE
					if (rsLive.FindFirstRecord2("EmployeeNumber,DeductionNumber", rsOffline.Get_Fields("EmployeeNumber") + "," + rsOffline.Get_Fields_Int32("AdditionalDeductionID"), ","))
					{
						rsLive.Edit();
					}
					else
					{
						rsLive.AddNew();
					}
					rsLive.Set_Fields("EmployeeNumber", rsOffline.Get_Fields("EmployeeNumber"));
					rsLive.Set_Fields("DeductionNumber", rsOffline.Get_Fields_Int32("AdditionalDeductionID"));
					rsLive.Set_Fields("CYTDAmount", rsOffline.Get_Fields("Amount"));
					if (rsOfflineSetup.FindFirstRecord("ID", rsOffline.Get_Fields_Int32("AdditionalDeductionID") - 9000))
					{
						rsLive.Set_Fields("ThirdPartySave", (FCConvert.ToBoolean(rsOfflineSetup.Get_Fields_Boolean("ThirdParty")) ? 1 : 0));
					}
					else
					{
						rsLive.Set_Fields("ThirdPartySave", false);
					}
					if (FCConvert.ToBoolean(rsOffline.Get_Fields_Boolean("Box12")))
					{
						rsLive.Set_Fields("Box14", 0);
					}
					else if (rsOffline.Get_Fields_Boolean("Box14"))
					{
						rsLive.Set_Fields("Box14", 1);
					}
					else
					{
						rsLive.Set_Fields("Box14", 1);
					}
					rsLive.Update();
					rsOffline.MoveNext();
				}
				// Call rsData.Execute("Update tblDefaultInformation SET W2MasterSaveDone = False", "TWPY0000.vb1")
				MessageBox.Show("Merge of additional W-2 BOX 14 information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("The merge of additional W-2 BOX 14 information has already been done. Cannot repeat process.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			// NOW ADJUST THE GROSS PAYS
			rsLive.OpenRecordset("Select * from tblW2EditTable");
			rsOffline.OpenRecordset("SELECT * FROM tblW2AdditionalInfoData");
			rsOfflineSetup.OpenRecordset("SELECT * FROM tblW2AdditionalInfo");
			while (!rsOffline.EndOfFile())
			{
				// FIND THE CORRECT SETUP RECORD SO THAT WE'LL KNOW IF THIS RECORD IS FED,FICA,MEDICARE,STATE
				if (rsOfflineSetup.FindFirstRecord("ID", rsOffline.Get_Fields_Int32("AdditionalDeductionID") - 9000))
				{
					if (rsLive.FindFirstRecord("EmployeeNumber", rsOffline.Get_Fields("EmployeeNumber")))
					{
						rsLive.Edit();
						if (FCConvert.ToBoolean(rsOfflineSetup.Get_Fields("Federal")))
						{
							rsLive.Set_Fields("FederalWage", rsLive.Get_Fields_Double("FederalWage") + FCConvert.ToDouble(rsOffline.Get_Fields("Amount")));
						}
						if (FCConvert.ToBoolean(rsOfflineSetup.Get_Fields("FICA")))
						{
							rsLive.Set_Fields("FICAWage", rsLive.Get_Fields("FICAWage") + FCConvert.ToDouble(rsOffline.Get_Fields("Amount")));
						}
						if (FCConvert.ToBoolean(rsOfflineSetup.Get_Fields_Boolean("Medicare")))
						{
							rsLive.Set_Fields("MedicareWage", rsLive.Get_Fields_Double("MedicareWage") + FCConvert.ToDouble(rsOffline.Get_Fields("Amount")));
                            if (rsOfflineSetup.Get_Fields_Boolean("ThirdParty") && ! rsLive.Get_Fields_Boolean("W23rdPartySickPay"))
                            {
                                rsLive.Set_Fields("W23rdPartySickPay", rsOfflineSetup.Get_Fields_Boolean("ThirdParty") );
                            }                        
                        }
						if (FCConvert.ToBoolean(rsOfflineSetup.Get_Fields("State")))
						{
							rsLive.Set_Fields("StateWage", rsLive.Get_Fields_Double("StateWage") + FCConvert.ToDouble(rsOffline.Get_Fields("Amount")));
						}
						rsLive.Update();
					}
				}
				rsOffline.MoveNext();
			}
		}
		// Public Sub SetBLS3020Data(ByVal strSQL As String, ByVal intStartMonth As Integer, ByVal clsMonth1 As clsDRWrapper, ByVal clsMonth2 As clsDRWrapper, ByVal clsMonth3 As clsDRWrapper, ByVal rsUnemployment As clsDRWrapper)
		// MATTHEW 9/30/2004
		// THE EMPLOYEE MASTER SCREEN NEEDS TO HAVE THE BLS SETUP FIELD SET UP
		// SO THAT IT IS IN THE FORMAT FIELD IT IS RECORDED AS REPORT NUMBER - WORKSITE NUMBER
		// THIS NEEDS TO HAVE THE DASH IN BETWEEN THE TWO FIELD NUMBERS AS THAT IS WHAT
		// I DO MY SPLIT ON.
		//
		// Dim rsExecute               As New clsDRWrapper
		// Dim rsData                  As New clsDRWrapper
		// Dim rsParameters            As New clsDRWrapper
		// Dim strWhere                As String
		// Dim intWorksiteID           As Integer
		// Dim intCounter              As Integer
		// Dim intMonth                As Integer
		// Dim strFinalWhere           As String
		// Dim boolSkip                As Boolean
		// Dim intIterations           As Integer
		// Dim strEmployeeNumber       As String
		// Dim boolFirstMonthFound     As Boolean
		// Dim boolSecondMonthFound    As Boolean
		// Dim boolThirdMonthFound     As Boolean
		// Dim intReport               As Integer
		// Dim strSplit
		//
		// REDIM THE ARRAYS
		// ReDim typMultipleWorksite(0)
		// ReDim typMultipleWorksite(5000)
		// ReDim aryCountedEmployees(0)
		//
		// CHECK TO SEE IF THIS REPORT IS RUN AT ALL
		// If UCase(Trim(gstrReportID)) = "NONE" Then Exit Sub
		//
		// OPEN COREY'S C1 RECORDSET SO THAT I'LL HAVE ALL OF THE EMPLOYEES THAT
		// WILL BE PAID FOR THIS QUARTER
		// Call rsData.OpenRecordset(strSQL, "TWPY0000.vb1")
		//
		// While Not rsData.EndOfFile
		// boolFirstMonthFound = False
		// boolSecondMonthFound = False
		// boolThirdMonthFound = False
		// strEmployeeNumber = rsData.Fields("C1Query5.EmployeeNumber")
		// strEmployeeNumber = rsData.Fields("EmployeeNumber")
		//
		// MAKE SURE THAT THEY ARE NOT SET TO BE UNEMPLOYMENT EXEMPT
		// If rsUnemployment.FindFirstRecord(, , "EmployeeNumber='" & strEmployeeNumber & "'") Then
		// If rsUnemployment.Fields("Unemployment") = "N" Then
		// boolSkip = True
		// Else
		// boolSkip = False
		// End If
		// Else
		// boolSkip = True
		// End If
		//
		// If boolSkip Then
		// Else
		// strSplit = Split(rsData.Fields("BLSWorksiteID"), "-")
		// If UBound(strSplit) = 0 Then
		// If strSplit(0) > 10 Then
		// GoTo NextEmployee
		// Else
		// intReport = strSplit(0)
		// intWorksiteID = 0
		// End If
		//
		// ElseIf UBound(strSplit) < 0 Then
		// intReport = 0
		// intWorksiteID = 0
		//
		// ElseIf UBound(strSplit) > 0 Then
		// intReport = strSplit(0)
		// intWorksiteID = strSplit(1)
		//
		// End If
		//
		// If intReport = Val(gstrReportID) Or UCase(gstrReportID) = "ALL" Then
		// this is the same report
		// If intWorksiteID > 0 Then
		// SET THE START MONTH SO THAT I CAN SET THE CAPTIONS ON THE REPORT
		// typMultipleWorksite(intWorksiteID).intMonthID = intStartMonth
		//
		// SET WHICH WORKSITE THIS WILL BE WITH
		// typMultipleWorksite(intWorksiteID).WorksiteID = intWorksiteID
		//
		// THIS REPORT HAS THREE MONTHS SO I NEED THE VALUES FOR EACH MONTH
		// For intMonth = intStartMonth To intStartMonth + 2
		// strFinalWhere = "Where EmployeeNumber = '" & strEmployeeNumber & "' AND Month(PayDate) = " & intMonth & " AND checkvoid = 0 AND (DistU <> 'N' AND DistU <> '')"
		// strFinalWhere = "Where EmployeeNumber = '" & strEmployeeNumber & "' AND Month(PayDate) = " & intMonth
		//
		// GET THE DATA FOR THIS EMPLOYEE FOR THE SELETED MONTH
		// Call rsExecute.OpenRecordset("SELECT Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay FROM tblCheckDetail " & strFinalWhere, "TWPY0000.vb1")
		// If rsExecute.EndOfFile Then
		// Else
		// If intMonth = intStartMonth Then
		// THIS IS THE FIRST MONTH
		// If clsMonth1.FindFirstRecord(, , "EmployeeNumber ='" & strEmployeeNumber & "'") Then
		// MAKE SURE THAT HIS EMPLOYEE ACTUALLY GOT PAID FOR THE FIRST MONTH OF THE QUARTER
		// If Val(rsExecute.Fields("SumOfDistGrossPay")) <> 0 Then
		// CHECK TO MAKE SURE THAT THIS EMPLOYEE HAS NOT ALREADY BEEN ADDED TO THE COUND
		// typMultipleWorksite(intWorksiteID).intMonth1 = typMultipleWorksite(intWorksiteID).intMonth1 + 1
		// End If
		// End If
		//
		// ElseIf intMonth = intStartMonth + 1 Then
		// this is the second month
		// If clsMonth2.FindFirstRecord(, , "EmployeeNumber ='" & strEmployeeNumber & "'") Then
		// MAKE SURE THAT HIS EMPLOYEE ACTUALLY GOT PAID FOR THE SECOND MONTH OF THE QUARTER
		// If Val(rsExecute.Fields("SumOfDistGrossPay")) <> 0 Then
		// CHECK TO MAKE SURE THAT THIS EMPLOYEE HAS NOT ALREADY BEEN ADDED TO THE COUND
		// typMultipleWorksite(intWorksiteID).intMonth2 = typMultipleWorksite(intWorksiteID).intMonth2 + 1
		// End If
		// End If
		//
		// ElseIf intMonth = intStartMonth + 2 Then
		// this is the third month
		// If clsMonth3.FindFirstRecord(, , "EmployeeNumber ='" & strEmployeeNumber & "'") Then
		// MAKE SURE THAT HIS EMPLOYEE ACTUALLY GOT PAID FOR THE THIRD MONTH OF THE QUARTER
		// If Val(rsExecute.Fields("SumOfDistGrossPay")) <> 0 Then
		// CHECK TO MAKE SURE THAT THIS EMPLOYEE HAS NOT ALREADY BEEN ADDED TO THE COUND
		// typMultipleWorksite(intWorksiteID).intMonth3 = typMultipleWorksite(intWorksiteID).intMonth3 + 1
		// End If
		// End If
		// End If
		//
		// ADD THE WAGE AMOUNT
		// typMultipleWorksite(intWorksiteID).dblWages = typMultipleWorksite(intWorksiteID).dblWages + Val(rsExecute.Fields("SumOfDistGrossPay"))
		//
		// End If
		// Next
		//
		// typMultipleWorksite(intWorksiteID).dblWages = typMultipleWorksite(intWorksiteID).dblWages + Val(rsData.Fields("QTDGrossPay"))
		// End If
		// End If
		// End If
		//
		// NextEmployee:
		// GOTO THE NEXT EMPLOYEE
		// rsData.MoveNext
		// Wend
		// End Sub
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static object CheckEmployeeNumber(string EmployeeNumber, int intMonth)
		{
			object CheckEmployeeNumber = null;
			// THIS FUNCTION WILL CHECK TO SEE
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			bool boolFound;
			boolFound = false;
			for (intCounter = 0; intCounter <= (Information.UBound(Statics.aryCountedEmployees, 1) - 1); intCounter++)
			{
				if (Statics.aryCountedEmployees[intCounter].strEmployeeNumber == EmployeeNumber && Statics.aryCountedEmployees[intCounter].intMonthApplied == intMonth)
				{
					boolFound = true;
					break;
				}
			}
			if (boolFound)
			{
				CheckEmployeeNumber = false;
			}
			else
			{
				Array.Resize(ref Statics.aryCountedEmployees, Information.UBound(Statics.aryCountedEmployees, 1) + 1 + 1);
				Statics.aryCountedEmployees[Information.UBound(Statics.aryCountedEmployees) - 1].strEmployeeNumber = EmployeeNumber;
				Statics.aryCountedEmployees[Information.UBound(Statics.aryCountedEmployees) - 1].intMonthApplied = intMonth;
				CheckEmployeeNumber = true;
			}
			return CheckEmployeeNumber;
		}

		public static Global.modBudgetaryAccounting.FundType[] CashAccountForM(ref modBudgetaryAccounting.FundType[] Accounts)
		{
			Global.modBudgetaryAccounting.FundType[] CashAccountForM = null;
			// this function will setup the cash account for any user that does not have the BD module
			// it will return an array of FundTypes with only one fund which is the default cash account
			try
			{
				// On Error GoTo ERROR_HANDLER
				fecherFoundation.Information.Err().Clear();
				modBudgetaryAccounting.FundType[] ftFunds = new modBudgetaryAccounting.FundType[99 + 1];
				// this will be the array that is returned
				int lngCT;
				// total number of accounts in the array
				int lngUB;
				// this is the upper bound of the array passed in
				lngUB = Information.UBound(Accounts, 1);
				// reset all of the funds
				FCUtils.EraseSafe(ftFunds);
				ftFunds[1].Account = "M CASH";
				ftFunds[1].Description = "CASH ACCOUNT";
				for (lngCT = 0; lngCT <= lngUB; lngCT++)
				{
					// check all of the accounts
					// check to see if there is an invalid account number
					if (Strings.InStr(1, Accounts[lngCT].Account, "_", CompareConstants.vbBinaryCompare) == 0 && Accounts[lngCT].Account.Length > 2)
					{
						// extract the fund number and add the amount
						ftFunds[1].Amount -= Accounts[lngCT].Amount;
					}
					else
					{
						// invalid account format
					}
				}
				CashAccountForM = ftFunds;
				return CashAccountForM;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + " has occured.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CashAccountForM;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object StripDashes(string strText)
		{
			object StripDashes = null;
			StripDashes = strText.Replace("-", "");
			return StripDashes;
		}

		public static void UpdateW2StatusData(string strType)
		{
			string strField = "";
			clsDRWrapper rsData = new clsDRWrapper();
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "SAVE")
			{
				strField = "SaveInformation";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "UPDATE")
			{
				strField = "UpdateInformation";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "CONTACT")
			{
				strField = "ContactInformation";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "EDITREPORT")
			{
				strField = "EditReport";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "PRINT")
			{
				strField = "PrintW2s";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "CREATEELECTRONIC")
			{
				strField = "CreateElectronicFile";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strType)) == "PRINTELECTRONIC")
			{
				strField = "PrintElectronicFile";
			}
			rsData.OpenRecordset("Select * from tblW2StatusTable", "TWPY0000.vb1");
			if (rsData.EndOfFile())
			{
				rsData.AddNew();
			}
			else
			{
				rsData.Edit();
			}
			rsData.Set_Fields(strField, DateTime.Now);
			rsData.Update();
		}
		// vbPorter upgrade warning: rptReport As object	OnWrite(rptW2Narrow2Up, rptW2Wide2Up, rptW2TF, rptW2Laser2x2, rptW2Laser, rptW2C, rptW3C)
		// vbPorter upgrade warning: 'Return' As object	OnWriteFCConvert.ToDouble(
		public static object SetW2HorizAdjustment(FCSectionReport rptReport)
		{
			object SetW2HorizAdjustment = null;
			clsDRWrapper rsData = new clsDRWrapper();
			double dblLaserAdjustment;
			double dblAdjust;
			// Call rsData.OpenRecordset("select * from tblcheckformat", "twpy0000.vb1")
			// If rsData.EndOfFile Then
			dblLaserAdjustment = 0;
			// Else
			// dblLaserAdjustment = CDbl(Val(rsData.Fields("w2horizontal")))
			// End If
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("W2Horizontal", "PY"))) != string.Empty)
			{
				dblLaserAdjustment = Conversion.Val(modRegistry.GetRegistryKey("W2Horizontal", "PY"));
			}
			dblAdjust = 120 * dblLaserAdjustment / 1440F;
			if (dblLaserAdjustment < 0)
			{
				if (dblAdjust + rptReport.PageSettings.Margins.Left < .25)
				{
					if (rptReport.PageSettings.Margins.Left >= .25)
					{
						rptReport.PageSettings.Margins.Left = .25F;
					}
				}
				else
				{
					rptReport.PageSettings.Margins.Left += FCConvert.ToSingle(dblAdjust);
				}
				dblAdjust = 0;
			}
			else if (dblLaserAdjustment > 0)
			{
				if (dblAdjust > .5)
					dblAdjust = .5;
				// cap at .5 in adjustment
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in rptReport.GetAllControls())
				{
					ControlName.Left += FCConvert.ToSingle(dblAdjust);
				}
			}
			SetW2HorizAdjustment = dblAdjust;
			return SetW2HorizAdjustment;
		}
		// vbPorter upgrade warning: rptReport As object	OnWrite(rptW3Regular, rptW2Narrow2Up, rptW2Wide2Up, rptW2TF, rptW3Laser, rptW2Laser, rptW2C, rptW3C)
		// vbPorter upgrade warning: 'Return' As object	OnWriteFCConvert.ToDouble(
		public static double SetW2LineAdjustment(FCSectionReport rptReport)
		{
			double SetW2LineAdjustment = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			double dblLaserLineAdjustment;
			// Call rsData.OpenRecordset("select * from tblCheckFormat", "twpy0000.vb1")
			// If rsData.EndOfFile Then
			dblLaserLineAdjustment = 0;
			// Else
			// dblLaserLineAdjustment = CDbl(Val(rsData.Fields("W2Alignment")))
			// End If
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("W2Alignment", "PY"))) != string.Empty)
			{
				dblLaserLineAdjustment = Conversion.Val(modRegistry.GetRegistryKey("W2Alignment", "PY"));
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in rptReport.GetAllControls())
			{
				ControlName.Top += FCConvert.ToSingle((200 * dblLaserLineAdjustment)) / 1440F;
			}
			SetW2LineAdjustment = dblLaserLineAdjustment;
			return SetW2LineAdjustment;
		}

		public static void UpdateTAAdjustmentAudit(DateTime gdatCurrentPayDate, int gintCurrentPayRun, int lngJournal, int lngWarrant, int intAccountingPer, int CurrentRecipient, double dblAmountToReduce, int lngCatID, int lngCheckNumber, string strAccount, string strDescription)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblTAAudit", "TWPY0000.vb1");
			rsData.AddNew();
			rsData.Set_Fields("CurrentPayDate", gdatCurrentPayDate);
			rsData.Set_Fields("CurrentPayRun", gintCurrentPayRun);
			rsData.Set_Fields("Journal", lngJournal);
			rsData.Set_Fields("Warrant", lngWarrant);
			rsData.Set_Fields("AccountingPer", intAccountingPer);
			rsData.Set_Fields("CurrentRecipient", CurrentRecipient);
			rsData.Set_Fields("Amount", dblAmountToReduce);
			rsData.Set_Fields("CategoryID", lngCatID);
			rsData.Set_Fields("CheckNumber", lngCheckNumber);
			rsData.Set_Fields("Account", strAccount);
			rsData.Set_Fields("Description", strDescription);
			rsData.Update();
		}
		// vbPorter upgrade warning: strData As string	OnWrite(string, int)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static string CheckForBlank(string strData)
		{
			if (fecherFoundation.Strings.Trim(strData) == string.Empty)
			{
				return "0.00";
			}
			else
			{
				return Strings.Format(strData, "#,##0.00");
			}
		}

		public static void CreateNetTotalAdjustmentRecords()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsDeductions = new clsDRWrapper();
				clsDRWrapper rsNetTotal = new clsDRWrapper();
				clsDRWrapper rsTaxes = new clsDRWrapper();
				double dblTotalTaxes = 0;
				double dblNewNet = 0;
				double dblDeductions = 0;
				int intCounter = 0;
				rsNetTotal.OpenRecordset("SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber, tblCheckDetail.NetPay, tblCheckDetail.GrossPay, tblCheckDetail.ID From tblCheckDetail Where (((tblCheckDetail.TotalRecord) = 1) And ((tblCheckDetail.AdjustRecord) = 0)) ORDER BY tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber;");
				rsDeductions.OpenRecordset("SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber, Sum(tblCheckDetail.DedAmount) AS SumOfDedAmount From tblCheckDetail Where (((tblCheckDetail.AdjustRecord) = 0) And ((tblCheckDetail.DeductionRecord) = 1)) GROUP BY tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber ORDER BY tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber;");
				rsTaxes.OpenRecordset("SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber, convert(double, tblCheckDetail.FederalTaxWH)+convert(double, tblCheckDetail.StateTaxWH)+convert(double, tblCheckDetail.FICATaxWH)+convert(double, tblCheckDetail.MedicareTaxWH) AS TotalTaxes From tblCheckDetail Where (((tblCheckDetail.TotalRecord) = 1) And ((tblCheckDetail.AdjustRecord) = 0)) ORDER BY tblCheckDetail.EmployeeNumber, tblCheckDetail.CheckNumber;");
				rsNetTotal.MoveLast();
				rsNetTotal.MoveFirst();
				MessageBox.Show(FCConvert.ToString(rsNetTotal.RecordCount()));
				while (!rsNetTotal.EndOfFile())
				{
					// If rsTotalRecord.Fields("EmployeeNumber") <> "429" Then GoTo NextNetRecord
					intCounter += 1;
					rsTaxes.FindFirstRecord2("EmployeeNumber,CheckNumber", rsNetTotal.Get_Fields("EmployeeNumber") + "," + rsNetTotal.Get_Fields("CheckNumber"), ",");
					if (rsTaxes.NoMatch)
					{
						dblTotalTaxes = 0;
					}
					else
					{
						dblTotalTaxes = rsTaxes.Get_Fields("TotalTaxes");
					}
					rsDeductions.FindFirstRecord2("EmployeeNumber,CheckNumber", rsNetTotal.Get_Fields("EmployeeNumber") + "," + rsNetTotal.Get_Fields("CheckNumber"), ",");
					if (rsDeductions.NoMatch)
					{
						dblDeductions = 0;
					}
					else
					{
						dblDeductions = rsDeductions.Get_Fields("SumOfDedAmount");
					}
					dblNewNet = Conversion.Val(rsNetTotal.Get_Fields("GrossPay")) - dblDeductions - dblTotalTaxes;
					if (dblNewNet != Conversion.Val(rsNetTotal.Get_Fields_Decimal("NetPay")))
					{
						rsNetTotal.Execute("Update tblCheckDetail Set NetPay = " + FCConvert.ToString(dblNewNet) + " Where ID = " + rsNetTotal.Get_Fields("ID"), "Payroll");
					}
					NextNetRecord:
					;
					rsNetTotal.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}
		// vbPorter upgrade warning: StartDate As DateTime	OnWrite(DateTime, string)
		// vbPorter upgrade warning: EndDate As DateTime	OnWrite(DateTime, string)
		// vbPorter upgrade warning: datTempDate As DateTime	OnWrite(DateTime, string)
		public static void GetDatesForAQuarter(ref DateTime StartDate, ref DateTime EndDate, DateTime datTempDate)
		{
			// takes a quarter and optional year.  If not specified, the current year is assumed
			// will return the beginning and ending dates of that quarter
			switch (datTempDate.Month)
			{
				case 1:
				case 2:
				case 3:
					{
						// jan - march
						StartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(datTempDate.Year));
						EndDate = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(datTempDate.Year));
						break;
					}
				case 4:
				case 5:
				case 6:
					{
						// april - june
						StartDate = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(datTempDate.Year));
						EndDate = FCConvert.ToDateTime("6/30/" + FCConvert.ToString(datTempDate.Year));
						break;
					}
				case 7:
				case 8:
				case 9:
					{
						// july - sept
						StartDate = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(datTempDate.Year));
						EndDate = FCConvert.ToDateTime("9/30/" + FCConvert.ToString(datTempDate.Year));
						break;
					}
				case 10:
				case 11:
				case 12:
					{
						// oct - dec
						StartDate = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(datTempDate.Year));
						EndDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(datTempDate.Year));
						break;
					}
			}
			//end switch
		}

		public static int GetMonthOfAQuarter(DateTime datTempDate)
		{
			int GetMonthOfAQuarter = 0;
			// takes a quarter and optional year.  If not specified, the current year is assumed
			// will return the beginning and ending dates of that quarter
			switch (datTempDate.Month)
			{
				case 1:
				case 2:
				case 3:
					{
						// jan - march
						if (datTempDate.Month == 1)
						{
							GetMonthOfAQuarter = 1;
						}
						else if (datTempDate.Month == 2)
						{
							GetMonthOfAQuarter = 2;
						}
						else if (datTempDate.Month == 3)
						{
							GetMonthOfAQuarter = 3;
						}
						break;
					}
				case 4:
				case 5:
				case 6:
					{
						// april - june
						if (datTempDate.Month == 4)
						{
							GetMonthOfAQuarter = 1;
						}
						else if (datTempDate.Month == 5)
						{
							GetMonthOfAQuarter = 2;
						}
						else if (datTempDate.Month == 6)
						{
							GetMonthOfAQuarter = 3;
						}
						break;
					}
				case 7:
				case 8:
				case 9:
					{
						// july - sept
						if (datTempDate.Month == 7)
						{
							GetMonthOfAQuarter = 1;
						}
						else if (datTempDate.Month == 8)
						{
							GetMonthOfAQuarter = 2;
						}
						else if (datTempDate.Month == 9)
						{
							GetMonthOfAQuarter = 3;
						}
						break;
					}
				case 10:
				case 11:
				case 12:
					{
						// oct - dec
						if (datTempDate.Month == 10)
						{
							GetMonthOfAQuarter = 1;
						}
						else if (datTempDate.Month == 11)
						{
							GetMonthOfAQuarter = 2;
						}
						else if (datTempDate.Month == 12)
						{
							GetMonthOfAQuarter = 3;
						}
						break;
					}
			}
			//end switch
			return GetMonthOfAQuarter;
		}

		public static void RunFullSetOfReports()
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
                string strDefaultPrinterName = string.Empty;
                
                modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet = true;
                frmFullSetofReports.InstancePtr.Init(strDefaultPrinterName);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 32755)
				{
					// cancel button was selected.
					fecherFoundation.Information.Err(ex).Clear();
					return;
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		public static string GetSchoolFicaMatchAccount()
		{
			string GetSchoolFicaMatchAccount = "";
			return GetSchoolFicaMatchAccount;
		}
		// vbPorter upgrade warning: strOriginalAcct As object	OnRead(string)
		public static string GetSchoolMedicareMatchAccount(ref object strOriginalAcct)
		{
			string GetSchoolMedicareMatchAccount = "";
			string strReturn;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strReturn = FCConvert.ToString(strOriginalAcct);
				GetSchoolMedicareMatchAccount = FCConvert.ToString(strOriginalAcct);
				GetSchoolMedicareMatchAccount = strReturn;
				return GetSchoolMedicareMatchAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetSchoolMedicareMatchAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetSchoolMedicareMatchAccount;
		}
		// Public Function CreateSchoolFicaMedEntries(dtPayDate As Date, ByVal intPYRun As Integer, Optional strTable As String = "tblTempPayProcess", Optional lngCheckNum As Long = 0) As Boolean
		// Dim strFICATaxAccount As String
		// Dim strMedicareTaxAccount As String
		// Dim strOriginalFICAAccount As String
		// Dim strOriginalMedAccount As String
		// Dim dblTotFICAWH As Double
		// Dim dblTotMedWH As Double
		// Dim dblTotEmployerFicaTax As Double
		// Dim dblTotEmployerMedicareTax As Double
		// Dim dblTotFICAGross As Double
		// Dim dblTotMedGross As Double
		// Dim dblFicaLeft As Double
		// Dim dblMedLeft As Double
		// Dim dblEmployerFicaLeft As Double
		// Dim dblEmployerMedicareLeft As Double
		// Dim dblFicaPercLeft As Double
		// Dim dblMedPercLeft As Double
		// Dim dblEmployerFicaPercLeft As Double
		// Dim dblEmployerMedicarePercLeft As Double
		// Dim strSQL As String
		// Dim rsLoad As New clsDRWrapper
		// Dim rsSave As New clsDRWrapper
		// Dim dblTemp As Double
		// Dim strTemp As String
		// Dim x As Integer
		// Dim intReturn As Integer
		// Dim intObj As Integer
		// Dim intCounter As Integer
		// Dim arySchoolObjects() As SchoolObjectCodesRec
		// Dim strObj As String
		// Dim strFund As String
		// Dim boolAllFica As Boolean
		// Dim boolAllMed As Boolean
		// Dim rsEmployee As New clsDRWrapper
		// Dim strTableToUse As String
		// Dim boolFicaMultiFund As Boolean
		// Dim boolMedMultiFund As Boolean
		//
		// On Error GoTo ErrorHandler
		//
		// strTableToUse = strTable
		// for now assume that we always want to vary the fund
		// boolFicaMultiFund = True
		// boolMedMultiFund = True
		// CreateSchoolFicaMedEntries = False
		//
		// Call rsLoad.OpenRecordset("select * from payrollschoolobjectcodes order by ID", "twpy0000.vb1")
		// intCounter = 0
		// ReDim arySchoolObjects(1) As SchoolObjectCodesRec
		// Do While Not rsLoad.EndOfFile
		// If intCounter > 1 Then
		// ReDim Preserve arySchoolObjects(intCounter) As SchoolObjectCodesRec
		// End If
		// With arySchoolObjects(intCounter)
		// .aryCodes(CNSTSCHOOLOBJECTCODEDESCRIPTION) = rsLoad.Fields("description")
		// .aryCodes(CNSTSCHOOLOBJECTCODEGROUPINS) = rsLoad.Fields("groupins")
		// .aryCodes(CNSTSCHOOLOBJECTCODEHEALTHBENEFITS) = rsLoad.Fields("HealthBenefits")
		// .aryCodes(CNSTSCHOOLOBJECTCODEONBEHALF) = rsLoad.Fields("OnBehalf")
		// .aryCodes(CNSTSCHOOLOBJECTCODEOTHERBENEFITS) = rsLoad.Fields("OtherBenefits")
		// .aryCodes(CNSTSCHOOLOBJECTCODEOTHERGROUPINS) = rsLoad.Fields("OtherIns")
		// .aryCodes(CNSTSCHOOLOBJECTCODERETIREMENT) = rsLoad.Fields("Retirement")
		// .aryCodes(CNSTSCHOOLOBJECTCODESALARY) = rsLoad.Fields("salary")
		// .aryCodes(CNSTSCHOOLOBJECTCODESSMED) = rsLoad.Fields("ssmed")
		// .aryCodes(CNSTSCHOOLOBJECTCODETUITION) = rsLoad.Fields("tuition")
		// .aryCodes(CNSTSCHOOLOBJECTCODEUNEMP) = rsLoad.Fields("unemployment")
		// .aryCodes(CNSTSCHOOLOBJECTCODEWC) = rsLoad.Fields("wc")
		// End With
		// intCounter = intCounter + 1
		// rsLoad.MoveNext
		// Loop
		//
		// If Not gboolTownAccounts Then
		// Call rsLoad.OpenRecordset("select * from tblPayrollAccounts where code = 'EM'", "twpy0000.vb1")
		// If Not rsLoad.EndOfFile Then
		// strOriginalMedAccount = rsLoad.Fields("account")
		// Else
		// Exit Function
		// End If
		// Call rsLoad.OpenRecordset("select * from tblpayrollaccounts where code = 'EF'", "twpy0000.vb1")
		// If Not rsLoad.EndOfFile Then
		// strOriginalFICAAccount = rsLoad.Fields("account")
		// Else
		// Exit Function
		// End If
		// Else
		// Call rsLoad.OpenRecordset("select * from payrollschoolAccounts where code = 'EM'", "twpy0000.vb1")
		// If Not rsLoad.EndOfFile Then
		// strOriginalMedAccount = rsLoad.Fields("account")
		// Else
		// Exit Function
		// End If
		// Call rsLoad.OpenRecordset("select * from payrollschoolaccounts where code = 'EF'", "twpy0000.vb1")
		// If Not rsLoad.EndOfFile Then
		// strOriginalFICAAccount = rsLoad.Fields("account")
		// Else
		// Exit Function
		// End If
		// End If
		//
		// Dim strEmp As String
		// If UCase(MuniName) <> "JAY" Then
		// If lngCheckNum = 0 Then
		// Call rsEmployee.OpenRecordset("select employeenumber from " & strTableToUse & " WHERE totalrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " group by employeenumber", "twpy0000.vb1")
		// Else
		// Call rsEmployee.OpenRecordset("select employeenumber from " & strTableToUse & " WHERE checknumber = " & lngCheckNum & " and totalrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " group by employeenumber", "twpy0000.vb1")
		// End If
		// Else
		// If lngCheckNum = 0 Then
		// Call rsEmployee.OpenRecordset("select employeenumber from " & strTableToUse & " WHERE totalrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " group by employeenumber", "twpy0000.vb1")
		// Else
		// Call rsEmployee.OpenRecordset("select employeenumber from " & strTableToUse & " WHERE checknumber = " & lngCheckNum & " and sequencenumber <> 10 and totalrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " group by employeenumber", "twpy0000.vb1")
		// End If
		//
		// End If
		// Do While Not rsEmployee.EndOfFile
		// strEmp = rsEmployee.Fields("employeenumber")
		// If UCase(MuniName) <> "JAY" Then
		// If lngCheckNum = 0 Then
		// rsLoad.OpenRecordset "SELECT SUM(FICATaxWH) as FICATaxTotal,sum(EMployerFicaTax) as EmployerFicaTaxTotal,sum(EMployerMedicareTax) as EmployerMedicareTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, sum(FicaTaxGross) as FICAGrossTotal,sum(MedicareTaxGross) as MedGrossTotal FROM " & strTableToUse & " WHERE totalrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'"
		// Else
		// rsLoad.OpenRecordset "SELECT SUM(FICATaxWH) as FICATaxTotal,sum(EMployerFicaTax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, sum(FicaTaxGross) as FICAGrossTotal,sum(MedicareTaxGross) as MedGrossTotal FROM " & strTableToUse & " WHERE checknumber = " & lngCheckNum & " and totalrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'"
		// End If
		// Else
		// If lngCheckNum = 0 Then
		// rsLoad.OpenRecordset "SELECT SUM(FICATaxWH) as FICATaxTotal,sum(EMployerFicaTax) as EmployerFicaTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, sum(FicaTaxGross) as FICAGrossTotal,sum(MedicareTaxGross) as MedGrossTotal FROM " & strTableToUse & " WHERE totalrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'"
		// Else
		// rsLoad.OpenRecordset "SELECT SUM(FICATaxWH) as FICATaxTotal,sum(EMployerFicaTax) as EmployerFicaTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, sum(FicaTaxGross) as FICAGrossTotal,sum(MedicareTaxGross) as MedGrossTotal FROM " & strTableToUse & " WHERE checknumber = " & lngCheckNum & " and totalrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'"
		// End If
		// End If
		//
		// boolAllFica = True
		// boolAllMed = True
		// If Not rsLoad.EndOfFile Then
		// Dim dblFicaPerc As Double
		// Dim dblMedPerc As Double
		// Dim dblEmployerFicaRate As Double
		// Dim dblEmployerMedicareRate As Double
		// Dim dblTax As Double
		// Dim dblMatchTax As Double
		//
		// dblTotFICAWH = Val(rsLoad.Fields("FicaTaxTotal"))
		// dblTotMedWH = Val(rsLoad.Fields("medicaretaxtotal"))
		// dblTotEmployerFicaTax = Val(rsLoad.Fields("EmployerFicaTaxTotal"))
		// dblTotEmployerMedicareTax = Val(rsLoad.Fields("EmployerMedicareTaxTotal"))
		// If UCase(MuniName) <> "JAY" Then
		// If lngCheckNum = 0 Then
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross from " & strTableToUse & " where distributionrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'"
		// Else
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross from " & strTableToUse & " where checknumber = " & lngCheckNum & " and distributionrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun
		// End If
		// Else
		// If lngCheckNum = 0 Then
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross from " & strTableToUse & " where distributionrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'"
		// Else
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross from " & strTableToUse & " where checknumber = " & lngCheckNum & " and distributionrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun
		// End If
		// End If
		// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
		// If Not rsLoad.EndOfFile Then
		// dblTotFICAGross = Val(rsLoad.Fields("ficagross"))
		// dblTotMedGross = Val(rsLoad.Fields("Medgross"))
		// Else
		// dblTotFICAGross = 0
		// dblTotMedGross = 0
		// End If
		// dblFicaLeft = dblTotFICAWH
		// dblMedLeft = dblTotMedWH
		// dblEmployerFicaLeft = dblTotEmployerFicaTax
		// dblEmployerMedicareLeft = dblTotEmployerMedicareTax
		// dblFicaPercLeft = 1
		// dblMedPercLeft = 1
		// dblEmployerFicaPercLeft = 1
		// dblEmployerMedicarePercLeft = 1
		// rsLoad.OpenRecordset "SELECT sum(FicaTaxGross) as FICAGrossTotal,sum(MedicareTaxGross) as MedGrossTotal FROM " & strtabletouse & " WHERE distributionrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intpyrun
		// If Not rsLoad.EndOfFile Then
		// If Val(rsLoad.Fields("ficagrosstotal")) <> dblTotFICAGross Then
		// boolAllFica = False
		// End If
		// If Val(rsLoad.Fields("medgrosstotal")) <> dblTotMedGross Then
		// boolAllMed = False
		// End If
		// End If
		// have totals now get subtotals for each salary code
		// If UCase(MuniName) <> "JAY" Then
		// If lngCheckNum = 0 Then
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross,distaccountnumber from " & strTableToUse & " where distributionrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'" & " group by distaccountnumber order by sum(ficataxgross)"
		// Else
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross,distaccountnumber from " & strTableToUse & " where checknumber = " & lngCheckNum & " and distributionrecord = 1 and fromschool = 1 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " group by distaccountnumber order by sum(ficataxgross)"
		// End If
		// Else
		// If lngCheckNum = 0 Then
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross,distaccountnumber from " & strTableToUse & " where distributionrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " and employeenumber = '" & strEmp & "'" & " group by distaccountnumber order by sum(ficataxgross)"
		// Else
		// strSQL = "select sum(ficataxgross) as ficagross,sum(medicaretaxgross) as medgross,distaccountnumber from " & strTableToUse & " where checknumber = " & lngCheckNum & " and distributionrecord = 1 and fromschool = 1 and sequencenumber <> 10 and PayDate = '" & dtPayDate & "' AND PayRunID = " & intPYRun & " group by distaccountnumber order by sum(ficataxgross)"
		// End If
		// End If
		// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
		// Call rsSave.OpenRecordset("select * from tbltemptax where id = -1", "twpy0000.vb1")
		// Do While Not rsLoad.EndOfFile
		// If Val(rsLoad.Fields("ficagross")) > 0 And dblFicaPercLeft > 0 And dblFicaLeft > 0 Then
		// dblFicaPerc = Format(Val(rsLoad.Fields("ficagross")) / dblTotFICAGross, "0.0000")
		// If dblFicaPerc > dblFicaPercLeft Then
		// dblFicaPerc = dblFicaPercLeft
		// End If
		// dblFicaPercLeft = dblFicaPercLeft - dblFicaPerc
		// dblTax = Format(dblFicaPerc * dblTotFICAWH, "0.00")
		// If dblTax > dblFicaLeft Then
		// dblTax = dblFicaLeft
		// End If
		// dblFicaLeft = dblFicaLeft - dblTax
		// dblMatchTax = Format(dblEmployerFicaRate * dblTotEmployerFicaTax, "0.00")
		// If dblMatchTax > dblEmployerFicaLeft Then
		// dblMatchTax = dblEmployerFicaLeft
		// End If
		// dblEmployerFicaLeft = dblEmployerFicaLeft - dblMatchTax
		// strFICATaxAccount = strOriginalFICAAccount
		// strFICATaxAccount = rsLoad.Fields("distaccountnumber")
		// If rsLoad.Fields("distaccountnumber") <> vbNullString Then
		// strObj = GetSchoolObject(rsLoad.Fields("distaccountnumber"))
		// strFund = GetSchoolFund(rsLoad.Fields("distaccountnumber"))
		//
		//
		// strTemp = ""
		// For x = 0 To UBound(arySchoolObjects)
		// If arySchoolObjects(x).aryCodes(CNSTSCHOOLOBJECTCODESALARY) = strObj Then
		// strTemp = arySchoolObjects(x).aryCodes(CNSTSCHOOLOBJECTCODESSMED)
		// Exit For
		// End If
		// Next x
		// If strTemp <> vbNullString Then
		// strFICATaxAccount = ChangeSchoolObject(strFICATaxAccount, strTemp)
		// End If
		// If boolFicaMultiFund Then
		// strFICATaxAccount = ChangeSchoolFund(strFICATaxAccount, strFund)
		// End If
		// End If
		// rsSave.AddNew
		// rsSave.Fields("DeptDiv") = strFICATaxAccount
		// rsSave.Fields("MedicareTax") = 0
		// rsSave.Fields("FicaTax") = dblTax
		// rsSave.Fields("EmployerFicaTax") = dblMatchTax
		// rsSave.Update
		// End If
		// If Val(rsLoad.Fields("Medgross")) > 0 And dblMedPercLeft > 0 And dblMedLeft > 0 Then
		// dblMedPerc = Format(Val(rsLoad.Fields("medgross")) / dblTotMedGross, "0.0000")
		// If dblMedPerc > dblMedPercLeft Then
		// dblMedPerc = dblMedPercLeft
		// End If
		// dblMedPercLeft = dblMedPercLeft - dblMedPerc
		// dblTax = Format(dblMedPerc * dblTotMedWH, "0.00")
		// If dblTax > dblMedLeft Then
		// dblTax = dblMedLeft
		// End If
		// dblMedLeft = dblMedLeft - dblTax
		// strMedicareTaxAccount = strOriginalMedAccount
		// dblMatchTax = Format(dblEmployerMedicareRate * dblTotEmployerMedicareTax, "0.00")
		// If dblMatchTax > dblEmployerMedicareLeft Then
		// dblMatchTax = dblEmployerMedicareLeft
		// End If
		// dblEmployerMedicareLeft = dblEmployerMedicareLeft - dblMatchTax
		// strMedicareTaxAccount = rsLoad.Fields("Distaccountnumber")
		// If rsLoad.Fields("distaccountnumber") <> vbNullString Then
		// strObj = GetSchoolObject(rsLoad.Fields("Distaccountnumber"))
		// strFund = GetSchoolFund(rsLoad.Fields("distaccountnumber"))
		//
		// strTemp = ""
		// For x = 0 To UBound(arySchoolObjects)
		// If arySchoolObjects(x).aryCodes(CNSTSCHOOLOBJECTCODESALARY) = strObj Then
		// strTemp = arySchoolObjects(x).aryCodes(CNSTSCHOOLOBJECTCODESSMED)
		// Exit For
		// End If
		// Next x
		// If strTemp <> vbNullString Then
		// strMedicareTaxAccount = ChangeSchoolObject(strMedicareTaxAccount, strTemp)
		// End If
		// If boolMedMultiFund Then
		// strMedicareTaxAccount = ChangeSchoolFund(strMedicareTaxAccount, strFund)
		// End If
		// End If
		// rsSave.AddNew
		// rsSave.Fields("DeptDiv") = strMedicareTaxAccount
		// rsSave.Fields("Medicaretax") = dblTax
		// rsSave.Fields("FicaTax") = 0
		// rsSave.Update
		// End If
		// rsLoad.MoveNext
		// Loop
		// if any left over put them in the last records accounts
		// If dblFicaLeft > 0 Or dblEmployerFicaLeft > 0 Then
		// rsSave.AddNew
		// rsSave.Fields("Deptdiv") = strFICATaxAccount
		// rsSave.Fields("MedicareTax") = 0
		// rsSave.Fields("EmployerMedicareTax") = 0
		// rsSave.Fields("FicaTax") = dblFicaLeft
		// rsSave.Fields("EMployerFicaTax") = dblEmployerFicaLeft
		// rsSave.Update
		// End If
		// If dblMedLeft > 0 Or dblEmployerMedicareLeft > 0 Then
		// rsSave.AddNew
		// rsSave.Fields("DeptDiv") = strMedicareTaxAccount
		// rsSave.Fields("MedicareTax") = dblMedLeft
		// rsSave.Fields("FicaTax") = 0
		// rsSave.Fields("EmployerFicaTax") = 0
		// rsSave.Fields("EmployerMedicareTax") = dblEmployerMedicareLeft
		// rsSave.Update
		// End If
		//
		// End If
		// rsEmployee.MoveNext
		// Loop
		// CreateSchoolFicaMedEntries = True
		// Exit Function
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In CreateSchoolFicaMedEntries", vbCritical, "Error"
		// End Function
		// vbPorter upgrade warning: intPYRun As int	OnWriteFCConvert.ToInt32(
		public static void SetupTempTaxTable(DateTime dtPayDate, int intPYRun, string strTable = "tblTempPayProcess", int lngCheckNum = 0)
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			// vbPorter upgrade warning: intDeptDivLength As int	OnWrite(double, int)
			int intDeptDivLength = 0;
			// vbPorter upgrade warning: intExpObjLength As int	OnWrite(double, int)
			int intExpObjLength = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: curMedicareTotal As Decimal	OnWrite(int, Decimal)
			Decimal curMedicareTotal;
			// vbPorter upgrade warning: curFICATotal As Decimal	OnWrite(int, Decimal)
			Decimal curFICATotal;
			// vbPorter upgrade warning: curFICADeductionTotal As Decimal	OnWrite(int, Decimal)
			Decimal curFICADeductionTotal;
			// vbPorter upgrade warning: curMedicareDeductionTotal As Decimal	OnWrite(int, Decimal)
			Decimal curMedicareDeductionTotal;
			double dblMedicareRate = 0;
			double dblFICARate = 0;
			double dblEmployerFicaRate = 0;
			clsDRWrapper rsAdjust = new clsDRWrapper();
			bool blnMedicareComplexBreakdown;
			bool blnFICAComplexBreakdown;
			string strFICATaxAccount = "";
			string strMedicareTaxAccount = "";
			clsDRWrapper rsSchoolAccounts = new clsDRWrapper();
			string strSchoolFicaAccount = "";
			string strSchoolMedicareAccount = "";
			bool boolSeparateAccounts;
			string strTableToUse;
			string strQuery1 = "";
			string strQuery2 = "";
			double dblEmployerMedicareRate = 0;
			strTableToUse = strTable;
			boolSeparateAccounts =false;
			// Clear out Temp table
			// rsAccountInfo.Execute "DELETE FROM tblTempTax"
			// Check if we need to do all these calculations for report
			blnFICAComplexBreakdown = false;
			blnMedicareComplexBreakdown = false;
			// If gboolTownAccounts Then
			// Calculate the length of the Department Division and Expense Object portions of the Expense Accounts
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				intDeptDivLength = FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1);
			}
			else
			{
				intDeptDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))));
			}
			if (!modAccountTitle.Statics.ObjFlag)
			{
				intExpObjLength = FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)) + 1);
			}
			else
			{
				intExpObjLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))));
			}
			rsAdjust.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'EF' OR Code = 'EM'");
			do
			{
				if (FCConvert.ToString(rsAdjust.Get_Fields("Code")) == "EF")
				{
					strFICATaxAccount = FCConvert.ToString(rsAdjust.Get_Fields("Account"));
				}
				else
				{
					strMedicareTaxAccount = FCConvert.ToString(rsAdjust.Get_Fields("Account"));
				}
				if (Strings.InStr(1, FCConvert.ToString(rsAdjust.Get_Fields("Account")), "X", CompareConstants.vbBinaryCompare) != 0 || fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "southwest harbor")
				{
					if (FCConvert.ToString(rsAdjust.Get_Fields("Code")) == "EF")
					{
						blnFICAComplexBreakdown = true;
					}
					else
					{
						blnMedicareComplexBreakdown = true;
					}
				}
				rsAdjust.MoveNext();
			}
			while (rsAdjust.EndOfFile() != true);
			if (!blnMedicareComplexBreakdown || !blnFICAComplexBreakdown)
			{
                
				if (lngCheckNum == 0)
				{
					rsAccountInfo.OpenRecordset("SELECT SUM(FICATaxWH) as FICATaxTotal,sum(employerficatax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal FROM " + strTableToUse + " WHERE TotalRecord = 1 and PayDate = '" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun));
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT SUM(FICATaxWH) as FICATaxTotal,sum(employerficatax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + " and TotalRecord = 1 and PayDate = '" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun));
				}
			
				if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
				{
					rsTemp.OpenRecordset("SELECT * FROM tblTempTax");
					if (!blnFICAComplexBreakdown)
					{
						rsTemp.AddNew();
						rsTemp.Set_Fields("DeptDiv", strFICATaxAccount);
						rsTemp.Set_Fields("MedicareTax", 0);
						rsTemp.Set_Fields("EmployerMedicareTax", 0);
						rsTemp.Set_Fields("FICATax", FCConvert.ToString(Conversion.Val(rsAccountInfo.Get_Fields("FICATaxTotal"))));
						rsTemp.Set_Fields("EmployerFicaTax", FCConvert.ToString(Conversion.Val(rsAccountInfo.Get_Fields("EmployerFicaTaxTotal"))));
						rsTemp.Update(true);
					}
					if (!blnMedicareComplexBreakdown)
					{
						rsTemp.AddNew();
						rsTemp.Set_Fields("DeptDiv", strMedicareTaxAccount);
						rsTemp.Set_Fields("MedicareTax", FCConvert.ToString(Conversion.Val(rsAccountInfo.Get_Fields("MedicareTaxTotal"))));
						rsTemp.Set_Fields("EmployerMedicareTax", FCConvert.ToString(Conversion.Val(rsAccountInfo.Get_Fields("Employermedicaretaxtotal"))));
						rsTemp.Set_Fields("FICATax", 0);
						rsTemp.Set_Fields("EmployerFicaTax", 0);
						rsTemp.Update(true);
					}
				}
				if (!blnFICAComplexBreakdown && !blnMedicareComplexBreakdown)
					return;
				// Exit Sub
			}
			// Get Sum of FICA Gross and Medicare Gross FROM Ditribution Records and Employer Match Records
			
		
			if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "southwest harbor")
			{
				if (lngCheckNum == 0)
				{
					strQuery1 = " (SELECT EmployeeNumber, Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE  distributionrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1)" + " = 'E' GROUP BY Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, " + strTableToUse + ".distaccountnumber as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE  " + strTableToUse + ".distributionrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1) <> 'E' GROUP BY " + strTableToUse + ".distaccountnumber, " + strTableToUse + ".EmployeeNumber) as TempTaxQuery ";
					strQuery2 = " (SELECT EmployeeNumber, Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE matchrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1)" + " = 'E' GROUP BY Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber," + strTableToUse + ".matchaccountnumber as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE " + strTableToUse + ".matchrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1) <> 'E' GROUP BY " + strTableToUse + ".matchaccountnumber, " + strTableToUse + ".EmployeeNumber) as TempTaxQuery2 ";
					rsAccountInfo.OpenRecordset("SELECT EmployeeNumber, DeptDiv, SUM(FICATotal) AS FICASumTotal, SUM(MedicareTotal) AS MedicareSumTotal FROM (SELECT * FROM " + strQuery1 + " UNION ALL SELECT * FROM " + strQuery2 + ") GROUP BY DeptDiv, EmployeeNumber");
				}
				else
				{
					strQuery1 = " (SELECT EmployeeNumber, Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + "  and distributionrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1)" + " = 'E' GROUP BY Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + "  and " + strTableToUse + ".distributionrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1) <> 'E' GROUP BY DeptDiv, " + strTableToUse + ".EmployeeNumber) as TempTaxQuery ";
					strQuery2 = " (SELECT EmployeeNumber, Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + " and matchrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1)" + " = 'E' GROUP BY Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE " + strTableToUse + ".checknumber = " + FCConvert.ToString(lngCheckNum) + " and " + strTableToUse + ".matchrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1) <> 'E' GROUP BY DeptDiv, " + strTableToUse + ".EmployeeNumber) as TempTaxQuery2 ";
					rsAccountInfo.OpenRecordset("SELECT EmployeeNumber, DeptDiv, SUM(FICATotal) AS FICASumTotal, SUM(MedicareTotal) AS MedicareSumTotal FROM (SELECT * FROM " + strQuery1 + " UNION ALL SELECT * FROM " + strQuery2 + ") GROUP BY DeptDiv, EmployeeNumber");
				}
			}
			else
			{
				if (lngCheckNum == 0)
				{
					// strQuery1 = " (SELECT EmployeeNumber, substring(DistAccountNumber,3," & intDeptDivLength & ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " & strTableToUse & " WHERE  isnull(distributionrecord,0) = 1 AND PayDate ='" & dtPayDate & "' AND PayRunID = " & intPYRun & " AND Left(DistAccountNumber,1)"
					// & " = 'E' GROUP BY Substring(DistAccountNumber,3," & intDeptDivLength & "), EmployeeNumber UNION SELECT " & strTableToUse & ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" & strTableToUse & ".FICATaxGross) as FICATotal, SUM(" & strTableToUse & ".MedicareTaxGross) as MedicareTotal FROM (" & strTableToUse & " INNER JOIN tblEmployeeMaster ON " & strTableToUse & ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE  isnull(" & strTableToUse & ".distributionrecord,0) = 1 AND " & strTableToUse & ".PayDate ='" & dtPayDate & "' AND " & strTableToUse & ".PayRunID = " & intPYRun & " AND Left(DistAccountNumber,1) <> 'E' GROUP BY DeptDiv, " & strTableToUse & ".EmployeeNumber) as TempTaxQuery "
					strQuery1 = " SELECT EmployeeNumber, substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE  isnull(distributionrecord,0) = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1)" + " = 'E' GROUP BY Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE  isnull(" + strTableToUse + ".distributionrecord,0) = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1) <> 'E' GROUP BY DeptDiv, " + strTableToUse + ".EmployeeNumber";
					// strQuery2 = " (SELECT EmployeeNumber, Substring(MatchAccountNumber,3," & intDeptDivLength & ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " & strTableToUse & " WHERE matchrecord = 1 AND PayDate ='" & dtPayDate & "' AND PayRunID = " & intPYRun & " AND Left(MatchAccountNumber,1)"
					// & " = 'E' GROUP BY Substring(MatchAccountNumber,3," & intDeptDivLength & "), EmployeeNumber UNION SELECT " & strTableToUse & ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" & strTableToUse & ".FICATaxGross) as FICATotal, SUM(" & strTableToUse & ".MedicareTaxGross) as MedicareTotal FROM (" & strTableToUse & " INNER JOIN tblEmployeeMaster ON " & strTableToUse & ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE " & strTableToUse & ".matchrecord = 1 AND " & strTableToUse & ".PayDate ='" & dtPayDate & "' AND " & strTableToUse & ".PayRunID = " & intPYRun & " AND Left(MatchAccountNumber,1) <> 'E' GROUP BY DeptDiv, " & strTableToUse & ".EmployeeNumber) as TempTaxQuery2 "
					strQuery2 = " SELECT EmployeeNumber, Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE isnull(matchrecord,0) = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1)" + " = 'E' GROUP BY Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE " + strTableToUse + ".matchrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1) <> 'E' GROUP BY DeptDiv, " + strTableToUse + ".EmployeeNumber ";
					// rsAccountInfo.OpenRecordset "SELECT EmployeeNumber, DeptDiv, SUM(FICATotal) AS FICASumTotal, SUM(MedicareTotal) AS MedicareSumTotal FROM (SELECT * FROM " & strQuery1 & " UNION ALL SELECT * FROM " & strQuery2 & ") GROUP BY DeptDiv, EmployeeNumber"
					rsAccountInfo.OpenRecordset("SELECT EmployeeNumber, DeptDiv, SUM(FICATotal) AS FICASumTotal, SUM(MedicareTotal) AS MedicareSumTotal FROM ( " + strQuery1 + " UNION ALL  " + strQuery2 + ") as tbl1 GROUP BY DeptDiv, EmployeeNumber");
				}
				else
				{
					// strQuery1 = " (SELECT EmployeeNumber, Substring(DistAccountNumber,3," & intDeptDivLength & ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " & strTableToUse & " WHERE checknumber = " & lngCheckNum & "  and distributionrecord = 1 AND PayDate ='" & dtPayDate & "' AND PayRunID = " & intPYRun & " AND Left(DistAccountNumber,1)"
					// & " = 'E' GROUP BY Substring(DistAccountNumber,3," & intDeptDivLength & "), EmployeeNumber UNION SELECT " & strTableToUse & ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" & strTableToUse & ".FICATaxGross) as FICATotal, SUM(" & strTableToUse & ".MedicareTaxGross) as MedicareTotal FROM (" & strTableToUse & " INNER JOIN tblEmployeeMaster ON " & strTableToUse & ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE checknumber = " & lngCheckNum & "  and " & strTableToUse & ".distributionrecord = 1 AND " & strTableToUse & ".PayDate ='" & dtPayDate & "' AND " & strTableToUse & ".PayRunID = " & intPYRun & " AND Left(DistAccountNumber,1) <> 'E' GROUP BY DeptDiv, " & strTableToUse & ".EmployeeNumber) as TempTaxQuery "
					strQuery1 = " SELECT EmployeeNumber, Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + "  and distributionrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1)" + " = 'E' GROUP BY Substring(DistAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + "  and " + strTableToUse + ".distributionrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(DistAccountNumber,1) <> 'E' GROUP BY DeptDiv, " + strTableToUse + ".EmployeeNumber";
					// strQuery2 = " (SELECT EmployeeNumber, Substring(MatchAccountNumber,3," & intDeptDivLength & ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " & strTableToUse & " WHERE checknumber = " & lngCheckNum & " and matchrecord = 1 AND PayDate ='" & dtPayDate & "' AND PayRunID = " & intPYRun & " AND Left(MatchAccountNumber,1)"
					// & " = 'E' GROUP BY Substring(MatchAccountNumber,3," & intDeptDivLength & "), EmployeeNumber UNION SELECT " & strTableToUse & ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" & strTableToUse & ".FICATaxGross) as FICATotal, SUM(" & strTableToUse & ".MedicareTaxGross) as MedicareTotal FROM (" & strTableToUse & " INNER JOIN tblEmployeeMaster ON " & strTableToUse & ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE " & strTableToUse & ".checknumber = " & lngCheckNum & " and " & strTableToUse & ".matchrecord = 1 AND " & strTableToUse & ".PayDate ='" & dtPayDate & "' AND " & strTableToUse & ".PayRunID = " & intPYRun & " AND Left(MatchAccountNumber,1) <> 'E' GROUP BY DeptDiv, " & strTableToUse & ".EmployeeNumber) as TempTaxQuery2 "
					strQuery2 = " SELECT EmployeeNumber, Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + ") AS DeptDiv, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + " and matchrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1)" + " = 'E' GROUP BY Substring(MatchAccountNumber,3," + FCConvert.ToString(intDeptDivLength) + "), EmployeeNumber UNION SELECT " + strTableToUse + ".EmployeeNumber, tblEmployeeMaster.DeptDiv as DeptDiv, SUM(" + strTableToUse + ".FICATaxGross) as FICATotal, SUM(" + strTableToUse + ".MedicareTaxGross) as MedicareTotal FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE " + strTableToUse + ".checknumber = " + FCConvert.ToString(lngCheckNum) + " and " + strTableToUse + ".matchrecord = 1 AND " + strTableToUse + ".PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND " + strTableToUse + ".PayRunID = " + FCConvert.ToString(intPYRun) + " AND Left(MatchAccountNumber,1) <> 'E' GROUP BY DeptDiv, " + strTableToUse + ".EmployeeNumber ";
					// rsAccountInfo.OpenRecordset "SELECT EmployeeNumber, DeptDiv, SUM(FICATotal) AS FICASumTotal, SUM(MedicareTotal) AS MedicareSumTotal FROM (SELECT * FROM " & strQuery1 & " UNION ALL SELECT * FROM " & strQuery2 & ") GROUP BY DeptDiv, EmployeeNumber"
					rsAccountInfo.OpenRecordset("SELECT EmployeeNumber, DeptDiv, SUM(FICATotal) AS FICASumTotal, SUM(MedicareTotal) AS MedicareSumTotal FROM ( " + strQuery1 + " UNION ALL  " + strQuery2 + ") as tbl1 GROUP BY DeptDiv, EmployeeNumber");
				}
			}
		
			// Insert this information into the table
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				rsTemp.OpenRecordset("SELECT * FROM tblTempTax");
				do
				{
					if (blnMedicareComplexBreakdown)
					{
						rsTemp.AddNew();
						rsTemp.Set_Fields("EmployeeNumber", rsAccountInfo.Get_Fields("EmployeeNumber"));
						rsTemp.Set_Fields("DeptDiv", rsAccountInfo.Get_Fields("DeptDiv"));
						rsTemp.Set_Fields("MedicareTotal", rsAccountInfo.Get_Fields("MedicareSumTotal"));
						rsTemp.Set_Fields("FICATotal", 0);
						rsTemp.Update(true);
					}
					if (blnFICAComplexBreakdown)
					{
						rsTemp.AddNew();
						rsTemp.Set_Fields("EmployeeNumber", rsAccountInfo.Get_Fields("EmployeeNumber"));
						rsTemp.Set_Fields("DeptDiv", rsAccountInfo.Get_Fields("DeptDiv"));
						rsTemp.Set_Fields("MedicareTotal", 0);
						rsTemp.Set_Fields("FICATotal", rsAccountInfo.Get_Fields("FICASumTotal"));
						rsTemp.Update(true);
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			// Subtract out deduction amounts
			if (lngCheckNum == 0)
			{
				rsAccountInfo.OpenRecordset("SELECT EmployeeNumber, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE deductionrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " GROUP BY EmployeeNumber HAVING (Sum(FICATaxGross) <> 0 OR Sum(MedicareTaxGross)<>0) ORDER BY EmployeeNumber");
			}
			else
			{
				rsAccountInfo.OpenRecordset("SELECT EmployeeNumber, Sum(FICATaxGross) AS FICATotal, Sum(MedicareTaxGross) AS MedicareTotal FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + " and deductionrecord = 1 AND PayDate ='" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " GROUP BY EmployeeNumber HAVING (Sum(FICATaxGross) <> 0 OR Sum(MedicareTaxGross)<>0) ORDER BY EmployeeNumber");
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					curFICATotal = 0;
					curMedicareTotal = 0;
					// calculate total gross for the employee
					rsTemp.OpenRecordset("SELECT * FROM tblTempTax WHERE isnull(EmployeeNumber, '') = '" + rsAccountInfo.Get_Fields("EmployeeNumber") + "' ORDER BY FICATotal, MedicareTotal");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						do
						{
							curFICATotal += rsTemp.Get_Fields_Decimal("FICATotal");
							curMedicareTotal += rsTemp.Get_Fields_Decimal("MedicareTotal");
							rsTemp.MoveNext();
						}
						while (rsTemp.EndOfFile() != true);
						rsTemp.MoveFirst();
					}
					// proportionally take out deductions from the different department division records for the current employee
					curFICADeductionTotal = 0;
					curMedicareDeductionTotal = 0;
					do
					{
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							rsTemp.Edit();
							if (blnMedicareComplexBreakdown && curMedicareTotal > 0)
							{
								curMedicareDeductionTotal += FCConvert.ToDecimal(FCUtils.Round((rsTemp.Get_Fields_Decimal("MedicareTotal") / curMedicareTotal) * rsAccountInfo.Get_Fields_Decimal("MedicareTotal"), 2));
							}
							else
							{
								curMedicareDeductionTotal = 0;
							}
							if (blnFICAComplexBreakdown && curFICATotal > 0)
							{
								curFICADeductionTotal += FCConvert.ToDecimal(FCUtils.Round((rsTemp.Get_Fields_Decimal("FICATotal") / curFICATotal) * rsAccountInfo.Get_Fields_Decimal("FICATotal"), 2));
							}
							else
							{
								curFICADeductionTotal = 0;
							}
							if (blnMedicareComplexBreakdown && curMedicareTotal > 0)
							{
								rsTemp.Set_Fields("MedicareTotal", rsTemp.Get_Fields_Decimal("MedicareTotal") + FCConvert.ToDecimal(FCUtils.Round((rsTemp.Get_Fields_Decimal("MedicareTotal") / curMedicareTotal) * rsAccountInfo.Get_Fields_Decimal("MedicareTotal"), 2)));
							}
							if (blnFICAComplexBreakdown && curFICATotal > 0)
							{
								rsTemp.Set_Fields("FICATotal", rsTemp.Get_Fields_Decimal("FICATotal") + FCConvert.ToDecimal(FCUtils.Round((rsTemp.Get_Fields_Decimal("FICATotal") / curFICATotal) * rsAccountInfo.Get_Fields_Decimal("FICATotal"), 2)));
							}
							rsTemp.Update(true);
							rsTemp.MoveNext();
						}
					}
					while (rsTemp.EndOfFile() != true);
					// because of rounding issues the amount we come up with may not match the total deduction so we place the leftover amount into the largest amount record for the employee
					if (blnFICAComplexBreakdown)
					{
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							rsTemp.MovePrevious();
							rsTemp.Edit();
							rsTemp.Set_Fields("FICATotal", rsTemp.Get_Fields_Decimal("FICATotal") + (rsAccountInfo.Get_Fields_Decimal("FICATotal") - curFICADeductionTotal));
							rsTemp.Update(true);
						}
					}
					if (blnMedicareComplexBreakdown)
					{
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							do
							{
								rsTemp.MovePrevious();
							}
							while (FCConvert.ToInt32(rsTemp.Get_Fields_Decimal("MedicareTotal")) == 0);
							rsTemp.Edit();
							rsTemp.Set_Fields("MedicareTotal", rsTemp.Get_Fields_Decimal("MedicareTotal") + (rsAccountInfo.Get_Fields_Decimal("MedicareTotal") - curMedicareDeductionTotal));
							rsTemp.Update(true);
						}
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			// Calculate Tax amounts for each employees distribution records
			rsTemp.OpenRecordset("SELECT * FROM tblStandardLimits");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				dblMedicareRate = rsTemp.Get_Fields_Double("PercentMedicare") / 100;
				dblFICARate = rsTemp.Get_Fields_Double("PercentSSN") / 100;
				dblEmployerFicaRate = rsTemp.Get_Fields_Double("EmployerFicaRate") / 100;
				dblEmployerMedicareRate = rsTemp.Get_Fields_Double("EmployerMedicareRate") / 100;
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM tblTempTax");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					rsAccountInfo.Edit();
					if (blnMedicareComplexBreakdown)
					{
						rsAccountInfo.Set_Fields("MedicareTax", FCUtils.Round(FCConvert.ToDouble(Conversion.Val(rsAccountInfo.Get_Fields_Decimal("MedicareTotal"))) * dblMedicareRate, 2));
						rsAccountInfo.Set_Fields("EmployerMedicareTax", FCUtils.Round(FCConvert.ToDouble(Conversion.Val(rsAccountInfo.Get_Fields_Decimal("MedicareTotal"))) * dblEmployerMedicareRate, 2));
					}
					if (blnFICAComplexBreakdown)
					{
						rsAccountInfo.Set_Fields("FICATax", FCUtils.Round(FCConvert.ToDouble(Conversion.Val(rsAccountInfo.Get_Fields_Decimal("FICATotal"))) * dblFICARate, 2));
						rsAccountInfo.Set_Fields("EmployerFicaTax", FCUtils.Round(FCConvert.ToDouble(Conversion.Val(rsAccountInfo.Get_Fields("FicaTotal"))) * dblEmployerFicaRate, 2));
					}
					rsAccountInfo.Update(true);
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			// Compare tax calculate per employee to total records tax if totals dont match add remainder to largest employee distribution amount
			
			if (lngCheckNum == 0)
			{
				rsTemp.OpenRecordset("SELECT EmployeeNumber, SUM(FICATaxWH) as TotalFICATaxWH,sum(EmployerFicaTax) as EmployerTotalFicaTax,sum(EmployerMedicareTax) as EmployerTotalMedicareTax, SUM(MedicareTaxWH) as TotalMedicareTaxWH FROM " + strTableToUse + " WHERE  TotalRecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " GROUP BY EmployeeNumber ORDER BY EmployeeNumber");
			}
			else
			{
				rsTemp.OpenRecordset("SELECT EmployeeNumber, SUM(FICATaxWH) as TotalFICATaxWH,sum(EmployerFicaTax) as EmployerTotalFicaTax,sum(EmployerMedicareTax) as EmployerTotalMedicareTax, SUM(MedicareTaxWH) as TotalMedicareTaxWH FROM " + strTableToUse + " WHERE checknumber = " + FCConvert.ToString(lngCheckNum) + "  and TotalRecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPYRun) + " GROUP BY EmployeeNumber ORDER BY EmployeeNumber");
			}
		
			rsAccountInfo.OpenRecordset("SELECT SUM(MedicareTax) as MedicareTaxTotal, SUM(FICATax) as FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EMployerMedicareTaxTotal, EmployeeNumber FROM tblTempTax GROUP BY EmployeeNumber ORDER BY EmployeeNumber");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true && rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					rsAccountInfo.FindFirstRecord("EmployeeNumber", rsTemp.Get_Fields("EmployeeNumber"));
					if (rsAccountInfo.NoMatch)
					{
						MessageBox.Show("ERROR");
					}
					else
					{
						if (Conversion.Val(rsTemp.Get_Fields("EmployerTotalMedicareTax")) != Conversion.Val(rsAccountInfo.Get_Fields("EmployerMedicareTaxTotal")) || Conversion.Val(rsTemp.Get_Fields("EmployerTotalFicaTax")) != Conversion.Val(rsAccountInfo.Get_Fields("EmployerFicaTaxTotal")) || Conversion.Val(rsTemp.Get_Fields("TotalFICATaxWH")) != Conversion.Val(rsAccountInfo.Get_Fields("FICATaxTotal")) || Conversion.Val(rsTemp.Get_Fields("TotalMedicareTaxWH")) != Conversion.Val(rsAccountInfo.Get_Fields("MedicareTaxTotal")))
						{
							if (blnMedicareComplexBreakdown && Conversion.Val(rsTemp.Get_Fields("TotalMedicareTaxWH")) != 0)
							{
								rsAdjust.OpenRecordset("SELECT TOP 1 * FROM tblTempTax WHERE isnull(EmployeeNumber, '') = '" + rsTemp.Get_Fields("EmployeeNumber") + "' ORDER BY MedicareTotal DESC");
								if (rsAdjust.EndOfFile() != true && rsAdjust.BeginningOfFile() != true)
								{
									rsAdjust.Edit();
									rsAdjust.Set_Fields("MedicareTax", Conversion.Val(rsAdjust.Get_Fields("MedicareTax")) + (rsTemp.Get_Fields("TotalMedicareTaxWH") - Conversion.Val(rsAccountInfo.Get_Fields("MedicareTaxTotal"))));
									rsAdjust.Update(true);
								}
							}
							else
							{
								rsAdjust.Execute("update tblTempTax set MedicareTax = 0 WHERE EmployeeNumber = '" + rsTemp.Get_Fields("EmployeeNumber") + "'", "Payroll");
							}
							if (blnFICAComplexBreakdown && Conversion.Val(rsTemp.Get_Fields("TotalFICATaxWH")) != 0)
							{
								rsAdjust.OpenRecordset("SELECT TOP 1 * FROM tblTempTax WHERE isnull(EmployeeNumber, '') = '" + rsTemp.Get_Fields("EmployeeNumber") + "' ORDER BY FICATotal DESC");
								if (rsAdjust.EndOfFile() != true && rsAdjust.BeginningOfFile() != true)
								{
									rsAdjust.Edit();
									rsAdjust.Set_Fields("FICATax", Conversion.Val(rsAdjust.Get_Fields("FICATax")) + (rsTemp.Get_Fields("TotalFICATaxWH") - Conversion.Val(rsAccountInfo.Get_Fields("FICATaxTotal"))));
									rsAdjust.Update(true);
								}
							}
							else
							{
								rsAdjust.Execute("update tblTempTax set FicaTax = 0 WHERE EmployeeNumber = '" + rsTemp.Get_Fields("EmployeeNumber") + "'", "Payroll");
							}
							if (blnFICAComplexBreakdown && Conversion.Val(rsTemp.Get_Fields("EmployerTotalFicaTax")) != 0)
							{
								rsAdjust.OpenRecordset("select top 1 * from tbltemptax where isnull(EmployeeNumber, '') = '" + rsTemp.Get_Fields("employeenumber") + "' order by ficatotal desc");
								if (rsAdjust.EndOfFile() != true && rsAdjust.BeginningOfFile() != true)
								{
									rsAdjust.Edit();
									rsAdjust.Set_Fields("EmployerFicaTax", Conversion.Val(rsAdjust.Get_Fields_Double("EmployerFicaTax")) + (rsTemp.Get_Fields("EmployerTotalFicaTax") - Conversion.Val(rsAccountInfo.Get_Fields("EmployerFicaTaxTotal"))));
									rsAdjust.Update(true);
								}
							}
							else
							{
								rsAdjust.Execute("update tbltemptax set EmployerFicaTax = 0 where employeenumber = '" + rsTemp.Get_Fields("employeenumber") + "'", "Payroll");
							}
							if (blnMedicareComplexBreakdown && Conversion.Val(rsTemp.Get_Fields("EmployerTotalMedicareTax")) != 0)
							{
								rsAdjust.OpenRecordset("select top 1 * from tbltemptax where employeenumber = '" + rsTemp.Get_Fields("employeenumber") + "' order by medicaretotal desc");
								if (rsAdjust.EndOfFile() != true && rsAdjust.BeginningOfFile() != true)
								{
									rsAdjust.Edit();
									rsAdjust.Set_Fields("EmployerMedicareTax", Conversion.Val(rsAdjust.Get_Fields_Double("EmployerMedicareTax")) + (rsTemp.Get_Fields("EmployerTotalMedicareTax") - Conversion.Val(rsAccountInfo.Get_Fields("EmployerMedicareTaxTotal"))));
									rsAdjust.Update(true);
								}
							}
							else
							{
								rsAdjust.Execute("update tbltemptax set EmployerMedicareTax = 0 where employeenumber = '" + rsTemp.Get_Fields("employeenumber") + "'", "Twpy0000.vb1");
							}
						}
					}
					rsTemp.MoveNext();
				}
				while (rsTemp.EndOfFile() != true);
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM tblTempTax");
			if (!(fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "southwest harbor"))
			{
				if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
				{
					do
					{
						rsAccountInfo.Edit();
						if (rsAccountInfo.Get_Fields_Decimal("MedicareTotal") != 0)
						{
							if (blnMedicareComplexBreakdown)
							{
								rsAccountInfo.Set_Fields("DeptDiv", "E " + rsAccountInfo.Get_Fields("DeptDiv") + "-" + Strings.Right(strMedicareTaxAccount, intExpObjLength));
							}
						}
						else
						{
							if (blnFICAComplexBreakdown)
							{
								rsAccountInfo.Set_Fields("DeptDiv", "E " + rsAccountInfo.Get_Fields("DeptDiv") + "-" + Strings.Right(strFICATaxAccount, intExpObjLength));
							}
						}
						rsAccountInfo.Update();
						rsAccountInfo.MoveNext();
					}
					while (rsAccountInfo.EndOfFile() != true);
				}
			}
			else
			{
				if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
				{
					do
					{
						rsAccountInfo.Edit();
						if (rsAccountInfo.Get_Fields_Decimal("MedicareTotal") != 0)
						{
							if (blnMedicareComplexBreakdown)
							{
								if (Strings.Left(rsAccountInfo.Get_Fields("deptdiv") + " ", 1) != "G")
								{
									rsAccountInfo.Set_Fields("DeptDiv", "E " + rsAccountInfo.Get_Fields("DeptDiv") + "-" + Strings.Right(strMedicareTaxAccount, intExpObjLength));
								}
							}
						}
						else
						{
							if (blnFICAComplexBreakdown)
							{
								if (Strings.Left(rsAccountInfo.Get_Fields("deptdiv") + " ", 1) != "G")
								{
									rsAccountInfo.Set_Fields("DeptDiv", "E " + rsAccountInfo.Get_Fields("DeptDiv") + "-" + Strings.Right(strFICATaxAccount, intExpObjLength));
								}
							}
						}
						rsAccountInfo.Update();
						rsAccountInfo.MoveNext();
					}
					while (rsAccountInfo.EndOfFile() != true);
				}
			}
		}
		// vbPorter upgrade warning: boolNoShow As object	OnWrite(bool)
		public static bool CheckIfPayProcessCompleted(string strPriorStep, string strNextStep, bool boolNoShow = false)
		{
			bool checkIfPayProcessCompleted = false;
			try
			{
                string strDescription = "";
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					checkIfPayProcessCompleted = false;
				}
				else
				{
					checkIfPayProcessCompleted = rsData.Get_Fields_Boolean(strPriorStep);
                }
				if (checkIfPayProcessCompleted)
                {
                    return true;
                }
				else
				{
					if (FCConvert.ToBoolean(boolNoShow))
                    {
                        return false;
                    }
					else
					{
                        FCMessageBox.Show(
                            "Step: " + strNextStep + " cannot be done until " + strPriorStep + " has been completed.",
                            MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
                    }
				}
				return checkIfPayProcessCompleted;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return checkIfPayProcessCompleted;
			}
		}

		public static void ClearAuditDataToArchive()
		{
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			clsDRWrapper rsAuditInfo = new clsDRWrapper();
			// If MsgBox("Are you sure you wish to Archive audit history?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
			rsArchiveInfo.OpenRecordset("SELECT * FROM AuditArchive where id = -1");
			rsAuditInfo.OpenRecordset("SELECT * FROM AuditHistory");
			if (rsAuditInfo.EndOfFile() != true && rsAuditInfo.BeginningOfFile() != true)
			{
				while (!rsAuditInfo.EndOfFile())
				{
					rsArchiveInfo.AddNew();
					rsArchiveInfo.Set_Fields("UserID", rsAuditInfo.Get_Fields("UserID"));
					rsArchiveInfo.Set_Fields("AuditArchiveDate", rsAuditInfo.Get_Fields_DateTime("AuditHistoryDate"));
					rsArchiveInfo.Set_Fields("AuditArchiveTime", rsAuditInfo.Get_Fields_DateTime("AuditHistoryTime"));
					rsArchiveInfo.Set_Fields("Description1", rsAuditInfo.Get_Fields_String("Description1"));
					rsArchiveInfo.Set_Fields("Description2", rsAuditInfo.Get_Fields_String("Description2"));
					rsArchiveInfo.Set_Fields("Description3", rsAuditInfo.Get_Fields_String("Description3"));
					rsArchiveInfo.Set_Fields("Description4", rsAuditInfo.Get_Fields_String("Description4"));
					rsArchiveInfo.Set_Fields("Description5", rsAuditInfo.Get_Fields_String("Description5"));
					rsArchiveInfo.Set_Fields("boolUseSecurity", rsAuditInfo.Get_Fields_Boolean("boolUseSecurity"));
					rsArchiveInfo.Set_Fields("EmployeeNumber", rsAuditInfo.Get_Fields("EmployeeNumber"));
					rsArchiveInfo.Set_Fields("PayDate", modGlobalVariables.Statics.gdatCurrentPayDate);
					rsArchiveInfo.Set_Fields("PayRunID", modGlobalVariables.Statics.gintCurrentPayRun);
					rsArchiveInfo.Update();
					// rsAuditInfo.Delete
					rsAuditInfo.MoveNext();
				}
				rsAuditInfo.Execute("delete from audithistory", "Payroll");
			}
			rsAuditInfo.OpenRecordset("SELECT * FROM AuditChanges", modGlobalVariables.DEFAULTDATABASE);
			rsArchiveInfo.OpenRecordset("SELECT * FROM AuditChangesArchive where id = -1", modGlobalVariables.DEFAULTDATABASE);
			if (rsAuditInfo.EndOfFile() != true && rsAuditInfo.BeginningOfFile() != true)
			{
				do
				{
					rsArchiveInfo.AddNew();
					rsArchiveInfo.Set_Fields("Location", rsAuditInfo.Get_Fields("Location"));
					rsArchiveInfo.Set_Fields("ChangeDescription", rsAuditInfo.Get_Fields_String("ChangeDescription"));
					rsArchiveInfo.Set_Fields("UserField1", rsAuditInfo.Get_Fields_String("UserField1"));
					rsArchiveInfo.Set_Fields("UserField2", rsAuditInfo.Get_Fields_String("UserField2"));
					rsArchiveInfo.Set_Fields("UserField3", rsAuditInfo.Get_Fields_String("UserField3"));
					rsArchiveInfo.Set_Fields("UserField4", rsAuditInfo.Get_Fields_String("UserField4"));
					rsArchiveInfo.Set_Fields("UserID", rsAuditInfo.Get_Fields("UserID"));
					rsArchiveInfo.Set_Fields("DateUpdated", rsAuditInfo.Get_Fields_DateTime("DateUpdated"));
					rsArchiveInfo.Set_Fields("TimeUpdated", rsAuditInfo.Get_Fields_DateTime("TimeUpdated"));
					rsArchiveInfo.Update();
					// rsAuditInfo.Delete
					rsAuditInfo.MoveNext();
				}
				while (rsAuditInfo.EndOfFile() != true);
				rsAuditInfo.Execute("delete from auditchanges", "Payroll");
			}
			// End If
		}

		public static void UpdatePayrollStepTable(string strStep, string strEmployeeNumber = "", string strDescription = "", DateTime? tempdatPayDate = null, int intPayRun = -1, cPayRun pRun = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdatPayDate == null)
			{
                //tempdatPayDate = DateTime.FromOADate(0);
                //FC:FINAl:SBE - initialize parameter with default value from original application
                tempdatPayDate = new DateTime(1200, 1, 1);
            }
			DateTime datPayDate = tempdatPayDate.Value;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "UpdatePayrollStepTable";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsData = new clsDRWrapper();
				if (datPayDate.ToOADate() == Convert.ToDateTime("01/01/1200").ToOADate())
					datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				if (intPayRun == -1)
					intPayRun = modGlobalVariables.Statics.gintCurrentPayRun;
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRun), modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					rsData.AddNew();
					rsData.Set_Fields("PayRunID", intPayRun);
					rsData.Set_Fields("PayDate", datPayDate);
					rsData.Set_Fields("WeekNumber", modGlobalVariables.Statics.gintWeekNumber);
					if (!(pRun == null))
					{
						if (Information.IsDate(pRun.PeriodStart))
						{
							rsData.Set_Fields("PeriodStartDate", pRun.PeriodStart);
						}
						if (Information.IsDate(pRun.PeriodEnd))
						{
							rsData.Set_Fields("WeekEndDate", pRun.PeriodEnd);
						}
					}
				}
				else
				{
					rsData.Edit();
					rsData.Set_Fields("WeekNumber", modGlobalVariables.Statics.gintWeekNumber);
				}
				if (strEmployeeNumber == string.Empty)
				{
					// update just the step
					UpdateStepFields(strStep, ref rsData);
				}
				else if (strEmployeeNumber != string.Empty)
				{
					// update the Employee and description
					if (fecherFoundation.Strings.UCase(strStep) == "DATAENTRY")
					{
						rsData.Set_Fields("EmployeeNumber", strEmployeeNumber);
						rsData.Set_Fields("Description", rsData.Get_Fields("Description") + ";" + strEmployeeNumber);
						rsData.Set_Fields("DescriptionDateTime", DateTime.Now);
					}
					else
					{
						rsData.Set_Fields("EmployeeNumber", strEmployeeNumber);
						rsData.Set_Fields("Description", strDescription);
						rsData.Set_Fields("DescriptionDateTime", DateTime.Now);
					}
				}
				// *****************************************************************************************
				// MATTHEW 4/14/2005
				// FOR CALL ID 66690
				if (Information.IsDate(datPayDate))
				{
					rsData.Set_Fields("ScheduleBDate", datPayDate);
				}
				// *****************************************************************************************
				rsData.Update();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public static void UpdateStepFields(string strStep, ref clsDRWrapper rsData)
		{
			// vbPorter upgrade warning: strFieldName As object	OnWrite(string())
			string[] strFieldName;
			int intCounter;
			//DAO.Field FieldName = new DAO.Field();
			// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
			int intCounter2;
			rsData.Set_Fields(strStep, true);
			rsData.Set_Fields(strStep + "UserID", modGlobalVariables.Statics.gstrUser);
			rsData.Set_Fields(strStep + "DateTime", DateTime.Now);
			rsData.Set_Fields("EmployeeNumber", 0);
			rsData.Set_Fields("Description", " ");
			rsData.Set_Fields("DescriptionDateTime", null);
			// now we need a way to clear out all functions after the
			// step that is being set. THIS IS UGLY AND WE NEED TO CHANGE THIS...MATTHEW 04/01/2003
			if (strStep == "FileSetup")
			{
				strStep = "FileSetup";
			}
			else if (strStep == "DataEntry")
			{
				strStep = "FileSetup:DataEntry";
			}
			else if (strStep == "EditTotals")
			{
				strStep = "FileSetup:DataEntry:EditTotals";
			}
			else if (strStep == "Calculation")
			{
				strStep = "FileSetup:DataEntry:EditTotals:Calculation";
			}
			else if (strStep == "VerifyAccept")
			{
				strStep = "FileSetup:DataEntry:EditTotals:Calculation:";
				strStep += "VerifyAccept:";
			}
			else if (strStep == "Checks")
			{
				strStep = "FileSetup:DataEntry:EditTotals:Calculation:";
				strStep += "VerifyAccept:Checks:";
				// Case "ChecksDirectDeposit"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// 
				// Case "CheckRegister"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:"
				// 
				// Case "PaySummary"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:"
				// 
				// Case "DeductionReports"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:"
				// 
				// Case "BankList"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// 
				// Case "TrustAgency"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:"
				// 
				// Case "AccountingSummary"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:"
				// 
				// Case "DEForms"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:"
				// 
				// Case "VacationReport"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:"
				// 
				// Case "SickReport"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:"
				// 
				// Case "Warrant"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:"
				// 
				// Case "AuditReport"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:"
				// 
				// Case "AuditClear"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:AuditClear:"
				// 
				// Case "MTDProcess"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:AuditClear:MTDProcess:"
				// 
				// Case "QTDProcess"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:AuditClear:MTDProcess:"
				// strStep = strStep & "QTDProcess:"
				// 
				// Case "YTDFiscalProcess"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:AuditClear:MTDProcess:"
				// strStep = strStep & "QTDProcess:YTDFiscalProcess:"
				// 
				// Case "YTDCalendarProcess"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:AuditClear:MTDProcess:"
				// strStep = strStep & "QTDProcess:YTDFiscalProcess:YTDCalendarProcess:"
				// 
				// Case "ResetTempData"
				// strStep = "FileSetup:DataEntry:EditTotals:Calculation:"
				// strStep = strStep & "VerifyAccept:Checks:ChecksDirectDeposit:"
				// strStep = strStep & "CheckRegister:PaySummary:DeductionReports:BankList:"
				// strStep = strStep & "TrustAgency:AccountingSummary:DEForms:VacationReport:SickReport:Warrant:AuditReport:AuditClear:MTDProcess:"
				// strStep = strStep & "QTDProcess:YTDFiscalProcess:YTDCalendarProcess:ResetTempData:"
			}
			strFieldName = Strings.Split(strStep, ":", -1, CompareConstants.vbBinaryCompare);
			// corey 6/23/03
			if ((strFieldName[Information.UBound(strFieldName, 1)] == "FileSetup") || (strFieldName[Information.UBound(strFieldName, 1)] == "DataEntry") || (strFieldName[Information.UBound(strFieldName, 1)] == "EditTotals") || (strFieldName[Information.UBound(strFieldName, 1)] == "Calculation") || (strFieldName[Information.UBound(strFieldName, 1)] == "VerifyAccept"))
			{
				for (intCounter = 0; intCounter <= rsData.FieldsCount - 1; intCounter++)
				{
					if (rsData.Get_FieldsIndexName(intCounter) == "ID")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "EmployeeNumber")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "Description")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "DescriptionDateTime")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "PayDate")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "PayRunID")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "WarrantNumber")
					{
					}
					else if (rsData.Get_FieldsIndexName(intCounter) == "JournalNumber")
					{
					}
					else if (Strings.Right(rsData.Get_FieldsIndexName(intCounter), 6) == "UserID")
					{
					}
					else if (Strings.Right(rsData.Get_FieldsIndexName(intCounter), 8) == "DateTime")
					{
					}
					else
					{
						string vbPorterVar = rsData.Get_FieldsIndexName(intCounter);
						if ((vbPorterVar == "FileSetup") || (vbPorterVar == "DataEntry") || (vbPorterVar == "EditTotals") || (vbPorterVar == "Calculation") || (vbPorterVar == "VerifyAccept") || (vbPorterVar == "Checks") || (vbPorterVar == "VerifyAccept"))
						{
							for (intCounter2 = 0; intCounter2 <= (Information.UBound(strFieldName, 1)); intCounter2++)
							{
								if (rsData.Get_FieldsIndexName(intCounter) == strFieldName[intCounter2])
								{
									goto EndLoop;
								}
							}
							// clear out these fields
							rsData.Set_Fields(rsData.Get_FieldsIndexName(intCounter), false);
							rsData.Set_Fields(rsData.Get_FieldsIndexName(intCounter) + "UserID", " ");
							rsData.Set_Fields(rsData.Get_FieldsIndexName(intCounter) + "DateTime", null);
						}
						else
						{
						}
					}
					EndLoop:
					;
				}
			}
		}

		public static bool ApplyThisEmployeeDeduction(int intCode, string strEmployeeNumber, string strType = "MAIN")
		{
			bool ApplyThisEmployeeDeduction = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ApplyThisEmployeeDeduction";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Dim rsLengths As New clsDRWrapper
				int intCount;
				int intWeekNumber;
				// With rsLengths
				// Call .OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" & strEmployeeNumber & "'", "TWPY0000.vb1")
				// ggintDedsUpTop = .Fields("PeriodDeds")
				// End With
				intWeekNumber = Statics.ggintWeekNumber;
				Statics.gintDeductionWeeks = 0;
                int weekLimit = 0;
				switch (intCode)
				{
					case 1:
						{
							// Weekly
							Statics.gintDeductionWeeks = Statics.ggintDedsUpTop;
							break;
						}
					case 2:
						{
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            // Week One Only
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if (strType == "DED" || strType == "MATCH")
								{
									if (Statics.ggboolBiWeekly)
									{
										// town is bi
										ApplyThisEmployeeDeduction = intWeekNumber == 1;
									}
									else
									{
										// town is weekly
										if (Statics.ggintFreqCodeID == 8)
										{
											// employee is bi
											if (Statics.ggboolSetupBiWeekly)
											{
												// This is from the customize screen as to if to pay weekly with bi-weekly
												ApplyThisEmployeeDeduction = (intWeekNumber == 1 || intWeekNumber == 2);
											}
											else
											{
												ApplyThisEmployeeDeduction = false;
											}
										}
										else
										{
											ApplyThisEmployeeDeduction = intWeekNumber == 1;
										}
									}
								}
								else
								{
									// THIS IS THE CHECK OF THE EMPLOYEE'S FREQ
									ApplyThisEmployeeDeduction = intWeekNumber == 1;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 3:
                    {
                        weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            // Week Two Only
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if (strType == "DED" || strType == "MATCH")
								{
									if (Statics.ggboolBiWeekly)
									{
										// town is bi
										ApplyThisEmployeeDeduction = intWeekNumber == 2;
									}
									else
									{
										// town is weekly
										if (Statics.ggintFreqCodeID == 8)
										{
											// employee is bi
											if (Statics.ggboolSetupBiWeekly)
											{
												// This is from the customize screen as to if to pay weekly with bi-weekly
												ApplyThisEmployeeDeduction = (intWeekNumber == 3 || intWeekNumber == 4);
											}
											else
											{
												ApplyThisEmployeeDeduction = false;
											}
										}
										else
										{
											ApplyThisEmployeeDeduction = intWeekNumber == 2;
										}
									}
								}
								else
								{
									// THIS IS THE CHECK OF THE EMPLOYEE'S FREQ
									ApplyThisEmployeeDeduction = intWeekNumber == 2;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 4:
						{
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            // Week Three Only
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if (intWeekNumber == 3)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 5:
						{
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            // Week Four Only
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if (intWeekNumber == 4)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 6:
						{
							// Yearly (when coded)
							Statics.gintDeductionWeeks = Statics.ggintDedsUpTop;
							break;
						}
					case 7:
						{
							// Monthly (when coded)
							Statics.gintDeductionWeeks = Statics.ggintDedsUpTop;
							break;
						}
					case 8:
						{
							// Bi-Weekly
							int intBiWeekWeekNumber = 0;
							if (Statics.ggboolBiWeekly)
							{
								// town is bi
								ApplyThisEmployeeDeduction = true;
								intBiWeekWeekNumber = 0;
							}
							else
							{
								// weekly town
								if (Statics.ggboolBiWeeklyDeds)
								{
									// take the bi ded's
									ApplyThisEmployeeDeduction = true;
									intBiWeekWeekNumber = 1;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
									intBiWeekWeekNumber = 2;
								}
							}
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if (intBiWeekWeekNumber == 0)
								{
									Statics.gintDeductionWeeks += 1;
								}
								else if (intBiWeekWeekNumber == 1)
								{
									Statics.gintDeductionWeeks += 1;
									intBiWeekWeekNumber = 2;
								}
								else if (intBiWeekWeekNumber == 2)
								{
									intBiWeekWeekNumber = 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 9:
						{
							// Hold
							Statics.gintDeductionWeeks = 0;
							break;
						}
					case 10:
						{
							// Hold (this week only)
							Statics.gintDeductionWeeks = 0;
							break;
						}
					case 11:
						{
							// Week 1-4, Not 5
                             weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
							for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if (Statics.ggboolBiWeekly)
								{
									ApplyThisEmployeeDeduction = intWeekNumber != 3;
								}
								else
								{
									ApplyThisEmployeeDeduction = intWeekNumber != 5;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 12:
						{
							// Default
							Statics.gintDeductionWeeks = Statics.ggintDedsUpTop;
							break;
						}
					case 13:
						{
                            // Quarterly
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 1 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 4 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 7 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 10) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 14:
						{
							// Semi-Annually (1,7)
							for (intCount = intWeekNumber; intCount <= intWeekNumber + Statics.ggintDedsUpTop - 1; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 1 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 7) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 15:
						{
                            // Semi-Annually (2,8)
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 2 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 8) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 16:
						{
                            // Semi-Annually (3,9)
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 3 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 9) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 17:
						{
                            // Semi-Annually (4,10)
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 4 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 10) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 18:
						{
                            // Semi-Annually (5,11)
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 5 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 11) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 19:
						{
                            // Semi-Annually (6,12)
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 6 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 12) && intWeekNumber == 1)
								{
									ApplyThisEmployeeDeduction = true;
								}
								else
								{
									ApplyThisEmployeeDeduction = false;
								}
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 20:
						{
                            // Week 1 and 3 only
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								ApplyThisEmployeeDeduction = (intWeekNumber == 1 || intWeekNumber == 3);
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 21:
						{
                            // Week 2 and 4 only
                            weekLimit = intWeekNumber + Statics.ggintDedsUpTop - 1;
                            for (intCount = intWeekNumber; intCount <= weekLimit; intCount++)
							{
								ApplyThisEmployeeDeduction = (intWeekNumber == 2 || intWeekNumber == 4);
								if (ApplyThisEmployeeDeduction)
								{
									Statics.gintDeductionWeeks += 1;
								}
								intWeekNumber += 1;
								if (intWeekNumber > Statics.ggintNumberPayWeeks)
									intWeekNumber = 1;
							}
							break;
						}
					case 22:
						{
							// Quarterly (when coded)
							Statics.gintDeductionWeeks = Statics.ggintDedsUpTop;
							break;
						}
				}
				//end switch
				if (Statics.gintDeductionWeeks > 0)
				{
					ApplyThisEmployeeDeduction = true;
				}
				else
				{
					ApplyThisEmployeeDeduction = false;
				}
				return ApplyThisEmployeeDeduction;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return ApplyThisEmployeeDeduction;
			}
		}

		public static bool ApplyThisEmployee(int intCode, string strEmployeeNumber, string strType = "MAIN", bool boolDontShowMsg = false)
		{
			bool ApplyThisEmployee = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ApplyThisEmployee";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Dim rsLengths As New clsDRWrapper
				// Dim boolWeekly As Boolean
				// Dim ggboolBiWeekly As Boolean
				// Dim ggboolSetupBiWeekly As Boolean
				// Dim ggboolPRBiWeekly As Boolean
				// Dim ggintWeekNumber As Integer
				// Dim intNumberPayWeeks As Integer
				// Dim ggboolBiWeeklyDeds As Boolean
				// 
				// With rsLengths
				// Call .OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1")
				// If Not rsLengths.EndOfFile Then
				// boolWeekly = IIf(.Fields("PayFreqWeekly"), True, False)
				// ggboolBiWeekly = IIf(.Fields("PayFreqBiWeekly"), True, False)
				// End If
				// 
				// Call .OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1")
				// If Not rsLengths.EndOfFile Then
				// ggboolPRBiWeekly = IIf(.Fields("WeeklyPR"), True, False)
				// End If
				// 
				// Call .OpenRecordset("Select * from tblPayrollProcessSetup")
				// If Not .EndOfFile Then
				// NOT SURE WHY I WAS ADDING ONE TO THIS FIELD. MATTHEW 5/17/03
				// ggintWeekNumber = .Fields("WeekNumber") + 1
				// intNumberPayWeeks = .Fields("NumberOfPayWeeks") + 1
				// 
				// ggintWeekNumber = .Fields("WeekNumber")
				// intNumberPayWeeks = .Fields("NumberOfPayWeeks")
				// ggboolSetupBiWeekly = IIf(.Fields("PayBiWeeklys"), True, False) 'pay run is to pay weeklys with bi's
				// ggboolBiWeeklyDeds = IIf(.Fields("BiWeeklyDeds"), True, False)
				// End If
				// 
				// Call .OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" & strEmployeeNumber & "'", "TWPY0000.vb1")
				// End With
				switch (intCode)
				{
					case 1:
						{
							// Weekly
							ApplyThisEmployee = true;
							break;
						}
					case 2:
						{
							// Week One Only
							if (strType == "DED" || strType == "MATCH")
							{
								if (Statics.ggboolBiWeekly)
								{
									// town is bi
									ApplyThisEmployee = Statics.ggintWeekNumber == 1;
								}
								else
								{
									// town is weekly
									if (Statics.ggintFreqCodeID == 8)
									{
										// employee is bi
										if (Statics.ggboolSetupBiWeekly)
										{
											// This is from the customize screen as to if to pay weekly with bi-weekly
											ApplyThisEmployee = (Statics.ggintWeekNumber == 1 || Statics.ggintWeekNumber == 2);
										}
										else
										{
											ApplyThisEmployee = false;
										}
									}
									else
									{
										ApplyThisEmployee = Statics.ggintWeekNumber == 1;
									}
								}
							}
							else
							{
								// THIS IS THE CHECK OF THE EMPLOYEE'S FREQ
								ApplyThisEmployee = Statics.ggintWeekNumber == 1;
							}
							break;
						}
					case 3:
						{
							// Week Two Only
							if (strType == "DED" || strType == "MATCH")
							{
								if (Statics.ggboolBiWeekly)
								{
									// town is bi
									ApplyThisEmployee = Statics.ggintWeekNumber == 2;
								}
								else
								{
									// town is weekly
									if (Statics.ggintFreqCodeID == 8)
									{
										// employee is bi
										if (Statics.ggboolSetupBiWeekly)
										{
											// This is from the customize screen as to if to pay weekly with bi-weekly
											ApplyThisEmployee = (Statics.ggintWeekNumber == 3 || Statics.ggintWeekNumber == 4);
										}
										else
										{
											ApplyThisEmployee = false;
										}
									}
									else
									{
										ApplyThisEmployee = Statics.ggintWeekNumber == 2;
									}
								}
							}
							else
							{
								// THIS IS THE CHECK OF THE EMPLOYEE'S FREQ
								ApplyThisEmployee = Statics.ggintWeekNumber == 2;
							}
							break;
						}
					case 4:
						{
							// Week Three Only
							ApplyThisEmployee = Statics.ggintWeekNumber == 3;
							break;
						}
					case 5:
						{
							// Week Four Only
							ApplyThisEmployee = Statics.ggintWeekNumber == 4;
							break;
						}
					case 6:
						{
							// Yearly (when coded)
							ApplyThisEmployee = true;
							break;
						}
					case 7:
						{
							// Monthly (when coded)
							ApplyThisEmployee = true;
							break;
						}
					case 8:
						{
							// Bi-Weekly
							// this is the field lengths screen
							if (strType == "DED" || strType == "MATCH")
							{
								if (Statics.ggboolBiWeekly)
								{
									// town is bi
									ApplyThisEmployee = true;
								}
								else
								{
									// weekly town
									if (Statics.ggboolBiWeeklyDeds)
									{
										// take the bi ded's
										ApplyThisEmployee = true;
									}
									else
									{
										ApplyThisEmployee = false;
									}
								}
							}
							else
							{
								if (Statics.ggboolBiWeekly)
								{
									// town is bi
									ApplyThisEmployee = true;
								}
								else
								{
									// the town is weekly
									if (Statics.ggboolSetupBiWeekly)
									{
										// this is the weekly and bi on the pay process setup screen and is true
										// add bi-weekly pays to this week
										ApplyThisEmployee = true;
									}
									else
									{
										if (!Statics.ggboolPRBiWeekly)
										{
											if (!boolDontShowMsg)
											{
												MessageBox.Show("Town is weekly. Employee " + strEmployeeNumber + " is coded as bi-weekly. This employee will not be paid.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
											}
											ApplyThisEmployee = false;
											return ApplyThisEmployee;
											// Else
											// ApplyThisEmployee = True
										}
									}
								}
							}
							break;
						}
					case 9:
						{
							// Hold
							ApplyThisEmployee = false;
							break;
						}
					case 10:
						{
							// Hold (this week only)
							ApplyThisEmployee = false;
							break;
						}
					case 11:
						{
							// Week 1-4, Not 5
							if (Statics.ggboolBiWeekly)
							{
								ApplyThisEmployee = Statics.ggintWeekNumber != 3;
							}
							else
							{
								ApplyThisEmployee = Statics.ggintWeekNumber != 5;
							}
							break;
						}
					case 12:
						{
							// Default
							ApplyThisEmployee = true;
							break;
						}
					case 13:
						{
							// Quarterly
							// MATTHEW 1/18/2004
							// ApplyThisEmployee = True
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 1 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 4 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 7 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 10) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 14:
						{
							// Semi-Annually (1,7)
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 1 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 7) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 15:
						{
							// Semi-Annually (2,8)
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 2 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 8) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 16:
						{
							// Semi-Annually (3,9)
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 3 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 9) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 17:
						{
							// Semi-Annually (4,10)
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 4 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 10) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 18:
						{
							// Semi-Annually (5,11)
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 5 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 11) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 19:
						{
							// Semi-Annually (6,12)
							if ((modGlobalVariables.Statics.gdatCurrentPayDate.Month == 6 || modGlobalVariables.Statics.gdatCurrentPayDate.Month == 12) && Statics.ggintWeekNumber == 1)
							{
								ApplyThisEmployee = true;
							}
							else
							{
								ApplyThisEmployee = false;
							}
							break;
						}
					case 20:
						{
							// Week 1 and 3 only
							// If ggboolBiWeekly Then
							// ApplyThisEmployee = ggintWeekNumber = 1
							// Else
							ApplyThisEmployee = (Statics.ggintWeekNumber == 1 || Statics.ggintWeekNumber == 3);
							// End If
							break;
						}
					case 21:
						{
							// Week 2 and 4 only
							// If ggboolBiWeekly Then
							// ApplyThisEmployee = ggintWeekNumber = 2
							// Else
							ApplyThisEmployee = (Statics.ggintWeekNumber == 2 || Statics.ggintWeekNumber == 4);
							// End If
							break;
						}
					case 22:
						{
							// Quarterly (when coded)
							ApplyThisEmployee = true;
							break;
						}
					case 23:
						{
							// semi-annually (when coded)
							ApplyThisEmployee = true;
							break;
						}
				}
				//end switch
				return ApplyThisEmployee;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return ApplyThisEmployee;
			}
		}
		// vbPorter upgrade warning: intMonthID As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object GetMonthDescription(int intMonthID)
		{
			object GetMonthDescription = null;
			switch (intMonthID)
			{
				case 1:
					{
						GetMonthDescription = "January";
						break;
					}
				case 2:
					{
						GetMonthDescription = "February";
						break;
					}
				case 3:
					{
						GetMonthDescription = "March";
						break;
					}
				case 4:
					{
						GetMonthDescription = "April";
						break;
					}
				case 5:
					{
						GetMonthDescription = "May";
						break;
					}
				case 6:
					{
						GetMonthDescription = "June";
						break;
					}
				case 7:
					{
						GetMonthDescription = "July";
						break;
					}
				case 8:
					{
						GetMonthDescription = "August";
						break;
					}
				case 9:
					{
						GetMonthDescription = "September";
						break;
					}
				case 10:
					{
						GetMonthDescription = "October";
						break;
					}
				case 11:
					{
						GetMonthDescription = "November";
						break;
					}
				case 12:
					{
						GetMonthDescription = "December";
						break;
					}
				default:
					{
						GetMonthDescription = "Unknown";
						break;
					}
			}
			//end switch
			return GetMonthDescription;
		}

		public static void ShowFirstFiscalMonth(ref FCTextBox ControlName)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (rsData.EndOfFile())
				{
					ControlName.Text = string.Empty;
				}
				else
				{
					ControlName.Text = FCConvert.ToString((ControlName.FindForm() as frmPayrollProcessSetup).GetMonth(FCConvert.ToInt32(rsData.Get_Fields_String("FiscalStart"))));
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					ControlName.Text = string.Empty;
				}
				else
				{
					// not sure why this is cboMonth Matthew 10/7/2003
					// ControlName.Text = ControlName.Parent.cboMonth.List(Val(rsData.Fields("FirstFiscalMonth")) - 1)
					// bug call id 5287
					// ControlName.Text = ControlName.Parent.cboFirstWeek.List(Val(rsData.Fields("FirstFiscalMonth")) - 1)
					ControlName.Text = FCConvert.ToString(GetMonthDescription(FCConvert.ToInt16(Conversion.Val(rsData.Get_Fields_Int16("FirstFiscalMonth")))));
				}
			}
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, string)
		// vbPorter upgrade warning: intLength As int	OnWrite(int, string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static string PadToString(int intValue, int intLength)
		{
			string PadToString = null;
			// converts an integer to string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, string)
		// vbPorter upgrade warning: intLength As int	OnWrite(int, string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static string PadToString(double intValue, int intLength)
		{
			string PadToString = null;
			// converts an integer to string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object AppendToString(string strValue, int intLength)
		{
			object AppendToString = null;
			// converts an integer to string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			AppendToString = strValue + Strings.StrDup(intLength - fecherFoundation.Strings.Trim(strValue).Length, " ");
			return AppendToString;
		}
		// vbPorter upgrade warning: lngPayCategory As int	OnWriteFCConvert.ToDouble(
		public static int GetNewTaxCode(int lngPayCategory)
		{
			int GetNewTaxCode = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("SELECT tblTaxStatusCodes.ID as TaxStatusID, tblTaxStatusCodes.TaxStatusCode, tblPayCategories.ID, tblPayCategories.CategoryNumber FROM tblPayCategories INNER JOIN tblTaxStatusCodes ON tblPayCategories.TaxStatus = tblTaxStatusCodes.ID WHERE tblPayCategories.ID = " + FCConvert.ToString(lngPayCategory));
				if (rsData.EndOfFile())
				{
					GetNewTaxCode = 0;
				}
				else
				{
					GetNewTaxCode = FCConvert.ToInt32(rsData.Get_Fields("TaxStatusID"));
				}
				return GetNewTaxCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetNewTaxCode;
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite
		public static string GetNewTaxStatusCode(int lngStatusCode)
		{
			string GetNewTaxStatusCode = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("SELECT * from tblTaxStatusCodes WHERE ID = " + FCConvert.ToString(lngStatusCode));
				if (rsData.EndOfFile())
				{
					GetNewTaxStatusCode = FCConvert.ToString(0);
				}
				else
				{
					GetNewTaxStatusCode = FCConvert.ToString(rsData.Get_Fields("TaxStatusCode"));
				}
				return GetNewTaxStatusCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetNewTaxStatusCode;
		}
		// vbPorter upgrade warning: lngDeductionID As int	OnWrite(string)
		// vbPorter upgrade warning: 'Return' As string	OnWrite
		public static string GetDeductionTaxStatusCode(int lngDeductionID)
		{
			string GetDeductionTaxStatusCode = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("SELECT * from tblDeductionSetup WHERE ID = " + FCConvert.ToString(lngDeductionID));
				if (rsData.EndOfFile())
				{
					GetDeductionTaxStatusCode = FCConvert.ToString(0);
				}
				else
				{
					rsData.OpenRecordset("SELECT * from tblTaxStatusCodes WHERE ID = " + rsData.Get_Fields("TaxStatusCode"));
					if (rsData.EndOfFile())
					{
						GetDeductionTaxStatusCode = FCConvert.ToString(0);
					}
					else
					{
						GetDeductionTaxStatusCode = FCConvert.ToString(rsData.Get_Fields("TaxStatusCode"));
					}
				}
				return GetDeductionTaxStatusCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetDeductionTaxStatusCode;
		}

		public static bool ValidToDelete(modGlobalVariables.gEnumCodeValidation intType, int intValue)
		{
			bool ValidToDelete = false;
			clsDRWrapper rsValidate = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string, DateTime)
			DateTime dtDate;
			DateTime dtDate2;
			switch (intType)
			{
				case (modGlobalVariables.gEnumCodeValidation)1:
					{
						// Frequency
						rsValidate.OpenRecordset("Select count(ID) as Total from tblDeductionSetup where FrequencyCode = " + FCConvert.ToString(intValue), "Twpy0000.vb1");
						if (rsValidate.Get_Fields_Decimal("Total") > 0)
						{
							MessageBox.Show("Frequency code " + FCConvert.ToString(intValue) + " exists in table Deduction Setup.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidToDelete;
						}
						rsValidate.OpenRecordset("Select count(ID) as Total from tblEmployeeDeductions where FrequencyCode = " + FCConvert.ToString(intValue), "Twpy0000.vb1");
						if (rsValidate.Get_Fields_Decimal("Total") > 0)
						{
							MessageBox.Show("Frequency code " + FCConvert.ToString(intValue) + " exists in table Employee Deductions.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidToDelete;
						}
						rsValidate.OpenRecordset("Select count(ID) as Total from tblRecipients where Freq = '" + FCConvert.ToString(intValue) + "'", "Twpy0000.vb1");
						if (rsValidate.Get_Fields_Decimal("Total") > 0)
						{
							MessageBox.Show("Frequency code " + FCConvert.ToString(intValue) + " exists in table Recipents.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidToDelete;
						}
						break;
					}
				case (modGlobalVariables.gEnumCodeValidation)2:
					{
						// Tax Status
						rsValidate.OpenRecordset("Select count(ID) as Total from tblDeductionSetup where TaxStatusCode = " + FCConvert.ToString(intValue), "Twpy0000.vb1");
						if (rsValidate.Get_Fields_Decimal("Total") > 0)
						{
							MessageBox.Show("Tax Status code " + FCConvert.ToString(intValue) + " exists in table Deduction Setup.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidToDelete;
						}
						// make work for deductions and employee match
						rsValidate.OpenRecordset("Select * from tblPayCategories where TaxStatus = " + FCConvert.ToString(intValue), "Twpy0000.vb1");
						if (!rsValidate.EndOfFile())
						{
							rsData.OpenRecordset("Select count(ID) as Total from tblPayrollDistribution where Cat = " + rsValidate.Get_Fields_Int32("CategoryNumber"), "Twpy0000.vb1");
							if (rsData.Get_Fields_Decimal("Total") > 0)
							{
								MessageBox.Show("Tax Status code " + GetNewTaxStatusCode(intValue) + " exists in table Payroll Distribution.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return ValidToDelete;
							}
						}
						rsValidate.OpenRecordset("Select * from tblDeductionSetup where TaxStatusCode = " + FCConvert.ToString(intValue), "Twpy0000.vb1");
						if (!rsValidate.EndOfFile())
						{
							rsData.OpenRecordset("Select count(ID) as Total from tblEmployeeDeductions where DeductionNumber = " + rsValidate.Get_Fields("TaxStatusCode"), "Twpy0000.vb1");
							if (rsData.Get_Fields_Decimal("Total") > 0)
							{
								MessageBox.Show("Tax Status code " + GetNewTaxStatusCode(rsValidate.Get_Fields("TaxStatusCode")) + " exists in table Employee Deductions.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return ValidToDelete;
							}
							rsData.OpenRecordset("Select count(ID) as Total from tblEmployersMatch where DeductionCode = " + rsValidate.Get_Fields("TaxStatusCode"), "Twpy0000.vb1");
							if (rsData.Get_Fields_Decimal("Total") > 0)
							{
								MessageBox.Show("Tax Status code " + GetNewTaxStatusCode(rsValidate.Get_Fields("TaxStatusCode")) + " exists in table Employers Match.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return ValidToDelete;
							}
						}
						rsValidate.OpenRecordset("Select count(ID) as Total from tblPayCategories where TaxStatus = " + FCConvert.ToString(intValue), "Twpy0000.vb1");
						if (rsValidate.Get_Fields_Decimal("Total") > 0)
						{
							MessageBox.Show("Tax Status code " + FCConvert.ToString(intValue) + " exists in table Pay Categories.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidToDelete;
						}
						break;
					}
				case (modGlobalVariables.gEnumCodeValidation)3:
					{
						// Pay Category
						dtDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
						dtDate2 = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
						if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, dtDate2) < 0)
						{
							dtDate = dtDate2;
						}
						rsValidate.OpenRecordset("select count(ID) as total from tblcheckdetail where distpaycategory = " + FCConvert.ToString(intValue) + " and paydate between '" + FCConvert.ToString(dtDate) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'", "twpy0000.vb1");
						if (Conversion.Val(rsValidate.Get_Fields_Decimal("Total")) > 0)
						{
							MessageBox.Show("Pay Category code " + FCConvert.ToString(intValue) + " has been used within the last fiscal/calendar year. Category cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidToDelete;
						}
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
			ValidToDelete = true;
			return ValidToDelete;
		}
		// vbPorter upgrade warning: intindex As int	OnWrite(double, int)
		public static void SetComboByIndex(ref FCComboBox ControlName, int intindex)
		{
			int intCounter;
			if (intindex == -1)
			{
				if (ControlName.Items.Count > 0)
				{
					ControlName.SelectedIndex = 0;
				}
			}
			else
			{
				for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
				{
					if (ControlName.ItemData(intCounter) == intindex)
					{
						ControlName.SelectedIndex = intCounter;
						break;
					}
				}
			}
		}

		public static void SetComboByName(ref FCComboBox ControlName, string strIndex)
		{
			int intCounter;
			if (strIndex == string.Empty)
			{
				ControlName.SelectedIndex = 0;
			}
			else
			{
				for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
				{
					if (Strings.Left(ControlName.Items[intCounter].ToString(), 2) == strIndex)
					{
						ControlName.SelectedIndex = intCounter;
						break;
					}
				}
			}
		}
		// vbPorter upgrade warning: ControlName As Control	OnWrite(VB.Label)
		public static void SetEmployeeCaption(Control ControlName, string intEmpNum)
		{
			SetCurrentEmployee(intEmpNum);
			if (intEmpNum != "0" && intEmpNum != "")
			{
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID != "0" && modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID != "")
				{
					ControlName.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "    " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				}
				else
				{
					ControlName.Text = "Employee 0";
				}
			}
			else
			{
				ControlName.Text = "Employee 0";
			}
			// Dim rsName As New clsDRWrapper
			// 
			// Call rsName.OpenRecordset("Select middlename,FirstName,LastName,Desig from tblEmployeeMaster where Employeenumber = '" & intEmpNum & "'", "Twpy0000.vb1")
			// If Not rsName.EndOfFile Then
			// gtypeCurrentEmployee.EmployeeLastName = rsName.Fields("LastName")
			// gtypeCurrentEmployee.EmployeeFirstName = rsName.Fields("FirstName")
			// gtypeCurrentEmployee.EmployeeMiddleName = rsName.Fields("middlename")
			// gtypeCurrentEmployee.EmployeeDesig = rsName.Fields("Desig")
			// gtypeCurrentEmployee.ParentID = intEmpNum
			// 
			// With gtypeCurrentEmployee
			// ControlName.Text = "Employee " & .EmployeeNumber & "    " & Trim(.EmployeeFirstName & " " & .EmployeeMiddleName) & " " & .EmployeeLastName & " " & .EmployeeDesig
			// End With
			// Else
			// ControlName.Text = "Employee 0"
			// End If
		}
		// vbPorter upgrade warning: intEmpNum As object	OnWrite(string)	OnRead(string)
		public static void SetCurrentEmployee(string intEmpNum)
		{
			clsDRWrapper rsName = new clsDRWrapper();
			rsName.OpenRecordset("Select middlename,FirstName,LastName,Desig from tblEmployeeMaster where Employeenumber = '" + intEmpNum + "'", "Twpy0000.vb1");
			if (!rsName.EndOfFile())
			{
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = FCConvert.ToString(rsName.Get_Fields_String("LastName"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = FCConvert.ToString(rsName.Get_Fields_String("FirstName"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName = FCConvert.ToString(rsName.Get_Fields("middlename"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig = FCConvert.ToString(rsName.Get_Fields_String("Desig"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = intEmpNum;
			}
		}
		// vbPorter upgrade warning: ControlName As Control	OnWrite(VB.TextBox)
		public static void SetEmployeeTextBox(Control ControlName, string intEmpNum)
		{
			clsDRWrapper rsName = new clsDRWrapper();
			rsName.OpenRecordset("Select middlename,FirstName,LastName,Desig from tblEmployeeMaster where Employeenumber = '" + intEmpNum + "'", "Twpy0000.vb1");
			if (!rsName.EndOfFile())
			{
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = FCConvert.ToString(rsName.Get_Fields_String("LastName"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = FCConvert.ToString(rsName.Get_Fields_String("FirstName"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig = FCConvert.ToString(rsName.Get_Fields_String("Desig"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName = FCConvert.ToString(rsName.Get_Fields("middlename"));
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = intEmpNum;
				ControlName.Text = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
			}
			else
			{
				ControlName.Text = string.Empty;
			}
		}

		public static void SetCurrentEmployeeType(string intEmpNum = "-1", string strLastName = "", string strFirstName = "", string strDesignation = "", bool boolChild = false, string intParent = "-1")
		{
			clsDRWrapper rsName = new clsDRWrapper();
			if (intEmpNum != string.Empty)
			{
				rsName.OpenRecordset("Select middlename,FirstName,LastName,Desig from tblEmployeeMaster where Employeenumber = '" + intEmpNum + "'", "Twpy0000.vb1");
				if (!rsName.EndOfFile())
				{
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = FCConvert.ToString(rsName.Get_Fields_String("LastName"));
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = FCConvert.ToString(rsName.Get_Fields_String("FirstName"));
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig = FCConvert.ToString(rsName.Get_Fields_String("Desig"));
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName = FCConvert.ToString(rsName.Get_Fields_String("MiddleName"));
				}
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intEmpNum;
				modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = boolChild;
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = intParent;
			}
		}

		public static void CheckCurrentEmployee()
		{
			if (modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord)
			{
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID;
			}
		}
		// vbPorter upgrade warning: frm As Form	OnWrite(frmTaxTables)
		public static bool SetComboBoxHeight(FCForm frm, ref FCComboBox objCB, float TheHeight)
		{
			bool SetComboBoxHeight = false;
			// *************************************************
			// PURPOSE: Sets dropdown height of combo box
			// in Inches
			// SAMPLE USAGE:
			// SetComboBoxHeight combo1, 4
			// 
			// RETURNS:  True if successful, false otherwise
			// 
			// USAGE NOTES: The combo box's parent must be a form
			// not a container such as picture box or
			// a frame
			// 
			// The scale mode of the form must
			// be twips
			// 
			// Only works to increase drop-down
			// height, not decrease it
			// 
			// The increase height won't go beyond
			// the maximum viewable space of the
			// combo box (i.e., there will be
			// no padded blank space on the bottom
			// of the combo box
			// *************************************************
			/*? On Error Resume Next  */
			float lHeight;
			lHeight = TheHeight * 1440;
			// inches to pixels
			//lHeight = lHeight / FCGlobal.Screen.TwipsPerPixelY;
			frm.ScaleMode = ScaleModeConstants.vbPixels;
			modAPIsConst.MoveWindow(objCB.Handle.ToInt32(), objCB.LeftOriginal, objCB.TopOriginal, objCB.WidthOriginal, FCConvert.ToInt32(lHeight), 1);
			frm.ScaleMode = ScaleModeConstants.vbTwips;
			SetComboBoxHeight = fecherFoundation.Information.Err().Number == 0 && fecherFoundation.Information.Err().LastDLLError == 0;
			return SetComboBoxHeight;
		}

		public static void LoadDefaultInformation()
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			// Dim dbDefaultInfo As DAO.Database
			// Set dbDefaultInfo = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
			rsDefaultInfo.OpenRecordset("Select * from tblDefaultInformation", "SystemSettings");
			if (!rsDefaultInfo.EndOfFile())
			{
				modGlobalVariables.Statics.DefaultInfo.City = fecherFoundation.Strings.Trim(rsDefaultInfo.Get_Fields_String("City") + " ");
				modGlobalVariables.Statics.DefaultInfo.State = fecherFoundation.Strings.Trim(rsDefaultInfo.Get_Fields("State") + " ");
				modGlobalVariables.Statics.DefaultInfo.Zip = fecherFoundation.Strings.Trim(rsDefaultInfo.Get_Fields("Zip") + " ");
				rsDefaultInfo.OpenRecordset("Select * from States where State = '" + modGlobalVariables.Statics.DefaultInfo.State + "'", "twpy0000.vb1");
				modGlobalVariables.Statics.DefaultInfo.StateID = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(rsDefaultInfo.Get_Fields("ID") + " "))));
			}
			// Set rsDefaultInfo = Nothing
			// Set dbDefaultInfo = Nothing
		}
		// Public Function StripColon(ByVal strValue As String)
		// If Len(strValue) = 5 And Right(strValue, 1) = ":" Then
		// StripColon = Left(strValue, 4)
		// Else
		// StripColon = strValue
		// End If
		// End Function
		public static void GetEmployeeType()
		{
			clsDRWrapper rsLengths = new clsDRWrapper();
			// Dim dbLengths As DAO.Database
			// Set dbLengths = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
			rsLengths.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
			if (!rsLengths.EndOfFile())
			{
				modGlobalVariables.Statics.gboolEmployeeTypeNumeric = FCConvert.ToString(rsLengths.Get_Fields_String("EmployeeType")) == "Numeric";
			}
			// Set rsLengths = Nothing
			// Set dbLengths = Nothing
		}

		public static void LoadStates(ref FCComboBox objState, ref clsDRWrapper dbDatabase)
		{
			int intCounter;
			// Dim rsUser As DAO.Recordset
			dbDatabase.OpenRecordset("Select * from States", "TWPY0000.vb1");
			if (!dbDatabase.EndOfFile())
			{
				dbDatabase.MoveLast();
				dbDatabase.MoveFirst();
				for (intCounter = 0; intCounter <= dbDatabase.RecordCount() - 1; intCounter++)
				{
					objState.AddItem(dbDatabase.Get_Fields("State") + " - " + dbDatabase.Get_Fields("Description"));
					objState.ItemData(objState.NewIndex, dbDatabase.Get_Fields("ID"));
					dbDatabase.MoveNext();
				}
			}
			// Set dbDatabase = Nothing
		}
		// vbPorter upgrade warning: GridName As object	OnWrite(FCGridCtl.VSFlexGrid)
		public static void CenterGridCaptions(ref FCGrid GridName)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "CenterGridCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			int intCounter;
			for (intCounter = 0; intCounter <= GridName.Cols - 1; intCounter++)
			{
				GridName.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, intCounter, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
		}

		public static void ShowEmployeeInformation(bool boolState)
		{
			//MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Alignment = HorizontalAlignment.Center;
			//FC:FINAL:DDU:#2554 - show employee in status bar
			if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber != null && boolState && Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) >= 0)
			{
				//MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Text = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				App.MainForm.StatusBarText3 = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
			}
			else
			{
				App.MainForm.StatusBarText3 = " ";
			}
		}
		// Public Function GetRegistryKey(ByVal KeyName As String)
		// On Error GoTo ErrorHandler
		//
		// Call GetKeyValues(HKEY_CURRENT_USER, REGISTRYKEY, KeyName, gstrReturn)
		// If gstrReturn = vbNullString Then
		// gstrReturn = vbNullString
		// Else
		// GetRegistryKey = gstrReturn
		// End If
		// Exit Function
		//
		// ErrorHandler:
		// MsgBox Err.Description
		// End Function
		// Public Sub EntryFlagFile(Optional boolSave = False, Optional NewValue As String, Optional boolUseRegistry As Boolean = False)
		// If boolUseRegistry Then
		// Select Case boolSave
		// Case True           'if true then write to the registry
		// Call SaveKeyValue(HKEY_CURRENT_USER, REGISTRYKEY, "EntryFlag", NewValue)
		// Case False          'if false then read in from the registry and save it in gstrEntryFlag
		// If Trim(gstrEntryFlag) = "" Then
		// Call GetKeyValues(HKEY_CURRENT_USER, REGISTRYKEY, "EntryFlag", gstrEntryFlag)
		// End If
		// End Select
		// Else
		// If UCase(MuniName) <> "AUGUSTA" Then
		// Select Case boolSave
		// Case True           'if true then write to the entry flag file
		// Close
		// Open gstrTrioSDrive & "TWXDFLAG.TD1" For Output As #1 Len = 2
		// Print #1, NewValue
		//
		// Case False          'if false then read in the entry flag and save it in gstrEntryFlag
		// Close
		// Open gstrTrioSDrive & "TWXDFLAG.TD1" For Random As #1 Len = 2
		// Close #1
		//
		// Open gstrTrioSDrive & "TWXDFLAG.TD1" For Input As #1 Len = 2
		// If Not EOF(1) Then
		// Input #1, gstrEntryFlag
		// Else
		// gstrEntryFlag = "  "
		// End If
		// End Select
		// End If
		// Select Case boolSave
		// Case True           'if true then write to the registry
		// Call SaveKeyValue(HKEY_CURRENT_USER, REGISTRYKEY, "EntryFlag", NewValue)
		// Case False          'if false then read in from the registry and save it in gstrEntryFlag
		// If Trim(gstrEntryFlag) = "" Then
		// Call GetKeyValues(HKEY_CURRENT_USER, REGISTRYKEY, "EntryFlag", gstrEntryFlag)
		// End If
		// End Select
		// Close #1
		// End If
		// End Sub
		// Public Sub SetDatabase()
		// Dim ff As New FCFCFileSystem
		// Set secRS = Nothing
		// Set secDB = Nothing
		// If ff.FileExists("TWGN0000.vb1") = True Then
		// Set secDB = OpenDatabase("TWGN0000.vb1", False, False, ";PWD=" & DATABASEPASSWORD)
		// Set secRS = secDB.OpenRecordset("SELECT * FROM Security")
		// End If
		// End Sub
		// Public Sub UpdateGlobalVariable(ByVal strVariableName As String, ByVal NewValue)
		// On Error GoTo ErrorHandler
		//
		// Dim ff As New FCFCFileSystem
		// Set rsVariables = Nothing
		// Set dbVariables = Nothing
		// If ff.FileExists("TWGN0000.vb1") = True Then
		// Set dbVariables = OpenDatabase("TWGN0000.vb1", False, False, ";PWD=" & DATABASEPASSWORD)
		// Set rsVariables = dbVariables.OpenRecordset("SELECT * FROM GlobalVariables")
		// End If
		//
		// If rsVariables.EOF Then
		// rsVariables.AddNew
		// Else
		// rsVariables.Edit
		// End If
		//
		// rsVariables.Fields(strVariableName) = NewValue
		// rsVariables.Update
		//
		// Set rsVariables = Nothing
		// Set dbVariables = Nothing
		// Exit Sub
		//
		// ErrorHandler:
		// MsgBox Err.Description
		// End Sub
		// Public Sub GetGlobalVariables()
		// On Error GoTo ErrorHandler
		//
		// Dim dtExp       As Date
		// Dim ff          As New FCFCFileSystem
		// Dim rsSecurity         As DAO.RecordSet
		// Dim rsModules     As DAO.RecordSet
		//
		// gboolBD = False
		//
		// Set rsVariables = Nothing
		// Set dbVariables = Nothing
		// If ff.FileExists("TWGN0000.vb1") = True Then
		// Set dbVariables = OpenDatabase("TWGN0000.vb1", False, False, ";PWD=" & DATABASEPASSWORD)
		// Set rsVariables = dbVariables.OpenRecordset("SELECT * FROM GlobalVariables")
		// Set WWK = dbVariables.OpenRecordset("SELECT * FROM GlobalVariables")
		// Set rsSecurity = dbVariables.OpenRecordset("SELECT * FROM Security", dbOpenDynaset, 0, dbOptimistic)
		// Set rsModules = dbVariables.OpenRecordset("SELECT * FROM Modules", dbOpenDynaset, 0, dbOptimistic)
		//
		// rsSecurity.MoveFirst
		// rsModules.MoveFirst
		//
		// If rsModules!BD Then             'checks for a licence for budgetary
		// dtExp = rsModules!BDdate
		// If dtExp >= Now Then            'compares expiration date to the current date
		// gboolBD = True
		// End If
		// End If
		// End If
		//
		// If rsVariables.EOF Then
		// gstrMuniName = vbNullString
		// Else
		// gstrMuniName = rsVariables.Fields("MUNINAME")
		// End If
		// Exit Sub
		//
		// ErrorHandler:
		// MsgBox Err.Number & " " & Err.Description, vbCritical + vbOKOnly, "TRIO Software"
		// End Sub
		public static void GetLocalVariables()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCFileSystem ff = new FCFileSystem();
				modGlobalVariables.Statics.WPP = null;
				modGlobalVariables.Statics.dbVariables = null;
				if (FCFileSystem.FileExists("TWRE0000.vb1") == true)
				{
					//modGlobalVariables.dbVariables = OpenDatabase("TWRE0000.vb1", false, false, ";PWD="+modGlobalConstants.DATABASEPASSWORD);
					modGlobalVariables.Statics.dbVariables.OpenRecordset("SELECT * FROM WPP where UseriD = " + modReplaceWorkFiles.Statics.gintSecurityID, Type.Missing.ToString(), FCConvert.ToInt32(Type.Missing), FCConvert.ToBoolean(Type.Missing));
				}
				if (modGlobalVariables.Statics.WPP.EOF)
				{
					modGlobalVariables.Statics.WPP.AddNew();
				}
				else
				{
					modGlobalVariables.Statics.WPP.Edit();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(fecherFoundation.Information.Err(ex).Description);
			}
		}

		public static void SaveLocalVariables()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCFileSystem ff = new FCFileSystem();
				modGlobalVariables.Statics.WPP.Fields["UserID"].Value = modReplaceWorkFiles.Statics.gintSecurityID;
				modGlobalVariables.Statics.WPP.Update(1, false);
				GetLocalVariables();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(fecherFoundation.Information.Err(ex).Description);
			}
		}
		// Public Function GetVariable(ByVal strName As String)
		// On Error GoTo ErrorHandler
		//
		// Dim ff As New FCFCFileSystem
		// Set rsVariables = Nothing
		// Set dbVariables = Nothing
		// If ff.FileExists("TWGN0000.vb1") = True Then
		// Set dbVariables = OpenDatabase("TWGN0000.vb1", False, False, ";PWD=" & DATABASEPASSWORD)
		// Set rsVariables = dbVariables.OpenRecordset("SELECT * FROM GlobalVariables")
		// End If
		//
		// If rsVariables.EOF Then
		// GetVariable = vbNullString
		// Else
		// GetVariable = rsVariables.Fields(strName)
		// End If
		//
		// If IsNull(GetVariable) Then GetVariable = ""
		// Exit Function
		//
		// ErrorHandler:
		// MsgBox Err.Description
		// End Function
		public static void CloseChildForms()
		{
			//FC:FINAL:DDU:cannot close a form from foreach enumeration
			//foreach (FCForm frm in FCGlobal.Statics.Forms)
			//{
			//	if (frm.Name != "MDIParent")
			//	{
			//		if (!modGlobalVariables.Statics.gboolLoadHide)
			//			frm.Close();
			//	}
			//}
			FCUtils.CloseFormsOnProject();
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmEmployeePayTotals, frmEmployersMatch, frmVacationSick, frmEmployeeDeductions, frmMiscUpdate, frmEmployeeMaster, frmVoidCheck, frmDirectDeposit, frmPayrollDistribution)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static object FormsExist(FCForm FormName = null)
		{
			object FormsExist = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// this function is used by the search screen to close
				// all child forms if the user is selecting a new employee
				// th
				bool boolDirty = false;
				foreach (FCForm frm in FCGlobal.Statics.Forms)
				{
					string vbPorterVar = frm.Name;
					if (vbPorterVar == "frmEmployeeMaster")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmEmployeeMaster.InstancePtr.FormDirty;
						}
					}
					else if (vbPorterVar == "frmPayrollDistribution")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmPayrollDistribution.InstancePtr.FormDirty;
						}
					}
					else if (vbPorterVar == "frmDirectDeposit")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmDirectDeposit.InstancePtr.FormDirty;
						}
					}
					else if (vbPorterVar == "frmEmployeeDeductions")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmEmployeeDeductions.InstancePtr.FormDirty;
						}
					}
					else if (vbPorterVar == "frmEmployersMatch")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmEmployersMatch.InstancePtr.FormDirty;
						}
					}
					else if (vbPorterVar == "frmVacationSick")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmVacationSick.InstancePtr.FormDirty;
						}
					}
					else if (vbPorterVar == "frmMiscUpdate")
					{
						if (FormName.Name != frm.Name)
						{
							boolDirty = frmMiscUpdate.InstancePtr.FormDirty;
						}
					}
					if (boolDirty)
						break;
				}
				if (boolDirty)
				{
					if (MessageBox.Show("Other Employee screens are open that utilize this Employee: " + "\r" + "\r" + "#" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig + "\r" + "\r" + "If you continue these screens will be closed WITHOUT saving. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						// DO NOT CLOSE OUT ANY SCREENS
						FormsExist = true;
						return FormsExist;
					}
				}
                //FC:FINAL:AM:#3473 - can't use foreach
                //foreach (FCForm frm in FCGlobal.Statics.Forms)
                Form[] forms = FCGlobal.Statics.Forms.Cast<Form>().ToArray();
				// close out all forms
				for(int i = 0; i < forms.Length; i++)
				{
                    Form frm = forms[i];
                    string vbPorterVar1 = frm.Name;
					if (vbPorterVar1 == "frmEmployeeMaster")
					{
						if (FormName.Name != frm.Name)
						{
							frmEmployeeMaster.InstancePtr.FormDirty = false;
							frmEmployeeMaster.InstancePtr.Unload();
						}
					}
					else if (vbPorterVar1 == "frmPayrollDistribution")
					{
						if (FormName.Name != frm.Name)
						{
							frmPayrollDistribution.InstancePtr.FormDirty = false;
							frmPayrollDistribution.InstancePtr.Unload();
						}
					}
					else if (vbPorterVar1 == "frmEmployeeDeductions")
					{
						if (FormName.Name != frm.Name)
						{
							frmEmployeeDeductions.InstancePtr.FormDirty = false;
							frmEmployeeDeductions.InstancePtr.Unload();
						}
					}
					else if (vbPorterVar1 == "frmEmployersMatch")
					{
						if (FormName.Name != frm.Name)
						{
							frmEmployersMatch.InstancePtr.FormDirty = false;
							frmEmployersMatch.InstancePtr.Unload();
						}
					}
					else if (vbPorterVar1 == "frmDirectDeposit")
					{
						if (FormName.Name != frm.Name)
						{
							frmDirectDeposit.InstancePtr.FormDirty = false;
							frmDirectDeposit.InstancePtr.Unload();
						}
					}
					else if (vbPorterVar1 == "frmVacationSick")
					{
						if (FormName.Name != frm.Name)
						{
							frmVacationSick.InstancePtr.FormDirty = false;
							frmVacationSick.InstancePtr.Unload();
						}
					}
					else if (vbPorterVar1 == "frmMiscUpdate")
					{
						if (FormName.Name != frm.Name)
						{
							frmMiscUpdate.InstancePtr.FormDirty = false;
							frmMiscUpdate.InstancePtr.Unload();
						}
					}
				}
				// no forms are open so let them chose a new employee
				FormsExist = false;
				return FormsExist;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 91)
				{
					/*? Resume Next; */
				}
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FormsExist;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmPayCategories, frmTaxStatusCodes, frmDeductionSetup, frmVacationCodes, frmPayCodes, frmTaxTables, frmEmployeeSearch, frmEmployeeDeductionHistory, frmEmployersMatch, frmEmployeeDataEntry, frmVacationSick, frmW3Information, frmReportSelection, frmFrequencyCodes, frmAdjust941ScheduleB, frmEmployeeDeductions, frmEditLockedForm, frmW2AdditionalInfoSetup, frmCheckRecipients, frmPrintBankSetup, frmClearLTDDeductions, frmMiscUpdate, frmPayStatuses, frmSelectDateInfo, frmPayrollAccountsSetup, frmUnemploymentReport, frmWarrant, frmCreateCustomCheckFormat, frmCalender, frmSelectCustomCheckFormat, frmDistributionBaseRateUpdate, frmHelp, frmCheckRegister, frmLoadValidAccounts, frmPurgeArchive, frmSetupAuditArchiveReport, frmPayrollDistribution, frmSelectBankNumber, frmSelectPostingJournal, frm941TaxLiability)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static bool FormExist(FCForm FormName)
		{
			bool FormExist = false;
			foreach (FCForm frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}
		// vbPorter upgrade warning: intState As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object GetDefaultStateName(int intState)
		{
			object GetDefaultStateName = null;
			clsDRWrapper rsD = new clsDRWrapper();
			rsD.OpenRecordset("SELECT * FROM States where ID = " + FCConvert.ToString(intState), "Twpy0000.vb1");
			if (!rsD.EndOfFile())
			{
				GetDefaultStateName = rsD.Get_Fields("State");
			}
			else
			{
				GetDefaultStateName = "OTHER";
			}
			return GetDefaultStateName;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(int, object)
		public static object GetDeductionNumber(int lngDeductionID)
		{
			object GetDeductionNumber = null;
			modGlobalVariables.Statics.rsDeductionID.FindFirstRecord("ID", lngDeductionID);
			if (modGlobalVariables.Statics.rsDeductionID.NoMatch)
			{
				GetDeductionNumber = lngDeductionID;
			}
			else
			{
				GetDeductionNumber = modGlobalVariables.Statics.rsDeductionID.Get_Fields_Int32("DeductionNumber");
			}
			return GetDeductionNumber;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmPayCategories, frmTaxStatusCodes, frmEmployeePayTotals, frmDeductionSetup, frmVacationCodes, frmPayCodes, frmTaxTables, frmEmployeeSearch, frmEmployeeDeductionHistory, frmEmployeeDataEntry, frmVacationSick, frmW3Information, frmReportSelection, frmFrequencyCodes, frmEmployeeDeductions, frmEditLockedForm, frmCheckRecipients, frmPrintBankSetup, frmClearLTDDeductions, frmMiscUpdate, frmMultiPay, frmEmployeeMaster, frmCheckRecipientsCategories, frmAssociate, frmDefaultInformation, frmProcessEditSummary, frmProcessEditTotals, frmPayStatuses, frmCheckReturn, frmDirectDeposit, frmDistributionBaseRateUpdate, frmBanks, frmFieldLengths, frmStandardLimits, frmPayrollDistribution)

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object FixQuote(string strValue)
		{
			object FixQuote = null;
			FixQuote = strValue.Replace("'", "''");
			return FixQuote;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object FixNewQuote(string strValue)
		{
			object FixNewQuote = null;
			FixNewQuote = strValue.Replace("'", "''");
			FixNewQuote = FCConvert.ToString(FixNewQuote).Replace(" ''", " '");
			FixNewQuote = FCConvert.ToString(FixNewQuote).Replace("*''", "*'");
			FixNewQuote = FCConvert.ToString(FixNewQuote).Replace("'' ", "' ");
			return FixNewQuote;
		}
		// vbPorter upgrade warning: clsLoad As object	OnWrite(clsDRWrapper)
		public static void GetC1Data(ref clsDRWrapper clsLoad, ref double dblRate, ref int lnglimit, ref bool boolElectronicC1, ref int lngYearToUse, ref int intQuarterToUse, ref int intCurrentQuarter, int lngRangeStart = -1, int lngRangeEnd = -1)
		{
			string strSQL = "";
			int lngYear;
			int intQuarter = 0;
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			int intCurQuarter;
			int lngCurYear;
			string strQuery1 = "";
			string strQuery2 = "";
			string strQuery3 = "";
			string strQuery4 = "";
			string strRange = "";
			string strStateEmploymentQuery1 = "";
			string strStateEmploymentQuery2 = "";
			string strStateEmploymentQuery3 = "";
			if (lngRangeStart >= 0)
			{
				// strRange = " and seqnumber between " & lngRangeStart & " and " & lngRangeEnd
				strRange = " where seqnumber between " + FCConvert.ToString(lngRangeStart) + " and " + FCConvert.ToString(lngRangeEnd);
			}
			else
			{
				strRange = "";
			}
			clsLoad.OpenRecordset("select unemploymentrate,unemploymentbase from tblStandardLimits", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				dblRate = Conversion.Val(clsLoad.Get_Fields("unemploymentrate"));
				lnglimit = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("unemploymentbase"))));
			}
			else
			{
				dblRate = 0;
				lnglimit = 7000;
			}
			lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
			switch (modGlobalVariables.Statics.gdatCurrentPayDate.Month)
			{
				case 1:
				case 2:
				case 3:
					{
						intQuarter = 1;
						break;
					}
				case 4:
				case 5:
				case 6:
					{
						intQuarter = 2;
						break;
					}
				case 7:
				case 8:
				case 9:
					{
						intQuarter = 3;
						break;
					}
				case 10:
				case 11:
				case 12:
					{
						intQuarter = 4;
						break;
					}
			}
			//end switch
			intCurQuarter = intQuarter;
			lngCurYear = lngYear;
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				intQuarterToUse = intCurQuarter;
				lngYearToUse = lngCurYear;
			}
			else
			{
				if (!boolElectronicC1)
				{
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init3("Select the Quarter and Year for the C1 Report", ref lngYear, ref intQuarter, -1, ref tmpArg , -1, false);
					intQuarterToUse = intQuarter;
					lngYearToUse = lngYear;
				}
				else
				{
					lngYear = lngYearToUse;
					intQuarter = intQuarterToUse;
				}
			}
			intCurrentQuarter = intQuarter;
			// get the range of dates that that quarter covered
			modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarter, lngYear);
			if (intCurrentQuarter == 1)
			{
				// ytd and qtd are same
				strSQL = "select employeenumber,sum(distGrossPay) as qtdGrossPay,sum(distGrossPay) as YTDGrossPay from tblCheckDetail where distributionrecord = 1 and Paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 AND (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )";
				strStateEmploymentQuery1 = " (" + strSQL + ") as StateUnemploymentQuery1 ";
				strSQL = "select employeenumber as employeenum,sum(grosspay) as qtdTotalGross,sum(FederalTaxGross) as qtdFedTaxGross,sum(grosspay) as YTDTotalGross,sum(StateTaxWH) as qtdStateTax from tblCheckDetail where PayDate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber having (sum(distgrosspay) > 0 or sum(statetaxwh) > 0)";
				strStateEmploymentQuery2 = " (" + strSQL + ") as StateUnemploymentQuery2 ";
				strQuery1 = "(select * from " + strStateEmploymentQuery2 + " left join " + strStateEmploymentQuery1 + " on (stateunemploymentquery1.employeenumber = stateunemploymentquery2.employeenum) where (qtdgrosspay > 0 or qtdstatetax > 0) ) as C1Query1";
				strSQL = "select *, TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join (" + strQuery1 + " LEFT join tblmiscupdate on (tblmiscupdate.employeenumber = c1query1.employeenum)) on (tblemployeemaster.employeenumber = c1query1.employeenum)  " + strRange + " order by tblemployeemaster.LASTNAME,TBLemployeemaster.firstname";
			}
			else
			{
				// YTD is not same as qtd
				strSQL = "Select employeenumber as enum1,sum(distGrossPay) as qtdGrossPay from tblCheckDetail where distributionrecord = 1 and Paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 AND isnull(MSRSAdjustRecord, 0) = 0 and (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )";
				strStateEmploymentQuery2 = " (" + strSQL + ") as StateUnemploymentQuery2 ";
				strSQL = "Select employeenumber as employeenum,sum(distGrossPay) as YTDGrossPay from tblcheckdetail where distributionrecord = 1 and paydate between '" + "1/1/" + FCConvert.ToString(lngYear) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 AND isnull(MSRSAdjustRecord, 0) = 0 and (distu <> 'N' and distu <> '') group by employeenumber ";
				strStateEmploymentQuery3 = " (" + strSQL + ") as StateUnemploymentQuery3 ";
				strQuery1 = "(Select * from " + strStateEmploymentQuery3 + " inner join " + strStateEmploymentQuery2 + " on (stateunemploymentquery2.enum1 = stateunemploymentquery3.employeenum)) as C1Query1";
				strQuery2 = "(Select employeenumber,sum(GrossPay) as qtdTotalGross,sum(stateTaxWH) as qtdStateTax,sum(FederalTaxGross) as qtdFedTaxGross from tblcheckdetail where  paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 AND isnull(MSRSAdjustRecord, 0) = 0 group by employeenumber having (sum(GrossPay) > 0  or sum(statetaxwh) > 0) ) as C1Query2 ";
				strQuery3 = "(Select * from " + strQuery2 + " left join " + strQuery1 + " on (c1query1.eMPLOYEENUM = c1query2.employeenumBER) where (qtdgrosspay > 0 or qtdstatetax > 0)) as C1Query3";
				strQuery4 = "(select * from " + strQuery3 + " left join (select employeenumber as enum,unemployment from tblMiscUpdate) as C1Query4 on (c1query4.enum = c1query3.employeenumber)) as C1Query5 ";
				strSQL = "select *, TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join " + strQuery4 + " on (tblemployeemaster.employeenumber = c1query5.employeenumber)  " + strRange + " order by tblemployeemaster.LastName,tblemployeemaster.firstname";
				// tblemployeemaster.employeenumber
			}
			clsLoad.OpenRecordset(strSQL, "TWPY0000.vb1");
		}

		public class StaticVariables
		{
			// Public typMultipleWorksite() As BLS3020
			public FedStateFilingStatus[] aryFedStateFilingStatus = null;
			public int gintDeductionWeeks;
			public bool ggboolWeekly;
			public bool ggboolBiWeekly;
			public bool ggboolSetupBiWeekly;
			public bool ggboolPRBiWeekly;
			public int ggintWeekNumber;
			public int ggintNumberPayWeeks;
			public bool ggboolBiWeeklyDeds;
			public int ggintDedsUpTop;
			public int ggintFreqCodeID;
			public string gstrReportID = "";
			public bool gboolShowVerifyTotalsMessage;
			public CountedEmployees[] aryCountedEmployees;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
