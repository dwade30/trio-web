﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPrintProcessDetail.
	/// </summary>
	public partial class rptPrintProcessDetail : BaseSectionReport
	{
		public rptPrintProcessDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Process Detail Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPrintProcessDetail InstancePtr
		{
			get
			{
				return (rptPrintProcessDetail)Sys.GetInstance(typeof(rptPrintProcessDetail));
			}
		}

		protected rptPrintProcessDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrintProcessDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
		int intCount;
		int intCounter;
		bool boolApply;
		int intShade;
		int PageCounter;
		// vbPorter upgrade warning: aryEmployees As object	OnWrite(string())
		string[] aryEmployees;
		bool boolInPayCatOrder;

		public void Init(bool modalDialog, bool boolPayCatOrder = false)
		{
			boolInPayCatOrder = boolPayCatOrder;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "PrintProcessDetail", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			NextRecord:
			;
			if (intCounter < frmProcessDisplay.InstancePtr.vsGrid.Rows)
			{
				if (frmProcessDisplay.InstancePtr.vsGrid.RowOutlineLevel(intCounter) == 0)
				{
					intShade = 1;
				}
				else if (frmProcessDisplay.InstancePtr.vsGrid.RowOutlineLevel(intCounter) == 1)
				{
					intShade = 2;
				}
				else
				{
					intShade = 3;
				}
				if (frmProcessDisplay.InstancePtr.vsGrid.RowOutlineLevel(intCounter) == 0)
				{
					// CHECK TO SEE IF THIS EMPLOYEE IS APPLIED
					if (fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 1)) == string.Empty)
					{
						intCounter += 1;
						goto NextRecord;
					}
					else if (fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 1)) == "Totals")
					{
						// DO NOT WANT TO SHOW THE TOTALS AS THE GRID SHOWS THE
						// TOTALS FOR ALL RECORDS AND NOT JUST THE ONES THAT THE
						// USER HAS CHOSEN FROM THE SELECTION SCREEN. MATTHEW 5/27/03
						// YES THIS EMPLOYEE NEEDS TO BE SHOWN
						// boolApply = True
						// txtRow1 = Trim(.TextMatrix(intCounter, 1))
						// txtRow2 = Trim(.TextMatrix(intCounter, 2))
						// txtRow3 = Trim(.TextMatrix(intCounter, 3))
						// txtRow4 = Trim(.TextMatrix(intCounter, 4))
						// txtRow5 = Trim(.TextMatrix(intCounter, 5))
						// txtRow6 = Trim(.TextMatrix(intCounter, 6))
						// txtRow7 = Trim(.TextMatrix(intCounter, 7))
						// txtRow8 = Trim(.TextMatrix(intCounter, 8))
						// txtRow9 = Trim(.TextMatrix(intCounter, 9))
						// txtRow10 = Trim(.TextMatrix(intCounter, 10))
						// txtRow11 = Trim(.TextMatrix(intCounter, 11))
						// 
						// intCounter = intCounter + 1
						// EOF = False
						// Exit Sub
					}
					else
					{
						for (intCount = 0; intCount <= (Information.UBound(aryEmployees, 1)); intCount++)
						{
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(aryEmployees[intCount])) == fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 12) + Strings.Mid(fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 1)), 11, fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 1)).Length - 10)))
							{
								// YES THIS EMPLOYEE NEEDS TO BE SHOWN
								boolApply = true;
								txtRow1.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 1)) + "     Gross -";
								txtRow2.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 2));
								txtRow3.Text = "Net - " + fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 3));
								txtRow4.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 4));
								txtRow5.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 5));
								txtRow6.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 6));
								txtRow7.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 7));
								txtRow8.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 8));
								txtRow9.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 9));
								txtRow10.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 10));
								txtRow11.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 11));
								intCounter += 1;
								eArgs.EOF = false;
								return;
							}
						}
						boolApply = false;
						intCounter += 1;
						goto NextRecord;
					}
				}
				else
				{
					if (boolApply)
					{
						txtRow1.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 1));
						txtRow2.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 2));
						txtRow3.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 3));
						txtRow4.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 4));
						txtRow5.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 5));
						txtRow6.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 6));
						txtRow7.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 7));
						txtRow8.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 8));
						txtRow9.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 9));
						txtRow10.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 10));
						txtRow11.Text = fecherFoundation.Strings.Trim(frmProcessDisplay.InstancePtr.vsGrid.TextMatrix(intCounter, 11));
						intCounter += 1;
						eArgs.EOF = false;
						return;
					}
					else
					{
						intCounter += 1;
						goto NextRecord;
					}
				}
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			aryEmployees = Strings.Split(modGlobalVariables.Statics.gstrPrintProcessDetailReport, "^", -1, CompareConstants.vbBinaryCompare);
			this.Field1.Text = DateTime.Now.ToString();
			PageCounter = 0;
			intCounter = 1;
			modPrintToFile.SetPrintProperties(this);
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			if (intShade == 1)
			{
				Detail.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			}
			else if (intShade == 2)
			{
				Detail.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			else
			{
				Detail.BackColor = Color.White;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			txtPage.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
