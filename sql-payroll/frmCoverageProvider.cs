﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCoverageProvider : BaseForm
	{
		public frmCoverageProvider()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCoverageProvider InstancePtr
		{
			get
			{
				return (frmCoverageProvider)Sys.GetInstance(typeof(frmCoverageProvider));
			}
		}

		protected frmCoverageProvider _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolRefreshing;
		private cCoverageProviderView theView = new cCoverageProviderView();

		public string PlanName
		{
			set
			{
				txtPlan.Text = value;
			}
			get
			{
				string PlanName = "";
				PlanName = txtPlan.Text;
				return PlanName;
			}
		}

		private void chk1095B_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.Use1095B = chk1095B.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void cmbPlanMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.PlanMonth = cmbPlanMonth.SelectedIndex;
				}
			}
		}

		private void cmbPlans_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				int lngID = 0;
				lngID = cmbPlans.ItemData(cmbPlans.SelectedIndex);
				if (theView.IsCurrentPlan)
				{
					if (theView.CurrentPlan.ID != lngID)
					{
						if (theView.Changed)
						{
							if (OkToChangePlans())
							{
							}
						}
					}
				}
				if (lngID > 0)
				{
					theView.LoadPlan(lngID);
				}
				else
				{
					theView.AddPlan();
				}
				ShowView();
			}
		}

		private void frmCoverageProvider_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCoverageProvider_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupForm();
		}

		private void SetupForm()
		{
			boolRefreshing = true;
			cmbPlanMonth.Clear();
			cmbPlanMonth.AddItem("N/A");
			cmbPlanMonth.AddItem("01");
			cmbPlanMonth.AddItem("02");
			cmbPlanMonth.AddItem("03");
			cmbPlanMonth.AddItem("04");
			cmbPlanMonth.AddItem("05");
			cmbPlanMonth.AddItem("06");
			cmbPlanMonth.AddItem("07");
			cmbPlanMonth.AddItem("08");
			cmbPlanMonth.AddItem("09");
			cmbPlanMonth.AddItem("10");
			cmbPlanMonth.AddItem("11");
			cmbPlanMonth.AddItem("12");
			RefreshPlanList(0);
			boolRefreshing = false;
		}

		private void RefreshPlanList(int lngID)
		{
			boolRefreshing = true;
			cGenericCollection tList;
			theView.LoadPlans();
			tList = (cGenericCollection)theView.ListOfPlans;
			cCoverageProvider tPlan;
			cmbPlans.Clear();
			cmbPlans.AddItem("New");
			cmbPlans.ItemData(cmbPlans.NewIndex, 0);
			if (!(tList == null))
			{
				tList.MoveFirst();
				while (tList.IsCurrent())
				{
					tPlan = (cCoverageProvider)tList.GetCurrentItem();
					cmbPlans.AddItem(tPlan.PlanName);
					cmbPlans.ItemData(cmbPlans.NewIndex, tPlan.ID);
					tList.MoveNext();
				}
			}
			boolRefreshing = false;
			if (cmbPlans.Items.Count > 1)
			{
				if (lngID > 0)
				{
					int x;
					for (x = 0; x <= cmbPlans.Items.Count - 1; x++)
					{
						if (cmbPlans.ItemData(x) == lngID)
						{
							cmbPlans.SelectedIndex = x;
							return;
						}
					}
					// x
				}
				else
				{
					cmbPlans.SelectedIndex = 1;
				}
			}
			else
			{
				cmbPlans.SelectedIndex = 0;
			}
			if (cmbPlans.SelectedIndex < 0)
				cmbPlans.SelectedIndex = 0;
			boolRefreshing = false;
		}

		private void mnuAddPlan_Click(object sender, System.EventArgs e)
		{
			AddNewPlan();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveProvider();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveProvider())
			{
				Close();
			}
		}

		private bool SaveProvider()
		{
			if (theView.IsCurrentPlan)
			{
				if (txtPlan.Text.Trim() == "")
				{
					MessageBox.Show("You must enter a plan description before saving", "Invalid plan name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}
                if (cmbPlanMonth.SelectedIndex < 1)
                {
                    MessageBox.Show("You must select a plan month before saving", "Invalid plan month", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
				if (theView.SavePlan())
				{
					RefreshPlanList(theView.CurrentPlan.ID);
                    FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
                    return true;
                }
			}
			return false;
		}

		private bool OkToChangePlans()
		{
			bool OkToChangePlans = false;
			bool boolReturn;
			boolReturn = true;
			if (!(theView.CurrentPlan == null))
			{
				if (theView.Changed)
				{
					DialogResult intReturn = 0;
					intReturn = MessageBox.Show("Changes have been made to the current plan." + "\r\n" + "Save changes first?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
					if (intReturn == DialogResult.Cancel)
					{
						boolReturn = false;
					}
					else if (intReturn == DialogResult.Yes)
					{
						if (!SaveProvider())
						{
							boolReturn = false;
						}
					}
				}
			}
			OkToChangePlans = boolReturn;
			return OkToChangePlans;
		}

		private void txtAddress_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.Address = txtAddress.Text;
				}
			}
		}

		private void txtCity_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.City = txtCity.Text;
				}
			}
		}

		private void txtEIN_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.EIN = txtEIN.Text;
				}
			}
		}

		private void txtPhone_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.Telephone = txtPhone.Text;
				}
			}
		}

		private void txtPlan_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.PlanName = txtPlan.Text;
				}
			}
		}

		private void txtPostalCode_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.PostalCode = txtPostalCode.Text;
				}
			}
		}

		private void txtProviderName_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.Name = txtProviderName.Text;
				}
			}
		}

		private void txtState_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentPlan)
				{
					theView.CurrentPlan.State = txtState.Text;
				}
			}
		}

		private void AddNewPlan()
		{
			if (OkToChangePlans())
			{
				// Call theView.AddPlan
				// ShowView
				cmbPlans.SelectedIndex = 0;
			}
		}

		private void ShowView()
		{
			boolRefreshing = true;
			if (theView.IsCurrentPlan)
			{
				txtPlan.Text = theView.CurrentPlan.PlanName;
				cmbPlanMonth.SelectedIndex = theView.CurrentPlan.PlanMonth;
				if (theView.CurrentPlan.Use1095B)
				{
					chk1095B.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chk1095B.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtProviderName.Text = theView.CurrentPlan.Name;
				txtAddress.Text = theView.CurrentPlan.Address;
				txtCity.Text = theView.CurrentPlan.City;
				txtState.Text = theView.CurrentPlan.State;
				txtPostalCode.Text = theView.CurrentPlan.PostalCode;
				txtEIN.Text = theView.CurrentPlan.EIN;
				txtPhone.Text = theView.CurrentPlan.Telephone;
			}
			boolRefreshing = false;
		}

        private void cmdSave_Click(object sender, System.EventArgs e)
        {
            mnuSave_Click(cmdSave, EventArgs.Empty);
        }
    }
}
