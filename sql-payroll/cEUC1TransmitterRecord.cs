﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEUC1TransmitterRecord
	{
		//=========================================================
		private int intYear;
		private string strFederalID = string.Empty;
		private string strName = string.Empty;
		private string strStreetAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZipExt = string.Empty;
		private string strContact = string.Empty;
		private string strContactPhone = string.Empty;
		private string strContactExtension = string.Empty;

		public int TaxYear
		{
			set
			{
				intYear = value;
			}
			get
			{
				int TaxYear = 0;
				TaxYear = intYear;
				return TaxYear;
			}
		}

		public string FederalID
		{
			set
			{
				strFederalID = value;
			}
			get
			{
				string FederalID = "";
				FederalID = strFederalID;
				return FederalID;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public string Address
		{
			set
			{
				strStreetAddress = value;
			}
			get
			{
				string Address = "";
				Address = strStreetAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string zip = "";
				zip = strZip;
				return zip;
			}
		}

		public string ZipExt
		{
			set
			{
				strZipExt = value;
			}
			get
			{
				string ZipExt = "";
				ZipExt = strZipExt;
				return ZipExt;
			}
		}

		public string Contact
		{
			set
			{
				strContact = value;
			}
			get
			{
				string Contact = "";
				Contact = strContact;
				return Contact;
			}
		}

		public string ContactPhone
		{
			set
			{
				strContactPhone = value;
			}
			get
			{
				string ContactPhone = "";
				ContactPhone = strContactPhone;
				return ContactPhone;
			}
		}

		public string ContactPhoneExt
		{
			set
			{
				strContactExtension = value;
			}
			get
			{
				string ContactPhoneExt = "";
				ContactPhoneExt = strContactExtension;
				return ContactPhoneExt;
			}
		}

		public string ToString()
		{
			string ToString = "";
			string strReturn;
			strReturn = "A";
			strReturn += FCConvert.ToString(intYear);
			strReturn += strFederalID;
			strReturn += "WAGE";
			strReturn += Strings.StrDup(5, " ");
			// not used by maine
			strReturn += Strings.Left(strName + Strings.StrDup(50, " "), 50);
			strReturn += Strings.Left(strStreetAddress + Strings.StrDup(40, " "), 40);
			strReturn += Strings.Left(strCity + Strings.StrDup(25, " "), 25);
			strReturn += strState;
			strReturn += Strings.StrDup(13, " ");
			// not used
			strReturn += Strings.Left(strZip + Strings.StrDup(5, " "), 5);
			if (fecherFoundation.Strings.Trim(strZipExt) != "")
			{
				strReturn += "-" + Strings.Left(strZipExt + Strings.StrDup(4, " "), 4);
			}
			else
			{
				strReturn += Strings.StrDup(5, " ");
			}
			strReturn += Strings.Left(strContact + Strings.StrDup(30, " "), 30);
			strReturn += Strings.Format(strContactPhone.Replace("(", "").Replace(")", "").Replace("-", ""), "0000000000");
			if (fecherFoundation.Strings.Trim(strContactExtension) != "")
			{
				strReturn += Strings.Left(strContactExtension + Strings.StrDup(4, " "), 4);
			}
			else
			{
				strReturn += Strings.StrDup(4, " ");
			}
			strReturn += Strings.StrDup(68, " ");
			// not used by maine
			ToString = strReturn;
			return ToString;
		}
	}
}
