//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEmpInfoSetup : BaseForm
	{
		public frmEmpInfoSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.Check1 = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
            this.Check1.AddControlArrayElement(Check1_0, 0);
            this.Check1.AddControlArrayElement(Check1_1, 1);
            this.Check1.AddControlArrayElement(Check1_2, 2);
            this.Check1.AddControlArrayElement(Check1_3, 3);
            this.Check1.AddControlArrayElement(Check1_4, 4);
            this.Check1.AddControlArrayElement(Check1_5, 5);
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmpInfoSetup InstancePtr
		{
			get
			{
				return (frmEmpInfoSetup)Sys.GetInstance(typeof(frmEmpInfoSetup));
			}
		}

		protected frmEmpInfoSetup _InstancePtr = null;
		//=========================================================
		private cEmployeeService empService = new cEmployeeService();

		private void chkDeptDiv_CheckedChanged(object sender, System.EventArgs e)
		{
			txtDeptDiv.Visible = 0 != (chkDeptDiv.CheckState);
			if (chkDeptDiv.CheckState == CheckState.Unchecked)
			{
				txtDeptDiv.Text = string.Empty;
			}
			if (txtDeptDiv.Visible)
				txtDeptDiv.Focus();
		}

		private void chkSequence_CheckedChanged(object sender, System.EventArgs e)
		{
			txtSequence.Visible = 0 != (chkSequence.CheckState);
			if (chkSequence.CheckState == CheckState.Unchecked)
			{
				txtSequence.Text = string.Empty;
			}
			if (txtSequence.Visible)
				txtSequence.Focus();
		}

		private void chkSimple_CheckedChanged(object sender, System.EventArgs e)
		{
			fraBenefits.Visible = 0 != (chkSimple.CheckState);
			//this.mnuAddBenefitName.Enabled = 0 != (chkSimple.CheckState);
            this.cmdAddBenefitName.Enabled = 0 != (chkSimple.CheckState);
            //this.mnuDeleteBenefit.Enabled = 0 != (chkSimple.CheckState);
            this.cmdDeleteBenefit.Enabled = 0 != (chkSimple.CheckState);
            int intCounter;
			for (intCounter = 0; intCounter <= 5; intCounter++)
			{
				Check1[intCounter].Enabled = !FCConvert.CBool(chkSimple.CheckState);
			}
			// For Each ControlName In Me.Controls
			// If TypeOf ControlName Is Line Then
			// ElseIf TypeOf ControlName Is Menu Then
			// ElseIf TypeOf ControlName Is Frame Then
			// Else
			// 
			// If ControlName.Name <> "chkSimple" Then
			// ControlName.Enabled = Not CBool(chkSimple)
			// Else
			// ControlName.Enabled = CBool(chkSimple)
			// End If
			// End If
			// Next
		}

		private void chkSinglePerson_CheckedChanged(object sender, System.EventArgs e)
		{
			txtSinglePerson.Visible = 0 != (chkSinglePerson.CheckState);
			if (chkSinglePerson.CheckState == CheckState.Unchecked)
			{
				txtSinglePerson.Text = string.Empty;
			}
			if (txtSinglePerson.Visible)
				txtSinglePerson.Focus();
		}

		private void chkStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			cboStatus.Visible = 0 != (chkStatus.CheckState);
			if (chkStatus.CheckState == CheckState.Unchecked)
			{
				cboStatus.Text = string.Empty;
			}
			if (cboStatus.Visible)
				cboStatus.Focus();
		}

		private void frmEmpInfoSetup_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmEmpInfoSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEmpInfoSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmpInfoSetup properties;
			//frmEmpInfoSetup.ScaleWidth	= 5880;
			//frmEmpInfoSetup.ScaleHeight	= 4065;
			//frmEmpInfoSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			clsDRWrapper rsData = new clsDRWrapper();
			vsData.Cols = 2;
			vsData.ColWidth(0, 0);
			rsData.OpenRecordset("Select * from tblSimpleEmployeeReport order by Description");
			while (!rsData.EndOfFile())
			{
				vsData.Rows += 1;
				vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
				vsData.TextMatrix(vsData.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("Description")));
				rsData.MoveNext();
			}
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuAddBenefitName_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.Execute("Insert into tblSimpleEmployeeReport (Description) VALUES ('')", "Payroll");
			rsData.OpenRecordset("Select Max(ID) as MaxID from tblSimpleEmployeeReport");
			vsData.Rows += 1;
			vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(rsData.Get_Fields("MaxID")));
			vsData.TextMatrix(vsData.Rows - 1, 1, "");
			vsData.Select(vsData.Rows - 1, 1);
			vsData.EditCell();
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			// 
			int intOptions;
			cGenericCollection employeeList = new cGenericCollection();
			cGenericCollection tempList;
			intOptions = 0;
			if (Check1[0].CheckState == Wisej.Web.CheckState.Checked)
			{
				intOptions = 1;
			}
			if (Check1[1].CheckState == Wisej.Web.CheckState.Checked)
			{
				intOptions = intOptions | 2;
			}
			if (Check1[2].CheckState == Wisej.Web.CheckState.Checked)
			{
				intOptions = intOptions | 4;
			}
			if (Check1[3].CheckState == Wisej.Web.CheckState.Checked)
			{
				intOptions = intOptions | 8;
			}
			if (Check1[4].CheckState == Wisej.Web.CheckState.Checked)
			{
				intOptions = intOptions | 16;
			}
			if (Check1[5].CheckState == Wisej.Web.CheckState.Checked)
			{
				intOptions = intOptions | 32;
			}
			clsDRWrapper rsData = new clsDRWrapper();
			string strOrderBy = "";
			rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 0)
			{
				// employee name
				strOrderBy = "lastname,firstname ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 1)
			{
				// employee number
				strOrderBy = "employeenumber ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 2)
			{
				// sequence
				strOrderBy = "seqnumber,lastname,firstname ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 3)
			{
				// dept/div
				strOrderBy = "DEPTDIV,lastname,firstname ";
			}
			if (chkSinglePerson.CheckState == CheckState.Checked)
			{
				if (fecherFoundation.Strings.Trim(txtSinglePerson.Text) == string.Empty)
				{
					MessageBox.Show("A single person report cannot be created until an employee number is specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtSinglePerson.Focus();
					return;
				}
				else
				{
					rsData.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + txtSinglePerson.Text + "'");
					if (rsData.EndOfFile())
					{
						MessageBox.Show("Invalid Employee Number: " + modGlobalVariables.Statics.gstrSinglePersonRecord, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else
					{
						if (empService.IsEmployeePermissable(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), txtSinglePerson.Text))
						{
							modGlobalVariables.Statics.gstrSinglePersonRecord = " EmployeeNumber = '" + txtSinglePerson.Text + "'";
							tempList = new cGenericCollection();
							tempList.AddItem(empService.GetEmployeeByNumber(txtSinglePerson.Text));
						}
						else
						{
							MessageBox.Show("You don't have permissions to view that employee", "Invalid Employee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
				}
			}
			else
			{
				modGlobalVariables.Statics.gstrSinglePersonRecord = string.Empty;
				tempList = empService.GetEmployeesPermissable(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), strOrderBy, false);
			}
			if (chkStatus.CheckState == Wisej.Web.CheckState.Checked || chkDeptDiv.CheckState == Wisej.Web.CheckState.Checked || chkSequence.CheckState == Wisej.Web.CheckState.Checked)
			{
				cEmployee emp;
				bool boolUse = false;
				tempList.MoveFirst();
				while (tempList.IsCurrent())
				{
					//App.DoEvents();
					emp = (cEmployee)tempList.GetCurrentItem();
					boolUse = true;
					if (chkStatus.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (emp.Status != cboStatus.Text)
						{
							boolUse = false;
						}
					}
					if (chkDeptDiv.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (emp.DepartmentDivision != txtDeptDiv.Text)
						{
							boolUse = false;
						}
					}
					if (chkSequence.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (emp.SequenceNumber != Conversion.Val(txtSequence.Text))
						{
							boolUse = false;
						}
					}
					if (boolUse)
					{
						employeeList.AddItem(emp);
					}
					tempList.MoveNext();
				}
			}
			else
			{
				employeeList = tempList;
			}
			// If chkStatus Then
			// If Trim(cboStatus) = vbNullString Then
			// MsgBox "An employee status must be specified.", vbInformation + vbOKOnly, "TRIO Software"
			// cboStatus.SetFocus
			// Exit Sub
			// Else
			// If Trim(gstrSinglePersonRecord) = vbNullString Then
			// gstrSinglePersonRecord = " Status = '" & cboStatus & "'"
			// Else
			// gstrSinglePersonRecord = gstrSinglePersonRecord & " AND Status = '" & cboStatus & "'"
			// End If
			// End If
			// End If
			// 
			// If chkDeptDiv Then
			// If Trim(txtDeptDiv) = vbNullString Then
			// MsgBox "A Department/Division must be specified.", vbInformation + vbOKOnly, "TRIO Software"
			// txtDeptDiv.SetFocus
			// Exit Sub
			// Else
			// If Trim(gstrSinglePersonRecord) = vbNullString Then
			// gstrSinglePersonRecord = " DeptDiv = '" & txtDeptDiv & "'"
			// Else
			// gstrSinglePersonRecord = gstrSinglePersonRecord & " AND DeptDiv = '" & txtDeptDiv & "'"
			// End If
			// End If
			// End If
			// 
			// If chkSequence Then
			// If Trim(txtSequence) = vbNullString Then
			// MsgBox "A Sequence Number must be specified.", vbInformation + vbOKOnly, "TRIO Software"
			// txtSequence.SetFocus
			// Exit Sub
			// Else
			// If Trim(gstrSinglePersonRecord) = vbNullString Then
			// gstrSinglePersonRecord = " SeqNumber = " & txtSequence
			// Else
			// gstrSinglePersonRecord = gstrSinglePersonRecord & " AND SeqNumber = " & txtSequence
			// End If
			// End If
			// End If
			// If Trim(gstrSinglePersonRecord) = vbNullString Then
			// Else
			// gstrSinglePersonRecord = "Where " & gstrSinglePersonRecord
			// End If
			if (chkSimple.CheckState == CheckState.Checked)
			{
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				rsData.OpenRecordset("Select * from tblSimpleEmployeeReport");
				for (intCounter = 0; intCounter <= vsData.Rows - 1; intCounter++)
				{
					vsData.Select(0, 0);
					if (rsData.FindFirstRecord("ID", vsData.TextMatrix(intCounter, 0)))
					{
						rsData.Edit();
						rsData.Set_Fields("Description", vsData.TextMatrix(intCounter, 1));
						rsData.Update();
					}
				}
				rptSimpleEmployeeInformationReport.InstancePtr.Init(employeeList);
			}
			else
			{
				if (intOptions > 0)
				{
					rptEmployeeInformationReport.InstancePtr.Init(intOptions, employeeList);
				}
			}
		}

		private void mnuDeleteBenefit_Click(object sender, System.EventArgs e)
		{
			if (vsData.Row >= 0)
			{
				if (MessageBox.Show("Are you sure you wish to delete the benefit: " + vsData.TextMatrix(vsData.Row, 1) + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.Execute("Delete from tblSimpleEmployeeReport where ID = " + vsData.TextMatrix(vsData.Row, 0), "Payroll");
					vsData.RemoveItem(vsData.Row);
				}
			}
			else
			{
				MessageBox.Show("Benefit name must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
