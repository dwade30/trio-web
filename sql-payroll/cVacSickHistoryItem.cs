﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cVacSickHistoryItem
	{
		//=========================================================
		private string strDescription = string.Empty;
		private int lngID;
		private double dblCalAccrued;
		private double dblCalUsed;
		private double dblBalanceUsed;
		private double dblCurrentAccrued;
		private double dblCurrentUsed;

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public double Accrued
		{
			set
			{
				dblCurrentAccrued = value;
			}
			get
			{
				double Accrued = 0;
				Accrued = dblCurrentAccrued;
				return Accrued;
			}
		}

		public double Used
		{
			set
			{
				dblCurrentUsed = value;
			}
			get
			{
				double Used = 0;
				Used = dblCurrentUsed;
				return Used;
			}
		}

		public double Balance
		{
			set
			{
				dblBalanceUsed = value;
			}
			get
			{
				double Balance = 0;
				Balance = dblBalanceUsed;
				return Balance;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public double CalendarAccrued
		{
			set
			{
				dblCalAccrued = value;
			}
			get
			{
				double CalendarAccrued = 0;
				CalendarAccrued = dblCalAccrued;
				return CalendarAccrued;
			}
		}

		public double CalendarUsed
		{
			set
			{
				dblCalUsed = value;
			}
			get
			{
				double CalendarUsed = 0;
				CalendarUsed = dblCalUsed;
				return CalendarUsed;
			}
		}
	}
}
