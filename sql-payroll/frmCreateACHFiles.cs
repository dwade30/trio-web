//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCreateACHFiles : BaseForm
	{
		public frmCreateACHFiles()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.lblDateSelect = new System.Collections.Generic.List<FCLabel>();
			this.lblDateSelect.AddControlArrayElement(lblDateSelect_3, 0);
			this.lblDateSelect.AddControlArrayElement(lblDateSelect_4, 1);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCreateACHFiles InstancePtr
		{
			get
			{
				return (frmCreateACHFiles)Sys.GetInstance(typeof(frmCreateACHFiles));
			}
		}

		protected frmCreateACHFiles _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private clsACHSetup tACHSetup = new clsACHSetup();
		private string strACHEmployerIDPrefix = string.Empty;
		private bool boolDontRespondToClick;
		private bool boolInitRun;
		bool boolBalanced;

		private void cmdPrenote_Click(object sender, System.EventArgs e)
		{
			CreatePrenotes();
		}

		private void frmCreateACHFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCreateACHFiles_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			//modGlobalFunctions.SetTRIOColors(this);
			// boolMadePrenoteFile = False
			strACHEmployerIDPrefix = "1";
			boolBalanced = false;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
			if (!rsData.EndOfFile())
			{
				// GET IF THERE IS TO BE A PREFIX ADDED TO THE EMPLOYER NUMBER ON THE ACH
				// ELECTRONIC FILE.
				if (Conversion.Val(rsData.Get_Fields_Int16("ACHEmployerIDPrefix")) == 0)
				{
					if (!fecherFoundation.FCUtils.IsNull(rsData.Get_Fields("ACHEmployeridPrefix")))
					{
						strACHEmployerIDPrefix = " ";
					}
				}
				else if (Conversion.Val(rsData.Get_Fields_Int16("ACHEmployerIDPrefix")) == 1)
				{
					strACHEmployerIDPrefix = "1";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int16("ACHEmployerIDPrefix")) == 9)
				{
					strACHEmployerIDPrefix = "9";
				}
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("achbalanced")))
				{
					boolBalanced = true;
				}
				else
				{
					boolBalanced = false;
				}
			}
			clsACHSetupController tLoad = new clsACHSetupController();
			tLoad.Load(ref tACHSetup);
			FillYearCombo();
		}

		public void Init()
		{
			// dtPayDateChosen = 0
			// intPayRunChosen = 0
			// boolInitRun = boolFirstRun
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void cboPayDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillPayRunCombo();
		}

		private void cboPayDateYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// FILL THE PAY DATE COMBO WITH ALL PAY DATES THAT HAVE BEEN PAID FROM THE CHECK DETAIL TABLE.
			FillPayDateCombo();
		}

		private void FillPayRunCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			// vbPorter upgrade warning: dtPDate As DateTime	OnWrite(string)
			DateTime dtPDate;
			if (boolDontRespondToClick)
				return;
			if (cboPayDate.Items.Count < 1 || cboPayDate.SelectedIndex < 0)
			{
				mnuSaveContinue.Enabled = false;
				return;
			}
			dtPDate = FCConvert.ToDateTime(cboPayDate.Items[cboPayDate.SelectedIndex].ToString());
			strSQL = "Select PayRunID from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPDate) + "' and BankRecord = 1 group by payrunid order by payrunid";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			cboPayRunID.Clear();
			while (!clsLoad.EndOfFile())
			{
				cboPayRunID.AddItem(FCConvert.ToString(clsLoad.Get_Fields("payrunid")));
				clsLoad.MoveNext();
			}
			if (cboPayRunID.Items.Count > 0)
			{
				cboPayRunID.SelectedIndex = 0;
				mnuSaveContinue.Enabled = true;
				T2KEffectiveEntryDate.Text = Strings.Format(dtPDate, "MM/dd/yyyy");
				if (boolInitRun)
				{
					cboPayRunID.Enabled = false;
					cboPayDate.Enabled = false;
					cboPayDateYear.Enabled = false;
				}
			}
			else
			{
				mnuSaveContinue.Enabled = false;
			}
		}

		private void FillPayDateCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			// vbPorter upgrade warning: lngYear As int	OnWrite(string)
			int lngYear;
			if (boolDontRespondToClick)
				return;
			boolDontRespondToClick = true;
			if (cboPayDateYear.Items.Count < 1 || cboPayDateYear.SelectedIndex < 0)
				return;
			lngYear = FCConvert.ToInt32(cboPayDateYear.Items[cboPayDateYear.SelectedIndex].ToString());
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet || modGlobalVariables.Statics.gstrMQYProcessing != "NONE")
			{
				strSQL = "select paydate from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and BankRecord = 1 group by paydate order by paydate desc";
			}
			else
			{
				strSQL = "Select paydate from tblcheckdetail where paydate between '1/1/" + FCConvert.ToString(lngYear) + "' and '12/31/" + FCConvert.ToString(lngYear) + "' and BankRecord = 1 group by paydate order by paydate desc";
			}
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			cboPayDate.Clear();
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("There are no direct deposit records");
			}
			while (!clsLoad.EndOfFile())
			{
				cboPayDate.AddItem(Strings.Format(clsLoad.Get_Fields("Paydate"), "MM/dd/yyyy"));
				clsLoad.MoveNext();
			}
			boolDontRespondToClick = false;
			if (cboPayDate.Items.Count > 0)
			{
				cboPayDate.SelectedIndex = 0;
			}
			else
			{
				mnuSaveContinue.Enabled = false;
			}
		}

		private void FillYearCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			boolDontRespondToClick = true;
			// There are two different ways
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet || modGlobalVariables.Statics.gstrMQYProcessing != "NONE")
			{
				strSQL = "select year(paydate) as payyear from tblcheckdetail where TrustRecord = 1 and checknumber > 0 and year(paydate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + " group by year(paydate) order by year(paydate) desc";
			}
			else
			{
				strSQL = "Select year(paydate) as payyear from tblcheckdetail where TrustRecord = 1 and checknumber > 0 group by year(paydate) order by  year(paydate) desc";
			}
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			cboPayDateYear.Clear();
			while (!clsLoad.EndOfFile())
			{
				cboPayDateYear.AddItem(FCConvert.ToString(clsLoad.Get_Fields("PayYear")));
				clsLoad.MoveNext();
			}
			boolDontRespondToClick = false;
			if (cboPayDateYear.Items.Count > 0)
			{
				cboPayDateYear.SelectedIndex = 0;
			}
			else
			{
				mnuSaveContinue.Enabled = false;
			}
			if (clsLoad.EndOfFile() && clsLoad.BeginningOfFile())
			{
				MessageBox.Show("There are no records to create entries for");
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (CreateACHFiles())
			{
				Close();
			}
		}

		private bool CreatePrenotes()
		{
			bool CreatePrenotes = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsACHFileController tFileCon = new clsACHFileController();
				clsACHFile tFile;
				bool boolHasEntries;
				string strEffectiveEntryDate;
				boolHasEntries = false;
				bool boolSomethingHadEntries;
				boolSomethingHadEntries = false;
				string strFileNameList;
				clsDRWrapper rsRecips = new clsDRWrapper();
				bool boolForcePreNote;
				boolForcePreNote = cmbPrenote.Text == "Only recipients marked Prenote";
				strFileNameList = "";
				strEffectiveEntryDate = T2KEffectiveEntryDate.Text;
				if (!Information.IsDate(strEffectiveEntryDate))
				{
					strEffectiveEntryDate = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				}
				if (!boolForcePreNote)
				{
					rsRecips.OpenRecordset("select * from tblrecipients where eft and ach and eftseparatefile and eftprenote order by name", "twpy0000.vb1");
				}
				else
				{
					rsRecips.OpenRecordset("select * from tblrecipients where eft and ach and eftseparatefile  order by name", "twpy0000.vb1");
				}
				// do grouped recipients
				tFile = tFileCon.CreateACHFile(false, true, false, true, boolBalanced, Convert.ToDateTime(strEffectiveEntryDate), 0, boolForcePreNote);
				if (tFile == null && tFileCon.LastError != "")
				{
					MessageBox.Show("Could not create file." + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreatePrenotes;
				}
				if (!(tFile == null))
				{
					tFile.FileInfo().FileName = Environment.CurrentDirectory + "\\" + txtFileName.Text;
					if (tFile.Batches().Count > 0)
					{
						foreach (clsACHBatch tBatch in tFile.Batches())
						{
							if (tBatch.Details().Count > 0)
							{
								boolHasEntries = true;
								boolSomethingHadEntries = true;
								break;
							}
						}
						// tBatch
						if (boolHasEntries)
						{
							if (!tFileCon.WriteACHFile(ref tFile))
							{
							}
							else
							{
							}
						}
					}
				}
				// do all separate files now
				while (!rsRecips.EndOfFile())
				{
					tFile = tFileCon.CreateACHFile(false, true, false, true, boolBalanced, Convert.ToDateTime(strEffectiveEntryDate), FCConvert.ToInt32(Conversion.Val(rsRecips.Get_Fields("ID"))), boolForcePreNote);
					if (!(tFile == null))
					{
						if (tFile.Batches().Count > 0)
						{
							foreach (clsACHBatch tBatch in tFile.Batches())
							{
								if (tBatch.Details().Count > 0)
								{
									boolHasEntries = true;
									boolSomethingHadEntries = true;
									break;
								}
							}
							// tBatch
							if (boolHasEntries)
							{
								tFile.FileInfo().FileName = Environment.CurrentDirectory + "\\" + rsRecips.Get_Fields("eftfileprefix") + "." + rsRecips.Get_Fields("eftfileextension");
								if (!tFileCon.WriteACHFile(ref tFile))
								{
								}
								else
								{
								}
							}
						}
					}
					else if (tFileCon.LastError != "")
					{
						MessageBox.Show("Could not create file " + Environment.CurrentDirectory + "\\" + rsRecips.Get_Fields("eftfileprefix") + "." + rsRecips.Get_Fields("eftfileextension") + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return CreatePrenotes;
					}
					rsRecips.MoveNext();
				}
				CreatePrenotes = true;
				if (boolSomethingHadEntries)
				{
					MessageBox.Show("Prenote file(s) created.", "Created Prenotes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("No file was created because no prenote entries were found", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return CreatePrenotes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreatePrenotes;
		}

		private bool CreateACHFiles()
		{
			bool CreateACHFiles = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// go through all trust records
				clsACHFileController tFileCon = new clsACHFileController();
				clsACHFile tFile;
				bool boolHasEntries;
				string strEffectiveEntryDate;
				boolHasEntries = false;
				bool boolSomethingHadEntries;
				boolSomethingHadEntries = false;
				string strFileNameList;
				clsDRWrapper rsRecips = new clsDRWrapper();
				strFileNameList = "";
				strEffectiveEntryDate = T2KEffectiveEntryDate.Text;
				if (!Information.IsDate(strEffectiveEntryDate))
				{
					MessageBox.Show("You must enter a valid effective entry date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateACHFiles;
				}
				rsRecips.OpenRecordset("select * from tblrecipients where eft = 1 and ach = 1 and eftseparatefile = 1 order by name", "twpy0000.vb1");
				// do grouped recipients
				tFile = tFileCon.CreateACHFile(true, false, false, true, boolBalanced, Convert.ToDateTime(strEffectiveEntryDate), 0, false);
				if (tFile == null)
				{
					if (tFileCon.LastError != string.Empty)
					{
						MessageBox.Show("Could not create file." + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return CreateACHFiles;
					}
				}
				else
				{
					tFile.FileInfo().FileName = Environment.CurrentDirectory + "\\" + txtFileName.Text;
					if (tFile.Batches().Count > 0)
					{
						foreach (clsACHBatch tBatch in tFile.Batches())
						{
							if (tBatch.Details().Count > 0)
							{
								boolHasEntries = true;
								boolSomethingHadEntries = true;
								break;
							}
						}
						// tBatch
						if (boolHasEntries)
						{
							if (!tFileCon.WriteACHFile(ref tFile))
							{
								MessageBox.Show("File creation failed for " + tFile.FileInfo().FileName + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information);
								return CreateACHFiles;
							}
							else
							{
							}
						}
					}
				}
				// do all separate files now
				while (!rsRecips.EndOfFile())
				{
					tFile = tFileCon.CreateACHFile(true, false, false, true, boolBalanced, Convert.ToDateTime(strEffectiveEntryDate), FCConvert.ToInt32(Conversion.Val(rsRecips.Get_Fields("ID"))), false);
					if (!(tFile == null))
					{
						if (tFile.Batches().Count > 0)
						{
							foreach (clsACHBatch tBatch in tFile.Batches())
							{
								if (tBatch.Details().Count > 0)
								{
									boolHasEntries = true;
									boolSomethingHadEntries = true;
									break;
								}
							}
							// tBatch
							if (boolHasEntries)
							{
								tFile.FileInfo().FileName = Environment.CurrentDirectory + "\\" + rsRecips.Get_Fields("eftfileprefix") + "." + rsRecips.Get_Fields("eftfileextension");
								if (!tFileCon.WriteACHFile(ref tFile))
								{
									MessageBox.Show("File creation failed for " + tFile.FileInfo().FileName + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information);
									return CreateACHFiles;
								}
								else
								{
								}
							}
						}
					}
					else if (tFileCon.LastError != "")
					{
						MessageBox.Show("Could not create file " + Environment.CurrentDirectory + "\\" + rsRecips.Get_Fields("eftfileprefix") + "." + rsRecips.Get_Fields("eftfileextension") + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return CreateACHFiles;
					}
					rsRecips.MoveNext();
				}
				CreateACHFiles = true;
				if (boolSomethingHadEntries)
				{
					MessageBox.Show("ACH file(s) created.", "File(s) Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("No file was created because either no entries or no eft recipients were found", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return CreateACHFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateACHFiles;
		}       
    }
}
