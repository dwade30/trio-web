﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmContractAddEdit.
	/// </summary>
	partial class frmContractAddEdit
	{
		public FCGrid GridContracts;
		public FCGrid GridAccounts;
		public FCGrid GridEmployee;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddContract;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSetup;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSummary;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCustomContract;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuPopup;
		public fecherFoundation.FCToolStripMenuItem mnuEditContract;
		public fecherFoundation.FCToolStripMenuItem mnuPayoff;
		public fecherFoundation.FCToolStripMenuItem mnuVoid;
		public fecherFoundation.FCToolStripMenuItem mnuPopupSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            this.GridContracts = new fecherFoundation.FCGrid();
            this.GridAccounts = new fecherFoundation.FCGrid();
            this.GridEmployee = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddContract = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintSetup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintSummary = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintCustomContract = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPopup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditContract = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayoff = new fecherFoundation.FCToolStripMenuItem();
            this.mnuVoid = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPopupSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdAddContract = new fecherFoundation.FCButton();
            this.cmdPrintSetup = new fecherFoundation.FCButton();
            this.cmdPrintSummary = new fecherFoundation.FCButton();
            this.cmdPrintCustomContract = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridContracts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintCustomContract)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdAddContract);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(860, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridContracts);
            this.ClientArea.Controls.Add(this.GridAccounts);
            this.ClientArea.Controls.Add(this.GridEmployee);
            this.ClientArea.Size = new System.Drawing.Size(860, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintSetup);
            this.TopPanel.Controls.Add(this.cmdPrintSummary);
            this.TopPanel.Controls.Add(this.cmdPrintCustomContract);
            this.TopPanel.Size = new System.Drawing.Size(860, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintCustomContract, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintSummary, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintSetup, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(117, 30);
            this.HeaderText.Text = "Contracts";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // GridContracts
            // 
            this.GridContracts.AllowSelection = false;
            this.GridContracts.AllowUserToResizeColumns = false;
            this.GridContracts.AllowUserToResizeRows = false;
            this.GridContracts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridContracts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridContracts.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridContracts.BackColorBkg = System.Drawing.Color.Empty;
            this.GridContracts.BackColorFixed = System.Drawing.Color.Empty;
            this.GridContracts.BackColorSel = System.Drawing.Color.Empty;
            this.GridContracts.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridContracts.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridContracts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridContracts.ColumnHeadersHeight = 30;
            this.GridContracts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridContracts.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridContracts.DragIcon = null;
            this.GridContracts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridContracts.ExtendLastCol = true;
            this.GridContracts.FixedCols = 0;
            this.GridContracts.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridContracts.FrozenCols = 0;
            this.GridContracts.GridColor = System.Drawing.Color.Empty;
            this.GridContracts.GridColorFixed = System.Drawing.Color.Empty;
            this.GridContracts.Location = new System.Drawing.Point(30, 307);
            this.GridContracts.Name = "GridContracts";
            this.GridContracts.OutlineCol = 0;
            this.GridContracts.ReadOnly = true;
            this.GridContracts.RowHeadersVisible = false;
            this.GridContracts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridContracts.RowHeightMin = 0;
            this.GridContracts.Rows = 1;
            this.GridContracts.ScrollTipText = null;
            this.GridContracts.ShowColumnVisibilityMenu = false;
            this.GridContracts.Size = new System.Drawing.Size(790, 91);
            this.GridContracts.StandardTab = true;
            this.GridContracts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridContracts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridContracts.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.GridContracts, "Double-Click to edit or Right-Click for options menu");
            this.GridContracts.CurrentCellChanged += new System.EventHandler(this.GridContracts_RowColChange);
            this.GridContracts.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridContracts_MouseDownEvent);
            this.GridContracts.DoubleClick += new System.EventHandler(this.GridContracts_DblClick);
            // 
            // GridAccounts
            // 
            this.GridAccounts.AllowSelection = false;
            this.GridAccounts.AllowUserToResizeColumns = false;
            this.GridAccounts.AllowUserToResizeRows = false;
            this.GridAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridAccounts.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridAccounts.BackColorBkg = System.Drawing.Color.Empty;
            this.GridAccounts.BackColorFixed = System.Drawing.Color.Empty;
            this.GridAccounts.BackColorSel = System.Drawing.Color.Empty;
            this.GridAccounts.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridAccounts.Cols = 6;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridAccounts.ColumnHeadersHeight = 30;
            this.GridAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridAccounts.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridAccounts.DragIcon = null;
            this.GridAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridAccounts.ExtendLastCol = true;
            this.GridAccounts.FixedCols = 0;
            this.GridAccounts.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridAccounts.FrozenCols = 0;
            this.GridAccounts.GridColor = System.Drawing.Color.Empty;
            this.GridAccounts.GridColorFixed = System.Drawing.Color.Empty;
            this.GridAccounts.Location = new System.Drawing.Point(30, 418);
            this.GridAccounts.Name = "GridAccounts";
            this.GridAccounts.OutlineCol = 0;
            this.GridAccounts.ReadOnly = true;
            this.GridAccounts.RowHeadersVisible = false;
            this.GridAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridAccounts.RowHeightMin = 0;
            this.GridAccounts.Rows = 1;
            this.GridAccounts.ScrollTipText = null;
            this.GridAccounts.ShowColumnVisibilityMenu = false;
            this.GridAccounts.Size = new System.Drawing.Size(790, 109);
            this.GridAccounts.StandardTab = true;
            this.GridAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridAccounts.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.GridAccounts, null);
            // 
            // GridEmployee
            // 
            this.GridEmployee.AllowSelection = false;
            this.GridEmployee.AllowUserToResizeColumns = false;
            this.GridEmployee.AllowUserToResizeRows = false;
            this.GridEmployee.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridEmployee.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridEmployee.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridEmployee.BackColorBkg = System.Drawing.Color.Empty;
            this.GridEmployee.BackColorFixed = System.Drawing.Color.Empty;
            this.GridEmployee.BackColorSel = System.Drawing.Color.Empty;
            this.GridEmployee.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridEmployee.Cols = 10;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridEmployee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.GridEmployee.ColumnHeadersHeight = 30;
            this.GridEmployee.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridEmployee.DefaultCellStyle = dataGridViewCellStyle6;
            this.GridEmployee.DragIcon = null;
            this.GridEmployee.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridEmployee.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridEmployee.ExtendLastCol = true;
            this.GridEmployee.FixedCols = 0;
            this.GridEmployee.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridEmployee.FrozenCols = 0;
            this.GridEmployee.GridColor = System.Drawing.Color.Empty;
            this.GridEmployee.GridColorFixed = System.Drawing.Color.Empty;
            this.GridEmployee.Location = new System.Drawing.Point(30, 30);
            this.GridEmployee.Name = "GridEmployee";
            this.GridEmployee.OutlineCol = 0;
            this.GridEmployee.ReadOnly = true;
            this.GridEmployee.RowHeadersVisible = false;
            this.GridEmployee.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridEmployee.RowHeightMin = 0;
            this.GridEmployee.Rows = 1;
            this.GridEmployee.ScrollTipText = null;
            this.GridEmployee.ShowColumnVisibilityMenu = false;
            this.GridEmployee.Size = new System.Drawing.Size(790, 257);
            this.GridEmployee.StandardTab = true;
            this.GridEmployee.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridEmployee.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridEmployee.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.GridEmployee, "Use Insert to add contracts");
            this.GridEmployee.CurrentCellChanged += new System.EventHandler(this.GridEmployee_RowColChange);
            this.GridEmployee.KeyDown += new Wisej.Web.KeyEventHandler(this.GridEmployee_KeyDownEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddContract,
            this.mnuSepar3,
            this.mnuPrintSetup,
            this.mnuPrintSummary,
            this.mnuPrintCustomContract,
            this.mnuSepar2,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddContract
            // 
            this.mnuAddContract.Index = 0;
            this.mnuAddContract.Name = "mnuAddContract";
            this.mnuAddContract.Text = "Add Contract";
            this.mnuAddContract.Click += new System.EventHandler(this.mnuAddContract_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuPrintSetup
            // 
            this.mnuPrintSetup.Index = 2;
            this.mnuPrintSetup.Name = "mnuPrintSetup";
            this.mnuPrintSetup.Text = "Print Contract Detail";
            this.mnuPrintSetup.Click += new System.EventHandler(this.mnuPrintSetup_Click);
            // 
            // mnuPrintSummary
            // 
            this.mnuPrintSummary.Index = 3;
            this.mnuPrintSummary.Name = "mnuPrintSummary";
            this.mnuPrintSummary.Text = "Print Contract Summary";
            this.mnuPrintSummary.Click += new System.EventHandler(this.mnuPrintSummary_Click);
            // 
            // mnuPrintCustomContract
            // 
            this.mnuPrintCustomContract.Index = 4;
            this.mnuPrintCustomContract.Name = "mnuPrintCustomContract";
            this.mnuPrintCustomContract.Text = "Print Custom Contract Report";
            this.mnuPrintCustomContract.Click += new System.EventHandler(this.mnuPrintCustomContract_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 5;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            this.mnuSepar2.Visible = false;
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 6;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Visible = false;
            // 
            // Seperator
            // 
            this.Seperator.Index = 7;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuPopup
            // 
            this.mnuPopup.Index = -1;
            this.mnuPopup.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditContract,
            this.mnuPayoff,
            this.mnuVoid,
            this.mnuPopupSepar1,
            this.mnuCancel});
            this.mnuPopup.Name = "mnuPopup";
            this.mnuPopup.Text = "PopUp";
            this.mnuPopup.Visible = false;
            // 
            // mnuEditContract
            // 
            this.mnuEditContract.Index = 0;
            this.mnuEditContract.Name = "mnuEditContract";
            this.mnuEditContract.Text = "Edit";
            this.mnuEditContract.Click += new System.EventHandler(this.mnuEditContract_Click);
            // 
            // mnuPayoff
            // 
            this.mnuPayoff.Index = 1;
            this.mnuPayoff.Name = "mnuPayoff";
            this.mnuPayoff.Text = "Pay Off";
            this.mnuPayoff.Click += new System.EventHandler(this.mnuPayoff_Click);
            // 
            // mnuVoid
            // 
            this.mnuVoid.Index = 2;
            this.mnuVoid.Name = "mnuVoid";
            this.mnuVoid.Text = "Void";
            this.mnuVoid.Click += new System.EventHandler(this.mnuVoid_Click);
            // 
            // mnuPopupSepar1
            // 
            this.mnuPopupSepar1.Index = 3;
            this.mnuPopupSepar1.Name = "mnuPopupSepar1";
            this.mnuPopupSepar1.Text = "-";
            // 
            // mnuCancel
            // 
            this.mnuCancel.Index = 4;
            this.mnuCancel.Name = "mnuCancel";
            this.mnuCancel.Text = "Cancel";
            // 
            // cmdAddContract
            // 
            this.cmdAddContract.AppearanceKey = "acceptButton";
            this.cmdAddContract.Location = new System.Drawing.Point(347, 30);
            this.cmdAddContract.Name = "cmdAddContract";
            this.cmdAddContract.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdAddContract.Size = new System.Drawing.Size(149, 48);
            this.cmdAddContract.TabIndex = 1;
            this.cmdAddContract.Text = "Add Contract";
            this.ToolTip1.SetToolTip(this.cmdAddContract, null);
            this.cmdAddContract.Click += new System.EventHandler(this.mnuAddContract_Click);
            // 
            // cmdPrintSetup
            // 
            this.cmdPrintSetup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintSetup.AppearanceKey = "toolbarButton";
            this.cmdPrintSetup.Location = new System.Drawing.Point(308, 29);
            this.cmdPrintSetup.Name = "cmdPrintSetup";
            this.cmdPrintSetup.Size = new System.Drawing.Size(150, 24);
            this.cmdPrintSetup.TabIndex = 2;
            this.cmdPrintSetup.Text = "Print Contract Details";
            this.ToolTip1.SetToolTip(this.cmdPrintSetup, null);
            this.cmdPrintSetup.Click += new System.EventHandler(this.mnuPrintSetup_Click);
            // 
            // cmdPrintSummary
            // 
            this.cmdPrintSummary.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintSummary.AppearanceKey = "toolbarButton";
            this.cmdPrintSummary.Location = new System.Drawing.Point(464, 29);
            this.cmdPrintSummary.Name = "cmdPrintSummary";
            this.cmdPrintSummary.Size = new System.Drawing.Size(164, 24);
            this.cmdPrintSummary.TabIndex = 3;
            this.cmdPrintSummary.Text = "Print Contract Summary";
            this.ToolTip1.SetToolTip(this.cmdPrintSummary, null);
            this.cmdPrintSummary.Click += new System.EventHandler(this.mnuPrintSummary_Click);
            // 
            // cmdPrintCustomContract
            // 
            this.cmdPrintCustomContract.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintCustomContract.AppearanceKey = "toolbarButton";
            this.cmdPrintCustomContract.Location = new System.Drawing.Point(634, 29);
            this.cmdPrintCustomContract.Name = "cmdPrintCustomContract";
            this.cmdPrintCustomContract.Size = new System.Drawing.Size(198, 24);
            this.cmdPrintCustomContract.TabIndex = 4;
            this.cmdPrintCustomContract.Text = "Print Custom Contract Report";
            this.ToolTip1.SetToolTip(this.cmdPrintCustomContract, null);
            this.cmdPrintCustomContract.Click += new System.EventHandler(this.mnuPrintCustomContract_Click);
            // 
            // frmContractAddEdit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(860, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmContractAddEdit";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Contracts";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmContractAddEdit_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmContractAddEdit_KeyDown);
            this.Resize += new System.EventHandler(this.frmContractAddEdit_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridContracts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintCustomContract)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdPrintCustomContract;
        private FCButton cmdPrintSummary;
        private FCButton cmdPrintSetup;
        private FCButton cmdAddContract;
    }
}
