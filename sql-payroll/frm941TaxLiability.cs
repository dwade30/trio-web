//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frm941TaxLiability : BaseForm
	{
		public frm941TaxLiability()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frm941TaxLiability InstancePtr
		{
			get
			{
				return (frm941TaxLiability)Sys.GetInstance(typeof(frm941TaxLiability));
			}
		}

		protected frm941TaxLiability _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       July 14, 2004
		//
		// MODIFIED BY:
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsData = new clsDRWrapper();
		private int intCounter;
		const int CNSTGRIDCOLPAYDATE = 1;
		const int CNSTGRIDCOLPAYRUNID = 2;
		const int CNSTGRIDCOLAMOUNT = 3;
		const int CNSTGRIDCOLADJUSTMENT = 4;
		const int CNSTGRIDCOLEFFECTIVEDATE = 5;
		DateTime dtFirstDay;
		DateTime dtLastDay;
		private bool boolContinue941;
		private cFed941ViewContext viewContext;

		private void frm941TaxLiability_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public bool Init(cFed941ViewContext theView)
		{
			bool Init = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolContinue941 = false;
				viewContext = theView;
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtFirstDay, ref dtLastDay, theView.ReportQuarter, FCConvert.ToInt32(theView.ReportYear));
				// t
				// dtFirstDay = datStart
				// dtLastDay = datEnd
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				// set the grid column headers/widths/etc....
				SetGridProperties();
				rsData.OpenRecordset("select paydate,payrunid,schedulebdate from TBLPAYROLLSTEPS where paydate between '" + FCConvert.ToString(dtFirstDay) + "' and '" + FCConvert.ToString(dtLastDay) + "' or schedulebdate between '" + FCConvert.ToString(dtFirstDay) + "' and '" + FCConvert.ToString(dtLastDay) + "' order by paydate,payrunid", "Payroll");
				string strWhere = "";
				string[] strAry = null;
				string strSequence;
				strSequence = theView.Sequence;
				if (strSequence != string.Empty)
				{
					strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(strAry, 1) > 0)
					{
						strWhere = " seqnumber between " + strAry[0] + " and " + strAry[1];
					}
					else
					{
						strWhere = " seqnumber = " + strAry[0];
					}
				}
				// Load the grid with data
				LoadGrid(strWhere);
				modGlobalFunctions.SetTRIOColors(this);
				modColorScheme.ColorGrid(vsBanks, 1, vsBanks.Rows - 1, 1, vsBanks.Cols - 1, false);
				this.vsBanks.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
                //FC:FINAL:AKV:#4228 - Reports & Cleanup Routines - Federal 941 - Unable to expand grid
                vsBanks.MergeRow(0, true);
                this.vsBanks.AddExpandButton();
                //FC:FINAL:DSE:#i2437 Expand/Collapse button is set on first non-frozen column
                //this.vsBanks.FrozenCols = 2;
                this.Show(FCForm.FormShowEnum.Modal);
				Init = boolContinue941;
				return Init;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return Init;
			}
		}

		private void LoadGrid(string strWhere)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			double dblTemp = 0;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth = 0;
			double dblTotal = 0;
			double dblGrandTotal = 0;
			int intHeadingRow = 0;
			clsDRWrapper rsAdjustmentRecords = new clsDRWrapper();
			string strNewWhere = "";
			// 
			vsBanks.Rows = 1;
			intCounter = 1;
			while (!rsData.EndOfFile())
			{
				if (intMonth == FCConvert.ToInt32(rsData.Get_Fields_DateTime("PayDate").Month))
				{
					vsBanks.Rows += 1;
					intMonth = FCConvert.ToInt32(rsData.Get_Fields_DateTime("PayDate").Month);
					this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE, FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate")));
					vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYRUNID, FCConvert.ToString(rsData.Get_Fields("payrunid")));
					vsBanks.TextMatrix(intCounter, CNSTGRIDCOLEFFECTIVEDATE, Strings.Format(rsData.Get_Fields("schedulebdate"), "MM/dd/yyyy"));
					strNewWhere = strWhere;
					if (strNewWhere == string.Empty)
					{
						strNewWhere = " payrunid = " + rsData.Get_Fields("payrunid");
					}
					else
					{
						strNewWhere += " and payrunid = " + rsData.Get_Fields("payrunid");
					}
					// dblTemp = Val(Format(GetFedTaxLiability(rsData.Fields("PayDate"), rsData.Fields("PayDate"), strNewWhere), "0.00"))
					dblTemp = Conversion.Val(Strings.Format(modCoreysSweeterCode.GetFedTaxLiability_1(rsData.Get_Fields_DateTime("PayDate"), rsData.Get_Fields_DateTime("PayDate"), ref strNewWhere, true), "0.00"));
					// If dbltemp = "1488.59" Then MsgBox "A"
					this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, Strings.Format(dblTemp, "#,##0.00"));
					dblTotal += dblTemp;
					rsAdjustmentRecords.OpenRecordset("Select AdjustRecord from tblCheckDetail where PayDate = '" + rsData.Get_Fields_DateTime("PayDate") + "' and payrunid = " + rsData.Get_Fields("payrunid") + " and AdjustRecord = 1", "Payroll");
					if (rsAdjustmentRecords.EndOfFile())
					{
						this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLADJUSTMENT, FCConvert.ToString(false));
					}
					else
					{
						this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLADJUSTMENT, FCConvert.ToString(true));
					}
					vsBanks.RowOutlineLevel(intCounter, 2);
					vsBanks.IsSubtotal(intCounter, false);
					intCounter += 1;
				}
				else
				{
					if (dblTotal == 0)
					{
						vsBanks.Rows += 1;
						this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE, FCConvert.ToString(GetMonthDescription(rsData.Get_Fields_DateTime("PayDate").Month)));
						vsBanks.RowOutlineLevel(intCounter, 1);
						vsBanks.IsSubtotal(intCounter, true);
						intHeadingRow = intCounter;
						intCounter += 1;
					}
					else
					{
						dblGrandTotal += dblTotal;
						this.vsBanks.TextMatrix(intHeadingRow, CNSTGRIDCOLAMOUNT, Strings.Format(dblTotal, "#,##0.00"));
						dblTotal = 0;
						vsBanks.Rows += 1;
						this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE, FCConvert.ToString(GetMonthDescription(rsData.Get_Fields_DateTime("PayDate").Month)));
						vsBanks.RowOutlineLevel(intCounter, 1);
						vsBanks.IsSubtotal(intCounter, true);
						intHeadingRow = intCounter;
						intCounter += 1;
					}
					vsBanks.Rows += 1;
					intMonth = FCConvert.ToInt32(rsData.Get_Fields_DateTime("PayDate").Month);
					this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE, FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate")));
					vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYRUNID, FCConvert.ToString(rsData.Get_Fields("payrunid")));
					vsBanks.TextMatrix(intCounter, CNSTGRIDCOLEFFECTIVEDATE, Strings.Format(rsData.Get_Fields("schedulebdate"), "MM/dd/yyyy"));
					strNewWhere = strWhere;
					if (strNewWhere == string.Empty)
					{
						strNewWhere = " payrunid = " + rsData.Get_Fields("payrunid");
					}
					else
					{
						strNewWhere += " and payrunid = " + rsData.Get_Fields("payrunid");
					}
					// dblTemp = Val(Format(GetFedTaxLiability(rsData.Fields("PayDate"), rsData.Fields("PayDate"), strNewWhere), "0.00"))
					dblTemp = Conversion.Val(Strings.Format(modCoreysSweeterCode.GetFedTaxLiability_1(rsData.Get_Fields_DateTime("PayDate"), rsData.Get_Fields_DateTime("PayDate"), ref strNewWhere, true), "0.00"));
					// If dbltemp = "1488.59" Then MsgBox "A"
					this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, Strings.Format(dblTemp, "#,##0.00"));
					dblTotal += dblTemp;
					rsAdjustmentRecords.OpenRecordset("Select AdjustRecord from tblCheckDetail where PayDate = '" + rsData.Get_Fields_DateTime("PayDate") + "' and payrunid = " + rsData.Get_Fields("payrunid") + " and AdjustRecord = 1", "Payroll");
					if (rsAdjustmentRecords.EndOfFile())
					{
						this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLADJUSTMENT, FCConvert.ToString(false));
					}
					else
					{
						this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLADJUSTMENT, FCConvert.ToString(true));
					}
					vsBanks.RowOutlineLevel(intCounter, 2);
					vsBanks.IsSubtotal(intCounter, false);
					intCounter += 1;
				}
				rsData.MoveNext();
			}
			if (dblTotal != 0)
			{
				this.vsBanks.TextMatrix(intHeadingRow, CNSTGRIDCOLAMOUNT, Strings.Format(dblTotal, "#,##0.00"));
				dblGrandTotal += dblTotal;
				vsBanks.Rows += 1;
				this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE, "Grand Total");
				this.vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, Strings.Format(dblGrandTotal, "#,##0.00"));
				vsBanks.RowOutlineLevel(intCounter, 1);
				vsBanks.IsSubtotal(intCounter, true);
			}
			for (intCounter = 1; intCounter <= (vsBanks.Rows - 1); intCounter++)
			{
				vsBanks.IsCollapsed(intCounter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
			}
		}
		// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetMonthDescription(int intMonth)
		{
			object GetMonthDescription = null;
			switch (intMonth)
			{
				case 1:
					{
						GetMonthDescription = "January";
						break;
					}
				case 2:
					{
						GetMonthDescription = "February";
						break;
					}
				case 3:
					{
						GetMonthDescription = "March";
						break;
					}
				case 4:
					{
						GetMonthDescription = "April";
						break;
					}
				case 5:
					{
						GetMonthDescription = "May";
						break;
					}
				case 6:
					{
						GetMonthDescription = "June";
						break;
					}
				case 7:
					{
						GetMonthDescription = "July";
						break;
					}
				case 8:
					{
						GetMonthDescription = "August";
						break;
					}
				case 9:
					{
						GetMonthDescription = "September";
						break;
					}
				case 10:
					{
						GetMonthDescription = "October";
						break;
					}
				case 11:
					{
						GetMonthDescription = "November";
						break;
					}
				case 12:
					{
						GetMonthDescription = "December";
						break;
					}
			}
			//end switch
			return GetMonthDescription;
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsBanks.Cols = 6;
				// vsBanks.ColHidden(0) = True
				// vsBanks.FixedCols = 2
				// set column 0 properties
				vsBanks.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// set column 1 properties
				vsBanks.ColAlignment(CNSTGRIDCOLPAYDATE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsBanks.TextMatrix(0, CNSTGRIDCOLPAYDATE, "Pay Date");
				vsBanks.ColAlignment(CNSTGRIDCOLPAYRUNID, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsBanks.TextMatrix(0, CNSTGRIDCOLPAYRUNID, "Pay Run");
				vsBanks.ColAlignment(CNSTGRIDCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsBanks.TextMatrix(0, CNSTGRIDCOLAMOUNT, "Amount");
				vsBanks.ColAlignment(CNSTGRIDCOLADJUSTMENT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsBanks.ColDataType(CNSTGRIDCOLADJUSTMENT, FCGrid.DataTypeSettings.flexDTBoolean);
				vsBanks.TextMatrix(0, CNSTGRIDCOLADJUSTMENT, "Adjustment Records");
				vsBanks.TextMatrix(0, CNSTGRIDCOLEFFECTIVEDATE, "Effective Date");
				vsBanks.ColDataType(CNSTGRIDCOLEFFECTIVEDATE, FCGrid.DataTypeSettings.flexDTDate);
                //FC:FINAL:BSE #2750 align header and cols 
                vsBanks.ColAlignment(CNSTGRIDCOLEFFECTIVEDATE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                //vsBanks.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBanks.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frm941TaxLiability_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				modGlobalVariables.Statics.gboolExitWithoutShowingReport = true;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frm941TaxLiability_Resize(object sender, System.EventArgs e)
		{
			int GridWidth = 0;
			GridWidth = vsBanks.WidthOriginal;
			// .ColWidth(1) = 0.1 * GridWidth
			vsBanks.ColWidth(CNSTGRIDCOLPAYDATE, FCConvert.ToInt32(0.19 * GridWidth));
			vsBanks.ColWidth(CNSTGRIDCOLPAYRUNID, FCConvert.ToInt32(0.15 * GridWidth));
			vsBanks.ColWidth(CNSTGRIDCOLAMOUNT, FCConvert.ToInt32(0.18 * GridWidth));
			vsBanks.ColWidth(CNSTGRIDCOLADJUSTMENT, FCConvert.ToInt32(0.26 * GridWidth));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modGlobalVariables.Statics.gboolExitWithoutShowingReport = true;
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private bool ValidData()
		{
			bool ValidData = false;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			double dblTotal = 0;
			double dblAmount = 0;
			string strDescription = "";
			ValidData = false;
			for (intCounter = 1; intCounter <= (vsBanks.Rows - 1); intCounter++)
			{
				if (vsBanks.RowOutlineLevel(intCounter) == 1)
				{
					if (dblTotal == 0)
					{
						if (Conversion.Val(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT)) != 0)
						{
							dblTotal = FCConvert.ToDouble(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
						}
						strDescription = vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE);
					}
					else
					{
						if (Strings.Format(dblTotal, "0.00") != Strings.Format(dblAmount, "0.00"))
						{
							// this is invalid so stop
							MessageBox.Show("The amounts for the month of: " + strDescription + " do not equal the total of: " + FCConvert.ToString(dblTotal) + " for that month.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return ValidData;
						}
						else
						{
							dblAmount = 0;
							if (Conversion.Val(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT)) != 0)
							{
								dblTotal = FCConvert.ToDouble(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
							}
							strDescription = vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE);
						}
					}
				}
				else
				{
					if (Conversion.Val(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT)) != 0)
					{
						dblAmount += FCConvert.ToDouble(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
					}
					if (!Information.IsDate(vsBanks.TextMatrix(intCounter, CNSTGRIDCOLEFFECTIVEDATE)))
					{
						MessageBox.Show("The effective date for paydate " + vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE) + " and payrun " + vsBanks.TextMatrix(intCounter, CNSTGRIDCOLPAYDATE) + " is not a valid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return ValidData;
					}
				}
			}
			ValidData = true;
			return ValidData;
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				double[] MonthTotal = new double[3 + 1];
				// vbPorter upgrade warning: Months As int	OnWriteFCConvert.ToInt32(
				int[] Months = new int[3 + 1];
				double[,] MonthDetail = new double[3 + 1, 31 + 1];
				// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string)
				DateTime dtTemp;
				int intindex = 0;
				double dblTemp = 0;
				double dblTotal;
				int intMonthCounter;
				SaveData = false;
				if (!ValidData())
					return SaveData;
				Months[1] = dtFirstDay.Month;
				Months[2] = Months[1] + 1;
				Months[3] = Months[1] + 2;
				MonthTotal[1] = 0;
				MonthTotal[2] = 0;
				MonthTotal[3] = 0;
				for (x = 1; x <= 31; x++)
				{
					MonthDetail[1, x] = 0;
					MonthDetail[2, x] = 0;
					MonthDetail[3, x] = 0;
				}
				// x
				// first save the effective dates
				for (x = 1; x <= vsBanks.Rows - 1; x++)
				{
					if (vsBanks.RowOutlineLevel(x) == 2)
					{
						clsSave.Execute("update tblpayrollsteps set schedulebdate = '" + vsBanks.TextMatrix(x, CNSTGRIDCOLEFFECTIVEDATE) + "' where paydate = '" + vsBanks.TextMatrix(x, CNSTGRIDCOLPAYDATE) + "' and payrunid = " + vsBanks.TextMatrix(x, CNSTGRIDCOLPAYRUNID), "Payroll");
					}
				}
				// x
				cTaxAdj941 tAdj;
				viewContext.ClearLiabilityDetails();
				// now go through and process effectivedates that fall in the correct date range
				// the effective dates might have been changed and put them in a different quarter
				for (x = 1; x <= vsBanks.Rows - 1; x++)
				{
					if (vsBanks.RowOutlineLevel(x) == 2)
					{
						dtTemp = FCConvert.ToDateTime(vsBanks.TextMatrix(x, CNSTGRIDCOLEFFECTIVEDATE));
						intindex = 0;
						if (dtTemp.Month == Months[1])
						{
							intindex = 1;
						}
						else if (dtTemp.Month == Months[2])
						{
							intindex = 2;
						}
						else if (dtTemp.Month == Months[3])
						{
							intindex = 3;
						}
						if (intindex > 0)
						{
							dblTemp = FCConvert.ToDouble(vsBanks.TextMatrix(x, CNSTGRIDCOLAMOUNT));
							tAdj = new cTaxAdj941();
							tAdj.Amount = dblTemp;
							tAdj.EffectiveDate = FCConvert.ToString(dtTemp);
							tAdj.PayDate = vsBanks.TextMatrix(x, CNSTGRIDCOLPAYDATE);
							tAdj.PayRunID = FCConvert.ToInt32(vsBanks.TextMatrix(x, CNSTGRIDCOLPAYRUNID));
							viewContext.LiabilityCollection.Add(tAdj);
							MonthTotal[intindex] += dblTemp;
							MonthDetail[intindex, dtTemp.Day] = MonthDetail[intindex, dtTemp.Day] + dblTemp;
						}
					}
				}
				// x
				// totals are done. Now put them on the report
				// srpt941ScheduleB.Detail.Controls("txtMonth1Total").Text = Format(MonthTotal(1), "#,###,##0.00")
				// srpt941ScheduleB.Detail.Controls("txtMonth2Total").Text = Format(MonthTotal(2), "#,###,##0.00")
				// srpt941ScheduleB.Detail.Controls("txtMonth3Total").Text = Format(MonthTotal(3), "#,###,##0.00")
				// dblTotal = MonthTotal(1) + MonthTotal(2) + MonthTotal(3)
				// 
				// srpt941ScheduleB.txtTotal.Text = Format(dblTotal, "#,###,###,##0.00")
				// For intMonthCounter = 1 To 3
				// For x = 1 To 31
				// If MonthDetail(intMonthCounter, x) > 0 Then
				// srpt941ScheduleB.Detail.Controls("txtMonth" & intMonthCounter & "Line" & x).Text = Format(MonthDetail(intMonthCounter, x), "#,###,###,##0.00")
				// Else
				// srpt941ScheduleB.Detail.Controls("txtMonth" & intMonthCounter & "Line" & x).Text = ""
				// End If
				// Next x
				// Next intMonthCounter
				boolContinue941 = true;
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				Close();
			}
		}
		// Private Sub mnuSave_Click()
		// Dim clsSave As New clsDataConnection
		//
		// On Error GoTo CallErrorRoutine
		// gstrCurrentRoutine = "cmdSave_Click"
		// Dim Mouse As clsMousePointer
		// Set Mouse = New clsMousePointer
		// GoTo ResumeCode
		// CallErrorRoutine:
		// Call SetErrorHandler
		// Exit Sub
		// ResumeCode:
		//
		// Dim x As Integer
		// Dim intRowNumber As Integer
		// Dim intHeadingRow As Integer
		// Dim dblTotal As Double
		//
		// If Not ValidData Then Exit Sub
		//
		// intRowNumber = 1
		// For x = 1 To 3
		// NextRecord:
		// If intRowNumber = vsBanks.Rows - 1 Then
		// Exit For
		// Else
		// If frmAdjust941ScheduleB.vsBanks.RowOutlineLevel(intRowNumber) = 1 Then
		// If intRowNumber = 1 Then
		// intHeadingRow = intRowNumber
		// intRowNumber = intRowNumber + 1
		// GoTo NextRecord
		// Else
		// GoTo NextMonth
		// End If
		//
		// ElseIf frmAdjust941ScheduleB.vsBanks.RowOutlineLevel(intRowNumber) = 2 Then
		// srpt941ScheduleB.Detail.Controls("txtMonth" & x & "Line" & Val(Day(vsBanks.TextMatrix(intRowNumber, CNSTGRIDCOLPAYDATE)))).Text = Format(frmAdjust941ScheduleB.vsBanks.TextMatrix(intRowNumber, CNSTGRIDCOLAMOUNT), "#,###,###,##0.00")
		// intRowNumber = intRowNumber + 1
		// GoTo NextRecord
		// End If
		// End If
		//
		// NextMonth:
		// srpt941ScheduleB.Detail.Controls("txtMonth" & x & "Total").Text = Format(vsBanks.TextMatrix(intHeadingRow, CNSTGRIDCOLAMOUNT), "#,###,##0.00")
		// dblTotal = dblTotal + Format(vsBanks.TextMatrix(intHeadingRow, CNSTGRIDCOLAMOUNT), "#,###,##0.00")
		// intHeadingRow = intRowNumber
		// intRowNumber = intRowNumber + 1
		// Next x
		//
		// srpt941ScheduleB.Detail.Controls("txtMonth" & x & "Total").Text = Format(vsBanks.TextMatrix(intHeadingRow, CNSTGRIDCOLAMOUNT), "#,###,##0.00")
		// dblTotal = dblTotal + Format(vsBanks.TextMatrix(intHeadingRow, 2), "#,###,##0.00")
		//
		// srpt941ScheduleB.txtTotal.Text = Format(dblTotal, "#,###,###,##0.00")
		// Unload Me
		// Exit Sub
		//
		// ExitRoutine:
		// MsgBox "Record(s) NOT saved successfully. Incorrect data on grid row #" & intCounter, vbInformation + vbOKOnly, "Payroll Banks"
		// End Sub
		private void vsBanks_RowColChange(object sender, System.EventArgs e)
		{
			if (vsBanks.Col == CNSTGRIDCOLAMOUNT || vsBanks.Col == CNSTGRIDCOLEFFECTIVEDATE)
			{
				vsBanks.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsBanks.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void frm941TaxLiability_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frm941TaxLiability properties;
			//frm941TaxLiability.ScaleWidth	= 6930;
			//frm941TaxLiability.ScaleHeight	= 5655;
			//frm941TaxLiability.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
