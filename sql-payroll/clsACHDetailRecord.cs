﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsACHDetailRecord
	{
		//=========================================================
		private bool boolPreNote;
		private double dblTotalAmount;
		private string strName = "";
		private string strImmediateOriginRT = "";
		private string strIdentifier = "";
		private int intRecordNumber;
		private string strAccountNumber = "";
		private string strAccountType = "";
		private string strBankID = "";
		private int intAddendaIndicator;
		private string strLastError = string.Empty;

		private string RecordCode()
		{
			string RecordCode = "";
			RecordCode = "6";
			return RecordCode;
		}

		public string TransactionByAccountType()
		{
			string TransactionByAccountType = "";
			strLastError = "";
			string strReturn = "";
			switch (FCConvert.ToInt32(fecherFoundation.Strings.LCase(strAccountType)))
			{
				case modCoreysSweeterCode.CNSTACHACCOUNTTYPECHECKING:
					{
						strReturn = "2";
						break;
					}
				case modCoreysSweeterCode.CNSTACHACCOUNTTYPESAVINGS:
					{
						strReturn = "3";
						break;
					}
				case modCoreysSweeterCode.CNSTACHACCOUNTTYPELOAN:
					{
						strReturn = "5";
						break;
					}
				default:
					{
						strLastError = "Invalid account type.";
						break;
					}
			}
			//end switch
			if (!boolPreNote)
			{
				TransactionByAccountType = strReturn + "2";
			}
			else
			{
				TransactionByAccountType = strReturn + "3";
			}
			return TransactionByAccountType;
		}

		private bool CheckData()
		{
			bool CheckData = false;
			CheckData = false;
			if (strName == "")
			{
				strLastError = "Name is blank.";
				return CheckData;
			}
			if (strImmediateOriginRT == "")
			{
				strLastError = "The immediate origin RT is blank.";
				return CheckData;
			}
			if (intRecordNumber <= 0)
			{
				strLastError = "The trance number is invalid.";
				return CheckData;
			}
			if (strAccountNumber == "")
			{
				strLastError = "The account number is blank.";
				return CheckData;
			}
			if (strAccountType == "")
			{
				strLastError = "The account type is blank.";
				return CheckData;
			}
			if (strBankID == "")
			{
				strLastError = "The DFI identifier is blank.";
				return CheckData;
			}
			CheckData = true;
			return CheckData;
		}

		public int HashNumber
		{
			get
			{
				int HashNumber = 0;
				HashNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(Strings.Right("000000000" + strBankID, 9), 8))));
				return HashNumber;
			}
		}

		public double EffectiveTotal
		{
			get
			{
				double EffectiveTotal = 0;
				if (!boolPreNote)
				{
					EffectiveTotal = dblTotalAmount;
				}
				else
				{
					EffectiveTotal = 0;
				}
				return EffectiveTotal;
			}
		}

		public string TraceNumber
		{
			get
			{
				string TraceNumber = "";
				string strTemp;
				strTemp = Strings.Left(strImmediateOriginRT, 8);
				strTemp += Strings.Right("0000000" + FCConvert.ToString(intRecordNumber), 7);
				TraceNumber = strTemp;
				return TraceNumber;
			}
		}

		public string OutputLineByRec(int intRecNum)
		{
			string OutputLineByRec = "";
			intRecordNumber = intRecNum;
			OutputLineByRec = OutputLine();
			return OutputLineByRec;
		}

		public string OutputLine()
		{
			string OutputLine = "";
			strLastError = "";
			string strLine = "";
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (CheckData())
				{
					strLine = RecordType;
					strTemp = TransactionByAccountType();
					if (strTemp != "")
					{
						strLine += strTemp;
					}
					else
					{
						return OutputLine;
					}
					strTemp = Strings.Right("000000000" + strBankID, 9);
					strLine += strTemp;
					strTemp = strAccountNumber;
					strTemp = FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strTemp, 17, false));
					strLine += strTemp;
					strTemp = Strings.Format((EffectiveTotal * 100), "0000000000");
					strLine += strTemp;
					strTemp = Strings.Left(strIdentifier + "               ", 15);
					strLine += strTemp;
					strTemp = Strings.Left(strName + Strings.StrDup(22, " "), 22);
					strLine += strTemp;
					strLine += Strings.StrDup(2, " ");
					strLine += FCConvert.ToString(intAddendaIndicator);
					strLine += TraceNumber;
					OutputLine = strLine;
				}
				else
				{
					strLastError = "Could not build detail record." + "\r\n" + strLastError;
				}
				return OutputLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = "Could not build detail record." + "\r\n" + fecherFoundation.Information.Err(ex).Description;
			}
			return OutputLine;
		}

		public string LastError
		{
			set
			{
				strLastError = value;
			}
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public bool PreNote
		{
			set
			{
				boolPreNote = value;
			}
			get
			{
				bool PreNote = false;
				PreNote = boolPreNote;
				return PreNote;
			}
		}

		public double TotalAmount
		{
			set
			{
				dblTotalAmount = value;
			}
			get
			{
				double TotalAmount = 0;
				TotalAmount = dblTotalAmount;
				return TotalAmount;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public string ImmediateOriginRT
		{
			set
			{
				strImmediateOriginRT = value;
			}
			get
			{
				string ImmediateOriginRT = "";
				ImmediateOriginRT = strImmediateOriginRT;
				return ImmediateOriginRT;
			}
		}

		public string Identifier
		{
			set
			{
				strIdentifier = value;
			}
			get
			{
				string Identifier = "";
				Identifier = strIdentifier;
				return Identifier;
			}
		}

		public int RecordNumber
		{
			set
			{
				intRecordNumber = value;
			}
			get
			{
				int RecordNumber = 0;
				RecordNumber = intRecordNumber;
				return RecordNumber;
			}
		}

		public string AccountNumber
		{
			set
			{
				strAccountNumber = value;
			}
			get
			{
				string AccountNumber = "";
				AccountNumber = strAccountNumber;
				return AccountNumber;
			}
		}

		public string AccountType
		{
			set
			{
				strAccountType = value;
			}
			get
			{
				string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}

		public string BankID
		{
			set
			{
				strBankID = value;
			}
			get
			{
				string BankID = "";
				BankID = strBankID;
				return BankID;
			}
		}

		public int AddendaIndicator
		{
			set
			{
				intAddendaIndicator = value;
			}
			get
			{
				int AddendaIndicator = 0;
				AddendaIndicator = intAddendaIndicator;
				return AddendaIndicator;
			}
		}

		public string RecordType
		{
			get
			{
				string RecordType = "";
				RecordType = RecordCode();
				return RecordType;
			}
		}
	}
}
