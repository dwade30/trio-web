﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSimpleEmployeeInformationReport.
	/// </summary>
	partial class rptSimpleEmployeeInformationReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSimpleEmployeeInformationReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.srptEmployeeDeductions = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblEmpNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFederalStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtStateStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtFederalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFICATax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMedicareTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srptBenefits = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOtherIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblOtherIncome = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOtherDeduction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblOtherDeduction = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtW4Date = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmpNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedicareTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherDeduction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherDeduction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtW4Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape4,
            this.Shape3,
            this.lblHeader,
            this.Line1,
            this.srptEmployeeDeductions,
            this.txtEmployeeNumber,
            this.lblEmpNumber,
            this.txtEmployeeName,
            this.Label2,
            this.txtSSN,
            this.Label3,
            this.txtFederalStatus,
            this.Label4,
            this.txtStateStatus,
            this.Label5,
            this.txtTotalPay,
            this.Label6,
            this.Label7,
            this.Line2,
            this.txtFederalTax,
            this.Label8,
            this.txtFICATax,
            this.Label9,
            this.txtMedicareTax,
            this.Label10,
            this.txtStateTax,
            this.Label11,
            this.Line3,
            this.txtTotalTax,
            this.srptBenefits,
            this.txtOtherIncome,
            this.lblOtherIncome,
            this.txtOtherDeduction,
            this.lblOtherDeduction,
            this.txtW4Date,
            this.label13});
            this.Detail.Height = 2.552083F;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // Shape4
            // 
            this.Shape4.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.Shape4.Height = 0.2F;
            this.Shape4.Left = 4.4F;
            this.Shape4.Name = "Shape4";
            this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape4.Top = 0.06666667F;
            this.Shape4.Width = 2.9F;
            // 
            // Shape3
            // 
            this.Shape3.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.Shape3.Height = 0.2F;
            this.Shape3.Left = 0.1333333F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Top = 0.06666667F;
            this.Shape3.Width = 3.866667F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1666667F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0.1333333F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-weight: bold; text-align: center";
            this.lblHeader.Text = "EMPLOYEE DATA";
            this.lblHeader.Top = 0.1F;
            this.lblHeader.Width = 3.866667F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.1333333F;
            this.Line1.LineColor = System.Drawing.Color.FromArgb(2, 0, 0);
            this.Line1.LineWeight = 3F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2666667F;
            this.Line1.Width = 3.866667F;
            this.Line1.X1 = 0.1333333F;
            this.Line1.X2 = 4F;
            this.Line1.Y1 = 0.2666667F;
            this.Line1.Y2 = 0.2666667F;
            // 
            // srptEmployeeDeductions
            // 
            this.srptEmployeeDeductions.CloseBorder = false;
            this.srptEmployeeDeductions.Height = 0.1333333F;
            this.srptEmployeeDeductions.Left = 0.1666667F;
            this.srptEmployeeDeductions.Name = "srptEmployeeDeductions";
            this.srptEmployeeDeductions.Report = null;
            this.srptEmployeeDeductions.Top = 2.2F;
            this.srptEmployeeDeductions.Width = 7.166667F;
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.Height = 0.1666667F;
            this.txtEmployeeNumber.Left = 1.5F;
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Text = null;
            this.txtEmployeeNumber.Top = 0.3333333F;
            this.txtEmployeeNumber.Width = 2.633333F;
            // 
            // lblEmpNumber
            // 
            this.lblEmpNumber.Height = 0.1666667F;
            this.lblEmpNumber.HyperLink = null;
            this.lblEmpNumber.Left = 0.1333333F;
            this.lblEmpNumber.Name = "lblEmpNumber";
            this.lblEmpNumber.Style = "font-weight: bold";
            this.lblEmpNumber.Text = "Employee Number";
            this.lblEmpNumber.Top = 0.3333333F;
            this.lblEmpNumber.Width = 1.3F;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Height = 0.1666667F;
            this.txtEmployeeName.Left = 1.5F;
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Text = null;
            this.txtEmployeeName.Top = 0.5333334F;
            this.txtEmployeeName.Width = 2.833333F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1666667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.1333333F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-weight: bold";
            this.Label2.Text = "Employee Name";
            this.Label2.Top = 0.5333334F;
            this.Label2.Width = 1.3F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1666667F;
            this.txtSSN.Left = 1.5F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 0.7666667F;
            this.txtSSN.Width = 2.6F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1666667F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.1333333F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold";
            this.Label3.Text = "SSN";
            this.Label3.Top = 0.7666667F;
            this.Label3.Width = 1.3F;
            // 
            // txtFederalStatus
            // 
            this.txtFederalStatus.Height = 0.1666667F;
            this.txtFederalStatus.Left = 1.5F;
            this.txtFederalStatus.Name = "txtFederalStatus";
            this.txtFederalStatus.Text = null;
            this.txtFederalStatus.Top = 1F;
            this.txtFederalStatus.Width = 2.833333F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1666667F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.1333333F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "Federal Tax Status";
            this.Label4.Top = 1F;
            this.Label4.Width = 1.3F;
            // 
            // txtStateStatus
            // 
            this.txtStateStatus.Height = 0.1666667F;
            this.txtStateStatus.Left = 1.5F;
            this.txtStateStatus.Name = "txtStateStatus";
            this.txtStateStatus.Text = null;
            this.txtStateStatus.Top = 1.4F;
            this.txtStateStatus.Width = 2.833333F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1666667F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.1333333F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "State Tax Status";
            this.Label5.Top = 1.4F;
            this.Label5.Width = 1.3F;
            // 
            // txtTotalPay
            // 
            this.txtTotalPay.Height = 0.1666667F;
            this.txtTotalPay.Left = 1.5F;
            this.txtTotalPay.Name = "txtTotalPay";
            this.txtTotalPay.Text = null;
            this.txtTotalPay.Top = 1.6F;
            this.txtTotalPay.Width = 2.833333F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1666667F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.1333333F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold";
            this.Label6.Text = "Total Pay for Year";
            this.Label6.Top = 1.6F;
            this.Label6.Width = 1.3F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1666667F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.4F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-weight: bold; text-align: center";
            this.Label7.Text = "TAX DATA";
            this.Label7.Top = 0.1F;
            this.Label7.Width = 2.9F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 4.4F;
            this.Line2.LineColor = System.Drawing.Color.FromArgb(2, 0, 0);
            this.Line2.LineWeight = 3F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.2666667F;
            this.Line2.Width = 2.9F;
            this.Line2.X1 = 4.4F;
            this.Line2.X2 = 7.3F;
            this.Line2.Y1 = 0.2666667F;
            this.Line2.Y2 = 0.2666667F;
            // 
            // txtFederalTax
            // 
            this.txtFederalTax.Height = 0.1666667F;
            this.txtFederalTax.Left = 5.8F;
            this.txtFederalTax.Name = "txtFederalTax";
            this.txtFederalTax.Style = "text-align: right";
            this.txtFederalTax.Text = null;
            this.txtFederalTax.Top = 0.3666667F;
            this.txtFederalTax.Width = 1.5F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1666667F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 4.433333F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-weight: bold";
            this.Label8.Text = "Federal";
            this.Label8.Top = 0.3666667F;
            this.Label8.Width = 1.3F;
            // 
            // txtFICATax
            // 
            this.txtFICATax.Height = 0.1666667F;
            this.txtFICATax.Left = 5.8F;
            this.txtFICATax.Name = "txtFICATax";
            this.txtFICATax.Style = "text-align: right";
            this.txtFICATax.Text = null;
            this.txtFICATax.Top = 0.5666667F;
            this.txtFICATax.Width = 1.5F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1666667F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 4.433333F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold";
            this.Label9.Text = "FICA";
            this.Label9.Top = 0.5666667F;
            this.Label9.Width = 1.3F;
            // 
            // txtMedicareTax
            // 
            this.txtMedicareTax.Height = 0.1666667F;
            this.txtMedicareTax.Left = 5.8F;
            this.txtMedicareTax.Name = "txtMedicareTax";
            this.txtMedicareTax.Style = "text-align: right";
            this.txtMedicareTax.Text = null;
            this.txtMedicareTax.Top = 0.7666667F;
            this.txtMedicareTax.Width = 1.5F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1666667F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 4.433333F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold";
            this.Label10.Text = "Medicare";
            this.Label10.Top = 0.7666667F;
            this.Label10.Width = 1.3F;
            // 
            // txtStateTax
            // 
            this.txtStateTax.Height = 0.1666667F;
            this.txtStateTax.Left = 5.8F;
            this.txtStateTax.Name = "txtStateTax";
            this.txtStateTax.Style = "text-align: right";
            this.txtStateTax.Text = null;
            this.txtStateTax.Top = 1F;
            this.txtStateTax.Width = 1.5F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1666667F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 4.433333F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold";
            this.Label11.Text = "State";
            this.Label11.Top = 1F;
            this.Label11.Width = 1.3F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 5.7F;
            this.Line3.LineColor = System.Drawing.Color.FromArgb(2, 0, 0);
            this.Line3.LineWeight = 3F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 1.233333F;
            this.Line3.Width = 1.666667F;
            this.Line3.X1 = 5.7F;
            this.Line3.X2 = 7.366667F;
            this.Line3.Y1 = 1.233333F;
            this.Line3.Y2 = 1.233333F;
            // 
            // txtTotalTax
            // 
            this.txtTotalTax.Height = 0.1666667F;
            this.txtTotalTax.Left = 5.8F;
            this.txtTotalTax.Name = "txtTotalTax";
            this.txtTotalTax.Style = "text-align: right";
            this.txtTotalTax.Text = null;
            this.txtTotalTax.Top = 1.3F;
            this.txtTotalTax.Width = 1.5F;
            // 
            // srptBenefits
            // 
            this.srptBenefits.CloseBorder = false;
            this.srptBenefits.Height = 0.1333333F;
            this.srptBenefits.Left = 0.1666667F;
            this.srptBenefits.Name = "srptBenefits";
            this.srptBenefits.Report = null;
            this.srptBenefits.Top = 2.367F;
            this.srptBenefits.Width = 7.166667F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtPage,
            this.txtTime});
            this.PageHeader.Height = 0.5104167F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "background-color: rgb(255,255,255); font-weight: bold; text-align: center";
            this.Label1.Text = "SIMPLE EMPLOYEE INFORMATION REPORT";
            this.Label1.Top = 0.0625F;
            this.Label1.Width = 7.1875F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1875F;
            this.txtMuniName.Left = 0.125F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Courier\'; font-size: 8pt";
            this.txtMuniName.Text = null;
            this.txtMuniName.Top = 0.0625F;
            this.txtMuniName.Width = 1.4375F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 5.875F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.0625F;
            this.txtDate.Width = 1.4375F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 5.875F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.25F;
            this.txtPage.Width = 1.4375F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0.125F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Courier\'; font-size: 8pt";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.25F;
            this.txtTime.Width = 1.4375F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label12});
            this.PageFooter.Height = 0.5416667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Label12
            // 
            this.Label12.Height = 0.3333333F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.9F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "background-color: rgb(255,255,255); font-weight: bold; text-align: center";
            this.Label12.Text = "This form is being provided so you can view your deductions and benefits for the " +
    "year. Should you have any questions, please contact your Human Resources Directo" +
    "r.";
            this.Label12.Top = 0.06666667F;
            this.Label12.Width = 5.866667F;
            // 
            // txtOtherIncome
            // 
            this.txtOtherIncome.Height = 0.1666667F;
            this.txtOtherIncome.Left = 1.5F;
            this.txtOtherIncome.Name = "txtOtherIncome";
            this.txtOtherIncome.Text = null;
            this.txtOtherIncome.Top = 1.8F;
            this.txtOtherIncome.Width = 2.833333F;
            // 
            // lblOtherIncome
            // 
            this.lblOtherIncome.Height = 0.1666667F;
            this.lblOtherIncome.HyperLink = null;
            this.lblOtherIncome.Left = 0.1333333F;
            this.lblOtherIncome.Name = "lblOtherIncome";
            this.lblOtherIncome.Style = "font-weight: bold";
            this.lblOtherIncome.Text = "Other Income";
            this.lblOtherIncome.Top = 1.8F;
            this.lblOtherIncome.Width = 1.3F;
            // 
            // txtOtherDeduction
            // 
            this.txtOtherDeduction.Height = 0.1666667F;
            this.txtOtherDeduction.Left = 1.5F;
            this.txtOtherDeduction.Name = "txtOtherDeduction";
            this.txtOtherDeduction.Text = null;
            this.txtOtherDeduction.Top = 2F;
            this.txtOtherDeduction.Width = 2.833333F;
            // 
            // lblOtherDeduction
            // 
            this.lblOtherDeduction.Height = 0.1666667F;
            this.lblOtherDeduction.HyperLink = null;
            this.lblOtherDeduction.Left = 0.1333333F;
            this.lblOtherDeduction.Name = "lblOtherDeduction";
            this.lblOtherDeduction.Style = "font-weight: bold";
            this.lblOtherDeduction.Text = "Other Deduction";
            this.lblOtherDeduction.Top = 2F;
            this.lblOtherDeduction.Width = 1.3F;
            // 
            // txtW4Date
            // 
            this.txtW4Date.Height = 0.1666667F;
            this.txtW4Date.Left = 1.5F;
            this.txtW4Date.Name = "txtW4Date";
            this.txtW4Date.Text = null;
            this.txtW4Date.Top = 1.2F;
            this.txtW4Date.Width = 2.833333F;
            // 
            // label13
            // 
            this.label13.Height = 0.1666667F;
            this.label13.HyperLink = null;
            this.label13.Left = 0.133F;
            this.label13.Name = "label13";
            this.label13.Style = "font-weight: bold";
            this.label13.Text = "W-4 Date";
            this.label13.Top = 1.2F;
            this.label13.Width = 1.3F;
            // 
            // rptSimpleEmployeeInformationReport
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.447917F;
            this.Script = "\r\nSub ActiveReport_ReportStart\r\n\r\nEnd Sub\r\n\r\nSub ActiveReport_FetchData(eof)\r\n\r\nE" +
    "nd Sub\r\n";
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmpNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedicareTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherDeduction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherDeduction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtW4Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptEmployeeDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEmpNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalStatus;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateStatus;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicareTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptBenefits;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherIncome;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherIncome;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherDeduction;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherDeduction;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtW4Date;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
    }
}
