//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmError.
	/// </summary>
	partial class frmError
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCButton cmdContinue;
		public fecherFoundation.FCRichTextBox RichTextBox1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1_0;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmError));
            this.Text1 = new fecherFoundation.FCTextBox();
            this.Picture1 = new fecherFoundation.FCPictureBox();
            this.cmdExit = new fecherFoundation.FCButton();
            this.cmdContinue = new fecherFoundation.FCButton();
            this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            this.Picture1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 347);
            this.BottomPanel.Size = new System.Drawing.Size(414, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Text1);
            this.ClientArea.Controls.Add(this.Picture1);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(414, 347);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(414, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // Text1
            // 
            this.Text1.Appearance = 0;
            this.Text1.AutoSize = false;
            this.Text1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Text1.LinkItem = null;
            this.Text1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.Text1.LinkTopic = null;
            this.Text1.Location = new System.Drawing.Point(0, 0);
            this.Text1.Name = "Text1";
            this.Text1.Size = new System.Drawing.Size(406, 17);
            this.Text1.TabIndex = 9;
            this.Text1.Text = "  Error Message";
            // 
            // Picture1
            // 
            this.Picture1.AllowDrop = true;
            this.Picture1.Controls.Add(this.cmdExit);
            this.Picture1.Controls.Add(this.cmdContinue);
            this.Picture1.Controls.Add(this.RichTextBox1);
            this.Picture1.Controls.Add(this.Label1_2);
            this.Picture1.Controls.Add(this.Label1_3);
            this.Picture1.Controls.Add(this.Label1_4);
            this.Picture1.Controls.Add(this.Label1_1);
            this.Picture1.Controls.Add(this.Label2);
            this.Picture1.Controls.Add(this.Label1_0);
            this.Picture1.DrawStyle = ((short)(0));
            this.Picture1.DrawWidth = ((short)(1));
            this.Picture1.FillColor = 16777215;
            this.Picture1.FillStyle = ((short)(1));
            this.Picture1.FontTransparent = true;
            this.Picture1.Image = ((System.Drawing.Image)(resources.GetObject("Picture1.Image")));
            this.Picture1.Location = new System.Drawing.Point(0, 16);
            this.Picture1.Name = "Picture1";
            this.Picture1.Picture = ((System.Drawing.Image)(resources.GetObject("Picture1.Picture")));
            this.Picture1.Size = new System.Drawing.Size(406, 325);
            this.Picture1.TabIndex = 0;
            // 
            // cmdExit
            // 
            this.cmdExit.AppearanceKey = "actionButton";
            this.cmdExit.Location = new System.Drawing.Point(321, 65);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(74, 40);
            this.cmdExit.TabIndex = 8;
            this.cmdExit.Text = "Exit";
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "actionButton";
            this.cmdContinue.Location = new System.Drawing.Point(282, 8);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Size = new System.Drawing.Size(113, 40);
            this.cmdContinue.TabIndex = 7;
            this.cmdContinue.Text = "Continue";
            this.cmdContinue.Visible = false;
            this.cmdContinue.Click += new System.EventHandler(this.cmdContinue_Click);
            // 
            // RichTextBox1
            // 
            this.RichTextBox1.Location = new System.Drawing.Point(0, 121);
            this.RichTextBox1.Multiline = true;
            this.RichTextBox1.Name = "RichTextBox1";
            this.RichTextBox1.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.RichTextBox1.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.RichTextBox1.SelTabCount = null;
            this.RichTextBox1.Size = new System.Drawing.Size(406, 203);
            this.RichTextBox1.TabIndex = 1;
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(8, 53);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(300, 48);
            this.Label1_2.TabIndex = 3;
            this.Label1_2.Text = "LABEL1";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(8, 34);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(383, 25);
            this.Label1_3.TabIndex = 2;
            this.Label1_3.Text = "LABEL1";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(299, 87);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(74, 25);
            this.Label1_4.TabIndex = 10;
            this.Label1_4.Text = "LABEL1";
            this.Label1_4.Visible = false;
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(8, 18);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(383, 25);
            this.Label1_1.TabIndex = 6;
            this.Label1_1.Text = "LABEL1";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(2, 105);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(122, 17);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "CALL STACK";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(8, 4);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(383, 25);
            this.Label1_0.TabIndex = 4;
            this.Label1_0.Text = "LABEL1";
            // 
            // frmError
            // 
            this.ClientSize = new System.Drawing.Size(414, 347);
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmError";
            this.ShowInTaskbar = false;
            this.Text = "Error";
            this.Load += new System.EventHandler(this.frmError_Load);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            this.Picture1.ResumeLayout(false);
            this.Picture1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}