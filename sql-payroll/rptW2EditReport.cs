//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2EditReport.
	/// </summary>
	public partial class rptW2EditReport : BaseSectionReport
	{
		public rptW2EditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W2 Edit Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2EditReport InstancePtr
		{
			get
			{
				return (rptW2EditReport)Sys.GetInstance(typeof(rptW2EditReport));
			}
		}

		protected rptW2EditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				rsBox10?.Dispose();
                rsBox10 = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2EditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intRow;
		int intFound;
		int intpage;
		bool boolShade;
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		
		string strEmployee = "";
		double dblTTLWage;
		double dblFTXGross;
		double dblSSGross;
		double dblMedGross;
		double dblSTXGross;
		double dblBox10;
		double dblFedTax;
		double dblSSTax;
		double dblMedTax;
		double dblStateTax;
		bool boolSummaryDone;
		int intW2Count;
		clsDRWrapper rsBox10 = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			double dblTemp = 0;
			if (rsData.EndOfFile())
			{
				// If boolSummaryDone Then
				eArgs.EOF = true;
				return;
				// Else
				// lblBox12Total.Visible = True
				// txtBox12Total.Visible = True
				// lblBox14Total.Visible = True
				// txtBox14Total.Visible = True
				// txtW2Count.Visible = True
				// lblW2Count.Visible = True
				// 
				// txtW2Count.Text = intW2Count
				// txtBox12Total.Text = Format(gdblBox12Total, "#,##0.00")
				// txtBox14Total.Text = Format(gdblBox14Total, "#,##0.00")
				// 
				// lblBox10.Visible = True
				// txtDependentCare.Visible = True
				// txtDependentCare.Text = Format(dblBox10, "#,##0.00")
				// lblBox10.Font.Bold = True
				// 
				// boolSummaryDone = True
				// txtEmployee.Text = "Summary Totals"
				// txtAddress.Text = vbNullString
				// txtCityStateZip.Text = vbNullString
				// txtSSN.Text = vbNullString
				// txtPen.Text = vbNullString
				// txtDef.Text = vbNullString
				// txtStatEmployee.Text = vbNullString
				// 
				// txtTTLWage.Text = Format(dblTTLWage, "#,##0.00")
				// txtFTXGross.Text = Format(dblFTXGross, "#,##0.00")
				// txtSSGross.Text = Format(dblSSGross, "#,##0.00")
				// txtMEDGross.Text = Format(dblMedGross, "#,##0.00")
				// txtSTXGross.Text = Format(dblSTXGross, "#,##0.00")
				// 
				// PER RON AND KPORT 12/08/2004
				// txtLTXGross.Text = Format(0, "#,##0.00")
				// txtLTXGross.Text = Format(dblLTXGross, "#,##0.00")
				// 
				// txtFedTax.Text = Format(dblFedTax, "#,##0.00")
				// txtSSTax.Text = Format(dblSSTax, "#,##0.00")
				// txtMedTax.Text = Format(dblMedTax, "#,##0.00")
				// txtStateTax.Text = Format(dblStateTax, "#,##0.00")
				// 
				// PER RON AND KPORT 12/08/2004
				// txtLocalTax.Text = Format(0, "#,##0.00")
				// txtLocalTax.Text = Format(dblLocalTax, "#,##0.00")
				// txtEmpID = "ABC123"
				// EOF = False
				// Exit Sub
				// End If
			}
			// lblBox12Total.Visible = False
			// txtBox12Total.Visible = False
			// lblBox14Total.Visible = False
			// txtBox14Total.Visible = False
			// txtW2Count.Visible = False
			// lblW2Count.Visible = False
			intW2Count += 1;
			txtEmployee.Text = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")) + " - " + rsData.Get_Fields_String("EmployeeName");
			txtEmpID.Text = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
			// If txtEmpID.Text = "971" Then
			// txtSSN.Text = txtSSN.Text
			// End If
			if (fecherFoundation.Strings.Trim(txtEmployee.Text) != string.Empty)
			{
				txtAddress.Text = rsData.Get_Fields_String("Address");
				txtCityStateZip.Text = rsData.Get_Fields_String("CityStateZip");
				txtSSN.Text = "SSN=" + Strings.Format(modGlobalRoutines.StripDashes(rsData.Get_Fields_String("SSN")), "000-00-0000");
				if (txtSSN.Text == "SSN=111-11-1111" || Strings.Left(txtSSN.Text, 10) == "SSN=999-99" || txtSSN.Text == "SSN=333-33-3333" || Strings.Left(txtSSN.Text, 10) == "SSN=888-88" || txtSSN.Text == "SSN=123-45-6789" || Strings.Left(txtSSN.Text, 7) == "SSN=000" || Strings.Left(txtSSN.Text, 6) == "999-99" || Strings.Left(txtSSN.Text, 6) == "888-88" || Strings.Left(txtSSN.Text, 3) == "000")
				{
					MessageBox.Show("Employee: " + txtEmployee.Text + " has an invalid SSN number. This must be changed", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					eArgs.EOF = true;
					return;
				}
				txtPen.Text = "PEN=" + (rsData.Get_Fields_Boolean("W2Retirement") ? "Yes" : "No");
				txtDef.Text = "DEF=" + (rsData.Get_Fields_Boolean("W2Deferred") ? "Yes" : "No");
				txtMQGE.Text = "MQGE=" + (rsData.Get_Fields_Boolean("mqge") ? "Yes" : "No");
				txtStatEmployee.Text = "Statutory Employee=" + (rsData.Get_Fields_Boolean("W2StatutoryEmployee") ? "Yes" : "No");
				txtTTLWage.Text = Strings.Format(rsData.Get_Fields_Double("TotalGross"), "#,##0.00");
				dblTTLWage += rsData.Get_Fields_Double("TotalGross");
				txtFTXGross.Text = Strings.Format(rsData.Get_Fields_Double("FederalWage"), "#,##0.00");
				dblFTXGross += rsData.Get_Fields_Double("FederalWage");
				txtSSGross.Text = Strings.Format(rsData.Get_Fields("FICAWage"), "#,##0.00");
				dblSSGross += rsData.Get_Fields_Double("FICAWage");
				txtMEDGross.Text = Strings.Format(rsData.Get_Fields_Double("MedicareWage"), "#,##0.00");
				dblMedGross += rsData.Get_Fields_Double("MedicareWage");
				txtSTXGross.Text = Strings.Format(rsData.Get_Fields_Double("StateWage"), "#,##0.00");
				dblSTXGross += rsData.Get_Fields_Double("StateWage");
				// PER RON AND KPORT 12/08/2004
				txtFedTax.Text = Strings.Format(rsData.Get_Fields_Double("FederalTax"), "#,##0.00");
				dblFedTax += rsData.Get_Fields_Double("FederalTax");
				txtSSTax.Text = Strings.Format(rsData.Get_Fields("FICATax"), "#,##0.00");
				dblSSTax += rsData.Get_Fields_Double("FICATax");
				txtMedTax.Text = Strings.Format(rsData.Get_Fields("MedicareTax"), "#,##0.00");
				dblMedTax += rsData.Get_Fields_Double("MedicareTax");
				txtStateTax.Text = Strings.Format(rsData.Get_Fields_Double("StateTax"), "#,##0.00");
				dblStateTax += rsData.Get_Fields_Double("StateTax");
				// PER RON AND KPORT 12/08/2004
				// NOW FILL BOX 10
				// Call rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber = '" & rsData.Fields("EmployeeNumber") & "'", "TWPY0000.vb1")
				// If Not rsBox10.EndOfFile Then
				// txtDependentCare.Text = Format(Val(rsBox10.Fields("SumOfCYTDAmount")), "#,###,##0.00")
				// dblBox10 = dblBox10 + Val(rsBox10.Fields("SumOfCYTDAmount"))
				dblTemp = 0;
				rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
				if (!rsBox10.EndOfFile())
				{
					dblTemp = Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
				}
				rsBox10.OpenRecordset("SELECT Sum(tblW2matches.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2matches ON tblW2Box12And14.DeductionNumber = tblW2matches.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2matches.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
				if (!rsBox10.EndOfFile())
				{
					dblTemp += Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
				}
				txtDepCare.Text = Strings.Format(dblTemp, "0.00");
				dblBox10 += dblTemp;
			}
			rsData.MoveNext();
			boolShade = !boolShade;
			eArgs.EOF = false;
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intpage += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strReportOrder = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intpage = 1;
			intRow = 1;
			modGlobalVariables.Statics.gdblBox12Total = 0;
			modGlobalVariables.Statics.gdblBox14Total = 0;
			rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
			if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 0)
			{
				// employee name
				strReportOrder = "LastName,FirstName,MiddleName";
			}
			else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 1)
			{
				// employee number
				strReportOrder = "EmployeeNumber";
			}
			else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 2)
			{
				// seqnumber
				strReportOrder = "seqnumber,lastname,firstname";
			}
			else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 3)
			{
				// deptdiv
				strReportOrder = "deptdiv,lastname,firstname";
			}
			rsData.OpenRecordset("SELECT * from tblW2EditTable order by " + strReportOrder, "TWPY0000.vb1");
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modGlobalRoutines.UpdateW2StatusData("EditReport");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolShade)
			{
				Detail.BackColor = ColorTranslator.FromOle(0xF3F3F2);
			}
			else
			{
				Detail.BackColor = Color.White;
			}
			SubReport1.Visible = true;
			SubReport1.Report = new srptW2EditReport();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (intpage == 1)
			{
				txtMuniName.Visible = false;
				txtDate.Visible = false;
				txtPage.Visible = false;
				txtTime.Visible = false;
				txtLabel.Visible = false;
				txtSubLabel.Visible = false;
			}
			else
			{
				txtMuniName.Visible = true;
				txtDate.Visible = true;
				txtPage.Visible = true;
				txtTime.Visible = true;
				txtLabel.Visible = true;
				txtSubLabel.Visible = true;
				txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtPage.Text = "Page " + FCConvert.ToString(intpage);
				txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtBox12Total.Text = Strings.Format(modGlobalVariables.Statics.gdblBox12Total, "#,###,###,##0.00");
			txtBox14Total.Text = Strings.Format(modGlobalVariables.Statics.gdblBox14Total, "#,###,###,##0.00");
			txtW2Count.Text = intW2Count.ToString();
			txtDependentCare.Text = Strings.Format(dblBox10, "#,##0.00");
			lblBox10.Font = new System.Drawing.Font(lblBox10.Font, System.Drawing.FontStyle.Bold);
			boolSummaryDone = true;
			txtTotTTLWage.Text = Strings.Format(dblTTLWage, "#,##0.00");
			txtTotFTXGross.Text = Strings.Format(dblFTXGross, "#,##0.00");
			txtTotSSGross.Text = Strings.Format(dblSSGross, "#,##0.00");
			txtTotMedGross.Text = Strings.Format(dblMedGross, "#,##0.00");
			txtTotSTXGross.Text = Strings.Format(dblSTXGross, "#,##0.00");
			// PER RON AND KPORT 12/08/2004
			// txtLTXGross.Text = Format(dblLTXGross, "#,##0.00")
			txtTotFedTax.Text = Strings.Format(dblFedTax, "#,##0.00");
			txtTotSSTax.Text = Strings.Format(dblSSTax, "#,##0.00");
			txtTotMedTax.Text = Strings.Format(dblMedTax, "#,##0.00");
			txtTotStateTax.Text = Strings.Format(dblStateTax, "#,##0.00");
			// PER RON AND KPORT 12/08/2004
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			txtRHMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtRHDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtRHPage.Text = "Page " + FCConvert.ToString(intpage);
			txtRHTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
            using (clsDRWrapper rsReportHeader = new clsDRWrapper())
            {
                rsReportHeader.OpenRecordset("Select * from tblW2Master");
                if (!rsReportHeader.EndOfFile())
                {
                    txtFederalEIN.Text = rsReportHeader.Get_Fields_String("EmployersFederalID");
                    txtStateEIN.Text = rsReportHeader.Get_Fields_String("EmployersStateID");
                    txtState.Text = rsReportHeader.Get_Fields_String("StateCode");
                    txtName.Text = rsReportHeader.Get_Fields_String("EmployersName");
                    txtAddressField.Text = rsReportHeader.Get_Fields_String("Address1");
                    txtCity.Text = rsReportHeader.Get_Fields_String("City") + " " + rsReportHeader.Get_Fields("State") +
                                   " " + rsReportHeader.Get_Fields("Zip");
                    txtContact.Text = rsReportHeader.Get_Fields_String("ContactPerson");
                    txtPhone.Text = rsReportHeader.Get_Fields_String("ContactTelephoneNumber");
                    txtExtension.Text = rsReportHeader.Get_Fields_String("ContactTelephoneExt");
                    txtFax.Text = rsReportHeader.Get_Fields_String("ContactFaxNumber");
                    if (FCConvert.ToInt32(rsReportHeader.Get_Fields_String("ContactPreference1")) == 1)
                    {
                        txtContactMethod.Text = "E-Mail/Internet";
                        // ElseIf .Fields("ContactPreference1") = 2 Then
                        // txtContactMethod = "Fax"
                    }
                    else
                    {
                        txtContactMethod.Text = "Postal Service";
                    }

                    txtEmail.Text = rsReportHeader.Get_Fields_String("ContactEmilAddress");
                }
            }
        }

		
	}
}
