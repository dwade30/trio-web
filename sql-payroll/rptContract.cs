//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptContract.
	/// </summary>
	public partial class rptContract : BaseSectionReport
	{
		public rptContract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Contract Setup";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptContract InstancePtr
		{
			get
			{
				return (rptContract)Sys.GetInstance(typeof(rptContract));
			}
		}

		protected rptContract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptContract	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolOnlySummary;
		clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(bool boolSummary)
		{
			boolOnlySummary = boolSummary;
			rsReport.OpenRecordset("select contracts.*,lastname,firstname,middlename,desig from tblemployeemaster inner join contracts on (tblemployeemaster.employeenumber = contracts.employeenumber) where not STATUS = 'Terminated' and not status = 'Resigned' order by lastname,firstname,middlename,ID", "twpy0000.vb1");
			if (rsReport.EndOfFile())
			{
				return;
			}
			if (boolOnlySummary)
			{
				lblTitle.Text = "Contract Summary";
			}
			else
			{
				lblTitle.Text = "Contract Detail";
			}
			frmReportViewer.InstancePtr.Init(this, boolLandscape: true, boolAllowEmail: true, strAttachmentName: "Contracts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			if (!boolOnlySummary)
			{
				lblTitle.Text = "Contract Detail";
				SubReport1.Visible = true;
				SubReport1.Report = new srptContractAccounts();
			}
			else
			{
				lblTitle.Text = "Contract Summary";
				SubReport1.Visible = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				string strTemp = "";
				clsContract clsConCT = new clsContract();
				txtEmployeeNumber.Text = rsReport.Get_Fields_String("employeenumber");
				txtName.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields("lastname") + ", " + fecherFoundation.Strings.Trim(rsReport.Get_Fields("firstname") + " " + rsReport.Get_Fields("middlename")) + " " + rsReport.Get_Fields("desig"));
				txtContract.Text = rsReport.Get_Fields_String("contractid");
				txtDescription.Text = rsReport.Get_Fields_String("description");
				strTemp = "";
				if (Information.IsDate(rsReport.Get_Fields("startdate")))
				{
					if (Convert.ToDateTime(rsReport.Get_Fields("startdate")).ToOADate() != 0)
					{
						strTemp = Strings.Format(rsReport.Get_Fields("startdate"), "MM/dd/yyyy");
					}
				}
				txtStart.Text = strTemp;
				strTemp = "";
				if (Information.IsDate(rsReport.Get_Fields("enddate")))
				{
					if (Convert.ToDateTime(rsReport.Get_Fields("enddate")).ToOADate() != 0)
					{
						strTemp = Strings.Format(rsReport.Get_Fields("enddate"), "MM/dd/yyyy");
					}
				}
				txtEnd.Text = strTemp;
				clsConCT.LoadContract(FCConvert.ToInt32(Conversion.Val(rsReport.Get_Fields("contractid"))));
				txtAmount.Text = Strings.Format(clsConCT.OriginalAmount, "#,###,##0.00");
				txtPaid.Text = Strings.Format(clsConCT.AmountPaid, "#,###,##0.00");
				txtRemaining.Text = Strings.Format(clsConCT.OriginalAmount - clsConCT.AmountPaid, "#,###,##0.00");
				if (!boolOnlySummary)
				{
					SubReport1.Report.UserData = clsConCT.ContractID;
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
