//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmWarrant : BaseForm
	{
		public frmWarrant()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmWarrant InstancePtr
		{
			get
			{
				return (frmWarrant)Sys.GetInstance(typeof(frmWarrant));
			}
		}

		protected frmWarrant _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/26/02
		// This form will be used to select which warrant report to
		// print whether it be a new one or reprinting an old one
		// ********************************************************
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsInfo = new clsDRWrapper();
			public clsDRWrapper rsInfo_AutoInitialized = null;
			public clsDRWrapper rsInfo
			{
				get
				{
					if ( rsInfo_AutoInitialized == null)
					{
						 rsInfo_AutoInitialized = new clsDRWrapper();
					}
					return rsInfo_AutoInitialized;
				}
				set
				{
					 rsInfo_AutoInitialized = value;
				}
			}
		public string strPrinterName = "";
        public List<string> batchReports;

        private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdCancel2_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (cmbReprint.Text == "Initial Run")
			{
				rsInfo.OpenRecordset("SELECT DISTINCT PayDate, PayRunID From tblPayrollSteps WHERE CheckRegister = 1 AND isnull(Warrant, 0) = 0");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						strPrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					}
					else
					{
						rptWarrant.InstancePtr.Unload();
					}
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
					{
						this.Hide();
					}
					if (rsInfo.RecordCount() > 1)
					{
						do
						{
							//FCGlobal.Printer.DeviceName = strPrinterName;
							modDuplexPrinting.DuplexPrintReport(rptWarrant.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptWarrant.PrintReport False
							rptWarrant.InstancePtr.Unload();
							CheckAgain:
							;
							if (JobComplete())
							{
								rsInfo.MoveNext();
							}
							else
							{
								goto CheckAgain;
							}
						}
						while (rsInfo.EndOfFile() != true);
					}
					else
					{
						//FCGlobal.Printer.DeviceName = strPrinterName;
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
						{
							modDuplexPrinting.DuplexPrintReport(rptWarrant.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
							// rptWarrant.PrintReport False
						}
						else
						{
							modDuplexPrinting.DuplexPrintReport(rptWarrant.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptWarrant.PrintReport False
							// rptWarrant.Show
						}
					}
					Close();
				}
				else
				{
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
						rptNoInformation.InstancePtr.Init("Payroll Warrant");
						rptNoInformation.InstancePtr.Unload();
						modDavesSweetCode.Statics.blnReportCompleted = true;
						this.Hide();
						return;
					}
					else
					{
						MessageBox.Show("There is no information ready to be put on a Warrant.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					// DAVE IS THIS CORRECT? MATTHEW 04/21/2003
					modDavesSweetCode.Statics.blnReportCompleted = true;
					Close();
					return;
				}
			}
			else
			{
				if (cboReport.Items.Count > 0)
				{
					fraSelectReport.Visible = true;
					cmbReprint.Visible = false;
                    lblReprint.Visible = false;
                    cmdProcess.Visible = false;
				}
				else
				{
					MessageBox.Show("There are no reports to print.", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					return;
				}
			}
			
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			if (cboReport.SelectedIndex == -1)
			{
				MessageBox.Show("You must select which report you wish to reprint before you may continue.", "No Report Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (cmbReprint.Text == "Initial Run")
				{
					//fecherFoundation.Information.Err().Clear();
					//dlg1.Show();
					//if (fecherFoundation.Information.Err().Number == 0)
					//{
					//	strPrinterName = FCGlobal.Printer.DeviceName;
					//}
					//else
					//{
					//	// User Clicked Cancel   **** DO NOT PRINT ****
					//	fecherFoundation.Information.Err().Clear();
					//	return;
					//}
				}
				// rsInfo.OpenRecordset "SELECT DISTINCT PayDate, PayRunID From tblPayrollSteps WHERE Warrant = 1 AND PayDate = '" & Mid(cboReport.Text, 25, 10) & "' AND PayRunID = " & Mid(cboReport.Text, 41, 2)
				rsInfo.OpenRecordset("SELECT DISTINCT PayDate, PayRunID From tblPayrollSteps WHERE Warrant = 1 AND Id = " + FCConvert.ToString(cboReport.ItemData(cboReport.SelectedIndex)), "Payroll");
				if (rsInfo.EndOfFile())
				{
					MessageBox.Show("TRIO Error message for warrant posting. Information for PayDate and Pay run not found in tblPayrollSteps.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						modDuplexPrinting.DuplexPrintReport(rptWarrant.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
						// rptWarrant.PrintReport False
					}
					else
					{
						rptWarrant.InstancePtr.Unload();
						if (cmbReprint.Text == "Initial Run")
						{
							modDuplexPrinting.DuplexPrintReport(rptWarrant.InstancePtr, strPrinterName, batchReports: batchReports);
						}
						else
						{
							rptWarrant.InstancePtr.Init(strPrinterName, true);
						}
						// rptWarrant.PrintReport False
						// rptWarrant.Show
					}
				}
				
				Close();
			}
		}

		private void Command2_Click()
		{
			Close();
		}

		public void Init()
		{
		}

		private void frmWarrant_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsReportInfo = new clsDRWrapper();
			clsDRWrapper Master = new clsDRWrapper();
			// vbPorter upgrade warning: strWarrant As string	OnWriteFCConvert.ToInt32(
			string strWarrant = "";
			// Call ForceFormToResize(Me)
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			rsReportInfo.OpenRecordset("SELECT * FROM tblPayrollSteps ORDER BY PayDate Desc, PayRunID");
			cboReport.Clear();
			if (rsReportInfo.EndOfFile() != true && rsReportInfo.BeginningOfFile() != true)
			{
                do
				{
					// *********************************************
					// added on 2/24/2004 as there were warrants showing that were 0000
					// after just a setup on a pay run. Matthew
					strWarrant = FCConvert.ToString(modDavesSweetCode.GetPayrollRunWarrantNumber(rsReportInfo.Get_Fields_DateTime("PayDate"), rsReportInfo.Get_Fields_Int32("PayRunID")));
					if (FCConvert.ToDouble(strWarrant) != 0)
					{
						// *********************************************
						cboReport.AddItem("Warrant " + strWarrant + "  Pay Date: " + Strings.Format(rsReportInfo.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy") + " Run: " + Strings.Format(rsReportInfo.Get_Fields("PayRunID"), "00"));
						cboReport.ItemData(cboReport.NewIndex, FCConvert.ToInt32(rsReportInfo.Get_Fields("id")));
					}
					rsReportInfo.MoveNext();
				}
				while (rsReportInfo.EndOfFile() != true);
			}
			if (cboReport.Items.Count > 0)
			{
				cboReport.SelectedIndex = 0;
			}
			if (modGlobalVariables.Statics.gstrMQYProcessing != "NONE" || modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				cmdProcess_Click();
			}
			else
			{
				// optReprint.Value = True
				// cmdProcess_Click
			}
			this.Refresh();
		}

		private void frmWarrant_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}

		private void frmWarrant_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool JobComplete()
		{
			bool JobComplete = false;
			foreach (FCForm ff in FCGlobal.Statics.Forms)
			{
				if (ff.Name == "rptWarrant")
				{
					JobComplete = false;
					return JobComplete;
				}
			}
			JobComplete = true;
			return JobComplete;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.SetFocus
		}

		private void cboReport_DropDown(object sender, System.EventArgs e)
		{
			//modAPIsConst.SendMessageByNum(cboReport.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}
	}
}
