﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCustomDistributionReports.
	/// </summary>
	partial class frmCustomDistributionReports
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCComboBox cmbReportOn;
		public fecherFoundation.FCFrame fraSummary;
		public fecherFoundation.FCCheckBox chkSubgroup;
		public fecherFoundation.FCCheckBox chkSummaryOfAccounts;
		public fecherFoundation.FCCheckBox chkShowDetails;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtGroup;
		public fecherFoundation.FCTextBox txtEmployeeBoth;
		public fecherFoundation.FCTextBox txtAccountEmployee;
		public T2KDateBox txtEndDate;
		public T2KDateBox txtStartDate;
		public fecherFoundation.FCTextBox txtEmployeeNumber;
		public FCGrid VSGrid;
		public fecherFoundation.FCLabel lblGroup;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel lblEmployeeBoth;
		public fecherFoundation.FCLabel lblAccountEmp;
		public fecherFoundation.FCLabel lblCaption;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCListBox lstDistributions;
		public fecherFoundation.FCFrame fraSelect;
		public fecherFoundation.FCCheckBox ChkAll;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbType = new fecherFoundation.FCComboBox();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.cmbReportOn = new fecherFoundation.FCComboBox();
            this.fraSummary = new fecherFoundation.FCFrame();
            this.chkSubgroup = new fecherFoundation.FCCheckBox();
            this.chkSummaryOfAccounts = new fecherFoundation.FCCheckBox();
            this.chkShowDetails = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtGroup = new fecherFoundation.FCTextBox();
            this.txtEmployeeBoth = new fecherFoundation.FCTextBox();
            this.txtAccountEmployee = new fecherFoundation.FCTextBox();
            this.txtEndDate = new T2KDateBox();
            this.txtStartDate = new T2KDateBox();
            this.txtEmployeeNumber = new fecherFoundation.FCTextBox();
            this.VSGrid = new fecherFoundation.FCGrid();
            this.lblGroup = new fecherFoundation.FCLabel();
            this.lblDate = new fecherFoundation.FCLabel();
            this.lblEmployeeBoth = new fecherFoundation.FCLabel();
            this.lblAccountEmp = new fecherFoundation.FCLabel();
            this.lblCaption = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lstDistributions = new fecherFoundation.FCListBox();
            this.fraSelect = new fecherFoundation.FCFrame();
            this.ChkAll = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
            this.fraSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSubgroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOfAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VSGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelect)).BeginInit();
            this.fraSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(733, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSummary);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lstDistributions);
            this.ClientArea.Controls.Add(this.fraSelect);
            this.ClientArea.Size = new System.Drawing.Size(733, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(733, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(229, 30);
            this.HeaderText.Text = "Distribution Reports";
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Detail",
            "Summary",
            "Distribution Setup Information"});
            this.cmbType.Location = new System.Drawing.Point(20, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(374, 40);
            this.cmbType.TabIndex = 13;
            this.cmbType.Text = "Detail";
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.optType_CheckedChanged);
            // 
            // cmbSort
            // 
            this.cmbSort.Items.AddRange(new object[] {
            "Employee Number",
            "Employee Name",
            "Pay Category",
            "Pay Date",
            "Account Number",
            "Home Dept / Div",
            "Paid Dept / Div"});
            this.cmbSort.Location = new System.Drawing.Point(209, 442);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(295, 40);
            this.cmbSort.TabIndex = 24;
            this.cmbSort.Text = "Employee Number";
            this.cmbSort.SelectedIndexChanged += new System.EventHandler(this.optSort_CheckedChanged);
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(30, 456);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(127, 15);
            this.lblSort.TabIndex = 25;
            this.lblSort.Text = "GROUP REPORT BY";
            // 
            // cmbReportOn
            // 
            this.cmbReportOn.Items.AddRange(new object[] {
            "Single Employee",
            "Emp / Date Range",
            "Date Range",
            "Account Numbers",
            "Emp / Account Num",
            "Single Group",
            "All Files"});
            this.cmbReportOn.Location = new System.Drawing.Point(20, 30);
            this.cmbReportOn.Name = "cmbReportOn";
            this.cmbReportOn.Size = new System.Drawing.Size(233, 40);
            this.cmbReportOn.TabIndex = 8;
            this.cmbReportOn.Text = "Single Employee";
            this.cmbReportOn.SelectedIndexChanged += new System.EventHandler(this.optReportOn_CheckedChanged);
            // 
            // fraSummary
            // 
            this.fraSummary.Controls.Add(this.chkSubgroup);
            this.fraSummary.Controls.Add(this.cmbType);
            this.fraSummary.Controls.Add(this.chkSummaryOfAccounts);
            this.fraSummary.Controls.Add(this.chkShowDetails);
            this.fraSummary.Location = new System.Drawing.Point(283, 502);
            this.fraSummary.Name = "fraSummary";
            this.fraSummary.Size = new System.Drawing.Size(414, 137);
            this.fraSummary.TabIndex = 23;
            this.fraSummary.Text = "Type Of Report";
            // 
            // chkSubgroup
            // 
            this.chkSubgroup.Location = new System.Drawing.Point(163, 90);
            this.chkSubgroup.Name = "chkSubgroup";
            this.chkSubgroup.Size = new System.Drawing.Size(230, 27);
            this.chkSubgroup.TabIndex = 12;
            this.chkSubgroup.Text = "Sub-group by Pay Category";
            this.chkSubgroup.Visible = false;
            // 
            // chkSummaryOfAccounts
            // 
            this.chkSummaryOfAccounts.Location = new System.Drawing.Point(163, 90);
            this.chkSummaryOfAccounts.Name = "chkSummaryOfAccounts";
            this.chkSummaryOfAccounts.Size = new System.Drawing.Size(209, 27);
            this.chkSummaryOfAccounts.TabIndex = 35;
            this.chkSummaryOfAccounts.Text = "Order/Group by Dept/Div";
            this.chkSummaryOfAccounts.Visible = false;
            // 
            // chkShowDetails
            // 
            this.chkShowDetails.Checked = true;
            this.chkShowDetails.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkShowDetails.Location = new System.Drawing.Point(20, 90);
            this.chkShowDetails.Name = "chkShowDetails";
            this.chkShowDetails.Size = new System.Drawing.Size(123, 27);
            this.chkShowDetails.TabIndex = 11;
            this.chkShowDetails.Text = "Show Details";
            this.chkShowDetails.Visible = false;
            this.chkShowDetails.CheckedChanged += new System.EventHandler(this.chkShowDetails_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtGroup);
            this.Frame1.Controls.Add(this.cmbReportOn);
            this.Frame1.Controls.Add(this.txtEmployeeBoth);
            this.Frame1.Controls.Add(this.txtAccountEmployee);
            this.Frame1.Controls.Add(this.txtEndDate);
            this.Frame1.Controls.Add(this.txtStartDate);
            this.Frame1.Controls.Add(this.txtEmployeeNumber);
            this.Frame1.Controls.Add(this.VSGrid);
            this.Frame1.Controls.Add(this.lblGroup);
            this.Frame1.Controls.Add(this.lblDate);
            this.Frame1.Controls.Add(this.lblEmployeeBoth);
            this.Frame1.Controls.Add(this.lblAccountEmp);
            this.Frame1.Controls.Add(this.lblCaption);
            this.Frame1.Controls.Add(this.lblAccount);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(474, 392);
            this.Frame1.TabIndex = 21;
            this.Frame1.Text = "Report Criteria";
            // 
            // txtGroup
            // 
            this.txtGroup.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroup.Location = new System.Drawing.Point(154, 332);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(140, 40);
            this.txtGroup.TabIndex = 7;
            this.txtGroup.Visible = false;
            // 
            // txtEmployeeBoth
            // 
            this.txtEmployeeBoth.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeBoth.Location = new System.Drawing.Point(154, 150);
            this.txtEmployeeBoth.Name = "txtEmployeeBoth";
            this.txtEmployeeBoth.Size = new System.Drawing.Size(140, 40);
            this.txtEmployeeBoth.TabIndex = 2;
            // 
            // txtAccountEmployee
            // 
            this.txtAccountEmployee.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccountEmployee.Location = new System.Drawing.Point(154, 272);
            this.txtAccountEmployee.Name = "txtAccountEmployee";
            this.txtAccountEmployee.Size = new System.Drawing.Size(140, 40);
            this.txtAccountEmployee.TabIndex = 6;
            this.txtAccountEmployee.Visible = false;
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEndDate.Location = new System.Drawing.Point(314, 150);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(140, 40);
            this.txtEndDate.TabIndex = 4;
            this.txtEndDate.Visible = false;
            //this.txtEndDate.Enter += new System.EventHandler(this.txtEndDate_Enter);
            //this.txtEndDate.TextChanged += new System.EventHandler(this.txtEndDate_TextChanged);
            // 
            // txtStartDate
            // 
            this.txtStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtStartDate.Location = new System.Drawing.Point(314, 90);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(140, 40);
            this.txtStartDate.TabIndex = 3;
            this.txtStartDate.Visible = false;
            //this.txtStartDate.Enter += new System.EventHandler(this.txtStartDate_Enter);
            //this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_TextChanged);
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeNumber.Location = new System.Drawing.Point(154, 90);
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Size = new System.Drawing.Size(140, 40);
            this.txtEmployeeNumber.TabIndex = 1;
            this.txtEmployeeNumber.Visible = false;
            // 
            // VSGrid
            // 
            this.VSGrid.ColumnHeadersVisible = false;
            this.VSGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.VSGrid.FixedCols = 0;
            this.VSGrid.FixedRows = 0;
            this.VSGrid.Location = new System.Drawing.Point(154, 210);
            this.VSGrid.Name = "VSGrid";
            this.VSGrid.ReadOnly = false;
            this.VSGrid.RowHeadersVisible = false;
            this.VSGrid.Rows = 1;
            this.VSGrid.Size = new System.Drawing.Size(300, 42);
            this.VSGrid.StandardTab = false;
            this.VSGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.VSGrid.TabIndex = 5;
            this.VSGrid.Visible = false;
            this.VSGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.VSGrid_ValidateEdit);
            this.VSGrid.CurrentCellChanged += new System.EventHandler(this.VSGrid_RowColChange);
            // 
            // lblGroup
            // 
            this.lblGroup.Location = new System.Drawing.Point(20, 346);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(67, 15);
            this.lblGroup.TabIndex = 36;
            this.lblGroup.Text = "GROUP #";
            this.lblGroup.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(314, 55);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(45, 15);
            this.lblDate.TabIndex = 34;
            this.lblDate.Text = "DATE";
            this.lblDate.Visible = false;
            // 
            // lblEmployeeBoth
            // 
            this.lblEmployeeBoth.Location = new System.Drawing.Point(20, 164);
            this.lblEmployeeBoth.Name = "lblEmployeeBoth";
            this.lblEmployeeBoth.Size = new System.Drawing.Size(81, 15);
            this.lblEmployeeBoth.TabIndex = 33;
            this.lblEmployeeBoth.Text = "EMPLOYEE";
            // 
            // lblAccountEmp
            // 
            this.lblAccountEmp.Location = new System.Drawing.Point(20, 286);
            this.lblAccountEmp.Name = "lblAccountEmp";
            this.lblAccountEmp.Size = new System.Drawing.Size(81, 15);
            this.lblAccountEmp.TabIndex = 32;
            this.lblAccountEmp.Text = "EMPLOYEE";
            this.lblAccountEmp.Visible = false;
            // 
            // lblCaption
            // 
            this.lblCaption.Location = new System.Drawing.Point(20, 104);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(81, 15);
            this.lblCaption.TabIndex = 31;
            this.lblCaption.Text = "EMPLOYEE";
            this.lblCaption.Visible = false;
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(20, 224);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(83, 15);
            this.lblAccount.TabIndex = 30;
            this.lblAccount.Text = "ACCOUNTS";
            this.lblAccount.Visible = false;
            // 
            // lstDistributions
            // 
            this.lstDistributions.BackColor = System.Drawing.SystemColors.Window;
            this.lstDistributions.CheckBoxes = true;
            this.lstDistributions.Location = new System.Drawing.Point(30, 659);
            this.lstDistributions.Name = "lstDistributions";
            this.lstDistributions.Size = new System.Drawing.Size(667, 242);
            this.lstDistributions.Style = 1;
            this.lstDistributions.TabIndex = 13;
            // 
            // fraSelect
            // 
            this.fraSelect.Controls.Add(this.ChkAll);
            this.fraSelect.Location = new System.Drawing.Point(30, 502);
            this.fraSelect.Name = "fraSelect";
            this.fraSelect.Size = new System.Drawing.Size(223, 137);
            this.fraSelect.TabIndex = 20;
            this.fraSelect.Text = "Select";
            // 
            // ChkAll
            // 
            this.ChkAll.Location = new System.Drawing.Point(20, 30);
            this.ChkAll.Name = "ChkAll";
            this.ChkAll.Size = new System.Drawing.Size(183, 27);
            this.ChkAll.TabIndex = 9;
            this.ChkAll.Text = "Select All Distribution";
            this.ChkAll.CheckedChanged += new System.EventHandler(this.ChkAll_CheckedChanged);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Print / Preview";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(291, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(160, 48);
            this.cmdSaveContinue.Text = "Print / Preview";
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
            // 
            // frmCustomDistributionReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(733, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomDistributionReports";
            this.ShowInTaskbar = false;
            this.Text = "Distribution Reports";
            this.Load += new System.EventHandler(this.frmCustomDistributionReports_Load);
            this.Activated += new System.EventHandler(this.frmCustomDistributionReports_Activated);
            this.Resize += new System.EventHandler(this.frmCustomDistributionReports_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomDistributionReports_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
            this.fraSummary.ResumeLayout(false);
            this.fraSummary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSubgroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOfAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VSGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelect)).EndInit();
            this.fraSelect.ResumeLayout(false);
            this.fraSelect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
    }
}
