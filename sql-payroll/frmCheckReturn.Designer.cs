//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCheckReturn.
	/// </summary>
	partial class frmCheckReturn
	{
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblCaption;
        public fecherFoundation.FCButton cmbCancel;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAll;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsData = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblCaption = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAll = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cmbCancel = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 514);
            this.BottomPanel.Size = new System.Drawing.Size(781, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblCaption);
            this.ClientArea.Size = new System.Drawing.Size(781, 454);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmbCancel);
            this.TopPanel.Size = new System.Drawing.Size(781, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(162, 30);
            this.HeaderText.Text = "Check Return";
            // 
            // vsData
            // 
            this.vsData.AllowSelection = false;
            this.vsData.AllowUserToResizeColumns = false;
            this.vsData.AllowUserToResizeRows = false;
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsData.BackColorBkg = System.Drawing.Color.Empty;
            this.vsData.BackColorFixed = System.Drawing.Color.Empty;
            this.vsData.BackColorSel = System.Drawing.Color.Empty;
            this.vsData.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsData.Cols = 4;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsData.ColumnHeadersHeight = 30;
            this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsData.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsData.DragIcon = null;
            this.vsData.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsData.FixedCols = 0;
            this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsData.FrozenCols = 0;
            this.vsData.GridColor = System.Drawing.Color.Empty;
            this.vsData.GridColorFixed = System.Drawing.Color.Empty;
            this.vsData.Location = new System.Drawing.Point(30, 105);
            this.vsData.Name = "vsData";
            this.vsData.OutlineCol = 0;
            this.vsData.RowHeadersVisible = false;
            this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsData.RowHeightMin = 0;
            this.vsData.Rows = 1;
            this.vsData.ScrollTipText = null;
            this.vsData.ShowColumnVisibilityMenu = false;
            this.vsData.Size = new System.Drawing.Size(723, 339);
            this.vsData.StandardTab = true;
            this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsData.TabIndex = 0;
            this.vsData.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsData_ClickEvent);
            this.vsData.DoubleClick += new System.EventHandler(this.vsData_DblClick);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 70);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(723, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "SELECTING YES WILL INDICATE THAT YOU WISH THE NEXT TA CHECK FOR THIS RECIPIENT TO" +
    " BE REDUCED BY THOSE AMOUNTS";
            // 
            // lblCaption
            // 
            this.lblCaption.Location = new System.Drawing.Point(30, 30);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(723, 30);
            this.lblCaption.TabIndex = 1;
            this.lblCaption.Text = "LABEL1";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess,
            this.mnuAll,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Process";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuAll
            // 
            this.mnuAll.Index = 1;
            this.mnuAll.Name = "mnuAll";
            this.mnuAll.Text = "Select/Deselect All";
            this.mnuAll.Visible = false;
            this.mnuAll.Click += new System.EventHandler(this.mnuAll_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Visible = false;
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(332, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(102, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			//
            //cmbCancel
            //
            this.cmbCancel.Anchor = (Wisej.Web.AnchorStyles)(Wisej.Web.AnchorStyles.Right | Wisej.Web.AnchorStyles.Top);
            this.cmbCancel.AppearanceKey = "toolbarButton";
            this.cmbCancel.Location = new System.Drawing.Point(841, 10);
            this.cmbCancel.Name = "cmbCancel";
            this.cmbCancel.Size = new System.Drawing.Size(60, 20);
            this.cmbCancel.Text = "Cancel";
            this.cmbCancel.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmCheckReturn
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(781, 622);
            this.CancelButton = cmbCancel;
            //this.CloseBox = true;
            this.ControlBox = false;
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCheckReturn";
            this.Text = "Check Return";
            this.Load += new System.EventHandler(this.frmCheckReturn_Load);
            this.Activated += new System.EventHandler(this.frmCheckReturn_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCheckReturn_KeyPress);
            this.Resize += new System.EventHandler(this.frmCheckReturn_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}