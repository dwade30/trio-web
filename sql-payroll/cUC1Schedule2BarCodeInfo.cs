﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cUC1Schedule2BarCodeInfo
	{
		//=========================================================
		const int Max_UC1_Detail_Lines = 18;
		// vbPorter upgrade warning: strVendorCode As int	OnWrite(string)
		private int strVendorCode;
		private string strSoftwareVersion = string.Empty;
		// vbPorter upgrade warning: strMaineVendorCode As int	OnWrite(string)
		private int strMaineVendorCode;
		private string strStateCode = string.Empty;
		private string strYear = string.Empty;
		private string strFormCode = string.Empty;
		private string strFormVersionNumber = string.Empty;
		private DateTime dtStart;
		private DateTime dtEnd;
		private string strFedId = string.Empty;
		private string[] strPayeeSSN = new string[Max_UC1_Detail_Lines + 1];
		private double[] dblUCGrossWages = new double[Max_UC1_Detail_Lines + 1];
		private bool[] boolIsSeasonal = new bool[Max_UC1_Detail_Lines + 1];
		private double dblUCGrossPageTotal;

		public void Set_SSN(int intindex, string strSSN)
		{
			strPayeeSSN[intindex - 1] = strSSN;
		}

		public string Get_SSN(int intindex)
		{
			string SSN = "";
			SSN = strPayeeSSN[intindex - 1];
			return SSN;
		}

		public void Set_UCGrossWages(int intindex, double dblWages)
		{
			dblUCGrossWages[intindex - 1] = dblWages;
		}

		public double Get_UCGrossWages(int intindex)
		{
			double UCGrossWages = 0;
			UCGrossWages = dblUCGrossWages[intindex - 1];
			return UCGrossWages;
		}

		public void Set_IsSeasonal(int intindex, bool boolSeasonal)
		{
			boolIsSeasonal[intindex - 1] = boolSeasonal;
		}

		public bool Get_IsSeasonal(int intindex)
		{
			bool IsSeasonal = false;
			IsSeasonal = boolIsSeasonal[intindex - 1];
			return IsSeasonal;
		}

		public double UCGrossPageTotal
		{
			set
			{
				dblUCGrossPageTotal = value;
			}
			get
			{
				double UCGrossPageTotal = 0;
				UCGrossPageTotal = dblUCGrossPageTotal;
				return UCGrossPageTotal;
			}
		}
		// vbPorter upgrade warning: strCode As string	OnRead
		public string VendorCode
		{
			set
			{
				strVendorCode = FCConvert.ToInt32(value);
			}
			// vbPorter upgrade warning: 'Return' As string	OnWrite
			get
			{
				string VendorCode = "";
				VendorCode = FCConvert.ToString(strVendorCode);
				return VendorCode;
			}
		}

		public string SoftwareVersion
		{
			set
			{
				strSoftwareVersion = value;
			}
			get
			{
				string SoftwareVersion = "";
				SoftwareVersion = strSoftwareVersion;
				return SoftwareVersion;
			}
		}
		// vbPorter upgrade warning: strCode As string	OnRead
		public string MaineVendorCode
		{
			set
			{
				strMaineVendorCode = FCConvert.ToInt32(value);
			}
			// vbPorter upgrade warning: 'Return' As string	OnWrite
			get
			{
				string MaineVendorCode = "";
				MaineVendorCode = FCConvert.ToString(strMaineVendorCode);
				return MaineVendorCode;
			}
		}

		public string StateCode
		{
			set
			{
				strStateCode = value;
			}
			get
			{
				string StateCode = "";
				StateCode = strStateCode;
				return StateCode;
			}
		}

		public string ReportYear
		{
			set
			{
				strYear = value;
			}
			get
			{
				string ReportYear = "";
				ReportYear = strYear;
				return ReportYear;
			}
		}

		public string FormCode
		{
			set
			{
				strFormCode = value;
			}
			get
			{
				string FormCode = "";
				FormCode = strFormCode;
				return FormCode;
			}
		}

		public string FormVersionNumber
		{
			set
			{
				strFormVersionNumber = value;
			}
			get
			{
				string FormVersionNumber = "";
				FormVersionNumber = strFormVersionNumber;
				return FormVersionNumber;
			}
		}

		public DateTime StartDate
		{
			set
			{
				dtStart = value;
			}
			get
			{
				DateTime StartDate = System.DateTime.Now;
				StartDate = dtStart;
				return StartDate;
			}
		}

		public DateTime EndDate
		{
			set
			{
				dtEnd = value;
			}
			get
			{
				DateTime EndDate = System.DateTime.Now;
				EndDate = dtEnd;
				return EndDate;
			}
		}

		public string FederalID
		{
			set
			{
				strFedId = value;
			}
			get
			{
				string FederalID = "";
				FederalID = strFedId;
				return FederalID;
			}
		}

		private string AddNumLine(object intValue)
		{
			string AddNumLine = "";
			if (FCConvert.ToInt32(intValue) != 0)
			{
				AddNumLine = intValue + "\r";
			}
			else
			{
				AddNumLine = "\r";
			}
			return AddNumLine;
		}

		private string AddString(string strValue, int maxChars = 0)
		{
			string AddString = "";
			if (fecherFoundation.Strings.Trim(strValue) != "")
			{
				if (maxChars == 0 || strValue.Length <= maxChars)
				{
					AddString = strValue + "\r";
				}
				else
				{
					AddString = Strings.Left(strValue, maxChars);
				}
			}
			else
			{
				AddString = "\r";
			}
			return AddString;
		}

		private string AddDouble(double dblValue)
		{
			string AddDouble = "";
			if (dblValue != 0)
			{
				AddDouble = Strings.Format(dblValue, "0.00") + "\r";
			}
			else
			{
				AddDouble = "\r";
			}
			return AddDouble;
		}

		public cUC1Schedule2BarCodeInfo() : base()
		{
			Clear();
			strFormVersionNumber = "01";
			strStateCode = "ME";
			strVendorCode = FCConvert.ToInt32("1442");
			strFormCode = "MEUC1SCH2";
			strMaineVendorCode = FCConvert.ToInt32("15");
		}

		public void Clear()
		{
			int x;
			for (x = 0; x <= 18; x++)
			{
				strPayeeSSN[x] = "";
				dblUCGrossWages[x] = 0;
				boolIsSeasonal[x] = false;
				dblUCGrossPageTotal = 0;
			}
			// x
		}

		public string Get2DBarCodeString()
		{
			string Get2DBarCodeString = "";
			string strReturn;
			string strTemp;
			int x;
			double dblTotal;
			strReturn = "T1" + "\r";
			strReturn += FCConvert.ToString(strVendorCode) + "\r";
			strReturn += strSoftwareVersion + "\r";
			strReturn += FCConvert.ToString(strMaineVendorCode) + "\r";
			strReturn += strStateCode + "\r";
			strReturn += strYear + "\r";
			strReturn += strFormCode + "\r";
			strReturn += strFormVersionNumber + "\r";
			strTemp = Strings.Right("00" + FCConvert.ToString(dtStart.Month), 2) + Strings.Right("00" + FCConvert.ToString(dtStart.Day), 2) + FCConvert.ToString(dtStart.Year);
			strReturn += strTemp + "\r";
			strTemp = Strings.Right("00" + FCConvert.ToString(dtEnd.Month), 2) + Strings.Right("00" + FCConvert.ToString(dtEnd.Day), 2) + FCConvert.ToString(dtEnd.Year);
			strReturn += strTemp + "\r";
			strReturn += strFedId.Replace(" ", "").Replace("-", "") + "\r";
			dblTotal = 0;
			for (x = 0; x <= Max_UC1_Detail_Lines - 1; x++)
			{
				if (strPayeeSSN[x] != "")
				{
					dblTotal += dblUCGrossWages[x];
					strReturn += AddString(strPayeeSSN[x]);
					strReturn += AddDouble(dblUCGrossWages[x]);
					if (boolIsSeasonal[x])
					{
						strReturn += AddString("1");
					}
					else
					{
						strReturn += AddString("0");
					}
				}
				else
				{
					strReturn += "\r";
					strReturn += "\r";
					strReturn += "\r";
				}
			}
			// x
			dblUCGrossPageTotal = dblTotal;
			strReturn += AddDouble(dblUCGrossPageTotal);
			strReturn += "*EOD*";
			Get2DBarCodeString = strReturn;
			return Get2DBarCodeString;
		}
	}
}
