//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsShiftType
	{
		//=========================================================
		private string strShiftName = string.Empty;
		private int lngShiftID;
		private int lngShiftType;
		private int lngOvertimePayCat;
		private FCCollection BreakDownList = new FCCollection();
		private bool boolUnused;
		private int intCurrentIndex;
		private string strAccount = string.Empty;
		private double dblLunchTime;

		public string Account
		{
			set
			{
				strAccount = fecherFoundation.Strings.Trim(Strings.Replace(value, "_", "X", 1, -1, CompareConstants.vbTextCompare));
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double LunchTime
		{
			set
			{
				dblLunchTime = value;
			}
			get
			{
				double LunchTime = 0;
				LunchTime = dblLunchTime;
				return LunchTime;
			}
		}

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public string ShiftName
		{
			set
			{
				strShiftName = value;
			}
			get
			{
				string ShiftName = "";
				ShiftName = strShiftName;
				return ShiftName;
			}
		}

		public int ShiftID
		{
			set
			{
				lngShiftID = value;
			}
			get
			{
				int ShiftID = 0;
				ShiftID = lngShiftID;
				return ShiftID;
			}
		}

		public int ShiftType
		{
			set
			{
				lngShiftType = value;
			}
			get
			{
				int ShiftType = 0;
				ShiftType = lngShiftType;
				return ShiftType;
			}
		}

		public int OvertimePayCatID
		{
			set
			{
				lngOvertimePayCat = value;
			}
			get
			{
				int OvertimePayCatID = 0;
				OvertimePayCatID = lngOvertimePayCat;
				return OvertimePayCatID;
			}
		}

		public int AddLine(int lngPayCategory, double dblFactor, modSchedule.ShiftFactorType intFactorType, double dblAfterUnits, double dblUpToUnits, double dblMaxUnits, modSchedule.ShiftPerType intMaxType, int lngID = 0)
		{
			int AddLine = 0;
			clsShiftPayLines tLine = new clsShiftPayLines();
			tLine.AfterUnits = dblAfterUnits;
			tLine.BreakdownID = lngID;
			tLine.FactorOrUnits = intFactorType;
			tLine.FactorUnits = dblFactor;
			tLine.MaxType = intMaxType;
			tLine.MaxUnits = dblMaxUnits;
			tLine.PayCategory = lngPayCategory;
			tLine.UpToUnits = dblUpToUnits;
			tLine.ShiftID = lngShiftID;
			tLine.Unused = false;
			AddLine = InsertLine(ref tLine);
			return AddLine;
		}

		public int InsertLine(ref clsShiftPayLines tLine)
		{
			int InsertLine = 0;
			int intReturn;
			tLine.ShiftID = lngShiftID;
			BreakDownList.Add(tLine);
			intCurrentIndex = BreakDownList.Count;
			intReturn = BreakDownList.Count;
			InsertLine = intReturn;
			return InsertLine;
		}

		public void LoadType(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				ClearBreakdownlist();
				strSQL = "select * from shifttypes where id = " + FCConvert.ToString(lngID);
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					lngShiftID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("ID"))));
					lngShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ShiftType"))));
					dblLunchTime = Conversion.Val(rsLoad.Get_Fields("lunchtime"));
					strAccount = FCConvert.ToString(rsLoad.Get_Fields("account"));
					lngOvertimePayCat = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("OverTimePayCatID"))));
					strShiftName = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					strSQL = "select id from shiftbreakdowns where shiftid = " + FCConvert.ToString(lngShiftID);
					rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
					clsShiftPayLines tLine;
					int intTemp;
					while (!rsLoad.EndOfFile())
					{
						tLine = new clsShiftPayLines();
						tLine.LoadPayLine(FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("id"))));
						InsertLine(ref tLine);
						rsLoad.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveType()
		{
			bool SaveType = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolReturn;
				boolReturn = true;
				SaveType = false;
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				// Dim strAccount As String
				if (!boolUnused)
				{
					strSQL = "Select * from ShiftTypes where shiftid = " + FCConvert.ToString(lngShiftID);
					rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("ShiftType", lngShiftType);
					rsSave.Set_Fields("OvertimePayCatID", lngOvertimePayCat);
					rsSave.Set_Fields("Name", strShiftName);
					rsSave.Set_Fields("LunchTime", dblLunchTime);
					strAccount = Strings.Replace(strAccount, "_", "X", 1, -1, CompareConstants.vbTextCompare);
					rsSave.Set_Fields("account", strAccount);
					rsSave.Update();
					lngShiftID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ShiftID"));
					int X;
					foreach (clsShiftPayLines tLine in BreakDownList)
					{
						tLine.ShiftID = lngShiftID;
						boolReturn = boolReturn && tLine.Save();
					}
					// tLine
					clsShiftPayLines tLine1;
					for (X = BreakDownList.Count; X >= 1; X--)
					{
						tLine1 = BreakDownList[X];
						if (tLine1.Unused)
						{
							BreakDownList.Remove(X);
							tLine1 = null;
						}
					}
					// X
				}
				else
				{
					DeleteType();
				}
				SaveType = boolReturn;
				return SaveType;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveType;
		}

		public void DeleteType()
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				fecherFoundation.Information.Err().Clear();
				if (!(BreakDownList == null))
				{
					boolUnused = true;
					foreach (clsShiftPayLines tLine in BreakDownList)
					{
						tLine.Delete();
						BreakDownList.Remove(1);
					}
					// tLine
					// make anything that used to be this type a regular type
					int lngID = 0;
					int lngHID = 0;
					strSQL = "select * from shifttypes where shifttype = " + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ScheduleShiftType.Regular));
					rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						lngID = FCConvert.ToInt32(rsSave.Get_Fields("shiftid"));
					}
					strSQL = "select * from shifttypes where shifttype = " + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ScheduleShiftType.Holiday));
					rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						lngHID = FCConvert.ToInt32(rsSave.Get_Fields("shiftid"));
					}
					strSQL = "update Employeeshifts set shifttypeid = " + FCConvert.ToString(lngID) + " where shifttypeid = " + FCConvert.ToString(lngShiftID);
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update Defaultshifts set shifttypeid = " + FCConvert.ToString(lngID) + " where shifttypeid = " + FCConvert.ToString(lngShiftID);
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update Holidays set shift = " + FCConvert.ToString(lngHID) + " where shift = " + FCConvert.ToString(lngShiftID);
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "delete from shifttypes where shiftid = " + FCConvert.ToString(lngShiftID);
					rsSave.Execute(strSQL, "twpy0000.vb1");
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearBreakdownlist()
		{
			if (!(BreakDownList == null))
			{
				foreach (clsShiftPayLines tLine in BreakDownList)
				{
					BreakDownList.Remove(1);
				}
				// tLine
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				if (intCurrentIndex > BreakDownList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= BreakDownList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > BreakDownList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (BreakDownList[intCurrentIndex] == null)
						{
						}
						else if (BreakDownList[intCurrentIndex].Unused)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public void MoveFirst()
		{
			if (!(BreakDownList == null))
			{
				if (!FCUtils.IsEmpty(BreakDownList))
				{
					if (BreakDownList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsShiftPayLines GetCurrentBreakdown()
		{
			clsShiftPayLines GetCurrentBreakdown = null;
			clsShiftPayLines tBreakdown;
			tBreakdown = null;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				if (intCurrentIndex > 0)
				{
					if (!(BreakDownList[intCurrentIndex] == null))
					{
						if (!BreakDownList[intCurrentIndex].Unused)
						{
							tBreakdown = BreakDownList[intCurrentIndex];
						}
					}
				}
			}
			GetCurrentBreakdown = tBreakdown;
			return GetCurrentBreakdown;
		}
		// vbPorter upgrade warning: intindex As int	OnWrite(double, int)
		public clsShiftPayLines GetBreakdownByIndex(int intindex)
		{
			clsShiftPayLines GetBreakdownByIndex = null;
			clsShiftPayLines tBreakdown;
			tBreakdown = null;
			if (!FCUtils.IsEmpty(BreakDownList) && intindex > 0)
			{
				if (!(BreakDownList[intindex] == null))
				{
					if (!BreakDownList[intindex].Unused)
					{
						intCurrentIndex = intindex;
						tBreakdown = BreakDownList[intindex];
					}
				}
			}
			GetBreakdownByIndex = tBreakdown;
			return GetBreakdownByIndex;
		}
	}
}
