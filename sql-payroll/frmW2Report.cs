//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.IO.Compression;

namespace TWPY0000
{
	public partial class frmW2Report : BaseForm
	{
		public frmW2Report()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2Report InstancePtr
		{
			get
			{
				return (frmW2Report)Sys.GetInstance(typeof(frmW2Report));
			}
		}

		protected frmW2Report _InstancePtr = null;
		//=========================================================
		FCFileSystem fso = new FCFileSystem();
        StreamWriter ts;
		clsDRWrapper clsW2 = new clsDRWrapper();
		int lngTotFedWages;
		int lngTotFedTax;
		int lngTotHealthSavingsAccount;
		int lngTotFicaWages;
		int lngTotFicaTax;
		int lngTotMedicareWages;
		int lngTotMidecareTax;
		int lngTotDependentCare;
		int lngTot401k;
		int lngTot403b;
		int lngTot408k;
		int lngTot457b;
		int lngTot501c;
		int lngTotLife;
		int lngTotY;
		int lngTotAA;
		int lngTotBB;
		int lngTotDD;
		int lngTotFF;
		bool boolFileCreated;
		bool boolSeparateMQGE;
		int lngYearToUse;
		//private void AbaleZip1_InsertDisk(object sender, AxAbaleZipLibrary._IAbaleZipEvents_InsertDiskEvent e)
		//{
		//	if (MessageBox.Show("Insert disk "+FCConvert.ToString(e.lDiskNumber)+" and press OK to continue", "Insert Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)==DialogResult.OK) {
		//		e.bDiskInserted = true;
		//	} else {
		//		e.bDiskInserted = false;
		//	}
		//}
		private void frmW2Report_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmW2Report_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2Report properties;
			//frmW2Report.ScaleWidth	= 3885;
			//frmW2Report.ScaleHeight	= 2235;
			//frmW2Report.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			boolFileCreated = false;
		}

		private bool DestIsReady()
		{
			bool DestIsReady = false;
			DriveInfo dr;
			string strTemp;
			FileInfo fl;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				DestIsReady = false;
				dr = fso.GetDrive("A");
				while (!dr.IsReady)
				{
					if (MessageBox.Show("Drive A: is not ready.  Please insert a blank disk and click OK when ready.", "Disk Not Ready", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
					{
						Close();
						//App.DoEvents();
						return DestIsReady;
					}
				}
				strTemp = FCFileSystem.Dir("a:", 0);
				if (strTemp != string.Empty)
				{
					if (MessageBox.Show("The disk is not empty.  Would you like to erase it now?", "Disk Not Empty", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						while (strTemp != string.Empty)
						{
							FCFileSystem.DeleteFile("a:\\" + strTemp);
							strTemp = FCFileSystem.Dir();
						}
					}
					else
					{
						Close();
						//App.DoEvents();
						return DestIsReady;
					}
				}
				DestIsReady = true;
				return DestIsReady;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DestIsReady", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DestIsReady;
		}

		private void mnuCopy_Click(object sender, System.EventArgs e)
		{
			DriveInfo dr;
			string strTemp = "";
			FileInfo fl;
			int lngTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				dr = fso.GetDrive("A");
				while (!dr.IsReady)
				{
					if (MessageBox.Show("Drive A: is not ready.  Please insert a blank formatted disk and click OK when ready.", "Disk Not Ready", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
					{
						return;
					}
				}
				// strTemp = Dir("a:")
				// If strTemp <> vbNullString Then
				// If MsgBox("The disk is not empty.  Would you like erase it now?", vbYesNo + vbQuestion, "Disk Not Empty") = vbYes Then
				// Do While strTemp <> vbNullString
				// fso.DeleteFile ("a:\" & strTemp)
				// strTemp = Dir
				// Loop
				// Else
				// Exit Sub
				// End If
				// End If
				// disk is in drive and empty
				// now see if it needs to be zipped
				//lngTemp = dr.FreeSpace;
				fl = new FileInfo("W2Report");
				if (fl.Length> lngTemp)
				{
					// do a spanned zip
					//FC:TODO: Replace abalezip with .NET Classes
					//AbaleZip1.AddFilesToProcess("W2Report");
					//AbaleZip1.ZipFilename = "a:\\W2Report";
					//AbaleZip1.Zip();
				}
				else
				{
					// can fit the way it is
					File.Copy("W2Report", "a:\\W2Report", true);
				}
				MessageBox.Show("Copy Complete", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuCopy_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void Init()
		{
            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
            //this.Show(FCForm.FormShowEnum.Modeless);
            this.Show(App.MainForm);
            //App.DoEvents();
            if (CreateW2File())
			{
				PrintW2Report();
				frmW3ME.InstancePtr.Init(lngYearToUse, true);
			}
		}

		private void PrintW2Report()
		{
			if (boolFileCreated)
			{
                //FC:FINAL:BSE:#4166 form should be modal
				rptW2Electronic.InstancePtr.Init(true);
				Close();
			}
			// Unload Me
		}

		private bool CreateW2File()
		{
			bool CreateW2File = false;
			bool boolStarted = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine = "";
				int lngNumRecords;
				int lngTotalRecsToProcess = 0;
				DriveInfo dr;
				string strTemp = "";
				FileInfo fl;
				int lngTemp;
				string strgroup = "";
				bool boolNewGroup;
				int lngTotNumRecords = 0;
				// mnuFile.Enabled = False
				boolStarted = false;
				CreateW2File = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmW2Report.InstancePtr.Enabled = false;
				if (FCFileSystem.FileExists("W2Report"))
				{
					FCFileSystem.DeleteFile("W2Report");
				}
                //ts = fso.CreateTextFile("W2Report", true, false);
                ts = FCFileSystem.CreateTextFile("W2Report");
                boolStarted = true;
				lngNumRecords = 0;
				lngTotFedWages = 0;
				lngTotFedTax = 0;
				lngTotFicaWages = 0;
				lngTotFicaTax = 0;
				lngTotMedicareWages = 0;
				lngTotMidecareTax = 0;
				lngTotDependentCare = 0;
				lngTot401k = 0;
				lngTot403b = 0;
				lngTot408k = 0;
				lngTot457b = 0;
				lngTot501c = 0;
				lngTotLife = 0;
				clsW2.OpenRecordset("select max(year) as TaxYear from tblw2edittable", "twpy0000.vb1");
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsW2.Get_Fields("taxyear"))));
				if (lngTemp == 0)
				{
                    //FC:FINAl:SBE - close stream to avoid exception when the same process is executed again
                    ts.Close();
					MessageBox.Show("There is no information in the W-2 Edit Table", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					lngYearToUse = lngTemp;
					boolNewGroup = false;
					lngTotNumRecords = 0;
					clsW2.Execute("Update tblW2EditTable Set Year = " + FCConvert.ToString(lngTemp), "TWPY0000.vb1");
					boolSeparateMQGE = true;
					if (!boolSeparateMQGE)
					{
						clsW2.OpenRecordset("select * from tblW2EditTable where [year] = " + FCConvert.ToString(lngTemp), "twpy0000.vb1");
						strgroup = "";
					}
					else
					{
						clsW2.OpenRecordset("select * from tblw2edittable where [year] = " + FCConvert.ToString(lngTemp) + " order by mqge,FICAWAGE,FICATAX", "twpy0000.vb1");
						if (!clsW2.EndOfFile())
						{
							if (clsW2.Get_Fields_Boolean("mqge") && Conversion.Val(clsW2.Get_Fields("ficawage")) == 0 && Conversion.Val(clsW2.Get_Fields("ficatax")) == 0)
							{
								strgroup = "MQGE";
							}
							else
							{
								strgroup = "";
							}
						}
					}
					lngTotalRecsToProcess = clsW2.RecordCount() + 4;
					ProgressBar1.Maximum = lngTotalRecsToProcess;
					ProgressBar1.Value = 0;
					strLine = CreateRARecord();
					if (strLine == string.Empty)
					{
						ts.Close();
						MessageBox.Show("Electronic record could not be created", "Error Creating File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						Close();
						return CreateW2File;
					}
					ts.WriteLine(strLine);
					ProgressBar1.Value = ProgressBar1.Value + 1;
					lblPercDone.Text = FCConvert.ToString(FCConvert.ToInt16((FCConvert.ToDouble(ProgressBar1.Value) / lngTotalRecsToProcess) * 100)) + "% Done";
					//App.DoEvents();
					strLine = fecherFoundation.Strings.UCase(CreateRERecord(ref strgroup));
					ts.WriteLine(strLine);
					ProgressBar1.Value = ProgressBar1.Value + 1;
					lblPercDone.Text = FCConvert.ToString(FCConvert.ToInt16((FCConvert.ToDouble(ProgressBar1.Value) / lngTotalRecsToProcess) * 100)) + "% Done";
					//App.DoEvents();
					// now do all employee records
					while (!clsW2.EndOfFile())
					{
						if (!(clsW2.Get_Fields_Boolean("mqge") && Conversion.Val(clsW2.Get_Fields("ficawage")) == 0 && Conversion.Val(clsW2.Get_Fields("ficatax")) == 0))
						{
							if (strgroup == "MQGE")
							{
								strLine = fecherFoundation.Strings.UCase(CreateRTRecord(ref lngNumRecords));
								lngNumRecords = 0;
								ts.WriteLine(strLine);
								strgroup = "";
								strLine = fecherFoundation.Strings.UCase(CreateRERecord(ref strgroup));
								ts.WriteLine(strLine);
							}
						}
						else
						{
							if (strgroup == "")
							{
								strLine = fecherFoundation.Strings.UCase(CreateRTRecord(ref lngNumRecords));
								lngNumRecords = 0;
								ts.WriteLine(strLine);
								strgroup = "MQGE";
								strLine = fecherFoundation.Strings.UCase(CreateRERecord(ref strgroup));
								ts.WriteLine(strLine);
							}
						}
						strLine = fecherFoundation.Strings.UCase(CreateRWRecord(strgroup));
						if (strLine == string.Empty)
						{
							ts.Close();
							MessageBox.Show("Electronic record could not be created for employee " + clsW2.Get_Fields("employeenumber") + ". Fix errors and recreate file.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							Close();
							return CreateW2File;
						}
						ts.WriteLine(strLine);
						strLine = fecherFoundation.Strings.UCase(CreateRSRecord());
						if (strLine == string.Empty)
						{
							ts.Close();
							MessageBox.Show("Electronic record could not be created for employee " + clsW2.Get_Fields("employeenumber") + ". Fix errors and recreate file.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							Close();
							return CreateW2File;
						}
						ts.WriteLine(strLine);
						lngNumRecords += 1;
						lngTotNumRecords += 1;
						ProgressBar1.Value = ProgressBar1.Value + 1;
						lblPercDone.Text = FCConvert.ToString(FCConvert.ToInt16((FCConvert.ToDouble(ProgressBar1.Value) / lngTotalRecsToProcess) * 100)) + "% Done";
						//App.DoEvents();
						clsW2.MoveNext();
					}
					// rsrecord is quarterly
					// strLine = UCase(CreateRSRecord)
					// ts.WriteLine (strLine)
					strLine = fecherFoundation.Strings.UCase(CreateRTRecord(ref lngNumRecords));
					ts.WriteLine(strLine);
					ProgressBar1.Value = ProgressBar1.Value + 1;
					lblPercDone.Text = FCConvert.ToString(FCConvert.ToInt16((FCConvert.ToDouble(ProgressBar1.Value) / lngTotalRecsToProcess) * 100)) + "% Done";
					//App.DoEvents();
					strLine = fecherFoundation.Strings.UCase(CreateRFRecord(ref lngTotNumRecords));
					ts.WriteLine(strLine);
					ProgressBar1.Value = ProgressBar1.Value + 1;
					lblPercDone.Text = FCConvert.ToString(FCConvert.ToInt16((FCConvert.ToDouble(ProgressBar1.Value) / lngTotalRecsToProcess) * 100)) + "% Done";
					//App.DoEvents();
					ts.Close();
					boolStarted = false;
					boolFileCreated = true;
					// disk is in drive and empty
					// now see if it needs to be zipped
					// If Not DestIsReady Then
					// Exit Sub
					// End If
					// Set dr = fso.GetDrive("A")
					// lngTemp = dr.FreeSpace
					// 
					// Set fl = fso.GetFile("W2Report")
					// If fl.Size > lngTemp Then
					// do a spanned zip
					// AbaleZip1.AddFilesToProcess ("W2Report")
					// AbaleZip1.ZipFilename = "a:\W2Report"
					// If AbaleZip1.Zip = aerSuccess Then
					// MsgBox "File Saved Successfully", vbInformation, "Done"
					// Screen.MousePointer = vbDefault
					// boolFileCreated = True
					// Exit Sub
					// Else
					// MsgBox "Error while trying to zip file.", vbCritical, "Error"
					// boolFileCreated = False
					// End If
					// Else
					// can fit the way it is
					// Call fso.CopyFile("W2Report", "a:\W2Report", True)
					// MsgBox "File Saved Successfully", vbInformation, "Done"
					// Screen.MousePointer = vbDefault
					// boolFileCreated = True
					// Exit Sub
					// End If
					frmW2Report.InstancePtr.Enabled = true;
                    //mnuCopy.Enabled = true;
                    //FC:FINAL:BSE:#4166 remove button
                    //cmdCopy.Enabled = true;
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("W2Report created successfully.", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //FC:FINAL:BSE:#4166 process changed to prompt the user to download file 
                    FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "W2Report"), "W2Report.txt");
                    CreateW2File = true;
				}
				frmW2Report.InstancePtr.Enabled = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return CreateW2File;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// mnuFile.Enabled = True
				frmW2Report.InstancePtr.Enabled = true;
				if (boolStarted)
					ts.Close();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateW2File", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateW2File;
		}

		private string CreateRARecord()
		{
			string CreateRARecord = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strOut;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strOut = "";
				clsLoad.OpenRecordset("select * from tblw2master", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (FCConvert.ToInt32(clsLoad.Get_Fields("contactpreference1")) != 2)
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("contactemiladdress"))) == string.Empty)
						{
							MessageBox.Show("You have chosen E-mail as your contact preference but there is no valid E-mail address", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CreateRARecord;
						}
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("deliveryaddress"))) == string.Empty || fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("city"))) == string.Empty || fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("state"))) == string.Empty || fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("zip"))) == string.Empty || fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("locationaddress"))) == string.Empty)
						{
							MessageBox.Show("You have chosen postal service as your contact preference but there is no valid address", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CreateRARecord;
						}
					}
					strOut = "RA";
					// record identifier
					strTemp = FCConvert.ToString(clsLoad.Get_Fields("employersfederalid"));
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strOut += Strings.Mid(strTemp + Strings.StrDup(9, "0"), 1, 9);
					// pin number
					strOut += Strings.Mid(clsLoad.Get_Fields("employeepin") + Strings.StrDup(8, "0"), 1, 8);
					strOut += "1422";
					// Harris software vendor code
					strOut += Strings.StrDup(5, " ");
					strOut += "0";
					// resub indicator
					strOut += Strings.StrDup(6, " ");
					strOut += "99";
					// software code - off the shelf
					strTemp = FCConvert.ToString(clsLoad.Get_Fields("employersname"));
					strTemp = Strings.Mid(strTemp + Strings.StrDup(57, " "), 1, 57);
					// company name 57 chars
					strOut += strTemp;
					// location address
					strOut += Strings.Mid(clsLoad.Get_Fields_String("LocationAddress") + Strings.StrDup(22, " "), 1, 22);
					// delivery address
					strOut += Strings.Mid(clsLoad.Get_Fields_String("DeliveryAddress") + Strings.StrDup(22, " "), 1, 22);
					// city
					strTemp = Strings.Mid(clsLoad.Get_Fields("city") + Strings.StrDup(22, " "), 1, 22);
					strOut += strTemp;
					strOut += Strings.Mid(clsLoad.Get_Fields("state") + "  ", 1, 2);
					strOut += Strings.Mid(clsLoad.Get_Fields_String("zip") + "     ", 1, 5);
					strOut += "    ";
					// zip extension
					strOut += "     ";
					// 5 blanks
					strOut += Strings.StrDup(23, " ");
					// foreign state
					strOut += Strings.StrDup(15, " ");
					// foreign zip
					strOut += "  ";
					// country code
					strOut += Strings.Mid(clsLoad.Get_Fields("employersname") + Strings.StrDup(57, " "), 1, 57);
					// location address
					strOut += Strings.Mid(clsLoad.Get_Fields("locationaddress") + Strings.StrDup(22, " "), 1, 22);
					// delivery address
					strOut += Strings.Mid(clsLoad.Get_Fields("deliveryaddress") + Strings.StrDup(22, " "), 1, 22);
					// city
					strOut += Strings.Mid(clsLoad.Get_Fields("city") + Strings.StrDup(22, " "), 1, 22);
					strOut += Strings.Mid(clsLoad.Get_Fields("state") + "  ", 1, 2);
					strOut += Strings.Mid(clsLoad.Get_Fields_String("zip") + "     ", 1, 5);
					strOut += "    ";
					// zip extension
					strOut += Strings.StrDup(5, " ");
					strOut += Strings.StrDup(23, " ");
					// foreign state
					strOut += Strings.StrDup(15, " ");
					// foreign postal code
					strOut += Strings.StrDup(2, " ");
					// country code
					strTemp = FCConvert.ToString(clsLoad.Get_Fields("contactperson"));
					strTemp = Strings.Replace(strTemp, ",", " ", 1, -1, CompareConstants.vbTextCompare);
					strOut += fecherFoundation.Strings.UCase(Strings.Mid(strTemp + Strings.StrDup(27, " "), 1, 27));
					// contact name
					strOut += Strings.Mid(clsLoad.Get_Fields("contacttelephonenumber") + Strings.StrDup(15, " "), 1, 15);
					strOut += Strings.Mid(clsLoad.Get_Fields("contacttelephoneext") + Strings.StrDup(5, " "), 1, 5);
					strOut += Strings.StrDup(3, " ");
					// blanks
					strOut = fecherFoundation.Strings.UCase(strOut);
					strOut += Strings.Mid(clsLoad.Get_Fields("contactemiladdress") + Strings.StrDup(40, " "), 1, 40);
					strOut += Strings.StrDup(3, " ");
					// blanks
					strOut += Strings.Mid(clsLoad.Get_Fields("contactfaxnumber") + Strings.StrDup(10, " "), 1, 10);
					strOut += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("contactpreference1")));
					// preferred method of notification
					strOut += "L";
					// perparer code - self prepared
					strOut += Strings.StrDup(12, " ");
				}
				// everything but email address should be all caps
				CreateRARecord = strOut;
				return CreateRARecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRARecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRARecord;
		}

		private string CreateRSRecord()
		{
			string CreateRSRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strOut;
				string strTemp;
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
				int lngTemp;
				strOut = "";
				strOut = "RS";
				// record identifier
				strOut += "23";
				// state code is 23
				strOut += Strings.StrDup(5, " ");
				// blanks
				strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(clsW2.Get_Fields("ssn")));
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strOut += Strings.Mid(strTemp + Strings.StrDup(9, "0"), 1, 9);
				strOut += Strings.Mid(clsW2.Get_Fields("firstNAME") + Strings.StrDup(15, " "), 1, 15);
				strOut += Strings.Mid(clsW2.Get_Fields("middlename") + Strings.StrDup(15, " "), 1, 15);
				// middle name
				strOut += Strings.Mid(FCConvert.ToString(clsW2.Get_Fields("lastname")).Replace(",", "") + Strings.StrDup(20, " "), 1, 20);
				strOut += Strings.Mid(clsW2.Get_Fields("desig") + Strings.StrDup(4, " "), 1, 4);
				strOut += Strings.Mid(clsW2.Get_Fields("address") + Strings.StrDup(22, " "), 1, 22);
				// location address
				strOut += Strings.StrDup(22, " ");
				// delivery address
				strOut += Strings.Mid(clsW2.Get_Fields("city") + Strings.StrDup(22, " "), 1, 22);
				// city
				strOut += Strings.Mid(clsW2.Get_Fields("STATE") + "  ", 1, 2);
				// state
				strOut += Strings.Mid(clsW2.Get_Fields_String("zip") + Strings.StrDup(5, " "), 1, 5);
				// zip
				// strOut = strOut & String(4, " ") 'zip extension
				strOut += Strings.StrDup(47, " ");
				strOut += Strings.StrDup(2, " ");
				// country code
				strOut += Strings.StrDup(53, " ");
				if (Conversion.Val(clsW2.Get_Fields("statetax")) > 0)
				{
					clsTemp.OpenRecordset("select * from tblemployerinfo", "twpy0000.vb1");
					strTemp = Strings.StrDup(11, "0");
					if (!clsTemp.EndOfFile())
					{
						strTemp = Strings.Replace(FCConvert.ToString(clsTemp.Get_Fields("mrswithholdingid")), "-", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Left(strTemp + Strings.StrDup(11, "0"), 11);
					}
					strOut += strTemp;
				}
				else
				{
					strOut += Strings.StrDup(11, " ");
					// state employer account number
				}
				strOut += Strings.StrDup(15, " ");
				// blank
				strOut += "23";
				// state Code
				lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields_Double("StateWage")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// state taxable wages
				lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("statetax")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// state income tax withheld
				strOut += Strings.StrDup(10, "0");
				// MPERS
				strOut += Strings.StrDup(205, " ");
				CreateRSRecord = strOut;
				return CreateRSRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRSRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRSRecord;
		}

		private string CreateRERecord(ref string strgroup)
		{
			string CreateRERecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strOut;
				// vbPorter upgrade warning: strTemp As string	OnWrite(int, string)
				string strTemp = "";
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				clsDRWrapper rsW3 = new clsDRWrapper();
				rsW3.OpenRecordset("select * from tblw3information", "twpy0000.vb1");
				strOut = "";
				clsLoad.OpenRecordset("select * from tblw2master", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					strOut = "RE";
					// record identifier
					strTemp = FCConvert.ToString(DateTime.Today.Year - 1);
					if (DateTime.Today.Month > 6)
						strTemp = FCConvert.ToString(DateTime.Today.Year);
					clsTemp.OpenRecordset("select top 1 * from tblW2EDITTABLE order by [year]", "twpy0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						if (Conversion.Val(clsTemp.Get_Fields("year")) > 0)
						{
							strTemp = Strings.Format(Strings.Mid(clsTemp.Get_Fields("year") + Strings.StrDup(4, " "), 1, 4), "0000");
						}
					}
					strOut += strTemp;
					// tax year
					strOut += " ";
					// agent indicator code
					strTemp = FCConvert.ToString(clsLoad.Get_Fields("employersfederalid"));
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strOut += Strings.Mid(strTemp + Strings.StrDup(9, "0"), 1, 9);
					strOut += Strings.StrDup(9, " ");
					strOut += "0";
					// terminating business indicator
					strOut += Strings.StrDup(4, " ");
					// establishment number
					strOut += Strings.StrDup(9, " ");
					// other ein number
					strOut += Strings.Mid(clsLoad.Get_Fields("employersname") + Strings.StrDup(57, " "), 1, 57);
					strOut += Strings.Mid(clsLoad.Get_Fields("locationaddress") + Strings.StrDup(22, " "), 1, 22);
					strOut += Strings.Mid(clsLoad.Get_Fields("deliveryaddress") + Strings.StrDup(22, " "), 1, 22);
					strOut += Strings.Mid(clsLoad.Get_Fields("city") + Strings.StrDup(22, " "), 1, 22);
					strOut += Strings.Mid(clsLoad.Get_Fields("state") + Strings.StrDup(2, " "), 1, 2);
					strOut += Strings.Mid(clsLoad.Get_Fields_String("zip") + Strings.StrDup(5, " "), 1, 5);
					strOut += Strings.StrDup(4, " ");
					// zip ext
					strOut += Strings.Left(rsW3.Get_Fields_String("KindOfEmployer") + "S", 1);
					// kind of employer
					strOut += Strings.StrDup(4, " ");
					// blank
					strOut += Strings.StrDup(23, " ");
					// foreign state
					strOut += Strings.StrDup(15, " ");
					// foreign postal code
					strOut += Strings.StrDup(2, " ");
					// country code
					if (fecherFoundation.Strings.UCase(strgroup) == "MQGE")
					{
						strOut += "Q";
					}
					else
					{
						strOut += "R";
						// employement code
					}
					strOut += " ";
					strOut += "0";
					strOut += Strings.StrDup(291, " ");
				}
				CreateRERecord = strOut;
				return CreateRERecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRERecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRERecord;
		}

		private string CreateRWRecord(string strgroup = "R")
		{
			string CreateRWRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strOut;
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper rsBox10 = new clsDRWrapper();
				clsDRWrapper rsSickPay = new clsDRWrapper();
				// vbPorter upgrade warning: lngTemp As int	OnWrite(double, int)
				int lngTemp = 0;
				int lng401k = 0;
				// 401k
				int lng403b = 0;
				// 403b
				int lng408k = 0;
				// 408k
				int lng457b = 0;
				// 457b
				int lng501c = 0;
				// 501c
				int lngY = 0;
				int lngAA = 0;
				int lngBB = 0;
				int lngDD = 0;
				int lngFF = 0;
				int lngLife = 0;
				string strTemp = "";
				strOut = "";
				strOut = "RW";
				// record identifier
				strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(clsW2.Get_Fields("ssn")));

				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strOut += Strings.Mid(strTemp + Strings.StrDup(9, "0"), 1, 9);
				strOut += Strings.Mid(clsW2.Get_Fields("firstNAME") + Strings.StrDup(15, " "), 1, 15);
				strOut += Strings.Mid(clsW2.Get_Fields("middlename") + Strings.StrDup(15, " "), 1, 15);
				// middle name
				strOut += Strings.Mid(FCConvert.ToString(clsW2.Get_Fields("lastname")).Replace(",", "") + Strings.StrDup(20, " "), 1, 20);
				strOut += Strings.Mid(clsW2.Get_Fields("desig") + Strings.StrDup(4, " "), 1, 4);
				strOut += Strings.Mid(clsW2.Get_Fields("address") + Strings.StrDup(22, " "), 1, 22);
				strOut += Strings.StrDup(22, " ");
				strOut += Strings.Mid(clsW2.Get_Fields("city") + Strings.StrDup(22, " "), 1, 22);
				// strOut = strOut & Mid(GetStateAbbr(Val(clsLoad.Fields("state"))) & "  ", 1, 2)
				strOut += Strings.Mid(clsW2.Get_Fields("state") + "  ", 1, 2);
				strOut += Strings.Mid(clsW2.Get_Fields_String("zip") + Strings.StrDup(5, " "), 1, 5);
				strOut += Strings.StrDup(4, " ");
				// zip extension
				strOut += Strings.StrDup(5, " ");
				// blank
				strOut += Strings.StrDup(23, " ");
				// foreign state
				strOut += Strings.StrDup(15, " ");
				// foreign postal code
				strOut += Strings.StrDup(2, " ");
				// country code
				// now start financial stuff
				lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("federalwage")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// wages,tips
				lngTotFedWages += lngTemp;
				lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("federaltax")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// federal income tax wh
				lngTotFedTax += lngTemp;
				if ((strgroup == "X") || (strgroup == "MQGE") || (strgroup == "Q"))
				{
					lngTemp = 0;
				}
				else
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("ficawage")) * 100);
				}
				strOut += Strings.Format(lngTemp, "00000000000");
				// social sec. wages
				lngTotFicaWages += lngTemp;
				if ((strgroup == "X") || (strgroup == "MQGE") || (strgroup == "Q"))
				{
					lngTemp = 0;
				}
				else
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("ficatax")) * 100);
				}
				strOut += Strings.Format(lngTemp, "00000000000");
				// social sec. tax
				lngTotFicaTax += lngTemp;
				lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("medicarewage")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// medicare wages
				lngTotMedicareWages += lngTemp;
				lngTemp = FCConvert.ToInt32(Conversion.Val(clsW2.Get_Fields("medicaretax")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// medicare tax
				lngTotMidecareTax += lngTemp;
				strOut += Strings.StrDup(11, "0");
				// social security tips
				strOut += "00000000000";
				// eic
				// MATTHEW 05/16/2005
				rsBox10.OpenRecordset("SELECT Sum(CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 and tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber = '" + clsW2.Get_Fields("employeenumber") + "'", "TWPY0000.vb1");
				lngTemp = FCConvert.ToInt32(Conversion.Val(rsBox10.Get_Fields("SumOfCYTDAmount")) * 100);
				strOut += Strings.Format(lngTemp, "00000000000");
				// dependent care benefits
				lngTotDependentCare += lngTemp;
				// get retirement amounts
				lng401k = 0;
				lng403b = 0;
				lng408k = 0;
				lng457b = 0;
				lng501c = 0;
				lngLife = 0;
				lngY = 0;
				lngAA = 0;
				lngBB = 0;
				lngDD = 0;
				lngFF = 0;
				clsLoad.OpenRecordset("select * from tblW2DEDUCTIONS where employeenumber = '" + clsW2.Get_Fields("employeenumber") + "' and Box12 = 1", "twpy0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("cytdamount")) * 100);
					if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "C")
					{
						// life term
						lngLife += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "D")
					{
						// 401k
						lng401k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "E")
					{
						// 403b
						lng403b += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "F")
					{
						// 408
						lng408k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "G")
					{
						// 457
						lng457b += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "H")
					{
						// 501
						lng501c += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "Y")
					{
						lngY += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "AA")
					{
						lngAA += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "BB")
					{
						lngBB += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "DD")
					{
						lngDD += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + "  ", 1, 2)) == "FF")
					{
						lngFF += lngTemp;
					}
					clsLoad.MoveNext();
				}
				clsLoad.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  TBLW2MATCHES.employeenumber = '" + clsW2.Get_Fields("employeenumber") + "' and tblw2box12and14.Box12 = 1 GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code ", "TWPY0000.VB1");
				while (!clsLoad.EndOfFile())
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("sumofcytdamount")) * 100);
					if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "C")
					{
						// life term
						lngLife += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "D")
					{
						// 401k
						lng401k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "E")
					{
						// 403b
						lng403b += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "F")
					{
						// 408
						lng408k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "G")
					{
						// 457
						lng457b += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "H")
					{
						// 501
						lng501c += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "Y")
					{
						lngY += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "AA")
					{
						lngAA += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "BB")
					{
						lngBB += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "DD")
					{
						lngDD += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("code"))) + " ", 1, 2)) == "FF")
					{
						lngFF += lngTemp;
					}
					clsLoad.MoveNext();
				}
				lngTot401k += lng401k;
				lngTot403b += lng403b;
				lngTot408k += lng408k;
				lngTot457b += lng457b;
				lngTot501c += lng501c;
				lngTotLife += lngLife;
				lngTotY += lngY;
				lngTotAA += lngAA;
				lngTotBB += lngBB;
				lngTotDD += lngDD;
				lngTotFF += lngFF;
				strOut += Strings.Format(lng401k, "00000000000");
				// 401k
				strOut += Strings.Format(lng403b, "00000000000");
				// 403b
				strOut += Strings.Format(lng408k, "00000000000");
				// 408k
				strOut += Strings.Format(lng457b, "00000000000");
				// 457b
				strOut += Strings.Format(lng501c, "00000000000");
				// 501c
				// ****************************************************************
				// MATTHEW CALL ID 83574 12/13/2005
				// strOut = strOut & String(11, " ")   'blank
				strOut += Strings.StrDup(11, "0");
				// mILITARY eMPLOYEES bASIC qUARTERS, sUBSISTENCE AND cOMBAT pAY
				// ****************************************************************
				strOut += Strings.StrDup(11, "0");
				// non-qualified plan
				// Employer contributions to a health savings account Code "W"
				rsSickPay.OpenRecordset("select * from tblW2Deductions where employeenumber = '" + clsW2.Get_Fields("employeenumber") + "' AND Code = 'W'", "twpy0000.vb1");
				if (rsSickPay.EndOfFile())
				{
					rsSickPay.OpenRecordset("select * from tblW2Matches where employeenumber = '" + clsW2.Get_Fields("employeenumber") + "' AND Code = 'W'", "twpy0000.vb1");
					if (rsSickPay.EndOfFile())
					{
						strOut += Strings.StrDup(11, "0");
						// Employer contributions to a health savings account Code "W"
					}
					else
					{
						strOut += Strings.Format(rsSickPay.Get_Fields_Double("CYTDAmount"), "00000000000");
						lngTotHealthSavingsAccount += FCConvert.ToInt32(rsSickPay.Get_Fields_Double("CYTDAmount"));
					}
				}
				else
				{
					strOut += Strings.Format(rsSickPay.Get_Fields_Double("CYTDAmount"), "00000000000");
					lngTotHealthSavingsAccount += FCConvert.ToInt32(rsSickPay.Get_Fields_Double("CYTDAmount"));
				}
				strOut += Strings.StrDup(11, "0");
				// non-qualified
				// ****************************************************************
				// MATTHEW CALL ID 83574 12/13/2005
				// strOut = strOut & String(22, " ")
				strOut += Strings.StrDup(22, "0");
				// ****************************************************************
				strOut += Strings.Format(lngLife, "00000000000");
				// employer cost of premiums for group life
				strOut += Strings.StrDup(11, "0");
				// nonstatutory stock options
				// ****************************************************************
				// MATTHEW CALL ID 83574 12/13/2005
				// strOut = strOut & String(56, " ")
				// strOut = strOut & String(56, "0")
				strOut += Strings.Format(lngY, "00000000000");
				// deferrals under 409a non-qualified deferred comp. plan
				strOut += Strings.Format(lngAA, "00000000000");
				// roth contributions to a 401k
				strOut += Strings.Format(lngBB, "00000000000");
				// roth contributions under 403b salary reduction agreement
				strOut += Strings.Format(lngDD, "00000000000");
				// employer sponsored health coverage
				strOut += Strings.Format(lngFF, "00000000000");
				// qualified small employer health reimbursement arrangement
				strOut += Strings.StrDup(1, " ");
				// ****************************************************************
				if (FCConvert.ToBoolean(clsW2.Get_Fields_Boolean("W2StatutoryEmployee")))
				{
					strOut += "1";
				}
				else
				{
					strOut += "0";
					// Statutory Employee Indicator
				}
				strOut += " ";
				// blank
				if (FCConvert.ToBoolean(clsW2.Get_Fields_Boolean("w2retirement")))
				{
					strOut += "1";
				}
				else
				{
					strOut += "0";
					// retirement plan indicator
				}
				rsSickPay.OpenRecordset("select * from tblW2Deductions where employeenumber = '" + clsW2.Get_Fields("employeenumber") + "' AND ThirdPartySave = 1", "twpy0000.vb1");
				if (rsSickPay.EndOfFile())
				{
					rsSickPay.OpenRecordset("select * from tblW2Matches where employeenumber = '" + clsW2.Get_Fields("employeenumber") + "' AND ThirdPartySave = 1", "twpy0000.vb1");
					if (rsSickPay.EndOfFile())
					{
                        // 3rd party sick pay indicator
                        if (clsW2.Get_Fields_Boolean("W23rdPartySickPay"))
                        {
                            strOut = strOut + "1";
                        }
                        else
                        {
                            strOut = strOut + "0";           
                        }
                    }
					else
					{
						strOut += "1";						
					}
				}
				else
				{
					strOut += "1";
					// 3rd party sick pay indicator
				}
				strOut += Strings.StrDup(23, " ");
				// End If
				CreateRWRecord = strOut;
				return CreateRWRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRWRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRWRecord;
		}

		private string CreateRTRecord(ref int lngNumRecords)
		{
			string CreateRTRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strOut;
				strOut = "";
				strOut = "RT";
				// record identifier
				strOut += Strings.Format(lngNumRecords, "0000000");
				strOut += Strings.Format(lngTotFedWages, "000000000000000");
				strOut += Strings.Format(lngTotFedTax, "000000000000000");
				strOut += Strings.Format(lngTotFicaWages, "000000000000000");
				strOut += Strings.Format(lngTotFicaTax, "000000000000000");
				strOut += Strings.Format(lngTotMedicareWages, "000000000000000");
				strOut += Strings.Format(lngTotMidecareTax, "000000000000000");
				strOut += Strings.StrDup(15, "0");
				strOut += "000000000000000";
				// MATTHEW 05/16/2005
				strOut += Strings.Format(lngTotDependentCare, "000000000000000");
				strOut += Strings.Format(lngTot401k, "000000000000000");
				strOut += Strings.Format(lngTot403b, "000000000000000");
				strOut += Strings.Format(lngTot408k, "000000000000000");
				strOut += Strings.Format(lngTot457b, "000000000000000");
				strOut += Strings.Format(lngTot501c, "000000000000000");
				// ****************************************************************
				// MATTHEW CALL ID 83574 12/13/2005
				// strOut = strOut & String(15, " ")   'blank
				strOut += Strings.StrDup(15, "0");
				// ****************************************************************
				strOut += Strings.StrDup(15, "0");
				strOut += Strings.Format(lngTotHealthSavingsAccount, "000000000000000");
				// Health Savings Account Code 'W'
				strOut += Strings.StrDup(15, "0");
				// ****************************************************************
				// MATTHEW CALL ID 83574 12/13/2005
				// strOut = strOut & String(30, " ")
				strOut += Strings.StrDup(15, "0");
				// ****************************************************************
				strOut += Strings.Format(lngTotDD, "000000000000000");
				// cost of employer sponsored health coverage
				strOut += Strings.Format(lngTotLife, "000000000000000");
				strOut += Strings.StrDup(15, "0");
				strOut += Strings.StrDup(15, "0");
				// stock options
				strOut += Strings.StrDup(15, "0");
				// 409A
				strOut += Strings.StrDup(15, "0");
				// 401k roth
				strOut += Strings.StrDup(15, "0");
				// roth 403b
				strOut += Strings.Format(lngTotFF, "000000000000000");
				// ****************************************************************
				// MATTHEW CALL ID 83574 12/13/2005
				// strOut = strOut & String(158, " ")
				strOut += Strings.StrDup(98, " ");
				// ****************************************************************
				CreateRTRecord = strOut;
				// clear out the totals for the next group
				lngTotFedWages = 0;
				lngTotFedTax = 0;
				lngTotFicaWages = 0;
				lngTotFicaTax = 0;
				lngTotMedicareWages = 0;
				lngTotMidecareTax = 0;
				lngTotDependentCare = 0;
				lngTot401k = 0;
				lngTot403b = 0;
				lngTot408k = 0;
				lngTot457b = 0;
				lngTot501c = 0;
				lngTotY = 0;
				lngTotAA = 0;
				lngTotBB = 0;
				lngTotDD = 0;
				lngTotFF = 0;
				lngTotHealthSavingsAccount = 0;
				lngTotLife = 0;
				return CreateRTRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRTRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRTRecord;
		}

		private string CreateRFRecord(ref int lngNumRecords)
		{
			string CreateRFRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strOut;
				strOut = "";
				strOut = "RF";
				// record identifier
				strOut += Strings.StrDup(5, " ");
				strOut += Strings.Format(lngNumRecords, "000000000");
				strOut += Strings.StrDup(496, " ");
				CreateRFRecord = strOut;
				return CreateRFRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRFRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRFRecord;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
