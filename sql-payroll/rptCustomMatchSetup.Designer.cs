﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomMatchSetup.
	/// </summary>
	partial class rptCustomMatchSetup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomMatchSetup));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.t = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLimit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeductionDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLimitType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrequency = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotFYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductionDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimitType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTaxStatus,
				this.txtLimit,
				this.txtDeductionDescription,
				this.txtAmount,
				this.txtAmountType,
				this.txtLimitType,
				this.txtAccount,
				this.txtMTD,
				this.txtQTD,
				this.txtCYTD,
				this.txtFYTD,
				this.txtFrequency,
				this.txtStatus
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotMTD,
				this.txtTotQTD,
				this.txtTotFYTD,
				this.txtTotCYTD,
				this.Line2
			});
			this.ReportFooter.Height = 0.34375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.t,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Line1,
				this.Field12
			});
			this.PageHeader.Height = 0.8541667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployee
			});
			this.GroupHeader1.Height = 0.1770833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0.125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Microsoft Sans Ser" + "if\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL -  MATCH SETUP REPORT";
			this.Label1.Top = 0F;
			this.Label1.Width = 9.6875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" + "ign: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 8.375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" + "ign: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.3125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.2916667F;
			this.Field1.Left = 1.65625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field1.Text = "Tax Status";
			this.Field1.Top = 0.5416667F;
			this.Field1.Width = 0.5F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 2.25F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field2.Text = "Amount";
			this.Field2.Top = 0.6666667F;
			this.Field2.Width = 0.53125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2916667F;
			this.Field3.Left = 2.8125F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field3.Text = "Amt. Type";
			this.Field3.Top = 0.5416667F;
			this.Field3.Width = 0.40625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1666667F;
			this.Field4.Left = 4.21875F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field4.Text = "Limit";
			this.Field4.Top = 0.6666667F;
			this.Field4.Width = 0.46875F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2916667F;
			this.Field5.Left = 4.71875F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field5.Text = "Limit Type";
			this.Field5.Top = 0.5416667F;
			this.Field5.Width = 0.40625F;
			// 
			// t
			// 
			this.t.Height = 0.1666667F;
			this.t.Left = 5.25F;
			this.t.Name = "t";
			this.t.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.t.Text = "Account";
			this.t.Top = 0.6666667F;
			this.t.Width = 0.5625F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1666667F;
			this.Field7.Left = 7.09375F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field7.Text = "MTD";
			this.Field7.Top = 0.6666667F;
			this.Field7.Width = 0.5F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1666667F;
			this.Field8.Left = 7.75F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field8.Text = "QTD";
			this.Field8.Top = 0.6666667F;
			this.Field8.Width = 0.5F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1666667F;
			this.Field9.Left = 8.5F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field9.Text = "CYTD";
			this.Field9.Top = 0.6666667F;
			this.Field9.Width = 0.5F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1666667F;
			this.Field10.Left = 9.21875F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field10.Text = "FYTD";
			this.Field10.Top = 0.6666667F;
			this.Field10.Width = 0.5F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1666667F;
			this.Field11.Left = 3.25F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field11.Text = "Freq.";
			this.Field11.Top = 0.6666667F;
			this.Field11.Width = 0.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.8333333F;
			this.Line1.Width = 8.125F;
			this.Line1.X1 = 1.625F;
			this.Line1.X2 = 9.75F;
			this.Line1.Y1 = 0.8333333F;
			this.Line1.Y2 = 0.8333333F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1666667F;
			this.Field12.Left = 6.28125F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field12.Text = "Status";
			this.Field12.Top = 0.6666667F;
			this.Field12.Width = 0.5625F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.1666667F;
			this.txtEmployee.Left = 0.0625F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtEmployee.Text = "Field1";
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 4.65625F;
			// 
			// txtTaxStatus
			// 
			this.txtTaxStatus.Height = 0.1666667F;
			this.txtTaxStatus.Left = 1.71875F;
			this.txtTaxStatus.Name = "txtTaxStatus";
			this.txtTaxStatus.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtTaxStatus.Text = "Field1";
			this.txtTaxStatus.Top = 0F;
			this.txtTaxStatus.Width = 0.40625F;
			// 
			// txtLimit
			// 
			this.txtLimit.Height = 0.1666667F;
			this.txtLimit.Left = 4.034722F;
			this.txtLimit.Name = "txtLimit";
			this.txtLimit.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLimit.Text = "Field1";
			this.txtLimit.Top = 0F;
			this.txtLimit.Width = 0.65625F;
			// 
			// txtDeductionDescription
			// 
			this.txtDeductionDescription.Height = 0.1666667F;
			this.txtDeductionDescription.Left = 0.21875F;
			this.txtDeductionDescription.Name = "txtDeductionDescription";
			this.txtDeductionDescription.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDeductionDescription.Text = "Field1";
			this.txtDeductionDescription.Top = 0F;
			this.txtDeductionDescription.Width = 1.4375F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1666667F;
			this.txtAmount.Left = 2.1875F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmount.Text = "Field1";
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.59375F;
			// 
			// txtAmountType
			// 
			this.txtAmountType.Height = 0.1666667F;
			this.txtAmountType.Left = 2.815972F;
			this.txtAmountType.Name = "txtAmountType";
			this.txtAmountType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtAmountType.Text = "Field1";
			this.txtAmountType.Top = 0F;
			this.txtAmountType.Width = 0.40625F;
			// 
			// txtLimitType
			// 
			this.txtLimitType.Height = 0.1666667F;
			this.txtLimitType.Left = 4.722222F;
			this.txtLimitType.Name = "txtLimitType";
			this.txtLimitType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLimitType.Text = null;
			this.txtLimitType.Top = 0F;
			this.txtLimitType.Width = 0.5F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1666667F;
			this.txtAccount.Left = 5.25F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.034722F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.1666667F;
			this.txtMTD.Left = 6.875F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMTD.Text = "Field1";
			this.txtMTD.Top = 0F;
			this.txtMTD.Width = 0.6875F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.1666667F;
			this.txtQTD.Left = 7.5625F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtQTD.Text = "Field1";
			this.txtQTD.Top = 0F;
			this.txtQTD.Width = 0.6875F;
			// 
			// txtCYTD
			// 
			this.txtCYTD.Height = 0.1666667F;
			this.txtCYTD.Left = 8.25F;
			this.txtCYTD.Name = "txtCYTD";
			this.txtCYTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtCYTD.Text = "Field1";
			this.txtCYTD.Top = 0F;
			this.txtCYTD.Width = 0.75F;
			// 
			// txtFYTD
			// 
			this.txtFYTD.Height = 0.1666667F;
			this.txtFYTD.Left = 9F;
			this.txtFYTD.Name = "txtFYTD";
			this.txtFYTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFYTD.Text = "Field1";
			this.txtFYTD.Top = 0F;
			this.txtFYTD.Width = 0.71875F;
			// 
			// txtFrequency
			// 
			this.txtFrequency.Height = 0.1666667F;
			this.txtFrequency.Left = 3.25F;
			this.txtFrequency.Name = "txtFrequency";
			this.txtFrequency.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtFrequency.Text = "Field1";
			this.txtFrequency.Top = 0F;
			this.txtFrequency.Width = 0.75F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1666667F;
			this.txtStatus.Left = 6.3125F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtStatus.Text = "Field1";
			this.txtStatus.Top = 0F;
			this.txtStatus.Width = 0.5625F;
			// 
			// txtTotMTD
			// 
			this.txtTotMTD.Height = 0.1875F;
			this.txtTotMTD.Left = 6.9375F;
			this.txtTotMTD.Name = "txtTotMTD";
			this.txtTotMTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotMTD.Text = "Field1";
			this.txtTotMTD.Top = 0.15625F;
			this.txtTotMTD.Width = 0.65625F;
			// 
			// txtTotQTD
			// 
			this.txtTotQTD.Height = 0.1875F;
			this.txtTotQTD.Left = 7.59375F;
			this.txtTotQTD.Name = "txtTotQTD";
			this.txtTotQTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotQTD.Text = "Field1";
			this.txtTotQTD.Top = 0.15625F;
			this.txtTotQTD.Width = 0.65625F;
			// 
			// txtTotFYTD
			// 
			this.txtTotFYTD.Height = 0.1666667F;
			this.txtTotFYTD.Left = 8.96875F;
			this.txtTotFYTD.Name = "txtTotFYTD";
			this.txtTotFYTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotFYTD.Text = "Field1";
			this.txtTotFYTD.Top = 0.15625F;
			this.txtTotFYTD.Width = 0.75F;
			// 
			// txtTotCYTD
			// 
			this.txtTotCYTD.Height = 0.1666667F;
			this.txtTotCYTD.Left = 8.333333F;
			this.txtTotCYTD.Name = "txtTotCYTD";
			this.txtTotCYTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotCYTD.Text = "Field1";
			this.txtTotCYTD.Top = 0.1666667F;
			this.txtTotCYTD.Width = 0.6666667F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 6.90625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 2.84375F;
			this.Line2.X1 = 6.90625F;
			this.Line2.X2 = 9.75F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// rptCustomMatchSetup
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.770833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductionDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimitType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLimit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeductionDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLimitType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrequency;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotFYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCYTD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox t;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
