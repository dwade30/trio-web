﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPaySummaryReport.
	/// </summary>
	partial class rptPaySummaryReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPaySummaryReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			//this.vsHeader = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHead9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			//this.vsData = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				//this.vsData,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Line2
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field19,
				this.Field20,
				this.Field21,
				this.Field22,
				this.Field23,
				this.Field24,
				this.Field25,
				this.Field27,
				this.Field28,
				this.Line4
			});
			this.ReportFooter.Height = 0.4375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.BeforePrint += new System.EventHandler(this.PageHeader_BeforePrint);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				//this.vsHeader,
				this.txtDate,
				this.txtMuniName,
				this.txtPage,
				this.Line1,
				this.DLG1,
				this.txtTime,
				this.txtTitle2,
				this.txtHead1,
				this.txtHead2,
				this.txtHead3,
				this.txtHead4,
				this.txtHead5,
				this.txtHead6,
				this.txtHead7,
				this.txtHead8,
				this.txtHead9
			});
			this.PageHeader.Height = 0.9375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup
			});
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0.1979167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field17,
				this.Field18,
				this.Line3
			});
			this.GroupFooter1.Height = 0.3541667F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.Left = 1.6875F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 4.0625F;
			// 
			// vsHeader
			// 
			//this.vsHeader.Height = 0.375F;
			//this.vsHeader.Left = 0.25F;
			//this.vsHeader.Name = "vsHeader";
			//this.vsHeader.Top = 0F;
			//this.vsHeader.Visible = false;
			//this.vsHeader.Width = 6.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.25F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 2.1875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.3125F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 7.499306F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.499306F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3125F;
			this.DLG1.Left = 0F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// txtTitle2
			// 
			this.txtTitle2.Height = 0.1875F;
			this.txtTitle2.Left = 1.6875F;
			this.txtTitle2.MultiLine = false;
			this.txtTitle2.Name = "txtTitle2";
			this.txtTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtTitle2.Text = "Page";
			this.txtTitle2.Top = 0.25F;
			this.txtTitle2.Width = 4.0625F;
			// 
			// txtHead1
			// 
			this.txtHead1.Height = 0.1875F;
			this.txtHead1.Left = 0F;
			this.txtHead1.Name = "txtHead1";
			this.txtHead1.Text = "Employee";
			this.txtHead1.Top = 0.625F;
			this.txtHead1.Width = 0.5625F;
			// 
			// txtHead2
			// 
			this.txtHead2.Height = 0.1875F;
			this.txtHead2.Left = 0.5625F;
			this.txtHead2.Name = "txtHead2";
			this.txtHead2.Text = null;
			this.txtHead2.Top = 0.625F;
			this.txtHead2.Width = 1.25F;
			// 
			// txtHead3
			// 
			this.txtHead3.Height = 0.1875F;
			this.txtHead3.Left = 1.9375F;
			this.txtHead3.Name = "txtHead3";
			this.txtHead3.Style = "text-align: right";
			this.txtHead3.Text = "Regular";
			this.txtHead3.Top = 0.625F;
			this.txtHead3.Width = 0.625F;
			// 
			// txtHead4
			// 
			this.txtHead4.Height = 0.1875F;
			this.txtHead4.Left = 2.75F;
			this.txtHead4.Name = "txtHead4";
			this.txtHead4.Style = "text-align: right";
			this.txtHead4.Text = "Overtime";
			this.txtHead4.Top = 0.625F;
			this.txtHead4.Width = 0.625F;
			// 
			// txtHead5
			// 
			this.txtHead5.Height = 0.1875F;
			this.txtHead5.Left = 3.5625F;
			this.txtHead5.Name = "txtHead5";
			this.txtHead5.Style = "text-align: right";
			this.txtHead5.Text = "Other";
			this.txtHead5.Top = 0.625F;
			this.txtHead5.Width = 0.625F;
			// 
			// txtHead6
			// 
			this.txtHead6.Height = 0.1875F;
			this.txtHead6.Left = 4.375F;
			this.txtHead6.Name = "txtHead6";
			this.txtHead6.Style = "text-align: right";
			this.txtHead6.Text = "Gross";
			this.txtHead6.Top = 0.625F;
			this.txtHead6.Width = 0.625F;
			// 
			// txtHead7
			// 
			this.txtHead7.Height = 0.1875F;
			this.txtHead7.Left = 5.3125F;
			this.txtHead7.Name = "txtHead7";
			this.txtHead7.Style = "text-align: right";
			this.txtHead7.Text = "Taxes";
			this.txtHead7.Top = 0.625F;
			this.txtHead7.Width = 0.5F;
			// 
			// txtHead8
			// 
			this.txtHead8.Height = 0.1875F;
			this.txtHead8.Left = 6F;
			this.txtHead8.Name = "txtHead8";
			this.txtHead8.Style = "text-align: right";
			this.txtHead8.Text = "Deducts";
			this.txtHead8.Top = 0.625F;
			this.txtHead8.Width = 0.625F;
			// 
			// txtHead9
			// 
			this.txtHead9.Height = 0.1875F;
			this.txtHead9.Left = 6.8125F;
			this.txtHead9.Name = "txtHead9";
			this.txtHead9.Style = "text-align: right";
			this.txtHead9.Text = "Net";
			this.txtHead9.Top = 0.625F;
			this.txtHead9.Width = 0.625F;
			// 
			// txtGroup
			// 
			this.txtGroup.Height = 0.2F;
			this.txtGroup.Left = 0F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "font-size: 8.5pt; font-weight: bold";
			this.txtGroup.Text = null;
			this.txtGroup.Top = 0F;
			this.txtGroup.Width = 2.833333F;
			// 
			// vsData
			// 
			//this.vsData.Height = 0.1875F;
			//this.vsData.Left = 0F;
			//this.vsData.Name = "vsData";
			//this.vsData.Top = 0.3125F;
			//this.vsData.Visible = false;
			//this.vsData.Width = 6.3125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "Field1";
			this.Field1.Top = 0F;
			this.Field1.Width = 0.5625F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.5625F;
			this.Field2.Name = "Field2";
			this.Field2.Text = "Field2";
			this.Field2.Top = 0F;
			this.Field2.Width = 1.25F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 1.75F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "text-align: right";
			this.Field3.Text = "Field3";
			this.Field3.Top = 0F;
			this.Field3.Width = 0.8125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 2.5625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "text-align: right";
			this.Field4.Text = "Field4";
			this.Field4.Top = 0F;
			this.Field4.Width = 0.8125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 3.375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "text-align: right";
			this.Field5.Text = "Field5";
			this.Field5.Top = 0F;
			this.Field5.Width = 0.8125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 4.1875F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "text-align: right";
			this.Field6.Text = "Field6";
			this.Field6.Top = 0F;
			this.Field6.Width = 0.8125F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 5F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "text-align: right";
			this.Field7.Text = "Field7";
			this.Field7.Top = 0F;
			this.Field7.Width = 0.8125F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 5.8125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "text-align: right";
			this.Field8.Text = "Field8";
			this.Field8.Top = 0F;
			this.Field8.Width = 0.8125F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 6.625F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "text-align: right";
			this.Field9.Text = "Field9";
			this.Field9.Top = 0F;
			this.Field9.Width = 0.8125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Visible = false;
			this.Line2.Width = 7.499306F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.499306F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 1.75F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "text-align: right";
			this.Field12.Text = "Field12";
			this.Field12.Top = 0F;
			this.Field12.Width = 0.8125F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 2.5625F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "text-align: right";
			this.Field13.Text = "Field13";
			this.Field13.Top = 0F;
			this.Field13.Width = 0.8125F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 3.375F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "text-align: right";
			this.Field14.Text = "Field14";
			this.Field14.Top = 0F;
			this.Field14.Width = 0.8125F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 4.1875F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "text-align: right";
			this.Field15.Text = "Field15";
			this.Field15.Top = 0F;
			this.Field15.Width = 0.8125F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 5F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "text-align: right";
			this.Field16.Text = "Field16";
			this.Field16.Top = 0F;
			this.Field16.Width = 0.8125F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 5.8125F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "text-align: right";
			this.Field17.Text = "Field17";
			this.Field17.Top = 0F;
			this.Field17.Width = 0.8125F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 6.625F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "text-align: right";
			this.Field18.Text = "Field18";
			this.Field18.Top = 0F;
			this.Field18.Width = 0.8125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 7.499306F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.499306F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 1.75F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "text-align: right";
			this.Field19.Text = "Field19";
			this.Field19.Top = 0.125F;
			this.Field19.Width = 0.8125F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 2.5625F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "text-align: right";
			this.Field20.Text = "Field20";
			this.Field20.Top = 0.125F;
			this.Field20.Width = 0.8125F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 3.375F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "text-align: right";
			this.Field21.Text = "Field21";
			this.Field21.Top = 0.125F;
			this.Field21.Width = 0.8125F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 4.1875F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "text-align: right";
			this.Field22.Text = "Field22";
			this.Field22.Top = 0.125F;
			this.Field22.Width = 0.8125F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 5F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "text-align: right";
			this.Field23.Text = "Field23";
			this.Field23.Top = 0.125F;
			this.Field23.Width = 0.8125F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 5.8125F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "text-align: right";
			this.Field24.Text = "Field24";
			this.Field24.Top = 0.125F;
			this.Field24.Width = 0.8125F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 6.625F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "text-align: right";
			this.Field25.Text = "Field25";
			this.Field25.Top = 0.125F;
			this.Field25.Width = 0.8125F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.1875F;
			this.Field27.Left = 0F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-size: 8.5pt; font-weight: bold";
			this.Field27.Text = "Totals";
			this.Field27.Top = 0.125F;
			this.Field27.Width = 0.6875F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 0.5625F;
			this.Field28.Name = "Field28";
			this.Field28.Text = "Field28";
			this.Field28.Top = 0.125F;
			this.Field28.Width = 1.25F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.125F;
			this.Line4.Width = 7.499306F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.499306F;
			this.Line4.Y1 = 0.125F;
			this.Line4.Y2 = 0.125F;
			// 
			// rptPaySummaryReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHead9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl vsData;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl vsHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHead9;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
	}
}
