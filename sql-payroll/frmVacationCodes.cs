//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	public partial class frmVacationCodes : BaseForm
	{
		public frmVacationCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVacationCodes InstancePtr
		{
			get
			{
				return (frmVacationCodes)Sys.GetInstance(typeof(frmVacationCodes));
			}
		}

		protected frmVacationCodes _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE AND DAN C. SOLTESZ
		// DATE:       APRIL 30,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ ON 06/11/2001
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbVacationCodes     As DAO.Database
		private clsDRWrapper rsVacationCodes = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, bool)
		private int intDataChanged;
		// vbPorter upgrade warning: intSave As int	OnWriteFCConvert.ToInt32(
		private int intSave;
		private int intNumRowsLocked;
		private bool boolGridUnlocked;
		private bool boolSkip;
		private bool boolComboScroll;
		private bool boolFormLoading;
		private string strPER = "";
		private bool boolGridSort;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cboType_TextChanged(object sender, System.EventArgs e)
		{
			// Call cboType_Click
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cboType_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				if (boolComboScroll)
				{
					boolComboScroll = false;
					return;
				}
				boolComboScroll = false;
				if (cboType.SelectedIndex < 0)
					intSave = 0;
				intSave = cboType.ItemData(cboType.SelectedIndex);
				modGlobalVariables.Statics.gintVacationCodeType = intSave;
				LoadGrid();
				LoadList();
				// 
				// If vsVacationCodes.Rows >= 2 Then
				// vsVacationCodes.Select 1, 2
				// 
				// If Not boolFormLoading Then vsVacationCodes.SetFocus
				// End If
				intDataChanged = 0;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cboType_Click()
		{
			cboType_SelectedIndexChanged(cboType, new System.EventArgs());
		}

		private void cboType_Enter(object sender, System.EventArgs e)
		{
			SaveChanges();
		}

		private void cboType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				if (modAPIsConst.SendMessage(this.cboType.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, IntPtr.Zero) == 0)
				{
					modAPIsConst.SendMessage(this.cboType.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, IntPtr.Zero);
					KeyCode = (Keys)0;
				}
				else
				{
					//App.DoEvents();
					cboType_Click();
				}
			}
			else if (KeyCode == Keys.Up || KeyCode == Keys.Down)
			{
				//App.DoEvents();
				cboType_Click();
			}
			else
			{
				boolComboScroll = true;
			}
		}

		private void cmdAss_Click(object sender, System.EventArgs e)
		{
			if (intSave != 0)
			{
				modGlobalVariables.Statics.gintVacationCodeType = intSave;
				frmAssociate.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				LoadList();
			}
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsVacationCodes.Row < 1)
					return;
				if (vsVacationCodes.Col < 0)
					return;
				if (Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 6)) == 0)
				{
					vsVacationCodes.RemoveItem(vsVacationCodes.Row);
					return;
				}
				if (MessageBox.Show("This action will delete record #" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 1) + " for the type: " + cboType.Text + "   Continue?", "Payroll Vacation\\Sick\\Other Codes ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// we don't want to remove this record from the table as there is still
					// a deduction number ??? be we want to clear it out.
					rsVacationCodes.OpenRecordset("Select top 1 * from tblVacationSick where CodeID = " + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 6) + " AND selected = 1");
					if (rsVacationCodes.EndOfFile())
					{
						clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsVacationCodes);
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsVacationCodes.Row) + "from Type: " + cboType.Text + " in Vacation / Sick / Other Codes Screen");
						// ------------------------------------------------------------------
						if (Strings.Right(FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 0)), 2) == "..")
						{
							rsVacationCodes.Execute("Delete from tblCodes Where ID = " + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 6), "Payroll");
							rsVacationCodes.Execute("delete from tblvacationsick where codeid > 0 and CODEid = " + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 6), "Payroll");
							// clear the edit symbol from the row number
							vsVacationCodes.TextMatrix(vsVacationCodes.Row, 0, Strings.Mid(vsVacationCodes.TextMatrix(vsVacationCodes.Row, 0), 1, (vsVacationCodes.TextMatrix(vsVacationCodes.Row, 0).Length < 2 ? 2 : vsVacationCodes.TextMatrix(vsVacationCodes.Row, 0).Length - 2)));
							LoadGrid();
						}
						else if (Strings.Right(FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 0)), 1) == ".")
						{
							vsVacationCodes.RemoveItem(vsVacationCodes.Row);
						}
						else
						{
							rsVacationCodes.Execute("Delete from tblCodes Where ID = " + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 6), "Payroll");
							rsVacationCodes.Execute("delete from tblvacationsick where codeid > 0 and CODEid = " + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 6), "Payroll");
							vsVacationCodes.RemoveItem(vsVacationCodes.Row);
						}
						// load the grid with the new data to show the 'clean out' of the current row
						// decrement the counter as to how many records are dirty
						intDataChanged -= 1;
						MessageBox.Show("Record Deleted successfully.", "Vacation Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Code #" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 1) + " is used by employee number: " + rsVacationCodes.Get_Fields("EmployeeNumber") + "\r" + "This code cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Vacation Codes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// append a new row to the end of the grid
				vsVacationCodes.AddItem(FCConvert.ToString(vsVacationCodes.Rows) + "\t" + FCConvert.ToString(vsVacationCodes.Rows));
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row " + FCConvert.ToString(vsVacationCodes.Rows - 1) + "to Type: " + cboType.Text + " in Vacation / Sick / Other Codes Screen");
				// ------------------------------------------------------------------
				// increment the counter as to how many records are dirty
				intDataChanged += 1;
				// vbPorter upgrade warning: intMaxRowNumber As int	OnWrite(int, double)
				int intMaxRowNumber;
				intMaxRowNumber = 0;
				if (intMaxRowNumber < FCConvert.ToDouble(vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 1)))
				{
					intMaxRowNumber = FCConvert.ToInt16(Conversion.Val(vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 2, 1)) + 1);
				}
				here:
				;
				// assign a row number to this new record
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 1, Strings.Format(intMaxRowNumber, "000"));
				// & "." 'vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 0) & "."
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 0, vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 0) + ".");
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 3, "0");
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 4, "Hours");
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 5, "Pay Period");
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 8, "9999.99");
				vsVacationCodes.TextMatrix(vsVacationCodes.Rows - 1, 9, "Never");
				// Call vsVacationCodes_AfterEdit(vsVacationCodes.Rows - 1, 0)
				// set focus to this new row
				vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Rows - 1, 7, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsVacationCodes.Select(vsVacationCodes.Rows - 1, 2);
				vsVacationCodes.Focus();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// display the Vacation code report
				frmReportViewer.InstancePtr.Init(rptVacationCodes.InstancePtr, boolAllowEmail: true, strAttachmentName: "VacationCodes");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				SaveChanges();
				// load the grid of current data from the database
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				string strTemp = "";
				vsVacationCodes.Select(0, 0);
				if (NotValidData())
					goto ExitRoutine;
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Vacation / Sick / Other Codes", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				for (intCounter = 1; intCounter <= (vsVacationCodes.Rows - 1); intCounter++)
				{
					if (Strings.Right(FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
					{
						// record has been edited so we need to update it
						// strTemp = Split(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5), "added")
						strTemp = FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5));
						if (FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) == string.Empty && Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) == 0)
						{
						}
						else
						{
							rsVacationCodes.Execute("Update tblCodes set Code ='" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "',  Description = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2))) + "', Rate = " + FCConvert.ToString(Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3))) + ", DHW = '" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4) + "', Type = '" + fecherFoundation.Strings.Trim(strTemp) + "', MonthDay = '" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7) + "', Max = " + FCConvert.ToString(Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 8))) + " , Cleared = '" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 9) + "' , CodeType = " + FCConvert.ToString(intSave) + ", LastUserID = ' " + modGlobalVariables.Statics.gstrUser + "' Where ID = " + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6), "Payroll");
						}
						// clear the edit symbol from the row number
						vsVacationCodes.TextMatrix(intCounter, 0, Strings.Mid(vsVacationCodes.TextMatrix(intCounter, 0), 1, vsVacationCodes.TextMatrix(intCounter, 0).Length - 2));
						goto NextRecord;
					}
					if (Strings.Right(FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
					{
						// record has been added so we need to save it
						// strTemp = Split(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5), "added")
						strTemp = FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5));
						if (FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) == string.Empty && Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) == 0)
						{
						}
						else
						{
							rsVacationCodes.Execute("Insert into tblCodes (Code, Description, Rate, DHW, MonthDay, Max, Type,Cleared,CodeType,LastUserID) VALUES ('" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "','" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'," + FCConvert.ToString(Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3))) + ",'" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4) + "','" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7) + "'," + FCConvert.ToString(Conversion.Val(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 8))) + " ,'" + (fecherFoundation.Strings.Trim(strTemp) != "" ? fecherFoundation.Strings.Trim(strTemp) : "Pay Period") + "','" + vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 9) + "'," + FCConvert.ToString(intSave) + ",'" + modGlobalVariables.Statics.gstrUser + "')", "Payroll");
						}
						// clear the edit symbol from the row number
						vsVacationCodes.TextMatrix(intCounter, 0, Strings.Mid(vsVacationCodes.TextMatrix(intCounter, 0), 1, vsVacationCodes.TextMatrix(intCounter, 0).Length - 1));
						// get the new ID from the table for the record that was just added and place it into column 3
						rsVacationCodes.OpenRecordset("Select Max(ID) as NewID from tblCodes", "TWPY0000.vb1");
						vsVacationCodes.TextMatrix(intCounter, 6, FCConvert.ToString(rsVacationCodes.Get_Fields("NewID")));
					}
					NextRecord:
					;
				}
				MessageBox.Show("Record(s) Saved successfully.", "Vacation/Sick Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				boolComboScroll = false;
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				clsHistoryClass.Compare();
				LoadGrid();
				return;
				ExitRoutine:
				;
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Vacationroll Vacation Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			for (intCounter = 1; intCounter <= (this.vsVacationCodes.Rows - 1); intCounter++)
			{
				for (intCount = 1; intCount <= (this.vsVacationCodes.Rows - 1); intCount++)
				{
					if (vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCount, 1) == vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))
					{
						if (intCount != intCounter)
						{
							MessageBox.Show("Invalid Code Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							vsVacationCodes.Select(intCount, 0);
							NotValidData = true;
							return NotValidData;
						}
					}
				}
			}
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmVacationCodes_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
				vsVacationCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				boolFormLoading = false;
				cboType.Focus();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadList()
		{
			lstAss.Clear();
			rsVacationCodes.OpenRecordset("Select * from tblAssociateCodes where CodeType = " + FCConvert.ToString(modGlobalVariables.Statics.gintVacationCodeType), "TWPY0000.vb1");
			if (!rsVacationCodes.EndOfFile())
			{
				rsVacationCodes.MoveLast();
				rsVacationCodes.MoveFirst();
			}
			else
			{
				// MsgBox "No Pay Codes Have Been Associated With", , "Payroll"
			}
			while (!rsVacationCodes.EndOfFile())
			{
				lstAss.AddItem(FCConvert.ToString(rsVacationCodes.Get_Fields("Description")));
				rsVacationCodes.MoveNext();
			}
		}

		public void NEWRECORD()
		{
			// Set the error routines
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "NewRecord";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strType;
				strType = Interaction.InputBox("Enter name for new Code Type.", "TRIO Software", null);
				if (strType != string.Empty)
				{
					rsVacationCodes.Execute("Insert into tblCodeTypes (Description,LastUserID) VALUES ('" + FCConvert.ToString(modGlobalRoutines.FixQuote(strType)) + "','" + modGlobalVariables.Statics.gstrUser + "')", "Payroll");
					vsVacationCodes.Rows = 1;
					LoadCombo();
					// Dave 12/14/2006---------------------------------------------------
					// Add change record for adding a row to the grid
					clsReportChanges.AddChange("Added Type: " + strType + " in Vacation / Sick / Other Codes Screen");
					// ------------------------------------------------------------------
					lstAss.Clear();
					// mnuEdit.Visible = True
				}
				cboType.SelectedIndex = cboType.Items.Count - 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void AddNewGridRecord()
		{
			mnuNew_Click(null, null);
		}

		public void DeleteRecord()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "DeleteRecord";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strType = "";
				if (cboType.SelectedIndex < 0)
					return;
				rsVacationCodes.OpenRecordset("Select * from tblCodes where CodeType = " + FCConvert.ToString(intSave), "TWPY0000.vb1");
				if (rsVacationCodes.EndOfFile())
				{
					rsVacationCodes.Execute("Delete from tblCodeTypes where ID = " + FCConvert.ToString(intSave), "Payroll");
					vsVacationCodes.Rows = 1;
					LoadCombo();
				}
				else
				{
					MessageBox.Show("Code Type: " + cboType.Text + " has records defined. Cannot delete.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void LockGridCells(int intStart)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LockGridCells";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolGridUnlocked = false;
				if (vsVacationCodes.Rows > 1)
				{
					vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, intStart, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					if (intStart < vsVacationCodes.Rows - 1)
					{
						vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intStart + 1, 2, (vsVacationCodes.Rows - 1), 2, Color.White);
						boolGridUnlocked = true;
					}
				}
				// mnuEdit.Visible = boolGridUnlocked
				intNumRowsLocked = intStart;
				vsVacationCodes.Refresh();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmVacationCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyDown";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (Shift == 1 && KeyCode == Keys.F12)
				{
					modGlobalVariables.Statics.gobjEditFormName = this;
					frmEditLockedForm.InstancePtr.Show();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmVacationCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmVacationCodes_Load(object sender, System.EventArgs e)
		{
            try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				boolFormLoading = true;
				// open the forms global database connection
				// Set dbVacationCodes = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsVacationCodes.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				// load the combo with the code types
				LoadCombo();
				LoadList();
				// clear out the grid
				vsVacationCodes.Rows = 1;
				int intCount;
				for (intCount = 0; intCount <= cboType.Items.Count; intCount++)
				{
					if (cboType.Items[intCount].ToString() == "Vacation")
					{
						cboType.SelectedIndex = intCount;
						break;
					}
				}
				intDataChanged = FCConvert.ToInt16(false);
				vsVacationCodes.Select(0, 0);
				clsHistoryClass.SetGridIDColumn("PY", "vsVacationCodes", 6);

                lstAss.Columns[0].Width = FCConvert.ToInt32(lstAss.Width * 0.9);
                lstAss.GridLineStyle = GridLineStyle.None;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadCombo()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadCombo";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				cboType.Clear();
				rsVacationCodes.OpenRecordset("Select * from tblCodeTypes Order by ID", "TWPY0000.vb1");
				if (!rsVacationCodes.EndOfFile())
				{
					rsVacationCodes.MoveLast();
					rsVacationCodes.MoveFirst();
					for (intCounter = 1; intCounter <= (rsVacationCodes.RecordCount()); intCounter++)
					{
						cboType.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsVacationCodes.Get_Fields("Description"))));
						cboType.ItemData(cboType.NewIndex, FCConvert.ToInt32(rsVacationCodes.Get_Fields("ID")));
						rsVacationCodes.MoveNext();
					}
				}
				// cboType.ListIndex = 0
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int counter;
				vsVacationCodes.Rows = 1;
				if (cboType.SelectedIndex < 0)
					return;
				string strCodeName = "";
				// get all of the records from the database
				rsVacationCodes.OpenRecordset("Select * from tblCodes where CodeType = " + FCConvert.ToString(intSave) + " Order by Code", "TWPY0000.vb1");
				if (!rsVacationCodes.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsVacationCodes.MoveLast();
					rsVacationCodes.MoveFirst();
					// set the number of rows in the grid
					vsVacationCodes.Rows = rsVacationCodes.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsVacationCodes.RecordCount()); intCounter++)
					{
						vsVacationCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						vsVacationCodes.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields("Code") + " "));
						vsVacationCodes.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields("Description") + " "));
						vsVacationCodes.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields("Rate") + " "));
						vsVacationCodes.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("DHW") + " "));
						vsVacationCodes.TextMatrix(intCounter, 5, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields("Type") + " "));
						if (vsVacationCodes.TextMatrix(intCounter, 5) == "Pay Period")
						{
							vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 7, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
                        vsVacationCodes.TextMatrix(intCounter, 6, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields("ID") + " "));
						vsVacationCodes.TextMatrix(intCounter, 7, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("MonthDay") + " "));
						vsVacationCodes.TextMatrix(intCounter, 8, fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields("Max") + " "));
                        if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "Anniversary")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(1));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "Never")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(6));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "January 1")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(4));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "1st Wk Fiscal Yr")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(3));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "1st Wk Cal Yr")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(2));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "Anniversary Date")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(5));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "Anniversary Month")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(1));
						}
						else if (fecherFoundation.Strings.Trim(rsVacationCodes.Get_Fields_String("Cleared") + " ") == "")
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(1));
						}
						else
						{
							vsVacationCodes.TextMatrix(intCounter, 9, FCConvert.ToString(Conversion.Val(rsVacationCodes.Get_Fields("CLeared"))));
						}
						rsVacationCodes.MoveNext();
					}
					// Call LockGridCells(rsVacationCodes.RecordCount)
					vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsVacationCodes.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				// vsVacationCodes.WordWrap = True
				vsVacationCodes.Cols = 10;
				vsVacationCodes.FixedCols = 1;
				vsVacationCodes.ColHidden(0, true);
				vsVacationCodes.ColHidden(6, true);
				modGlobalRoutines.CenterGridCaptions(ref vsVacationCodes);
				// set column 0 properties
				vsVacationCodes.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(0) = 400
				// set column 1 properties
				vsVacationCodes.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(1) = 700
				vsVacationCodes.TextMatrix(0, 1, "Code");
				// vsVacationCodes.ColFormat(1) = "000"
				// set column 2 properties
				vsVacationCodes.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(2) = 2350
				vsVacationCodes.TextMatrix(0, 2, "Description");
				// set column 3 properties
				vsVacationCodes.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsVacationCodes.ColWidth(3) = 1000
				vsVacationCodes.TextMatrix(0, 3, "Rate");
                //FC:FINAL:BSE #2581 set allowed keys for client side check
                vsVacationCodes.ColAllowedKeys(3, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                // set column 4 properties
                vsVacationCodes.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(4) = 700
				vsVacationCodes.TextMatrix(0, 4, "U/M");
				vsVacationCodes.ColComboList(4, "#1;Days|#2;Hours|#3;Weeks");
				// set column 5 properties
				vsVacationCodes.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(5) = 1000
				vsVacationCodes.TextMatrix(0, 5, "Per");
				// vsVacationCodes.ColComboList(5, "#1;Pay Period|#2;Month|#3;Quarter|#4;Year|#5;Anniversary"
				//vsVacationCodes.ColComboList(5, "#1;Pay Period" + "\t" + "Added Each Pay Period|#2;Month" + "\t" + "Added Monthly|#4;Quarter" + "\t" + "Added Quarterly|#5;Year" + "\t" + "Added Yearly|#6;Anniversary" + "\t" + "Added Anniversary Month|#8;Anniversary Date");
                var comboString = "#" + EarnedTimeAccrual.Hour.ToInteger() + ";"
								  + EarnedTimeAccrual.Hour.ToName() + "\t"
								  + EarnedTimeAccrual.Hour.ToDescription()
                                  + "|#" + EarnedTimeAccrual.PayPeriod.ToInteger() + ";" +
                                  EarnedTimeAccrual.PayPeriod.ToName() + "\t" +
                                  EarnedTimeAccrual.PayPeriod.ToDescription() +
                                  "|#" + EarnedTimeAccrual.Month.ToInteger() + ";" +
                                  EarnedTimeAccrual.Month.ToName() + "\t" +
                                  EarnedTimeAccrual.Month.ToDescription() +
                                  "|#" + EarnedTimeAccrual.Quarter.ToInteger() + ";" +
                                  EarnedTimeAccrual.Quarter.ToName() + "\t" +
                                  EarnedTimeAccrual.Quarter.ToDescription() +
                                  "|#" + EarnedTimeAccrual.Year.ToInteger() + ";" +
                                  EarnedTimeAccrual.Year.ToName() + "\t" +
                                  EarnedTimeAccrual.Year.ToDescription() +
                                  "|#" + EarnedTimeAccrual.AnniversaryMonth.ToInteger() + ";" +
                                  EarnedTimeAccrual.AnniversaryMonth.ToName() + "\t" +
                                  EarnedTimeAccrual.AnniversaryMonth.ToDescription() +
                                  "|#" + EarnedTimeAccrual.AnniversaryDate.ToInteger() + ";" +
                                  EarnedTimeAccrual.AnniversaryDate.ToName() + "\t" +
                                  EarnedTimeAccrual.AnniversaryDate.ToDescription();
                vsVacationCodes.ColComboList(5, comboString);                
				// set column 7 properties
				vsVacationCodes.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(7) = 1000
				vsVacationCodes.TextMatrix(0, 7, "Added On");
                //FC:FINAL:BSE #2581 set allowed keys for client side check
                vsVacationCodes.ColAllowedKeys(7, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',47..57");
                // set column 8 properties
                vsVacationCodes.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsVacationCodes.ColWidth(8) = 1000
				vsVacationCodes.TextMatrix(0, 8, "Max");
				vsVacationCodes.ColFormat(8, "###0.00");
				// set column 9 properties
				vsVacationCodes.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsVacationCodes.ColWidth(9) = 1000
				vsVacationCodes.TextMatrix(0, 9, "Cleared");
				vsVacationCodes.ColComboList(9, "#1;Anniversary Month|#5;Anniversary Date|#2;1st Wk Cal Yr|#3;1st Wk Fiscal Yr|#6;Never");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmVacationCodes_Resize(object sender, System.EventArgs e)
		{
			vsVacationCodes.ColWidth(0, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.05));
			vsVacationCodes.ColWidth(1, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.06));
			vsVacationCodes.ColWidth(2, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.28));
			vsVacationCodes.ColWidth(3, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.07));
			vsVacationCodes.ColWidth(4, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.08));
			vsVacationCodes.ColWidth(5, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.16));
			vsVacationCodes.ColWidth(7, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.11));
			vsVacationCodes.ColWidth(8, FCConvert.ToInt32(vsVacationCodes.WidthOriginal * 0.08));
			// vsVacationCodes.ColWidth(9) = vsVacationCodes.Width * 0.1
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsVacationCodes.Select(0, 0);
				SaveChanges();
				intDataChanged = 0;
				if (frmVacationSick.InstancePtr.boolFromVacSick == true)
				{
					frmVacationSick.InstancePtr.Show();
				}
				else
				{
					//MDIParent.InstancePtr.Show();
					//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
				}
				frmVacationSick.InstancePtr.boolFromVacSick = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuChangeCode_Click()
		{
			string strType;
			strType = Interaction.InputBox("Enter name for changing Code Type.", "TRIO Software", null);
			if (strType != string.Empty)
			{
				rsVacationCodes.Execute("Update tblCodeTypes set Description = '" + strType + "',   LastUserID = '" + modGlobalVariables.Statics.gstrUser + "' where ID = " + FCConvert.ToString(intSave), "Payroll");
				vsVacationCodes.Rows = 1;
				LoadCombo();
				// mnuEdit.Visible = True
			}
			cboType.SelectedIndex = cboType.Items.Count - 1;
			MessageBox.Show("Code Successfully Changed", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (cboType.SelectedIndex >= 0)
			{
				if (MessageBox.Show("This will delete the current code: " + cboType.Text + "   Continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsVacationCodes.OpenRecordset("Select * from tblCheckDetail where VSTypeID = " + FCConvert.ToString(cboType.ItemData(cboType.SelectedIndex)));
					if (rsVacationCodes.EndOfFile())
					{
						rsVacationCodes.OpenRecordset("Select * from tblVacationSick where selected = 1 and TypeID = " + FCConvert.ToString(cboType.ItemData(cboType.SelectedIndex)));
						if (rsVacationCodes.EndOfFile())
						{
							rsVacationCodes.OpenRecordset("Select * from tblCodes  where CodeType = " + FCConvert.ToString(cboType.ItemData(cboType.SelectedIndex)));
							if (rsVacationCodes.EndOfFile())
							{
								// there are no codes for this type. Allow to delete
								if (cboType.Text != "Sick" && cboType.Text != "Vacation")
								{
									// Dave 12/14/2006---------------------------------------------------
									// Add change record for adding a row to the grid
									clsReportChanges.AddChange("Deleted Type: " + cboType.Text + " in Vacation / Sick / Other Codes Screen");
									// ------------------------------------------------------------------
									rsVacationCodes.Execute("Delete from tblCodeTypes where ID = " + FCConvert.ToString(cboType.ItemData(cboType.SelectedIndex)), "Payroll");
									rsVacationCodes.Execute("Delete from tblCodes where CodeType = " + FCConvert.ToString(cboType.ItemData(cboType.SelectedIndex)), "Payroll");
									rsVacationCodes.Execute("delete from tblvacationsick where typeid = " + FCConvert.ToString(cboType.ItemData(cboType.SelectedIndex)), "Payroll");
								}
							}
							else
							{
								MessageBox.Show("There are codes defined for this type. This code type cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							LoadCombo();
							LoadGrid();
							MessageBox.Show("Code Type has been deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("This type is currently being used by employee: " + rsVacationCodes.Get_Fields("EmployeeNumber") + "\r" + "Code type cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						MessageBox.Show("This type is currently being used by employee: " + rsVacationCodes.Get_Fields("EmployeeNumber") + "\r" + "Code type cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuEditTypeDescription_Click(object sender, System.EventArgs e)
		{
			// Set the error routines
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuEditTypeDescription_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strType = "";
				if (fecherFoundation.Strings.Trim(this.cboType.Text) == "Sick" || fecherFoundation.Strings.Trim(this.cboType.Text) == "Vacation")
				{
					// DO NOT WANT TO ALLOW THEM TO CHANGE THE VACATION AND SICK CODES BUT THEY
					// CAN CHANGE ANY OF THE OTHER ONES. MATTHEW 9/22/2004
					MessageBox.Show("The types sick and vacation cannot be changed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					strType = Interaction.InputBox("Enter new name for the type: " + this.cboType.Text, "TRIO Software", null);
					if (strType != string.Empty)
					{
						rsVacationCodes.Execute("Update tblCodeTypes Set Description = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(strType)) + "' Where Description = '" + cboType.Text + "'", "TWPY0000.vb1");
						vsVacationCodes.Rows = 1;
						LoadCombo();
					}
					cboType.SelectedIndex = cboType.Items.Count - 1;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// display the Vacation code report
				modDuplexPrinting.DuplexPrintReport(rptVacationCodes.InstancePtr);
				// rptVacationCodes.PrintReport False
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				cmdPrint_Click();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			this.vsVacationCodes.Select(0, 0);
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE #4124 close form after saving
            this.vsVacationCodes.Select(0, 0);
            cmdSave_Click();
			cmdExit_Click();
		}

		private void mnuType_Click(object sender, System.EventArgs e)
		{
			NEWRECORD();
		}

		private void vsVacationCodes_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsVacationCodes_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (vsVacationCodes.Col == 1)
				{
					// For intCounter = 1 To vsVacationCodes.Rows - 1
					// If vsVacationCodes.TextMatrix(Row, Col) = vsVacationCodes.TextMatrix(intCounter, 1) Then
					// Call MsgBox("Code Already In Use", vbInformation, "Error")
					// vsVacationCodes.Select vsVacationCodes.Row, 1
					// Exit Sub
					// End If
					// Next
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col, Strings.Format(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, vsVacationCodes.Col), "000"));
				}
				if (vsVacationCodes.Col == 3)
				{
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col, Strings.Format(FCConvert.ToString(Conversion.Val(vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col))), "#.0#####"));
				}
				if (vsVacationCodes.Col == 8)
				{
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col, Strings.Format(FCConvert.ToString(Conversion.Val(vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col))), "#.0#"));
				}
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, 0, vsVacationCodes.TextMatrix(vsVacationCodes.Row, 0) + "..");
				}
				if (vsVacationCodes.Col == 5 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == "4")
				{
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, 7, FCConvert.ToString(DateTime.Today.Month) + "/1");
					// Format(Date, "MM/DD")
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				if (vsVacationCodes.Col == 7 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, 5) == "4" && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == string.Empty)
				{
					MessageBox.Show("Please Enter Month/Day", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col, FCConvert.ToString(DateTime.Today.Month) + "/1");
					// Format(Date, "MM/DD")
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				if (vsVacationCodes.Col == 5 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == "2")
				{
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, 7, FCConvert.ToString(1));
					// Format(Date, "DD")
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				if (vsVacationCodes.Col == 7 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, 5) == "2" && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == string.Empty)
				{
					MessageBox.Show("Please Enter Month/Day", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col, FCConvert.ToString(1));
					// Format(Date, "DD")
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				if (vsVacationCodes.Col == 5 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == "3")
				{
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, 7, FCConvert.ToString(1));
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				if (vsVacationCodes.Col == 7 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, 5) == "3" && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == string.Empty)
				{
					MessageBox.Show("Please Enter Month/Day", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col, Strings.Format(DateTime.Today, "MM"));
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				if (vsVacationCodes.Col == 5 && vsVacationCodes.TextMatrix(vsVacationCodes.Row, vsVacationCodes.Col) == "5")
				{
					vsVacationCodes.TextMatrix(vsVacationCodes.Row, 7, FCConvert.ToString(DateTime.Today.Month) + "/1");
					vsVacationCodes.Select(vsVacationCodes.Row, 7);
				}
				// vsVacationCodes.EditCell
				// vsVacationCodes.EditSelStart = 0
				// vsVacationCodes.EditSelLength = Len(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, vsVacationCodes.Col))
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsVacationCodes_ChangeEdit(object sender, System.EventArgs e)
		{
			string strTemp;
			// strTemp = Split(vsVacationCodes.EditText, "added")
			strTemp = vsVacationCodes.EditText;
			if (vsVacationCodes.Col == 5)
			{
				if (fecherFoundation.Strings.Trim(strTemp) == "Year")
				{
					vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7, Color.White);
					strPER = "Year";
				}
				else if (fecherFoundation.Strings.Trim(strTemp) == "Month")
				{
					vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7, Color.White);
					strPER = "Month";
				}
				else if (fecherFoundation.Strings.Trim(strTemp) == "Quarter")
				{
					vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7, Color.White);
					strPER = "Quarter";
				}
				else if (fecherFoundation.Strings.Trim(strTemp) == "Anniversary")
				{
					vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7, Color.White);
					strPER = "Anniversary";
				}
				else
				{
					strPER = "";
					if (vsVacationCodes.Row > 0)
					{
						vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsVacationCodes.TextMatrix(vsVacationCodes.Row, 7, " ");
					}
				}
			}
		}

		private void vsVacationCodes_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsVacationCodes.Rows < 2)
				return;
			if (vsVacationCodes.MouseRow == 0)
			{
				vsVacationCodes.Select(1, 0, vsVacationCodes.Rows - 1, vsVacationCodes.Cols - 1);
				vsVacationCodes.ColSel = vsVacationCodes.MouseCol;
				vsVacationCodes.Col = vsVacationCodes.MouseCol;
				if (boolGridSort)
				{
					vsVacationCodes.Sort = (FCGrid.SortSettings)1;
				}
				else
				{
					vsVacationCodes.Sort = (FCGrid.SortSettings)2;
				}
				boolGridSort = !boolGridSort;
				vsVacationCodes.Select(0, 0);
			}
		}

		private void vsVacationCodes_DblClick(object sender, System.EventArgs e)
		{
			vsVacationCodes.EditCell();
		}

		private void vsVacationCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			modGridKeyMove.MoveGridProsition(ref vsVacationCodes, 1, 9, e.KeyCode, 9999, true);
			// if the key is the TAB then make it work like the right arrow
			// If KeyCode = 9 Then KeyCode = 39
			// If KeyCode = 13 Then KeyCode = 39
			// 
			// want to skip any Month/Day getdata that are disabled
			// If vsVacationCodes.Col = 7 Then
			// up key
			// If KeyCode = 38 Then
			// NextCol:
			// If vsVacationCodes.Row > 1 Then
			// boolSkip = True
			// vsVacationCodes.Row = vsVacationCodes.Row - 1
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, vsVacationCodes.Col) = TRIOCOLORGRAYBACKGROUND Then
			// GoTo NextCol
			// End If
			// End If
			// 
			// KeyCode = 0
			// ElseIf KeyCode = 40 Then
			// down key
			// NextCol2:
			// If vsVacationCodes.Row < vsVacationCodes.Rows - 1 Then
			// this will skip all code in the rowcolchange event
			// boolSkip = True
			// vsVacationCodes.Row = vsVacationCodes.Row + 1
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, vsVacationCodes.Col) = TRIOCOLORGRAYBACKGROUND Then
			// GoTo NextCol2
			// End If
			// End If
			// NextCol3:
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, vsVacationCodes.Col) = TRIOCOLORGRAYBACKGROUND Then
			// boolSkip = True
			// vsVacationCodes.Row = vsVacationCodes.Row - 1
			// GoTo NextCol3
			// End If
			// 
			// KeyCode = 0
			// End If
			// End If
			// 
			// If vsVacationCodes.Col = 8 Then
			// If KeyCode = 37 Then
			// left key
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7) = TRIOCOLORGRAYBACKGROUND Then
			// vsVacationCodes.Col = 5
			// KeyCode = 0
			// End If
			// End If
			// End If
			// 
			// If vsVacationCodes.Col = 5 Then
			// If KeyCode = 39 Then
			// right key
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 7) = TRIOCOLORGRAYBACKGROUND Then
			// vsVacationCodes.Col = 8
			// KeyCode = 0
			// End If
			// End If
			// End If
			// 
			// If vsVacationCodes.Col = 3 Then
			// If KeyCode = 37 Then
			// left key
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, 2) = TRIOCOLORGRAYBACKGROUND Then
			// If vsVacationCodes.Row > 1 Then
			// 
			// vsVacationCodes.Col = 9
			// 
			// vsVacationCodes.Row = vsVacationCodes.Row - 1
			// End If
			// KeyCode = 0
			// End If
			// End If
			// End If
			// 
			// If vsVacationCodes.Col = 2 Then
			// If KeyCode = 38 Then
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row - 1, 2) = TRIOCOLORGRAYBACKGROUND Then
			// KeyCode = 0
			// End If
			// 
			// ElseIf KeyCode = 37 Then
			// left key
			// If vsVacationCodes.Row > 1 Then
			// vsVacationCodes.Col = 9
			// 
			// vsVacationCodes.Row = vsVacationCodes.Row - 1
			// End If
			// KeyCode = 0
			// End If
			// End If
			// 
			// If vsVacationCodes.Col = 9 Then
			// If KeyCode = 39 Then
			// right key
			// If vsVacationCodes.Row < vsVacationCodes.Rows - 1 Then
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row + 1, 2) = TRIOCOLORGRAYBACKGROUND Then
			// vsVacationCodes.Col = 3
			// Else
			// vsVacationCodes.Col = 1
			// End If
			// 
			// vsVacationCodes.Row = vsVacationCodes.Row + 1
			// KeyCode = 0
			// Else
			// Call cmdNew_Click
			// KeyCode = 0
			// End If
			// End If
			// End If
		}

		private void vsVacationCodes_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsVacationCodes_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsVacationCodes.Col == 9)
					{
						if (vsVacationCodes.Row < vsVacationCodes.Rows - 1)
						{
							vsVacationCodes.Row += 1;
							vsVacationCodes.Col = 2;
						}
						else
						{
							// if there is no other row then create a new one
							cmdNew_Click();
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsVacationCodes_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsVacationCodes_KeyPressEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int KeyAscii = Strings.Asc(e.KeyChar);
				if (vsVacationCodes.Col == 3 || vsVacationCodes.Col == 8 || vsVacationCodes.Col == 1)
				{
					// if the user is in the RATE or MAX columns then do not
					// allow the entry of a character
					if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 46 && KeyAscii != 8)
					{
						KeyAscii = 0;
					}
				}
				if (vsVacationCodes.Col == 7)
				{
					if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 47)
					{
						KeyAscii = 0;
					}
				}
				e.KeyChar = Strings.Chr(KeyAscii);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsVacationCodes_KeyUpEdit(object sender, KeyEventArgs e)
		{
			// DoEvents
			// If vsVacationCodes.Col = 7 Then
			// If Len(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpText, Row, Col)) = 1 Then
			// If vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Row, Col) > 1 Then
			// vsVacationCodes.TextMatrix(Row, Col) = "0" & Chr(KeyCode) & "/"
			// KeyAscii = 0
			// End If
			// End If
			// End If
		}

		private void vsVacationCodes_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			string strTemp = "";
			// vbPorter upgrade warning: intMouseCol As int	OnWriteFCConvert.ToInt32(
			int intMouseCol;
			// vbPorter upgrade warning: intMouseRow As int	OnWriteFCConvert.ToInt32(
			int intMouseRow;
            //FC:FINAL:BSE:#4179 - set tooltips
            if(e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            intMouseCol = vsVacationCodes.GetFlexColIndex(e.ColumnIndex);
			intMouseRow = vsVacationCodes.GetFlexRowIndex(e.RowIndex);
            DataGridViewCell cell = vsVacationCodes[e.ColumnIndex, e.RowIndex];
            if (intMouseCol == 7)
			{
				// strTemp = Split(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsVacationCodes.MouseRow, 5), " ")
				strTemp = FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intMouseRow, 5));
				// If UBound(strTemp) >= 0 Then
				if (strTemp == "Year")
				{
					//ToolTip1.SetToolTip(vsVacationCodes, "Enter Month/Week of Month");
                    cell.ToolTipText = "Enter Month/ Week of Month";

                }
				else if (strTemp == "Month")
				{
					//ToolTip1.SetToolTip(vsVacationCodes, "Enter Week of Month");
                    cell.ToolTipText = "Enter Week of Month";

                }
				else if (strTemp == "Quarter")
				{
                    //ToolTip1.SetToolTip(vsVacationCodes, "Enter Month of Quarter (1-3) / Week of Month");
                    cell.ToolTipText = "Enter Month of Quarter (1-3) / Week of Month";

                }
				else if (strTemp == "Anniversary")
				{
					//ToolTip1.SetToolTip(vsVacationCodes, "Enter Week of Month");
                    cell.ToolTipText = "Enter Week of Month";

                }
				else if (strTemp == "Pay Period")
				{
					//ToolTip1.SetToolTip(vsVacationCodes, "");
                    cell.ToolTipText = "";
				}
				else
				{
					//ToolTip1.SetToolTip(vsVacationCodes, "");
                    cell.ToolTipText = "";
				}
				// End If
			}
			else if (intMouseCol == 8)
			{
				//ToolTip1.SetToolTip(vsVacationCodes, FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intMouseRow, 4)));
                cell.ToolTipText = FCConvert.ToString(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intMouseRow, 4));

            }
			else
			{
                //ToolTip1.SetToolTip(vsVacationCodes, "");
                cell.ToolTipText = "";
			}
		}

		private void vsVacationCodes_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsVacationCodes_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolSkip = false;
				if (FCConvert.ToInt32(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, vsVacationCodes.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsVacationCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsVacationCodes.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				if (vsVacationCodes.Col == 8)
				{
					vsVacationCodes.EditMask = "####.##";
				}
				else
				{
					vsVacationCodes.EditMask = string.Empty;
				}
				// vsVacationCodes.EditCell
				// vsVacationCodes.EditSelStart = 0
				// vsVacationCodes.EditSelLength = Len(vsVacationCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVacationCodes.Row, vsVacationCodes.Col))
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsVacationCodes_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsVacationCodes.Col == 1)
			{
				for (intCounter = 1; intCounter <= (vsVacationCodes.Rows - 1); intCounter++)
				{
					if (intCounter != vsVacationCodes.Row)
					{
						if (Conversion.Val(vsVacationCodes.EditText) == Conversion.Val(vsVacationCodes.TextMatrix(intCounter, 1)))
						{
							MessageBox.Show("Code Already In Use", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							// vsVacationCodes.Select Row, Col
							e.Cancel = true;
							return;
						}
					}
				}
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsVacationCodes.Rows - 1); intRows++)
			{
				for (intCols = 2; intCols <= (vsVacationCodes.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "vsVacationCodes";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Row " + FCConvert.ToString(intRows) + " " + vsVacationCodes.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
		}
	}
}
