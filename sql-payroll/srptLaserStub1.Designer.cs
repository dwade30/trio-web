﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptLaserStub1.
	/// </summary>
	partial class srptLaserStub1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLaserStub1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtEmployeeNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentDeductions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFICA = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVacationBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSickBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEMatchCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmatchYTD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtChkAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepChkLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepSav = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositSavLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDDeds = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDDeds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployeeNo,
            this.txtName,
            this.txtCheckNo,
            this.txtPay,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.txtCurrentGross,
            this.Label10,
            this.Label11,
            this.txtCurrentDeductions,
            this.txtCurrentNet,
            this.Label18,
            this.Label19,
            this.Label20,
            this.txtCurrentFed,
            this.txtCurrentFica,
            this.txtCurrentState,
            this.txtPayDesc1,
            this.txtHours1,
            this.txtPayAmount1,
            this.txtDedDesc1,
            this.txtDedAmount1,
            this.txtDedYTD1,
            this.txtPayDesc2,
            this.txtHours2,
            this.txtPayAmount2,
            this.txtDedDesc2,
            this.txtDedAmount2,
            this.txtDedYTD2,
            this.txtPayDesc3,
            this.txtHours3,
            this.txtPayAmount3,
            this.txtDedDesc3,
            this.txtDedAmount3,
            this.txtDedYTD3,
            this.txtPayDesc4,
            this.txtHours4,
            this.txtPayAmount4,
            this.txtDedDesc4,
            this.txtDedAmount4,
            this.txtDedYTD4,
            this.txtPayDesc5,
            this.txtHours5,
            this.txtPayAmount5,
            this.txtDedDesc5,
            this.txtDedAmount5,
            this.txtDedYTD5,
            this.txtPayDesc6,
            this.txtHours6,
            this.txtPayAmount6,
            this.txtDedDesc6,
            this.txtDedAmount6,
            this.txtDedYTD6,
            this.txtPayDesc7,
            this.txtHours7,
            this.txtPayAmount7,
            this.txtDedDesc7,
            this.txtDedAmount7,
            this.txtDedYTD7,
            this.txtPayDesc8,
            this.txtHours8,
            this.txtPayAmount8,
            this.txtDedDesc8,
            this.txtDedAmount8,
            this.txtDedYTD8,
            this.txtPayDesc9,
            this.txtHours9,
            this.txtPayAmount9,
            this.txtDedDesc9,
            this.txtDedAmount9,
            this.txtDedYTD9,
            this.Label9,
            this.txtTotalHours,
            this.txtTotalAmount,
            this.Label16,
            this.txtYTDGross,
            this.txtYTDFed,
            this.txtYTDFICA,
            this.txtYTDState,
            this.txtYTDNet,
            this.Label12,
            this.Label13,
            this.Label14,
            this.txtVacationBalance,
            this.txtSickBalance,
            this.Label28,
            this.txtEMatchCurrent,
            this.txtEmatchYTD,
            this.txtDirectDepositLabel,
            this.txtDirectDeposit,
            this.txtChkAmount,
            this.lblCheckAmount,
            this.lblPayRate,
            this.txtPayRate,
            this.Label29,
            this.txtOtherBalance,
            this.txtDate,
            this.txtDirectDepChkLabel,
            this.txtDirectDepSav,
            this.txtDirectDepositSavLabel,
            this.lblDeposit1,
            this.lblDepositAmount1,
            this.lblDeposit2,
            this.lblDepositAmount2,
            this.lblDeposit3,
            this.lblDepositAmount3,
            this.lblDeposit4,
            this.lblDepositAmount4,
            this.lblDeposit7,
            this.lblDepositAmount7,
            this.lblDeposit5,
            this.lblDepositAmount5,
            this.lblDeposit6,
            this.lblDepositAmount6,
            this.lblDeposit8,
            this.lblDepositAmount8,
            this.lblDeposit9,
            this.lblDepositAmount9,
            this.lblCode1,
            this.lblCode2,
            this.lblCode1Balance,
            this.lblCode2Balance,
            this.lblCode3,
            this.lblCode3Balance,
            this.lblCheckMessage,
            this.txtYTDDeds,
            this.lblDeptDiv,
            this.txtDeptDiv,
            this.Line1});
			this.Detail.Height = 3.125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtEmployeeNo
			// 
			this.txtEmployeeNo.Height = 0.1666667F;
			this.txtEmployeeNo.HyperLink = null;
			this.txtEmployeeNo.Left = 0F;
			this.txtEmployeeNo.Name = "txtEmployeeNo";
			this.txtEmployeeNo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtEmployeeNo.Tag = "text";
			this.txtEmployeeNo.Text = "EMPLOYEE";
			this.txtEmployeeNo.Top = 0.1666667F;
			this.txtEmployeeNo.Width = 1.375F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.HyperLink = null;
			this.txtName.Left = 1.416667F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
			this.txtName.Tag = "text";
			this.txtName.Text = "EMPLOYEE";
			this.txtName.Top = 0.1666667F;
			this.txtName.Width = 2.916667F;
			// 
			// txtCheckNo
			// 
			this.txtCheckNo.Height = 0.1666667F;
			this.txtCheckNo.HyperLink = null;
			this.txtCheckNo.Left = 5.875F;
			this.txtCheckNo.Name = "txtCheckNo";
			this.txtCheckNo.Style = "font-size: 10pt; text-align: right";
			this.txtCheckNo.Tag = "text";
			this.txtCheckNo.Text = "CHECK";
			this.txtCheckNo.Top = 0.1666667F;
			this.txtCheckNo.Width = 1.375F;
			// 
			// txtPay
			// 
			this.txtPay.Height = 0.1666667F;
			this.txtPay.HyperLink = null;
			this.txtPay.Left = 0.0625F;
			this.txtPay.Name = "txtPay";
			this.txtPay.Style = "font-size: 10pt; text-align: left";
			this.txtPay.Tag = "text";
			this.txtPay.Text = "PAY-------";
			this.txtPay.Top = 0.5416667F;
			this.txtPay.Width = 0.875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.96875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 10pt; text-align: right";
			this.Label1.Tag = "text";
			this.Label1.Text = "HOURS";
			this.Label1.Top = 0.5416667F;
			this.Label1.Width = 0.59375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.59375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 10pt; text-align: right";
			this.Label2.Tag = "text";
			this.Label2.Text = "AMOUNT";
			this.Label2.Top = 0.5416667F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.4375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 10pt; text-align: left";
			this.Label3.Tag = "text";
			this.Label3.Text = "DEDUCTIONS-------";
			this.Label3.Top = 0.5416667F;
			this.Label3.Width = 1.28125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.75F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 10pt; text-align: right";
			this.Label4.Tag = "text";
			this.Label4.Text = "CURRENT";
			this.Label4.Top = 0.5416667F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.5F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 10pt; text-align: center";
			this.Label5.Tag = "text";
			this.Label5.Text = "--YTD--";
			this.Label5.Top = 0.5416667F;
			this.Label5.Width = 0.6875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.96875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 10pt; text-align: right";
			this.Label6.Tag = "text";
			this.Label6.Text = "CURRENT";
			this.Label6.Top = 0.5416667F;
			this.Label6.Width = 0.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.25F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 10pt; text-align: left";
			this.Label7.Tag = "text";
			this.Label7.Text = "GROSS";
			this.Label7.Top = 0.7083333F;
			this.Label7.Width = 0.75F;
			// 
			// txtCurrentGross
			// 
			this.txtCurrentGross.Height = 0.1666667F;
			this.txtCurrentGross.HyperLink = null;
			this.txtCurrentGross.Left = 6F;
			this.txtCurrentGross.Name = "txtCurrentGross";
			this.txtCurrentGross.Style = "font-size: 10pt; text-align: right";
			this.txtCurrentGross.Tag = "text";
			this.txtCurrentGross.Text = "0.00";
			this.txtCurrentGross.Top = 0.7083333F;
			this.txtCurrentGross.Width = 0.75F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.25F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 10pt; text-align: left";
			this.Label10.Tag = "text";
			this.Label10.Text = "DEDS.";
			this.Label10.Top = 1.375F;
			this.Label10.Width = 0.5625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 10pt; text-align: left";
			this.Label11.Tag = "text";
			this.Label11.Text = "NET";
			this.Label11.Top = 1.708333F;
			this.Label11.Width = 0.5625F;
			// 
			// txtCurrentDeductions
			// 
			this.txtCurrentDeductions.Height = 0.1666667F;
			this.txtCurrentDeductions.HyperLink = null;
			this.txtCurrentDeductions.Left = 6F;
			this.txtCurrentDeductions.Name = "txtCurrentDeductions";
			this.txtCurrentDeductions.Style = "font-size: 10pt; text-align: right";
			this.txtCurrentDeductions.Tag = "text";
			this.txtCurrentDeductions.Text = "0.00";
			this.txtCurrentDeductions.Top = 1.375F;
			this.txtCurrentDeductions.Width = 0.75F;
			// 
			// txtCurrentNet
			// 
			this.txtCurrentNet.Height = 0.1666667F;
			this.txtCurrentNet.HyperLink = null;
			this.txtCurrentNet.Left = 6F;
			this.txtCurrentNet.Name = "txtCurrentNet";
			this.txtCurrentNet.Style = "font-size: 10pt; text-align: right";
			this.txtCurrentNet.Tag = "text";
			this.txtCurrentNet.Text = "0.00";
			this.txtCurrentNet.Top = 1.708333F;
			this.txtCurrentNet.Width = 0.75F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.25F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 10pt; text-align: left";
			this.Label18.Tag = "text";
			this.Label18.Text = "FED.TAX";
			this.Label18.Top = 0.875F;
			this.Label18.Width = 0.78125F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1666667F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.25F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 10pt; text-align: left";
			this.Label19.Tag = "text";
			this.Label19.Text = "FICA/MED";
			this.Label19.Top = 1.041667F;
			this.Label19.Width = 0.8333333F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 5.25F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 10pt; text-align: left";
			this.Label20.Tag = "text";
			this.Label20.Text = "STATE";
			this.Label20.Top = 1.208333F;
			this.Label20.Width = 0.75F;
			// 
			// txtCurrentFed
			// 
			this.txtCurrentFed.Height = 0.1666667F;
			this.txtCurrentFed.HyperLink = null;
			this.txtCurrentFed.Left = 6F;
			this.txtCurrentFed.Name = "txtCurrentFed";
			this.txtCurrentFed.Style = "font-size: 10pt; text-align: right";
			this.txtCurrentFed.Tag = "text";
			this.txtCurrentFed.Text = "0.00";
			this.txtCurrentFed.Top = 0.875F;
			this.txtCurrentFed.Width = 0.75F;
			// 
			// txtCurrentFica
			// 
			this.txtCurrentFica.Height = 0.1666667F;
			this.txtCurrentFica.HyperLink = null;
			this.txtCurrentFica.Left = 6F;
			this.txtCurrentFica.Name = "txtCurrentFica";
			this.txtCurrentFica.Style = "font-size: 10pt; text-align: right";
			this.txtCurrentFica.Tag = "text";
			this.txtCurrentFica.Text = "0.00";
			this.txtCurrentFica.Top = 1.041667F;
			this.txtCurrentFica.Width = 0.75F;
			// 
			// txtCurrentState
			// 
			this.txtCurrentState.Height = 0.1666667F;
			this.txtCurrentState.HyperLink = null;
			this.txtCurrentState.Left = 6F;
			this.txtCurrentState.Name = "txtCurrentState";
			this.txtCurrentState.Style = "font-size: 10pt; text-align: right";
			this.txtCurrentState.Tag = "text";
			this.txtCurrentState.Text = "0.00";
			this.txtCurrentState.Top = 1.208333F;
			this.txtCurrentState.Width = 0.75F;
			// 
			// txtPayDesc1
			// 
			this.txtPayDesc1.Height = 0.1666667F;
			this.txtPayDesc1.HyperLink = null;
			this.txtPayDesc1.Left = 0.0625F;
			this.txtPayDesc1.Name = "txtPayDesc1";
			this.txtPayDesc1.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc1.Tag = "text";
			this.txtPayDesc1.Text = null;
			this.txtPayDesc1.Top = 0.7083333F;
			this.txtPayDesc1.Width = 0.875F;
			// 
			// txtHours1
			// 
			this.txtHours1.Height = 0.1666667F;
			this.txtHours1.HyperLink = null;
			this.txtHours1.Left = 0.96875F;
			this.txtHours1.Name = "txtHours1";
			this.txtHours1.Style = "font-size: 10pt; text-align: right";
			this.txtHours1.Tag = "text";
			this.txtHours1.Text = null;
			this.txtHours1.Top = 0.7083333F;
			this.txtHours1.Width = 0.59375F;
			// 
			// txtPayAmount1
			// 
			this.txtPayAmount1.Height = 0.1666667F;
			this.txtPayAmount1.HyperLink = null;
			this.txtPayAmount1.Left = 1.59375F;
			this.txtPayAmount1.Name = "txtPayAmount1";
			this.txtPayAmount1.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount1.Tag = "text";
			this.txtPayAmount1.Text = null;
			this.txtPayAmount1.Top = 0.7083333F;
			this.txtPayAmount1.Width = 0.6875F;
			// 
			// txtDedDesc1
			// 
			this.txtDedDesc1.Height = 0.1666667F;
			this.txtDedDesc1.HyperLink = null;
			this.txtDedDesc1.Left = 2.4375F;
			this.txtDedDesc1.Name = "txtDedDesc1";
			this.txtDedDesc1.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc1.Tag = "text";
			this.txtDedDesc1.Text = null;
			this.txtDedDesc1.Top = 0.7083333F;
			this.txtDedDesc1.Width = 1.28125F;
			// 
			// txtDedAmount1
			// 
			this.txtDedAmount1.Height = 0.1666667F;
			this.txtDedAmount1.HyperLink = null;
			this.txtDedAmount1.Left = 3.75F;
			this.txtDedAmount1.Name = "txtDedAmount1";
			this.txtDedAmount1.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount1.Tag = "text";
			this.txtDedAmount1.Text = null;
			this.txtDedAmount1.Top = 0.7083333F;
			this.txtDedAmount1.Width = 0.75F;
			// 
			// txtDedYTD1
			// 
			this.txtDedYTD1.Height = 0.1666667F;
			this.txtDedYTD1.HyperLink = null;
			this.txtDedYTD1.Left = 4.5F;
			this.txtDedYTD1.Name = "txtDedYTD1";
			this.txtDedYTD1.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD1.Tag = "text";
			this.txtDedYTD1.Text = null;
			this.txtDedYTD1.Top = 0.7083333F;
			this.txtDedYTD1.Width = 0.6875F;
			// 
			// txtPayDesc2
			// 
			this.txtPayDesc2.Height = 0.1666667F;
			this.txtPayDesc2.HyperLink = null;
			this.txtPayDesc2.Left = 0.0625F;
			this.txtPayDesc2.Name = "txtPayDesc2";
			this.txtPayDesc2.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc2.Tag = "text";
			this.txtPayDesc2.Text = null;
			this.txtPayDesc2.Top = 0.875F;
			this.txtPayDesc2.Width = 0.875F;
			// 
			// txtHours2
			// 
			this.txtHours2.Height = 0.1666667F;
			this.txtHours2.HyperLink = null;
			this.txtHours2.Left = 0.96875F;
			this.txtHours2.Name = "txtHours2";
			this.txtHours2.Style = "font-size: 10pt; text-align: right";
			this.txtHours2.Tag = "text";
			this.txtHours2.Text = null;
			this.txtHours2.Top = 0.875F;
			this.txtHours2.Width = 0.59375F;
			// 
			// txtPayAmount2
			// 
			this.txtPayAmount2.Height = 0.1666667F;
			this.txtPayAmount2.HyperLink = null;
			this.txtPayAmount2.Left = 1.59375F;
			this.txtPayAmount2.Name = "txtPayAmount2";
			this.txtPayAmount2.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount2.Tag = "text";
			this.txtPayAmount2.Text = null;
			this.txtPayAmount2.Top = 0.875F;
			this.txtPayAmount2.Width = 0.6875F;
			// 
			// txtDedDesc2
			// 
			this.txtDedDesc2.Height = 0.1666667F;
			this.txtDedDesc2.HyperLink = null;
			this.txtDedDesc2.Left = 2.4375F;
			this.txtDedDesc2.Name = "txtDedDesc2";
			this.txtDedDesc2.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc2.Tag = "text";
			this.txtDedDesc2.Text = null;
			this.txtDedDesc2.Top = 0.875F;
			this.txtDedDesc2.Width = 1.28125F;
			// 
			// txtDedAmount2
			// 
			this.txtDedAmount2.Height = 0.1666667F;
			this.txtDedAmount2.HyperLink = null;
			this.txtDedAmount2.Left = 3.75F;
			this.txtDedAmount2.Name = "txtDedAmount2";
			this.txtDedAmount2.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount2.Tag = "text";
			this.txtDedAmount2.Text = null;
			this.txtDedAmount2.Top = 0.875F;
			this.txtDedAmount2.Width = 0.75F;
			// 
			// txtDedYTD2
			// 
			this.txtDedYTD2.Height = 0.1666667F;
			this.txtDedYTD2.HyperLink = null;
			this.txtDedYTD2.Left = 4.5F;
			this.txtDedYTD2.Name = "txtDedYTD2";
			this.txtDedYTD2.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD2.Tag = "text";
			this.txtDedYTD2.Text = null;
			this.txtDedYTD2.Top = 0.875F;
			this.txtDedYTD2.Width = 0.6875F;
			// 
			// txtPayDesc3
			// 
			this.txtPayDesc3.Height = 0.1666667F;
			this.txtPayDesc3.HyperLink = null;
			this.txtPayDesc3.Left = 0.0625F;
			this.txtPayDesc3.Name = "txtPayDesc3";
			this.txtPayDesc3.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc3.Tag = "text";
			this.txtPayDesc3.Text = null;
			this.txtPayDesc3.Top = 1.041667F;
			this.txtPayDesc3.Width = 0.875F;
			// 
			// txtHours3
			// 
			this.txtHours3.Height = 0.1666667F;
			this.txtHours3.HyperLink = null;
			this.txtHours3.Left = 0.96875F;
			this.txtHours3.Name = "txtHours3";
			this.txtHours3.Style = "font-size: 10pt; text-align: right";
			this.txtHours3.Tag = "text";
			this.txtHours3.Text = null;
			this.txtHours3.Top = 1.041667F;
			this.txtHours3.Width = 0.59375F;
			// 
			// txtPayAmount3
			// 
			this.txtPayAmount3.Height = 0.1666667F;
			this.txtPayAmount3.HyperLink = null;
			this.txtPayAmount3.Left = 1.59375F;
			this.txtPayAmount3.Name = "txtPayAmount3";
			this.txtPayAmount3.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount3.Tag = "text";
			this.txtPayAmount3.Text = null;
			this.txtPayAmount3.Top = 1.041667F;
			this.txtPayAmount3.Width = 0.6875F;
			// 
			// txtDedDesc3
			// 
			this.txtDedDesc3.Height = 0.1666667F;
			this.txtDedDesc3.HyperLink = null;
			this.txtDedDesc3.Left = 2.4375F;
			this.txtDedDesc3.Name = "txtDedDesc3";
			this.txtDedDesc3.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc3.Tag = "text";
			this.txtDedDesc3.Text = null;
			this.txtDedDesc3.Top = 1.041667F;
			this.txtDedDesc3.Width = 1.28125F;
			// 
			// txtDedAmount3
			// 
			this.txtDedAmount3.Height = 0.1666667F;
			this.txtDedAmount3.HyperLink = null;
			this.txtDedAmount3.Left = 3.75F;
			this.txtDedAmount3.Name = "txtDedAmount3";
			this.txtDedAmount3.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount3.Tag = "text";
			this.txtDedAmount3.Text = null;
			this.txtDedAmount3.Top = 1.041667F;
			this.txtDedAmount3.Width = 0.75F;
			// 
			// txtDedYTD3
			// 
			this.txtDedYTD3.Height = 0.1666667F;
			this.txtDedYTD3.HyperLink = null;
			this.txtDedYTD3.Left = 4.5F;
			this.txtDedYTD3.Name = "txtDedYTD3";
			this.txtDedYTD3.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD3.Tag = "text";
			this.txtDedYTD3.Text = null;
			this.txtDedYTD3.Top = 1.041667F;
			this.txtDedYTD3.Width = 0.6875F;
			// 
			// txtPayDesc4
			// 
			this.txtPayDesc4.Height = 0.1666667F;
			this.txtPayDesc4.HyperLink = null;
			this.txtPayDesc4.Left = 0.0625F;
			this.txtPayDesc4.Name = "txtPayDesc4";
			this.txtPayDesc4.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc4.Tag = "text";
			this.txtPayDesc4.Text = null;
			this.txtPayDesc4.Top = 1.208333F;
			this.txtPayDesc4.Width = 0.875F;
			// 
			// txtHours4
			// 
			this.txtHours4.Height = 0.1666667F;
			this.txtHours4.HyperLink = null;
			this.txtHours4.Left = 0.96875F;
			this.txtHours4.Name = "txtHours4";
			this.txtHours4.Style = "font-size: 10pt; text-align: right";
			this.txtHours4.Tag = "text";
			this.txtHours4.Text = null;
			this.txtHours4.Top = 1.208333F;
			this.txtHours4.Width = 0.59375F;
			// 
			// txtPayAmount4
			// 
			this.txtPayAmount4.Height = 0.1666667F;
			this.txtPayAmount4.HyperLink = null;
			this.txtPayAmount4.Left = 1.59375F;
			this.txtPayAmount4.Name = "txtPayAmount4";
			this.txtPayAmount4.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount4.Tag = "text";
			this.txtPayAmount4.Text = null;
			this.txtPayAmount4.Top = 1.208333F;
			this.txtPayAmount4.Width = 0.6875F;
			// 
			// txtDedDesc4
			// 
			this.txtDedDesc4.Height = 0.1666667F;
			this.txtDedDesc4.HyperLink = null;
			this.txtDedDesc4.Left = 2.4375F;
			this.txtDedDesc4.Name = "txtDedDesc4";
			this.txtDedDesc4.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc4.Tag = "text";
			this.txtDedDesc4.Text = null;
			this.txtDedDesc4.Top = 1.208333F;
			this.txtDedDesc4.Width = 1.28125F;
			// 
			// txtDedAmount4
			// 
			this.txtDedAmount4.Height = 0.1666667F;
			this.txtDedAmount4.HyperLink = null;
			this.txtDedAmount4.Left = 3.75F;
			this.txtDedAmount4.Name = "txtDedAmount4";
			this.txtDedAmount4.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount4.Tag = "text";
			this.txtDedAmount4.Text = null;
			this.txtDedAmount4.Top = 1.208333F;
			this.txtDedAmount4.Width = 0.75F;
			// 
			// txtDedYTD4
			// 
			this.txtDedYTD4.Height = 0.1666667F;
			this.txtDedYTD4.HyperLink = null;
			this.txtDedYTD4.Left = 4.5F;
			this.txtDedYTD4.Name = "txtDedYTD4";
			this.txtDedYTD4.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD4.Tag = "text";
			this.txtDedYTD4.Text = null;
			this.txtDedYTD4.Top = 1.208333F;
			this.txtDedYTD4.Width = 0.6875F;
			// 
			// txtPayDesc5
			// 
			this.txtPayDesc5.Height = 0.1666667F;
			this.txtPayDesc5.HyperLink = null;
			this.txtPayDesc5.Left = 0.0625F;
			this.txtPayDesc5.Name = "txtPayDesc5";
			this.txtPayDesc5.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc5.Tag = "text";
			this.txtPayDesc5.Text = null;
			this.txtPayDesc5.Top = 1.375F;
			this.txtPayDesc5.Width = 0.875F;
			// 
			// txtHours5
			// 
			this.txtHours5.Height = 0.1666667F;
			this.txtHours5.HyperLink = null;
			this.txtHours5.Left = 0.96875F;
			this.txtHours5.Name = "txtHours5";
			this.txtHours5.Style = "font-size: 10pt; text-align: right";
			this.txtHours5.Tag = "text";
			this.txtHours5.Text = null;
			this.txtHours5.Top = 1.375F;
			this.txtHours5.Width = 0.59375F;
			// 
			// txtPayAmount5
			// 
			this.txtPayAmount5.Height = 0.1666667F;
			this.txtPayAmount5.HyperLink = null;
			this.txtPayAmount5.Left = 1.59375F;
			this.txtPayAmount5.Name = "txtPayAmount5";
			this.txtPayAmount5.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount5.Tag = "text";
			this.txtPayAmount5.Text = null;
			this.txtPayAmount5.Top = 1.375F;
			this.txtPayAmount5.Width = 0.6875F;
			// 
			// txtDedDesc5
			// 
			this.txtDedDesc5.Height = 0.1666667F;
			this.txtDedDesc5.HyperLink = null;
			this.txtDedDesc5.Left = 2.4375F;
			this.txtDedDesc5.Name = "txtDedDesc5";
			this.txtDedDesc5.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc5.Tag = "text";
			this.txtDedDesc5.Text = null;
			this.txtDedDesc5.Top = 1.375F;
			this.txtDedDesc5.Width = 1.28125F;
			// 
			// txtDedAmount5
			// 
			this.txtDedAmount5.Height = 0.1666667F;
			this.txtDedAmount5.HyperLink = null;
			this.txtDedAmount5.Left = 3.75F;
			this.txtDedAmount5.Name = "txtDedAmount5";
			this.txtDedAmount5.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount5.Tag = "text";
			this.txtDedAmount5.Text = null;
			this.txtDedAmount5.Top = 1.375F;
			this.txtDedAmount5.Width = 0.75F;
			// 
			// txtDedYTD5
			// 
			this.txtDedYTD5.Height = 0.1666667F;
			this.txtDedYTD5.HyperLink = null;
			this.txtDedYTD5.Left = 4.5F;
			this.txtDedYTD5.Name = "txtDedYTD5";
			this.txtDedYTD5.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD5.Tag = "text";
			this.txtDedYTD5.Text = null;
			this.txtDedYTD5.Top = 1.375F;
			this.txtDedYTD5.Width = 0.6875F;
			// 
			// txtPayDesc6
			// 
			this.txtPayDesc6.Height = 0.1666667F;
			this.txtPayDesc6.HyperLink = null;
			this.txtPayDesc6.Left = 0.0625F;
			this.txtPayDesc6.Name = "txtPayDesc6";
			this.txtPayDesc6.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc6.Tag = "text";
			this.txtPayDesc6.Text = null;
			this.txtPayDesc6.Top = 1.541667F;
			this.txtPayDesc6.Width = 0.875F;
			// 
			// txtHours6
			// 
			this.txtHours6.Height = 0.1666667F;
			this.txtHours6.HyperLink = null;
			this.txtHours6.Left = 0.96875F;
			this.txtHours6.Name = "txtHours6";
			this.txtHours6.Style = "font-size: 10pt; text-align: right";
			this.txtHours6.Tag = "text";
			this.txtHours6.Text = null;
			this.txtHours6.Top = 1.541667F;
			this.txtHours6.Width = 0.59375F;
			// 
			// txtPayAmount6
			// 
			this.txtPayAmount6.Height = 0.1666667F;
			this.txtPayAmount6.HyperLink = null;
			this.txtPayAmount6.Left = 1.59375F;
			this.txtPayAmount6.Name = "txtPayAmount6";
			this.txtPayAmount6.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount6.Tag = "text";
			this.txtPayAmount6.Text = null;
			this.txtPayAmount6.Top = 1.541667F;
			this.txtPayAmount6.Width = 0.6875F;
			// 
			// txtDedDesc6
			// 
			this.txtDedDesc6.Height = 0.1666667F;
			this.txtDedDesc6.HyperLink = null;
			this.txtDedDesc6.Left = 2.4375F;
			this.txtDedDesc6.Name = "txtDedDesc6";
			this.txtDedDesc6.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc6.Tag = "text";
			this.txtDedDesc6.Text = null;
			this.txtDedDesc6.Top = 1.541667F;
			this.txtDedDesc6.Width = 1.28125F;
			// 
			// txtDedAmount6
			// 
			this.txtDedAmount6.Height = 0.1666667F;
			this.txtDedAmount6.HyperLink = null;
			this.txtDedAmount6.Left = 3.75F;
			this.txtDedAmount6.Name = "txtDedAmount6";
			this.txtDedAmount6.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount6.Tag = "text";
			this.txtDedAmount6.Text = null;
			this.txtDedAmount6.Top = 1.541667F;
			this.txtDedAmount6.Width = 0.75F;
			// 
			// txtDedYTD6
			// 
			this.txtDedYTD6.Height = 0.1666667F;
			this.txtDedYTD6.HyperLink = null;
			this.txtDedYTD6.Left = 4.5F;
			this.txtDedYTD6.Name = "txtDedYTD6";
			this.txtDedYTD6.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD6.Tag = "text";
			this.txtDedYTD6.Text = null;
			this.txtDedYTD6.Top = 1.541667F;
			this.txtDedYTD6.Width = 0.6875F;
			// 
			// txtPayDesc7
			// 
			this.txtPayDesc7.Height = 0.1666667F;
			this.txtPayDesc7.HyperLink = null;
			this.txtPayDesc7.Left = 0.0625F;
			this.txtPayDesc7.Name = "txtPayDesc7";
			this.txtPayDesc7.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc7.Tag = "text";
			this.txtPayDesc7.Text = null;
			this.txtPayDesc7.Top = 1.708333F;
			this.txtPayDesc7.Width = 0.875F;
			// 
			// txtHours7
			// 
			this.txtHours7.Height = 0.1666667F;
			this.txtHours7.HyperLink = null;
			this.txtHours7.Left = 0.96875F;
			this.txtHours7.Name = "txtHours7";
			this.txtHours7.Style = "font-size: 10pt; text-align: right";
			this.txtHours7.Tag = "text";
			this.txtHours7.Text = null;
			this.txtHours7.Top = 1.708333F;
			this.txtHours7.Width = 0.59375F;
			// 
			// txtPayAmount7
			// 
			this.txtPayAmount7.Height = 0.1666667F;
			this.txtPayAmount7.HyperLink = null;
			this.txtPayAmount7.Left = 1.59375F;
			this.txtPayAmount7.Name = "txtPayAmount7";
			this.txtPayAmount7.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount7.Tag = "text";
			this.txtPayAmount7.Text = null;
			this.txtPayAmount7.Top = 1.708333F;
			this.txtPayAmount7.Width = 0.6875F;
			// 
			// txtDedDesc7
			// 
			this.txtDedDesc7.Height = 0.1666667F;
			this.txtDedDesc7.HyperLink = null;
			this.txtDedDesc7.Left = 2.4375F;
			this.txtDedDesc7.Name = "txtDedDesc7";
			this.txtDedDesc7.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc7.Tag = "text";
			this.txtDedDesc7.Text = null;
			this.txtDedDesc7.Top = 1.708333F;
			this.txtDedDesc7.Width = 1.28125F;
			// 
			// txtDedAmount7
			// 
			this.txtDedAmount7.Height = 0.1666667F;
			this.txtDedAmount7.HyperLink = null;
			this.txtDedAmount7.Left = 3.75F;
			this.txtDedAmount7.Name = "txtDedAmount7";
			this.txtDedAmount7.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount7.Tag = "text";
			this.txtDedAmount7.Text = null;
			this.txtDedAmount7.Top = 1.708333F;
			this.txtDedAmount7.Width = 0.75F;
			// 
			// txtDedYTD7
			// 
			this.txtDedYTD7.Height = 0.1666667F;
			this.txtDedYTD7.HyperLink = null;
			this.txtDedYTD7.Left = 4.5F;
			this.txtDedYTD7.Name = "txtDedYTD7";
			this.txtDedYTD7.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD7.Tag = "text";
			this.txtDedYTD7.Text = null;
			this.txtDedYTD7.Top = 1.708333F;
			this.txtDedYTD7.Width = 0.6875F;
			// 
			// txtPayDesc8
			// 
			this.txtPayDesc8.Height = 0.1666667F;
			this.txtPayDesc8.HyperLink = null;
			this.txtPayDesc8.Left = 0.0625F;
			this.txtPayDesc8.Name = "txtPayDesc8";
			this.txtPayDesc8.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc8.Tag = "text";
			this.txtPayDesc8.Text = null;
			this.txtPayDesc8.Top = 1.875F;
			this.txtPayDesc8.Width = 0.875F;
			// 
			// txtHours8
			// 
			this.txtHours8.Height = 0.1666667F;
			this.txtHours8.HyperLink = null;
			this.txtHours8.Left = 0.96875F;
			this.txtHours8.Name = "txtHours8";
			this.txtHours8.Style = "font-size: 10pt; text-align: right";
			this.txtHours8.Tag = "text";
			this.txtHours8.Text = null;
			this.txtHours8.Top = 1.875F;
			this.txtHours8.Width = 0.59375F;
			// 
			// txtPayAmount8
			// 
			this.txtPayAmount8.Height = 0.1666667F;
			this.txtPayAmount8.HyperLink = null;
			this.txtPayAmount8.Left = 1.59375F;
			this.txtPayAmount8.Name = "txtPayAmount8";
			this.txtPayAmount8.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount8.Tag = "text";
			this.txtPayAmount8.Text = null;
			this.txtPayAmount8.Top = 1.875F;
			this.txtPayAmount8.Width = 0.6875F;
			// 
			// txtDedDesc8
			// 
			this.txtDedDesc8.Height = 0.1666667F;
			this.txtDedDesc8.HyperLink = null;
			this.txtDedDesc8.Left = 2.4375F;
			this.txtDedDesc8.Name = "txtDedDesc8";
			this.txtDedDesc8.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc8.Tag = "text";
			this.txtDedDesc8.Text = null;
			this.txtDedDesc8.Top = 1.875F;
			this.txtDedDesc8.Width = 1.28125F;
			// 
			// txtDedAmount8
			// 
			this.txtDedAmount8.Height = 0.1666667F;
			this.txtDedAmount8.HyperLink = null;
			this.txtDedAmount8.Left = 3.75F;
			this.txtDedAmount8.Name = "txtDedAmount8";
			this.txtDedAmount8.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount8.Tag = "text";
			this.txtDedAmount8.Text = null;
			this.txtDedAmount8.Top = 1.875F;
			this.txtDedAmount8.Width = 0.75F;
			// 
			// txtDedYTD8
			// 
			this.txtDedYTD8.Height = 0.1666667F;
			this.txtDedYTD8.HyperLink = null;
			this.txtDedYTD8.Left = 4.5F;
			this.txtDedYTD8.Name = "txtDedYTD8";
			this.txtDedYTD8.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD8.Tag = "text";
			this.txtDedYTD8.Text = null;
			this.txtDedYTD8.Top = 1.875F;
			this.txtDedYTD8.Width = 0.6875F;
			// 
			// txtPayDesc9
			// 
			this.txtPayDesc9.Height = 0.1666667F;
			this.txtPayDesc9.HyperLink = null;
			this.txtPayDesc9.Left = 0.0625F;
			this.txtPayDesc9.Name = "txtPayDesc9";
			this.txtPayDesc9.Style = "font-size: 10pt; text-align: left";
			this.txtPayDesc9.Tag = "text";
			this.txtPayDesc9.Text = null;
			this.txtPayDesc9.Top = 2.041667F;
			this.txtPayDesc9.Width = 0.875F;
			// 
			// txtHours9
			// 
			this.txtHours9.Height = 0.1666667F;
			this.txtHours9.HyperLink = null;
			this.txtHours9.Left = 0.96875F;
			this.txtHours9.Name = "txtHours9";
			this.txtHours9.Style = "font-size: 10pt; text-align: right";
			this.txtHours9.Tag = "text";
			this.txtHours9.Text = null;
			this.txtHours9.Top = 2.041667F;
			this.txtHours9.Width = 0.59375F;
			// 
			// txtPayAmount9
			// 
			this.txtPayAmount9.Height = 0.1666667F;
			this.txtPayAmount9.HyperLink = null;
			this.txtPayAmount9.Left = 1.59375F;
			this.txtPayAmount9.Name = "txtPayAmount9";
			this.txtPayAmount9.Style = "font-size: 10pt; text-align: right";
			this.txtPayAmount9.Tag = "text";
			this.txtPayAmount9.Text = null;
			this.txtPayAmount9.Top = 2.041667F;
			this.txtPayAmount9.Width = 0.6875F;
			// 
			// txtDedDesc9
			// 
			this.txtDedDesc9.Height = 0.1666667F;
			this.txtDedDesc9.HyperLink = null;
			this.txtDedDesc9.Left = 2.4375F;
			this.txtDedDesc9.Name = "txtDedDesc9";
			this.txtDedDesc9.Style = "font-size: 10pt; text-align: left";
			this.txtDedDesc9.Tag = "text";
			this.txtDedDesc9.Text = null;
			this.txtDedDesc9.Top = 2.041667F;
			this.txtDedDesc9.Width = 1.28125F;
			// 
			// txtDedAmount9
			// 
			this.txtDedAmount9.Height = 0.1666667F;
			this.txtDedAmount9.HyperLink = null;
			this.txtDedAmount9.Left = 3.75F;
			this.txtDedAmount9.Name = "txtDedAmount9";
			this.txtDedAmount9.Style = "font-size: 10pt; text-align: right";
			this.txtDedAmount9.Tag = "text";
			this.txtDedAmount9.Text = null;
			this.txtDedAmount9.Top = 2.041667F;
			this.txtDedAmount9.Width = 0.75F;
			// 
			// txtDedYTD9
			// 
			this.txtDedYTD9.Height = 0.1666667F;
			this.txtDedYTD9.HyperLink = null;
			this.txtDedYTD9.Left = 4.5F;
			this.txtDedYTD9.Name = "txtDedYTD9";
			this.txtDedYTD9.Style = "font-size: 10pt; text-align: right";
			this.txtDedYTD9.Tag = "text";
			this.txtDedYTD9.Text = null;
			this.txtDedYTD9.Top = 2.041667F;
			this.txtDedYTD9.Width = 0.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 10pt; text-align: left";
			this.Label9.Tag = "text";
			this.Label9.Text = "TOTAL";
			this.Label9.Top = 2.208333F;
			this.Label9.Width = 0.5F;
			// 
			// txtTotalHours
			// 
			this.txtTotalHours.Height = 0.1666667F;
			this.txtTotalHours.HyperLink = null;
			this.txtTotalHours.Left = 0.9375F;
			this.txtTotalHours.Name = "txtTotalHours";
			this.txtTotalHours.Style = "font-size: 10pt; text-align: right";
			this.txtTotalHours.Tag = "text";
			this.txtTotalHours.Text = "0.00";
			this.txtTotalHours.Top = 2.208333F;
			this.txtTotalHours.Width = 0.625F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.HyperLink = null;
			this.txtTotalAmount.Left = 1.59375F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "font-size: 10pt; text-align: right";
			this.txtTotalAmount.Tag = "text";
			this.txtTotalAmount.Text = "0.00";
			this.txtTotalAmount.Top = 2.208333F;
			this.txtTotalAmount.Width = 0.6875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 6.8125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 10pt; text-align: right";
			this.Label16.Tag = "text";
			this.Label16.Text = "--YTD--";
			this.Label16.Top = 0.5416667F;
			this.Label16.Width = 0.8125F;
			// 
			// txtYTDGross
			// 
			this.txtYTDGross.Height = 0.1666667F;
			this.txtYTDGross.HyperLink = null;
			this.txtYTDGross.Left = 6.8125F;
			this.txtYTDGross.Name = "txtYTDGross";
			this.txtYTDGross.Style = "font-size: 10pt; text-align: right";
			this.txtYTDGross.Tag = "text";
			this.txtYTDGross.Text = "0.00";
			this.txtYTDGross.Top = 0.7083333F;
			this.txtYTDGross.Width = 0.8125F;
			// 
			// txtYTDFed
			// 
			this.txtYTDFed.Height = 0.1666667F;
			this.txtYTDFed.HyperLink = null;
			this.txtYTDFed.Left = 6.8125F;
			this.txtYTDFed.Name = "txtYTDFed";
			this.txtYTDFed.Style = "font-size: 10pt; text-align: right";
			this.txtYTDFed.Tag = "text";
			this.txtYTDFed.Text = "0.00";
			this.txtYTDFed.Top = 0.875F;
			this.txtYTDFed.Width = 0.8125F;
			// 
			// txtYTDFICA
			// 
			this.txtYTDFICA.Height = 0.1666667F;
			this.txtYTDFICA.HyperLink = null;
			this.txtYTDFICA.Left = 6.8125F;
			this.txtYTDFICA.Name = "txtYTDFICA";
			this.txtYTDFICA.Style = "font-size: 10pt; text-align: right";
			this.txtYTDFICA.Tag = "text";
			this.txtYTDFICA.Text = "0.00";
			this.txtYTDFICA.Top = 1.041667F;
			this.txtYTDFICA.Width = 0.8125F;
			// 
			// txtYTDState
			// 
			this.txtYTDState.Height = 0.1666667F;
			this.txtYTDState.HyperLink = null;
			this.txtYTDState.Left = 6.8125F;
			this.txtYTDState.Name = "txtYTDState";
			this.txtYTDState.Style = "font-size: 10pt; text-align: right";
			this.txtYTDState.Tag = "text";
			this.txtYTDState.Text = "0.00";
			this.txtYTDState.Top = 1.208333F;
			this.txtYTDState.Width = 0.8125F;
			// 
			// txtYTDNet
			// 
			this.txtYTDNet.Height = 0.1666667F;
			this.txtYTDNet.HyperLink = null;
			this.txtYTDNet.Left = 6.8125F;
			this.txtYTDNet.Name = "txtYTDNet";
			this.txtYTDNet.Style = "font-size: 10pt; text-align: right";
			this.txtYTDNet.Tag = "text";
			this.txtYTDNet.Text = "0.00";
			this.txtYTDNet.Top = 1.708333F;
			this.txtYTDNet.Width = 0.8125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.46875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 10pt; text-align: center";
			this.Label12.Tag = "text";
			this.Label12.Text = "BALANCE";
			this.Label12.Top = 1.916667F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.28125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 10pt; text-align: left";
			this.Label13.Tag = "text";
			this.Label13.Text = "VAC.";
			this.Label13.Top = 2.083333F;
			this.Label13.Width = 0.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.28125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 10pt; text-align: left";
			this.Label14.Tag = "text";
			this.Label14.Text = "SICK";
			this.Label14.Top = 2.25F;
			this.Label14.Width = 0.4375F;
			// 
			// txtVacationBalance
			// 
			this.txtVacationBalance.Height = 0.1666667F;
			this.txtVacationBalance.HyperLink = null;
			this.txtVacationBalance.Left = 5.78125F;
			this.txtVacationBalance.Name = "txtVacationBalance";
			this.txtVacationBalance.Style = "font-size: 10pt; text-align: right";
			this.txtVacationBalance.Tag = "text";
			this.txtVacationBalance.Text = "0.00";
			this.txtVacationBalance.Top = 2.083333F;
			this.txtVacationBalance.Width = 0.625F;
			// 
			// txtSickBalance
			// 
			this.txtSickBalance.Height = 0.1666667F;
			this.txtSickBalance.HyperLink = null;
			this.txtSickBalance.Left = 5.78125F;
			this.txtSickBalance.Name = "txtSickBalance";
			this.txtSickBalance.Style = "font-size: 10pt; text-align: right";
			this.txtSickBalance.Tag = "text";
			this.txtSickBalance.Text = "0.00";
			this.txtSickBalance.Top = 2.25F;
			this.txtSickBalance.Width = 0.625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1666667F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 2.4375F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 10pt; text-align: left";
			this.Label28.Tag = "text";
			this.Label28.Text = "E/MATCH";
			this.Label28.Top = 2.208333F;
			this.Label28.Width = 0.9375F;
			// 
			// txtEMatchCurrent
			// 
			this.txtEMatchCurrent.Height = 0.1666667F;
			this.txtEMatchCurrent.HyperLink = null;
			this.txtEMatchCurrent.Left = 3.75F;
			this.txtEMatchCurrent.Name = "txtEMatchCurrent";
			this.txtEMatchCurrent.Style = "font-size: 10pt; text-align: right";
			this.txtEMatchCurrent.Tag = "text";
			this.txtEMatchCurrent.Text = "0.00";
			this.txtEMatchCurrent.Top = 2.208333F;
			this.txtEMatchCurrent.Width = 0.75F;
			// 
			// txtEmatchYTD
			// 
			this.txtEmatchYTD.Height = 0.1666667F;
			this.txtEmatchYTD.HyperLink = null;
			this.txtEmatchYTD.Left = 4.5F;
			this.txtEmatchYTD.Name = "txtEmatchYTD";
			this.txtEmatchYTD.Style = "font-size: 10pt; text-align: right";
			this.txtEmatchYTD.Tag = "text";
			this.txtEmatchYTD.Text = "0.00";
			this.txtEmatchYTD.Top = 2.208333F;
			this.txtEmatchYTD.Width = 0.6875F;
			// 
			// txtDirectDepositLabel
			// 
			this.txtDirectDepositLabel.Height = 0.1666667F;
			this.txtDirectDepositLabel.HyperLink = null;
			this.txtDirectDepositLabel.Left = 0.0625F;
			this.txtDirectDepositLabel.Name = "txtDirectDepositLabel";
			this.txtDirectDepositLabel.Style = "font-size: 10pt; text-align: right";
			this.txtDirectDepositLabel.Tag = "text";
			this.txtDirectDepositLabel.Text = "DIRECT DEPOSIT";
			this.txtDirectDepositLabel.Top = 2.458333F;
			this.txtDirectDepositLabel.Visible = false;
			this.txtDirectDepositLabel.Width = 1.4375F;
			// 
			// txtDirectDeposit
			// 
			this.txtDirectDeposit.Height = 0.1666667F;
			this.txtDirectDeposit.HyperLink = null;
			this.txtDirectDeposit.Left = 1F;
			this.txtDirectDeposit.Name = "txtDirectDeposit";
			this.txtDirectDeposit.Style = "font-size: 10pt; text-align: right";
			this.txtDirectDeposit.Tag = "text";
			this.txtDirectDeposit.Text = "0.00";
			this.txtDirectDeposit.Top = 2.625F;
			this.txtDirectDeposit.Visible = false;
			this.txtDirectDeposit.Width = 0.6875F;
			// 
			// txtChkAmount
			// 
			this.txtChkAmount.Height = 0.1666667F;
			this.txtChkAmount.HyperLink = null;
			this.txtChkAmount.Left = 1F;
			this.txtChkAmount.Name = "txtChkAmount";
			this.txtChkAmount.Style = "font-size: 10pt; text-align: right";
			this.txtChkAmount.Tag = "text";
			this.txtChkAmount.Text = "0.00";
			this.txtChkAmount.Top = 2.958333F;
			this.txtChkAmount.Width = 0.6875F;
			// 
			// lblCheckAmount
			// 
			this.lblCheckAmount.Height = 0.1666667F;
			this.lblCheckAmount.HyperLink = null;
			this.lblCheckAmount.Left = 0.0625F;
			this.lblCheckAmount.Name = "lblCheckAmount";
			this.lblCheckAmount.Style = "font-size: 10pt; text-align: left";
			this.lblCheckAmount.Tag = "text";
			this.lblCheckAmount.Text = "CHK AMOUNT";
			this.lblCheckAmount.Top = 2.958333F;
			this.lblCheckAmount.Width = 0.9375F;
			// 
			// lblPayRate
			// 
			this.lblPayRate.Height = 0.1666667F;
			this.lblPayRate.HyperLink = null;
			this.lblPayRate.Left = 0.375F;
			this.lblPayRate.Name = "lblPayRate";
			this.lblPayRate.Style = "font-size: 10pt; text-align: left";
			this.lblPayRate.Tag = "text";
			this.lblPayRate.Text = "PAY RATE:";
			this.lblPayRate.Top = 0.3333333F;
			this.lblPayRate.Width = 0.875F;
			// 
			// txtPayRate
			// 
			this.txtPayRate.Height = 0.1666667F;
			this.txtPayRate.HyperLink = null;
			this.txtPayRate.Left = 1.3125F;
			this.txtPayRate.Name = "txtPayRate";
			this.txtPayRate.Style = "font-size: 10pt; text-align: right";
			this.txtPayRate.Tag = "text";
			this.txtPayRate.Text = null;
			this.txtPayRate.Top = 0.3333333F;
			this.txtPayRate.Width = 0.5625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1666667F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.28125F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 10pt; text-align: left";
			this.Label29.Tag = "text";
			this.Label29.Text = "OTHER";
			this.Label29.Top = 2.416667F;
			this.Label29.Width = 0.5F;
			// 
			// txtOtherBalance
			// 
			this.txtOtherBalance.Height = 0.1666667F;
			this.txtOtherBalance.HyperLink = null;
			this.txtOtherBalance.Left = 5.78125F;
			this.txtOtherBalance.Name = "txtOtherBalance";
			this.txtOtherBalance.Style = "font-size: 10pt; text-align: right";
			this.txtOtherBalance.Tag = "text";
			this.txtOtherBalance.Text = "0.00";
			this.txtOtherBalance.Top = 2.416667F;
			this.txtOtherBalance.Width = 0.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 4.3125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 10pt; text-align: right";
			this.txtDate.Tag = "text";
			this.txtDate.Text = "DATE";
			this.txtDate.Top = 0.1666667F;
			this.txtDate.Width = 1.5625F;
			// 
			// txtDirectDepChkLabel
			// 
			this.txtDirectDepChkLabel.Height = 0.1666667F;
			this.txtDirectDepChkLabel.HyperLink = null;
			this.txtDirectDepChkLabel.Left = 0.0625F;
			this.txtDirectDepChkLabel.Name = "txtDirectDepChkLabel";
			this.txtDirectDepChkLabel.Style = "font-size: 10pt; text-align: left";
			this.txtDirectDepChkLabel.Tag = "text";
			this.txtDirectDepChkLabel.Text = "CHECKING";
			this.txtDirectDepChkLabel.Top = 2.625F;
			this.txtDirectDepChkLabel.Visible = false;
			this.txtDirectDepChkLabel.Width = 0.9375F;
			// 
			// txtDirectDepSav
			// 
			this.txtDirectDepSav.Height = 0.1666667F;
			this.txtDirectDepSav.HyperLink = null;
			this.txtDirectDepSav.Left = 1F;
			this.txtDirectDepSav.Name = "txtDirectDepSav";
			this.txtDirectDepSav.Style = "font-size: 10pt; text-align: right";
			this.txtDirectDepSav.Tag = "text";
			this.txtDirectDepSav.Text = "0.00";
			this.txtDirectDepSav.Top = 2.791667F;
			this.txtDirectDepSav.Visible = false;
			this.txtDirectDepSav.Width = 0.6875F;
			// 
			// txtDirectDepositSavLabel
			// 
			this.txtDirectDepositSavLabel.Height = 0.1666667F;
			this.txtDirectDepositSavLabel.HyperLink = null;
			this.txtDirectDepositSavLabel.Left = 0.0625F;
			this.txtDirectDepositSavLabel.Name = "txtDirectDepositSavLabel";
			this.txtDirectDepositSavLabel.Style = "font-size: 10pt; text-align: left";
			this.txtDirectDepositSavLabel.Tag = "text";
			this.txtDirectDepositSavLabel.Text = "SAVINGS";
			this.txtDirectDepositSavLabel.Top = 2.791667F;
			this.txtDirectDepositSavLabel.Visible = false;
			this.txtDirectDepositSavLabel.Width = 0.9375F;
			// 
			// lblDeposit1
			// 
			this.lblDeposit1.Height = 0.1666667F;
			this.lblDeposit1.HyperLink = null;
			this.lblDeposit1.Left = 1.71875F;
			this.lblDeposit1.Name = "lblDeposit1";
			this.lblDeposit1.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit1.Tag = "text";
			this.lblDeposit1.Text = "DEPOSIT #1";
			this.lblDeposit1.Top = 2.625F;
			this.lblDeposit1.Visible = false;
			this.lblDeposit1.Width = 1.15625F;
			// 
			// lblDepositAmount1
			// 
			this.lblDepositAmount1.Height = 0.1666667F;
			this.lblDepositAmount1.HyperLink = null;
			this.lblDepositAmount1.Left = 2.875F;
			this.lblDepositAmount1.Name = "lblDepositAmount1";
			this.lblDepositAmount1.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount1.Tag = "text";
			this.lblDepositAmount1.Text = "0.00";
			this.lblDepositAmount1.Top = 2.625F;
			this.lblDepositAmount1.Visible = false;
			this.lblDepositAmount1.Width = 0.875F;
			// 
			// lblDeposit2
			// 
			this.lblDeposit2.Height = 0.1666667F;
			this.lblDeposit2.HyperLink = null;
			this.lblDeposit2.Left = 1.71875F;
			this.lblDeposit2.Name = "lblDeposit2";
			this.lblDeposit2.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit2.Tag = "text";
			this.lblDeposit2.Text = "DEPOSIT #2";
			this.lblDeposit2.Top = 2.791667F;
			this.lblDeposit2.Visible = false;
			this.lblDeposit2.Width = 1.15625F;
			// 
			// lblDepositAmount2
			// 
			this.lblDepositAmount2.Height = 0.1666667F;
			this.lblDepositAmount2.HyperLink = null;
			this.lblDepositAmount2.Left = 2.875F;
			this.lblDepositAmount2.Name = "lblDepositAmount2";
			this.lblDepositAmount2.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount2.Tag = "text";
			this.lblDepositAmount2.Text = "0.00";
			this.lblDepositAmount2.Top = 2.791667F;
			this.lblDepositAmount2.Visible = false;
			this.lblDepositAmount2.Width = 0.875F;
			// 
			// lblDeposit3
			// 
			this.lblDeposit3.Height = 0.1666667F;
			this.lblDeposit3.HyperLink = null;
			this.lblDeposit3.Left = 1.71875F;
			this.lblDeposit3.Name = "lblDeposit3";
			this.lblDeposit3.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit3.Tag = "text";
			this.lblDeposit3.Text = "DEPOSIT #3";
			this.lblDeposit3.Top = 2.958333F;
			this.lblDeposit3.Visible = false;
			this.lblDeposit3.Width = 1.15625F;
			// 
			// lblDepositAmount3
			// 
			this.lblDepositAmount3.Height = 0.1666667F;
			this.lblDepositAmount3.HyperLink = null;
			this.lblDepositAmount3.Left = 2.875F;
			this.lblDepositAmount3.Name = "lblDepositAmount3";
			this.lblDepositAmount3.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount3.Tag = "text";
			this.lblDepositAmount3.Text = "0.00";
			this.lblDepositAmount3.Top = 2.958333F;
			this.lblDepositAmount3.Visible = false;
			this.lblDepositAmount3.Width = 0.875F;
			// 
			// lblDeposit4
			// 
			this.lblDeposit4.Height = 0.1666667F;
			this.lblDeposit4.HyperLink = null;
			this.lblDeposit4.Left = 3.78125F;
			this.lblDeposit4.Name = "lblDeposit4";
			this.lblDeposit4.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit4.Tag = "text";
			this.lblDeposit4.Text = "DEPOSIT #4";
			this.lblDeposit4.Top = 2.625F;
			this.lblDeposit4.Visible = false;
			this.lblDeposit4.Width = 1.25F;
			// 
			// lblDepositAmount4
			// 
			this.lblDepositAmount4.Height = 0.1666667F;
			this.lblDepositAmount4.HyperLink = null;
			this.lblDepositAmount4.Left = 5.0625F;
			this.lblDepositAmount4.Name = "lblDepositAmount4";
			this.lblDepositAmount4.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount4.Tag = "text";
			this.lblDepositAmount4.Text = "0.00";
			this.lblDepositAmount4.Top = 2.625F;
			this.lblDepositAmount4.Visible = false;
			this.lblDepositAmount4.Width = 0.78125F;
			// 
			// lblDeposit7
			// 
			this.lblDeposit7.Height = 0.1666667F;
			this.lblDeposit7.HyperLink = null;
			this.lblDeposit7.Left = 5.90625F;
			this.lblDeposit7.Name = "lblDeposit7";
			this.lblDeposit7.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit7.Tag = "text";
			this.lblDeposit7.Text = "DEPOSIT #7";
			this.lblDeposit7.Top = 2.625F;
			this.lblDeposit7.Visible = false;
			this.lblDeposit7.Width = 0.96875F;
			// 
			// lblDepositAmount7
			// 
			this.lblDepositAmount7.Height = 0.1666667F;
			this.lblDepositAmount7.HyperLink = null;
			this.lblDepositAmount7.Left = 6.9375F;
			this.lblDepositAmount7.Name = "lblDepositAmount7";
			this.lblDepositAmount7.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount7.Tag = "text";
			this.lblDepositAmount7.Text = "0.00";
			this.lblDepositAmount7.Top = 2.625F;
			this.lblDepositAmount7.Visible = false;
			this.lblDepositAmount7.Width = 0.6875F;
			// 
			// lblDeposit5
			// 
			this.lblDeposit5.Height = 0.1666667F;
			this.lblDeposit5.HyperLink = null;
			this.lblDeposit5.Left = 3.78125F;
			this.lblDeposit5.Name = "lblDeposit5";
			this.lblDeposit5.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit5.Tag = "text";
			this.lblDeposit5.Text = "DEPOSIT #5";
			this.lblDeposit5.Top = 2.791667F;
			this.lblDeposit5.Visible = false;
			this.lblDeposit5.Width = 1.25F;
			// 
			// lblDepositAmount5
			// 
			this.lblDepositAmount5.Height = 0.1666667F;
			this.lblDepositAmount5.HyperLink = null;
			this.lblDepositAmount5.Left = 5.0625F;
			this.lblDepositAmount5.Name = "lblDepositAmount5";
			this.lblDepositAmount5.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount5.Tag = "text";
			this.lblDepositAmount5.Text = "0.00";
			this.lblDepositAmount5.Top = 2.791667F;
			this.lblDepositAmount5.Visible = false;
			this.lblDepositAmount5.Width = 0.78125F;
			// 
			// lblDeposit6
			// 
			this.lblDeposit6.Height = 0.1666667F;
			this.lblDeposit6.HyperLink = null;
			this.lblDeposit6.Left = 3.78125F;
			this.lblDeposit6.Name = "lblDeposit6";
			this.lblDeposit6.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit6.Tag = "text";
			this.lblDeposit6.Text = "DEPOSIT #6";
			this.lblDeposit6.Top = 2.958333F;
			this.lblDeposit6.Visible = false;
			this.lblDeposit6.Width = 1.25F;
			// 
			// lblDepositAmount6
			// 
			this.lblDepositAmount6.Height = 0.1666667F;
			this.lblDepositAmount6.HyperLink = null;
			this.lblDepositAmount6.Left = 5.0625F;
			this.lblDepositAmount6.Name = "lblDepositAmount6";
			this.lblDepositAmount6.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount6.Tag = "text";
			this.lblDepositAmount6.Text = "0.00";
			this.lblDepositAmount6.Top = 2.958333F;
			this.lblDepositAmount6.Visible = false;
			this.lblDepositAmount6.Width = 0.78125F;
			// 
			// lblDeposit8
			// 
			this.lblDeposit8.Height = 0.1666667F;
			this.lblDeposit8.HyperLink = null;
			this.lblDeposit8.Left = 5.90625F;
			this.lblDeposit8.Name = "lblDeposit8";
			this.lblDeposit8.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit8.Tag = "text";
			this.lblDeposit8.Text = "DEPOSIT #8";
			this.lblDeposit8.Top = 2.791667F;
			this.lblDeposit8.Visible = false;
			this.lblDeposit8.Width = 0.96875F;
			// 
			// lblDepositAmount8
			// 
			this.lblDepositAmount8.Height = 0.1666667F;
			this.lblDepositAmount8.HyperLink = null;
			this.lblDepositAmount8.Left = 6.9375F;
			this.lblDepositAmount8.Name = "lblDepositAmount8";
			this.lblDepositAmount8.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount8.Tag = "text";
			this.lblDepositAmount8.Text = "0.00";
			this.lblDepositAmount8.Top = 2.791667F;
			this.lblDepositAmount8.Visible = false;
			this.lblDepositAmount8.Width = 0.6875F;
			// 
			// lblDeposit9
			// 
			this.lblDeposit9.Height = 0.1666667F;
			this.lblDeposit9.HyperLink = null;
			this.lblDeposit9.Left = 5.90625F;
			this.lblDeposit9.Name = "lblDeposit9";
			this.lblDeposit9.Style = "font-size: 10pt; text-align: left";
			this.lblDeposit9.Tag = "text";
			this.lblDeposit9.Text = "DEPOSIT #9";
			this.lblDeposit9.Top = 2.958333F;
			this.lblDeposit9.Visible = false;
			this.lblDeposit9.Width = 0.96875F;
			// 
			// lblDepositAmount9
			// 
			this.lblDepositAmount9.Height = 0.1666667F;
			this.lblDepositAmount9.HyperLink = null;
			this.lblDepositAmount9.Left = 6.9375F;
			this.lblDepositAmount9.Name = "lblDepositAmount9";
			this.lblDepositAmount9.Style = "font-size: 10pt; text-align: right";
			this.lblDepositAmount9.Tag = "text";
			this.lblDepositAmount9.Text = "0.00";
			this.lblDepositAmount9.Top = 2.958333F;
			this.lblDepositAmount9.Visible = false;
			this.lblDepositAmount9.Width = 0.6875F;
			// 
			// lblCode1
			// 
			this.lblCode1.Height = 0.1666667F;
			this.lblCode1.HyperLink = null;
			this.lblCode1.Left = 6.5F;
			this.lblCode1.Name = "lblCode1";
			this.lblCode1.Style = "font-size: 10pt; text-align: left";
			this.lblCode1.Tag = "text";
			this.lblCode1.Text = "Code1";
			this.lblCode1.Top = 2.083333F;
			this.lblCode1.Width = 0.46875F;
			// 
			// lblCode2
			// 
			this.lblCode2.Height = 0.1666667F;
			this.lblCode2.HyperLink = null;
			this.lblCode2.Left = 6.5F;
			this.lblCode2.Name = "lblCode2";
			this.lblCode2.Style = "font-size: 10pt; text-align: left";
			this.lblCode2.Tag = "text";
			this.lblCode2.Text = "Code2";
			this.lblCode2.Top = 2.25F;
			this.lblCode2.Width = 0.46875F;
			// 
			// lblCode1Balance
			// 
			this.lblCode1Balance.Height = 0.1666667F;
			this.lblCode1Balance.HyperLink = null;
			this.lblCode1Balance.Left = 7F;
			this.lblCode1Balance.Name = "lblCode1Balance";
			this.lblCode1Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode1Balance.Tag = "text";
			this.lblCode1Balance.Text = "0.00";
			this.lblCode1Balance.Top = 2.083333F;
			this.lblCode1Balance.Width = 0.625F;
			// 
			// lblCode2Balance
			// 
			this.lblCode2Balance.Height = 0.1666667F;
			this.lblCode2Balance.HyperLink = null;
			this.lblCode2Balance.Left = 7F;
			this.lblCode2Balance.Name = "lblCode2Balance";
			this.lblCode2Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode2Balance.Tag = "text";
			this.lblCode2Balance.Text = "0.00";
			this.lblCode2Balance.Top = 2.25F;
			this.lblCode2Balance.Width = 0.625F;
			// 
			// lblCode3
			// 
			this.lblCode3.Height = 0.1666667F;
			this.lblCode3.HyperLink = null;
			this.lblCode3.Left = 6.5F;
			this.lblCode3.Name = "lblCode3";
			this.lblCode3.Style = "font-size: 10pt; text-align: left";
			this.lblCode3.Tag = "text";
			this.lblCode3.Text = "Code3";
			this.lblCode3.Top = 2.416667F;
			this.lblCode3.Width = 0.5F;
			// 
			// lblCode3Balance
			// 
			this.lblCode3Balance.Height = 0.1666667F;
			this.lblCode3Balance.HyperLink = null;
			this.lblCode3Balance.Left = 7F;
			this.lblCode3Balance.Name = "lblCode3Balance";
			this.lblCode3Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode3Balance.Tag = "text";
			this.lblCode3Balance.Text = "0.00";
			this.lblCode3Balance.Top = 2.416667F;
			this.lblCode3Balance.Width = 0.625F;
			// 
			// lblCheckMessage
			// 
			this.lblCheckMessage.Height = 0.1666667F;
			this.lblCheckMessage.HyperLink = null;
			this.lblCheckMessage.Left = 3.84375F;
			this.lblCheckMessage.Name = "lblCheckMessage";
			this.lblCheckMessage.Style = "font-size: 10pt; text-align: right";
			this.lblCheckMessage.Text = null;
			this.lblCheckMessage.Top = 0.3333333F;
			this.lblCheckMessage.Width = 3.40625F;
			// 
			// txtYTDDeds
			// 
			this.txtYTDDeds.Height = 0.1666667F;
			this.txtYTDDeds.HyperLink = null;
			this.txtYTDDeds.Left = 6.8125F;
			this.txtYTDDeds.Name = "txtYTDDeds";
			this.txtYTDDeds.Style = "font-size: 10pt; text-align: right";
			this.txtYTDDeds.Tag = "text";
			this.txtYTDDeds.Text = "0.00";
			this.txtYTDDeds.Top = 1.364583F;
			this.txtYTDDeds.Width = 0.8125F;
			// 
			// lblDeptDiv
			// 
			this.lblDeptDiv.Height = 0.1666667F;
			this.lblDeptDiv.HyperLink = null;
			this.lblDeptDiv.Left = 1.958333F;
			this.lblDeptDiv.Name = "lblDeptDiv";
			this.lblDeptDiv.Style = "font-size: 10pt; text-align: left";
			this.lblDeptDiv.Tag = "text";
			this.lblDeptDiv.Text = "Dept/Div:";
			this.lblDeptDiv.Top = 0.3333333F;
			this.lblDeptDiv.Width = 0.875F;
			// 
			// txtDeptDiv
			// 
			this.txtDeptDiv.Height = 0.1666667F;
			this.txtDeptDiv.HyperLink = null;
			this.txtDeptDiv.Left = 2.895833F;
			this.txtDeptDiv.Name = "txtDeptDiv";
			this.txtDeptDiv.Style = "font-size: 10pt; text-align: left";
			this.txtDeptDiv.Tag = "text";
			this.txtDeptDiv.Text = null;
			this.txtDeptDiv.Top = 0.3333333F;
			this.txtDeptDiv.Width = 0.8125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.01041667F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.006944444F;
			this.Line1.Visible = false;
			this.Line1.Width = 7.614583F;
			this.Line1.X1 = 0.01041667F;
			this.Line1.X2 = 7.625F;
			this.Line1.Y1 = 0.006944444F;
			this.Line1.Y2 = 0.006944444F;
			// 
			// srptLaserStub1
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.677083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDDeds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFICA;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEMatchCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmatchYTD;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtChkAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtOtherBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepChkLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepSav;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositSavLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDDeds;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
