namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptClearLTDDeductions.
	/// </summary>
	partial class rptClearLTDDeductions
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptClearLTDDeductions));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOldValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNewValue,
				this.txtNumber,
				this.txtOldValue,
				this.txtName
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.lblAddress1,
				this.Label2,
				this.Line1,
				this.lblAddress2,
				this.Label6,
				this.lblPage,
				this.txtTime
			});
			this.PageHeader.Height = 1.020833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1666667F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.08333334F;
			this.txtCaption.Width = 7.75F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 5.84375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.08333334F;
			this.txtDate.Width = 1.96875F;
			// 
			// lblAddress1
			// 
			this.lblAddress1.Height = 0.1875F;
			this.lblAddress1.HyperLink = null;
			this.lblAddress1.Left = 3.25F;
			this.lblAddress1.Name = "lblAddress1";
			this.lblAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.lblAddress1.Text = "Old Value";
			this.lblAddress1.Top = 0.8125F;
			this.lblAddress1.Width = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label2.Text = "Emp #";
			this.Label2.Top = 0.8125F;
			this.Label2.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// lblAddress2
			// 
			this.lblAddress2.Height = 0.1875F;
			this.lblAddress2.HyperLink = null;
			this.lblAddress2.Left = 4.8125F;
			this.lblAddress2.Name = "lblAddress2";
			this.lblAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.lblAddress2.Text = "New Value";
			this.lblAddress2.Top = 0.8125F;
			this.lblAddress2.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.75F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label6.Text = "Name";
			this.Label6.Top = 0.8125F;
			this.Label6.Width = 0.5625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.2083333F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.84375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblPage.Text = "Label5";
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.96875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.84375F;
			// 
			// txtNewValue
			// 
			this.txtNewValue.Height = 0.1875F;
			this.txtNewValue.Left = 4.8125F;
			this.txtNewValue.Name = "txtNewValue";
			this.txtNewValue.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtNewValue.Text = null;
			this.txtNewValue.Top = 0F;
			this.txtNewValue.Width = 1.5F;
			// 
			// txtNumber
			// 
			this.txtNumber.Height = 0.1875F;
			this.txtNumber.Left = 0.0625F;
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtNumber.Text = null;
			this.txtNumber.Top = 0F;
			this.txtNumber.Width = 0.5F;
			// 
			// txtOldValue
			// 
			this.txtOldValue.Height = 0.1875F;
			this.txtOldValue.Left = 3.25F;
			this.txtOldValue.Name = "txtOldValue";
			this.txtOldValue.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtOldValue.Text = null;
			this.txtOldValue.Top = 0F;
			this.txtOldValue.Width = 1.5F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.75F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.4375F;
			// 
			// rptClearLTDDeductions
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.2222222F;
			this.PageSettings.Margins.Right = 0.2222222F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOldValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}