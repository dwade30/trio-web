namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTaxStatusCodes.
	/// </summary>
	partial class rptTaxStatusCodes
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxStatusCodes));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxStatusCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkFed = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkFica = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkMed = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkState = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkLocal = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatusCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDescription,
				this.txtTaxStatusCode,
				this.chkFed,
				this.chkFica,
				this.chkMed,
				this.chkState,
				this.chkLocal
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtCaption2,
				this.txtDate,
				this.Label1,
				this.Label3,
				this.Label7,
				this.Line1,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.txtTime,
				this.lblPage
			});
			this.PageHeader.Height = 1.03125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Width = 7.8125F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1875F;
			this.txtCaption2.Left = 0.0625F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtCaption2.Text = null;
			this.txtCaption2.Top = 0.25F;
			this.txtCaption2.Width = 7.8125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.8125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.0625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label1.Text = "Description";
			this.Label1.Top = 0.8125F;
			this.Label1.Width = 0.8125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.5F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label3.Text = "FED";
			this.Label3.Top = 0.8125F;
			this.Label3.Width = 0.625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label7.Text = "Tax Status Code";
			this.Label7.Top = 0.8125F;
			this.Label7.Width = 1.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label8.Text = "FICA";
			this.Label8.Top = 0.8125F;
			this.Label8.Width = 0.625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label9.Text = "MED";
			this.Label9.Top = 0.8125F;
			this.Label9.Width = 0.625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.75F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label10.Text = "STATE";
			this.Label10.Top = 0.8125F;
			this.Label10.Width = 0.625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.5F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label11.Text = "LOCAL";
			this.Label11.Top = 0.8125F;
			this.Label11.Width = 0.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtTime.Text = "MuniName";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 2F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Label5";
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.25F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 1.5625F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.6875F;
			// 
			// txtTaxStatusCode
			// 
			this.txtTaxStatusCode.Height = 0.1875F;
			this.txtTaxStatusCode.Left = 0.125F;
			this.txtTaxStatusCode.Name = "txtTaxStatusCode";
			this.txtTaxStatusCode.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtTaxStatusCode.Text = null;
			this.txtTaxStatusCode.Top = 0F;
			this.txtTaxStatusCode.Width = 0.375F;
			// 
			// chkFed
			// 
			this.chkFed.Height = 0.1875F;
			this.chkFed.Left = 3.75F;
			this.chkFed.Name = "chkFed";
			this.chkFed.Style = "font-size: 8pt";
			this.chkFed.Text = null;
			this.chkFed.Top = 0F;
			this.chkFed.Width = 0.1875F;
			// 
			// chkFica
			// 
			this.chkFica.Height = 0.1875F;
			this.chkFica.Left = 4.5F;
			this.chkFica.Name = "chkFica";
			this.chkFica.Style = "font-size: 8pt";
			this.chkFica.Text = null;
			this.chkFica.Top = 0F;
			this.chkFica.Width = 0.1875F;
			// 
			// chkMed
			// 
			this.chkMed.Height = 0.1875F;
			this.chkMed.Left = 5.25F;
			this.chkMed.Name = "chkMed";
			this.chkMed.Style = "font-size: 8pt";
			this.chkMed.Text = null;
			this.chkMed.Top = 0F;
			this.chkMed.Width = 0.1875F;
			// 
			// chkState
			// 
			this.chkState.Height = 0.1875F;
			this.chkState.Left = 6F;
			this.chkState.Name = "chkState";
			this.chkState.Style = "font-size: 8pt";
			this.chkState.Text = null;
			this.chkState.Top = 0F;
			this.chkState.Width = 0.1875F;
			// 
			// chkLocal
			// 
			this.chkLocal.Height = 0.1875F;
			this.chkLocal.Left = 6.75F;
			this.chkLocal.Name = "chkLocal";
			this.chkLocal.Style = "font-size: 8pt";
			this.chkLocal.Text = null;
			this.chkLocal.Top = 0F;
			this.chkLocal.Width = 0.1875F;
			// 
			// rptTaxStatusCodes
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.4861111F;
			this.PageSettings.Margins.Right = 0.4861111F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.0625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatusCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxStatusCode;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFed;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFica;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkMed;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkState;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkLocal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}