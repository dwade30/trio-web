//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmDeptDiv : BaseForm
	{
		public frmDeptDiv()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDeptDiv InstancePtr
		{
			get
			{
				return (frmDeptDiv)Sys.GetInstance(typeof(frmDeptDiv));
			}
		}

		protected frmDeptDiv _InstancePtr = null;
		//=========================================================
		private void frmDeptDiv_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmDeptDiv_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeptDiv properties;
			//frmDeptDiv.ScaleWidth	= 5700;
			//frmDeptDiv.ScaleHeight	= 3975;
			//frmDeptDiv.LinkTopic	= "Form1";
			//frmDeptDiv.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDeptDiv_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmDeptDiv_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = vsData.WidthOriginal;
			vsData.ColWidth(0, FCConvert.ToInt32(0.15 * GridWidth));
            //FC:FINAL:AM:#3271 - set column widths (flexMergeSpill not supported)
            vsData.ColWidth(1, FCConvert.ToInt32(0.35 * GridWidth));
            vsData.ColWidth(2, FCConvert.ToInt32(0.50 * GridWidth));
            // .ColWidth(1) = .15 * gridwidth
        }

		public void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsData = new clsDRWrapper();
				vsData.Redraw = false;
				vsData.TextMatrix(0, 0, "Dept");
				vsData.TextMatrix(0, 1, "Div");
				vsData.TextMatrix(0, 2, "Description");
				vsData.OutlineCol = 0;
				// FILL THE GRID WITH DATA FROM THE DEPT DIV TABLE
				rsData.OpenRecordset("Select * from DeptDivTitles Order by Department,Division", "Twbd0000.vb1");
				vsData.Rows = 1;
				vsData.Cols = 3;
				while (!rsData.EndOfFile())
				{
					// If .Fields("Division") = "00" Then
					if (FCConvert.ToString(rsData.Get_Fields_String("Division")) == Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))), "0"))
					{
						// MAKE THIS A PARENT
						vsData.AddItem(rsData.Get_Fields_String("Department") + "\t" + rsData.Get_Fields("ShortDescription"));
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.RowOutlineLevel(vsData.Rows - 1, 0);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpFontBold, vsData.Rows - 1, 0, true);
					}
					else if (FCConvert.ToString(rsData.Get_Fields_String("Division")).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))
					{
						vsData.AddItem(rsData.Get_Fields_String("Department") + "\t" + rsData.Get_Fields("ShortDescription"));
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.RowOutlineLevel(vsData.Rows - 1, 0);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpFontBold, vsData.Rows - 1, 0, true);
					}
					else
					{
						// MAKE THIS A CHILD
						vsData.AddItem("\t" + rsData.Get_Fields_String("Division") + "\t" + rsData.Get_Fields_String("LongDescription"));
						vsData.RowOutlineLevel(vsData.Rows - 1, 1);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpFontBold, vsData.Rows - 1, 1, true);
					}
					rsData.MoveNext();
				}
				vsData.Outline(-1);
                //FC:FINAL:AM:#3271 - don't autosize the columns
				//vsData.AutoSize(0, 1, false);
				vsData.Outline(1);
				vsData.ExtendLastCol = true;
				vsData.Redraw = true;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
				{
					vsData.IsCollapsed(intCounter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
				modColorScheme.ColorGrid(vsData, 1);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 381)
				{
					fecherFoundation.Information.Err(ex).Clear();
					{
						/*? Resume Next; */}
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				}
			}
		}

		private void frmDeptDiv_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsData_DblClick(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (vsData.MouseRow > 0)
				{
					if (vsData.IsSubtotal(vsData.MouseRow))
					{
						modGlobalVariables.Statics.gstrDepartmentNum = FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsData.MouseRow, 0));
						modGlobalVariables.Statics.gstrDivisionNum = Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), "0");
					}
					else
					{
						modGlobalVariables.Statics.gstrDepartmentNum = FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsData.MouseRow, 0));
						modGlobalVariables.Statics.gstrDivisionNum = FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsData.MouseRow, 1));
					}
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 381)
				{
					fecherFoundation.Information.Err(ex).Clear();
					{
						/*? Resume Next; */}
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				}
			}
		}
	}
}
