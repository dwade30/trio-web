﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsACHFileControlRecord
	{
		//=========================================================
		private int intBatchCount;
		private int intBlockCount;
		private int intEntryAddendaCount;
		private double dblTotalDebit;
		private double dblTotalCredit;
		private string strLastError = "";
		private double dblHash;

		public string RecordCode
		{
			get
			{
				string RecordCode = "";
				RecordCode = "9";
				return RecordCode;
			}
		}

		public int BatchCount
		{
			get
			{
				int BatchCount = 0;
				BatchCount = intBatchCount;
				return BatchCount;
			}
			set
			{
				intBatchCount = value;
			}
		}

		public int BlockCount
		{
			get
			{
				int BlockCount = 0;
				BlockCount = intBlockCount;
				return BlockCount;
			}
			set
			{
				intBlockCount = value;
			}
		}

		public int EntryAddendaCount
		{
			get
			{
				int EntryAddendaCount = 0;
				EntryAddendaCount = intEntryAddendaCount;
				return EntryAddendaCount;
			}
			set
			{
				intEntryAddendaCount = value;
			}
		}

		public double TotalDebit
		{
			get
			{
				double TotalDebit = 0;
				TotalDebit = dblTotalDebit;
				return TotalDebit;
			}
			set
			{
				dblTotalDebit = value;
			}
		}

		public double TotalCredit
		{
			get
			{
				double TotalCredit = 0;
				TotalCredit = dblTotalCredit;
				return TotalCredit;
			}
			set
			{
				dblTotalCredit = value;
			}
		}

		public string LastError
		{
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public double Hash
		{
			get
			{
				double Hash = 0;
				Hash = dblHash;
				return Hash;
			}
			set
			{
				dblHash = value;
			}
		}

		public string OutputLine()
		{
			string OutputLine = "";
			strLastError = "";
			string strLine;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strLine = RecordCode;
				strLine += Strings.Right(Strings.StrDup(6, "0") + FCConvert.ToString(intBatchCount), 6);
				strLine += Strings.Right(Strings.StrDup(6, "0") + FCConvert.ToString(intBlockCount), 6);
				strLine += Strings.Right(Strings.StrDup(8, "0") + FCConvert.ToString(intEntryAddendaCount), 8);
				strLine += Strings.Right(Strings.StrDup(10, "0") + FCConvert.ToString(dblHash), 10);
				strLine += Strings.Right(Strings.StrDup(12, "0") + FCConvert.ToString(dblTotalDebit * 100), 12);
				strLine += Strings.Right(Strings.StrDup(12, "0") + FCConvert.ToString(dblTotalCredit * 100), 12);
				strLine += Strings.StrDup(39, " ");
				// reserved
				OutputLine = strLine;
				return OutputLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = "Could not build file control record." + "\r\n" + fecherFoundation.Information.Err(ex).Description;
			}
			return OutputLine;
		}
	}
}
