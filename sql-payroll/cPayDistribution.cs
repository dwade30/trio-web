﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPayDistribution
	{
		//=========================================================
		private int lngRecordID;
		private bool boolCheckVoid;
		private string strEmployeeNumber = string.Empty;
		private int lngCheckNumber;
		private int lngDistributionRecordID;
		private string strEmployeeName = string.Empty;
		private string strPayDate = "";
		private int lngJournalNumber;
		private int lngWarrantNumber;
		private int intAccountingPeriod;
		private double dblFicaTaxableGross;
		private double dblMedicareTaxableGross;
		private int intDistLineCode;
		private string strAccountNumber = string.Empty;
		private int lngPayCategory;
		private int lngTaxCode;
		private string strDistM = string.Empty;
		private bool boolDistE;
		private string strWCCode = string.Empty;
		private int lngPayCode;
		private double dblPayRate;
		private double dblMultiplier;
		private int intNumTaxPeriods;
		private double dblHours;
		private double dblGrossPay;
		private string strMasterRecord = string.Empty;
		private int lngPayrunID;
		private double dblBasePayRate;
		private string strDistU = string.Empty;
		private bool boolAdjustRecord;
		private string strStatusCode = string.Empty;
		private int intReportType;
		private int lngMSRSID;
		private int lngSequenceNumber;
		private bool boolUpdated;
		private bool boolDeleted;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsVoid
		{
			set
			{
				boolCheckVoid = value;
				IsUpdated = true;
			}
			get
			{
				bool IsVoid = false;
				IsVoid = boolCheckVoid;
				return IsVoid;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
				IsUpdated = true;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
				IsUpdated = true;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}

		public int DistributionRecordID
		{
			set
			{
				lngDistributionRecordID = value;
				IsUpdated = true;
			}
			get
			{
				int DistributionRecordID = 0;
				DistributionRecordID = lngDistributionRecordID;
				return DistributionRecordID;
			}
		}

		public string EmployeeName
		{
			set
			{
				strEmployeeName = value;
				IsUpdated = true;
			}
			get
			{
				string EmployeeName = "";
				EmployeeName = strEmployeeName;
				return EmployeeName;
			}
		}

		public string PayDate
		{
			set
			{
				if (fecherFoundation.Information.IsDate(value))
				{
					strPayDate = value;
				}
				else
				{
					strPayDate = "";
				}
			}
			get
			{
				string PayDate = "";
				PayDate = strPayDate;
				return PayDate;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
				IsUpdated = true;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public int WarrantNumber
		{
			set
			{
				lngWarrantNumber = value;
				IsUpdated = true;
			}
			get
			{
				int WarrantNumber = 0;
				WarrantNumber = lngWarrantNumber;
				return WarrantNumber;
			}
		}

		public int AccountingPeriod
		{
			set
			{
				intAccountingPeriod = value;
				IsUpdated = true;
			}
			get
			{
				int AccountingPeriod = 0;
				AccountingPeriod = intAccountingPeriod;
				return AccountingPeriod;
			}
		}

		public double FicaTaxableGross
		{
			set
			{
				dblFicaTaxableGross = value;
				IsUpdated = true;
			}
			get
			{
				double FicaTaxableGross = 0;
				FicaTaxableGross = dblFicaTaxableGross;
				return FicaTaxableGross;
			}
		}

		public double MedicareTaxableGross
		{
			set
			{
				dblMedicareTaxableGross = value;
				IsUpdated = true;
			}
			get
			{
				double MedicareTaxableGross = 0;
				MedicareTaxableGross = dblMedicareTaxableGross;
				return MedicareTaxableGross;
			}
		}

		public int DistLineCode
		{
			set
			{
				intDistLineCode = value;
				IsUpdated = true;
			}
			get
			{
				int DistLineCode = 0;
				DistLineCode = intDistLineCode;
				return DistLineCode;
			}
		}

		public string AccountNumber
		{
			set
			{
				strAccountNumber = value;
				IsUpdated = true;
			}
			get
			{
				string AccountNumber = "";
				AccountNumber = strAccountNumber;
				return AccountNumber;
			}
		}

		public int PayCategory
		{
			set
			{
				lngPayCategory = value;
				IsUpdated = true;
			}
			get
			{
				int PayCategory = 0;
				PayCategory = lngPayCategory;
				return PayCategory;
			}
		}

		public int TaxCode
		{
			set
			{
				lngTaxCode = value;
				IsUpdated = true;
			}
			get
			{
				int TaxCode = 0;
				TaxCode = lngTaxCode;
				return TaxCode;
			}
		}

		public string DistM
		{
			set
			{
				strDistM = value;
				IsUpdated = true;
			}
			get
			{
				string DistM = "";
				DistM = strDistM;
				return DistM;
			}
		}

		public bool DistE
		{
			set
			{
				boolDistE = value;
				IsUpdated = true;
			}
			get
			{
				bool DistE = false;
				DistE = boolDistE;
				return DistE;
			}
		}

		public string WCCode
		{
			set
			{
				strWCCode = value;
				IsUpdated = true;
			}
			get
			{
				string WCCode = "";
				WCCode = strWCCode;
				return WCCode;
			}
		}

		public int PayCode
		{
			set
			{
				lngPayCode = value;
				IsUpdated = true;
			}
			get
			{
				int PayCode = 0;
				PayCode = lngPayCode;
				return PayCode;
			}
		}

		public double PayRate
		{
			set
			{
				dblPayRate = value;
				IsUpdated = true;
			}
			get
			{
				double PayRate = 0;
				PayRate = dblPayRate;
				return PayRate;
			}
		}

		public double Multiplier
		{
			set
			{
				dblMultiplier = value;
				IsUpdated = true;
			}
			get
			{
				double Multiplier = 0;
				Multiplier = dblMultiplier;
				return Multiplier;
			}
		}

		public int NumberTaxPeriods
		{
			set
			{
				intNumTaxPeriods = value;
				IsUpdated = true;
			}
			get
			{
				int NumberTaxPeriods = 0;
				NumberTaxPeriods = intNumTaxPeriods;
				return NumberTaxPeriods;
			}
		}

		public double Hours
		{
			set
			{
				dblHours = value;
				IsUpdated = true;
			}
			get
			{
				double Hours = 0;
				Hours = dblHours;
				return Hours;
			}
		}

		public double GrossPay
		{
			set
			{
				dblGrossPay = value;
				IsUpdated = true;
			}
			get
			{
				double GrossPay = 0;
				GrossPay = dblGrossPay;
				return GrossPay;
			}
		}

		public string MasterRecord
		{
			set
			{
				strMasterRecord = value;
				IsUpdated = true;
			}
			get
			{
				string MasterRecord = "";
				MasterRecord = strMasterRecord;
				return MasterRecord;
			}
		}

		public int PayrunID
		{
			set
			{
				lngPayrunID = value;
				IsUpdated = true;
			}
			get
			{
				int PayrunID = 0;
				PayrunID = lngPayrunID;
				return PayrunID;
			}
		}

		public double BasePayrate
		{
			set
			{
				dblBasePayRate = value;
				IsUpdated = true;
			}
			get
			{
				double BasePayrate = 0;
				BasePayrate = dblBasePayRate;
				return BasePayrate;
			}
		}

		public string DistU
		{
			set
			{
				IsUpdated = true;
				strDistU = value;
			}
			get
			{
				string DistU = "";
				DistU = strDistU;
				return DistU;
			}
		}

		public bool IsAdjustmentRecord
		{
			set
			{
				boolAdjustRecord = value;
				IsUpdated = true;
			}
			get
			{
				bool IsAdjustmentRecord = false;
				IsAdjustmentRecord = boolAdjustRecord;
				return IsAdjustmentRecord;
			}
		}

		public string StatusCode
		{
			set
			{
				strStatusCode = value;
				IsUpdated = true;
			}
			get
			{
				string StatusCode = "";
				StatusCode = strStatusCode;
				return StatusCode;
			}
		}

		public int ReportType
		{
			set
			{
				intReportType = value;
				IsUpdated = true;
			}
			get
			{
				int ReportType = 0;
				ReportType = intReportType;
				return ReportType;
			}
		}

		public int MSRSID
		{
			set
			{
				lngMSRSID = value;
				IsUpdated = true;
			}
			get
			{
				int MSRSID = 0;
				MSRSID = lngMSRSID;
				return MSRSID;
			}
		}

		public int SequenceNumber
		{
			set
			{
				IsUpdated = true;
				lngSequenceNumber = value;
			}
			get
			{
				int SequenceNumber = 0;
				SequenceNumber = lngSequenceNumber;
				return SequenceNumber;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
