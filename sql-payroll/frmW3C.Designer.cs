//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW3C.
	/// </summary>
	partial class frmW3C
	{
		public fecherFoundation.FCComboBox cmbKindOfPayer;
		public fecherFoundation.FCLabel lblKindOfPayer;
		public fecherFoundation.FCComboBox cmbFedGovt;
		public fecherFoundation.FCLabel lblFedGovt;
		public fecherFoundation.FCTextBox txtEstablishment;
		public fecherFoundation.FCCheckBox chkThirdPartySickPay;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtIncorrectStateID;
		public fecherFoundation.FCTextBox txtEstablishmentNumber;
		public fecherFoundation.FCTextBox txtIncorrectEIN;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public Global.T2KDateBox t2kAdjustment;
		public fecherFoundation.FCCheckBox chkEmpReturnAdjusted;
		public fecherFoundation.FCTextBox txtDecreases;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtTitle;
		public Global.T2KPhoneNumberBox txtPhoneNumber;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txtContactPerson;
		public Global.T2KPhoneNumberBox txtFaxNumber;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbKindOfPayer = new fecherFoundation.FCComboBox();
            this.lblKindOfPayer = new fecherFoundation.FCLabel();
            this.cmbFedGovt = new fecherFoundation.FCComboBox();
            this.lblFedGovt = new fecherFoundation.FCLabel();
            this.txtEstablishment = new fecherFoundation.FCTextBox();
            this.chkThirdPartySickPay = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtIncorrectStateID = new fecherFoundation.FCTextBox();
            this.txtEstablishmentNumber = new fecherFoundation.FCTextBox();
            this.txtIncorrectEIN = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.t2kAdjustment = new Global.T2KDateBox();
            this.chkEmpReturnAdjusted = new fecherFoundation.FCCheckBox();
            this.txtDecreases = new fecherFoundation.FCTextBox();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.txtPhoneNumber = new Global.T2KPhoneNumberBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.txtContactPerson = new fecherFoundation.FCTextBox();
            this.txtFaxNumber = new Global.T2KPhoneNumberBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkThirdPartySickPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kAdjustment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmpReturnAdjusted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaxNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(848, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtEstablishment);
            this.ClientArea.Controls.Add(this.cmbKindOfPayer);
            this.ClientArea.Controls.Add(this.lblKindOfPayer);
            this.ClientArea.Controls.Add(this.cmbFedGovt);
            this.ClientArea.Controls.Add(this.lblFedGovt);
            this.ClientArea.Controls.Add(this.chkThirdPartySickPay);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.t2kAdjustment);
            this.ClientArea.Controls.Add(this.chkEmpReturnAdjusted);
            this.ClientArea.Controls.Add(this.txtDecreases);
            this.ClientArea.Controls.Add(this.Frame7);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(848, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(848, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(75, 30);
            this.HeaderText.Text = "W-3C";
            // 
            // cmbKindOfPayer
            // 
            this.cmbKindOfPayer.Items.AddRange(new object[] {
            "941",
            "CT - 1",
            "Military",
            "Hshld Emp.",
            "943",
            "Medicare Govt Emp",
            "944"});
            this.cmbKindOfPayer.Location = new System.Drawing.Point(185, 280);
            this.cmbKindOfPayer.Name = "cmbKindOfPayer";
            this.cmbKindOfPayer.Size = new System.Drawing.Size(236, 40);
            this.cmbKindOfPayer.TabIndex = 19;
            this.cmbKindOfPayer.Text = "941";
            // 
            // lblKindOfPayer
            // 
            this.lblKindOfPayer.AutoSize = true;
            this.lblKindOfPayer.Location = new System.Drawing.Point(30, 294);
            this.lblKindOfPayer.Name = "lblKindOfPayer";
            this.lblKindOfPayer.Size = new System.Drawing.Size(103, 15);
            this.lblKindOfPayer.TabIndex = 20;
            this.lblKindOfPayer.Text = "KIND OF PAYER";
            // 
            // cmbFedGovt
            // 
            this.cmbFedGovt.Items.AddRange(new object[] {
            "Federal govt",
            "State/local 501c",
            "501c non-govt",
            "State/Local non 501c",
            "None Apply"});
            this.cmbFedGovt.Location = new System.Drawing.Point(185, 330);
            this.cmbFedGovt.Name = "cmbFedGovt";
            this.cmbFedGovt.Size = new System.Drawing.Size(236, 40);
            this.cmbFedGovt.TabIndex = 21;
            this.cmbFedGovt.Text = "State/Local non 501c";
            // 
            // lblFedGovt
            // 
            this.lblFedGovt.AutoSize = true;
            this.lblFedGovt.Location = new System.Drawing.Point(30, 344);
            this.lblFedGovt.Name = "lblFedGovt";
            this.lblFedGovt.Size = new System.Drawing.Size(129, 15);
            this.lblFedGovt.TabIndex = 22;
            this.lblFedGovt.Text = "KIND OF EMPLOYER";
            // 
            // txtEstablishment
            // 
            this.txtEstablishment.BackColor = System.Drawing.SystemColors.Window;
            this.txtEstablishment.Location = new System.Drawing.Point(211, 427);
            this.txtEstablishment.MaxLength = 4;
            this.txtEstablishment.Name = "txtEstablishment";
            this.txtEstablishment.Size = new System.Drawing.Size(211, 40);
            this.txtEstablishment.TabIndex = 18;
            // 
            // chkThirdPartySickPay
            // 
            this.chkThirdPartySickPay.Location = new System.Drawing.Point(30, 390);
            this.chkThirdPartySickPay.Name = "chkThirdPartySickPay";
            this.chkThirdPartySickPay.Size = new System.Drawing.Size(170, 27);
            this.chkThirdPartySickPay.TabIndex = 17;
            this.chkThirdPartySickPay.Text = "Third-party sick pay";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtIncorrectStateID);
            this.Frame2.Controls.Add(this.txtEstablishmentNumber);
            this.Frame2.Controls.Add(this.txtIncorrectEIN);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Location = new System.Drawing.Point(30, 640);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(770, 116);
            this.Frame2.TabIndex = 29;
            this.Frame2.Text = "Corrections";
            // 
            // txtIncorrectStateID
            // 
            this.txtIncorrectStateID.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectStateID.Location = new System.Drawing.Point(520, 56);
            this.txtIncorrectStateID.Name = "txtIncorrectStateID";
            this.txtIncorrectStateID.Size = new System.Drawing.Size(230, 40);
            this.txtIncorrectStateID.TabIndex = 34;
            // 
            // txtEstablishmentNumber
            // 
            this.txtEstablishmentNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtEstablishmentNumber.Location = new System.Drawing.Point(270, 56);
            this.txtEstablishmentNumber.MaxLength = 4;
            this.txtEstablishmentNumber.Name = "txtEstablishmentNumber";
            this.txtEstablishmentNumber.Size = new System.Drawing.Size(230, 40);
            this.txtEstablishmentNumber.TabIndex = 32;
            // 
            // txtIncorrectEIN
            // 
            this.txtIncorrectEIN.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectEIN.Location = new System.Drawing.Point(20, 56);
            this.txtIncorrectEIN.Name = "txtIncorrectEIN";
            this.txtIncorrectEIN.Size = new System.Drawing.Size(230, 40);
            this.txtIncorrectEIN.TabIndex = 30;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(520, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(122, 14);
            this.Label5.TabIndex = 35;
            this.Label5.Text = "INCORRECT STATE ID";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(270, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(230, 16);
            this.Label4.TabIndex = 33;
            this.Label4.Text = "INCORRECT ESTABLISHMENT NUMBER";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(105, 14);
            this.Label3.TabIndex = 31;
            this.Label3.Text = "INCORRECT EIN";
            // 
            // t2kAdjustment
            // 
            this.t2kAdjustment.Location = new System.Drawing.Point(211, 590);
            this.t2kAdjustment.Name = "t2kAdjustment";
            this.t2kAdjustment.Size = new System.Drawing.Size(115, 40);
            this.t2kAdjustment.TabIndex = 21;
            // 
            // chkEmpReturnAdjusted
            // 
            this.chkEmpReturnAdjusted.Location = new System.Drawing.Point(30, 553);
            this.chkEmpReturnAdjusted.Name = "chkEmpReturnAdjusted";
            this.chkEmpReturnAdjusted.Size = new System.Drawing.Size(748, 27);
            this.chkEmpReturnAdjusted.TabIndex = 20;
            this.chkEmpReturnAdjusted.Text = "An adjustment has been made on an employment tax return filed with the Internal R" +
    "evenue Service";
            // 
            // txtDecreases
            // 
            this.txtDecreases.BackColor = System.Drawing.SystemColors.Window;
            this.txtDecreases.Location = new System.Drawing.Point(30, 503);
            this.txtDecreases.Name = "txtDecreases";
            this.txtDecreases.Size = new System.Drawing.Size(770, 40);
            this.txtDecreases.TabIndex = 19;
            // 
            // Frame7
            // 
            this.Frame7.AppearanceKey = "groupBoxNoBorders";
            this.Frame7.Controls.Add(this.txtTitle);
            this.Frame7.Controls.Add(this.txtPhoneNumber);
            this.Frame7.Controls.Add(this.txtEmail);
            this.Frame7.Controls.Add(this.txtContactPerson);
            this.Frame7.Controls.Add(this.txtFaxNumber);
            this.Frame7.Controls.Add(this.Label7);
            this.Frame7.Controls.Add(this.Label14);
            this.Frame7.Controls.Add(this.Label13);
            this.Frame7.Controls.Add(this.Label10);
            this.Frame7.Controls.Add(this.Label9);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(422, 270);
            this.Frame7.TabIndex = 22;
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.Location = new System.Drawing.Point(185, 80);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(236, 40);
            this.txtTitle.TabIndex = 1;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(185, 180);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(237, 40);
            this.txtPhoneNumber.TabIndex = 3;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(185, 130);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(236, 40);
            this.txtEmail.TabIndex = 2;
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactPerson.Location = new System.Drawing.Point(185, 30);
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(236, 40);
            this.txtContactPerson.TabIndex = 4;
            // 
            // txtFaxNumber
            // 
            this.txtFaxNumber.Location = new System.Drawing.Point(185, 230);
            this.txtFaxNumber.Name = "txtFaxNumber";
            this.txtFaxNumber.Size = new System.Drawing.Size(237, 40);
            this.txtFaxNumber.TabIndex = 4;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 94);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(110, 18);
            this.Label7.TabIndex = 37;
            this.Label7.Text = "TITLE";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(30, 244);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(98, 18);
            this.Label14.TabIndex = 26;
            this.Label14.Text = "FAX NUMBER";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(30, 194);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(130, 18);
            this.Label13.TabIndex = 25;
            this.Label13.Text = "TELEPHONE NUMBER";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(30, 144);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(108, 18);
            this.Label10.TabIndex = 24;
            this.Label10.Text = "EMAIL ADDRESS";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(110, 18);
            this.Label9.TabIndex = 23;
            this.Label9.Text = "CONTACT PERSON";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 477);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(224, 16);
            this.Label2.TabIndex = 28;
            this.Label2.Text = "EXPLAIN DECREASES HERE";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 441);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(150, 16);
            this.Label6.TabIndex = 36;
            this.Label6.Text = "ESTABLISHMENT NUMBER";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 604);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(160, 16);
            this.Label1.TabIndex = 27;
            this.Label1.Text = "DATE ADJUSTMENT MADE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(374, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmW3C
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(848, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmW3C";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "W-3C";
            this.Load += new System.EventHandler(this.frmW3C_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW3C_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkThirdPartySickPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.t2kAdjustment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmpReturnAdjusted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            this.Frame7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaxNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
