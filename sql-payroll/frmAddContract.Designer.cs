﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmAddContract.
	/// </summary>
	partial class frmAddContract
	{
		public fecherFoundation.FCComboBox cmbLoadBack;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame Frame4;
		public FCGrid GridTotals;
		public fecherFoundation.FCButton cmd4Back;
		public fecherFoundation.FCButton cmd4Next;
		public FCGrid GridAccounts;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmd2Back;
		public fecherFoundation.FCButton cmd2Next;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmd3Next;
		public fecherFoundation.FCButton cmd3Back;
		public FCGrid GridPay;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmd1Next;
		public fecherFoundation.FCTextBox txtDescription;
		public Global.T2KDateBox t2kStartDate;
		public Global.T2KDateBox t2kEndDate;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbLoadBack = new fecherFoundation.FCComboBox();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.cmd4Back = new fecherFoundation.FCButton();
			this.GridTotals = new fecherFoundation.FCGrid();
			this.cmd4Next = new fecherFoundation.FCButton();
			this.GridAccounts = new fecherFoundation.FCGrid();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cmd2Back = new fecherFoundation.FCButton();
			this.cmd2Next = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.cmd3Next = new fecherFoundation.FCButton();
			this.cmd3Back = new fecherFoundation.FCButton();
			this.GridPay = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmd1Next = new fecherFoundation.FCButton();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.t2kStartDate = new Global.T2KDateBox();
			this.t2kEndDate = new Global.T2KDateBox();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSaveExit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmd4Back)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmd4Next)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmd2Back)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmd2Next)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmd3Next)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmd3Back)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmd1Next)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(706, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(706, 520);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(706, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(155, 30);
			this.HeaderText.Text = "Add Contract";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbLoadBack
			// 
			this.cmbLoadBack.AutoSize = false;
			this.cmbLoadBack.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLoadBack.FormattingEnabled = true;
			this.cmbLoadBack.Items.AddRange(new object[] {
            "No",
            "Yes"});
			this.cmbLoadBack.Location = new System.Drawing.Point(503, 30);
			this.cmbLoadBack.Name = "cmbLoadBack";
			this.cmbLoadBack.Size = new System.Drawing.Size(115, 40);
			this.cmbLoadBack.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.cmbLoadBack, null);
			// 
			// Frame4
			// 
			this.Frame4.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame4.AppearanceKey = "groupBoxNoBorders";
			this.Frame4.Controls.Add(this.cmd4Back);
			this.Frame4.Controls.Add(this.GridTotals);
			this.Frame4.Controls.Add(this.cmd4Next);
			this.Frame4.Controls.Add(this.GridAccounts);
			this.Frame4.Location = new System.Drawing.Point(30, 278);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(650, 365);
			this.Frame4.TabIndex = 25;
			this.ToolTip1.SetToolTip(this.Frame4, null);
			this.Frame4.Visible = false;
			// 
			// cmd4Back
			// 
			this.cmd4Back.AppearanceKey = "actionButton";
			this.cmd4Back.Location = new System.Drawing.Point(0, 322);
			this.cmd4Back.Name = "cmd4Back";
			this.cmd4Back.Size = new System.Drawing.Size(108, 40);
			this.cmd4Back.TabIndex = 16;
			this.cmd4Back.Text = "<-- Back";
			this.ToolTip1.SetToolTip(this.cmd4Back, null);
			this.cmd4Back.Click += new System.EventHandler(this.cmd4Back_Click);
			// 
			// GridTotals
			// 
			this.GridTotals.AllowSelection = false;
			this.GridTotals.AllowUserToResizeColumns = false;
			this.GridTotals.AllowUserToResizeRows = false;
			this.GridTotals.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridTotals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridTotals.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridTotals.BackColorBkg = System.Drawing.Color.Empty;
			this.GridTotals.BackColorFixed = System.Drawing.Color.Empty;
			this.GridTotals.BackColorSel = System.Drawing.Color.Empty;
			this.GridTotals.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridTotals.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridTotals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridTotals.ColumnHeadersHeight = 30;
			this.GridTotals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridTotals.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridTotals.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridTotals.DragIcon = null;
			this.GridTotals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridTotals.ExtendLastCol = true;
			this.GridTotals.FixedCols = 3;
			this.GridTotals.FixedRows = 0;
			this.GridTotals.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridTotals.FrozenCols = 0;
			this.GridTotals.GridColor = System.Drawing.Color.Empty;
			this.GridTotals.GridColorFixed = System.Drawing.Color.Empty;
			this.GridTotals.Location = new System.Drawing.Point(0, 260);
			this.GridTotals.Name = "GridTotals";
			this.GridTotals.OutlineCol = 0;
			this.GridTotals.ReadOnly = true;
			this.GridTotals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridTotals.RowHeightMin = 0;
			this.GridTotals.Rows = 1;
			this.GridTotals.ScrollTipText = null;
			this.GridTotals.ShowColumnVisibilityMenu = false;
			this.GridTotals.Size = new System.Drawing.Size(650, 42);
			this.GridTotals.StandardTab = true;
			this.GridTotals.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridTotals.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.GridTotals, null);
			// 
			// cmd4Next
			// 
			this.cmd4Next.AppearanceKey = "actionButton";
			this.cmd4Next.Location = new System.Drawing.Point(124, 322);
			this.cmd4Next.Name = "cmd4Next";
			this.cmd4Next.Size = new System.Drawing.Size(92, 40);
			this.cmd4Next.TabIndex = 17;
			this.cmd4Next.Text = "Finish";
			this.ToolTip1.SetToolTip(this.cmd4Next, null);
			this.cmd4Next.Click += new System.EventHandler(this.cmd4Next_Click);
			// 
			// GridAccounts
			// 
			this.GridAccounts.AllowSelection = false;
			this.GridAccounts.AllowUserToResizeColumns = false;
			this.GridAccounts.AllowUserToResizeRows = false;
			this.GridAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.GridAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.GridAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.GridAccounts.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridAccounts.Cols = 6;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridAccounts.ColumnHeadersHeight = 30;
			this.GridAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridAccounts.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridAccounts.DragIcon = null;
			this.GridAccounts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridAccounts.ExtendLastCol = true;
			this.GridAccounts.FixedCols = 0;
			this.GridAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridAccounts.FrozenCols = 0;
			this.GridAccounts.GridColor = System.Drawing.Color.Empty;
			this.GridAccounts.GridColorFixed = System.Drawing.Color.Empty;
			this.GridAccounts.Location = new System.Drawing.Point(0, 0);
			this.GridAccounts.Name = "GridAccounts";
			this.GridAccounts.OutlineCol = 0;
			this.GridAccounts.RowHeadersVisible = false;
			this.GridAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridAccounts.RowHeightMin = 0;
			this.GridAccounts.Rows = 1;
			this.GridAccounts.ScrollTipText = null;
			this.GridAccounts.ShowColumnVisibilityMenu = false;
			this.GridAccounts.Size = new System.Drawing.Size(650, 240);
			this.GridAccounts.StandardTab = true;
			this.GridAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridAccounts.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.GridAccounts, "Use Insert and Delete to add and remove accounts");
			this.GridAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridAccounts_ValidateEdit);
			this.GridAccounts.CurrentCellChanged += new System.EventHandler(this.GridAccounts_RowColChange);
			this.GridAccounts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridAccounts_MouseMoveEvent);
			this.GridAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.GridAccounts_KeyDownEvent);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.cmd2Back);
			this.Frame2.Controls.Add(this.cmbLoadBack);
			this.Frame2.Controls.Add(this.cmd2Next);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Location = new System.Drawing.Point(10, 263);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(632, 150);
			this.Frame2.TabIndex = 23;
			this.ToolTip1.SetToolTip(this.Frame2, null);
			this.Frame2.Visible = false;
			// 
			// cmd2Back
			// 
			this.cmd2Back.AppearanceKey = "actionButton";
			this.cmd2Back.Location = new System.Drawing.Point(20, 90);
			this.cmd2Back.Name = "cmd2Back";
			this.cmd2Back.Size = new System.Drawing.Size(104, 40);
			this.cmd2Back.TabIndex = 7;
			this.cmd2Back.Text = "<-- Back";
			this.ToolTip1.SetToolTip(this.cmd2Back, null);
			this.cmd2Back.Click += new System.EventHandler(this.cmd2Back_Click);
			// 
			// cmd2Next
			// 
			this.cmd2Next.AppearanceKey = "actionButton";
			this.cmd2Next.Location = new System.Drawing.Point(144, 90);
			this.cmd2Next.Name = "cmd2Next";
			this.cmd2Next.Size = new System.Drawing.Size(104, 40);
			this.cmd2Next.TabIndex = 8;
			this.cmd2Next.Text = "Next -->";
			this.ToolTip1.SetToolTip(this.cmd2Next, null);
			this.cmd2Next.Click += new System.EventHandler(this.cmd2Next_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(466, 15);
			this.Label2.TabIndex = 24;
			this.Label2.Text = "DO YOU NEED TO APPLY PREVIOUS PAY RECORDS TOWARDS THIS CONTRACT?";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Frame3
			// 
			this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.cmd3Next);
			this.Frame3.Controls.Add(this.cmd3Back);
			this.Frame3.Controls.Add(this.GridPay);
			this.Frame3.Location = new System.Drawing.Point(30, 278);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(673, 330);
			this.Frame3.TabIndex = 26;
			this.ToolTip1.SetToolTip(this.Frame3, null);
			this.Frame3.Visible = false;
			// 
			// cmd3Next
			// 
			this.cmd3Next.AppearanceKey = "actionButton";
			this.cmd3Next.Location = new System.Drawing.Point(126, 290);
			this.cmd3Next.Name = "cmd3Next";
			this.cmd3Next.Size = new System.Drawing.Size(104, 40);
			this.cmd3Next.TabIndex = 12;
			this.cmd3Next.Text = "Next -->";
			this.ToolTip1.SetToolTip(this.cmd3Next, null);
			this.cmd3Next.Click += new System.EventHandler(this.cmd3Next_Click);
			// 
			// cmd3Back
			// 
			this.cmd3Back.AppearanceKey = "actionButton";
			this.cmd3Back.Location = new System.Drawing.Point(0, 290);
			this.cmd3Back.Name = "cmd3Back";
			this.cmd3Back.Size = new System.Drawing.Size(106, 40);
			this.cmd3Back.TabIndex = 11;
			this.cmd3Back.Text = "<-- Back";
			this.ToolTip1.SetToolTip(this.cmd3Back, null);
			this.cmd3Back.Click += new System.EventHandler(this.cmd3Back_Click);
			// 
			// GridPay
			// 
			this.GridPay.AllowSelection = false;
			this.GridPay.AllowUserToResizeColumns = false;
			this.GridPay.AllowUserToResizeRows = false;
			this.GridPay.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridPay.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridPay.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridPay.BackColorBkg = System.Drawing.Color.Empty;
			this.GridPay.BackColorFixed = System.Drawing.Color.Empty;
			this.GridPay.BackColorSel = System.Drawing.Color.Empty;
			this.GridPay.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridPay.Cols = 4;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridPay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.GridPay.ColumnHeadersHeight = 30;
			this.GridPay.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridPay.DefaultCellStyle = dataGridViewCellStyle6;
			this.GridPay.DragIcon = null;
			this.GridPay.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridPay.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridPay.ExtendLastCol = true;
			this.GridPay.FixedCols = 3;
			this.GridPay.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridPay.FrozenCols = 0;
			this.GridPay.GridColor = System.Drawing.Color.Empty;
			this.GridPay.GridColorFixed = System.Drawing.Color.Empty;
			this.GridPay.Location = new System.Drawing.Point(0, 0);
			this.GridPay.Name = "GridPay";
			this.GridPay.OutlineCol = 0;
			this.GridPay.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridPay.RowHeightMin = 0;
			this.GridPay.Rows = 1;
			this.GridPay.ScrollTipText = null;
			this.GridPay.ShowColumnVisibilityMenu = false;
			this.GridPay.Size = new System.Drawing.Size(642, 270);
			this.GridPay.StandardTab = true;
			this.GridPay.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridPay.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridPay.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.GridPay, null);
			this.GridPay.Click += new System.EventHandler(this.GridPay_ClickEvent);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.cmd1Next);
			this.Frame1.Controls.Add(this.txtDescription);
			this.Frame1.Controls.Add(this.t2kStartDate);
			this.Frame1.Controls.Add(this.t2kEndDate);
			this.Frame1.Controls.Add(this.Label1_0);
			this.Frame1.Controls.Add(this.Label1_3);
			this.Frame1.Controls.Add(this.Label1_4);
			this.Frame1.Location = new System.Drawing.Point(10, 10);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(416, 255);
			this.Frame1.TabIndex = 19;
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// cmd1Next
			// 
			this.cmd1Next.AppearanceKey = "actionButton";
			this.cmd1Next.Enabled = false;
			this.cmd1Next.Location = new System.Drawing.Point(20, 210);
			this.cmd1Next.Name = "cmd1Next";
			this.cmd1Next.Size = new System.Drawing.Size(104, 40);
			this.cmd1Next.TabIndex = 3;
			this.cmd1Next.Text = "Next -->";
			this.ToolTip1.SetToolTip(this.cmd1Next, null);
			this.cmd1Next.Click += new System.EventHandler(this.cmd1Next_Click);
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
			this.txtDescription.Location = new System.Drawing.Point(166, 30);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(230, 40);
			this.txtDescription.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
			// 
			// t2kStartDate
			// 
			this.t2kStartDate.Location = new System.Drawing.Point(166, 90);
			this.t2kStartDate.Mask = "00/00/0000";
			this.t2kStartDate.MaxLength = 10;
			this.t2kStartDate.Name = "t2kStartDate";
			this.t2kStartDate.Size = new System.Drawing.Size(115, 40);
			this.t2kStartDate.TabIndex = 1;
			this.t2kStartDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.t2kStartDate, null);
			this.t2kStartDate.TextChanged += new System.EventHandler(this.t2kStartDate_Change);
			// 
			// t2kEndDate
			// 
			this.t2kEndDate.Location = new System.Drawing.Point(166, 150);
			this.t2kEndDate.Mask = "00/00/0000";
			this.t2kEndDate.MaxLength = 10;
			this.t2kEndDate.Name = "t2kEndDate";
			this.t2kEndDate.Size = new System.Drawing.Size(115, 40);
			this.t2kEndDate.TabIndex = 2;
			this.t2kEndDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.t2kEndDate, null);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(20, 44);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(99, 15);
			this.Label1_0.TabIndex = 22;
			this.Label1_0.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// Label1_3
			// 
			this.Label1_3.Location = new System.Drawing.Point(20, 104);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(94, 15);
			this.Label1_3.TabIndex = 21;
			this.Label1_3.Text = "START DATE";
			this.ToolTip1.SetToolTip(this.Label1_3, null);
			// 
			// Label1_4
			// 
			this.Label1_4.Location = new System.Drawing.Point(20, 164);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(77, 15);
			this.Label1_4.TabIndex = 20;
			this.Label1_4.Text = "END DATE";
			this.ToolTip1.SetToolTip(this.Label1_4, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Enabled = false;
			this.mnuSaveExit.Index = 0;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveExit
			// 
			this.cmdSaveExit.AppearanceKey = "acceptButton";
			this.cmdSaveExit.Enabled = false;
			this.cmdSaveExit.Location = new System.Drawing.Point(281, 30);
			this.cmdSaveExit.Name = "cmdSaveExit";
			this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveExit.Size = new System.Drawing.Size(134, 48);
			this.cmdSaveExit.TabIndex = 0;
			this.cmdSaveExit.Text = "Save & Exit";
			this.ToolTip1.SetToolTip(this.cmdSaveExit, null);
			this.cmdSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// frmAddContract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(706, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmAddContract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Add Contract";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmAddContract_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAddContract_KeyDown);
			this.Resize += new System.EventHandler(this.frmAddContract_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmd4Back)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmd4Next)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmd2Back)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmd2Next)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmd3Next)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmd3Back)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmd1Next)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSaveExit;
    }
}
