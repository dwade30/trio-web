﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPayrollDistribution
	{
		//=========================================================
		private int lngRecordID;
		private int lngRecordNumber;
		private bool boolUpdated;
		private bool boolDeleted;
		private string strEmployeeNumber = string.Empty;
		private string strAccountCode = string.Empty;
		private string strAccountNumber = string.Empty;
		private string strWorkComp = string.Empty;
		private int lngCategory;
		private string strMSRS = string.Empty;
		private string strWC = string.Empty;
		private int lngTaxCode;
		private int lngCD;
		private double dblBaseRate;
		private double dblFactor;
		private int intNumberWeeks;
		private double dblDefaultHours;
		private double dblHoursWeek;
		private double dblGross;
		private string strDistU = string.Empty;
		private string strStatusCode = string.Empty;
		private string strGrantFunded = string.Empty;
		private int lngMSRSID;
		private int lngProject;
		private int intWeeksTaxWithheld;
		private int lngContractID;

		public double DefaultHours
		{
			set
			{
				dblDefaultHours = value;
			}
			get
			{
				double DefaultHours = 0;
				DefaultHours = dblDefaultHours;
				return DefaultHours;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int RecordNumber
		{
			set
			{
				lngRecordNumber = value;
				IsUpdated = true;
			}
			get
			{
				int RecordNumber = 0;
				RecordNumber = lngRecordNumber;
				return RecordNumber;
			}
		}

		public bool Deleted
		{
			set
			{
				IsUpdated = true;
				boolDeleted = value;
			}
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				IsUpdated = true;
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public string AccountCode
		{
			set
			{
				IsUpdated = true;
				strAccountCode = value;
			}
			get
			{
				string AccountCode = "";
				AccountCode = strAccountCode;
				return AccountCode;
			}
		}

		public string AccountNumber
		{
			set
			{
				IsUpdated = true;
				strAccountNumber = value;
			}
			get
			{
				string AccountNumber = "";
				AccountNumber = strAccountNumber;
				return AccountNumber;
			}
		}

		public string WorkComp
		{
			set
			{
				strWorkComp = value;
				IsUpdated = true;
			}
			get
			{
				string WorkComp = "";
				WorkComp = strWorkComp;
				return WorkComp;
			}
		}

		public int Category
		{
			set
			{
				lngCategory = value;
				IsUpdated = true;
			}
			get
			{
				int Category = 0;
				Category = lngCategory;
				return Category;
			}
		}

		public string MSRS
		{
			set
			{
				strMSRS = value;
				IsUpdated = true;
			}
			get
			{
				string MSRS = "";
				MSRS = strMSRS;
				return MSRS;
			}
		}

		public string WC
		{
			set
			{
				strWC = value;
				IsUpdated = true;
			}
			get
			{
				string WC = "";
				WC = strWC;
				return WC;
			}
		}

		public int TaxCode
		{
			set
			{
				lngTaxCode = value;
				IsUpdated = true;
			}
			get
			{
				int TaxCode = 0;
				TaxCode = lngTaxCode;
				return TaxCode;
			}
		}

		public int CD
		{
			set
			{
				lngCD = value;
				IsUpdated = true;
			}
			get
			{
				int CD = 0;
				CD = lngCD;
				return CD;
			}
		}

		public double BaseRate
		{
			set
			{
				IsUpdated = true;
				dblBaseRate = value;
			}
			get
			{
				double BaseRate = 0;
				BaseRate = dblBaseRate;
				return BaseRate;
			}
		}

		public double Factor
		{
			set
			{
				IsUpdated = true;
				dblFactor = value;
			}
			get
			{
				double Factor = 0;
				Factor = dblFactor;
				return Factor;
			}
		}

		public int NumberWeeks
		{
			set
			{
				IsUpdated = true;
				intNumberWeeks = value;
			}
			get
			{
				int NumberWeeks = 0;
				NumberWeeks = intNumberWeeks;
				return NumberWeeks;
			}
		}

		public double HoursWeek
		{
			set
			{
				IsUpdated = true;
				dblHoursWeek = value;
			}
			get
			{
				double HoursWeek = 0;
				HoursWeek = dblHoursWeek;
				return HoursWeek;
			}
		}

		public double Gross
		{
			set
			{
				IsUpdated = true;
				dblGross = value;
			}
			get
			{
				double Gross = 0;
				Gross = dblGross;
				return Gross;
			}
		}

		public string DistU
		{
			set
			{
				IsUpdated = true;
				strDistU = value;
			}
			get
			{
				string DistU = "";
				DistU = strDistU;
				return DistU;
			}
		}

		public string StatusCode
		{
			set
			{
				strStatusCode = value;
				IsUpdated = true;
			}
			get
			{
				string StatusCode = "";
				StatusCode = strStatusCode;
				return StatusCode;
			}
		}

		public string GrantFunded
		{
			set
			{
				IsUpdated = true;
				strGrantFunded = value;
			}
			get
			{
				string GrantFunded = "";
				GrantFunded = strGrantFunded;
				return GrantFunded;
			}
		}

		public int MSRSID
		{
			set
			{
				IsUpdated = true;
				lngMSRSID = value;
			}
			get
			{
				int MSRSID = 0;
				MSRSID = lngMSRSID;
				return MSRSID;
			}
		}

		public int Project
		{
			set
			{
				IsUpdated = true;
				lngProject = value;
			}
			get
			{
				int Project = 0;
				Project = lngProject;
				return Project;
			}
		}

		public int WeeksTaxWithheld
		{
			set
			{
				IsUpdated = true;
				intWeeksTaxWithheld = value;
			}
			get
			{
				int WeeksTaxWithheld = 0;
				WeeksTaxWithheld = intWeeksTaxWithheld;
				return WeeksTaxWithheld;
			}
		}

		public int ContractID
		{
			set
			{
				lngContractID = value;
				IsUpdated = true;
			}
			get
			{
				int ContractID = 0;
				ContractID = lngContractID;
				return ContractID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}
	}
}
