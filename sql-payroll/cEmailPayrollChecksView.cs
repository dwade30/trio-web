﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cEmailPayrollChecksView
	{
		//=========================================================
		private cEmployeeService employeeService = new cEmployeeService();
		private cCheckService checkService = new cCheckService();
		private string strExportFormat = string.Empty;
		private string dtPayDate = "";
		private int intPayRun;
		private bool boolAllNonNegotiable;
        private bool boolShowPayPeriodOnStub;
		private cEmployeeChecksReport employeeChecks;
		private cSettingsController setCont = new cSettingsController();
		private int lngCheckFormat;
		private string strExportDir = string.Empty;
		private cChecksToEmailView checkSelector;
		rptEmailPayrollCheck rECheck = null;
        public event EventHandler ChecksLoaded;
		public string ExportDirectory
		{
			set
			{
				strExportDir = value;
			}
			get
			{
				string ExportDirectory = "";
				ExportDirectory = strExportDir;
				return ExportDirectory;
			}
		}

		public string ExportFormat
		{
			set
			{
				strExportFormat = value;
			}
			get
			{
				string ExportFormat = "";
				ExportFormat = strExportFormat;
				return ExportFormat;
			}
		}

		public string PayDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					dtPayDate = value;
				}
				else
				{
					dtPayDate = "";
				}
			}
			get
			{
				string PayDate = "";
				PayDate = dtPayDate;
				return PayDate;
			}
		}

		public int PayRun
		{
			set
			{
				intPayRun = value;
			}
			get
			{
				int PayRun = 0;
				PayRun = intPayRun;
				return PayRun;
			}
		}

		public int CheckFormat
		{
			set
			{
				lngCheckFormat = value;
			}
			get
			{
				int CheckFormat = 0;
				CheckFormat = lngCheckFormat;
				return CheckFormat;
			}
		}

        public bool ShowPayPeriodOnStub
        {
            set
            {
                boolShowPayPeriodOnStub = value;
            }
            get
            {
                return boolShowPayPeriodOnStub;
            }
        }
		public void BuildChecksReport(string strExpDir, FCForm parentForm)
		{
			//frmBusy fb = new frmBusy();
			FCUtils.StartTask(parentForm, () =>
			{
				//fb.StartBusy();
				employeeChecks = new cEmployeeChecksReport();
				cGenericCollection selectedEmployees = new cGenericCollection();
				selectedEmployees = employeeService.GetEmployeesForEmail();
				employeeChecks.PayDate = FCConvert.ToDateTime(dtPayDate);
				employeeChecks.PayrunNumber = intPayRun;
				employeeChecks.AllNonNegotiable = true;
                employeeChecks.ShowPayPeriodOnStub = boolShowPayPeriodOnStub;
				setCont.SaveSetting(FCConvert.ToString(lngCheckFormat), "PayrollEmailCheckFormat", "Payroll", "", "", "");
				setCont.SaveSetting(strExportFormat, "PayrollCheckEmailFormat", "Payroll", "", "", "");
				employeeChecks.CheckFormatID = lngCheckFormat;
				checkService.FillEmployeeChecksReport(ref employeeChecks, ref selectedEmployees);
				GetChecksToUse();
                ChecksLoaded?.Invoke(this,new EventArgs());
                //FCUtils.UnlockUserInterface();
			});
			//fb.Show(FCForm.FormShowEnum.Modal);
		}

		private void GetChecksToUse()
		{
			checkSelector = new cChecksToEmailView();
            checkSelector.Cancelled += new cChecksToEmailView.CancelledEventHandler(checkSelector_Cancelled);
            checkSelector.SelectionsCommitted += new cChecksToEmailView.SelectionsCommittedEventHandler(checkSelector_SelectionsCommitted);
			checkSelector.Checks.ClearList();
			employeeChecks.employeeChecks.MoveFirst();
			while (employeeChecks.employeeChecks.IsCurrent())
			{
				//App.DoEvents();
				checkSelector.Checks.AddItem(employeeChecks.employeeChecks.GetCurrentItem());
				employeeChecks.employeeChecks.MoveNext();
			}
			//fb.StopBusy();
			//fb.Close();
			//FCUtils.UnlockUserInterface();
			frmChecksToEmail ctoe = new frmChecksToEmail();
			ctoe.SetView(ref checkSelector);
			//ctoe.Show(FCForm.FormShowEnum.Modeless);
            ctoe.Show();
        }

		public void EmailChecks()
		{
			/*- rECheck = null; */
			rECheck = new rptEmailPayrollCheck();
            rECheck.SentEmails += new rptEmailPayrollCheck.SentEmailsEventHandler(rECheck_SentEmails);
			rECheck.Init(ref employeeChecks, strExportDir, strExportFormat == "html");
			rECheck.Run();
		}

		public void rECheck_SentEmails()
		{
			EmailListReport();
		}

		private void EmailListReport()
		{
			rptEmailList rEList = new rptEmailList();
			rEList.Init(ref employeeChecks);
        }

		private void checkSelector_Cancelled()
		{
            checkSelector.Cancelled -= checkSelector_Cancelled;
            checkSelector.SelectionsCommitted -= checkSelector_SelectionsCommitted;
            checkSelector = null;
		}

		private void checkSelector_SelectionsCommitted()
		{
			if (!(employeeChecks == null))
			{
				employeeChecks.employeeChecks.MoveFirst();
				cEmployeeCheck eCheck;
				while (employeeChecks.employeeChecks.IsCurrent())
				{
					eCheck = (cEmployeeCheck)employeeChecks.employeeChecks.GetCurrentItem();
					if (!checkSelector.SelectedChecks.ContainsKey(eCheck.ID))
					{
						employeeChecks.employeeChecks.RemoveCurrent();
					}
					employeeChecks.employeeChecks.MoveNext();
				}
			}
            checkSelector.Cancelled -= checkSelector_Cancelled;
            checkSelector.SelectionsCommitted -= checkSelector_SelectionsCommitted;
            checkSelector = null;
			if (employeeChecks.employeeChecks.ItemCount() > 0)
			{
				EmailChecks();
			}
			else
			{
				MessageBox.Show("You haven't selected anything to email", "Nothing Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public cEmailPayrollChecksView() : base()
		{
			strExportFormat = "PDF";
		}
	}
}
