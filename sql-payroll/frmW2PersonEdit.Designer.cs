//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2PersonEdit.
	/// </summary>
	partial class frmW2PersonEdit
	{
		public fecherFoundation.FCFrame fraMatch;
		public fecherFoundation.FCGrid vsMatch;
		public fecherFoundation.FCFrame fraDeductions;
		public fecherFoundation.FCGrid vsDeductions;
		public fecherFoundation.FCGrid vsData;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddDeduction;
		public fecherFoundation.FCToolStripMenuItem mnuAddEmpMatchCode;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteDeductionCode;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteMatch;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem SaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.fraMatch = new fecherFoundation.FCFrame();
            this.vsMatch = new fecherFoundation.FCGrid();
            this.fraDeductions = new fecherFoundation.FCFrame();
            this.vsDeductions = new fecherFoundation.FCGrid();
            this.vsData = new fecherFoundation.FCGrid();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuDeleteDeductionCode = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteMatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddDeduction = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddEmpMatchCode = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.SaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cmdAddDeductionCode = new fecherFoundation.FCButton();
            this.cmdEmployersMatch = new fecherFoundation.FCButton();
            this.fcButton1 = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMatch)).BeginInit();
            this.fraMatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeductions)).BeginInit();
            this.fraDeductions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddDeductionCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEmployersMatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 648);
            this.BottomPanel.Size = new System.Drawing.Size(1048, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraMatch);
            this.ClientArea.Controls.Add(this.fraDeductions);
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Size = new System.Drawing.Size(1068, 606);
            this.ClientArea.Controls.SetChildIndex(this.vsData, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraDeductions, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraMatch, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddDeductionCode);
            this.TopPanel.Controls.Add(this.cmdEmployersMatch);
            this.TopPanel.Controls.Add(this.fcButton1);
            this.TopPanel.Size = new System.Drawing.Size(1068, 60);
            this.TopPanel.Controls.SetChildIndex(this.fcButton1, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEmployersMatch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddDeductionCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(296, 30);
            this.HeaderText.Text = "W2 Employee Information";
            // 
            // fraMatch
            // 
            this.fraMatch.AppearanceKey = "groupBoxNoBorders";
            this.fraMatch.Controls.Add(this.vsMatch);
            this.fraMatch.Location = new System.Drawing.Point(538, 418);
            this.fraMatch.Name = "fraMatch";
            this.fraMatch.Size = new System.Drawing.Size(451, 230);
            this.fraMatch.TabIndex = 2;
            this.fraMatch.Text = "Employer\'s Matches";
            // 
            // vsMatch
            // 
            this.vsMatch.Anchor = Wisej.Web.AnchorStyles.Left;
            this.vsMatch.Cols = 10;
            this.vsMatch.FixedCols = 0;
            this.vsMatch.Location = new System.Drawing.Point(0, 30);
            this.vsMatch.Name = "vsMatch";
            this.vsMatch.RowHeadersVisible = false;
            this.vsMatch.Rows = 50;
            this.vsMatch.Size = new System.Drawing.Size(451, 200);
            this.vsMatch.StandardTab = false;
            this.vsMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsMatch.TabIndex = 4;
            this.vsMatch.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsMatch_AfterEdit);
            this.vsMatch.CurrentCellChanged += new System.EventHandler(this.vsMatch_RowColChange);
            // 
            // fraDeductions
            // 
            this.fraDeductions.AppearanceKey = "groupBoxNoBorders";
            this.fraDeductions.Controls.Add(this.vsDeductions);
            this.fraDeductions.Location = new System.Drawing.Point(28, 418);
            this.fraDeductions.Name = "fraDeductions";
            this.fraDeductions.Size = new System.Drawing.Size(462, 230);
            this.fraDeductions.TabIndex = 1;
            this.fraDeductions.Text = "Employee Deductions";
            // 
            // vsDeductions
            // 
            this.vsDeductions.Cols = 10;
            this.vsDeductions.FixedCols = 0;
            this.vsDeductions.Location = new System.Drawing.Point(0, 30);
            this.vsDeductions.Name = "vsDeductions";
            this.vsDeductions.RowHeadersVisible = false;
            this.vsDeductions.Rows = 50;
            this.vsDeductions.Size = new System.Drawing.Size(462, 200);
            this.vsDeductions.StandardTab = false;
            this.vsDeductions.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDeductions.TabIndex = 3;
            this.vsDeductions.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_AfterEdit);
            this.vsDeductions.CurrentCellChanged += new System.EventHandler(this.vsDeductions_RowColChange);
            // 
            // vsData
            // 
            this.vsData.Cols = 10;
            this.vsData.FixedCols = 0;
            this.vsData.Location = new System.Drawing.Point(30, 30);
            this.vsData.Name = "vsData";
            this.vsData.RowHeadersVisible = false;
            this.vsData.Rows = 50;
            this.vsData.Size = new System.Drawing.Size(959, 368);
            this.vsData.StandardTab = false;
            this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsData.TabIndex = 3;
            this.vsData.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsData_KeyPressEdit);
            this.vsData.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsData_MouseMoveEvent);
            this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
            this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDeleteDeductionCode,
            this.mnuDeleteMatch});
            this.MainMenu1.Name = null;
            // 
            // mnuDeleteDeductionCode
            // 
            this.mnuDeleteDeductionCode.Index = 0;
            this.mnuDeleteDeductionCode.Name = "mnuDeleteDeductionCode";
            this.mnuDeleteDeductionCode.Text = "Delete Deduction Code";
            this.mnuDeleteDeductionCode.Click += new System.EventHandler(this.mnuDeleteDeductionCode_Click);
            // 
            // mnuDeleteMatch
            // 
            this.mnuDeleteMatch.Index = 1;
            this.mnuDeleteMatch.Name = "mnuDeleteMatch";
            this.mnuDeleteMatch.Text = "Delete Employer Match Code";
            this.mnuDeleteMatch.Click += new System.EventHandler(this.mnuDeleteMatch_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuAddDeduction
            // 
            this.mnuAddDeduction.Index = -1;
            this.mnuAddDeduction.Name = "mnuAddDeduction";
            this.mnuAddDeduction.Text = "Add Deduction Code";
            this.mnuAddDeduction.Click += new System.EventHandler(this.mnuAddDeduction_Click);
            // 
            // mnuAddEmpMatchCode
            // 
            this.mnuAddEmpMatchCode.Index = -1;
            this.mnuAddEmpMatchCode.Name = "mnuAddEmpMatchCode";
            this.mnuAddEmpMatchCode.Text = "Add Employers Match Code";
            this.mnuAddEmpMatchCode.Click += new System.EventHandler(this.mnuAddEmpMatchCode_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuDeleteEmployee
            // 
            this.mnuDeleteEmployee.Index = -1;
            this.mnuDeleteEmployee.Name = "mnuDeleteEmployee";
            this.mnuDeleteEmployee.Text = "Delete Entire Employee";
            this.mnuDeleteEmployee.Click += new System.EventHandler(this.mnuDeleteEmployee_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // SaveContinue
            // 
            this.SaveContinue.Index = -1;
            this.SaveContinue.Name = "SaveContinue";
            this.SaveContinue.Shortcut = Wisej.Web.Shortcut.F11;
            this.SaveContinue.Text = "Save";
            this.SaveContinue.Click += new System.EventHandler(this.SaveContinue_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Save & Continue";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP4
            // 
            this.mnuSP4.Index = -1;
            this.mnuSP4.Name = "mnuSP4";
            this.mnuSP4.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(383, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(80, 48);
            this.cmdProcess.Text = "Save";
            this.cmdProcess.Click += new System.EventHandler(this.SaveContinue_Click);
            // 
            // cmdAddDeductionCode
            // 
            this.cmdAddDeductionCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddDeductionCode.Location = new System.Drawing.Point(523, 29);
            this.cmdAddDeductionCode.Name = "cmdAddDeductionCode";
            this.cmdAddDeductionCode.Size = new System.Drawing.Size(149, 24);
            this.cmdAddDeductionCode.TabIndex = 1;
            this.cmdAddDeductionCode.Text = "Add Deduction Code";
            this.cmdAddDeductionCode.Click += new System.EventHandler(this.mnuAddDeduction_Click);
            // 
            // cmdEmployersMatch
            // 
            this.cmdEmployersMatch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEmployersMatch.Location = new System.Drawing.Point(683, 29);
            this.cmdEmployersMatch.Name = "cmdEmployersMatch";
            this.cmdEmployersMatch.Size = new System.Drawing.Size(190, 24);
            this.cmdEmployersMatch.TabIndex = 2;
            this.cmdEmployersMatch.Text = "Add Employers Match Code";
            this.cmdEmployersMatch.Click += new System.EventHandler(this.mnuAddEmpMatchCode_Click);
            // 
            // fcButton1
            // 
            this.fcButton1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.fcButton1.Location = new System.Drawing.Point(874, 29);
            this.fcButton1.Name = "fcButton1";
            this.fcButton1.Size = new System.Drawing.Size(166, 24);
            this.fcButton1.TabIndex = 3;
            this.fcButton1.Text = "Delete Entire Employee";
            this.fcButton1.Click += new System.EventHandler(this.mnuDeleteEmployee_Click);
            // 
            // frmW2PersonEdit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1068, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmW2PersonEdit";
            this.Text = "W2 Employee Information";
            this.Load += new System.EventHandler(this.frmW2PersonEdit_Load);
            this.Activated += new System.EventHandler(this.frmW2PersonEdit_Activated);
            this.FormClosed += new Wisej.Web.FormClosedEventHandler(this.Form_Unload);
            this.Resize += new System.EventHandler(this.frmW2PersonEdit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2PersonEdit_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmW2PersonEdit_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMatch)).EndInit();
            this.fraMatch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeductions)).EndInit();
            this.fraDeductions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddDeductionCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEmployersMatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcButton1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
		private FCButton cmdEmployersMatch;
		private FCButton cmdAddDeductionCode;
		private FCButton fcButton1;
	}
}