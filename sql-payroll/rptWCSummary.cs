//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptWCSummary.
	/// </summary>
	public partial class rptWCSummary : BaseSectionReport
	{
		public rptWCSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Workers Compensation Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptWCSummary InstancePtr
		{
			get
			{
				return (rptWCSummary)Sys.GetInstance(typeof(rptWCSummary));
			}
		}

		protected rptWCSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsMTDData?.Dispose();
				rsEmployeeData?.Dispose();
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWCSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// **********************************************************************************
		// PROPERTY OF TRIO SOFTWARE
		// WRITTEN BY MATTHEW LARRABEE
		// JUNE 6/3/2005
		//
		// THIS REPORT WILL SHOW THE SUM OF ALL DISTRIBUTION LINES FROM THE CHECK DETAIL TABLE
		// THAT HAVE A W/C CODE AND GROUP THEM ALL TOGETHER AND SHOW THEM AS A SUM.
		// THEY WILL BE SORTED BY THE WC CODE SO AN EMPLOYEE COULD HAVE MORE THEN ONE ENTRY
		// **********************************************************************************
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsMTDData = new clsDRWrapper();
		private int intMonth;
		private int lngYear;
		private int intpage;
		private DateTime datLowDate;
		private DateTime datHighDate;
		private DateTime dtStartDate;
		private DateTime dtQTREndDate;
		private DateTime dtQTRStartDate;
		private double MTDTotal;
		private double QTDTotal;
		private double CYTDTotal;
		private int intCounter;
		private int intQuarter;
		private string strSQL = string.Empty;
		private string strEmployee = "";
		private bool boolSpace;
		private bool boolSecondShowing;
		private bool boolLastTotal;
		private clsDRWrapper rsEmployeeData = new clsDRWrapper();
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsEmployeeData.OpenRecordset("Select * from tblEmployeeMaster");
			// MTD Totals
			// strSQL = "SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode, Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay From tblCheckDetail WHERE paydate >= '" & gdatStart & "' and paydate <= '" & gdatEnd & "' and checkvoid = 0 GROUP BY tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode HAVING (((tblCheckDetail.DistWCCode)<>'N' And (tblCheckDetail.DistWCCode)<>'')) Order by tblCheckDetail.DistWCCode"
			// strSQL = "SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode, Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay, tblEmployeeMaster.WorkCompCode FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where PayDate >= '" & gdatStart & "' and paydate <= '" & gdatEnd & "' And tblCheckDetail.CheckVoid = 0 GROUP BY tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode, tblEmployeeMaster.WorkCompCode HAVING (((tblCheckDetail.DistWCCode)<>'N' And (tblCheckDetail.DistWCCode)<>'')) ORDER BY WorkCompCode"
			strSQL = "SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.DistWCCode, Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay, tblEmployeeMaster.WorkCompCode FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where PayDate >= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatStart) + "' and paydate <= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatEnd) + "' And tblCheckDetail.CheckVoid = 0 GROUP BY tblCheckDetail.EmployeeNumber,  tblCheckDetail.DistWCCode, tblEmployeeMaster.WorkCompCode HAVING (((tblCheckDetail.DistWCCode)<>'N' And (tblCheckDetail.DistWCCode)<>'')) ORDER BY WorkCompCode";
			rsMTDData.OpenRecordset(strSQL);
			if (!rsMTDData.EndOfFile())
				strEmployee = FCConvert.ToString(rsMTDData.Get_Fields_String("WorkCompCode"));
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		private object GetEmployeeWCCode(string strEmployeeNumber)
		{
			object GetEmployeeWCCode = null;
			if (rsEmployeeData.FindFirstRecord("EmployeeNumber", strEmployeeNumber))
			{
				GetEmployeeWCCode = rsEmployeeData.Get_Fields_String("WorkCompCode");
			}
			else
			{
				GetEmployeeWCCode = "";
			}
			return GetEmployeeWCCode;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
			lblDate.Text = FCConvert.ToString(modGlobalVariables.Statics.gdatStart) + " - " + FCConvert.ToString(modGlobalVariables.Statics.gdatEnd);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				while (!rsMTDData.EndOfFile())
				{
					if (!employeeDict.ContainsKey(rsMTDData.Get_Fields("EmployeeNumber")))
					{
						rsMTDData.MoveNext();
					}
					else
					{
						break;
					}
				}
				if (rsMTDData.EndOfFile())
				{
					if (boolLastTotal)
					{
						eArgs.EOF = true;
						return;
					}
					else
					{
						boolLastTotal = true;
						Line1.Visible = true;
						lblTotal.Visible = true;
						txtMTD.Text = Strings.Format(MTDTotal, "0.00");
						txtQTD.Text = Strings.Format(QTDTotal, "0.00");
						txtCYTD.Text = Strings.Format(CYTDTotal, "0.00");
						txtEmployee.Text = string.Empty;
						txtWCCode.Text = string.Empty;
						MTDTotal = 0;
						QTDTotal = 0;
						CYTDTotal = 0;
						if (!rsMTDData.EndOfFile())
							strEmployee = FCConvert.ToString(rsMTDData.Get_Fields_String("WorkCompCode"));
						boolSpace = true;
						eArgs.EOF = false;
						return;
					}
				}
				if (boolSpace)
				{
					// MAKE THE TOTAL LINE NOT SHOW IF THIS IS A SPACE.
					Line1.Visible = false;
					lblTotal.Visible = false;
					txtMTD.Text = string.Empty;
					txtQTD.Text = string.Empty;
					txtCYTD.Text = string.Empty;
					txtEmployee.Text = string.Empty;
					txtWCCode.Text = string.Empty;
					boolSpace = false;
					eArgs.EOF = false;
					return;
				}
				if (strEmployee == FCConvert.ToString(rsMTDData.Get_Fields_String("WorkCompCode")))
				{
					Line1.Visible = false;
					lblTotal.Visible = false;
					txtEmployee.Text = rsMTDData.Get_Fields_String("EmployeeNumber");
					if (rsEmployeeData.FindFirstRecord("Employeenumber", rsMTDData.Get_Fields("employeenumber")))
					{
						txtEmployee.Text = rsMTDData.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsEmployeeData.Get_Fields("firstname") + " " + rsEmployeeData.Get_Fields("lastname") + " " + rsEmployeeData.Get_Fields("desig"));
					}
					txtWCCode.Text = GetEmployeeWCCode(rsMTDData.Get_Fields("EmployeeNumber"));
					txtMTD.Text = Strings.Format(rsMTDData.Get_Fields("SumOfDistGrossPay"), "0.00");
					// QTD Totals
					strSQL = "SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode, Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay From tblCheckDetail WHERE paydate >= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatStart) + "' and paydate <= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatEnd) + "' and checkvoid = 0 GROUP BY tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode HAVING (tblCheckDetail.DistWCCode  = '" + rsMTDData.Get_Fields_String("DistWCCode") + "' AND EmployeeNumber = '" + rsMTDData.Get_Fields("EmployeeNumber") + "')";
					rsData.OpenRecordset(strSQL);
					txtQTD.Text = Strings.Format(rsData.Get_Fields("SumOfDistGrossPay"), "0.00");
					// CYTD Totals
					strSQL = "SELECT tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode, Sum(tblCheckDetail.DistGrossPay) AS SumOfDistGrossPay From tblCheckDetail WHERE paydate >= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatStart) + "' and paydate <= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatEnd) + "' and checkvoid = 0 GROUP BY tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.DistWCCode HAVING (tblCheckDetail.DistWCCode  = '" + rsMTDData.Get_Fields_String("DistWCCode") + "' AND EmployeeNumber = '" + rsMTDData.Get_Fields("EmployeeNumber") + "')";
					rsData.OpenRecordset(strSQL);
					txtCYTD.Text = Strings.Format(rsData.Get_Fields("SumOfDistGrossPay"), "0.00");
					MTDTotal += Conversion.Val(txtMTD.Text);
					QTDTotal += Conversion.Val(txtQTD.Text);
					CYTDTotal += Conversion.Val(txtCYTD.Text);
					eArgs.EOF = false;
					if (!rsMTDData.EndOfFile())
						rsMTDData.MoveNext();
				}
				else
				{
					LastTotal:
					;
					Line1.Visible = true;
					lblTotal.Visible = true;
					txtMTD.Text = Strings.Format(MTDTotal, "0.00");
					txtQTD.Text = Strings.Format(QTDTotal, "0.00");
					txtCYTD.Text = Strings.Format(CYTDTotal, "0.00");
					txtEmployee.Text = string.Empty;
					txtWCCode.Text = string.Empty;
					MTDTotal = 0;
					QTDTotal = 0;
					CYTDTotal = 0;
					if (!rsMTDData.EndOfFile())
						strEmployee = FCConvert.ToString(rsMTDData.Get_Fields_String("WorkCompCode"));
					boolSpace = true;
					eArgs.EOF = false;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "WCSummary");
		}

		
	}
}
