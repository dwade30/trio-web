//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeDeductions.
	/// </summary>
	partial class frmEmployeeDeductions
	{
		public FCGrid vsDeductions;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid vsTotals;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuHistory;
		public fecherFoundation.FCToolStripMenuItem mnuTemporaryOverrides;
		public fecherFoundation.FCToolStripMenuItem mnuSp2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSep4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuMultPays;
		public fecherFoundation.FCToolStripMenuItem mnuMultAdd;
		public fecherFoundation.FCToolStripMenuItem mnuCaption1;
		public fecherFoundation.FCToolStripMenuItem mnuCaption2;
		public fecherFoundation.FCToolStripMenuItem mnuCaption3;
		public fecherFoundation.FCToolStripMenuItem mnuCaption4;
		public fecherFoundation.FCToolStripMenuItem mnuCaption5;
		public fecherFoundation.FCToolStripMenuItem mnuCaption6;
		public fecherFoundation.FCToolStripMenuItem mnuCaption7;
		public fecherFoundation.FCToolStripMenuItem mnuCaption8;
		public fecherFoundation.FCToolStripMenuItem mnuCaption9;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.vsDeductions = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.vsTotals = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.mnuMultPays = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMultAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption7 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption8 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption9 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHistory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTemporaryOverrides = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSp2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdNewDeduction = new fecherFoundation.FCButton();
            this.cmdDeleteDeduction = new fecherFoundation.FCButton();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdTemporaryOverrides = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.cmdHistory = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewDeduction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDeduction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTemporaryOverrides)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 678);
            this.BottomPanel.Size = new System.Drawing.Size(948, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Size = new System.Drawing.Size(968, 628);
            this.ClientArea.Controls.SetChildIndex(this.lblEmployeeNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNewDeduction);
            this.TopPanel.Controls.Add(this.cmdDeleteDeduction);
            this.TopPanel.Controls.Add(this.cmdRefresh);
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Controls.Add(this.cmdHistory);
            this.TopPanel.Controls.Add(this.cmdTemporaryOverrides);
            this.TopPanel.Size = new System.Drawing.Size(968, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdTemporaryOverrides, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdHistory, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRefresh, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteDeduction, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewDeduction, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(124, 28);
            this.HeaderText.Text = "Deductions";
            // 
            // vsDeductions
            // 
            this.vsDeductions.AllowDrag = true;
            this.vsDeductions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDeductions.Cols = 10;
            this.vsDeductions.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
            this.vsDeductions.Location = new System.Drawing.Point(0, 30);
            this.vsDeductions.Name = "vsDeductions";
            this.vsDeductions.Rows = 50;
            this.vsDeductions.Size = new System.Drawing.Size(863, 270);
            this.vsDeductions.StandardTab = false;
            this.vsDeductions.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDeductions.TabIndex = 9;
            this.vsDeductions.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsDeductions_KeyDownEdit);
            this.vsDeductions.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsDeductions_KeyPressEdit);
            this.vsDeductions.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_AfterEdit);
            this.vsDeductions.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_ChangeEdit);
            this.vsDeductions.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDeductions_CellFormatting);
            this.vsDeductions.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsDeductions_ValidateEdit);
            this.vsDeductions.CurrentCellChanged += new System.EventHandler(this.vsDeductions_RowColChange);
            this.vsDeductions.KeyDown += new Wisej.Web.KeyEventHandler(this.vsDeductions_KeyDownEvent);
            this.vsDeductions.KeyUp += new Wisej.Web.KeyEventHandler(this.vsDeductions_KeyUpEvent);
            // 
            // Frame2
            // 
            this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.vsTotals);
            this.Frame2.Location = new System.Drawing.Point(30, 378);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(863, 300);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Totals";
            // 
            // vsTotals
            // 
            this.vsTotals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTotals.Cols = 10;
            this.vsTotals.Location = new System.Drawing.Point(0, 30);
            this.vsTotals.Name = "vsTotals";
            this.vsTotals.Rows = 50;
            this.vsTotals.Size = new System.Drawing.Size(863, 270);
            this.vsTotals.TabIndex = 10;
            this.vsTotals.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsTotals_KeyDownEdit);
            this.vsTotals.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsTotals_KeyPressEdit);
            this.vsTotals.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.vsTotals_BeforeRowColChange);
            this.vsTotals.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsTotals_AfterEdit);
            this.vsTotals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsTotals_KeyDownEvent);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsDeductions);
            this.Frame1.Location = new System.Drawing.Point(30, 57);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(863, 300);
            this.Frame1.TabIndex = 7;
            this.Frame1.Text = "Deductions";
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(30, 30);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(863, 15);
            this.lblEmployeeNumber.TabIndex = 6;
            this.lblEmployeeNumber.Tag = "Employee #";
            this.lblEmployeeNumber.Text = "EMPLOYEE #";
            // 
            // mnuMultPays
            // 
            this.mnuMultPays.Index = -1;
            this.mnuMultPays.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuMultAdd,
            this.mnuCaption1,
            this.mnuCaption2,
            this.mnuCaption3,
            this.mnuCaption4,
            this.mnuCaption5,
            this.mnuCaption6,
            this.mnuCaption7,
            this.mnuCaption8,
            this.mnuCaption9});
            this.mnuMultPays.Name = "mnuMultPays";
            this.mnuMultPays.Text = "Mult Pays";
            this.mnuMultPays.Visible = false;
            // 
            // mnuMultAdd
            // 
            this.mnuMultAdd.Enabled = false;
            this.mnuMultAdd.Index = 0;
            this.mnuMultAdd.Name = "mnuMultAdd";
            this.mnuMultAdd.Text = "Add/Edit Additional Pay Master";
            // 
            // mnuCaption1
            // 
            this.mnuCaption1.Index = 1;
            this.mnuCaption1.Name = "mnuCaption1";
            this.mnuCaption1.Text = "Caption1";
            this.mnuCaption1.Click += new System.EventHandler(this.mnuCaption1_Click);
            // 
            // mnuCaption2
            // 
            this.mnuCaption2.Index = 2;
            this.mnuCaption2.Name = "mnuCaption2";
            this.mnuCaption2.Text = "Caption2";
            this.mnuCaption2.Click += new System.EventHandler(this.mnuCaption2_Click);
            // 
            // mnuCaption3
            // 
            this.mnuCaption3.Index = 3;
            this.mnuCaption3.Name = "mnuCaption3";
            this.mnuCaption3.Text = "Caption3";
            this.mnuCaption3.Click += new System.EventHandler(this.mnuCaption3_Click);
            // 
            // mnuCaption4
            // 
            this.mnuCaption4.Index = 4;
            this.mnuCaption4.Name = "mnuCaption4";
            this.mnuCaption4.Text = "Caption4";
            this.mnuCaption4.Click += new System.EventHandler(this.mnuCaption4_Click);
            // 
            // mnuCaption5
            // 
            this.mnuCaption5.Index = 5;
            this.mnuCaption5.Name = "mnuCaption5";
            this.mnuCaption5.Text = "Caption5";
            this.mnuCaption5.Click += new System.EventHandler(this.mnuCaption5_Click);
            // 
            // mnuCaption6
            // 
            this.mnuCaption6.Index = 6;
            this.mnuCaption6.Name = "mnuCaption6";
            this.mnuCaption6.Text = "Caption6";
            this.mnuCaption6.Click += new System.EventHandler(this.mnuCaption6_Click);
            // 
            // mnuCaption7
            // 
            this.mnuCaption7.Index = 7;
            this.mnuCaption7.Name = "mnuCaption7";
            this.mnuCaption7.Text = "Caption7";
            this.mnuCaption7.Click += new System.EventHandler(this.mnuCaption7_Click);
            // 
            // mnuCaption8
            // 
            this.mnuCaption8.Index = 8;
            this.mnuCaption8.Name = "mnuCaption8";
            this.mnuCaption8.Text = "Caption8";
            this.mnuCaption8.Click += new System.EventHandler(this.mnuCaption8_Click);
            // 
            // mnuCaption9
            // 
            this.mnuCaption9.Index = 9;
            this.mnuCaption9.Name = "mnuCaption9";
            this.mnuCaption9.Text = "Caption9";
            this.mnuCaption9.Click += new System.EventHandler(this.mnuCaption9_Click);
            // 
            // mnuHistory
            // 
            this.mnuHistory.Index = -1;
            this.mnuHistory.Name = "mnuHistory";
            this.mnuHistory.Text = "Display History";
            this.mnuHistory.Visible = false;
            this.mnuHistory.Click += new System.EventHandler(this.mnuHistory_Click);
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = -1;
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Visible = false;
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = -1;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuNew.Text = "New Deduction";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Deduction";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = -1;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuTemporaryOverrides
            // 
            this.mnuTemporaryOverrides.Index = -1;
            this.mnuTemporaryOverrides.Name = "mnuTemporaryOverrides";
            this.mnuTemporaryOverrides.Text = "Temporary Overrides";
            this.mnuTemporaryOverrides.Click += new System.EventHandler(this.mnuTemporaryOverrides_Click);
            // 
            // mnuSp2
            // 
            this.mnuSp2.Index = -1;
            this.mnuSp2.Name = "mnuSp2";
            this.mnuSp2.Text = "-";
            this.mnuSp2.Visible = false;
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Visible = false;
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Visible = false;
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                    ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSep4
            // 
            this.mnuSep4.Index = -1;
            this.mnuSep4.Name = "mnuSep4";
            this.mnuSep4.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNewDeduction
            // 
            this.cmdNewDeduction.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewDeduction.Location = new System.Drawing.Point(226, 29);
            this.cmdNewDeduction.Name = "cmdNewDeduction";
            this.cmdNewDeduction.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdNewDeduction.Size = new System.Drawing.Size(114, 24);
            this.cmdNewDeduction.TabIndex = 1;
            this.cmdNewDeduction.Text = "New Deduction";
            this.cmdNewDeduction.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdDeleteDeduction
            // 
            this.cmdDeleteDeduction.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteDeduction.Location = new System.Drawing.Point(346, 29);
            this.cmdDeleteDeduction.Name = "cmdDeleteDeduction";
            this.cmdDeleteDeduction.Size = new System.Drawing.Size(126, 24);
            this.cmdDeleteDeduction.TabIndex = 2;
            this.cmdDeleteDeduction.Text = "Delete Deduction";
            this.cmdDeleteDeduction.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.Location = new System.Drawing.Point(548, 29);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelect.Size = new System.Drawing.Size(120, 24);
            this.cmdSelect.TabIndex = 3;
            this.cmdSelect.Text = "Select Employee";
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // cmdTemporaryOverrides
            // 
            this.cmdTemporaryOverrides.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdTemporaryOverrides.Location = new System.Drawing.Point(790, 29);
            this.cmdTemporaryOverrides.Name = "cmdTemporaryOverrides";
            this.cmdTemporaryOverrides.Size = new System.Drawing.Size(150, 24);
            this.cmdTemporaryOverrides.TabIndex = 4;
            this.cmdTemporaryOverrides.Text = "Temporary Overrides";
            this.cmdTemporaryOverrides.Click += new System.EventHandler(this.mnuTemporaryOverrides_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(429, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRefresh.Location = new System.Drawing.Point(478, 29);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(64, 24);
            this.cmdRefresh.TabIndex = 6;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Visible = false;
            this.cmdRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // cmdHistory
            // 
            this.cmdHistory.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdHistory.Location = new System.Drawing.Point(674, 29);
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Size = new System.Drawing.Size(110, 24);
            this.cmdHistory.TabIndex = 7;
            this.cmdHistory.Text = "Display History";
            this.cmdHistory.Visible = false;
            this.cmdHistory.Click += new System.EventHandler(this.mnuHistory_Click);
            // 
            // frmEmployeeDeductions
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(968, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeeDeductions";
            this.Text = "Deductions";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmEmployeeDeductions_Load);
            this.Activated += new System.EventHandler(this.frmEmployeeDeductions_Activated);
            this.Resize += new System.EventHandler(this.frmEmployeeDeductions_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmployeeDeductions_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeeDeductions_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewDeduction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDeduction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTemporaryOverrides)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHistory)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDeleteDeduction;
		private FCButton cmdNewDeduction;
		private FCButton cmdTemporaryOverrides;
		private FCButton cmdSelect;
		private FCButton cmdSave;
		private FCButton cmdRefresh;
		private FCButton cmdHistory;
	}
}