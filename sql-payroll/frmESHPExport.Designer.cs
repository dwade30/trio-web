//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmESHPExport.
	/// </summary>
	partial class frmESHPExport
	{
		public fecherFoundation.FCComboBox cmbPayrun;
		public fecherFoundation.FCComboBox cmbPayDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbPayrun = new fecherFoundation.FCComboBox();
            this.cmbPayDate = new fecherFoundation.FCComboBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 256);
            this.BottomPanel.Size = new System.Drawing.Size(356, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.cmbPayrun);
            this.ClientArea.Controls.Add(this.cmbPayDate);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(356, 196);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(356, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // cmbPayrun
            // 
            this.cmbPayrun.AutoSize = false;
            this.cmbPayrun.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPayrun.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPayrun.FormattingEnabled = true;
            this.cmbPayrun.Location = new System.Drawing.Point(225, 59);
            this.cmbPayrun.Name = "cmbPayrun";
            this.cmbPayrun.Size = new System.Drawing.Size(104, 40);
            this.cmbPayrun.TabIndex = 1;
            this.cmbPayrun.Text = "Combo1";
            // 
            // cmbPayDate
            // 
            this.cmbPayDate.AutoSize = false;
            this.cmbPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPayDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPayDate.FormattingEnabled = true;
            this.cmbPayDate.Location = new System.Drawing.Point(30, 59);
            this.cmbPayDate.Name = "cmbPayDate";
            this.cmbPayDate.Size = new System.Drawing.Size(174, 40);
            this.cmbPayDate.TabIndex = 0;
            this.cmbPayDate.Text = "Combo1";
            this.cmbPayDate.SelectedIndexChanged += new System.EventHandler(this.cmbPayDate_SelectedIndexChanged);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(225, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(70, 16);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "PAY RUN";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(70, 16);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "PAY DATE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 119);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(176, 48);
            this.cmdProcess.TabIndex = 4;
            this.cmdProcess.Text = "Save & Continue";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmESHPExport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(356, 256);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmESHPExport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "ESP";
            this.Load += new System.EventHandler(this.frmESHPExport_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmESHPExport_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}