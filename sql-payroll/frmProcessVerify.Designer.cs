//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmProcessVerify.
	/// </summary>
	partial class frmProcessVerify
	{
		public fecherFoundation.FCGrid vsPayCatGrid;
		public fecherFoundation.FCGrid vsDeductionGrid;
		public fecherFoundation.FCGrid vsMatchGrid;
		public fecherFoundation.FCGrid vsTotals;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuDDReport;
		public fecherFoundation.FCToolStripMenuItem mnuDDReportNotGrouped;
		public fecherFoundation.FCToolStripMenuItem mnuPrintScreen;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsPayCatGrid = new fecherFoundation.FCGrid();
            this.vsDeductionGrid = new fecherFoundation.FCGrid();
            this.vsMatchGrid = new fecherFoundation.FCGrid();
            this.vsTotals = new fecherFoundation.FCGrid();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuDDReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDDReportNotGrouped = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintScreen = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAccept = new fecherFoundation.FCButton();
            this.cmdPrintScreen = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.GridCheckSummary = new fecherFoundation.FCGrid();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayCatGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductionGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMatchGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCheckSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdAccept);
            this.BottomPanel.Location = new System.Drawing.Point(0, 992);
            this.BottomPanel.Size = new System.Drawing.Size(1258, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridCheckSummary);
            this.ClientArea.Controls.Add(this.vsPayCatGrid);
            this.ClientArea.Controls.Add(this.vsDeductionGrid);
            this.ClientArea.Controls.Add(this.vsMatchGrid);
            this.ClientArea.Controls.Add(this.vsTotals);
            this.ClientArea.Size = new System.Drawing.Size(1278, 786);
            this.ClientArea.Controls.SetChildIndex(this.vsTotals, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsMatchGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsDeductionGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsPayCatGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridCheckSummary, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdPrintScreen);
            this.TopPanel.Size = new System.Drawing.Size(1278, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintScreen, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(312, 30);
            this.HeaderText.Text = "Verify / Accept Calculations";
            // 
            // vsPayCatGrid
            // 
            this.vsPayCatGrid.Cols = 3;
            this.vsPayCatGrid.FixedCols = 0;
            this.vsPayCatGrid.Location = new System.Drawing.Point(30, 30);
            this.vsPayCatGrid.Name = "vsPayCatGrid";
            this.vsPayCatGrid.RowHeadersVisible = false;
            this.vsPayCatGrid.Rows = 50;
            this.vsPayCatGrid.Size = new System.Drawing.Size(517, 301);
            this.vsPayCatGrid.TabIndex = 1001;
            // 
            // vsDeductionGrid
            // 
            this.vsDeductionGrid.Cols = 1;
            this.vsDeductionGrid.FixedCols = 0;
            this.vsDeductionGrid.Location = new System.Drawing.Point(577, 30);
            this.vsDeductionGrid.Name = "vsDeductionGrid";
            this.vsDeductionGrid.RowHeadersVisible = false;
            this.vsDeductionGrid.Rows = 50;
            this.vsDeductionGrid.Size = new System.Drawing.Size(470, 301);
            this.vsDeductionGrid.TabIndex = 1;
            // 
            // vsMatchGrid
            // 
            this.vsMatchGrid.Cols = 1;
            this.vsMatchGrid.FixedCols = 0;
            this.vsMatchGrid.Location = new System.Drawing.Point(577, 350);
            this.vsMatchGrid.Name = "vsMatchGrid";
            this.vsMatchGrid.RowHeadersVisible = false;
            this.vsMatchGrid.Rows = 50;
            this.vsMatchGrid.Size = new System.Drawing.Size(470, 231);
            this.vsMatchGrid.TabIndex = 2;
            // 
            // vsTotals
            // 
            this.vsTotals.Cols = 5;
            this.vsTotals.FixedCols = 0;
            this.vsTotals.Location = new System.Drawing.Point(30, 350);
            this.vsTotals.Name = "vsTotals";
            this.vsTotals.RowHeadersVisible = false;
            this.vsTotals.Rows = 50;
            this.vsTotals.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vsTotals.Size = new System.Drawing.Size(517, 231);
            this.vsTotals.TabIndex = 3;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDDReport,
            this.mnuDDReportNotGrouped});
            this.MainMenu1.Name = null;
            // 
            // mnuDDReport
            // 
            this.mnuDDReport.Index = 0;
            this.mnuDDReport.Name = "mnuDDReport";
            this.mnuDDReport.Text = "Print Direct Deposit Preview (separate pages)";
            this.mnuDDReport.Click += new System.EventHandler(this.mnuDDReport_Click);
            // 
            // mnuDDReportNotGrouped
            // 
            this.mnuDDReportNotGrouped.Index = 1;
            this.mnuDDReportNotGrouped.Name = "mnuDDReportNotGrouped";
            this.mnuDDReportNotGrouped.Text = "Print Direct Deposit Preview (not separate)";
            this.mnuDDReportNotGrouped.Click += new System.EventHandler(this.mnuDDReportNotGrouped_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Accept           ";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print Accounting Preview";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintScreen
            // 
            this.mnuPrintScreen.Index = -1;
            this.mnuPrintScreen.Name = "mnuPrintScreen";
            this.mnuPrintScreen.Text = "Print Screen";
            this.mnuPrintScreen.Click += new System.EventHandler(this.mnuPrintScreen_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAccept
            // 
            this.cmdAccept.AppearanceKey = "acceptButton";
            this.cmdAccept.Location = new System.Drawing.Point(282, 30);
            this.cmdAccept.Name = "cmdAccept";
            this.cmdAccept.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdAccept.Size = new System.Drawing.Size(100, 48);
            this.cmdAccept.Text = "Accept";
            this.cmdAccept.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // cmdPrintScreen
            // 
            this.cmdPrintScreen.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintScreen.Location = new System.Drawing.Point(1156, 29);
            this.cmdPrintScreen.Name = "cmdPrintScreen";
            this.cmdPrintScreen.Size = new System.Drawing.Size(94, 24);
            this.cmdPrintScreen.TabIndex = 1;
            this.cmdPrintScreen.Text = "Print Screen";
            this.cmdPrintScreen.Click += new System.EventHandler(this.mnuPrintScreen_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(976, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(174, 24);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Print Accounting Preview";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // GridCheckSummary
            // 
            this.GridCheckSummary.Cols = 5;
            this.GridCheckSummary.FixedCols = 0;
            this.GridCheckSummary.Location = new System.Drawing.Point(30, 602);
            this.GridCheckSummary.Name = "GridCheckSummary";
            this.GridCheckSummary.RowHeadersVisible = false;
            this.GridCheckSummary.Rows = 50;
            this.GridCheckSummary.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.GridCheckSummary.Size = new System.Drawing.Size(517, 390);
            this.GridCheckSummary.TabIndex = 1002;
            // 
            // frmProcessVerify
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1278, 846);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmProcessVerify";
            this.Text = "Verify / Accept Calculations";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmProcessVerify_Load);
            this.Activated += new System.EventHandler(this.frmProcessVerify_Activated);
            this.Resize += new System.EventHandler(this.frmProcessVerify_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProcessVerify_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayCatGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductionGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMatchGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCheckSummary)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdAccept;
		private FCButton cmdPrint;
		private FCButton cmdPrintScreen;
        public FCGrid GridCheckSummary;
    }
}