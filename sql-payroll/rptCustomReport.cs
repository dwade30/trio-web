//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomReport.
	/// </summary>
	public partial class rptCustomReport : BaseSectionReport
	{
		public rptCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomReport InstancePtr
		{
			get
			{
				return (rptCustomReport)Sys.GetInstance(typeof(rptCustomReport));
			}
		}

		protected rptCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		int intPageNumber;
		FieldMessage[] strMessage = new FieldMessage[50 + 1];
        private SSNViewType ssnViewPermission = SSNViewType.None;
        // vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
        double dblSizeRatio = 1;

		private struct FieldMessage
		{
			public string strMessage;
			public bool boolShown;
			public bool boolDontContinue;
		};

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			int intControl;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				while (!rsData.EndOfFile())
				{
					if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
					{
						rsData.MoveNext();
					}
					else
					{
						break;
					}
				}
				// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				// 
				for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
				{
					for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
					{
						for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
						{
							//Application.DoEvents();
							if (Detail.Controls[intControl].Name == "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol))
							{
								break;
							}
						}
						if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) >= 0)
						{
							if (intCol > 0)
							{
								if (!FCUtils.IsEmpty(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))
								{
									if (FCUtils.IsEmpty(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))
									{
										// ADD THE DATA TO THE CORRECT CELL IN THE GRID
										if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == modCustomReport.GRIDDATE)
										{
											SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES()), "MM/dd/yyyy"), intRow, intCol);
										}
										else
										{
											if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
											{
												SetCellWithData(intControl, string.Empty, intRow, intCol);
											}
											else
											{
												SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES())), intRow, intCol);
											}
										}
									}
									else
									{
										if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1) >= 0)
										{
											// this will check to see if to merge the columns
											// not sure about this change but we ran into the case where
											// the data in the first two fields were the same but the fields
											// were different so we did NOT want to merge the two fields.
											// If rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))) = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))))) Then
											if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))]))
											{
												if ((intControl + 2) > Detail.Controls.Count)
												{
													Detail.Controls[intControl].Width = Detail.Controls[intControl].Width;
												}
												else
												{
													Detail.Controls[intControl].Width += Detail.Controls[intControl + 1].Width;
												}
											}
											else
											{
												// ADD THE DATA TO THE CORRECT CELL IN THE GRID
												if (FCConvert.ToInt32(Conversion.Val(modCustomReport.Statics.strWhereType[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == modCustomReport.GRIDDATE)
												{
													SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES()), "MM/dd/yyyy"), intRow, intCol);
												}
												else
												{
													// 
													if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
													{
														SetCellWithData(intControl, string.Empty, intRow, intCol);
													}
													else
													{
														SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES())), intRow, intCol);
													}
												}
											}
										}
										else
										{
											// ADD THE DATA TO THE CORRECT CELL IN THE GRID
											if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == modCustomReport.GRIDDATE)
											{
												SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES()), "MM/dd/yyyy"), intRow, intCol);
											}
											else
											{
												if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
												{
													SetCellWithData(intControl, string.Empty, intRow, intCol);
												}
												else
												{
													SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES())), intRow, intCol);
												}
											}
										}
									}
								}
							}
							else
							{
								// ADD THE DATA TO THE CORRECT CELL IN THE GRID
								if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == modCustomReport.GRIDDATE)
								{
									SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES()), "MM/dd/yyyy"), intRow, intCol);
								}
								else
								{
									if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
									{
										SetCellWithData(intControl, string.Empty, intRow, intCol);
									}
									else
									{
										SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToStringES())), intRow, intCol);
									}
								}
							}
						}
						if (strMessage[intControl].boolDontContinue)
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				rsData.MoveNext();
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Unable to build custom report. Close custom report screen and start again.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				eArgs.EOF = true;
			}
		}

		private void SetCellWithData(int intCounter, string strData, int intRow, int intCol)
        {
            var strNameOfField = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(
                modCustomReport.Statics.strFields[
                    FCConvert.ToInt32(Conversion.Val(
                        frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow,
                            intCol)))]));

            if (strNameOfField == "LEGITIMATE")
			{
				if (strMessage[intCounter].boolShown)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(!FCConvert.CBool(strData));
				}
				else
				{
					if (MessageBox.Show("A BOW record is about to be shown. Continue with report?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(!FCConvert.CBool(strData));
						strMessage[intCounter].boolShown = true;
						strMessage[intCounter].boolDontContinue = false;
					}
					else
					{
						strMessage[intCounter].boolDontContinue = true;
					}
				}
			}
			else if (strNameOfField == "FEDFILINGSTATUSID")
			{
				if (Conversion.Val(strData) == 3)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Married Filing Single";
				}
				else if (Conversion.Val(strData) == 4)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Single";
				}
				else if (Conversion.Val(strData) == 5)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Married";
				}
				else 
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
			else if (strNameOfField == "STATEFILINGSTATUSID")
			{
				if (Conversion.Val(strData) == 6)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Married";
				}
				else if (Conversion.Val(strData) == 7)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Single";
				}
				else if (Conversion.Val(strData) == 10)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Married Filing Single";
				}
				else
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
            else if (strNameOfField == "SSN" || strNameOfField == "SOCIALSECURITYNUMBER")
            {
                if (ssnViewPermission == SSNViewType.Masked)
                {
                    strData = "***-**-" + strData.Right(4);
                }
                else if (ssnViewPermission == SSNViewType.None)
                {
                    strData = "***-**-****";
                }
                else
                {
                    strData = strData.Left(3) + "-" + strData.Substring(3, 2) + "-" + strData.Right(4);
                }
                (Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
            }
			else
			{
				(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			//dblSizeRatio = (frmCustomReport.InstancePtr.Line2.X2 - frmCustomReport.InstancePtr.Line2.X1) / 1440;
			// it is most likely resized. See by how much
			if (frmCustomReport.InstancePtr.chkLandscape.CheckState == CheckState.Checked)
			{
                this.PrintWidth = 14200 / 1440f;
				this.Document.Printer.DefaultPageSettings.Landscape = true;
				this.Line1.X2 = this.PrintWidth;
				//this.Width = this.PrintWidth;
				txtDate.Left = this.PrintWidth - txtDate.Width;
				this.txtPage.Left = this.PrintWidth - txtPage.Width;
				this.txtTitle.Width = this.PrintWidth;
			}
			else
			{
                this.PrintWidth = 10500 / 1440f;
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
				this.Line1.X2 = this.PrintWidth;
				//this.Width = this.PrintWidth;
				txtDate.Left = this.PrintWidth - txtDate.Width;
				this.txtPage.Left = this.PrintWidth - txtPage.Width;
				this.txtTitle.Width = this.PrintWidth;
			}
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobalVariables.DEFAULTDATABASE);
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			CreateHeaderFields();
			CreateDataFields();
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
        }

		private void CreateHeaderFields()
		{
			int intControlNumber = 0;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 0; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					intControlNumber += 1;
					NewField = PageHeader.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtHeader" + FCConvert.ToString(intControlNumber));
					//NewField.Name = "txtHeader" + FCConvert.ToString(intControlNumber);
					NewField.Top = intRow * frmCustomReport.InstancePtr.vsLayout.RowHeight(0) / 1440F + 1000 / 1440F;
					NewField.Left = FCConvert.ToSingle(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol)) / dblSizeRatio) / 1440F;
					if (intCol > 0)
					{
						if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) == frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol - 1))
						{
							NewField.Text = string.Empty;
						}
						else
						{
							NewField.Text = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
						}
					}
					else
					{
						NewField.Text = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					NewField.Width = FCConvert.ToSingle(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol)) / dblSizeRatio) / 1440F;
					NewField.WordWrap = false;
					//PageHeader.Controls.Add(NewField);
				}
			}
			// ADD A LINE SEPERATOR BETWEEN THE CAPTIONS AND THE DATA THAT
			// IS DISPLAYED
			Line1.X1 = 0;
			Line1.X2 = this.PrintWidth;
			Line1.Y1 = (intRow * frmCustomReport.InstancePtr.vsLayout.RowHeight(0)) / 1440F + 800 / 1440F;
			Line1.Y2 = Line1.Y1;
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// dblSizeRatio = frmCustomReport.Line1.X2 / 1440
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol));
					//NewField.Name = "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol);
                    //NewField.Top = ((intRow - 1) * frmCustomReport.InstancePtr.vsLayout.RowHeight(0) + 50) / 1440F;
                    NewField.Top = ((intRow - 1) * 285 + 50) / 1440F;
                    NewField.Left = FCConvert.ToSingle(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol)) / dblSizeRatio) / 1440F;
					NewField.Width = FCConvert.ToSingle(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol)) / dblSizeRatio) / 1440F;
                    //NewField.Height = frmCustomReport.InstancePtr.vsLayout.RowHeight(1) / 1440F;
                    NewField.Height = 285 / 1440F;
                    NewField.Text = string.Empty;
					NewField.WordWrap = false;
					//Detail.Controls.Add(NewField);
				}
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= Detail.Controls.Count - 1; intCounter++)
			{
				if (Detail.Controls[intCounter].Tag != null && FCConvert.ToString(Detail.Controls[intCounter].Tag) != string.Empty)
				{
					if (Strings.Left(Detail.Controls[intCounter].Name, 7) == "txtData")
					{
						if (Conversion.Val(Detail.Controls[intCounter].Tag) <= this.PrintWidth)
						{
							// MsgBox Val(Detail.Controls[intCounter].Tag)
							Detail.Controls[intCounter].Width = FCConvert.ToSingle(Detail.Controls[intCounter].Tag);
						}
						else
						{
							Detail.Controls[intCounter].Width = this.PrintWidth;
						}
					}
				}
			}
			Detail.Height = frmCustomReport.InstancePtr.vsLayout.RowHeight(0) * (frmCustomReport.InstancePtr.vsLayout.Rows - 1) / 1440F;
		}

		
	}
}
