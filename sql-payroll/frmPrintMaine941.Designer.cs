//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPrintMaine941.
	/// </summary>
	partial class frmPrintMaine941
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtFederalID;
		public fecherFoundation.FCTextBox txtMRSID;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txttitle;
		public fecherFoundation.FCTextBox txtExtension;
		public Global.T2KPhoneNumberBox txtPhone;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCTextBox txtStreetAddress;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintTest;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtFederalID = new fecherFoundation.FCTextBox();
            this.txtMRSID = new fecherFoundation.FCTextBox();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.txttitle = new fecherFoundation.FCTextBox();
            this.txtExtension = new fecherFoundation.FCTextBox();
            this.txtPhone = new Global.T2KPhoneNumberBox();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.txtStreetAddress = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintTest = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdPrintReport = new fecherFoundation.FCButton();
            this.cmdTest = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTest)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 644);
            this.BottomPanel.Size = new System.Drawing.Size(578, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(598, 628);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdTest);
            this.TopPanel.Controls.Add(this.cmdPrintReport);
            this.TopPanel.Size = new System.Drawing.Size(598, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintReport, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdTest, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(161, 28);
            this.HeaderText.Text = "State 941 RPT";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtFederalID);
            this.Frame3.Controls.Add(this.txtMRSID);
            this.Frame3.Controls.Add(this.Label1_11);
            this.Frame3.Controls.Add(this.Label1_10);
            this.Frame3.Location = new System.Drawing.Point(30, 502);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(527, 142);
            this.Frame3.TabIndex = 21;
            this.Frame3.Text = "Employer Account Number & Id Numbers";
            // 
            // txtFederalID
            // 
            this.txtFederalID.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalID.Location = new System.Drawing.Point(367, 83);
            this.txtFederalID.MaxLength = 9;
            this.txtFederalID.Name = "txtFederalID";
            this.txtFederalID.Size = new System.Drawing.Size(140, 40);
            this.txtFederalID.TabIndex = 23;
            // 
            // txtMRSID
            // 
            this.txtMRSID.BackColor = System.Drawing.SystemColors.Window;
            this.txtMRSID.Location = new System.Drawing.Point(367, 30);
            this.txtMRSID.MaxLength = 11;
            this.txtMRSID.Name = "txtMRSID";
            this.txtMRSID.Size = new System.Drawing.Size(140, 40);
            this.txtMRSID.TabIndex = 10;
            this.txtMRSID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRSID_KeyPress);
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(20, 97);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(320, 16);
            this.Label1_11.TabIndex = 24;
            this.Label1_11.Text = "FEDERAL EMPLOYER ID";
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(20, 44);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(320, 16);
            this.Label1_10.TabIndex = 22;
            this.Label1_10.Text = "MAINE REVENUE SERVICES WITHHOLDING ACCOUNT ID";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtEmail);
            this.Frame2.Controls.Add(this.txttitle);
            this.Frame2.Controls.Add(this.txtExtension);
            this.Frame2.Controls.Add(this.txtPhone);
            this.Frame2.Controls.Add(this.Label1_8);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_7);
            this.Frame2.Location = new System.Drawing.Point(30, 291);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(527, 191);
            this.Frame2.TabIndex = 17;
            this.Frame2.Text = "Individual Responsible For Accuracy Of Report";
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(96, 130);
            this.txtEmail.MaxLength = 30;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(411, 40);
            this.txtEmail.TabIndex = 9;
            // 
            // txttitle
            // 
            this.txttitle.BackColor = System.Drawing.SystemColors.Window;
            this.txttitle.Location = new System.Drawing.Point(96, 30);
            this.txttitle.MaxLength = 30;
            this.txttitle.Name = "txttitle";
            this.txttitle.Size = new System.Drawing.Size(411, 40);
            this.txttitle.TabIndex = 6;
            // 
            // txtExtension
            // 
            this.txtExtension.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtension.Location = new System.Drawing.Point(355, 80);
            this.txtExtension.MaxLength = 4;
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(152, 40);
            this.txtExtension.TabIndex = 8;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(96, 80);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(120, 22);
            this.txtPhone.TabIndex = 7;
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(20, 144);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(45, 16);
            this.Label1_8.TabIndex = 25;
            this.Label1_8.Text = "EMAIL";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 44);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(45, 16);
            this.Label1_5.TabIndex = 20;
            this.Label1_5.Text = "TITLE";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(20, 94);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(45, 16);
            this.Label1_6.TabIndex = 19;
            this.Label1_6.Text = "PHONE";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(236, 94);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(105, 16);
            this.Label1_7.TabIndex = 18;
            this.Label1_7.Text = "EXT. OR MAILBOX";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtEmployerName);
            this.Frame1.Controls.Add(this.txtStreetAddress);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtZip4);
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Controls.Add(this.Label1_1);
            this.Frame1.Controls.Add(this.Label1_2);
            this.Frame1.Controls.Add(this.Label1_3);
            this.Frame1.Controls.Add(this.Label1_4);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(527, 240);
            this.Frame1.TabIndex = 11;
            this.Frame1.Text = "Employer Name & Address";
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.Location = new System.Drawing.Point(116, 30);
            this.txtEmployerName.MaxLength = 44;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(391, 40);
            this.txtEmployerName.TabIndex = 0;
            // 
            // txtStreetAddress
            // 
            this.txtStreetAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetAddress.Location = new System.Drawing.Point(116, 80);
            this.txtStreetAddress.MaxLength = 35;
            this.txtStreetAddress.Name = "txtStreetAddress";
            this.txtStreetAddress.Size = new System.Drawing.Size(391, 40);
            this.txtStreetAddress.TabIndex = 1;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(116, 130);
            this.txtCity.MaxLength = 20;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(391, 40);
            this.txtCity.TabIndex = 2;
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(116, 180);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(60, 40);
            this.txtState.TabIndex = 3;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(247, 180);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(120, 40);
            this.txtZip.TabIndex = 4;
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Location = new System.Drawing.Point(387, 180);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(120, 40);
            this.txtZip4.TabIndex = 5;
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 44);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(70, 16);
            this.Label1_0.TabIndex = 16;
            this.Label1_0.Text = "EMPLOYER";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 94);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(70, 16);
            this.Label1_1.TabIndex = 15;
            this.Label1_1.Text = "ADDRESS";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 144);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(70, 16);
            this.Label1_2.TabIndex = 14;
            this.Label1_2.Text = "CITY";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 194);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(70, 16);
            this.Label1_3.TabIndex = 13;
            this.Label1_3.Text = "STATE";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(196, 194);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(27, 24);
            this.Label1_4.TabIndex = 12;
            this.Label1_4.Text = "ZIP";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuPrintTest,
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print Report";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintTest
            // 
            this.mnuPrintTest.Index = 1;
            this.mnuPrintTest.Name = "mnuPrintTest";
            this.mnuPrintTest.Text = "Print Test Report";
            this.mnuPrintTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 2;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 3;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(198, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // cmdPrintReport
            // 
            this.cmdPrintReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintReport.Location = new System.Drawing.Point(350, 29);
            this.cmdPrintReport.Name = "cmdPrintReport";
            this.cmdPrintReport.Size = new System.Drawing.Size(92, 24);
            this.cmdPrintReport.TabIndex = 1;
            this.cmdPrintReport.Text = "Print Report";
            this.cmdPrintReport.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdTest
            // 
            this.cmdTest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdTest.Location = new System.Drawing.Point(448, 29);
            this.cmdTest.Name = "cmdTest";
            this.cmdTest.Size = new System.Drawing.Size(122, 24);
            this.cmdTest.TabIndex = 2;
            this.cmdTest.Text = "Print Test Report";
            this.cmdTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // frmPrintMaine941
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(598, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrintMaine941";
            this.Text = "State 941 RPT";
            this.Load += new System.EventHandler(this.frmPrintMaine941_Load);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTest)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdTest;
		private FCButton cmdPrintReport;
	}
}
