//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt940.
	/// </summary>
	public partial class rpt940 : BaseSectionReport
	{
		public rpt940()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "FTD FICA & WITHHOLDINGS";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt940 InstancePtr
		{
			get
			{
				return (rpt940)Sys.GetInstance(typeof(rpt940));
			}
		}

		protected rpt940 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsFTD?.Dispose();
                rsFTD = null;
				rsEmployeeInfo?.Dispose();
				rsYTDEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
                rsYTDEmployeeInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt940	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsFTD = new clsDRWrapper();
		double dblRate;
		int intQuarter;
		// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
		int intMonth;
		int lngYear;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		clsDRWrapper rsYTDEmployeeInfo = new clsDRWrapper();
		// vbPorter upgrade warning: datLowDate As DateTime	OnWrite(string, DateTime)
		DateTime datLowDate;
		DateTime datHighDate;
		// vbPorter upgrade warning: curTotalPayroll As Decimal	OnWrite(int, Decimal)
		Decimal curTotalPayroll;
		// vbPorter upgrade warning: curExcess As Decimal	OnWrite(int, Decimal)
		Decimal curExcess;
		Decimal curFUTABase;
		// vbPorter upgrade warning: curYTDEmployeePay As Decimal	OnWrite(Decimal, int)
		Decimal curYTDEmployeePay;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("FTD940");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			fldFUTALimit.Text = Strings.Format(curFUTABase, "0.00");
			// OLD SQL STATEMENTS PRIOR TO 11/05/03
			// rsEmployeeInfo.OpenRecordset "SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as CurrentGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblEmployeeMaster.UnemploymentExempt = 0 AND tblCheckDetail.DistU <> 'N' AND tblCheckDetail.PayDate >= '" & datLowDate & "' AND tblCheckDetail.PayDate <= '" & datHighDate & "' GROUP BY tblEmployeeMaster.EmployeeNumber"
			// rsYTDEmployeeInfo.OpenRecordset "SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as YTDGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblEmployeeMaster.UnemploymentExempt = 0 AND tblCheckDetail.DistU <> 'N' AND tblCheckDetail.PayDate >= '1/1/" & lngYear & "' AND tblCheckDetail.PayDate <= '" & datHighDate & "' GROUP BY tblEmployeeMaster.EmployeeNumber"
			rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as CurrentGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE (not tblcheckdetail.checkvoid = 1) and tblCheckDetail.PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND tblCheckDetail.PayDate <= '" + FCConvert.ToString(datHighDate) + "' GROUP BY tblEmployeeMaster.EmployeeNumber");
			rsYTDEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as YTDGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE (not tblcheckdetail.checkvoid = 1) and tblCheckDetail.PayDate >= '1/1/" + FCConvert.ToString(lngYear) + "' AND tblCheckDetail.PayDate <= '" + FCConvert.ToString(datHighDate) + "' GROUP BY tblEmployeeMaster.EmployeeNumber");
			curTotalPayroll = 0;
			curExcess = 0;
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				do
				{
					if (rsYTDEmployeeInfo.FindFirstRecord("EmployeeNumber", rsEmployeeInfo.Get_Fields("EmployeeNumber")))
					{
						curYTDEmployeePay = FCConvert.ToDecimal(Conversion.Val(rsYTDEmployeeInfo.Get_Fields("YTDGross")));
					}
					else
					{
						curYTDEmployeePay = 0;
					}
					curTotalPayroll += FCConvert.ToDecimal(rsEmployeeInfo.Get_Fields("CurrentGross"));
					curExcess += modDavesSweetCode.CalculateExcess(curYTDEmployeePay, FCConvert.ToDecimal(rsEmployeeInfo.Get_Fields("CurrentGross")), curFUTABase);
					rsEmployeeInfo.MoveNext();
				}
				while (rsEmployeeInfo.EndOfFile() != true);
			}
			txtAmount.Text = Strings.Format(curTotalPayroll, "#,##0.00");
			// GET THE EXCESS AMOUNT
			rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as CurrentGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE (not tblcheckdetail.checkvoid = 1) and tblEmployeeMaster.UnemploymentExempt = 0 AND tblCheckDetail.DistU <> 'N' AND tblCheckDetail.PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND tblCheckDetail.PayDate <= '" + FCConvert.ToString(datHighDate) + "' GROUP BY tblEmployeeMaster.EmployeeNumber");
			rsYTDEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as YTDGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE (not tblcheckdetail.checkvoid = 1) and tblEmployeeMaster.UnemploymentExempt = 0 AND tblCheckDetail.DistU <> 'N' AND tblCheckDetail.PayDate >= '1/1/" + FCConvert.ToString(lngYear) + "' AND tblCheckDetail.PayDate <= '" + FCConvert.ToString(datHighDate) + "' GROUP BY tblEmployeeMaster.EmployeeNumber");
			curExcess = 0;
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				do
				{
					if (rsYTDEmployeeInfo.FindFirstRecord("EmployeeNumber", rsEmployeeInfo.Get_Fields("EmployeeNumber")))
					{
						curYTDEmployeePay = FCConvert.ToDecimal(Conversion.Val(rsYTDEmployeeInfo.Get_Fields("YTDGross")));
					}
					else
					{
						curYTDEmployeePay = 0;
					}
					curExcess += modDavesSweetCode.CalculateExcess(curYTDEmployeePay, FCConvert.ToDecimal(rsEmployeeInfo.Get_Fields("CurrentGross")), curFUTABase);
					rsEmployeeInfo.MoveNext();
				}
				while (rsEmployeeInfo.EndOfFile() != true);
			}
			txtExcess.Text = Strings.Format(curExcess, "#,##0.00");
			// GET THE EXEMPT PAYMENTS
			rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as CurrentGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE (not tblcheckdetail.checkvoid = 1) and (tblEmployeeMaster.UnemploymentExempt = 1 or tblCheckDetail.DistU = 'N') AND tblCheckDetail.PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND tblCheckDetail.PayDate <= '" + FCConvert.ToString(datHighDate) + "' GROUP BY tblEmployeeMaster.EmployeeNumber");
			curTotalPayroll = 0;
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				do
				{
					curTotalPayroll += FCConvert.ToDecimal(rsEmployeeInfo.Get_Fields("CurrentGross"));
					rsEmployeeInfo.MoveNext();
				}
				while (rsEmployeeInfo.EndOfFile() != true);
			}
			txtExemptPayments.Text = Strings.Format(curTotalPayroll, "#,##0.00");
			txtTaxable.Text = Strings.Format(FCConvert.ToDouble(txtAmount.Text) - FCConvert.ToDouble(txtExcess.Text) - FCConvert.ToDouble(txtExemptPayments.Text), "#,##0.00");
			txtRate.Text = Strings.Format(dblRate, "0.0000");
			txtDue.Text = Strings.Format((FCConvert.ToDouble(txtAmount.Text) - FCConvert.ToDouble(txtExcess.Text) - FCConvert.ToDouble(txtExemptPayments.Text)) * (dblRate / 100), "#,##0.00");
		}

		public void Init(List<string> batchReports = null)
		{
			intMonth = 0;
			intQuarter = 0;
			lngYear = 0;
			rsFTD.OpenRecordset("SELECT FTD940, FutaBase, FUTAActual from tblStandardLimits", "TWPY0000.vb1");
			if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
			{
				if (FCConvert.ToString(rsFTD.Get_Fields("FTD940")) == "Monthly")
				{
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init5("Please select the month and year to report on.", ref lngYear, -1, ref intMonth, ref tmpArg, -1, false);
					if (intMonth != -1)
					{
						lblMQY.Text = "MONTHLY";
						Label6.Text = "Total MTD Payroll";
						datLowDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
						datHighDate = modDavesSweetCode.FindLastDay(fecherFoundation.Strings.Trim(datLowDate.ToString()));
						lblDate.Text = "Month: " + modBudgetaryAccounting.MonthCalc(intMonth) + " " + FCConvert.ToString(lngYear);
					}
					else
					{
						this.Close();
						return;
					}
				}
				else if (rsFTD.Get_Fields("FTD940") == "Quarterly")
				{
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init3("Please select the quarter and year to report on.", ref lngYear, ref intQuarter, -1, ref tmpArg, -1, false);
					if (intQuarter != -1)
					{
						lblMQY.Text = "QUARTERLY";
						Label6.Text = "Total QTD Payroll";
						modCoreysSweeterCode.GetDateRangeForYearQuarter(ref datLowDate, ref datHighDate, intQuarter, lngYear);
						lblDate.Text = "Quarter: " + FCConvert.ToString(intQuarter) + "/" + FCConvert.ToString(lngYear);
					}
					else
					{
						this.Close();
						return;
					}
				}
				else
				{
					datLowDate = DateTime.Today;
                    int intPayRunID = -1;
                    frmSelectDateInfo.InstancePtr.Init2("Please select the pay date you wish to have reported for.", -1, -1, -1, ref datLowDate, ref intPayRunID, false);
					if (datLowDate.ToOADate() != 0)
					{
						lblMQY.Text = "WEEKLY";
						Label6.Text = "Total Weekly Payroll";
						datHighDate = datLowDate;
						lblDate.Text = "Pay Date: " + Strings.Format(datLowDate, "MM/dd/yyyy");
					}
					else
					{
						this.Close();
						return;
					}
				}
			}
			else if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
			{
				switch (modGlobalVariables.Statics.gdatCurrentPayDate.Month)
				{
					case 1:
					case 2:
					case 3:
						{
							intQuarter = 1;
							break;
						}
					case 4:
					case 5:
					case 6:
						{
							intQuarter = 2;
							break;
						}
					case 7:
					case 8:
					case 9:
						{
							intQuarter = 3;
							break;
						}
					default:
						{
							intQuarter = 4;
							break;
						}
				}
				//end switch
				lblMQY.Text = "QUARTERLY";
				Label6.Text = "Total QTD Payroll";
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref datLowDate, ref datHighDate, intQuarter, lngYear);
				lblDate.Text = "Quarter: " + FCConvert.ToString(intQuarter) + "/" + FCConvert.ToString(lngYear);
			}
			else if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
			{
				intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
				lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
				lblMQY.Text = "MONTHLY";
				Label6.Text = "Total MTD Payroll";
				datLowDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
				datHighDate = modDavesSweetCode.FindLastDay(fecherFoundation.Strings.Trim(datLowDate.ToString()));
				lblDate.Text = "Month: " + modBudgetaryAccounting.MonthCalc(intMonth) + " " + FCConvert.ToString(lngYear);
			}
			else
			{
				datLowDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				lblMQY.Text = "WEEKLY";
				Label6.Text = "Total Weekly Payroll";
				datHighDate = datLowDate;
				lblDate.Text = "Pay Date: " + Strings.Format(datLowDate, "MM/dd/yyyy");
			}
			curFUTABase = FCConvert.ToDecimal(Conversion.Val(rsFTD.Get_Fields_Double("FutaBase")));
			dblRate = Conversion.Val(rsFTD.Get_Fields_Double("FutaActual"));
			if (lngYear != -1)
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					
					modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
					// rpt940.PrintReport False
				}
				else
				{
					// frmReportViewer.Init rpt940
					modCoreysSweeterCode.CheckDefaultPrint(rpt940.InstancePtr, boolAllowEmail: true, strAttachmentName: "Fed940");
				}
			}
			else
			{
				this.Cancel();
				this.Close();
			}
		}

		
	}
}
