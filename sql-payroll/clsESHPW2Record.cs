﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsESHPW2Record
	{
		//=========================================================
		private int intYear;
		private string strSSN = string.Empty;

		public string SocialSecurityNumber
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SocialSecurityNumber = "";
				SocialSecurityNumber = strSSN;
				return SocialSecurityNumber;
			}
		}
	}
}
