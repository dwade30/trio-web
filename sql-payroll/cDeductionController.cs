//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class cDeductionController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngError, string strError)
		{
			lngLastError = lngError;
			strLastError = strError;
		}

		public cGenericCollection GetUsedMatches()
		{
			cGenericCollection GetUsedMatches = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection collReturn = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "Select tbldeductionsetup.* from tbldeductionsetup inner join (select distinct deductioncode from tblemployersmatch) tbl1 on tbldeductionsetup.id = tbl1.deductioncode order by tbldeductionsetup.description";
				rsLoad.OpenRecordset(strSQL, "Payroll");
				cDeductionSetup dedSet;
				while (!rsLoad.EndOfFile())
				{
					dedSet = new cDeductionSetup();
					FillDedSetup(ref dedSet, ref rsLoad);
					collReturn.AddItem(dedSet);
					rsLoad.MoveNext();
				}
				GetUsedMatches = collReturn;
				return GetUsedMatches;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetUsedMatches;
		}

		public cGenericCollection GetUsedDeductions()
		{
			cGenericCollection GetUsedDeductions = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection collReturn = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "Select tbldeductionsetup.* from tbldeductionsetup inner join (select distinct deductioncode from tblemployeedeductions) tbl1 on tbldeductionsetup.id = tbl1.deductioncode order by tbldeductionsetup.description";
				rsLoad.OpenRecordset(strSQL, "Payroll");
				cDeductionSetup dedSet;
				while (!rsLoad.EndOfFile())
				{
					dedSet = new cDeductionSetup();
					FillDedSetup(ref dedSet, ref rsLoad);
					collReturn.AddItem(dedSet);
					rsLoad.MoveNext();
				}
				GetUsedDeductions = collReturn;
				return GetUsedDeductions;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetUsedDeductions;
		}

		private void FillDedSetup(ref cDeductionSetup dedSet, ref clsDRWrapper rsLoad)
		{
			if (!(dedSet == null))
			{
				if (rsLoad != null)
				{
					if (!rsLoad.EndOfFile())
					{
						dedSet.AccountDescription = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("AccountDescription"));
						dedSet.AccountID = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("AccountID"));
						dedSet.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
						dedSet.AmountType = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("amounttype"));
						dedSet.DeductionNumber = rsLoad.Get_Fields_Int32("deductionnumber");
						dedSet.Description = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("Description"));
						dedSet.FrequencyCode = rsLoad.Get_Fields_Int32("FrequencyCode");
						dedSet.ID = rsLoad.Get_Fields_Int32("ID");
						if (Information.IsDate(rsLoad.Get_Fields("LastUpdate")))
						{
							dedSet.LastUpdate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("LastUpdate"));
						}
						dedSet.LastUserID = rsLoad.Get_Fields_String("LastUserID");
						dedSet.Limit = Conversion.Val(rsLoad.Get_Fields_Double("Limit"));
						dedSet.LimitType = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("LimitType"));
						dedSet.OldFrequency = rsLoad.Get_Fields_Int32("OldFrequency");
						dedSet.Percent = rsLoad.Get_Fields_String("Percent");
						dedSet.Priority = rsLoad.Get_Fields_Int32("Priority");
						dedSet.Status = rsLoad.Get_Fields_String("Status");
						dedSet.TaxStatusCode = rsLoad.Get_Fields_Int32("TaxStatusCode");
						dedSet.UExempt = rsLoad.Get_Fields_Boolean("Uexempt");
						dedSet.IsUpdated = false;
					}
				}
			}
		}

		public cGenericCollection GetDeductionLimits()
		{
			cGenericCollection GetDeductionLimits = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection collLim = new cGenericCollection();
				collLim.AddItem(GetNewCode(1, "Life", "Life"));
				collLim.AddItem(GetNewCode(2, "Fiscal", "Fiscal"));
				collLim.AddItem(GetNewCode(3, "Calendar", "Calendar"));
				collLim.AddItem(GetNewCode(4, "Quarter", "Quarter"));
				collLim.AddItem(GetNewCode(5, "Month", "Month"));
				collLim.AddItem(GetNewCode(6, "Period", "Period"));
				collLim.AddItem(GetNewCode(7, "% Gross", "% Gross"));
				GetDeductionLimits = collLim;
				return GetDeductionLimits;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetDeductionLimits;
		}

		public cGenericCollection GetFrequencyCodes()
		{
			cGenericCollection GetFrequencyCodes = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection collFreq = new cGenericCollection();
				collFreq.AddItem(GetNewCode(1, "W", "Weekly"));
				collFreq.AddItem(GetNewCode(2, "1", "Week One Only"));
				collFreq.AddItem(GetNewCode(3, "2", "Week Two Only"));
				collFreq.AddItem(GetNewCode(4, "3", "Week Three Only"));
				collFreq.AddItem(GetNewCode(5, "4", "Week Four Only"));
				collFreq.AddItem(GetNewCode(6, "A", "Yearly"));
				collFreq.AddItem(GetNewCode(7, "M", "Monthly"));
				collFreq.AddItem(GetNewCode(8, "B", "Bi-Weekly"));
				collFreq.AddItem(GetNewCode(9, "N", "Hold"));
				collFreq.AddItem(GetNewCode(10, "P", "Hold (this week only)"));
				collFreq.AddItem(GetNewCode(11, "T", "Week 1-4, Not 5"));
				collFreq.AddItem(GetNewCode(12, "D", "Default"));
				collFreq.AddItem(GetNewCode(13, "X", "Test"));
				collFreq.AddItem(GetNewCode(14, "S", "Semi-Annually (1,7)"));
				collFreq.AddItem(GetNewCode(15, "H", "Semi-Annually (2,8)"));
				collFreq.AddItem(GetNewCode(16, "I", "Semi-Annually (3,9)"));
				collFreq.AddItem(GetNewCode(17, "J", "Semi-Annually (4,10)"));
				collFreq.AddItem(GetNewCode(18, "K", "Semi-Annually (5,11)"));
				collFreq.AddItem(GetNewCode(19, "L", "Semi-Annually (6,12)"));
				collFreq.AddItem(GetNewCode(20, "R", "Week one and three only"));
				collFreq.AddItem(GetNewCode(21, "C", "Week two and four only"));
				collFreq.AddItem(GetNewCode(22, "E", "Quarterly"));
				collFreq.AddItem(GetNewCode(23, "F", "Semi-Annually"));
				GetFrequencyCodes = collFreq;
				return GetFrequencyCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetFrequencyCodes;
		}

		private cCodeDescription GetNewCode(int lngID, string strCode, string strDescription)
		{
			cCodeDescription GetNewCode = null;
			cCodeDescription cd = new cCodeDescription();
			cd.ID = lngID;
			cd.Code = strCode;
			cd.Description = strDescription;
			GetNewCode = cd;
			return GetNewCode;
		}

		public cGenericCollection GetEmployeesUsingMatch(int lngID)
		{
			cGenericCollection GetEmployeesUsingMatch = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cGenericCollection collEmployees = new cGenericCollection();
				rsLoad.OpenRecordset("select distinct employeenumber from tblemployersmatch where deductioncode = " + FCConvert.ToString(lngID) + " order by employeenumber", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					//App.DoEvents();
					collEmployees.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("EmployeeNumber"))));
					rsLoad.MoveNext();
				}
				GetEmployeesUsingMatch = collEmployees;
				return GetEmployeesUsingMatch;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetEmployeesUsingMatch;
		}

		public cGenericCollection GetEmployeesUsingDeduction(int lngID)
		{
			cGenericCollection GetEmployeesUsingDeduction = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cGenericCollection collEmployees = new cGenericCollection();
				rsLoad.OpenRecordset("Select distinct employeenumber from tblemployeedeductions where deductioncode = " + FCConvert.ToString(lngID) + " order by employeenumber", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					//App.DoEvents();
					collEmployees.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("EmployeeNumber"))));
					rsLoad.MoveNext();
				}
				GetEmployeesUsingDeduction = collEmployees;
				return GetEmployeesUsingDeduction;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetEmployeesUsingDeduction;
		}
	}
}
