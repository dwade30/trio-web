﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmLoadFromTimeClockPlus : BaseForm
	{
		public frmLoadFromTimeClockPlus()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmLoadFromTimeClockPlus InstancePtr
		{
			get
			{
				return (frmLoadFromTimeClockPlus)Sys.GetInstance(typeof(frmLoadFromTimeClockPlus));
			}
		}

		protected frmLoadFromTimeClockPlus _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strDefaultDir;
				strDefaultDir = Environment.CurrentDirectory;
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.InitDir = strDefaultDir;
				string strReturnFile;
				strReturnFile = "";
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strReturnFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				txtImport.Text = strReturnFile;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (!(fecherFoundation.Information.Err(ex).Number == 32755))
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In cmdBrowse", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					fecherFoundation.Information.Err(ex).Clear();
				}
			}
		}

		private void frmLoadFromTimeClockPlus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLoadFromTimeClockPlus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLoadFromTimeClockPlus properties;
			//frmLoadFromTimeClockPlus.FillStyle	= 0;
			//frmLoadFromTimeClockPlus.ScaleWidth	= 5880;
			//frmLoadFromTimeClockPlus.ScaleHeight	= 4050;
			//frmLoadFromTimeClockPlus.LinkTopic	= "Form2";
			//frmLoadFromTimeClockPlus.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			string strTemp;
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("TimeClockFile", "PY", ""));
			txtImport.Text = strTemp;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			clsTimeClockPlus tFil = new clsTimeClockPlus();
			FCFileSystem fso = new FCFileSystem();
			if (MessageBox.Show("This will overwrite this weeks hours for all employees found in the file" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
			{
				return;
			}
			if (FCFileSystem.FileExists(txtImport.Text))
			{
				// Call SaveRegistryKey("TimeClockImportFile", "Time Card", "PY")
				modRegistry.SaveRegistryKey("TimeClockFile", txtImport.Text, "PY");
				if (tFil.LoadFile(txtImport.Text))
				{
					if (tFil.ProcessImport())
					{
						string strMsg = "";
						strMsg = tFil.Messages;
						MessageBox.Show("File processed", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
						if (fecherFoundation.Strings.Trim(strMsg) != string.Empty)
						{
							rptRichText.InstancePtr.Init(strMsg, "Warnings");
						}
						frmEmployeeListing.InstancePtr.ReloadAll();
						Close();
						return;
					}
					else
					{
						MessageBox.Show("Unable to successfully process file", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				MessageBox.Show("File " + txtImport.Text + " does not exist", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
