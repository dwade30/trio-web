﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmPrintBankSetup : BaseForm
	{
		public frmPrintBankSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            //FC:FINAL:BSE:#4158 allow only numeric inputs in textbox
            txtStart.AllowOnlyNumericInput();
            txtEnd.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintBankSetup InstancePtr
		{
			get
			{
				return (frmPrintBankSetup)Sys.GetInstance(typeof(frmPrintBankSetup));
			}
		}

		protected frmPrintBankSetup _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       June 27, 2001
		//
		// NOTES:
		//
		//
		//
		// **************************************************
		// private local variables
		// Private dbDeductions    As DAO.Database
		// Private rsDeductions    As DAO.Recordset
		private int intCounter;
		private int intDataChanged;

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (txtStart.Enabled)
			{
				if (txtStart.Text == string.Empty)
				{
					MessageBox.Show("Start value must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtStart.Focus();
					return;
				}
				else if (txtEnd.Text == string.Empty)
				{
					MessageBox.Show("End value must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtEnd.Focus();
					return;
				}
				else
				{
					if (cmbtion1.Text == "Range of Banks")
					{
						modGlobalVariables.Statics.gstrBankStart = txtStart.Text;
						modGlobalVariables.Statics.gstrBankEnd = txtEnd.Text;
					}
				}
			}
			modGlobalVariables.Statics.gboolSortBankName = cmbSort.Text == "By Name";
			if (cmbType.Text == "Listing")
			{
				// rptEmployeeData.Show
				// rptEmployeeDistribution.Show
				// matthew
				rptBankInfoListing.InstancePtr.Init();
			}
			else
			{
				rptBankInfoLabels.InstancePtr.Init();
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void frmPrintBankSetup_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPrintBankSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData)) / 0x10000;
			if (KeyCode == Keys.Escape)
				Close();
			if (KeyCode == Keys.F6)
				cmdPrint_Click();
		}

		private void frmPrintBankSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintBankSetup properties;
			//frmPrintBankSetup.ScaleWidth	= 7170;
			//frmPrintBankSetup.ScaleHeight	= 5310;
			//frmPrintBankSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				// open the forms global database connection
				// Set dbDeductions = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				modGlobalVariables.Statics.gstrBankStart = string.Empty;
				modGlobalVariables.Statics.gstrBankEnd = string.Empty;
				modGlobalVariables.Statics.gboolSortBankName = true;
				optType_Click(0);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						// Call cmdSave_Click
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void Option1_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			txtStart.Text = string.Empty;
			txtEnd.Text = string.Empty;
			txtStart.Enabled = Index == 1;
			txtEnd.Enabled = Index == 1;
			if (txtStart.Enabled)
			{
				txtStart.BackColor = Color.White;
			}
			else
			{
				txtStart.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			if (txtEnd.Enabled)
			{
				txtEnd.BackColor = Color.White;
			}
			else
			{
				txtEnd.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
		}

		private void Option1_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtion1.SelectedIndex;
			Option1_CheckedChanged(index, sender, e);
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			cmbStyle.Enabled = Index == 1;
		}

		public void optType_Click(int Index)
		{
			optType_CheckedChanged(Index, cmbType, new System.EventArgs());
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

		private void txtEnd_Enter(object sender, System.EventArgs e)
		{
			txtStart.SelectionStart = 1;
			txtStart.SelectionLength = txtStart.Text.Length;
		}

		private void txtEnd_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// number keys or the backspace
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || KeyAscii == Keys.Back)
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStart_Enter(object sender, System.EventArgs e)
		{
			txtStart.SelectionStart = 1;
			txtStart.SelectionLength = txtStart.Text.Length;
		}

		private void txtStart_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// number keys or the backspace
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || KeyAscii == Keys.Back)
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
