//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Payroll.Enums;
using Wisej.Web;

namespace TWPY0000
{
	public class modCustomReport
	{
		//=========================================================
		// vbPorter upgrade warning: GRIDTEXT As int	OnRead(string)
		public const int GRIDTEXT = 1;
		// vbPorter upgrade warning: GRIDDATE As int	OnRead(string)
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		// vbPorter upgrade warning: GRIDNUMRANGE As int	OnRead(string)
		public const int GRIDNUMRANGE = 4;
		// vbPorter upgrade warning: GRIDCOMBOIDNUM As int	OnRead(string)
		public const int GRIDCOMBOIDNUM = 5;
		// vbPorter upgrade warning: GRIDCOMBOTEXT As int	OnRead(string)
		public const int GRIDCOMBOTEXT = 6;
		// vbPorter upgrade warning: GRIDTEXTRANGE As int	OnRead(string)
		public const int GRIDTEXTRANGE = 7;
		// vbPorter upgrade warning: GRIDNONE As int	OnRead(string)
		public const int GRIDNONE = 8;
		// vbPorter upgrade warning: GRIDBOOLEAN As int	OnRead(string)
		public const int GRIDBOOLEAN = 9;
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object FixQuotes(string strValue)
		{
			object FixQuotes = null;
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		public static void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// vbPorter upgrade warning: strTemp As object	OnWrite(string())
			string[] strTemp;
			object strTemp2;
			int intCount;
			int intCounter;
			if (fecherFoundation.Strings.Trim(strSQL) == string.Empty)
			{
				Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(FCConvert.ToString(strTemp[0]), 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			Statics.intNumberOfSQLFields = Information.UBound(strTemp, 1);
			for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[intCounter])) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
					{
						if (Statics.strFields[intCount] == fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[intCounter])))
						{
							Statics.strFieldCaptions[intCounter] = Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object CheckForAS(string strFieldName, int intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As object	OnWrite(string())
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(fecherFoundation.Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(strTemp[1])) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(fecherFoundation.Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[0]));
			}
			return CheckForAS;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 50; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomReport, frmCustomLabels)
		public static void SetFormFieldCaptions(dynamic FormName, string ReportType)
		{
			// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
			// 
			// ****************************************************************
			// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
			// TO ADD A NEW REPORT TYPE.
			// ****************************************************************
			Statics.strReportType = fecherFoundation.Strings.UCase(ReportType);
			// CLEAR THE COMBO LIST ARRAY
			ClearComboListArray();
			if (fecherFoundation.Strings.UCase(ReportType) == "CUSTOMVACSICK")
			{
				SetCustomVacSick(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "CUSTOMCHECKDETAIL")
			{
				SetCustomCheckDetail(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "CUSTOMPAYTOTALS")
			{
				SetCustomPayTotals(ref FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "EMPLOYEEINFO") || (fecherFoundation.Strings.UCase(ReportType) == "PAYROLLLABELS"))
			{
				SetEmployeeInfoParameters(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "EMPLOYEEVACATION")
			{
				SetEmployeeVacationParameters(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "DATAENTRYFORMS")
			{
				SetDataEntryFormsParameters(ref FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "PAYSUMMARY") || (fecherFoundation.Strings.UCase(ReportType) == "TAXSUMMARY") || (fecherFoundation.Strings.UCase(ReportType) == "YTDTAXSUMMARY") || (fecherFoundation.Strings.UCase(ReportType) == "TAXABLEWAGESUMMARY"))
			{
				SetPaySummaryParameters(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "EMPLOYEEDEDUCTIONS")
			{
				SetEmployeeDeductionsParameters(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "EMPLOYEEMATCH")
			{
				SetEmployeeMatchParameters(ref FormName);
			}
			else if (fecherFoundation.Strings.UCase(ReportType) == "BANKLISTING")
			{
				SetBankListingParameters(ref FormName);
			}
			// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
			LoadSortList(ref FormName);
			// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
			LoadWhereGrid(ref FormName);
			// LOAD THE SAVED REPORT COMBO ON THE FORM
			FCUtils.CallByName(FormName, "LoadCombo", CallType.Method);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetEmployeeDeductionsParameters(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			// FormName.lstFields.AddItem "Pay Date"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 3
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber";
			Statics.strFields[1] = "tblEmployeeMaster.LastName";
			Statics.strFields[2] = "tblEmployeeMaster.FirstName";
			// strFields(3) = "PayDate"
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Last Name";
			Statics.strCaptions[2] = "First Name";
			// strCaptions(3) = "Pay Date"
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			// strWhereType(3) = GRIDDATE
			// FormName.fraFields.Tag = "SELECT tblEmployeeDeductions.*, tblEmployeeMaster.*, tblDeductionSetup.Description FROM (tblEmployeeDeductions INNER JOIN tblEmployeeMaster ON tblEmployeeDeductions.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblDeductionSetup ON tblEmployeeDeductions.DeductionCode = tblDeductionSetup.DeductionNumber"
			FormName.fraFields.Tag = "";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetBankListingParameters(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Bank Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Pay Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber";
			Statics.strFields[1] = "Bank";
			Statics.strFields[2] = "tblEmployeeMaster.LastName";
			Statics.strFields[3] = "tblEmployeeMaster.FirstName";
			Statics.strFields[4] = "PayDate";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Bank Number";
			Statics.strCaptions[2] = "Last Name";
			Statics.strCaptions[3] = "First Name";
			Statics.strCaptions[4] = "Pay Date";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDDATE);
			FormName.fraFields.Tag = "SELECT tblDirectDeposit.*, tblBanks.*, tblEmployeeMaster.* FROM (tblDirectDeposit INNER JOIN tblBanks ON tblDirectDeposit.Bank = tblBanks.ID) INNER JOIN tblEmployeeMaster ON tblDirectDeposit.EmployeeNumber = tblEmployeeMaster.EmployeeNumber";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetEmployeeMatchParameters(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Pay Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber";
			Statics.strFields[1] = "tblEmployeeMaster.LastName";
			Statics.strFields[2] = "tblEmployeeMaster.FirstName";
			Statics.strFields[3] = "PayDate";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Last Name";
			Statics.strCaptions[2] = "First Name";
			Statics.strCaptions[3] = "Pay Date";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDDATE);
			FormName.fraFields.Tag = "SELECT tblEmployersMatch.*, tblEmployeeMaster.*, tblDeductionSetup.Description FROM (tblEmployersMatch INNER JOIN tblEmployeeMaster ON tblEmployersMatch.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblDeductionSetup ON tblEmployersMatch.DeductionCode = tblDeductionSetup.DeductionNumber";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetCustomVacSick(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Full Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Dept/Div");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Type Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Type ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Accr Current");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Accr Fiscal");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Accr Calendar");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Used Current");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Used Fiscal");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Used Calendar");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Balance");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Code Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Code ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber as EmployeeNum";
			Statics.strFields[1] = "LastName+','+space(1)+FirstName+space(1)+MiddleName+space(1)+Desig AS FullName";
			Statics.strFields[2] = "LastName";
			Statics.strFields[3] = "FirstName";
			Statics.strFields[4] = "MiddleName";
			Statics.strFields[5] = "DeptDiv";
			Statics.strFields[6] = "tblCodeTypes.Description As TypeDescription";
			Statics.strFields[7] = "TypeID";
			Statics.strFields[8] = "AccruedCurrent";
			Statics.strFields[9] = "AccruedFiscal";
			Statics.strFields[10] = "AccruedCalendar";
			Statics.strFields[11] = "UsedCurrent";
			Statics.strFields[12] = "UsedFiscal";
			Statics.strFields[13] = "UsedCalendar";
			Statics.strFields[14] = "UsedBalance";
			Statics.strFields[15] = "tblCodes.Description As CodeDescription";
			Statics.strFields[16] = "CodeID";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Full Name";
			Statics.strCaptions[2] = "Last Name";
			Statics.strCaptions[3] = "First Name";
			Statics.strCaptions[4] = "Middle Name";
			Statics.strCaptions[5] = "Dept Div";
			Statics.strCaptions[6] = "TypeDescription";
			Statics.strCaptions[7] = "TypeID";
			Statics.strCaptions[8] = "Accr Curr";
			Statics.strCaptions[9] = "Accr Fiscal";
			Statics.strCaptions[10] = "Accr Cal";
			Statics.strCaptions[11] = "Used Current";
			Statics.strCaptions[12] = "Used Fiscal";
			Statics.strCaptions[13] = "Used Calendar";
			Statics.strCaptions[14] = "Balance";
			Statics.strCaptions[15] = "CodeDescription";
			Statics.strCaptions[16] = "CodeID";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDNONE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDNONE);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDCOMBOIDNUM);
			LoadGridCellAsCombo(FormName, 6, "Select * from tblCodeTypes", "Description", "ID", "Description");
			LoadGridCellAsCombo(FormName, 7, "select * from tblcodetypes", "Description", "ID", "Description");
			// FormName.fraFields.Tag = " FROM (tblVacationSick INNER JOIN tblCodeTypes ON tblVacationSick.TypeID = tblCodeTypes.ID) INNER JOIN tblEmployeeMaster ON tblVacationSick.EmployeeNumber = tblEmployeeMaster.EmployeeNumber"
			FormName.fraFields.Tag = " FROM ((tblVacationSick INNER JOIN tblCodeTypes ON tblVacationSick.TypeID = tblCodeTypes.ID) INNER JOIN tblEmployeeMaster ON tblVacationSick.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) LEFT JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetCustomPayTotals(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Pay Cat Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Standard Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Pay Category ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Standard Pay Total ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Current Total");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("MTD Total");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("QTD Total");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Fiscal YTD Total");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Calendar YTD Total");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber As EmployeeNum";
			Statics.strFields[1] = "LastName";
			Statics.strFields[2] = "FirstName";
			Statics.strFields[3] = "MiddleName";
			Statics.strFields[4] = "PayCategoryDescription";
			Statics.strFields[5] = "StandardDescription";
			Statics.strFields[6] = "PayCategory";
			Statics.strFields[7] = "StandardPayTotal";
			Statics.strFields[8] = "CurrentTotal";
			Statics.strFields[9] = "MTDTotal";
			Statics.strFields[10] = "QTDTotal";
			Statics.strFields[11] = "FiscalYTDTotal";
			Statics.strFields[12] = "CalendarYTDTotal";
			Statics.strCaptions[0] = "ID";
			Statics.strCaptions[1] = "Last Name";
			Statics.strCaptions[2] = "First Name";
			Statics.strCaptions[3] = "Middle Name";
			Statics.strCaptions[4] = "Pay Cat Description";
			Statics.strCaptions[5] = "Standard Description";
			Statics.strCaptions[6] = "Pay Category";
			Statics.strCaptions[7] = "Standard Pay Total";
			Statics.strCaptions[8] = "Current Total";
			Statics.strCaptions[9] = "MTD Total";
			Statics.strCaptions[10] = "QTD Total";
			Statics.strCaptions[11] = "Fiscal YTD Total";
			Statics.strCaptions[12] = "Calendar YTD Total";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDNONE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDNONE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDNUMRANGE);
			LoadGridCellAsCombo(FormName, 6, "Select * from tblPayCategories", "Description", "ID", "Description");
			LoadGridCellAsCombo(FormName, 7, "Select * from tblStandardPayTotals", "Description", "ID", "Description");
			FormName.fraFields.Tag = ", tblPayCategories.Description as PayCategoryDescription, tblStandardPayTotals.Description as StandardDescription FROM ((tblEmployeeMaster INNER JOIN tblPayTotals ON tblEmployeeMaster.EmployeeNumber = tblPayTotals.EmployeeNumber) LEFT JOIN tblPayCategories ON tblPayTotals.PayCategory = tblPayCategories.ID) LEFT JOIN tblStandardPayTotals ON tblPayTotals.StandardPayTotal = tblStandardPayTotals.ID";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetEmployeeInfoParameters(ref dynamic FormName)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strOpen1;
			string strOpen2;
			strOpen1 = "";
			strOpen2 = "";
			rsTemp.OpenRecordset("select open1,open2 from tbldefaultinformation", "twpy0000.vb1");
			if (!rsTemp.EndOfFile())
			{
				strOpen1 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("open1")));
				strOpen2 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("open2")));
			}
			if (strOpen1 == "")
			{
				strOpen1 = "Code 1";
			}
			if (strOpen2 == "")
			{
				strOpen2 = "Code 2";
			}
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Full Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Desig");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("SSN");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Date Hired");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Birth Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Anniversary Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Date Terminated");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("DeptDiv");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("SeqNumber");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("GroupID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Status");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Address1");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Address2");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Date Retired");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			// MATTHEW 02/03/2005
			// ADDED THIS FIELD FOR THE BLS REPORT AS SOME PEOPLE WILL NOT SHOW ON THE BLS
			// REPORT BUT THEY STILL SHOW ON THE C-1 AND T
			FormName.lstFields.AddItem("BLS Worksite ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			// MATTHEW 5/25/2005 CALL ID 68488
			FormName.lstFields.AddItem("Base Rate");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			// MATTHEW 5/25/2005 CALL ID 77575
			FormName.lstFields.AddItem("Gender");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			// MATTHEW 10/25/2005 CALL ID 76227
			FormName.lstFields.AddItem("Part Time Employee");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("Federal Filing Status");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("State Filing Status");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			FormName.lstFields.AddItem(strOpen1);
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 27);
			FormName.lstFields.AddItem(strOpen2);
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 28);
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber as EmpNumber";
			Statics.strFields[1] = "LastName+','+space(1)+FirstName+space(1)+MiddleName+space(1)+Desig AS FullName";
			Statics.strFields[2] = "MiddleName";
			Statics.strFields[3] = "LastName";
			Statics.strFields[4] = "FirstName";
			Statics.strFields[5] = "Desig";
			Statics.strFields[6] = "SSN";
			Statics.strFields[7] = "DateHire";
			Statics.strFields[8] = "DateBirth";
			Statics.strFields[9] = "DateAnniversary";
			Statics.strFields[10] = "DateTerminated";
			Statics.strFields[11] = "DeptDiv";
			Statics.strFields[12] = "SeqNumber";
			Statics.strFields[13] = "GroupID";
			Statics.strFields[14] = "Status";
			Statics.strFields[15] = "Address1";
			Statics.strFields[16] = "Address2";
			Statics.strFields[17] = "City";
			Statics.strFields[18] = "States.Description as StateDescription";
			Statics.strFields[19] = "Zip";
			Statics.strFields[20] = "DateRetired";
			Statics.strFields[21] = "BLSWorksiteID";
			Statics.strFields[22] = "BaseRate";
			Statics.strFields[23] = "Sex";
			Statics.strFields[24] = "FTOrPT";
			Statics.strFields[25] = "FedFilingStatusID";
			Statics.strFields[26] = "StateFilingStatusID";
			Statics.strFields[27] = "Code1";
			Statics.strFields[28] = "Code2";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Full Name";
			Statics.strCaptions[2] = "MiddleName";
			Statics.strCaptions[3] = "Last Name";
			Statics.strCaptions[4] = "First Name";
			Statics.strCaptions[5] = "Desig";
			Statics.strCaptions[6] = "SSN";
			Statics.strCaptions[7] = "Date Hired";
			Statics.strCaptions[8] = "Birth Date";
			Statics.strCaptions[9] = "Anniversary Date";
			Statics.strCaptions[10] = "Date Terminated";
			Statics.strCaptions[11] = "Dept Division";
			Statics.strCaptions[12] = "Sequence Number";
			Statics.strCaptions[13] = "Group ID";
			Statics.strCaptions[14] = "Status";
			Statics.strCaptions[15] = "Address1";
			Statics.strCaptions[16] = "Address2";
			Statics.strCaptions[17] = "City";
			Statics.strCaptions[18] = "State";
			Statics.strCaptions[19] = "Zip";
			Statics.strCaptions[20] = "Date Retired";
			Statics.strCaptions[21] = "BLS Worksite ID";
			Statics.strCaptions[22] = "Pay Rate";
			Statics.strCaptions[23] = "Gender";
			Statics.strCaptions[24] = "Part Time Employee";
			Statics.strCaptions[25] = "Federal Status";
			Statics.strCaptions[26] = "State Status";
			Statics.strCaptions[27] = strOpen1;
			Statics.strCaptions[28] = strOpen2;
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[22] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[23] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[24] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[25] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[26] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[27] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[28] = FCConvert.ToString(GRIDTEXTRANGE);
			LoadGridCellAsCombo(FormName, 14, "#0;Active|#1;Terminated|#2;Suspended|#3;Hold 1 Week|#4;Retired|#5;Resigned");
			LoadGridCellAsCombo(FormName, 23, "#0;Male|#2;Female");
			LoadGridCellAsCombo(FormName, 24, "#0;False|#1;True");
			LoadGridCellAsCombo(FormName, 25, LoadPayStatusString("FEDERAL"));
			LoadGridCellAsCombo(FormName, 26, LoadPayStatusString("STATE"));
			// FormName.fraFields.Tag = " FROM tblEmployeeMaster Left JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID"
			// call id 76611
			FormName.fraFields.Tag = "EmployeeInformation";
			// FormName.fraFields.Tag = " ,tblPayrollDistribution.RecordNumber FROM (tblEmployeeMaster LEFT JOIN tblPayrollDistribution ON tblEmployeeMaster.EmployeeNumber = tblPayrollDistribution.EmployeeNumber) INNER JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID WHERE tblEmployeeMaster.EmployeeNumber <>''"
			// FormName.fraFields.Tag = " FROM tblEmployeeMaster INNER JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID WHERE tblEmployeeMaster.EmployeeNumber <>''"
		}

		private static string LoadPayStatusString(string strType)
		{
			string LoadPayStatusString = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strReturn;
			strReturn = "";
			LoadPayStatusString = "";
			rsLoad.OpenRecordset("SELECT * FROM tblPayStatuses WHERE Type='" + strType + "'", "TWPY0000.vb1");
			rsLoad.MoveLast();
			rsLoad.MoveFirst();
			
			while (!rsLoad.EndOfFile())
			{
				// display Filing status
				strReturn += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (fecherFoundation.Strings.Trim(strReturn) != string.Empty)
			{
				strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
			}
			LoadPayStatusString = strReturn;
			return LoadPayStatusString;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetCustomCheckDetail(ref dynamic FormName)
		{
			// ADD THE FIELDS THAT WILL BE SHOWN ON THE CHECK DETAIL CUSTOM REPORT
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Employee Full Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Status");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Check Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Pay Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Pay Run ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Journal Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Warrant Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Net Pay");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Gross Pay");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Check Void");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("FederalTaxWH");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("StateTaxWH");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("FICATaxWH");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("MedicareTaxWH");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("FederalTaxGross");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("StateTaxGross");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("FICATaxGross");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("MedicareTaxGross");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("DeptDiv");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			// IF A CHANGE IS MADE TO THESE FIELDS THEN BE SURE TO CHECK OUT THE
			// FUNCTION chkGroup_Click ON THE FORM FRMCUSTOMREPORT
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber as EmpNumber";
			Statics.strFields[1] = "LastName+','+space(1)+FirstName+space(1)+MiddleName+space(1)+Desig AS FullName";
			Statics.strFields[2] = "LastName";
			Statics.strFields[3] = "FirstName";
			Statics.strFields[4] = "tblEmployeeMaster.Status";
			Statics.strFields[5] = "CheckNumber";
			Statics.strFields[6] = "PayDate";
			Statics.strFields[7] = "PayRunID";
			Statics.strFields[8] = "JournalNumber";
			Statics.strFields[9] = "WarrantNumber";
			Statics.strFields[10] = "NetPay";
			Statics.strFields[11] = "GrossPay";
			Statics.strFields[12] = "CheckVoid";
			Statics.strFields[13] = "FederalTaxWH";
			Statics.strFields[14] = "StateTaxWH";
			Statics.strFields[15] = "FICATaxWH";
			Statics.strFields[16] = "MedicareTaxWH";
			Statics.strFields[17] = "FederalTaxGross";
			Statics.strFields[18] = "StateTaxGross";
			Statics.strFields[19] = "FICATaxGross";
			Statics.strFields[20] = "MedicareTaxGross";
			Statics.strFields[21] = "DeptDiv";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Full Name";
			Statics.strCaptions[2] = "Last Name";
			Statics.strCaptions[3] = "First Name";
			Statics.strCaptions[4] = "Status";
			Statics.strCaptions[5] = "Check #";
			Statics.strCaptions[6] = "Pay Date";
			Statics.strCaptions[7] = "Pay Run ID";
			Statics.strCaptions[8] = "Journal";
			Statics.strCaptions[9] = "Warrant";
			Statics.strCaptions[10] = "Net Pay";
			Statics.strCaptions[11] = "Gross Pay";
			Statics.strCaptions[12] = "Check Void";
			Statics.strCaptions[13] = "FederalTaxWH";
			Statics.strCaptions[14] = "StateTaxWH";
			Statics.strCaptions[15] = "FICATaxWH";
			Statics.strCaptions[16] = "MedicareTaxWH";
			Statics.strCaptions[17] = "FederalTaxGross";
			Statics.strCaptions[18] = "StateTaxGross";
			Statics.strCaptions[19] = "FICATaxGross";
			Statics.strCaptions[20] = "MedicareTaxGross";
			Statics.strCaptions[21] = "DeptDiv";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDTEXTRANGE);
			LoadGridCellAsCombo(FormName, 4, "#0;Active|#1;InActive|#2;Terminated|#3;Hold 1 Week|#4;Retired|#5;Resigned");
			LoadGridCellAsCombo(FormName, 12, "#0;False|#1;True");
			FormName.fraFields.Tag = ",tblcheckdetail.status, tblCheckDetail.TotalRecord FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON (tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) Where (tblCheckDetail.TotalRecord = 1 AND AdjustRecord = 0)";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetDataEntryFormsParameters(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Pay Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			Statics.strFields[0] = "EmployeeNumber";
			Statics.strFields[1] = "LastName";
			Statics.strFields[2] = "FirstName";
			Statics.strFields[3] = "PayDate";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Last Name";
			Statics.strCaptions[2] = "First Name";
			Statics.strCaptions[3] = "Pay Date";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDDATE);
			FormName.fraFields.Tag = "Select * from tblEmployeeMaster where DataEntry = True";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetEmployeeVacationParameters(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Pay Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			Statics.strFields[0] = "EmployeeNumber";
			Statics.strFields[1] = "LastName";
			Statics.strFields[2] = "FirstName";
			Statics.strFields[3] = "PayDate";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Last Name";
			Statics.strCaptions[2] = "First Name";
			Statics.strCaptions[3] = "Pay Date";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDDATE);
			FormName.fraFields.Tag = "SELECT tblCodes.*, tblVacationSick.*, tblEmployeeMaster.*, tblCodes.Code" + " FROM ((tblVacationSick LEFT JOIN tblCodes ON tblVacationSick.CodeID = tblCodes.ID) LEFT JOIN tblCodeTypes ON tblVacationSick.TypeID = tblCodeTypes.ID) INNER JOIN tblEmployeeMaster ON tblVacationSick.EmployeeNumber = tblEmployeeMaster.EmployeeNumber";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetPaySummaryParameters(ref dynamic FormName)
		{
			FormName.lstFields.AddItem("Employee Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Employee Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Employee First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Pay Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			Statics.strFields[0] = "tblEmployeeMaster.EmployeeNumber";
			Statics.strFields[1] = "tblEmployeeMaster.LastName";
			Statics.strFields[2] = "tblEmployeeMaster.FirstName";
			Statics.strFields[3] = "PayDate";
			Statics.strCaptions[0] = "Employee Number";
			Statics.strCaptions[1] = "Last Name";
			Statics.strCaptions[2] = "First Name";
			Statics.strCaptions[3] = "Pay Date";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDDATE);
			FormName.fraFields.Tag = "Select Distinct tblPayTotals.EmployeeNumber,tblEmployeeMaster.Lastname,tblEmployeeMaster.Firstname from tblpaytotals INNER JOIN tblEmployeeMaster ON tblPayTotals.EmployeeNumber = tblEmployeeMaster.EmployeeNumber";
		}

		public static void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (fecherFoundation.Strings.UCase(Statics.strReportType))
			{
			// Case "DOGS"
			// 
			// 
			// Case "BIRTHS"
				default:
					{
						// GridName.BackColor = vbBlue
						// GridName.ForeColor = vbWhite
						break;
					}
			}
			//end switch
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		public static void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (fecherFoundation.Strings.UCase(Statics.strReportType) == "DOGS")
			{
			}
			else if (fecherFoundation.Strings.UCase(Statics.strReportType) == "BIRTHS")
			{
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadSortList(ref dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
            var ssnViewPermission = SSNViewType.None;
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
            for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
                if (!(FormName.lstFields.List(intCounter).ToString().ToLower() == "ssn" ||
                    FormName.lstFields.List(intCounter).ToString().ToLower() == "socialsecuritynumber") || ssnViewPermission == SSNViewType.Full)
                {
                    FormName.lstSort.AddItem(FormName.lstFields.List(intCounter));
                    FormName.lstSort.ItemData(FormName.lstSort.NewIndex, FormName.lstFields.ItemData(intCounter));
                }				
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadWhereGrid(ref dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
            var ssnViewPermission = SSNViewType.None;
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
            for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				FormName.vsWhere.AddItem(FormName.lstFields.List(intCounter));
				// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
				// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
				// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
				if (Conversion.Val(Statics.strWhereType[intCounter]) == GRIDNONE)
				{
					//FC:FINAL:DDU:#i2120 - set correct background color to form
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else if (Conversion.Val(Statics.strWhereType[intCounter]) != GRIDDATE && Conversion.Val(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && Conversion.Val(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
				{
					//FC:FINAL:DDU:#i2120 - set correct background color to form
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}

                if ((Statics.strFields[intCounter].ToLower() == "ssn" ||
                     Statics.strFields[intCounter].ToLower() == "socialsecuritynumber") &&
                    ssnViewPermission != SSNViewType.Full)
                {
                    FormName.vsWhere.RowHidden(intCounter, true);                    
                }
			}
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			// THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
			// A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
			// THE USER MOVES TO A NEW ONE.
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, FCConvert.ToInt32(FormName.vsWhere.WidthOriginal * 0.55));
			FormName.vsWhere.ColWidth(1, FCConvert.ToInt32(FormName.vsWhere.WidthOriginal * 0.2));
			FormName.vsWhere.ColWidth(2, FCConvert.ToInt32(FormName.vsWhere.WidthOriginal * 0.2));
			FormName.vsWhere.ColWidth(3, 0);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadGridCellAsCombo(dynamic FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, modGlobalVariables.DEFAULTDATABASE);
				while (!rsCombo.EndOfFile())
				{
					Statics.strComboList[intRowNumber, 0] += "|";
					Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
					Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
				if (Statics.strComboList[intRowNumber, 0] != string.Empty)
				{
					Statics.strComboList[intRowNumber, 0] = Strings.Mid(Statics.strComboList[intRowNumber, 0], 2);
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static object IsValidDate(string DateToCheck)
		{
			object IsValidDate = null;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}

		public class StaticVariables
		{
			// vbPorter upgrade warning: intNumberOfSQLFields As int	OnWriteFCConvert.ToInt32(
			public int intNumberOfSQLFields;
			public int[] strFieldWidth = new int[50 + 1];
			public string strCustomSQL = "";
			public string strColumnCaptions = "";
			public string[] strFields = new string[50 + 1];
			public string[] strSumFields = new string[50 + 1];
			public double[] dblSumFieldsValue = new double[50 + 1];
			public string[] strFieldNames = new string[50 + 1];
			public string[] strFieldCaptions = new string[50 + 1];
			public string[] strCaptions = new string[50 + 1];
			public string strCustomTitle = "";
			public string strReportType = string.Empty;
			public string[,] strComboList = new string[50 + 1, 50 + 1];
			// vbPorter upgrade warning: strWhereType As string	OnWrite
			public string[] strWhereType = new string[50 + 1];
			public string strPreSetReport = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
