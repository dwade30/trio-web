//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public partial class frmConfirmation : BaseForm
	{
		public frmConfirmation()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmConfirmation InstancePtr
		{
			get
			{
				return (frmConfirmation)Sys.GetInstance(typeof(frmConfirmation));
			}
		}

		protected frmConfirmation _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW LARRABEE
		// DATE:       DECEMBER 26, 2002
		//
		// NOTES:
		//
		//
		// **************************************************
		private clsDRWrapper rsData = new clsDRWrapper();

		private void frmConfirmation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmConfirmation properties;
			//frmConfirmation.ScaleWidth	= 11880;
			//frmConfirmation.ScaleHeight	= 6825;
			//frmConfirmation.LinkTopic	= "Form1";
			//End Unmaped Properties
			LoadGridData();
		}

		private void LoadGridData()
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow = 0;
			string strName = "";
			string strType = "";
			bool boolDistType = false;
			bool boolMatchType = false;
			bool boolTotalType = false;
			bool boolVSType = false;
			// GET THE PA
			rsData.OpenRecordset("Select * from tblTempPayProcess Order by EmployeeNumber,DistributionRecord,DeductionRecord,MatchRecord,VSRecord,TotalRecord", modGlobalVariables.DEFAULTDATABASE);
			if (!rsData.EndOfFile())
			{
				vsGrid.Cols = 11;
				vsGrid.Rows = 1;
			}
			// SET THE HEADINGS FOR THE GRID
			vsGrid.TextMatrix(0, 1, "Employee #");
			vsGrid.ColWidth(1, 1000);
			vsGrid.ColWidth(2, 1300);
			vsGrid.ColWidth(3, 2000);
			vsGrid.ColWidth(4, 1300);
			vsGrid.ColWidth(5, 1300);
			vsGrid.ColWidth(6, 1300);
			vsGrid.ColWidth(7, 1300);
			vsGrid.ColWidth(8, 1400);
			vsGrid.ColWidth(9, 1400);
			vsGrid.ColWidth(10, 2000);
			while (!rsData.EndOfFile())
			{
				intRow += 1;
				vsGrid.Rows += 1;
				if (!rsData.EndOfFile())
				{
					if (FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")) == strName)
					{
						if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DistributionRecord")))
						{
							if (boolDistType == true)
							{
								vsGrid.TextMatrix(intRow, 3, FCConvert.ToString(rsData.Get_Fields_String("EmployeeName")));
								vsGrid.TextMatrix(intRow, 4, FCConvert.ToString(rsData.Get_Fields_String("Status")));
								vsGrid.TextMatrix(intRow, 5, FCConvert.ToString(rsData.Get_Fields("CheckNumber")));
								vsGrid.TextMatrix(intRow, 6, FCConvert.ToString(rsData.Get_Fields_DateTime("PayDate")));
								vsGrid.TextMatrix(intRow, 7, FCConvert.ToString(rsData.Get_Fields_Decimal("NetPay")));
								vsGrid.TextMatrix(intRow, 8, FCConvert.ToString(rsData.Get_Fields("JournalNumber")));
								vsGrid.TextMatrix(intRow, 9, FCConvert.ToString(rsData.Get_Fields_Int32("WarrantNumber")));
								vsGrid.TextMatrix(intRow, 10, FCConvert.ToString(rsData.Get_Fields_Int32("AccountingPeriod")));
								vsGrid.RowOutlineLevel(intRow, 2);
								vsGrid.IsSubtotal(intRow, false);
							}
							else
							{
								vsGrid.TextMatrix(intRow, 2, "Distributions");
								vsGrid.TextMatrix(intRow, 3, "Employee Name");
								vsGrid.TextMatrix(intRow, 4, "Status");
								vsGrid.TextMatrix(intRow, 5, "Check Number");
								vsGrid.TextMatrix(intRow, 6, "Pay Date");
								vsGrid.TextMatrix(intRow, 7, "Net Pay");
								vsGrid.TextMatrix(intRow, 8, "Journal Number");
								vsGrid.TextMatrix(intRow, 9, "Warrant Number");
								vsGrid.TextMatrix(intRow, 10, "Accounting Period");
								vsGrid.RowOutlineLevel(intRow, 1);
								vsGrid.IsSubtotal(intRow, true);
								boolDistType = true;
								goto NextRow;
							}
						}
						else if (rsData.Get_Fields_Boolean("MatchRecord"))
						{
							if (boolMatchType == true)
							{
								vsGrid.TextMatrix(intRow, 3, FCConvert.ToString(rsData.Get_Fields_Int32("MatchDeductionNumber")));
								vsGrid.TextMatrix(intRow, 4, FCConvert.ToString(rsData.Get_Fields("MatchTaxCode")));
								vsGrid.TextMatrix(intRow, 5, FCConvert.ToString(rsData.Get_Fields("MatchAmount")));
								vsGrid.TextMatrix(intRow, 6, FCConvert.ToString(rsData.Get_Fields_Boolean("MatchHitMax")));
								vsGrid.TextMatrix(intRow, 7, FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber")));
								vsGrid.RowOutlineLevel(intRow, 2);
								vsGrid.IsSubtotal(intRow, false);
							}
							else
							{
								vsGrid.TextMatrix(intRow, 2, "Employer Match");
								vsGrid.TextMatrix(intRow, 3, "Deduction Number");
								vsGrid.TextMatrix(intRow, 4, "Tax Code");
								vsGrid.TextMatrix(intRow, 5, "Amount");
								vsGrid.TextMatrix(intRow, 6, "Hit Max");
								vsGrid.TextMatrix(intRow, 7, "Account Number");
								vsGrid.RowOutlineLevel(intRow, 1);
								vsGrid.IsSubtotal(intRow, true);
								boolMatchType = true;
								goto NextRow;
							}
						}
						else if (rsData.Get_Fields_Boolean("VSRecord"))
						{
							if (boolVSType == true)
							{
								vsGrid.TextMatrix(intRow, 3, FCConvert.ToString(rsData.Get_Fields_Double("VSAccrued")));
								vsGrid.TextMatrix(intRow, 4, FCConvert.ToString(rsData.Get_Fields_Double("VSUsed")));
								vsGrid.TextMatrix(intRow, 5, FCConvert.ToString(GetPayFreqDesc(rsData.Get_Fields_Int32("VSTypeID"))));
								vsGrid.RowOutlineLevel(intRow, 2);
								vsGrid.IsSubtotal(intRow, false);
							}
							else
							{
								vsGrid.TextMatrix(intRow, 2, "Vacation / Sick");
								vsGrid.TextMatrix(intRow, 3, "Accrued");
								vsGrid.TextMatrix(intRow, 4, "Used");
								vsGrid.TextMatrix(intRow, 5, "Type");
								vsGrid.RowOutlineLevel(intRow, 1);
								vsGrid.IsSubtotal(intRow, true);
								boolVSType = true;
								goto NextRow;
							}
						}
						else if (rsData.Get_Fields_Boolean("TotalRecord"))
						{
							if (boolTotalType == true)
							{
								vsGrid.TextMatrix(intRow, 3, FCConvert.ToString(rsData.Get_Fields("FederalTaxWH")));
								vsGrid.TextMatrix(intRow, 4, FCConvert.ToString(rsData.Get_Fields("StateTaxWH")));
								vsGrid.TextMatrix(intRow, 5, FCConvert.ToString(rsData.Get_Fields("FICATaxWH")));
								vsGrid.TextMatrix(intRow, 6, FCConvert.ToString(rsData.Get_Fields("MedicareTaxWH")));
								vsGrid.RowOutlineLevel(intRow, 2);
								vsGrid.IsSubtotal(intRow, false);
							}
							else
							{
								vsGrid.TextMatrix(intRow, 2, "Tax Totals");
								vsGrid.TextMatrix(intRow, 3, "Federal Tax WH");
								vsGrid.TextMatrix(intRow, 4, "State Tax WH");
								vsGrid.TextMatrix(intRow, 5, "FICA Tax WH");
								vsGrid.TextMatrix(intRow, 6, "Medicare Tax WH");
								vsGrid.RowOutlineLevel(intRow, 1);
								vsGrid.IsSubtotal(intRow, true);
								boolTotalType = true;
								goto NextRow;
							}
						}
					}
					else
					{
						vsGrid.TextMatrix(intRow, 1, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
						vsGrid.RowOutlineLevel(intRow, 0);
						vsGrid.IsSubtotal(intRow, true);
						strName = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
						boolDistType = false;
						boolMatchType = false;
						boolTotalType = false;
						boolVSType = false;
						goto NextRow;
					}
				}
				rsData.MoveNext();
				NextRow:
				;
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, 1, intRow, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			// COLLAPSE ALL ROWS TO SHOW JUST THE HEADERS
			for (intRow = 1; intRow <= (vsGrid.Rows - 1); intRow++)
			{
				if (vsGrid.RowOutlineLevel(intRow) == 0 || vsGrid.RowOutlineLevel(intRow) == 1)
				{
					vsGrid.IsCollapsed(intRow, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			vsGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeOutline;
			modColorScheme.ColorGrid(vsGrid, 1, vsGrid.Rows - 1, 1, (vsGrid.Cols - 1), false);
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public object GetPayFreqDesc(int lngID)
		{
			object GetPayFreqDesc = null;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblCodeTypes where ID = " + FCConvert.ToString(lngID), modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				GetPayFreqDesc = string.Empty;
			}
			else
			{
				GetPayFreqDesc = GetPayFreqDesc + rsData.Get_Fields("Description");
			}
			return GetPayFreqDesc;
		}

		private void frmConfirmation_Resize(object sender, System.EventArgs e)
		{
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.13));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
			vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.13));
			vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.13));
			vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.13));
			vsGrid.ColWidth(7, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.13));
			vsGrid.ColWidth(8, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.14));
			vsGrid.ColWidth(9, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.14));
			vsGrid.ColWidth(10, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
		}
	}
}
