//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmDeductionSetup : BaseForm
	{
		public frmDeductionSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDeductionSetup InstancePtr
		{
			get
			{
				return (frmDeductionSetup)Sys.GetInstance(typeof(frmDeductionSetup));
			}
		}

		protected frmDeductionSetup _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 25,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbDeductions    As DAO.Database
		private clsDRWrapper rsDeductions = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		private DialogResult intDataChanged;
		// vbPorter upgrade warning: intRowNumber As int	OnWriteFCConvert.ToInt32(
		private int intRowNumber;
		private bool boolSkip;
		// vbPorter upgrade warning: dblAmtPercentTotal As double	OnWrite(int, double, string)
		private double dblAmtPercentTotal;
		private string tEmp = "";
		// used to put txtacct1 data onto grid
		private bool boolSaveOK;
		private bool boolCancel;
		const int CNSTVSDEDUCTIONSCOL = 10;
		const int CNSTCOLUNEMPLOYMENTEXEMPT = 15;
		private clsGridAccount clsGA = new clsGridAccount();
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsDeductions.Row < 0)
					return;
				if (vsDeductions.Col < 0)
					return;
				// need to do some sort of validation
				if (MessageBox.Show("This action will delete record #" + vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, 1) + ". Continue?", "Payroll Deduction Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// we don't want to remove this record from the table as there is still
					// a deduction number ??? be we want to clear it out.
					// Call dbDeductions.Execute("Update tblDeductionSetup set Description ='"
					// & "" & "', FrequencyCode = 0,Amount = 0,AmountType = ' ',Limit = 0, LimitCode = ' ',TaxStatusCode = "
					// Call dbDeductions.Execute("Delete from tblDeductionSetup Where ID = " & vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, 14))
					MessageBox.Show("Deductions cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsDeductions);
					// clear the edit symbol from the row number
					vsDeductions.TextMatrix(vsDeductions.Row, 0, Strings.Mid(vsDeductions.TextMatrix(vsDeductions.Row, 0), 1, (vsDeductions.TextMatrix(vsDeductions.Row, 0).Length < 2 ? 2 : vsDeductions.TextMatrix(vsDeductions.Row, 0).Length - 2)));
					// load the grid with the new data to show the 'clean out' of the current row
					LoadGrid();
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int lngMax;
				string strNumber;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				// we need to ask which deduction number they want to add so if we currently only
				// have 5 records and they want to set #100 then we need to add records 6 - 100
				strNumber = Interaction.InputBox("Enter Deduction Number to add.", "Payroll Deduction Setup", FCConvert.ToString(FCConvert.ToInt32(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Rows - 1, 1))) + 1));
				if (Conversion.Val(strNumber) != 0)
				{
					// check to see if this deduction number actuall exists
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (strNumber == vsDeductions.TextMatrix(intCounter, 1))
						{
							// set the cursor to the desired deduction number
							vsDeductions.Select(FCConvert.ToInt32(strNumber), 2);
							return;
						}
					}
					vsDeductions.Rows += 1;
					// assign a row number to this new record
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 0, vsDeductions.TextMatrix(vsDeductions.Rows - 1, 0) + ".");
					// add the deduction number to the first visible column
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 1, strNumber);
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 2, string.Empty);
					rsDeductions.OpenRecordset("Select ID from tblTaxStatusCodes where TaxStatusCode = 'T'", "TWPY0000.vb1");
					if (!rsDeductions.EndOfFile())
					{
						vsDeductions.TextMatrix(vsDeductions.Rows - 1, 3, FCConvert.ToString(rsDeductions.Get_Fields("ID")));
					}
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 4, FCConvert.ToString(0));
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 5, "Dollars");
					rsDeductions.OpenRecordset("Select ID from tblFrequencyCodes where FrequencyCode = 'W'", "TWPY0000.vb1");
					if (!rsDeductions.EndOfFile())
					{
						vsDeductions.TextMatrix(vsDeductions.Rows - 1, 7, FCConvert.ToString(rsDeductions.Get_Fields("ID")));
					}
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 8, FCConvert.ToString(999999.99));
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 9, "Life");
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 12, "Active");
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, 13, FCConvert.ToString(9));
					vsDeductions.TextMatrix(vsDeductions.Rows - 1, CNSTCOLUNEMPLOYMENTEXEMPT, FCConvert.ToString(false));
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Rows - 1, 6, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Rows - 1, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Select(vsDeductions.Rows - 1, 2);
				}
				else
				{
					// set the cursor to the desired deduction number
					vsDeductions.Select(FCConvert.ToInt32(strNumber), 2);
				}
				// increment the counter as to how many records are dirty
				intDataChanged += 1;
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Distribution Setup");
				// ------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// show the deduction setup report
				// rptDeductionSetup.Show
				frmReportViewer.InstancePtr.Init(rptDeductionSetup.InstancePtr, boolAllowEmail: true, strAttachmentName: "DeductionSetup");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				// Call SaveChanges
				SetGridProperties();
				// load the grid of current data from the database
				// Call LoadGrid
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			dblAmtPercentTotal = 0;
			boolSaveOK = false;
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			for (intCount = 1; intCount <= (vsDeductions.Rows - 1); intCount++)
			{
				if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCount, 5)) == "Percent")
				{
					if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCount, 6)) == string.Empty)
					{
						MessageBox.Show("Percent Cannot Be 0", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsDeductions.Select(intCount, 6);
						NotValidData = true;
						return NotValidData;
					}
					if (Conversion.Val(vsDeductions.TextMatrix(intCount, 4)) > 100)
					{
						MessageBox.Show("Amounts For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsDeductions.Select(intCount, 5);
						NotValidData = true;
						return NotValidData;
					}
				}
			}
			NotValidData = false;
			boolSaveOK = true;
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmDeductionSetup_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDeductionSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// If KeyCode = vbKeyF11 Then Call mnuSave_Click
			// If KeyCode = vbKeyF12 Then
			// Call cmdSave_Click
			// If boolSaveOK Then Call cmdExit_Click
			// End If
			if (fecherFoundation.Strings.UCase(this.ActiveControl.GetName()) == "VSDEDUCTIONS")
			{
				if (vsDeductions.Col == CNSTVSDEDUCTIONSCOL && vsDeductions.Row > 0)
				{
					modNewAccountBox.CheckFormKeyDown(vsDeductions, vsDeductions.Row, vsDeductions.Col, KeyCode, Shift, vsDeductions.EditSelStart, vsDeductions.EditText, vsDeductions.EditSelLength);
				}
			}
		}

		private void frmDeductionSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDeductionSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeductionSetup properties;
			//frmDeductionSetup.ScaleWidth	= 11820;
			//frmDeductionSetup.ScaleHeight	= 8490;
			//frmDeductionSetup.LinkTopic	= "Form1";
			//frmDeductionSetup.LockControls	= -1  'True;
			//End Unmaped Properties
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vsElasticLight1.Enabled = True
				vsDeductions.Cols = 16;
				clsGA.GRID7Light = vsDeductions;
				clsGA.AccountCol = CNSTVSDEDUCTIONSCOL;
				clsGA.DefaultAccountType = "G";
				clsGA.OnlyAllowDefaultType = false;
				clsGA.Validation = true;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				rsDeductions.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				clsHistoryClass.SetGridIDColumn("PY", "vsDeductions", 14);
				// Load the grid with data
				LoadGrid();
				//vsDeductions.FixedCols = 2;
				intDataChanged = 0;
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsRecNumber = new clsDRWrapper();
				vsDeductions.Rows = 1;
				// get all of the records from the database
				rsDeductions.OpenRecordset("Select * from tblDeductionSetup Order by DeductionNumber", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// set the number of rows in the grid
					vsDeductions.Rows = rsDeductions.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						// **************************************************************************
						// COMMENTED THIS OUT PER CALL ID 3045 AND DEB2 ON 6/27/03
						// THIS BIT OF CODE MADE SURE THAT THERE WOULD BE NO SPACES
						// BETWEEN THE DEDUCTION NUMBERS
						// NOW THAT I HAVE COMMENTED THIS OUT THERE WILL BE A GAP
						// IN DEDUCTION NUMBERS.
						// If Trim(rsDeductions.Fields("DeductionNumber") & " ") <> intCounter Then
						// Call rsRecNumber.Execute("Update tblDeductionSetup SET DeductionNumber = " & intCounter & " where DeductionNumber = " & rsDeductions.Fields("DeductionNumber"), "Twpy0000.vb1")
						// End If
						// **************************************************************************
						vsDeductions.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						vsDeductions.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionNumber") + " "));
						vsDeductions.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " "));
						vsDeductions.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("TaxStatusCode") + " "));
						vsDeductions.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Amount") + " "));
						vsDeductions.TextMatrix(intCounter, 5, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " "));
						// If vsDeductions.TextMatrix(intCounter, 5) = "Dollars" Then
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 6, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						// End If
						vsDeductions.TextMatrix(intCounter, 6, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("Percent") + " "));
						vsDeductions.TextMatrix(intCounter, 7, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " "));
						vsDeductions.TextMatrix(intCounter, 8, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " "));
						vsDeductions.TextMatrix(intCounter, 9, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("LimitType") + " "));
						// matthew 9/23/03 per DOS conversion
						if (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AccountID") + " ").Length == 1)
						{
							if (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AccountID") + " ") != "M")
							{
								vsDeductions.TextMatrix(intCounter, 10, "M");
							}
							else
							{
								vsDeductions.TextMatrix(intCounter, 10, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AccountID") + " "));
							}
						}
						else
						{
							vsDeductions.TextMatrix(intCounter, 10, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AccountID") + " "));
						}
						vsDeductions.TextMatrix(intCounter, 11, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AccountDescription") + " "));
						vsDeductions.TextMatrix(intCounter, 12, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("Status") + " "));
						vsDeductions.TextMatrix(intCounter, 13, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " "));
						vsDeductions.TextMatrix(intCounter, 14, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("ID") + " "));
						vsDeductions.TextMatrix(intCounter, CNSTCOLUNEMPLOYMENTEXEMPT, FCConvert.ToString(rsDeductions.Get_Fields_Boolean("uexempt")));
						rsDeductions.MoveNext();
					}
                    //FC:FINAL:BSE:#4172 allow only numeric values in grid columns
                    vsDeductions.EditingControlShowing -= Grid_EditingControlShowing;
                    vsDeductions.EditingControlShowing += Grid_EditingControlShowing;
                    vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 11, vsDeductions.Rows - 1, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
				clsHistoryClass.Init = this;
				// If vsDeductions.Rows > 2 Then vsDeductions.Select 1, 2
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// SET THE GIRD PROPERTIES
                //FC:FINAL:AM:#2532 - don't set WordWrap
				//vsDeductions.WordWrap = true;
                vsDeductions.WordWrap = false;
                vsDeductions.Cols = 16;
                vsDeductions.FixedCols = 2;
                //FC:FINAL:AM:#2533 - increase FrozenCols because first column is hidden
                //vsDeductions.FrozenCols = 1;
                vsDeductions.FrozenCols = 2;
                vsDeductions.ColHidden(0, true);
				vsDeductions.ColHidden(14, true);
				//vsDeductions.RowHeight(0, vsDeductions.RowHeight(1) * 2);
				modGlobalRoutines.CenterGridCaptions(ref vsDeductions);
				// SET COLUMN 0 PROPERTIES
				vsDeductions.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsDeductions.ColWidth(0, 400);
				// SET COLUMN 1 PROPERTIES
				vsDeductions.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(1, 400);
				// SET COLUMN 2 PROPERTIES
				vsDeductions.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(2, 2300);
				vsDeductions.TextMatrix(0, 2, "Description");
				// SET COLUMN 3 PROPERTIES
				vsDeductions.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(3, 600);
				vsDeductions.TextMatrix(0, 3, "Tax Status");
				vsDeductions.ColComboList(3, string.Empty);
				rsDeductions.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// THIS FORCES COLUMN 3 TO BE OF A TYPE OF A COMBO BOX
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						// If intCounter = 3 Then
						// vsDeductions.ColComboList(3, vsDeductions.ColComboList(3) & "|#0; " & vbTab & ""
						// vsDeductions.ColComboList(3, vsDeductions.ColComboList(3) & "|#00; " & vbTab & "   EXEMPT FROM"
						// vsMatch.ColComboList(9, vsMatch.ColComboList(9) & "|#000; " & vbTab & ""
						// End If
						// 
						if (intCounter == 1)
						{
							vsDeductions.ColComboList(3, vsDeductions.ColComboList(3) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " "));
						}
						else if (intCounter > 2)
						{
							vsDeductions.ColComboList(3, vsDeductions.ColComboList(3) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " Exempt"));
						}
						else
						{
							vsDeductions.ColComboList(3, vsDeductions.ColComboList(3) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " "));
						}
						rsDeductions.MoveNext();
					}
				}
				// SET COLUMN 4 PROPERTIES
				vsDeductions.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsDeductions.ColWidth(4, 1200);
				vsDeductions.TextMatrix(0, 4, "Amount");
				vsDeductions.ColFormat(4, "#,##0.00;(#,###.00)");
				// SET COLUMN 5 PROPERTIES
				vsDeductions.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(5, 800);
				vsDeductions.TextMatrix(0, 5, "Amount Type");
				vsDeductions.ColComboList(5, "#1;Dollars|#2;Percent");
				// SET COLUMN 6 PROPERTIES
				vsDeductions.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(6, 600);
				vsDeductions.TextMatrix(0, 6, " % of");
				// vsDeductions.ColComboList(6, "#1;Gross"
				// SET COLUMN 7 PROPERTIES
				vsDeductions.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(7, 500);
				vsDeductions.TextMatrix(0, 7, "Freq");
				vsDeductions.ColComboList(7, string.Empty);
				rsDeductions.OpenRecordset("Select * from tblFrequencyCodes Where FrequencyCode <> 'D' order by ID", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblFrequencyCodes table
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							vsDeductions.ColComboList(7, vsDeductions.ColComboList(7) + "#" + rsDeductions.Get_Fields("ID") + ";" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("FrequencyCode"))) + "\t" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) + "   ");
						}
						else
						{
							vsDeductions.ColComboList(7, vsDeductions.ColComboList(7) + "|#" + rsDeductions.Get_Fields("ID") + ";" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("FrequencyCode"))) + "\t" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) + "   ");
						}
						rsDeductions.MoveNext();
					}
				}
				// SET COLUMN 8 PROPERTIES
				vsDeductions.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsDeductions.ColWidth(8, 1150);
				vsDeductions.TextMatrix(0, 8, "Limit");
				vsDeductions.ColFormat(8, "#,##0.00;(#,###.00)");
				// SET COLUMN 9 PROPERTIES
				vsDeductions.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(9, 1000);
				vsDeductions.TextMatrix(0, 9, "Limit Type");
				vsDeductions.ColComboList(9, "#1;Life|#2;Calendar|#3;Fiscal|#4;Pay-Period|#5;Month|#6;Quarter");
				// SET COLUMN 10 PROPERTIES (ACCOUNT NUMBER)
				vsDeductions.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(10, 1800);
				vsDeductions.TextMatrix(0, 10, "G/L Account");
				vsDeductions.ColComboList(10, string.Empty);
				// this fills the box with the values from the tblFrequencyCodes table
				// call dbDeductions.OpenRecordset("Select * from tblDeductionAccounts Order by ID")
				// If Not rsDeductions.eof Then
				// this will make the recordcount property work correctly
				// rsDeductions.MoveLast
				// rsDeductions.MoveFirst
				// 
				// set the number of rows in the grid
				// vsDeductions.Rows = rsDeductions.RecordCount + 1
				// 
				// fill the grid
				// For intCounter = 1 To rsDeductions.RecordCount
				// If intCounter = 1 Then
				// vsDeductions.ColComboList(10, vsDeductions.ColComboList(10) & "#" & rsDeductions.Fields("ID") & ";" & rsDeductions.Fields("AccountNumber")
				// Else
				// vsDeductions.ColComboList(10, vsDeductions.ColComboList(10) & "|#" & rsDeductions.Fields("ID") & ";" & rsDeductions.Fields("AccountNumber")
				// End If
				// 
				// rsDeductions.MoveNext
				// Next
				// End If
				// SET COLUMN 11 PROPERTIES (DESCRIPTION)
				vsDeductions.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(11, 2500);
				vsDeductions.TextMatrix(0, 11, "Acct Description");
				// SET COLUMN 12 PROPERTIES
				vsDeductions.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(12, 900);
				vsDeductions.TextMatrix(0, 12, "Status");
				vsDeductions.ColComboList(12, "#1;Active|#2;InActive|#3;Hold 1 Week");
				// SET COLUMN 13 PROPERTIES
				vsDeductions.ColAlignment(13, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(13, 1200);
				vsDeductions.TextMatrix(0, 13, "Priority");
				vsDeductions.ColFormat(13, "##");
				vsDeductions.ColDataType(15, FCGrid.DataTypeSettings.flexDTBoolean);
				vsDeductions.ColAlignment(15, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.TextMatrix(0, 15, "U Exempt");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDeductionSetup_Resize(object sender, System.EventArgs e)
		{
			//vsDeductions.ColWidth(0, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.04));
			vsDeductions.ColWidth(1, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.04));
			vsDeductions.ColWidth(2, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.25));
			vsDeductions.ColWidth(3, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.08));
			vsDeductions.ColWidth(4, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.12));
			vsDeductions.ColWidth(5, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(6, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.07));
			vsDeductions.ColWidth(7, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.06));
			vsDeductions.ColWidth(8, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.14));
			vsDeductions.ColWidth(9, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.1));
			vsDeductions.ColWidth(10, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.2));
			vsDeductions.ColWidth(11, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.2));
			vsDeductions.ColWidth(12, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.1));
			vsDeductions.ColWidth(13, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.08));
			vsDeductions.ColWidth(15, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.1));
		}
		// UNLOAD THE FORM DEDUCTION SETUP
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (frmEmployersMatch.InstancePtr.boolFromMatch == true)
				{
					frmEmployersMatch.InstancePtr.Show();
				}
				else
				{
					SaveChanges();
					if (intDataChanged == (DialogResult)2)
					{
						e.Cancel = true;
						return;
					}
					//MDIParent.InstancePtr.Show();
					// set focus back to the menu options of the MDIParent
					//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
				}
				frmEmployersMatch.InstancePtr.boolFromMatch = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuFrequencyCodes_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gboolShowDeductionSetup = true;
			frmFrequencyCodes.InstancePtr.Show();
		}

		private void mnuGlobalDeductionUpdate_Click(object sender, System.EventArgs e)
		{
			frmUpdateDeductions.InstancePtr.Show(App.MainForm);
		}

		private void mnuGlobalMatchUpdate_Click(object sender, System.EventArgs e)
		{
			frmUpdateMatches.InstancePtr.Show(App.MainForm);
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:DSE:#i2314 Display report
			//rptDeductionSetup.InstancePtr.Run()/*.Init(MDIParent.InstancePtr)*/;
			frmReportViewer.InstancePtr.Init(rptDeductionSetup.InstancePtr);
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intID;
				int x;
				vsDeductions.Select(0, 0);
				if (NotValidData() == true)
					return;
				for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
				{
					if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
					{
						if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
						{
							// CALL ID 65745   MATTHEW 3/23/2005
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 10))) == string.Empty)
							{
								MessageBox.Show("Deduction account number must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								vsDeductions.Select(intCounter, 10);
								boolSaveOK = false;
								return;
							}
						}
					}
				}
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Deduction Setup", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				//App.DoEvents();
				if (boolSaveOK)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
						{
							// record has been edited so we need to update it
							// If NotValidData Then Exit Sub
							if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) == string.Empty)
							{
							}
							else
							{
								rsDeductions.OpenRecordset("Select * from tblDeductionSetup Where ID = " + FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 14))), "TWPY0000.vb1");
								if (rsDeductions.EndOfFile())
								{
									rsDeductions.AddNew();
								}
								else
								{
									rsDeductions.Edit();
								}
								rsDeductions.SetData("DeductionNumber", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)));
								rsDeductions.SetData("Description", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2));
								rsDeductions.SetData("TaxStatusCode", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3));
								rsDeductions.SetData("Amount", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 4)));
								rsDeductions.SetData("AmountType", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5));
								rsDeductions.SetData("Percent", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6));
								rsDeductions.SetData("FrequencyCode", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 7));
								if (Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 7)) != 9 && Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 7)) != 10)
								{
									rsDeductions.SetData("OldFrequency", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 7));
								}
								rsDeductions.SetData("Limit", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 8));
								rsDeductions.SetData("LimitType", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 9));
								// CALL ID 65745   MATTHEW 3/23/2005
								// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 10)) = vbNullString Then
								// MsgBox "Deduction account number must be specified.", vbCritical + vbOKOnly, "TRIO Software"
								// vsDeductions.Select intCounter, 10
								// boolSaveOK = False
								// Exit Sub
								// Else
								rsDeductions.SetData("AccountID", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 10));
								// End If
								rsDeductions.SetData("AccountDescription", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 11));
								rsDeductions.SetData("Status", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 12));
								rsDeductions.SetData("Priority", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 13)));
								rsDeductions.Set_Fields("uexempt", FCConvert.CBool(vsDeductions.TextMatrix(intCounter, CNSTCOLUNEMPLOYMENTEXEMPT)));
								rsDeductions.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
								rsDeductions.Update();
								if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
								{
									// clear the edit symbol from the row number
									vsDeductions.TextMatrix(intCounter, 0, Strings.Mid(vsDeductions.TextMatrix(intCounter, 0), 1, vsDeductions.TextMatrix(intCounter, 0).Length - 2));
								}
								else
								{
									// clear the edit symbol from the row number
									vsDeductions.TextMatrix(intCounter, 0, Strings.Mid(vsDeductions.TextMatrix(intCounter, 0), 1, vsDeductions.TextMatrix(intCounter, 0).Length - 1));
									// get the new ID from the table for the record that was just added and place it into column 11
									rsDeductions.OpenRecordset("Select Max(ID) as NewID from tblDeductionSetup", "TWPY0000.vb1");
									vsDeductions.TextMatrix(intCounter, 14, FCConvert.ToString(rsDeductions.Get_Fields("NewID")));
								}
							}
						}
					}
                    //FC:FINAL:SBE - #i2469 - move MessageBox after LoadGrid to disable FormClosing during long running process
					//MessageBox.Show("Record(s) Saved successfully.", "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
					clsHistoryClass.Compare();
					// LOAD THE GRID WITH THE DATA
					LoadGrid();
					// CHECK TO SEE IF THE CALCULATION HAS ALREADY BEEN RUN
					if (modGlobalRoutines.CheckIfPayProcessCompleted("Calculation", "Calculation", true))
					{
						// CHECK TO SEE IF THE VERIFY AND ACCEPT HAS BEEN RUN
						if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "VerifyAccept", true))
						{
							// ACCEPT HAS BEEN DONE SO DO NOT CLEAR OUT THE CALCULATION FLAG
						}
						else
						{
							// CLEAR OUT THE CALCULATION FIELD
							modGlobalRoutines.UpdatePayrollStepTable("EditTotals");
						}
					}
					// INITIALIZE THE COUNTER AS TO  HAVE MANY ROWS ARE DIRTY
					intDataChanged = 0;
                    //FC:FINAL:SBE - #i2469 - move MessageBox after LoadGrid to disable FormClosing during long running process
                    MessageBox.Show("Record(s) Saved successfully.", "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
				}
				ExitRoutine:
				;
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + vsDeductions.TextMatrix(vsDeductions.Row, 1), "Payroll Deduction Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			if (boolSaveOK)
				cmdExit_Click();
		}

		private void mnuTaxStatusCodes_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gboolShowDeductionSetup = true;
			frmTaxStatusCodes.InstancePtr.Show();
		}

		private void mnuViewValidAccounts_Click()
		{
			// vbPorter upgrade warning: intGridRow As int	OnWriteFCConvert.ToInt32(
			int intGridRow;
			intGridRow = vsDeductions.Row;
			// frmValidAccounts.Show 1, MDIParent
			modGlobalVariables.Statics.gstrSelectedAccount = frmLoadValidAccounts.InstancePtr.Init(vsDeductions.TextMatrix(intGridRow, 10));
			if (modGlobalVariables.Statics.gstrSelectedAccount != string.Empty)
			{
				if (intGridRow > 1)
				{
					vsDeductions.TextMatrix(intGridRow, 10, modGlobalVariables.Statics.gstrSelectedAccount);
					vsDeductions.Select(intGridRow, 10);
				}
				else
				{
					MessageBox.Show("Cursor must be in row that you wish account number to be intered in.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void vsDeductions_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// dblAmtPercentTotal = 0
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsDeductions.TextMatrix(vsDeductions.Row, 0, vsDeductions.TextMatrix(vsDeductions.Row, 0) + "..");
				}
				if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, 5)) == "Dollars")
				{
					vsDeductions.TextMatrix(vsDeductions.Row, 6, "");
					// vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, 6) = TRIOCOLORGRAYBACKGROUND
					// If vsDeductions.Col = 6 Then vsDeductions.Col = 7
				}
				else
				{
					vsDeductions.TextMatrix(vsDeductions.Row, 6, "Gross");
					// vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, 6) = vbWhite
				}
				// If Col = 10 Then
				// If Trim(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, Row, 10)) = vbNullString Then Exit Sub
				// call dbDeductions.OpenRecordset("Select * from tblDeductionAccounts where ID =" & vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, Row, 10))
				// If Not rsDeductions.eof Then
				// vsDeductions.TextMatrix(Row, 11) = rsDeductions.Fields("Description")
				// End If
				// End If
				// For intCounter = 1 To vsDeductions.Rows - 1
				// If vsDeductions.TextMatrix(intCounter, 5) = "Percent" Then
				// dblAmtPercentTotal = dblAmtPercentTotal + vsDeductions.TextMatrix(intCounter, 4)
				// If dblAmtPercentTotal > 100 Then
				// MsgBox "Amount Totals For Percent Cannot Be Greater Than 100", vbOKOnly, "Error"
				// vsDeductions.Select intCounter, 5
				// Exit Sub
				// End If
				// End If
				// Next
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDeductions_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsDeductions.Col == 10)
			{
				vsDeductions.TextMatrix(vsDeductions.Row, 11, modAccountTitle.ReturnAccountDescription(vsDeductions.EditText));
			}
		}

		private void vsDeductions_ComboDropDown(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void vsDeductions_EnterCell(object sender, System.EventArgs e)
		{
			// called from the KeyDown event
			if (boolSkip)
			{
				boolSkip = false;
				return;
			}
			if (vsDeductions.Col == 6)
			{
				if (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsDeductions.Col = 7;
				}
			}
			if (vsDeductions.Col != CNSTVSDEDUCTIONSCOL && vsDeductions.Col != CNSTCOLUNEMPLOYMENTEXEMPT)
			{
				vsDeductions.EditCell();
				vsDeductions.EditSelStart = 0;
				vsDeductions.EditSelLength = FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText)).Length;
			}
			if (vsDeductions.Col == 11)
				vsDeductions.Col = 12;
		}

		private void vsDeductions_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			if (Shift == 1)
				return;
			// if the key is the TAB then make it work like the right arrow
			if (KeyCode == (Keys)9)
				KeyCode = (Keys)39;
			if (KeyCode == (Keys)13)
				KeyCode = (Keys)39;
			// used in the EnterCell event
			boolSkip = true;
			// want to skip any Month/Day getdata that are disabled
			if (vsDeductions.Col == 6)
			{
				intRowNumber = vsDeductions.Row;
				// up key
				if (KeyCode == (Keys)38)
				{
					NextCol:
					;
					if (vsDeductions.Row > 1)
					{
						vsDeductions.Row -= 1;
						if (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							goto NextCol;
						}
					}
					else
					{
						vsDeductions.Select(intRowNumber, 6);
					}
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)40)
				{
					// down key
					NextCol2:
					;
					if (vsDeductions.Row < vsDeductions.Rows - 1)
					{
						// this will skip all code in the rowcolchange event
						vsDeductions.Row += 1;
						if (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							goto NextCol2;
						}
					}
					else
					{
						vsDeductions.Select(intRowNumber, 6);
					}
					KeyCode = 0;
				}
			}
			if (vsDeductions.Col == 5)
			{
				if (KeyCode == (Keys)39)
				{
					// right key
					if (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, 6)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsDeductions.Col = 7;
						KeyCode = 0;
					}
				}
			}
			if (vsDeductions.Col == 7)
			{
				if (KeyCode == (Keys)37)
				{
					// left key
					if (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, 6)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsDeductions.Col = 5;
						KeyCode = 0;
					}
				}
			}
			if (vsDeductions.Col == 2)
			{
				if (KeyCode == (Keys)37)
				{
					// left key
					if (vsDeductions.Row > 1)
					{
						vsDeductions.Col = 13;
						vsDeductions.LeftCol = 13;
						vsDeductions.Row -= 1;
					}
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)39)
				{
					vsDeductions.Col = 3;
					KeyCode = 0;
				}
			}
			if (vsDeductions.Col == 12)
			{
				if (KeyCode == (Keys)37)
				{
					// left key
					// vsDeductions.Editable = False
					vsDeductions.Col = 10;
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)39)
				{
					vsDeductions.Col = 13;
					KeyCode = 0;
				}
			}
			if (vsDeductions.Col == 10)
			{
				if (KeyCode == (Keys)39)
				{
					// right key
					vsDeductions.Col = 12;
					KeyCode = 0;
				}
			}
			if (vsDeductions.Col == 13)
			{
				if (KeyCode == (Keys)39)
				{
					// right key
					if (vsDeductions.Row < vsDeductions.Rows - 1)
					{
						vsDeductions.Col = 2;
						vsDeductions.LeftCol = 2;
						vsDeductions.Row += 1;
						KeyCode = 0;
					}
					else
					{
						cmdNew_Click();
						KeyCode = 0;
					}
				}
			}
		}
		// Private Sub vsDeductions_KeyDownEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
		// On Error GoTo CallErrorRoutine
		// gstrCurrentRoutine = "vsDeductions_KeyDownEdit"
		// Dim Mouse As clsMousePointer
		// Set Mouse = New clsMousePointer
		// GoTo ResumeCode
		// CallErrorRoutine:
		// Call SetErrorHandler
		// Exit Sub
		// ResumeCode:
		//
		//
		// if this is the return key then make it function as a tab
		// Call vsDeductions_KeyDown(KeyCode, Shift)
		//
		// increment the counter as to how many records are dirty
		// intDataChanged = intDataChanged + 1
		// End Sub
		private void vsDeductions_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_KeyPressEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// If Col = 10 Then Call CheckAccountKeyPress(vsDeductions, Row, Col, KeyAscii)
				int KeyAscii = Strings.Asc(e.KeyChar);
				if (vsDeductions.Col == 4 || vsDeductions.Col == 8 || vsDeductions.Col == 13)
				{
					if (KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9))
					{
						// allow the entry of a backspace
						if (KeyAscii == FCConvert.ToInt32(Keys.Back))
						{
						}
						else
						{
							if (KeyAscii == 46)
								return;
							KeyAscii = 0;
							e.KeyChar = Strings.Chr(KeyAscii);
							return;
						}
					}
				}
				// allow the entry of a decimal point
				if (KeyAscii == 46)
					return;
				intDataChanged += 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
        // Private Sub vsDeductions_KeyUp(KeyCode As Integer, Shift As Integer)
        // On Error GoTo CallErrorRoutine
        // gstrCurrentRoutine = "vsDeductions_KeyUp"
        // Dim Mouse As clsMousePointer
        // Set Mouse = New clsMousePointer
        // GoTo ResumeCode
        // CallErrorRoutine:
        // Call SetErrorHandler
        // Exit Sub
        // ResumeCode:
        //
        // show the help screen according to which column the user is currently in
        // Call ShowHelpScreen(KeyCode)
        // End Sub
        // Private Sub vsDeductions_KeyUpEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
        // If Col = 10 Then
        // Call CheckAccountKeyCode(vsDeductions, Row, Col, KeyCode, 0)
        // KeyCode = 0
        // End If
        // End Sub

        //FC:FINAL:AM:#2539 - set the tooltip for each cell
        //private void vsDeductions_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	if (vsDeductions.MouseCol == 10)
        //	{
        //		if (modGlobalVariables.Statics.gboolBudgetary)
        //		{
        //			if (vsDeductions.MouseRow > 0)
        //			{
        //				ToolTip1.SetToolTip(vsDeductions, modDavesSweetCode.GetAccountDescription(vsDeductions.TextMatrix(vsDeductions.MouseRow, 10)));
        //			}
        //			else
        //			{
        //				ToolTip1.SetToolTip(vsDeductions, string.Empty);
        //			}
        //		}
        //		else
        //		{
        //			ToolTip1.SetToolTip(vsDeductions, string.Empty);
        //		}
        //	}
        //	else if (vsDeductions.MouseCol == 13)
        //	{
        //		if (vsDeductions.MouseRow > 0)
        //		{
        //			ToolTip1.SetToolTip(vsDeductions, "A number of 1 is HIGH and 9 is LOW");
        //		}
        //		else
        //		{
        //			ToolTip1.SetToolTip(vsDeductions, string.Empty);
        //		}
        //	}
        //	else if (vsDeductions.MouseCol == CNSTCOLUNEMPLOYMENTEXEMPT)
        //	{
        //		ToolTip1.SetToolTip(vsDeductions, "Unemployment Exempt");
        //	}
        //	else
        //	{
        //		ToolTip1.SetToolTip(vsDeductions, string.Empty);
        //	}
        //}

        private void VsDeductions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (vsDeductions.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = vsDeductions[e.ColumnIndex, e.RowIndex];
                int col = vsDeductions.GetFlexColIndex(e.ColumnIndex);
                if (col == 10)
                {
                    if (modGlobalVariables.Statics.gboolBudgetary)
                    {
                        cell.ToolTipText = modDavesSweetCode.GetAccountDescription(vsDeductions.TextMatrix(vsDeductions.GetFlexRowIndex(e.RowIndex), 10));
                    }
                }
                else if (col == 13)
                {
                    cell.ToolTipText = "A number of 1 is HIGH and 9 is LOW";
                }
                else if (col == CNSTCOLUNEMPLOYMENTEXEMPT)
                {
                    cell.ToolTipText = "Unemployment Exempt";
                }
            }
        }

        private void vsDeductions_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// If vsDeductions.Col = 10 Then Call GetMaxLength(vsDeductions.TextMatrix(vsDeductions.Row, vsDeductions.Col))
				vsDeductions.Editable = (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND) ? FCGrid.EditableSettings.flexEDKbdMouse: FCGrid.EditableSettings.flexEDNone;
				if (vsDeductions.Col == 12)
				{
					vsDeductions.EditCell();
				}
				if (modGlobalConstants.Statics.gboolBD)
				{
					vsDeductions.TextMatrix(vsDeductions.Row, 11, modDavesSweetCode.GetAccountDescription(vsDeductions.TextMatrix(vsDeductions.Row, 10)));
				}
				else
				{
					// vsDeductions.TextMatrix(vsDeductions.Row, 11) = "Manual Account"
				}
				// set the account box
				if (vsDeductions.Col == 10)
					vsDeductions.LeftCol = 10;
				if (vsDeductions.Col == 7 && fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, 6)) == string.Empty && Conversion.Val(fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, 5))) == 2)
				{
					vsDeductions.Col = 6;
				}
				if (vsDeductions.Col == 6 && vsDeductions.CellBackColor == System.Drawing.ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND))
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowHelpScreen(Keys KeyCode)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowHelpScreen";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// this variable is used in the form frmHelp to determine what data to show
				modGlobalVariables.Statics.gstrHelpFieldName = string.Empty;
				if (KeyCode == Keys.F9)
				{
					switch (vsDeductions.Col)
					{
						case 3:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "FREQUENCYCODE";
								break;
							}
						case 4:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "TAXSTATUSCODE";
								break;
							}
						case 6:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "AMOUNTTYPE";
								break;
							}
						case 8:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "LIMITCODE";
								break;
							}
						default:
							{
								break;
							}
					}
					//end switch
					frmHelp.InstancePtr.Show();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDeductions_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// If Col = 10 Then
			// Cancel = CheckAccountValidate(vsDeductions, Row, Col, Cancel)
			// boolSaveOK = Not Cancel
			// Exit Sub
			// End If
			dblAmtPercentTotal = 0;
			if (vsDeductions.Col == 3)
			{
				if (fecherFoundation.Strings.Trim(vsDeductions.EditText) == string.Empty)
				{
					MessageBox.Show("Invalid Tax Status.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
				}
			}
			if (vsDeductions.Col == 4)
			{
				if (vsDeductions.TextMatrix(vsDeductions.Row, 5) == "Percent" || vsDeductions.TextMatrix(vsDeductions.Row, 5) == "2")
				{
					dblAmtPercentTotal = Conversion.Val(vsDeductions.EditText);
				}
				// For intCounter = 1 To vsDeductions.Rows - 1
				// If vsDeductions.TextMatrix(intCounter, 5) = "Percent" Or vsDeductions.TextMatrix(intCounter, 5) = "2" Then
				// If intCounter = vsDeductions.Row Then
				// dblAmtPercentTotal = dblAmtPercentTotal + Val(vsDeductions.EditText)
				// Else
				// dblAmtPercentTotal = dblAmtPercentTotal + vsDeductions.TextMatrix(intCounter, 4)
				// End If
				// End If
				// Next
			}
			if (vsDeductions.Col == 5)
			{
				if (vsDeductions.EditText == "Percent" || vsDeductions.EditText == "2")
				{
					dblAmtPercentTotal = FCConvert.ToDouble(vsDeductions.TextMatrix(vsDeductions.Row, 4));
				}
				// For intCounter = 1 To vsDeductions.Rows - 1
				// If intCounter = vsDeductions.Row Then
				// If vsDeductions.EditText = "Percent" Or vsDeductions.EditText = "2" Then
				// dblAmtPercentTotal = dblAmtPercentTotal + vsDeductions.TextMatrix(intCounter, 4)
				// End If
				// Else
				// If vsDeductions.TextMatrix(intCounter, 5) = "Percent" Or vsDeductions.TextMatrix(intCounter, 5) = "2" Then
				// dblAmtPercentTotal = dblAmtPercentTotal + vsDeductions.TextMatrix(intCounter, 4)
				// End If
				// End If
				// 
				// Next
			}
			if (dblAmtPercentTotal > 100)
			{
				MessageBox.Show("Amount Totals For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsDeductions.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (vsDeductions.Cols - 1); intCols++)
				{
					if (intCols != 6 && intCols != 11)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsDeductions";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsDeductions.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSave_Click(cmdSave, EventArgs.Empty);
        }
        //FC:FINAL:BSE:#4172 allow only numeric input in grid columns and decimal values
        private void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                if (vsDeductions.Col == 4 || vsDeductions.Col == 8 || vsDeductions.Col == 13 )
                {
                    var box = e.Control as TextBox;
                    if (box != null)
                    {
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
                    }
                }
            }
        }
    }
}
