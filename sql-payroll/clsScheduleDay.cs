//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsScheduleDay
	{
		//=========================================================
		private clsWorkShift[] ShiftList = null;
		private clsWorkShift CurrentShift = new clsWorkShift();
		private int intCurrentIndex;
		// vbPorter upgrade warning: dtCurrentDate As DateTime	OnWrite(DateTime, int)
		private DateTime dtCurrentDate;
		private int intWeekDay;
		// vbPorter upgrade warning: dtWorkDate As DateTime	OnWrite
		private DateTime dtWorkDate;
		private int lngParentWeekID;
		private int lngParentScheduleId;
		private string strEmployeeNumber = string.Empty;

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public void ClearDay()
		{
			FCUtils.EraseSafe(ShiftList);
			intCurrentIndex = -1;
			intWeekDay = 0;
		}

		public void DeleteDefaultDay()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				strSQL = "Delete from defaultshifts where weekid = " + FCConvert.ToString(lngParentWeekID);
				rsSave.Execute(strSQL, "Twpy0000.vb1");
				ClearDay();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteDefaultDay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void LoadDay(ref int intDay, ref string strEmployee, DateTime? tempdtDate = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtDate == null)
			{
				tempdtDate = DateTime.FromOADate(0);
			}
			DateTime dtDate = tempdtDate.Value;

			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strEmployeeNumber = strEmployee;
				strSQL = "select * from EmployeeShifts where DayNo = " + FCConvert.ToString(intDay) + " and employeenumber = '" + strEmployee + "'";
				if (dtDate.ToOADate() != 0)
				{
					strSQL += " and workdate = '" + FCConvert.ToString(dtDate) + "'";
					dtCurrentDate = dtDate;
				}
				else
				{
					dtCurrentDate = DateTime.FromOADate(0);
				}
				intWeekDay = intDay;
				FCUtils.EraseSafe(ShiftList);
				clsWorkShift tShift;
				while (!rsLoad.EndOfFile())
				{
					tShift = new clsWorkShift();
					tShift.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					tShift.ActualEnd = FCConvert.ToString(rsLoad.Get_Fields_String("ActualEnd"));
					tShift.ActualStart = FCConvert.ToString(rsLoad.Get_Fields_String("ActualStart"));
					tShift.ScheduledEnd = FCConvert.ToString(rsLoad.Get_Fields_String("ScheduledEnd"));
					tShift.ScheduledStart = FCConvert.ToString(rsLoad.Get_Fields_String("ScheduledStart"));
					tShift.ShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ShiftTypeID"))));
					tShift.LunchTime = Conversion.Val(rsLoad.Get_Fields_Double("LunchTime"));
					InsertShift(tShift);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadDay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public int WeekID
		{
			set
			{
				lngParentWeekID = value;
			}
			get
			{
				int WeekID = 0;
				WeekID = lngParentWeekID;
				return WeekID;
			}
		}

		public int ScheduleID
		{
			set
			{
				lngParentScheduleId = value;
			}
			get
			{
				int ScheduleID = 0;
				ScheduleID = lngParentScheduleId;
				return ScheduleID;
			}
		}

		public void LoadEmployeeDay(DateTime dtDate, ref string strEmployee)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strEmployeeNumber = strEmployee;
				strSQL = "Select * from EmployeeShifts where employeenumber = '" + strEmployeeNumber + "' and shiftdate = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "'";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				clsWorkShift tShift;
				dtCurrentDate = dtDate;
				while (!rsLoad.EndOfFile())
				{
					tShift = new clsWorkShift();
					tShift.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					tShift.ScheduledStart = FCConvert.ToString(rsLoad.Get_Fields("scheduledstart"));
					tShift.ScheduledEnd = FCConvert.ToString(rsLoad.Get_Fields("scheduledend"));
					tShift.ActualEnd = FCConvert.ToString(rsLoad.Get_Fields("actualend"));
					tShift.ActualStart = FCConvert.ToString(rsLoad.Get_Fields("actualstart"));
					tShift.ShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("shifttypeid"))));
					tShift.LunchTime = Conversion.Val(rsLoad.Get_Fields("lunchtime"));
					tShift.Account = FCConvert.ToString(rsLoad.Get_Fields("account"));
					InsertShift(tShift);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadEmployeeDay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void LoadDefaultDay(int lngScheduleID, int lngWeekID, int intDay)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "Select * from DefaultShifts where ScheduleID = " + FCConvert.ToString(lngScheduleID) + " and WeekID = " + FCConvert.ToString(lngWeekID) + " and DayNo = " + FCConvert.ToString(intDay);
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				clsWorkShift tShift;
				dtWorkDate = DateTime.FromOADate(0);
				lngParentWeekID = lngWeekID;
				lngParentScheduleId = lngScheduleID;
				while (!rsLoad.EndOfFile())
				{
					tShift = new clsWorkShift();
					tShift.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					tShift.ScheduledStart = FCConvert.ToString(rsLoad.Get_Fields_String("ScheduledStart"));
					tShift.ScheduledEnd = FCConvert.ToString(rsLoad.Get_Fields_String("ScheduledEnd"));
					tShift.ActualEnd = "Unworked";
					tShift.ActualStart = "Unworked";
					tShift.ShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ShiftTypeID"))));
					tShift.LunchTime = Conversion.Val(rsLoad.Get_Fields("lunchtime"));
					tShift.Account = FCConvert.ToString(rsLoad.Get_Fields("account"));
					InsertShift(tShift);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadDefaultDay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveDefaultDay()
		{
			bool SaveDefaultDay = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				SaveDefaultDay = false;
				if (!FCUtils.IsEmpty(ShiftList))
				{
					// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
					int X;
					for (X = 0; X <= (Information.UBound(ShiftList, 1)); X++)
					{
						if (!(ShiftList[X] == null))
						{
							if (fecherFoundation.Strings.UCase(ShiftList[X].ScheduledStart) == "UNSCHEDULED")
							{
								if (ShiftList[X].ID > 0)
								{
									strSQL = "delete from defaultshifts where ID = " + FCConvert.ToString(ShiftList[X].ID);
									rsSave.Execute(strSQL, "twpy0000.vb1");
									ShiftList[X] = null;
								}
							}
							else
							{
								strSQL = "Select * from defaultshifts where ID = " + FCConvert.ToString(ShiftList[X].ID);
								rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsSave.EndOfFile())
								{
									rsSave.Edit();
								}
								else
								{
									rsSave.AddNew();
								}
								rsSave.Set_Fields("scheduleid", lngParentScheduleId);
								rsSave.Set_Fields("WeekID", lngParentWeekID);
								rsSave.Set_Fields("DayNo", intWeekDay);
								rsSave.Set_Fields("ShiftTypeID", ShiftList[X].ShiftType);
								rsSave.Set_Fields("ScheduledStart", ShiftList[X].ScheduledStart);
								rsSave.Set_Fields("ScheduledEnd", ShiftList[X].ScheduledEnd);
								rsSave.Set_Fields("lunchtime", ShiftList[X].LunchTime);
								rsSave.Set_Fields("account", ShiftList[X].Account);
								rsSave.Update();
								ShiftList[X].ID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							}
						}
					}
					// X
					MoveFirst();
				}
				SaveDefaultDay = true;
				return SaveDefaultDay;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveDefaultDay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveDefaultDay;
		}

		public bool SaveEmployeeDay()
		{
			bool SaveEmployeeDay = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				SaveEmployeeDay = false;
				if (!FCUtils.IsEmpty(ShiftList))
				{
					for (X = 0; X <= (Information.UBound(ShiftList, 1)); X++)
					{
						if (!(ShiftList[X] == null))
						{
							if ((fecherFoundation.Strings.UCase(ShiftList[X].ScheduledStart) == "UNSCHEDULED" && fecherFoundation.Strings.UCase(ShiftList[X].ActualStart) == "UNWORKED") || ShiftList[X].Unused)
							{
								if (ShiftList[X].ID > 0)
								{
									strSQL = "delete from employeeshifts where ID = " + FCConvert.ToString(ShiftList[X].ID);
									rsSave.Execute(strSQL, "twpy0000.vb1");
									ShiftList[X] = null;
								}
							}
							else
							{
								strSQL = "select * from employeeshifts where ID = " + FCConvert.ToString(ShiftList[X].ID);
								rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsSave.EndOfFile())
								{
									rsSave.Edit();
								}
								else
								{
									rsSave.AddNew();
								}
								rsSave.Set_Fields("SHIFTtypeid", ShiftList[X].ShiftType);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("actualstart", ShiftList[X].ActualStart);
								rsSave.Set_Fields("actualend", ShiftList[X].ActualEnd);
								rsSave.Set_Fields("scheduledstart", ShiftList[X].ScheduledStart);
								rsSave.Set_Fields("scheduledend", ShiftList[X].ScheduledEnd);
								rsSave.Set_Fields("dayno", intWeekDay);
								rsSave.Set_Fields("lunchtime", ShiftList[X].LunchTime);
								rsSave.Set_Fields("shiftdate", dtCurrentDate);
								rsSave.Set_Fields("account", ShiftList[X].Account);
								rsSave.Update();
								ShiftList[X].ID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							}
						}
					}
					// X
					MoveFirst();
				}
				SaveEmployeeDay = true;
				return SaveEmployeeDay;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveEmployeeDay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveEmployeeDay;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = intCurrentIndex;
			return GetCurrentIndex;
		}

		public void DeleteShift(int intShiftIndex)
		{
			if (!FCUtils.IsEmpty(ShiftList))
			{
				if (intShiftIndex >= 0)
				{
					if (Information.UBound(ShiftList, 1) >= intShiftIndex)
					{
						ShiftList[intShiftIndex].ScheduledEnd = "Unscheduled";
						ShiftList[intShiftIndex].ScheduledStart = "Unscheduled";
						ShiftList[intShiftIndex].ActualEnd = "Unworked";
						ShiftList[intShiftIndex].ActualStart = "Unworked";
						ShiftList[intShiftIndex].Unused = true;
						if (intShiftIndex == intCurrentIndex)
						{
							MoveFirst();
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: lngShiftTypeID As int	OnWrite(int, double)
		public int AddShift(int lngShiftTypeID, string strStartTime, string strEndTime, string strActualStart, string strActualEnd, int lngID, double dblLunch, string strAccount)
		{
			int AddShift = 0;
			int intindex;
			clsWorkShift tShift;
			tShift = new clsWorkShift();
			tShift.ID = lngID;
			tShift.Unused = false;
			tShift.ScheduledEnd = strEndTime;
			tShift.ScheduledStart = strStartTime;
			tShift.ActualEnd = strEndTime;
			tShift.ActualStart = strActualStart;
			tShift.ShiftType = lngShiftTypeID;
			tShift.LunchTime = dblLunch;
			tShift.Account = strAccount;
			this.InsertShift(tShift);
			CurrentShift.ID = lngID;
			CurrentShift.ScheduledEnd = strEndTime;
			CurrentShift.ScheduledStart = strStartTime;
			CurrentShift.ShiftType = lngShiftTypeID;
			AddShift = intCurrentIndex;
			return AddShift;
		}

		public int InsertShift(clsWorkShift tShift)
		{
			int InsertShift = 0;
			// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
			int intindex = 0;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				intindex = Information.UBound(ShiftList, 1);
				if (intindex < 0)
				{
					intindex = 0;
				}
				Array.Resize(ref ShiftList, intindex + 1 + 1);
				intindex = Information.UBound(ShiftList, 1);
			}
			else
			{
				ShiftList = new clsWorkShift[1 + 1];
				intindex = 0;
			}
			ShiftList[intindex] = tShift;
			ShiftList[intindex].Unused = false;
			intCurrentIndex = intindex;
			InsertShift = intCurrentIndex;
			return InsertShift;
		}
		// vbPorter upgrade warning: intindex As int	OnWrite(string, int)
		public clsWorkShift GetShiftByIndex(int intindex)
		{
			clsWorkShift GetShiftByIndex = null;
			GetShiftByIndex = null;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				if (intindex >= 0)
				{
					if (Information.UBound(ShiftList, 1) >= intindex)
					{
						if (!(ShiftList[intindex] == null))
						{
							if (!ShiftList[intindex].Unused)
							{
								GetShiftByIndex = ShiftList[intindex];
							}
						}
					}
				}
			}
			return GetShiftByIndex;
		}

		public clsWorkShift GetCurrentShift()
		{
			clsWorkShift GetCurrentShift = null;
			GetCurrentShift = null;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				if (intCurrentIndex >= 0)
				{
					if (Information.UBound(ShiftList, 1) >= intCurrentIndex)
					{
						if (!(ShiftList[intCurrentIndex] == null))
						{
							if (!ShiftList[intCurrentIndex].Unused)
							{
								GetCurrentShift = ShiftList[intCurrentIndex];
							}
						}
					}
				}
			}
			return GetCurrentShift;
		}

		public clsWorkShift FindShift(int lngShiftType)
		{
			clsWorkShift FindShift = null;
			FindShift = null;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(ShiftList, 1)); X++)
				{
					if (!(ShiftList[X] == null))
					{
						if (!ShiftList[X].Unused)
						{
							if (ShiftList[X].ShiftType == lngShiftType)
							{
								FindShift = ShiftList[X];
								return FindShift;
							}
						}
					}
				}
				// X
			}
			return FindShift;
		}

		public clsWorkShift FindShiftByCat(int lngCat)
		{
			clsWorkShift FindShiftByCat = null;
			FindShiftByCat = null;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(ShiftList, 1)); X++)
				{
					if (!(ShiftList[X] == null))
					{
						if (!ShiftList[X].Unused)
						{
						}
					}
				}
				// X
			}
			return FindShiftByCat;
		}

		public clsWorkShift FindFirstShift()
		{
			clsWorkShift FindFirstShift = null;
			FindFirstShift = null;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(ShiftList, 1)); X++)
				{
					if (!(ShiftList[X] == null))
					{
						if (!ShiftList[X].Unused)
						{
							FindFirstShift = ShiftList[X];
							return FindFirstShift;
						}
					}
				}
				// X
			}
			return FindFirstShift;
		}

		public DateTime WorkDate
		{
			set
			{
				dtCurrentDate = value;
			}
			get
			{
				DateTime WorkDate = System.DateTime.Now;
				WorkDate = dtCurrentDate;
				return WorkDate;
			}
		}

		public int DayOfWeek
		{
			set
			{
				intWeekDay = value;
			}
			get
			{
				int DayOfWeek = 0;
				DayOfWeek = intWeekDay;
				return DayOfWeek;
			}
		}

		public string WeekDayName
		{
			get
			{
				string WeekDayName = "";
				string strReturn = "";
				switch ((System.DayOfWeek)intWeekDay)
				{
					case System.DayOfWeek.Sunday:
						{
							strReturn = "Sunday";
							break;
						}
					case System.DayOfWeek.Monday:
						{
							strReturn = "Monday";
							break;
						}
					case System.DayOfWeek.Tuesday:
						{
							strReturn = "Tuesday";
							break;
						}
					case System.DayOfWeek.Wednesday:
						{
							strReturn = "Wednesday";
							break;
						}
					case System.DayOfWeek.Thursday:
						{
							strReturn = "Thursday";
							break;
						}
					case System.DayOfWeek.Friday:
						{
							strReturn = "Friday";
							break;
						}
					case System.DayOfWeek.Saturday:
						{
							strReturn = "Saturday";
							break;
						}
				}
				//end switch
				WeekDayName = strReturn;
				return WeekDayName;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			MoveNext = -1;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				if (intCurrentIndex >= Information.UBound(ShiftList, 1))
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= Information.UBound(ShiftList, 1))
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > Information.UBound(ShiftList, 1))
						{
							intReturn = -1;
						}
						else if (ShiftList[intCurrentIndex] == null)
						{
						}
						else if (ShiftList[intCurrentIndex].Unused)
						{
							if (intCurrentIndex == Information.UBound(ShiftList, 1))
							{
								intCurrentIndex += 1;
							}
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intCurrentIndex > Information.UBound(ShiftList, 1))
					intReturn = -1;
				intCurrentIndex = intReturn;
			}
			MoveNext = intReturn;
			return MoveNext;
		}

		public void MoveFirst()
		{
			intCurrentIndex = -1;
			MoveNext();
		}

		public void MarkAllShiftsAsUnworked()
		{
			int intCIndex;
			intCIndex = intCurrentIndex;
			MoveFirst();
			if (intCurrentIndex >= 0)
			{
				clsWorkShift tShift;
				do
				{
					tShift = GetCurrentShift();
					if (!(tShift == null))
					{
						if (!(tShift.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.GiftHoliday)))
						{
							tShift.ActualEnd = "UNWORKED";
							tShift.ActualStart = "UNWORKED";
						}
					}
				}
				while (!(MoveNext() < 0));
			}
			intCurrentIndex = intCIndex;
		}

		public void MarkAllShiftsAsWorked()
		{
			int intCIndex;
			intCIndex = intCurrentIndex;
			MoveFirst();
			if (intCurrentIndex >= 0)
			{
				clsWorkShift tShift;
				do
				{
					tShift = GetCurrentShift();
					if (!(tShift == null))
					{
						if (fecherFoundation.Strings.UCase(tShift.ScheduledEnd) != "UNSCHEDULED")
						{
							tShift.ActualEnd = tShift.ScheduledEnd;
						}
						if (fecherFoundation.Strings.UCase(tShift.ScheduledStart) != "UNSCHEDULED")
						{
							tShift.ActualStart = tShift.ScheduledStart;
						}
					}
				}
				while (!(MoveNext() < 0));
			}
			intCurrentIndex = intCIndex;
		}

		public void ClearShiftIDs()
		{
			int intCIndex;
			intCIndex = intCurrentIndex;
			MoveFirst();
			if (intCurrentIndex >= 0)
			{
				clsWorkShift tShift;
				do
				{
					tShift = GetCurrentShift();
					if (!(tShift == null))
					{
						tShift.ID = 0;
					}
				}
				while (!(MoveNext() < 0));
			}
			intCurrentIndex = intCIndex;
		}

		public bool HasUnworkedOrSick()
		{
			bool HasUnworkedOrSick = false;
			int intCIndex;
			bool boolReturn;
			clsWorkShift tShift;
			boolReturn = false;
			intCIndex = intCurrentIndex;
			MoveFirst();
			while (GetCurrentIndex() >= 0)
			{
				tShift = GetCurrentShift();
				if (!(tShift == null))
				{
					clsShiftType tSType = new clsShiftType();
					tSType.LoadType(tShift.ShiftType);
					if (fecherFoundation.Strings.UCase(tShift.ScheduledEnd) != "UNSCHEDULED" && fecherFoundation.Strings.UCase(tShift.ScheduledStart) != "UNSCHEDULED")
					{
						if (fecherFoundation.Strings.UCase(tShift.ActualEnd) == "UNWORKED" || fecherFoundation.Strings.UCase(tShift.ActualStart) == "UNWORKED")
						{
							if (tSType.ShiftType != FCConvert.ToInt32(modSchedule.ScheduleShiftType.GiftHoliday))
							{
								boolReturn = true;
							}
						}
					}
					if (tSType.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.Sick))
					{
						boolReturn = true;
					}
				}
				MoveNext();
			}
			intCurrentIndex = intCIndex;
			HasUnworkedOrSick = boolReturn;
			return HasUnworkedOrSick;
		}
	}
}
