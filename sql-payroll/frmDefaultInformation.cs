//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using System.IO;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	public partial class frmDefaultInformation : BaseForm
	{
		public frmDefaultInformation()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDefaultInformation InstancePtr
		{
			get
			{
				return (frmDefaultInformation)Sys.GetInstance(typeof(frmDefaultInformation));
			}
		}

		protected frmDefaultInformation _InstancePtr = null;
		const int CNSTCOLID = 0;
		const int CNSTCOLDEDCODE = 1;
		const int CNSTCOLCODE = 2;
		const int CNSTCOLFUNDID = 3;
		const int CNSTCOLIRSNUMBER = 4;
		const int CNSTCOLTAXYEAR = 5;
		const int CNSTCOLSOURCE = 6;
		const int CNSTCOLPLANNAME = 7;
		const int CNSTEXTRAMSRSCOLDESCRIPTION = 0;
		const int CNSTEXTRAMSRSCOLAMOUNT = 1;
		const int CNSTEXTRAMSRSCOLACCOUNT = 2;
		const int CNSTEXTRAMSRSCOLRECIPIENT = 3;
		private int intDataChanged;
		private int intLastCheckType;
		private clsHistory clsHistoryClass = new clsHistory();
		private string strLogoFile = "";
		private bool boolLoading;

		private struct ExtraMSRSInfo
		{
			public string Account;
			public int RECIPIENT;
			public double Amount;
		};
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		ExtraMSRSInfo[] aryMSRSInfo = new ExtraMSRSInfo[3 + 1];
		private clsGridAccount clsGA = new clsGridAccount();
		private cSettingsController setCont = new cSettingsController();
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;

        private void LoadICMA()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsLoad.OpenRecordset("select * from icmainformation order by ID", "twpy0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTCOLID, FCConvert.ToString(clsLoad.Get_Fields("ID")));
					Grid.TextMatrix(lngRow, CNSTCOLDEDCODE, FCConvert.ToString(clsLoad.Get_Fields("dedcode")));
					Grid.TextMatrix(lngRow, CNSTCOLCODE, FCConvert.ToString(clsLoad.Get_Fields("rcplan")));
					Grid.TextMatrix(lngRow, CNSTCOLFUNDID, FCConvert.ToString(clsLoad.Get_Fields("fundid")));
					Grid.TextMatrix(lngRow, CNSTCOLPLANNAME, FCConvert.ToString(clsLoad.Get_Fields("planname")));
					Grid.TextMatrix(lngRow, CNSTCOLSOURCE, FCConvert.ToString(clsLoad.Get_Fields("source")));
					Grid.TextMatrix(lngRow, CNSTCOLIRSNUMBER, FCConvert.ToString(clsLoad.Get_Fields_String("IRSNumber")));
					Grid.TextMatrix(lngRow, CNSTCOLTAXYEAR, FCConvert.ToString(clsLoad.Get_Fields("Taxyear")));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadICMA", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveICMA()
		{
			bool SaveICMA = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			bool boolReturn;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveICMA = false;
				boolReturn = true;
				// take care of deletions first
				for (x = 0; x <= (GridDeleteICMA.Rows - 1); x++)
				{
					clsSave.Execute("delete from ICMAInformation where ID = " + FCConvert.ToString(Conversion.Val(GridDeleteICMA.TextMatrix(x, 0))), "twpy0000.vb1");
				}
				// x
				GridDeleteICMA.Rows = 0;
				for (x = 1; x <= (Grid.Rows - 1); x++)
				{
					clsSave.OpenRecordset("select * from icmainformation where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, CNSTCOLID))), "twpy0000.vb1");
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
					}
					clsSave.Set_Fields("planname", fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTCOLPLANNAME)));
					clsSave.Set_Fields("FUNDID", fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTCOLFUNDID)));
					clsSave.Set_Fields("dedcode", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, CNSTCOLDEDCODE))));
					if (Grid.TextMatrix(x, CNSTCOLCODE).Length != 6)
					{
						boolReturn = false;
					}
					else
					{
						if (Conversion.Val(Strings.Left(Grid.TextMatrix(x, CNSTCOLCODE) + "0", 1)) == 1
							|| Conversion.Val(Strings.Left(Grid.TextMatrix(x, CNSTCOLCODE) + "0", 1)) == 3
							|| Conversion.Val(Strings.Left(Grid.TextMatrix(x, CNSTCOLCODE) + "0", 1)) == 7
							|| Conversion.Val(Strings.Left(Grid.TextMatrix(x, CNSTCOLCODE) + "0", 1)) == 8)
						{
						}
						else
						{
							boolReturn = false;
						}
					}
					if (boolReturn)
					{
						clsSave.Set_Fields("rcplan", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE))));
					}
					clsSave.Set_Fields("source", fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTCOLSOURCE)));
					clsSave.Set_Fields("IRSNumber", fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTCOLIRSNUMBER)));
					clsSave.Set_Fields("taxyear", fecherFoundation.Strings.Trim(Grid.TextMatrix(x, CNSTCOLTAXYEAR)));
					clsSave.Update();
					Grid.TextMatrix(x, CNSTCOLID, FCConvert.ToString(clsSave.Get_Fields("ID")));
				}
				// x
				SaveICMA = boolReturn;
				return SaveICMA;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveICMA", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveICMA;
		}

		private void cmdSampleW2_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
			int intindex;
			double dblAdj;
			double dblHoriz;
			if (cboW2CheckType.SelectedIndex >= 0)
			{
				intindex = cboW2CheckType.ItemData(cboW2CheckType.SelectedIndex);
			}
			else
			{
				intindex = modGlobalVariables.CNSTW2TYPELASER;
			}
			dblAdj = Conversion.Val(txtW2Adjustment.Text);
			dblHoriz = Conversion.Val(txtW2Horizontal.Text);
			switch (intindex)
			{
				case modGlobalVariables.CNSTW2TYPELASER:
					{
						rptW2Laser.InstancePtr.Init(true, dblAdj, dblHoriz);
						break;
					}
				case modGlobalVariables.CNSTW2TYPELASER2X2:
					{
						rptW2Laser2x2.InstancePtr.Init(true, dblAdj);
						break;
					}
			}
			//end switch
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						mnuDeleteICMARow_Click();
						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						mnuAddICMARow_Click();
						break;
					}
			}
			//end switch
		}

		private void mnuAddICMARow_Click()
		{
			intDataChanged += 1;
			Grid.Rows += 1;
			Grid.TopRow = Grid.Rows - 1;
			Grid.ComboList = " ";
			// Dave 12/14/2006---------------------------------------------------
			// Add change record for adding a row to the grid
			clsReportChanges.AddChange("Added Row to ICMA Deductions");
			// ------------------------------------------------------------------
		}

		private void mnuDeleteICMARow_Click()
		{
			if (Grid.Row < 1)
				return;
			if (MessageBox.Show("This will delete the current ICMA deduction" + "\r\n" + "Do you want to continue?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			intDataChanged += 1;
			GridDeleteICMA.Rows += 1;
			GridDeleteICMA.TextMatrix(GridDeleteICMA.Rows - 1, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTCOLID))));
			// Dave 12/14/2006---------------------------------------------------
			// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
			modAuditReporting.RemoveGridRow_8("Grid", intTotalNumberOfControls - 1, Grid.Row, clsControlInfo);
			// We then add a change record saying the row was deleted
			clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(Grid.Row) + " from ICMA Deductions");
			// -------------------------------------------------------------------
			Grid.RemoveItem(Grid.Row);
		}

		private void SetupGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			Grid.TextMatrix(0, CNSTCOLCODE, "Plan #");
			Grid.TextMatrix(0, CNSTCOLSOURCE, "Source");
			Grid.TextMatrix(0, CNSTCOLFUNDID, "Fund ID");
			Grid.TextMatrix(0, CNSTCOLPLANNAME, "Plan Name");
			Grid.TextMatrix(0, CNSTCOLDEDCODE, "Deduction");
			Grid.TextMatrix(0, CNSTCOLIRSNUMBER, "IRS Number");
			Grid.TextMatrix(0, CNSTCOLTAXYEAR, "Tax Year");
			Grid.ColHidden(CNSTCOLID, true);
			clsLoad.OpenRecordset("select * from TBLDEDUCTIONSETUP order by description", "twpy0000.vb1");
			strTemp = "";
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + clsLoad.Get_Fields("ID") + ";" + clsLoad.Get_Fields_String("description") + "|";
				clsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			Grid.ColComboList(CNSTCOLDEDCODE, strTemp);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTCOLCODE, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTCOLDEDCODE, FCConvert.ToInt32(0.27 * GridWidth));
			Grid.ColWidth(CNSTCOLFUNDID, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTCOLIRSNUMBER, FCConvert.ToInt32(0.13 * GridWidth));
			Grid.ColWidth(CNSTCOLTAXYEAR, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTCOLSOURCE, FCConvert.ToInt32(0.08 * GridWidth));
		}

		private void Grid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			intDataChanged += 1;
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngCol;
			lngRow = Grid.Row;
			lngCol = Grid.Col;
			if (lngRow < 1)
				return;
			if (lngCol == CNSTCOLSOURCE)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTCOLCODE)) != string.Empty)
				{
					if (Conversion.Val(Strings.Left(Grid.TextMatrix(lngRow, CNSTCOLCODE), 1)) == 1
						|| Conversion.Val(Strings.Left(Grid.TextMatrix(lngRow, CNSTCOLCODE), 1)) == 8)
					{
						Grid.ComboList = "ER" + "\t" + "Employer|EE" + "\t" + "Pre-Tax / Mandatory After-Tax|EV" + "\t" + "Voluntary After-Tax|LN" + "\t" + "Loan Repayment";
					}
					else if (Conversion.Val(Strings.Left(Grid.TextMatrix(lngRow, CNSTCOLCODE), 1)) == 3)
					{
						Grid.ComboList = " |LN" + "\t" + "Loan Repayment";
						// Grid.TextMatrix(lngRow, CNSTCOLSOURCE) = " "
					}
					else if (Conversion.Val(Strings.Left(Grid.TextMatrix(lngRow, CNSTCOLCODE), 1)) == 7)
					{
						Grid.ComboList = "EE|LN" + "\t" + "Loan Repayment";
						// Grid.TextMatrix(lngRow, CNSTCOLSOURCE) = "EE"
					}
					else
					{
						Grid.ComboList = " |LN" + "\t" + "Loan Repayment";
						// Grid.TextMatrix(lngRow, CNSTCOLSOURCE) = " "
					}
				}
				else
				{
					Grid.ComboList = " ";
				}
			}
			else if (lngCol == CNSTCOLTAXYEAR)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTCOLCODE)) != string.Empty)
				{
					if (Conversion.Val(Strings.Left(Grid.TextMatrix(lngRow, CNSTCOLCODE), 1)) == 7)
					{
						Grid.ComboList = "C" + "\t" + "Current Year|P" + "\t" + "Previous Year";
					}
					else
					{
						Grid.ComboList = " ";
					}
				}
				else
				{
					Grid.ComboList = " ";
				}
			}
			else
			{
				Grid.ComboList = "";
			}
		}

		private void cboDataEntry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboPrintSequence_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboReportSequence_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboTypeChecks_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
			chkPrintSeparate.Visible = true;
            //FC:FINAL:AM:#2641 - disable button
            cmdEditCheck.Enabled = this.cboTypeChecks.Text == "Custom";
        }

		private void chkBankDD_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkACHClearingHouse_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
			if (chkACHClearingHouse.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkBalanced.Visible = true;
				lblEmployerID.Visible = true;
				cboEmployerIDPrefix.Visible = true;
				cmdACHPrenote.Visible = true;
			}
			else
			{
				chkBalanced.Visible = false;
				lblEmployerID.Visible = false;
				cboEmployerIDPrefix.Visible = false;
				cmdACHPrenote.Visible = false;
			}
		}
		
		private void chkWeeklyPR_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkNoMSRS_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cmdACHPrenote_Click(object sender, System.EventArgs e)
		{
			frmACHBankInformation.InstancePtr.Init(true, true);
		}

		private void cmdChooseFile_Click()
		{
			string strPath;
			FCFileSystem fso = new FCFileSystem();
			strPath = "";
			short intType = modSignatureFile.PAYROLLSIG;
			strPath = modSignatureFile.GetSignatureFile(ref intType);
			// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			// MDIParent.InstancePtr.CommonDialog1_Save.Flags = (vbPorterConverter.cdlOFNNoChangeDir || vbPorterConverter.cdlOFNFileMustExist ? -1 : 0)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			if (strPath != "")
			{
				MDIParent.InstancePtr.CommonDialog1.InitDir = Path.GetDirectoryName(strPath);
			}
			MDIParent.InstancePtr.CommonDialog1.Filter = "*.BMP;*.GIF;*.JPG";
			MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Select the signature file";
			MDIParent.InstancePtr.CommonDialog1.ShowSave();
			strPath = MDIParent.InstancePtr.CommonDialog1.FileName;
			if (strPath != string.Empty)
			{
				if (modSignatureFile.SaveSignatureFile(modSignatureFile.PAYROLLSIG, strPath))
				{
				}
			}
		}

		private void cmdSampleCheck_Click(object sender, System.EventArgs e)
		{
			// show an actual sample check
			frmReportViewer.InstancePtr.Init(rptNewSampleCheck.InstancePtr, showModal: this.Modal);
			//rptNewSampleCheck.InstancePtr.Show(App.MainForm);
		}

		private void frmDefaultInformation_Resize(object sender, System.EventArgs e)
		{
			ResizePic();
			ResizeGrid();
			ResizeGridExtraMSRS();
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			switch (Grid.Col)
			{
				case CNSTCOLCODE:
					{
						if (Conversion.Val(Strings.Left(Grid.EditText + "1", 1)) == 1 || Conversion.Val(Strings.Left(Grid.EditText + "1", 1)) == 3
							|| Conversion.Val(Strings.Left(Grid.EditText + "1", 1)) == 7 || Conversion.Val(Strings.Left(Grid.EditText + "1", 1)) == 8)
						{
							if (Grid.EditText.Length != 6)
							{
								MessageBox.Show("Plans should be 6 digits", "Bad length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								Grid.EditText = Strings.Left(Grid.EditText, 6);
								e.Cancel = true;
							}
						}
						else
						{
							MessageBox.Show("Plan type must begin with a 1,3,7 or 8", "Bad Plan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							Grid.EditText = "";
							e.Cancel = true;
							return;
						}
						break;
					}
			}
			//end switch
		}

		private void lstMSRS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void lstRetirement_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mnuClearWarrantLock_Click(object sender, System.EventArgs e)
		{
			// NEED A WAY TO ALLOW THE USER TO CLEAR THE WARRANT LOCK FIELD
			modGlobalFunctions.AddCYAEntry_6("PY", "Clear Warrant Lock");
			// DELETE THE WARRANT LOCK ENTRY
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.Execute("Delete from WarrantLock", "Payroll");
			MessageBox.Show("Warrant Lock Cleared Successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuCreateACH_Click(object sender, System.EventArgs e)
		{
			frmCreateACHFiles.InstancePtr.Init();
		}

		private void mnuEditCheck_Click(object sender, System.EventArgs e)
		{
			if (this.cboTypeChecks.Text == "Custom")
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("update tblcheckformat set customsetup = 1", "twpy0000.vb1");
				rsSave.Execute("update tbldefaultinformation set checktype = 2", "twpy0000.vb1");
				frmCreateCustomCheckFormat.InstancePtr.Show(App.MainForm);
			}
		}

		private void mnuEditPrint_Click(object sender, System.EventArgs e)
		{
			if (framCheckOptions.Visible == true)
			{
				framCheckOptions.Visible = false;
				framPayrollDefaults.Visible = true;
				mnuEditPrint.Text = "Edit Printing Options";
                cmdEditPrint.Text = "Edit Printing Options";
            }
			else if (fraW2s.Visible == true)
			{
				fraW2s.Visible = false;
				framCheckOptions.Visible = false;
				framPayrollDefaults.Visible = true;
				mnuEditPrint.Text = "Edit Printing Options";
                cmdEditPrint.Text = "Edit Printing Options";
            }
            else if (framRetirement.Visible == true)
			{
				framRetirement.Visible = false;
				fraW2s.Visible = false;
				framCheckOptions.Visible = false;
				framPayrollDefaults.Visible = true;
				mnuEditPrint.Text = "Edit Printing Options";
                cmdEditPrint.Text = "Edit Printing Options";
            }
            else if (framESHP.Visible == true)
			{
				framESHP.Visible = false;
				fraW2s.Visible = false;
				framCheckOptions.Visible = false;
				framPayrollDefaults.Visible = true;
				mnuEditPrint.Text = "Edit Printing Options";
                cmdEditPrint.Text = "Edit Printing Options";
            }
            else
			{
				mnuEditPrint.Text = "Edit Payroll Defaults";
                cmdEditPrint.Text = "Edit Payroll Defaults";
                framCheckOptions.BringToFront();
				framCheckOptions.Visible = true;
				framPayrollDefaults.Visible = false;
			}
		}

		private void mnuEditRetirementDeductions_Click(object sender, System.EventArgs e)
		{
			framCheckOptions.Visible = false;
			framPayrollDefaults.Visible = false;
			framESHP.Visible = false;
			mnuEditPrint.Text = "Edit Payroll Defaults";
            cmdEditPrint.Text = "Edit Payroll Defaults";
            framRetirement.Visible = true;
			framRetirement.BringToFront();
		}

		private void mnuESHPOptions_Click(object sender, System.EventArgs e)
		{
			framCheckOptions.Visible = false;
			framPayrollDefaults.Visible = false;
			framRetirement.Visible = false;
			framRetirement.Visible = false;
			mnuEditPrint.Text = "Edit Payroll Defaults";
            cmdEditPrint.Text = "Edit Payroll Defaults";
            framESHP.Visible = true;
			framESHP.BringToFront();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void frmDefaultInformation_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadMSRSData()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int x;
			rsLoad.OpenRecordset("select * from tblmsrstable", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				aryMSRSInfo[FCConvert.ToInt32(rsLoad.Get_Fields("reporttype"))].Amount = Conversion.Val(rsLoad.Get_Fields("extramsrs"));
				aryMSRSInfo[FCConvert.ToInt32(rsLoad.Get_Fields("reporttype"))].Account = FCConvert.ToString(rsLoad.Get_Fields("msrsaCCOUNT"));
				aryMSRSInfo[FCConvert.ToInt32(rsLoad.Get_Fields("reporttype"))].RECIPIENT = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("recipient"))));
				rsLoad.MoveNext();
			}
			for (x = 1; x <= 3; x++)
			{
				gridExtraMSRS.TextMatrix(x, CNSTEXTRAMSRSCOLACCOUNT, aryMSRSInfo[x].Account);
				gridExtraMSRS.TextMatrix(x, CNSTEXTRAMSRSCOLAMOUNT, Strings.Format(aryMSRSInfo[x].Amount, "0.00"));
				gridExtraMSRS.TextMatrix(x, CNSTEXTRAMSRSCOLRECIPIENT, FCConvert.ToString(aryMSRSInfo[x].RECIPIENT));
			}
			// x
		}

		private void frmDefaultInformation_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				Support.SendKeys("{TAB}", false);
				KeyCode = (Keys)0;
			}
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDefaultInformation_Load(object sender, System.EventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolLoading = true;
				clsGA.GRID7Light = gridExtraMSRS;
				clsGA.DefaultAccountType = "E";
				clsGA.OnlyAllowDefaultType = false;
				clsGA.AccountCol = CNSTEXTRAMSRSCOLACCOUNT;
				clsGA.Validation = true;
				clsGA.AllowSplits = modRegionalTown.IsRegionalTown();
				// vsElasticLight1.Enabled = True
				aryMSRSInfo[0].Amount = 0;
				aryMSRSInfo[1].Amount = 0;
				aryMSRSInfo[2].Amount = 0;
				aryMSRSInfo[3].Amount = 0;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewBankAccountNumbers.ToInteger()))
                {
                    case "F":
                        bankAccountViewPermission = BankAccountViewType.Full;
                        break;
                    case "P":
                        bankAccountViewPermission = BankAccountViewType.Masked;
                        break;
                    default:
                        bankAccountViewPermission = BankAccountViewType.None;
                        break;
                }

                if (bankAccountViewPermission != BankAccountViewType.Full)
                {
                    cmdACHPrenote.Enabled = false;
                }
                if (!modCoreysSweeterCode.Statics.gboolESP && fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "trioville")
				{
					mnuESHPOptions.Enabled = false;
					cmdESHPOptions.Enabled = false;
                }

                SetupcmbW2Type();
                SetupcboW2CheckType();
				SetupcmbTrackLastCheckNumber();
                SetupcboTypeChecks();
				if (modGlobalConstants.Statics.gboolBD)
				{
					cmbTrackLastCheckNumber.Enabled = false;
					ToolTip1.SetToolTip(cmbTrackLastCheckNumber, "Must be edited in Budgetary");
					ToolTip1.SetToolTip(Label25, "Must be edited in Budgetary");
				}
				SetupGridExtraMSRS();
				cboEmployerIDPrefix.Clear();
				cboEmployerIDPrefix.AddItem(" ");
				cboEmployerIDPrefix.ItemData(cboEmployerIDPrefix.NewIndex, 0);
				cboEmployerIDPrefix.AddItem("1");
				cboEmployerIDPrefix.ItemData(cboEmployerIDPrefix.NewIndex, 1);
				cboEmployerIDPrefix.AddItem("9");
				cboEmployerIDPrefix.ItemData(cboEmployerIDPrefix.NewIndex, 9);
				cboDataEntry.SelectedIndex = 0;
				cboPrintSequence.SelectedIndex = 0;
				cboReportSequence.SelectedIndex = 0;
				cboTypeChecks.SelectedIndex = 0;
				cboW2CheckType.SelectedIndex = 0;
				GridDeleteICMA.Rows = 0;
				FillListBox(ref lstRetirement);
				FillListBox(ref lstMSRS);
				FillListBox(ref lstPayback);
				FillListBox(ref lstMatchesBox1);
				SetupGrid();
				ShowData();
				LoadMSRSData();
				LoadICMA();
				SetListBox(ref lstRetirement);
				SetListBox(ref lstMSRS);
				SetListBox(ref lstPayback);
				SetListBox(ref lstMatchesBox1);
				if (modSecurity.ValidPermissions_6(null, modGlobalVariables.CHECKS, false))
				{
					mnuCreateACH.Enabled = true;
				}
				else
				{
					mnuCreateACH.Enabled = false;
				}
				clsHistoryClass.SetGridIDColumn("PY");
				intDataChanged = 0;
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClass();
				// This is a function you will need to use in each form if you have a listbox to add which list items you want to keep track of changes for
				FillControlInformationClassFromListBox();
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
                //FC:FINAL:AM:#2641 - disable button
                cmdEditCheck.Enabled = this.cboTypeChecks.Text == "Custom";
                // counter
                // ---------------------------------------------------------------------
                boolLoading = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void FillListBox(ref FCListBox ControlName)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FillListBox";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsDeductions = new clsDRWrapper();
				ControlName.Clear();
				rsDeductions.OpenRecordset("Select * from tblDeductionSetup", "Twpy0000.vb1");
				while (!rsDeductions.EndOfFile())
				{
					ControlName.AddItem(rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(10 - FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber")).Length, " ") + rsDeductions.Get_Fields("Description"));
					ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsDeductions.Get_Fields("ID")));
					rsDeductions.MoveNext();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetupGridExtraMSRS()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			gridExtraMSRS.TextMatrix(1, CNSTEXTRAMSRSCOLDESCRIPTION, "PLD");
			gridExtraMSRS.TextMatrix(2, CNSTEXTRAMSRSCOLDESCRIPTION, "School PLD");
			gridExtraMSRS.TextMatrix(3, CNSTEXTRAMSRSCOLDESCRIPTION, "Teacher");
			gridExtraMSRS.TextMatrix(0, CNSTEXTRAMSRSCOLACCOUNT, "Account");
			gridExtraMSRS.TextMatrix(0, CNSTEXTRAMSRSCOLAMOUNT, "Amount");
			gridExtraMSRS.TextMatrix(0, CNSTEXTRAMSRSCOLRECIPIENT, "Recipient");
            gridExtraMSRS.ColAlignment(CNSTEXTRAMSRSCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			rsLoad.OpenRecordset("select * from tblRecipients order by name", "twpy0000.vb1");
			strTemp = "#0;None|";
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("Name") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != "")
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			gridExtraMSRS.ColComboList(CNSTEXTRAMSRSCOLRECIPIENT, strTemp);
		}

		private void ResizeGridExtraMSRS()
		{
			int GridWidth = 0;
			GridWidth = gridExtraMSRS.WidthOriginal;
			gridExtraMSRS.ColWidth(CNSTEXTRAMSRSCOLDESCRIPTION, FCConvert.ToInt32(0.15 * GridWidth));
			gridExtraMSRS.ColWidth(CNSTEXTRAMSRSCOLAMOUNT, FCConvert.ToInt32(0.15 * GridWidth));
			gridExtraMSRS.ColWidth(CNSTEXTRAMSRSCOLACCOUNT, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				FCFileSystem fso = new FCFileSystem();
				clsDRWrapper rsSave = new clsDRWrapper();
				
				if (FCConvert.CBool(modRegistry.GetRegistryKey("PrefillAccounts", "PY", "True")))
				{
					chkFillAccount.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFillAccount.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				string strTemp;
				int intTemp;
				int x;
				intTemp = 0;
				strTemp = setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", "");
				if (fecherFoundation.Strings.LCase(strTemp) == "no")
				{
					intTemp = 0;
				}
				else if (fecherFoundation.Strings.LCase(strTemp) == "bank")
				{
					intTemp = 1;
				}
				else if (fecherFoundation.Strings.LCase(strTemp) == "check type")
				{
					intTemp = 2;
				}
				for (x = 0; x <= cmbTrackLastCheckNumber.Items.Count - 1; x++)
				{
					if (cmbTrackLastCheckNumber.ItemData(x) == intTemp)
					{
						cmbTrackLastCheckNumber.SelectedIndex = x;
						break;
					}
				}
				// x
				// GET THE DATA TO FILL THE SCREEN WITH
				rsSave.OpenRecordset("Select * from tblDefaultInformation", "Twpy0000.vb1");
				if (!rsSave.EndOfFile())
				{
					txtRA1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("ReturnAddress1")));
					txtRA2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("ReturnAddress2")));
					txtRA3.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("ReturnAddress3")));
					txtRA4.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("ReturnAddress4")));
					txtCode1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("open1")));
					txtCode2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("open2")));
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("ignoreprinterfonts")))
					{
						chkIgnorePrinterFonts.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkIgnorePrinterFonts.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					cmbCheckImage.SelectedIndex = FCConvert.ToInt32(rsSave.Get_Fields_Int16("UseCheckImage"));
					cboPrintSequence.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsSave.Get_Fields_Int32("PrintSequence")));
					cboDataEntry.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsSave.Get_Fields_Int32("DataEntrySequence")));
					cboReportSequence.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsSave.Get_Fields_Int32("ReportSequence")));
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("EncumberContracts")))
					{
						chkContractEncumbrances.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkContractEncumbrances.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intLastCheckType = rsSave.Get_Fields_Int32("checktype");

                    for (x = 0; x < cboTypeChecks.Items.Count; x++)
                    {
                        if (cboTypeChecks.ItemData(x) == intLastCheckType)
                        {
                            cboTypeChecks.SelectedIndex = x;
                            break;
                        }
                    }
					for (x = 0; x <= cboW2CheckType.Items.Count - 1; x++)
					{
						if (cboW2CheckType.ItemData(x) == Conversion.Val(rsSave.Get_Fields("w2checktype")))
						{
							cboW2CheckType.SelectedIndex = x;
							break;
						}
					}
                    // x
                    // cboW2CheckType.ListIndex = Val(.Fields("W2CheckType"))
                    //FC:FINAL:AM:#i2095 - convert to CheckState
                    //chkMMAUnemployment.CheckState = FCConvert.ToInt32(rsSave.Get_Fields("MMAUnemployment")) * -1;
                    chkMMAUnemployment.CheckState = rsSave.Get_Fields_Boolean("MMAUnemployment") ? CheckState.Checked : CheckState.Unchecked;
                    if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("achclearinghouse")))
					{
						chkACHClearingHouse.CheckState = Wisej.Web.CheckState.Checked;
						chkBalanced.Visible = true;
						lblEmployerID.Visible = true;
						cboEmployerIDPrefix.Visible = true;
					}
					else
					{
						chkACHClearingHouse.CheckState = Wisej.Web.CheckState.Unchecked;
						chkBalanced.Visible = false;
						lblEmployerID.Visible = false;
						cboEmployerIDPrefix.Visible = false;
					}
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("achbalanced")))
					{
						chkBalanced.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkBalanced.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					cboEmployerIDPrefix.Text = FCConvert.ToString(1);
					for (x = 0; x <= cboEmployerIDPrefix.Items.Count - 1; x++)
					{
						if (FCConvert.ToInt32(rsSave.Get_Fields("achemployeridprefix")) == cboEmployerIDPrefix.ItemData(x))
						{
							cboEmployerIDPrefix.SelectedIndex = x;
						}
					}
					// x
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("electronicc1")))
					{
						chkElectronicC1.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkElectronicC1.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("UseMultiFund")))
					{
						chkMultiFunds.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkMultiFunds.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("CheckNegBalances")))
					{
						chkNegBalances.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkNegBalances.CheckState = Wisej.Web.CheckState.Unchecked;
					}
                    //chkWeeklyPR.CheckState = FCConvert.ToInt32(rsSave.Get_Fields("WeeklyPR")) * -1;
                    chkWeeklyPR.CheckState = rsSave.Get_Fields_Boolean("WeeklyPR") ? CheckState.Checked : CheckState.Unchecked;
                    // CHANGED FOR CALL ID 62852 MATTHEW 2/10/2005
                    //chkNoMSRS.CheckState = FCConvert.ToInt32(rsSave.Get_Fields("NoMSRS")) * -1;
                    chkNoMSRS.CheckState = rsSave.Get_Fields_Boolean("NoMSRS") ? CheckState.Checked : CheckState.Unchecked;
                    // chkBankDD = (.Fields("BankDD")) * -1
                    if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("printsignature")))
					{
						chkSignature.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSignature.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("skippageded")))
					{
						chkSkipPages.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSkipPages.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("PrintInvalidSeparate")))
					{
						chkPrintSeparate.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPrintSeparate.CheckState = Wisej.Web.CheckState.Unchecked;
					}

					strLogoFile = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("CheckLogo")));
					if (strLogoFile != string.Empty)
					{
						if (FCFileSystem.FileExists(strLogoFile))
						{
							imgLogo.Image = FCUtils.LoadPicture(strLogoFile);
						}
						else
						{
							strLogoFile = "";
						}
					}
					if (Conversion.Val(rsSave.Get_Fields_Int32("ESPPort")) > 0)
					{
						txtEspPort.Text = FCConvert.ToString(Conversion.Val(rsSave.Get_Fields("espport")));
					}
					else
					{
						txtEspPort.Text = FCConvert.ToString(22);
					}
					txtEspOrg.Text = FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int16("ESPOrganization")));
					txtEspHost.Text = FCConvert.ToString(rsSave.Get_Fields_String("ESPHost"));
					txtESPUser.Text = FCConvert.ToString(rsSave.Get_Fields_String("ESPUser"));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("espserverdatadir"))) != "")
					{
						txtESPDataPath.Text = FCConvert.ToString(rsSave.Get_Fields("espserverdatadir"));
					}
					else
					{
						txtESPDataPath.Text = "Data/Import";
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("espservercheckdir"))) != "")
					{
						txtESPCheckPath.Text = FCConvert.ToString(rsSave.Get_Fields("espservercheckdir"));
					}
					else
					{
						txtESPCheckPath.Text = "PDF/Checks";
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("espserverw2dir"))) != "")
					{
						txtESPW2Path.Text = FCConvert.ToString(rsSave.Get_Fields("espserverw2dir"));
					}
					else
					{
						txtESPW2Path.Text = "PDF/W2";
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("espserverexportdir"))) != "")
					{
						txtESPExportPath.Text = FCConvert.ToString(rsSave.Get_Fields("espserverexportdir"));
					}
					else
					{
						txtESPExportPath.Text = "Data/Export";
					}
					txtESPPassword.Text = FCConvert.ToString(rsSave.Get_Fields("esppassword"));
					txtESPPassword.Tag = rsSave.Get_Fields("esppassword");
					txtESPPasswordConfirm.Text = FCConvert.ToString(rsSave.Get_Fields("esppassword"));
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("ShowPayPeriodOnStub")))
					{
						chkPayPeriod.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPayPeriod.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("CheckAlignment", "PY"));
				if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
				{
					rsSave.OpenRecordset("select * from tblCheckFormat", "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						txtCheckLineAdjustment.Text = FCConvert.ToString(rsSave.Get_Fields("LaserAlignment"));
						txtW2Adjustment.Text = FCConvert.ToString(rsSave.Get_Fields("W2Alignment"));
						txtW2Horizontal.Text = FCConvert.ToString(rsSave.Get_Fields("w2horizontal"));
					}
				}
				else
				{
					txtCheckLineAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("CheckAlignment", "PY")));
					txtW2Adjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("W2Alignment", "PY")));
					txtW2Horizontal.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("W2Horizontal", "PY")));
				}
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private bool NotValidToSave()
		{
			bool NotValidToSave = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "NotValidToSave";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (cboPrintSequence.SelectedIndex < 0)
					NotValidToSave = true;
				if (cboDataEntry.SelectedIndex < 0)
					NotValidToSave = true;
				if (cboReportSequence.SelectedIndex < 0)
					NotValidToSave = true;
				if (cboTypeChecks.SelectedIndex < 0)
					NotValidToSave = true;
				return NotValidToSave;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return NotValidToSave;
			}
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			mnuEditCheck.Enabled = this.cboTypeChecks.Text == "Custom";
            cmdEditCheck.Enabled = this.cboTypeChecks.Text == "Custom";
        }

        private void mnuSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveData();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveListBox(ref FCListBox ControlName)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveListBox";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsSave = new clsDRWrapper();
				bool boolMSRS;
				int intCounter;
				string strValue = "";
				if (ControlName.GetName() == "lstRetirement")
				{
					strValue = "R";
				}
				else if (ControlName.GetName() == "lstMSRS")
				{
					strValue = "L";
				}
				else if (ControlName.GetName() == "lstPayback")
				{
					strValue = "P";
				}
				else if (ControlName.GetName() == "lstMatchesBox1")
				{
					strValue = "M";
				}
				rsSave.Execute("Delete from tblDefaultDeductions where Type = '" + strValue + "'", "Twpy0000.vb1");
				rsSave.OpenRecordset("Select * from tblDefaultDeductions where Type = '" + strValue + "'", "Twpy0000.vb1");
				if (rsSave.EndOfFile())
				{
					for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
					{
						if (ControlName.Selected(intCounter))
						{
							rsSave.AddNew();
							rsSave.Set_Fields("Type", strValue);
							rsSave.Set_Fields("DeductionID", ControlName.ItemData(intCounter));
							rsSave.Set_Fields("LastUserID", modGlobalRoutines.FixQuote(modGlobalVariables.Statics.gstrUser));
							rsSave.Update();
						}
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetListBox(ref FCListBox ControlName)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetListBox";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsSet = new clsDRWrapper();
				bool boolMSRS;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				int intCount;
				string strValue = "";
				boolMSRS = ControlName.GetName() == "lstMSRS";
				if (ControlName.GetName() == "lstRetirement")
				{
					strValue = "R";
				}
				else if (ControlName.GetName() == "lstMSRS")
				{
					strValue = "L";
				}
				else if (ControlName.GetName() == "lstPayback")
				{
					strValue = "P";
				}
				else if (ControlName.GetName() == "lstMatchesBox1")
				{
					strValue = "M";
				}
				rsSet.OpenRecordset("Select * from tblDefaultDeductions where Type = '" + strValue + "'", "Twpy0000.vb1");
				if (!rsSet.EndOfFile())
				{
					for (intCounter = 0; intCounter <= (rsSet.RecordCount() - 1); intCounter++)
					{
						for (intCount = 0; intCount <= ControlName.Items.Count - 1; intCount++)
						{
							if (ControlName.ItemData(intCount) == FCConvert.ToInt32(rsSet.Get_Fields_Int32("DeductionID")))
							{
								ControlName.SetSelected(intCount, true);
								break;
							}
						}
						rsSet.MoveNext();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (!SaveChanges())
				{
					e.Cancel = true;
					return;
				}
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public bool SaveChanges()
		{
			bool SaveChanges = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				bool boolReturn;
				boolReturn = true;
				SaveChanges = false;
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						boolReturn = SaveData();
					}
				}
				SaveChanges = boolReturn;
				return SaveChanges;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return SaveChanges;
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				int intpos = 0;
				string strSQL = "";
				FCFileSystem fso = new FCFileSystem();
				string strPath = "";
				bool boolReturn;
				int x;
				if (NotValidToSave())
					return SaveData;
				gridExtraMSRS.Row = 0;
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Customize", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromListBox();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				Grid.Row = -1;
				boolReturn = true;
				string strTemp;
				strTemp = "No";
				if (cmbTrackLastCheckNumber.SelectedIndex >= 0)
				{
					switch (cmbTrackLastCheckNumber.ItemData(cmbTrackLastCheckNumber.SelectedIndex))
					{
						case 0:
							{
								strTemp = "No";
								break;
							}
						case 1:
							{
								strTemp = "Bank";
								break;
							}
						case 2:
							{
								strTemp = "Check Type";
								break;
							}
					}
					//end switch
				}
				setCont.SaveSetting(strTemp, "UseBanksLastCheckNumber", "", "", "", "");
				
				if (chkFillAccount.CheckState == Wisej.Web.CheckState.Checked)
				{
					modRegistry.SaveRegistryKey("PrefillAccounts", FCConvert.ToString(true), "PY");
				}
				else
				{
					modRegistry.SaveRegistryKey("PrefillAccounts", FCConvert.ToString(false), "PY");
				}

                var checkType = 0;
                if (cboTypeChecks.SelectedIndex >= 0)
                {
                    checkType = cboTypeChecks.ItemData(cboTypeChecks.ListIndex);
                }

                switch (checkType)
				{
					case 1:
						{
							// laser
							intpos = 1;
							// laser only allow check to be in middle
							break;
						}
					case 2:
						{
                            // custom
                            intpos = 2;
       //                     rsSave.OpenRecordset("select * from customchecks", "twpy0000.vb1");
							//if (!rsSave.EndOfFile())
							//{
							//	string vbPorterVar = FCConvert.ToString(rsSave.Get_Fields("checktype"));
							//	if (vbPorterVar == "L")
							//	{
							//		intpos = 2;
							//		// laser
							//	}
							//	else if (vbPorterVar == "D")
							//	{
							//		intpos = 1;
							//	}
							//}
							//else
							//{
							//	intpos = 1;
							//	// default to standard
							//}
							break;
						}
				}
				//end switch
				rsSave.Execute("update tblCheckFormat set NumberOfStubs = " + FCConvert.ToString(intpos) + ",LaserAlignment = " + FCConvert.ToString(Conversion.Val(txtCheckLineAdjustment.Text)) + ",W2Alignment = " + FCConvert.ToString(Conversion.Val(txtW2Adjustment.Text)) + ",w2horizontal = " + FCConvert.ToString(Conversion.Val(txtW2Horizontal.Text)), "twpy0000.vb1");
				modRegistry.SaveRegistryKey("CheckAlignment", FCConvert.ToString(Conversion.Val(txtCheckLineAdjustment.Text)), "PY");
				modRegistry.SaveRegistryKey("W2Alignment", FCConvert.ToString(Conversion.Val(txtW2Adjustment.Text)), "PY");
				modRegistry.SaveRegistryKey("W2Horizontal", FCConvert.ToString(Conversion.Val(txtW2Horizontal.Text)), "PY");
				if (checkType != 2)
				{
					rsSave.Execute("update tblcheckformat set CustomSetup = 0", "twpy0000.vb1");
				}
				else
				{
					rsSave.Execute("update tblcheckformat set customsetup = 1", "twpy0000.vb1");
				}
				for (x = 1; x <= 3; x++)
				{
					rsSave.OpenRecordset("select * from tblMSRSTable where reporttype = " + FCConvert.ToString(x), "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("reporttype", x);
					}
					rsSave.Set_Fields("ExtraMSRS", FCConvert.ToString(Conversion.Val(gridExtraMSRS.TextMatrix(x, CNSTEXTRAMSRSCOLAMOUNT))));
					rsSave.Set_Fields("recipient", FCConvert.ToString(Conversion.Val(gridExtraMSRS.TextMatrix(x, CNSTEXTRAMSRSCOLRECIPIENT))));
					rsSave.Set_Fields("msrsAccount", gridExtraMSRS.TextMatrix(x, CNSTEXTRAMSRSCOLACCOUNT));
					rsSave.Update();
				}
				// x
				rsSave.OpenRecordset("Select * from tblDefaultInformation", "Twpy0000.vb1");
				if (rsSave.EndOfFile())
				{
					rsSave.AddNew();
				}
				else
				{
					rsSave.Edit();
				}
				if (cmbCheckImage.Text == "None")
				{
					rsSave.Set_Fields("UseCheckImage", 0);
				}
				else if (cmbCheckImage.Text == "Use Town Seal")
				{
					rsSave.Set_Fields("UseCheckImage", 1);
				}
				else if (cmbCheckImage.Text == "Use Selected Image")
				{
					rsSave.Set_Fields("UseCheckImage", 2);
				}
				rsSave.Set_Fields("ReturnAddress1", fecherFoundation.Strings.Trim(txtRA1.Text));
				rsSave.Set_Fields("ReturnAddress2", fecherFoundation.Strings.Trim(txtRA2.Text));
				rsSave.Set_Fields("ReturnAddress3", fecherFoundation.Strings.Trim(txtRA3.Text));
				rsSave.Set_Fields("ReturnAddress4", fecherFoundation.Strings.Trim(txtRA4.Text));
				rsSave.Set_Fields("open1", fecherFoundation.Strings.Trim(txtCode1.Text));
				rsSave.Set_Fields("open2", fecherFoundation.Strings.Trim(txtCode2.Text));
				rsSave.Set_Fields("PrintSequence", cboPrintSequence.SelectedIndex);
				rsSave.Set_Fields("DataEntrySequence", cboDataEntry.SelectedIndex);
				rsSave.Set_Fields("ReportSequence", cboReportSequence.SelectedIndex);
				rsSave.Set_Fields("CheckType", checkType);
				rsSave.Set_Fields("W2CheckType", cboW2CheckType.ItemData(cboW2CheckType.SelectedIndex));
				rsSave.Set_Fields("ACHClearingHouse", chkACHClearingHouse.CheckState == Wisej.Web.CheckState.Checked);
				rsSave.Set_Fields("ACHBalanced", chkBalanced.CheckState == Wisej.Web.CheckState.Checked);
				if (cboEmployerIDPrefix.Text == string.Empty)
					cboEmployerIDPrefix.Text = FCConvert.ToString(1);
				if (cboEmployerIDPrefix.SelectedIndex < 0)
					cboEmployerIDPrefix.SelectedIndex = 1;
				// default to 1
				// .Fields("ACHEmployerIDPrefix") = cboEmployerIDPrefix.Text
				rsSave.Set_Fields("achemployeridprefix", cboEmployerIDPrefix.ItemData(cboEmployerIDPrefix.SelectedIndex));
				rsSave.Set_Fields("MMAUnemployment", chkMMAUnemployment.CheckState);
				rsSave.Set_Fields("WeeklyPR", chkWeeklyPR.CheckState);
				if (chkContractEncumbrances.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("EncumberContracts", true);
				}
				else
				{
					rsSave.Set_Fields("EncumberContracts", false);
				}
				if (chkIgnorePrinterFonts.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("IgnorePrinterFonts", true);
				}
				else
				{
					rsSave.Set_Fields("IgnorePrinterFonts", false);
				}
				if (chkPayPeriod.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("ShowPayPeriodOnStub", true);
				}
				else
				{
					rsSave.Set_Fields("ShowPayPeriodOnStub", false);
				}
				// CHANGED FOR CALL ID 62852 MATTHEW 2/10/2005
				rsSave.Set_Fields("NoMSRS", chkNoMSRS.CheckState);
				modGlobalVariables.Statics.gboolNoMSRS = 0 != chkNoMSRS.CheckState;
				// .Fields("BankDD") = chkBankDD
				rsSave.Set_Fields("skippageded", chkSkipPages.CheckState == Wisej.Web.CheckState.Checked);
				rsSave.Set_Fields("PrintSignature", chkSignature.CheckState == Wisej.Web.CheckState.Checked);
				rsSave.Set_Fields("ElectronicC1", chkElectronicC1.CheckState == Wisej.Web.CheckState.Checked);
				rsSave.Set_Fields("UseMultiFund", chkMultiFunds.CheckState == Wisej.Web.CheckState.Checked);
				rsSave.Set_Fields("CheckNegBalances", chkNegBalances.CheckState == Wisej.Web.CheckState.Checked);
				modGlobalVariables.Statics.gboolCheckNegBalances = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("CheckNegBalances"));
				//if (cboTypeChecks.SelectedIndex == 0)
				//{
				//	chkPrintSeparate.CheckState = Wisej.Web.CheckState.Unchecked;
				//}
				if (chkPrintSeparate.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("PrintInvalidSeparate", true);
				}
				else
				{
					rsSave.Set_Fields("printinvalidseparate", false);
				}
				if (strLogoFile != string.Empty)
				{
					if (!Directory.Exists("CheckIcons"))
					{
                        Directory.CreateDirectory("CheckIcons");
					}
					strPath = Environment.CurrentDirectory;
					if (Strings.Right(strPath, 1) != "\\")
						strPath += "\\";
					strPath += "CheckIcons\\";
					strPath += Path.GetFileName(strLogoFile);
					File.Copy(strLogoFile, strPath, true);
					FileInfo fl;
					fl = new FileInfo(strLogoFile);
					fl.Attributes = FileAttributes.Normal;
					strLogoFile = strPath;
				}
				rsSave.Set_Fields("CheckLogo", fecherFoundation.Strings.Trim(strLogoFile));
				if (Conversion.Val(txtEspPort.Text) > 0)
				{
					rsSave.Set_Fields("espport", FCConvert.ToString(Conversion.Val(txtEspPort.Text)));
				}
				else
				{
					rsSave.Set_Fields("espport", 22);
				}
				rsSave.Set_Fields("ESPOrganization", FCConvert.ToString(Conversion.Val(txtEspOrg.Text)));
				rsSave.Set_Fields("ESPHost", txtEspHost.Text);
				rsSave.Set_Fields("ESPUser", txtESPUser.Text);
				rsSave.Set_Fields("espserverdatadir", txtESPDataPath.Text);
				rsSave.Set_Fields("espservercheckdir", txtESPCheckPath.Text);
				rsSave.Set_Fields("espserverw2dir", txtESPW2Path.Text);
				rsSave.Set_Fields("espserverexportdir", txtESPExportPath.Text);
				if (FCConvert.ToString(txtESPPassword.Tag) != txtESPPassword.Text)
				{
					if (txtESPPassword.Text != txtESPPasswordConfirm.Text)
					{
						MessageBox.Show("The ESHP password you entered does not match the confirmation password.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveData = false;
						return SaveData;
					}
					rsSave.Set_Fields("esppassword", txtESPPassword.Text);
				}
				rsSave.Update();
				SaveListBox(ref lstRetirement);
				SaveListBox(ref lstMSRS);
				SaveListBox(ref lstPayback);
				SaveListBox(ref lstMatchesBox1);
				boolReturn = boolReturn && SaveICMA();
				clsHistoryClass.Compare();
				if (boolReturn)
				{
					MessageBox.Show("Records saved successfully.", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Some records did not save", "Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				intDataChanged = 0;
				SaveData = boolReturn;
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void mnuViewW2Types_Click(object sender, System.EventArgs e)
		{
			int x;
			fraW2s.Visible = true;
            //Support.ZOrder(fraW2s, 0);
            framRetirement.Visible = false;
            framCheckOptions.Visible = false;
			framPayrollDefaults.Visible = false;
            framESHP.Visible = false;
            mnuEditPrint.Text = "Edit Payroll Defaults";
			cmdEditPrint.Text = "Edit Payroll Defaults";
			if (cboW2CheckType.SelectedIndex >= 0)
			{
				// if a w2 type is chosen, set the view form to that type
				// this assumes the radio buttons are always in the same order as the
				// combo box
                if (cmbW2Type.SelectedIndex >= 0 && cmbW2Type.ItemData(cmbW2Type.SelectedIndex) == cboW2CheckType.ItemData(cboW2CheckType.SelectedIndex))
				//if (cmbW2Type.SelectedIndex == cboW2CheckType.ItemData(cboW2CheckType.SelectedIndex))
				{
					// if its already on it, it won't run the click event code
					// so run that code ourselves
					// Image1.Picture = ImageList1.ListImages(cboW2CheckType.ItemData(cboW2CheckType.ListIndex) + 1).Picture
					switch (cboW2CheckType.ItemData(cboW2CheckType.SelectedIndex))
					{
						case 0:
							{
								Image1.Image = ImageList1.Images[2 - 1];
								break;
							}
						case 1:
							{
								Image1.Image = ImageList1.Images[3 - 1];
								break;
							}
					}
					//end switch
				}
				else
				{					
                    SetW2ImageType(cboW2CheckType.ItemData(cboW2CheckType.ListIndex));
                }
                ResizePic();
            }
		}

        private void SetW2ImageType(int w2Type)
        {
            for (int x = 0; x < cmbW2Type.ListCount; x++)
            {
                if (cmbW2Type.ItemData(x) == w2Type)
                {
                    cmbW2Type.SelectedIndex = x;
                }
            }
        }
		private void optW2Type_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// Image1.Picture = ImageList1.ListImages(Index + 1).Picture
			switch (Index)
			{
				case 0:
                    Image1.Image = ImageList1.Images[2 - 1];
                    break;
                case 1:
                    Image1.Image = ImageList1.Images[3 - 1];
                    break;
    //                {
				//		Image1.Image = ImageList1.Images[2 - 1];
				//		break;
				//	}
				//case 2:
				//	{
				//		Image1.Image = ImageList1.Images[0];
				//		break;
				//	}
				//case 3:
				//	{
				//		Image1.Image = ImageList1.Images[4 - 1];
				//		break;
				//	}
				//case 4:
				//	{
				//		Image1.Image = ImageList1.Images[3 - 1];
				//		break;
				//	}
				default:
					{
						Image1.Image = ImageList1.Images[ 1];
						break;
					}
			}
			//end switch
			ResizePic();
		}

		private void optW2Type_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbW2Type.SelectedIndex;
			optW2Type_CheckedChanged(index, sender, e);
		}

		private void ResizePic()
		{
            double dblRatio = 2.0d;
            if (Image1.Image != null)
            {
                this.Image1.Width = FCConvert.ToInt32(this.Image1.Image.Width * dblRatio);
                this.Image1.Height = FCConvert.ToInt32(this.Image1.Image.Height * dblRatio);
                this.fraW2s.Width = this.Image1.Width + 40;
                this.fraW2s.Height = this.Image1.Height + 100;
            }           
		}

        private void SetupcboTypeChecks()
        {
            cboTypeChecks.Clear();
            cboTypeChecks.AddItem("Laser");
            cboTypeChecks.ItemData(cboTypeChecks.NewIndex, 1);
            cboTypeChecks.AddItem("Custom");
            cboTypeChecks.ItemData(cboTypeChecks.NewIndex, 2);
        }


        private void SetupcboW2CheckType()
		{
			cboW2CheckType.Clear();
			cboW2CheckType.AddItem("Laser");
			cboW2CheckType.ItemData(cboW2CheckType.NewIndex, modGlobalVariables.CNSTW2TYPELASER);
			cboW2CheckType.AddItem("Laser 4 Up");
			cboW2CheckType.ItemData(cboW2CheckType.NewIndex, modGlobalVariables.CNSTW2TYPELASER2X2);
		}

        private void SetupcmbW2Type()
        {
            cmbW2Type.Clear();
            cmbW2Type.AddItem("Laser Form 5201");
            cmbW2Type.ItemData(cmbW2Type.NewIndex, modGlobalVariables.CNSTW2TYPELASER);
            cmbW2Type.AddItem("Laser 4 Up Form 5205");
            cmbW2Type.ItemData(cmbW2Type.NewIndex, modGlobalVariables.CNSTW2TYPELASER2X2);
        }

		private void SetupcmbTrackLastCheckNumber()
		{
			cmbTrackLastCheckNumber.Clear();
			cmbTrackLastCheckNumber.AddItem("No");
			cmbTrackLastCheckNumber.ItemData(0, 0);
			cmbTrackLastCheckNumber.AddItem("By Bank");
			cmbTrackLastCheckNumber.ItemData(1, 1);
			cmbTrackLastCheckNumber.AddItem("By Type");
			cmbTrackLastCheckNumber.ItemData(2, 2);
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboPrintSequence";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Check Print Sequence";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboReportSequence";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Report Sequence";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboDataEntry";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Data Entry Sequence";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboTypeChecks";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type of Checks";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboW2CheckType";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "W2 Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkBankDD";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Direct Deposit Checks to Bank";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkNoMSRS";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Location does NOT use MSRS";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkWeeklyPR";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Weekly P/R with Bi-Weekly";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkMMAUnemployment";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "MMA Unemployment";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkElectronicC1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Electronic 941/C1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkNegBalances";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Check for Neg Pay Cat Balances";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkSignature";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Use Electronic Signature";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkPrintSeparate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Print direct deposit and EFT checks separately";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboEmployerIDPrefix";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Employer ID Prefix";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkSkipPages";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Skip pages between deductions";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkACHClearingHouse";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Automated Clearing House (ACH)";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkBalanced";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Balanced ACH File";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkIgnorePrinterFonts";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Do Not Use Printer Fonts";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkDefaultChecks";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Checks use default printer";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkPProcessReports";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Payroll Process Reports use default printer";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtCheckLineAdjustment";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Checks Laser Printer Line Adjustment Vertical";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtW2Adjustment";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "W2 Laser Printer Line Adjustment Vertical";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtW2Horizontal";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "W2 Laser Printer Line Adjustment Horizontal";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
           
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cmbCheckImage";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Check Image";
            intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromListBox()
		{
			int intItems;
			for (intItems = 0; intItems <= lstRetirement.Items.Count - 1; intItems++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "lstRetirement";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ListBox;
				clsControlInfo[intTotalNumberOfControls].ListIndex = intItems;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Retirement Cont Deductions - " + lstRetirement.Items[intItems].Text;
				intTotalNumberOfControls += 1;
			}
			for (intItems = 0; intItems <= lstMSRS.Items.Count - 1; intItems++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "lstMSRS";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ListBox;
				clsControlInfo[intTotalNumberOfControls].ListIndex = intItems;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "MSRS Life Ins Deductions - " + lstMSRS.Items[intItems].Text;
				intTotalNumberOfControls += 1;
			}
			for (intItems = 0; intItems <= lstPayback.Items.Count - 1; intItems++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "lstPayback";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ListBox;
				clsControlInfo[intTotalNumberOfControls].ListIndex = intItems;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Payback Ins Deductions - " + lstPayback.Items[intItems].Text;
				intTotalNumberOfControls += 1;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			for (intRows = 1; intRows <= (Grid.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (Grid.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "Grid";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + Grid.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
		}

		private void txtCode1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Back)
			{
			}
			else if (KeyAscii >= Keys.A && KeyAscii <= Keys.Z)
			{
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
			}
			else if (KeyAscii == Keys.Space)
			{
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCode2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Back)
			{
			}
			else if (KeyAscii >= Keys.A && KeyAscii <= Keys.Z)
			{
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
			}
			else if (KeyAscii == Keys.Space)
			{
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		
		//FC:FINAL:DDU:#i2406 - changed upload implementation with Upload control and attached event
		public void fileUpload_Uploaded(object sender, UploadedEventArgs e)
		{
			if (e.Files != null && e.Files.Count > 0)
			{
				System.Web.HttpPostedFile file = e.Files[0];
				string path = FCFileSystem.Statics.UserDataFolder;
				if (!Directory.Exists(path))
				{
					Directory.CreateDirectory(path);
				}
				path = Path.Combine(path, file.FileName);
				if (path != string.Empty)
				{
					imgLogo.Image = FCUtils.LoadPicture(path);
				}
				else
				{
					imgLogo.Image = null;
				}
				using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
				{
					file.InputStream.CopyTo(fileStream);
					file.InputStream.Close();
				}
			}
		}
	}
}
