﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptRichText.
	/// </summary>
	public partial class rptRichText : BaseSectionReport
	{
		public rptRichText()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptRichText InstancePtr
		{
			get
			{
				return (rptRichText)Sys.GetInstance(typeof(rptRichText));
			}
		}

		protected rptRichText _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRichText	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public void Init(string strText, string strDescription)
		{
			//FC:FINAL:DSE WordWrapping not working in RichTextBox
			//RichEdit1.Text = strText;
			RichEdit1.SetHtmlText(strText);
			this.Name = strDescription;
			frmReportViewer.InstancePtr.Init(this);
		}

		
	}
}
