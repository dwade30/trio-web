//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmGetSequence.
	/// </summary>
	partial class frmGetSequence
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraSequenceRange;
		public fecherFoundation.FCCheckBox chkSame;
		public fecherFoundation.FCComboBox cboStartSequence;
		public fecherFoundation.FCComboBox cboEndSequence;
		public fecherFoundation.FCLabel lblTo_3;
		public fecherFoundation.FCFrame fraSequenceSingleSelection;
		public fecherFoundation.FCComboBox cboSingleSequence;
		public fecherFoundation.FCComboBox cboSequenceRange;
		public fecherFoundation.FCFrame fraBLS;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboReportNumber;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.fraSequenceRange = new fecherFoundation.FCFrame();
            this.fraSequenceSingleSelection = new fecherFoundation.FCFrame();
            this.cboSingleSequence = new fecherFoundation.FCComboBox();
            this.chkSame = new fecherFoundation.FCCheckBox();
            this.fraSequenceRangeSelection = new fecherFoundation.FCFrame();
            this.cboStartSequence = new fecherFoundation.FCComboBox();
            this.cboEndSequence = new fecherFoundation.FCComboBox();
            this.lblTo_3 = new fecherFoundation.FCLabel();
            this.cboSequenceRange = new fecherFoundation.FCComboBox();
            this.fraBLS = new fecherFoundation.FCFrame();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboReportNumber = new fecherFoundation.FCComboBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSequenceRange)).BeginInit();
            this.fraSequenceRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSequenceSingleSelection)).BeginInit();
            this.fraSequenceSingleSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSequenceRangeSelection)).BeginInit();
            this.fraSequenceRangeSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBLS)).BeginInit();
            this.fraBLS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 410);
            this.BottomPanel.Size = new System.Drawing.Size(514, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSequenceRange);
            this.ClientArea.Controls.Add(this.fraBLS);
            this.ClientArea.Size = new System.Drawing.Size(514, 350);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(514, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(198, 30);
            this.HeaderText.Text = "Select Sequence";
            // 
            // fraSequenceRange
            // 
            this.fraSequenceRange.AppearanceKey = "groupBoxLeftBorder";
            this.fraSequenceRange.Controls.Add(this.fraSequenceSingleSelection);
            this.fraSequenceRange.Controls.Add(this.chkSame);
            this.fraSequenceRange.Controls.Add(this.fraSequenceRangeSelection);
            this.fraSequenceRange.Controls.Add(this.cboSequenceRange);
            this.fraSequenceRange.Location = new System.Drawing.Point(30, 30);
            this.fraSequenceRange.Name = "fraSequenceRange";
            this.fraSequenceRange.Size = new System.Drawing.Size(454, 187);
            this.fraSequenceRange.TabIndex = 3;
            this.fraSequenceRange.Text = "Sequence Range";
            // 
            // fraSequenceSingleSelection
            // 
            this.fraSequenceSingleSelection.Controls.Add(this.cboSingleSequence);
            this.fraSequenceSingleSelection.Location = new System.Drawing.Point(20, 90);
            this.fraSequenceSingleSelection.Name = "fraSequenceSingleSelection";
            this.fraSequenceSingleSelection.Size = new System.Drawing.Size(191, 90);
            this.fraSequenceSingleSelection.TabIndex = 5;
            this.fraSequenceSingleSelection.Text = "Select Sequence ";
            this.fraSequenceSingleSelection.Visible = false;
            // 
            // cboSingleSequence
            // 
            this.cboSingleSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleSequence.Location = new System.Drawing.Point(20, 30);
            this.cboSingleSequence.Name = "cboSingleSequence";
            this.cboSingleSequence.Size = new System.Drawing.Size(150, 40);
            this.cboSingleSequence.TabIndex = 6;
            // 
            // chkSame
            // 
            this.chkSame.Location = new System.Drawing.Point(20, 200);
            this.chkSame.Name = "chkSame";
            this.chkSame.Size = new System.Drawing.Size(435, 27);
            this.chkSame.TabIndex = 11;
            this.chkSame.Text = "Make Payment Amounts same as WithHolding Amounts";
            this.chkSame.Visible = false;
            // 
            // fraSequenceRangeSelection
            // 
            this.fraSequenceRangeSelection.Controls.Add(this.cboStartSequence);
            this.fraSequenceRangeSelection.Controls.Add(this.cboEndSequence);
            this.fraSequenceRangeSelection.Controls.Add(this.lblTo_3);
            this.fraSequenceRangeSelection.Location = new System.Drawing.Point(20, 90);
            this.fraSequenceRangeSelection.Name = "fraSequenceRangeSelection";
            this.fraSequenceRangeSelection.Size = new System.Drawing.Size(294, 90);
            this.fraSequenceRangeSelection.TabIndex = 7;
            this.fraSequenceRangeSelection.Text = "Select Sequence Range";
            this.fraSequenceRangeSelection.Visible = false;
            // 
            // cboStartSequence
            // 
            this.cboStartSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cboStartSequence.Location = new System.Drawing.Point(20, 30);
            this.cboStartSequence.Name = "cboStartSequence";
            this.cboStartSequence.Size = new System.Drawing.Size(100, 40);
            this.cboStartSequence.TabIndex = 9;
            // 
            // cboEndSequence
            // 
            this.cboEndSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndSequence.Location = new System.Drawing.Point(175, 30);
            this.cboEndSequence.Name = "cboEndSequence";
            this.cboEndSequence.Size = new System.Drawing.Size(100, 40);
            this.cboEndSequence.TabIndex = 8;
            // 
            // lblTo_3
            // 
            this.lblTo_3.Location = new System.Drawing.Point(137, 44);
            this.lblTo_3.Name = "lblTo_3";
            this.lblTo_3.Size = new System.Drawing.Size(23, 16);
            this.lblTo_3.TabIndex = 10;
            this.lblTo_3.Text = "TO";
            this.lblTo_3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cboSequenceRange
            // 
            this.cboSequenceRange.BackColor = System.Drawing.SystemColors.Window;
            this.cboSequenceRange.Items.AddRange(new object[] {
            "All Sequences",
            "Single Sequence",
            "Range of Sequences"});
            this.cboSequenceRange.Location = new System.Drawing.Point(20, 30);
            this.cboSequenceRange.Name = "cboSequenceRange";
            this.cboSequenceRange.Size = new System.Drawing.Size(295, 40);
            this.cboSequenceRange.TabIndex = 4;
            this.cboSequenceRange.SelectedIndexChanged += new System.EventHandler(this.cboSequenceRange_SelectedIndexChanged);
            // 
            // fraBLS
            // 
            this.fraBLS.AppearanceKey = "groupBoxLeftBorder";
            this.fraBLS.Controls.Add(this.Frame1);
            this.fraBLS.FormatCaption = false;
            this.fraBLS.Location = new System.Drawing.Point(30, 227);
            this.fraBLS.Name = "fraBLS";
            this.fraBLS.Size = new System.Drawing.Size(321, 120);
            this.fraBLS.TabIndex = 4;
            this.fraBLS.Text = "BLS 3020 Multiple Worksite Report ";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cboReportNumber);
            this.Frame1.Location = new System.Drawing.Point(20, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(295, 90);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Report Parameter";
            // 
            // cboReportNumber
            // 
            this.cboReportNumber.BackColor = System.Drawing.SystemColors.Window;
            this.cboReportNumber.Location = new System.Drawing.Point(20, 30);
            this.cboReportNumber.Name = "cboReportNumber";
            this.cboReportNumber.Size = new System.Drawing.Size(255, 40);
            this.cboReportNumber.TabIndex = 2;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(169, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmGetSequence
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(514, 543);
            this.FillColor = 0;
            this.Name = "frmGetSequence";
            this.Text = "Select Sequence";
            this.Load += new System.EventHandler(this.frmGetSequence_Load);
            this.Activated += new System.EventHandler(this.frmGetSequence_Activated);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSequenceRange)).EndInit();
            this.fraSequenceRange.ResumeLayout(false);
            this.fraSequenceRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSequenceSingleSelection)).EndInit();
            this.fraSequenceSingleSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSequenceRangeSelection)).EndInit();
            this.fraSequenceRangeSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraBLS)).EndInit();
            this.fraBLS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		public FCFrame fraSequenceRangeSelection;
	}
}