﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPrintProcessVerify.
	/// </summary>
	public partial class rptPrintProcessVerify : BaseSectionReport
	{
		public rptPrintProcessVerify()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Pay Totals Verify / Accept";
			if (_InstancePtr == null)
				_InstancePtr = this;
			vsTotals = new FCGrid();
			vsPayCatGrid = new FCGrid();
			vsDeductionGrid = new FCGrid();
			vsMatchGrid = new FCGrid();
		}

		public static rptPrintProcessVerify InstancePtr
		{
			get
			{
				return (rptPrintProcessVerify)Sys.GetInstance(typeof(rptPrintProcessVerify));
			}
		}

		protected rptPrintProcessVerify _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				vsCheckSummary?.Dispose();
				vsDeductionGrid?.Dispose();
                vsPayCatGrid?.Dispose();
				vsDeductionGrid?.Dispose();
				vsMatchGrid?.Dispose();
                vsDeductionGrid = null;
                vsCheckSummary = null;
                vsPayCatGrid = null;
                vsDeductionGrid = null;
                vsMatchGrid = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrintProcessVerify	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		
		private int intCounter;
		private int intDataChanged;
		private double dblDeductionTotal;
		private int lngPeriodNumber;
		private int lngJournalNumber;
		FCGrid vsTotals;
		FCGrid vsPayCatGrid;
		FCGrid vsDeductionGrid;
		FCGrid vsMatchGrid;
        private FCGrid vsCheckSummary;
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnFirstRecord = true;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblPayDate.Text = "Pay Date: " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy");
			SetPayCatGridProperties();
			SetDeductionGridProperties();
			SetMatchGridProperties();
			SetTotalsGridProperties();
			vsDeductionGrid.Cols = frmProcessVerify.InstancePtr.vsDeductionGrid.Cols;
			vsDeductionGrid.ColWidth(0, FCConvert.ToInt32(vsDeductionGrid.Width * 0.3));
			vsDeductionGrid.ColWidth(1, FCConvert.ToInt32(vsDeductionGrid.Width * 0.21));
			vsDeductionGrid.ColWidth(2, FCConvert.ToInt32(vsDeductionGrid.Width * 0.21));
			vsDeductionGrid.ColWidth(3, FCConvert.ToInt32(vsDeductionGrid.Width * 0.21));
			vsMatchGrid.Cols = frmProcessVerify.InstancePtr.vsMatchGrid.Cols;
			vsMatchGrid.ColWidth(0, FCConvert.ToInt32(vsMatchGrid.Width * 0.3));
			vsMatchGrid.ColWidth(1, FCConvert.ToInt32(vsMatchGrid.Width * 0.21));
			vsMatchGrid.ColWidth(2, FCConvert.ToInt32(vsMatchGrid.Width * 0.21));
			vsMatchGrid.ColWidth(3, FCConvert.ToInt32(vsMatchGrid.Width * 0.21));
			vsPayCatGrid.Cols = frmProcessVerify.InstancePtr.vsPayCatGrid.Cols;
			vsPayCatGrid.ColWidth(0, FCConvert.ToInt32(vsPayCatGrid.Width * 0.5));
			vsPayCatGrid.ColWidth(1, FCConvert.ToInt32(vsPayCatGrid.Width * 0.22));
			vsPayCatGrid.ColWidth(2, FCConvert.ToInt32(vsPayCatGrid.Width * 0.22));
			vsTotals.Cols = frmProcessVerify.InstancePtr.vsTotals.Cols;
			vsTotals.ColWidth(0, FCConvert.ToInt32(vsTotals.Width * 0.2));
			vsTotals.ColWidth(1, FCConvert.ToInt32(vsTotals.Width * 0.26));
			vsTotals.ColWidth(2, FCConvert.ToInt32(vsTotals.Width * 0.02));
			vsTotals.ColWidth(3, FCConvert.ToInt32(vsTotals.Width * 0.21));
			vsTotals.ColWidth(4, FCConvert.ToInt32(vsTotals.Width * 0.26));
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			LoadPayCatGrid();
			LoadDeductionGrid();
			LoadMatchGrid();
			LoadTotalsGrid();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void SetTotalsGridProperties()
		{
			vsTotals.Cols = frmProcessVerify.InstancePtr.vsTotals.Cols;
			vsTotals.Rows = frmProcessVerify.InstancePtr.vsTotals.Rows;
			vsTotals.Height = frmProcessVerify.InstancePtr.vsTotals.HeightOriginal;
			vsTotals.MergeRow(0, true);
			vsTotals.MergeRow(7, true);
			vsTotals.MergeRow(8, true);
			vsTotals.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			vsTotals.FixedCols = frmProcessVerify.InstancePtr.vsTotals.Cols;
		}

		private void LoadTotalsGrid()
		{
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			for (intRow = 0; intRow <= (frmProcessVerify.InstancePtr.vsTotals.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmProcessVerify.InstancePtr.vsTotals.Cols - 1); intCol++)
				{
					vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol, frmProcessVerify.InstancePtr.vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol));
					vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol, frmProcessVerify.InstancePtr.vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol));
					vsTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol, frmProcessVerify.InstancePtr.vsTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol));
					vsTotals.TextMatrix(intRow, intCol, frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intRow, intCol));
				}
			}
			if (vsPayCatGrid.Height > vsDeductionGrid.Height)
			{
				vsTotals.Top = vsPayCatGrid.Height + FCConvert.ToInt32(500 / 1440F);
			}
			else
			{
				vsTotals.Top = vsDeductionGrid.Height + FCConvert.ToInt32(500 / 1440F);
			}
			//vsTotals.Height = vsTotals.Rows * vsTotals.RowHeight(0);
		}

		private void SetPayCatGridProperties()
		{
			vsPayCatGrid.Cols = frmProcessVerify.InstancePtr.vsPayCatGrid.Cols;
			vsPayCatGrid.Rows = frmProcessVerify.InstancePtr.vsPayCatGrid.Rows;
			vsPayCatGrid.Height = frmProcessVerify.InstancePtr.vsPayCatGrid.HeightOriginal;
			vsPayCatGrid.ColFormat(1, "0.00");
			vsPayCatGrid.ColFormat(2, "Currency");
			vsPayCatGrid.FixedCols = frmProcessVerify.InstancePtr.vsPayCatGrid.Cols;
		}

		private void SetDeductionGridProperties()
		{
			vsDeductionGrid.Cols = frmProcessVerify.InstancePtr.vsDeductionGrid.Cols;
			vsDeductionGrid.Rows = frmProcessVerify.InstancePtr.vsDeductionGrid.Rows;
			vsDeductionGrid.Height = frmProcessVerify.InstancePtr.vsDeductionGrid.HeightOriginal;
			vsDeductionGrid.ColFormat(1, "Currency");
			vsDeductionGrid.ColFormat(2, "Currency");
			vsDeductionGrid.ColFormat(3, "Currency");
			vsDeductionGrid.FixedCols = frmProcessVerify.InstancePtr.vsDeductionGrid.Cols;
		}

		private void SetMatchGridProperties()
		{
			vsMatchGrid.Cols = frmProcessVerify.InstancePtr.vsMatchGrid.Cols;
			vsMatchGrid.Rows = frmProcessVerify.InstancePtr.vsMatchGrid.Rows;
			vsMatchGrid.Height = frmProcessVerify.InstancePtr.vsMatchGrid.HeightOriginal;
			vsMatchGrid.ColFormat(1, "Currency");
			vsMatchGrid.ColFormat(2, "Currency");
			vsMatchGrid.ColFormat(3, "Currency");
			vsMatchGrid.FixedCols = frmProcessVerify.InstancePtr.vsMatchGrid.Cols;
		}

		private void LoadPayCatGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadPayCatGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
				int intCol;
				// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
				int intRow;
				for (intRow = 0; intRow <= (frmProcessVerify.InstancePtr.vsPayCatGrid.Rows - 1); intRow++)
				{
					for (intCol = 0; intCol <= (frmProcessVerify.InstancePtr.vsPayCatGrid.Cols - 1); intCol++)
					{
						vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol, frmProcessVerify.InstancePtr.vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol));
						vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol, frmProcessVerify.InstancePtr.vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol));
						vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol, frmProcessVerify.InstancePtr.vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol));
						vsPayCatGrid.TextMatrix(intRow, intCol, frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intRow, intCol));
					}
				}
				//vsPayCatGrid.Height = vsPayCatGrid.Rows * frmProcessVerify.InstancePtr.vsPayCatGrid.RowHeight(0);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadDeductionGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadDistributionGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
				int intCol;
				// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
				int intRow;
				for (intRow = 0; intRow <= (frmProcessVerify.InstancePtr.vsDeductionGrid.Rows - 1); intRow++)
				{
					for (intCol = 0; intCol <= (frmProcessVerify.InstancePtr.vsDeductionGrid.Cols - 1); intCol++)
					{
						vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol, frmProcessVerify.InstancePtr.vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol));
						vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol, frmProcessVerify.InstancePtr.vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol));
						vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol, frmProcessVerify.InstancePtr.vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol));
						vsDeductionGrid.TextMatrix(intRow, intCol, frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intRow, intCol));
					}
				}
				//vsDeductionGrid.Height = vsDeductionGrid.Rows * vsDeductionGrid.RowHeight(0);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadMatchGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadDistributionGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
				int intCol;
				// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
				int intRow;
				for (intRow = 0; intRow <= (frmProcessVerify.InstancePtr.vsMatchGrid.Rows - 1); intRow++)
				{
					for (intCol = 0; intCol <= (frmProcessVerify.InstancePtr.vsMatchGrid.Cols - 1); intCol++)
					{
						vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol, frmProcessVerify.InstancePtr.vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol));
						vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol, frmProcessVerify.InstancePtr.vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol));
						vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol, frmProcessVerify.InstancePtr.vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol));
						vsMatchGrid.TextMatrix(intRow, intCol, frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intRow, intCol));
					}
				}
				if (vsPayCatGrid.Height > vsDeductionGrid.Height)
				{
					vsMatchGrid.Top = vsPayCatGrid.Height + FCConvert.ToInt32(500 / 1440F);
				}
				else
				{
					vsMatchGrid.Top = vsDeductionGrid.Height + FCConvert.ToInt32(500 / 1440F);
				}
				//vsMatchGrid.Height = vsMatchGrid.Rows * vsMatchGrid.RowHeight(0);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

	
	}
}
