//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmMiscUpdate.
	/// </summary>
	partial class frmMiscUpdate
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public FCGrid GridDelete;
		public fecherFoundation.FCTextBox txtFullName;
		public fecherFoundation.FCTextBox txtSSN;
		public fecherFoundation.FCTextBox txtDateHire;
		public fecherFoundation.FCFrame fraBanks;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtRateOfPayFactor;
		public fecherFoundation.FCComboBox cboRateOfPay;
		public fecherFoundation.FCTextBox txtMonthlyHDFactor;
		public fecherFoundation.FCComboBox cboMonthlyHD;
		public fecherFoundation.FCCheckBox chkInclude;
		public fecherFoundation.FCTextBox txtExcess;
		public fecherFoundation.FCTextBox txtLifeInsLevel;
		public fecherFoundation.FCComboBox cmbPositionCategory;
		public FCGrid gridLifeInsuranceCode;
		public FCGrid gridPlanCode;
		public FCGrid gridLifeInsScheduleCode;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_17;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_18;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboUnemployment;
		public fecherFoundation.FCTextBox txtNatureCode;
		public fecherFoundation.FCLabel Label1_15;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCTextBox txtWorkWeekPerYear;
		public fecherFoundation.FCTextBox txtFedCompAmount;
		public fecherFoundation.FCTextBox txtFederalComp;
		public fecherFoundation.FCTextBox txtPayRateCode;
		public fecherFoundation.FCTextBox txtPositionCode;
		public fecherFoundation.FCTextBox txtStatusCode;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel Label1_16;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public FCGrid Grid;
		public fecherFoundation.FCLabel lblFullName;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuAddLine;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteLine;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.GridDelete = new fecherFoundation.FCGrid();
            this.txtFullName = new fecherFoundation.FCTextBox();
            this.txtSSN = new fecherFoundation.FCTextBox();
            this.txtDateHire = new fecherFoundation.FCTextBox();
            this.fraBanks = new fecherFoundation.FCFrame();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtRateOfPayFactor = new fecherFoundation.FCTextBox();
            this.cboRateOfPay = new fecherFoundation.FCComboBox();
            this.txtMonthlyHDFactor = new fecherFoundation.FCTextBox();
            this.cboMonthlyHD = new fecherFoundation.FCComboBox();
            this.chkInclude = new fecherFoundation.FCCheckBox();
            this.txtExcess = new fecherFoundation.FCTextBox();
            this.txtLifeInsLevel = new fecherFoundation.FCTextBox();
            this.cmbPositionCategory = new fecherFoundation.FCComboBox();
            this.gridLifeInsuranceCode = new fecherFoundation.FCGrid();
            this.gridPlanCode = new fecherFoundation.FCGrid();
            this.gridLifeInsScheduleCode = new fecherFoundation.FCGrid();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_17 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_18 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboUnemployment = new fecherFoundation.FCComboBox();
            this.txtNatureCode = new fecherFoundation.FCTextBox();
            this.Label1_15 = new fecherFoundation.FCLabel();
            this.Label1_14 = new fecherFoundation.FCLabel();
            this.txtWorkWeekPerYear = new fecherFoundation.FCTextBox();
            this.txtFedCompAmount = new fecherFoundation.FCTextBox();
            this.txtFederalComp = new fecherFoundation.FCTextBox();
            this.txtPayRateCode = new fecherFoundation.FCTextBox();
            this.txtPositionCode = new fecherFoundation.FCTextBox();
            this.txtStatusCode = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.Label1_16 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Grid = new fecherFoundation.FCGrid();
            this.lblFullName = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSelectEmployee = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBanks)).BeginInit();
            this.fraBanks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLifeInsuranceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlanCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLifeInsScheduleCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 937);
            this.BottomPanel.Size = new System.Drawing.Size(918, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridDelete);
            this.ClientArea.Controls.Add(this.txtFullName);
            this.ClientArea.Controls.Add(this.txtSSN);
            this.ClientArea.Controls.Add(this.txtDateHire);
            this.ClientArea.Controls.Add(this.fraBanks);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.lblFullName);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Size = new System.Drawing.Size(938, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblFullName, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraBanks, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDateHire, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSSN, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtFullName, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridDelete, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdAdd);
            this.TopPanel.Controls.Add(this.cmdSelectEmployee);
            this.TopPanel.Size = new System.Drawing.Size(938, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelectEmployee, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(380, 30);
            this.HeaderText.Text = "MainePERS / Unemployment Info";
            // 
            // GridDelete
            // 
            this.GridDelete.Cols = 1;
            this.GridDelete.ColumnHeadersVisible = false;
            this.GridDelete.FixedCols = 0;
            this.GridDelete.FixedRows = 0;
            this.GridDelete.Location = new System.Drawing.Point(413, 24);
            this.GridDelete.Name = "GridDelete";
            this.GridDelete.RowHeadersVisible = false;
            this.GridDelete.Rows = 0;
            this.GridDelete.Size = new System.Drawing.Size(9, 10);
            this.GridDelete.TabIndex = 4;
            this.GridDelete.Visible = false;
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(128, 30);
            this.txtFullName.LockedOriginal = true;
            this.txtFullName.MaxLength = 25;
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.ReadOnly = true;
            this.txtFullName.Size = new System.Drawing.Size(210, 40);
            this.txtFullName.TabIndex = 1;
            this.txtFullName.TabStop = false;
            this.txtFullName.Tag = "KEEP";
            this.txtFullName.TextChanged += new System.EventHandler(this.txtFullName_TextChanged);
            // 
            // txtSSN
            // 
            this.txtSSN.Location = new System.Drawing.Point(424, 30);
            this.txtSSN.LockedOriginal = true;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.ReadOnly = true;
            this.txtSSN.Size = new System.Drawing.Size(197, 40);
            this.txtSSN.TabIndex = 3;
            this.txtSSN.TabStop = false;
            this.txtSSN.Tag = "KEEP";
            this.txtSSN.Text = "Text1";
            this.txtSSN.TextChanged += new System.EventHandler(this.txtSSN_TextChanged);
            // 
            // txtDateHire
            // 
            this.txtDateHire.Location = new System.Drawing.Point(745, 30);
            this.txtDateHire.LockedOriginal = true;
            this.txtDateHire.Name = "txtDateHire";
            this.txtDateHire.ReadOnly = true;
            this.txtDateHire.Size = new System.Drawing.Size(150, 40);
            this.txtDateHire.TabIndex = 6;
            this.txtDateHire.TabStop = false;
            this.txtDateHire.Tag = "KEEP";
            this.txtDateHire.Text = "Text1";
            this.txtDateHire.TextChanged += new System.EventHandler(this.txtDateHire_TextChanged);
            // 
            // fraBanks
            // 
            this.fraBanks.AppearanceKey = "groupBoxNoBorders";
            this.fraBanks.Controls.Add(this.Frame2);
            this.fraBanks.Controls.Add(this.Frame1);
            this.fraBanks.Controls.Add(this.txtWorkWeekPerYear);
            this.fraBanks.Controls.Add(this.txtFedCompAmount);
            this.fraBanks.Controls.Add(this.txtFederalComp);
            this.fraBanks.Controls.Add(this.txtPayRateCode);
            this.fraBanks.Controls.Add(this.txtPositionCode);
            this.fraBanks.Controls.Add(this.txtStatusCode);
            this.fraBanks.Controls.Add(this.lblDescription);
            this.fraBanks.Controls.Add(this.Label1_16);
            this.fraBanks.Controls.Add(this.Label1_12);
            this.fraBanks.Controls.Add(this.Label1_11);
            this.fraBanks.Controls.Add(this.Label1_10);
            this.fraBanks.Controls.Add(this.Label1_4);
            this.fraBanks.Controls.Add(this.Label1_3);
            this.fraBanks.Location = new System.Drawing.Point(10, 70);
            this.fraBanks.Name = "fraBanks";
            this.fraBanks.Size = new System.Drawing.Size(862, 598);
            this.fraBanks.TabIndex = 7;
            this.fraBanks.Tag = "Employee #";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtRateOfPayFactor);
            this.Frame2.Controls.Add(this.cboRateOfPay);
            this.Frame2.Controls.Add(this.txtMonthlyHDFactor);
            this.Frame2.Controls.Add(this.cboMonthlyHD);
            this.Frame2.Controls.Add(this.chkInclude);
            this.Frame2.Controls.Add(this.txtExcess);
            this.Frame2.Controls.Add(this.txtLifeInsLevel);
            this.Frame2.Controls.Add(this.cmbPositionCategory);
            this.Frame2.Controls.Add(this.gridLifeInsuranceCode);
            this.Frame2.Controls.Add(this.gridPlanCode);
            this.Frame2.Controls.Add(this.gridLifeInsScheduleCode);
            this.Frame2.Controls.Add(this.Label1_1);
            this.Frame2.Controls.Add(this.Label1_17);
            this.Frame2.Controls.Add(this.Label1_2);
            this.Frame2.Controls.Add(this.Label1_18);
            this.Frame2.Controls.Add(this.Label1_0);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_7);
            this.Frame2.Controls.Add(this.Label1_9);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Location = new System.Drawing.Point(20, 10);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(433, 583);
            this.Frame2.Text = "Mainepers";
            // 
            // txtRateOfPayFactor
            // 
            this.txtRateOfPayFactor.BackColor = System.Drawing.SystemColors.Window;
            this.txtRateOfPayFactor.Location = new System.Drawing.Point(198, 523);
            this.txtRateOfPayFactor.Name = "txtRateOfPayFactor";
            this.txtRateOfPayFactor.Size = new System.Drawing.Size(215, 40);
            this.txtRateOfPayFactor.TabIndex = 21;
            this.txtRateOfPayFactor.Enter += new System.EventHandler(this.txtRateOfPayFactor_Enter);
            this.txtRateOfPayFactor.TextChanged += new System.EventHandler(this.txtRateOfPayFactor_TextChanged);
            this.txtRateOfPayFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtRateOfPayFactor_Validating);
            this.txtRateOfPayFactor.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRateOfPayFactor_KeyPress);
            // 
            // cboRateOfPay
            // 
            this.cboRateOfPay.BackColor = System.Drawing.SystemColors.Window;
            this.cboRateOfPay.Location = new System.Drawing.Point(198, 473);
            this.cboRateOfPay.Name = "cboRateOfPay";
            this.cboRateOfPay.Size = new System.Drawing.Size(215, 40);
            this.cboRateOfPay.TabIndex = 19;
            this.cboRateOfPay.SelectedIndexChanged += new System.EventHandler(this.cboRateOfPay_SelectedIndexChanged);
            this.cboRateOfPay.DropDown += new System.EventHandler(this.cboRateOfPay_DropDown);
            this.cboRateOfPay.Enter += new System.EventHandler(this.cboRateOfPay_Enter);
            this.cboRateOfPay.Validating += new System.ComponentModel.CancelEventHandler(this.cboRateOfPay_Validating);
            // 
            // txtMonthlyHDFactor
            // 
            this.txtMonthlyHDFactor.BackColor = System.Drawing.SystemColors.Window;
            this.txtMonthlyHDFactor.Location = new System.Drawing.Point(198, 423);
            this.txtMonthlyHDFactor.Name = "txtMonthlyHDFactor";
            this.txtMonthlyHDFactor.Size = new System.Drawing.Size(215, 40);
            this.txtMonthlyHDFactor.TabIndex = 17;
            this.txtMonthlyHDFactor.Enter += new System.EventHandler(this.txtMonthlyHDFactor_Enter);
            this.txtMonthlyHDFactor.TextChanged += new System.EventHandler(this.txtMonthlyHDFactor_TextChanged);
            this.txtMonthlyHDFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthlyHDFactor_Validating);
            this.txtMonthlyHDFactor.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMonthlyHDFactor_KeyPress);
            // 
            // cboMonthlyHD
            // 
            this.cboMonthlyHD.BackColor = System.Drawing.SystemColors.Window;
            this.cboMonthlyHD.Location = new System.Drawing.Point(198, 373);
            this.cboMonthlyHD.Name = "cboMonthlyHD";
            this.cboMonthlyHD.Size = new System.Drawing.Size(215, 40);
            this.cboMonthlyHD.TabIndex = 15;
            this.cboMonthlyHD.SelectedIndexChanged += new System.EventHandler(this.cboMonthlyHD_SelectedIndexChanged);
            this.cboMonthlyHD.DropDown += new System.EventHandler(this.cboMonthlyHD_DropDown);
            this.cboMonthlyHD.Enter += new System.EventHandler(this.cboMonthlyHD_Enter);
            this.cboMonthlyHD.Validating += new System.ComponentModel.CancelEventHandler(this.cboMonthlyHD_Validating);
            // 
            // chkInclude
            // 
            this.chkInclude.Location = new System.Drawing.Point(198, 30);
            this.chkInclude.Name = "chkInclude";
            this.chkInclude.Size = new System.Drawing.Size(22, 23);
            this.chkInclude.TabIndex = 1;
            this.chkInclude.Tag = "KEEP";
            this.chkInclude.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            this.chkInclude.Enter += new System.EventHandler(this.chkInclude_Enter);
            // 
            // txtExcess
            // 
            this.txtExcess.BackColor = System.Drawing.SystemColors.Window;
            this.txtExcess.Location = new System.Drawing.Point(198, 171);
            this.txtExcess.MaxLength = 1;
            this.txtExcess.Name = "txtExcess";
            this.txtExcess.Size = new System.Drawing.Size(215, 40);
            this.txtExcess.TabIndex = 7;
            this.txtExcess.Enter += new System.EventHandler(this.txtExcess_Enter);
            this.txtExcess.TextChanged += new System.EventHandler(this.txtExcess_TextChanged);
            this.txtExcess.Validating += new System.ComponentModel.CancelEventHandler(this.txtExcess_Validating);
            // 
            // txtLifeInsLevel
            // 
            this.txtLifeInsLevel.BackColor = System.Drawing.SystemColors.Window;
            this.txtLifeInsLevel.Location = new System.Drawing.Point(198, 273);
            this.txtLifeInsLevel.Name = "txtLifeInsLevel";
            this.txtLifeInsLevel.Size = new System.Drawing.Size(215, 40);
            this.txtLifeInsLevel.TabIndex = 11;
            this.txtLifeInsLevel.Enter += new System.EventHandler(this.txtLifeInsLevel_Enter);
            this.txtLifeInsLevel.TextChanged += new System.EventHandler(this.txtLifeInsLevel_TextChanged);
            this.txtLifeInsLevel.Validating += new System.ComponentModel.CancelEventHandler(this.txtLifeInsLevel_Validating);
            // 
            // cmbPositionCategory
            // 
            this.cmbPositionCategory.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPositionCategory.Location = new System.Drawing.Point(198, 323);
            this.cmbPositionCategory.Name = "cmbPositionCategory";
            this.cmbPositionCategory.Size = new System.Drawing.Size(215, 40);
            this.cmbPositionCategory.TabIndex = 13;
            this.cmbPositionCategory.SelectedIndexChanged += new System.EventHandler(this.cmbPositionCategory_SelectedIndexChanged);
            this.cmbPositionCategory.Enter += new System.EventHandler(this.cmbPositionCategory_Enter);
            // 
            // gridLifeInsuranceCode
            // 
            this.gridLifeInsuranceCode.ColumnHeadersVisible = false;
            this.gridLifeInsuranceCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridLifeInsuranceCode.ExtendLastCol = true;
            this.gridLifeInsuranceCode.FixedCols = 0;
            this.gridLifeInsuranceCode.FixedRows = 0;
            this.gridLifeInsuranceCode.Location = new System.Drawing.Point(198, 67);
            this.gridLifeInsuranceCode.Name = "gridLifeInsuranceCode";
            this.gridLifeInsuranceCode.ReadOnly = false;
            this.gridLifeInsuranceCode.RowHeadersVisible = false;
            this.gridLifeInsuranceCode.Rows = 1;
            this.gridLifeInsuranceCode.Size = new System.Drawing.Size(215, 42);
            this.gridLifeInsuranceCode.TabIndex = 3;
            this.gridLifeInsuranceCode.Enter += new System.EventHandler(this.gridLifeInsuranceCode_Enter);
            // 
            // gridPlanCode
            // 
            this.gridPlanCode.ColumnHeadersVisible = false;
            this.gridPlanCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridPlanCode.ExtendLastCol = true;
            this.gridPlanCode.FixedCols = 0;
            this.gridPlanCode.FixedRows = 0;
            this.gridPlanCode.Location = new System.Drawing.Point(198, 119);
            this.gridPlanCode.Name = "gridPlanCode";
            this.gridPlanCode.ReadOnly = false;
            this.gridPlanCode.RowHeadersVisible = false;
            this.gridPlanCode.Rows = 1;
            this.gridPlanCode.Size = new System.Drawing.Size(215, 42);
            this.gridPlanCode.TabIndex = 5;
            this.gridPlanCode.Enter += new System.EventHandler(this.gridPlanCode_Enter);
            // 
            // gridLifeInsScheduleCode
            // 
            this.gridLifeInsScheduleCode.ColumnHeadersVisible = false;
            this.gridLifeInsScheduleCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridLifeInsScheduleCode.ExtendLastCol = true;
            this.gridLifeInsScheduleCode.FixedCols = 0;
            this.gridLifeInsScheduleCode.FixedRows = 0;
            this.gridLifeInsScheduleCode.Location = new System.Drawing.Point(198, 221);
            this.gridLifeInsScheduleCode.Name = "gridLifeInsScheduleCode";
            this.gridLifeInsScheduleCode.ReadOnly = false;
            this.gridLifeInsScheduleCode.RowHeadersVisible = false;
            this.gridLifeInsScheduleCode.Rows = 1;
            this.gridLifeInsScheduleCode.Size = new System.Drawing.Size(215, 42);
            this.gridLifeInsScheduleCode.TabIndex = 9;
            this.gridLifeInsScheduleCode.Enter += new System.EventHandler(this.gridLifeInsScheduleCode_Enter);
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(101, 537);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(64, 15);
            this.Label1_1.TabIndex = 20;
            this.Label1_1.Text = ":FACTOR";
            // 
            // Label1_17
            // 
            this.Label1_17.Location = new System.Drawing.Point(20, 487);
            this.Label1_17.Name = "Label1_17";
            this.Label1_17.Size = new System.Drawing.Size(135, 15);
            this.Label1_17.TabIndex = 18;
            this.Label1_17.Text = "RATE OF PAY: M OR D";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(101, 437);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(52, 15);
            this.Label1_2.TabIndex = 16;
            this.Label1_2.Text = ":FACTOR";
            // 
            // Label1_18
            // 
            this.Label1_18.Location = new System.Drawing.Point(20, 387);
            this.Label1_18.Name = "Label1_18";
            this.Label1_18.Size = new System.Drawing.Size(135, 15);
            this.Label1_18.TabIndex = 14;
            this.Label1_18.Text = "MONTHLY H/D: M OR D";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 81);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(84, 15);
            this.Label1_0.TabIndex = 2;
            this.Label1_0.Text = "LIFE INS CODE";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 133);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(100, 15);
            this.Label1_5.TabIndex = 4;
            this.Label1_5.Text = "PLAN CODE";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(20, 235);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(157, 15);
            this.Label1_6.TabIndex = 8;
            this.Label1_6.Text = "LIFE INS SCHEDULE CODE";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(20, 184);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(157, 15);
            this.Label1_7.TabIndex = 6;
            this.Label1_7.Text = "EXCESS / PURCHASE";
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(20, 287);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(157, 15);
            this.Label1_9.TabIndex = 10;
            this.Label1_9.Text = "LIFE INS LEVEL";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 37);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 15);
            this.Label2.Text = "INCLUDE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 337);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(130, 15);
            this.Label3.TabIndex = 12;
            this.Label3.Text = "POSITION CATEGORY";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cboUnemployment);
            this.Frame1.Controls.Add(this.txtNatureCode);
            this.Frame1.Controls.Add(this.Label1_15);
            this.Frame1.Controls.Add(this.Label1_14);
            this.Frame1.Location = new System.Drawing.Point(471, 453);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(261, 140);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "State Unemployment Info";
            // 
            // cboUnemployment
            // 
            this.cboUnemployment.BackColor = System.Drawing.SystemColors.Window;
            this.cboUnemployment.Items.AddRange(new object[] {
            "Y",
            "N",
            "S"});
            this.cboUnemployment.Location = new System.Drawing.Point(141, 30);
            this.cboUnemployment.Name = "cboUnemployment";
            this.cboUnemployment.Size = new System.Drawing.Size(100, 40);
            this.cboUnemployment.TabIndex = 1;
            this.cboUnemployment.SelectedIndexChanged += new System.EventHandler(this.cboUnemployment_SelectedIndexChanged);
            this.cboUnemployment.Enter += new System.EventHandler(this.cboUnemployment_Enter);
            this.cboUnemployment.Validating += new System.ComponentModel.CancelEventHandler(this.cboUnemployment_Validating);
            // 
            // txtNatureCode
            // 
            this.txtNatureCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtNatureCode.Location = new System.Drawing.Point(141, 80);
            this.txtNatureCode.Name = "txtNatureCode";
            this.txtNatureCode.Size = new System.Drawing.Size(100, 40);
            this.txtNatureCode.TabIndex = 3;
            this.txtNatureCode.Enter += new System.EventHandler(this.txtNatureCode_Enter);
            this.txtNatureCode.TextChanged += new System.EventHandler(this.txtNatureCode_TextChanged);
            this.txtNatureCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtNatureCode_Validating);
            // 
            // Label1_15
            // 
            this.Label1_15.Location = new System.Drawing.Point(20, 94);
            this.Label1_15.Name = "Label1_15";
            this.Label1_15.Size = new System.Drawing.Size(84, 17);
            this.Label1_15.TabIndex = 2;
            this.Label1_15.Text = "NATURE CODE";
            // 
            // Label1_14
            // 
            this.Label1_14.Location = new System.Drawing.Point(20, 44);
            this.Label1_14.Name = "Label1_14";
            this.Label1_14.Size = new System.Drawing.Size(92, 17);
            this.Label1_14.Text = "INCLUDE Y/N/S";
            // 
            // txtWorkWeekPerYear
            // 
            this.txtWorkWeekPerYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtWorkWeekPerYear.Location = new System.Drawing.Point(199, 765);
            this.txtWorkWeekPerYear.Name = "txtWorkWeekPerYear";
            this.txtWorkWeekPerYear.Size = new System.Drawing.Size(50, 40);
            this.txtWorkWeekPerYear.TabIndex = 8;
            this.txtWorkWeekPerYear.Enter += new System.EventHandler(this.txtWorkWeekPerYear_Enter);
            this.txtWorkWeekPerYear.TextChanged += new System.EventHandler(this.txtWorkWeekPerYear_TextChanged);
            this.txtWorkWeekPerYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtWorkWeekPerYear_Validating);
            // 
            // txtFedCompAmount
            // 
            this.txtFedCompAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtFedCompAmount.Location = new System.Drawing.Point(199, 715);
            this.txtFedCompAmount.Name = "txtFedCompAmount";
            this.txtFedCompAmount.Size = new System.Drawing.Size(50, 40);
            this.txtFedCompAmount.TabIndex = 6;
            this.txtFedCompAmount.Enter += new System.EventHandler(this.txtFedCompAmount_Enter);
            // 
            // txtFederalComp
            // 
            this.txtFederalComp.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalComp.Location = new System.Drawing.Point(416, 715);
            this.txtFederalComp.MaxLength = 1;
            this.txtFederalComp.Name = "txtFederalComp";
            this.txtFederalComp.Size = new System.Drawing.Size(50, 40);
            this.txtFederalComp.TabIndex = 12;
            this.txtFederalComp.Enter += new System.EventHandler(this.txtFederalComp_Enter);
            this.txtFederalComp.TextChanged += new System.EventHandler(this.txtFederalComp_TextChanged);
            this.txtFederalComp.Validating += new System.ComponentModel.CancelEventHandler(this.txtFederalComp_Validating);
            this.txtFederalComp.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalComp_KeyPress);
            // 
            // txtPayRateCode
            // 
            this.txtPayRateCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPayRateCode.Location = new System.Drawing.Point(199, 815);
            this.txtPayRateCode.Name = "txtPayRateCode";
            this.txtPayRateCode.Size = new System.Drawing.Size(50, 40);
            this.txtPayRateCode.TabIndex = 10;
            this.txtPayRateCode.Enter += new System.EventHandler(this.txtPayRateCode_Enter);
            this.txtPayRateCode.TextChanged += new System.EventHandler(this.txtPayRateCode_TextChanged);
            this.txtPayRateCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPayRateCode_KeyPress);
            // 
            // txtPositionCode
            // 
            this.txtPositionCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPositionCode.Location = new System.Drawing.Point(199, 665);
            this.txtPositionCode.Name = "txtPositionCode";
            this.txtPositionCode.Size = new System.Drawing.Size(50, 40);
            this.txtPositionCode.TabIndex = 4;
            this.txtPositionCode.Enter += new System.EventHandler(this.txtPositionCode_Enter);
            this.txtPositionCode.TextChanged += new System.EventHandler(this.txtPositionCode_TextChanged);
            this.txtPositionCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtPositionCode_Validating);
            // 
            // txtStatusCode
            // 
            this.txtStatusCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtStatusCode.Location = new System.Drawing.Point(199, 615);
            this.txtStatusCode.Name = "txtStatusCode";
            this.txtStatusCode.Size = new System.Drawing.Size(58, 40);
            this.txtStatusCode.TabIndex = 2;
            this.txtStatusCode.Enter += new System.EventHandler(this.txtStatusCode_Enter);
            this.txtStatusCode.TextChanged += new System.EventHandler(this.txtStatusCode_TextChanged);
            this.txtStatusCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtStatusCode_Validating);
            this.txtStatusCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStatusCode_KeyPress);
            // 
            // lblDescription
            // 
            this.lblDescription.BorderStyle = 1;
            this.lblDescription.Location = new System.Drawing.Point(473, 10);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(349, 183);
            this.lblDescription.TabIndex = 1;
            // 
            // Label1_16
            // 
            this.Label1_16.Location = new System.Drawing.Point(22, 779);
            this.Label1_16.Name = "Label1_16";
            this.Label1_16.Size = new System.Drawing.Size(150, 17);
            this.Label1_16.TabIndex = 7;
            this.Label1_16.Text = "WORK WEEKS PER YEAR";
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(22, 829);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(150, 17);
            this.Label1_12.TabIndex = 9;
            this.Label1_12.Text = "PAY RATE CODE H/D";
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(22, 729);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(150, 17);
            this.Label1_11.TabIndex = 5;
            this.Label1_11.Text = "FEDERAL COMP AMOUNT";
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(270, 729);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(120, 17);
            this.Label1_10.TabIndex = 11;
            this.Label1_10.Text = "FEDERAL COMP D/%";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(20, 679);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(150, 17);
            this.Label1_4.TabIndex = 3;
            this.Label1_4.Text = "POSITION CODE";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 629);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(150, 17);
            this.Label1_3.TabIndex = 1;
            this.Label1_3.Text = "STATUS CODE";
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 12;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.Location = new System.Drawing.Point(30, 674);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(880, 263);
            this.Grid.TabIndex = 8;
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            this.Grid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.Grid_MouseMoveEvent);
            // 
            // lblFullName
            // 
            this.lblFullName.Location = new System.Drawing.Point(30, 44);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(80, 15);
            this.lblFullName.Text = "FULL NAME";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(641, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(69, 15);
            this.Label5.TabIndex = 5;
            this.Label5.Text = "DATE HIRED";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(358, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(50, 15);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "SSN #";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuSelectEmployee,
            this.mnuSP1,
            this.mnuAddLine,
            this.mnuDeleteLine,
            this.mnuSepar4,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP3,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Enabled = false;
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear";
            this.mnuClear.Visible = false;
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = 1;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuAddLine
            // 
            this.mnuAddLine.Index = 3;
            this.mnuAddLine.Name = "mnuAddLine";
            this.mnuAddLine.Text = "Add Position Line";
            this.mnuAddLine.Click += new System.EventHandler(this.mnuAddLine_Click);
            // 
            // mnuDeleteLine
            // 
            this.mnuDeleteLine.Index = 4;
            this.mnuDeleteLine.Name = "mnuDeleteLine";
            this.mnuDeleteLine.Text = "Delete Position Line";
            this.mnuDeleteLine.Click += new System.EventHandler(this.mnuDeleteLine_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = 5;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            this.mnuSepar4.Visible = false;
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 6;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Visible = false;
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = 7;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Visible = false;
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 8;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 9;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                               ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 10;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                 ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 11;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 12;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSelectEmployee
            // 
            this.cmdSelectEmployee.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelectEmployee.Location = new System.Drawing.Point(506, 29);
            this.cmdSelectEmployee.Name = "cmdSelectEmployee";
            this.cmdSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelectEmployee.Size = new System.Drawing.Size(120, 24);
            this.cmdSelectEmployee.TabIndex = 1;
            this.cmdSelectEmployee.Text = "Select Employee";
            this.cmdSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(349, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdd.Location = new System.Drawing.Point(632, 29);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(128, 24);
            this.cmdAdd.TabIndex = 2;
            this.cmdAdd.Text = "Add Position Line";
            this.cmdAdd.Click += new System.EventHandler(this.mnuAddLine_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(766, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(144, 24);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete Position Line";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteLine_Click);
            // 
            // frmMiscUpdate
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(938, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmMiscUpdate";
            this.Text = "MainePERS / Unemployment Info";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmMiscUpdate_Load);
            this.Activated += new System.EventHandler(this.frmMiscUpdate_Activated);
            this.Resize += new System.EventHandler(this.frmMiscUpdate_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMiscUpdate_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMiscUpdate_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBanks)).EndInit();
            this.fraBanks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLifeInsuranceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlanCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLifeInsScheduleCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSelectEmployee;
		private FCButton cmdSave;
		private FCButton cmdDelete;
		private FCButton cmdAdd;
	}
}
