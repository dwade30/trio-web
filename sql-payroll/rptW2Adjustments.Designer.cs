﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Adjustments.
	/// </summary>
	partial class rptW2Adjustments
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2Adjustments));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFica = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotFed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotFica = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNumber,
				this.txtName,
				this.txtFederal,
				this.txtState,
				this.txtFica,
				this.txtMedicare
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.txtTotFed,
				this.txtTotState,
				this.txtTotFica,
				this.txtTotMedicare,
				this.Line1
			});
			this.ReportFooter.Height = 0.3958333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.lblTime,
				this.lblMuni,
				this.lblPage,
				this.lblDate
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Number";
			this.Label1.Top = 0.5833333F;
			this.Label1.Width = 0.6666667F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.6666667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.5833333F;
			this.Label2.Width = 2.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.5F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Federal";
			this.Label3.Top = 0.5833333F;
			this.Label3.Width = 0.9166667F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "State";
			this.Label4.Top = 0.5833333F;
			this.Label4.Width = 0.9166667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.5F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Fica";
			this.Label5.Top = 0.5833333F;
			this.Label5.Width = 0.9166667F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.5F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Medicare";
			this.Label6.Top = 0.5833333F;
			this.Label6.Width = 0.9166667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.21875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.5F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label7.Text = "W2 Adjustments";
			this.Label7.Top = 0F;
			this.Label7.Width = 4.635417F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblTime.Text = "Label8";
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.5F;
			// 
			// lblMuni
			// 
			this.lblMuni.Height = 0.1875F;
			this.lblMuni.HyperLink = null;
			this.lblMuni.Left = 0F;
			this.lblMuni.Name = "lblMuni";
			this.lblMuni.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblMuni.Text = "Label9";
			this.lblMuni.Top = 0F;
			this.lblMuni.Width = 2.25F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Label10";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.3125F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblDate.Text = "Label11";
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.3125F;
			// 
			// txtEmployeenumber
			// 
			this.txtEmployeeNumber.Height = 0.1979167F;
			this.txtEmployeeNumber.Left = 0.01041667F;
			this.txtEmployeeNumber.Name = "txtEmployeenumber";
			this.txtEmployeeNumber.Text = "Field1";
			this.txtEmployeeNumber.Top = 0.04166667F;
			this.txtEmployeeNumber.Width = 0.6354167F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1979167F;
			this.txtName.Left = 0.65625F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0.04166667F;
			this.txtName.Width = 2.75F;
			// 
			// txtFederal
			// 
			this.txtFederal.Height = 0.1979167F;
			this.txtFederal.Left = 3.46875F;
			this.txtFederal.Name = "txtFederal";
			this.txtFederal.Style = "text-align: right";
			this.txtFederal.Text = "Field1";
			this.txtFederal.Top = 0.04166667F;
			this.txtFederal.Width = 0.9479167F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1979167F;
			this.txtState.Left = 4.46875F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "text-align: right";
			this.txtState.Text = "Field1";
			this.txtState.Top = 0.04166667F;
			this.txtState.Width = 0.9479167F;
			// 
			// txtFica
			// 
			this.txtFica.Height = 0.1979167F;
			this.txtFica.Left = 5.46875F;
			this.txtFica.Name = "txtFica";
			this.txtFica.Style = "text-align: right";
			this.txtFica.Text = "Field1";
			this.txtFica.Top = 0.04166667F;
			this.txtFica.Width = 0.9479167F;
			// 
			// txtMedicare
			// 
			this.txtMedicare.Height = 0.1979167F;
			this.txtMedicare.Left = 6.46875F;
			this.txtMedicare.Name = "txtMedicare";
			this.txtMedicare.Style = "text-align: right";
			this.txtMedicare.Text = "Field1";
			this.txtMedicare.Top = 0.04166667F;
			this.txtMedicare.Width = 0.9479167F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1979167F;
			this.Field1.Left = 2.760417F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold";
			this.Field1.Text = "Total";
			this.Field1.Top = 0.125F;
			this.Field1.Width = 0.6354167F;
			// 
			// txtTotFed
			// 
			this.txtTotFed.Height = 0.1979167F;
			this.txtTotFed.Left = 3.46875F;
			this.txtTotFed.Name = "txtTotFed";
			this.txtTotFed.Style = "text-align: right";
			this.txtTotFed.Text = "Field1";
			this.txtTotFed.Top = 0.125F;
			this.txtTotFed.Width = 0.9479167F;
			// 
			// txtTotState
			// 
			this.txtTotState.Height = 0.1979167F;
			this.txtTotState.Left = 4.46875F;
			this.txtTotState.Name = "txtTotState";
			this.txtTotState.Style = "text-align: right";
			this.txtTotState.Text = "Field1";
			this.txtTotState.Top = 0.125F;
			this.txtTotState.Width = 0.9479167F;
			// 
			// txtTotFica
			// 
			this.txtTotFica.Height = 0.1979167F;
			this.txtTotFica.Left = 5.46875F;
			this.txtTotFica.Name = "txtTotFica";
			this.txtTotFica.Style = "text-align: right";
			this.txtTotFica.Text = "Field1";
			this.txtTotFica.Top = 0.125F;
			this.txtTotFica.Width = 0.9479167F;
			// 
			// txtTotMedicare
			// 
			this.txtTotMedicare.Height = 0.1979167F;
			this.txtTotMedicare.Left = 6.46875F;
			this.txtTotMedicare.Name = "txtTotMedicare";
			this.txtTotMedicare.Style = "text-align: right";
			this.txtTotMedicare.Text = "Field1";
			this.txtTotMedicare.Top = 0.125F;
			this.txtTotMedicare.Width = 0.9479167F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.05208333F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.04166667F;
			this.Line1.Width = 7.322917F;
			this.Line1.X1 = 0.05208333F;
			this.Line1.X2 = 7.375F;
			this.Line1.Y1 = 0.04166667F;
			this.Line1.Y2 = 0.04166667F;
			// 
			// rptW2Adjustments
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFica;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotFed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotFica;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
