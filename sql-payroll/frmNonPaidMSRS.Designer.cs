//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmNonPaidMSRS.
	/// </summary>
	partial class frmNonPaidMSRS
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame framEmployee;
		public fecherFoundation.FCComboBox cboReportType;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtSocialSecurity;
		public fecherFoundation.FCComboBox cmbStatus;
		public fecherFoundation.FCTextBox txtRecordNumber;
		public fecherFoundation.FCTextBox txtSequence;
		public fecherFoundation.FCComboBox cmbPositionCategory;
		public Global.T2KDateBox t2kDateHired;
		public Global.T2KDateBox t2kDateOfBirth;
		public Global.T2KDateBox t2kTerminationDate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCFrame framMSRS;
		public FCGrid Grid;
		public fecherFoundation.FCTextBox txtWorkWeekPerYear;
		public fecherFoundation.FCTextBox txtFedCompAmount;
		public fecherFoundation.FCTextBox txtLifeInsLevel;
		public fecherFoundation.FCTextBox txtPayRateCode;
		public fecherFoundation.FCTextBox txtPlanCode;
		public fecherFoundation.FCTextBox txtExcess;
		public fecherFoundation.FCTextBox txtPositionCode;
		public fecherFoundation.FCTextBox txtPayPeriodCode;
		public fecherFoundation.FCCheckBox chkInclude;
		public fecherFoundation.FCTextBox txtStatusCode;
		public fecherFoundation.FCTextBox txtLifeInsCode;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel Label1_16;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel MonthlyHours;
		public fecherFoundation.FCFrame framSearch;
		public FCGrid GridSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.framEmployee = new fecherFoundation.FCFrame();
            this.cboReportType = new fecherFoundation.FCComboBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.txtSocialSecurity = new fecherFoundation.FCTextBox();
            this.cmbStatus = new fecherFoundation.FCComboBox();
            this.txtRecordNumber = new fecherFoundation.FCTextBox();
            this.txtSequence = new fecherFoundation.FCTextBox();
            this.cmbPositionCategory = new fecherFoundation.FCComboBox();
            this.t2kDateHired = new Global.T2KDateBox();
            this.t2kDateOfBirth = new Global.T2KDateBox();
            this.t2kTerminationDate = new Global.T2KDateBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.framMSRS = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.txtWorkWeekPerYear = new fecherFoundation.FCTextBox();
            this.txtFedCompAmount = new fecherFoundation.FCTextBox();
            this.txtLifeInsLevel = new fecherFoundation.FCTextBox();
            this.txtPayRateCode = new fecherFoundation.FCTextBox();
            this.txtPlanCode = new fecherFoundation.FCTextBox();
            this.txtExcess = new fecherFoundation.FCTextBox();
            this.txtPositionCode = new fecherFoundation.FCTextBox();
            this.txtPayPeriodCode = new fecherFoundation.FCTextBox();
            this.chkInclude = new fecherFoundation.FCCheckBox();
            this.txtStatusCode = new fecherFoundation.FCTextBox();
            this.txtLifeInsCode = new fecherFoundation.FCTextBox();
            this.Text1 = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.Label1_16 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.MonthlyHours = new fecherFoundation.FCLabel();
            this.framSearch = new fecherFoundation.FCFrame();
            this.GridSearch = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framEmployee)).BeginInit();
            this.framEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateHired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateOfBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTerminationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framMSRS)).BeginInit();
            this.framMSRS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framSearch)).BeginInit();
            this.framSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 1017);
            this.BottomPanel.Size = new System.Drawing.Size(748, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framMSRS);
            this.ClientArea.Controls.Add(this.framEmployee);
            this.ClientArea.Controls.Add(this.framSearch);
            this.ClientArea.Size = new System.Drawing.Size(768, 628);
            this.ClientArea.Controls.SetChildIndex(this.framSearch, 0);
            this.ClientArea.Controls.SetChildIndex(this.framEmployee, 0);
            this.ClientArea.Controls.SetChildIndex(this.framMSRS, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Controls.Add(this.cmdSave);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(768, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(304, 30);
            this.HeaderText.Text = "Non-Paid MainePERS Info";
            // 
            // framEmployee
            // 
            this.framEmployee.Controls.Add(this.cboReportType);
            this.framEmployee.Controls.Add(this.txtLastName);
            this.framEmployee.Controls.Add(this.txtFirstName);
            this.framEmployee.Controls.Add(this.txtSocialSecurity);
            this.framEmployee.Controls.Add(this.cmbStatus);
            this.framEmployee.Controls.Add(this.txtRecordNumber);
            this.framEmployee.Controls.Add(this.txtSequence);
            this.framEmployee.Controls.Add(this.cmbPositionCategory);
            this.framEmployee.Controls.Add(this.t2kDateHired);
            this.framEmployee.Controls.Add(this.t2kDateOfBirth);
            this.framEmployee.Controls.Add(this.t2kTerminationDate);
            this.framEmployee.Controls.Add(this.Label4);
            this.framEmployee.Controls.Add(this.lblName);
            this.framEmployee.Controls.Add(this.Label3);
            this.framEmployee.Controls.Add(this.Label6);
            this.framEmployee.Controls.Add(this.Label5);
            this.framEmployee.Controls.Add(this.Label2);
            this.framEmployee.Controls.Add(this.Label8);
            this.framEmployee.Controls.Add(this.Label9);
            this.framEmployee.Controls.Add(this.Label10);
            this.framEmployee.Controls.Add(this.Label11);
            this.framEmployee.Location = new System.Drawing.Point(30, 30);
            this.framEmployee.Name = "framEmployee";
            this.framEmployee.Size = new System.Drawing.Size(696, 340);
            this.framEmployee.TabIndex = 13;
            this.framEmployee.Tag = "Employee #";
            this.framEmployee.Text = "Employee Information";
            this.framEmployee.Visible = false;
            // 
            // cboReportType
            // 
            this.cboReportType.BackColor = System.Drawing.SystemColors.Window;
            this.cboReportType.Location = new System.Drawing.Point(173, 280);
            this.cboReportType.Name = "cboReportType";
            this.cboReportType.Size = new System.Drawing.Size(220, 40);
            this.cboReportType.TabIndex = 6;
            this.cboReportType.DropDown += new System.EventHandler(this.cboReportType_DropDown);
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.Location = new System.Drawing.Point(173, 30);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(220, 40);
            this.txtLastName.TabIndex = 7;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Location = new System.Drawing.Point(413, 30);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(263, 40);
            this.txtFirstName.TabIndex = 1;
            // 
            // txtSocialSecurity
            // 
            this.txtSocialSecurity.BackColor = System.Drawing.SystemColors.Window;
            this.txtSocialSecurity.Location = new System.Drawing.Point(173, 80);
            this.txtSocialSecurity.MaxLength = 11;
            this.txtSocialSecurity.Name = "txtSocialSecurity";
            this.txtSocialSecurity.Size = new System.Drawing.Size(220, 40);
            this.txtSocialSecurity.TabIndex = 2;
            this.txtSocialSecurity.Validating += new System.ComponentModel.CancelEventHandler(this.txtSocialSecurity_Validating);
            // 
            // cmbStatus
            // 
            this.cmbStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.Location = new System.Drawing.Point(173, 180);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(220, 40);
            this.cmbStatus.TabIndex = 4;
            this.cmbStatus.DropDown += new System.EventHandler(this.cmbStatus_DropDown);
            // 
            // txtRecordNumber
            // 
            this.txtRecordNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtRecordNumber.Location = new System.Drawing.Point(563, 130);
            this.txtRecordNumber.Name = "txtRecordNumber";
            this.txtRecordNumber.Size = new System.Drawing.Size(113, 40);
            this.txtRecordNumber.TabIndex = 8;
            // 
            // txtSequence
            // 
            this.txtSequence.BackColor = System.Drawing.SystemColors.Window;
            this.txtSequence.Location = new System.Drawing.Point(563, 180);
            this.txtSequence.Name = "txtSequence";
            this.txtSequence.Size = new System.Drawing.Size(113, 40);
            this.txtSequence.TabIndex = 9;
            // 
            // cmbPositionCategory
            // 
            this.cmbPositionCategory.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPositionCategory.Location = new System.Drawing.Point(173, 230);
            this.cmbPositionCategory.Name = "cmbPositionCategory";
            this.cmbPositionCategory.Size = new System.Drawing.Size(220, 40);
            this.cmbPositionCategory.TabIndex = 5;
            this.cmbPositionCategory.DropDown += new System.EventHandler(this.cmbPositionCategory_DropDown);
            // 
            // t2kDateHired
            // 
            this.t2kDateHired.Location = new System.Drawing.Point(563, 80);
            this.t2kDateHired.Name = "t2kDateHired";
            this.t2kDateHired.Size = new System.Drawing.Size(115, 22);
            this.t2kDateHired.TabIndex = 7;
            // 
            // t2kDateOfBirth
            // 
            this.t2kDateOfBirth.Location = new System.Drawing.Point(173, 130);
            this.t2kDateOfBirth.Name = "t2kDateOfBirth";
            this.t2kDateOfBirth.Size = new System.Drawing.Size(115, 22);
            this.t2kDateOfBirth.TabIndex = 3;
            // 
            // t2kTerminationDate
            // 
            this.t2kTerminationDate.Location = new System.Drawing.Point(563, 230);
            this.t2kTerminationDate.Name = "t2kTerminationDate";
            this.t2kTerminationDate.Size = new System.Drawing.Size(115, 22);
            this.t2kTerminationDate.TabIndex = 10;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 294);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(130, 18);
            this.Label4.TabIndex = 50;
            this.Label4.Text = "REPORT TYPE";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(20, 44);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(110, 14);
            this.lblName.TabIndex = 49;
            this.lblName.Text = "LAST / FIRST NAME";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 194);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(48, 18);
            this.Label3.TabIndex = 21;
            this.Label3.Text = "STATUS";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 94);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(56, 16);
            this.Label6.TabIndex = 20;
            this.Label6.Text = "SSN #";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(413, 94);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(84, 14);
            this.Label5.TabIndex = 19;
            this.Label5.Text = "DATE HIRED";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 144);
            this.Label2.Name = "Label2";
            this.Label2.TabIndex = 18;
            this.Label2.Text = "DATE OF BIRTH";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(413, 144);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(110, 14);
            this.Label8.TabIndex = 17;
            this.Label8.Text = "RECORD NUMBER";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(413, 194);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(120, 18);
            this.Label9.TabIndex = 16;
            this.Label9.Text = "SEQUENCE NUMBER";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(413, 244);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(84, 14);
            this.Label10.TabIndex = 15;
            this.Label10.Text = "TERMINATED";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 244);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(130, 18);
            this.Label11.TabIndex = 14;
            this.Label11.Text = "POSITION CATEGORY";
            // 
            // framMSRS
            // 
            this.framMSRS.Controls.Add(this.Grid);
            this.framMSRS.Controls.Add(this.txtWorkWeekPerYear);
            this.framMSRS.Controls.Add(this.txtFedCompAmount);
            this.framMSRS.Controls.Add(this.txtLifeInsLevel);
            this.framMSRS.Controls.Add(this.txtPayRateCode);
            this.framMSRS.Controls.Add(this.txtPlanCode);
            this.framMSRS.Controls.Add(this.txtExcess);
            this.framMSRS.Controls.Add(this.txtPositionCode);
            this.framMSRS.Controls.Add(this.txtPayPeriodCode);
            this.framMSRS.Controls.Add(this.chkInclude);
            this.framMSRS.Controls.Add(this.txtStatusCode);
            this.framMSRS.Controls.Add(this.txtLifeInsCode);
            this.framMSRS.Controls.Add(this.Text1);
            this.framMSRS.Controls.Add(this.lblDescription);
            this.framMSRS.Controls.Add(this.Label1_16);
            this.framMSRS.Controls.Add(this.Label1_12);
            this.framMSRS.Controls.Add(this.Label1_11);
            this.framMSRS.Controls.Add(this.Label1_9);
            this.framMSRS.Controls.Add(this.Label1_8);
            this.framMSRS.Controls.Add(this.Label1_7);
            this.framMSRS.Controls.Add(this.Label1_6);
            this.framMSRS.Controls.Add(this.Label1_5);
            this.framMSRS.Controls.Add(this.Label1_4);
            this.framMSRS.Controls.Add(this.Label1_3);
            this.framMSRS.Controls.Add(this.Label1_0);
            this.framMSRS.Controls.Add(this.Label1_1);
            this.framMSRS.Controls.Add(this.MonthlyHours);
            this.framMSRS.FormatCaption = false;
            this.framMSRS.Location = new System.Drawing.Point(30, 390);
            this.framMSRS.Name = "framMSRS";
            this.framMSRS.Size = new System.Drawing.Size(696, 627);
            this.framMSRS.TabIndex = 22;
            this.framMSRS.Tag = "Employee #";
            this.framMSRS.Text = "MainePERS";
            this.framMSRS.Visible = false;
            // 
            // Grid
            // 
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(20, 67);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.Rows = 17;
            this.Grid.Size = new System.Drawing.Size(319, 540);
            this.Grid.StandardTab = false;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 12;
            this.Grid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.Grid_KeyPressEdit);
            this.Grid.ComboDropDown += new System.EventHandler(this.Grid_ComboDropDown);
            this.Grid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.Grid_BeforeEdit);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            // 
            // txtWorkWeekPerYear
            // 
            this.txtWorkWeekPerYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtWorkWeekPerYear.Location = new System.Drawing.Point(199, 517);
            this.txtWorkWeekPerYear.Name = "txtWorkWeekPerYear";
            this.txtWorkWeekPerYear.Size = new System.Drawing.Size(140, 40);
            this.txtWorkWeekPerYear.TabIndex = 33;
            // 
            // txtFedCompAmount
            // 
            this.txtFedCompAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtFedCompAmount.Location = new System.Drawing.Point(199, 417);
            this.txtFedCompAmount.Name = "txtFedCompAmount";
            this.txtFedCompAmount.Size = new System.Drawing.Size(140, 40);
            this.txtFedCompAmount.TabIndex = 32;
            // 
            // txtLifeInsLevel
            // 
            this.txtLifeInsLevel.BackColor = System.Drawing.SystemColors.Window;
            this.txtLifeInsLevel.Location = new System.Drawing.Point(199, 367);
            this.txtLifeInsLevel.Name = "txtLifeInsLevel";
            this.txtLifeInsLevel.Size = new System.Drawing.Size(140, 40);
            this.txtLifeInsLevel.TabIndex = 31;
            // 
            // txtPayRateCode
            // 
            this.txtPayRateCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPayRateCode.Location = new System.Drawing.Point(199, 567);
            this.txtPayRateCode.Name = "txtPayRateCode";
            this.txtPayRateCode.Size = new System.Drawing.Size(140, 40);
            this.txtPayRateCode.TabIndex = 30;
            this.txtPayRateCode.Visible = false;
            // 
            // txtPlanCode
            // 
            this.txtPlanCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlanCode.Location = new System.Drawing.Point(199, 217);
            this.txtPlanCode.Name = "txtPlanCode";
            this.txtPlanCode.Size = new System.Drawing.Size(140, 40);
            this.txtPlanCode.TabIndex = 29;
            // 
            // txtExcess
            // 
            this.txtExcess.BackColor = System.Drawing.SystemColors.Window;
            this.txtExcess.Location = new System.Drawing.Point(199, 267);
            this.txtExcess.MaxLength = 1;
            this.txtExcess.Name = "txtExcess";
            this.txtExcess.Size = new System.Drawing.Size(140, 40);
            this.txtExcess.TabIndex = 28;
            // 
            // txtPositionCode
            // 
            this.txtPositionCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPositionCode.Location = new System.Drawing.Point(199, 167);
            this.txtPositionCode.Name = "txtPositionCode";
            this.txtPositionCode.Size = new System.Drawing.Size(140, 40);
            this.txtPositionCode.TabIndex = 27;
            // 
            // txtPayPeriodCode
            // 
            this.txtPayPeriodCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPayPeriodCode.Location = new System.Drawing.Point(199, 317);
            this.txtPayPeriodCode.MaxLength = 1;
            this.txtPayPeriodCode.Name = "txtPayPeriodCode";
            this.txtPayPeriodCode.Size = new System.Drawing.Size(140, 40);
            this.txtPayPeriodCode.TabIndex = 26;
            // 
            // chkInclude
            // 
            this.chkInclude.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkInclude.Location = new System.Drawing.Point(20, 30);
            this.chkInclude.Name = "chkInclude";
            this.chkInclude.Size = new System.Drawing.Size(73, 24);
            this.chkInclude.TabIndex = 11;
            this.chkInclude.Text = "Include";
            // 
            // txtStatusCode
            // 
            this.txtStatusCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtStatusCode.Location = new System.Drawing.Point(199, 117);
            this.txtStatusCode.Name = "txtStatusCode";
            this.txtStatusCode.Size = new System.Drawing.Size(140, 40);
            this.txtStatusCode.TabIndex = 25;
            // 
            // txtLifeInsCode
            // 
            this.txtLifeInsCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtLifeInsCode.Location = new System.Drawing.Point(199, 67);
            this.txtLifeInsCode.Name = "txtLifeInsCode";
            this.txtLifeInsCode.Size = new System.Drawing.Size(140, 40);
            this.txtLifeInsCode.TabIndex = 24;
            // 
            // Text1
            // 
            this.Text1.BackColor = System.Drawing.SystemColors.Window;
            this.Text1.Location = new System.Drawing.Point(199, 467);
            this.Text1.Name = "Text1";
            this.Text1.Size = new System.Drawing.Size(140, 40);
            this.Text1.TabIndex = 23;
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(359, 67);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(317, 540);
            this.lblDescription.TabIndex = 47;
            // 
            // Label1_16
            // 
            this.Label1_16.Location = new System.Drawing.Point(20, 531);
            this.Label1_16.Name = "Label1_16";
            this.Label1_16.Size = new System.Drawing.Size(155, 16);
            this.Label1_16.TabIndex = 46;
            this.Label1_16.Text = "WORK WEEKS PER YEAR";
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(20, 581);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(116, 16);
            this.Label1_12.TabIndex = 45;
            this.Label1_12.Text = "PAY RATE CODE H/D";
            this.Label1_12.Visible = false;
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(20, 431);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(155, 16);
            this.Label1_11.TabIndex = 44;
            this.Label1_11.Text = "FEDERAL COMP AMOUNT";
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(20, 381);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(94, 16);
            this.Label1_9.TabIndex = 43;
            this.Label1_9.Text = "LIFE INS LEVEL";
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(20, 322);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(160, 15);
            this.Label1_8.TabIndex = 42;
            this.Label1_8.Text = "LIFE INS SCHEDULE CODE";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(36, 292);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(130, 15);
            this.Label1_7.TabIndex = 41;
            this.Label1_7.Text = "EXCESS / PURCHASE";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(20, 272);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(170, 15);
            this.Label1_6.TabIndex = 40;
            this.Label1_6.Text = "LIFE INS SCHEDULE CODE";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 231);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(110, 16);
            this.Label1_5.TabIndex = 39;
            this.Label1_5.Text = "PLAN CODE";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(20, 181);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(106, 16);
            this.Label1_4.TabIndex = 38;
            this.Label1_4.Text = "POSITION CODE";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 131);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(116, 16);
            this.Label1_3.TabIndex = 37;
            this.Label1_3.Text = "STATUS CODE";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 81);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(84, 16);
            this.Label1_0.TabIndex = 36;
            this.Label1_0.Text = "LIFE INS CODE";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(35, 342);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(30, 15);
            this.Label1_1.TabIndex = 35;
            this.Label1_1.Text = "B-Z";
            // 
            // MonthlyHours
            // 
            this.MonthlyHours.Location = new System.Drawing.Point(20, 481);
            this.MonthlyHours.Name = "MonthlyHours";
            this.MonthlyHours.Size = new System.Drawing.Size(155, 16);
            this.MonthlyHours.TabIndex = 34;
            this.MonthlyHours.Text = "MONTHLY HOURS";
            // 
            // framSearch
            // 
            this.framSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framSearch.AppearanceKey = "groupBoxNoBorders";
            this.framSearch.Controls.Add(this.GridSearch);
            this.framSearch.Location = new System.Drawing.Point(30, 30);
            this.framSearch.Name = "framSearch";
            this.framSearch.Size = new System.Drawing.Size(693, 593);
            this.framSearch.TabIndex = 51;
            this.framSearch.Text = "Non-Employees";
            // 
            // GridSearch
            // 
            this.GridSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridSearch.Cols = 8;
            this.GridSearch.ExtendLastCol = true;
            this.GridSearch.FixedCols = 0;
            this.GridSearch.Location = new System.Drawing.Point(0, 30);
            this.GridSearch.Name = "GridSearch";
            this.GridSearch.RowHeadersVisible = false;
            this.GridSearch.Rows = 1;
            this.GridSearch.Size = new System.Drawing.Size(693, 557);
            this.GridSearch.TabIndex = 48;
            this.GridSearch.DoubleClick += new System.EventHandler(this.GridSearch_DblClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuSearch,
            this.mnuSepar1,
            this.mnuSave,
            this.mnuSaveContinue,
            this.mnuSepar2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSearch
            // 
            this.mnuSearch.Enabled = false;
            this.mnuSearch.Index = 1;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Text = "Search for Employee";
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 2;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Enabled = false;
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Enabled = false;
            this.mnuSaveContinue.Index = 4;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 5;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Enabled = false;
            this.cmdSaveContinue.Location = new System.Drawing.Point(280, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(176, 48);
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(480, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(50, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSave.Enabled = false;
            this.cmdSave.Location = new System.Drawing.Point(690, 29);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdSave.Size = new System.Drawing.Size(50, 24);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.Enabled = false;
            this.cmdSearch.Location = new System.Drawing.Point(536, 29);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(148, 24);
            this.cmdSearch.TabIndex = 3;
            this.cmdSearch.Text = "Search for Employee";
            this.cmdSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // frmNonPaidMSRS
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(768, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmNonPaidMSRS";
            this.Text = "Non-Paid MainePERS Info";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmNonPaidMSRS_Load);
            this.Activated += new System.EventHandler(this.frmNonPaidMSRS_Activated);
            this.Resize += new System.EventHandler(this.frmNonPaidMSRS_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNonPaidMSRS_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framEmployee)).EndInit();
            this.framEmployee.ResumeLayout(false);
            this.framEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateHired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateOfBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTerminationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framMSRS)).EndInit();
            this.framMSRS.ResumeLayout(false);
            this.framMSRS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framSearch)).EndInit();
            this.framSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSaveContinue;
		private FCButton cmdNew;
		private FCButton cmdSave;
		private FCButton cmdSearch;
	}
}
