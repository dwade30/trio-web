﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMEUC1.
	/// </summary>
	public partial class rptMEUC1 : BaseSectionReport
	{
		public rptMEUC1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "UC1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMEUC1 InstancePtr
		{
			get
			{
				return (rptMEUC1)Sys.GetInstance(typeof(rptMEUC1));
			}
		}

		protected rptMEUC1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMEUC1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolPrintTest;
		double dblRate;
		double dblLimit;
		string strSeq = "";
		int intQuarterCovered;
		int lngYearCovered;
		double dblCSSFRate;
        double dblUPAFRate;
		cUC1Schedule2Detail lstDetails;
		cUC1Summary ucSummary;
		cUC1Report theReport;

		public cUC1Report UCReportObject
		{
			get
			{
				cUC1Report UCReportObject = null;
				UCReportObject = theReport;
				return UCReportObject;
			}
		}

		public bool IsTestPrint
		{
			get
			{
				bool IsTestPrint = false;
				IsTestPrint = boolPrintTest;
				return IsTestPrint;
			}
		}

		private void LoadData()
		{
			string strUnEmpQuery1 = "";
			string strUnEmpQuery2 = "";
			string strSQL2 = "";
			int lngSeqStart;
			int lngSeqEnd;
			string[] strAry = null;
			string strRange = "";
			string strQuery1 = "";
			string strQuery2 = "";
			string strQuery3 = "";
			string strQuery4 = "";
			DateTime dtStartDate;
			DateTime dtEndDate;
			string strSQL = "";
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modCoreysSweeterCode.Statics.EWRWageTotals.dblExcessWages = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblLimit = dblLimit;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblRate = dblRate;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblCSSFRate = dblCSSFRate;
                modCoreysSweeterCode.Statics.EWRWageTotals.dblUPAFRate = dblUPAFRate;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalGrossReportableWages = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalStateWithheld = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthFemales = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthWorkers = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthFemales = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthWorkers = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthFemales = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthWorkers = 0;
				SubReport1.Report = new srptMEUC1Schedule2();
				SubReport1.Report.UserData = strSQL;
				SubReport2.Report = new srptMEUC1First();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			LoadData();
		}

		public void Init(cUC1Report rep, bool modalDialog, bool boolTestPrint = false)
		{
			boolPrintTest = boolTestPrint;
			theReport = rep;
			modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter = theReport.Quarter;
			modCoreysSweeterCode.Statics.EWRWageTotals.lngYear = theReport.YearCovered;
			modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest = boolPrintTest;
			frmReportViewer.InstancePtr.Init(this, "", 1, showModal: modalDialog);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			srptMEUC1First.InstancePtr.Unload();
			srptMEUC1Schedule2.InstancePtr.Unload();
		}
	}
}
