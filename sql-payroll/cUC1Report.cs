﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cUC1Report
	{
		//=========================================================
		private int intQuarter;
		int lngYearCovered;
		double dblCSSFRate;
        double dblUPAFRate;
		private cUC1DetailList lstDetails = new cUC1DetailList();
		private cUC1Summary ucSummary = new cUC1Summary();
		private double dblUCRate;
		private double dblLimit;
		private cUC1Transmitter ucTransmitter = new cUC1Transmitter();
		private string[] strEmploymentMonthStartDate = new string[3 + 1];
		private string[] strEmploymentMonthEndDate = new string[3 + 1];

		public void Set_EmploymentMonthEndDate(int intIndex, string strDate)
		{
			if (intIndex > 0 && intIndex < 4)
			{
				strEmploymentMonthEndDate[intIndex - 1] = strDate;
			}
		}

		public string Get_EmploymentMonthEndDate(int intIndex)
		{
			string EmploymentMonthEndDate = "";
			if (intIndex > 0 && intIndex < 4)
			{
				EmploymentMonthEndDate = strEmploymentMonthEndDate[intIndex - 1];
			}
			return EmploymentMonthEndDate;
		}

		public void Set_EmploymentMonthStartDate(int intIndex, string strDate)
		{
			if (intIndex > 0 && intIndex < 4)
			{
				strEmploymentMonthStartDate[intIndex - 1] = strDate;
			}
		}

		public string Get_EmploymentMonthStartDate(int intIndex)
		{
			string EmploymentMonthStartDate = "";
			if (intIndex > 0 && intIndex < 4)
			{
				EmploymentMonthStartDate = strEmploymentMonthStartDate[intIndex - 1];
			}
			return EmploymentMonthStartDate;
		}

		public cUC1Transmitter Transmitter
		{
			set
			{
				ucTransmitter = value;
			}
			get
			{
				cUC1Transmitter Transmitter = null;
				Transmitter = ucTransmitter;
				return Transmitter;
			}
		}

		public double UCRate
		{
			set
			{
				dblUCRate = value;
			}
			get
			{
				double UCRate = 0;
				UCRate = dblUCRate;
				return UCRate;
			}
		}

		public double Limit
		{
			set
			{
				dblLimit = value;
			}
			get
			{
				double Limit = 0;
				Limit = dblLimit;
				return Limit;
			}
		}

		public int Quarter
		{
			set
			{
				intQuarter = value;
			}
			get
			{
				int Quarter = 0;
				Quarter = intQuarter;
				return Quarter;
			}
		}

		public int YearCovered
		{
			set
			{
				lngYearCovered = value;
			}
			get
			{
				int YearCovered = 0;
				YearCovered = lngYearCovered;
				return YearCovered;
			}
		}

		public double CSSFRate
		{
			set
			{
				dblCSSFRate = value;
			}
			get
			{
				double CSSFRate = 0;
				CSSFRate = dblCSSFRate;
				return CSSFRate;
			}
		}

        public double UPAFRate
        {
            set
            {
                dblUPAFRate = value;
            }
            get
            {
                double UPAFRate = 0;
                UPAFRate = dblUPAFRate;
                return UPAFRate;
            }
        }

		public cUC1DetailList Details
		{
			set
			{
				lstDetails = value;
			}
			get
			{
				cUC1DetailList Details = null;
				Details = lstDetails;
				return Details;
			}
		}

		public cUC1Summary Summary
		{
			set
			{
				ucSummary = value;
			}
			get
			{
				cUC1Summary Summary = null;
				Summary = ucSummary;
				return Summary;
			}
		}
	}
}
