//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTaxSumaryReport.
	/// </summary>
	public partial class rptTaxSumaryReport : BaseSectionReport
	{
		public rptTaxSumaryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Tax Summary Report";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			vsData = new FCGrid();
			vsData.Rows = 1;
			vsData.Cols = 10;
			vsHeader = new FCGrid();
			vsHeader.Rows = 2;
			vsHeader.Cols = 10;
		}

		public static rptTaxSumaryReport InstancePtr
		{
			get
			{
				return (rptTaxSumaryReport)Sys.GetInstance(typeof(rptTaxSumaryReport));
			}
		}

		protected rptTaxSumaryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsDeds?.Dispose();
				rsData?.Dispose();
				rsTotalPay?.Dispose();
                rsDeds = null;
                rsData = null;
                rsTotalPay = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTaxSumaryReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		
		int intRow;
		int intCol;
		int intPageNumber;
		bool boolDoubleHeader;
		string strEmployeeNumber = "";
		double dblTotal;
		int intCounter;
		double dblTotalPayTotal;
		double dblFederalTotal;
		double dblMedicareTotal;
		double dblStateTotal;
		double dblFICATotal;
		double dblLocalTotal;
		double dblDeductsTotal;
		double dblNetTotal;
		int intEmployeeTotal;
		int intSetTotals;
		clsDRWrapper rsTotalPay = new clsDRWrapper();
		clsDRWrapper rsDeds = new clsDRWrapper();
		string strOrderBy = "";
		double dblGross;
		double dblNet;
		double dblTaxes;
		double dblDeducts;
		double dblFed;
		double dblFica;
		double dblMed;
		double dblState;
		private string strWhereClause = string.Empty;
		FCGrid vsData;
		FCGrid vsHeader;

		public void Init(int intMQYF, DateTime dtToDate, int intPayrun = 1, List<string> batchReports = null)
		{
			this.Fields.Add("grpHeader");
			// any date will work, but should be a paydate since the titles will display this date
			string strWhere;
			int intMonth;
			int intQuarter = 0;
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(DateTime, string)
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			string strSQL;
			string strTaxQuery;
			string strPayRunWhere;
			DateTime dtTempDate;
			string strDedQuery;
			dtEndDate = dtToDate;
			strPayRunWhere = "";
			switch (intMQYF)
			{
				case 0:
					{
						dtStartDate = dtToDate;
						if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
						{
							txtTitle2.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
						}
						else
						{
							strPayRunWhere = " and payrunid = " + FCConvert.ToString(intPayrun) + " ";
							txtTitle2.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						}
						break;
					}
				case 1:
					{
						// monthly
						dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(dtEndDate.Month) + "/1/" + FCConvert.ToString(dtEndDate.Year));
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "MTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
				case 2:
					{
						// quarterly
						if (dtEndDate.Month >= 1 && dtEndDate.Month <= 3)
						{
							intQuarter = 1;
						}
						else if (dtEndDate.Month >= 4 && dtEndDate.Month <= 6)
						{
							intQuarter = 2;
						}
						else if (dtEndDate.Month >= 7 && dtEndDate.Month <= 9)
						{
							intQuarter = 3;
						}
						else if (dtEndDate.Month >= 10 && dtEndDate.Month <= 12)
						{
							intQuarter = 4;
						}
						modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarter, dtToDate.Year);
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "QTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
				case 3:
					{
						// Calendar Year
						dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtEndDate.Year));
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "Calendar YTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
				case 4:
					{
						// Fiscal Year
						dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtToDate);
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "Fiscal YTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
			}
			//end switch
			strWhere = " paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' " + strPayRunWhere;
			strWhereClause = strWhere;
			// GOT RID OF THE TotalRecord = 1 SO THAT TOTAL NET WOULD PICK UP ADJUSTMENT RECORDS
			strSQL = "select employeenumber,sum(netpay) as TotNet,sum(FederalTaxWH) as TotFed,sum(StateTaxWH) as TotState,sum(FicaTaxWH) as TotFica,sum(MedicareTaxWH) as TotMedicare,sum(GrossPay) as TotGross from tblCheckDetail where " + strWhere + "  and checkvoid = 0 group by employeenumber having  SUM(grossPAY) > 0";
			strTaxQuery = "(" + strSQL + ") as TaxSummaryQuery ";
			rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 0)
			{
				// employee name
				strOrderBy = " order by lastname,firstname ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 1)
			{
				// employee number
				strOrderBy = " order by tblemployeemaster.employeenumber ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 2)
			{
				// sequence
				strOrderBy = " order by seqnumber,lastname,firstname ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 3)
			{
				// dept/div
				strOrderBy = " order by DEPTDIV,lastname,firstname ";
			}
			strSQL = "Select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblEmployeemaster inner join " + strTaxQuery + " on (tblEmployeeMaster.employeenumber = TaxSummaryQuery.employeenumber) " + strOrderBy;
			rsData.OpenRecordset(strSQL, "twpy0000.vb1");
			strSQL = "select employeenumber,sum(DEDAMOUNT) as dedsum from tblcheckdetail where (checkvoid = 0) and deductionrecord = 1 and " + strWhere + " group by employeenumber ";
			strDedQuery = "(" + strSQL + ") as TaxDedQuery ";
			strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strDedQuery + " on (tblemployeemaster.employeenumber = taxdedquery.employeenumber) " + strOrderBy;
			rsDeds.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				if (strOrderBy == " order by lastname,firstname ")
				{
					// employee name
					txtGroup.Text = rsData.Get_Fields_String("LastName") + ", " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName");
					this.Fields["grpHeader"].Value = rsData.Get_Fields_String("LastName") + ", " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName");
				}
				else if (strOrderBy == " order by tblemployeemaster.employeenumber ")
				{
					// employee number
					txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber"));
					this.Fields["grpHeader"].Value = rsData.Get_Fields("tblemployeemasteremployeenumber");
				}
				else if (strOrderBy == " order by seqnumber,lastname,firstname ")
				{
					// sequence
					txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("SeqNumber"));
					this.Fields["grpHeader"].Value = rsData.Get_Fields("SeqNumber");
				}
				else if (strOrderBy == " order by DEPTDIV,lastname,firstname ")
				{
					// dept/div
					txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
					this.Fields["grpHeader"].Value = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("DeptDiv")));
				}
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// Me.PrintReport (False)
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this, "", 1, boolAllowEmail: true, strAttachmentName: "TaxSummary", showModal:true);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// On Error Resume Next
			//clsDRWrapper rsTemp = new clsDRWrapper();
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			NextRecord:
			;
			Line2.Visible = false;
			DateTime dtLastDate;
			int intLastPayrun;
			string strMastRecord = "";
			if (rsData.EndOfFile())
			{
				vsData.TextMatrix(0, 0, string.Empty);
				vsData.TextMatrix(0, 1, string.Empty);
				vsData.TextMatrix(0, 2, string.Empty);
				vsData.TextMatrix(0, 3, string.Empty);
				vsData.TextMatrix(0, 4, string.Empty);
				vsData.TextMatrix(0, 5, string.Empty);
				vsData.TextMatrix(0, 6, string.Empty);
				vsData.TextMatrix(0, 7, string.Empty);
				vsData.TextMatrix(0, 8, string.Empty);
				vsData.TextMatrix(0, 9, string.Empty);
				Field1.Text = vsData.TextMatrix(0, 0);
				Field2.Text = vsData.TextMatrix(0, 1);
				Field3.Text = vsData.TextMatrix(0, 2);
				Field4.Text = vsData.TextMatrix(0, 3);
				Field5.Text = vsData.TextMatrix(0, 4);
				Field6.Text = vsData.TextMatrix(0, 5);
				Field7.Text = vsData.TextMatrix(0, 6);
				// Field8.Text = .TextMatrix(0, 7)
				Field9.Text = vsData.TextMatrix(0, 8);
				Field10.Text = vsData.TextMatrix(0, 9);

				intSetTotals += 1;
				if (intSetTotals == 1)
				{
					this.Fields["grpHeader"].Value = "101010101010110";
					this.GroupHeader1.Visible = false;
					eArgs.EOF = false;
					return;
				}
				else if (intSetTotals == 2)
				{
					this.Line3.Visible = false;
					this.GroupFooter1.Visible = false;
					this.GroupHeader1.Visible = false;
					vsData.TextMatrix(0, 0, "Final");
					eArgs.EOF = false;
					return;
				}
				else if (intSetTotals == 3)
				{
					Line2.Visible = true;
					vsData.TextMatrix(0, 0, "Totals");
					vsData.TextMatrix(0, 1, "Employees: " + FCConvert.ToString(intEmployeeTotal));
					vsData.TextMatrix(0, 2, Strings.Format(dblTotalPayTotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 3, Strings.Format(dblFederalTotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 4, Strings.Format(dblFICATotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 5, Strings.Format(dblMedicareTotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 6, Strings.Format(dblStateTotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 7, Strings.Format(dblLocalTotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 8, Strings.Format(dblDeductsTotal, "#,###,##0.00"));
					vsData.TextMatrix(0, 9, Strings.Format(dblNetTotal, "#,###,##0.00"));
					Field1.Text = vsData.TextMatrix(0, 0);
					Field2.Text = vsData.TextMatrix(0, 1);
					Field3.Text = vsData.TextMatrix(0, 2);
					Field4.Text = vsData.TextMatrix(0, 3);
					Field5.Text = vsData.TextMatrix(0, 4);
					Field6.Text = vsData.TextMatrix(0, 5);
					Field7.Text = vsData.TextMatrix(0, 6);
					// Field8.Text = .TextMatrix(0, 7)
					Field9.Text = vsData.TextMatrix(0, 8);
					Field10.Text = vsData.TextMatrix(0, 9);
					this.GroupHeader1.Visible = false;
					this.GroupFooter1.Visible = false;
					this.Line3.Visible = false;
					eArgs.EOF = false;
					return;
				}
				else if (intSetTotals == 4)
				{
					eArgs.EOF = true;
					return;
				}
			}
			// ADD THE DATA TO THE CORRECT CELL IN THE GRID
			if (strOrderBy == " order by lastname,firstname ")
			{
				// employee name
				txtGroup.Text = rsData.Get_Fields_String("LastName") + ", " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName");
				this.Fields["grpHeader"].Value = rsData.Get_Fields_String("LastName") + ", " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName");
			}
			else if (strOrderBy == " order by tblemployeemaster.employeenumber ")
			{
				// employee number
				txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber"));
				this.Fields["grpHeader"].Value = rsData.Get_Fields("tblemployeemasteremployeenumber");
			}
			else if (strOrderBy == " order by seqnumber,lastname,firstname ")
			{
				// sequence
				txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("SeqNumber"));
				this.Fields["grpHeader"].Value = rsData.Get_Fields("SeqNumber");
			}
			else if (strOrderBy == " order by DEPTDIV,lastname,firstname ")
			{
				// dept/div
				txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
				this.Fields["grpHeader"].Value = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("DeptDiv")));
			}
			dblGross = Conversion.Val(rsData.Get_Fields("TotGross"));
			dblTaxes = Conversion.Val(rsData.Get_Fields("totfed")) + Conversion.Val(rsData.Get_Fields("totstate")) + Conversion.Val(rsData.Get_Fields("totfica")) + Conversion.Val(rsData.Get_Fields("totlocal")) + Conversion.Val(rsData.Get_Fields("totMedicare"));
			// Get Deductions
			if (!rsDeds.EndOfFile())
			{
				if (rsDeds.Get_Fields("tblemployeemasteremployeenumber") == rsData.Get_Fields("tblemployeemasteremployeenumber"))
				{
					dblDeducts = Conversion.Val(rsDeds.Get_Fields("dedsum"));
				}
				else
				{
					rsDeds.FindNextRecord("tblemployeemasteremployeenumber", rsData.Get_Fields("tblemployeemasteremployeenumber"));
					if (!rsDeds.NoMatch)
					{
						dblDeducts = Conversion.Val(rsDeds.Get_Fields("dedsum"));
					}
					else
					{
						dblDeducts = 0;
					}
				}
			}
			else
			{
				dblDeducts = 0;
			}
			// ADD THE DATA TO THE CORRECT CELL IN THE GRID
			if (strEmployeeNumber == fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber"))))
			{
				if (!rsData.EndOfFile())
					rsData.MoveNext();
				goto NextRecord;
			}
			strEmployeeNumber = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber")));
			vsData.TextMatrix(0, 0, fecherFoundation.Strings.Trim(strEmployeeNumber));
			vsData.TextMatrix(0, 1, fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LastName"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("middlename"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Desig")))));
			intEmployeeTotal += 1;
			dblNet = Conversion.Val(rsData.Get_Fields("TotNet"));
			// dblNet = 0
			// Call rsTemp.OpenRecordset("select netpay,paydate,payrunid,masterrecord from tblcheckdetail where employeenumber = '" & strEmployeeNumber & "' and " & strWhereClause & " and checkvoid = 0 and employeenumber = '" & strEmployeeNumber & "' and totalrecord order by paydate,payrunid,masterrecord,ID desc", "twpy0000.vb1")
			// 
			// 
			// intLastPayrun = 0
			// dtLastDate = 0
			// strMastRecord = ""
			// Do While Not rsTemp.EndOfFile
			// If dtLastDate <> rsTemp.Fields("paydate") Or intLastPayrun <> rsTemp.Fields("payrunid") Or strMastRecord <> rsTemp.Fields("masterrecord") Then
			// dtLastDate = rsTemp.Fields("paydate")
			// intLastPayrun = Val(rsTemp.Fields("payrunid"))
			// strMastRecord = rsTemp.Fields("masterrecord")
			// dblNet = dblNet + Val(rsTemp.Fields("netpay"))
			// End If
			// rsTemp.MoveNext
			// Loop
			dblTotal = 0;
			// calculate the total Pay
			dblTotal = dblGross;
			vsData.TextMatrix(0, 2, Strings.Format(Conversion.Val(dblTotal), "0.00"));
			dblTotalPayTotal += dblTotal;
			vsData.TextMatrix(0, 3, Strings.Format(rsData.Get_Fields("totfed"), "0.00"));
			dblFed = FCConvert.ToDouble(vsData.TextMatrix(0, 3));
			dblFederalTotal += Conversion.Val(vsData.TextMatrix(0, 3));
			vsData.TextMatrix(0, 4, Strings.Format(rsData.Get_Fields("totfica"), "0.00"));
			dblFica = FCConvert.ToDouble(vsData.TextMatrix(0, 4));
			dblFICATotal += Conversion.Val(vsData.TextMatrix(0, 4));
			if (FCConvert.ToString(rsData.Get_Fields("totMedicare")) == string.Empty)
			{
				vsData.TextMatrix(0, 5, "0.00");
			}
			else
			{
				vsData.TextMatrix(0, 5, Strings.Format(rsData.Get_Fields("totMedicare"), "0.00"));
			}
			dblMed = FCConvert.ToDouble(vsData.TextMatrix(0, 5));
			dblMedicareTotal += Conversion.Val(vsData.TextMatrix(0, 5));
			vsData.TextMatrix(0, 6, Strings.Format(rsData.Get_Fields("totstate"), "0.00"));
			dblState = FCConvert.ToDouble(vsData.TextMatrix(0, 6));
			dblStateTotal += Conversion.Val(vsData.TextMatrix(0, 6));
			// get the employee dedutions
			vsData.TextMatrix(0, 8, Strings.Format(dblDeducts, "0.00"));
			dblDeductsTotal += Conversion.Val(vsData.TextMatrix(0, 8));
			vsData.TextMatrix(0, 9, Strings.Format(dblNet, "0.00"));
			dblNetTotal += dblNet;
			Field1.Text = vsData.TextMatrix(0, 0);
			Field2.Text = vsData.TextMatrix(0, 1);
			Field3.Text = vsData.TextMatrix(0, 2);
			Field4.Text = vsData.TextMatrix(0, 3);
			Field5.Text = vsData.TextMatrix(0, 4);
			Field6.Text = vsData.TextMatrix(0, 5);
			Field7.Text = vsData.TextMatrix(0, 6);
			// Field8.Text = .TextMatrix(0, 7)
			Field9.Text = vsData.TextMatrix(0, 8);
			Field10.Text = vsData.TextMatrix(0, 9);
			Field12.Text = FCConvert.ToString(Conversion.Val(Field12.Text) + Conversion.Val(vsData.TextMatrix(0, 2)));
			Field13.Text = FCConvert.ToString(Conversion.Val(Field13.Text) + Conversion.Val(vsData.TextMatrix(0, 3)));
			Field14.Text = FCConvert.ToString(Conversion.Val(Field14.Text) + Conversion.Val(vsData.TextMatrix(0, 4)));
			Field15.Text = FCConvert.ToString(Conversion.Val(Field15.Text) + Conversion.Val(vsData.TextMatrix(0, 5)));
			Field16.Text = FCConvert.ToString(Conversion.Val(Field16.Text) + Conversion.Val(vsData.TextMatrix(0, 6)));
			// Field17.Text = Val(Field17.Text) + .TextMatrix(0, 7)
			Field18.Text = FCConvert.ToString(Conversion.Val(Field18.Text) + Conversion.Val(vsData.TextMatrix(0, 8)));
			Field19.Text = FCConvert.ToString(Conversion.Val(Field19.Text) + Conversion.Val(vsData.TextMatrix(0, 9)));
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intSetTotals = 0;
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			intRow = 0;
			boolDoubleHeader = false;
			vsHeader.TextMatrix(0, 0, "Employee");
			vsHeader.TextMatrix(0, 1, "Employee");
			vsHeader.TextMatrix(0, 2, "Gross");
			vsHeader.TextMatrix(0, 3, "- - - - - - - -");
			vsHeader.TextMatrix(0, 4, "- - - - - - - -");
			vsHeader.TextMatrix(0, 5, "Taxes");
			vsHeader.TextMatrix(0, 6, "- - - - - - - -");
			vsHeader.TextMatrix(0, 7, "- - - - - - - -");
			vsHeader.TextMatrix(1, 0, "ID");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 1, "Name");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 2, "- Pay -");
			vsHeader.ColWidth(2, 1000);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 3, "Federal");
			vsHeader.ColWidth(3, 900);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 4, "FICA");
			vsHeader.ColWidth(4, 900);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 5, "Medicare");
			vsHeader.ColWidth(5, 900);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 6, "State");
			vsHeader.ColWidth(6, 900);
			vsHeader.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 7, "Local");
			vsHeader.ColWidth(7, 900);
			vsHeader.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 8, "Deducts");
			vsHeader.ColWidth(8, 1100);
			vsHeader.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 9, "Net");
			vsHeader.ColWidth(9, 1100);
			vsHeader.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, 7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
			// txtHead1.Width = 805
			// txtHead1.Text = .TextMatrix(0, 0)
			// txtHead1.Left = 0
			// 
			// txtHead2.Width = .ColWidth(1)
			// 
			// txtHead2.Left = txtHead1.Left + txtHead1.Width
			// txtHead2.Text = .TextMatrix(1, 1)
			// txtHead3.Width = .ColWidth(2)
			// txtHead3.Left = txtHead2.Left + txtHead2.Width
			// txtHead3.Text = .TextMatrix(1, 2)
			// txtHead3Top.Width = .ColWidth(2)
			// txtHead3Top.Left = txtHead3.Left
			// txtHead3Top.Text = .TextMatrix(0, 2)
			// txtHead4.Width = .ColWidth(3)
			// txtHead4.Left = txtHead3.Left + txtHead3.Width
			// txtHead4.Text = .TextMatrix(1, 3)
			// 
			// txtHead5.Width = .ColWidth(4)
			// txtHead5.Left = txtHead4.Left + txtHead4.Width
			// txtHead5.Text = .TextMatrix(1, 4)
			// txtHead6.Width = .ColWidth(5)
			// txtHead6.Left = txtHead5.Left + txtHead5.Width
			// txtHead6.Text = .TextMatrix(1, 5)
			// txtHead7.Width = .ColWidth(6)
			// txtHead7.Left = txtHead6.Left + txtHead6.Width
			// txtHead7.Text = .TextMatrix(1, 6)
			// txtHead8.Width = .ColWidth(7)
			// txtHead8.Left = txtHead7.Left + txtHead7.Width
			// txtHead8.Text = .TextMatrix(1, 7)
			// txtHead9.Width = .ColWidth(8)
			// txtHead9.Left = txtHead8.Left + txtHead8.Width
			// txtHead9.Text = .TextMatrix(1, 8)
			// txtHead10.Width = .ColWidth(9)
			// txtHead10.Left = txtHead9.Left + txtHead9.Width
			// txtHead10.Text = .TextMatrix(1, 9)
			// txtHeadDashes1.Left = txtHead4.Left
			// txtHeadTaxes.Left = txtHeadDashes1.Left + txtHeadDashes1.Width
			// 
			// txtHeadDashes2.Left = txtHeadTaxes.Left + txtHeadTaxes.Width
			// txtHeadDashes2.Left = txtHead8.Left + txtHead8.Width - txtHeadDashes2.Width
			// txtHeadTaxes.Left = ((txtHeadDashes2.Left + txtHeadDashes1.Left + txtHeadDashes1.Width) / 2) - (txtHeadTaxes.Width / 2)
			vsData.ColWidth(0, vsHeader.ColWidth(0));
			vsData.ColWidth(1, vsHeader.ColWidth(1));
			vsData.ColWidth(2, vsHeader.ColWidth(2));
			vsData.ColWidth(3, vsHeader.ColWidth(3));
			vsData.ColWidth(4, vsHeader.ColWidth(4));
			vsData.ColWidth(5, vsHeader.ColWidth(5));
			vsData.ColWidth(6, vsHeader.ColWidth(6));
			vsData.ColWidth(7, vsHeader.ColWidth(7));
			vsData.ColWidth(8, vsHeader.ColWidth(8));
			vsData.ColWidth(9, vsHeader.ColWidth(9));
			vsData.ColAlignment(0, vsHeader.ColAlignment(0));
			vsData.ColAlignment(1, vsHeader.ColAlignment(1));
			vsData.ColAlignment(2, vsHeader.ColAlignment(2));
			vsData.ColAlignment(3, vsHeader.ColAlignment(3));
			vsData.ColAlignment(4, vsHeader.ColAlignment(4));
			vsData.ColAlignment(5, vsHeader.ColAlignment(5));
			vsData.ColAlignment(6, vsHeader.ColAlignment(6));
			vsData.ColAlignment(7, vsHeader.ColAlignment(7));
			vsData.ColAlignment(8, vsHeader.ColAlignment(8));
			vsData.ColAlignment(9, vsHeader.ColAlignment(9));
			// Field1.Width = 805
			// Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left
			// Field1.Left = 0
			// Field2.Width = .ColWidth(1) - 200
			// Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left
			// Field2.Left = Field1.Left + Field1.Width
			// Field3.Width = .ColWidth(2) + 200
			// Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field3.Left = Field2.Left + Field2.Width
			// Field4.Width = .ColWidth(3)
			// Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field4.Left = Field3.Left + Field3.Width
			// Field5.Width = .ColWidth(4)
			// Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field5.Left = Field4.Left + Field4.Width
			// Field6.Width = .ColWidth(5)
			// Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field6.Left = Field5.Left + Field5.Width
			// Field7.Width = .ColWidth(6)
			// Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field7.Left = Field6.Left + Field6.Width
			// Field8.Width = .ColWidth(7)
			// Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field8.Left = Field7.Left + Field7.Width
			// Field9.Width = .ColWidth(8)
			// Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field9.Left = Field8.Left + Field8.Width
			// Field10.Width = .ColWidth(9)
			// Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field10.Left = Field9.Left + Field9.Width
			// 
			// Field12.Width = Field3.Width
			// Field12.Alignment = Field3.Alignment
			// Field12.Left = Field3.Left
			// Field13.Width = Field4.Width
			// Field13.Alignment = Field4.Alignment
			// Field13.Left = Field4.Left
			// Field14.Width = Field5.Width
			// Field14.Alignment = Field5.Alignment
			// Field14.Left = Field5.Left
			// Field15.Width = Field6.Width
			// Field15.Alignment = Field6.Alignment
			// Field15.Left = Field6.Left
			// Field16.Width = Field7.Width
			// Field16.Alignment = Field7.Alignment
			// Field16.Left = Field7.Left
			// Field17.Width = Field8.Width
			// Field17.Alignment = Field8.Alignment
			// Field17.Left = Field8.Left
			// Field18.Width = Field9.Width
			// Field18.Alignment = Field9.Alignment
			// Field18.Left = Field9.Left
			// Field19.Width = Field10.Width
			// Field19.Alignment = Field10.Alignment
			// Field19.Left = Field10.Left
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			PageHeader.Height += 100 / 1440F;
			Detail.Height = vsData.Height / 1440F + 30 / 1440F;
			intRow = 0;
			modPrintToFile.SetPrintProperties(this);
			if (strOrderBy == " order by lastname,firstname ")
			{
				// employee name
				this.GroupHeader1.Visible = false;
				this.GroupFooter1.Visible = false;
			}
			else if (strOrderBy == " order by tblemployeemaster.employeenumber ")
			{
				// employee number
				this.GroupHeader1.Visible = false;
				this.GroupFooter1.Visible = false;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void PageHeader_BeforePrint(object sender, EventArgs e)
		{
			vsHeader.TextMatrix(0, 0, "Employee");
			vsHeader.TextMatrix(0, 1, "Employee");
			vsHeader.TextMatrix(0, 2, "Total");
			vsHeader.TextMatrix(0, 3, "- - - - - - - -");
			vsHeader.TextMatrix(0, 4, "- - - - - - - -");
			vsHeader.TextMatrix(0, 5, "T  a  x  e  s");
			vsHeader.TextMatrix(0, 6, "- - - - - - - -");
			vsHeader.TextMatrix(0, 7, "- - - - - - - -");
			vsHeader.TextMatrix(1, 0, "ID");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 1, "Name");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 2, "- Pay -");
			vsHeader.ColWidth(2, 800);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 3, "Federal");
			vsHeader.ColWidth(3, 800);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 4, "FICA");
			vsHeader.ColWidth(4, 800);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 5, "Medicare");
			vsHeader.ColWidth(5, 800);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 6, "State");
			vsHeader.ColWidth(6, 800);
			vsHeader.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 7, "Local");
			vsHeader.ColWidth(7, 800);
			vsHeader.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 8, "Deducts");
			vsHeader.ColWidth(8, 800);
			vsHeader.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 9, "NET");
			vsHeader.ColWidth(9, 800);
			vsHeader.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, 7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (intSetTotals == 1)
			{
				Field12.Text = Strings.Format(Field12.Text, "#,###,##0.00");
				Field13.Text = Strings.Format(Field13.Text, "#,###,##0.00");
				Field14.Text = Strings.Format(Field14.Text, "#,###,##0.00");
				Field15.Text = Strings.Format(Field15.Text, "#,###,##0.00");
				Field16.Text = Strings.Format(Field16.Text, "#,###,##0.00");
				// Field17.Text = Format(Field17.Text, "#,###,##0.00")
				Field18.Text = Strings.Format(Field18.Text, "#,###,##0.00");
				Field19.Text = Strings.Format(Field19.Text, "#,###,##0.00");
			}
			else
			{
				Field12.Text = Strings.Format(Conversion.Val(Field12.Text) - dblGross, "#,###,##0.00");
				Field13.Text = Strings.Format(Conversion.Val(Field13.Text) - dblFed, "#,###,##0.00");
				Field14.Text = Strings.Format(Conversion.Val(Field14.Text) - dblFica, "#,###,##0.00");
				Field15.Text = Strings.Format(Conversion.Val(Field15.Text) - dblMed, "#,###,##0.00");
				Field16.Text = Strings.Format(Conversion.Val(Field16.Text) - dblState, "#,###,##0.00");
				// Field17.Text = Format(Val(Field17) - dblLocal, "#,###,##0.00")
				Field18.Text = Strings.Format(Conversion.Val(Field18.Text) - dblDeducts, "#,###,##0.00");
				Field19.Text = Strings.Format(Conversion.Val(Field19.Text) - dblNet, "#,###,##0.00");
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			Field12.Text = Strings.Format(dblGross, "0.00");
			Field13.Text = Strings.Format(dblFed, "0.00");
			Field14.Text = Strings.Format(dblFica, "0.00");
			Field15.Text = Strings.Format(dblMed, "0.00");
			Field16.Text = Strings.Format(dblState, "0.00");
			// Field17.Text = dblLocal
			Field18.Text = Strings.Format(dblDeducts, "0.00");
			Field19.Text = Strings.Format(dblNet, "0.00");
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			if (!this.Fields.Contains("grpHeader"))
			{
				this.Fields.Add("grpHeader");
			}
		}
	}
}
