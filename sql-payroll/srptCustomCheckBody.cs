//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Extensions;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptCustomCheckBody.
	/// </summary>
	public partial class srptCustomCheckBody : FCSectionReport
	{
		public srptCustomCheckBody()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Sample Check";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptCustomCheckBody InstancePtr
		{
			get
			{
				return (srptCustomCheckBody)Sys.GetInstance(typeof(srptCustomCheckBody));
			}
		}

		protected srptCustomCheckBody _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCustomCheckBody	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolMuniInfoNonNegotiableOnly;
		private bool boolNonNegotiable;
		private int lngFormatToUse;
		private cEmployeeChecksReport checkReport;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			checkReport = (this.ParentReport as rptEmailPayrollCheck).checksReport;
			Image1.Visible = false;
			Image1.Image = null;
			Image1.Visible = false;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngTop As int	OnWrite(double, int)
			float lngTop = 0;
			// vbPorter upgrade warning: lngLeft As int	OnWriteFCConvert.ToDouble(
			float lngLeft = 0;
			int lngSignatureLeft;
			int lngSignatureTop;
			double dblDirectDeposit;
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                FCFileSystem fso = new FCFileSystem();
                string strLogoFile = "";
                cCustomCheckFormat checkFormat;
                cEmployeeCheck eCheck;
                cEmployee checkEmployee;
                if (!checkReport.employeeChecks.IsCurrent())
                {
                    return;
                }

                checkFormat = checkReport.checkFormat;
                eCheck = (this.ParentReport as rptEmailPayrollCheck).GetCheckByIndex(this.UserData.ToString()
                    .ToIntegerValue());

                boolNonNegotiable = eCheck.NonNegotiable;
                if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "TOWN" ||
                    fecherFoundation.Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "CITY")
                {
                    lblMunicipalName.Text = modGlobalConstants.Statics.gstrCityTown + " of " +
                                            modGlobalConstants.Statics.MuniName;
                }
                else
                {
                    lblMunicipalName.Text = modGlobalConstants.Statics.MuniName;
                }

                lblVoidAfter90Days.Text = checkFormat.VoidAfterMessage;
                checkFormat.CheckFields.MoveFirst();
                cCustomCheckField checkField;
                while (checkFormat.CheckFields.IsCurrent())
                {
                    checkField = (cCustomCheckField) checkFormat.CheckFields.GetCurrentItem();
                    Detail.Controls[checkField.ControlName].Visible = checkField.IncludeField;
                    lngLeft = FCConvert.ToSingle(144 * checkField.XPosition) / 1440F;
                    lngTop = FCConvert.ToSingle(240 * checkField.YPosition) / 1440F;
                    Detail.Controls[checkField.ControlName].Left = lngLeft;
                    Detail.Controls[checkField.ControlName].Top = lngTop;
                    checkFormat.CheckFields.MoveNext();
                }

                // If boolMuniInfoNonNegotiableOnly Then
                // If Not boolNonNegotiable Then
                // lblReturnAddress.Visible = False
                // TXTlOGO.Visible = False
                // imgLogo.Visible = False
                // lblMunicipalName.Visible = False
                // End If
                // End If
                if (txtAddressName.Visible == false)
                {
                    txtAddress2.Visible = false;
                    txtAddress1.Visible = false;
                    txtCityStateZip.Visible = false;
                }

                lngTop = txtAddressName.Top + 240 / 1440F;
                lngLeft = txtAddressName.Left;
                txtAddress1.Left = lngLeft;
                txtAddress1.Top = lngTop;
                lngTop += 240 / 1440F;
                txtAddress2.Left = lngLeft;
                txtAddress2.Top = lngTop;
                lngTop += 240 / 1440F;
                txtCityStateZip.Left = lngLeft;
                txtCityStateZip.Top = lngTop;
                lngLeft = lblStandardVoid2.Left;
                lngTop = lblStandardVoid2.Top;
                if (lngLeft > 0 || lngTop > 0)
                {
                    Image1.Top = (lngTop) - Image1.Height + 240 / 1440F;
                    Image1.Left = lngLeft;
                }

                clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    string straddress = "";
                    straddress =
                        fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress1")));
                    if (fecherFoundation.Strings.Trim(
                        FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress2"))) != "")
                    {
                        if (straddress != "")
                            straddress += "\r\n";
                        straddress +=
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress2")));
                    }

                    if (fecherFoundation.Strings.Trim(
                        FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress3"))) != "")
                    {
                        if (straddress != "")
                            straddress += "\r\n";
                        straddress +=
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress3")));
                    }

                    if (fecherFoundation.Strings.Trim(
                        FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress4"))) != "")
                    {
                        if (straddress != "")
                            straddress += "\r\n";
                        straddress +=
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress4")));
                    }

                    lblReturnAddress.Text = straddress;
                    if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 0 ||
                        (boolMuniInfoNonNegotiableOnly && !boolNonNegotiable))
                    {
                        txtLogo.Visible = false;
                        txtLogo.Image = null;
                    }
                    else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 1)
                    {
                        // town seal
                        // If TXTlOGO.Visible Or Not rptLaserCheck1.boolCustomCheck Then
                        if (txtLogo.Visible)
                        {
                            // Or Not Me.ParentReport.boolCustomCheck Then
                            txtLogo.Visible = true;
                            if (Strings.Left(Environment.CurrentDirectory, 1) != "\\")
                            {
                                strLogoFile = Environment.CurrentDirectory + "\\TownSeal.pic";
                            }
                            else
                            {
                                strLogoFile = Environment.CurrentDirectory + "TownSeal.pic";
                            }

                            if (strLogoFile != string.Empty)
                            {
                                if (FCFileSystem.FileExists(strLogoFile))
                                {
                                    txtLogo.Image = FCUtils.LoadPicture(strLogoFile);
                                }
                            }
                        }
                    }
                    else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 2)
                    {
                        // custom image
                        if (txtLogo.Visible || !rptLaserCheck1.InstancePtr.boolCustomCheck)
                        {
                            txtLogo.Visible = true;
                            strLogoFile =
                                fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("checklogo")));
                            if (strLogoFile != string.Empty)
                            {
                                if (FCFileSystem.FileExists(strLogoFile))
                                {
                                    txtLogo.Image = FCUtils.LoadPicture(strLogoFile);
                                }
                            }
                        }
                    }
                }

                // End If
                // End With
                // 
                // If Val(.TextMatrix(0, 30)) = 0 And (Not rptLaserCheck1.clsFormat.Fields("customsetup") = True) Then
                // txtDate.Top = txtDate.Top + 720 'three lines
                // txtCheckAmount.Top = txtCheckAmount.Top + 720
                // txtDate.Left = txtDate.Left - 288 'two characters
                // End If
                string strCheckAmount = "";
                dblDirectDeposit = eCheck.CheckingAmount + eCheck.SavingsAmount;
                if (eCheck.CheckAmount < 0.01)
                {
                    txtAmountString.Text = "** VOID *** VOID *** VOID *** VOID *** VOID *** VOID *** VOID **";
                    boolNonNegotiable = true;
                    if (Strings.Format(eCheck.Net, "0.00") == Strings.Format(dblDirectDeposit, "0.00"))
                    {
                        // MATTHEW 6/15/2004
                        // the whole check is a direct deposit
                        strCheckAmount = Strings.Format(dblDirectDeposit, "0.00");
                    }
                    else
                    {
                        strCheckAmount = Strings.Format(eCheck.Net - dblDirectDeposit, "0.00");
                    }

                    strCheckAmount = "$" + Strings.StrDup(13 - strCheckAmount.Length, "*") + strCheckAmount;
                }
                else
                {
                    txtAmountString.Text =
                        modConvertAmountToString.ConvertAmountToString(
                            FCConvert.ToDecimal(eCheck.Net - dblDirectDeposit)) + " ";
                    if (txtAmountString.Text.Length > 64)
                    {
                        // just leave it alone
                    }
                    else
                    {
                        txtAmountString.Text =
                            txtAmountString.Text + Strings.StrDup(65 - txtAmountString.Text.Length, "*");
                    }

                    strCheckAmount = Strings.Format(eCheck.Net - dblDirectDeposit, "0.00");
                    strCheckAmount = "$" + Strings.StrDup(13 - strCheckAmount.Length, "*") + strCheckAmount;
                }

                checkEmployee = eCheck.Employee;
                txtAddressName.Text = checkEmployee.FullName;
                txtName.Text = checkEmployee.FullName;
                txtAddress1.Text = checkEmployee.Address1;
                txtAddress2.Text = checkEmployee.Address2;
                if (txtAddressName.Visible)
                {
                    if (txtAddress2.Text == string.Empty)
                    {
                        txtCityStateZip.Top = txtAddress2.Top;
                        txtAddress2.Visible = false;
                    }
                    else
                    {
                        txtAddress2.Visible = true;
                    }
                }

                fldBankName.Text = checkReport.BankName;
                txtCityStateZip.Text = fecherFoundation.Strings.Trim(
                    fecherFoundation.Strings.Trim(checkEmployee.City + " " + checkEmployee.State + " " +
                                                  checkEmployee.Zip) + " " + checkEmployee.Zip4);
                txtEmployeeNumCheck.Text = eCheck.EmployeeNumber;
                txtCheckAmount.Text = strCheckAmount;
                txtCheckNumber.Text = FCConvert.ToString(eCheck.CheckNumber);
                fldLargeCheckNumber.Text = FCConvert.ToString(eCheck.CheckNumber);
                txtDate.Text = FCConvert.ToString(eCheck.CheckDate);
                lblLaserVoid.Visible = false;
                lblLaserVoid2.Visible = false;
                lblStandardVoid1.Visible = false;
                lblStandardVoid2.Visible = false;
                dblDirectDeposit = eCheck.CheckingAmount + eCheck.SavingsAmount;
                string strRoutingNumber = "";
                string strAccount = "";
                lblLaserVoid2.Visible = true;
                lblLaserVoid.Visible = true;
                Image1.Visible = false;
                fldMICRLine.Text = "";
            }
        }

		
	}
}
