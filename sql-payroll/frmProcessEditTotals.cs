//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmProcessEditTotals : BaseForm
	{
		public frmProcessEditTotals()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmProcessEditTotals InstancePtr
		{
			get
			{
				return (frmProcessEditTotals)Sys.GetInstance(typeof(frmProcessEditTotals));
			}
		}

		protected frmProcessEditTotals _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       DECEMBER 27, 2002
		//
		// MODIFIED BY:
		// NOTES:
		//
		//
		// **************************************************
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;

		private void frmProcessEditTotals_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmProcessEditTotals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the key press is ESC then close out this form
				if (KeyAscii == Keys.Escape)
					Close();
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmProcessEditTotals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmProcessEditTotals properties;
			//frmProcessEditTotals.ScaleWidth	= 6105;
			//frmProcessEditTotals.ScaleHeight	= 4020;
			//frmProcessEditTotals.LinkTopic	= "Form1";
			//frmProcessEditTotals.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (!rsTemp.EndOfFile())
				{
					modGlobalRoutines.Statics.ggboolWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayFreqWeekly")) ? true : false);
					modGlobalRoutines.Statics.ggboolBiWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayFreqBiWeekly")) ? true : false);
				}
				rsTemp.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				if (!rsTemp.EndOfFile())
				{
					modGlobalRoutines.Statics.ggboolPRBiWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("WeeklyPR")) ? true : false);
				}
				rsTemp.OpenRecordset("Select * from tblPayrollProcessSetup");
				if (!rsTemp.EndOfFile())
				{
					modGlobalRoutines.Statics.ggintWeekNumber = FCConvert.ToInt16(rsTemp.Get_Fields("WeekNumber"));
					modGlobalRoutines.Statics.ggintNumberPayWeeks = FCConvert.ToInt16(rsTemp.Get_Fields_Int32("NumberOfPayWeeks"));
					modGlobalRoutines.Statics.ggboolSetupBiWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayBiWeeklys")) ? true : false);
					// pay run is to pay weeklys with bi's
					modGlobalRoutines.Statics.ggboolBiWeeklyDeds = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("BiWeeklyDeds")) ? true : false);
				}
				// set the grid column headers/widths/etc....
				SetGridProperties();
				LoadData();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 2);
				modGlobalFunctions.SetTRIOColors(this);
				txtHours.ForeColor = Color.Blue;
				txtDollars.ForeColor = Color.Blue;
				lblTotals.ForeColor = Color.Blue;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsGrid.FixedCols = 0;
				vsGrid.FixedRows = 0;
				vsGrid.Cols = 3;
				vsGrid.Rows = 2;
				// Adjust the widths of the columns to be a
				// percentage of the grid with itself
				vsGrid.ColWidth(0, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.57));
				vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
				vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
				// .ColWidth(0) = 2500
				// .ColWidth(1) = 1650
				// .ColWidth(2) = 1500
				vsGrid.FixedRows = 2;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadData()
		{
            var rsMaster = new clsDRWrapper();
            var rsData = new clsDRWrapper();
            try
            {
                // On Error GoTo CallErrorRoutine
                fecherFoundation.Information.Err().Clear();
                modErrorHandler.Statics.gstrCurrentRoutine = "LoadData";
                clsMousePointer Mouse;
                Mouse = new clsMousePointer();
                int intCounter = 0;
                int intCatNumber = 0;
                double dblTotal = 0;
                double dblDollarsTotal = 0;
                string strEmployeeNumber = "";
               
                double dblGrandTotal = 0;
                double dblGrandDollarsTotal = 0;
                bool boolOkay = false;
                string strFind;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;

                // get all employees
                strFind = "";
                int lngID;
                lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());


                rsMaster.OpenRecordset("select * from payrollpermissions where [userid] = " + FCConvert.ToString(lngID), "twpy0000.vb1");

                while (!rsMaster.EndOfFile())
                {
                    switch (rsMaster.Get_Fields_Int32("type"))
                    {
                        case CNSTPYTYPEDEPTDIV:
                        {
                            strFind += "not isnull(deptdiv,'') = '" + rsMaster.Get_Fields("val") + "' and ";

                            break;
                        }
                        case CNSTPYTYPEGROUP:
                        {
                            strFind += "not groupid = '" + rsMaster.Get_Fields("val") + "' and ";

                            break;
                        }
                        case CNSTPYTYPEIND:
                        {
                            strFind += "not isnull(employeenumber,'') = '" + rsMaster.Get_Fields("val") + "' and ";

                            break;
                        }
                        case CNSTPYTYPESEQ:
                        {
                            strFind += "not isnull(seqnumber,0) = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("val"))) + " and ";

                            break;
                        }
                        case CNSTPYTYPEDEPARTMENT:
                        {
                            strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("val"))) + " and ";

                            break;
                        }
                    }

                    //end switch
                    rsMaster.MoveNext();
                }

                if (strFind != string.Empty)
                {
                    strFind = Strings.Mid(strFind, 1, strFind.Length - 4);
                    strFind = " where " + strFind;
                }

                rsMaster.OpenRecordset("Select * from tblEmployeeMaster" + strFind, modGlobalVariables.DEFAULTDATABASE);
                vsGrid.TextMatrix(0, 0, "");
                vsGrid.TextMatrix(0, 1, "Entered");
                vsGrid.TextMatrix(0, 2, "Entered");
                vsGrid.TextMatrix(1, 0, "Pay Category");
                vsGrid.TextMatrix(1, 1, "Hours");
                vsGrid.TextMatrix(1, 2, "Dollars");

                //FC:FINAL:BSE:#4144 fix columns alignment
                vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                vsGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                vsGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
                vsGrid.MergeRow(0, true);
                vsGrid.MergeRow(1, true);

                //vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                //vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                // get the distributions
                // Call rsData.OpenRecordset("Select * from tblPayrollDistribution where DefaultHours <> HoursWeek Order by Cat", DEFAULTDATABASE)
                // If Not rsData.EndOfFile Then
                // intCatNumber = rsData.Fields("CAT")
                // intCounter = 0
                // Else
                // Exit Sub
                // End If
                // Call rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours <> [HoursWeek]) AND BaseRate <> 0) Or (tblPayCategories.Type = 'DOLLARS' and HoursWeek <> 0)) ORDER BY tblPayrollDistribution.Cat", DEFAULTDATABASE)
                rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours <> [HoursWeek]) AND BaseRate <> 0) Or (tblPayCategories.Type = 'DOLLARS' and HoursWeek <> 0)) and isnull(accountcode,'') <> '1' ORDER BY tblPayrollDistribution.Cat", modGlobalVariables.DEFAULTDATABASE);

                if (rsData.EndOfFile())
                {
                    goto CheckDefaultHours;
                }
                else
                {
                    intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("CAT"));
                    intCounter = 2;
                }

                while (!rsData.EndOfFile())
                {
                    strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
                    rsMaster.FindFirstRecord("EmployeeNumber", strEmployeeNumber);

                    if (rsMaster.NoMatch)
                    {
                        goto NextRecord;
                    }
                    else
                    {
                        // check the status and frequency
                        if (FCConvert.ToString(rsMaster.Get_Fields_String("Status")) != "Active")
                        {
                            goto NextRecord;
                        }
                        else
                        {
                            if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strEmployeeNumber))
                            {
                                boolOkay = false;

                                if (Conversion.Val(rsData.Get_Fields("accountcode")) != 13)
                                {
                                    boolOkay = true;
                                }
                                else
                                {
                                    if (modGlobalRoutines.Statics.ggboolBiWeekly)
                                    {
                                        if (modGlobalRoutines.Statics.ggintWeekNumber < 3)
                                        {
                                            boolOkay = true;
                                        }
                                    }
                                    else
                                    {
                                        if (modGlobalRoutines.Statics.ggintWeekNumber < 5)
                                        {
                                            boolOkay = true;
                                        }
                                    }
                                }

                                if (boolOkay)
                                {
                                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;

                                    // everything is ok so add this to the totals
                                    if (intCatNumber != FCConvert.ToInt32(rsData.Get_Fields_Int32("CAT")))
                                    {
                                        vsGrid.Rows += 1;
                                        vsGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollCategory(intCatNumber)));
                                        vsGrid.TextMatrix(intCounter, 1, Strings.Format(dblTotal, "0.00"));
                                        vsGrid.TextMatrix(intCounter, 2, Strings.Format(dblDollarsTotal, "0.00"));
                                        dblGrandTotal += dblTotal;
                                        dblGrandDollarsTotal += dblDollarsTotal;
                                        intCounter += 1;
                                        intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("CAT"));
                                        dblTotal = 0;
                                        dblDollarsTotal = 0;
                                    }

                                    if (FCConvert.ToString(rsData.Get_Fields("Type")) == "Dollars")
                                    {
                                        dblDollarsTotal += Conversion.Val(rsData.Get_Fields_Decimal("BaseRate")) * Conversion.Val(rsData.Get_Fields("hoursweek"));
                                    }
                                    else
                                    {
                                        dblTotal += Conversion.Val(rsData.Get_Fields_Double("HoursWeek"));
                                    }

                                    // dblTotal = dblTotal + Val(rsData.Fields("HoursWeek"))
                                }
                            }
                            else
                            {
                                // GoTo NextRecord
                            }
                        }
                    }

                    NextRecord: ;
                    rsData.MoveNext();
                }

                CheckDefaultHours: ;

                // now set the last record
                if (dblGrandTotal == 0 && dblGrandDollarsTotal == 0 && dblTotal == 0 && dblDollarsTotal == 0)
                { }
                else
                {
                    vsGrid.Rows += 1;
                    vsGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollCategory(intCatNumber)));
                    vsGrid.TextMatrix(intCounter, 1, Strings.Format(dblTotal, "0.00"));
                    vsGrid.TextMatrix(intCounter, 2, Strings.Format(dblDollarsTotal, "0.00"));
                    dblGrandTotal += dblTotal;
                    dblGrandDollarsTotal += dblDollarsTotal;
                }

                dblTotal = 0;

                // Call rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours = [HoursWeek]) AND BaseRate <> 0 AND HoursWeek <> 0) Or (tblPayCategories.Type = 'DOLLARS' and HoursWeek <> 0)) ORDER BY tblPayrollDistribution.Cat", DEFAULTDATABASE)
                rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours = [HoursWeek]) AND BaseRate <> 0 AND HoursWeek <> 0) Or (tblPayCategories.Type = 'DOLLARS' and HoursWeek <> 0)) and accountcode <> '1' ORDER BY tblPayrollDistribution.Cat", modGlobalVariables.DEFAULTDATABASE);

                while (!rsData.EndOfFile())
                {
                    strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));

                    if (Conversion.Val(strEmployeeNumber) == 1)
                    {
                        strEmployeeNumber = strEmployeeNumber;
                    }

                    rsMaster.FindFirstRecord("EmployeeNumber", strEmployeeNumber);

                    if (rsMaster.NoMatch)
                    {
                        // GoTo NextRecord
                    }
                    else
                    {
                        // check the status and frequency
                        if (FCConvert.ToString(rsMaster.Get_Fields_String("Status")) != "Active")
                        {
                            goto NextRecord2;
                        }
                        else
                        {
                            if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strEmployeeNumber))
                            {
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;

                                if (FCConvert.ToString(rsData.Get_Fields("Type")) == "Dollars")
                                { }
                                else
                                {
                                    dblTotal += FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Double("HoursWeek"), "0.00"));
                                }
                            }
                        }
                    }

                    NextRecord2: ;
                    rsData.MoveNext();
                }

                // THIS IS
                if (dblTotal != 0)
                {
                    intCounter += 1;
                    vsGrid.Rows += 1;
                    vsGrid.TextMatrix(vsGrid.Rows - 1, 0, "DEFAULT");
                    vsGrid.TextMatrix(vsGrid.Rows - 1, 1, Strings.Format(dblTotal, "0.00"));
                    dblGrandTotal += FCConvert.ToDouble(Strings.Format(dblTotal, "0.00"));
                    vsGrid.TextMatrix(vsGrid.Rows - 1, 2, Strings.Format(0, "0.00"));
                    vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsGrid.Rows - 1, 0, vsGrid.Rows - 1, vsGrid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                }

                txtHours.Text = Strings.Format(dblGrandTotal, "0.00");
                txtDollars.Text = Strings.Format(dblGrandDollarsTotal, "0.00");
               
            }
            catch (Exception ex)
            {
                modErrorHandler.SetErrorHandler(ex);
            }
            finally
            {
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                rsData.DisposeOf();
                rsMaster.DisposeOf();
            }
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private object GetPayrollCategory(int intValue)
		{
			string _GetPayrollCategory = null;
            var rsData = new clsDRWrapper();

            try
            {
                // On Error GoTo CallErrorRoutine
                fecherFoundation.Information.Err().Clear();
                modErrorHandler.Statics.gstrCurrentRoutine = "GetPayrollCategory";
                clsMousePointer Mouse;
                Mouse = new clsMousePointer();
                rsData.OpenRecordset("Select Description from tblPayCategories where ID = " + FCConvert.ToString(intValue), modGlobalVariables.DEFAULTDATABASE);
                _GetPayrollCategory = rsData.EndOfFile() ? string.Empty : (string) rsData.Get_Fields("Description");
            }
            catch (Exception ex)
            {
                modErrorHandler.SetErrorHandler(ex);
            }
            finally
            {
                rsData.DisposeOf();
            }

            return _GetPayrollCategory;

		}

		private void frmProcessEditTotals_Resize(object sender, System.EventArgs e)
		{
			// Adjust the widths of the columns to be a
			// percentage of the grid with itself
			vsGrid.ColWidth(0, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.56));
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
		}

		private void mnuCalculate_Click(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.CheckIfPayProcessCompleted("DataEntry", "EditTotals"))
			{
				modGlobalRoutines.UpdatePayrollStepTable("EditTotals");
				// frmProcessDisplay.Show
				// If pboolUnloadProcessGrid Then
				// Else
				// frmProcessDisplay.mnuProcess_Click
				// End If
				modGlobalVariables.Statics.pboolUnloadProcessGrid = true;
				frmProcessDisplay.InstancePtr.Unload();
				modGlobalVariables.Statics.pboolUnloadProcessGrid = false;
				frmProcessDisplay.InstancePtr.Init();
				modGlobalVariables.Statics.pboolUnloadProcessGrid = false;
			}
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrintScreen_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptPrintProcessEditTotals.InstancePtr, boolAllowEmail: true, strAttachmentName: "ProcessEditTotals");
			// rptPrintProcessEditTotals.Show 1, MDIParent
		}

		private void mnuSummary_Click(object sender, System.EventArgs e)
		{
			frmProcessEditSummary.InstancePtr.Show(App.MainForm);
			Close();
		}
	}
}
