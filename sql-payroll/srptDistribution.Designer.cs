﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDistribution.
	/// </summary>
	partial class srptDistribution
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDistribution));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtU = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBaseRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMult = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDefHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCode,
            this.txtAccount,
            this.txtCat,
            this.txtTax,
            this.txtM,
            this.txtU,
            this.txtWC,
            this.txtCD,
            this.txtBaseRate,
            this.txtMult,
            this.txtDefHours});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.19F;
			this.txtCode.Left = 0.0625F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Text = null;
			this.txtCode.Top = 0.03125F;
			this.txtCode.Width = 0.3125F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.5F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 1.125F;
			// 
			// txtCat
			// 
			this.txtCat.Height = 0.19F;
			this.txtCat.Left = 1.6875F;
			this.txtCat.Name = "txtCat";
			this.txtCat.Text = null;
			this.txtCat.Top = 0.03125F;
			this.txtCat.Width = 0.3125F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 2.125F;
			this.txtTax.Name = "txtTax";
			this.txtTax.Style = "text-align: center";
			this.txtTax.Text = null;
			this.txtTax.Top = 0.03125F;
			this.txtTax.Width = 0.25F;
			// 
			// txtM
			// 
			this.txtM.Height = 0.19F;
			this.txtM.Left = 2.5F;
			this.txtM.Name = "txtM";
			this.txtM.Text = null;
			this.txtM.Top = 0.03125F;
			this.txtM.Width = 0.5F;
			// 
			// txtU
			// 
			this.txtU.Height = 0.19F;
			this.txtU.Left = 3.4375F;
			this.txtU.Name = "txtU";
			this.txtU.Text = null;
			this.txtU.Top = 0.03125F;
			this.txtU.Width = 0.5F;
			// 
			// txtWC
			// 
			this.txtWC.Height = 0.19F;
			this.txtWC.Left = 4F;
			this.txtWC.Name = "txtWC";
			this.txtWC.Text = null;
			this.txtWC.Top = 0.03125F;
			this.txtWC.Width = 0.3125F;
			// 
			// txtCD
			// 
			this.txtCD.Height = 0.19F;
			this.txtCD.Left = 4.4375F;
			this.txtCD.Name = "txtCD";
			this.txtCD.Text = null;
			this.txtCD.Top = 0.03125F;
			this.txtCD.Width = 0.3125F;
			// 
			// txtBaseRate
			// 
			this.txtBaseRate.Height = 0.19F;
			this.txtBaseRate.Left = 4.8125F;
			this.txtBaseRate.Name = "txtBaseRate";
			this.txtBaseRate.Style = "text-align: right";
			this.txtBaseRate.Text = null;
			this.txtBaseRate.Top = 0.03125F;
			this.txtBaseRate.Width = 0.625F;
			// 
			// txtMult
			// 
			this.txtMult.Height = 0.19F;
			this.txtMult.Left = 5.5F;
			this.txtMult.Name = "txtMult";
			this.txtMult.Style = "text-align: right";
			this.txtMult.Text = null;
			this.txtMult.Top = 0.03125F;
			this.txtMult.Width = 0.375F;
			// 
			// txtDefHours
			// 
			this.txtDefHours.Height = 0.19F;
			this.txtDefHours.Left = 5.9375F;
			this.txtDefHours.Name = "txtDefHours";
			this.txtDefHours.Style = "text-align: right";
			this.txtDefHours.Text = null;
			this.txtDefHours.Top = 0.03125F;
			this.txtDefHours.Width = 0.375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0.19F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape3,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label32,
            this.Label34,
            this.Label35,
            this.Line1,
            this.Line2,
            this.Line3,
            this.Line4});
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.3541667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Shape3
			// 
			this.Shape3.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
			this.Shape3.Height = 0.3125F;
			this.Shape3.Left = 0F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.03125F;
			this.Shape3.Width = 7.125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.21875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 9pt; font-weight: bold";
			this.Label21.Text = "Distribution";
			this.Label21.Top = 0.03125F;
			this.Label21.Width = 1.625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.21875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.0625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label22.Text = "Code";
			this.Label22.Top = 0.1875F;
			this.Label22.Width = 0.4375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.5F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label23.Text = "Account";
			this.Label23.Top = 0.1875F;
			this.Label23.Width = 0.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.6875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label24.Text = "CAT";
			this.Label24.Top = 0.1875F;
			this.Label24.Width = 0.3125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.21875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 3.4375F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label25.Text = "U";
			this.Label25.Top = 0.1875F;
			this.Label25.Width = 0.5F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.21875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 4F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label26.Text = "W/C";
			this.Label26.Top = 0.1875F;
			this.Label26.Width = 0.3125F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.21875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.5F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label27.Text = "Mult";
			this.Label27.Top = 0.1875F;
			this.Label27.Width = 0.375F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 4.8125F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label28.Text = "Base Rate";
			this.Label28.Top = 0.1875F;
			this.Label28.Width = 0.625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.21875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 4.4375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label29.Text = "CD";
			this.Label29.Top = 0.1875F;
			this.Label29.Width = 0.3125F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.28125F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.9375F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label30.Text = "Def Hours";
			this.Label30.Top = 0.0625F;
			this.Label30.Width = 0.375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.21875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 2.5F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label31.Text = "M";
			this.Label31.Top = 0.1875F;
			this.Label31.Width = 0.375F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.21875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 2.125F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label32.Text = "Tax";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 0.375F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 3.25F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label34.Text = "Include For";
			this.Label34.Top = 0.0625F;
			this.Label34.Width = 0.875F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.19F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 4.75F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label35.Text = "Pay";
			this.Label35.Top = 0.0625F;
			this.Label35.Width = 0.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.125F;
			this.Line1.Width = 0.1875F;
			this.Line1.X1 = 3.0625F;
			this.Line1.X2 = 3.25F;
			this.Line1.Y1 = 0.125F;
			this.Line1.Y2 = 0.125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 0.1875F;
			this.Line2.X1 = 4.125F;
			this.Line2.X2 = 4.3125F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.4375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.125F;
			this.Line3.Width = 0.3125F;
			this.Line3.X1 = 4.4375F;
			this.Line3.X2 = 4.75F;
			this.Line3.Y1 = 0.125F;
			this.Line3.Y2 = 0.125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 5.125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.125F;
			this.Line4.Width = 0.3125F;
			this.Line4.X1 = 5.125F;
			this.Line4.X2 = 5.4375F;
			this.Line4.Y1 = 0.125F;
			this.Line4.Y2 = 0.125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// srptDistribution
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtU;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBaseRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMult;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDefHours;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
