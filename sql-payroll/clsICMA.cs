//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public class clsICMA
	{
		//=========================================================
		private modCoreysSweeterCode.ICMAPlanRecord1 PlanRecord1 = new modCoreysSweeterCode.ICMAPlanRecord1();
		private modCoreysSweeterCode.ICMAPlanRecord2 PlanRecord2 = new modCoreysSweeterCode.ICMAPlanRecord2();
		private modCoreysSweeterCode.ICMAContributionRecord1 ContributionRecord1 = new modCoreysSweeterCode.ICMAContributionRecord1();
		private modCoreysSweeterCode.ICMAContributionRecord2 ContributionRecord2 = new modCoreysSweeterCode.ICMAContributionRecord2();
		private modCoreysSweeterCode.ICMALoanRepaymentRecord1 LoanRepaymentRecord1 = new modCoreysSweeterCode.ICMALoanRepaymentRecord1();
		private modCoreysSweeterCode.ICMALoanRepaymentRecord2 LoanRepaymentRecord2 = new modCoreysSweeterCode.ICMALoanRepaymentRecord2();
		private StreamWriter ts;
		private bool boolFileOpen;
		private FCFileSystem fso = new FCFileSystem();
		private string strFileList = "";

		public string GetFileList()
		{
			string GetFileList = "";
			GetFileList = strFileList;
			return GetFileList;
		}

		private void SavePlanRecordSet()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = CreatePlanRecord1();
				ts.WriteLine(strLine);
				strLine = CreatePlanRecord2();
				ts.WriteLine(strLine);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SavePlanRecordSet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveContributionRecordSet()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = CreateContributionRecord1();
				ts.WriteLine(strLine);
				strLine = CreateContributionRecord2();
				ts.WriteLine(strLine);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveContributionRecordSet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveLoanRepaymentRecordSet()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = CreateLoanRepaymentRecord1();
				ts.WriteLine(strLine);
				strLine = CreateLoanRepaymentRecord2();
				ts.WriteLine(strLine);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveLoanRepaymentRecordSet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string CreateLoanRepaymentRecord1()
		{
			string CreateLoanRepaymentRecord1 = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn = "";
				CreateLoanRepaymentRecord1 = "";
				strReturn = Strings.Format(LoanRepaymentRecord1.RCPlanNumber, "000000");
				strReturn += Strings.Format(LoanRepaymentRecord1.RecordType, "00");
				strReturn += Strings.Format(LoanRepaymentRecord1.RecordSequence, "0000");
				strReturn += Strings.StrDup(4, " ");
				strReturn += LoanRepaymentRecord1.ParticipantSSN;
				strReturn += Strings.Left(LoanRepaymentRecord1.ParticipantName + Strings.StrDup(30, " "), 30);
				strReturn += Strings.StrDup(24, " ");
				strReturn += LoanRepaymentRecord1.FormatID;
				CreateLoanRepaymentRecord1 = strReturn;
				return CreateLoanRepaymentRecord1;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateLoanRepaymentRecord1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateLoanRepaymentRecord1;
		}

		private string CreateLoanRepaymentRecord2()
		{
			string CreateLoanRepaymentRecord2 = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn = "";
				CreateLoanRepaymentRecord2 = "";
				strReturn = Strings.Format(LoanRepaymentRecord2.RCPlanNumber, "000000");
				strReturn += Strings.Format(LoanRepaymentRecord2.RecordType, "00");
				strReturn += Strings.Format(LoanRepaymentRecord2.RecordSequence, "0000");
				strReturn += " ";
				strReturn += Strings.Format(LoanRepaymentRecord2.LoanNumber, "000");
				strReturn += LoanRepaymentRecord2.ParticipantSSN;
				strReturn += Strings.Format(LoanRepaymentRecord2.LoanRepaymentAmount, "0000000000");
				strReturn += Strings.StrDup(40, " ");
				strReturn += Strings.Left(LoanRepaymentRecord2.LoanPayoffIndicator + "    ", 4);
				strReturn += LoanRepaymentRecord2.FormatID;
				CreateLoanRepaymentRecord2 = strReturn;
				return CreateLoanRepaymentRecord2;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateLoanRepaymentRecord2", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateLoanRepaymentRecord2;
		}

		private string CreateContributionRecord1()
		{
			string CreateContributionRecord1 = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn = "";
				CreateContributionRecord1 = "";
				strReturn = Strings.Format(ContributionRecord1.RCPlanNumber, "000000");
				strReturn += Strings.Format(ContributionRecord1.RecordType, "00");
				strReturn += Strings.Format(ContributionRecord1.RecordSequence, "0000");
				strReturn += Strings.StrDup(4, " ");
				strReturn += ContributionRecord1.ParticipantSSN;
				strReturn += Strings.Left(ContributionRecord1.ParticipantName + Strings.StrDup(30, " "), 30);
				strReturn += Strings.StrDup(24, " ");
				strReturn += ContributionRecord1.FormatID;
				CreateContributionRecord1 = strReturn;
				return CreateContributionRecord1;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateContributionRecord1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateContributionRecord1;
		}

		private string CreateContributionRecord2()
		{
			string CreateContributionRecord2 = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn = "";
				CreateContributionRecord2 = "";
				strReturn = Strings.Format(ContributionRecord2.RCPlanNumber, "000000");
				strReturn += Strings.Format(ContributionRecord2.RecordType, "00");
				strReturn += Strings.Format(ContributionRecord2.RecordSequence, "0000");
				strReturn += Strings.Left(ContributionRecord2.FundID + "  ", 2);
				strReturn += Strings.Left(ContributionRecord2.SourceCode + "  ", 2);
				strReturn += ContributionRecord2.ParticipantSSN;
				strReturn += Strings.Format(ContributionRecord2.ContributionAmount, "0000000000");
				strReturn += Strings.Left(ContributionRecord2.TaxYearIndicator + " ", 1);
				strReturn += Strings.StrDup(39, " ");
				strReturn += Strings.Left(ContributionRecord2.LocationID + "    ", 4);
				// location id not used at the moment
				strReturn += ContributionRecord2.FormatID;
				CreateContributionRecord2 = strReturn;
				return CreateContributionRecord2;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateContributionRecord2", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateContributionRecord2;
		}

		private string CreatePlanRecord1()
		{
			string CreatePlanRecord1 = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn = "";
				CreatePlanRecord1 = "";
				strReturn = Strings.Format(PlanRecord1.RCPlanNumber, "000000");
				strReturn += PlanRecord1.RecordType;
				strReturn += PlanRecord1.RecordSequence;
				strReturn += Strings.StrDup(4, " ");
				PlanRecord1.IRSNumber = Strings.Replace(PlanRecord1.IRSNumber, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += Strings.Format(PlanRecord1.IRSNumber, "000000000");
				strReturn += Strings.Left(PlanRecord1.PlanName + Strings.StrDup(30, " "), 30);
				strReturn += Strings.StrDup(24, " ");
				strReturn += Strings.Format(PlanRecord1.FormatID, "0");
				CreatePlanRecord1 = strReturn;
				return CreatePlanRecord1;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreatePlanRecord1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreatePlanRecord1;
		}

		private string CreatePlanRecord2()
		{
			string CreatePlanRecord2 = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn = "";
				CreatePlanRecord2 = "";
				strReturn = Strings.Format(PlanRecord2.RCPlanNumber, "000000");
				strReturn += PlanRecord2.RecordType;
				strReturn += Strings.Format(PlanRecord2.RecordSequence, "0000");
				strReturn += Strings.StrDup(4, " ");
				PlanRecord2.IRSNumber = Strings.Replace(PlanRecord2.IRSNumber, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += Strings.Format(PlanRecord2.IRSNumber, "000000000");
				strReturn += Strings.Format(PlanRecord2.TotalRemittance, "0000000000");
				strReturn += Strings.StrDup(25, " ");
				strReturn += Strings.Format(PlanRecord2.FormatCode, "00");
				strReturn += Strings.StrDup(6, " ");
				strReturn += PlanRecord2.PayrollDate;
				strReturn += Strings.Left(PlanRecord2.TaxYearIndicator + " ", 1);
				strReturn += Strings.StrDup(2, " ");
				strReturn += Strings.Format(PlanRecord2.FormatID, "0");
				CreatePlanRecord2 = strReturn;
				return CreatePlanRecord2;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreatePlanRecord2", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreatePlanRecord2;
		}

		public bool CreateFiles(ref DateTime dtPayDate)
		{
			bool CreateFiles = false;
			clsDRWrapper clsPlans = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CreateFiles = false;
				strFileList = "";
				strSQL = "select rcplan from ICMAINFORMATION group by rcplan order BY rcplan";
				clsPlans.OpenRecordset(strSQL, "twpy0000.vb1");
				if (clsPlans.EndOfFile())
				{
					MessageBox.Show("No ICMA plans have been set up", "No Plans", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateFiles;
				}
				while (!clsPlans.EndOfFile())
				{
					SavePlanFile(dtPayDate, FCConvert.ToString(clsPlans.Get_Fields("rcplan")));
					clsPlans.MoveNext();
				}
				if (fecherFoundation.Strings.Trim(strFileList) != string.Empty)
				{
					// trim off the extra comma
					strFileList = Strings.Mid(strFileList, 1, strFileList.Length - 1);
				}
				CreateFiles = true;
				return CreateFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
				{
					boolFileOpen = false;
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateFiles;
		}

		private bool StartFile(string strPlanNumber)
		{
			bool StartFile = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				StartFile = false;
				string temp = Path.Combine(FCFileSystem.Statics.UserDataFolder, "ICMA");
				if (!Directory.Exists(temp))
				{
					Directory.CreateDirectory(temp);
				}
				if (FCFileSystem.FileExists("ICMA/" + strPlanNumber + "RC.prn"))
				{
					FCFileSystem.DeleteFile("ICMA/" + strPlanNumber + "RC.prn");
				}
                //ts = fso.CreateTextFile("ICMA/" + strPlanNumber + "RC.prn", true, false);
                ts = FCFileSystem.CreateTextFile("ICMA/" + strPlanNumber + "RC.prn");
                strFileList += strPlanNumber + "RC.prn,";
				boolFileOpen = true;
				StartFile = true;
				return StartFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In StartFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return StartFile;
		}

		private void EndFile()
		{
			ts.Close();
			boolFileOpen = false;
		}
		// vbPorter upgrade warning: strPlanNumber As string	OnRead(int, string)
		private bool SavePlanFile(DateTime dtPayDate, string strPlanNumber)
		{
			bool SavePlanFile = false;
			SavePlanFile = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsPlan = new clsDRWrapper();
				string strSQL;
				bool boolSavedPlan;
				double dblPlanTotal;
				double dblReturn = 0;
				string strTemp = "";
				// don't load loan records
				strSQL = "Select source from icmainformation where SOURCE <> 'LN' and rcplan = " + strPlanNumber + " group by source ";
				clsPlan.OpenRecordset(strSQL, "twpy0000.vb1");
				boolSavedPlan = false;
				// find out if there are any records in check detail
				dblPlanTotal = 0;
				while (!clsPlan.EndOfFile())
				{
					dblReturn = SaveContributionRecords(dtPayDate, strPlanNumber, FCConvert.ToString(clsPlan.Get_Fields("source")), boolSavedPlan);
					if (dblReturn != 0)
					{
						dblPlanTotal += dblReturn;
						boolSavedPlan = true;
					}
					clsPlan.MoveNext();
				}
				dblReturn = SaveLoanRecords(dtPayDate, strPlanNumber, boolSavedPlan);
				if (dblReturn != 0)
				{
					dblPlanTotal += dblReturn;
					boolSavedPlan = true;
				}
				clsPlan.OpenRecordset("select * from icmainformation where rcplan = " + strPlanNumber, "twpy0000.vb1");
				if (boolSavedPlan)
				{
					PlanRecord1.FormatID = "3";
					PlanRecord1.RCPlanNumber = FCConvert.ToInt32(strPlanNumber);
					PlanRecord1.RecordSequence = "0001";
					PlanRecord1.RecordType = "01";
					strTemp = FCConvert.ToString(clsPlan.Get_Fields("planname"));
					strTemp = Strings.Left(strTemp + Strings.StrDup(30, " "), 30);
					PlanRecord1.PlanName = strTemp;
					PlanRecord1.IRSNumber = Strings.Left(clsPlan.Get_Fields("irsnumber") + Strings.StrDup(9, "0"), 9);
					PlanRecord2.FormatCode = "03";
					PlanRecord2.FormatID = "3";
					PlanRecord2.RCPlanNumber = strPlanNumber;
					PlanRecord2.RecordSequence = "0002";
					PlanRecord2.RecordType = "01";
					PlanRecord2.TotalRemittance = Strings.Format(dblPlanTotal * 100, "0000000000");
					strTemp = Strings.Format(dtPayDate, "MM/dd/yyyy");
					strTemp = Strings.Replace(strTemp, "/", "", 1, -1, CompareConstants.vbTextCompare);
					PlanRecord2.PayrollDate = strTemp;
					PlanRecord2.TaxYearIndicator = FCConvert.ToString(clsPlan.Get_Fields("taxyear"));
					PlanRecord2.IRSNumber = Strings.Left(clsPlan.Get_Fields("irsnumber") + Strings.StrDup(9, "0"), 9);
					SavePlanRecordSet();
					EndFile();
				}
				SavePlanFile = true;
				return SavePlanFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
				{
					boolFileOpen = false;
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SavePlanFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SavePlanFile;
		}

		private double SaveLoanRecords(DateTime dtPayDate, string strPlanNumber, bool boolSavedPlan)
		{
			double SaveLoanRecords = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsPlan = new clsDRWrapper();
				string strSQL;
				clsDRWrapper clsDetails = new clsDRWrapper();
				string strTemp;
				int x;
				string strWhere;
				double dblReturn;
				string strDedType = "";
				double dblTemp = 0;
				string strLastEmployeeNum;
				int intLoan = 0;
				double dblLimit = 0;
				clsDRWrapper clsTemp = new clsDRWrapper();
				SaveLoanRecords = 0;
				dblReturn = 0;
				strSQL = "select * from icmainformation where source = 'LN' and rcplan = " + strPlanNumber;
				clsPlan.OpenRecordset(strSQL, "twpy0000.vb1");
				clsTemp.OpenRecordset("select * from tbldeductionsetup", "twpy0000.vb1");
				LoanRepaymentRecord1.RCPlanNumber = strPlanNumber;
				LoanRepaymentRecord1.RecordType = "03";
				LoanRepaymentRecord1.RecordSequence = "0001";
				LoanRepaymentRecord1.FormatID = "3";
				LoanRepaymentRecord2.RCPlanNumber = strPlanNumber;
				LoanRepaymentRecord2.RecordType = "03";
				LoanRepaymentRecord2.RecordSequence = "0002";
				LoanRepaymentRecord2.LoanPayoffIndicator = "    ";
				LoanRepaymentRecord2.FormatID = "3";
				strTemp = " and (";
				while (!clsPlan.EndOfFile())
				{
					strTemp += "Deddeductionnumber = " + FCConvert.ToString(Conversion.Val(clsPlan.Get_Fields("dedcode"))) + " or ";
					clsPlan.MoveNext();
				}
				strWhere = strTemp;
				if (strWhere.Length > 6)
				{
					strWhere = Strings.Mid(strWhere, 1, strWhere.Length - 3);
					strWhere += ")";
				}
				else
				{
					strWhere = "";
					// no loans setup
					return SaveLoanRecords;
				}
				// strSQL = "SELECT tblemployeemaster.*,dedsum from tblemployeemaster inner join (select employeenumber,sum(convert(double, isnull(dedamount, 0))) as DedSum from tblcheckdetail where paydate = '" & dtPayDate & "' and checkvoid = 0 and deductionrecord = 1 and convert(double, isnull(dedamount, 0)) <> 0 " & strWhere & " ) as tbl1 on (tbl1.employeenumber = tblemployeemaster.employeenumber)"
				strSQL = "SELECT tblemployeemaster.*,dedsum,empdeductionid,icmaloan,ltdtotal,limit,tblemployeedeductions.deductioncode as dnumber from tblemployeedeductions inner join (tblemployeemaster inner join (select employeenumber,sum(convert(float, isnull(dedamount, 0))) as DedSum,empdeductionid from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and checkvoid = 0 and deductionrecord = 1 and convert(float, isnull(dedamount, 0)) <> 0 " + strWhere + " group by employeenumber,empdeductionid having sum(convert(float, isnull(dedamount, 0))) <> 0) as tbl1 on (tbl1.employeenumber = tblemployeemaster.employeenumber)) on (tbl1.empdeductionid = tblemployeedeductions.ID) order by tblemployeemaster.employeenumber,empdeductionid";
				clsDetails.OpenRecordset(strSQL, "twpy0000.vb1");
				if (clsDetails.EndOfFile())
				{
					return SaveLoanRecords;
				}
				if (!boolSavedPlan)
				{
					if (!StartFile(strPlanNumber))
					{
						return SaveLoanRecords;
					}
				}
				strLastEmployeeNum = "";
				while (!clsDetails.EndOfFile())
				{
					intLoan = FCConvert.ToInt32(Math.Round(Conversion.Val(clsDetails.Get_Fields("icmaloan"))));
					LoanRepaymentRecord2.LoanNumber = Strings.Format(intLoan, "000");
					dblLimit = Conversion.Val(clsDetails.Get_Fields("limit"));
					if (dblLimit == 0)
					{
						// look at the default
						if (clsTemp.FindFirstRecord("ID", Conversion.Val(clsDetails.Get_Fields("dnumber"))))
						{
							dblLimit = Conversion.Val(clsTemp.Get_Fields("limit"));
						}
					}
					if (Conversion.Val(clsDetails.Get_Fields("ltdtotal")) >= dblLimit)
					{
						LoanRepaymentRecord2.LoanPayoffIndicator = "POFF";
					}
					else
					{
						LoanRepaymentRecord2.LoanPayoffIndicator = "    ";
					}
					dblTemp = Conversion.Val(clsDetails.Get_Fields("dedsum"));
					dblReturn += dblTemp;
					LoanRepaymentRecord2.LoanRepaymentAmount = Strings.Format(dblTemp * 100, "0000000000");
					strTemp = FCConvert.ToString(clsDetails.Get_Fields("ssn"));
					strTemp = Strings.Format(strTemp, "000000000");
					LoanRepaymentRecord1.ParticipantSSN = strTemp;
					LoanRepaymentRecord2.ParticipantSSN = strTemp;
					LoanRepaymentRecord1.ParticipantName = Strings.Left(fecherFoundation.Strings.Trim(clsDetails.Get_Fields("lastname") + "," + clsDetails.Get_Fields("firstname") + " " + clsDetails.Get_Fields("desig")) + Strings.StrDup(30, " "), 30);
					SaveLoanRepaymentRecordSet();
					clsDetails.MoveNext();
				}
				SaveLoanRecords = dblReturn;
				return SaveLoanRecords;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveLoanRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveLoanRecords;
		}

		private double SaveContributionRecords(DateTime dtPayDate, string strPlanNumber, string strSource, bool boolSavedPlan)
		{
			double SaveContributionRecords = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsPlan = new clsDRWrapper();
				string strSQL;
				clsDRWrapper clsDetails = new clsDRWrapper();
				string strTemp;
				int x;
				string strWhere;
				double dblReturn;
				double dblTemp = 0;
				string strDedType;
				string strDedWhere = "";
				string strDedSelect = "";
				string strDedHaving = "";
				bool boolDeduction = false;
				bool boolMatch = false;
				SaveContributionRecords = 0;
				dblReturn = 0;
				strSQL = "select * from icmainformation where source = '" + strSource + "' and rcplan = " + strPlanNumber;
				clsPlan.OpenRecordset(strSQL, "Twpy0000.vb1");
				ContributionRecord1.RCPlanNumber = strPlanNumber;
				ContributionRecord1.RecordType = "02";
				ContributionRecord1.RecordSequence = "0001";
				ContributionRecord1.FormatID = "3";
				ContributionRecord2.RCPlanNumber = strPlanNumber;
				ContributionRecord2.RecordType = "02";
				ContributionRecord2.RecordSequence = "0002";
				ContributionRecord2.FundID = FCConvert.ToString(clsPlan.Get_Fields("fundid"));
				ContributionRecord2.SourceCode = strSource;
				ContributionRecord2.TaxYearIndicator = FCConvert.ToString(clsPlan.Get_Fields("taxyear"));
				ContributionRecord2.FormatID = "3";
				strDedType = " (deductionrecord) ";
				if (fecherFoundation.Strings.UCase(strSource) == "ER")
				{
					// should only happen with 401 and RHS plans
					strDedType = " (matchrecord = 1) ";
					strDedSelect = "0 as DedSum,sum(convert(decimal(10,2), isnull(MATCHAMOUNT, 0))) as matchsum";
					strDedWhere = " convert(decimal(10,2), isnull(MATCHAMOUNT, 0)) <> 0";
					strDedHaving = " sum(convert(decimal(10,2), isnull(MATCHAMOUNT, 0))) <> 0";
					boolDeduction = false;
					boolMatch = true;
				}
				else
				{
					if (FCConvert.ToDouble(Strings.Left(strPlanNumber, 1)) == 1 || FCConvert.ToDouble(Strings.Left(strPlanNumber, 1)) == 8)
					{
						// 401 and RHS plans
						strDedType = " (deductionrecord = 1) ";
						strDedSelect = "sum(convert(decimal(10,2), isnull(dedamount, 0))) as DedSum,0 as matchsum";
						strDedWhere = "convert(decimal(10,2), isnull(dedamount, 0)) <> 0";
						strDedHaving = " sum(convert(decimal(10,2), isnull(dedamount, 0))) <> 0";
						boolDeduction = true;
						boolMatch = false;
					}
					else
					{
						strDedType = " (deductionrecord = 1 or matchrecord = 1) ";
						strDedSelect = "sum(convert(decimal(10,2), isnull(dedamount, 0))) as DedSum,sum(convert(decimal(10,2), isnull(MATCHAMOUNT, 0))) as matchsum";
						strDedWhere = "(convert(decimal(10,2), isnull(dedamount, 0)) <> 0 or convert(decimal(10,2), isnull(MATCHAMOUNT, 0)) <> 0)";
						strDedHaving = " (sum(convert(decimal(10,2), isnull(dedamount, 0))) <> 0 or sum(convert(decimal(10,2), isnull(MATCHAMOUNT, 0))) <> 0)";
						boolDeduction = true;
						boolMatch = true;
					}
				}
				strTemp = " and (";
				while (!clsPlan.EndOfFile())
				{
					if (boolDeduction)
					{
						strTemp += "Deddeductionnumber = " + FCConvert.ToString(Conversion.Val(clsPlan.Get_Fields("dedcode"))) + " or ";
					}
					if (boolMatch)
					{
						strTemp += "matchdeductionnumber = " + FCConvert.ToString(Conversion.Val(clsPlan.Get_Fields("dedcode"))) + " or ";
					}
					clsPlan.MoveNext();
				}
				strWhere = strTemp;
				if (strWhere.Length > 6)
				{
					strWhere = Strings.Mid(strWhere, 1, strWhere.Length - 3);
					strWhere += ")";
				}
				else
				{
					strWhere = "";
					return SaveContributionRecords;
				}
				strSQL = "SELECT tblemployeemaster.*,dedsum,matchsum from tblemployeemaster inner join (select employeenumber," + strDedSelect + " from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and checkvoid = 0 and " + strDedType + " and " + strDedWhere + " " + strWhere + " group by employeenumber having " + strDedHaving + " ) as tbl1 on (tbl1.employeenumber = tblemployeemaster.employeenumber)";
				clsDetails.OpenRecordset(strSQL, "twpy0000.vb1");
				if (clsDetails.EndOfFile())
				{
					return SaveContributionRecords;
				}
				if (!boolSavedPlan)
				{
					if (!StartFile(strPlanNumber))
					{
						return SaveContributionRecords;
					}
				}
				while (!clsDetails.EndOfFile())
				{
					dblTemp = Conversion.Val(clsDetails.Get_Fields("dedsum")) + Conversion.Val(clsDetails.Get_Fields("MATCHSUM"));
					dblReturn += dblTemp;
					ContributionRecord2.ContributionAmount = Strings.Format(dblTemp * 100, "0000000000");
					strTemp = FCConvert.ToString(clsDetails.Get_Fields("ssn"));
					strTemp = Strings.Format(strTemp, "000000000");
					ContributionRecord2.ParticipantSSN = strTemp;
					ContributionRecord1.ParticipantSSN = strTemp;
					ContributionRecord1.ParticipantName = Strings.Left(fecherFoundation.Strings.Trim(clsDetails.Get_Fields("lastname") + "," + clsDetails.Get_Fields("firstname") + " " + clsDetails.Get_Fields("desig")) + Strings.StrDup(30, " "), 30);
					SaveContributionRecordSet();
					clsDetails.MoveNext();
				}
				SaveContributionRecords = dblReturn;
				return SaveContributionRecords;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveContributionRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveContributionRecords;
		}

		public clsICMA() : base()
		{
			strFileList = "";
		}
	}
}
