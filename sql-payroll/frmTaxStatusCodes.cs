//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmTaxStatusCodes : BaseForm
	{
		public frmTaxStatusCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTaxStatusCodes InstancePtr
		{
			get
			{
				return (frmTaxStatusCodes)Sys.GetInstance(typeof(frmTaxStatusCodes));
			}
		}

		protected frmTaxStatusCodes _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 26,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ
		// Date:  07/14/2001
		// 08/10/2001
		//
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbTaxStatusCodes    As DAO.Database
		private clsDRWrapper rsTaxStatusCodes = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private bool boolInvalidData;
		private bool boolState;
		private bool boolPassword;

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsTaxStatusCodes.Row < 0)
					return;
				if (vsTaxStatusCodes.Col < 0)
					return;
				if (!modGlobalRoutines.ValidToDelete(modGlobalVariables.gEnumCodeValidation.TaxStatus, vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsTaxStatusCodes.Row, 8)))
					return;
				if (MessageBox.Show("This action will delete Tax Status Code '" + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsTaxStatusCodes.Row, 1) + "'. Continue?", "Payroll Tax Status Codes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// delete the record from the database
					rsTaxStatusCodes.Execute("Delete from tblTaxStatusCodes where ID = " + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsTaxStatusCodes.Row, 8), "Payroll");
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsTaxStatusCodes);
					// delete the record from the grid
					vsTaxStatusCodes.RemoveItem(vsTaxStatusCodes.Row);
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Tax Status Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolInvalidData == true)
					return;
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll Tax Status Codes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// append a new row to the end of the grid
				vsTaxStatusCodes.AddItem(FCConvert.ToString(vsTaxStatusCodes.Rows));
				// increment the counter as to how many records are dirty
				intDataChanged += 1;
				// assign a row number to this new record
				vsTaxStatusCodes.TextMatrix(vsTaxStatusCodes.Rows - 1, 0, vsTaxStatusCodes.TextMatrix(vsTaxStatusCodes.Rows - 1, 0) + ".");
				vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 0, vsTaxStatusCodes.Rows - 1, vsTaxStatusCodes.Cols - 1, Color.White);
				vsTaxStatusCodes.Select(vsTaxStatusCodes.Rows - 1, 1);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// rptTaxStatusCodes.Show
			frmReportViewer.InstancePtr.Init(rptTaxStatusCodes.InstancePtr, boolAllowEmail: true, strAttachmentName: "TaxStatusCodes");
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				SaveChanges();
				// load the grid of current data from the database
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsTaxStatusCodes.Select(0, 0);
				string strSQL = "";
				for (intCounter = 2; intCounter <= (vsTaxStatusCodes.Rows - 1); intCounter++)
				{
					if (Strings.Right(FCConvert.ToString(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
					{
						// record has been edited so we need to update it
						if (NotValidData())
							goto ExitRoutine;
						strSQL = "Update tblTaxStatusCodes set TaxStatusCode ='" + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "',  Description = '" + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2) + "', FedExempt = " + FCConvert.ToString(ConvertBoolValues(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3))) + ", FicaExempt = " + FCConvert.ToString(ConvertBoolValues(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 4))) + ", MedicareExempt = " + FCConvert.ToString(ConvertBoolValues(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 5))) + ", StateExempt = " + FCConvert.ToString(ConvertBoolValues(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 6))) + ", Pertains = " + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 9) + ", LastUserID = '" + modGlobalVariables.Statics.gstrUser + "' Where ID = " + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 8);
						rsTaxStatusCodes.Execute(strSQL, "Payroll");
						// clear the edit symbol from the row number
						vsTaxStatusCodes.TextMatrix(intCounter, 0, Strings.Mid(vsTaxStatusCodes.TextMatrix(intCounter, 0), 1, vsTaxStatusCodes.TextMatrix(intCounter, 0).Length - 2));
						goto NextRecord;
					}
					if (Strings.Right(FCConvert.ToString(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
					{
						// record has been added so we need to save it
						if (NotValidData())
							goto ExitRoutine;
						rsTaxStatusCodes.Execute("Insert into tblTaxStatusCodes (TaxStatusCode,Description,FedExempt,FicaExempt,MedicareExempt,StateExempt,LastUserID,Pertains) VALUES ('" + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "','" + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'," + FCConvert.ToString(FCConvert.CBool(Conversion.Val(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)))) + "," + FCConvert.ToString(FCConvert.CBool(Conversion.Val(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)))) + "," + FCConvert.ToString(FCConvert.CBool(Conversion.Val(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)))) + "," + FCConvert.ToString(FCConvert.CBool(Conversion.Val(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)))) + "," + FCConvert.ToString(FCConvert.CBool(Conversion.Val(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)))) + ",'" + modGlobalVariables.Statics.gstrUser + "','" + vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 9) + "')", "Payroll");
						// clear the edit symbol from the row number
						vsTaxStatusCodes.TextMatrix(intCounter, 0, Strings.Mid(vsTaxStatusCodes.TextMatrix(intCounter, 0), 1, vsTaxStatusCodes.TextMatrix(intCounter, 0).Length - 1));
						// get the new ID from the table for the record that was just added and place it into column 3
						rsTaxStatusCodes.OpenRecordset("Select Max(ID) as NewID from tblTaxStatusCodes", "TWPY0000.vb1");
						vsTaxStatusCodes.TextMatrix(intCounter, 8, FCConvert.ToString(rsTaxStatusCodes.Get_Fields("NewID")));
					}
					NextRecord:
					;
				}
				clsHistoryClass.Compare();
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Tax Status Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				return;
				ExitRoutine:
				;
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Tax Status Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private int ConvertBoolValues(string strBool)
		{
			int ConvertBoolValues = 0;
			if ((fecherFoundation.Strings.LCase(strBool) == "true") || (fecherFoundation.Strings.LCase(strBool) == "t") || (fecherFoundation.Strings.LCase(strBool) == "1"))
			{
				ConvertBoolValues = 1;
			}
			else
			{
				ConvertBoolValues = 0;
			}
			return ConvertBoolValues;
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			boolInvalidData = true;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 1; intCounter <= (vsTaxStatusCodes.Rows - 1); intCounter++)
			{
				if (vsTaxStatusCodes.TextMatrix(intCounter, 1) == string.Empty)
				{
					// MsgBox "Code Required", vbOKOnly, "Error"
					vsTaxStatusCodes.Select(intCounter, 1);
					NotValidData = true;
					return NotValidData;
				}
			}
			NotValidData = false;
			boolInvalidData = false;
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmTaxStatusCodes_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmTaxStatusCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTaxStatusCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxStatusCodes properties;
			//frmTaxStatusCodes.ScaleWidth	= 9045;
			//frmTaxStatusCodes.ScaleHeight	= 7110;
			//frmTaxStatusCodes.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolInvalidData = false;
				// open the forms global database connection
				// Set dbTaxStatusCodes = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsTaxStatusCodes.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				// Load the grid with data
				LoadGrid();
				// disable the grid
				boolState = true;
				// With vsTaxStatusCodes
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 0, .Rows - 1, .Cols - 2) = TRIOCOLORGRAYBACKGROUND
				// .Editable = False
				// End With
				vsTaxStatusCodes.FixedCols = vsTaxStatusCodes.Cols - 2;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				clsHistoryClass.SetGridIDColumn("PY", "vsTaxStatusCodes", 8);
				this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDNone;
				boolPassword = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				rsTaxStatusCodes.OpenRecordset("Select * from tblTaxStatusCodes Order by TaxStatusCode", "TWPY0000.vb1");
				if (!rsTaxStatusCodes.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsTaxStatusCodes.MoveLast();
					rsTaxStatusCodes.MoveFirst();
					// set the number of rows in the grid
					vsTaxStatusCodes.Rows = rsTaxStatusCodes.RecordCount() + 2;
					// fill the grid
					for (intCounter = 2; intCounter <= (rsTaxStatusCodes.RecordCount() + 1); intCounter++)
					{
						vsTaxStatusCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						vsTaxStatusCodes.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsTaxStatusCodes.Get_Fields("TaxStatusCode") + " "));
						vsTaxStatusCodes.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsTaxStatusCodes.Get_Fields("Description") + " "));
						vsTaxStatusCodes.TextMatrix(intCounter, 3, FCConvert.ToString(rsTaxStatusCodes.Get_Fields_Boolean("FedExempt")));
						vsTaxStatusCodes.TextMatrix(intCounter, 4, FCConvert.ToString(rsTaxStatusCodes.Get_Fields_Boolean("FicaExempt")));
						vsTaxStatusCodes.TextMatrix(intCounter, 5, FCConvert.ToString(rsTaxStatusCodes.Get_Fields_Boolean("MedicareExempt")));
						vsTaxStatusCodes.TextMatrix(intCounter, 6, FCConvert.ToString(rsTaxStatusCodes.Get_Fields_Boolean("StateExempt")));
                        //FC:FINAL:AM:#4173 - set tooltips
                        vsTaxStatusCodes.CellToolTip(intCounter, 3, "Exempt From");
                        vsTaxStatusCodes.CellToolTip(intCounter, 4, "Exempt From");
                        vsTaxStatusCodes.CellToolTip(intCounter, 5, "Exempt From");
                        vsTaxStatusCodes.CellToolTip(intCounter, 6, "Exempt From");
                        vsTaxStatusCodes.TextMatrix(intCounter, 8, fecherFoundation.Strings.Trim(rsTaxStatusCodes.Get_Fields("ID") + " "));
						vsTaxStatusCodes.TextMatrix(intCounter, 9, fecherFoundation.Strings.Trim(rsTaxStatusCodes.Get_Fields_Int32("Pertains") + " "));
						rsTaxStatusCodes.MoveNext();
					}
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
				vsTaxStatusCodes.MergeRow(0, true);
				//vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 8, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                // set grid properties
                vsTaxStatusCodes.FixedRows = 2;
                vsTaxStatusCodes.WordWrap = true;
				vsTaxStatusCodes.Cols = 10;
				// vsTaxStatusCodes.ExtendLastCol = True
				vsTaxStatusCodes.ColHidden(8, true);
				// set column 0 properties
				vsTaxStatusCodes.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// set column 1 properties
				vsTaxStatusCodes.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTaxStatusCodes.TextMatrix(1, 1, "Code");
				//vsTaxStatusCodes.TextMatrix(0, 1, "~");
				// set column 2 properties
				vsTaxStatusCodes.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTaxStatusCodes.TextMatrix(1, 2, "Description");
                vsTaxStatusCodes.TextMatrix(0, 2, "~");
                // set column 3 properties
                vsTaxStatusCodes.ColDataType(3, FCGrid.DataTypeSettings.flexDTBoolean);
                vsTaxStatusCodes.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTaxStatusCodes.TextMatrix(1, 3, "FEDERAL");
                //vsTaxStatusCodes.TextMatrix(0, 3, "EXEMPT FROM");
                // set column 4 properties
                vsTaxStatusCodes.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
                vsTaxStatusCodes.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTaxStatusCodes.TextMatrix(1, 4, "FICA");
                //vsTaxStatusCodes.TextMatrix(0, 4, "EXEMPT FROM");
                // set column 5 properties
                vsTaxStatusCodes.ColDataType(5, FCGrid.DataTypeSettings.flexDTBoolean);
                vsTaxStatusCodes.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTaxStatusCodes.TextMatrix(1, 5, "MED");
				vsTaxStatusCodes.TextMatrix(0, 5, "EXEMPT FROM");
                // set column 6 properties
                vsTaxStatusCodes.ColDataType(6, FCGrid.DataTypeSettings.flexDTBoolean);
                vsTaxStatusCodes.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTaxStatusCodes.TextMatrix(1, 6, "STATE");
                //vsTaxStatusCodes.TextMatrix(0, 6, "EXEMPT FROM");
                // set column 9 properties
				vsTaxStatusCodes.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsTaxStatusCodes.ColComboList(9, "#1;Employee|#2;Employer|#3;Both");
                vsTaxStatusCodes.TextMatrix(0, 9, "PERTAINS");
				vsTaxStatusCodes.TextMatrix(1, 9, "TO");
				//vsTaxStatusCodes.FixedRows = 2;
				//vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 1, 9, Color.Blue);
				vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 1, 9, true);
				// vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 1, 9) = vbWhite
				//vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 0, 2, this.BackColor);
				vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 9, 1, 9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 0, 2, this.BackColor);
				// vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1)
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmTaxStatusCodes_Resize(object sender, System.EventArgs e)
		{
			vsTaxStatusCodes.ColWidth(0, vsTaxStatusCodes.WidthOriginal * 0);
			vsTaxStatusCodes.ColWidth(1, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.07));
			vsTaxStatusCodes.ColWidth(2, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.22));
			vsTaxStatusCodes.ColWidth(3, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.11));
			vsTaxStatusCodes.ColWidth(4, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.11));
			vsTaxStatusCodes.ColWidth(5, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.11));
			vsTaxStatusCodes.ColWidth(6, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.11));
			vsTaxStatusCodes.ColWidth(7, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.11));
			vsTaxStatusCodes.ColWidth(8, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.11));
			vsTaxStatusCodes.ColWidth(9, FCConvert.ToInt32(vsTaxStatusCodes.WidthOriginal * 0.12));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsTaxStatusCodes.Select(0, 0);
				if (intDataChanged != 0)
					SaveChanges();
				if (modGlobalVariables.Statics.gboolShowDeductionSetup)
				{
					frmDeductionSetup.InstancePtr.Show();
				}
				else if (frmEmployersMatch.InstancePtr.boolFromMatch == true)
				{
					frmEmployersMatch.InstancePtr.Show();
				}
				else
				{
					//MDIParent.InstancePtr.Show();
					// set focus back to the menu options of the MDIParent
					//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
				}
				frmEmployersMatch.InstancePtr.boolFromMatch = false;
				modGlobalVariables.Statics.gboolShowDeductionSetup = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuEditable_Click(object sender, System.EventArgs e)
		{
			boolState = Interaction.InputBox("Enter TRIO Password to edit Frequency Codes.", "Frequency Codes", null) == modGlobalConstants.DATABASEPASSWORD;
			if (boolState)
			{
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 2, .Cols - 1) = TRIOCOLORGRAYBACKGROUND
				vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 0, vsTaxStatusCodes.Rows - 1, vsTaxStatusCodes.Cols - 1, Color.White);
			}
			else
			{
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 2, .Cols - 1) = vbBlue
				vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 0, vsTaxStatusCodes.Rows - 1, vsTaxStatusCodes.Cols - 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			if (boolState)
			{
				vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			//vsTaxStatusCodes.Editable = boolState;
			boolPassword = boolState;
			if (boolPassword)
				vsTaxStatusCodes.FixedCols = 0;
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			this.cmdDelete.Enabled = boolPassword;
			this.cmdNew.Enabled = boolPassword;
			this.cmdPrint.Enabled = false;
			this.cmdRefresh.Enabled = boolState;
			this.cmdSave.Enabled = boolState;
			this.mnuSaveExit.Enabled = boolState;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void vsTaxStatusCodes_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsTaxStatusCodes_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsTaxStatusCodes.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsTaxStatusCodes.TextMatrix(vsTaxStatusCodes.Row, 0, vsTaxStatusCodes.TextMatrix(vsTaxStatusCodes.Row, 0) + "..");
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsTaxStatusCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == (Keys)37 && vsTaxStatusCodes.Col == 1)
			{
				if (vsTaxStatusCodes.Row != 1)
				{
					vsTaxStatusCodes.Col = 7;
					vsTaxStatusCodes.Row -= 1;
					KeyCode = 0;
				}
			}
		}

		private void vsTaxStatusCodes_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsTaxStatusCodes_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsTaxStatusCodes.Col == 7)
					{
						if (vsTaxStatusCodes.Row < vsTaxStatusCodes.Rows - 1)
						{
							vsTaxStatusCodes.Row += 1;
							vsTaxStatusCodes.Col = 1;
						}
						else
						{
							// if there is no other row then create a new one
							vsTaxStatusCodes.AddItem("");
							vsTaxStatusCodes.Row += 1;
							vsTaxStatusCodes.Col = 1;
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsTaxStatusCodes_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			//if (vsTaxStatusCodes.MouseCol > 2 && vsTaxStatusCodes.MouseCol < 8)
			//{
			//	ToolTip1.SetToolTip(vsTaxStatusCodes, "Exempt From");
			//}
			//else
			//{
			//	ToolTip1.SetToolTip(vsTaxStatusCodes, "");
			//}
			if (vsTaxStatusCodes.MouseCol == 9)
			{
				this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				if (boolPassword)
				{
					this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsTaxStatusCodes_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsTaxStatusCodes_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (vsTaxStatusCodes.Col == 9)
				{
					this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					if (boolPassword)
					{
						this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						this.vsTaxStatusCodes.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
	}
}
