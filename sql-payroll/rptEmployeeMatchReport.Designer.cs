﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeMatchReport.
	/// </summary>
	partial class rptEmployeeMatchReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmployeeMatchReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			//this.vsHeader = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			//this.vsData = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			//this.vsSummary = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			//this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
			//         this.vsData});
			this.Detail.Height = 0.1770833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtDate,
				this.txtMuniName,
				this.txtPage,
				this.DLG1,
				this.txtTime
			});
			this.PageHeader.Height = 0.7083333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				//this.vsHeader,
				this.Line1
			});
			this.GroupHeader1.Height = 0.5833333F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			//this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
			//         this.vsSummary});
			this.GroupFooter1.Height = 0.65625F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 0F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 6.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 4.125F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 2.1875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Roman 10cpi\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 1";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.1875F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.125F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3125F;
			this.DLG1.Left = 0F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.25F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Roman 10cpi\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 1";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// vsHeader
			// 
			//this.vsHeader.Height = 0.5625F;
			//this.vsHeader.Left = 0F;
			//this.vsHeader.Name = "vsHeader";
			//this.vsHeader.Top = 0F;
			//this.vsHeader.Width = 6.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5625F;
			this.Line1.Width = 6.3125F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.3125F;
			this.Line1.Y1 = 0.5625F;
			this.Line1.Y2 = 0.5625F;
			// 
			// vsData
			// 
			//this.vsData.Height = 0.1875F;
			//this.vsData.Left = 0F;
			//this.vsData.Name = "vsData";
			//this.vsData.Top = 0F;
			//this.vsData.Width = 6.3125F;
			// 
			// vsSummary
			// 
			//this.vsSummary.Height = 0.375F;
			//this.vsSummary.Left = 0F;
			//this.vsSummary.Name = "vsSummary";
			//this.vsSummary.Top = 0.0625F;
			//this.vsSummary.Width = 6.3125F;
			// 
			// rptEmployeeMatchReport
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl vsData;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl vsHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl vsSummary;
	}
}
