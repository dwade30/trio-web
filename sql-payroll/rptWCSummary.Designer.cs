﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptWCSummary.
	/// </summary>
	partial class rptWCSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWCSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtWCCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtWCCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtWCCode,
            this.txtCYTD,
            this.txtQTD,
            this.txtMTD,
            this.txtEmployee,
            this.Line1,
            this.lblTotal});
			this.Detail.Height = 0.28125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtWCCode
			// 
			this.txtWCCode.Height = 0.21875F;
			this.txtWCCode.Left = 3.9375F;
			this.txtWCCode.Name = "txtWCCode";
			this.txtWCCode.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtWCCode.Text = null;
			this.txtWCCode.Top = 0.0625F;
			this.txtWCCode.Width = 0.8125F;
			// 
			// txtCYTD
			// 
			this.txtCYTD.Height = 0.2F;
			this.txtCYTD.Left = 6.533333F;
			this.txtCYTD.Name = "txtCYTD";
			this.txtCYTD.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtCYTD.Text = null;
			this.txtCYTD.Top = 0.06666667F;
			this.txtCYTD.Visible = false;
			this.txtCYTD.Width = 0.9333333F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.21875F;
			this.txtQTD.Left = 6.40625F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtQTD.Text = null;
			this.txtQTD.Top = 0.0625F;
			this.txtQTD.Visible = false;
			this.txtQTD.Width = 0.9375F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.1875F;
			this.txtMTD.Left = 4.90625F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMTD.Text = null;
			this.txtMTD.Top = 0.0625F;
			this.txtMTD.Width = 1.46875F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2F;
			this.txtEmployee.Left = 0F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtEmployee.Text = null;
			this.txtEmployee.Top = 0.06666667F;
			this.txtEmployee.Width = 2.533333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.1F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.05F;
			this.Line1.Width = 4.366667F;
			this.Line1.X1 = 3.1F;
			this.Line1.X2 = 7.466667F;
			this.Line1.Y1 = 0.05F;
			this.Line1.Y2 = 0.05F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1666667F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 2.6F;
			this.lblTotal.MultiLine = false;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.1F;
			this.lblTotal.Width = 0.9F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtTime,
            this.txtPage,
            this.lblDate,
            this.Label7,
            this.Label11,
            this.Label12,
            this.lblWC});
			this.PageHeader.Height = 1.177083F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape1.Height = 0.3125F;
			this.Shape1.Left = 0F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.84375F;
			this.Shape1.Width = 7.84375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL -  W/C Summary Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.96875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.4375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.4375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.4375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblDate.Text = null;
			this.lblDate.Top = 0.21875F;
			this.lblDate.Width = 4.96875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.28125F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label7.Text = "W/C Value";
			this.Label7.Top = 0.9375F;
			this.Label7.Width = 1.53125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.84375F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label11.Text = "QTD";
			this.Label11.Top = 0.5F;
			this.Label11.Visible = false;
			this.Label11.Width = 0.53125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.3125F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label12.Text = "CYTD";
			this.Label12.Top = 0.5F;
			this.Label12.Visible = false;
			this.Label12.Width = 0.5F;
			// 
			// lblWC
			// 
			this.lblWC.Height = 0.19F;
			this.lblWC.HyperLink = null;
			this.lblWC.Left = 3.40625F;
			this.lblWC.MultiLine = false;
			this.lblWC.Name = "lblWC";
			this.lblWC.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.lblWC.Text = "W/C Code";
			this.lblWC.Top = 0.9375F;
			this.lblWC.Width = 1F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptWCSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.90625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtWCCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWCCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWC;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
