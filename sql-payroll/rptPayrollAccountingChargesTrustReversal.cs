//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayrollAccountingChargesTrustReversal.
	/// </summary>
	public partial class rptPayrollAccountingChargesTrustReversal : BaseSectionReport
	{
		public rptPayrollAccountingChargesTrustReversal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Accounting Charges Trust Reversal";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPayrollAccountingChargesTrustReversal InstancePtr
		{
			get
			{
				return (rptPayrollAccountingChargesTrustReversal)Sys.GetInstance(typeof(rptPayrollAccountingChargesTrustReversal));
			}
		}

		protected rptPayrollAccountingChargesTrustReversal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPayrollAccountingChargesTrustReversal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curEmployeeTotal As Decimal	OnWrite(int, Decimal)
		Decimal curEmployeeTotal;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		bool blnSummaryPage;
		public DateTime datPayDate;
		// vbPorter upgrade warning: intPayRunID As int	OnWriteFCConvert.ToInt32(
		public int intPayRunID;
		public int lngCheckNumber;
		// vbPorter upgrade warning: intValidAcctHolder As int	OnWriteFCConvert.ToInt32(
		int intValidAcctHolder;
		public bool blnShowBadAccountLabel;
		private string pstrDeductionDescription = "";
		private string pstrRecipientID = "";
		private clsDRWrapper clsBanks = new clsDRWrapper();
		public bool boolPreviousFiscal;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("EmployeeBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsEmployeeInfo.MoveNext();
				eArgs.EOF = rsEmployeeInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				this.Fields["EmployeeBinder"].Value = rsEmployeeInfo.Get_Fields_Int32("TrustRecipientID");
			}
			else
			{
				blnSummaryPage = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			//clsDRWrapper rsMaster = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnShowBadAccountLabel = false;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblPayDate.Text = "Pay Date: " + Strings.Format(datPayDate, "MM/dd/yyyy");
			lblCheckReturn.Text = "Check Return: " + FCConvert.ToString(frmVoidCheck.InstancePtr.lngCheckNumber);
			blnFirstRecord = true;
			curEmployeeTotal = 0;
			curFinalTotal = 0;
			blnSummaryPage = false;
			// rsEmployeeInfo.OpenRecordset "SELECT tblRecipients.Name AS Name, tblCheckDetail.TrustRecipientID as TrustRecipientID, tblCheckDetail.TrustDeductionDescription as TrustDeductionDescription, SUM(tblCheckDetail.TrustAmount) AS TotalTrustAmount FROM (tblCheckDetail INNER JOIN tblRecipients ON tblCheckDetail.TrustRecipientID = tblRecipients.ID) WHERE tblCheckDetail.CheckNumber = " & lngCheckNumber & " AND PayDate = '" & datPayDate & "' AND PayRunID = " & intPayRunID & " AND TrustRecord = 1 GROUP BY tblRecipients.Name, tblCheckDetail.TrustRecipientID, tblCheckDetail.TrustDeductionDescription ORDER BY tblCheckDetail.TrustRecipientID, tblCheckDetail.TrustDeductionDescription"
			rsEmployeeInfo.OpenRecordset("SELECT   TrustRecipientID, TrustDeductionDescription, SUM(TrustAmount) AS TotalTrustAmount FROM tblCheckDetail  WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND TrustRecord = 1 GROUP BY  TrustRecipientID, TrustDeductionDescription ORDER BY TrustRecipientID, TrustDeductionDescription");
			clsBanks.OpenRecordset("select * from tblRecipients", "twpy0000.vb1");
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No information found for this pay date.", "No information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			fldAmount.Text = Strings.Format(Conversion.Val(rsEmployeeInfo.Get_Fields("TotalTrustAmount")) * -1, "#,##0.00");
			curEmployeeTotal -= FCConvert.ToDecimal(rsEmployeeInfo.Get_Fields("TotalTrustAmount"));
			intValidAcctHolder = modValidateAccount.Statics.ValidAcctCheck;
			modValidateAccount.Statics.ValidAcctCheck = 3;
			// If Not AccountValidate(rsEmployeeInfo.Fields("TrustDeductionDescription")) Then
			// blnShowBadAccountLabel = True
			// fldAccount = "**" & rsEmployeeInfo.Fields("TrustDeductionDescription")
			// Else
			fldAccount.Text = rsEmployeeInfo.Get_Fields_String("TrustDeductionDescription");
			// End If
			modValidateAccount.Statics.ValidAcctCheck = intValidAcctHolder;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFinalTotal As object	OnWrite(string)
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
			curFinalTotal = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldEmployeeTotal As object	OnWrite(string)
			fldEmployeeTotal.Text = Strings.Format(curEmployeeTotal, "#,##0.00");
			curFinalTotal += curEmployeeTotal;
			curEmployeeTotal = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsCheckHistory = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblCheckDetail where TrustRecord = 1 AND CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID));
			rsCheckHistory.OpenRecordset("Select * from tblCheckHistory where CheckNumber = 1");
			rsCheckHistory.AddNew();
			rsCheckHistory.Set_Fields("CheckNumber", lngCheckNumber);
			rsCheckHistory.Set_Fields("ReplacedByCheckNumber", lngCheckNumber);
			rsCheckHistory.Set_Fields("EMPLOYEENUMBER", "T & A " + pstrRecipientID);
			// rsEmployeeInfo.Fields("TrustRecipientID")
			rsCheckHistory.Set_Fields("EMPLOYEENAME", pstrDeductionDescription);
			// rsEmployeeInfo.Fields("TrustDeductionDescription")
			rsCheckHistory.Set_Fields("Amount", FCConvert.ToDouble(fldEmployeeTotal.Text) * -1);
			rsCheckHistory.Set_Fields("CheckDate", datPayDate);
			rsCheckHistory.Set_Fields("CheckRun", intPayRunID);
			rsCheckHistory.Set_Fields("CheckType", "T");
			rsCheckHistory.Set_Fields("Status", "V");
			rsCheckHistory.Set_Fields("ReportNumber", 0);
			rsCheckHistory.Set_Fields("WarrantNumber", rsData.Get_Fields_Int32("WarrantNumber"));
			rsCheckHistory.Update();
			rsCheckHistory.Dispose();
			rsData.Dispose();
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			Subreport1.Report = new srptAccountingSummaryTrustReversal();
		}

		private void GroupFooter4_Format(object sender, EventArgs e)
		{
			if (blnShowBadAccountLabel == true)
			{
				lblBadAccount.Visible = true;
			}
			else
			{
				lblBadAccount.Visible = false;
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldEmployee As object	OnWrite(string)
			fldEmployee.Text = Strings.Format(rsEmployeeInfo.Get_Fields_Int32("TrustRecipientID"), "00");
			if (!(clsBanks.EndOfFile() && clsBanks.BeginningOfFile()))
			{
				if (clsBanks.FindFirstRecord("ID", rsEmployeeInfo.Get_Fields("trustrecipientid")))
				{
					fldEmployee.Text = fldEmployee.Text + "  " + clsBanks.Get_Fields("name");
					pstrDeductionDescription = FCConvert.ToString(clsBanks.Get_Fields_String("Name"));
				}
				else
				{
					pstrDeductionDescription = "";
				}
			}
			else
			{
				pstrDeductionDescription = "";
			}
			pstrRecipientID = FCConvert.ToString(rsEmployeeInfo.Get_Fields_Int32("TrustRecipientID"));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			if (blnSummaryPage)
			{
				Line2.Visible = false;
				Label8.Visible = false;
				Label9.Visible = false;
				Label10.Visible = false;
			}
		}
		// vbPorter upgrade warning: intReversalPayRunID As int	OnRead
		public void Init(ref int lngCheck, ref DateTime datReversalPayDate, ref int intReversalPayRunID, ref bool boolFromPreviousFiscalYear, bool modalDialog)
		{
			boolPreviousFiscal = boolFromPreviousFiscalYear;
			datPayDate = datReversalPayDate;
			intPayRunID = intReversalPayRunID;
			lngCheckNumber = lngCheck;
			// Me.Show vbModal, MDIParent
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "AccountingChargesTAReversal", showModal: modalDialog);
		}

		
	}
}
