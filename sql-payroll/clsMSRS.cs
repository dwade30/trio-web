//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public class clsMSRS
	{
		//=========================================================
		// vbPorter upgrade warning: intReportingMonth As int	OnWriteFCConvert.ToInt32(
		private int intReportingMonth;
		private int lngReportingYear;
		private int intSubmissionNumber;
		private int lngReportType;
		private StreamWriter ts;
		private modCoreysSweeterCode.MSRSData TransmitterInfo = new modCoreysSweeterCode.MSRSData();
		private modCoreysSweeterCode.MSRSDetailRecord DetailInfo = new modCoreysSweeterCode.MSRSDetailRecord();
		private modCoreysSweeterCode.MSRSSummaryRecord SummaryInfo = new modCoreysSweeterCode.MSRSSummaryRecord();
		private int lngFilingType;
		private int lngNumDetailRecords;
		const int CNSTMSRSFILINGTYPEORIGINAL = 0;
		const int CNSTMSRSFILINGTYPETEST = 1;
		const int CNSTMSRSFILINGTYPEREPLACEMENT = 2;
        string strFileName;

        private string GetLetterFromSubmissionNumber()
		{
			string GetLetterFromSubmissionNumber = "";
			GetLetterFromSubmissionNumber = FCConvert.ToString(Convert.ToChar(intSubmissionNumber + 64));
			return GetLetterFromSubmissionNumber;
		}

		private string GetLetterFromFilingType()
		{
			string GetLetterFromFilingType = "";
			switch (lngFilingType)
			{
				case CNSTMSRSFILINGTYPETEST:
					{
						GetLetterFromFilingType = "T";
						// Case CNSTMSRSFILINGTYPEREPLACEMENT
						// GetLetterFromFilingType = "R"
						break;
					}
				default:
					{
						GetLetterFromFilingType = "O";
						break;
					}
			}
			//end switch
			return GetLetterFromFilingType;
		}

		public void Init(int lngRepType)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			clsLoad.OpenRecordset("select * from tblMSRSTable where reporttype = " + FCConvert.ToString(lngRepType), "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				strTemp = FCConvert.ToString(clsLoad.Get_Fields_DateTime("PaidDatesEnding1"));
				intReportingMonth = FCConvert.ToDateTime(strTemp).Month;
				lngReportingYear = FCConvert.ToDateTime(strTemp).Year;
				intSubmissionNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("submissionnumber"))));
				lngReportType = lngRepType;
				lngFilingType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("FilingType"))));
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("FilingType"))) == "T")
				{
					lngFilingType = CNSTMSRSFILINGTYPETEST;
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("FilingType"))) == "R")
				{
					lngFilingType = CNSTMSRSFILINGTYPEREPLACEMENT;
				}
				else
				{
					lngFilingType = CNSTMSRSFILINGTYPEORIGINAL;
				}
				TransmitterInfo.ContactPerson = FCConvert.ToString(clsLoad.Get_Fields("transmittercontactperson"));
				strTemp = FCConvert.ToString(clsLoad.Get_Fields("telephone"));
				strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Right("0000000000" + strTemp, 10);
				TransmitterInfo.Telephone = strTemp;
				TransmitterInfo.TransmitterAddress = FCConvert.ToString(clsLoad.Get_Fields("transmitteraddress"));
				TransmitterInfo.TransmitterCode = FCConvert.ToString(clsLoad.Get_Fields("transmittercode"));
				TransmitterInfo.TransmitterName = FCConvert.ToString(clsLoad.Get_Fields("transmittername"));
				SummaryInfo.EmployerCode = FCConvert.ToString(clsLoad.Get_Fields("employercode"));
				SummaryInfo.EmployerName = FCConvert.ToString(clsLoad.Get_Fields("employername"));
				// SummaryInfo.ScheduleNumber = Right("000000" & clsLoad.Fields("schedulenumber"), 6)
				SummaryInfo.BasicInsuranceRate = Conversion.Val(clsLoad.Get_Fields("effbasinsrate"));
				SummaryInfo.PayrollCycle = FCConvert.ToString(clsLoad.Get_Fields("payrollcycle"));
				if (Information.IsDate(clsLoad.Get_Fields("PAIDDATESENDING1")))
				{
					SummaryInfo.PayDate1 = FCConvert.ToString(clsLoad.Get_Fields("paiddatesending1"));
					DetailInfo.LastPayDate = FCConvert.ToDateTime(SummaryInfo.PayDate1);
				}
				else
				{
					SummaryInfo.PayDate1 = FCConvert.ToString(0);
				}
				if (Information.IsDate(clsLoad.Get_Fields("PAIDDATESENDING2")))
				{
					SummaryInfo.PayDate2 = FCConvert.ToString(clsLoad.Get_Fields("paiddatesending2"));
					if (Convert.ToDateTime(clsLoad.Get_Fields("Paiddatesending2")).ToOADate() != 0)
					{
						DetailInfo.LastPayDate = FCConvert.ToDateTime(SummaryInfo.PayDate2);
					}
				}
				else
				{
					SummaryInfo.PayDate2 = FCConvert.ToString(0);
				}
				if (Information.IsDate(clsLoad.Get_Fields("PAIDDATESENDING3")))
				{
					SummaryInfo.PayDate3 = FCConvert.ToString(clsLoad.Get_Fields("paiddatesending3"));
					if (Convert.ToDateTime(clsLoad.Get_Fields("Paiddatesending3")).ToOADate() != 0)
					{
						DetailInfo.LastPayDate = FCConvert.ToDateTime(SummaryInfo.PayDate3);
					}
				}
				else
				{
					SummaryInfo.PayDate3 = FCConvert.ToString(0);
				}
				if (Information.IsDate(clsLoad.Get_Fields("PAIDDATESENDING4")))
				{
					SummaryInfo.PayDate4 = FCConvert.ToString(clsLoad.Get_Fields("paiddatesending4"));
					if (Convert.ToDateTime(clsLoad.Get_Fields("Paiddatesending4")).ToOADate() != 0)
					{
						DetailInfo.LastPayDate = FCConvert.ToDateTime(SummaryInfo.PayDate4);
					}
				}
				else
				{
					SummaryInfo.PayDate4 = FCConvert.ToString(0);
				}
				if (Information.IsDate(clsLoad.Get_Fields("PAIDDATESENDING5")))
				{
					SummaryInfo.PayDate5 = FCConvert.ToString(clsLoad.Get_Fields("paiddatesending5"));
					if (Convert.ToDateTime(clsLoad.Get_Fields("Paiddatesending5")).ToOADate() != 0)
					{
						DetailInfo.LastPayDate = FCConvert.ToDateTime(SummaryInfo.PayDate5);
					}
				}
				else
				{
					SummaryInfo.PayDate5 = FCConvert.ToString(0);
				}
			}
			else
			{
				return;
			}
			StartMSRSFile();
		}

		private void StartMSRSFile()
		{
			FCFileSystem fso = new FCFileSystem();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// strFileName = SummaryInfo.EmployerCode & Format(intReportingMonth, "00") & "." & Right(Format(lngReportingYear, "00"), 2) & GetLetterFromSubmissionNumber()
				strFileName = SummaryInfo.EmployerCode + Strings.Format(intReportingMonth, "00") + Strings.Right(Strings.Format(lngReportingYear, "00"), 2) + GetLetterFromFilingType();
				if (!FCFileSystem.DirectoryExists("MSRS"))
				{
					FCFileSystem.CreateDirectory("MSRS");
				}
				if (FCFileSystem.FileExists("MSRS\\" + strFileName))
				{
                    FCFileSystem.DeleteFile("MSRS\\" + strFileName);
				}
				ts = FCFileSystem.CreateTextFile("MSRS\\" + strFileName);
				SaveHeaderRecord();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In StartMSRSFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void EndMSRSFIle()
		{
			ts.Close();
		}

		private string CreateHeaderRecord()
		{
			string CreateHeaderRecord = "";
			string strTemp = "";
			string strReturn;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strReturn = "";
				CreateHeaderRecord = "";
				if (fecherFoundation.Strings.Trim(TransmitterInfo.TransmitterCode).Length > 5)
				{
					strTemp = fecherFoundation.Strings.UCase(Strings.Right(TransmitterInfo.TransmitterCode, 5));
				}
				else
				{
					strTemp = fecherFoundation.Strings.UCase(Strings.Left(TransmitterInfo.TransmitterCode + "     ", 5));
				}
				strReturn += strTemp;
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(TransmitterInfo.TransmitterName + Strings.StrDup(20, " "), 20));
				strReturn += strTemp;
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(TransmitterInfo.TransmitterAddress + Strings.StrDup(50, " "), 50));
				strReturn += strTemp;
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(TransmitterInfo.ContactPerson + Strings.StrDup(20, " "), 20));
				strReturn += strTemp;
				strTemp = Strings.Right("0000000000" + TransmitterInfo.Telephone, 10);
				strReturn += strTemp;
				strTemp = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				strReturn += Strings.Left(strTemp, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
				switch (lngFilingType)
				{
					case CNSTMSRSFILINGTYPEORIGINAL:
						{
							strReturn += "O";
							break;
						}
					case CNSTMSRSFILINGTYPETEST:
						{
							strReturn += "T";
							break;
						}
					case CNSTMSRSFILINGTYPEREPLACEMENT:
						{
							strReturn += "R";
							break;
						}
				}
				//end switch
				strReturn += "FILE VERSION";
				// must be 12 chars
				strTemp = Strings.Left("EPF" + Strings.StrDup(8, " "), 8);
				// file identification
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				strReturn += "004";
				// filing format
				strReturn += Strings.StrDup(118, " ");
				// filler
				strReturn += "H";
				CreateHeaderRecord = strReturn;
				return CreateHeaderRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateHeaderRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateHeaderRecord;
		}

		private void SaveHeaderRecord()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = CreateHeaderRecord();
				ts.WriteLine(strLine);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveHeaderRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveDetailRecord()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = CreateDetailRecord();
				ts.WriteLine(strLine);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveDetailRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void SetDetailRecord()
		{
			DetailInfo.BenefitPlanCode = modCoreysSweeterCode.Statics.MSRSDetRec.BenefitPlanCode;
			DetailInfo.DateOfBirth = modCoreysSweeterCode.Statics.MSRSDetRec.DateOfBirth;
			DetailInfo.EarnableCompensation = modCoreysSweeterCode.Statics.MSRSDetRec.EarnableCompensation;
			DetailInfo.EmployeeContributions = modCoreysSweeterCode.Statics.MSRSDetRec.EmployeeContributions;
			DetailInfo.ExcessPaybackContributions = modCoreysSweeterCode.Statics.MSRSDetRec.ExcessPaybackContributions;
			DetailInfo.ExcessPaybackStatus = modCoreysSweeterCode.Statics.MSRSDetRec.ExcessPaybackStatus;
			DetailInfo.FullTimeEquivalent = modCoreysSweeterCode.Statics.MSRSDetRec.FullTimeEquivalent;
			DetailInfo.FullTimeWorkWeekDaysHours = modCoreysSweeterCode.Statics.MSRSDetRec.FullTimeWorkWeekDaysHours;
			// .LastPayDate = MSRSDetRec.LastPayDate
			DetailInfo.LifeInsuranceCode = modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceCode;
			DetailInfo.LifeInsuranceLevel = modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceLevel;
			DetailInfo.LifeInsurancePremium = modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsurancePremium;
			DetailInfo.LifeInsuranceSchedule = modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceSchedule;
			DetailInfo.MonthlyHoursDays = modCoreysSweeterCode.Statics.MSRSDetRec.MonthlyHoursDays;
			DetailInfo.Name = modCoreysSweeterCode.Statics.MSRSDetRec.Name;
			DetailInfo.PayRate = modCoreysSweeterCode.Statics.MSRSDetRec.PayRate;
			DetailInfo.PayRateCode = modCoreysSweeterCode.Statics.MSRSDetRec.PayRateCode;
			DetailInfo.PositionCode = modCoreysSweeterCode.Statics.MSRSDetRec.PositionCode;
			DetailInfo.RateSchedule = modCoreysSweeterCode.Statics.MSRSDetRec.RateSchedule;
			DetailInfo.RetirementPlan = modCoreysSweeterCode.Statics.MSRSDetRec.RetirementPlan;
			DetailInfo.SSN = modCoreysSweeterCode.Statics.MSRSDetRec.SSN;
			DetailInfo.StatusCode = modCoreysSweeterCode.Statics.MSRSDetRec.StatusCode;
			DetailInfo.WorkWeeksPerYear = modCoreysSweeterCode.Statics.MSRSDetRec.WorkWeeksPerYear;
			SaveDetailRecord();
		}

		public void SetSummaryRecord()
		{
			SummaryInfo.TotalAdjustmentsGrantFunded = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalAdjustmentsGrantFunded;
			SummaryInfo.TotalAdjustmentsGrantFundedEmployer = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalAdjustmentsGrantFundedEmployer;
			SummaryInfo.TotalDependent = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalDependent;
			SummaryInfo.TotalEarnableCompensation = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalEarnableCompensation;
			SummaryInfo.EmployerPaidTotalEarnableCompensation = modCoreysSweeterCode.Statics.MSRSSummaryRec.EmployerPaidTotalEarnableCompensation;
			SummaryInfo.TotalActive = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalActive;
			SummaryInfo.TotalGrantFunded = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalGrantFunded;
			SummaryInfo.TotalExcessPayback = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalExcessPayback;
			SummaryInfo.TotalGrantFundedEmployerContributions = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalGrantFundedEmployerContributions;
			SummaryInfo.TotalLifeInsurancePremiums = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalLifeInsurancePremiums;
			SummaryInfo.TotalRetiree = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalRetiree;
			SummaryInfo.TotalRetirementContributions = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalRetirementContributions;
			SummaryInfo.TotalSupplemental = modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalSupplemental;
			SaveSummaryRecord();
			ts.Close();
            //FC:FINAL:AM:#3079 - download the file
            FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "MSRS", strFileName));
		}

		private string CreateDetailRecord()
		{
			string CreateDetailRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn;
				string strTemp = "";
				lngNumDetailRecords += 1;
				strReturn = "";
				CreateDetailRecord = "";
				if (fecherFoundation.Strings.Trim(TransmitterInfo.TransmitterCode).Length > 5)
				{
					strTemp = fecherFoundation.Strings.UCase(Strings.Right(TransmitterInfo.TransmitterCode, 5));
				}
				else
				{
					strTemp = fecherFoundation.Strings.UCase(Strings.Left(TransmitterInfo.TransmitterCode + "     ", 5));
				}
				strReturn += strTemp;
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(SummaryInfo.EmployerCode + Strings.StrDup(6, " "), 6));
				strReturn += strTemp;
				strTemp = Strings.Format(intReportingMonth, "00") + Strings.Format(lngReportingYear, "0000");
				strReturn += strTemp;
				strTemp = fecherFoundation.Strings.Trim(DetailInfo.SSN);
				strTemp = Strings.Left(strTemp + Strings.StrDup(9, "0"), 9);
				strReturn += strTemp;
				strTemp = fecherFoundation.Strings.Trim(DetailInfo.Name);
				strTemp = Strings.Left(strTemp + Strings.StrDup(30, " "), 30);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				strTemp = Strings.Format((DetailInfo.FullTimeEquivalent * 100), "000000000");
				strReturn += strTemp;
				strReturn += Strings.StrDup(11, " ");
				// filler
				strTemp = fecherFoundation.Strings.Trim(DetailInfo.DateOfBirth);
				strTemp += Strings.StrDup(8, "0");
				strTemp = Strings.Left(strTemp, 8);
				strReturn += strTemp;
				strTemp = Strings.Left(DetailInfo.StatusCode + "  ", 2);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				strReturn += "  ";
				// bargaining unit code for state employees only
				strTemp = Strings.Left(fecherFoundation.Strings.UCase(DetailInfo.PositionCode) + Strings.StrDup(10, " "), 10);
				strReturn += strTemp;
				strReturn += " ";
				// filler
				strReturn += " ";
				// filler
				strTemp = Strings.Left(fecherFoundation.Strings.Trim(DetailInfo.ExcessPaybackStatus) + " ", 1);
				strTemp = Strings.Replace(strTemp, "-", " ", 1, -1, CompareConstants.vbTextCompare);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				strTemp = Strings.Left(fecherFoundation.Strings.Trim(DetailInfo.LifeInsuranceSchedule) + " ", 1);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				strTemp = Strings.StrDup(6, " ");
				strReturn += strTemp;
				// filler
				strTemp = Strings.Left(fecherFoundation.Strings.Trim(DetailInfo.LifeInsuranceCode) + "   ", 3);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				// Corey 10/25/2005  Was only putting A in.  #79570
				// Select Case .StatusCode
				// Case 52, 65, 67
				// .RetirementPlan = "N"
				// Case 53
				// .RetirementPlan = "A"
				// Case Else
				// .RetirementPlan = "Y"
				// End Select
				if (fecherFoundation.Strings.Trim(DetailInfo.RetirementPlan) == string.Empty)
				{
					DetailInfo.RetirementPlan = "A";
				}
				strTemp = Strings.Left(fecherFoundation.Strings.Trim(DetailInfo.RetirementPlan) + " ", 1);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				strTemp = Strings.Format(DetailInfo.LifeInsuranceLevel, "000000");
				strReturn += strTemp;
				strTemp = Strings.Format(DetailInfo.LifeInsurancePremium * 100, "00000");
				strReturn += strTemp;
				strTemp = Strings.Format(DetailInfo.EarnableCompensation * 100, "00000000");
				strReturn += strTemp;
				strReturn += Strings.StrDup(7, " ");
				// filler
				strTemp = Strings.Format(DetailInfo.EmployeeContributions * 100, "0000000");
				strReturn += strTemp;
				strTemp = Strings.Format(DetailInfo.ExcessPaybackContributions * 100, "0000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(DetailInfo.MonthlyHoursDays, "0.00")) * 100, "00000");
				strReturn += strTemp;
				strTemp = Strings.Left(DetailInfo.PayRateCode + " ", 1);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				// state won't accept anything over 40
				if (DetailInfo.FullTimeWorkWeekDaysHours <= 40)
				{
					strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(DetailInfo.FullTimeWorkWeekDaysHours, "0.00")) * 100, "0000");
				}
				else
				{
					strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(40, "0.00")) * 100, "0000");
				}
				strReturn += strTemp;
				strTemp = Strings.Format(DetailInfo.WorkWeeksPerYear, "00");
				strReturn += strTemp;
				strReturn += "00000";
				// furlough days. State only
				strReturn += Strings.StrDup(8, "0");
				// furlough comp. state only
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(DetailInfo.PayRate, "0.00")) * 100, "000000");
				strReturn += strTemp;
				strReturn += Strings.StrDup(8, " ");
				// filler
				// strTemp = UCase(GetLetterFromSubmissionNumber)
				strTemp = fecherFoundation.Strings.UCase(GetLetterFromFilingType());
				strReturn += strTemp;
				strReturn += Strings.StrDup(33, " ");
				// filler
				strTemp = Strings.Left(DetailInfo.BenefitPlanCode + Strings.StrDup(5, " "), 5);
				strReturn += fecherFoundation.Strings.UCase(strTemp);
				// If .RateSchedule = "000001" And .RetirementPlan = "Y" Then
				// strTemp = SummaryInfo.ScheduleNumber
				// Else
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(DetailInfo.RateSchedule)) != string.Empty)
				{
					strTemp = Strings.Right(Strings.Format(DetailInfo.RateSchedule, "000000"), 6);
				}
				else
				{
					strTemp = Strings.StrDup(6, " ");
				}
				// End If
				strReturn += strTemp;
				strReturn += Strings.StrDup(7, " ");
				// filler
				strTemp = Strings.Format(DetailInfo.LastPayDate, "MM/dd/yyyy");
				strTemp = Strings.Mid(strTemp, 1, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
				strReturn += strTemp;
				strReturn += "N   ";
				// normal,adjusted or Retroactive
				strReturn += "D";
				// detail record
				SummaryInfo.NumberOfDetailRecords += 1;
				CreateDetailRecord = strReturn;
				return CreateDetailRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateDetailRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateDetailRecord;
		}

		private void SaveSummaryRecord()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = CreateSummaryRecord();
				ts.WriteLine(strLine);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveSummaryRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string CreateSummaryRecord()
		{
			string CreateSummaryRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn;
				string strTemp = "";
				strReturn = "";
				CreateSummaryRecord = "";
				if (fecherFoundation.Strings.Trim(TransmitterInfo.TransmitterCode).Length > 5)
				{
					strTemp = fecherFoundation.Strings.UCase(Strings.Right(TransmitterInfo.TransmitterCode, 5));
				}
				else
				{
					strTemp = Strings.Left(fecherFoundation.Strings.UCase(TransmitterInfo.TransmitterCode) + Strings.StrDup(5, " "), 5);
				}
				strReturn += strTemp;
				strTemp = Strings.Left(fecherFoundation.Strings.UCase(SummaryInfo.EmployerCode) + Strings.StrDup(6, " "), 6);
				strReturn += strTemp;
				strTemp = Strings.Left(fecherFoundation.Strings.UCase(SummaryInfo.EmployerName) + Strings.StrDup(20, " "), 20);
				strReturn += strTemp;
				strTemp = Strings.Format(intReportingMonth, "00") + Strings.Format(lngReportingYear, "0000");
				strReturn += strTemp;
				if (Information.IsDate(SummaryInfo.PayDate1))
				{
					//if (SummaryInfo.PayDate1 != "12:00:00 AM")
					if (SummaryInfo.PayDate1 != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
					{
						strTemp = Strings.Format(SummaryInfo.PayDate1, "MM/dd/yyyy");
						strTemp = Strings.Mid(strTemp, 1, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
					}
					else
					{
						strTemp = Strings.StrDup(8, " ");
					}
				}
				else
				{
					strTemp = Strings.StrDup(8, " ");
				}
				strReturn += strTemp;
				if (Information.IsDate(SummaryInfo.PayDate2))
				{
					//if (SummaryInfo.PayDate2 != "12:00:00 AM")
					if (SummaryInfo.PayDate2 != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
					{
						strTemp = Strings.Format(SummaryInfo.PayDate2, "MM/dd/yyyy");
						strTemp = Strings.Mid(strTemp, 1, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
					}
					else
					{
						strTemp = Strings.StrDup(8, " ");
					}
				}
				else
				{
					strTemp = Strings.StrDup(8, " ");
				}
				strReturn += strTemp;
				if (Information.IsDate(SummaryInfo.PayDate3))
				{
					//if (SummaryInfo.PayDate3 != "12:00:00 AM")
					if (SummaryInfo.PayDate3 != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
					{
						strTemp = Strings.Format(SummaryInfo.PayDate3, "MM/dd/yyyy");
						strTemp = Strings.Mid(strTemp, 1, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
					}
					else
					{
						strTemp = Strings.StrDup(8, " ");
					}
				}
				else
				{
					strTemp = Strings.StrDup(8, " ");
				}
				strReturn += strTemp;
				if (Information.IsDate(SummaryInfo.PayDate4))
				{
					//if (SummaryInfo.PayDate4 != "12:00:00 AM")
					if (SummaryInfo.PayDate4 != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
					{
						strTemp = Strings.Format(SummaryInfo.PayDate4, "MM/dd/yyyy");
						strTemp = Strings.Mid(strTemp, 1, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
					}
					else
					{
						strTemp = Strings.StrDup(8, " ");
					}
				}
				else
				{
					strTemp = Strings.StrDup(8, " ");
				}
				strReturn += strTemp;
				if (Information.IsDate(SummaryInfo.PayDate5))
				{
					//if (SummaryInfo.PayDate5 != "12:00:00 AM")
					if (SummaryInfo.PayDate5 != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
					{
						strTemp = Strings.Format(SummaryInfo.PayDate5, "MM/dd/yyyy");
						strTemp = Strings.Mid(strTemp, 1, 2) + Strings.Mid(strTemp, 4, 2) + Strings.Mid(strTemp, 7, 4);
					}
					else
					{
						strTemp = Strings.StrDup(8, " ");
					}
				}
				else
				{
					strTemp = Strings.StrDup(8, " ");
				}
				strReturn += strTemp;
				// W - Weekly
				// T- Biweekly
				// M - Monthly
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(SummaryInfo.PayrollCycle) + " ", 1));
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.BasicInsuranceRate, "0.00")) * 100, "00000");
				strReturn += strTemp;
				strReturn += "M";
				// reporting code - monthly
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalEarnableCompensation, "0.00")) * 100, "000000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.EmployerPaidTotalEarnableCompensation, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalRetirementContributions, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalExcessPayback, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalLifeInsurancePremiums, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Right("0000" + FCConvert.ToString(lngNumDetailRecords), 4);
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalActive, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalRetiree, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalSupplemental, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalDependent, "0.00")) * 100, "00000000");
				strReturn += strTemp;
				strTemp = Strings.StrDup(25, " ");
				// reserved
				strReturn += strTemp;
				strTemp = Strings.StrDup(8, " ");
				// filler
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalGrantFunded, "0.00")) * 100, "000000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalGrantFundedEmployerContributions, "0.00")) * 100, "000000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalAdjustmentsGrantFunded, "0.00")) * 100, "000000000");
				strReturn += strTemp;
				strTemp = Strings.Format(FCConvert.ToInt32(Strings.Format(SummaryInfo.TotalAdjustmentsGrantFundedEmployer, "0.00")) * 100, "000000000");
				strReturn += strTemp;
				strTemp = Strings.StrDup(25, " ");
				// filler
				strReturn += strTemp;
				strReturn += "S";
				// summary record
				CreateSummaryRecord = strReturn;
				return CreateSummaryRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateSummaryRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateSummaryRecord;
		}
	}
}
