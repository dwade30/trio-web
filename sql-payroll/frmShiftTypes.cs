//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmShiftTypes : BaseForm
	{
		public frmShiftTypes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmShiftTypes InstancePtr
		{
			get
			{
				return (frmShiftTypes)Sys.GetInstance(typeof(frmShiftTypes));
			}
		}

		protected frmShiftTypes _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDSHIFTTYPESCOLINDEX = 0;
		const int CNSTGRIDSHIFTTYPESCOLNAME = 1;
		const int CNSTGRIDSHIFTTYPESCOLSHIFTTYPE = 2;
		const int CNSTGRIDSHIFTTYPECOLOVERTIME = 3;
		const int CNSTGRIDBREAKDOWNSCOLINDEX = 0;
		const int CNSTGRIDBREAKDOWNSCOLPAYCAT = 1;
		const int CNSTGRIDBREAKDOWNSCOLFACTORORUNITS = 2;
		const int CNSTGRIDBREAKDOWNSCOLFACTORUNITS = 3;
		const int CNSTGRIDBREAKDOWNSCOLAFTERUNITS = 4;
		const int CNSTGRIDBREAKDOWNSCOLUPTOUNITS = 5;
		const int CNSTGRIDBREAKDOWNSCOLMAXUNITS = 6;
		const int CNSTGRIDBREAKDOWNSCOLMAXTYPE = 7;
		private clsShiftTypeList ShiftList = new clsShiftTypeList();
		private bool boolLoading;
		private int lngDefaultPayCat;
		private bool boolDataChanged;

		public void Init()
		{
			this.Show(App.MainForm);
		}

		private string CreateFormat(string strType)
		{
			string CreateFormat = "";
			string strTemp = "";
			int counter = 0;
			if (strType == "E")
			{
				strTemp = modAccountTitle.Statics.Exp;
			}
			else if (strType == "R")
			{
				strTemp = modAccountTitle.Statics.Rev;
			}
			else if (strType == "G")
			{
				strTemp = modAccountTitle.Statics.Ledger;
			}
			else if (strType == "P")
			{
				strTemp = modNewAccountBox.Statics.PSegmentSize;
			}
			else if (strType == "V")
			{
				strTemp = modNewAccountBox.Statics.VSegmentSize;
			}
			else if (strType == "L")
			{
				strTemp = modNewAccountBox.Statics.LSegmentSize;
			}
			else
			{
				strTemp = "";
			}
			if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
				return CreateFormat;
			do
			{
				counter = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, 2))));
				strTemp = Strings.Right(strTemp, strTemp.Length - 2);
				if (counter > 0)
				{
					for (counter = counter; counter >= 1; counter--)
					{
						// If blnAllowLetters Then
						CreateFormat = CreateFormat + "A";
						// Else
						// CreateFormat = CreateFormat & "0"
						// End If
					}
					CreateFormat = CreateFormat + "-";
				}
			}
			while (strTemp != "");
			CreateFormat = Strings.Left(CreateFormat, CreateFormat.Length - 1);
			return CreateFormat;
		}

		private void FillCmbAccount()
		{
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				cmbAccount.AddItem(" ");
				cmbAccount.AddItem("E");
				cmbAccount.AddItem("G");
				cmbAccount.AddItem("R");
				
			}
			else
			{
				cmbAccount.AddItem(" ");
				cmbAccount.AddItem("M");
			}
			string strTemp = "";
			// strTemp = CreateFormat("E")
			// gridAccount.EditMask = strTemp
		}

		private void cmbAccount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (fecherFoundation.Strings.Trim(cmbAccount.Text) != "")
			{
				strTemp = CreateFormat(cmbAccount.Text);
				gridAccount.EditMask = strTemp;
				if (FCConvert.ToString(gridAccount.Tag) != fecherFoundation.Strings.Trim(cmbAccount.Text))
				{
					strTemp = Strings.Replace(strTemp, "A", "X", 1, -1, CompareConstants.vbTextCompare);
					gridAccount.TextMatrix(0, 0, strTemp);
				}
				gridAccount.Visible = true;
				gridAccount.Tag = (System.Object)(fecherFoundation.Strings.Trim(cmbAccount.Text));
			}
			else
			{
				gridAccount.TextMatrix(0, 0, "");
				gridAccount.Visible = false;
				gridAccount.Tag = (System.Object)("");
			}
		}

		private void frmShiftTypes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmShiftTypes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmShiftTypes properties;
			//frmShiftTypes.FillStyle	= 0;
			//frmShiftTypes.ScaleWidth	= 9300;
			//frmShiftTypes.ScaleHeight	= 7755;
			//frmShiftTypes.LinkTopic	= "Form2";
			//frmShiftTypes.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrids();
			FillCmbAccount();
			lngDefaultPayCat = 2;
			LoadInfo();
			boolDataChanged = false;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				// vbPorter upgrade warning: lngReturn As int	OnWrite(DialogResult)
				DialogResult lngReturn = 0;
				lngReturn = MessageBox.Show("Data has been changed but not saved" + "\r\n" + "Save data now?", null, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (lngReturn == DialogResult.Cancel)
				{
					e.Cancel = true;
				}
				else if (lngReturn == DialogResult.Yes)
				{
					if (!SaveInfo())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmShiftTypes_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void gridAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 120)
			{
				KeyAscii -= 32;
			}
			else if (KeyAscii == 88 || KeyAscii == 8 || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
					KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void GridBreakdowns_ComboCloseUp(object sender, EventArgs e)
		{
			clsShiftPayLines tBreakdown;
			clsShiftType tShift;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			intCol = GridBreakdowns.Col;
			tShift = ShiftList.GetCurrentShiftType();
			if (!(tShift == null))
			{
				tBreakdown = tShift.GetCurrentBreakdown();
				if (!(tBreakdown == null))
				{
					switch (intCol)
					{
						case CNSTGRIDBREAKDOWNSCOLFACTORORUNITS:
							{
								tBreakdown.FactorOrUnits = (modSchedule.ShiftFactorType)Conversion.Val(GridBreakdowns.ComboData());
								break;
							}
						case CNSTGRIDBREAKDOWNSCOLMAXTYPE:
							{
								tBreakdown.MaxType = (modSchedule.ShiftPerType)Conversion.Val(GridBreakdowns.ComboData());
								break;
							}
						case CNSTGRIDBREAKDOWNSCOLPAYCAT:
							{
								tBreakdown.PayCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBreakdowns.ComboData())));
								break;
							}
					}
					//end switch
				}
				boolDataChanged = true;
			}
		}

		private void GridBreakdowns_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddLine();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteLine();
						break;
					}
			}
			//end switch
		}
		// Private Sub GridBreakdowns_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
		// Dim intCol As Integer
		// intCol = GridBreakdowns.MouseCol
		// GridBreakdowns.ToolTipText = GetToolTipText(intCol)
		// End Sub
		private void GridBreakdowns_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			boolDataChanged = true;
		}

		private void GridShiftTypes_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			int lngRow;
			lngRow = e.OldRow;
			if (!boolLoading)
			{
				if (e.OldRow > 0)
				{
					// Save info
					UpdateShiftType();
				}
				int intindex = 0;
				intindex = FCConvert.ToInt32(Math.Round(Conversion.Val(GridShiftTypes.TextMatrix(e.NewRow, CNSTGRIDSHIFTTYPESCOLINDEX))));
				clsShiftType tShift;
				tShift = ShiftList.GetShiftTypeByIndex(intindex);
				tShift.MoveFirst();
				ShowBreakdowns();
			}
		}

		private void GridShiftTypes_ComboCloseUp(object sender, EventArgs e)
		{
			int intOption = 0;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			intCol = GridShiftTypes.Col;
			switch (intCol)
			{
				case CNSTGRIDSHIFTTYPECOLOVERTIME:
					{
						intOption = FCConvert.ToInt32(Math.Round(Conversion.Val(GridShiftTypes.ComboData())));
						clsShiftType tShift;
						tShift = ShiftList.GetCurrentShiftType();
						if (!(tShift == null))
						{
							tShift.OvertimePayCatID = intOption;
						}
						break;
					}
			}
			//end switch
			boolDataChanged = true;
		}

		private void GridShiftTypes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddShiftType();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteShiftType(GridShiftTypes.Row);
						break;
					}
			}
			//end switch
		}

		private void GridShiftTypes_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridShiftTypes.Row > 0)
			{
				clsShiftType tShift;
				switch (GridShiftTypes.Col)
				{
					case CNSTGRIDSHIFTTYPESCOLNAME:
						{
							tShift = ShiftList.GetCurrentShiftType();
							if (!(tShift == null))
							{
								tShift.ShiftName = GridShiftTypes.EditText;
							}
							break;
						}
				}
				//end switch
				boolDataChanged = true;
			}
		}

		private void menuAddLine_Click(object sender, System.EventArgs e)
		{
			AddLine();
		}

		private void mnuAddShiftType_Click(object sender, System.EventArgs e)
		{
			AddShiftType();
		}

		private void mnuDeleteLine_Click(object sender, System.EventArgs e)
		{
			DeleteLine();
		}

		private void mnuDeleteShift_Click(object sender, System.EventArgs e)
		{
			DeleteShiftType(GridShiftTypes.Row);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrids()
		{
			string strTemp = "";
			GridShiftTypes.Cols = 4;
			GridShiftTypes.Rows = 1;
			GridShiftTypes.ColHidden(CNSTGRIDSHIFTTYPESCOLINDEX, true);
			GridShiftTypes.TextMatrix(0, CNSTGRIDSHIFTTYPESCOLNAME, "Shift");
			GridShiftTypes.TextMatrix(0, CNSTGRIDSHIFTTYPECOLOVERTIME, "Overtime Pay Category");
			GridShiftTypes.ColHidden(CNSTGRIDSHIFTTYPESCOLSHIFTTYPE, true);
			strTemp = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tblPayCategories order by categorynumber", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields("Description") + "\t" + rsLoad.Get_Fields("Type") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != "")
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			GridShiftTypes.ColComboList(CNSTGRIDSHIFTTYPECOLOVERTIME, "#0;None|" + strTemp);
			GridBreakdowns.Cols = 8;
			GridBreakdowns.Rows = 1;
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLAFTERUNITS, "After");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLFACTORUNITS, "Fctr/Unit");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLFACTORORUNITS, "Type");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLMAXTYPE, "");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLMAXUNITS, "Max");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLMAXTYPE, "Max Type");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLPAYCAT, "Pay Category");
			GridBreakdowns.TextMatrix(0, CNSTGRIDBREAKDOWNSCOLUPTOUNITS, "Up To");
			GridBreakdowns.ColHidden(CNSTGRIDBREAKDOWNSCOLINDEX, true);
			GridBreakdowns.ColComboList(CNSTGRIDBREAKDOWNSCOLPAYCAT, strTemp);
			strTemp = "#" + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftPerType.PerShift)) + ";Per Shift|#" + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftPerType.PerDay)) + ";Per Day|#" + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftPerType.PerWeek)) + ";Per Week|#" + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftPerType.PerPayPeriod)) + ";Per Pay Period";
			GridBreakdowns.ColComboList(CNSTGRIDBREAKDOWNSCOLMAXTYPE, strTemp);
			strTemp = "#" + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftFactorType.Factor)) + ";Factor|#" + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftFactorType.Units)) + ";Units";
			GridBreakdowns.ColComboList(CNSTGRIDBREAKDOWNSCOLFACTORORUNITS, strTemp);
		}
		// Private Function GetToolTipText(intCol As Integer) As String
		// Dim strReturn As String
		// strReturn = ""
		// Select Case intCol
		// Case CNSTGRIDBREAKDOWNSCOLAFTERUNITS
		// strReturn = "Number of units before this rule takes affect"
		// Case CNSTGRIDBREAKDOWNSCOLFACTORORUNITS
		// strReturn = "Number is a factor times hours worked or is straight units (hours/dollars)"
		// Case CNSTGRIDBREAKDOWNSCOLFACTORUNITS
		// strReturn = "Factor to multiply entered hours by or units (hours/dollars) to add"
		// Case CNSTGRIDBREAKDOWNSCOLMAXTYPE
		// strReturn = "Limit type for max units"
		// Case CNSTGRIDBREAKDOWNSCOLMAXUNITS
		// strReturn = "Max units allowed per shift,day etc."
		// Case CNSTGRIDBREAKDOWNSCOLPAYCAT
		// strReturn = "Pay category to add to"
		// Case CNSTGRIDBREAKDOWNSCOLUPTOUNITS
		// strReturn = "Apply this rule up to this amount.  Enter 0 for unlimited"
		// End Select
		// GetToolTipText = strReturn
		// End Function
		private void ResizeGrids()
		{
			int GridWidth = 0;
			GridWidth = GridShiftTypes.WidthOriginal;
			GridShiftTypes.ColWidth(CNSTGRIDSHIFTTYPESCOLNAME, FCConvert.ToInt32(0.5 * GridWidth));
			GridWidth = GridBreakdowns.WidthOriginal;
			GridBreakdowns.ColWidth(CNSTGRIDBREAKDOWNSCOLPAYCAT, FCConvert.ToInt32(0.3 * GridWidth));
			GridBreakdowns.ColWidth(CNSTGRIDBREAKDOWNSCOLFACTORORUNITS, FCConvert.ToInt32(0.15 * GridWidth));
			GridBreakdowns.ColWidth(CNSTGRIDBREAKDOWNSCOLFACTORUNITS, FCConvert.ToInt32(0.1 * GridWidth));
			GridBreakdowns.ColWidth(CNSTGRIDBREAKDOWNSCOLMAXUNITS, FCConvert.ToInt32(0.1 * GridWidth));
			GridBreakdowns.ColWidth(CNSTGRIDBREAKDOWNSCOLUPTOUNITS, FCConvert.ToInt32(0.1 * GridWidth));
			GridBreakdowns.ColWidth(CNSTGRIDBREAKDOWNSCOLAFTERUNITS, FCConvert.ToInt32(0.1 * GridWidth));
            //gridAccount.Height = gridAccount.RowHeight(0) + 30;
            //FC:FINAL:BSE #4267 increase column width
            gridAccount.ColWidth(0, FCConvert.ToInt32(gridAccount.WidthOriginal * 0.9));
		}

		private void LoadInfo()
		{
			ShiftList.LoadTypes();
			boolLoading = true;
			GridShiftTypes.Rows = 1;
			GridBreakdowns.Rows = 1;
			int lngRow;
			ShiftList.MoveFirst();
			clsShiftType tShift;
			while (ShiftList.GetCurrentIndex() >= 0)
			{
				tShift = ShiftList.GetCurrentShiftType();
				if (!(tShift == null))
				{
					if (!tShift.Unused)
					{
						GridShiftTypes.Rows += 1;
						lngRow = GridShiftTypes.Rows - 1;
						GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPESCOLINDEX, FCConvert.ToString(ShiftList.GetCurrentIndex()));
						GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPESCOLNAME, tShift.ShiftName);
						GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPECOLOVERTIME, FCConvert.ToString(tShift.OvertimePayCatID));
						GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPESCOLSHIFTTYPE, FCConvert.ToString(tShift.ShiftType));
						txtLunch.Text = FCConvert.ToString(tShift.LunchTime);
					}
				}
				ShiftList.MoveNext();
			}
			if (GridShiftTypes.Rows > 1)
			{
				ShiftList.MoveFirst();
				GridShiftTypes.Row = 1;
			}
			ShowBreakdowns();
			boolLoading = false;
		}

		private void ShowBreakdowns()
		{
			GridBreakdowns.Rows = 1;
			boolLoading = true;
			txtLunch.Text = FCConvert.ToString(0);
			cmbAccount.SelectedIndex = 0;
			gridAccount.TextMatrix(0, 0, "");
			gridAccount.Visible = false;
			if (ShiftList.GetCurrentIndex() >= 0)
			{
				int lngRow;
				clsShiftType tShift;
				tShift = ShiftList.GetCurrentShiftType();
				clsShiftPayLines tBreakdown;
				if (!(tShift == null))
				{
					tShift.MoveFirst();
					txtLunch.Text = FCConvert.ToString(tShift.LunchTime);
					if (tShift.Account != "")
					{
						gridAccount.Visible = true;
						cmbAccount.Text = Strings.Left(tShift.Account, 1);
						//App.DoEvents();
						gridAccount.TextMatrix(0, 0, Strings.Mid(tShift.Account, 3));
						gridAccount.EditMask = CreateFormat(cmbAccount.Text);
					}
					while (tShift.GetCurrentIndex >= 0)
					{
						tBreakdown = tShift.GetCurrentBreakdown();
						if (!(tBreakdown == null))
						{
							GridBreakdowns.Rows += 1;
							lngRow = GridBreakdowns.Rows - 1;
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLINDEX, FCConvert.ToString(tShift.GetCurrentIndex));
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLAFTERUNITS, FCConvert.ToString(tBreakdown.AfterUnits));
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLFACTORORUNITS, tBreakdown.FactorOrUnits);
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLFACTORUNITS, FCConvert.ToString(tBreakdown.FactorUnits));
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLMAXTYPE, FCConvert.ToInt32(tBreakdown.MaxType));
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLMAXUNITS, FCConvert.ToString(tBreakdown.MaxUnits));
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLPAYCAT, FCConvert.ToString(tBreakdown.PayCategory));
							GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLUPTOUNITS, FCConvert.ToString(tBreakdown.UpToUnits));
						}
						tShift.MoveNext();
					}
					tShift.MoveFirst();
				}
			}
			if (GridBreakdowns.Rows > 1)
			{
				GridBreakdowns.Row = 1;
			}
			boolLoading = false;
		}

		private void UpdateShiftType()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsShiftType tShift;
				clsShiftPayLines tBreakdown;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				gridAccount.Col = -1;
				//App.DoEvents();
				tShift = ShiftList.GetCurrentShiftType();
				if (!(tShift == null))
				{
					tShift.LunchTime = Conversion.Val(txtLunch.Text);
					if (fecherFoundation.Strings.Trim(cmbAccount.Text) != "")
					{
						tShift.Account = cmbAccount.Text + " " + gridAccount.TextMatrix(0, 0);
					}
					else
					{
						tShift.Account = "";
					}
					for (x = 1; x <= (GridBreakdowns.Rows - 1); x++)
					{
						tBreakdown = tShift.GetBreakdownByIndex(FCConvert.ToInt32(Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLINDEX))));
						if (!(tBreakdown == null))
						{
							tBreakdown.AfterUnits = Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLAFTERUNITS));
							tBreakdown.FactorOrUnits = (modSchedule.ShiftFactorType)Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLFACTORORUNITS));
							tBreakdown.FactorUnits = Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLFACTORUNITS));
							tBreakdown.MaxType = (modSchedule.ShiftPerType)Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLMAXTYPE));
							tBreakdown.MaxUnits = Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLMAXUNITS));
							tBreakdown.PayCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLPAYCAT))));
							tBreakdown.Unused = false;
							tBreakdown.UpToUnits = Conversion.Val(GridBreakdowns.TextMatrix(x, CNSTGRIDBREAKDOWNSCOLUPTOUNITS));
						}
					}
					// x
					tShift.MoveFirst();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateShiftType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddShiftType()
		{
			UpdateShiftType();
			clsShiftType tShift;
			int intindex;
			intindex = ShiftList.AddShiftType("New Type", modSchedule.ScheduleShiftType.UserDefined, lngDefaultPayCat, 0, 0);
			boolLoading = true;
			tShift = ShiftList.GetCurrentShiftType();
			int lngRow;
			GridShiftTypes.Rows += 1;
			lngRow = GridShiftTypes.Rows - 1;
			GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPESCOLINDEX, FCConvert.ToString(intindex));
			GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPESCOLNAME, "New Type");
			GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPESCOLSHIFTTYPE, FCConvert.ToString(FCConvert.ToInt32(modSchedule.ScheduleShiftType.UserDefined)));
			GridShiftTypes.TextMatrix(lngRow, CNSTGRIDSHIFTTYPECOLOVERTIME, FCConvert.ToString(lngDefaultPayCat));
			boolDataChanged = true;
			txtLunch.Text = FCConvert.ToString(0);
			tShift.MoveFirst();
			ShowBreakdowns();
			GridShiftTypes.TopRow = lngRow;
			GridShiftTypes.Row = lngRow;
			boolLoading = false;
		}

		private void DeleteShiftType(int lngRow)
		{
			clsShiftType tShift;
			if (lngRow < 1)
				return;
			tShift = ShiftList.GetCurrentShiftType();
			if (tShift == null)
				return;
			tShift.Unused = true;
			GridShiftTypes.RemoveItem(lngRow);
			int intindex = 0;
			if (GridShiftTypes.Row > 0)
			{
				intindex = FCConvert.ToInt32(Math.Round(Conversion.Val(GridShiftTypes.TextMatrix(GridShiftTypes.Row, CNSTGRIDSHIFTTYPESCOLINDEX))));
			}
			else
			{
				intindex = -1;
			}
			tShift = ShiftList.GetShiftTypeByIndex(intindex);
			boolDataChanged = true;
			tShift.MoveFirst();
			ShowBreakdowns();
		}

		private void DeleteLine()
		{
			int lngRow;
			clsShiftPayLines tBreakdown;
			clsShiftType tShift;
			// vbPorter upgrade warning: intindex As int	OnWrite(string)
			int intindex = 0;
			lngRow = GridBreakdowns.Row;
			if (lngRow > 1 || GridBreakdowns.Rows > 2)
			{
				intindex = FCConvert.ToInt32(GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLINDEX));
				tShift = ShiftList.GetCurrentShiftType();
				if (!(tShift == null))
				{
					tBreakdown = tShift.GetBreakdownByIndex(intindex);
					if (!(tBreakdown == null))
					{
						tBreakdown.Unused = true;
					}
					GridBreakdowns.RemoveItem(lngRow);
					boolDataChanged = true;
				}
			}
		}

		private void AddLine()
		{
			clsShiftPayLines tBreakdown;
			clsShiftType tShift;
			int intindex = 0;
			int lngRow = 0;
			tShift = ShiftList.GetCurrentShiftType();
			boolLoading = true;
			if (!(tShift == null))
			{
				intindex = tShift.AddLine(1, 1, modSchedule.ShiftFactorType.Factor, 0, 0, 0, modSchedule.ShiftPerType.PerShift);
				GridBreakdowns.Rows += 1;
				lngRow = GridBreakdowns.Rows - 1;
				GridBreakdowns.Row = lngRow;
				GridBreakdowns.TopRow = lngRow;
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLAFTERUNITS, FCConvert.ToString(0));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLFACTORORUNITS, FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftFactorType.Factor)));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLFACTORUNITS, FCConvert.ToString(1));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLINDEX, FCConvert.ToString(intindex));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLMAXTYPE, FCConvert.ToString(FCConvert.ToInt32(modSchedule.ShiftPerType.PerShift)));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLMAXUNITS, FCConvert.ToString(0));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLPAYCAT, FCConvert.ToString(1));
				GridBreakdowns.TextMatrix(lngRow, CNSTGRIDBREAKDOWNSCOLUPTOUNITS, FCConvert.ToString(0));
				boolDataChanged = true;
			}
			boolLoading = false;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			LoadInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				Close();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolReturn;
				boolReturn = true;
				SaveInfo = false;
				GridBreakdowns.Row = 0;
				//App.DoEvents();
				UpdateShiftType();
				SaveInfo = ShiftList.SaveShiftTypes();
				if (boolReturn)
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				if (boolReturn)
					boolDataChanged = false;
				SaveInfo = boolReturn;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}
	}
}
