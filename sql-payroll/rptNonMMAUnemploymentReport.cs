//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNonMMAUnemploymentReport.
	/// </summary>
	public partial class rptNonMMAUnemploymentReport : BaseSectionReport
	{
		public rptNonMMAUnemploymentReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Non MMA Unemployment Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNonMMAUnemploymentReport InstancePtr
		{
			get
			{
				return (rptNonMMAUnemploymentReport)Sys.GetInstance(typeof(rptNonMMAUnemploymentReport));
			}
		}

		protected rptNonMMAUnemploymentReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNonMMAUnemploymentReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		int[] intGrandTotalFemales = new int[3 + 1];
		int[] intGrandTotalEmployees = new int[3 + 1];
		// vbPorter upgrade warning: curGrandTotalGross As Decimal	OnWrite(int, Decimal)
		Decimal curGrandTotalGross;
		// vbPorter upgrade warning: curGrandTotalTaxable As Decimal	OnWrite(int, Decimal)
		Decimal curGrandTotalTaxable;
		// vbPorter upgrade warning: curGrandTotalExcess As Decimal	OnWrite(int, Decimal)
		Decimal curGrandTotalExcess;
		bool boolRunAgain;
		string strPerson = "";
		bool[] blnPeriod = new bool[3 + 1];
        private SSNViewType ssnViewPermission = SSNViewType.None;
        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				if (boolRunAgain)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsEmployeeInfo.MoveNext();
					eArgs.EOF = rsEmployeeInfo.EndOfFile();
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			int counter;
			modDavesSweetCode.Statics.blnReportStarted = true;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);

			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			for (counter = 0; counter <= 3; counter++)
			{
				intGrandTotalEmployees[counter] = 0;
				intGrandTotalFemales[counter] = 0;
			}
			curGrandTotalGross = 0;
			curGrandTotalTaxable = 0;
			curGrandTotalExcess = 0;
			bool blnSummaryPage = false;
			if (frmUnemploymentReport.InstancePtr.cboSequence.SelectedIndex == 0)
			{
				if (frmUnemploymentReport.InstancePtr.cboSequenceRange.SelectedIndex == 0)
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM (tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) WHERE tblMiscUpdate.Unemployment <> 'N' ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName");
				}
				else if (frmUnemploymentReport.InstancePtr.cboSequenceRange.SelectedIndex == 1)
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM (tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) WHERE tblEmployeeMaster.SeqNumber = " + frmUnemploymentReport.InstancePtr.cboSingleSequence.Text + " AND tblMiscUpdate.Unemployment <> 'N' ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName");
				}
				else
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM (tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) WHERE (tblEmployeeMaster.SeqNumber >= " + frmUnemploymentReport.InstancePtr.cboStartSequence.Text + " AND tblEmployeeMaster.SeqNumber <= " + frmUnemploymentReport.InstancePtr.cboEndSequence.Text + ") AND tblMiscUpdate.Unemployment <> 'N' ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName");
				}
			}
			else
			{
				if (frmUnemploymentReport.InstancePtr.cboSequenceRange.SelectedIndex == 0)
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM (tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) WHERE tblMiscUpdate.Unemployment <> 'N' ORDER BY tblEmployeeMaster.SeqNumber");
				}
				else if (frmUnemploymentReport.InstancePtr.cboSequenceRange.SelectedIndex == 1)
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM (tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) WHERE tblEmployeeMaster.SeqNumber = " + frmUnemploymentReport.InstancePtr.cboSingleSequence.Text + " AND tblMiscUpdate.Unemployment <> 'N' ORDER BY tblEmployeeMaster.SeqNumber");
				}
				else
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM (tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) WHERE (tblEmployeeMaster.SeqNumber >= " + frmUnemploymentReport.InstancePtr.cboStartSequence.Text + " AND tblEmployeeMaster.SeqNumber <= " + frmUnemploymentReport.InstancePtr.cboEndSequence.Text + ") AND tblMiscUpdate.Unemployment <> 'N' ORDER BY tblEmployeeMaster.SeqNumber");
				}
			}
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No information found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Cancel();
				this.Close();
				return;
			}
			fldReportNumber.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtReportNumber.Text);
			fldEmployer.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtEmployer.Text);
			fldQuarter.Text = frmUnemploymentReport.InstancePtr.cboQuarter.Text + "/" + frmUnemploymentReport.InstancePtr.cboYear.Text;
			fldPreparer.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtPreparer.Text);
			fldTelephoneNumber.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtTelephone.Text);
			fldFederalID.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtFederalID.Text);
			fldStateID.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtStateID.Text);
			fldMemberCode.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtMemberCode.Text);
			fldSeasonalCode.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtSeasonalCode.Text);
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("NonMMA");
				frmUnemploymentReport.InstancePtr.Unload();
			}
			// frmUnemploymentReport.Show , MDIParent
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsFirstInfo = new clsDRWrapper();
			clsDRWrapper rsSecondInfo = new clsDRWrapper();
			clsDRWrapper rsThirdInfo = new clsDRWrapper();
			clsDRWrapper rsTotalPay = new clsDRWrapper();
			clsDRWrapper rsDed = new clsDRWrapper();
			// vbPorter upgrade warning: curPayTotal As Decimal	OnWrite(int, double)
			Decimal curPayTotal;
			DateTime datLowDate;
			DateTime datHighDate;
			// vbPorter upgrade warning: curTaxablePayTotal As Decimal	OnWrite(int, Decimal)
			Decimal curTaxablePayTotal;
			// vbPorter upgrade warning: datLowBegBalDate As DateTime	OnWrite(string)
			DateTime datLowBegBalDate;
			DateTime datHighBegBalDate;
			clsDRWrapper rsBegBalInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curBegBal As Decimal	OnWrite(int, double)
			Decimal curBegBal;
			clsDRWrapper rsStateInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curStateBase As Decimal	OnWrite
			Decimal curStateBase;
			bool boolSamePerson = false;
			double dblTotDed = 0;
			double dblTempDed = 0;
			double dblFirstDed = 0;
			double dblSecondDed = 0;
			double dblThirdDed = 0;
			double dblFirstTot = 0;
			double dblSecTot = 0;
			double dblThirdTot = 0;
			fldSocialSecurity.Visible = true;
			fldName.Visible = true;
			fldSeasonal.Visible = true;
			fldOne.Visible = true;
			fldTwo.Visible = true;
			fldThree.Visible = true;
			fldGross.Visible = true;
			fldTaxable.Visible = true;
			fldExcess.Visible = true;
			fldFem.Visible = true;

			rsStateInfo.OpenRecordset("SELECT * FROM tblStandardLimits");
			if (rsStateInfo.EndOfFile() != true && rsStateInfo.BeginningOfFile() != true)
			{
				curStateBase = FCConvert.ToDecimal(rsStateInfo.Get_Fields_Double("UnemploymentBase"));
			}
			else
			{
				rsStateInfo.AddNew();
				rsStateInfo.Update(true);
				rsStateInfo.OpenRecordset("SELECT * FROM tblStandardLimits");
				curStateBase = 0;
			}
			// ***********************************************************************************
			if (FCConvert.ToDouble(frmUnemploymentReport.InstancePtr.cboQuarter.Text) == 1)
			{
				curBegBal = 0;
			}
			else if (FCConvert.ToDouble(frmUnemploymentReport.InstancePtr.cboQuarter.Text) == 2)
			{
				datLowBegBalDate = FCConvert.ToDateTime("1/1/" + frmUnemploymentReport.InstancePtr.cboYear.Text);
				datHighBegBalDate = modDavesSweetCode.FindLastDay("3/1/" + frmUnemploymentReport.InstancePtr.cboYear.Text);
				rsBegBalInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE rtrim(DistU) <> 'N' AND rtrim(DistU) <> '' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowBegBalDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighBegBalDate) + "' AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowBegBalDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighBegBalDate) + "' AND CheckVoid = 0", "twpy0000.vb1");
				dblTempDed = 0;
				if (!rsDed.EndOfFile())
				{
					dblTempDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (rsBegBalInfo.EndOfFile() != true && rsBegBalInfo.BeginningOfFile() != true)
				{
					curBegBal = FCConvert.ToDecimal(Conversion.Val(rsBegBalInfo.Get_Fields("GrossPayTotal")) - dblTempDed);
				}
				else
				{
					curBegBal = 0;
				}
			}
			else if (FCConvert.ToDouble(frmUnemploymentReport.InstancePtr.cboQuarter.Text) == 3)
			{
				datLowBegBalDate = FCConvert.ToDateTime("1/1/" + frmUnemploymentReport.InstancePtr.cboYear.Text);
				datHighBegBalDate = modDavesSweetCode.FindLastDay("6/1/" + frmUnemploymentReport.InstancePtr.cboYear.Text);
				rsBegBalInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE rtrim(DistU) <> 'N' AND rtrim(DistU) <> '' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowBegBalDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighBegBalDate) + "' AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(Dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowBegBalDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighBegBalDate) + "' AND CheckVoid = 0", "twpy0000.vb1");
				dblTempDed = 0;
				if (!rsDed.EndOfFile())
				{
					dblTempDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (rsBegBalInfo.EndOfFile() != true && rsBegBalInfo.BeginningOfFile() != true)
				{
					curBegBal = FCConvert.ToDecimal(Conversion.Val(rsBegBalInfo.Get_Fields("GrossPayTotal")) - dblTempDed);
				}
				else
				{
					curBegBal = 0;
				}
			}
			else
			{
				datLowBegBalDate = FCConvert.ToDateTime("1/1/" + frmUnemploymentReport.InstancePtr.cboYear.Text);
				datHighBegBalDate = modDavesSweetCode.FindLastDay("9/1/" + frmUnemploymentReport.InstancePtr.cboYear.Text);
				rsBegBalInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE rtrim(DistU) <> 'N' AND rtrim(DistU) <> '' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowBegBalDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighBegBalDate) + "' AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowBegBalDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighBegBalDate) + "' AND CheckVoid = 0", "twpy0000.vb1");
				dblTempDed = 0;
				if (!rsDed.EndOfFile())
				{
					dblTempDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (rsBegBalInfo.EndOfFile() != true && rsBegBalInfo.BeginningOfFile() != true)
				{
					curBegBal = FCConvert.ToDecimal(Conversion.Val(rsBegBalInfo.Get_Fields("GrossPayTotal")) - dblTempDed);
				}
				else
				{
					curBegBal = 0;
				}
			}
			curPayTotal = 0;
			curTaxablePayTotal = 0;
			datLowDate = modDavesSweetCode.FindFirstDay(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text);
			datHighDate = modDavesSweetCode.FindLastDay(FCConvert.ToString(modDavesSweetCode.FindFirstDay(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)));

			if (boolRunAgain)
			{
				rsFirstInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'S' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[0].Text)) + "' AND CheckVoid = 0");
				rsSecondInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'S' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[1].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[1].Text)) + "' AND CheckVoid = 0");
				rsThirdInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'S' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[2].Text)) + "' AND CheckVoid = 0");
				// MATTHEW 4/9/04
				// rsTotalPay.OpenRecordset "SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'S' AND EmployeeNumber = '" & rsEmployeeInfo.Fields("EmployeeNumber") & "' AND PayDate >= '" & datLowDate & "' AND PayDate <= '" & datHighDate & "'"
				rsTotalPay.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'S' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND MSRSAdjustRecord = 0 AND CheckVoid = 0");
				curPayTotal = FCConvert.ToDecimal(Conversion.Val(rsTotalPay.Get_Fields("GrossPayTotal")) - dblTotDed);
				fldSeasonal.Text = "X";
				boolRunAgain = false;
				if (!rsFirstInfo.EndOfFile())
				{
					dblFirstTot = Conversion.Val(rsFirstInfo.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblFirstTot = 0;
				}
				if (dblFirstTot < dblFirstDed)
				{
					dblFirstDed -= dblFirstTot;
					dblFirstTot = 0;
				}
				else
				{
					dblFirstTot -= dblFirstDed;
					dblFirstDed = 0;
				}
				if (!rsSecondInfo.EndOfFile())
				{
					dblSecTot = Conversion.Val(rsSecondInfo.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblSecTot = 0;
				}
				if (dblSecTot < dblSecondDed)
				{
					dblSecondDed -= dblSecTot;
					dblSecTot = 0;
				}
				else
				{
					dblSecTot -= dblSecondDed;
					dblSecondDed = 0;
				}
				if (!rsThirdInfo.EndOfFile())
				{
					dblThirdTot = Conversion.Val(rsThirdInfo.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblThirdTot = 0;
				}
				if (dblThirdTot < dblThirdDed)
				{
					dblThirdDed -= dblThirdTot;
					dblThirdTot = 0;
				}
				else
				{
					dblThirdTot -= dblThirdDed;
					dblThirdDed = 0;
				}
			}
			else
			{
				dblTotDed = dblTempDed;
				rsFirstInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'Y' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[0].Text)) + "' AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(Dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[0].Text)) + "' AND CheckVoid = 0", "twpy0000.vb1");
				dblFirstDed = 0;
				if (!rsDed.EndOfFile())
				{
					dblFirstDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (!rsFirstInfo.EndOfFile())
				{
					dblFirstTot = Conversion.Val(rsFirstInfo.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblFirstTot = 0;
				}
				if (dblFirstTot < dblFirstDed)
				{
					dblFirstDed -= dblFirstTot;
					dblFirstTot = 0;
				}
				else
				{
					dblFirstTot -= dblFirstDed;
					dblFirstDed = 0;
				}
				rsSecondInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'Y' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[1].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[1].Text)) + "' AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[1].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[1].Text)) + "' AND CheckVoid = 0", "twpy0000.vb1");
				dblSecondDed = 0;
				if (!rsDed.EndOfFile())
				{
					dblSecondDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (!rsSecondInfo.EndOfFile())
				{
					dblSecTot = Conversion.Val(rsSecondInfo.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblSecTot = 0;
				}
				if (dblSecTot < dblSecondDed)
				{
					dblSecondDed -= dblSecTot;
					dblSecTot = 0;
				}
				else
				{
					dblSecTot -= dblSecondDed;
					dblSecondDed = 0;
				}
				rsThirdInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'Y' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[2].Text)) + "' AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[2].Text)) + "' AND CheckVoid = 0", "twpy0000.vb1");
				dblThirdDed = 0;
				if (!rsDed.EndOfFile())
				{
					dblThirdDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (!rsThirdInfo.EndOfFile())
				{
					dblThirdTot = Conversion.Val(rsThirdInfo.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblThirdTot = 0;
				}
				if (dblThirdTot < dblThirdDed)
				{
					dblThirdDed -= dblThirdTot;
					dblThirdTot = 0;
				}
				else
				{
					dblThirdTot -= dblThirdDed;
					dblThirdDed = 0;
				}
				// MATTHEW 4/9/04
				// rsTotalPay.OpenRecordset "SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'Y' AND EmployeeNumber = '" & rsEmployeeInfo.Fields("EmployeeNumber") & "' AND PayDate >= '" & datLowDate & "' AND PayDate <= '" & datHighDate & "'"
				rsTotalPay.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal, SUM(StateTaxGross) as TaxablePayTotal FROM tblCheckDetail WHERE left(DistU, 1) = 'Y' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND MSRSAdjustRecord = 0 AND CheckVoid = 0");
				rsDed.OpenRecordset("SELECT SUM(dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'  AND CheckVoid = 0", "twpy0000.vb1");
				if (!rsDed.EndOfFile())
				{
					dblTotDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				else
				{
					dblTotDed = 0;
				}
				curPayTotal = FCConvert.ToDecimal(Conversion.Val(rsTotalPay.Get_Fields("GrossPayTotal")) - dblTotDed);
				if (Conversion.Val(rsTotalPay.Get_Fields("grosspaytotal")) < dblTotDed)
				{
					curPayTotal = 0;
					dblTotDed -= Conversion.Val(rsTotalPay.Get_Fields("grosspaytotal"));
				}
				else
				{
					dblTotDed = 0;
				}
				fldSeasonal.Text = "";
				boolRunAgain = true;
			}
			// End If
			// If Val(rsTotalPay.Fields("GrossPayTotal")) <> 0 Then
			if (curPayTotal != 0)
			{				
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) != "")
				{
					fldName.Text = FCConvert.ToString(fecherFoundation.Strings.StrConv(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName")), VbStrConv.ProperCase)) + " " + fecherFoundation.Strings.Trim(Strings.Left(fecherFoundation.Strings.UCase(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) + " ", 1)) + " " + FCConvert.ToString(fecherFoundation.Strings.StrConv(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName")), VbStrConv.ProperCase));
				}
				else
				{
					fldName.Text = FCConvert.ToString(fecherFoundation.Strings.StrConv(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName")), VbStrConv.ProperCase)) + " " + FCConvert.ToString(fecherFoundation.Strings.StrConv(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName")), VbStrConv.ProperCase));
				}
                var strTempSSN = Strings.Format(rsEmployeeInfo.Get_Fields_String("SSN"), "000-00-0000"); ;
                switch (ssnViewPermission)
                {
                    case SSNViewType.Full:
                        fldSocialSecurity.Text = strTempSSN;
                        break;
                    case SSNViewType.Masked:
                        fldSocialSecurity.Text = "***-**-" + "-" + strTempSSN.Right(4);
                        break;
                    default:
                        fldSocialSecurity.Text = "***-**-****";
                        break;
                }
                boolSamePerson = strTempSSN == strPerson;
				strPerson = strTempSSN;
				if (!boolSamePerson)
				{
					blnPeriod[1] = false;
					blnPeriod[2] = false;
					blnPeriod[3] = false;
				}
				// If rsFirstInfo.EndOfFile <> True And rsFirstInfo.BeginningOfFile <> True Then
				// If Val(rsFirstInfo.Fields("GrossPayTotal")) > 0 Then
				if (dblFirstTot > 0)
				{
					fldOne.Text = "X";
					if (!boolSamePerson || blnPeriod[1] == false)
					{
						intGrandTotalEmployees[1] += 1;
						if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
						{
							intGrandTotalFemales[1] += 1;
						}
						blnPeriod[1] = true;
					}
				}
				else
				{
					fldOne.Text = "";
				}
				// Else
				// fldOne = ""
				// End If
				// If rsSecondInfo.EndOfFile <> True And rsSecondInfo.BeginningOfFile <> True Then
				// If Val(rsSecondInfo.Fields("GrossPayTotal")) > 0 Then
				if (dblSecTot > 0)
				{
					fldTwo.Text = "X";
					if (!boolSamePerson || blnPeriod[2] == false)
					{
						intGrandTotalEmployees[2] += 1;
						if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
						{
							intGrandTotalFemales[2] += 1;
						}
						blnPeriod[2] = true;
					}
				}
				else
				{
					fldTwo.Text = "";
				}
				// Else
				// fldTwo = ""
				// End If
				// If rsThirdInfo.EndOfFile <> True And rsThirdInfo.BeginningOfFile <> True Then
				// If Val(rsThirdInfo.Fields("GrossPayTotal")) > 0 Then
				if (dblThirdTot > 0)
				{
					fldThree.Text = "X";
					if (!boolSamePerson || blnPeriod[3] == false)
					{
						intGrandTotalEmployees[3] += 1;
						if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
						{
							intGrandTotalFemales[3] += 1;
						}
						blnPeriod[3] = true;
					}
				}
				else
				{
					fldThree.Text = "";
				}
				// Else
				// fldThree = ""
				// End If
				// curPayTotal = Val(rsTotalPay.Fields("GrossPayTotal"))
				curTaxablePayTotal = curPayTotal - modDavesSweetCode.CalculateExcess(curPayTotal + curBegBal, curPayTotal, curStateBase);
				fldGross.Text = Strings.Format(curPayTotal, "#,##0.00");
				fldTaxable.Text = Strings.Format(curTaxablePayTotal, "#,##0.00");
				fldExcess.Text = Strings.Format(curPayTotal - curTaxablePayTotal, "#,##0.00");
				curGrandTotalGross += curPayTotal;
				curGrandTotalTaxable += curTaxablePayTotal;
				curGrandTotalExcess += (curPayTotal - curTaxablePayTotal);
				if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
				{
					fldFem.Text = "Y";
				}
				else
				{
					fldFem.Text = "N";
				}
			}
			else
			{
				fldSocialSecurity.Visible = false;
				fldName.Visible = false;
				fldSeasonal.Visible = false;
				fldOne.Visible = false;
				fldTwo.Visible = false;
				fldThree.Visible = false;
				fldGross.Visible = false;
				fldTaxable.Visible = false;
				fldExcess.Visible = false;
				fldFem.Visible = false;
			}
			rsTotalPay.Dispose();
			rsSecondInfo.Dispose();
			rsThirdInfo.Dispose();
			rsDed.Dispose();
			rsBegBalInfo.Dispose();
			rsFirstInfo.Dispose();
			rsStateInfo.Dispose();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalOne As object	OnWrite
			// vbPorter upgrade warning: fldTotalTwo As object	OnWrite
			// vbPorter upgrade warning: fldTotalThree As object	OnWrite
			// vbPorter upgrade warning: fldFemOne As object	OnWrite
			// vbPorter upgrade warning: fldFemTwo As object	OnWrite
			// vbPorter upgrade warning: fldFemThree As object	OnWrite
			// vbPorter upgrade warning: fldTotalGross As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalTaxable As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalExcess As object	OnWrite(string)
			fldTotalOne.Text = intGrandTotalEmployees[1].ToString();
			fldTotalTwo.Text = intGrandTotalEmployees[2].ToString();
			fldTotalThree.Text = intGrandTotalEmployees[3].ToString();
			fldFemOne.Text = intGrandTotalFemales[1].ToString();
			fldFemTwo.Text = intGrandTotalFemales[2].ToString();
			fldFemThree.Text = intGrandTotalFemales[3].ToString();
			fldTotalGross.Text = Strings.Format(curGrandTotalGross, "#,##0.00");
			fldTotalTaxable.Text = Strings.Format(curGrandTotalTaxable, "#,##0.00");
			fldTotalExcess.Text = Strings.Format(curGrandTotalGross - curGrandTotalTaxable, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
