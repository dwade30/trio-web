//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmProcessEditSummary.
	/// </summary>
	partial class frmProcessEditSummary
	{
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuTotals;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsGrid = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTotals = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdShowEditTotals = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowEditTotals)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdShowEditTotals);
            this.BottomPanel.Location = new System.Drawing.Point(0, 411);
            this.BottomPanel.Size = new System.Drawing.Size(586, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsGrid);
            this.ClientArea.Size = new System.Drawing.Size(586, 351);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(586, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(258, 30);
            this.HeaderText.Text = "Edit Totals Breakdown";
            // 
            // vsGrid
            // 
            this.vsGrid.Cols = 10;
            this.vsGrid.Location = new System.Drawing.Point(30, 30);
            this.vsGrid.Name = "vsGrid";
            this.vsGrid.Rows = 50;
            this.vsGrid.Size = new System.Drawing.Size(528, 311);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTotals,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuTotals
            // 
            this.mnuTotals.Index = 0;
            this.mnuTotals.Name = "mnuTotals";
            this.mnuTotals.Shortcut = Wisej.Web.Shortcut.F9;
            this.mnuTotals.Text = "Show Edit Totals                   ";
            this.mnuTotals.Click += new System.EventHandler(this.mnuTotals_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdShowEditTotals
            // 
            this.cmdShowEditTotals.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdShowEditTotals.AppearanceKey = "acceptButton";
            this.cmdShowEditTotals.Location = new System.Drawing.Point(204, 30);
            this.cmdShowEditTotals.Name = "cmdShowEditTotals";
            this.cmdShowEditTotals.Shortcut = Wisej.Web.Shortcut.F9;
            this.cmdShowEditTotals.Size = new System.Drawing.Size(178, 48);
            this.cmdShowEditTotals.TabIndex = 1;
            this.cmdShowEditTotals.Text = "Show Edit Totals";
            this.cmdShowEditTotals.Click += new System.EventHandler(this.mnuTotals_Click);
            // 
            // frmProcessEditSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(586, 519);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmProcessEditSummary";
            this.Text = "Edit Totals Breakdown";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmProcessEditSummary_Load);
            this.Activated += new System.EventHandler(this.frmProcessEditSummary_Activated);
            this.Resize += new System.EventHandler(this.frmProcessEditSummary_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProcessEditSummary_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowEditTotals)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdShowEditTotals;
	}
}