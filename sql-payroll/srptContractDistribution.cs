//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptContractDistribution.
	/// </summary>
	public partial class srptContractDistribution : FCSectionReport
	{
		public srptContractDistribution()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptContractDistribution InstancePtr
		{
			get
			{
				return (srptContractDistribution)Sys.GetInstance(typeof(srptContractDistribution));
			}
		}

		protected srptContractDistribution _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
                conCT = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptContractDistribution	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngContractID;
		clsContract conCT = new clsContract();
		clsDRWrapper rsReport = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// EOF = conCT.EndOfAccounts
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strAsOf;
			string[] strAry = null;
			strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
			lngContractID = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			strAsOf = "";
			if (Information.UBound(strAry, 1) > 0)
			{
				strAsOf = " and paydate <= '" + Strings.Format(strAry[1], "MM/dd/yyyy") + "' ";
			}
			if (lngContractID > 0)
			{
				conCT.LoadContract(lngContractID);
				rsReport.OpenRecordset("select distaccountnumber,sum(convert(int, isnull(distgrosspay, 0))) as sumgross,paydate from tblcheckdetail where checkvoid = 0 " + strAsOf + " and distributionrecord = 1 and contractid = " + FCConvert.ToString(lngContractID) + "  group by paydate,distaccountnumber having sum(convert(int, isnull(distgrosspay, 0))) > 0 order by paydate,distaccountnumber", "twpy0000.vb1");
				if (rsReport.EndOfFile())
				{
					// Me.Stop
					// Unload Me
					return;
				}
			}
			else
			{
				// Me.Stop
				// Unload Me
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// If Not conCT.EndOfAccounts Then
			// txtAccount.Text = conCT.CurAccount
			// txtAmount.Text = Format(conCT.CurAccountOriginalAmount, "#,###,##0.00")
			// txtPaid.Text = Format(conCT.CurAccountPaid, "#,###,##0.00")
			// txtRemaining.Text = Format(conCT.CurAccountOriginalAmount - conCT.CurAccountPaid, "#,###,##0.00")
			// conCT.MoveNextAccount
			// End If
			if (!rsReport.EndOfFile())
			{
				txtAccount.Text = rsReport.Get_Fields_String("distaccountnumber");
				txtPaid.Text = Strings.Format(rsReport.Get_Fields("sumgross"), "#,###,##0.00");
				txtPaydate.Text = Strings.Format(rsReport.Get_Fields("paydate"), "MM/dd/yyyy");
				rsReport.MoveNext();
			}
		}

		
	}
}
