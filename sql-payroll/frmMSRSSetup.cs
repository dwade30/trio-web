//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;

namespace TWPY0000
{
	public partial class frmMSRSSetup : BaseForm
	{
		public frmMSRSSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.lblTeacherInfo = new System.Collections.Generic.List<FCLabel>();
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_0, 0);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_1, 1);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_2, 2);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_3, 3);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_4, 4);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_5, 5);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_6, 6);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_7, 7);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_8, 8);
			this.lblTeacherInfo.AddControlArrayElement(lblTeacherInfo_9, 9);
			this.t2kPaidDatesEnding = new System.Collections.Generic.List<T2KDateBox>();
			this.t2kPaidDatesEnding.AddControlArrayElement(t2kPaidDatesEnding_0, 0);
			this.t2kPaidDatesEnding.AddControlArrayElement(t2kPaidDatesEnding_1, 1);
			this.t2kPaidDatesEnding.AddControlArrayElement(t2kPaidDatesEnding_2, 2);
			this.t2kPaidDatesEnding.AddControlArrayElement(t2kPaidDatesEnding_3, 3);
			this.t2kPaidDatesEnding.AddControlArrayElement(t2kPaidDatesEnding_4, 4);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMSRSSetup InstancePtr
		{
			get
			{
				return (frmMSRSSetup)Sys.GetInstance(typeof(frmMSRSSetup));
			}
		}

		protected frmMSRSSetup _InstancePtr = null;
		//=========================================================
		bool boolExitOK;
		bool boolTeacher;
		bool boolLoading;
		int lngYearToUse;
		int intMonthToUse;
		DateTime dtDateToUse;
		bool boolNewMonth;
		private clsHistory clsHistoryClass = new clsHistory();
        public List<string> batchReports;

        private void LoadInfo(int lngReportType, int intMonth, int lngYear, DateTime TempDate)
		{
			// loads msrs data
			clsDRWrapper clsLoad = new clsDRWrapper();
			// Dim tempDate As Date
			// Dim intMonth As Integer
			int x;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// Dim lngYear As Long
			// If Not gtypeFullSetReports.boolFullSet Then
			// lngYear = Year(gdatCurrentPayDate)
			// intMonth = Month(gdatCurrentPayDate)
			// Call frmSelectDateInfo.Init("Select the Month and Year for the report", lngYear, , intMonth)
			// If lngYear = 0 Then
			// gboolCancelSelected = True
			// Exit Sub
			// Else
			// tempDate = intMonth & "/1/" & lngYear
			// tempDate = FindLastDay(tempDate)
			// End If
			// Else
			// load the default paid ending date
			// tempDate = gdatCurrentPayDate
			// intMonth = Month(tempDate)
			// tempDate = intMonth & "/1/" & Year(tempDate)
			// tempDate = FindLastDay(tempDate)
			// End If
			// now load previously saved info
			clsLoad.OpenRecordset("select * from tblmsrstable where reporttype = " + FCConvert.ToString(lngReportType), "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("MakeElectronicMSRS")))
				{
					chkElectronicFile.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkElectronicFile.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				boolNewMonth = true;
				if (Information.IsDate(clsLoad.Get_Fields("paiddatesending1")))
				{
					if (FCConvert.ToInt32(clsLoad.Get_Fields("paiddatesending1").Month) == intMonth)
					{
						boolNewMonth = false;
					}
				}
				txtEmployerCode.Text = FCConvert.ToString(clsLoad.Get_Fields_String("EmployerCode"));
				txtEmployerName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName"));
				txtPreparersName.Text = FCConvert.ToString(clsLoad.Get_Fields("preparersname"));
				txtTelephone.Text = FCConvert.ToString(clsLoad.Get_Fields("telephone"));
				txtBasicRate.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("EffBasInsRate")), "0.0000");
				txtDepARate.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("DepARate")), "0.0000");
				txtDepBRate.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("DepBRate")), "0.0000");
				GridPremiums.TextMatrix(1, 0, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PremiumActive"))), "0.00"));
				GridPremiums.TextMatrix(1, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PremiumRetiree"))), "0.00"));
				GridPremiums.TextMatrix(1, 2, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PremiumSupplemental"))), "0.00"));
				GridPremiums.TextMatrix(1, 3, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PremiumDependent"))), "0.00"));
				GridCreditsDebits.TextMatrix(1, 0, FCConvert.ToString(clsLoad.Get_Fields_String("CreditOrDebitNo1")));
				GridCreditsDebits.TextMatrix(1, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("CreditOrDebitTaken1"))), "0.00"));
				GridCreditsDebits.TextMatrix(2, 0, FCConvert.ToString(clsLoad.Get_Fields_String("CreditOrDebitNo2")));
				GridCreditsDebits.TextMatrix(2, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("CreditOrDebitTaken2"))), "0.00"));
				txtDaysWorked.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("NumDaysWorked")));
				txtPayrollPayments.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("NumPayrollPayments")));
				txtFedFundPerc.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fedfundperc")));
				txtEmployeesCoveredLast.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("NumEmpCoveredLast")));
				txtEmployeesSeparatedLast.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("NumEmpSeparatedLast")));
				txtEmployeesSeparatedThis.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("NumEmpSeparatedThis")));
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("AutoAddEmployerCredit")))
				{
					chkAutoMatch.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkAutoMatch.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtCompensationAdjustments.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("priorperiodcompensationadjustment")), "0.00");
				txtEmployerContributionAdjustments.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("priorperiodcontributionadjustment")), "0.00");
				txtPurchaseAgreements.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("purchaseagreements")), "0.00");
			}
			else
			{
				chkElectronicFile.CheckState = Wisej.Web.CheckState.Unchecked;
				// txtSubmissionNumber.Text = "1"
				chkAutoMatch.CheckState = Wisej.Web.CheckState.Unchecked;
				txtEmployerCode.Text = "";
				txtEmployerName.Text = "";
				txtPreparersName.Text = "";
				txtTelephone.Text = "(000)000-0000";
				txtBasicRate.Text = "0.0000";
				txtDepARate.Text = "0.0000";
				txtDepBRate.Text = "0.0000";
				GridPremiums.TextMatrix(1, 0, "0.00");
				GridPremiums.TextMatrix(1, 1, "0.00");
				GridPremiums.TextMatrix(1, 2, "0.00");
				GridPremiums.TextMatrix(1, 3, "0.00");
				GridCreditsDebits.TextMatrix(1, 0, "");
				GridCreditsDebits.TextMatrix(1, 1, "0.00");
				GridCreditsDebits.TextMatrix(2, 0, "");
				GridCreditsDebits.TextMatrix(2, 1, "0.00");
				txtDaysWorked.Text = "0";
				txtPayrollPayments.Text = "0";
				txtFedFundPerc.Text = "0";
				txtEmployeesCoveredLast.Text = "0";
				txtEmployeesSeparatedLast.Text = "0";
				txtEmployeesSeparatedThis.Text = "0";
			}
			clsLoad.OpenRecordset("select distinct paydate from tblcheckdetail where paydate between '" + FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(TempDate.Year) + "' and '" + FCConvert.ToString(TempDate) + "' AND isnull(AdjustRecord,0) = 0 ORDER BY PayDate", "twpy0000.vb1");
			x = 0;
			// corey 7/7/2004  Matt thinks it would be better to have the first pay date and the last 4 paydates.  This may not
			// be all but will reflect the range a little better
			// intCounter = clsLoad.RecordCount - 5
			intCounter = (clsLoad.RecordCount() - 4);
			if (intCounter > 0)
			{
				t2kPaidDatesEnding[x].Text = Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy");
				x += 1;
			}
			for (int i= 1; i <= intCounter; i++)
			{
				clsLoad.MoveNext();
			}
			while (!clsLoad.EndOfFile())
			{
				t2kPaidDatesEnding[x].Text = Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy");
				x += 1;
				clsLoad.MoveNext();
			}
			// now load the code/rate information
			Grid.Rows = 2;
			Grid.AddItem("");
			clsLoad.OpenRecordset("select * from tblmsrsCodeRate where reporttype = " + FCConvert.ToString(lngReportType), "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				Grid.Rows = 2;
				// delete the blank row for now
				while (!clsLoad.EndOfFile())
				{
					Grid.AddItem(clsLoad.Get_Fields("Code") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields("Rate")), "0.0000") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("CostOrCredit")), "0.00"));
					if (Conversion.Val(clsLoad.Get_Fields("costorcredit")) < 0)
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORRED);
					}
					clsLoad.MoveNext();
				}
				Grid.AddItem("");
			}
			Grid.Row = 2;
			Grid.Col = 0;
			clsHistoryClass.Init = this;
		}

		private void SaveInfo()
		{
			// save the screen information
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			string strTelephone = "";
			// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
			int intpos;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// make sure no data is stuck in .edittext in a grid
				txtEmployerCode.Focus();
				//App.DoEvents();
				// save employer info first
				boolExitOK = false;
				clsSave.OpenRecordset("select * from tblmsrstable where reporttype = " + FCConvert.ToString(cmbReportType.ItemData(cmbReportType.SelectedIndex)), "twpy0000.vb1");
				if (clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				clsSave.Set_Fields("reporttype", cmbReportType.ItemData(cmbReportType.SelectedIndex));
				clsSave.Set_Fields("EmployerName", fecherFoundation.Strings.Trim(txtEmployerName.Text));
				clsSave.Set_Fields("EmployerCode", fecherFoundation.Strings.Trim(txtEmployerCode.Text));
				clsSave.Set_Fields("PreparersName", fecherFoundation.Strings.Trim(txtPreparersName.Text));
				clsSave.Set_Fields("telephone", txtTelephone.Text);
				// save the ins rates next
				clsSave.Set_Fields("effbasinsrate", FCConvert.ToString(Conversion.Val(txtBasicRate.Text)));
				clsSave.Set_Fields("DepARate", FCConvert.ToString(Conversion.Val(txtDepARate.Text)));
				clsSave.Set_Fields("DepBRate", FCConvert.ToString(Conversion.Val(txtDepBRate.Text)));
				// save premium rates next
				clsSave.Set_Fields("PremiumActive", FCConvert.ToString(Conversion.Val(GridPremiums.TextMatrix(1, 0))));
				clsSave.Set_Fields("PremiumRetiree", FCConvert.ToString(Conversion.Val(GridPremiums.TextMatrix(1, 1))));
				clsSave.Set_Fields("PremiumSupplemental", FCConvert.ToString(Conversion.Val(GridPremiums.TextMatrix(1, 2))));
				clsSave.Set_Fields("PremiumDependent", FCConvert.ToString(Conversion.Val(GridPremiums.TextMatrix(1, 3))));
				// Credits/debits
				clsSave.Set_Fields("CreditOrDebitNo1", fecherFoundation.Strings.Trim(GridCreditsDebits.TextMatrix(1, 0)));
				clsSave.Set_Fields("CreditOrDebitTaken1", FCConvert.ToString(Conversion.Val(GridCreditsDebits.TextMatrix(1, 1))));
				clsSave.Set_Fields("CreditorDebitNo2", fecherFoundation.Strings.Trim(GridCreditsDebits.TextMatrix(2, 0)));
				clsSave.Set_Fields("CreditOrDebitTaken2", FCConvert.ToString(Conversion.Val(GridCreditsDebits.TextMatrix(2, 1))));
				if (chkAutoMatch.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("AutoAddEmployerCredit", true);
				}
				else
				{
					clsSave.Set_Fields("AutoAddEmployerCredit", false);
				}
				if (chkElectronicFile.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("MakeElectronicMSRS", true);
				}
				else
				{
					clsSave.Set_Fields("MakeElectronicMSRS", false);
				}
				// clsSave.Fields("submissionnumber") = Val(txtSubmissionNumber.Text)
				// teacher stuff
				clsSave.Set_Fields("NumDaysWorked", FCConvert.ToString(Conversion.Val(txtDaysWorked.Text)));
				clsSave.Set_Fields("NumPayrollPayments", FCConvert.ToString(Conversion.Val(txtPayrollPayments.Text)));
				clsSave.Set_Fields("fedfundperc", FCConvert.ToString(Conversion.Val(txtFedFundPerc.Text)));
				clsSave.Set_Fields("NumempcoveredLast", FCConvert.ToString(Conversion.Val(txtEmployeesCoveredLast.Text)));
				clsSave.Set_Fields("NumEmpSeparatedlast", FCConvert.ToString(Conversion.Val(txtEmployeesSeparatedLast.Text)));
				clsSave.Set_Fields("NumEmpSeparatedThis", FCConvert.ToString(Conversion.Val(txtEmployeesSeparatedThis.Text)));
				clsSave.Set_Fields("priorperiodcompensationadjustment", FCConvert.ToString(Conversion.Val(txtCompensationAdjustments.Text)));
				clsSave.Set_Fields("priorperiodcontributionadjustment", FCConvert.ToString(Conversion.Val(txtEmployerContributionAdjustments.Text)));
				clsSave.Set_Fields("purchaseagreements", FCConvert.ToString(Conversion.Val(txtPurchaseAgreements.Text)));
				for (intpos = 0; intpos <= 4; intpos++)
				{
					clsSave.Set_Fields("PaidDatesEnding" + FCConvert.ToString(intpos + 1), 0);
					if (Information.IsDate(t2kPaidDatesEnding[intpos].Text) && t2kPaidDatesEnding[intpos].Text != FCConvert.ToString(clsSave.Get_Fields_DateTime("PaidDatesEnding" + FCConvert.ToString(intpos + 1))))
					{
						clsSave.Set_Fields("PaidDatesEnding" + FCConvert.ToString(intpos + 1), t2kPaidDatesEnding[intpos].Text);
					}
				}
				// intpos
				if (cmbReportType.ItemData(cmbReportType.SelectedIndex) == modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER)
				{
					boolTeacher = true;
				}
				else
				{
					boolTeacher = false;
				}
				// If optReportType(0).Value Then
				// pld
				// clsSave.Fields("TeacherOrPld") = False
				// boolTeacher = False
				// Else
				// teacher
				// clsSave.Fields("TeacherOrPld") = True
				// boolTeacher = True
				// End If
				if (cmbRange.Text == "All")
				{
					clsSave.Set_Fields("Range", false);
					clsSave.Set_Fields("RangeStart", "");
					clsSave.Set_Fields("RangeEnd", "");
				}
				else
				{
					clsSave.Set_Fields("Range", true);
					clsSave.Set_Fields("rangestart", fecherFoundation.Strings.Trim(txtStart.Text));
					clsSave.Set_Fields("rangeEnd", fecherFoundation.Strings.Trim(txtEnd.Text));
				}
				clsSave.Update();
				// save the code/rate info last
				clsSave.Execute("DELETE from tblMSRSCodeRate where reporttype = " + FCConvert.ToString(cmbReportType.ItemData(cmbReportType.SelectedIndex)), "twpy0000.vb1");
				for (intpos = 2; intpos <= (Grid.Rows - 1); intpos++)
				{
					if (fecherFoundation.Strings.Trim(Grid.TextMatrix(intpos, 0)) != string.Empty)
					{
						clsSave.Execute("Insert into tblmsrscoderate (code,rate,costorcredit,reporttype) values ('" + fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.TextMatrix(intpos, 0))) + "'," + Strings.Format(Conversion.Val(Grid.TextMatrix(intpos, 1)), "0.0000") + "," + Strings.Format(Conversion.Val(Grid.TextMatrix(intpos, 2)), "0.00") + "," + FCConvert.ToString(cmbReportType.ItemData(cmbReportType.SelectedIndex)) + ")", "twpy0000.vb1");
					}
					Grid.TextMatrix(intpos, 0, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.TextMatrix(intpos, 0))));
				}
				// intpos
				boolExitOK = true;
				clsHistoryClass.Compare();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkAutoMatch_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			if (chkAutoMatch.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 2; x <= (Grid.Rows - 1); x++)
				{
					Grid.TextMatrix(x, 2, "");
				}
				// x
			}
		}

		private void cmbReportType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				LoadInfo(cmbReportType.ItemData(cmbReportType.SelectedIndex), intMonthToUse, lngYearToUse, dtDateToUse);
			}
			switch (cmbReportType.ItemData(cmbReportType.SelectedIndex))
			{
				case modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD:
				case modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD:
					{
						// pld
						//lblTeacherInfo[0].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[1].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[2].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[3].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[4].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[5].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[6].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[7].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[8].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[9].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtDaysWorked.Enabled = false;
						txtPayrollPayments.Enabled = false;
						txtFedFundPerc.Enabled = false;
						txtEmployeesCoveredLast.Enabled = false;
						txtEmployeesSeparatedLast.Enabled = false;
						txtEmployeesSeparatedThis.Enabled = false;
						txtEmployerContributionAdjustments.Enabled = false;
						txtCompensationAdjustments.Enabled = false;
						txtPurchaseAgreements.Enabled = false;
						break;
					}
				case modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER:
					{
						// teacher
						//lblTeacherInfo[0].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[1].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[2].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[3].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[4].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[5].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[6].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[7].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[8].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						//lblTeacherInfo[9].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						txtDaysWorked.Enabled = true;
						txtPayrollPayments.Enabled = true;
						txtFedFundPerc.Enabled = true;
						txtEmployeesCoveredLast.Enabled = true;
						txtEmployeesSeparatedLast.Enabled = true;
						txtEmployeesSeparatedThis.Enabled = true;
						txtEmployerContributionAdjustments.Enabled = true;
						txtCompensationAdjustments.Enabled = true;
						txtPurchaseAgreements.Enabled = true;
						break;
					}
			}
			//end switch
		}

		private void frmMSRSSetup_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			if (modGlobalVariables.Statics.gboolCancelSelected)
			{
				modGlobalVariables.Statics.gboolCancelSelected = false;
				Close();
			}
		}

		private void frmMSRSSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMSRSSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMSRSSetup properties;
			//frmMSRSSetup.ScaleWidth	= 9195;
			//frmMSRSSetup.ScaleHeight	= 8415;
			//frmMSRSSetup.LinkTopic	= "Form1";
			//frmMSRSSetup.LockControls	= -1  'True;
			//End Unmaped Properties
			int lngYear = 0;
			// vbPorter upgrade warning: TempDate As DateTime	OnWrite(string, DateTime)
			DateTime TempDate;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth = 0;
			boolLoading = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//lblTeacherInfo[0].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[1].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[2].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[3].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[4].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[5].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[6].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[7].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[8].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//lblTeacherInfo[9].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			txtDaysWorked.Enabled = false;
			txtPayrollPayments.Enabled = false;
			txtFedFundPerc.Enabled = false;
			txtEmployerContributionAdjustments.Enabled = false;
			txtCompensationAdjustments.Enabled = false;
			txtPurchaseAgreements.Enabled = false;
			SetupcmbReportType();
			SetupGrid();
			SetupGridPremiums();
			SetupGridCreditsDebits();
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
				intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
				//FC:FINAL:DSE:#i2421 Use date parameter by reference
				DateTime tmpArg = DateTime.FromOADate(0);
				frmSelectDateInfo.InstancePtr.Init5("Select the Month and Year for the report", ref lngYear, -1, ref intMonth, ref tmpArg, -1, false);
				if (lngYear == 0)
				{
					modGlobalVariables.Statics.gboolCancelSelected = true;
					return;
				}
				else
				{
					TempDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
					TempDate = modDavesSweetCode.FindLastDay(FCConvert.ToString(TempDate));
				}
			}
			else
			{
				// load the default paid ending date
				TempDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				intMonth = TempDate.Month;
				TempDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(TempDate.Year));
				TempDate = modDavesSweetCode.FindLastDay(FCConvert.ToString(TempDate));
			}
			dtDateToUse = TempDate;
			intMonthToUse = intMonth;
			lngYearToUse = lngYear;
			LoadInfo(modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD, intMonth, lngYear, TempDate);
			boolLoading = false;
			clsHistoryClass.SetGridIDColumn("PY");
		}

		private void frmMSRSSetup_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridPremiums();
			ResizeGridCreditsDebits();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// check for a blank row
			// make sure the only blank is the bottom row
			for (x = 2; x <= (Grid.Rows - 1); x++)
			{
				// the original limit is wrong once you delete rows
				if (x > Grid.Rows - 1)
					break;
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(x, 0)) == string.Empty)
				{
					Grid.RemoveItem(x);
				}
				else
				{
					if (Conversion.Val(Grid.TextMatrix(x, 2)) < 0)
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, x, 2, modGlobalConstants.Statics.TRIOCOLORRED);
					}
					else
					{
						Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, x, 2, modGlobalConstants.Statics.TRIOCOLORBLACK);
					}
				}
			}
			// x
			// add the blank row at the end so it can be used to enter new codes
			Grid.AddItem("");
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						mnuDelete_Click();
						break;
					}
			}
			//end switch
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			Grid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (Grid.Row == Grid.Rows - 1)
			{
				if (Grid.Col == Grid.Cols - 1)
				{
					Grid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Grid.Col == 0)
				return;
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(Grid.Row, 0)) != string.Empty)
			{
				switch (Grid.Col)
				{
					case 1:
						{
							Grid.EditText = Strings.Format(Grid.EditText, "#0.0000");
							break;
						}
					case 2:
						{
							Grid.EditText = Strings.Format(Grid.EditText, "#0.00");
							break;
						}
				}
				//end switch
			}
		}

		private void GridCreditsDebits_RowColChange(object sender, System.EventArgs e)
		{
			GridCreditsDebits.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (GridCreditsDebits.Row == GridCreditsDebits.Rows - 1)
			{
				if (GridCreditsDebits.Col == GridCreditsDebits.Cols - 1)
				{
					GridCreditsDebits.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
		}

		private void GridCreditsDebits_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridCreditsDebits.Col == 1 || GridCreditsDebits.Col == 2)
			{
				GridCreditsDebits.EditText = Strings.Format(GridCreditsDebits.EditText, "#0.00");
			}
		}

		private void GridPremiums_RowColChange(object sender, System.EventArgs e)
		{
			GridPremiums.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (GridPremiums.Row == GridPremiums.Rows - 1)
			{
				if (GridPremiums.Col == GridPremiums.Cols - 1)
				{
					GridPremiums.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
		}

		private void GridPremiums_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			Grid.EditText = Strings.Format(Grid.EditText, "#0.00");
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			if (Grid.Row > 1)
			{
				// not a fixed row
				Grid.RemoveItem(Grid.Row);
			}
			else
			{
				return;
			}
			// remove any blank codes as well
			for (x = 2; x <= (Grid.Rows - 1); x++)
			{
				if (x > Grid.Rows - 1)
					break;
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(x, 0)) == string.Empty)
					Grid.RemoveItem();
			}
			// x
			// now add blank line back, especially since this might be the one they deleted anyway
			Grid.AddItem("");
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuEditReview_Click(object sender, System.EventArgs e)
		{
			// load form to view and edit records
			frmMSRSReview.InstancePtr.Init(ref dtDateToUse);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Grid.FixedRows = 2;
				Grid.ExtendLastCol = true;
				Grid.TextMatrix(0, 2, "Cost +");
				Grid.TextMatrix(1, 0, "Code");
				Grid.TextMatrix(1, 1, "Rate");
                //FC:FINAL:BSE #2706 fixed row font should be red
                //Grid.TextMatrix(1, 2, "Credit -");
                Grid.TextMatrix(1, 2, "<font color = 'red'> Credit - </font>");
                //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 2, modGlobalConstants.Statics.TRIOCOLORRED);
				Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);

                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGrid");
			}
		}

		private void SetupcmbReportType()
		{
			cmbReportType.Clear();
			cmbReportType.AddItem("PLD");
			cmbReportType.ItemData(cmbReportType.NewIndex, modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD);
			cmbReportType.AddItem("School PLD");
			cmbReportType.ItemData(cmbReportType.NewIndex, modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD);
			cmbReportType.AddItem("Teacher");
			cmbReportType.ItemData(cmbReportType.NewIndex, modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
			cmbReportType.SelectedIndex = 0;
		}

		private void ResizeGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngGridWidth = 0;
				lngGridWidth = Grid.WidthOriginal;
				Grid.ColWidth(0, FCConvert.ToInt32(lngGridWidth * 0.32));
				Grid.ColWidth(1, FCConvert.ToInt32(lngGridWidth * 0.32));
				Grid.ColWidth(2, FCConvert.ToInt32(lngGridWidth * 0.32));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ResizeGrid");
			}
		}

		private void SetupGridPremiums()
		{
			GridPremiums.ExtendLastCol = true;
			GridPremiums.TextMatrix(0, 0, "Active");
			GridPremiums.TextMatrix(0, 1, "Retiree");
			GridPremiums.TextMatrix(0, 2, "Supplemental");
			GridPremiums.TextMatrix(0, 3, "Dependent");
			GridPremiums.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridPremiums.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridPremiums.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridPremiums.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void SetupGridCreditsDebits()
		{
			GridCreditsDebits.TextMatrix(0, 0, "(CM) or DM #");
			GridCreditsDebits.TextMatrix(0, 1, "(Credit) or Debit");
			// .TextMatrix(0, 2) = "(CM) or DM #"
			// .TextMatrix(0, 3) = "(Credit) or Debit"
			GridCreditsDebits.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridCreditsDebits.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGridCreditsDebits()
		{
			int lngGridWidth = 0;
			lngGridWidth = GridCreditsDebits.WidthOriginal;
			GridCreditsDebits.ColWidth(0, FCConvert.ToInt32(0.5 * lngGridWidth));
		}

		private void ResizeGridPremiums()
		{
			int lngGridWidth = 0;
			lngGridWidth = GridPremiums.WidthOriginal;
			GridPremiums.ColWidth(0, FCConvert.ToInt32(0.2 * lngGridWidth));
			GridPremiums.ColWidth(1, FCConvert.ToInt32(0.2 * lngGridWidth));
			GridPremiums.ColWidth(2, FCConvert.ToInt32(0.28 * lngGridWidth));
			GridPremiums.ColWidth(3, FCConvert.ToInt32(0.28 * lngGridWidth));
			//GridPremiums.Height = GridPremiums.RowHeight(0) * 2 + 50;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			bool boolUseElectronic = false;
			clsMSRS MSRSObject = new clsMSRS();
			SaveInfo();
			if (!Information.IsDate(t2kPaidDatesEnding[0].Text))
			{
				MessageBox.Show("You must have at least one paid date to run the report", "No Dates", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolExitOK)
			{
				cmdSaveContinue.Visible = false;
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					// corey 6/29/2005 call id 71070
					modRegistry.SaveRegistryKey("MSRSReportType", FCConvert.ToString(cmbReportType.ItemData(cmbReportType.SelectedIndex)));					
					if (chkElectronicFile.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolUseElectronic = frmElectronicMSRS.InstancePtr.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex), boolNewMonth);
					}
					if (boolUseElectronic)
						MSRSObject.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex));
					rptMSRSNewWayPayrollDetail.InstancePtr.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex), boolUseElectronic, MSRSObject, batchReports: batchReports);
					if (!boolTeacher)
					{
						rptMSRSPayrollSummary.InstancePtr.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex), boolUseElectronic, MSRSObject, batchReports: batchReports);
					}
					else
					{
						// Call rptMSRSPayrollSummaryTeacher.Init
						rptMSRSNewPayrollSummary.InstancePtr.Init(boolUseElectronic, MSRSObject, batchReports: batchReports);
					}
				}
				else
				{
					// Call rptMSRSPayrollDetail.Init
					if (chkElectronicFile.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolUseElectronic = frmElectronicMSRS.InstancePtr.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex), boolNewMonth);
					}
					if (boolUseElectronic)
						MSRSObject.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex));
					rptMSRSNewWayPayrollDetail.InstancePtr.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex), boolUseElectronic, MSRSObject, batchReports: batchReports);
					if (!boolTeacher)
					{
						rptMSRSPayrollSummary.InstancePtr.Init(cmbReportType.ItemData(cmbReportType.SelectedIndex), boolUseElectronic, MSRSObject, batchReports: batchReports);
					}
					else
					{
						// Call rptMSRSPayrollSummaryTeacher.Init
						rptMSRSNewPayrollSummary.InstancePtr.Init(boolUseElectronic, MSRSObject, batchReports: batchReports);
					}
				}
				mnuExit_Click();
			}
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 1:
					{
						// all
						lblTo.Visible = false;
						txtStart.Visible = false;
						txtEnd.Visible = false;
						lblSequence.Visible = false;
						break;
					}
				case 0:
					{
						// range
						lblTo.Visible = true;
						txtStart.Visible = true;
						txtEnd.Visible = true;
						lblSequence.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void optReportType_Click(ref int Index)
		{
			switch (Index)
			{
				case 0:
					{
						// pld
						//lblTeacherInfo[0].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[1].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[2].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[3].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[4].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[5].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[6].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[7].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[8].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						//lblTeacherInfo[9].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtDaysWorked.Enabled = false;
						txtPayrollPayments.Enabled = false;
						txtFedFundPerc.Enabled = false;
						txtEmployeesCoveredLast.Enabled = false;
						txtEmployeesSeparatedLast.Enabled = false;
						txtEmployeesSeparatedThis.Enabled = false;
						txtEmployerContributionAdjustments.Enabled = false;
						txtCompensationAdjustments.Enabled = false;
						txtPurchaseAgreements.Enabled = false;
						break;
					}
				case 1:
					{
						// teacher
						lblTeacherInfo[0].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[1].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[2].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[3].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[4].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[5].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[6].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[7].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[8].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						lblTeacherInfo[9].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
						txtDaysWorked.Enabled = true;
						txtPayrollPayments.Enabled = true;
						txtFedFundPerc.Enabled = true;
						txtEmployeesCoveredLast.Enabled = true;
						txtEmployeesSeparatedLast.Enabled = true;
						txtEmployeesSeparatedThis.Enabled = true;
						txtEmployerContributionAdjustments.Enabled = true;
						txtCompensationAdjustments.Enabled = true;
						txtPurchaseAgreements.Enabled = true;
						break;
					}
			}
			//end switch
		}
	}
}
