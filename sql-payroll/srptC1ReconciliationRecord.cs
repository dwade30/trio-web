﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1ReconciliationlRecord.
	/// </summary>
	public partial class srptC1ReconciliationlRecord : FCSectionReport
	{
		public srptC1ReconciliationlRecord()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1ReconciliationlRecord InstancePtr
		{
			get
			{
				return (srptC1ReconciliationlRecord)Sys.GetInstance(typeof(srptC1ReconciliationlRecord));
			}
		}

		protected srptC1ReconciliationlRecord _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1ReconciliationlRecord	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strRec;
			string strTemp;
			double dblTemp;
			int lngTemp;
			strRec = FCConvert.ToString(this.UserData);
			strTemp = Strings.Mid(strRec, 2, 8);
			strTemp = Strings.Mid(strTemp, 1, 2) + "/" + Strings.Mid(strTemp, 3, 2) + "/" + Strings.Mid(strTemp, 5);
			txtDatePaid.Text = strTemp;
			strTemp = Strings.Mid(strRec, 10, 9);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtWH.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 19, 9);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtPayment.Text = Strings.Format(dblTemp, "#,###,##0.00");
		}

		
	}
}
