﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPayCode
	{
		//=========================================================
		private int lngRecordID;
		private string strPayCode = string.Empty;
		private string strDescription = string.Empty;
		private double dblRate;
		private string strLastUserID = string.Empty;
		private string strLastUpdate = "";

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string PayCode
		{
			set
			{
				strPayCode = value;
			}
			get
			{
				string PayCode = "";
				PayCode = strPayCode;
				return PayCode;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public double Rate
		{
			set
			{
				dblRate = value;
			}
			get
			{
				double Rate = 0;
				Rate = dblRate;
				return Rate;
			}
		}

		public string LastUserID
		{
			set
			{
				strLastUserID = value;
			}
			get
			{
				string LastUserID = "";
				LastUserID = strLastUserID;
				return LastUserID;
			}
		}

		public string LastUpdate
		{
			set
			{
				if (fecherFoundation.Information.IsDate(value))
				{
					strLastUpdate = value;
				}
				else
				{
					strLastUpdate = "";
				}
			}
			get
			{
				string LastUpdate = "";
				LastUpdate = strLastUpdate;
				return LastUpdate;
			}
		}
	}
}
