//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmEmployeeDeductions : BaseForm
	{
		public frmEmployeeDeductions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployeeDeductions InstancePtr
		{
			get
			{
				return (frmEmployeeDeductions)Sys.GetInstance(typeof(frmEmployeeDeductions));
			}
		}

		protected frmEmployeeDeductions _InstancePtr = null;
        private clsDRWrapper rsDeductions = new clsDRWrapper();
		private clsDRWrapper rsDeductionSetup = new clsDRWrapper();
        private int intCounter;
		private int intDataChanged;
		private bool boolUpdateDeduction;
		private double dblAmtPercentTotal;
        private int intChildID;
		private bool boolCancelSave;
		private bool boolCantEdit;
		private bool boolTempMode;
        clsAuditControlInformation[] clsControlInfo = null;
        clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
        int intTotalNumberOfControls;
        const int CNSTGRIDCOLDIRTY = 0;
		const int CNSTGRIDCOLROWNUM = 1;
		const int CNSTGRIDCOLFIXEDCOL = 2;
		const int CNSTGRIDCOLDEDNUMBER = 3;
		const int CNSTGRIDCOLDESCRIPTION = 4;
		const int CNSTGRIDCOLTAX = 5;
		const int CNSTGRIDCOLAMOUNT = 6;
		const int CNSTGRIDCOLAMOUNTTYPE = 7;
		const int CNSTGRIDCOLPERCENT = 8;
		const int CNSTGRIDCOLFREQ = 9;
		const int CNSTGRIDCOLLIMIT = 10;
		const int CNSTGRIDCOLLIMITTYPE = 11;
		const int CNSTGRIDCOLPRIORITY = 12;
		const int cnstgridcolID = 13;
		const int CNSTGRIDCOLDEDID = 14;
		const int CNSTGRIDCOLDEFAULT = 15;
		const int CNSTGRIDCOLLOAN = 16;
		const int CNSTGRIDTOTALCOLLTD = 9;

		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsDeductions.Row < 0)
					return;
				if (vsDeductions.Col < 0)
					return;
                DateTime dtDate;
				DateTime dtTemp;
				dtDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
				dtTemp = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp, dtDate) > 0)
				{
					dtDate = dtTemp;
				}
				clsDRWrapper rsData = new clsDRWrapper();
				if (Conversion.Val(vsDeductions.TextMatrix(vsDeductions.Row, cnstgridcolID)) > 0)
				{
					rsData.OpenRecordset("select * from tblcheckdetail where deductionrecord = 1 and empdeductionid = " + vsDeductions.TextMatrix(vsDeductions.Row, cnstgridcolID) + " and checkvoid = 0 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and paydate >= '" + FCConvert.ToString(dtDate) + "' order by paydate desc", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						MessageBox.Show("There is a history record for the Pay Date of " + rsData.Get_Fields_DateTime("PayDate") + "\r\n" + "This deduction cannot be deleted.", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}
                if (MessageBox.Show("This action will delete record #" + vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, CNSTGRIDCOLROWNUM) + ". Continue?", "Payroll Employee Deductions", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
                    modGlobalFunctions.AddCYAEntry_6("PY", "Deduction Number " + fecherFoundation.Strings.Trim(Strings.Left(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER)), CNSTGRIDCOLAMOUNTTYPE)) + " was Deleted", "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
                    modAuditReporting.RemoveGridRow_8("vsDeductions", intTotalNumberOfControls - 1, vsDeductions.Row, clsControlInfo);
					// We then add a change record saying the row was deleted
					clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsDeductions.Row) + " from Deductions");
					// -------------------------------------------------------------------
					if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, cnstgridcolID)) == string.Empty)
					{
						// this is a new record that has not been saved
						vsDeductions.RemoveItem(vsDeductions.Row);
					}
					else
					{
						rsDeductions.Execute("Delete from tblEmployeeDeductions Where ID = " + vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, cnstgridcolID), "Payroll");
						// clear the edit symbol from the row number
						vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDIRTY, Strings.Mid(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDIRTY), 1, (vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDIRTY).Length < 2 ? 2 : vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDIRTY).Length - 2)));
						// load the grid with the new data to show the 'clean out' of the current row
						LoadGrid();
					}
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Employee Deductions", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// End If
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(mnuDelete, new System.EventArgs());
		}

		public object DeductionNotUsed(int intDeduction)
		{
			object DeductionNotUsed = null;
			clsDRWrapper rsCheck = new clsDRWrapper();
			rsCheck.OpenRecordset("Select * from tblEmployersMatch where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and DeductionCode = " + FCConvert.ToString(intDeduction), "Twpy0000.vb1");
			if (rsCheck.EndOfFile())
			{
				DeductionNotUsed = true;
			}
			else
			{
				MessageBox.Show("Employee Deduction is used in the Employee Match screen. This deduction cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				DeductionNotUsed = false;
			}
			return DeductionNotUsed;
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				LoadGrid();
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				int x;
				boolCancelSave = false;
				SaveData = false;
				if (NotValidData() == true)
					return SaveData;
				bool boolNew = false;
				clsDRWrapper rsOldDeduction = new clsDRWrapper();
				rsOldDeduction.OpenRecordset("Select * from tblEmployeeDeductions where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'");
				vsDeductions.Select(0, 0);
				vsTotals.Select(0, 0);
				if (boolCancelSave)
					return SaveData;
				for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
				{
					if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLDEDNUMBER)) == string.Empty)
					{
					}
					else
					{
						rsDeductions.OpenRecordset("Select * from tblDeductionSetup where DeductionNumber = " + FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLDEDID))), "TWPY0000.vb1");
						if (!rsDeductions.EndOfFile())
						{
							rsDeductionSetup.FindFirstRecord("DeductionNumber", rsDeductions.Get_Fields_Int32("DeductionNumber"));
						}
						rsDeductions.OpenRecordset("Select * from tblEmployeeDeductions where ID = " + FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, cnstgridcolID))), "TWPY0000.vb1");
						// Dave 12/14/2006--------------------------------------------
						// Set New Information so we can compare
						for (x = 0; x <= intTotalNumberOfControls - 1; x++)
						{
							clsControlInfo[x].FillNewValue(this);
						}
						// Thsi function compares old and new values and creates change records for any differences
						modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
						// This function takes all the change records and writes them into the AuditChanges table in the database
						clsReportChanges.SaveToAuditChangesTable("Deductions", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
						// Reset all information pertianing to changes and start again
						intTotalNumberOfControls = 0;
						clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
						clsReportChanges.Reset();
						// Initialize all the control and old data values
						FillControlInformationClassFromGrid();
						for (x = 0; x <= intTotalNumberOfControls - 1; x++)
						{
							clsControlInfo[x].FillOldValue(this);
						}
						// ----------------------------------------------------------------
						if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLDIRTY)), 2) == ".." || Strings.Right(FCConvert.ToString(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
						{
							boolNew = false;
							rsDeductions.Edit();
						}
						else if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLDIRTY)), 1) == "." || Strings.Right(FCConvert.ToString(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
						{
							boolNew = true;
							rsDeductions.AddNew();
						}
						else
						{
							goto NextRecord;
						}
						// Call SaveHistoryRecord(rsDeductions, rsDeductionSetup, intCounter)
						rsDeductions.SetData("DeductionNumber", rsDeductionSetup.Get_Fields_Int32("DeductionNumber"));
						rsDeductions.SetData("RecordNumber", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLROWNUM));
						rsDeductions.Set_Fields("RecordNumber", intCounter);
						rsDeductions.SetData("DeductionCode", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLDEDNUMBER) + " "));
						if (vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLDESCRIPTION) == rsDeductionSetup.Get_Fields("Description"))
						{
							rsDeductions.SetData("Description", "");
						}
						else
						{
							rsDeductions.SetData("Description", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLDESCRIPTION));
						}
						// DON'T SAVE THE TAX CODE AS WE ALWAYS WANT TO USE THE ONE FROM THE SETUP SCREEN.
						// If Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, cnstgridcoltax)) = rsDeductionSetup.Fields("TaxStatusCode") Then
						// rsDeductions.SetData "TaxStatusCode", 0
						// Else
						// rsDeductions.SetData "TaxStatusCode", Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, cnstgridcoltax))
						// End If
						if (Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLAMOUNT)) == rsDeductionSetup.Get_Fields("Amount"))
						{
							rsDeductions.SetData("Amount", 0);
						}
						else
						{
							rsDeductions.SetData("Amount", FCConvert.ToDouble(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLAMOUNT)));
						}
						if (vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLAMOUNTTYPE) == rsDeductionSetup.Get_Fields_String("AmountType"))
						{
							rsDeductions.SetData("AmountType", "");
						}
						else
						{
							rsDeductions.SetData("AmountType", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLAMOUNTTYPE));
						}
						rsDeductions.SetData("PercentType", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLPERCENT));
						rsDeductions.Set_Fields("ICMALoan", fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLOAN)));
						// If Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, cnstgridcolfreq)) = cnstgridcollimittype Then
						// rsDeductions.Fields("HoldFreqCode") = rsDeductions.Fields("FrequencyCode")
						// Else
						// rsDeductions.Fields("HoldFreqCode") = 0
						// End If
						if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLAMOUNTTYPE)) == "Levy")
						{
							// rsDeductions.SetData "FrequencyCode", 1
							rsDeductions.SetData("FrequencyCode", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ)));
							rsDeductions.SetData("Priority", 9);
						}
						else
						{
							if (Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ)) == rsDeductionSetup.Get_Fields("FrequencyCode"))
							{
								rsDeductions.SetData("FrequencyCode", 0);
							}
							else
							{
								rsDeductions.SetData("FrequencyCode", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ)));
							}
							if (Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLPRIORITY)) == rsDeductionSetup.Get_Fields("Priority"))
							{
								rsDeductions.SetData("Priority", 0);
							}
							else
							{
								rsDeductions.SetData("Priority", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLPRIORITY)));
							}
						}
						// SAVE THE VALUE OF THE FREQUENCY SO THAT IF IT IS EVERY CHANGED TO
						// AND N OR P THEN THE RESET TEMP FLAG REPORT WILL KNOW WHAT TO REVERT IT TO
						if (Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ)) != 9 && Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ)) != CNSTGRIDCOLLIMITTYPE)
						{
							rsDeductions.SetData("OldFrequency", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ)));
						}
						if (Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLLIMIT)) == rsDeductionSetup.Get_Fields_Double("Limit"))
						{
							rsDeductions.SetData("Limit", 0);
						}
						else
						{
							rsDeductions.SetData("Limit", Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLLIMIT)));
						}
						rsDeductions.SetData("LimitType", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLLIMITTYPE));
						rsDeductions.SetData("LTDTotal", Conversion.Val(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 9)));
						rsDeductions.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
						rsDeductions.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
						rsDeductions.Update();
						/*? On Error Resume Next  */
						if (boolNew)
						{
							// clear the new symbol from the row number
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY, Strings.Mid(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY), 1, vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY).Length - 1));
							vsTotals.TextMatrix(intCounter, 0, Strings.Mid(vsTotals.TextMatrix(intCounter, 0), 1, vsTotals.TextMatrix(intCounter, 0).Length - 1));
							// get the new ID from the table for the record that was just added and place it into column cnstgridcoldescription
							rsDeductions.OpenRecordset("Select Max(ID) as NewID from tblEmployeeDeductions", "TWPY0000.vb1");
							vsDeductions.TextMatrix(intCounter, cnstgridcolID, FCConvert.ToString(rsDeductions.Get_Fields("NewID")));
							vsTotals.TextMatrix(intCounter, 10, FCConvert.ToString(rsDeductions.Get_Fields("NewID")));
						}
						else
						{
							// clear the edit symbol from the row number
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY, Strings.Mid(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY), 1, vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY).Length - 2));
							if (Strings.Right(vsTotals.TextMatrix(intCounter, 0), 2) == "..")
							{
								vsTotals.TextMatrix(intCounter, 0, Strings.Mid(vsTotals.TextMatrix(intCounter, 0), 1, vsTotals.TextMatrix(intCounter, 0).Length - 2));
							}
						}
					}
					fecherFoundation.Information.Err().Clear();
					NextRecord:
					;
				}
				// Call clsHistoryClass.Compare
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Employee Deductions", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//FC:FINAL:DSE:#i2263 Prevent Collection Modified exception by disabling grid client update
				vsDeductions.Redraw = false;
				vsTotals.Redraw = false;
				LoadGrid();
				//FC:FINAL:DSE:#i2263 Reenable client grid update
				vsTotals.Redraw = true;
				vsDeductions.Redraw = true;
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}
		
		private bool NotValidData()
		{
			bool NotValidData = false;
			dblAmtPercentTotal = 0;
			for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
			{
				if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent")
				{
					dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
				}
				if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLOAN).Length > 3)
				{
					MessageBox.Show("ICMA loan numbers cannot be greater than 3 digits", "Bad Loan Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return NotValidData;
					NotValidData = true;
				}
			}
			if (dblAmtPercentTotal > 100)
			{
				MessageBox.Show("Amount Totals For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				NotValidData = true;
				return NotValidData;
			}
			NotValidData = false;
            return NotValidData;
		}

		public void frmEmployeeDeductions_Activated(object sender, System.EventArgs e)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
				this.vsDeductions.Select(0, 0);
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if key is the ins key then add a new record
				if (KeyCode == Keys.Insert && !boolCantEdit)
				{
					cmdNew_Click();
				}
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the key press is ESC then close out this form
				if (KeyAscii == Keys.Escape)
					Close();
				//e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductions_Load(object sender, System.EventArgs e)
		{
            int counter;
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modGlobalRoutines.Statics.gboolShowVerifyTotalsMessage = false;
				modGlobalRoutines.CheckCurrentEmployee();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				rsDeductions.DefaultDB = "Payroll";
				rsDeductionSetup.DefaultDB = "Payroll";

				rsDeductionSetup.OpenRecordset("Select * from tblDeductionSetup Order by ID", "TWPY0000.vb1");
                SetGridProperties();
                SetTotalGridProperties();
                LoadGrid();
				modMultiPay.Statics.gstrEmployeeCaption = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modMultiPay.SetMultiPayCaptions(this);
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					lblEmployeeNumber.Text = "";
				}
				else
				{
					lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				}
				boolCantEdit = false;
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					cmdNewDeduction.Enabled = false;
					cmdDeleteDeduction.Enabled = false;
					cmdSave.Enabled = false;
					mnuSaveExit.Enabled = false;
					cmdTemporaryOverrides.Enabled = false;
					vsTotals.Enabled = false;
					vsDeductions.Enabled = false;
				}
				vsTotals.Select(0, 10);
                FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                vsDeductions.AddItem(FCConvert.ToString(vsDeductions.Rows));
				vsTotals.AddItem(FCConvert.ToString(vsTotals.Rows));
				vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                intDataChanged += 1;
                vsDeductions.TextMatrix(vsDeductions.Rows - 1, CNSTGRIDCOLDIRTY, vsDeductions.TextMatrix(vsDeductions.Rows - 1, CNSTGRIDCOLDIRTY) + ".");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 0, vsTotals.TextMatrix(vsTotals.Rows - 1, 0) + ".");
				vsDeductions.TextMatrix(vsDeductions.Rows - 1, CNSTGRIDCOLROWNUM, FCConvert.ToString(vsDeductions.Rows - 1));
				vsTotals.TextMatrix(vsTotals.Rows - 1, 1, FCConvert.ToString(vsTotals.Rows - 1));
				vsDeductions.Select(vsDeductions.Rows - 1, CNSTGRIDCOLDEDNUMBER);
				vsTotals.Select(vsTotals.Rows - 1, 2);
				vsTotals.TextMatrix(vsTotals.Rows - 1, 4, "0.0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 5, "0.0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 6, "0.0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 7, "0.0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 8, "0.0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 9, "0.0");
				vsDeductions.TextMatrix(vsDeductions.Rows - 1, CNSTGRIDCOLLIMIT, "999,999.99");
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Rows - 1, CNSTGRIDCOLLIMITTYPE, 1);
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLDESCRIPTION, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLTAX, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLDEFAULT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                clsReportChanges.AddChange("Added Row to Deductions");
                vsDeductions.EditCell();
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNewDeduction, new System.EventArgs());
		}

		private void LoadGrid()
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                vsDeductions.Rows = 1;
				vsTotals.Rows = 1;
				// get all of the records from the database
				rsDeductions.OpenRecordset("Select * from tblEmployeeDeductions Where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' Order by RecordNumber", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
                    rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
                    vsDeductions.Rows = rsDeductions.RecordCount() + 1;
					vsTotals.Rows = vsDeductions.Rows;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY, FCConvert.ToString(intCounter));
						if (intCounter != FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("RecordNumber") + " ")))
						{
                            rsDeductions.Edit();
							rsDeductions.Set_Fields("recordnumber", intCounter);
							rsDeductions.Update();
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLROWNUM, FCConvert.ToString(intCounter));
						}
						else
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLROWNUM, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("RecordNumber") + " "));
						}
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDNUMBER, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionCode") + " "))));
                        rsDeductionSetup.FindFirstRecord("ID", rsDeductions.Get_Fields_Int32("DeductionCode"));
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDESCRIPTION, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " ") == string.Empty ? fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("Description") + " ") : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " ")));
                        vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX, modGlobalRoutines.GetDeductionTaxStatusCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionCode") + " "))))));
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLOAN, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("ICMALoan"))));
						if (FCConvert.ToString(rsDeductions.Get_Fields_String("AmountType")) == "Levy")
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("Amount"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("AmountType"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("PercentType"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLFREQ, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("FrequencyCode"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields_Double("Limit"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPRIORITY, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("Priority"))));
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLAMOUNT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                            vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMITTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
						else
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Amount") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("Amount") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Amount") + " ")))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " ") == string.Empty ? fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields_String("AmountType") + " ") : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " ")));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("PercentType") + " ") == string.Empty ? "" : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("PercentType") + " ")));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLFREQ, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("FrequencyCode") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " ")))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields_Double("Limit") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " ")))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("LimitType") + " ") == string.Empty ? "" : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("LimitType") + " ")));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPRIORITY, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("Priority") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " ")))));
							if (FCConvert.ToString(rsDeductions.Get_Fields("amounttype")) == "Dollars")
							{
								vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
						}
						vsDeductions.TextMatrix(intCounter, cnstgridcolID, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("ID") + " "));
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDID, fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields_Int32("DeductionNumber") + " "));

						vsTotals.TextMatrix(intCounter, 0, vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLROWNUM));
						vsTotals.TextMatrix(intCounter, 1, vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDNUMBER));
						vsTotals.TextMatrix(intCounter, 2, vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDESCRIPTION));
						vsTotals.TextMatrix(intCounter, 3, vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX));
						vsTotals.TextMatrix(intCounter, 4, FCConvert.ToString(modCoreysSweeterCode.GetCurrentDeductionTotal(rsDeductions.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsDeductions.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 5, FCConvert.ToString(modCoreysSweeterCode.GetMTDDeductionTotal(rsDeductions.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsDeductions.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 6, FCConvert.ToString(modCoreysSweeterCode.GetQTDDeductionTotal(rsDeductions.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsDeductions.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 7, FCConvert.ToString(modCoreysSweeterCode.GetFYTDDeductionTotal(rsDeductions.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsDeductions.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 8, FCConvert.ToString(modCoreysSweeterCode.GetCYTDDeductionTotal(rsDeductions.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsDeductions.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 9, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("LTDTotal") + " "))));
						vsTotals.TextMatrix(intCounter, 10, vsDeductions.TextMatrix(intCounter, cnstgridcolID));
                        // GRAY OUT THAT DEDUCTION IF THERE IS SOMETHING IN THE TOTAL
                        // GRID AS THEY SHOULD NOT BE ABLE TO ALTER THIS DEDUCTION NUMBER
                        if (Conversion.Val(vsTotals.TextMatrix(intCounter, 4)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 5)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 6)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 7)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 8)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 9)) != 0)
						{
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEDNUMBER, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
						else
						{
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEDNUMBER, Color.White);
						}
						// CHECK TO SEE IF THIS ITEM HAS HIT ITS LIMIT
						// "#1;Life|#2;Fiscal|#3;Calendar|#4;Quarter|#5;Month|#6;Period"
						if (fecherFoundation.Strings.UCase(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE)) == "PERIOD")
						{
							if (Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(intCounter, 4)))
							{
								if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) != 0)
								{
									// limit has been reached
									vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, Color.Yellow);
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE)) == "MONTH")
						{
							if (Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(intCounter, 5)))
							{
								if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) != 0)
								{
									// limit has been reached
									vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, Color.Yellow);
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE)) == "QUARTER")
						{
							if (Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(intCounter, 6)))
							{
								if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) != 0)
								{
									// limit has been reached
									vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, Color.Yellow);
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE)) == "CALENDAR")
						{
							if (Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(intCounter, 8)))
							{
								if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) != 0)
								{
									// limit has been reached
									vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, Color.Yellow);
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE)) == "FISCAL")
						{
							if (Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(intCounter, 7)))
							{
								if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) != 0)
								{
									// limit has been reached
									vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, Color.Yellow);
								}
							}
						}
						else if (fecherFoundation.Strings.UCase(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE)) == "LIFE")
						{
							if (Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(intCounter, 9)))
							{
								if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT)) != 0)
								{
									// limit has been reached
									vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, Color.Yellow);
								}
							}
						}
						// is this record using the default information
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEFAULT, FCConvert.ToString(IsDefaultDedutionGrid(intCounter)));
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEFAULT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						rsDeductions.MoveNext();
					}
					
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLDESCRIPTION, vsDeductions.Rows - 1, CNSTGRIDCOLDESCRIPTION, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLTAX, vsDeductions.Rows - 1, CNSTGRIDCOLTAX, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                    
                    vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor,1, 1, vsTotals.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                    vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor,1,2, vsTotals.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                    
                    intDataChanged = 0;
				}
				FillControlInformationClassFromGrid();
				int counter;
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
            }
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                Close();
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                if (intDataChanged > 0 && !boolCantEdit)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
                        cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
            }
			catch (Exception ex)
			{
                modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductions_Resize(object sender, System.EventArgs e)
		{
			vsDeductions.WordWrap = true;
			vsDeductions.Cols = 17;
			vsDeductions.FixedCols = 3;
			vsDeductions.FixedRows = 1;
			vsDeductions.ColHidden(CNSTGRIDCOLDIRTY, true);
			vsDeductions.ColHidden(CNSTGRIDCOLROWNUM, true);
			vsTotals.ColHidden(0, true);
			vsDeductions.ColHidden(cnstgridcolID, true);
			vsDeductions.ColHidden(CNSTGRIDCOLDEDID, true);
			vsDeductions.RowHeight(0, FCConvert.ToInt32(vsDeductions.HeightOriginal * 0.17));
            vsDeductions.ColWidth(CNSTGRIDCOLFIXEDCOL, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.02));
			vsDeductions.ColWidth(CNSTGRIDCOLDEDNUMBER, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.045));
			vsDeductions.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.14));
			vsDeductions.ColWidth(CNSTGRIDCOLTAX, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.05));
			vsDeductions.ColWidth(CNSTGRIDCOLAMOUNT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(CNSTGRIDCOLAMOUNTTYPE, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(CNSTGRIDCOLPERCENT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.07));
			vsDeductions.ColWidth(CNSTGRIDCOLFREQ, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.05));
			vsDeductions.ColWidth(CNSTGRIDCOLLIMIT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.13));
			vsDeductions.ColWidth(CNSTGRIDCOLLIMITTYPE, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(CNSTGRIDCOLPRIORITY, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.08));
			vsDeductions.ColWidth(CNSTGRIDCOLDEFAULT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.065));
			vsDeductions.ColWidth(CNSTGRIDCOLLOAN, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.065));
            vsTotals.ColWidth(0, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.05));
			vsTotals.ColWidth(1, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.05));
			vsTotals.ColWidth(2, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.19));
			vsTotals.ColWidth(3, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.05));
			vsTotals.ColWidth(4, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(5, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(6, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(7, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(8, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(9, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
		}

		private void SetGridProperties()
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                vsDeductions.WordWrap = true;
				vsDeductions.Cols = 17;
				vsDeductions.FixedCols = 3;
				vsDeductions.FixedRows = 1;
				vsDeductions.ColHidden(CNSTGRIDCOLDIRTY, true);
				vsDeductions.ColHidden(CNSTGRIDCOLROWNUM, true);
				vsTotals.ColHidden(0, true);
				vsDeductions.ColHidden(cnstgridcolID, true);
				vsDeductions.ColHidden(CNSTGRIDCOLDEDID, true);
                modGlobalRoutines.CenterGridCaptions(ref vsDeductions);
                vsDeductions.ColAlignment(CNSTGRIDCOLDIRTY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDIRTY, 400);
                vsDeductions.ColAlignment(CNSTGRIDCOLROWNUM, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLROWNUM, 400);
                vsDeductions.ColAlignment(CNSTGRIDCOLDEDNUMBER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDEDNUMBER, 500);
                vsDeductions.ColAllowedKeys(CNSTGRIDCOLAMOUNT, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.','-',48..57");
				vsDeductions.ColAllowedKeys(CNSTGRIDCOLLIMIT, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				vsDeductions.ColAllowedKeys(CNSTGRIDCOLPRIORITY, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				vsDeductions.ColAllowedKeys(CNSTGRIDCOLLOAN, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				vsDeductions.TextMatrix(0, CNSTGRIDCOLDEDNUMBER, "Ded");
				rsDeductions.OpenRecordset("Select * from tblDeductionSetup order by DeductionNumber", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// This forces this column to work as a combo box
					// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
					int intCounter2;
					for (intCounter2 = 1; intCounter2 <= (rsDeductions.RecordCount()); intCounter2++)
					{
						if (intCounter2 == 1)
						{
							vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER, vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						else
						{
							vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER, vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						rsDeductions.MoveNext();
					}
				}
				// set column cnstgridcoldescription properties
				vsDeductions.ColAlignment(CNSTGRIDCOLDESCRIPTION, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDESCRIPTION, 1350);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
				// set column cnstgridcoltax properties
				vsDeductions.ColAlignment(CNSTGRIDCOLTAX, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLTAX, 300);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLTAX, "Tax");
				rsDeductions.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							vsDeductions.ColComboList(CNSTGRIDCOLTAX, vsDeductions.ColComboList(CNSTGRIDCOLTAX) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						else
						{
							vsDeductions.ColComboList(CNSTGRIDCOLTAX, vsDeductions.ColComboList(CNSTGRIDCOLTAX) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						rsDeductions.MoveNext();
					}
				}
				vsDeductions.ColComboList(CNSTGRIDCOLPERCENT, "Gross|Net");
				// set column cnstgridcolamount properties
				vsDeductions.ColAlignment(CNSTGRIDCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLAMOUNT, 1100);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLAMOUNT, "Amount");
				// vsDeductions.ColFormat(CNSTGRIDCOLAMOUNT) = "(#,###.00)"
				// set column cnstgridcolamounttype properties
				vsDeductions.ColAlignment(CNSTGRIDCOLAMOUNTTYPE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLAMOUNTTYPE, 800);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLAMOUNTTYPE, "Amount Type");
				vsDeductions.ColComboList(CNSTGRIDCOLAMOUNTTYPE, "#1;Dollars|#2;Percent|#3;Levy");
				// set column cnstgridcolpercent properties
				vsDeductions.ColAlignment(CNSTGRIDCOLPERCENT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLPERCENT, 600);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLPERCENT, " % of");
				// vsDeductions.ColComboList(cnstgridcolpercent, "#1;Gross"
				// will add in the NET option for version 84
				// vsDeductions.ColComboList(cnstgridcolpercent, "#1;Gross|#2;Net"
				// set column cnstgridcolfreq properties
				vsDeductions.ColAlignment(CNSTGRIDCOLFREQ, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLFREQ, 470);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLFREQ, "Freq");
				rsDeductions.OpenRecordset("Select * from tblFrequencyCodes order by ID", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblFrequencyCodes table
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						switch (rsDeductions.Get_Fields_Int32("ID"))
						{
							case 1:
							case 2:
							case 3:
							case 4:
							case 5:
							case 8:
							case 9:
							case 10:
							case 11:
							case 12:
							case 13:
							case 14:
							case 15:
							case 16:
							case 17:
							case 18:
							case 19:
							case 20:
							case 21:
								{
									if (intCounter == 1)
									{
										vsDeductions.ColComboList(CNSTGRIDCOLFREQ, vsDeductions.ColComboList(CNSTGRIDCOLFREQ) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("FrequencyCode") + "\t" + rsDeductions.Get_Fields("Description"));
									}
									else
									{
										vsDeductions.ColComboList(CNSTGRIDCOLFREQ, vsDeductions.ColComboList(CNSTGRIDCOLFREQ) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("FrequencyCode") + "\t" + rsDeductions.Get_Fields("Description"));
									}
									break;
								}
							default:
								{
									break;
								}
						}
						//end switch
						rsDeductions.MoveNext();
					}
				}
				// set column cnstgridcollimit properties
				vsDeductions.ColAlignment(CNSTGRIDCOLLIMIT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLLIMIT, 1100);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLLIMIT, "Limit");
				vsDeductions.ColFormat(CNSTGRIDCOLLIMIT, "#,##0.00;(#,###.00)");
				// set column cnstgridcollimittype properties
				vsDeductions.ColAlignment(CNSTGRIDCOLLIMITTYPE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLLIMITTYPE, 900);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLLIMITTYPE, "Limit Type");
				vsDeductions.ColComboList(CNSTGRIDCOLLIMITTYPE, "#1;Life|#2;Fiscal|#3;Calendar|#4;Quarter|#5;Month|#6;Period|#7;% Gross");
				// set column cnstgridcolpriority properties
				vsDeductions.ColAlignment(CNSTGRIDCOLPRIORITY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLPRIORITY, 650);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLPRIORITY, "Priority");
				// set column 14 properties
				vsDeductions.ColAlignment(CNSTGRIDCOLDEFAULT, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDEFAULT, 700);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLDEFAULT, "Default");
				vsDeductions.ColDataType(CNSTGRIDCOLDEFAULT, FCGrid.DataTypeSettings.flexDTBoolean);
				// icma loan number
				vsDeductions.TextMatrix(0, CNSTGRIDCOLLOAN, "ICMA Loan");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetTotalGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetTotalGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsTotals.WordWrap = true;
				vsTotals.Cols = 11;
				vsTotals.FixedCols = 4;
				vsTotals.ColHidden(10, true);
				//vsTotals.RowHeight(0) *= 2;
				vsTotals.ColHidden(3, true);
				modGlobalRoutines.CenterGridCaptions(ref vsTotals);
				// set column 0 properties
				vsTotals.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(0, 400);
				// set column 1 properties
				vsTotals.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(1, 600);
				vsTotals.TextMatrix(0, 1, "Ded");
				vsTotals.ColComboList(1, vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER));
				// set column 2 properties
				vsTotals.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(2, 2500);
				vsTotals.TextMatrix(0, 2, "Description");
				// set column 3 properties
				vsTotals.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(3, 400);
				vsTotals.TextMatrix(0, 3, "Tax");
				vsTotals.ColComboList(3, vsDeductions.ColComboList(CNSTGRIDCOLTAX));
				// set column 4 properties
				vsTotals.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(4, 900);
				vsTotals.TextMatrix(0, 4, "Current Period");
				vsTotals.ColFormat(4, "$#,##0.00;($#,###.00)");
				// set column 5 properties
				vsTotals.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(5, 900);
				vsTotals.TextMatrix(0, 5, "Month to Date");
				vsTotals.ColFormat(5, "$#,##0.00;($#,###.00)");
				// set column 6 properties
				vsTotals.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(6, 900);
				vsTotals.TextMatrix(0, 6, "Quarter to Date");
				vsTotals.ColFormat(6, "$#,##0.00;($#,###.00)");
				// set column 7 properties
				vsTotals.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(7, 900);
				vsTotals.TextMatrix(0, 7, "Fiscal YTD");
				vsTotals.ColFormat(7, "$#,##0.00;($#,###.00)");
				// set column 8 properties
				vsTotals.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(8, 900);
				vsTotals.TextMatrix(0, 8, "Calendar YTD");
				vsTotals.ColFormat(8, "$#,##0.00;($#,###.00)");
				vsTotals.ColAllowedKeys(8, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				// set column 9 properties
				vsTotals.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(9, 900);
				vsTotals.TextMatrix(0, 9, "Life to Date");
				vsTotals.ColFormat(9, "$#,##0.00;($#,###.00)");
				vsTotals.ColAllowedKeys(9, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuCaption1_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(1));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = FCConvert.ToBoolean(0);
			LoadGrid();
			lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
		}

		private void mnuCaption2_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(2));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption3_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(3));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption4_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(4));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption5_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(5));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption6_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(6));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption7_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(7));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption8_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(8));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption9_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(9));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuHistory_Click(object sender, System.EventArgs e)
		{
			frmEmployeeDeductionHistory.InstancePtr.Show();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
			// Call SetVerifyAdjustNeeded(True)
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// Call cmdPrint_Click
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				cmdExit_Click();
			}
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			SaveChanges();
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				modGlobalRoutines.SetEmployeeCaption(lblEmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				LoadGrid();
			}
		}

		private void mnuTemporaryOverrides_Click(object sender, System.EventArgs e)
		{
			frmTempEmployeeSetup.InstancePtr.Show(App.MainForm);
		}

		private void vsDeductions_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsDeductions.Col == CNSTGRIDCOLAMOUNTTYPE)
			{
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLAMOUNT, Color.White);
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLFREQ, Color.White);
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLLIMITTYPE, Color.White);
				if (vsDeductions.EditText == "Percent")
				{
					if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
					{
						vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
					}
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
				}
				else if (vsDeductions.EditText == "Levy")
				{
                    int intCounter;
					vsDeductions.Select(vsDeductions.Row, CNSTGRIDCOLFREQ);
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "3" || vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Levy")
						{
							if (vsDeductions.Row != intCounter)
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE, "Dollars");
								vsDeductions.Select(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE);
								return;
							}
						}
					}
					// disable a few of the fields
					vsDeductions.Select(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE);
					// vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLFREQ) = "1"
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNT, "0.00");
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLAMOUNT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLFREQ) = TRIOCOLORGRAYBACKGROUND
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLLIMITTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
		}

		private void vsDeductions_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == (Keys)68)
			{
				vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
            }
			else if (KeyCode == (Keys)80)
			{
				vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
            }
			modGridKeyMove.MoveGridProsition(ref vsDeductions, 1, CNSTGRIDCOLPRIORITY, e.KeyCode, 9999, true);
		}

		private void vsDeductions_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_KeyPressEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int KeyAscii = Strings.Asc(e.KeyChar);
				// allow the entry of a decimal point
				if (KeyAscii == 46)
					return;
				// allow the entry of a backspace
				if (KeyAscii == 8)
					return;
				if (vsDeductions.Col == CNSTGRIDCOLAMOUNT || vsDeductions.Col == CNSTGRIDCOLLIMIT)
				{
					// if the user is in the AMOUNT or LIMIT columns then do not
					// allow the entry of a character
					if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 45)
					{
						KeyAscii = 0;
					}
				}
				e.KeyChar = Strings.Chr(KeyAscii);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDeductions_KeyUpEvent(object sender, KeyEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_KeyUp";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// this variable is used in the form frmHelp to determine what data to show
				modGlobalVariables.Statics.gstrHelpFieldName = string.Empty;
				if (e.KeyCode == Keys.F9)
				{
					switch (vsDeductions.Col)
					{
						case CNSTGRIDCOLDEDNUMBER:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "DEDUCTIONCODE";
								break;
							}
						case CNSTGRIDCOLTAX:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "TAXSTATUSCODE";
								break;
							}
						case CNSTGRIDCOLAMOUNTTYPE:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "AMOUNTTYPE";
								break;
							}
						case CNSTGRIDCOLFREQ:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "FREQUENCYCODE";
								break;
							}
						case CNSTGRIDCOLLIMITTYPE:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "LIMITCODE";
								break;
							}
						default:
							{
								break;
							}
					}
					//end switch
					frmHelp.InstancePtr.Show();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

        private void vsDeductions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (vsDeductions.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                int col = vsDeductions.GetFlexColIndex(e.ColumnIndex);
                DataGridViewCell cell = vsDeductions[e.ColumnIndex, e.RowIndex];
                if (col == CNSTGRIDCOLLIMIT)
                {
                    cell.ToolTipText = "Yellow background indicates Limit has been reached.";
                }
                else if (col == CNSTGRIDCOLPRIORITY)
                {
                    cell.ToolTipText = "A number of 1 is HIGH and 9 is LOW";
                }
            }
		}

		private void vsDeductions_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_Validate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there are values in the total grid do not allow the user
				// to alter the deduction codes for that record
				vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				//FC:FINAL:DDU:#i2031 - fixed grid editable conversion property
				if (vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbd;
				}
				else
				{
					vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				if (Conversion.Val(vsTotals.TextMatrix(vsDeductions.Row, 4)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsDeductions.Row, 5)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsDeductions.Row, 6)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsDeductions.Row, 7)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsDeductions.Row, 8)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsDeductions.Row, 9)) != 0)
				{
					// need code here to lock this one row
					if (vsDeductions.Col == CNSTGRIDCOLDEDNUMBER)
					{
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDeductions_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// dblAmtPercentTotal = 0
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, CNSTGRIDCOLDIRTY)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDIRTY, vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDIRTY) + "..");
				}
				if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER)) == string.Empty)
				{
					return;
				}
				if (boolUpdateDeduction)
				{
					if (Strings.Right(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, CNSTGRIDCOLDIRTY)), 1) == ".")
					{
						//FC:FINAL:DDU:#i2031 - get just number from column and set it to number only
						string temp = vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER);
                        if (!string.IsNullOrEmpty(temp) && temp.Contains("-"))
                        {
                            int x = temp.IndexOf("-");
                            if (x > -1)
                            {
                                vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER, Conversion.Val(Strings.Trim(temp.Substring(0, x))));
                            }
                        }

                        rsDeductions.OpenRecordset("Select * from tblDeductionSetup where ID = " + vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER), "TWPY0000.vb1");
						if (!rsDeductions.EndOfFile())
						{
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(rsDeductions.Get_Fields("Description")));
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLTAX, FCConvert.ToString(rsDeductions.Get_Fields("TaxStatusCode")));
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNT, FCConvert.ToString(rsDeductions.Get_Fields("Amount")));
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " "));
							if (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " ") == "Percent")
							{
								if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
								{
									vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
								}
								vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
							}
							else
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
								vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLFREQ, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " "))));
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLLIMIT, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " "))));
							if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("LimitTYpe")))) != "PAY-PERIOD")
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLLIMITTYPE, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("LimitType") + " "));
							}
							else
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLLIMITTYPE, "Period");
							}
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPRIORITY, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " "))));
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDEDID, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionNumber") + " "))));
						}
						// increment the counter as to how many records are dirty
						intDataChanged += 1;
					}

				}
				if (vsDeductions.Col == CNSTGRIDCOLDEDNUMBER)
				{
					vsTotals.TextMatrix(vsDeductions.Row, 1, fecherFoundation.Strings.Trim(Strings.Left(vsDeductions.ComboItem(vsDeductions.ComboIndex), 3)));
					vsTotals.TextMatrix(vsDeductions.Row, 2, FCConvert.ToString(rsDeductions.Get_Fields("Description")));
				}
				if (vsDeductions.Col == CNSTGRIDCOLAMOUNT)
				{
					if (Conversion.Val(vsDeductions.EditText) == 0 && vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE) != "Levy")
					{
						MessageBox.Show("Changing the amount to zero will force the application to ALWAYS use the amount from the Deduction Setup Screen. If a value of zero is required, the user should change the frequency code to 'N'", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				if (vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDEDNUMBER) == string.Empty)
				{
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpChecked, vsDeductions.Row, CNSTGRIDCOLDEFAULT, false);
					return;
				}
				// these columns are not effected in the default record
				if (vsDeductions.Col != CNSTGRIDCOLAMOUNTTYPE && vsDeductions.Col != CNSTGRIDCOLLIMITTYPE && vsDeductions.Col != CNSTGRIDCOLPRIORITY)
				{
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDEFAULT, FCConvert.ToString(IsDefaultDedutionGrid(vsDeductions.Row)));
					// increment the counter as to how many records are dirty
					intDataChanged += 1;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private object IsDefaultDedutionGrid(int intCount)
		{
			object IsDefaultDedutionGrid = null;
			clsDRWrapper rsDeductions = new clsDRWrapper();
			//FC:FINAL:DDU:#i2031 - get just number from column and set it to number only
			string temp = vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, CNSTGRIDCOLDEDNUMBER);
			if (!string.IsNullOrEmpty(temp) && temp.Contains("-"))
			{
				int x = temp.IndexOf("-");
				if (x > -1)
				{
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, CNSTGRIDCOLDEDNUMBER, Conversion.Val(Strings.Trim(temp.Substring(0, x))));
				}
			}
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup where ID = " + vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCount, CNSTGRIDCOLDEDNUMBER), "TWPY0000.vb1");
			if (!rsDeductions.EndOfFile())
			{
				if (vsDeductions.TextMatrix(intCount, CNSTGRIDCOLDESCRIPTION) != FCConvert.ToString(rsDeductions.Get_Fields("Description")))
				{
					return IsDefaultDedutionGrid;
				}
				// If vsDeductions.TextMatrix(intCount, cnstgridcoltax) <> rsDeductions.Fields("TaxStatusCode") Then
				// Exit Function
				// End If
				if (vsDeductions.TextMatrix(intCount, CNSTGRIDCOLAMOUNT) != FCConvert.ToString(rsDeductions.Get_Fields("Amount")))
				{
					return IsDefaultDedutionGrid;
				}
				if (vsDeductions.TextMatrix(intCount, CNSTGRIDCOLAMOUNTTYPE) != fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " "))
				{
					return IsDefaultDedutionGrid;
				}
				if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCount, CNSTGRIDCOLFREQ)) != Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " ")))
				{
					return IsDefaultDedutionGrid;
				}
				if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCount, CNSTGRIDCOLLIMIT)) != Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " ")))
				{
					return IsDefaultDedutionGrid;
				}
				if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCount, CNSTGRIDCOLPRIORITY)) != Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " ")))
				{
					return IsDefaultDedutionGrid;
				}
				if (FCConvert.ToDouble(vsDeductions.TextMatrix(intCount, CNSTGRIDCOLDEDID)) != Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionNumber") + " ")))
				{
					return IsDefaultDedutionGrid;
				}
				IsDefaultDedutionGrid = true;
			}
			return IsDefaultDedutionGrid;
		}

		private void vsDeductions_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsDeductions.Col == CNSTGRIDCOLPRIORITY)
					{
						if (vsDeductions.Row < vsDeductions.Rows - 1)
						{
							vsDeductions.Row += 1;
							vsDeductions.Col = CNSTGRIDCOLDEDNUMBER;
						}
						else
						{
							// if there is no other row then create a new one
							cmdNew_Click();
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDeductions_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_ValidateEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				dblAmtPercentTotal = 0;
				boolUpdateDeduction = false;
				// vbPorter upgrade warning: dblPercentNetTotal As int	OnWrite(int, double)
				double dblPercentNetTotal;
				dblPercentNetTotal = 0;
				if (vsDeductions.Col == CNSTGRIDCOLDEDNUMBER)
				{
					// If Right(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, cnstgridcoldirty), 1) = "." Then
					boolUpdateDeduction = true;
					// End If
				}
				if (vsDeductions.Col == CNSTGRIDCOLAMOUNT)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent" || vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "2")
						{
							if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) != "Net")
							{
								if (intCounter == vsDeductions.Row)
								{
									dblAmtPercentTotal += Conversion.Val(vsDeductions.EditText);
								}
								else
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
							}
							else
							{
								if (intCounter == vsDeductions.Row)
								{
									dblPercentNetTotal += Conversion.Val(vsDeductions.EditText);
								}
								else
								{
									dblPercentNetTotal += Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
							}
						}
					}
				}
				if (vsDeductions.Col == CNSTGRIDCOLAMOUNTTYPE)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (intCounter == vsDeductions.Row)
						{
							if (vsDeductions.EditText == "Percent" || vsDeductions.EditText == "2")
							{
								if (vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT) != "Net")
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
								else
								{
									dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
									if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
									{
										e.Cancel = true;
										boolCancelSave = true;
										MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
							}
						}
						else
						{
							if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent" || vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "2")
							{
								if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) != "Net")
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
								else
								{
									dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
									if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "2" && vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
									{
										e.Cancel = true;
										boolCancelSave = true;
										MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
							}
						}
					}
				}
				if (vsDeductions.Col == CNSTGRIDCOLPERCENT)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (intCounter == vsDeductions.Row)
						{
							if (vsDeductions.EditText == "Net")
							{
								dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "2" && vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
								{
									e.Cancel = true;
									boolCancelSave = true;
									MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else
							{
								dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
							}
						}
						else
						{
							if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent")
							{
								if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) == "Net")
								{
									dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
									if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "2" && vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
									{
										e.Cancel = true;
										boolCancelSave = true;
										MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
								else if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) == "Gross")
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
							}
						}
					}
					// intCounter
				}
				if (dblAmtPercentTotal > 100)
				{
					MessageBox.Show("Amount Totals For Percent Gross Cannot Be Greater Than 100", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					boolCancelSave = true;
				}
				if (dblPercentNetTotal > 100)
				{
					MessageBox.Show("Amount Total for Percent Net Cannot be Greater than 100", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					boolCancelSave = true;
				}
				if (vsDeductions.Col == CNSTGRIDCOLLOAN)
				{
					if (vsDeductions.EditText.Length > 3)
					{
						MessageBox.Show("ICMA loan numbers cannot be greater than 3 digits", "Bad Load Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						boolCancelSave = true;
					}
				}
				if (boolCancelSave == true)
				{
					if (vsDeductions.Col != CNSTGRIDCOLAMOUNTTYPE)
					{
						if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE)) == "Percent" || fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE)) == "2")
						{
							if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
							}
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
						}
						else
						{
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
					}
					else
					{
						if (vsDeductions.EditText == "Percent")
						{
							if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
							}
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
						}
						else
						{
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsTotals_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsTotals_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsTotals.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsTotals.TextMatrix(vsTotals.Row, 0, vsTotals.TextMatrix(vsTotals.Row, 0) + "..");
					// Call SetVerifyAdjustNeeded(True)
					if (Conversion.Val(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(vsTotals.Row, 4)) || Conversion.Val(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(vsTotals.Row, 5)) || Conversion.Val(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(vsTotals.Row, 6)) || Conversion.Val(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(vsTotals.Row, 7)) || Conversion.Val(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(vsTotals.Row, 8)) || Conversion.Val(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) <= Conversion.Val(vsTotals.TextMatrix(vsTotals.Row, 9)))
					{
						if (FCConvert.ToDouble(vsDeductions.TextMatrix(vsTotals.Row, CNSTGRIDCOLLIMIT)) != 0)
						{
							// limit has been reached
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsTotals.Row, CNSTGRIDCOLLIMIT, Color.Yellow);
						}
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsTotals_BeforeRowColChange(object sender, System.EventArgs e)
		{
			switch (vsTotals.Col)
			{
				case CNSTGRIDTOTALCOLLTD:
					{
						vsTotals.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsTotals.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void vsTotals_KeyDownEvent(object sender, KeyEventArgs e)
		{
			modGridKeyMove.MoveGridProsition(ref vsTotals, 4, 9, e.KeyCode, 9999, true);
		}

		private void vsTotals_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsTotals_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					// If vsTotals.Col = 9 Then
					if (vsTotals.Row < vsTotals.Rows - 1)
					{
						vsTotals.Row += 1;
						vsTotals.Col = 4;
					}
					else
					{
						// if there is no other row then create a new one
						cmdNew_Click();
					}
					// Else
					// move to the next column
					// SendKeys "{RIGHT}"
					// End If
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsTotals_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsTotals_KeyPressEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int KeyAscii = Strings.Asc(e.KeyChar);
				// allow the entry of a decimal point
				if (KeyAscii == 46)
					return;
				// allow the entry of a backspace
				if (KeyAscii == 8)
					return;
				if (vsTotals.Col >= 4 || vsTotals.Col <= 9)
				{
					// if the user is in the AMOUNT or LIMIT columns then do not
					// allow the entry of a character
					if (KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9))
					{
						KeyAscii = 0;
					}
				}
				e.KeyChar = Strings.Chr(KeyAscii);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsDeductions.Rows - 1); intRows++)
			{
				for (intCols = 3; intCols <= (vsDeductions.Cols - 1); intCols++)
				{
					if (intCols != 4 && intCols != 5 && intCols != 8 && intCols != 13 && intCols != 14 && intCols != 15)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsDeductions";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsDeductions.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
			for (intRows = 1; intRows <= (vsTotals.Rows - 1); intRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "vsTotals";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = 9;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsTotals.TextMatrix(0, 9);
				intTotalNumberOfControls += 1;
			}
		}
	}
}
