//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptStateUnemploymentC1.
	/// </summary>
	public partial class rptStateUnemploymentC1 : BaseSectionReport
	{
		public rptStateUnemploymentC1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "State Unemployment C-1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptStateUnemploymentC1 InstancePtr
		{
			get
			{
				return (rptStateUnemploymentC1)Sys.GetInstance(typeof(rptStateUnemploymentC1));
			}
		}

		protected rptStateUnemploymentC1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptStateUnemploymentC1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double dblTotalTotalGross;
		double dblTotalFederalGross;
		double dblTotalStateWithheld;
		double dblTotalExcessOverLimit;
		int lnglimit;
		double dblRate;
		clsDRWrapper clsLoad = new clsDRWrapper();
		int intCurrentQuarter;
		int lngYearToUse;
		int intQuarterToUse;
		bool boolElectronicC1;
		string strSequence = "";
		string strQTDWhere = "";
		string strYTDWhere = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("GroupBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("StateUnemployment");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string[] strAry = null;
			int lngSeqStart = 0;
			int lngSeqEnd = 0;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			dblTotalTotalGross = 0;
			dblTotalFederalGross = 0;
			dblTotalStateWithheld = 0;
			dblTotalExcessOverLimit = 0;
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				strSequence = "-1";
				// all
			}
			else
			{
				if (strSequence == string.Empty)
				{
					strSequence = frmGetSequence.InstancePtr.Init();
				}
			}
			if (strSequence == string.Empty)
			{
				// cancelled
				this.Close();
				return;
			}
			strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
			if (Information.UBound(strAry, 1) < 1)
			{
				// all or single
				lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			}
			else
			{
				// range
				lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
			}
			modGlobalRoutines.GetC1Data(ref clsLoad, ref dblRate, ref lnglimit, ref boolElectronicC1, ref lngYearToUse, ref intQuarterToUse, ref intCurrentQuarter, lngSeqStart, lngSeqEnd);
			lblExcess.Text = lblExcess.Text + " " + FCConvert.ToString(lnglimit);
			txtTitle3.Text = "Quarter = " + FCConvert.ToString(intQuarterToUse) + "/" + FCConvert.ToString(lngYearToUse);
			if (clsLoad.EndOfFile())
			{
				if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					MessageBox.Show("No data found", "No employees", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
					return;
				}
			}
			switch (intQuarterToUse)
			{
				case 1:
					{
						strQTDWhere = " where paydate between '01/01/" + FCConvert.ToString(lngYearToUse) + "' and '3/31/" + FCConvert.ToString(lngYearToUse) + "' ";
						strYTDWhere = strQTDWhere;
						break;
					}
				case 2:
					{
						strQTDWhere = " where paydate between '04/01/" + FCConvert.ToString(lngYearToUse) + "' and '6/30/" + FCConvert.ToString(lngYearToUse) + "' ";
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(lngYearToUse) + "' and '6/30/" + FCConvert.ToString(lngYearToUse) + "' ";
						break;
					}
				case 3:
					{
						strQTDWhere = " where paydate between '07/01/" + FCConvert.ToString(lngYearToUse) + "' and '9/30/" + FCConvert.ToString(lngYearToUse) + "' ";
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(lngYearToUse) + "' and '9/30/" + FCConvert.ToString(lngYearToUse) + "' ";
						break;
					}
				case 4:
					{
						strQTDWhere = " where paydate between '10/1/" + FCConvert.ToString(lngYearToUse) + "' and '12/31/" + FCConvert.ToString(lngYearToUse) + "' ";
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(lngYearToUse) + "' and '12/31/" + FCConvert.ToString(lngYearToUse) + "' ";
						break;
					}
			}
			//end switch
			strQTDWhere += " and deductionrecord = 1 and uexempt = 1 and checkvoid = 0 ";
			strYTDWhere += " and deductionrecord = 1 and uexempt = 1 and checkvoid = 0 ";
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalRoutines.Statics.gstrReportID)) == "NONE")
				{
				}
				else
				{
                    fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                    rptBLS.InstancePtr.Init(ref intQuarterToUse, ref lngYearToUse, true, strSequence);
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: dblTGross As double	OnWrite(string, double, int)
				double dblTGross = 0;
				double dblFGross;
				double dblSWH = 0;
				// vbPorter upgrade warning: dblExcess As double	OnWrite(int, Decimal)
				double dblExcess = 0;
				double dblYTDDed = 0;
				double dblQTDDed = 0;
				double dblYTDGross = 0;
                using (clsDRWrapper rsUDed = new clsDRWrapper())
                {
                    if (!clsLoad.EndOfFile())
                    {
                        dblExcess = 0;
                        txtSocialSecurityNum.Text = Strings.Format(clsLoad.Get_Fields_String("SSN"), "000-00-0000");
                        txtName.Text =
                            fecherFoundation.Strings.Trim(clsLoad.Get_Fields("lastname") + ", " +
                                                          clsLoad.Get_Fields("firstname"));
                        // If .Fields("EmployeeNumber") = "314" Then MsgBox "A"
                        dblTGross = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("qtdgrosspay")),
                            "0.00"));
                        // If .Fields("unemployment") = "N" Then
                        // If .Fields("seasonal") = "N" Then
                        // dblTGross = 0
                        // End If
                        rsUDed.OpenRecordset(
                            "select sum(dedamount) as totded from tblcheckdetail " + strYTDWhere +
                            " and employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
                        if (!rsUDed.EndOfFile())
                        {
                            dblYTDDed = Conversion.Val(rsUDed.Get_Fields("totded"));
                        }
                        else
                        {
                            dblYTDDed = 0;
                        }

                        if (dblYTDDed > 0)
                        {
                            rsUDed.OpenRecordset(
                                "select sum(dedamount) as totded from tblcheckdetail " + strQTDWhere +
                                " and employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
                            if (!rsUDed.EndOfFile())
                            {
                                dblQTDDed = Conversion.Val(rsUDed.Get_Fields("totded"));
                            }
                            else
                            {
                                dblQTDDed = 0;
                            }
                        }
                        else
                        {
                            dblQTDDed = 0;
                        }

                        dblTGross -= dblQTDDed;
                        if (dblTGross < 0)
                            dblTGross = 0;
                        dblYTDGross = Conversion.Val(clsLoad.Get_Fields("ytdgrosspay")) - dblYTDDed;
                        if (dblYTDGross < 0)
                        {
                            dblYTDGross = 0;
                        }

                        // IF YTD IS FGREATER THEN THE
                        dblExcess = FCConvert.ToDouble(modDavesSweetCode.CalculateExcess(
                            FCConvert.ToDecimal(dblYTDGross), FCConvert.ToDecimal(dblTGross),
                            FCConvert.ToDecimal(lnglimit)));
                        if (FCConvert.ToString(clsLoad.Get_Fields("unemployment")) == "N")
                        {
                            // If .Fields("seasonal") = "N" Then
                            dblExcess = 0;
                        }

                        // MATTHEW 7/9/2004
                        // ADDED THIS IN FOR DEB IN TOPSHAM. SHE WANTED TO SEE WHAT THE TOTAL GROSS WAGE PAID WAS.
                        txtGrossWagesPaid.Text = Strings.Format(dblTGross, "#,###,##0.00");
                        // MATTHEW 6/17/2004 WOOLWICH WE ONLY WANT TO SEE THE AMOUNT THAT IS UNDER THE LIMIT
                        // txtTotalGross.Text = Format(dblTGross, "#,###,##0.00")
                        txtTotalGross.Text = Strings.Format(dblTGross - dblExcess, "#,###,##0.00");
                        // COREY 6/22/2004
                        // dblTotalTotalGross = dblTotalTotalGross + (dblTGross - dblExcess)
                        dblTotalTotalGross += (dblTGross);
                        if (dblExcess > 0)
                        {
                            dblTotalExcessOverLimit += dblExcess;
                        }

                        // Changed for Caribou 03/31/04
                        // Changed to look at eligible gross not total gross instead of total gross
                        // dblFGross = Val(.Fields("qtdfedtaxgross"))
                        // dblFGross = Val(.Fields("qtdgrosspay"))
                        // txtFederalGross.Text = Format(dblFGross, "#,###,##0.00")
                        // dblTotalFederalGross = dblTotalFederalGross + dblFGross
                        dblSWH = Conversion.Val(clsLoad.Get_Fields("qtdstatetax"));
                        txtStateTaxWithheld.Text = Strings.Format(dblSWH, "#,###,##0.00");
                        dblTotalStateWithheld += dblSWH;
                        clsLoad.MoveNext();
                    }
                }

                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in Detail Format");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			double dblTaxable;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.None;
			// COREY 6/22/2004
			// txtTotalTotalGross.Text = Format(dblTotalTotalGross, "#,###,##0.00")
			txtTotalTotalGross.Text = Strings.Format(dblTotalTotalGross - dblTotalExcessOverLimit, "#,###,##0.00");
			// MATTHEW 7/9/2004
			// ADDED THIS IN FOR DEB IN TOPSHAM. SHE WANTED TO SEE WHAT THE TOTAL GROSS WAGE PAID WAS.
			txtTotaGrossWagesPd.Text = Strings.Format(dblTotalTotalGross, "#,###,##0.00");
			txtTotalWithheld.Text = Strings.Format(dblTotalStateWithheld, "#,###,##0.00");
			txtTotalQTDPayroll.Text = Strings.Format(dblTotalTotalGross, "#,###,##0.00");
			txtExcessOver7000.Text = Strings.Format(dblTotalExcessOverLimit, "#,###,##0.00");
			dblTaxable = dblTotalTotalGross - dblTotalExcessOverLimit;
			txtTaxable.Text = Strings.Format(dblTaxable, "#,###,##0.00");
			txtRate.Text = Strings.Format(dblRate, "0.0000");
			txtDue.Text = "$" + Strings.Format(dblTaxable * (dblRate / 100), "#,###,##0.00");
		}

		public void Init(int intQuarter, int lngYear, string strSeq = "")
		{
			strSequence = strSeq;
			boolElectronicC1 = true;
			intQuarterToUse = intQuarter;
			lngYearToUse = lngYear;
			// Me.Show , MDIParent
			modCoreysSweeterCode.CheckDefaultPrint(this, boolAllowEmail: false);
		}

		
	}
}
