//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsDistributionLine
	{
		//=========================================================
		private int lngID;
		private int lngRecordNumber;
		private string strEmployeeNumber = string.Empty;
		private int lngAccountCode;
		private string strAccountNumber = string.Empty;
		private string strWorkComp = string.Empty;
		private int lngPayCategory;
		private string strMSRS = string.Empty;
		private string strWC = string.Empty;
		private int lngTaxCode;
		private int lngCD;
		private double dblBaseRate;
		private double dblFactor;
		private int lngNumberWeeks;
		private double dblDefaultHours;
		private double dblHoursWeek;
		private double dblGross;
		private string strDistU = string.Empty;
		private string strStatusCode = string.Empty;
		private string strGrantFunded = string.Empty;
		private int lngMSRSID;
		private int lngProject;
		private int lngWeeksTaxWithheld;
		private int lngContractID;
		private string strAccountNumberMatch = string.Empty;

		public string AccountNumberMatch
		{
			set
			{
				strAccountNumberMatch = value;
			}
			get
			{
				string AccountNumberMatch = "";
				AccountNumberMatch = strAccountNumberMatch;
				return AccountNumberMatch;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public int RecordNumber
		{
			set
			{
				lngRecordNumber = value;
			}
			get
			{
				int RecordNumber = 0;
				RecordNumber = lngRecordNumber;
				return RecordNumber;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public int AccountCode
		{
			set
			{
				lngAccountCode = value;
			}
			get
			{
				int AccountCode = 0;
				AccountCode = lngAccountCode;
				return AccountCode;
			}
		}

		public string AccountNumber
		{
			set
			{
				strAccountNumber = value;
			}
			get
			{
				string AccountNumber = "";
				AccountNumber = strAccountNumber;
				return AccountNumber;
			}
		}

		public string WorkComp
		{
			set
			{
				strWorkComp = value;
			}
			get
			{
				string WorkComp = "";
				WorkComp = strWorkComp;
				return WorkComp;
			}
		}

		public int PayCategory
		{
			set
			{
				lngPayCategory = value;
			}
			get
			{
				int PayCategory = 0;
				PayCategory = lngPayCategory;
				return PayCategory;
			}
		}

		public string MSRS
		{
			set
			{
				strMSRS = value;
			}
			get
			{
				string MSRS = "";
				MSRS = strMSRS;
				return MSRS;
			}
		}

		public string WC
		{
			set
			{
				strWC = value;
			}
			get
			{
				string WC = "";
				WC = strWC;
				return WC;
			}
		}

		public int TaxCode
		{
			set
			{
				lngTaxCode = value;
			}
			get
			{
				int TaxCode = 0;
				TaxCode = lngTaxCode;
				return TaxCode;
			}
		}

		public int CD
		{
			set
			{
				lngCD = value;
			}
			get
			{
				int CD = 0;
				CD = lngCD;
				return CD;
			}
		}

		public double BaseRate
		{
			set
			{
				dblBaseRate = value;
			}
			get
			{
				double BaseRate = 0;
				BaseRate = dblBaseRate;
				return BaseRate;
			}
		}

		public double Factor
		{
			set
			{
				dblFactor = value;
			}
			get
			{
				double Factor = 0;
				Factor = dblFactor;
				return Factor;
			}
		}

		public int NumberWeeks
		{
			set
			{
				lngNumberWeeks = value;
			}
			get
			{
				int NumberWeeks = 0;
				NumberWeeks = lngNumberWeeks;
				return NumberWeeks;
			}
		}

		public double DefaultHours
		{
			set
			{
				dblDefaultHours = value;
			}
			get
			{
				double DefaultHours = 0;
				DefaultHours = dblDefaultHours;
				return DefaultHours;
			}
		}

		public double HoursWeek
		{
			set
			{
				dblHoursWeek = value;
			}
			get
			{
				double HoursWeek = 0;
				HoursWeek = dblHoursWeek;
				return HoursWeek;
			}
		}

		public double Gross
		{
			set
			{
				dblGross = value;
			}
			get
			{
				double Gross = 0;
				Gross = dblGross;
				return Gross;
			}
		}

		public string DistU
		{
			set
			{
				strDistU = value;
			}
			get
			{
				string DistU = "";
				DistU = strDistU;
				return DistU;
			}
		}

		public string StatusCode
		{
			set
			{
				strStatusCode = value;
			}
			get
			{
				string StatusCode = "";
				StatusCode = strStatusCode;
				return StatusCode;
			}
		}

		public string GrantFunded
		{
			set
			{
				strGrantFunded = value;
			}
			get
			{
				string GrantFunded = "";
				GrantFunded = strGrantFunded;
				return GrantFunded;
			}
		}

		public int MSRSID
		{
			set
			{
				lngMSRSID = value;
			}
			get
			{
				int MSRSID = 0;
				MSRSID = lngMSRSID;
				return MSRSID;
			}
		}

		public int Project
		{
			set
			{
				lngProject = value;
			}
			get
			{
				int Project = 0;
				Project = lngProject;
				return Project;
			}
		}

		public int WeeksTaxWithheld
		{
			set
			{
				lngWeeksTaxWithheld = value;
			}
			get
			{
				int WeeksTaxWithheld = 0;
				WeeksTaxWithheld = lngWeeksTaxWithheld;
				return WeeksTaxWithheld;
			}
		}

		public int ContractID
		{
			set
			{
				lngContractID = value;
			}
			get
			{
				int ContractID = 0;
				ContractID = lngContractID;
				return ContractID;
			}
		}

		private void LoadDefault()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "Select * from tblTaxStatusCodes where TaxStatusCode ='T'";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					lngTaxCode = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				}
				strSQL = "Select * from tblPayCodes where PayCode = '0'";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					CD = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				}
				strWC = "Y";
				ContractID = 0;
				Project = 0;
				MSRSID = modCoreysSweeterCode.CNSTDISTMNOMSRS;
				DistU = "Y";
				WorkComp = "Y";
				PayCategory = 1;
				Factor = 1;
				GrantFunded = "0";
				AccountCode = 2;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadDefault", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
