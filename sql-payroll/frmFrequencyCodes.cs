//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmFrequencyCodes : BaseForm
	{
		public frmFrequencyCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFrequencyCodes InstancePtr
		{
			get
			{
				return (frmFrequencyCodes)Sys.GetInstance(typeof(frmFrequencyCodes));
			}
		}

		protected frmFrequencyCodes _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 26,2001
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbFrequencyCodes    As DAO.Database
		private clsDRWrapper rsFrequencyCodes = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private bool boolState;

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsFrequencyCodes.Row < 0)
					return;
				if (vsFrequencyCodes.Col < 0)
					return;
				if (!modGlobalRoutines.ValidToDelete(modGlobalVariables.gEnumCodeValidation.Frequency, FCConvert.ToInt32(Conversion.Val(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsFrequencyCodes.Row, 3)))))
					return;
				if (MessageBox.Show("This action will delete Frequency Code '" + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsFrequencyCodes.Row, 1) + "'. Continue?", "Payroll Frequency Codes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// delete the record from the database
					rsFrequencyCodes.Execute("Delete from tblFrequencyCodes where ID = " + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsFrequencyCodes.Row, 3), "Payroll");
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsFrequencyCodes);
					// delete the record from the grid
					vsFrequencyCodes.RemoveItem(vsFrequencyCodes.Row);
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Frequency Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll Frequency Codes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// append a new row to the end of the grid
				vsFrequencyCodes.AddItem(FCConvert.ToString(vsFrequencyCodes.Rows));
				// increment the counter as to how many records are dirty
				intDataChanged += 1;
				// assign a row number to this new record
				vsFrequencyCodes.TextMatrix(vsFrequencyCodes.Rows - 1, 0, vsFrequencyCodes.TextMatrix(vsFrequencyCodes.Rows - 1, 0) + ".");
				vsFrequencyCodes.Select(vsFrequencyCodes.Rows - 1, 1);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// display the frequency code report
				// rptFrequencyCodes.Show
				frmReportViewer.InstancePtr.Init(rptFrequencyCodes.InstancePtr, boolAllowEmail: true, strAttachmentName: "FrequencyCodes");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				SaveChanges();
				// load the grid of current data from the database
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsFrequencyCodes.Select(0, 0);
				for (intCounter = 1; intCounter <= (vsFrequencyCodes.Rows - 1); intCounter++)
				{
					if (Strings.Right(FCConvert.ToString(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
					{
						// record has been edited so we need to update it
						if (NotValidData())
							goto ExitRoutine;
						rsFrequencyCodes.Execute("Update tblFrequencyCodes set FrequencyCode ='" + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "',  Description = '" + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2) + "',  PeriodsPerYear = " + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 4) + " Where ID = " + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3), "Payroll");
						// clear the edit symbol from the row number
						vsFrequencyCodes.TextMatrix(intCounter, 0, Strings.Mid(vsFrequencyCodes.TextMatrix(intCounter, 0), 1, vsFrequencyCodes.TextMatrix(intCounter, 0).Length - 2));
						goto NextRecord;
					}
					if (Strings.Right(FCConvert.ToString(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
					{
						// record has been added so we need to save it
						if (NotValidData())
							goto ExitRoutine;
						vsFrequencyCodes.Select(1, 1);
						rsFrequencyCodes.Execute("Insert into tblFrequencyCodes (FrequencyCode,Description,LastUserID,PeriodsPerYear) VALUES ('" + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "','" + vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "','" + modGlobalVariables.Statics.gstrUser + "'," + FCConvert.ToString(Conversion.Val(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4))) + ")", "Payroll");
						// clear the edit symbol from the row number
						vsFrequencyCodes.TextMatrix(intCounter, 0, Strings.Mid(vsFrequencyCodes.TextMatrix(intCounter, 0), 1, vsFrequencyCodes.TextMatrix(intCounter, 0).Length - 1));
						// get the new ID from the table for the record that was just added and place it into column 3
						rsFrequencyCodes.OpenRecordset("Select Max(ID) as NewID from tblFrequencyCodes", "TWPY0000.vb1");
						vsFrequencyCodes.TextMatrix(intCounter, 3, FCConvert.ToString(rsFrequencyCodes.Get_Fields("NewID")));
					}
					NextRecord:
					;
				}
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Frequency Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				clsHistoryClass.Compare();
				return;
				ExitRoutine:
				;
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Frequency Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmFrequencyCodes_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmFrequencyCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFrequencyCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFrequencyCodes properties;
			//frmFrequencyCodes.ScaleWidth	= 5880;
			//frmFrequencyCodes.ScaleHeight	= 4125;
			//frmFrequencyCodes.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				// open the forms global database connection
				// Set dbFrequencyCodes = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsFrequencyCodes.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				// Load the grid with data
				LoadGrid();
				// disable the grid
				boolState = false;
				vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vsFrequencyCodes.Rows - 1, vsFrequencyCodes.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsFrequencyCodes.Editable = FCGrid.EditableSettings.flexEDNone;
				clsHistoryClass.SetGridIDColumn("PY", "vsFrequencyCodes", 3);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				rsFrequencyCodes.OpenRecordset("Select * from tblFrequencyCodes Order by ID", "TWPY0000.vb1");
				if (!rsFrequencyCodes.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsFrequencyCodes.MoveLast();
					rsFrequencyCodes.MoveFirst();
					// set the number of rows in the grid
					vsFrequencyCodes.Rows = rsFrequencyCodes.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsFrequencyCodes.RecordCount()); intCounter++)
					{
						vsFrequencyCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						vsFrequencyCodes.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsFrequencyCodes.Get_Fields("FrequencyCode") + " "));
						vsFrequencyCodes.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsFrequencyCodes.Get_Fields("Description") + " "));
						vsFrequencyCodes.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsFrequencyCodes.Get_Fields("ID") + " "));
						vsFrequencyCodes.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(rsFrequencyCodes.Get_Fields_Int32("PeriodsPerYear") + " "));
						rsFrequencyCodes.MoveNext();
					}
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
                //FC:FINAL:BSE:#3840 remove multiline property in grid - text should be centered vertically 
				//vsFrequencyCodes.WordWrap = true;
				vsFrequencyCodes.Cols = 5;
				// vsFrequencyCodes.ExtendLastCol = True
				vsFrequencyCodes.ColHidden(3, true);
				// set column 0 properties
				vsFrequencyCodes.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsFrequencyCodes.ColWidth(0, 0);
				// set column 1 properties
				vsFrequencyCodes.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsFrequencyCodes.ColWidth(1, 600);
				vsFrequencyCodes.TextMatrix(0, 1, "Code");
				// set column 2 properties
				vsFrequencyCodes.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsFrequencyCodes.ColWidth(2, 3500);
				vsFrequencyCodes.TextMatrix(0, 2, "Description");
				// set column 3 properties
				// ID field that is invisible
				// set column 4 properties (Periods Per Year)
				vsFrequencyCodes.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsFrequencyCodes.ColWidth(4, 500);
				vsFrequencyCodes.TextMatrix(0, 4, "# Periods Per Year");
				// vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, 0, 4) = vbBlue
                //FC:FINAL:BSE:#4505 - change backcolor to default color
				//vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 0, 4, Color.Blue);
				vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 0, 4, true);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmFrequencyCodes_Resize(object sender, System.EventArgs e)
		{
			vsFrequencyCodes.ColWidth(0, vsFrequencyCodes.WidthOriginal * 0);
			vsFrequencyCodes.ColWidth(1, FCConvert.ToInt32(vsFrequencyCodes.WidthOriginal * 0.1));
			vsFrequencyCodes.ColWidth(2, FCConvert.ToInt32(vsFrequencyCodes.WidthOriginal * 0.55));
			vsFrequencyCodes.ColWidth(4, FCConvert.ToInt32(vsFrequencyCodes.WidthOriginal * 0.3));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (modGlobalVariables.Statics.gboolShowDeductionSetup)
				{
					frmDeductionSetup.InstancePtr.Show();
				}
				if (frmEmployeeMaster.InstancePtr.boolFromMaster == true)
				{
					frmEmployeeMaster.InstancePtr.Show();
				}
				else
				{
					//MDIParent.InstancePtr.Show();
					// set focus back to the menu options of the MDIParent
					//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
				}
				frmEmployeeMaster.InstancePtr.boolFromMaster = false;
				modGlobalVariables.Statics.gboolShowDeductionSetup = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuEnable_Click(object sender, System.EventArgs e)
		{
			boolState = Interaction.InputBox("Enter TRIO Password to edit Frequency Codes.", "Frequency Codes", null) == modGlobalConstants.DATABASEPASSWORD;
			if (boolState)
			{
				vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vsFrequencyCodes.Rows - 1, vsFrequencyCodes.Cols - 1, Color.White);
			}
			else
			{
				MessageBox.Show("Invalid password. Grid will NOT be enabled.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vsFrequencyCodes.Rows - 1, vsFrequencyCodes.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			if (boolState)
			{
				vsFrequencyCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsFrequencyCodes.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			//FC:FINAL:DDU:#i2108 - call here menu file click event in order that we don't have anymore a menu
			mnuFile_Click();
			//vsFrequencyCodes.Editable = boolState;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuFile_Click()
		{
			mnuFile_Click(null, null);
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			this.cmdDelete.Enabled = boolState;
			this.cmdNew.Enabled = boolState;
			this.cmdPrint.Enabled = false;
			this.cmdRefresh.Enabled = boolState;
			this.cmdSave.Enabled = boolState;
			this.mnuSaveExit.Enabled = boolState;
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void vsFrequencyCodes_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsFrequencyCodes_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsFrequencyCodes.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsFrequencyCodes.TextMatrix(vsFrequencyCodes.Row, 0, vsFrequencyCodes.TextMatrix(vsFrequencyCodes.Row, 0) + "..");
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsFrequencyCodes_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsFrequencyCodes_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsFrequencyCodes.Col == 2)
					{
						if (vsFrequencyCodes.Row < vsFrequencyCodes.Rows - 1)
						{
							vsFrequencyCodes.Row += 1;
							vsFrequencyCodes.Col = 1;
						}
						else
						{
							// if there is no other row then create a new one
							vsFrequencyCodes.AddItem("");
							vsFrequencyCodes.Row += 1;
							vsFrequencyCodes.Col = 1;
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsFrequencyCodes_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsFrequencyCodes_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// select all of the data in the new cell
				if (boolState)
				{
					vsFrequencyCodes.EditCell();
					vsFrequencyCodes.EditSelStart = 0;
					vsFrequencyCodes.EditSelLength = FCConvert.ToString(vsFrequencyCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsFrequencyCodes.Row, vsFrequencyCodes.Col)).Length;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
	}
}
