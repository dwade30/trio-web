//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmShiftTypes.
	/// </summary>
	partial class frmShiftTypes
	{
		public FCGrid gridAccount;
		public fecherFoundation.FCComboBox cmbAccount;
		public fecherFoundation.FCTextBox txtLunch;
		public FCGrid GridShiftTypes;
		public FCGrid GridBreakdowns;
		public fecherFoundation.FCLabel lblDefaultAccount;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddShiftType;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteShift;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem menuAddLine;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteLine;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            this.gridAccount = new fecherFoundation.FCGrid();
            this.cmbAccount = new fecherFoundation.FCComboBox();
            this.txtLunch = new fecherFoundation.FCTextBox();
            this.GridShiftTypes = new fecherFoundation.FCGrid();
            this.GridBreakdowns = new fecherFoundation.FCGrid();
            this.lblDefaultAccount = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddShiftType = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteShift = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.menuAddLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdAddShift = new fecherFoundation.FCButton();
            this.cmdDeleteShift = new fecherFoundation.FCButton();
            this.cmdAddLine = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridShiftTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBreakdowns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(658, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.gridAccount);
            this.ClientArea.Controls.Add(this.cmbAccount);
            this.ClientArea.Controls.Add(this.txtLunch);
            this.ClientArea.Controls.Add(this.GridShiftTypes);
            this.ClientArea.Controls.Add(this.GridBreakdowns);
            this.ClientArea.Controls.Add(this.lblDefaultAccount);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(658, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdAddLine);
            this.TopPanel.Controls.Add(this.cmdDeleteShift);
            this.TopPanel.Controls.Add(this.cmdAddShift);
            this.TopPanel.Size = new System.Drawing.Size(658, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddShift, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteShift, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddLine, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(135, 30);
            this.HeaderText.Text = "Shift Types";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // gridAccount
            // 
            this.gridAccount.AllowSelection = false;
            this.gridAccount.AllowUserToResizeColumns = false;
            this.gridAccount.AllowUserToResizeRows = false;
            this.gridAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridAccount.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridAccount.BackColorBkg = System.Drawing.Color.Empty;
            this.gridAccount.BackColorFixed = System.Drawing.Color.Empty;
            this.gridAccount.BackColorSel = System.Drawing.Color.Empty;
            this.gridAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridAccount.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridAccount.ColumnHeadersHeight = 30;
            this.gridAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridAccount.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridAccount.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridAccount.DragIcon = null;
            this.gridAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridAccount.ExtendLastCol = true;
            this.gridAccount.FixedCols = 0;
            this.gridAccount.FixedRows = 0;
            this.gridAccount.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridAccount.FrozenCols = 0;
            this.gridAccount.GridColor = System.Drawing.Color.Empty;
            this.gridAccount.GridColorFixed = System.Drawing.Color.Empty;
            this.gridAccount.Location = new System.Drawing.Point(361, 354);
            this.gridAccount.Name = "gridAccount";
            this.gridAccount.OutlineCol = 0;
            this.gridAccount.RowHeadersVisible = false;
            this.gridAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridAccount.RowHeightMin = 0;
            this.gridAccount.Rows = 1;
            this.gridAccount.ScrollTipText = null;
            this.gridAccount.ShowColumnVisibilityMenu = false;
            this.gridAccount.Size = new System.Drawing.Size(159, 42);
            this.gridAccount.StandardTab = true;
            this.gridAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridAccount.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.gridAccount, null);
            this.gridAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.gridAccount_KeyPressEdit);
            // 
            // cmbAccount
            // 
            this.cmbAccount.AutoSize = false;
            this.cmbAccount.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAccount.FormattingEnabled = true;
            this.cmbAccount.Location = new System.Drawing.Point(235, 354);
            this.cmbAccount.Name = "cmbAccount";
            this.cmbAccount.Size = new System.Drawing.Size(106, 40);
            this.cmbAccount.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbAccount, null);
            this.cmbAccount.SelectedIndexChanged += new System.EventHandler(this.cmbAccount_SelectedIndexChanged);
            // 
            // txtLunch
            // 
            this.txtLunch.AutoSize = false;
            this.txtLunch.BackColor = System.Drawing.SystemColors.Window;
            this.txtLunch.LinkItem = null;
            this.txtLunch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLunch.LinkTopic = null;
            this.txtLunch.Location = new System.Drawing.Point(235, 293);
            this.txtLunch.Name = "txtLunch";
            this.txtLunch.Size = new System.Drawing.Size(106, 40);
            this.txtLunch.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtLunch, null);
            // 
            // GridShiftTypes
            // 
            this.GridShiftTypes.AllowSelection = false;
            this.GridShiftTypes.AllowUserToResizeColumns = false;
            this.GridShiftTypes.AllowUserToResizeRows = false;
            this.GridShiftTypes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridShiftTypes.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridShiftTypes.BackColorBkg = System.Drawing.Color.Empty;
            this.GridShiftTypes.BackColorFixed = System.Drawing.Color.Empty;
            this.GridShiftTypes.BackColorSel = System.Drawing.Color.Empty;
            this.GridShiftTypes.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridShiftTypes.Cols = 10;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridShiftTypes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridShiftTypes.ColumnHeadersHeight = 30;
            this.GridShiftTypes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridShiftTypes.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridShiftTypes.DragIcon = null;
            this.GridShiftTypes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridShiftTypes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.GridShiftTypes.ExtendLastCol = true;
            this.GridShiftTypes.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridShiftTypes.FrozenCols = 0;
            this.GridShiftTypes.GridColor = System.Drawing.Color.Empty;
            this.GridShiftTypes.GridColorFixed = System.Drawing.Color.Empty;
            this.GridShiftTypes.Location = new System.Drawing.Point(30, 30);
            this.GridShiftTypes.Name = "GridShiftTypes";
            this.GridShiftTypes.OutlineCol = 0;
            this.GridShiftTypes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridShiftTypes.RowHeightMin = 0;
            this.GridShiftTypes.Rows = 50;
            this.GridShiftTypes.ScrollTipText = null;
            this.GridShiftTypes.ShowColumnVisibilityMenu = false;
            this.GridShiftTypes.Size = new System.Drawing.Size(594, 243);
            this.GridShiftTypes.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridShiftTypes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridShiftTypes.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.GridShiftTypes, "Use Insert and Delete to add and delete shifts");
            this.GridShiftTypes.ComboCloseUp += new System.EventHandler(this.GridShiftTypes_ComboCloseUp);
            this.GridShiftTypes.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridShiftTypes_BeforeRowColChange);
            this.GridShiftTypes.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridShiftTypes_ValidateEdit);
            this.GridShiftTypes.KeyDown += new Wisej.Web.KeyEventHandler(this.GridShiftTypes_KeyDownEvent);
            // 
            // GridBreakdowns
            // 
            this.GridBreakdowns.AllowSelection = false;
            this.GridBreakdowns.AllowUserToResizeColumns = false;
            this.GridBreakdowns.AllowUserToResizeRows = false;
            this.GridBreakdowns.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridBreakdowns.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridBreakdowns.BackColorBkg = System.Drawing.Color.Empty;
            this.GridBreakdowns.BackColorFixed = System.Drawing.Color.Empty;
            this.GridBreakdowns.BackColorSel = System.Drawing.Color.Empty;
            this.GridBreakdowns.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridBreakdowns.Cols = 10;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridBreakdowns.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.GridBreakdowns.ColumnHeadersHeight = 30;
            this.GridBreakdowns.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridBreakdowns.DefaultCellStyle = dataGridViewCellStyle6;
            this.GridBreakdowns.DragIcon = null;
            this.GridBreakdowns.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridBreakdowns.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.GridBreakdowns.ExtendLastCol = true;
            this.GridBreakdowns.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridBreakdowns.FrozenCols = 0;
            this.GridBreakdowns.GridColor = System.Drawing.Color.Empty;
            this.GridBreakdowns.GridColorFixed = System.Drawing.Color.Empty;
            this.GridBreakdowns.Location = new System.Drawing.Point(30, 415);
            this.GridBreakdowns.Name = "GridBreakdowns";
            this.GridBreakdowns.OutlineCol = 0;
            this.GridBreakdowns.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridBreakdowns.RowHeightMin = 0;
            this.GridBreakdowns.Rows = 50;
            this.GridBreakdowns.ScrollTipText = null;
            this.GridBreakdowns.ShowColumnVisibilityMenu = false;
            this.GridBreakdowns.Size = new System.Drawing.Size(594, 139);
            this.GridBreakdowns.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridBreakdowns.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridBreakdowns.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.GridBreakdowns, "Use Insert and Delete to add and delete lines");
            this.GridBreakdowns.ComboCloseUp += new System.EventHandler(this.GridBreakdowns_ComboCloseUp);
            this.GridBreakdowns.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridBreakdowns_ValidateEdit);
            this.GridBreakdowns.KeyDown += new Wisej.Web.KeyEventHandler(this.GridBreakdowns_KeyDownEvent);
            // 
            // lblDefaultAccount
            // 
            this.lblDefaultAccount.Location = new System.Drawing.Point(30, 368);
            this.lblDefaultAccount.Name = "lblDefaultAccount";
            this.lblDefaultAccount.Size = new System.Drawing.Size(150, 16);
            this.lblDefaultAccount.TabIndex = 6;
            this.lblDefaultAccount.Text = "DEFAULT ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblDefaultAccount, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 307);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(150, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "DEFAULT LUNCH BREAK";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddShiftType,
            this.mnuDeleteShift,
            this.mnuSepar3,
            this.menuAddLine,
            this.mnuDeleteLine,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddShiftType
            // 
            this.mnuAddShiftType.Index = 0;
            this.mnuAddShiftType.Name = "mnuAddShiftType";
            this.mnuAddShiftType.Text = "Add Shift Type";
            this.mnuAddShiftType.Click += new System.EventHandler(this.mnuAddShiftType_Click);
            // 
            // mnuDeleteShift
            // 
            this.mnuDeleteShift.Index = 1;
            this.mnuDeleteShift.Name = "mnuDeleteShift";
            this.mnuDeleteShift.Text = "Delete Shift Type";
            this.mnuDeleteShift.Click += new System.EventHandler(this.mnuDeleteShift_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 2;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // menuAddLine
            // 
            this.menuAddLine.Index = 3;
            this.menuAddLine.Name = "menuAddLine";
            this.menuAddLine.Text = "Add Line";
            this.menuAddLine.Click += new System.EventHandler(this.menuAddLine_Click);
            // 
            // mnuDeleteLine
            // 
            this.mnuDeleteLine.Index = 4;
            this.mnuDeleteLine.Name = "mnuDeleteLine";
            this.mnuDeleteLine.Text = "Delete Line";
            this.mnuDeleteLine.Click += new System.EventHandler(this.mnuDeleteLine_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 5;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 6;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 7;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 8;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 9;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddShift
            // 
            this.cmdAddShift.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddShift.AppearanceKey = "toolbarButton";
            this.cmdAddShift.Location = new System.Drawing.Point(232, 29);
            this.cmdAddShift.Name = "cmdAddShift";
            this.cmdAddShift.Size = new System.Drawing.Size(105, 24);
            this.cmdAddShift.TabIndex = 1;
            this.cmdAddShift.Text = "Add Shift Type";
            this.ToolTip1.SetToolTip(this.cmdAddShift, null);
            this.cmdAddShift.Click += new System.EventHandler(this.mnuAddShiftType_Click);
            // 
            // cmdDeleteShift
            // 
            this.cmdDeleteShift.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteShift.AppearanceKey = "toolbarButton";
            this.cmdDeleteShift.Location = new System.Drawing.Point(343, 29);
            this.cmdDeleteShift.Name = "cmdDeleteShift";
            this.cmdDeleteShift.Size = new System.Drawing.Size(120, 24);
            this.cmdDeleteShift.TabIndex = 2;
            this.cmdDeleteShift.Text = "Delete Shift Type";
            this.ToolTip1.SetToolTip(this.cmdDeleteShift, null);
            this.cmdDeleteShift.Click += new System.EventHandler(this.mnuDeleteShift_Click);
            // 
            // cmdAddLine
            // 
            this.cmdAddLine.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddLine.AppearanceKey = "toolbarButton";
            this.cmdAddLine.Location = new System.Drawing.Point(469, 29);
            this.cmdAddLine.Name = "cmdAddLine";
            this.cmdAddLine.Size = new System.Drawing.Size(70, 24);
            this.cmdAddLine.TabIndex = 3;
            this.cmdAddLine.Text = "Add Line";
            this.ToolTip1.SetToolTip(this.cmdAddLine, null);
            this.cmdAddLine.Click += new System.EventHandler(this.menuAddLine_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(545, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(85, 24);
            this.cmdDelete.TabIndex = 4;
            this.cmdDelete.Text = "Delete Line";
            this.ToolTip1.SetToolTip(this.cmdDelete, null);
            this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteLine_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(287, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmShiftTypes
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(658, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmShiftTypes";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Shift Types";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmShiftTypes_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmShiftTypes_KeyDown);
            this.Resize += new System.EventHandler(this.frmShiftTypes_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridShiftTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBreakdowns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDeleteShift;
		private FCButton cmdAddShift;
		private FCButton cmdDelete;
		private FCButton cmdAddLine;
		private FCButton cmdSave;
	}
}