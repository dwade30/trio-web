﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports.Document.Section;
using TWSharedLibrary;
using Wisej.Web;
using Page = GrapeCity.ActiveReports.Document.Section.Page;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmailPayrollCheck.
	/// </summary>
	public partial class rptEmailPayrollCheck : BaseSectionReport
	{
        public rptEmailPayrollCheck()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Email Checks";
        }

        protected rptEmailPayrollCheck _InstancePtr = null;

		protected override void Dispose(bool disposing)
		{
            if (disposing)
            {
                mService = null;
                employeeChecks = null;
                clsCheckList = null;
            }
			base.Dispose(disposing);
		}

		private cEmployeeChecksReport employeeChecks;
		private cEMailService mService = new cEMailService();
		const int CNSTMAXSTUBLINES = 23;
		private int intMaxPayLine;
		private int intMaxDedLine;
        string strTag;
        bool boolReprinting;
		int lngCurrentCheckNumber;
		DateTime dtPayDate;
        private int intPayrunToUse;
		private bool boolSendAsHTML;
		private clsESHPChecks clsCheckList = new clsESHPChecks();
		private string strDirForExport = string.Empty;
        private bool boolPrintPayPeriod;
		// vbPorter upgrade warning: strCurrentCheckNumber As string	OnWrite(string, int)
		//private string strCurrentCheckNumber = "";
		//private cEmployeeCheck currentEmployeeCheck;
		//private cGenericCollection colChecks = new cGenericCollection();
		//private cCheckService checkService = new cCheckService();
		//private string strCurrentEmailAddress = "";


        public delegate void SentEmailsEventHandler();

		public event SentEmailsEventHandler SentEmails;

        private string[] pageData;
        private List<string> pageDataList = new List<string>();
		public cEmployeeChecksReport checksReport
		{
			get
			{
				cEmployeeChecksReport checksReport = null;
				checksReport = employeeChecks;
				return checksReport;
			}
		}

		public cEmployeeCheck CurrentCheck
		{
			get
			{
				cEmployeeCheck CurrentCheck = null;
                if (employeeChecks.employeeChecks.IsCurrent())
				{
					CurrentCheck = (cEmployeeCheck)employeeChecks.employeeChecks.GetCurrentItem();
				}
				else
				{
					CurrentCheck = null;
				}
				return CurrentCheck;
			}
		}

        public cEmployeeCheck GetCheckByIndex(int index)
        {
            if (employeeChecks.employeeChecks.ItemCount() >= index)
            {
                return (cEmployeeCheck)employeeChecks.employeeChecks.GetItemAtIndex(index);
            }

            return null;
        }

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
            employeeChecks.employeeChecks.MoveLast();
			employeeChecks.employeeChecks.MoveNext();
			if (fecherFoundation.Strings.LCase(employeeChecks.checkFormat.CheckType) == "s")
			{
				SubReport2.Height = 0;
				SubReport2.Visible = false;
            }
			else
			{
				SubReport2.Visible = true;
				if (employeeChecks.checkFormat.UseDoubleStub)
				{
					if (employeeChecks.checkFormat.CheckPosition.ToLower() == "t")
					{
						SubReport2.Height = 9810 / 1440F;
					}
					else
					{
						SubReport1.Height = 9810 / 1440F;
						SubReport2.Top = SubReport1.Height + SubReport1.Top + 1;
					}
					Detail.Height = SubReport2.Top + SubReport2.Height + 1;
					this.PageSettings.PaperHeight = Detail.Height;
				}
			}
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			SubReport1.Report = null;
			SubReport2.Report = null;
			employeeChecks.employeeChecks.MoveNext();
			eArgs.EOF = !employeeChecks.employeeChecks.IsCurrent();
			if (!eArgs.EOF)
			{
				cEmployeeCheck eCheck;
				eCheck = (cEmployeeCheck)employeeChecks.employeeChecks.GetCurrentItem();
            }
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			SendEmails();
		}

		private void SendEmails()
		{
			cGenericCollection collEmails;
			if (!boolSendAsHTML)
			{
				collEmails = CreatePDFEmails();
			}
			else
			{
				collEmails = CreateHTMLEmails();
			}
			cEmailConfig eConfig;
			eConfig = mService.GetEffectiveEMailConfigByUserID(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			mService.Initialize(ref eConfig);
			mService.SendEmailCollection(ref collEmails);
			if (mService.HadError)
			{
                using (clsDRWrapper rsErr = new clsDRWrapper())
                {
                    rsErr.OpenRecordset("select * from ApplicationLog where id = -1", "CentralData");
                    rsErr.AddNew();
                    rsErr.Set_Fields("EntryTimeStamp", DateTime.Now);
                    rsErr.Set_Fields("Entry", "Error when mailing pay stubs: " + mService.LastErrorMessage);
                    rsErr.Set_Fields("Origin", "Payroll");
                    rsErr.Set_Fields("UserID", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                    rsErr.Set_Fields("User", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                    rsErr.Update();
                }

                MessageBox.Show("Error " + FCConvert.ToString(mService.LastErrorNumber) + "  " + mService.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				//MessageBox.Show("Emails Sent", "Sent", MessageBoxButtons.OK, MessageBoxIcon.Information); //message box causes focus to go back to Email Checks form and not report
				if (this.SentEmails != null)
					this.SentEmails();
			}
		}

		private cGenericCollection CreatePDFEmails()
		{
			cGenericCollection CreatePDFEmails = null;
			// export files
			try
			{
                cEmail tempEmail;
				cGenericCollection collEmails = new cGenericCollection();
				string strEmail = "";
				object str;
				int x;
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                cEmailDataAttachment dAttach;
                pageData = pageDataList.ToArray();
				for (x = 0; x <= this.Document.Pages.Count - 1; x++)
				{
                    var bytDat = new MemoryStream();
					byte[] btArray = null;

					var strArray = Strings.Split(pageData[x], "|", -1, CompareConstants.vbBinaryCompare);
					strEmail = strArray[0];
					var fileName = strArray[1] + ".pdf";
                    var subject = "Payroll Check Number " + strArray[2];

					pe.Export(this.Document, bytDat, (x + 1).ToString());
                    
                    btArray = bytDat.ToArray();
					dAttach = new cEmailDataAttachment();
					dAttach.AttachmentName = fileName;
					dAttach.SetAttachmentData(ref btArray);

                    bytDat.Close();
                    bytDat.Dispose();

                    tempEmail = new cEmail();
					tempEmail.SendTos.AddItem(strEmail);
					tempEmail.Subject = subject;
					tempEmail.Attachments.AddItem(dAttach);
					collEmails.AddItem(tempEmail);
				}
				
				CreatePDFEmails = collEmails;
				return CreatePDFEmails;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + ex.Message + "\r\n" + "In CreatePDFEmails", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
			}
			return CreatePDFEmails;
		}

		private cGenericCollection CreateHTMLEmails()
		{
			cGenericCollection CreateHTMLEmails = null;
			cEmail tempEmail;
			cGenericCollection collEmails = new cGenericCollection();
			int x;
			string strEmail = "";
			string strHTML = "";
			GrapeCity.ActiveReports.Export.Html.Section.HtmlExport he = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
			he = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
			he.MultiPage = false;
            string[] strArray = null;
			for (x = 0; x <= this.Document.Pages.Count - 1; x++)
			{
                MemoryStream bytDat = new MemoryStream();
				byte[] btArray;
				strArray = Strings.Split(pageData[x], "|", -1, CompareConstants.vbBinaryCompare);
				strEmail = strArray[0];
                he.Export(this.Document, bytDat, (x + 1).ToString());
                btArray = bytDat.ToArray();
                tempEmail = new cEmail();
                tempEmail.HtmlBody = System.Text.Encoding.Unicode.GetString(btArray);
                bytDat.Close();
                bytDat.Dispose();
				tempEmail.SendTos.AddItem(strEmail);
				tempEmail.Subject = "Payroll Check Number " + strArray[2];
				collEmails.AddItem(tempEmail);
			}
            CreateHTMLEmails = collEmails;
			return CreateHTMLEmails;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			intMaxPayLine = 1;
			intMaxDedLine = 1;
			clsCheckList.ClearList();
			clsCheckList.PayDate = employeeChecks.PayDate;
			clsCheckList.Payrun = employeeChecks.PayrunNumber;
			// get the max number of pay lines and the max deduction lines
			// there won't be more since the parent report has already taken into
			// account what cnstmaxstublines is.  This can be changed to put the burden
			// on the stub but would have to be done to both stub sub reports.
			cCustomCheckFormat customFormat;
			customFormat = employeeChecks.checkFormat;
			if (fecherFoundation.Strings.LCase(employeeChecks.checkFormat.CheckType) == "s")
			{
				SubReport2.Height = 0;
				SubReport2.Visible = false;
				// Detail.Height = SubReport1.Height + 1
			}
			else
			{
				SubReport2.Visible = true;
			}
			// customFormat
            
            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            PageSettings.PaperKind = PaperKind.Letter;
            PageSettings.Margins = new GrapeCity.ActiveReports.Document.Section.Margins(0.25f,0.25f,0.25f,0.25f);

        }
        public void Init(ref cEmployeeChecksReport checksReport, string strExportDir, bool boolAsHTML)
		{
			try
			{
                fecherFoundation.Information.Err().Clear();
				boolSendAsHTML = boolAsHTML;
				employeeChecks = checksReport;
                intPayrunToUse = checksReport.PayrunNumber;
				dtPayDate = checksReport.PayDate;
                boolPrintPayPeriod = employeeChecks.ShowPayPeriodOnStub;
				frmReportViewer.InstancePtr.Init(this, boolOverRideDuplex: true, boolAllowEmail: false);
				return;
			}
			catch (Exception ex)
			{
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", modal:false);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			cEmployeeCheck eCheck;
			strTag = "";
			if (employeeChecks.employeeChecks.IsCurrent())
            {
                eCheck = (cEmployeeCheck)employeeChecks.employeeChecks.GetCurrentItem();
                pageDataList.Add($"{eCheck.Employee.Email}|{GetFileName(eCheck.CheckNumber.ToString())}|{eCheck.CheckNumber.ToString()}");
				if (fecherFoundation.Strings.LCase(employeeChecks.checkFormat.CheckType) != "s")
				{
					if (fecherFoundation.Strings.LCase(employeeChecks.checkFormat.CheckPosition) == "t")
					{
						SubReport1.Report = new srptCustomCheckBody();
						if (employeeChecks.checkFormat.UseDoubleStub)
						{
							SubReport2.Report = new srptStandardLaserDoubleStub();
						}
						else
						{
							SubReport2.Report = new srptStandardStub();
						}
					}
					else
					{
						if (employeeChecks.checkFormat.UseDoubleStub)
						{
							SubReport1.Report = new srptStandardLaserDoubleStub();
						}
						else
						{
							SubReport1.Report = new srptStandardStub();
						}
						SubReport2.Report = new srptCustomCheckBody();
					}

					SubReport1.Report.UserData = employeeChecks.employeeChecks.GetCurrentIndex();
					SubReport2.Report.UserData = employeeChecks.employeeChecks.GetCurrentIndex();
				}
				else
				{
					if (employeeChecks.checkFormat.UseDoubleStub)
					{
						SubReport1.Report = new srptStandardLaserDoubleStub();
					}
					else
					{
						SubReport1.Report = new srptStandardStub();
					}
					SubReport2.Report = null;
				}
				SubReport1.Report.UserData = employeeChecks.employeeChecks.GetCurrentIndex();
			}
			else
			{
				SubReport1.Report = null;
				SubReport2.Report = null;
			}
		}

        public bool PrintPayPeriod
        {
            get
            {
                bool PrintPayPeriod = false;
                PrintPayPeriod = boolPrintPayPeriod;
                return PrintPayPeriod;
            }
        }

		private string GetFileName(string strCheckNumber)
		{
			string GetFileName = "";
			string strFileName;
			strFileName = "";
			strFileName = Strings.Format(dtPayDate, "MMddyyyy") + "_" + FCConvert.ToString(intPayrunToUse) + "_" + strCheckNumber;
			GetFileName = strFileName;
			return GetFileName;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{

        }

	}
}
