//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMSRS.
	/// </summary>
	public partial class srptMSRS : FCSectionReport
	{
		public srptMSRS()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptMSRS InstancePtr
		{
			get
			{
				return (srptMSRS)Sys.GetInstance(typeof(srptMSRS));
			}
		}

		protected srptMSRS _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptMSRS	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsLoad.OpenRecordset("select * from tblmiscupdate where EMPLOYEENUMBER = '" + this.UserData + "'", "twpy0000.vb1");
			if (clsLoad.EndOfFile())
			{
				//this.Visible = false;
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields("include")))
				{
					txtInclude.Text = "Yes";
				}
				else
				{
					txtInclude.Text = "No";
				}
				txtLifeInsCode.Text = clsLoad.Get_Fields("Lifeinscode");
				if (Strings.Left(clsLoad.Get_Fields("monthlyHD") + " ", 1) == "M")
				{
					txtMonthlyHD.Text = "Multiply";
				}
				else
				{
					txtMonthlyHD.Text = "Divide";
				}
				txtMonthlyFactor.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("monthlyfactor")), "0.00");
				if (Strings.Left(clsLoad.Get_Fields("rateofpay") + " ", 1) == "M")
				{
					txtRateOfPay.Text = "Multiply";
				}
				else
				{
					txtRateOfPay.Text = "Divide";
				}
				txtRateFactor.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("payfactor")), "0.00");
				txtStatusCode.Text = FCConvert.ToString(clsLoad.Get_Fields("statuscode"));
				txtPosition.Text = FCConvert.ToString(clsLoad.Get_Fields("positioncode"));
				txtPlanCode.Text = FCConvert.ToString(clsLoad.Get_Fields("plancode"));
				if (Strings.Left(clsLoad.Get_Fields_String("Excess") + "-", 1) == "-")
				{
					txtLifeInsSched.Text = "-";
				}
				else if (Strings.Left(clsLoad.Get_Fields_String("Excess") + "-", 1) == "E")
				{
					txtLifeInsSched.Text = "Excess";
				}
				else if (Strings.Left(clsLoad.Get_Fields_String("Excess") + "-", 1) == "P")
				{
					txtLifeInsSched.Text = "Purchase";
				}
				txtPayPeriod.Text = FCConvert.ToString(clsLoad.Get_Fields("payperiodscode"));
				txtLifeInsLevel.Text = Strings.Format(clsLoad.Get_Fields("lifeinslevel"), "#,###,###,##0");
				string vbPorterVar = FCConvert.ToString(clsLoad.Get_Fields("federalcomp"));
				if (vbPorterVar == "D")
				{
					txtFedCompDP.Text = "Dollars";
					txtFedCompAmount.Text = Strings.Format(clsLoad.Get_Fields_Double("FedCompAmount"), "#,###,###,##0");
				}
				else if (vbPorterVar == "P")
				{
					txtFedCompDP.Text = "Percent";
					txtFedCompAmount.Text = clsLoad.Get_Fields("fedcompamount") + " %";
				}
				txtWorkWeeks.Text = FCConvert.ToString(clsLoad.Get_Fields("workweekperyear"));
				string vbPorterVar1 = FCConvert.ToString(clsLoad.Get_Fields("payratecode"));
				if (vbPorterVar1 == "H")
				{
					txtPayRateHD.Text = "Hours";
				}
				else if (vbPorterVar1 == "D")
				{
					txtPayRateHD.Text = "Days";
				}
				txtNatureCode.Text = FCConvert.ToString(clsLoad.Get_Fields("naturecode"));
				string vbPorterVar2 = FCConvert.ToString(clsLoad.Get_Fields("unemployment"));
				if (vbPorterVar2 == "Y")
				{
					txtUnempInclude.Text = "Yes";
				}
				else if (vbPorterVar2 == "N")
				{
					txtUnempInclude.Text = "No";
				}
				else if (vbPorterVar2 == "S")
				{
					txtUnempInclude.Text = "Seasonal";
				}
			}
		}

		
	}
}
