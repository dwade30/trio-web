﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public partial class frmChecksToEmail : BaseForm
	{
		public frmChecksToEmail()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

		}

		const int cnstgridcolID = 0;
		const int CNSTGridColUse = 1;
		const int CNSTGridColEmpNo = 2;
		const int CNSTGridColName = 3;
		const int CNSTGridCOLEmail = 4;
		const int CNSTGridColCheckNo = 5;
		const int CNSTGRIDCOLAMOUNT = 6;
		private cChecksToEmailView theView;
		private bool boolLastSelection;

		public void SetView(ref cChecksToEmailView viewContext)
		{
			theView = viewContext;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			CancelSelection();
		}

		private void CancelSelection()
		{
			theView.Cancel();
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			Commit();
		}

		private void Commit()
		{
			Dictionary<int, object> useList = new Dictionary<int, object>();
			int lngRow;
			for (lngRow = 1; lngRow <= gridList.Rows - 1; lngRow++)
			{
				if (FCConvert.CBool(gridList.TextMatrix(lngRow, CNSTGridColUse)))
				{
					useList.Add(gridList.TextMatrix(lngRow, cnstgridcolID).ToIntegerValue(), gridList.TextMatrix(lngRow, cnstgridcolID));
				}
			}
			if (useList.Count > 0)
			{
                Close();
                theView.Commit(ref useList);
            }
			else
			{
				MessageBox.Show("You haven't selected anything to email", "Nothing Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void frmChecksToEmail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChecksToEmail_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			boolLastSelection = true;
			SetupGrid();
			FillGrid();
		}

		private void FillGrid()
		{
			theView.Checks.MoveFirst();
			cEmployeeCheck checkItem;
			// vbPorter upgrade warning: strCheckNumber As string	OnWrite(string, int)
			string strCheckNumber = "";
			while (theView.Checks.IsCurrent())
			{
				checkItem = (cEmployeeCheck)theView.Checks.GetCurrentItem();
				if (checkItem.Net > 0 && checkItem.CheckAmount == 0 && (checkItem.CheckingAmount > 0 || checkItem.SavingsAmount > 0))
				{
					strCheckNumber = "D" + FCConvert.ToString(checkItem.CheckNumber);
				}
				else
				{
					strCheckNumber = FCConvert.ToString(checkItem.CheckNumber);
				}
				AddItem(checkItem.ID, checkItem.EmployeeNumber, checkItem.EmployeeName, checkItem.Employee.Email, strCheckNumber, checkItem.Net, checkItem.Selected);
				theView.Checks.MoveNext();
			}
		}

		private void AddItem(int lngID, string strEmpNo, string strName, string strEmail, string strCheckNumber, double dblAmount, bool boolUse)
		{
			int lngRow;
			gridList.Rows += 1;
			lngRow = gridList.Rows - 1;
			gridList.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(lngID));
			gridList.TextMatrix(lngRow, CNSTGRIDCOLAMOUNT, Strings.Format(dblAmount, "0.00"));
			gridList.TextMatrix(lngRow, CNSTGridColCheckNo, strCheckNumber);
			gridList.TextMatrix(lngRow, CNSTGridCOLEmail, strEmail);
			gridList.TextMatrix(lngRow, CNSTGridColEmpNo, strEmpNo);
			gridList.TextMatrix(lngRow, CNSTGridColName, strName);
			gridList.TextMatrix(lngRow, CNSTGridColUse, FCConvert.ToString(boolUse));
		}

		private void FillTest()
		{
			int lngRow;
			gridList.Rows += 1;
			lngRow = gridList.Rows - 1;
			gridList.TextMatrix(lngRow, CNSTGRIDCOLAMOUNT, FCConvert.ToString(123.4));
			gridList.TextMatrix(lngRow, CNSTGridColName, "Employee One");
			gridList.TextMatrix(lngRow, CNSTGridCOLEmail, "someone@email.com");
			gridList.TextMatrix(lngRow, CNSTGridColEmpNo, "1");
			gridList.Rows += 1;
			lngRow = gridList.Rows - 1;
			gridList.TextMatrix(lngRow, CNSTGRIDCOLAMOUNT, FCConvert.ToString(343.23));
			gridList.TextMatrix(lngRow, CNSTGridColName, "Employee Two");
			gridList.TextMatrix(lngRow, CNSTGridCOLEmail, "ldkf@email.com");
			gridList.TextMatrix(lngRow, CNSTGridColEmpNo, "2");
		}

		private void SetupGrid()
		{
			gridList.ColHidden(cnstgridcolID, true);
			gridList.ColDataType(CNSTGridColUse, FCGrid.DataTypeSettings.flexDTBoolean);
			gridList.TextMatrix(0, CNSTGridColEmpNo, "Employee");
			gridList.TextMatrix(0, CNSTGridColName, "Employee Name");
			gridList.TextMatrix(0, CNSTGridCOLEmail, "Email");
			gridList.TextMatrix(0, CNSTGRIDCOLAMOUNT, "Amount");
			gridList.TextMatrix(0, CNSTGridColCheckNo, "Check");
			gridList.ColAlignment(CNSTGridColEmpNo, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridList.ColAlignment(CNSTGRIDCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			gridList.ColAlignment(CNSTGridColCheckNo, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGrid()
		{
			int lngGridWidth;
			lngGridWidth = gridList.WidthOriginal;
			gridList.ColWidth(CNSTGridColUse,(0.05 * lngGridWidth).ToInteger());
			gridList.ColWidth(CNSTGridColEmpNo, (0.1 * lngGridWidth).ToInteger());
			gridList.ColWidth(CNSTGridColName, (0.4 * lngGridWidth).ToInteger());
			gridList.ColWidth(CNSTGridColCheckNo, (0.1 * lngGridWidth).ToInteger());
			gridList.ColWidth(CNSTGridCOLEmail, (0.25 * lngGridWidth).ToInteger());
		}

		private void frmChecksToEmail_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void gridList_ClickEvent(object sender, System.EventArgs e)
		{
			
		}

		private void gridList_RowColChange(object sender, System.EventArgs e)
		{
			int lngCol;
			lngCol = gridList.MouseCol;
			if (lngCol == CNSTGridColUse)
			{
				gridList.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				gridList.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			CancelSelection();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Commit();
		}

        private void gridList_MouseDown(object sender, MouseEventArgs e)
        {
            // vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
            int intCol;
            int lngRow;
            intCol = gridList.MouseCol;
            lngRow = gridList.MouseRow;
            if (lngRow == 0)
            {
                if (intCol == CNSTGridColUse)
                {
                    if (boolLastSelection)
                    {
                        boolLastSelection = false;
                    }
                    else
                    {
                        boolLastSelection = true;
                    }
                    for (lngRow = 1; lngRow <= gridList.Rows - 1; lngRow++)
                    {
                        gridList.TextMatrix(lngRow, CNSTGridColUse, FCConvert.ToString(boolLastSelection));
                    }
                }
            }
        }
    }
}
