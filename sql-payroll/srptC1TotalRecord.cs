﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1TotalRecord.
	/// </summary>
	public partial class srptC1TotalRecord : FCSectionReport
	{
		public srptC1TotalRecord()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1TotalRecord InstancePtr
		{
			get
			{
				return (srptC1TotalRecord)Sys.GetInstance(typeof(srptC1TotalRecord));
			}
		}

		protected srptC1TotalRecord _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1TotalRecord	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strRec;
			string strTemp;
			double dblTemp;
			int lngTemp;
			double dblUCDue = 0;
			strRec = FCConvert.ToString(this.UserData);
			strTemp = Strings.Mid(strRec, 27, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtUGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 41, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtExcess.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 55, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtTaxable.Text = Strings.Format(dblTemp, "#,###,##0.00");
			if (strRec.Length <= 275)
			{
				strTemp = Strings.Mid(strRec, 88, 13);
				dblTemp = Conversion.Val(strTemp);
				dblTemp /= 100;
				txtUCDue.Text = Strings.Format(dblTemp, "#,###,##0.00");
			}
			else
			{
				dblUCDue = Conversion.Val(Strings.Mid(strRec, 276));
				txtUCDue.Text = Strings.Format(dblUCDue * dblTemp, "#,###,##0.00");
			}
			strTemp = Strings.Mid(strRec, 112, 11);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtVoucherPayments.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 123, 11);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtStateTaxDue.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 175, 11);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtTotalDue.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 199, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtStateGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 213, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtStateTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 227, 7);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			txtM1Employees.Text = Strings.Format(lngTemp, "#,###,##0");
			strTemp = Strings.Mid(strRec, 234, 7);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			txtM2Employees.Text = Strings.Format(lngTemp, "#,###,##0");
			strTemp = Strings.Mid(strRec, 241, 7);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			txtM3Employees.Text = Strings.Format(lngTemp, "#,###,##0");
			strTemp = Strings.Mid(strRec, 248, 7);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			txtM1Females.Text = Strings.Format(lngTemp, "#,###,##0");
			strTemp = Strings.Mid(strRec, 255, 7);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			txtM2Females.Text = Strings.Format(lngTemp, "#,###,##0");
			strTemp = Strings.Mid(strRec, 262, 7);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			txtM3Females.Text = Strings.Format(lngTemp, "#,###,##0");
		}

		
	}
}
