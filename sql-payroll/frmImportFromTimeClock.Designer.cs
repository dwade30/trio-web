//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmImportFromTimeClock.
	/// </summary>
	partial class frmImportFromTimeClock
	{
		public fecherFoundation.FCComboBox cmbTimeCard;
		public fecherFoundation.FCLabel lblTimeCard;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtImport;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbTimeCard = new fecherFoundation.FCComboBox();
            this.lblTimeCard = new fecherFoundation.FCLabel();
            this.cmdBrowse = new fecherFoundation.FCButton();
            this.txtImport = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcessFile = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessFile)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessFile);
            this.BottomPanel.Location = new System.Drawing.Point(0, 258);
            this.BottomPanel.Size = new System.Drawing.Size(494, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdBrowse);
            this.ClientArea.Controls.Add(this.txtImport);
            this.ClientArea.Controls.Add(this.cmbTimeCard);
            this.ClientArea.Controls.Add(this.lblTimeCard);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(494, 198);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(494, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(268, 30);
            this.HeaderText.Text = "Import from Time Clock";
            // 
            // cmbTimeCard
            // 
            this.cmbTimeCard.AutoSize = false;
            this.cmbTimeCard.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTimeCard.FormattingEnabled = true;
            this.cmbTimeCard.Items.AddRange(new object[] {
            "Time Card",
            "Detailed Shift"});
            this.cmbTimeCard.Location = new System.Drawing.Point(156, 30);
            this.cmbTimeCard.Name = "cmbTimeCard";
            this.cmbTimeCard.Size = new System.Drawing.Size(186, 40);
            this.cmbTimeCard.TabIndex = 6;
            // 
            // lblTimeCard
            // 
            this.lblTimeCard.AutoSize = true;
            this.lblTimeCard.Location = new System.Drawing.Point(30, 44);
            this.lblTimeCard.Name = "lblTimeCard";
            this.lblTimeCard.Size = new System.Drawing.Size(69, 15);
            this.lblTimeCard.TabIndex = 7;
            this.lblTimeCard.Text = "FILE TYPE";
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.AppearanceKey = "actionButton";
            this.cmdBrowse.Location = new System.Drawing.Point(362, 120);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(104, 40);
            this.cmdBrowse.TabIndex = 5;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // txtImport
            // 
            this.txtImport.AutoSize = false;
            this.txtImport.BackColor = System.Drawing.SystemColors.Window;
            this.txtImport.LinkItem = null;
            this.txtImport.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtImport.LinkTopic = null;
            this.txtImport.Location = new System.Drawing.Point(30, 120);
            this.txtImport.Name = "txtImport";
            this.txtImport.Size = new System.Drawing.Size(312, 40);
            this.txtImport.TabIndex = 4;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(106, 15);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "IMPORT FILE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Process File";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcessFile
            // 
            this.cmdProcessFile.AppearanceKey = "acceptButton";
            this.cmdProcessFile.Location = new System.Drawing.Point(172, 30);
            this.cmdProcessFile.Name = "cmdProcessFile";
            this.cmdProcessFile.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessFile.Size = new System.Drawing.Size(140, 48);
            this.cmdProcessFile.TabIndex = 0;
            this.cmdProcessFile.Text = "Process File";
            this.cmdProcessFile.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmImportFromTimeClock
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(494, 338);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmImportFromTimeClock";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Import from Time Clock";
            this.Load += new System.EventHandler(this.frmImportFromTimeClock_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportFromTimeClock_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessFile)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessFile;
		private System.ComponentModel.IContainer components;
	}
}