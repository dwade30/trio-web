//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPayrollAccountsSetup.
	/// </summary>
	partial class frmPayrollAccountsSetup
	{
		public fecherFoundation.FCGrid vsAccounts;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileValidAccounts;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileValidAccounts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdValidAccounts = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 437);
			this.BottomPanel.Size = new System.Drawing.Size(634, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.vsAccounts);
			this.ClientArea.Size = new System.Drawing.Size(654, 628);
			this.ClientArea.Controls.SetChildIndex(this.vsAccounts, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdSave, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdValidAccounts);
			this.TopPanel.Size = new System.Drawing.Size(654, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdValidAccounts, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(251, 28);
			this.HeaderText.Text = "Setup Payroll Accounts";
			// 
			// vsAccounts
			// 
			this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsAccounts.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsAccounts.Cols = 5;
			this.vsAccounts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsAccounts.ExtendLastCol = true;
			this.vsAccounts.FixedCols = 2;
			this.vsAccounts.FrozenCols = 1;
			this.vsAccounts.Location = new System.Drawing.Point(30, 30);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.ReadOnly = false;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.Size = new System.Drawing.Size(630, 337);
			this.vsAccounts.StandardTab = false;
			this.vsAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTAbove;
			this.vsAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsAccounts.TabIndex = 2;
			this.vsAccounts.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsAccounts_AfterEdit);
			this.vsAccounts.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsAccounts_ChangeEdit);
			this.vsAccounts.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsAccounts_BeforeEdit);
			this.vsAccounts.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.vsAccounts_EditingControlShowing);
			this.vsAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAccounts_ValidateEdit);
			this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
			this.vsAccounts.Enter += new System.EventHandler(this.vsAccounts_Enter);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileValidAccounts,
            this.mnuSeperator,
            this.mnuSave,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileValidAccounts
			// 
			this.mnuFileValidAccounts.Enabled = false;
			this.mnuFileValidAccounts.Index = 0;
			this.mnuFileValidAccounts.Name = "mnuFileValidAccounts";
			this.mnuFileValidAccounts.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuFileValidAccounts.Text = "Valid Accounts List";
			this.mnuFileValidAccounts.Click += new System.EventHandler(this.mnuFileValidAccounts_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 1;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 2;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 3;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdValidAccounts
			// 
			this.cmdValidAccounts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdValidAccounts.Location = new System.Drawing.Point(488, 29);
			this.cmdValidAccounts.Name = "cmdValidAccounts";
			this.cmdValidAccounts.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdValidAccounts.Size = new System.Drawing.Size(138, 24);
			this.cmdValidAccounts.TabIndex = 1;
			this.cmdValidAccounts.Text = "Valid Accounts List";
			this.cmdValidAccounts.Visible = false;
			this.cmdValidAccounts.Click += new System.EventHandler(this.mnuFileValidAccounts_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 389);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 1001;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmPayrollAccountsSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(654, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPayrollAccountsSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Setup Payroll Accounts";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmPayrollAccountsSetup_Load);
			this.Activated += new System.EventHandler(this.frmPayrollAccountsSetup_Activated);
			this.Resize += new System.EventHandler(this.frmPayrollAccountsSetup_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPayrollAccountsSetup_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdValidAccounts;
	}
}