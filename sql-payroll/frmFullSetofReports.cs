//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmFullSetofReports : BaseForm
	{
		public frmFullSetofReports()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFullSetofReports InstancePtr
		{
			get
			{
				return (frmFullSetofReports)Sys.GetInstance(typeof(frmFullSetofReports));
			}
		}

		protected frmFullSetofReports _InstancePtr = null;
		string strOldPrinter;

		public void Init(string strOldPrinterName)
		{
			strOldPrinter = strOldPrinterName;
			this.Show(App.MainForm);
            //FC:FINAL:BSE #2730 set form title 
            this.HeaderText.Text = App.MainForm.NavigationMenu.CurrentItem.Text;
            this.Text = App.MainForm.NavigationMenu.CurrentItem.Text;

        }

		private void frmFullSetofReports_Activated(object sender, System.EventArgs e)
		{

		}

		private void frmFullSetofReports_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsComplete = new clsDRWrapper();
			bool boolNoneDone;
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lblTrust.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.TRUST];
			lblCheckRegister.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.CHECKREG];
			lblPayTaxSummary.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.PAYTAX];
			lblDeductionReport.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.DEDUCTION];
			lblDirectDeposit.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.DIRECTDEPOSIT];
			lblAccountingEntry.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.ACCOUNTING];
			lblDataEntryForms.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.DATAENTRY];
			lblVacation.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.VACSICK];
			lblPayrollWarrant.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.WARRANT];
			boolNoneDone = true;
			lblAudit.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.AUDIT];
			lblReset.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETTEMP];
			lblClearAudit.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETAUDIT];
			lblFTD940.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940];
			lblFTD941.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941];
			lblMSRS.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS];
			lblMMA.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT];
			lblNonMMA.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA];
			lblStateUnemployment.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATEUNEMPLOYMENT];
			lblFUTA.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.FUTA940];
			lbl941Wage.Enabled = modGlobalVariables.Statics.ValidReports[modGlobalVariables.WAGE941];
			rsComplete.OpenRecordset("SELECT * FROM tblPayrollSteps WHERE PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun));
			if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
			{
				if (!FCConvert.ToBoolean(rsComplete.Get_Fields_Boolean("QTDProcess")))
				{
					// PrintAllReports
					return;
				}
			}
			else if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
			{
				if (!FCConvert.ToBoolean(rsComplete.Get_Fields_Boolean("MTDProcess")))
				{
					// PrintAllReports
					return;
				}
			}
			else if (modGlobalVariables.Statics.gstrMQYProcessing == "YEARLY")
			{
				if (!FCConvert.ToBoolean(rsComplete.Get_Fields_Boolean("YTDCalendarProcess")))
				{
					// PrintAllReports
					return;
				}
			}
			else
			{
				// Exit Sub
			}
			if (rsComplete.EndOfFile() != true && rsComplete.BeginningOfFile() != true)
			{
				if (lblTrust.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("TrustAgency"))
					{
						imgTrust.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblCheckRegister.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("CheckRegister"))
					{
						imgCheckRegister.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblPayTaxSummary.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("PaySummary"))
					{
						imgPayTaxSummary.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblDeductionReport.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("DeductionReports"))
					{
						imgDeductionReport.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblDirectDeposit.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("BankList"))
					{
						imgDirectDeposit.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblAccountingEntry.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("AccountingSummary"))
					{
						imgAccountingEntry.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblDataEntryForms.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("DEForms"))
					{
						imgDataEntryForms.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblVacation.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("VacationReport"))
					{
						imgVacation.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblPayrollWarrant.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("Warrant"))
					{
						imgPayrollWarant.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblAudit.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("AuditReport"))
					{
						imgAudit.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblClearAudit.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("AuditClear"))
					{
						imgClearAudit.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblReset.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("ResetTempData"))
					{
						imgReset.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblFTD940.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("FTD940"))
					{
						imgFTD940.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblFTD941.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("FTD941"))
					{
						imgFTD941.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblMSRS.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("MSRS"))
					{
						imgMSRS.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lbl941Wage.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("WAGE941"))
					{
						img941Wage.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblMMA.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("MMA"))
					{
						imgMMA.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblNonMMA.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("NonMMA"))
					{
						imgNonMMA.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblStateUnemployment.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("StateUnemployment"))
					{
						imgStateUnemployment.Visible = true;
						boolNoneDone = false;
					}
				}
				if (lblFUTA.Enabled)
				{
					if (rsComplete.Get_Fields_Boolean("FUTA"))
					{
						imgFUTA.Visible = true;
						boolNoneDone = false;
					}
				}
			}
			// If boolNoneDone Then
			// PrintAllReports
			// End If
		}

		private void frmFullSetofReports_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet = false;
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			PrintAllReports();
			// gtypeFullSetReports.boolFullSet = False
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void PrintAllReports()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolTemp = false;
			string strSequence = "";
            List<string> batchReports = new List<string>();
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.TRUST] && (imgTrust.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "TrustAgency"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					imgTrust.Visible = false;
					imgTrustProcessing.Visible = true;
					this.Refresh();
					//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
                    //App.DoEvents();
					RptTrustAndAgencyCodes.InstancePtr.Init(batchReports: batchReports);
					System.Threading.Thread.Sleep(1000);
					RptTrustAndAgencyCodes.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                CheckTrust:
					;
					if (ReportComplete("rptTrustAndAgency"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modGlobalRoutines.UpdatePayrollStepTable("TrustAgency");
							imgTrustProcessing.Visible = false;
							imgTrust.Visible = true;
							this.Refresh();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify Trust And Agency report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// Exit Sub
							modGlobalRoutines.UpdatePayrollStepTable("TrustAgency");
							imgTrustProcessing.Visible = false;
							imgTrust.Visible = true;
							this.Refresh();
						}
					}
					else
					{
						goto CheckTrust;
					}
				}
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("TrustAgency");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.CHECKREG] && (imgCheckRegister.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("TrustAgency", "CheckRegister", True) Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgCheckRegister.Visible = false;
				imgCheckRegisterProcessing.Visible = true;
				this.Refresh();
                //App.DoEvents();
                frmCheckRegister.InstancePtr.batchReports = batchReports;
				frmCheckRegister.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				//App.DoEvents();
				System.Threading.Thread.Sleep(1000);
                frmCheckRegister.InstancePtr.batchReports = null;
                rptCheckRegister.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckRegister:
				;
				if (ReportComplete("rptCheckRegister"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("CheckRegister");
						imgCheckRegisterProcessing.Visible = false;
						imgCheckRegister.Visible = true;
						this.Refresh();
						//App.DoEvents();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Check Register report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("CheckRegister");
						imgCheckRegisterProcessing.Visible = false;
						imgCheckRegister.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckRegister;
				}
				// End If
			}
			else
			{
				// Call UpdatePayrollStepTable("CheckRegister")
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.PAYTAX] && (imgPayTaxSummary.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("CheckRegister", "PaySummary", True) Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgPayTaxSummary.Visible = false;
				imgPayTaxSummaryProcessing.Visible = true;
				this.Refresh();
				//App.DoEvents();
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modCustomReport.Statics.strPreSetReport = "PAYSUMMARY";
				if (modGlobalVariables.Statics.gstrMQYProcessing == "WEEKLY")
				{
					modCoreysSweeterCode.PrintPaySummaryReport(0, batchReports: batchReports);
				}
				else if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
				{
					modCoreysSweeterCode.PrintPaySummaryReport(1, batchReports: batchReports);
				}
				else if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
				{
					modCoreysSweeterCode.PrintPaySummaryReport(2, batchReports: batchReports);
				}
				else if (modGlobalVariables.Statics.gstrMQYProcessing == "YEARLY")
				{
					modCoreysSweeterCode.PrintPaySummaryReport(3, batchReports: batchReports);
				}
				else
				{
					modCoreysSweeterCode.PrintPaySummaryReport(4, batchReports: batchReports);
				}
				//App.DoEvents();
				System.Threading.Thread.Sleep(1000);
				rptPaySummaryReport.InstancePtr.Unload();
				rptTaxSumaryReport.InstancePtr.Unload();
				rptTaxableWageSummaryReport.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckPaySummary:
				;
				if (ReportComplete("rptPaySummaryReport"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("PaySummary");
						imgPayTaxSummaryProcessing.Visible = false;
						imgPayTaxSummary.Visible = true;
						this.Refresh();
						//App.DoEvents();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Pay Summary Report report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("PaySummary");
						imgPayTaxSummaryProcessing.Visible = false;
						imgPayTaxSummary.Visible = true;
						this.Refresh();
						//App.DoEvents();
					}
				}
				else
				{
					goto CheckPaySummary;
				}
				// End If
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("PaySummary");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.DEDUCTION] && (imgDeductionReport.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("PaySummary", "DeductionReports") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgDeductionReport.Visible = false;
				imgDeductionReportProcessing.Visible = true;
				this.Refresh();
				//App.DoEvents();
				// rptEmployeeDeductionReport.FCGlobal.FCGlobal.Printer.DeviceName = gtypeFullSetReports.strFullSetName
				modCustomReport.Statics.strPreSetReport = "EMPLOYEEDEDUCTIONS";
				// DuplexPrintReport rptEmployeeDeductionReport, gtypeFullSetReports.strFullSetName
				rptEmployeeDeductionReport.InstancePtr.Init(batchReports: batchReports);
				// rptEmployeeDeductionReport.PrintReport False
				System.Threading.Thread.Sleep(2000);
				rptEmployeeDeductionReport.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckDeduction:
				;
				if (ReportComplete("rptEmployeeDeductionReport"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("DeductionReports");
						imgDeductionReportProcessing.Visible = false;
						imgDeductionReport.Visible = true;
						this.Refresh();
						//App.DoEvents();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Employee Deduction Report report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("DeductionReports");
						imgDeductionReportProcessing.Visible = false;
						imgDeductionReport.Visible = true;
						this.Refresh();
						//App.DoEvents();
					}
				}
				else
				{
					goto CheckDeduction;
				}
				// End If
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("DeductionReports");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.DIRECTDEPOSIT] && (imgDirectDeposit.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("CheckRegister", "ChecksDirectDeposit") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgDirectDeposit.Visible = false;
				imgDirectDepositProcessing.Visible = true;
				this.Refresh();
				//App.DoEvents();
				// check to see if this is ACH or Direct Deposit report
				clsTemp.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
				if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("achclearinghouse")))
				{
					boolTemp = true;
					//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					modCoreysSweeterCode.ShowACHReport(this.Modal, batchReports: batchReports);
					System.Threading.Thread.Sleep(1000);
					rptACH.InstancePtr.Unload();
				}
				else
				{
					boolTemp = false;
					//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					rptDirectDeposit.InstancePtr.Init(batchReports: batchReports);
					System.Threading.Thread.Sleep(1000);
					rptDirectDeposit.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                }
				CheckDeposit:
				;
				if (boolTemp)
				{
					if (ReportComplete("rptach"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modGlobalRoutines.UpdatePayrollStepTable("BankList");
							imgDirectDepositProcessing.Visible = false;
							imgDirectDeposit.Visible = true;
							this.Refresh();
							//App.DoEvents();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify Bank List report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// Exit Sub
							modGlobalRoutines.UpdatePayrollStepTable("BankList");
							imgDirectDepositProcessing.Visible = false;
							imgDirectDeposit.Visible = true;
							this.Refresh();
							//App.DoEvents();
						}
					}
					else
					{
						goto CheckDeposit;
					}
				}
				else
				{
					if (ReportComplete("rptDirectDeposit"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modGlobalRoutines.UpdatePayrollStepTable("BankList");
							imgDirectDepositProcessing.Visible = false;
							imgDirectDeposit.Visible = true;
							this.Refresh();
							//App.DoEvents();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify Direct Deposit report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// Exit Sub
							modGlobalRoutines.UpdatePayrollStepTable("BankList");
							imgDirectDepositProcessing.Visible = false;
							imgDirectDeposit.Visible = true;
							this.Refresh();
							//App.DoEvents();
						}
					}
					else
					{
						goto CheckDeposit;
					}
				}
				// End If
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("BankList");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.ACCOUNTING] && (imgAccountingEntry.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("BankList", "AccountingSummary") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgAccountingEntry.Visible = false;
				imgAccountingEntryProcessing.Visible = true;
				this.Refresh();
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				rptPayrollAccountingChargesPreview.InstancePtr.Init(this.Modal, false, batchReports: batchReports);
				System.Threading.Thread.Sleep(1000);
				rptPayrollAccountingChargesPreview.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckAccounting:
				;
				if (ReportComplete("rptPayrollAccountingChargesPreview"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("AccountingSummary");
						imgAccountingEntryProcessing.Visible = false;
						imgAccountingEntry.Visible = true;
						this.Refresh();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Accounting Summary report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("AccountingSummary");
						imgAccountingEntryProcessing.Visible = false;
						imgAccountingEntry.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckAccounting;
				}
				// End If
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("AccountingSummary");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.DATAENTRY] && (imgDataEntryForms.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("AccountingSummary", "DEForms") Then
				modDavesSweetCode.Statics.blnDataEntryCompleted = false;
				imgDataEntryForms.Visible = false;
				imgDataEntryFormsProcessing.Visible = true;
				this.Refresh();
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modCustomReport.Statics.strPreSetReport = "DATAENTRYFORMS";
				modCustomReport.Statics.strCustomTitle = "**** Data Entry Forms ****";
				clsTemp.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
				if (Conversion.Val(clsTemp.Get_Fields("dataentrysequence")) == 0)
				{
					// name
					modCustomReport.Statics.strCustomSQL = "select * from tblemployeemaster where dataentry = 1 order by lastname,firstname,employeenumber ";
				}
				else if (Conversion.Val(clsTemp.Get_Fields("dataentrysequence")) == 1)
				{
					// employeenumber
					modCustomReport.Statics.strCustomSQL = "select * from tblemployeemaster where dataentry = 1 order by employeenumber ";
				}
				else if (Conversion.Val(clsTemp.Get_Fields("dataentrysequence")) == 2)
				{
					// sequence
					modCustomReport.Statics.strCustomSQL = "select * from tblemployeemaster where dataentry = 1 order by sequence,lastname,firstname,employeenumber ";
				}
				else if (Conversion.Val(clsTemp.Get_Fields("dataentrysequence")) == 3)
				{
					// dept/div
					modCustomReport.Statics.strCustomSQL = "Select * from tblemployeemaster where dataentry = 1 order by deptdiv,lastname,firstname,employeenumber ";
				}
				modDuplexPrinting.DuplexPrintReport(rptDataEntryForms.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// rptDataEntryForms.PrintReport False
				System.Threading.Thread.Sleep(1000);
				rptDataEntryForms.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckDEForms:
				;
				if (ReportComplete("rptDataEntryForms"))
				{
					if (modDavesSweetCode.Statics.blnDataEntryCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("DEForms");
						imgDataEntryFormsProcessing.Visible = false;
						imgDataEntryForms.Visible = true;
						this.Refresh();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Data Entry Forms report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("DEForms");
						imgDataEntryFormsProcessing.Visible = false;
						imgDataEntryForms.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckDEForms;
				}
				// End If
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("DEForms");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.VACSICK] && (imgVacation.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("DEForms", "SickReport") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgVacation.Visible = false;
				imgVacationProcessing.Visible = true;
				this.Refresh();
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modGlobalVariables.Statics.gstrVacType = FCConvert.ToString(1);
				// sick
				modCustomReport.Statics.strCustomTitle = "Employee Sick Report";
				rptCurrEmployeeVacSick.InstancePtr.Init(1, false, batchReports: batchReports);
				System.Threading.Thread.Sleep(1000);
				rptCurrEmployeeVacSick.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
                modCustomReport.Statics.strPreSetReport = "EMPLOYEEVACATION";
				modGlobalVariables.Statics.gstrVacType = FCConvert.ToString(2);
				// vacation
				rptCurrEmployeeVacSick.InstancePtr.Init(2, false, batchReports: batchReports);
				System.Threading.Thread.Sleep(1000);
				rptCurrEmployeeVacSick.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
                clsTemp.OpenRecordset("select * from tblcodetypes where ID > 2 order by ID", "twpy0000.vb1");
				modGlobalVariables.Statics.gstrVacType = FCConvert.ToString(-1000);
				while (!clsTemp.EndOfFile())
				{
					rptCurrEmployeeVacSick.InstancePtr.Init(clsTemp.Get_Fields("ID"), false, batchReports: batchReports);
					System.Threading.Thread.Sleep(1000);
					rptCurrEmployeeVacSick.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                    clsTemp.MoveNext();
				}
				//App.DoEvents();
				// Corey unlooad your report in the Report_End event
				CheckVacSick:
				;
				if (ReportComplete("rptcurrEmployeeVacSick"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("SickReport");
						modGlobalRoutines.UpdatePayrollStepTable("VacationReport");
						modGlobalRoutines.UpdatePayrollStepTable("OtherCodeTypes");
						imgVacationProcessing.Visible = false;
						imgVacation.Visible = true;
						this.Refresh();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Employee Vac / Sick report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						modGlobalRoutines.UpdatePayrollStepTable("SickReport");
						modGlobalRoutines.UpdatePayrollStepTable("VacationReport");
						modGlobalRoutines.UpdatePayrollStepTable("OtherCodeTypes");
						imgVacationProcessing.Visible = false;
						imgVacation.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckVacSick;
				}
				// End If
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("SickReport");
				modGlobalRoutines.UpdatePayrollStepTable("VacationReport");
				modGlobalRoutines.UpdatePayrollStepTable("OtherCodeTypes");
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.WARRANT] && imgPayrollWarant.Visible == false)
			{
				// If CheckIfPayProcessCompleted("SickReport", "Warrant") Then
				modDavesSweetCode.Statics.blnWarrantCompleted = false;
				imgPayrollWarant.Visible = false;
				imgPayrollWarrantProcessing.Visible = true;
				frmWarrant.InstancePtr.Unload();
				this.Refresh();
                frmWarrant.InstancePtr.batchReports = batchReports;
                frmWarrant.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				//App.DoEvents();
				System.Threading.Thread.Sleep(4000);
                frmWarrant.InstancePtr.batchReports = null;
                rptWarrant.InstancePtr.Unload();
				frmWarrant.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckWarrant:
				;
				if (ReportComplete("rptWarrant"))
				{
					if (modDavesSweetCode.Statics.blnWarrantCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("Warrant");
						imgPayrollWarrantProcessing.Visible = false;
						imgPayrollWarant.Visible = true;
						this.Refresh();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Warrant report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("Warrant");
						imgPayrollWarrantProcessing.Visible = false;
						imgPayrollWarant.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckWarrant;
				}
				// End If
			}
			else
			{
				// Call UpdatePayrollStepTable("Warrant")
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.AUDIT] && (imgAudit.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// If CheckIfPayProcessCompleted("Warrant", "AuditReport") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgAudit.Visible = false;
				imgAuditProcessing.Visible = true;
				this.Refresh();
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(rptNewAuditHistory.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// rptAuditHistory.PrintReport False
				System.Threading.Thread.Sleep(1000);
				rptNewAuditHistory.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckAudit:
				;
				if (ReportComplete("rptAuditHistory"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("AuditReport");
						imgAuditProcessing.Visible = false;
						imgAudit.Visible = true;
						this.Refresh();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Audit History report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						// Exit Sub
						modGlobalRoutines.UpdatePayrollStepTable("AuditReport");
						imgAuditProcessing.Visible = false;
						imgAudit.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckAudit;
				}
				// End If
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETAUDIT] && (imgClearAudit.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// WE HAVE THE CASE WHERE THEY CHOOSE NOT TO HAVE THE AUDIT REPORT SHOW
				// WITH THEIR FULL SET SO WE CANNOT CHECK THIS REPORT FOR COMPLETION
				// MATTHEW 3/23/2004
				// If CheckIfPayProcessCompleted("AuditReport", "AuditClear") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgClearAudit.Visible = false;
				imgClearAuditProcessing.Visible = true;
				this.Refresh();
				modGlobalRoutines.ClearAuditDataToArchive();
				modGlobalRoutines.UpdatePayrollStepTable("AuditClear");
				imgClearAuditProcessing.Visible = false;
				imgClearAudit.Visible = true;
				this.Refresh();
				// End If
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETTEMP] && (imgReset.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				// WE HAVE THE CASE WHERE THEY CHOOSE NOT TO HAVE THE AUDIT REPORT SHOW
				// WITH THEIR FULL SET SO WE CANNOT CHECK THIS REPORT FOR COMPLETION
				// MATTHEW 3/23/2004
				// If CheckIfPayProcessCompleted("AuditReport", "ResetTempData") Then
				modDavesSweetCode.Statics.blnReportCompleted = false;
				imgReset.Visible = false;
				imgResetProcessing.Visible = true;
				this.Refresh();
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(rptTempFlagReset.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// rptTempFlagReset.PrintReport False
				System.Threading.Thread.Sleep(1000);
				rptTempFlagReset.InstancePtr.Unload();
                //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                //WaitForReportPrint();
				CheckReset:
				;
				if (ReportComplete("rptTempFlagReset"))
				{
					if (modDavesSweetCode.Statics.blnReportCompleted)
					{
						modGlobalRoutines.UpdatePayrollStepTable("ResetTempData");
						imgResetProcessing.Visible = false;
						imgReset.Visible = true;
						this.Refresh();
					}
					else
					{
						MessageBox.Show("Errors may have been encountered. Verify Temp Flag Reset report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						modGlobalRoutines.UpdatePayrollStepTable("ResetTempData");
						imgResetProcessing.Visible = false;
						imgReset.Visible = true;
						this.Refresh();
					}
				}
				else
				{
					goto CheckReset;
				}
				// End If
			}
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WASHBURN WATER & SEWER")
			{
				if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941] && (imgFTD941.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
				{
					if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "MTDProcess"))
					{
						modDavesSweetCode.Statics.blnReportCompleted = false;
						imgFTD941.Visible = false;
						imgFTD941Processing.Visible = true;
						this.Refresh();
						// Call frmPrint941.Init(0, 0)
						// Dim fedView As New cFed941ViewContext
						// strSequence = frmGetSequence.Init()
						// If strSequence <> vbNullString Then
						// If strSequence = "-1" Then
						// strSequence = ""
						// End If
						// End If
						// fedView.Sequence = strSequence
						// Call frm941TaxLiability.Init(fedView)
						// Call frmPrint941.Init(fedView)
						//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
						rpt941.InstancePtr.Init(batchReports: batchReports);
						System.Threading.Thread.Sleep(1000);
						rpt941.InstancePtr.Unload();
                        //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                        //WaitForReportPrint();
                    Checkrpt941:
						;
						if (ReportComplete("rpt941"))
						{
							if (modDavesSweetCode.Statics.blnReportCompleted)
							{
								modDavesSweetCode.UpdateReportStatus("FTD941");
								imgFTD941Processing.Visible = false;
								imgFTD941.Visible = true;
								this.Refresh();
							}
							else
							{
								MessageBox.Show("Errors may have been encountered. Verify 941 report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								imgFTD941Processing.Visible = false;
								imgFTD941.Visible = true;
								this.Refresh();
							}
						}
						else
						{
							goto Checkrpt941;
						}
					}
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] && (imgFTD940.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "MTDProcess"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					imgFTD940.Visible = false;
					imgFTD940Processing.Visible = true;
					this.Refresh();
					rpt940.InstancePtr.Init(batchReports: batchReports);
					System.Threading.Thread.Sleep(1000);
					rpt940.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                Checkrpt940:
					;
					if (ReportComplete("rpt940"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modDavesSweetCode.UpdateReportStatus("FTD940");
							imgFTD940Processing.Visible = false;
							imgFTD940.Visible = true;
							this.Refresh();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify 940 report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// Exit Sub
							imgFTD940Processing.Visible = false;
							imgFTD940.Visible = true;
							this.Refresh();
						}
					}
					else
					{
						goto Checkrpt940;
					}
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] && (imgMSRS.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "MTDProcess"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					imgMSRS.Visible = false;
					imgMSRSProcessing.Visible = true;
					this.Refresh();
                    frmMSRSSetup.InstancePtr.batchReports = batchReports;
                    frmMSRSSetup.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					//App.DoEvents();
					System.Threading.Thread.Sleep(1000);
                    frmMSRSSetup.InstancePtr.batchReports = null;
					// Unload rptMSRSPayrollDetail
					rptMSRSPayrollSummary.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                CheckMSRS:
					;
					if (ReportComplete("rptMSRSPayrollSummary"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modDavesSweetCode.UpdateReportStatus("MSRS");
							imgMSRSProcessing.Visible = false;
							imgMSRS.Visible = true;
							this.Refresh();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify MainePERS Payroll Summary report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// Exit Sub
							imgMSRSProcessing.Visible = false;
							imgMSRS.Visible = true;
							this.Refresh();
						}
					}
					else
					{
						goto CheckMSRS;
					}
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.WAGE941] && (img941Wage.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "QTDProcess"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					img941Wage.Visible = false;
					img941WageProcessing.Visible = true;
					this.Refresh();
					// Call frmPrint941.Init(0, 0)
					cFed941ViewContext fedView = new cFed941ViewContext();
					strSequence = frmGetSequence.InstancePtr.Init();
					if (strSequence != string.Empty)
					{
						if (strSequence == "-1")
						{
							strSequence = "";
						}
					}
					fedView.Sequence = strSequence;
					frm941TaxLiability.InstancePtr.Init(fedView);

                    frmPrint941.InstancePtr.batchReports = batchReports;
                    frmPrint941.InstancePtr.Init(fedView);
                    frmPrint941.InstancePtr.batchReports = null;
                    //FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
                    modDuplexPrinting.DuplexPrintReport(rpt941Wage.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
					// rpt941Wage.PrintReport False
					//App.DoEvents();
					System.Threading.Thread.Sleep(1000);
					rpt941Wage.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                Check941Wage:
					;
					if (ReportComplete("rpt941Wage"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modDavesSweetCode.UpdateReportStatus("941Wage");
							img941WageProcessing.Visible = false;
							img941Wage.Visible = true;
							this.Refresh();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify 941 Wage report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// Exit Sub
							img941WageProcessing.Visible = false;
							img941Wage.Visible = true;
							this.Refresh();
						}
					}
					else
					{
						goto Check941Wage;
					}
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT] && (imgMMA.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "MTDProcess"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					modDavesSweetCode.Statics.blnReportStarted = false;
					imgMMA.Visible = false;
					imgMMAProcessing.Visible = true;
					this.Refresh();
					//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					frmUnemploymentReport.InstancePtr.strReportType = "M";
                    frmUnemploymentReport.InstancePtr.batchReports = batchReports;
                    frmUnemploymentReport.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    frmUnemploymentReport.InstancePtr.batchReports = null;
                //App.DoEvents();
                CheckMMA:
					;
					// corey 12/29/2005 timing issue
					while (!modDavesSweetCode.Statics.blnReportStarted)
					{
						System.Threading.Thread.Sleep(500);
					}
					rptMMAUnemploymentReport.InstancePtr.Unload();
					while (!modDavesSweetCode.Statics.blnReportCompleted)
					{
						System.Threading.Thread.Sleep(500);
					}
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                    // If ReportComplete("rptMMAUnemploymentReport") Then
                    // If blnReportCompleted Then
                    modDavesSweetCode.UpdateReportStatus("MMA");
					imgMMAProcessing.Visible = false;
					imgMMA.Visible = true;
					this.Refresh();
					// Else
					// MsgBox "Errors may have been encountered. Verify MMA Unemployment report.", vbCritical, "Report Error"
					// Exit Sub
					// imgMMAProcessing.Visible = False
					// imgMMA.Visible = True
					// Me.Refresh
					// 
					// End If
					// Else
					// GoTo CheckMMA
					// End If
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA] && (imgNonMMA.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "MTDProcess"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					modDavesSweetCode.Statics.blnReportStarted = false;
					imgNonMMA.Visible = false;
					imgNonMMAProcessing.Visible = true;
					this.Refresh();
                    //FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
                    frmUnemploymentReport.InstancePtr.batchReports = batchReports;
                    frmUnemploymentReport.InstancePtr.strReportType = "N";
					frmUnemploymentReport.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    frmUnemploymentReport.InstancePtr.batchReports = null;
					//App.DoEvents();
					CheckNonMMA:
					;
					while (!modDavesSweetCode.Statics.blnReportStarted)
					{
						System.Threading.Thread.Sleep(500);
					}
					// corey 12/28/2005  timing issue causing error message
					rptNonMMAUnemploymentReport.InstancePtr.Unload();
					// If ReportComplete("rptNonMMAUnemploymentReport") Then
					// If blnReportCompleted Then
					while (!modDavesSweetCode.Statics.blnReportCompleted)
					{
						System.Threading.Thread.Sleep(500);
					}
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                    modDavesSweetCode.UpdateReportStatus("NonMMA");
					imgNonMMAProcessing.Visible = false;
					imgNonMMA.Visible = true;
					this.Refresh();
					// Else
					// MsgBox "Errors may have been encountered. Verify Non-MMA Unemployment report.", vbCritical, "Report Error"
					// Exit Sub
					// imgNonMMAProcessing.Visible = False
					// imgNonMMA.Visible = True
					// Me.Refresh
					// End If
					// Else
					// GoTo CheckNonMMA
					// End If
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATEUNEMPLOYMENT] && (imgStateUnemployment.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "QTDProcess"))
				{
					int intQ = 0;
					int lngY = 0;
					lngY = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 1 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 3)
					{
						intQ = 1;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 4 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 6)
					{
						intQ = 2;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 7 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 9)
					{
						intQ = 3;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 10 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 12)
					{
						intQ = 4;
					}
					strSequence = frmGetSequence.InstancePtr.Init();
					if (strSequence != string.Empty)
					{
						clsTemp.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                        frmElectronicWageReporting.InstancePtr.batchReports = batchReports;

                        if (!clsTemp.EndOfFile())
						{
							if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("electronicc1")))
							{
								frmElectronicWageReporting.InstancePtr.Init(intQ, lngY, strSequence);
							}
							else
							{
								frmElectronicWageReporting.InstancePtr.Init(intQ, lngY, strSequence, true);
							}
						}
						else
						{
							frmElectronicWageReporting.InstancePtr.Init(intQ, lngY, strSequence, true);
						}
						modDavesSweetCode.Statics.blnReportCompleted = false;
						imgStateUnemployment.Visible = false;
						imgStateUnemploymentProcessing.Visible = true;
						this.Refresh();
						//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
						modDuplexPrinting.DuplexPrintReport(rptStateUnemploymentC1.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
						// rptStateUnemploymentC1.PrintReport False
						System.Threading.Thread.Sleep(1000);
						rptStateUnemploymentC1.InstancePtr.Unload();
                        //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                        //WaitForReportPrint();
                    CheckState:
						;
						if (ReportComplete("rptStateUnemploymentC1"))
						{
							if (modDavesSweetCode.Statics.blnReportCompleted)
							{
								modDavesSweetCode.UpdateReportStatus("StateUnemployment");
								imgStateUnemploymentProcessing.Visible = false;
								imgStateUnemployment.Visible = true;
								this.Refresh();
							}
							else
							{
								MessageBox.Show("Errors may have been encountered. Verify State Unemployment C1 report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								// Exit Sub
								imgStateUnemploymentProcessing.Visible = false;
								imgStateUnemployment.Visible = true;
								this.Refresh();
							}
						}
						else
						{
							goto CheckState;
						}
					}
				}
			}
			if (modGlobalVariables.Statics.ValidReports[modGlobalVariables.FUTA940] && (imgFUTA.Visible == false || chkPrintAll.CheckState == Wisej.Web.CheckState.Checked))
			{
				int lngYear = 0;
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "YTDCalendarProcess"))
				{
					modDavesSweetCode.Statics.blnReportCompleted = false;
					imgFUTA.Visible = false;
					imgFUTAProcessing.Visible = true;
					this.Refresh();
					//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
					{
						//FC:FINAL:DSE:#i2421 Use date parameter by reference
						DateTime tmpArg = DateTime.FromOADate(0);
						frmSelectDateInfo.InstancePtr.Init4("Input FUTA Year", ref lngYear, -1, -1,ref tmpArg, -1, false);
					}
					else
					{
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					}
					if (lngYear > 0)
					{
						rptFUTA940Report.InstancePtr.Init(lngYear, batchReports: batchReports);
					}
					//App.DoEvents();
					System.Threading.Thread.Sleep(1000);
					rptFUTA940Report.InstancePtr.Unload();
                    //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
                    //WaitForReportPrint();
                CheckFUTA:
					;
					if (ReportComplete("rptFUTA940Report"))
					{
						if (modDavesSweetCode.Statics.blnReportCompleted)
						{
							modDavesSweetCode.UpdateReportStatus("FUTA");
							imgFUTAProcessing.Visible = false;
							imgFUTA.Visible = true;
							this.Refresh();
						}
						else
						{
							MessageBox.Show("Errors may have been encountered. Verify FUTA 940 report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							imgFUTAProcessing.Visible = false;
							imgFUTA.Visible = true;
							this.Refresh();
						}
					}
					else
					{
						goto CheckFUTA;
					}
				}
			}
			switch (modGlobalVariables.Statics.gstrMQYProcessing)
            {
                case "QUARTERLY":
                    modGlobalRoutines.UpdatePayrollStepTable("QTDProcess");

                    break;
                case "MONTHLY":
                    modGlobalRoutines.UpdatePayrollStepTable("MTDProcess");

                    break;
                case "YEARLY":
                    modGlobalRoutines.UpdatePayrollStepTable("YTDCalendarProcess");

                    break;
            }
            Global.Extensions.CombineAndPrintPDFs(batchReports);
			MessageBox.Show("Full Set of Reports Complete", "Reports Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			
			return;
		}

        internal static void WaitForReportPrint()
        {
            //FC:FINAL:SBE - show a message box after report was exported to stop the code execution. Next report will be exported after closing the MessageBox
            if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
            {
                //force running client scripts with Application.Update
                FCUtils.ApplicationUpdate(frmFullSetofReports.InstancePtr);
                //wait for client script to be executed
                System.Threading.Thread.Sleep(2000);
                MessageBox.Show("Report exported succesfully");
            }
        }

		private bool ReportComplete(string strReport)
		{
			bool ReportComplete = false;
			foreach (FCForm ff in FCGlobal.Statics.Forms)
			{
				if (ff.Name == strReport)
				{
					ReportComplete = false;
					return ReportComplete;
				}
			}
			ReportComplete = true;
			System.Threading.Thread.Sleep(2000);
			return ReportComplete;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet = false;
		}
	}
}
