﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptRetirementDeductionReport.
	/// </summary>
	partial class rptRetirementDeductionReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRetirementDeductionReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblField1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblField2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblField3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblField4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField1Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField2Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField3Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField4Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHoursTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSalaryTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSubtotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField1Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField4Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHoursTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalaryTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubtotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.txtField1,
				this.txtField2,
				this.txtField3,
				this.txtField4,
				this.txtHours,
				this.txtSalary,
				this.txtSubTotal
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtField1Total,
				this.txtField2Total,
				this.txtField3Total,
				this.txtField4Total,
				this.txtHoursTotal,
				this.txtSalaryTotal,
				this.Label11,
				this.txtSubtotalTotal,
				this.Line2
			});
			this.ReportFooter.Height = 0.3333333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.Label2,
				this.Label3,
				this.Label4,
				this.lblField1,
				this.lblField2,
				this.lblField3,
				this.lblField4,
				this.Label9,
				this.Label10,
				this.Label12,
				this.Line1
			});
			this.PageHeader.Height = 1.010417F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size" + ": 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL -  CUSTOM  RETIREMENT DEDUCTION  REPORT";
			this.Label1.Top = 0F;
			this.Label1.Width = 9.84375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.5625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 8.5625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.3125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.09375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label2.Text = "Emp #";
			this.Label2.Top = 0.8333333F;
			this.Label2.Width = 0.71875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.90625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label3.Text = "SSN";
			this.Label3.Top = 0.8333333F;
			this.Label3.Width = 0.71875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label4.Text = "Employee Name";
			this.Label4.Top = 0.8333333F;
			this.Label4.Width = 1.71875F;
			// 
			// lblField1
			// 
			this.lblField1.Height = 0.1666667F;
			this.lblField1.HyperLink = null;
			this.lblField1.Left = 3.75F;
			this.lblField1.Name = "lblField1";
			this.lblField1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblField1.Text = "Ded #1";
			this.lblField1.Top = 0.8333333F;
			this.lblField1.Width = 0.84375F;
			// 
			// lblField2
			// 
			this.lblField2.Height = 0.1666667F;
			this.lblField2.HyperLink = null;
			this.lblField2.Left = 4.65625F;
			this.lblField2.Name = "lblField2";
			this.lblField2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblField2.Text = "Ded #2";
			this.lblField2.Top = 0.8333333F;
			this.lblField2.Width = 0.84375F;
			// 
			// lblField3
			// 
			this.lblField3.Height = 0.1666667F;
			this.lblField3.HyperLink = null;
			this.lblField3.Left = 5.5625F;
			this.lblField3.Name = "lblField3";
			this.lblField3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblField3.Text = "Ded #3";
			this.lblField3.Top = 0.8333333F;
			this.lblField3.Width = 0.84375F;
			// 
			// lblField4
			// 
			this.lblField4.Height = 0.1666667F;
			this.lblField4.HyperLink = null;
			this.lblField4.Left = 6.46875F;
			this.lblField4.Name = "lblField4";
			this.lblField4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblField4.Text = "Match #1";
			this.lblField4.Top = 0.8333333F;
			this.lblField4.Width = 0.84375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 8.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label9.Text = "Total Hrs.";
			this.Label9.Top = 0.8333333F;
			this.Label9.Width = 0.75F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 9.03125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label10.Text = "Total Salary";
			this.Label10.Top = 0.8333333F;
			this.Label10.Width = 0.84375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 7.375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label12.Text = "Total";
			this.Label12.Top = 0.8333333F;
			this.Label12.Width = 0.84375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 9.8125F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 9.875F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.09375F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.59375F;
			// 
			// txtField1
			// 
			this.txtField1.Height = 0.1666667F;
			this.txtField1.Left = 3.75F;
			this.txtField1.Name = "txtField1";
			this.txtField1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField1.Text = "Field1";
			this.txtField1.Top = 0F;
			this.txtField1.Width = 0.84375F;
			// 
			// txtField2
			// 
			this.txtField2.Height = 0.1666667F;
			this.txtField2.Left = 4.65625F;
			this.txtField2.Name = "txtField2";
			this.txtField2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField2.Text = "Field2";
			this.txtField2.Top = 0F;
			this.txtField2.Width = 0.84375F;
			// 
			// txtField3
			// 
			this.txtField3.Height = 0.1666667F;
			this.txtField3.Left = 5.5625F;
			this.txtField3.Name = "txtField3";
			this.txtField3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField3.Text = "Field3";
			this.txtField3.Top = 0F;
			this.txtField3.Width = 0.84375F;
			// 
			// txtField4
			// 
			this.txtField4.Height = 0.1666667F;
			this.txtField4.Left = 6.46875F;
			this.txtField4.Name = "txtField4";
			this.txtField4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField4.Text = "Field4";
			this.txtField4.Top = 0F;
			this.txtField4.Width = 0.84375F;
			// 
			// txtHours
			// 
			this.txtHours.Height = 0.1666667F;
			this.txtHours.Left = 8.260417F;
			this.txtHours.Name = "txtHours";
			this.txtHours.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtHours.Text = null;
			this.txtHours.Top = 0F;
			this.txtHours.Width = 0.7395833F;
			// 
			// txtSalary
			// 
			this.txtSalary.Height = 0.1666667F;
			this.txtSalary.Left = 9.03125F;
			this.txtSalary.Name = "txtSalary";
			this.txtSalary.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSalary.Text = "Field6";
			this.txtSalary.Top = 0F;
			this.txtSalary.Width = 0.84375F;
			// 
			// txtSubTotal
			// 
			this.txtSubTotal.Height = 0.1666667F;
			this.txtSubTotal.Left = 7.375F;
			this.txtSubTotal.Name = "txtSubTotal";
			this.txtSubTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSubTotal.Text = "Field4";
			this.txtSubTotal.Top = 0F;
			this.txtSubTotal.Width = 0.84375F;
			// 
			// txtField1Total
			// 
			this.txtField1Total.Height = 0.1666667F;
			this.txtField1Total.Left = 3.75F;
			this.txtField1Total.Name = "txtField1Total";
			this.txtField1Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField1Total.Text = "Field1";
			this.txtField1Total.Top = 0.125F;
			this.txtField1Total.Width = 0.84375F;
			// 
			// txtField2Total
			// 
			this.txtField2Total.Height = 0.1666667F;
			this.txtField2Total.Left = 4.65625F;
			this.txtField2Total.Name = "txtField2Total";
			this.txtField2Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField2Total.Text = "Field2";
			this.txtField2Total.Top = 0.125F;
			this.txtField2Total.Width = 0.84375F;
			// 
			// txtField3Total
			// 
			this.txtField3Total.Height = 0.1666667F;
			this.txtField3Total.Left = 5.5625F;
			this.txtField3Total.Name = "txtField3Total";
			this.txtField3Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField3Total.Text = "Field3";
			this.txtField3Total.Top = 0.125F;
			this.txtField3Total.Width = 0.84375F;
			// 
			// txtField4Total
			// 
			this.txtField4Total.Height = 0.1666667F;
			this.txtField4Total.Left = 6.46875F;
			this.txtField4Total.Name = "txtField4Total";
			this.txtField4Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField4Total.Text = "Field4";
			this.txtField4Total.Top = 0.125F;
			this.txtField4Total.Width = 0.84375F;
			// 
			// txtHoursTotal
			// 
			this.txtHoursTotal.Height = 0.1666667F;
			this.txtHoursTotal.Left = 8.270833F;
			this.txtHoursTotal.Name = "txtHoursTotal";
			this.txtHoursTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtHoursTotal.Text = "Field5";
			this.txtHoursTotal.Top = 0.125F;
			this.txtHoursTotal.Width = 0.7291667F;
			// 
			// txtSalaryTotal
			// 
			this.txtSalaryTotal.Height = 0.1666667F;
			this.txtSalaryTotal.Left = 9.03125F;
			this.txtSalaryTotal.Name = "txtSalaryTotal";
			this.txtSalaryTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSalaryTotal.Text = "Field6";
			this.txtSalaryTotal.Top = 0.125F;
			this.txtSalaryTotal.Width = 0.84375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.8125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label11.Text = "Totals";
			this.Label11.Top = 0.125F;
			this.Label11.Width = 0.59375F;
			// 
			// txtSubtotalTotal
			// 
			this.txtSubtotalTotal.Height = 0.1666667F;
			this.txtSubtotalTotal.Left = 7.375F;
			this.txtSubtotalTotal.Name = "txtSubtotalTotal";
			this.txtSubtotalTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSubtotalTotal.Text = "Field4";
			this.txtSubtotalTotal.Top = 0.125F;
			this.txtSubtotalTotal.Width = 0.84375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.09375F;
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 9.8125F;
			this.Line2.X1 = 0.09375F;
			this.Line2.X2 = 9.90625F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// rptRetirementDeductionReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblField4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField1Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField4Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHoursTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalaryTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubtotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField1Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField2Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField3Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField4Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHoursTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalaryTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubtotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblField1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblField2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblField3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblField4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
