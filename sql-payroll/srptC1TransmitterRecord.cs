﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1TransmitterRecord.
	/// </summary>
	public partial class srptC1TransmitterRecord : FCSectionReport
	{
		public srptC1TransmitterRecord()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1TransmitterRecord InstancePtr
		{
			get
			{
				return (srptC1TransmitterRecord)Sys.GetInstance(typeof(srptC1TransmitterRecord));
			}
		}

		protected srptC1TransmitterRecord _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1TransmitterRecord	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strRec;
			string strTemp;
			strRec = FCConvert.ToString(this.UserData);
			strTemp = Strings.Mid(strRec, 2, 4);
			txtYearCovered.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 6, 9);
			txtFedID.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 24, 50);
			txtCompany.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 74, 40);
			txtAddress.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strRec, 114, 25)) + ", " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 139, 2)) + "  " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 154, 10));
			txtCityStateZip.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strRec, 164, 30));
			txtTitle.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 194, 10);
			strTemp = "(" + Strings.Mid(strRec, 194, 3) + ")" + Strings.Mid(strRec, 197, 3) + "-" + Strings.Mid(strRec, 200, 4);
			if (fecherFoundation.Strings.Trim(Strings.Mid(strRec, 204, 4)) != string.Empty)
			{
				strTemp += " Ext. " + Strings.Mid(strRec, 204, 4);
			}
			txtPhone.Text = strTemp;
		}

		
	}
}
