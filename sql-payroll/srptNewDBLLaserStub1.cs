﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptNewDBLLaserStub1.
	/// </summary>
	public partial class srptNewDBLLaserStub1 : FCSectionReport
	{
		public srptNewDBLLaserStub1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptNewDBLLaserStub1 InstancePtr
		{
			get
			{
				return (srptNewDBLLaserStub1)Sys.GetInstance(typeof(srptNewDBLLaserStub1));
			}
		}

		protected srptNewDBLLaserStub1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptNewDBLLaserStub1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTMAXSTUBLINES = 23;
		private int intMaxPayLine;
		private int intMaxDedLine;
		private bool boolTestPrint;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			CreateDynamicControls();
		}

		private void CreateDynamicControls()
		{
			GrapeCity.ActiveReports.SectionReportModel.Label ctl;
			int CRow = 0;
			int x;
			float lngStartTop;
			string strTemp = "";
			lngStartTop = txtPayDesc1.Top;
			intMaxPayLine = 1;
			intMaxDedLine = 1;
			// get the max number of pay lines and the max deduction lines
			// there won't be more since the parent report has already taken into
			// account what cnstmaxstublines is.  This can be changed to put the burden
			// on the stub but would have to be done to both stub sub reports.
			for (x = 1; x <= CNSTMAXSTUBLINES; x++)
			{
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 2)) != string.Empty)
				{
					intMaxPayLine = x + 1;
				}
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 4)) != string.Empty)
				{
					intMaxDedLine = x + 1;
				}
			}
			// x
			// create the boxes for the pay info
			for (x = 2; x <= intMaxPayLine; x++)
			{
				CRow = x;
                strTemp = "txtPayDesc" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtPayDesc1.Width;
				ctl.Left = txtPayDesc1.Left;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				ctl.Height = txtPayDesc1.Height;
				//strTemp = "txtPayDesc" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtPayDesc1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);

                strTemp = "txtHours" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtHours1.Width;
				ctl.Left = txtHours1.Left;
				ctl.Height = txtHours1.Height;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				//strTemp = "txtHours" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtHours1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);

                strTemp = "txtPayAmount" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtPayAmount1.Width;
				ctl.Left = txtPayAmount1.Left;
				ctl.Height = txtPayAmount1.Height;
				ctl.Alignment = txtPayAmount1.Alignment;
				//strTemp = "txtPayAmount" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtPayAmount1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);
			}
			// x
			// put this stuff just below the last one we created
			// lblTotalPay.Top = (240 * intMaxPayLine) + lngStartTop
			// txtTotalAmount.Top = lblTotalPay.Top
			// txtTotalHours.Top = lblTotalPay.Top
			// create the boxes for the deduction info
			lngStartTop = txtDedDesc1.Top;
			for (x = 2; x <= intMaxDedLine; x++)
			{
				CRow = x;
                strTemp = "txtDedDesc" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedDesc1.Width;
				ctl.Left = txtDedDesc1.Left;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				ctl.Height = txtDedDesc1.Height;
				//strTemp = "txtDedDesc" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedDesc1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);

                strTemp = "txtDedAmount" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedAmount1.Width;
				ctl.Left = txtDedAmount1.Left;
				ctl.Height = txtDedAmount1.Height;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				//strTemp = "txtDedAmount" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedAmount1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);

                strTemp = "txtDedYTD" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedYTD1.Width;
				ctl.Left = txtDedYTD1.Left;
				ctl.Height = txtDedYTD1.Height;
				ctl.Alignment = txtDedYTD1.Alignment;
				//strTemp = "txtDedYTD" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedYTD1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// fill textboxes from grid
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			int x;
			if (!boolTestPrint)
			{
				lblCheckMessage.Text = modGlobalVariables.Statics.gstrCheckMessage;
				for (x = 0; x <= intMaxPayLine - 1; x++)
				{
					(Detail.Controls["txtPayDesc" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 0);
					(Detail.Controls["txtHours" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 1);
					(Detail.Controls["txtPayAmount" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 2);
				}
				// x
				for (x = 0; x <= intMaxDedLine - 1; x++)
				{
					(Detail.Controls["txtDedDesc" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 3);
					(Detail.Controls["txtDedAmount" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 4);
					(Detail.Controls["txtDedYTD" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 5);
				}
				// x
				// For x = 1 To Detail.Controls.Count
				// 
				// Next x
				dblDirectDepChk = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 29));
				dblDirectDepSav = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 34));
				dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
				txtDate.Text = "DATE " + Strings.Format(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 24), "MM/dd/yyyy");
				txtCurrentGross.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 5);
				txtCurrentFed.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 6);
				txtCurrentFica.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 7);
				txtCurrentState.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 8);
				txtCurrentDeductions.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 23);
				txtCurrentNet.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9);
				txtVacationBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 0);
				txtSickBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 1);
				txtOtherBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 33);
				// txtYTDGross.Caption = .TextMatrix(0, 13)
				txtYTDFed.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 14);
				txtYTDFICA.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 15);
				txtYTDState.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 16);
				// txtYTDNet.Caption = .TextMatrix(0, 17)
				txtTotalHours.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 3);
				txtTotalAmount.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 4);
				txtEMatchCurrent.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 25);
				txtEmatchYTD.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 26);
				txtEmployeeNo.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 2);
				txtName.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 18);
				txtCheckNo.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12);
				if (dblDirectDeposit > 0)
				{
					txtDirectDeposit.Visible = true;
					txtDirectDepositLabel.Visible = true;
					txtChkAmount.Visible = true;
					lblCheckAmount.Visible = true;
					txtDirectDepChkLabel.Visible = true;
					txtDirectDepositSavLabel.Visible = true;
					txtDirectDepSav.Visible = true;
					txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
					txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
					txtChkAmount.Text = Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)) - dblDirectDeposit, "0.00");
				}
				else
				{
					txtDirectDeposit.Visible = false;
					txtDirectDepositLabel.Visible = false;
					txtChkAmount.Visible = false;
					lblCheckAmount.Visible = false;
					txtChkAmount.Text = Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)), "0.00");
				}
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 31)) == string.Empty)
				{
					lblPayRate.Visible = false;
					txtPayRate.Visible = false;
				}
				else
				{
					lblPayRate.Visible = true;
					txtPayRate.Visible = true;
					txtPayRate.Text = fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 31));
				}
				// MATTHEW 8/18/2004
				// ADD THE THREE NEW 'OTHER' VACATION/SICK CODES
				lblCode1.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 39);
				lblCode2.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 40);
				lblCode3.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 41);
				// WE DO NOT WANT THE OTHER FIELDS TO SHOW IF THERE
				// WERE NO TYPES DEFINED BY THE USER.
				if (lblCode1.Text != string.Empty)
				{
					lblCode1Balance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 36);
				}
				else
				{
					lblCode1Balance.Text = string.Empty;
				}
				if (lblCode2.Text != string.Empty)
				{
					lblCode2Balance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 37);
				}
				else
				{
					lblCode2Balance.Text = string.Empty;
				}
				if (lblCode3.Text != string.Empty)
				{
					lblCode3Balance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 38);
				}
				else
				{
					lblCode3Balance.Text = string.Empty;
				}
			}
		}

		
	}
}
