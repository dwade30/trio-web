﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsLeaveRecord
	{
		//=========================================================
		private bool boolUnused;
		private string strName = string.Empty;
		private string strID = string.Empty;
		private string strSSN = string.Empty;
		private string strHomeWG1 = string.Empty;
		private string strHomeWG2 = string.Empty;
		private string strHomeWG3 = string.Empty;
		private double dblHomeRate;
		private string strPayClass = string.Empty;
		private string strPayDes = string.Empty;
		private double dblHours;
		private double dblDollars;
		private double dblEffectiveRate;
		private string strWorkedWG1 = string.Empty;
		private string strWorkedWG2 = string.Empty;
		private string strWorkedWG3 = string.Empty;
		private string strStartDate = string.Empty;
		private string strEndDate = string.Empty;
		private string strWG1Code = string.Empty;
		private string strWG2Code = string.Empty;
		private string strWG3Code = string.Empty;
		private string strWorkedWG1Code = string.Empty;
		private string strWorkedWG2Code = string.Empty;
		private string strWorkedWG3Code = string.Empty;

		public string WorkedWG3Code
		{
			set
			{
				strWorkedWG3Code = value;
			}
			get
			{
				string WorkedWG3Code = "";
				WorkedWG3Code = strWorkedWG3Code;
				return WorkedWG3Code;
			}
		}

		public string WorkedWG2Code
		{
			set
			{
				strWorkedWG2Code = value;
			}
			get
			{
				string WorkedWG2Code = "";
				WorkedWG2Code = strWorkedWG2Code;
				return WorkedWG2Code;
			}
		}

		public string WorkedWG1Code
		{
			set
			{
				strWorkedWG1Code = value;
			}
			get
			{
				string WorkedWG1Code = "";
				WorkedWG1Code = strWorkedWG1Code;
				return WorkedWG1Code;
			}
		}

		public string WG3Code
		{
			set
			{
				strWG3Code = value;
			}
			get
			{
				string WG3Code = "";
				WG3Code = strWG3Code;
				return WG3Code;
			}
		}

		public string WG2Code
		{
			set
			{
				strWG2Code = value;
			}
			get
			{
				string WG2Code = "";
				WG2Code = strWG2Code;
				return WG2Code;
			}
		}

		public string WG1Code
		{
			set
			{
				strWG1Code = value;
			}
			get
			{
				string WG1Code = "";
				WG1Code = strWG1Code;
				return WG1Code;
			}
		}

		public string EndDate
		{
			set
			{
				strEndDate = value;
			}
			get
			{
				string EndDate = "";
				EndDate = strEndDate;
				return EndDate;
			}
		}

		public string StartDate
		{
			set
			{
				strStartDate = value;
			}
			get
			{
				string StartDate = "";
				StartDate = strStartDate;
				return StartDate;
			}
		}

		public string WorkedWG3
		{
			set
			{
				strWorkedWG3 = value;
			}
			get
			{
				string WorkedWG3 = "";
				WorkedWG3 = strWorkedWG3;
				return WorkedWG3;
			}
		}

		public string WorkedWG2
		{
			set
			{
				strWorkedWG2 = value;
			}
			get
			{
				string WorkedWG2 = "";
				WorkedWG2 = strWorkedWG2;
				return WorkedWG2;
			}
		}

		public string WorkedWG1
		{
			set
			{
				strWorkedWG1 = value;
			}
			get
			{
				string WorkedWG1 = "";
				WorkedWG1 = strWorkedWG1;
				return WorkedWG1;
			}
		}

		public double EffectiveRate
		{
			set
			{
				dblEffectiveRate = value;
			}
			get
			{
				double EffectiveRate = 0;
				EffectiveRate = dblEffectiveRate;
				return EffectiveRate;
			}
		}

		public double Dollars
		{
			set
			{
				dblDollars = value;
			}
			get
			{
				double Dollars = 0;
				Dollars = dblDollars;
				return Dollars;
			}
		}

		public double Hours
		{
			set
			{
				dblHours = value;
			}
			get
			{
				double Hours = 0;
				Hours = dblHours;
				return Hours;
			}
		}

		public string PayDes
		{
			set
			{
				strPayDes = value;
			}
			get
			{
				string PayDes = "";
				PayDes = strPayDes;
				return PayDes;
			}
		}

		public string PayClass
		{
			set
			{
				strPayClass = value;
			}
			get
			{
				string PayClass = "";
				PayClass = strPayClass;
				return PayClass;
			}
		}

		public double HomeRate
		{
			set
			{
				dblHomeRate = value;
			}
			get
			{
				double HomeRate = 0;
				HomeRate = dblHomeRate;
				return HomeRate;
			}
		}

		public string HomeWG3
		{
			set
			{
				strHomeWG3 = value;
			}
			get
			{
				string HomeWG3 = "";
				HomeWG3 = strHomeWG3;
				return HomeWG3;
			}
		}

		public string HomeWG2
		{
			set
			{
				strHomeWG2 = value;
			}
			get
			{
				string HomeWG2 = "";
				HomeWG2 = strHomeWG2;
				return HomeWG2;
			}
		}

		public string HomeWG1
		{
			set
			{
				strHomeWG1 = value;
			}
			get
			{
				string HomeWG1 = "";
				HomeWG1 = strHomeWG1;
				return HomeWG1;
			}
		}

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public string EmpName
		{
			set
			{
				strName = value;
			}
			get
			{
				string EmpName = "";
				EmpName = strName;
				return EmpName;
			}
		}

		public string ID
		{
			set
			{
				strID = value;
			}
			get
			{
				string ID = "";
				ID = strID;
				return ID;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}
	}
}
