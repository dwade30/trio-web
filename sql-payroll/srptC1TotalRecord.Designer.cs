﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1TotalRecord.
	/// </summary>
	partial class srptC1TotalRecord
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1TotalRecord));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVoucherPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtM1Employees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM2Employees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM3Employees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM1Females = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM2Females = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM3Females = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUCDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoucherPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM1Employees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM2Employees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM3Employees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM1Females)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM2Females)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM3Females)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label3,
				this.Label4,
				this.txtUGross,
				this.txtExcess,
				this.Label7,
				this.txtTaxable,
				this.Label8,
				this.Label9,
				this.txtStateGross,
				this.txtStateTax,
				this.Label10,
				this.txtVoucherPayments,
				this.Label11,
				this.txtStateTaxDue,
				this.Label12,
				this.txtTotalDue,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.txtM1Employees,
				this.txtM2Employees,
				this.txtM3Employees,
				this.txtM1Females,
				this.txtM2Females,
				this.txtM3Females,
				this.Label18,
				this.txtUCDue
			});
			this.Detail.Height = 1.520833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1770833F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: center";
			this.Label1.Text = "Total Record";
			this.Label1.Top = 0.02083333F;
			this.Label1.Width = 3.916667F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Unemployment Gross";
			this.Label3.Top = 0.28125F;
			this.Label3.Width = 1.65625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Excess";
			this.Label4.Top = 0.4375F;
			this.Label4.Width = 1.15625F;
			// 
			// txtUGross
			// 
			this.txtUGross.Height = 0.1666667F;
			this.txtUGross.Left = 1.822917F;
			this.txtUGross.Name = "txtUGross";
			this.txtUGross.Style = "text-align: right";
			this.txtUGross.Text = null;
			this.txtUGross.Top = 0.28125F;
			this.txtUGross.Width = 1.177083F;
			// 
			// txtExcess
			// 
			this.txtExcess.Height = 0.1666667F;
			this.txtExcess.Left = 1.822917F;
			this.txtExcess.Name = "txtExcess";
			this.txtExcess.Style = "text-align: right";
			this.txtExcess.Text = null;
			this.txtExcess.Top = 0.4375F;
			this.txtExcess.Width = 1.177083F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Taxable";
			this.Label7.Top = 0.59375F;
			this.Label7.Width = 1.15625F;
			// 
			// txtTaxable
			// 
			this.txtTaxable.Height = 0.1666667F;
			this.txtTaxable.Left = 1.822917F;
			this.txtTaxable.Name = "txtTaxable";
			this.txtTaxable.Style = "text-align: right";
			this.txtTaxable.Text = null;
			this.txtTaxable.Top = 0.59375F;
			this.txtTaxable.Width = 1.177083F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "State Gross";
			this.Label8.Top = 0.28125F;
			this.Label8.Width = 1.46875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "State Tax WH";
			this.Label9.Top = 0.4375F;
			this.Label9.Width = 1.15625F;
			// 
			// txtStateGross
			// 
			this.txtStateGross.Height = 0.1666667F;
			this.txtStateGross.Left = 4.635417F;
			this.txtStateGross.Name = "txtStateGross";
			this.txtStateGross.Style = "text-align: right";
			this.txtStateGross.Text = null;
			this.txtStateGross.Top = 0.28125F;
			this.txtStateGross.Width = 0.9895833F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.1666667F;
			this.txtStateTax.Left = 4.635417F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "text-align: right";
			this.txtStateTax.Text = null;
			this.txtStateTax.Top = 0.4375F;
			this.txtStateTax.Width = 0.9895833F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Voucher Payments";
			this.Label10.Top = 0.59375F;
			this.Label10.Width = 1.46875F;
			// 
			// txtVoucherPayments
			// 
			this.txtVoucherPayments.Height = 0.1666667F;
			this.txtVoucherPayments.Left = 4.635417F;
			this.txtVoucherPayments.Name = "txtVoucherPayments";
			this.txtVoucherPayments.Style = "text-align: right";
			this.txtVoucherPayments.Text = null;
			this.txtVoucherPayments.Top = 0.59375F;
			this.txtVoucherPayments.Width = 0.9895833F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.0625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "State Tax Due";
			this.Label11.Top = 0.7395833F;
			this.Label11.Width = 1.46875F;
			// 
			// txtStateTaxDue
			// 
			this.txtStateTaxDue.Height = 0.1666667F;
			this.txtStateTaxDue.Left = 4.635417F;
			this.txtStateTaxDue.Name = "txtStateTaxDue";
			this.txtStateTaxDue.Style = "text-align: right";
			this.txtStateTaxDue.Text = null;
			this.txtStateTaxDue.Top = 0.7395833F;
			this.txtStateTaxDue.Width = 0.9895833F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Text = "Total Due";
			this.Label12.Top = 0.7395833F;
			this.Label12.Width = 0.90625F;
			// 
			// txtTotalDue
			// 
			this.txtTotalDue.Height = 0.1666667F;
			this.txtTotalDue.Left = 6.635417F;
			this.txtTotalDue.Name = "txtTotalDue";
			this.txtTotalDue.Style = "text-align: right";
			this.txtTotalDue.Text = null;
			this.txtTotalDue.Top = 0.7395833F;
			this.txtTotalDue.Width = 0.8020833F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.25F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Text = "Month 1";
			this.Label13.Top = 0.96875F;
			this.Label13.Width = 0.71875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.010417F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Text = "Month 2";
			this.Label14.Top = 0.96875F;
			this.Label14.Width = 0.71875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.822917F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Text = "Month 3";
			this.Label15.Top = 0.96875F;
			this.Label15.Width = 0.71875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.0625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold";
			this.Label16.Text = "Employees";
			this.Label16.Top = 1.125F;
			this.Label16.Width = 0.90625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.0625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold";
			this.Label17.Text = "Females";
			this.Label17.Top = 1.291667F;
			this.Label17.Width = 0.90625F;
			// 
			// txtM1Employees
			// 
			this.txtM1Employees.Height = 0.1666667F;
			this.txtM1Employees.Left = 1.25F;
			this.txtM1Employees.Name = "txtM1Employees";
			this.txtM1Employees.Style = "text-align: right";
			this.txtM1Employees.Text = null;
			this.txtM1Employees.Top = 1.125F;
			this.txtM1Employees.Width = 0.71875F;
			// 
			// txtM2Employees
			// 
			this.txtM2Employees.Height = 0.1666667F;
			this.txtM2Employees.Left = 2.010417F;
			this.txtM2Employees.Name = "txtM2Employees";
			this.txtM2Employees.Style = "text-align: right";
			this.txtM2Employees.Text = null;
			this.txtM2Employees.Top = 1.125F;
			this.txtM2Employees.Width = 0.71875F;
			// 
			// txtM3Employees
			// 
			this.txtM3Employees.Height = 0.1666667F;
			this.txtM3Employees.Left = 2.822917F;
			this.txtM3Employees.Name = "txtM3Employees";
			this.txtM3Employees.Style = "text-align: right";
			this.txtM3Employees.Text = null;
			this.txtM3Employees.Top = 1.125F;
			this.txtM3Employees.Width = 0.71875F;
			// 
			// txtM1Females
			// 
			this.txtM1Females.Height = 0.1666667F;
			this.txtM1Females.Left = 1.25F;
			this.txtM1Females.Name = "txtM1Females";
			this.txtM1Females.Style = "text-align: right";
			this.txtM1Females.Text = null;
			this.txtM1Females.Top = 1.291667F;
			this.txtM1Females.Width = 0.71875F;
			// 
			// txtM2Females
			// 
			this.txtM2Females.Height = 0.1666667F;
			this.txtM2Females.Left = 2.010417F;
			this.txtM2Females.Name = "txtM2Females";
			this.txtM2Females.Style = "text-align: right";
			this.txtM2Females.Text = null;
			this.txtM2Females.Top = 1.291667F;
			this.txtM2Females.Width = 0.71875F;
			// 
			// txtM3Females
			// 
			this.txtM3Females.Height = 0.1666667F;
			this.txtM3Females.Left = 2.822917F;
			this.txtM3Females.Name = "txtM3Females";
			this.txtM3Females.Style = "text-align: right";
			this.txtM3Females.Text = null;
			this.txtM3Females.Top = 1.291667F;
			this.txtM3Females.Width = 0.71875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.0625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold";
			this.Label18.Text = "Total UC Due";
			this.Label18.Top = 0.7395833F;
			this.Label18.Width = 1.15625F;
			// 
			// txtUCDue
			// 
			this.txtUCDue.Height = 0.1666667F;
			this.txtUCDue.Left = 1.822917F;
			this.txtUCDue.Name = "txtUCDue";
			this.txtUCDue.Style = "text-align: right";
			this.txtUCDue.Text = null;
			this.txtUCDue.Top = 0.7395833F;
			this.txtUCDue.Width = 1.177083F;
			// 
			// srptC1TotalRecord
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoucherPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM1Employees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM2Employees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM3Employees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM1Females)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM2Females)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM3Females)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcess;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVoucherPayments;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM1Employees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM2Employees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM3Employees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM1Females;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM2Females;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM3Females;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCDue;
	}
}
