﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmDistributionBaseRateUpdate.
	/// </summary>
	partial class frmDistributionBaseRateUpdate
	{
		public fecherFoundation.FCComboBox cmbtion1;
		public fecherFoundation.FCTextBox txtNewRate;
		public fecherFoundation.FCFrame fraWarning;
		public fecherFoundation.FCLabel lblWarning;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCurrentRate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblPercentage;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCLabel lblEmployee;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbtion1 = new fecherFoundation.FCComboBox();
            this.txtNewRate = new fecherFoundation.FCTextBox();
            this.fraWarning = new fecherFoundation.FCFrame();
            this.lblWarning = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtCurrentRate = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblPercentage = new fecherFoundation.FCLabel();
            this.lblType = new fecherFoundation.FCLabel();
            this.lblEmployee = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWarning)).BeginInit();
            this.fraWarning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 440);
            this.BottomPanel.Size = new System.Drawing.Size(645, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraWarning);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lblEmployee);
            this.ClientArea.Size = new System.Drawing.Size(645, 380);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(645, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(436, 30);
            this.HeaderText.Text = "Multiple Distribution Base Rate Update";
            // 
            // cmbtion1
            // 
            this.cmbtion1.AutoSize = false;
            this.cmbtion1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbtion1.FormattingEnabled = true;
            this.cmbtion1.Items.AddRange(new object[] {
            "New Amount",
            "Percentage Adjustment"});
            this.cmbtion1.Location = new System.Drawing.Point(20, 30);
            this.cmbtion1.Name = "cmbtion1";
            this.cmbtion1.Size = new System.Drawing.Size(241, 40);
            this.cmbtion1.TabIndex = 3;
			this.cmbtion1.Text = "New Amount";
			this.cmbtion1.SelectedIndexChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // txtNewRate
            // 
            this.txtNewRate.AutoSize = false;
            this.txtNewRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtNewRate.LinkItem = null;
            this.txtNewRate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtNewRate.LinkTopic = null;
            this.txtNewRate.Location = new System.Drawing.Point(200, 119);
            this.txtNewRate.Name = "txtNewRate";
            this.txtNewRate.Size = new System.Drawing.Size(160, 40);
            this.txtNewRate.TabIndex = 3;
            this.txtNewRate.Enter += new System.EventHandler(this.txtNewRate_Enter);
            // 
            // fraWarning
            // 
            this.fraWarning.Controls.Add(this.lblWarning);
            this.fraWarning.Location = new System.Drawing.Point(30, 30);
            this.fraWarning.Name = "fraWarning";
            this.fraWarning.Size = new System.Drawing.Size(587, 79);
            this.fraWarning.TabIndex = 4;
            this.fraWarning.Text = "Warning!!!!";
            // 
            // lblWarning
            // 
            this.lblWarning.Location = new System.Drawing.Point(20, 30);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(547, 29);
            this.lblWarning.TabIndex = 5;
            this.lblWarning.Text = "CHANGES USING THIS SCREEN WILL AFFECT ONLY THIS EMPLOYEE BUT WILL EFFECT ALL DIST" +
    "RIBUTIONS WITH THE VALUE AS THE CURRENT RATE REGARDLESS OF THE PAY CATEGORY";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtNewRate);
            this.Frame1.Controls.Add(this.txtCurrentRate);
            this.Frame1.Controls.Add(this.cmbtion1);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.lblPercentage);
            this.Frame1.Controls.Add(this.lblType);
            this.Frame1.Location = new System.Drawing.Point(30, 158);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(380, 214);
            this.Frame1.TabIndex = 7;
            this.Frame1.Text = "Type Of Adjustment";
            // 
            // txtCurrentRate
            // 
            this.txtCurrentRate.AutoSize = false;
            this.txtCurrentRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrentRate.LinkItem = null;
            this.txtCurrentRate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCurrentRate.LinkTopic = null;
            this.txtCurrentRate.Location = new System.Drawing.Point(20, 119);
            this.txtCurrentRate.Name = "txtCurrentRate";
            this.txtCurrentRate.Size = new System.Drawing.Size(160, 40);
            this.txtCurrentRate.TabIndex = 2;
            this.txtCurrentRate.Enter += new System.EventHandler(this.txtCurrentRate_Enter);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(115, 15);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "CURRENT RATE";
            // 
            // lblPercentage
            // 
            this.lblPercentage.Location = new System.Drawing.Point(200, 179);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(138, 15);
            this.lblPercentage.TabIndex = 9;
            this.lblPercentage.Text = "FOR 3 % ENTER 3.00";
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(200, 90);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(80, 15);
            this.lblType.TabIndex = 8;
            this.lblType.Text = "NEW RATE";
            // 
            // lblEmployee
            // 
            this.lblEmployee.Location = new System.Drawing.Point(30, 129);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(397, 15);
            this.lblEmployee.TabIndex = 6;
            this.lblEmployee.Tag = "Employee #";
            this.lblEmployee.Text = "EMPLOYEE #";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(268, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(82, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmDistributionBaseRateUpdate
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(645, 548);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmDistributionBaseRateUpdate";
            this.Text = "Multiple Distribution Base Rate Update";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDistributionBaseRateUpdate_Load);
            this.Activated += new System.EventHandler(this.frmDistributionBaseRateUpdate_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDistributionBaseRateUpdate_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWarning)).EndInit();
            this.fraWarning.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSave;
    }
}
