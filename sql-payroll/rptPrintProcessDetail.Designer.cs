﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPrintProcessDetail.
	/// </summary>
	partial class rptPrintProcessDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPrintProcessDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRow4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtRow1,
				this.txtRow2,
				this.txtRow3,
				this.txtRow5,
				this.txtRow6,
				this.txtRow7,
				this.txtRow8,
				this.txtRow9,
				this.txtRow10,
				this.txtRow11,
				this.txtRow4
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field2
			});
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.txtPage
			});
			this.PageHeader.Height = 0.2291667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field2
			// 
			this.Field2.Height = 0.21875F;
			this.Field2.Left = 0F;
			this.Field2.Name = "Field2";
			this.Field2.OutputFormat = resources.GetString("Field2.OutputFormat");
			this.Field2.Style = "font-weight: bold; text-align: center";
			this.Field2.Text = "Payroll - Print Process Detail";
			this.Field2.Top = 0F;
			this.Field2.Width = 9.9375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 0.03125F;
			this.Field1.Name = "Field1";
			this.Field1.OutputFormat = resources.GetString("Field1.OutputFormat");
			this.Field1.Text = "Field1";
			this.Field1.Top = 0F;
			this.Field1.Width = 2.5625F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 7.40625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.OutputFormat = resources.GetString("txtPage.OutputFormat");
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0F;
			this.txtPage.Width = 2.5625F;
			// 
			// txtRow1
			// 
			this.txtRow1.Height = 0.1666667F;
			this.txtRow1.Left = 0F;
			this.txtRow1.Name = "txtRow1";
			this.txtRow1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtRow1.Text = "Employee";
			this.txtRow1.Top = 0F;
			this.txtRow1.Width = 1.34375F;
			// 
			// txtRow2
			// 
			this.txtRow2.Height = 0.1666667F;
			this.txtRow2.Left = 1.34375F;
			this.txtRow2.Name = "txtRow2";
			this.txtRow2.OutputFormat = resources.GetString("txtRow2.OutputFormat");
			this.txtRow2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow2.Text = null;
			this.txtRow2.Top = 0F;
			this.txtRow2.Width = 0.59375F;
			// 
			// txtRow3
			// 
			this.txtRow3.Height = 0.1666667F;
			this.txtRow3.Left = 1.9375F;
			this.txtRow3.Name = "txtRow3";
			this.txtRow3.OutputFormat = resources.GetString("txtRow3.OutputFormat");
			this.txtRow3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow3.Text = null;
			this.txtRow3.Top = 0F;
			this.txtRow3.Width = 1.0625F;
			// 
			// txtRow5
			// 
			this.txtRow5.Height = 0.1666667F;
			this.txtRow5.Left = 4.03125F;
			this.txtRow5.Name = "txtRow5";
			this.txtRow5.OutputFormat = resources.GetString("txtRow5.OutputFormat");
			this.txtRow5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow5.Text = null;
			this.txtRow5.Top = 0F;
			this.txtRow5.Width = 0.90625F;
			// 
			// txtRow6
			// 
			this.txtRow6.Height = 0.1666667F;
			this.txtRow6.Left = 4.90625F;
			this.txtRow6.Name = "txtRow6";
			this.txtRow6.OutputFormat = resources.GetString("txtRow6.OutputFormat");
			this.txtRow6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow6.Text = null;
			this.txtRow6.Top = 0F;
			this.txtRow6.Width = 0.90625F;
			// 
			// txtRow7
			// 
			this.txtRow7.Height = 0.1666667F;
			this.txtRow7.Left = 5.8125F;
			this.txtRow7.Name = "txtRow7";
			this.txtRow7.OutputFormat = resources.GetString("txtRow7.OutputFormat");
			this.txtRow7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow7.Text = null;
			this.txtRow7.Top = 0F;
			this.txtRow7.Width = 0.90625F;
			// 
			// txtRow8
			// 
			this.txtRow8.Height = 0.1666667F;
			this.txtRow8.Left = 6.71875F;
			this.txtRow8.Name = "txtRow8";
			this.txtRow8.OutputFormat = resources.GetString("txtRow8.OutputFormat");
			this.txtRow8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow8.Text = null;
			this.txtRow8.Top = 0F;
			this.txtRow8.Width = 0.90625F;
			// 
			// txtRow9
			// 
			this.txtRow9.Height = 0.1666667F;
			this.txtRow9.Left = 7.65625F;
			this.txtRow9.Name = "txtRow9";
			this.txtRow9.OutputFormat = resources.GetString("txtRow9.OutputFormat");
			this.txtRow9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow9.Text = null;
			this.txtRow9.Top = 0F;
			this.txtRow9.Width = 1.03125F;
			// 
			// txtRow10
			// 
			this.txtRow10.Height = 0.1666667F;
			this.txtRow10.Left = 8.6875F;
			this.txtRow10.Name = "txtRow10";
			this.txtRow10.OutputFormat = resources.GetString("txtRow10.OutputFormat");
			this.txtRow10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow10.Text = null;
			this.txtRow10.Top = 0F;
			this.txtRow10.Width = 0.65625F;
			// 
			// txtRow11
			// 
			this.txtRow11.Height = 0.1666667F;
			this.txtRow11.Left = 9.34375F;
			this.txtRow11.Name = "txtRow11";
			this.txtRow11.OutputFormat = resources.GetString("txtRow11.OutputFormat");
			this.txtRow11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow11.Text = null;
			this.txtRow11.Top = 0F;
			this.txtRow11.Width = 0.625F;
			// 
			// txtRow4
			// 
			this.txtRow4.Height = 0.1666667F;
			this.txtRow4.Left = 3F;
			this.txtRow4.Name = "txtRow4";
			this.txtRow4.OutputFormat = resources.GetString("txtRow4.OutputFormat");
			this.txtRow4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtRow4.Text = null;
			this.txtRow4.Top = 0F;
			this.txtRow4.Width = 1.03125F;
			// 
			// rptPrintProcessDetail
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRow4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRow4;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
