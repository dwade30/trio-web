﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmDropdown : BaseForm
	{
		public frmDropdown()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDropdown InstancePtr
		{
			get
			{
				return (frmDropdown)Sys.GetInstance(typeof(frmDropdown));
			}
		}

		protected frmDropdown _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int lngReturn;

		public int Init(ref string strOptions, int lngDefaultOption = 0, string strCaption = "", string strLabel = "")
		{
			int Init = 0;
			// pass it a list that will create a combo with id's or the combo choices must be whole numbers
			Label1.Text = strLabel;
			lngReturn = 0;
			Init = 0;
			this.Text = strCaption;
			Grid.ColComboList(0, strOptions);
			Grid.AutoSize(0, -1, false, 50);
			Grid.TextMatrix(0, 0, FCConvert.ToString(lngDefaultOption));
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void frmDropdown_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(0, 0))));
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmDropdown_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDropdown properties;
			//frmDropdown.FillStyle	= 0;
			//frmDropdown.ScaleWidth	= 3885;
			//frmDropdown.ScaleHeight	= 2235;
			//frmDropdown.LinkTopic	= "Form2";
			//frmDropdown.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmDropdown_Resize(object sender, System.EventArgs e)
		{
			Grid.AutoSize(0, -1, false, 5);
			//Grid.ColWidth(0) *= 2;
			Grid.Width = Grid.ColWidth(0) + 60;
			Grid.Left = FCConvert.ToInt32((this.WidthOriginal - Grid.WidthOriginal) / 2.0);
			//Grid.Height = Grid.RowHeight(0) + 60;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			Grid.Row = -1;
			//App.DoEvents();
			lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(0, 0))));
			Close();
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
