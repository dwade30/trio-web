//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeSearch.
	/// </summary>
	partial class frmEmployeeSearch
	{
		public fecherFoundation.FCGrid vsSearch;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkShowActive;
		public fecherFoundation.FCTextBox txtEmployeeNumber;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtSeqNumber;
		public fecherFoundation.FCButton cmdFind;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCTextBox txtDeptDiv;
		public fecherFoundation.FCButton cmdAddNew;
		public fecherFoundation.FCMaskedTextBox txtSSN;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuSelect;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuFind;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.vsSearch = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkShowActive = new fecherFoundation.FCCheckBox();
            this.txtEmployeeNumber = new fecherFoundation.FCTextBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.txtSeqNumber = new fecherFoundation.FCTextBox();
            this.txtDeptDiv = new fecherFoundation.FCTextBox();
            this.cmdAddNew = new fecherFoundation.FCButton();
            this.txtSSN = new fecherFoundation.FCMaskedTextBox();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.cmdFind = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelect = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFind = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdNew = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFind);
            this.BottomPanel.Location = new System.Drawing.Point(0, 617);
            this.BottomPanel.Size = new System.Drawing.Size(829, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsSearch);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmdSelect);
            this.ClientArea.Size = new System.Drawing.Size(849, 628);
            this.ClientArea.Controls.SetChildIndex(this.cmdSelect, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsSearch, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(849, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(197, 30);
            this.HeaderText.Text = "Select Employee";
            // 
            // vsSearch
            // 
            this.vsSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSearch.Cols = 10;
            this.vsSearch.Location = new System.Drawing.Point(30, 321);
            this.vsSearch.Name = "vsSearch";
            this.vsSearch.Rows = 50;
            this.vsSearch.Size = new System.Drawing.Size(791, 296);
            this.vsSearch.TabIndex = 9;
            this.vsSearch.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsSearch_MouseMoveEvent);
            this.vsSearch.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSearch_ClickEvent);
            this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
            this.vsSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsSearch_KeyPressEvent);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkShowActive);
            this.Frame1.Controls.Add(this.txtEmployeeNumber);
            this.Frame1.Controls.Add(this.txtLastName);
            this.Frame1.Controls.Add(this.txtSeqNumber);
            this.Frame1.Controls.Add(this.txtDeptDiv);
            this.Frame1.Controls.Add(this.cmdAddNew);
            this.Frame1.Controls.Add(this.txtSSN);
            this.Frame1.Controls.Add(this.txtFirstName);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(791, 271);
            this.Frame1.TabIndex = 8;
            this.Frame1.Text = "Search By";
            // 
            // chkShowActive
            // 
            this.chkShowActive.Location = new System.Drawing.Point(357, 157);
            this.chkShowActive.Name = "chkShowActive";
            this.chkShowActive.Size = new System.Drawing.Size(170, 19);
            this.chkShowActive.TabIndex = 19;
            this.chkShowActive.Text = "Active employees only";
            this.chkShowActive.CheckedChanged += new System.EventHandler(this.chkShowActive_CheckedChanged);
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeNumber.Location = new System.Drawing.Point(203, 30);
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Size = new System.Drawing.Size(134, 40);
            this.txtEmployeeNumber.TabIndex = 1;
            this.txtEmployeeNumber.Enter += new System.EventHandler(this.txtEmployeeNumber_Enter);
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.Location = new System.Drawing.Point(203, 90);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(134, 40);
            this.txtLastName.TabIndex = 2;
            // 
            // txtSeqNumber
            // 
            this.txtSeqNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSeqNumber.Location = new System.Drawing.Point(502, 30);
            this.txtSeqNumber.Name = "txtSeqNumber";
            this.txtSeqNumber.Size = new System.Drawing.Size(90, 40);
            this.txtSeqNumber.TabIndex = 5;
            // 
            // txtDeptDiv
            // 
            this.txtDeptDiv.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeptDiv.Location = new System.Drawing.Point(502, 90);
            this.txtDeptDiv.Name = "txtDeptDiv";
            this.txtDeptDiv.Size = new System.Drawing.Size(90, 40);
            this.txtDeptDiv.TabIndex = 6;
            // 
            // cmdAddNew
            // 
            this.cmdAddNew.AppearanceKey = "actionButton";
            this.cmdAddNew.Location = new System.Drawing.Point(612, 30);
            this.cmdAddNew.Name = "cmdAddNew";
            this.cmdAddNew.Size = new System.Drawing.Size(159, 40);
            this.cmdAddNew.TabIndex = 8;
            this.cmdAddNew.Text = "Add Employee";
            this.cmdAddNew.Visible = false;
            this.cmdAddNew.Click += new System.EventHandler(this.cmdAddNew_Click);
            // 
            // txtSSN
            // 
            this.txtSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtSSN.Location = new System.Drawing.Point(203, 210);
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Size = new System.Drawing.Size(134, 22);
            this.txtSSN.TabIndex = 4;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Location = new System.Drawing.Point(203, 150);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(134, 40);
            this.txtFirstName.TabIndex = 3;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(130, 16);
            this.Label2.TabIndex = 18;
            this.Label2.Text = "LAST NAME";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(130, 16);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "EMPLOYEE NUMBER";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(357, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(78, 19);
            this.Label3.TabIndex = 16;
            this.Label3.Text = "SEQ NUMBER";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 224);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(130, 16);
            this.Label5.TabIndex = 15;
            this.Label5.Text = "SOCIAL SECURITY #";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(357, 104);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(78, 19);
            this.Label6.TabIndex = 14;
            this.Label6.Text = "DEPT / DIV";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 164);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(130, 16);
            this.Label4.TabIndex = 13;
            this.Label4.Text = "FIRST NAME";
            // 
            // cmdFind
            // 
            this.cmdFind.AppearanceKey = "acceptButton";
            this.cmdFind.Location = new System.Drawing.Point(362, 30);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFind.Size = new System.Drawing.Size(76, 48);
            this.cmdFind.TabIndex = 6;
            this.cmdFind.Text = "Find";
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(723, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(98, 24);
            this.cmdClear.TabIndex = 7;
            this.cmdClear.Text = "Clear Screen";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.Location = new System.Drawing.Point(653, 371);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(55, 24);
            this.cmdSelect.TabIndex = 11;
            this.cmdSelect.Text = "Select";
            this.cmdSelect.Visible = false;
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = -1;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "New";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = -1;
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuRefresh.Text = "Refresh                               ";
            this.mnuRefresh.Visible = false;
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuSelect
            // 
            this.mnuSelect.Index = -1;
            this.mnuSelect.Name = "mnuSelect";
            this.mnuSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelect.Text = "Select                                   ";
            this.mnuSelect.Visible = false;
            this.mnuSelect.Click += new System.EventHandler(this.mnuSelect_Click);
            // 
            // mnuClear
            // 
            this.mnuClear.Index = -1;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Screen";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 0;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuFind
            // 
            this.mnuFind.Index = -1;
            this.mnuFind.Name = "mnuFind";
            this.mnuFind.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFind.Text = "Find";
            this.mnuFind.Click += new System.EventHandler(this.mnuFind_Click);
            // 
            // mnuSP4
            // 
            this.mnuSP4.Index = -1;
            this.mnuSP4.Name = "mnuSP4";
            this.mnuSP4.Text = "-";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(667, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(50, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // frmEmployeeSearch
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(849, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeeSearch";
            this.Text = "Select Employee";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployeeSearch_Load);
            this.Activated += new System.EventHandler(this.frmEmployeeSearch_Activated);
            this.Resize += new System.EventHandler(this.frmEmployeeSearch_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmployeeSearch_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeeSearch_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdNew;
		private System.ComponentModel.IContainer components;
	}
}