﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptCustomCheckBody.
	/// </summary>
	partial class srptCustomCheckBody
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCustomCheckBody));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAmountString = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddressName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLaserVoid = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStandardVoid1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStandardVoid2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLogo = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.lblLaserVoid2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMICRLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblToTheOrderOf = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVoidAfter90Days = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMemoLine = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignature1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignature2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLargeCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgLogo = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.lblMunicipalName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountString)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddressName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLaserVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStandardVoid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStandardVoid2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLogo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLaserVoid2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMICRLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblToTheOrderOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoidAfter90Days)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMemoLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLargeCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgLogo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmountString,
				this.txtAddressName,
				this.txtAddress1,
				this.txtCityStateZip,
				this.txtEmployeeNumCheck,
				this.txtDate,
				this.txtCheckAmount,
				this.txtAddress2,
				this.lblLaserVoid,
				this.lblStandardVoid1,
				this.lblStandardVoid2,
				this.txtCheckNumber,
				this.Image1,
				this.txtName,
				this.txtLogo,
				this.lblLaserVoid2,
				this.fldMICRLine,
				this.lblPay,
				this.lblToTheOrderOf,
				this.lblVoidAfter90Days,
				this.lblMemoLine,
				this.lblSignature1,
				this.lblSignature2,
				this.fldBankName,
				this.lblAmount,
				this.lblDate,
				this.fldLargeCheckNumber,
				this.lblReturnAddress,
				this.imgLogo,
				this.lblMunicipalName
			});
			this.Detail.Height = 3.40625F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtAmountString
			// 
			this.txtAmountString.Height = 0.3125F;
			this.txtAmountString.HyperLink = null;
			this.txtAmountString.Left = 0.0625F;
			this.txtAmountString.Name = "txtAmountString";
			this.txtAmountString.Style = "";
			this.txtAmountString.Tag = "TEXT";
			this.txtAmountString.Text = null;
			this.txtAmountString.Top = 1F;
			this.txtAmountString.Width = 7.375F;
			// 
			// txtAddressName
			// 
			this.txtAddressName.Height = 0.19F;
			this.txtAddressName.HyperLink = null;
			this.txtAddressName.Left = 0.5F;
			this.txtAddressName.Name = "txtAddressName";
			this.txtAddressName.Style = "";
			this.txtAddressName.Tag = "text";
			this.txtAddressName.Text = "EMPLOYEE";
			this.txtAddressName.Top = 1.84375F;
			this.txtAddressName.Width = 3.25F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.2F;
			this.txtAddress1.HyperLink = null;
			this.txtAddress1.Left = 0.5F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "";
			this.txtAddress1.Tag = "TEXT";
			this.txtAddress1.Text = "TRIOVILLE";
			this.txtAddress1.Top = 2F;
			this.txtAddress1.Width = 4.366667F;
			// 
			// txtCityStateZip
			// 
			this.txtCityStateZip.Height = 0.6333333F;
			this.txtCityStateZip.HyperLink = null;
			this.txtCityStateZip.Left = 0.5F;
			this.txtCityStateZip.Name = "txtCityStateZip";
			this.txtCityStateZip.Style = "";
			this.txtCityStateZip.Tag = "TEXT";
			this.txtCityStateZip.Text = "TRIOVILLE";
			this.txtCityStateZip.Top = 2.333333F;
			this.txtCityStateZip.Width = 4.366667F;
			// 
			// txtEmployeeNumCheck
			// 
			this.txtEmployeeNumCheck.Height = 0.1666667F;
			this.txtEmployeeNumCheck.HyperLink = null;
			this.txtEmployeeNumCheck.Left = 3.8125F;
			this.txtEmployeeNumCheck.Name = "txtEmployeeNumCheck";
			this.txtEmployeeNumCheck.Style = "text-align: right";
			this.txtEmployeeNumCheck.Tag = "TEXT";
			this.txtEmployeeNumCheck.Text = null;
			this.txtEmployeeNumCheck.Top = 1.833333F;
			this.txtEmployeeNumCheck.Width = 0.65625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 4.84375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: left";
			this.txtDate.Tag = "text";
			this.txtDate.Text = null;
			this.txtDate.Top = 1.333333F;
			this.txtDate.Width = 0.96875F;
			// 
			// txtCheckAmount
			// 
			this.txtCheckAmount.Height = 0.19F;
			this.txtCheckAmount.HyperLink = null;
			this.txtCheckAmount.Left = 5.9375F;
			this.txtCheckAmount.Name = "txtCheckAmount";
			this.txtCheckAmount.Style = "text-align: right";
			this.txtCheckAmount.Tag = "text";
			this.txtCheckAmount.Text = "0.00";
			this.txtCheckAmount.Top = 1.34375F;
			this.txtCheckAmount.Width = 1.5F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1333333F;
			this.txtAddress2.HyperLink = null;
			this.txtAddress2.Left = 0.5F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "";
			this.txtAddress2.Tag = "TEXT";
			this.txtAddress2.Text = "TRIOVILLE";
			this.txtAddress2.Top = 2.2F;
			this.txtAddress2.Width = 4.366667F;
			// 
			// lblLaserVoid
			// 
			this.lblLaserVoid.Height = 0.46875F;
			this.lblLaserVoid.HyperLink = null;
			this.lblLaserVoid.Left = 5.6875F;
			this.lblLaserVoid.Name = "lblLaserVoid";
			this.lblLaserVoid.Style = "font-size: 36pt; font-weight: bold; text-align: right";
			this.lblLaserVoid.Text = "VOID";
			this.lblLaserVoid.Top = 2.15625F;
			this.lblLaserVoid.Visible = false;
			this.lblLaserVoid.Width = 1.75F;
			// 
			// lblStandardVoid1
			// 
			this.lblStandardVoid1.Height = 0.19F;
			this.lblStandardVoid1.HyperLink = null;
			this.lblStandardVoid1.Left = 5F;
			this.lblStandardVoid1.Name = "lblStandardVoid1";
			this.lblStandardVoid1.Style = "";
			this.lblStandardVoid1.Tag = "TEXT";
			this.lblStandardVoid1.Text = "*** VOID *** VOID ***";
			this.lblStandardVoid1.Top = 2.34375F;
			this.lblStandardVoid1.Visible = false;
			this.lblStandardVoid1.Width = 2.3125F;
			// 
			// lblStandardVoid2
			// 
			this.lblStandardVoid2.Height = 0.1875F;
			this.lblStandardVoid2.HyperLink = null;
			this.lblStandardVoid2.Left = 5F;
			this.lblStandardVoid2.Name = "lblStandardVoid2";
			this.lblStandardVoid2.Style = "";
			this.lblStandardVoid2.Tag = "TEXT";
			this.lblStandardVoid2.Text = "*** VOID *** VOID ***";
			this.lblStandardVoid2.Top = 2.5F;
			this.lblStandardVoid2.Visible = false;
			this.lblStandardVoid2.Width = 2.3125F;
			// 
			// txtCheckNumber
			// 
			this.txtCheckNumber.Height = 0.19F;
			this.txtCheckNumber.HyperLink = null;
			this.txtCheckNumber.Left = 5F;
			this.txtCheckNumber.Name = "txtCheckNumber";
			this.txtCheckNumber.Style = "text-align: left";
			this.txtCheckNumber.Tag = "text";
			this.txtCheckNumber.Text = null;
			this.txtCheckNumber.Top = 0.1875F;
			this.txtCheckNumber.Visible = false;
			this.txtCheckNumber.Width = 1.3125F;
			// 
			// Image1
			// 
			this.Image1.Height = 0.5416667F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 5F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.Image1.Top = 2.125F;
			this.Image1.Width = 2.4375F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.HyperLink = null;
			this.txtName.Left = 0.5F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "";
			this.txtName.Tag = "text";
			this.txtName.Text = "EMPLOYEE";
			this.txtName.Top = 1.833333F;
			this.txtName.Visible = false;
			this.txtName.Width = 3.25F;
			// 
			// TXTlOGO
			// 
			this.txtLogo.Height = 0.96875F;
			this.txtLogo.HyperLink = null;
			this.txtLogo.ImageData = null;
			this.txtLogo.Left = 0F;
			this.txtLogo.LineWeight = 1F;
			this.txtLogo.Name = "txtLogo";
			this.txtLogo.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
			this.txtLogo.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.txtLogo.Top = 0F;
			this.txtLogo.Visible = false;
			this.txtLogo.Width = 0.96875F;
			// 
			// lblLaserVoid2
			// 
			this.lblLaserVoid2.Height = 0.4583333F;
			this.lblLaserVoid2.HyperLink = null;
			this.lblLaserVoid2.Left = 5.8125F;
			this.lblLaserVoid2.Name = "lblLaserVoid2";
			this.lblLaserVoid2.Style = "font-size: 36pt; font-weight: bold; text-align: right";
			this.lblLaserVoid2.Text = "VOID";
			this.lblLaserVoid2.Top = 0.8333333F;
			this.lblLaserVoid2.Visible = false;
			this.lblLaserVoid2.Width = 1.75F;
			// 
			// fldMICRLine
			// 
			this.fldMICRLine.Height = 0.1875F;
			this.fldMICRLine.Left = 2.09375F;
			this.fldMICRLine.Name = "fldMICRLine";
			this.fldMICRLine.Style = "font-family: \'IDAutomationMICR\'; font-size: 12pt";
			this.fldMICRLine.Tag = "Large";
			this.fldMICRLine.Text = "C123456C A345678909A  12345678901C";
			this.fldMICRLine.Top = 3.15625F;
			this.fldMICRLine.Visible = false;
			this.fldMICRLine.Width = 4.444445F;
			// 
			// lblPay
			// 
			this.lblPay.Height = 0.1875F;
			this.lblPay.HyperLink = null;
			this.lblPay.Left = 0.15625F;
			this.lblPay.Name = "lblPay";
			this.lblPay.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.lblPay.Text = "PAY";
			this.lblPay.Top = 1.15625F;
			this.lblPay.Visible = false;
			this.lblPay.Width = 0.375F;
			// 
			// lblToTheOrderOf
			// 
			this.lblToTheOrderOf.Height = 0.46875F;
			this.lblToTheOrderOf.HyperLink = null;
			this.lblToTheOrderOf.Left = 0.125F;
			this.lblToTheOrderOf.Name = "lblToTheOrderOf";
			this.lblToTheOrderOf.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblToTheOrderOf.Text = "TO THE ORDER OF";
			this.lblToTheOrderOf.Top = 1.5625F;
			this.lblToTheOrderOf.Visible = false;
			this.lblToTheOrderOf.Width = 0.5F;
			// 
			// lblVoidAfter90Days
			// 
			this.lblVoidAfter90Days.Height = 0.19F;
			this.lblVoidAfter90Days.HyperLink = null;
			this.lblVoidAfter90Days.Left = 5.25F;
			this.lblVoidAfter90Days.Name = "lblVoidAfter90Days";
			this.lblVoidAfter90Days.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblVoidAfter90Days.Text = null;
			this.lblVoidAfter90Days.Top = 1.84375F;
			this.lblVoidAfter90Days.Visible = false;
			this.lblVoidAfter90Days.Width = 2.28125F;
			// 
			// lblMemoLine
			// 
			this.lblMemoLine.Height = 0.19F;
			this.lblMemoLine.HyperLink = null;
			this.lblMemoLine.Left = 0.0625F;
			this.lblMemoLine.MultiLine = false;
			this.lblMemoLine.Name = "lblMemoLine";
			this.lblMemoLine.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 1";
			this.lblMemoLine.Text = "FOR________________________________________";
			this.lblMemoLine.Top = 2.90625F;
			this.lblMemoLine.Visible = false;
			this.lblMemoLine.Width = 3.1875F;
			// 
			// lblSignature1
			// 
			this.lblSignature1.Height = 0.19F;
			this.lblSignature1.HyperLink = null;
			this.lblSignature1.Left = 3.8125F;
			this.lblSignature1.MultiLine = false;
			this.lblSignature1.Name = "lblSignature1";
			this.lblSignature1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 1";
			this.lblSignature1.Text = "_________________________________________________________________________________" + "__________";
			this.lblSignature1.Top = 2.65625F;
			this.lblSignature1.Visible = false;
			this.lblSignature1.Width = 3.125F;
			// 
			// lblSignature2
			// 
			this.lblSignature2.Height = 0.19F;
			this.lblSignature2.HyperLink = null;
			this.lblSignature2.Left = 3.8125F;
			this.lblSignature2.MultiLine = false;
			this.lblSignature2.Name = "lblSignature2";
			this.lblSignature2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 1";
			this.lblSignature2.Text = "_________________________________________________________________________";
			this.lblSignature2.Top = 2.84375F;
			this.lblSignature2.Visible = false;
			this.lblSignature2.Width = 3.125F;
			// 
			// fldBankName
			// 
			this.fldBankName.Height = 0.1875F;
			this.fldBankName.Left = 4.21875F;
			this.fldBankName.Name = "fldBankName";
			this.fldBankName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldBankName.Tag = "Large";
			this.fldBankName.Text = null;
			this.fldBankName.Top = 0.3125F;
			this.fldBankName.Visible = false;
			this.fldBankName.Width = 2.375F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.19F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 6.71875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblAmount.Text = "AMOUNT";
			this.lblAmount.Top = 1.5F;
			this.lblAmount.Visible = false;
			this.lblAmount.Width = 0.59375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.19F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.1875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblDate.Text = "DATE";
			this.lblDate.Top = 1.46875F;
			this.lblDate.Visible = false;
			this.lblDate.Width = 0.4375F;
			// 
			// fldLargeCheckNumber
			// 
			this.fldLargeCheckNumber.Height = 0.28125F;
			this.fldLargeCheckNumber.Left = 6.25F;
			this.fldLargeCheckNumber.Name = "fldLargeCheckNumber";
			this.fldLargeCheckNumber.Style = "font-family: \'Tahoma\'; font-size: 18pt";
			this.fldLargeCheckNumber.Tag = "Large";
			this.fldLargeCheckNumber.Text = null;
			this.fldLargeCheckNumber.Top = 0.03125F;
			this.fldLargeCheckNumber.Visible = false;
			this.fldLargeCheckNumber.Width = 1.041667F;
			// 
			// lblReturnAddress
			// 
			this.lblReturnAddress.CanGrow = false;
			this.lblReturnAddress.Height = 0.78125F;
			this.lblReturnAddress.Left = 1.0625F;
			this.lblReturnAddress.Name = "lblReturnAddress";
			this.lblReturnAddress.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.lblReturnAddress.Tag = "Large";
			this.lblReturnAddress.Text = null;
			this.lblReturnAddress.Top = 0.03125F;
			this.lblReturnAddress.Visible = false;
			this.lblReturnAddress.Width = 3.125F;
			// 
			// imgLogo
			// 
			this.imgLogo.Height = 0.96875F;
			this.imgLogo.HyperLink = null;
			this.imgLogo.ImageData = null;
			this.imgLogo.Left = 0F;
			this.imgLogo.LineWeight = 1F;
			this.imgLogo.Name = "imgLogo";
			this.imgLogo.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
			this.imgLogo.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgLogo.Top = 0F;
			this.imgLogo.Visible = false;
			this.imgLogo.Width = 0.96875F;
			// 
			// lblMunicipalName
			// 
			this.lblMunicipalName.Height = 0.1875F;
			this.lblMunicipalName.Left = 4.21875F;
			this.lblMunicipalName.Name = "lblMunicipalName";
			this.lblMunicipalName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblMunicipalName.Tag = "Large";
			this.lblMunicipalName.Text = null;
			this.lblMunicipalName.Top = 0.5625F;
			this.lblMunicipalName.Visible = false;
			this.lblMunicipalName.Width = 2.375F;
			// 
			// srptCustomCheckBody
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.84375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtAmountString)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddressName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLaserVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStandardVoid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStandardVoid2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLogo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLaserVoid2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMICRLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblToTheOrderOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoidAfter90Days)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMemoLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLargeCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgLogo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAmountString;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddressName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNumCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLaserVoid;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStandardVoid1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStandardVoid2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Picture txtLogo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLaserVoid2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMICRLine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblToTheOrderOf;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVoidAfter90Days;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMemoLine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignature1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignature2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLargeCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgLogo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblMunicipalName;
	}
}
