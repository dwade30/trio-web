//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.cmbReport.Text = "Create New Report";
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomReport InstancePtr
		{
			get
			{
				return (frmCustomReport)Sys.GetInstance(typeof(frmCustomReport));
			}
		}

		protected frmCustomReport _InstancePtr = null;
		//=========================================================
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomReport.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomReport, "Births")
		//
		//
		int intCounter;
		// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToInt32(
		int intStart;
		int intEnd;
		// vbPorter upgrade warning: intID As int	OnWriteFCConvert.ToInt32(
		int intID;
		string strTemp = "";
		double dblDataLength;
		bool boolSaveReport;
		// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
		double dblSizeRatio;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			clsDRWrapper rs = new clsDRWrapper();
			if (cmbReport.Text == "Delete Saved Report")
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobalVariables.DEFAULTDATABASE);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (cmbReport.Text == "Show Saved Report")
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				vsLayout.Clear();
				vsLayout.Rows = 2;
				vsLayout.Cols = 1;
				rs.OpenRecordset("Select * from tblReportLayout where ReportID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)) + " Order by RowID, ColumnID", modGlobalVariables.DEFAULTDATABASE);
				while (!rs.EndOfFile())
				{
					if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) == 1)
					{
						if (rs.Get_Fields_Int16("ColumnID") > 0)
						{
							vsLayout.Cols += 1;
						}
					}
					else
					{
						if (rs.Get_Fields_Int16("RowID") >= vsLayout.Rows)
						{
							vsLayout.Rows += 1;
                            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
                            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
						}
					}
					vsLayout.TextMatrix(rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), FCConvert.ToString(rs.Get_Fields_String("DisplayText")));
					vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), rs.Get_Fields("FieldID"));
					vsLayout.ColWidth(rs.Get_Fields_Int16("ColumnID"), FCConvert.ToInt32(rs.Get_Fields("Width")));
					rs.MoveNext();
				}
			}
		}

		private void chkLandscape_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkLandscape.CheckState == Wisej.Web.CheckState.Checked)
			{
				dblDataLength = 10;
			}
			else
			{
				dblDataLength = 7.5;
			}
            Line1.Left = Convert.ToInt32(dblDataLength * 96);
			//if (Image1.LeftOriginal < 0)
			//{
			//	Line1.X1 = FCConvert.ToSingle(1440 * dblDataLength * dblSizeRatio);
			//	//Line1.X1 = FCConvert.ToSingle((1440 * dblDataLength - 720) * dblSizeRatio - (vsLayout.Left - Image1.Left))
			//}
			//else
			//{
			//	Line1.X1 = FCConvert.ToSingle(1440 * dblDataLength * dblSizeRatio);
			//	//Line1.X1 = FCConvert.ToSingle((1440 * dblDataLength - 720) * dblSizeRatio - (vsLayout.Left - Image1.Left))
			//}
			//Line1.X2 = Line1.X1;
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			string strReturn;
			clsDRWrapper rsSave = new clsDRWrapper();
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			clsDRWrapper RSLayout = new clsDRWrapper();
			int lngTemp = 0;
			strReturn = Interaction.InputBox("Enter name for new report.", "New Custom Report", null);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				boolSaveReport = true;
				cmdPrint_Click();
				boolSaveReport = false;
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + strReturn + "' and type = '" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobalVariables.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					// Call MsgBox("A report by that name already exists.", vbExclamation + vbOKOnly, "Report Exists")
					if (MessageBox.Show("A report by that name already exists. Do you want to overwrite it?", "Overwrite?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
					{
						return;
					}
					else
					{
						lngTemp = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
						rsSave.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(lngTemp), modGlobalVariables.DEFAULTDATABASE);
						rsSave.Execute("delete from tblreportlayout where reportid = " + FCConvert.ToString(lngTemp), modGlobalVariables.DEFAULTDATABASE);
					}
				}
				rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type) VALUES ('" + strReturn + "','" + FCConvert.ToString(modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL)) + "','" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "')", modGlobalVariables.DEFAULTDATABASE);
				rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modGlobalVariables.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobalVariables.DEFAULTDATABASE);
					for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
					{
						for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
						{
							RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobalVariables.DEFAULTDATABASE);
							RSLayout.AddNew();
							RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
							RSLayout.Set_Fields("RowID", intRow);
							RSLayout.Set_Fields("ColumnID", intCol);
							if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
							{
								RSLayout.Set_Fields("FieldID", -1);
							}
							else
							{
								RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
							}
							RSLayout.Set_Fields("Width", frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol));
							RSLayout.Set_Fields("DisplayText", frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							RSLayout.Update();
						}
					}
				}
				LoadCombo();
				MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// End If
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 0; intCounter <= (vsWhere.Rows - 1); intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		public void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			vsWhere.Select(0, 0);
			clsDRWrapper rsSQL = new clsDRWrapper();
			int intReturn;
			DateTime dtDate;
			int lngYear;
			int intMonth;
			int intQuarter;
			DateTime dtStartDate;
			// CLEAR THE FIELDS WIDTH ARRAY
			for (intCounter = 0; intCounter <= 49; intCounter++)
			{
				modCustomReport.Statics.strFieldWidth[intCounter] = 0;
			}
			// PREPARE THE SQL TO SHOW THE REPORT
			bool executeNoFields = false;
			if (cmbReport.Text == "Create New Report")
			{
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				BuildSQL();
				// IF NO FIELDS WERE CHOSEN TO PRINT THEN DO NOT SHOW THE REPORT
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
			}
			else
			{
				// IF THE USER DOES NOT CHOSE A REPORT THEN DO NOT SHOW ONE
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// GET THE SAVED SQL STATEMENT FOR THE CUSTOM REPORT
				rsSQL.OpenRecordset("Select SQL from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobalVariables.DEFAULTDATABASE);
				if (!rsSQL.EndOfFile())
				{
					modCustomReport.Statics.strCustomSQL = FCConvert.ToString(rsSQL.Get_Fields_String("SQL"));
				}
			}
			// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			modCustomReport.GetNumberOfFields(modCustomReport.Statics.strCustomSQL);
			if (modCustomReport.Statics.intNumberOfSQLFields < 0)
			{
				executeNoFields = true;
				goto NoFields;
			}
			NoFields:
			;
			if (executeNoFields)
			{
				executeNoFields = false;
				if (modCustomReport.Statics.strPreSetReport != string.Empty)
				{
				}
				else
				{
					MessageBox.Show("No fields were selected to display.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			// Call SetColumnCaptions(Me)
			if (!boolSaveReport)
			{
				// SHOW THE REPORT
				if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEINFO")
				{
					frmReportViewer.InstancePtr.Init(rptEmployeeInformationReport.InstancePtr);
				}
				else if ((modCustomReport.Statics.strPreSetReport == "EMPLOYEEVACATION") || (modCustomReport.Statics.strPreSetReport == "EMPLOYEESICK"))
				{
					frmReportViewer.InstancePtr.Init(rptEmployeeVacationSick.InstancePtr);
					// Call CheckDefaultPrint(rptEmployeeVacationSick)
				}
				else if (modCustomReport.Statics.strPreSetReport == "PAYSUMMARY")
				{
					if (modGlobalVariables.Statics.gstrMQYProcessing == "WEEKLY")
					{
						modCoreysSweeterCode.PrintPaySummaryReport(0);
					}
					else if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
					{
						modCoreysSweeterCode.PrintPaySummaryReport(1);
					}
					else if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
					{
						modCoreysSweeterCode.PrintPaySummaryReport(2);
					}
					else if (modGlobalVariables.Statics.gstrMQYProcessing == "YEARLY")
					{
						modCoreysSweeterCode.PrintPaySummaryReport(3);
					}
					else
					{
						modCoreysSweeterCode.PrintPaySummaryReport(4);
					}
					// 
					// strCustomTitle = "**** YTD Tax Summary Report ****"
					// rptYTDTaxSumaryReport.Show vbModal, MDIParent
				}
				else if (modCustomReport.Statics.strPreSetReport == "DATAENTRYFORMS")
				{
					// frmReportViewer.Init rptDataEntryForms
					modCoreysSweeterCode.CheckDefaultPrint(rptDataEntryForms.InstancePtr, boolAllowEmail: true, strAttachmentName: "DataEntry");
				}
				else if (modCustomReport.Statics.strPreSetReport == "TAXSUMMARY")
				{
					// frmReportViewer.Init rptTaxSumaryReport
					modCoreysSweeterCode.CheckDefaultPrint(rptTaxSumaryReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "TaxSummary");
				}
				else if (modCustomReport.Statics.strPreSetReport == "TAXABLEWAGESUMMARY")
				{
					frmReportViewer.InstancePtr.Init(rptTaxableWageSummaryReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "TaxableWageSummary");
				}
				else if (modCustomReport.Statics.strPreSetReport == "YTDTAXSUMMARY")
				{
					frmReportViewer.InstancePtr.Init(rptYDTaxSumaryReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "YTDTaxSummary");
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEDEDUCTIONS")
				{
					frmReportViewer.InstancePtr.Init(rptEmployeeDeductionReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "EmployeeDeductions");
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEMATCH")
				{
					frmReportViewer.InstancePtr.Init(rptEmployeeMatchReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "EmployeeMatch");
				}
				else if (modCustomReport.Statics.strPreSetReport == "BANKLISTING")
				{
					frmReportViewer.InstancePtr.Init(rptBankListingReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "CustomBankListing");
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptCustomReport.InstancePtr, boolAllowEmail: false);
				}
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			string[] strSelectedFields = new string[500 + 1];
			bool boolUsedEmployeeNumber = false;
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			// GET THE FIELD NAMES THAT THE USER HAS SELECTED
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				strSelectedFields[intCounter] = string.Empty;
			}
			for (intRow = 1; intRow <= (vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (vsLayout.Cols - 1); intCol++)
				{
					for (intCounter = 0; intCounter <= 499; intCounter++)
					{
						if (strSelectedFields[intCounter] == string.Empty)
						{
							modCustomReport.Statics.intNumberOfSQLFields += 1;
							strSelectedFields[intCounter] = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							if (modCustomReport.Statics.strPreSetReport != string.Empty)
							{
								modCustomReport.Statics.strCustomSQL = FCConvert.ToString(fraFields.Tag);
								goto SetString;
							}
							else
							{
								break;
							}
						}
						else if (strSelectedFields[intCounter] == vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol))
						{
							goto NextCell;
						}
					}
					NextCell:
					;
				}
			}
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				if (strSelectedFields[intCounter] == string.Empty)
				{
					goto Continue;
				}
				else
				{
					for (intRow = 0; intRow <= lstFields.Items.Count - 1; intRow++)
					{
						if (lstFields.Items[intRow].Text == strSelectedFields[intCounter])
						{
							if (modCustomReport.Statics.strCustomSQL != string.Empty)
								modCustomReport.Statics.strCustomSQL += ", ";
							modCustomReport.Statics.strCustomSQL += modCustomReport.Statics.strFields[lstFields.ItemData(intRow)];
							if (fecherFoundation.Strings.LCase(FCConvert.ToString(lstFields.ItemData(intRow))) == "employeenumber")
							{
								boolUsedEmployeeNumber = true;
							}
							break;
						}
					}
				}
			}
			Continue:
			;
			// CHECK TO SEE IF ANY FIELDS WERE SELCTED. IF THEY WERE THEN THE
			// STRCUSTOMSQL WOULD HAVE SOME DATA IN IT
			if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strCustomSQL) == string.Empty)
			{
				modCustomReport.Statics.intNumberOfSQLFields = -1;
				return;
			}
			// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
			if (FCConvert.ToString(fraFields.Tag) == "EmployeeInformation")
			{
				// vbPorter upgrade warning: strTemp As object	OnWrite(string())
				string[] strTemp = null;
				strTemp = Strings.Split(modCustomReport.Statics.strCustomSQL, "BaseRate", -1, CompareConstants.vbBinaryCompare);
				if (!boolUsedEmployeeNumber)
				{
					modCustomReport.Statics.strCustomSQL += ",tblemployeemaster.EmployeeNumber ";
				}
				if (Information.UBound(strTemp, 1) > 0)
				{
					modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " ,tblPayrollDistribution.RecordNumber FROM (tblEmployeeMaster LEFT JOIN tblPayrollDistribution ON tblEmployeeMaster.EmployeeNumber = tblPayrollDistribution.EmployeeNumber) INNER JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID WHERE tblEmployeeMaster.EmployeeNumber <>'' AND RecordNumber = 1";
				}
				else
				{
					modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " FROM tblEmployeeMaster INNER JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID WHERE tblEmployeeMaster.EmployeeNumber <> ''";
				}
			}
			else
			{
				if (!boolUsedEmployeeNumber)
				{
					modCustomReport.Statics.strCustomSQL += ",tblemployeemaster.EmployeeNumber ";
				}
				modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			}
			SetString:
			;
			// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
			BuildWhereParameter();
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			BuildSortParameter();
		}

		public void BuildSortParameter()
		{
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					strSort += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1));
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			if (fecherFoundation.Strings.Trim(strSort) != string.Empty)
			{
				if (modCustomReport.Statics.strPreSetReport == "PAYSUMMARY")
				{
					// strCustomSQL = strCustomSQL & " Order by tblEmployeeMaster.LastName,tblPayTotals.RecordNumber"
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEDEDUCTIONS")
				{
					// strCustomSQL = strCustomSQL & "Order by tblEmployeeDeductions.DeductionCode"
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEVACATION")
				{
					modCustomReport.Statics.strCustomSQL += "order by tblcodes.code";
					// will be inner joined anyway so don't order
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEESICK")
				{
					modCustomReport.Statics.strCustomSQL += " ORDER BY tblCodes.Code";
				}
				else
				{
					modCustomReport.Statics.strCustomSQL += " Order by" + strSort;
				}
			}
			else
			{
				if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEDEDUCTIONS")
				{
					// strCustomSQL = strCustomSQL & " Order by tblEmployeeDeductions.DeductionCode"
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEVACATION")
				{
					modCustomReport.Statics.strCustomSQL += "order by tblcodes.Code";
				}
				else if (modCustomReport.Statics.strPreSetReport == "EMPLOYEEMATCH")
				{
					modCustomReport.Statics.strCustomSQL += " Order by tblEmployersMatch.DeductionCode";
				}
				else if (modCustomReport.Statics.strPreSetReport == "BANKLISTING")
				{
					modCustomReport.Statics.strCustomSQL += " Order by Bank";
				}
			}
		}

		public void BuildWhereParameter()
		{
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// CLEAR THE VARIABLES
			strWhere = " ";
			// GET THE FIELDS TO FILTER BY
			for (intCounter = 0; intCounter <= (vsWhere.Rows - 1); intCounter++)
			{
				// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
				{
					case modCustomReport.GRIDTEXT:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (modCustomReport.Statics.strFields[intCounter] == "LastName&','&FirstName+Space(1)&Desig AS FullName")
							{
							}
							else
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
								}
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
							// THE POUND SYMBOL WRAPPED AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
									// FIELD SO WE NOW HAVE A DATE RANGE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and " + fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
									// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "'";
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
							// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " = '" + vsWhere.TextMatrix(intCounter, 3) + "'";
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = " + vsWhere.TextMatrix(intCounter, 3);
							}
							break;
						}
					case modCustomReport.GRIDCOMBOTEXT:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = '" + vsWhere.TextMatrix(intCounter, 1) + "'";
							}
							break;
						}
					case modCustomReport.GRIDBOOLEAN:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = ";
								if (fecherFoundation.Strings.LCase(vsWhere.TextMatrix(intCounter, 1)) == "true")
								{
									strWhere += "1";
								}
								else
								{
									strWhere += "0";
								}
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + " AND " + modCustomReport.Statics.strFields[intCounter] + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1);
								}
							}
							break;
						}
					case modCustomReport.GRIDTEXTRANGE:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter], 1)) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter], 1)) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "zzz'";
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter], 1)) + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
								}
							}
							break;
						}
				}
				//end switch
			}
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS
			// TO THE SQL STATEMENT
			if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
			{
				if (modCustomReport.Statics.strReportType == "EMPLOYEEVACATION")
				{
					modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere + " and vstypeid = " + modGlobalVariables.Statics.gstrVacType + " ";
				}
				else if (modCustomReport.Statics.strReportType == "PAYSUMMARY")
				{
					modCustomReport.Statics.strCustomSQL = " Where " + strWhere;
				}
				else if (modCustomReport.Statics.strReportType == "CUSTOMCHECKDETAIL" || modCustomReport.Statics.strReportType == "EMPLOYEEINFO")
				{
					modCustomReport.Statics.strCustomSQL += " AND " + strWhere;
				}
				else if (modCustomReport.Statics.strReportType == "CUSTOMVACSICK")
				{
					modCustomReport.Statics.strCustomSQL += " WHERE " + strWhere + " and selected = 1";
				}
				else
				{
					modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere;
				}
			}
			else
			{
				if (modCustomReport.Statics.strReportType == "EMPLOYEEVACATION")
				{
					if (modGlobalVariables.Statics.gboolPrintAllTypes)
					{
						modCustomReport.Statics.strCustomSQL += " where selected = 1";
					}
					else
					{
						modCustomReport.Statics.strCustomSQL += " WHERE TypeID = " + modGlobalVariables.Statics.gstrVacType + " ";
					}
				}
				else if (modCustomReport.Statics.strReportType == "PAYSUMMARY")
				{
					modCustomReport.Statics.strCustomSQL = "";
				}
				else if (modCustomReport.Statics.strReportType == "CUSTOMVACSICK")
				{
					modCustomReport.Statics.strCustomSQL += " where selected = 1";
				}
			}
		}

		private void frmCustomReport_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						mnuAddRow_Click();
						break;
					}
				case Keys.F3:
					{
						mnuAddColumn_Click();
						break;
					}
				case Keys.F4:
					{
						mnuDeleteRow_Click();
						break;
					}
				case Keys.F5:
					{
						mnuDeleteColumn_Click();
						break;
					}
			}
			//end switch
			// make the enter key work like the tab
			// If KeyCode = 13 Then
			// KeyCode = 0
			// SendKeys "{TAB}"
			// Exit Sub
			// End If
			// 
			// If KeyCode = vbKeyF11 Then Call mnuSave_Click
			// If KeyCode = vbKeyF12 Then
			// Call mnuSave_Click
			// Call mnuExit_Click
			// End If
		}

		private void frmCustomReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomReport properties;
			//frmCustomReport.ScaleWidth	= 8955;
			//frmCustomReport.ScaleHeight	= 7890;
			//frmCustomReport.LinkTopic	= "Form1";
			//frmCustomReport.LockControls	= -1  'True;
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			// With rsCreateTable
			// CREATE A NEW TABLE IF IT DOESN'T EXIST
			// If .CreateNewDatabaseTable("tblCustomReports", DEFAULTDATABASE) Then
			// CREATE THE NEW FIELDS
			// .CreateTableField "ID", dbLong
			// .CreateTableField "ReportName", dbText
			// .CreateTableField "Type", dbText
			// .CreateTableField "SQL", dbMemo
			// .CreateTableField "LastUpdated", dbDate
			// 
			// SET THE PROPERTIES OF THE NEW FIELDS
			// .SetFieldAttribute "ID", dbAutoIncrField
			// .SetFieldDefaultValue "LastUpdated", "NOW()"
			// .SetFieldAllowZeroLength "ReportName", True
			// .SetFieldAllowZeroLength "Type", True
			// .SetFieldAllowZeroLength "SQL", True
			// 
			// DO THE ACTUAL CREATION OF THE TABLE
			// .UpdateTableCreation
			// End If
			// End With
			// With rsCreateTable
			// CREATE A NEW TABLE IF IT DOESN'T EXIST
			// If .CreateNewDatabaseTable("tblReportLayout", DEFAULTDATABASE) Then
			// CREATE THE NEW FIELDS
			// .CreateTableField "ID", dbLong
			// .CreateTableField "ReportID", dbInteger
			// .CreateTableField "RowID", dbInteger
			// .CreateTableField "ColumnID", dbInteger
			// .CreateTableField "FieldID", dbInteger
			// .CreateTableField "Width", dbInteger
			// .CreateTableField "DisplayText", dbText
			// .CreateTableField "LastUpdated", dbDate
			// 
			// SET THE PROPERTIES OF THE NEW FIELDS
			// .SetFieldAttribute "ID", dbAutoIncrField
			// .SetFieldDefaultValue "LastUpdated", "NOW()"
			// .SetFieldAllowZeroLength "DisplayText", True
			// 
			// DO THE ACTUAL CREATION OF THE TABLE
			// .UpdateTableCreation
			// End If
			// End With
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION
			//Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			vsLayout.Rows = 2;
			vsLayout.Cols = 1;
			//Image1.WidthOriginal = 1440 * 14;
			//Image1.Image = ImageList1.Images[0];
			//HScroll1.Minimum = 0;
			//if (Image1.WidthOriginal > fraLayout.WidthOriginal)
			//{
			//	HScroll1.Maximum = Image1.WidthOriginal - fraLayout.WidthOriginal;
			//}
			//else
			//{
			//	HScroll1.Enabled = false;
			//}
			//vsLayout.Width = Image1.WidthOriginal - 720;
			vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(1, true);
			vsLayout.Left = Image1.Left;
			dblDataLength = 7.5;
			// Line1.X1 = Image1.Left + (1440 * dblDataLength) - 900
			//Line1.X1 = FCConvert.ToSingle(1440 * dblDataLength);
			//Line1.X2 = Line1.X1;
			//Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;
			// ENABLE OR DISABLE FRAME OPTIONS ACCORDING WEITHER
			// THIS
			fraLayout.Enabled = modCustomReport.Statics.strPreSetReport == string.Empty;
			//fraNotes.Enabled = fraLayout.Enabled;
			fraReports.Enabled = fraLayout.Enabled;
			fraMessage.Visible = !fraLayout.Enabled;
			fraFields.Enabled = fraLayout.Enabled;
			// IF THIS IS A PRESET REPORT THEN ADJUST THE MENU
			// OPTIONS ACCORDINGLY
			cmdAddColumn.Enabled = modCustomReport.Statics.strPreSetReport == string.Empty;
			cmdDeleteColumn.Enabled = modCustomReport.Statics.strPreSetReport == string.Empty;
			cmdAddRow.Enabled = modCustomReport.Statics.strPreSetReport == string.Empty;
			cmdDeleteRow.Enabled = modCustomReport.Statics.strPreSetReport == string.Empty;
			if (modCustomReport.Statics.strPreSetReport != "EMPLOYEEINFO")
			{
				fraSort.Enabled = fraLayout.Enabled;
			}
			else
			{
				fraSort.Enabled = true;
			}
			if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strCustomTitle) == string.Empty)
				modCustomReport.Statics.strCustomTitle = "Payroll Reports";
			this.Text = modCustomReport.Statics.strCustomTitle;

            lstFields.Columns[0].Width = (int)(lstFields.Width * 0.94);
            lstFields.GridLineStyle = GridLineStyle.None;

            lstSort.Columns[0].Width = (int)(lstSort.Width * 0.94);
            lstSort.GridLineStyle = GridLineStyle.None;

            Line1.BringToFront();
        }

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			//dblSizeRatio = Line2.X2 / 1440;
			////// Line1.X1 = (dblDataLength * 1440) * dblSizeRatio
			//Image1.WidthOriginal = FCConvert.ToInt32(1440 * 14 * dblSizeRatio);
			////// Line1.X1 = ((1440 * dblDataLength - 720)) * dblSizeRatio - (vsLayout.Left - Image1.Left)
			////// 7.5 inches * ratio - offset from the rulers left side.
			//Line1.X1 = FCConvert.ToSingle(1440 * dblDataLength * dblSizeRatio);
			//Line1.X2 = Line1.X1;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modCustomReport.Statics.strPreSetReport = string.Empty;
			modGlobalVariables.Statics.gboolGroupBy = false;
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			// vsLayout.Left = (720 * dblSizeRatio) + (-HScroll1.Value + 30)
			vsLayout.Left = Image1.Left;
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			// vsLayout.Left = (720 * dblSizeRatio) + (-HScroll1.Value + 30)
			vsLayout.Left = Image1.Left;
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("Select * from tblCustomReports where Type = '" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobalVariables.DEFAULTDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			if (vsLayout.Row < 1)
				vsLayout.Row = 1;
			vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, lstFields.Items[lstFields.SelectedIndex].Text);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, lstFields.ItemData(lstFields.SelectedIndex));
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart].Text = strTemp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		lstSort.SetSelected(lstSort.ListIndex, true);
		//		lstSort.SetSelected(intStart, true);
		//	}
		//}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		public void mnuAddColumn_Click()
		{
			mnuAddColumn_Click(mnuAddColumn, new System.EventArgs());
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(mnuAddRow, new System.EventArgs());
		}

		private void mnuChangeParameters_Click(object sender, System.EventArgs e)
		{
			frmPrintParameters.InstancePtr.Show(App.MainForm);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(mnuDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		public void mnuDeleteRow_Click()
		{
			mnuDeleteRow_Click(mnuDeleteRow, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = cmbReport.Text == "Create New Report";
			// cmdSave.Visible = Index = 1
			fraSort.Enabled = cmbReport.Text == "Create New Report";
			fraFields.Enabled = cmbReport.Text == "Create New Report";
			fraWhere.Enabled = cmbReport.Text == "Create New Report";
			int intCounter;
			for (intCounter = 0; intCounter <= this.lstSort.Items.Count - 1; intCounter++)
			{
				this.lstSort.SetSelected(intCounter, false);
			}
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (fecherFoundation.Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (fecherFoundation.Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
				case modCustomReport.GRIDBOOLEAN:
					{
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
			{
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
			if (vsWhere.Col == 2)
			{
				if (FCConvert.ToInt32(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsWhere.Col = 1;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (fecherFoundation.Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (fecherFoundation.Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (fecherFoundation.Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!FCConvert.ToBoolean(modCustomReport.IsValidDate(vsWhere.EditText)))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

        private void vsLayout_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {   
            ResizeGrid();
        }

        private void VsLayout_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
        {
            ResizeGrid();
        }

        private void VsLayout_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            ResizeGrid();
        }
        
        private void VsLayout_ColumnStateChanged(object sender, DataGridViewColumnStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Visible)
            {
                ResizeGrid();
            }
        }
        private void ResizeGrid()
        {
            int gridWidth = 0;
            //add grid columns width
            foreach (DataGridViewColumn column in vsLayout.Columns)
            {
                if (column.Visible)
                {
                    gridWidth += column.Width;
                }
            }
            //add grid border
            gridWidth += 2;
            gridWidth = gridWidth < 917 ? 917 : gridWidth;
            vsLayout.Width = gridWidth;
        }
    }
}
