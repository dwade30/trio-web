//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmNonPaidMSRS : BaseForm
	{
		public frmNonPaidMSRS()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_3, 2);
			this.Label1.AddControlArrayElement(Label1_4, 3);
			this.Label1.AddControlArrayElement(Label1_5, 4);
			this.Label1.AddControlArrayElement(Label1_6, 5);
			this.Label1.AddControlArrayElement(Label1_7, 6);
			this.Label1.AddControlArrayElement(Label1_8, 7);
			this.Label1.AddControlArrayElement(Label1_9, 8);
			this.Label1.AddControlArrayElement(Label1_11, 9);
			this.Label1.AddControlArrayElement(Label1_12, 10);
			this.Label1.AddControlArrayElement(Label1_16, 11);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmNonPaidMSRS InstancePtr
		{
			get
			{
				return (frmNonPaidMSRS)Sys.GetInstance(typeof(frmNonPaidMSRS));
			}
		}

		protected frmNonPaidMSRS _InstancePtr = null;
		//=========================================================
		bool boolSearchMode;
		int lngCurrentID;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		DialogResult intDataChanged;
		bool boolCantEdit;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
        // Counter keeps track of how many controls you are keeping track of
        // -------------------------------------------------------------------
        private SSNViewType ssnViewPermission = SSNViewType.None;
        private void cboReportType_DropDown(object sender, System.EventArgs e)
		{
			intDataChanged = (DialogResult)1;
		}

		private void cmbPositionCategory_DropDown(object sender, System.EventArgs e)
		{
			intDataChanged = (DialogResult)1;
		}

		private void cmbStatus_DropDown(object sender, System.EventArgs e)
		{
			intDataChanged = (DialogResult)1;
		}

		private void frmNonPaidMSRS_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmNonPaidMSRS_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
			intDataChanged = (DialogResult)1;
		}

		private void frmNonPaidMSRS_Load(object sender, System.EventArgs e)
		{
			boolCantEdit = false;
			boolSearchMode = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			cmbStatus.AddItem("Active");
			cmbStatus.AddItem("Retired");
			cmbStatus.AddItem("Terminated");
			SetupGrid();
			SetupGridSearch();
			FillcmbPositionCategory();
			
			cboReportType.AddItem("PLD");
			cboReportType.ItemData(cboReportType.NewIndex, modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD);
			cboReportType.AddItem("Teacher PLD");
			cboReportType.ItemData(cboReportType.NewIndex, modCoreysSweeterCode.CNSTMSRSREPORTTYPESCHOOLPLD);
			cboReportType.AddItem("Teacher");
			cboReportType.ItemData(cboReportType.NewIndex, modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
			intDataChanged = 0;
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    txtSocialSecurity.AllowOnlyNumericInput();
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    txtSocialSecurity.Enabled = false;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    txtSocialSecurity.Enabled = false;
                    break;
            }
            FillGridSearch();
            if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				boolCantEdit = true;
				Grid.Enabled = false;
				cmdNew.Enabled = false;
				cmdSave.Enabled = false;
				cmdSaveContinue.Enabled = false;
				t2kDateHired.Enabled = false;
				t2kDateOfBirth.Enabled = false;
				t2kTerminationDate.Enabled = false;
				cmbPositionCategory.Enabled = false;
				cmbStatus.Enabled = false;
				chkInclude.Enabled = false;
				txtExcess.Enabled = false;
				txtFedCompAmount.Enabled = false;
				txtFirstName.Enabled = false;
				txtLastName.Enabled = false;
				txtLifeInsCode.Enabled = false;
				txtLifeInsLevel.Enabled = false;
				txtPayPeriodCode.Enabled = false;
				txtPayRateCode.Enabled = false;
				txtPlanCode.Enabled = false;
				txtPositionCode.Enabled = false;
				txtRecordNumber.Enabled = false;
				txtSequence.Enabled = false;
				txtSocialSecurity.Enabled = false;
				txtStatusCode.Enabled = false;
				txtWorkWeekPerYear.Enabled = false;
			}

		}

		private void FillcmbPositionCategory()
		{
			cmbPositionCategory.Clear();
			cmbPositionCategory.AddItem("None");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYNONE);
			// .AddItem ("Firefighter/Law/Government")
			// .ItemData(.NewIndex) = CNSTMSRSPLANCATEGORYFIREFIGHTERLAWGOVERNMENT
			cmbPositionCategory.AddItem("Firefighter");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYFIREFIGHTER);
			cmbPositionCategory.AddItem("General Government");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYGENERALGOVERNMENT);
			cmbPositionCategory.AddItem("Law Enforcement");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYLAWENFORCEMENT);
			cmbPositionCategory.AddItem("Teacher");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYTEACHER);
			cmbPositionCategory.AddItem("Teacher (Before 7/1/93)");
			cmbPositionCategory.ItemData(cmbPositionCategory.NewIndex, modCoreysSweeterCode.CNSTMSRSPLANCATEGORYTEACHERBEFORE93);
			cmbPositionCategory.SelectedIndex = 0;
		}

		private void SetFormMode()
		{
			if (boolSearchMode)
			{
				framMSRS.Visible = false;
				framSearch.Visible = true;
				framEmployee.Enabled = false;
				framEmployee.Visible = false;
				cmdSearch.Enabled = false;
				cmdSave.Enabled = false;
				cmdSaveContinue.Enabled = false;
			}
			else
			{
				framMSRS.Visible = true;
				framSearch.Visible = false;
				framEmployee.Enabled = true;
				framEmployee.Visible = true;
				cmdSearch.Enabled = true;
				cmdSave.Enabled = true;
				cmdSaveContinue.Enabled = true;
			}
		}

		private void frmNonPaidMSRS_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridSearch();
		}

		private void SetupGridSearch()
		{
			GridSearch.ColHidden(4, true);
			GridSearch.ColHidden(5, true);
			GridSearch.ColHidden(6, true);
			GridSearch.ColHidden(7, true);
			GridSearch.TextMatrix(0, 0, "Record");
			GridSearch.TextMatrix(0, 1, "Last Name");
			GridSearch.TextMatrix(0, 2, "First Name");
			GridSearch.TextMatrix(0, 3, "Social Security #");
		}

		private void ResizeGridSearch()
		{
			int GridWidth = 0;
			GridWidth = GridSearch.WidthOriginal;
			GridSearch.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			GridSearch.ColWidth(1, FCConvert.ToInt32(0.37 * GridWidth));
			GridSearch.ColWidth(2, FCConvert.ToInt32(0.31 * GridWidth));
			GridSearch.ColWidth(3, FCConvert.ToInt32(0.18 * GridWidth));

            GridSearch.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridSearch.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void FillGridSearch()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			strSQL = "Select * from tblMSRSNonPaid order by lastname,firstname";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
            {
                var strSSN = clsLoad.Get_Fields_String("SocialSecurityNumber");
                if (ssnViewPermission == SSNViewType.Masked)
                {
                    strSSN = "***-**-" + strSSN.Right(4);
                }
                else if (ssnViewPermission == SSNViewType.None)
                {
                    strSSN = "***-**-****";
                }

                GridSearch.AddItem(clsLoad.Get_Fields_Int32("RecordNumber") + "\t" + clsLoad.Get_Fields_String("LastName") + "\t" + clsLoad.Get_Fields_String("FirstName") + "\t" + strSSN + "\t" + clsLoad.Get_Fields("ID") + "\t" + clsLoad.Get_Fields_String("Status") + "\t" + clsLoad.Get_Fields("DateofBirth") + "\t" + clsLoad.Get_Fields("DateofHire"));
				clsLoad.MoveNext();
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			SaveChanges();
			if (intDataChanged == (DialogResult)2 && !boolCantEdit)
			{
				e.Cancel = true;
				return;
			}
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Grid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			switch (Grid.Row)
			{
				case 0:
					{
						Grid.ComboList = "B|HA|HB|S1|S2|S3|F1A|F2A|F3A|F1B|F2B|F3B|I|R|OC";
						break;
					}
				case 1:
					{
						Grid.ComboList = "-|11|12|14|15|17|52|53|65";
						break;
					}
				case 3:
					{
						Grid.ComboList = "--|AC|BC|AN|1C|2C|3C|4C|1N|2N|3N|4N";
						break;
					}
				case 4:
					{
						Grid.ComboList = "-|E|P";
						break;
					}
				case 17:
					{
						Grid.ComboList = "A" + "\t" + "Active|N" + "\t" + "Ineligible|R" + "\t" + "Refused Membership|Y" + "\t" + "Employer Paid";
						break;
					}
				default:
					{
						Grid.ComboList = "";
						break;
					}
			}
			//end switch
			FillDescription(Grid.Row);
		}

		private void Grid_ComboDropDown(object sender, System.EventArgs e)
		{
			intDataChanged = (DialogResult)1;
		}

		private void Grid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			intDataChanged = (DialogResult)1;
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			FillDescription(Grid.Row);
		}

		private void GridSearch_DblClick(object sender, System.EventArgs e)
		{
			// open the record
			int lngRow;
			lngRow = GridSearch.MouseRow;
			if (lngRow < 1)
				return;
			LoadEmployee(FCConvert.ToInt32(Conversion.Val(GridSearch.TextMatrix(lngRow, 4))));
			boolSearchMode = false;
			SetFormMode();
			if (!boolCantEdit)
			{
				txtLastName.Focus();
			}
		}
		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		private void LoadEmployee(int lngID)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite
			DateTime dtDate;
			string strTemp = "";
			int x;
			if (lngID < 1)
			{
				ClearForm();
				return;
			}
			strSQL = "Select * from tblMSRSNonPaid where ID = " + FCConvert.ToString(lngID);
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				txtLastName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("LastName"));
				txtFirstName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("FirstName"));
				strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("socialsecuritynumber"));
				strTemp += Strings.StrDup(9, " ");
				strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6, 4);
                if (ssnViewPermission == SSNViewType.Masked)
                {
                    strTemp = "***-**-" + strTemp.Right(4);
                }
                else if (ssnViewPermission == SSNViewType.None)
                {
                    strTemp = "***-**-****";
                }
                txtSocialSecurity.Text = strTemp;
				txtRecordNumber.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("RecordNumber"));
				lngCurrentID = lngID;
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Status"))) == "R")
				{
					// retired
					cmbStatus.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Status"))) == "T")
				{
					// terminated
					cmbStatus.SelectedIndex = 2;
				}
				else
				{
					// active or unknown so default to active
					cmbStatus.SelectedIndex = 0;
				}
				dtDate = (DateTime)clsLoad.Get_Fields("DateofBirth");
				if (dtDate.IsEmptyDate())
				{
					t2kDateOfBirth.Text = "00/00/0000";
				}
				else
				{
					t2kDateOfBirth.Text = Strings.Format(clsLoad.Get_Fields("DateofBirth"), "MM/dd/yyyy");
				}
				dtDate = (DateTime)clsLoad.Get_Fields("DateofHire");
				if (dtDate.ToOADate() == 0)
				{
					t2kDateHired.Text = "00/00/0000";
				}
				else
				{
					t2kDateHired.Text = Strings.Format(clsLoad.Get_Fields("DateofHire"), "MM/dd/yyyy");
				}
				if (Information.IsDate(clsLoad.Get_Fields("DateTerminated")))
				{
					dtDate = (DateTime)clsLoad.Get_Fields_DateTime("DateTerminated");
				}
				else
				{
					dtDate = DateTime.FromOADate(0);
				}
				if (dtDate.IsEmptyDate())
				{
					t2kTerminationDate.Text = "00/00/0000";
				}
				else
				{
					t2kTerminationDate.Text = Strings.Format(clsLoad.Get_Fields("dateterminated"), "MM/dd/yyyy");
				}
				// now fill in the msrs stuff
				if (FCConvert.ToBoolean(clsLoad.Get_Fields("include")))
				{
					chkInclude.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkInclude.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// If Val(.Fields("msrstype")) = 2 Then
				// chkTeacher.Value = vbChecked
				// Else
				// chkTeacher.Value = vbUnchecked
				// End If
				if (Conversion.Val(clsLoad.Get_Fields("ReportType")) == 0)
				{
					cboReportType.SelectedIndex = 0;
				}
				else
				{
					int intItem;
					for (intItem = 0; intItem <= 2; intItem++)
					{
						if (cboReportType.ItemData(intItem) == Conversion.Val(clsLoad.Get_Fields("ReportType")))
						{
							cboReportType.SelectedIndex = intItem;
						}
					}
				}
				if (Conversion.Val(clsLoad.Get_Fields("seqnumber")) > 0)
				{
					txtSequence.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("seqnumber")));
				}
				else
				{
					txtSequence.Text = "";
				}
				Grid.TextMatrix(0, 1, FCConvert.ToString(clsLoad.Get_Fields_String("LifeInsuranceCode")));
				Grid.TextMatrix(1, 1, FCConvert.ToString(clsLoad.Get_Fields_String("StatusCode")));
				Grid.TextMatrix(2, 1, FCConvert.ToString(clsLoad.Get_Fields_String("PositionCode")));
				Grid.TextMatrix(3, 1, FCConvert.ToString(clsLoad.Get_Fields_String("PlanCode")));
				Grid.TextMatrix(4, 1, FCConvert.ToString(clsLoad.Get_Fields_String("LifeInsuranceScheduleEP")));
				Grid.TextMatrix(5, 1, FCConvert.ToString(clsLoad.Get_Fields_String("LifeInsuranceScheduleBZ")));
				Grid.TextMatrix(6, 1, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("LifeInsuranceLevel"))));
				Grid.TextMatrix(7, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("FederalCompAmount"))), "0.00"));
				Grid.TextMatrix(8, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("MonthlyHours"))), "0.00"));
				Grid.TextMatrix(9, 1, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("WorkWeeksPerYear"))));
				Grid.TextMatrix(10, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PaybackAmount"))), "0.00"));
				Grid.TextMatrix(11, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("HoursMTD"))), "0.00"));
				Grid.TextMatrix(12, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("GrossMTD"))), "0.00"));
				Grid.TextMatrix(13, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("GrossCalendar"))), "0.00"));
				Grid.TextMatrix(14, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PayRate"))), "0.00"));
				Grid.TextMatrix(15, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("RetirementContributions"))), "0.00"));
				Grid.TextMatrix(16, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("InsuranceContributions"))), "0.00"));
				Grid.TextMatrix(17, 1, FCConvert.ToString(clsLoad.Get_Fields_String("Participation")));
				Grid.TextMatrix(18, 1, FCConvert.ToString(clsLoad.Get_Fields("schedule")));
				for (x = 0; x <= cmbPositionCategory.Items.Count - 1; x++)
				{
					if (cmbPositionCategory.ItemData(x) == Conversion.Val(clsLoad.Get_Fields_Int32("MSRSJobCategory")))
					{
						cmbPositionCategory.SelectedIndex = x;
						break;
					}
				}
				// x
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClass();
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			else
			{
				ClearForm();
			}
		}

		private bool SaveEmployee()
		{
			bool SaveEmployee = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngRecordNumber;
			string strSQL = "";
			// vbPorter upgrade warning: intResponse As int	OnWrite(DialogResult)
			DialogResult intResponse = 0;
			string strTemp;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (this.txtLastName.Text == string.Empty)
				{
					MessageBox.Show("Employee's Last Name must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtLastName.Focus();
					SaveEmployee = false;
					return SaveEmployee;
				}
				SaveEmployee = false;
				// check if the record number is legit first
				lngRecordNumber = 0;
				if (Conversion.Val(txtRecordNumber.Text) > 0)
				{
					lngRecordNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtRecordNumber.Text)));
					clsSave.OpenRecordset("select ID,recordnumber from tblMSRSNonPaid where recordnumber = " + FCConvert.ToString(lngRecordNumber), "twpy0000.vb1");
					if (!clsSave.EndOfFile())
					{
						if (lngCurrentID != FCConvert.ToInt32(clsSave.Get_Fields("ID")))
						{
							intResponse = MessageBox.Show("This record number is already used.  A new one will be automatically generated.", "Record number already exists.", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
							if (intResponse == DialogResult.Cancel)
								return SaveEmployee;
							lngRecordNumber = 0;
							txtRecordNumber.Text = "";
						}
					}
				}
				clsSave.OpenRecordset("select * from tblmsrsnonpaid where ID = " + FCConvert.ToString(lngCurrentID), "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
					// Dave 12/14/2006--------------------------------------------
					// Set New Information so we can compare
					for (x = 0; x <= intTotalNumberOfControls - 1; x++)
					{
						clsControlInfo[x].FillNewValue(this);
					}
					// Thsi function compares old and new values and creates change records for any differences
					modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
					// This function takes all the change records and writes them into the AuditChanges table in the database
					clsReportChanges.SaveToAuditChangesTable("Non Paid MSRS", FCConvert.ToString(0), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
					// Reset all information pertianing to changes and start again
					intTotalNumberOfControls = 0;
					clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
					clsReportChanges.Reset();
					// Initialize all the control and old data values
					FillControlInformationClass();
					FillControlInformationClassFromGrid();
					for (x = 0; x <= intTotalNumberOfControls - 1; x++)
					{
						clsControlInfo[x].FillOldValue(this);
					}
					// ----------------------------------------------------------------
				}
				else
				{
					clsSave.AddNew();
					// Dave 12/14/2006---------------------------------------------------
					// Add change record for adding a row to the grid
					clsReportChanges.AddChange("Employee " + txtFirstName.Text + " " + txtLastName.Text + " added to Non Paid MSRS");
					// This function takes all the change records and writes them into the AuditChanges table in the database
					clsReportChanges.SaveToAuditChangesTable("Non Paid MSRS", FCConvert.ToString(0), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
					// ------------------------------------------------------------------
				}
				lngCurrentID = FCConvert.ToInt32(clsSave.Get_Fields("ID"));
				if (lngRecordNumber == 0)
				{
					clsSave.Set_Fields("RecordNumber", lngCurrentID);
				}
				else
				{
					clsSave.Set_Fields("RecordNumber", lngRecordNumber);
				}
				if (cboReportType.SelectedIndex > 0)
				{
					clsSave.Set_Fields("ReportType", cboReportType.ItemData(cboReportType.SelectedIndex));
				}
				else
				{
					clsSave.Set_Fields("ReportType", modCoreysSweeterCode.CNSTMSRSREPORTTYPEPLD);
				}
				clsSave.Set_Fields("LastName", fecherFoundation.Strings.Trim(txtLastName.Text));
				clsSave.Set_Fields("FirstName", fecherFoundation.Strings.Trim(txtFirstName.Text));
				
                if (ssnViewPermission == SSNViewType.Full)
                {
                    strTemp = txtSocialSecurity.Text;
                    strTemp = strTemp.Replace("-", "");
                    clsSave.Set_Fields("SocialSecurityNumber", strTemp);
                }

                if (!Information.IsDate(t2kDateOfBirth.Text))
				{
					clsSave.Set_Fields("Dateofbirth", 0);
				}
				else
				{
					clsSave.Set_Fields("Dateofbirth", t2kDateOfBirth.Text);
				}
				if (!Information.IsDate(t2kDateHired.Text))
				{
					clsSave.Set_Fields("DateOfHire", 0);
				}
				else
				{
					clsSave.Set_Fields("DateofHire", t2kDateHired.Text);
				}
				if (!Information.IsDate(t2kTerminationDate.Text))
				{
					clsSave.Set_Fields("DateTerminated", 0);
				}
				else
				{
					clsSave.Set_Fields("DateTerminated", t2kTerminationDate.Text);
				}
				if (Strings.Left(cmbStatus.Text + " ", 1) == "R")
				{
					// retired
					clsSave.Set_Fields("Status", "R");
				}
				else if (Strings.Left(cmbStatus.Text + " ", 1) == "T")
				{
					// terminated
					clsSave.Set_Fields("status", "T");
				}
				else
				{
					// active
					clsSave.Set_Fields("status", "A");
				}
				if (chkInclude.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("Include", true);
				}
				else
				{
					clsSave.Set_Fields("Include", false);
				}

				clsSave.Set_Fields("seqnumber", FCConvert.ToString(Conversion.Val(txtSequence.Text)));
				clsSave.Set_Fields("MSRSJobCategory", cmbPositionCategory.ItemData(cmbPositionCategory.SelectedIndex));
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(0, 1)) == string.Empty)
				{
					clsSave.Set_Fields("LifeInsuranceCode", "B");
				}
				else
				{
					clsSave.Set_Fields("LifeInsuranceCode", fecherFoundation.Strings.Trim(Grid.TextMatrix(0, 1)));
				}
				clsSave.Set_Fields("StatusCode", fecherFoundation.Strings.Trim(Grid.TextMatrix(1, 1)));
				clsSave.Set_Fields("PositionCode", fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.TextMatrix(2, 1))));
				clsSave.Set_Fields("PlanCode", fecherFoundation.Strings.Trim(Grid.TextMatrix(3, 1)));
				clsSave.Set_Fields("LifeInsuranceScheduleEP", fecherFoundation.Strings.Trim(Grid.TextMatrix(4, 1)));
				clsSave.Set_Fields("LifeInsuranceScheduleBZ", fecherFoundation.Strings.Trim(Grid.TextMatrix(5, 1)));
				clsSave.Set_Fields("LifeInsuranceLevel", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(6, 1))));
				clsSave.Set_Fields("FederalCompAmount", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(7, 1))));
				clsSave.Set_Fields("MonthlyHours", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(8, 1))));
				clsSave.Set_Fields("WorkWeeksPerYear", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(9, 1))));
				clsSave.Set_Fields("PaybackAmount", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(10, 1))));
				clsSave.Set_Fields("HoursMTD", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(11, 1))));
				clsSave.Set_Fields("GrossMTD", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(12, 1))));
				clsSave.Set_Fields("GrossCalendar", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(13, 1))));
				clsSave.Set_Fields("PayRate", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(14, 1))));
				clsSave.Set_Fields("RetirementContributions", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(15, 1))));
				clsSave.Set_Fields("InsuranceContributions", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(16, 1))));
				clsSave.Set_Fields("Participation", Grid.TextMatrix(17, 1));
				clsSave.Set_Fields("Schedule", Grid.TextMatrix(18, 1));
				clsSave.Update();
				SaveEmployee = true;
				MessageBox.Show("Employee saved successfully", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveEmployee;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveEmployee", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveEmployee;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.7 * GridWidth));
			//Grid.Height = Grid.Rows * Grid.RowHeight(0) + 50;
		}

		private void SetupGrid()
		{
			Grid.Rows = 19;
			Grid.TextMatrix(0, 0, "Life Insurance Code");
			Grid.TextMatrix(1, 0, "Status Code");
			Grid.TextMatrix(2, 0, "Position Code");
			Grid.TextMatrix(3, 0, "Plan Code");
			Grid.TextMatrix(4, 0, "Life Ins Sched E/P");
			Grid.TextMatrix(5, 0, "Life Ins Sched B-Z");
			Grid.TextMatrix(6, 0, "Life Insurance Level");
			Grid.TextMatrix(7, 0, "Federal Comp Amount");
			Grid.TextMatrix(8, 0, "Monthly Hours");
			Grid.TextMatrix(9, 0, "Work Weeks Per Year");
			Grid.TextMatrix(10, 0, "Payback Amount");
			Grid.TextMatrix(11, 0, "Hours MTD");
			Grid.TextMatrix(12, 0, "Gross MTD");
			Grid.TextMatrix(13, 0, "Gross Calendar");
			Grid.TextMatrix(14, 0, "Pay Rate");
			Grid.TextMatrix(15, 0, "Retirement Cont");
			Grid.TextMatrix(16, 0, "Insurance Cont");
			Grid.TextMatrix(17, 0, "Participation");
			Grid.TextMatrix(18, 0, "Schedule");
			Grid.Row = 0;
			Grid.Col = 1;
			FillDescription(0);
		}

		private void FillCombo(ref int intRow)
		{
		}
		// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
		private void FillDescription(int intRow)
		{
			string strDescription;
			strDescription = "";
			switch (intRow)
			{
				case 0:
					{
						strDescription = "B = Basic" + "\r\n" + "HA,HB = Basic + Dep A or Dep B" + "\r\n";
						strDescription += "S1,S2,S3 = Basic + Supp 1/2/3" + "\r\n" + "F1A,F2A,F3A = Basic + Supp 1/2/3 And Dep A" + "\r\n";
						strDescription += "F1B,F2B,F3B = Basic + Supp 1/2/3 And Dep B" + "\r\n";
						strDescription += "I = Ineligible" + "\r\n" + "R = Refused" + "\r\n" + "OC = Other Coverage";
						break;
					}
				case 1:
					{
						strDescription = "Code Specified To Indicate" + "\r\n" + "Employee's Status.";
						strDescription += "\r\n" + "Examples:" + "\r\n" + "11 = Full Time" + "\r\n" + "12 = Part Time";
						strDescription += "\r\n" + "14 = Seasonal" + "\r\n" + "15 = Seasonal Part Time";
						strDescription += "\r\n" + "17 = Project/Intermittent";
						// strDescription = strDescription & vbNewLine & "1A = Full Time Limited Period" & vbNewLine & "1B = Part Time Limited Period"
						strDescription += "\r\n" + "52 = Insurance Only - Retired" + "\r\n" + "53 = Retired-Returned / Work";
						strDescription += "\r\n" + "65 = Insurance Only - Active" + "\r\n";
						// strDescription = strDescription & "67 = Part Time-Insurance Only"
						break;
					}
				case 2:
					{
						strDescription = "Examples:" + "\r\n" + "Y0101 = Classroom Teacher" + "\r\n";
						strDescription += "Y0105 = Special Ed Teacher" + "\r\n" + "Y0206 = Ed Tech III";
						strDescription += "\r\n" + "09901 = Member With Or W/O Ins" + "\r\n" + "09904 = Police Officer";
						break;
					}
				case 3:
					{
						strDescription = "-- = No Plan Code" + "\r\n" + "AC = Regular Plan A" + "\r\n";
						strDescription += "BC = Regular Plan B" + "\r\n" + "AN = Regular Plan A-1" + "\r\n";
						strDescription += "1C-4C = Spec Plan 1 thru 4" + "\r\n" + "1N-4N = Spec Plan 1-A thru 4-A";
						break;
					}
				case 4:
					{
						strDescription = "E = Excess Contributions" + "\r\n" + "P = Re/Purchase Of Time" + "\r\n" + "- = Neither";
						break;
					}
				case 5:
					{
						break;
					}
				case 6:
					{
						strDescription = "Life Insurance Coverage Based" + "\r\n" + "On Total Gross Compensation" + "\r\n" + "In The Previous Calender Year.";
						break;
					}
				case 7:
					{
						break;
					}
				case 8:
					{
						break;
					}
				case 9:
					{
						strDescription = "Enter Number Of Weeks Per Year" + "\r\n" + "Considered Full Time For This Position";
						break;
					}
				case 10:
					{
						break;
					}
				case 11:
					{
						break;
					}
				case 12:
					{
						break;
					}
				case 13:
					{
						break;
					}
				case 14:
					{
						break;
					}
				case 15:
					{
						break;
					}
				case 16:
					{
						break;
					}
			}
			//end switch
			lblDescription.Text = strDescription;
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			// clears out the form
			boolSearchMode = false;
			SetFormMode();
			ClearForm();
		}

		private void ClearForm()
		{
			// clears the form
			int x;
			lngCurrentID = 0;
			txtLastName.Text = "";
			txtFirstName.Text = "";
			txtSocialSecurity.Text = "";
			t2kDateHired.Text = "00/00/0000";
			t2kTerminationDate.Text = "00/00/0000";
			t2kDateOfBirth.Text = "00/00/0000";
			chkInclude.CheckState = Wisej.Web.CheckState.Checked;
			cmbStatus.SelectedIndex = 0;
			txtRecordNumber.Text = "";
			Grid.TextMatrix(0, 1, "");
			Grid.TextMatrix(1, 1, "11");
			Grid.TextMatrix(2, 1, "");
			Grid.TextMatrix(3, 1, "--");
			Grid.TextMatrix(4, 1, "-");
			Grid.TextMatrix(5, 1, "");
			for (x = 6; x <= 16; x++)
			{
				Grid.TextMatrix(x, 1, "");
			}
			// x
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//App.DoEvents();
			SaveEmployee();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//App.DoEvents();
			if (SaveEmployee())
			{
				mnuSearch_Click();
			}
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			boolSearchMode = true;
			GridSearch.Rows = 1;
			FillGridSearch();
			SetFormMode();
		}

		public void mnuSearch_Click()
		{
			mnuSearch_Click(mnuSearch, new System.EventArgs());
		}

		private void txtSocialSecurity_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strTemp = "";
			if (txtSocialSecurity.Text.Length == 9)
			{
				strTemp = txtSocialSecurity.Text;
				txtSocialSecurity.Text = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6);
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkInclude";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Include in MSRS";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtLastName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Last Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFirstName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "First Name";
			intTotalNumberOfControls += 1;			
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "t2kDateHired";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Date Hired";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "t2kDateOfBirth";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Date of Birth";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRecordNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "RecordNumber";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtSequence";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Sequence";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cmbStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Status";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboReportType";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Report Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cmbPositionCategory";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Position Category";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE Instantiate control
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "t2kTerminationDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Date Terminated";
			intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			int intCols;
			for (intRows = 0; intRows <= (Grid.Rows - 1); intRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				//FC:FINAL:DSE Instantiate control
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "Grid";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = 1;
				clsControlInfo[intTotalNumberOfControls].DataDescription = Grid.TextMatrix(intRows, 0);
				intTotalNumberOfControls += 1;
			}
		}
	}
}
