﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptNewDBLLaserStub1.
	/// </summary>
	partial class srptNewDBLLaserStub1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptNewDBLLaserStub1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtEmployeeNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentDeductions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFICA = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtChkAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepChkLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepSav = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositSavLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label139 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label144 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label145 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label146 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label147 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label148 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label149 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label150 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label153 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label154 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label156 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label157 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label158 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label160 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label161 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label162 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label163 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label164 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label165 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label166 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label167 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEMatchCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmatchYTD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label205 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label211 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label213 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label214 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label215 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label216 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label218 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label220 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label221 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label222 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVacationBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSickBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label204 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label139)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label144)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label145)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label146)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label147)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label148)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label149)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label150)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label151)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label153)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label155)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label157)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label205)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label218)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label220)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label221)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label222)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNo,
				this.txtName,
				this.txtCheckNo,
				this.txtPay,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.txtCurrentGross,
				this.Label11,
				this.txtCurrentDeductions,
				this.txtCurrentNet,
				this.Label18,
				this.Label19,
				this.Label20,
				this.txtCurrentFed,
				this.txtCurrentFica,
				this.txtCurrentState,
				this.txtPayDesc1,
				this.txtHours1,
				this.txtPayAmount1,
				this.txtDedDesc1,
				this.txtDedAmount1,
				this.txtDedYTD1,
				this.txtYTDFed,
				this.txtYTDFICA,
				this.txtYTDState,
				this.txtDirectDepositLabel,
				this.txtDirectDeposit,
				this.txtChkAmount,
				this.lblCheckAmount,
				this.lblPayRate,
				this.txtPayRate,
				this.txtDate,
				this.txtDirectDepChkLabel,
				this.txtDirectDepSav,
				this.txtDirectDepositSavLabel,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label36,
				this.Label37,
				this.Label38,
				this.Line1,
				this.Label56,
				this.Label57,
				this.Label58,
				this.Label59,
				this.Label60,
				this.Line2,
				this.Label63,
				this.Label64,
				this.Label65,
				this.Label66,
				this.Label67,
				this.Label68,
				this.Label69,
				this.Label70,
				this.Label71,
				this.Label72,
				this.Label73,
				this.Label74,
				this.Label75,
				this.Label76,
				this.Label77,
				this.Label78,
				this.Label79,
				this.Label80,
				this.Label81,
				this.Label82,
				this.Label83,
				this.Label84,
				this.Label85,
				this.Label86,
				this.Label87,
				this.Label88,
				this.Label89,
				this.Label90,
				this.Label91,
				this.Label92,
				this.Label93,
				this.Label94,
				this.Label95,
				this.Label96,
				this.Label97,
				this.Label98,
				this.Label99,
				this.Label100,
				this.Label101,
				this.Label102,
				this.Label103,
				this.Label104,
				this.Label105,
				this.Label106,
				this.Label107,
				this.Label108,
				this.Label109,
				this.Label110,
				this.Label111,
				this.Label112,
				this.Label113,
				this.Label114,
				this.Label115,
				this.Label116,
				this.Label117,
				this.Label118,
				this.Label119,
				this.Label120,
				this.Label121,
				this.Label122,
				this.Label123,
				this.Label124,
				this.Label125,
				this.Label126,
				this.Label127,
				this.Label128,
				this.Label129,
				this.Label130,
				this.Label131,
				this.Label132,
				this.Label133,
				this.Label134,
				this.Label135,
				this.Label136,
				this.Label137,
				this.Label138,
				this.Label139,
				this.Label140,
				this.Label141,
				this.Label142,
				this.Label143,
				this.Label144,
				this.Label145,
				this.Label146,
				this.Label147,
				this.Label148,
				this.Label149,
				this.Label150,
				this.Label151,
				this.Label152,
				this.Label153,
				this.Label154,
				this.Label155,
				this.Label156,
				this.Label157,
				this.Label158,
				this.Label159,
				this.Label160,
				this.Label161,
				this.Label162,
				this.Label163,
				this.Label164,
				this.Label165,
				this.Label166,
				this.Label167,
				this.Label168,
				this.Label169,
				this.Label170,
				this.Label171,
				this.Label172,
				this.Label173,
				this.Label174,
				this.Label175,
				this.Label176,
				this.Label177,
				this.Label178,
				this.Label179,
				this.txtTotalHours,
				this.txtTotalAmount,
				this.txtEMatchCurrent,
				this.txtEmatchYTD,
				this.lblCheckMessage,
				this.Line3,
				this.Label205,
				this.Line4,
				this.Label206,
				this.Line5,
				this.Line6,
				this.Label209,
				this.Line7,
				this.Line8,
				this.Label210,
				this.Label211,
				this.Label212,
				this.Label213,
				this.Label214,
				this.Label215,
				this.Label216,
				this.Label217,
				this.Label218,
				this.Label219,
				this.Label220,
				this.Label221,
				this.Label222,
				this.Line9,
				this.Label13,
				this.Label14,
				this.txtVacationBalance,
				this.txtSickBalance,
				this.Label29,
				this.txtOtherBalance,
				this.lblCode1,
				this.lblCode2,
				this.lblCode1Balance,
				this.lblCode2Balance,
				this.lblCode3,
				this.lblCode3Balance,
				this.Label39,
				this.Label40,
				this.Label41,
				this.Label42,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Label48,
				this.Label49,
				this.Label51,
				this.Label52,
				this.Label53,
				this.Label204
			});
			this.Detail.Height = 6.010417F;
			this.Detail.Name = "Detail";
			// 
			// txtEmployeeNo
			// 
			this.txtEmployeeNo.Height = 0.1666667F;
			this.txtEmployeeNo.HyperLink = null;
			this.txtEmployeeNo.Left = 0.1875F;
			this.txtEmployeeNo.Name = "txtEmployeeNo";
			this.txtEmployeeNo.Style = "text-align: left";
			this.txtEmployeeNo.Tag = "text";
			this.txtEmployeeNo.Text = "Employee ID";
			this.txtEmployeeNo.Top = 0.1666667F;
			this.txtEmployeeNo.Width = 1.1875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.HyperLink = null;
			this.txtName.Left = 1.4375F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "";
			this.txtName.Tag = "text";
			this.txtName.Text = "EMPLOYEE";
			this.txtName.Top = 0.1666667F;
			this.txtName.Width = 2.875F;
			// 
			// txtCheckNo
			// 
			this.txtCheckNo.Height = 0.1666667F;
			this.txtCheckNo.HyperLink = null;
			this.txtCheckNo.Left = 5.875F;
			this.txtCheckNo.Name = "txtCheckNo";
			this.txtCheckNo.Style = "text-align: right";
			this.txtCheckNo.Tag = "text";
			this.txtCheckNo.Text = "CHECK";
			this.txtCheckNo.Top = 0.1666667F;
			this.txtCheckNo.Width = 1.375F;
			// 
			// txtPay
			// 
			this.txtPay.Height = 0.1666667F;
			this.txtPay.HyperLink = null;
			this.txtPay.Left = 0.1875F;
			this.txtPay.Name = "txtPay";
			this.txtPay.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.txtPay.Tag = "text";
			this.txtPay.Text = "PAY";
			this.txtPay.Top = 0.625F;
			this.txtPay.Width = 1F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label1.Tag = "text";
			this.Label1.Text = "HOURS";
			this.Label1.Top = 0.625F;
			this.Label1.Width = 0.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label2.Tag = "text";
			this.Label2.Text = "AMOUNT";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label3.Tag = "text";
			this.Label3.Text = "Deduction";
			this.Label3.Top = 2.375F;
			this.Label3.Width = 1.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label4.Tag = "text";
			this.Label4.Text = "Amount";
			this.Label4.Top = 2.375F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.4375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label5.Tag = "text";
			this.Label5.Text = "YTD";
			this.Label5.Top = 2.375F;
			this.Label5.Width = 0.6875F;
			// 
			// txtCurrentGross
			// 
			this.txtCurrentGross.Height = 0.1666667F;
			this.txtCurrentGross.HyperLink = null;
			this.txtCurrentGross.Left = 2.40625F;
			this.txtCurrentGross.Name = "txtCurrentGross";
			this.txtCurrentGross.Style = "text-align: right";
			this.txtCurrentGross.Tag = "text";
			this.txtCurrentGross.Text = "0.00";
			this.txtCurrentGross.Top = 5.489583F;
			this.txtCurrentGross.Width = 0.6875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.1875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label11.Tag = "text";
			this.Label11.Text = "Net";
			this.Label11.Top = 5.791667F;
			this.Label11.Width = 0.375F;
			// 
			// txtCurrentDeductions
			// 
			this.txtCurrentDeductions.Height = 0.1666667F;
			this.txtCurrentDeductions.HyperLink = null;
			this.txtCurrentDeductions.Left = 1.677083F;
			this.txtCurrentDeductions.Name = "txtCurrentDeductions";
			this.txtCurrentDeductions.Style = "text-align: right";
			this.txtCurrentDeductions.Tag = "text";
			this.txtCurrentDeductions.Text = "0.00";
			this.txtCurrentDeductions.Top = 5.21875F;
			this.txtCurrentDeductions.Width = 0.75F;
			// 
			// txtCurrentNet
			// 
			this.txtCurrentNet.Height = 0.1666667F;
			this.txtCurrentNet.HyperLink = null;
			this.txtCurrentNet.Left = 6.625F;
			this.txtCurrentNet.Name = "txtCurrentNet";
			this.txtCurrentNet.Style = "text-align: right";
			this.txtCurrentNet.Tag = "text";
			this.txtCurrentNet.Text = "0.00";
			this.txtCurrentNet.Top = 5.78125F;
			this.txtCurrentNet.Width = 0.75F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1388889F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.895833F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 8.5pt; text-align: left";
			this.Label18.Tag = "text";
			this.Label18.Text = "FIT";
			this.Label18.Top = 3.697917F;
			this.Label18.Width = 0.75F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1354167F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.895833F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 8.5pt; text-align: left";
			this.Label19.Tag = "text";
			this.Label19.Text = "FICA";
			this.Label19.Top = 3.833333F;
			this.Label19.Width = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1354167F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3.895833F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 8.5pt; text-align: left";
			this.Label20.Tag = "text";
			this.Label20.Text = "SWT-ME";
			this.Label20.Top = 4.114583F;
			this.Label20.Width = 0.75F;
			// 
			// txtCurrentFed
			// 
			this.txtCurrentFed.Height = 0.1388889F;
			this.txtCurrentFed.HyperLink = null;
			this.txtCurrentFed.Left = 4.645833F;
			this.txtCurrentFed.Name = "txtCurrentFed";
			this.txtCurrentFed.Style = "text-align: right";
			this.txtCurrentFed.Tag = "text";
			this.txtCurrentFed.Text = "0.00";
			this.txtCurrentFed.Top = 3.697917F;
			this.txtCurrentFed.Width = 0.75F;
			// 
			// txtCurrentFica
			// 
			this.txtCurrentFica.Height = 0.1354167F;
			this.txtCurrentFica.HyperLink = null;
			this.txtCurrentFica.Left = 4.645833F;
			this.txtCurrentFica.Name = "txtCurrentFica";
			this.txtCurrentFica.Style = "text-align: right";
			this.txtCurrentFica.Tag = "text";
			this.txtCurrentFica.Text = "0.00";
			this.txtCurrentFica.Top = 3.833333F;
			this.txtCurrentFica.Width = 0.75F;
			// 
			// txtCurrentState
			// 
			this.txtCurrentState.Height = 0.1354167F;
			this.txtCurrentState.HyperLink = null;
			this.txtCurrentState.Left = 4.645833F;
			this.txtCurrentState.Name = "txtCurrentState";
			this.txtCurrentState.Style = "text-align: right";
			this.txtCurrentState.Tag = "text";
			this.txtCurrentState.Text = "0.00";
			this.txtCurrentState.Top = 4.114583F;
			this.txtCurrentState.Width = 0.75F;
			// 
			// txtPayDesc1
			// 
			this.txtPayDesc1.Height = 0.1388889F;
			this.txtPayDesc1.HyperLink = null;
			this.txtPayDesc1.Left = 0.1875F;
			this.txtPayDesc1.Name = "txtPayDesc1";
			this.txtPayDesc1.Style = "text-align: left";
			this.txtPayDesc1.Tag = "text";
			this.txtPayDesc1.Text = "Regular";
			this.txtPayDesc1.Top = 0.7916667F;
			this.txtPayDesc1.Width = 1.489583F;
			// 
			// txtHours1
			// 
			this.txtHours1.Height = 0.1354167F;
			this.txtHours1.HyperLink = null;
			this.txtHours1.Left = 1.75F;
			this.txtHours1.Name = "txtHours1";
			this.txtHours1.Style = "text-align: right";
			this.txtHours1.Tag = "text";
			this.txtHours1.Text = "0.00";
			this.txtHours1.Top = 0.7916667F;
			this.txtHours1.Width = 0.6875F;
			// 
			// txtPayAmount1
			// 
			this.txtPayAmount1.Height = 0.1388889F;
			this.txtPayAmount1.HyperLink = null;
			this.txtPayAmount1.Left = 2.4375F;
			this.txtPayAmount1.Name = "txtPayAmount1";
			this.txtPayAmount1.Style = "text-align: right";
			this.txtPayAmount1.Tag = "text";
			this.txtPayAmount1.Text = "0.00";
			this.txtPayAmount1.Top = 0.7916667F;
			this.txtPayAmount1.Width = 0.6875F;
			// 
			// txtDedDesc1
			// 
			this.txtDedDesc1.Height = 0.1388889F;
			this.txtDedDesc1.HyperLink = null;
			this.txtDedDesc1.Left = 0.1875F;
			this.txtDedDesc1.Name = "txtDedDesc1";
			this.txtDedDesc1.Style = "text-align: left";
			this.txtDedDesc1.Tag = "text";
			this.txtDedDesc1.Text = "Deduction Name";
			this.txtDedDesc1.Top = 2.604167F;
			this.txtDedDesc1.Width = 1.4375F;
			// 
			// txtDedAmount1
			// 
			this.txtDedAmount1.Height = 0.1388889F;
			this.txtDedAmount1.HyperLink = null;
			this.txtDedAmount1.Left = 1.6875F;
			this.txtDedAmount1.Name = "txtDedAmount1";
			this.txtDedAmount1.Style = "text-align: right";
			this.txtDedAmount1.Tag = "text";
			this.txtDedAmount1.Text = "0.00";
			this.txtDedAmount1.Top = 2.604167F;
			this.txtDedAmount1.Width = 0.75F;
			// 
			// txtDedYTD1
			// 
			this.txtDedYTD1.Height = 0.1388889F;
			this.txtDedYTD1.HyperLink = null;
			this.txtDedYTD1.Left = 2.4375F;
			this.txtDedYTD1.Name = "txtDedYTD1";
			this.txtDedYTD1.Style = "text-align: right";
			this.txtDedYTD1.Tag = "text";
			this.txtDedYTD1.Text = "0.00";
			this.txtDedYTD1.Top = 2.604167F;
			this.txtDedYTD1.Width = 0.6875F;
			// 
			// txtYTDFed
			// 
			this.txtYTDFed.Height = 0.1388889F;
			this.txtYTDFed.HyperLink = null;
			this.txtYTDFed.Left = 5.53125F;
			this.txtYTDFed.Name = "txtYTDFed";
			this.txtYTDFed.Style = "text-align: right";
			this.txtYTDFed.Tag = "text";
			this.txtYTDFed.Text = "0.00";
			this.txtYTDFed.Top = 3.697917F;
			this.txtYTDFed.Width = 0.75F;
			// 
			// txtYTDFICA
			// 
			this.txtYTDFICA.Height = 0.1354167F;
			this.txtYTDFICA.HyperLink = null;
			this.txtYTDFICA.Left = 5.489583F;
			this.txtYTDFICA.Name = "txtYTDFICA";
			this.txtYTDFICA.Style = "text-align: right";
			this.txtYTDFICA.Tag = "text";
			this.txtYTDFICA.Text = "0.00";
			this.txtYTDFICA.Top = 3.833333F;
			this.txtYTDFICA.Width = 0.8125F;
			// 
			// txtYTDState
			// 
			this.txtYTDState.Height = 0.1354167F;
			this.txtYTDState.HyperLink = null;
			this.txtYTDState.Left = 5.489583F;
			this.txtYTDState.Name = "txtYTDState";
			this.txtYTDState.Style = "text-align: right";
			this.txtYTDState.Tag = "text";
			this.txtYTDState.Text = "0.00";
			this.txtYTDState.Top = 4.114583F;
			this.txtYTDState.Width = 0.8125F;
			// 
			// txtDirectDepositLabel
			// 
			this.txtDirectDepositLabel.Height = 0.1666667F;
			this.txtDirectDepositLabel.HyperLink = null;
			this.txtDirectDepositLabel.Left = 3.90625F;
			this.txtDirectDepositLabel.Name = "txtDirectDepositLabel";
			this.txtDirectDepositLabel.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.txtDirectDepositLabel.Tag = "text";
			this.txtDirectDepositLabel.Text = "DIRECT DEPOSIT";
			this.txtDirectDepositLabel.Top = 0.4270833F;
			this.txtDirectDepositLabel.Width = 1.4375F;
			// 
			// txtDirectDeposit
			// 
			this.txtDirectDeposit.Height = 0.1388889F;
			this.txtDirectDeposit.HyperLink = null;
			this.txtDirectDeposit.Left = 4.5625F;
			this.txtDirectDeposit.Name = "txtDirectDeposit";
			this.txtDirectDeposit.Style = "text-align: right";
			this.txtDirectDeposit.Tag = "text";
			this.txtDirectDeposit.Text = "0.00";
			this.txtDirectDeposit.Top = 0.5833333F;
			this.txtDirectDeposit.Width = 0.75F;
			// 
			// txtChkAmount
			// 
			this.txtChkAmount.Height = 0.1354167F;
			this.txtChkAmount.HyperLink = null;
			this.txtChkAmount.Left = 4.625F;
			this.txtChkAmount.Name = "txtChkAmount";
			this.txtChkAmount.Style = "text-align: right";
			this.txtChkAmount.Tag = "text";
			this.txtChkAmount.Text = "0.00";
			this.txtChkAmount.Top = 0.8541667F;
			this.txtChkAmount.Width = 0.6875F;
			// 
			// lblCheckAmount
			// 
			this.lblCheckAmount.Height = 0.1388889F;
			this.lblCheckAmount.HyperLink = null;
			this.lblCheckAmount.Left = 3.864583F;
			this.lblCheckAmount.Name = "lblCheckAmount";
			this.lblCheckAmount.Style = "text-align: left";
			this.lblCheckAmount.Tag = "text";
			this.lblCheckAmount.Text = "Negotiable";
			this.lblCheckAmount.Top = 0.8541667F;
			this.lblCheckAmount.Width = 0.6458333F;
			// 
			// lblPayRate
			// 
			this.lblPayRate.Height = 0.1666667F;
			this.lblPayRate.HyperLink = null;
			this.lblPayRate.Left = 0.1875F;
			this.lblPayRate.Name = "lblPayRate";
			this.lblPayRate.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.lblPayRate.Tag = "text";
			this.lblPayRate.Text = "Pay Rate";
			this.lblPayRate.Top = 0.3958333F;
			this.lblPayRate.Width = 0.875F;
			// 
			// txtPayRate
			// 
			this.txtPayRate.Height = 0.1666667F;
			this.txtPayRate.HyperLink = null;
			this.txtPayRate.Left = 1.0625F;
			this.txtPayRate.Name = "txtPayRate";
			this.txtPayRate.Style = "text-align: right";
			this.txtPayRate.Tag = "text";
			this.txtPayRate.Text = "0.00";
			this.txtPayRate.Top = 0.3958333F;
			this.txtPayRate.Width = 0.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 4.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "text";
			this.txtDate.Text = "DATE";
			this.txtDate.Top = 0.1666667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtDirectDepChkLabel
			// 
			this.txtDirectDepChkLabel.Height = 0.1388889F;
			this.txtDirectDepChkLabel.HyperLink = null;
			this.txtDirectDepChkLabel.Left = 3.875F;
			this.txtDirectDepChkLabel.Name = "txtDirectDepChkLabel";
			this.txtDirectDepChkLabel.Style = "text-align: left";
			this.txtDirectDepChkLabel.Tag = "text";
			this.txtDirectDepChkLabel.Text = "Checking";
			this.txtDirectDepChkLabel.Top = 0.5833333F;
			this.txtDirectDepChkLabel.Width = 0.625F;
			// 
			// txtDirectDepSav
			// 
			this.txtDirectDepSav.Height = 0.1388889F;
			this.txtDirectDepSav.HyperLink = null;
			this.txtDirectDepSav.Left = 4.5625F;
			this.txtDirectDepSav.Name = "txtDirectDepSav";
			this.txtDirectDepSav.Style = "text-align: right";
			this.txtDirectDepSav.Tag = "text";
			this.txtDirectDepSav.Text = "0.00";
			this.txtDirectDepSav.Top = 0.7083333F;
			this.txtDirectDepSav.Width = 0.75F;
			// 
			// txtDirectDepositSavLabel
			// 
			this.txtDirectDepositSavLabel.Height = 0.1388889F;
			this.txtDirectDepositSavLabel.HyperLink = null;
			this.txtDirectDepositSavLabel.Left = 3.875F;
			this.txtDirectDepositSavLabel.Name = "txtDirectDepositSavLabel";
			this.txtDirectDepositSavLabel.Style = "text-align: left";
			this.txtDirectDepositSavLabel.Tag = "text";
			this.txtDirectDepositSavLabel.Text = "Savings";
			this.txtDirectDepositSavLabel.Top = 0.71875F;
			this.txtDirectDepositSavLabel.Width = 0.625F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1770833F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 4.6875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label30.Text = "Current";
			this.Label30.Top = 3.46875F;
			this.Label30.Width = 0.7083333F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1770833F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 5.291667F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label31.Text = "YTD";
			this.Label31.Top = 3.458333F;
			this.Label31.Width = 0.9791667F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1770833F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 6.302083F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label32.Text = "YTD Taxable";
			this.Label32.Top = 3.447917F;
			this.Label32.Width = 0.9791667F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1354167F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 6.479167F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "text-align: right";
			this.Label36.Tag = "text";
			this.Label36.Text = "0.00";
			this.Label36.Top = 3.697917F;
			this.Label36.Width = 0.75F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1354167F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 6.479167F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "text-align: right";
			this.Label37.Tag = "text";
			this.Label37.Text = "0.00";
			this.Label37.Top = 3.833333F;
			this.Label37.Width = 0.75F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1354167F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 6.479167F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "text-align: right";
			this.Label38.Tag = "text";
			this.Label38.Text = "0.00";
			this.Label38.Top = 4.114583F;
			this.Label38.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.09375F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 5.75F;
			this.Line1.Width = 7.28125F;
			this.Line1.X1 = 0.09375F;
			this.Line1.X2 = 7.375F;
			this.Line1.Y1 = 5.75F;
			this.Line1.Y2 = 5.75F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1388889F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 3.3125F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label56.Tag = "text";
			this.Label56.Text = "Totals";
			this.Label56.Top = 5.541667F;
			this.Label56.Width = 0.75F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1354167F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 3.895833F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-size: 8.5pt; text-align: left";
			this.Label57.Tag = "text";
			this.Label57.Text = "MC";
			this.Label57.Top = 3.979167F;
			this.Label57.Width = 0.75F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1354167F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 4.645833F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "text-align: right";
			this.Label58.Tag = "text";
			this.Label58.Text = "0.00";
			this.Label58.Top = 3.979167F;
			this.Label58.Width = 0.75F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1354167F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 5.489583F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "text-align: right";
			this.Label59.Tag = "text";
			this.Label59.Text = "0.00";
			this.Label59.Top = 3.979167F;
			this.Label59.Width = 0.8125F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1354167F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 6.479167F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "text-align: right";
			this.Label60.Tag = "text";
			this.Label60.Text = "0.00";
			this.Label60.Top = 3.979167F;
			this.Label60.Width = 0.75F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.916667F;
			this.Line2.LineWeight = 2F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 3.677083F;
			this.Line2.Width = 3.260417F;
			this.Line2.X1 = 3.916667F;
			this.Line2.X2 = 7.177083F;
			this.Line2.Y1 = 3.677083F;
			this.Line2.Y2 = 3.677083F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1388889F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 0.1875F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "text-align: left";
			this.Label63.Tag = "text";
			this.Label63.Text = "Regular";
			this.Label63.Top = 0.9166667F;
			this.Label63.Width = 1.489583F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1354167F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 1.75F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "text-align: right";
			this.Label64.Tag = "text";
			this.Label64.Text = "0.00";
			this.Label64.Top = 0.9166667F;
			this.Label64.Width = 0.6875F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.1354167F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 2.4375F;
			this.Label65.Name = "Label65";
			this.Label65.Style = "text-align: right";
			this.Label65.Tag = "text";
			this.Label65.Text = "0.00";
			this.Label65.Top = 0.9166667F;
			this.Label65.Width = 0.6875F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1388889F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 0.1875F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "text-align: left";
			this.Label66.Tag = "text";
			this.Label66.Text = "Regular";
			this.Label66.Top = 1.041667F;
			this.Label66.Width = 1.489583F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1354167F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 1.75F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "text-align: right";
			this.Label67.Tag = "text";
			this.Label67.Text = "0.00";
			this.Label67.Top = 1.041667F;
			this.Label67.Width = 0.6875F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1354167F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 2.4375F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "text-align: right";
			this.Label68.Tag = "text";
			this.Label68.Text = "0.00";
			this.Label68.Top = 1.041667F;
			this.Label68.Width = 0.6875F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1388889F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.1875F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "text-align: left";
			this.Label69.Tag = "text";
			this.Label69.Text = "Regular";
			this.Label69.Top = 1.166667F;
			this.Label69.Width = 1.489583F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1354167F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 1.75F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "text-align: right";
			this.Label70.Tag = "text";
			this.Label70.Text = "0.00";
			this.Label70.Top = 1.166667F;
			this.Label70.Width = 0.6875F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1354167F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 2.4375F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "text-align: right";
			this.Label71.Tag = "text";
			this.Label71.Text = "0.00";
			this.Label71.Top = 1.166667F;
			this.Label71.Width = 0.6875F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1388889F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 0.1875F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "text-align: left";
			this.Label72.Tag = "text";
			this.Label72.Text = "Regular";
			this.Label72.Top = 1.291667F;
			this.Label72.Width = 1.489583F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1354167F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 1.75F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "text-align: right";
			this.Label73.Tag = "text";
			this.Label73.Text = "0.00";
			this.Label73.Top = 1.291667F;
			this.Label73.Width = 0.6875F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1354167F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 2.4375F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "text-align: right";
			this.Label74.Tag = "text";
			this.Label74.Text = "0.00";
			this.Label74.Top = 1.291667F;
			this.Label74.Width = 0.6875F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1388889F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 0.1875F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "text-align: left";
			this.Label75.Tag = "text";
			this.Label75.Text = "Regular";
			this.Label75.Top = 1.416667F;
			this.Label75.Width = 1.489583F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1354167F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 1.75F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "text-align: right";
			this.Label76.Tag = "text";
			this.Label76.Text = "0.00";
			this.Label76.Top = 1.416667F;
			this.Label76.Width = 0.6875F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1354167F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 2.4375F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "text-align: right";
			this.Label77.Tag = "text";
			this.Label77.Text = "0.00";
			this.Label77.Top = 1.416667F;
			this.Label77.Width = 0.6875F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1388889F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 6.6875F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "text-align: right";
			this.Label78.Tag = "text";
			this.Label78.Text = "0.00";
			this.Label78.Top = 0.5833333F;
			this.Label78.Width = 0.75F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1388889F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 6.75F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "text-align: right";
			this.Label79.Tag = "text";
			this.Label79.Text = "0.00";
			this.Label79.Top = 0.8333333F;
			this.Label79.Width = 0.6875F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1388889F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 5.427083F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "text-align: left";
			this.Label80.Tag = "text";
			this.Label80.Text = "Bank Three";
			this.Label80.Top = 0.8541667F;
			this.Label80.Width = 1.135417F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1388889F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 5.4375F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "text-align: left";
			this.Label81.Tag = "text";
			this.Label81.Text = "Bank One";
			this.Label81.Top = 0.5833333F;
			this.Label81.Width = 1.114583F;
			// 
			// Label82
			// 
			this.Label82.Height = 0.1388889F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 6.6875F;
			this.Label82.Name = "Label82";
			this.Label82.Style = "text-align: right";
			this.Label82.Tag = "text";
			this.Label82.Text = "0.00";
			this.Label82.Top = 0.7083333F;
			this.Label82.Width = 0.75F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1388889F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 5.4375F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "text-align: left";
			this.Label83.Tag = "text";
			this.Label83.Text = "Bank Two";
			this.Label83.Top = 0.71875F;
			this.Label83.Width = 1.114583F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1388889F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 0.1875F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "text-align: left";
			this.Label84.Tag = "text";
			this.Label84.Text = "Deduction Name";
			this.Label84.Top = 2.729167F;
			this.Label84.Width = 1.4375F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1388889F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 1.6875F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "text-align: right";
			this.Label85.Tag = "text";
			this.Label85.Text = "0.00";
			this.Label85.Top = 2.729167F;
			this.Label85.Width = 0.75F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1388889F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 2.4375F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "text-align: right";
			this.Label86.Tag = "text";
			this.Label86.Text = "0.00";
			this.Label86.Top = 2.729167F;
			this.Label86.Width = 0.6875F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1388889F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 0.1875F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "text-align: left";
			this.Label87.Tag = "text";
			this.Label87.Text = "Deduction Name";
			this.Label87.Top = 2.854167F;
			this.Label87.Width = 1.4375F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1388889F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 1.6875F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "text-align: right";
			this.Label88.Tag = "text";
			this.Label88.Text = "0.00";
			this.Label88.Top = 2.854167F;
			this.Label88.Width = 0.75F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1388889F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 2.4375F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "text-align: right";
			this.Label89.Tag = "text";
			this.Label89.Text = "0.00";
			this.Label89.Top = 2.854167F;
			this.Label89.Width = 0.6875F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1388889F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 0.1875F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "text-align: left";
			this.Label90.Tag = "text";
			this.Label90.Text = "Deduction Name";
			this.Label90.Top = 2.979167F;
			this.Label90.Width = 1.4375F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1388889F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 1.6875F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "text-align: right";
			this.Label91.Tag = "text";
			this.Label91.Text = "0.00";
			this.Label91.Top = 2.979167F;
			this.Label91.Width = 0.75F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1388889F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 2.4375F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "text-align: right";
			this.Label92.Tag = "text";
			this.Label92.Text = "0.00";
			this.Label92.Top = 2.979167F;
			this.Label92.Width = 0.6875F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1388889F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 0.1875F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "text-align: left";
			this.Label93.Tag = "text";
			this.Label93.Text = "Deduction Name";
			this.Label93.Top = 3.104167F;
			this.Label93.Width = 1.4375F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1388889F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 1.6875F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "text-align: right";
			this.Label94.Tag = "text";
			this.Label94.Text = "0.00";
			this.Label94.Top = 3.104167F;
			this.Label94.Width = 0.75F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1388889F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 2.4375F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "text-align: right";
			this.Label95.Tag = "text";
			this.Label95.Text = "0.00";
			this.Label95.Top = 3.104167F;
			this.Label95.Width = 0.6875F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1388889F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 0.1875F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "text-align: left";
			this.Label96.Tag = "text";
			this.Label96.Text = "Deduction Name";
			this.Label96.Top = 3.229167F;
			this.Label96.Width = 1.4375F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1388889F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 1.6875F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "text-align: right";
			this.Label97.Tag = "text";
			this.Label97.Text = "0.00";
			this.Label97.Top = 3.229167F;
			this.Label97.Width = 0.75F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1388889F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 2.4375F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "text-align: right";
			this.Label98.Tag = "text";
			this.Label98.Text = "0.00";
			this.Label98.Top = 3.229167F;
			this.Label98.Width = 0.6875F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1388889F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.1875F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "text-align: left";
			this.Label99.Tag = "text";
			this.Label99.Text = "Deduction Name";
			this.Label99.Top = 3.354167F;
			this.Label99.Width = 1.4375F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1388889F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 1.6875F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "text-align: right";
			this.Label100.Tag = "text";
			this.Label100.Text = "0.00";
			this.Label100.Top = 3.354167F;
			this.Label100.Width = 0.75F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1388889F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 2.4375F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "text-align: right";
			this.Label101.Tag = "text";
			this.Label101.Text = "0.00";
			this.Label101.Top = 3.354167F;
			this.Label101.Width = 0.6875F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1388889F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 0.1875F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "text-align: left";
			this.Label102.Tag = "text";
			this.Label102.Text = "Deduction Name";
			this.Label102.Top = 3.479167F;
			this.Label102.Width = 1.4375F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1388889F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 1.6875F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "text-align: right";
			this.Label103.Tag = "text";
			this.Label103.Text = "0.00";
			this.Label103.Top = 3.479167F;
			this.Label103.Width = 0.75F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1388889F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 2.4375F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "text-align: right";
			this.Label104.Tag = "text";
			this.Label104.Text = "0.00";
			this.Label104.Top = 3.479167F;
			this.Label104.Width = 0.6875F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1388889F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 0.1875F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "text-align: left";
			this.Label105.Tag = "text";
			this.Label105.Text = "Deduction Name";
			this.Label105.Top = 3.604167F;
			this.Label105.Width = 1.4375F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.1388889F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 1.6875F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "text-align: right";
			this.Label106.Tag = "text";
			this.Label106.Text = "0.00";
			this.Label106.Top = 3.604167F;
			this.Label106.Width = 0.75F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1388889F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 2.4375F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "text-align: right";
			this.Label107.Tag = "text";
			this.Label107.Text = "0.00";
			this.Label107.Top = 3.604167F;
			this.Label107.Width = 0.6875F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1388889F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 0.1875F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "text-align: left";
			this.Label108.Tag = "text";
			this.Label108.Text = "Deduction Name";
			this.Label108.Top = 3.729167F;
			this.Label108.Width = 1.4375F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.1388889F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 1.6875F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "text-align: right";
			this.Label109.Tag = "text";
			this.Label109.Text = "0.00";
			this.Label109.Top = 3.729167F;
			this.Label109.Width = 0.75F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1388889F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 2.4375F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "text-align: right";
			this.Label110.Tag = "text";
			this.Label110.Text = "0.00";
			this.Label110.Top = 3.729167F;
			this.Label110.Width = 0.6875F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1388889F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 0.1875F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "text-align: left";
			this.Label111.Tag = "text";
			this.Label111.Text = "Deduction Name";
			this.Label111.Top = 3.854167F;
			this.Label111.Width = 1.4375F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1388889F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 1.6875F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "text-align: right";
			this.Label112.Tag = "text";
			this.Label112.Text = "0.00";
			this.Label112.Top = 3.854167F;
			this.Label112.Width = 0.75F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1388889F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 2.4375F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "text-align: right";
			this.Label113.Tag = "text";
			this.Label113.Text = "0.00";
			this.Label113.Top = 3.854167F;
			this.Label113.Width = 0.6875F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1388889F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 0.1875F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "text-align: left";
			this.Label114.Tag = "text";
			this.Label114.Text = "Deduction Name";
			this.Label114.Top = 3.979167F;
			this.Label114.Width = 1.4375F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1388889F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 1.6875F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "text-align: right";
			this.Label115.Tag = "text";
			this.Label115.Text = "0.00";
			this.Label115.Top = 3.979167F;
			this.Label115.Width = 0.75F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1388889F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 2.4375F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "text-align: right";
			this.Label116.Tag = "text";
			this.Label116.Text = "0.00";
			this.Label116.Top = 3.979167F;
			this.Label116.Width = 0.6875F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1388889F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 0.1875F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "text-align: left";
			this.Label117.Tag = "text";
			this.Label117.Text = "Deduction Name";
			this.Label117.Top = 4.104167F;
			this.Label117.Width = 1.4375F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1388889F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 1.6875F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "text-align: right";
			this.Label118.Tag = "text";
			this.Label118.Text = "0.00";
			this.Label118.Top = 4.104167F;
			this.Label118.Width = 0.75F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1388889F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 2.4375F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "text-align: right";
			this.Label119.Tag = "text";
			this.Label119.Text = "0.00";
			this.Label119.Top = 4.104167F;
			this.Label119.Width = 0.6875F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1388889F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 0.1875F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "text-align: left";
			this.Label120.Tag = "text";
			this.Label120.Text = "Deduction Name";
			this.Label120.Top = 4.229167F;
			this.Label120.Width = 1.4375F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1388889F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 1.6875F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "text-align: right";
			this.Label121.Tag = "text";
			this.Label121.Text = "0.00";
			this.Label121.Top = 4.229167F;
			this.Label121.Width = 0.75F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1388889F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 2.4375F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "text-align: right";
			this.Label122.Tag = "text";
			this.Label122.Text = "0.00";
			this.Label122.Top = 4.229167F;
			this.Label122.Width = 0.6875F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1388889F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 0.1875F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "text-align: left";
			this.Label123.Tag = "text";
			this.Label123.Text = "Deduction Name";
			this.Label123.Top = 4.354167F;
			this.Label123.Width = 1.4375F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1388889F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 1.6875F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "text-align: right";
			this.Label124.Tag = "text";
			this.Label124.Text = "0.00";
			this.Label124.Top = 4.354167F;
			this.Label124.Width = 0.75F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1388889F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 2.4375F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "text-align: right";
			this.Label125.Tag = "text";
			this.Label125.Text = "0.00";
			this.Label125.Top = 4.354167F;
			this.Label125.Width = 0.6875F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1388889F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 0.1875F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "text-align: left";
			this.Label126.Tag = "text";
			this.Label126.Text = "Deduction Name";
			this.Label126.Top = 4.479167F;
			this.Label126.Width = 1.4375F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1388889F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 1.6875F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "text-align: right";
			this.Label127.Tag = "text";
			this.Label127.Text = "0.00";
			this.Label127.Top = 4.479167F;
			this.Label127.Width = 0.75F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1388889F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 2.4375F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "text-align: right";
			this.Label128.Tag = "text";
			this.Label128.Text = "0.00";
			this.Label128.Top = 4.479167F;
			this.Label128.Width = 0.6875F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1388889F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 0.1875F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "text-align: left";
			this.Label129.Tag = "text";
			this.Label129.Text = "Deduction Name";
			this.Label129.Top = 4.604167F;
			this.Label129.Width = 1.4375F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1388889F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 1.6875F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "text-align: right";
			this.Label130.Tag = "text";
			this.Label130.Text = "0.00";
			this.Label130.Top = 4.604167F;
			this.Label130.Width = 0.75F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1388889F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 2.4375F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "text-align: right";
			this.Label131.Tag = "text";
			this.Label131.Text = "0.00";
			this.Label131.Top = 4.604167F;
			this.Label131.Width = 0.6875F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1388889F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0.1875F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "text-align: left";
			this.Label132.Tag = "text";
			this.Label132.Text = "Deduction Name";
			this.Label132.Top = 4.729167F;
			this.Label132.Width = 1.4375F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1388889F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 1.6875F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "text-align: right";
			this.Label133.Tag = "text";
			this.Label133.Text = "0.00";
			this.Label133.Top = 4.729167F;
			this.Label133.Width = 0.75F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1388889F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 2.4375F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "text-align: right";
			this.Label134.Tag = "text";
			this.Label134.Text = "0.00";
			this.Label134.Top = 4.729167F;
			this.Label134.Width = 0.6875F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1388889F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 0.1875F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "text-align: left";
			this.Label135.Tag = "text";
			this.Label135.Text = "Deduction Name";
			this.Label135.Top = 4.854167F;
			this.Label135.Width = 1.4375F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1388889F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 1.6875F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "text-align: right";
			this.Label136.Tag = "text";
			this.Label136.Text = "0.00";
			this.Label136.Top = 4.854167F;
			this.Label136.Width = 0.75F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1388889F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 2.4375F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "text-align: right";
			this.Label137.Tag = "text";
			this.Label137.Text = "0.00";
			this.Label137.Top = 4.854167F;
			this.Label137.Width = 0.6875F;
			// 
			// Label138
			// 
			this.Label138.Height = 0.1388889F;
			this.Label138.HyperLink = null;
			this.Label138.Left = 0.1875F;
			this.Label138.Name = "Label138";
			this.Label138.Style = "text-align: left";
			this.Label138.Tag = "text";
			this.Label138.Text = "Deduction Name";
			this.Label138.Top = 4.979167F;
			this.Label138.Width = 1.4375F;
			// 
			// Label139
			// 
			this.Label139.Height = 0.1388889F;
			this.Label139.HyperLink = null;
			this.Label139.Left = 1.6875F;
			this.Label139.Name = "Label139";
			this.Label139.Style = "text-align: right";
			this.Label139.Tag = "text";
			this.Label139.Text = "0.00";
			this.Label139.Top = 4.979167F;
			this.Label139.Width = 0.75F;
			// 
			// Label140
			// 
			this.Label140.Height = 0.1388889F;
			this.Label140.HyperLink = null;
			this.Label140.Left = 2.4375F;
			this.Label140.Name = "Label140";
			this.Label140.Style = "font-family: \'Arial\'; font-size: 8pt; text-align: right";
			this.Label140.Tag = "text";
			this.Label140.Text = "0.00";
			this.Label140.Top = 4.979167F;
			this.Label140.Width = 0.6875F;
			// 
			// Label141
			// 
			this.Label141.Height = 0.1666667F;
			this.Label141.HyperLink = null;
			this.Label141.Left = 4.3125F;
			this.Label141.Name = "Label141";
			this.Label141.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label141.Tag = "text";
			this.Label141.Text = "Benefit";
			this.Label141.Top = 1.409722F;
			this.Label141.Width = 1.4375F;
			// 
			// Label142
			// 
			this.Label142.Height = 0.1666667F;
			this.Label142.HyperLink = null;
			this.Label142.Left = 5.8125F;
			this.Label142.Name = "Label142";
			this.Label142.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label142.Tag = "text";
			this.Label142.Text = "Amount";
			this.Label142.Top = 1.409722F;
			this.Label142.Width = 0.75F;
			// 
			// Label143
			// 
			this.Label143.Height = 0.1666667F;
			this.Label143.HyperLink = null;
			this.Label143.Left = 6.5625F;
			this.Label143.Name = "Label143";
			this.Label143.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label143.Tag = "text";
			this.Label143.Text = "YTD";
			this.Label143.Top = 1.409722F;
			this.Label143.Width = 0.6875F;
			// 
			// Label144
			// 
			this.Label144.Height = 0.1388889F;
			this.Label144.HyperLink = null;
			this.Label144.Left = 4.3125F;
			this.Label144.Name = "Label144";
			this.Label144.Style = "text-align: left";
			this.Label144.Tag = "text";
			this.Label144.Text = "Benefit Name";
			this.Label144.Top = 1.604167F;
			this.Label144.Width = 1.4375F;
			// 
			// Label145
			// 
			this.Label145.Height = 0.1388889F;
			this.Label145.HyperLink = null;
			this.Label145.Left = 5.8125F;
			this.Label145.Name = "Label145";
			this.Label145.Style = "text-align: right";
			this.Label145.Tag = "text";
			this.Label145.Text = "0.00";
			this.Label145.Top = 1.604167F;
			this.Label145.Width = 0.75F;
			// 
			// Label146
			// 
			this.Label146.Height = 0.1388889F;
			this.Label146.HyperLink = null;
			this.Label146.Left = 6.5625F;
			this.Label146.Name = "Label146";
			this.Label146.Style = "text-align: right";
			this.Label146.Tag = "text";
			this.Label146.Text = "0.00";
			this.Label146.Top = 1.604167F;
			this.Label146.Width = 0.6875F;
			// 
			// Label147
			// 
			this.Label147.Height = 0.1388889F;
			this.Label147.HyperLink = null;
			this.Label147.Left = 4.3125F;
			this.Label147.Name = "Label147";
			this.Label147.Style = "text-align: left";
			this.Label147.Tag = "text";
			this.Label147.Text = "Benefit Name";
			this.Label147.Top = 1.729167F;
			this.Label147.Width = 1.4375F;
			// 
			// Label148
			// 
			this.Label148.Height = 0.1388889F;
			this.Label148.HyperLink = null;
			this.Label148.Left = 5.8125F;
			this.Label148.Name = "Label148";
			this.Label148.Style = "text-align: right";
			this.Label148.Tag = "text";
			this.Label148.Text = "0.00";
			this.Label148.Top = 1.729167F;
			this.Label148.Width = 0.75F;
			// 
			// Label149
			// 
			this.Label149.Height = 0.1388889F;
			this.Label149.HyperLink = null;
			this.Label149.Left = 6.5625F;
			this.Label149.Name = "Label149";
			this.Label149.Style = "text-align: right";
			this.Label149.Tag = "text";
			this.Label149.Text = "0.00";
			this.Label149.Top = 1.729167F;
			this.Label149.Width = 0.6875F;
			// 
			// Label150
			// 
			this.Label150.Height = 0.1388889F;
			this.Label150.HyperLink = null;
			this.Label150.Left = 4.3125F;
			this.Label150.Name = "Label150";
			this.Label150.Style = "text-align: left";
			this.Label150.Tag = "text";
			this.Label150.Text = "Benefit Name";
			this.Label150.Top = 1.854167F;
			this.Label150.Width = 1.4375F;
			// 
			// Label151
			// 
			this.Label151.Height = 0.1388889F;
			this.Label151.HyperLink = null;
			this.Label151.Left = 5.8125F;
			this.Label151.Name = "Label151";
			this.Label151.Style = "text-align: right";
			this.Label151.Tag = "text";
			this.Label151.Text = "0.00";
			this.Label151.Top = 1.854167F;
			this.Label151.Width = 0.75F;
			// 
			// Label152
			// 
			this.Label152.Height = 0.1388889F;
			this.Label152.HyperLink = null;
			this.Label152.Left = 6.5625F;
			this.Label152.Name = "Label152";
			this.Label152.Style = "text-align: right";
			this.Label152.Tag = "text";
			this.Label152.Text = "0.00";
			this.Label152.Top = 1.854167F;
			this.Label152.Width = 0.6875F;
			// 
			// Label153
			// 
			this.Label153.Height = 0.1388889F;
			this.Label153.HyperLink = null;
			this.Label153.Left = 4.3125F;
			this.Label153.Name = "Label153";
			this.Label153.Style = "text-align: left";
			this.Label153.Tag = "text";
			this.Label153.Text = "Benefit Name";
			this.Label153.Top = 1.979167F;
			this.Label153.Width = 1.4375F;
			// 
			// Label154
			// 
			this.Label154.Height = 0.1388889F;
			this.Label154.HyperLink = null;
			this.Label154.Left = 5.8125F;
			this.Label154.Name = "Label154";
			this.Label154.Style = "text-align: right";
			this.Label154.Tag = "text";
			this.Label154.Text = "0.00";
			this.Label154.Top = 1.979167F;
			this.Label154.Width = 0.75F;
			// 
			// Label155
			// 
			this.Label155.Height = 0.1388889F;
			this.Label155.HyperLink = null;
			this.Label155.Left = 6.5625F;
			this.Label155.Name = "Label155";
			this.Label155.Style = "text-align: right";
			this.Label155.Tag = "text";
			this.Label155.Text = "0.00";
			this.Label155.Top = 1.979167F;
			this.Label155.Width = 0.6875F;
			// 
			// Label156
			// 
			this.Label156.Height = 0.1388889F;
			this.Label156.HyperLink = null;
			this.Label156.Left = 4.3125F;
			this.Label156.Name = "Label156";
			this.Label156.Style = "text-align: left";
			this.Label156.Tag = "text";
			this.Label156.Text = "Benefit Name";
			this.Label156.Top = 2.104167F;
			this.Label156.Width = 1.4375F;
			// 
			// Label157
			// 
			this.Label157.Height = 0.1388889F;
			this.Label157.HyperLink = null;
			this.Label157.Left = 5.8125F;
			this.Label157.Name = "Label157";
			this.Label157.Style = "text-align: right";
			this.Label157.Tag = "text";
			this.Label157.Text = "0.00";
			this.Label157.Top = 2.104167F;
			this.Label157.Width = 0.75F;
			// 
			// Label158
			// 
			this.Label158.Height = 0.1388889F;
			this.Label158.HyperLink = null;
			this.Label158.Left = 6.5625F;
			this.Label158.Name = "Label158";
			this.Label158.Style = "text-align: right";
			this.Label158.Tag = "text";
			this.Label158.Text = "0.00";
			this.Label158.Top = 2.104167F;
			this.Label158.Width = 0.6875F;
			// 
			// Label159
			// 
			this.Label159.Height = 0.1388889F;
			this.Label159.HyperLink = null;
			this.Label159.Left = 4.3125F;
			this.Label159.Name = "Label159";
			this.Label159.Style = "text-align: left";
			this.Label159.Tag = "text";
			this.Label159.Text = "Benefit Name";
			this.Label159.Top = 2.229167F;
			this.Label159.Width = 1.4375F;
			// 
			// Label160
			// 
			this.Label160.Height = 0.1388889F;
			this.Label160.HyperLink = null;
			this.Label160.Left = 5.8125F;
			this.Label160.Name = "Label160";
			this.Label160.Style = "text-align: right";
			this.Label160.Tag = "text";
			this.Label160.Text = "0.00";
			this.Label160.Top = 2.229167F;
			this.Label160.Width = 0.75F;
			// 
			// Label161
			// 
			this.Label161.Height = 0.1388889F;
			this.Label161.HyperLink = null;
			this.Label161.Left = 6.5625F;
			this.Label161.Name = "Label161";
			this.Label161.Style = "text-align: right";
			this.Label161.Tag = "text";
			this.Label161.Text = "0.00";
			this.Label161.Top = 2.229167F;
			this.Label161.Width = 0.6875F;
			// 
			// Label162
			// 
			this.Label162.Height = 0.1388889F;
			this.Label162.HyperLink = null;
			this.Label162.Left = 4.3125F;
			this.Label162.Name = "Label162";
			this.Label162.Style = "text-align: left";
			this.Label162.Tag = "text";
			this.Label162.Text = "Benefit Name";
			this.Label162.Top = 2.354167F;
			this.Label162.Width = 1.4375F;
			// 
			// Label163
			// 
			this.Label163.Height = 0.1388889F;
			this.Label163.HyperLink = null;
			this.Label163.Left = 5.8125F;
			this.Label163.Name = "Label163";
			this.Label163.Style = "text-align: right";
			this.Label163.Tag = "text";
			this.Label163.Text = "0.00";
			this.Label163.Top = 2.354167F;
			this.Label163.Width = 0.75F;
			// 
			// Label164
			// 
			this.Label164.Height = 0.1388889F;
			this.Label164.HyperLink = null;
			this.Label164.Left = 6.5625F;
			this.Label164.Name = "Label164";
			this.Label164.Style = "text-align: right";
			this.Label164.Tag = "text";
			this.Label164.Text = "0.00";
			this.Label164.Top = 2.354167F;
			this.Label164.Width = 0.6875F;
			// 
			// Label165
			// 
			this.Label165.Height = 0.1388889F;
			this.Label165.HyperLink = null;
			this.Label165.Left = 4.3125F;
			this.Label165.Name = "Label165";
			this.Label165.Style = "text-align: left";
			this.Label165.Tag = "text";
			this.Label165.Text = "Benefit Name";
			this.Label165.Top = 2.479167F;
			this.Label165.Width = 1.4375F;
			// 
			// Label166
			// 
			this.Label166.Height = 0.1388889F;
			this.Label166.HyperLink = null;
			this.Label166.Left = 5.8125F;
			this.Label166.Name = "Label166";
			this.Label166.Style = "text-align: right";
			this.Label166.Tag = "text";
			this.Label166.Text = "0.00";
			this.Label166.Top = 2.479167F;
			this.Label166.Width = 0.75F;
			// 
			// Label167
			// 
			this.Label167.Height = 0.1388889F;
			this.Label167.HyperLink = null;
			this.Label167.Left = 6.5625F;
			this.Label167.Name = "Label167";
			this.Label167.Style = "text-align: right";
			this.Label167.Tag = "text";
			this.Label167.Text = "0.00";
			this.Label167.Top = 2.479167F;
			this.Label167.Width = 0.6875F;
			// 
			// Label168
			// 
			this.Label168.Height = 0.1388889F;
			this.Label168.HyperLink = null;
			this.Label168.Left = 4.3125F;
			this.Label168.Name = "Label168";
			this.Label168.Style = "text-align: left";
			this.Label168.Tag = "text";
			this.Label168.Text = "Benefit Name";
			this.Label168.Top = 2.604167F;
			this.Label168.Width = 1.4375F;
			// 
			// Label169
			// 
			this.Label169.Height = 0.1388889F;
			this.Label169.HyperLink = null;
			this.Label169.Left = 5.8125F;
			this.Label169.Name = "Label169";
			this.Label169.Style = "text-align: right";
			this.Label169.Tag = "text";
			this.Label169.Text = "0.00";
			this.Label169.Top = 2.604167F;
			this.Label169.Width = 0.75F;
			// 
			// Label170
			// 
			this.Label170.Height = 0.1388889F;
			this.Label170.HyperLink = null;
			this.Label170.Left = 6.5625F;
			this.Label170.Name = "Label170";
			this.Label170.Style = "text-align: right";
			this.Label170.Tag = "text";
			this.Label170.Text = "0.00";
			this.Label170.Top = 2.604167F;
			this.Label170.Width = 0.6875F;
			// 
			// Label171
			// 
			this.Label171.Height = 0.1388889F;
			this.Label171.HyperLink = null;
			this.Label171.Left = 4.3125F;
			this.Label171.Name = "Label171";
			this.Label171.Style = "text-align: left";
			this.Label171.Tag = "text";
			this.Label171.Text = "Benefit Name";
			this.Label171.Top = 2.729167F;
			this.Label171.Width = 1.4375F;
			// 
			// Label172
			// 
			this.Label172.Height = 0.1388889F;
			this.Label172.HyperLink = null;
			this.Label172.Left = 5.8125F;
			this.Label172.Name = "Label172";
			this.Label172.Style = "text-align: right";
			this.Label172.Tag = "text";
			this.Label172.Text = "0.00";
			this.Label172.Top = 2.729167F;
			this.Label172.Width = 0.75F;
			// 
			// Label173
			// 
			this.Label173.Height = 0.1388889F;
			this.Label173.HyperLink = null;
			this.Label173.Left = 6.5625F;
			this.Label173.Name = "Label173";
			this.Label173.Style = "text-align: right";
			this.Label173.Tag = "text";
			this.Label173.Text = "0.00";
			this.Label173.Top = 2.729167F;
			this.Label173.Width = 0.6875F;
			// 
			// Label174
			// 
			this.Label174.Height = 0.1388889F;
			this.Label174.HyperLink = null;
			this.Label174.Left = 4.3125F;
			this.Label174.Name = "Label174";
			this.Label174.Style = "text-align: left";
			this.Label174.Tag = "text";
			this.Label174.Text = "Benefit Name";
			this.Label174.Top = 2.854167F;
			this.Label174.Width = 1.4375F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1388889F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 5.8125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "text-align: right";
			this.Label175.Tag = "text";
			this.Label175.Text = "0.00";
			this.Label175.Top = 2.854167F;
			this.Label175.Width = 0.75F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1388889F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 6.5625F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "text-align: right";
			this.Label176.Tag = "text";
			this.Label176.Text = "0.00";
			this.Label176.Top = 2.854167F;
			this.Label176.Width = 0.6875F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1388889F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 4.3125F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "text-align: left";
			this.Label177.Tag = "text";
			this.Label177.Text = "Benefit Name";
			this.Label177.Top = 2.979167F;
			this.Label177.Width = 1.4375F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.1388889F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 5.8125F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "text-align: right";
			this.Label178.Tag = "text";
			this.Label178.Text = "0.00";
			this.Label178.Top = 2.979167F;
			this.Label178.Width = 0.75F;
			// 
			// Label179
			// 
			this.Label179.Height = 0.1388889F;
			this.Label179.HyperLink = null;
			this.Label179.Left = 6.5625F;
			this.Label179.Name = "Label179";
			this.Label179.Style = "text-align: right";
			this.Label179.Tag = "text";
			this.Label179.Text = "0.00";
			this.Label179.Top = 2.979167F;
			this.Label179.Width = 0.6875F;
			// 
			// txtTotalHours
			// 
			this.txtTotalHours.Height = 0.1666667F;
			this.txtTotalHours.HyperLink = null;
			this.txtTotalHours.Left = 1.666667F;
			this.txtTotalHours.Name = "txtTotalHours";
			this.txtTotalHours.Style = "text-align: right";
			this.txtTotalHours.Tag = "text";
			this.txtTotalHours.Text = "0.00";
			this.txtTotalHours.Top = 2.09375F;
			this.txtTotalHours.Visible = false;
			this.txtTotalHours.Width = 0.75F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.HyperLink = null;
			this.txtTotalAmount.Left = 2.4375F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "text-align: right";
			this.txtTotalAmount.Tag = "text";
			this.txtTotalAmount.Text = "0.00";
			this.txtTotalAmount.Top = 2.104167F;
			this.txtTotalAmount.Visible = false;
			this.txtTotalAmount.Width = 0.6875F;
			// 
			// txtEMatchCurrent
			// 
			this.txtEMatchCurrent.Height = 0.1388889F;
			this.txtEMatchCurrent.HyperLink = null;
			this.txtEMatchCurrent.Left = 5.791667F;
			this.txtEMatchCurrent.Name = "txtEMatchCurrent";
			this.txtEMatchCurrent.Style = "text-align: right";
			this.txtEMatchCurrent.Tag = "text";
			this.txtEMatchCurrent.Text = "0.00";
			this.txtEMatchCurrent.Top = 3.145833F;
			this.txtEMatchCurrent.Visible = false;
			this.txtEMatchCurrent.Width = 0.75F;
			// 
			// txtEmatchYTD
			// 
			this.txtEmatchYTD.Height = 0.1388889F;
			this.txtEmatchYTD.HyperLink = null;
			this.txtEmatchYTD.Left = 6.552083F;
			this.txtEmatchYTD.Name = "txtEmatchYTD";
			this.txtEmatchYTD.Style = "text-align: right";
			this.txtEmatchYTD.Tag = "text";
			this.txtEmatchYTD.Text = "0.00";
			this.txtEmatchYTD.Top = 3.145833F;
			this.txtEmatchYTD.Visible = false;
			this.txtEmatchYTD.Width = 0.6875F;
			// 
			// lblCheckMessage
			// 
			this.lblCheckMessage.Height = 0.1666667F;
			this.lblCheckMessage.HyperLink = null;
			this.lblCheckMessage.Left = 3.6875F;
			this.lblCheckMessage.Name = "lblCheckMessage";
			this.lblCheckMessage.Style = "font-size: 10pt; text-align: right";
			this.lblCheckMessage.Text = null;
			this.lblCheckMessage.Top = 1.0625F;
			this.lblCheckMessage.Visible = false;
			this.lblCheckMessage.Width = 3.40625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.15625F;
			this.Line3.LineWeight = 2F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 5.166667F;
			this.Line3.Width = 3.010417F;
			this.Line3.X1 = 0.15625F;
			this.Line3.X2 = 3.166667F;
			this.Line3.Y1 = 5.166667F;
			this.Line3.Y2 = 5.166667F;
			// 
			// Label205
			// 
			this.Label205.Height = 0.1666667F;
			this.Label205.HyperLink = null;
			this.Label205.Left = 2.4375F;
			this.Label205.Name = "Label205";
			this.Label205.Style = "text-align: right";
			this.Label205.Tag = "text";
			this.Label205.Text = "0.00";
			this.Label205.Top = 5.21875F;
			this.Label205.Width = 0.6875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.125F;
			this.Line4.LineWeight = 2F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.569444F;
			this.Line4.Width = 3.010417F;
			this.Line4.X1 = 0.125F;
			this.Line4.X2 = 3.135417F;
			this.Line4.Y1 = 2.569444F;
			this.Line4.Y2 = 2.569444F;
			// 
			// Label206
			// 
			this.Label206.Height = 0.1666667F;
			this.Label206.HyperLink = null;
			this.Label206.Left = 0.1875F;
			this.Label206.Name = "Label206";
			this.Label206.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label206.Tag = "text";
			this.Label206.Text = "Total";
			this.Label206.Top = 5.208333F;
			this.Label206.Width = 1.4375F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.125F;
			this.Line5.LineWeight = 2F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.072917F;
			this.Line5.Width = 3.010417F;
			this.Line5.X1 = 0.125F;
			this.Line5.X2 = 3.135417F;
			this.Line5.Y1 = 2.072917F;
			this.Line5.Y2 = 2.072917F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0.125F;
			this.Line6.LineWeight = 2F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.7916667F;
			this.Line6.Width = 3.010417F;
			this.Line6.X1 = 0.125F;
			this.Line6.X2 = 3.135417F;
			this.Line6.Y1 = 0.7916667F;
			this.Line6.Y2 = 0.7916667F;
			// 
			// Label209
			// 
			this.Label209.Height = 0.1666667F;
			this.Label209.HyperLink = null;
			this.Label209.Left = 0.1875F;
			this.Label209.Name = "Label209";
			this.Label209.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label209.Tag = "text";
			this.Label209.Text = "Total";
			this.Label209.Top = 2.083333F;
			this.Label209.Width = 1.4375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 4.3125F;
			this.Line7.LineWeight = 2F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 3.131944F;
			this.Line7.Width = 3.010417F;
			this.Line7.X1 = 4.3125F;
			this.Line7.X2 = 7.322917F;
			this.Line7.Y1 = 3.131944F;
			this.Line7.Y2 = 3.131944F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 4.3125F;
			this.Line8.LineWeight = 2F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.586806F;
			this.Line8.Width = 3.010417F;
			this.Line8.X1 = 4.3125F;
			this.Line8.X2 = 7.322917F;
			this.Line8.Y1 = 1.586806F;
			this.Line8.Y2 = 1.586806F;
			// 
			// Label210
			// 
			this.Label210.Height = 0.1770833F;
			this.Label210.HyperLink = null;
			this.Label210.Left = 3.9375F;
			this.Label210.Name = "Label210";
			this.Label210.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label210.Text = "Tax";
			this.Label210.Top = 3.458333F;
			this.Label210.Width = 0.7083333F;
			// 
			// Label211
			// 
			this.Label211.Height = 0.1388889F;
			this.Label211.HyperLink = null;
			this.Label211.Left = 0.1875F;
			this.Label211.Name = "Label211";
			this.Label211.Style = "text-align: left";
			this.Label211.Tag = "text";
			this.Label211.Text = "Regular";
			this.Label211.Top = 1.541667F;
			this.Label211.Width = 1.489583F;
			// 
			// Label212
			// 
			this.Label212.Height = 0.1354167F;
			this.Label212.HyperLink = null;
			this.Label212.Left = 1.75F;
			this.Label212.Name = "Label212";
			this.Label212.Style = "text-align: right";
			this.Label212.Tag = "text";
			this.Label212.Text = "0.00";
			this.Label212.Top = 1.541667F;
			this.Label212.Width = 0.6875F;
			// 
			// Label213
			// 
			this.Label213.Height = 0.1354167F;
			this.Label213.HyperLink = null;
			this.Label213.Left = 2.4375F;
			this.Label213.Name = "Label213";
			this.Label213.Style = "text-align: right";
			this.Label213.Tag = "text";
			this.Label213.Text = "0.00";
			this.Label213.Top = 1.541667F;
			this.Label213.Width = 0.6875F;
			// 
			// Label214
			// 
			this.Label214.Height = 0.1388889F;
			this.Label214.HyperLink = null;
			this.Label214.Left = 0.1875F;
			this.Label214.Name = "Label214";
			this.Label214.Style = "text-align: left";
			this.Label214.Tag = "text";
			this.Label214.Text = "Regular";
			this.Label214.Top = 1.666667F;
			this.Label214.Width = 1.489583F;
			// 
			// Label215
			// 
			this.Label215.Height = 0.1354167F;
			this.Label215.HyperLink = null;
			this.Label215.Left = 1.75F;
			this.Label215.Name = "Label215";
			this.Label215.Style = "text-align: right";
			this.Label215.Tag = "text";
			this.Label215.Text = "0.00";
			this.Label215.Top = 1.666667F;
			this.Label215.Width = 0.6875F;
			// 
			// Label216
			// 
			this.Label216.Height = 0.1354167F;
			this.Label216.HyperLink = null;
			this.Label216.Left = 2.4375F;
			this.Label216.Name = "Label216";
			this.Label216.Style = "text-align: right";
			this.Label216.Tag = "text";
			this.Label216.Text = "0.00";
			this.Label216.Top = 1.666667F;
			this.Label216.Width = 0.6875F;
			// 
			// Label217
			// 
			this.Label217.Height = 0.1388889F;
			this.Label217.HyperLink = null;
			this.Label217.Left = 0.1875F;
			this.Label217.Name = "Label217";
			this.Label217.Style = "text-align: left";
			this.Label217.Tag = "text";
			this.Label217.Text = "Regular";
			this.Label217.Top = 1.791667F;
			this.Label217.Width = 1.489583F;
			// 
			// Label218
			// 
			this.Label218.Height = 0.1354167F;
			this.Label218.HyperLink = null;
			this.Label218.Left = 1.75F;
			this.Label218.Name = "Label218";
			this.Label218.Style = "text-align: right";
			this.Label218.Tag = "text";
			this.Label218.Text = "0.00";
			this.Label218.Top = 1.791667F;
			this.Label218.Width = 0.6875F;
			// 
			// Label219
			// 
			this.Label219.Height = 0.1354167F;
			this.Label219.HyperLink = null;
			this.Label219.Left = 2.4375F;
			this.Label219.Name = "Label219";
			this.Label219.Style = "text-align: right";
			this.Label219.Tag = "text";
			this.Label219.Text = "0.00";
			this.Label219.Top = 1.791667F;
			this.Label219.Width = 0.6875F;
			// 
			// Label220
			// 
			this.Label220.Height = 0.1388889F;
			this.Label220.HyperLink = null;
			this.Label220.Left = 0.1875F;
			this.Label220.Name = "Label220";
			this.Label220.Style = "text-align: left";
			this.Label220.Tag = "text";
			this.Label220.Text = "Regular";
			this.Label220.Top = 1.916667F;
			this.Label220.Width = 1.489583F;
			// 
			// Label221
			// 
			this.Label221.Height = 0.1354167F;
			this.Label221.HyperLink = null;
			this.Label221.Left = 1.75F;
			this.Label221.Name = "Label221";
			this.Label221.Style = "text-align: right";
			this.Label221.Tag = "text";
			this.Label221.Text = "0.00";
			this.Label221.Top = 1.916667F;
			this.Label221.Width = 0.6875F;
			// 
			// Label222
			// 
			this.Label222.Height = 0.1354167F;
			this.Label222.HyperLink = null;
			this.Label222.Left = 2.4375F;
			this.Label222.Name = "Label222";
			this.Label222.Style = "text-align: right";
			this.Label222.Tag = "text";
			this.Label222.Text = "0.00";
			this.Label222.Top = 1.916667F;
			this.Label222.Width = 0.6875F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 3.84375F;
			this.Line9.LineWeight = 2F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 0.59375F;
			this.Line9.Width = 3.260417F;
			this.Line9.X1 = 3.84375F;
			this.Line9.X2 = 7.104167F;
			this.Line9.Y1 = 0.59375F;
			this.Line9.Y2 = 0.59375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1388889F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.291667F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; text-align: left";
			this.Label13.Tag = "text";
			this.Label13.Text = "Vacation";
			this.Label13.Top = 4.59375F;
			this.Label13.Width = 0.8229167F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1388889F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.291667F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; text-align: left";
			this.Label14.Tag = "text";
			this.Label14.Text = "Sick";
			this.Label14.Top = 4.739583F;
			this.Label14.Width = 0.8229167F;
			// 
			// txtVacationBalance
			// 
			this.txtVacationBalance.Height = 0.1666667F;
			this.txtVacationBalance.HyperLink = null;
			this.txtVacationBalance.Left = 5.229167F;
			this.txtVacationBalance.Name = "txtVacationBalance";
			this.txtVacationBalance.Style = "text-align: right";
			this.txtVacationBalance.Tag = "text";
			this.txtVacationBalance.Text = "0.00";
			this.txtVacationBalance.Top = 4.59375F;
			this.txtVacationBalance.Width = 0.625F;
			// 
			// txtSickBalance
			// 
			this.txtSickBalance.Height = 0.1388889F;
			this.txtSickBalance.HyperLink = null;
			this.txtSickBalance.Left = 5.229167F;
			this.txtSickBalance.Name = "txtSickBalance";
			this.txtSickBalance.Style = "text-align: right";
			this.txtSickBalance.Tag = "text";
			this.txtSickBalance.Text = "0.00";
			this.txtSickBalance.Top = 4.739583F;
			this.txtSickBalance.Width = 0.625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1388889F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 4.291667F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 8.5pt; text-align: left";
			this.Label29.Tag = "text";
			this.Label29.Text = "Other";
			this.Label29.Top = 4.875F;
			this.Label29.Width = 0.8854167F;
			// 
			// txtOtherBalance
			// 
			this.txtOtherBalance.Height = 0.1388889F;
			this.txtOtherBalance.HyperLink = null;
			this.txtOtherBalance.Left = 5.229167F;
			this.txtOtherBalance.Name = "txtOtherBalance";
			this.txtOtherBalance.Style = "font-size: 8pt; text-align: right";
			this.txtOtherBalance.Tag = "text";
			this.txtOtherBalance.Text = "0.00";
			this.txtOtherBalance.Top = 4.875F;
			this.txtOtherBalance.Width = 0.625F;
			// 
			// lblCode1
			// 
			this.lblCode1.Height = 0.1388889F;
			this.lblCode1.HyperLink = null;
			this.lblCode1.Left = 4.291667F;
			this.lblCode1.Name = "lblCode1";
			this.lblCode1.Style = "font-size: 8.5pt; text-align: left";
			this.lblCode1.Tag = "text";
			this.lblCode1.Text = "Code1";
			this.lblCode1.Top = 5.020833F;
			this.lblCode1.Width = 0.8541667F;
			// 
			// lblCode2
			// 
			this.lblCode2.Height = 0.1388889F;
			this.lblCode2.HyperLink = null;
			this.lblCode2.Left = 4.291667F;
			this.lblCode2.Name = "lblCode2";
			this.lblCode2.Style = "font-size: 8.5pt; text-align: left";
			this.lblCode2.Tag = "text";
			this.lblCode2.Text = "Code2";
			this.lblCode2.Top = 5.145833F;
			this.lblCode2.Width = 0.8541667F;
			// 
			// lblCode1Balance
			// 
			this.lblCode1Balance.Height = 0.1388889F;
			this.lblCode1Balance.HyperLink = null;
			this.lblCode1Balance.Left = 5.229167F;
			this.lblCode1Balance.Name = "lblCode1Balance";
			this.lblCode1Balance.Style = "font-size: 8pt; text-align: right";
			this.lblCode1Balance.Tag = "text";
			this.lblCode1Balance.Text = "0.00";
			this.lblCode1Balance.Top = 5.020833F;
			this.lblCode1Balance.Width = 0.625F;
			// 
			// lblCode2Balance
			// 
			this.lblCode2Balance.Height = 0.1388889F;
			this.lblCode2Balance.HyperLink = null;
			this.lblCode2Balance.Left = 5.229167F;
			this.lblCode2Balance.Name = "lblCode2Balance";
			this.lblCode2Balance.Style = "font-size: 8pt; text-align: right";
			this.lblCode2Balance.Tag = "text";
			this.lblCode2Balance.Text = "0.00";
			this.lblCode2Balance.Top = 5.145833F;
			this.lblCode2Balance.Width = 0.625F;
			// 
			// lblCode3
			// 
			this.lblCode3.Height = 0.1388889F;
			this.lblCode3.HyperLink = null;
			this.lblCode3.Left = 4.291667F;
			this.lblCode3.Name = "lblCode3";
			this.lblCode3.Style = "font-size: 8.5pt; text-align: left";
			this.lblCode3.Tag = "text";
			this.lblCode3.Text = "Code3";
			this.lblCode3.Top = 5.291667F;
			this.lblCode3.Width = 0.8854167F;
			// 
			// lblCode3Balance
			// 
			this.lblCode3Balance.Height = 0.1388889F;
			this.lblCode3Balance.HyperLink = null;
			this.lblCode3Balance.Left = 5.229167F;
			this.lblCode3Balance.Name = "lblCode3Balance";
			this.lblCode3Balance.Style = "font-size: 8pt; text-align: right";
			this.lblCode3Balance.Tag = "text";
			this.lblCode3Balance.Text = "0.00";
			this.lblCode3Balance.Top = 5.291667F;
			this.lblCode3Balance.Width = 0.625F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1770833F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 5.520833F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label39.Text = "Taken";
			this.Label39.Top = 4.40625F;
			this.Label39.Width = 0.9791667F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1770833F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 6.270833F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label40.Text = "Balance";
			this.Label40.Top = 4.395833F;
			this.Label40.Width = 0.9791667F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1666667F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 5.916667F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "text-align: right";
			this.Label41.Tag = "text";
			this.Label41.Text = "0.00";
			this.Label41.Top = 4.59375F;
			this.Label41.Width = 0.625F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1388889F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 5.916667F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "text-align: right";
			this.Label42.Tag = "text";
			this.Label42.Text = "0.00";
			this.Label42.Top = 4.739583F;
			this.Label42.Width = 0.625F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1388889F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 5.916667F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "text-align: right";
			this.Label43.Tag = "text";
			this.Label43.Text = "0.00";
			this.Label43.Top = 4.875F;
			this.Label43.Width = 0.625F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1388889F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 5.916667F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-size: 8pt; text-align: right";
			this.Label44.Tag = "text";
			this.Label44.Text = "0.00";
			this.Label44.Top = 5.020833F;
			this.Label44.Width = 0.625F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1388889F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 5.916667F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-size: 8pt; text-align: right";
			this.Label45.Tag = "text";
			this.Label45.Text = "0.00";
			this.Label45.Top = 5.145833F;
			this.Label45.Width = 0.625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1388889F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 5.916667F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-size: 8pt; text-align: right";
			this.Label46.Tag = "text";
			this.Label46.Text = "0.00";
			this.Label46.Top = 5.291667F;
			this.Label46.Width = 0.625F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1388889F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 6.604167F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "text-align: right";
			this.Label47.Tag = "text";
			this.Label47.Text = "0.00";
			this.Label47.Top = 4.59375F;
			this.Label47.Width = 0.625F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1388889F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 6.604167F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "text-align: right";
			this.Label48.Tag = "text";
			this.Label48.Text = "0.00";
			this.Label48.Top = 4.739583F;
			this.Label48.Width = 0.625F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1388889F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 6.604167F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "text-align: right";
			this.Label49.Tag = "text";
			this.Label49.Text = "0.00";
			this.Label49.Top = 4.875F;
			this.Label49.Width = 0.625F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1388889F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 6.604167F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-size: 8pt; text-align: right";
			this.Label51.Tag = "text";
			this.Label51.Text = "0.00";
			this.Label51.Top = 5.145833F;
			this.Label51.Width = 0.625F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1388889F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 6.604167F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-size: 8pt; text-align: right";
			this.Label52.Tag = "text";
			this.Label52.Text = "0.00";
			this.Label52.Top = 5.291667F;
			this.Label52.Width = 0.625F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1770833F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 4.833333F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 8pt; font-weight: bold; text-align: right";
			this.Label53.Text = "Accrued";
			this.Label53.Top = 4.385417F;
			this.Label53.Width = 1.010417F;
			// 
			// Label204
			// 
			this.Label204.Height = 0.1388889F;
			this.Label204.HyperLink = null;
			this.Label204.Left = 6.604167F;
			this.Label204.Name = "Label204";
			this.Label204.Style = "text-align: right";
			this.Label204.Tag = "text";
			this.Label204.Text = "0.00";
			this.Label204.Top = 5.024305F;
			this.Label204.Width = 0.625F;
			// 
			// srptNewDBLLaserStub1
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label139)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label144)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label145)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label146)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label147)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label148)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label149)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label150)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label151)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label153)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label155)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label157)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label205)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label218)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label220)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label221)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label222)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFICA;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtChkAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepChkLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepSav;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositSavLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label139;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label140;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label144;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label145;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label146;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label147;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label148;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label149;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label150;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label151;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label153;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label154;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label155;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label156;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label157;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label158;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label159;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label160;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label161;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label162;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label163;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label164;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label165;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label166;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label167;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label168;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label169;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label170;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label171;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label172;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label173;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label174;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEMatchCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmatchYTD;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label205;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label210;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label211;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label213;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label214;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label215;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label216;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label218;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label219;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label220;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label221;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label222;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtOtherBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label204;
	}
}
