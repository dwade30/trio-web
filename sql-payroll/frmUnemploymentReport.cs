//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmUnemploymentReport : BaseForm
	{
		public frmUnemploymentReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.cmbEndDate = new System.Collections.Generic.List<FCComboBox>();
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_0, 0);
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_1, 1);
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_2, 2);
			this.cmbStartDate = new System.Collections.Generic.List<FCComboBox>();
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_0, 0);
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_1, 1);
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_2, 2);
			this.txtStartDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtStartDate.AddControlArrayElement(txtStartDate_0, 0);
			this.txtStartDate.AddControlArrayElement(txtStartDate_1, 1);
			this.txtStartDate.AddControlArrayElement(txtStartDate_2, 2);
			this.txtEndDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtEndDate.AddControlArrayElement(txtEndDate_0, 0);
			this.txtEndDate.AddControlArrayElement(txtEndDate_1, 1);
			this.txtEndDate.AddControlArrayElement(txtEndDate_2, 2);
			this.lblTo = new System.Collections.Generic.List<FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblFrom = new System.Collections.Generic.List<FCLabel>();
			this.lblFrom.AddControlArrayElement(lblFrom_0, 0);
			this.lblFrom.AddControlArrayElement(lblFrom_1, 1);
			this.lblFrom.AddControlArrayElement(lblFrom_2, 2);
			this.lblMonth = new System.Collections.Generic.List<FCLabel>();
			this.lblMonth.AddControlArrayElement(lblMonth_0, 0);
			this.lblMonth.AddControlArrayElement(lblMonth_1, 1);
			this.lblMonth.AddControlArrayElement(lblMonth_2, 2);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUnemploymentReport InstancePtr
		{
			get
			{
				return (frmUnemploymentReport)Sys.GetInstance(typeof(frmUnemploymentReport));
			}
		}

		protected frmUnemploymentReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         3/12/03
		// This form will be used to get the information needed to
		// print the MMA and Non MMA Unemployment reports
		// ********************************************************
		public string strReportType = "";
		// M - MMA   N - Non MMA
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intLastMonthInQuarter As int	OnWrite(double, int)
		int intLastMonthInQuarter;
		int lngYearCovered;
        internal List<string> batchReports;

		private void cboQuarter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int x;
			string strSQL = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			DateTime dtTemp;
			for (x = 0; x <= 2; x++)
			{
				txtStartDate[x].Text = string.Empty;
				txtEndDate[x].Text = string.Empty;
			}
			intLastMonthInQuarter = FCConvert.ToInt16(Conversion.Val(cboQuarter.Text) * 3);
			lngYearCovered = FCConvert.ToInt32(Math.Round(Conversion.Val(cboYear.Text)));
			FillDateCombos();
			for (x = 0; x <= 2; x++)
			{
				strSQL = "select * from TownInfo";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("Month" + FCConvert.ToString(x + 1) + "start")) && FCConvert.ToDateTime(clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "start")) != DateTime.FromOADate(0))
					{
						dtTemp = (DateTime)clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "start");
						if (dtTemp.Month == intLastMonthInQuarter + x - 2)
						{
							if (dtTemp.Year == lngYearCovered)
							{
								cmbStartDate[x].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
							}
						}
					}
					if (Information.IsDate(clsLoad.Get_Fields("Month" + FCConvert.ToString(x + 1) + "End")) && FCConvert.ToDateTime(clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "end")) != DateTime.FromOADate(0))
					{
						dtTemp = (DateTime)clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "end");
						if (dtTemp.Month == intLastMonthInQuarter + x - 2)
						{
							if (dtTemp.Year == lngYearCovered)
							{
								cmbEndDate[x].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
							}
						}
					}
				}
				if (!(Information.IsDate(cmbEndDate[x].Text)))
				{
					cmbEndDate[x].Text = cmbStartDate[x].Text;
				}
			}
		}

		private void cboSequenceRange_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboSequenceRange.SelectedIndex == 1)
			{
				fraSequenceRangeSelection.Visible = false;
				if (cboStartSequence.SelectedIndex != -1)
				{
					cboStartSequence.SelectedIndex = 0;
					cboEndSequence.SelectedIndex = 0;
				}
				fraSequenceSingleSelection.Visible = true;
				cboSingleSequence.Focus();
			}
			else if (cboSequenceRange.SelectedIndex == 2)
			{
				fraSequenceSingleSelection.Visible = false;
				if (cboSingleSequence.SelectedIndex != -1)
				{
					cboSingleSequence.SelectedIndex = 0;
				}
				fraSequenceRangeSelection.Visible = true;
				cboStartSequence.Focus();
			}
			else
			{
				fraSequenceRangeSelection.Visible = false;
				fraSequenceSingleSelection.Visible = false;
				if (cboStartSequence.SelectedIndex != -1)
				{
					cboStartSequence.SelectedIndex = 0;
					cboEndSequence.SelectedIndex = 0;
					cboSingleSequence.SelectedIndex = 0;
				}
			}
		}

		private void cboYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intLastMonthInQuarter = FCConvert.ToInt16(Conversion.Val(cboQuarter.Text) * 3);
			lngYearCovered = FCConvert.ToInt32(Math.Round(Conversion.Val(cboYear.Text)));
			FillDateCombos();
		}

		private void frmUnemploymentReport_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmUnemploymentReport_Load(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsSequenceInfo = new clsDRWrapper();
			clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			cboYear.Clear();
			for (counter = DateTime.Now.Year - 5; counter <= DateTime.Now.Year; counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
			rsPayDateInfo.OpenRecordset("SELECT * FROM tblPayrollProcessSetup");
			if (rsPayDateInfo.EndOfFile() != true && rsPayDateInfo.BeginningOfFile() != true)
			{
				int indexTemp;
				indexTemp = rsPayDateInfo.Get_Fields_DateTime("PayDate").Year - (DateTime.Now.Year - 5);
				if (indexTemp > cboYear.ListCount - 1)
				{
					cboYear.ListIndex = cboYear.ListCount - 1;
				}
				else
				{
					cboYear.ListIndex = indexTemp;
				}
			}
			else
			{
				cboYear.SelectedIndex = 2;
			}
			switch (FCConvert.ToInt32(rsPayDateInfo.Get_Fields_DateTime("PayDate").Month))
			{
				case 1:
				case 2:
				case 3:
					{
						cboQuarter.SelectedIndex = 0;
						break;
					}
				case 4:
				case 5:
				case 6:
					{
						cboQuarter.SelectedIndex = 1;
						break;
					}
				case 7:
				case 8:
				case 9:
					{
						cboQuarter.SelectedIndex = 2;
						break;
					}
				default:
					{
						cboQuarter.SelectedIndex = 3;
						break;
					}
			}
			//end switch
			intLastMonthInQuarter = ((cboQuarter.SelectedIndex + 1) * 3);
			lngYearCovered = FCConvert.ToInt32(rsPayDateInfo.Get_Fields("paydate").Year);
			FillDateCombos();
			if (strReportType == "M")
			{
				this.Text = "MMA Unemployment Report";
				this.HeaderText.Text = "MMA Unemployment Report";
				lblSequence.Visible = false;
				cboSequence.Visible = false;
			}
			else
			{
				this.Text = "Unemployment Report";
				this.HeaderText.Text = "Unemployment Report";
				lblSequence.Visible = true;
				cboSequence.Visible = true;
			}
			cboStartSequence.Clear();
			cboEndSequence.Clear();
			cboSingleSequence.Clear();
			rsSequenceInfo.OpenRecordset("SELECT DISTINCT SeqNumber FROM tblEmployeeMaster ORDER BY SeqNumber");
			if (rsSequenceInfo.EndOfFile() != true && rsSequenceInfo.BeginningOfFile() != true)
			{
				do
				{
					cboStartSequence.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsSequenceInfo.Get_Fields("SeqNumber"))));
					cboEndSequence.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsSequenceInfo.Get_Fields("SeqNumber"))));
					cboSingleSequence.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsSequenceInfo.Get_Fields("SeqNumber"))));
					rsSequenceInfo.MoveNext();
				}
				while (rsSequenceInfo.EndOfFile() != true);
				cboStartSequence.SelectedIndex = 0;
				cboEndSequence.SelectedIndex = 0;
				cboSingleSequence.SelectedIndex = 0;
			}
			cboSequence.SelectedIndex = 0;
			cboSequenceRange.SelectedIndex = 0;
			rsSequenceInfo.OpenRecordset("SELECT * FROM TownInfo");
			if (rsSequenceInfo.EndOfFile() != true && rsSequenceInfo.BeginningOfFile() != true)
			{
				txtReportNumber.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields("ReportNumber"));
				txtTelephone.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields_String("TelephoneNumber"));
				txtEmployer.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields_String("Employer"));
				txtPreparer.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields_String("Preparer"));
				txtFederalID.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields_String("FederalID"));
				txtStateID.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields_String("StateID"));
				txtMemberCode.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields_String("MemberCode"));
				txtSeasonalCode.Text = FCConvert.ToString(rsSequenceInfo.Get_Fields("SeasonalCode"));
				// If IsDate(rsSequenceInfo.Fields("Month1Start")) Then
				// txtStartDate(0).Text = Format(rsSequenceInfo.Fields("Month1Start"), "MM/dd/yyyy")
				// End If
				// If IsDate(rsSequenceInfo.Fields("Month1End")) Then
				// txtEndDate(0).Text = Format(rsSequenceInfo.Fields("Month1End"), "MM/dd/yyyy")
				// End If
				// If IsDate(rsSequenceInfo.Fields("Month2Start")) Then
				// txtStartDate(1).Text = Format(rsSequenceInfo.Fields("Month2Start"), "MM/dd/yyyy")
				// End If
				// If IsDate(rsSequenceInfo.Fields("Month2End")) Then
				// txtEndDate(1).Text = Format(rsSequenceInfo.Fields("Month2End"), "MM/dd/yyyy")
				// End If
				// If IsDate(rsSequenceInfo.Fields("Month3Start")) Then
				// txtStartDate(2).Text = Format(rsSequenceInfo.Fields("Month3Start"), "MM/dd/yyyy")
				// End If
				// If IsDate(rsSequenceInfo.Fields("Month3End")) Then
				// txtEndDate(2).Text = Format(rsSequenceInfo.Fields("Month3End"), "MM/dd/yyyy")
				// End If
				// corey 11/15/2005
				// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
				int intTemp = 0;
				if (Information.IsDate(rsSequenceInfo.Get_Fields("Month1Start")))
				{
					intTemp = rsSequenceInfo.Get_Fields_DateTime("month1start").Month;
					if (intTemp == intLastMonthInQuarter - 2)
					{
						cmbStartDate[0].Text = Strings.Format(rsSequenceInfo.Get_Fields("Month1Start"), "MM/dd/yyyy");
					}
				}
				if (Information.IsDate(rsSequenceInfo.Get_Fields("Month1End")))
				{
					intTemp = rsSequenceInfo.Get_Fields_DateTime("month1end").Month;
					if (intTemp == intLastMonthInQuarter - 2)
					{
						cmbEndDate[0].Text = Strings.Format(rsSequenceInfo.Get_Fields("Month1End"), "MM/dd/yyyy");
					}
				}
				if (Information.IsDate(rsSequenceInfo.Get_Fields("Month2Start")))
				{
					intTemp = rsSequenceInfo.Get_Fields_DateTime("month2start").Month;
					if (intTemp == intLastMonthInQuarter - 1)
					{
						cmbStartDate[1].Text = Strings.Format(rsSequenceInfo.Get_Fields_DateTime("Month2Start"), "MM/dd/yyyy");
					}
				}
				if (Information.IsDate(rsSequenceInfo.Get_Fields("Month2End")))
				{
					intTemp = rsSequenceInfo.Get_Fields_DateTime("month2end").Month;
					if (intTemp == intLastMonthInQuarter - 1)
					{
						cmbEndDate[1].Text = Strings.Format(rsSequenceInfo.Get_Fields_DateTime("Month2End"), "MM/dd/yyyy");
					}
				}
				if (Information.IsDate(rsSequenceInfo.Get_Fields("Month3Start")))
				{
					intTemp = rsSequenceInfo.Get_Fields_DateTime("month3start").Month;
					if (intTemp == intLastMonthInQuarter)
					{
						cmbStartDate[2].Text = Strings.Format(rsSequenceInfo.Get_Fields_DateTime("Month3Start"), "MM/dd/yyyy");
					}
				}
				if (Information.IsDate(rsSequenceInfo.Get_Fields("Month3End")))
				{
					intTemp = rsSequenceInfo.Get_Fields_DateTime("month3end").Month;
					if (intTemp == intLastMonthInQuarter)
					{
						cmbEndDate[2].Text = Strings.Format(rsSequenceInfo.Get_Fields_DateTime("Month3End"), "MM/dd/yyyy");
					}
				}
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			clsHistoryClass.Init = this;
			clsHistoryClass.SetGridIDColumn("PY");

            txtReportNumber.AllowOnlyNumericInput();
        }

		private void frmUnemploymentReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			// NEED TO CL
			for (counter = 0; counter <= 2; counter++)
			{
				txtStartDate[counter].Text = "";
				txtEndDate[counter].Text = "";
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			SaveTownInfo();
			if (strReportType == "M")
			{
				modDuplexPrinting.DuplexPrintReport(rptMMAUnemploymentReport.InstancePtr, batchReports: batchReports);
				// rptMMAUnemploymentReport.PrintReport True
			}
			else
			{
				modDuplexPrinting.DuplexPrintReport(rptNonMMAUnemploymentReport.InstancePtr, batchReports: batchReports);
				// rptNonMMAUnemploymentReport.PrintReport True
			}
			this.Hide();
		}

		private void mnuProcessPreview_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 2; counter++)
			{
				if (!Information.IsDate(cmbEndDate[counter].Text))
				{
					cmbEndDate[counter].Text = cmbStartDate[counter].Text;
				}
				if (Information.IsDate(cmbStartDate[counter].Text) && Information.IsDate(cmbEndDate[counter].Text))
				{
					// If Day(txtStartDate(counter).Text) > 12 Then
					// MsgBox "You must include the 12th day of the month in your date range", vbCritical, "Invalid Date"
					// txtStartDate(counter).SetFocus
					// Exit Sub
					// End If
					if (FCConvert.ToDateTime(cmbStartDate[counter].Text).Year != FCConvert.ToDouble(cboYear.Text))
					{
						MessageBox.Show("The date you entered is not part of the year you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cmbStartDate[counter].Focus();
						return;
					}
					if (FCConvert.ToDateTime(cmbEndDate[counter].Text).Year != FCConvert.ToDouble(cboYear.Text))
					{
						MessageBox.Show("The date you entered is not part of the year you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cmbEndDate[counter].Focus();
						return;
					}
					switch (cboQuarter.Text)
					{
						case "1":
							{
								// If Month(txtStartDate(counter).Text) < 1 Or Month(txtStartDate(counter).Text) > 3 Then
								if (FCConvert.ToDateTime(cmbStartDate[counter].Text).Month < 1 || FCConvert.ToDateTime(cmbStartDate[counter].Text).Month > 3)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbStartDate[counter].Focus();
									return;
								}
								if (FCConvert.ToDateTime(cmbEndDate[counter].Text).Month < 1 || FCConvert.ToDateTime(cmbEndDate[counter].Text).Month > 3)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbEndDate[counter].Focus();
									return;
								}
								break;
							}
						case "2":
							{
								if (FCConvert.ToDateTime(cmbStartDate[counter].Text).Month < 4 || FCConvert.ToDateTime(cmbStartDate[counter].Text).Month > 6)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbStartDate[counter].Focus();
									return;
								}
								if (FCConvert.ToDateTime(cmbEndDate[counter].Text).Month < 4 || FCConvert.ToDateTime(cmbEndDate[counter].Text).Month > 6)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbEndDate[counter].Focus();
									return;
								}
								break;
							}
						case "3":
							{
								if (FCConvert.ToDateTime(cmbStartDate[counter].Text).Month < 7 || FCConvert.ToDateTime(cmbStartDate[counter].Text).Month > 9)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbStartDate[counter].Focus();
									return;
								}
								if (FCConvert.ToDateTime(cmbEndDate[counter].Text).Month < 7 || FCConvert.ToDateTime(cmbEndDate[counter].Text).Month > 9)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbEndDate[counter].Focus();
									return;
								}
								break;
							}
						case "4":
							{
								if (FCConvert.ToDateTime(cmbStartDate[counter].Text).Month < 10 || FCConvert.ToDateTime(cmbStartDate[counter].Text).Month > 12)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbStartDate[counter].Focus();
									return;
								}
								if (FCConvert.ToDateTime(cmbEndDate[counter].Text).Month < 10 || FCConvert.ToDateTime(cmbEndDate[counter].Text).Month > 12)
								{
									MessageBox.Show("The date you entered is not part of the quarter you selected.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									cmbEndDate[counter].Focus();
									return;
								}
								break;
							}
					}
					//end switch
				}
				else
				{
					MessageBox.Show("Please enter all the dates before trying to proceed.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			SaveTownInfo();
			if (strReportType == "M")
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					modDuplexPrinting.DuplexPrintReport(rptMMAUnemploymentReport.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
					// rptMMAUnemploymentReport.PrintReport False
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptMMAUnemploymentReport.InstancePtr);
				}
			}
			else
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					modDuplexPrinting.DuplexPrintReport(rptNonMMAUnemploymentReport.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
					// rptNonMMAUnemploymentReport.PrintReport False
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptNonMMAUnemploymentReport.InstancePtr);
				}
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				Close();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveTownInfo()
		{
			clsDRWrapper rsTownInfo = new clsDRWrapper();
			rsTownInfo.OpenRecordset("SELECT * FROM TownInfo");
			if (rsTownInfo.EndOfFile() != true && rsTownInfo.BeginningOfFile() != true)
			{
				rsTownInfo.Edit();
			}
			else
			{
				rsTownInfo.AddNew();
			}
			rsTownInfo.Set_Fields("Employer", fecherFoundation.Strings.Trim(txtEmployer.Text));
			rsTownInfo.Set_Fields("ReportNumber", fecherFoundation.Strings.Trim(txtReportNumber.Text));
			rsTownInfo.Set_Fields("Preparer", fecherFoundation.Strings.Trim(txtPreparer.Text));
			rsTownInfo.Set_Fields("TelephoneNumber", fecherFoundation.Strings.Trim(txtTelephone.Text));
			rsTownInfo.Set_Fields("StateID", fecherFoundation.Strings.Trim(txtStateID.Text));
			rsTownInfo.Set_Fields("FederalID", fecherFoundation.Strings.Trim(txtFederalID.Text));
			rsTownInfo.Set_Fields("MemberCode", fecherFoundation.Strings.Trim(txtMemberCode.Text));
			rsTownInfo.Set_Fields("SeasonalCode", fecherFoundation.Strings.Trim(txtSeasonalCode.Text));
			// .Fields("Month1Start") = txtStartDate(0).Text
			// .Fields("Month1End") = txtEndDate(0).Text
			// .Fields("Month2Start") = txtStartDate(1).Text
			// .Fields("Month2End") = txtEndDate(1).Text
			// .Fields("Month3Start") = txtStartDate(2).Text
			// .Fields("Month3End") = txtEndDate(2).Text
			if (Information.IsDate(cmbStartDate[0].Text))
			{
				rsTownInfo.Set_Fields("Month1Start", cmbStartDate[0].Text);
			}
			if (Information.IsDate(cmbStartDate[1].Text))
			{
				rsTownInfo.Set_Fields("Month2start", cmbStartDate[1].Text);
			}
			if (Information.IsDate(cmbStartDate[2].Text))
			{
				rsTownInfo.Set_Fields("Month3start", cmbStartDate[2].Text);
			}
			if (Information.IsDate(cmbEndDate[0].Text))
			{
				rsTownInfo.Set_Fields("Month1End", cmbEndDate[0].Text);
			}
			else
			{
				rsTownInfo.Set_Fields("month1end", cmbStartDate[0].Text);
				cmbEndDate[0].Text = cmbStartDate[0].Text;
			}
			if (Information.IsDate(cmbEndDate[1].Text))
			{
				rsTownInfo.Set_Fields("Month2end", cmbEndDate[1].Text);
			}
			else
			{
				rsTownInfo.Set_Fields("month2end", cmbStartDate[1].Text);
				cmbEndDate[1].Text = cmbStartDate[1].Text;
			}
			if (Information.IsDate(cmbEndDate[2].Text))
			{
				rsTownInfo.Set_Fields("month3end", cmbEndDate[2].Text);
			}
			else
			{
				rsTownInfo.Set_Fields("month3end", cmbStartDate[2].Text);
				cmbEndDate[2].Text = cmbStartDate[2].Text;
			}
			rsTownInfo.Update(true);
			clsHistoryClass.Compare();
		}

		private void txtReportNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// Private Sub txtStartDate_Validate(Index As Integer, Cancel As Boolean)
		//
		// If IsDate(txtStartDate(Index).Text) Then
		// If Day(txtStartDate(Index).Text) > 12 Then
		// MsgBox "You must include the 12th day of the month in your date range", vbCritical, "Invalid Date"
		// Cancel = True
		// Exit Sub
		// End If
		//
		// If Not IsDate(txtEndDate(Index).Text) Then
		// txtEndDate(Index).Text = txtStartDate(Index).Text
		// End If
		// End If
		// End Sub
		private void FillDateCombos()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			int[] intM = new int[3 + 1];
			DateTime dtDateTemp;
			// vbPorter upgrade warning: dtDate1 As DateTime	OnWrite(string)
			DateTime dtDate1;
			DateTime dtDate2;
			int x;
			if (intLastMonthInQuarter < 3)
				return;
			intM[2] = intLastMonthInQuarter;
			intM[1] = intM[2] - 1;
			intM[0] = intM[2] - 2;
			// first months combos
			for (x = 0; x <= 2; x++)
			{
				dtDate1 = FCConvert.ToDateTime(FCConvert.ToString(intM[x]) + "/1/" + FCConvert.ToString(lngYearCovered));
				dtDateTemp = fecherFoundation.DateAndTime.DateAdd("m", 1, dtDate1);
				dtDate2 = fecherFoundation.DateAndTime.DateAdd("d", -1, dtDateTemp);
				strSQL = "select paydate from tblcheckdetail where paydate between '" + FCConvert.ToString(dtDate1) + "' and '" + FCConvert.ToString(dtDate2) + "' and checkvoid = 0 group by paydate order by paydate";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				cmbStartDate[x].Clear();
				cmbEndDate[x].Clear();
				cmbStartDate[x].AddItem("");
				cmbEndDate[x].AddItem("");
				while (!clsLoad.EndOfFile())
				{
					cmbStartDate[x].AddItem(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					cmbEndDate[x].AddItem(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					clsLoad.MoveNext();
				}
			}
			// x
		}
	}
}
