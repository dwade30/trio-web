﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt941.
	/// </summary>
	partial class rpt941
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt941));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblMQY = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFedWithheld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFicaMedWithheld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFicaMedDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalWithheld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFederalTaxableGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFICATaxableGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMedicareTaxableGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateTaxableGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateTaxWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblMQY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedWithheld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFicaMedWithheld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFicaMedDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWithheld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTaxableGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATaxableGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicareTaxableGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxableGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label6,
				this.Label7,
				this.Line2,
				this.Label9,
				this.Label10,
				this.txtFedWithheld,
				this.txtFicaMedWithheld,
				this.txtFedDue,
				this.txtFicaMedDue,
				this.txtTotalWithheld,
				this.txtTotalDue,
				this.txtDue,
				this.Line3,
				this.Label11,
				this.txtFederalTaxableGross,
				this.Label12,
				this.txtFICATaxableGross,
				this.Label13,
				this.txtMedicareTaxableGross,
				this.Label14,
				this.txtStateTaxableGross,
				this.Label15,
				this.txtStateTaxWH
			});
			this.Detail.Height = 3.385417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblMQY,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Line1,
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.Label2,
				this.txtTime,
				this.txtPage,
				this.lblDate
			});
			this.PageHeader.Height = 1.385417F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblMQY
			// 
			this.lblMQY.Height = 0.1875F;
			this.lblMQY.HyperLink = null;
			this.lblMQY.Left = 3.28125F;
			this.lblMQY.MultiLine = false;
			this.lblMQY.Name = "lblMQY";
			this.lblMQY.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.lblMQY.Text = null;
			this.lblMQY.Top = 0.6875F;
			this.lblMQY.Width = 1F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.90625F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label3.Text = "CATEGORY";
			this.Label3.Top = 1.1875F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.09375F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label4.Text = "WITHHELD";
			this.Label4.Top = 1.1875F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.65625F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label5.Text = "DUE";
			this.Label5.Top = 1.1875F;
			this.Label5.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.84375F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.375F;
			this.Line1.Width = 3.875F;
			this.Line1.X1 = 1.84375F;
			this.Line1.X2 = 5.71875F;
			this.Line1.Y1 = 1.375F;
			this.Line1.Y2 = 1.375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size" + ": 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL -  FTD (FICA / FED WTH)";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.625F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.Label2.Text = "Type of Tax = 941 (8109)";
			this.Label2.Top = 0.40625F;
			this.Label2.Width = 4.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.4375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblDate.Text = null;
			this.lblDate.Top = 0.21875F;
			this.lblDate.Width = 4.625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 1.84375F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label6.Text = "Federal Tax";
			this.Label6.Top = 0.0625F;
			this.Label6.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.84375F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label7.Text = "Fica/Medicare";
			this.Label7.Top = 0.375F;
			this.Label7.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.8125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.6875F;
			this.Line2.Width = 3.90625F;
			this.Line2.X1 = 1.8125F;
			this.Line2.X2 = 5.71875F;
			this.Line2.Y1 = 0.6875F;
			this.Line2.Y2 = 0.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.84375F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label9.Text = "Total";
			this.Label9.Top = 0.8125F;
			this.Label9.Width = 1F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4.09375F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label10.Text = "Due";
			this.Label10.Top = 1.125F;
			this.Label10.Width = 0.34375F;
			// 
			// txtFedWithheld
			// 
			this.txtFedWithheld.Height = 0.1875F;
			this.txtFedWithheld.Left = 2.96875F;
			this.txtFedWithheld.Name = "txtFedWithheld";
			this.txtFedWithheld.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFedWithheld.Text = null;
			this.txtFedWithheld.Top = 0.0625F;
			this.txtFedWithheld.Width = 1.125F;
			// 
			// txtFicaMedWithheld
			// 
			this.txtFicaMedWithheld.Height = 0.1875F;
			this.txtFicaMedWithheld.Left = 2.96875F;
			this.txtFicaMedWithheld.Name = "txtFicaMedWithheld";
			this.txtFicaMedWithheld.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFicaMedWithheld.Text = null;
			this.txtFicaMedWithheld.Top = 0.375F;
			this.txtFicaMedWithheld.Width = 1.125F;
			// 
			// txtFedDue
			// 
			this.txtFedDue.Height = 0.1875F;
			this.txtFedDue.Left = 4.53125F;
			this.txtFedDue.Name = "txtFedDue";
			this.txtFedDue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFedDue.Text = null;
			this.txtFedDue.Top = 0.0625F;
			this.txtFedDue.Width = 1.125F;
			// 
			// txtFicaMedDue
			// 
			this.txtFicaMedDue.Height = 0.1875F;
			this.txtFicaMedDue.Left = 4.53125F;
			this.txtFicaMedDue.Name = "txtFicaMedDue";
			this.txtFicaMedDue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFicaMedDue.Text = null;
			this.txtFicaMedDue.Top = 0.375F;
			this.txtFicaMedDue.Width = 1.125F;
			// 
			// txtTotalWithheld
			// 
			this.txtTotalWithheld.Height = 0.1875F;
			this.txtTotalWithheld.Left = 2.96875F;
			this.txtTotalWithheld.Name = "txtTotalWithheld";
			this.txtTotalWithheld.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalWithheld.Text = null;
			this.txtTotalWithheld.Top = 0.8125F;
			this.txtTotalWithheld.Width = 1.125F;
			// 
			// txtTotalDue
			// 
			this.txtTotalDue.Height = 0.1875F;
			this.txtTotalDue.Left = 4.53125F;
			this.txtTotalDue.Name = "txtTotalDue";
			this.txtTotalDue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalDue.Text = null;
			this.txtTotalDue.Top = 0.8125F;
			this.txtTotalDue.Width = 1.125F;
			// 
			// txtDue
			// 
			this.txtDue.Height = 0.1875F;
			this.txtDue.Left = 4.53125F;
			this.txtDue.Name = "txtDue";
			this.txtDue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtDue.Text = null;
			this.txtDue.Top = 1.125F;
			this.txtDue.Width = 1.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.09375F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.09375F;
			this.Line3.Width = 1.59375F;
			this.Line3.X1 = 4.09375F;
			this.Line3.X2 = 5.6875F;
			this.Line3.Y1 = 1.09375F;
			this.Line3.Y2 = 1.09375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.2F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.9F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label11.Text = "Federal Taxable Gross";
			this.Label11.Top = 1.766667F;
			this.Label11.Width = 1.566667F;
			// 
			// txtFederalTaxableGross
			// 
			this.txtFederalTaxableGross.Height = 0.1979167F;
			this.txtFederalTaxableGross.Left = 4.53125F;
			this.txtFederalTaxableGross.Name = "txtFederalTaxableGross";
			this.txtFederalTaxableGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFederalTaxableGross.Text = null;
			this.txtFederalTaxableGross.Top = 1.770833F;
			this.txtFederalTaxableGross.Width = 1.135417F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.2F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.9F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label12.Text = "FICA  Taxable Gross";
			this.Label12.Top = 2F;
			this.Label12.Width = 1.566667F;
			// 
			// txtFICATaxableGross
			// 
			this.txtFICATaxableGross.Height = 0.2F;
			this.txtFICATaxableGross.Left = 4.533333F;
			this.txtFICATaxableGross.Name = "txtFICATaxableGross";
			this.txtFICATaxableGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFICATaxableGross.Text = null;
			this.txtFICATaxableGross.Top = 2F;
			this.txtFICATaxableGross.Width = 1.133333F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.2F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.9F;
			this.Label13.MultiLine = false;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label13.Text = "Medicare Taxable Gross";
			this.Label13.Top = 2.233333F;
			this.Label13.Width = 1.566667F;
			// 
			// txtMedicareTaxableGross
			// 
			this.txtMedicareTaxableGross.Height = 0.2F;
			this.txtMedicareTaxableGross.Left = 4.533333F;
			this.txtMedicareTaxableGross.Name = "txtMedicareTaxableGross";
			this.txtMedicareTaxableGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMedicareTaxableGross.Text = null;
			this.txtMedicareTaxableGross.Top = 2.233333F;
			this.txtMedicareTaxableGross.Width = 1.133333F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.2F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.9F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label14.Text = "State Taxable Gross";
			this.Label14.Top = 2.766667F;
			this.Label14.Width = 1.566667F;
			// 
			// txtStateTaxableGross
			// 
			this.txtStateTaxableGross.Height = 0.2F;
			this.txtStateTaxableGross.Left = 4.533333F;
			this.txtStateTaxableGross.Name = "txtStateTaxableGross";
			this.txtStateTaxableGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtStateTaxableGross.Text = null;
			this.txtStateTaxableGross.Top = 2.766667F;
			this.txtStateTaxableGross.Width = 1.133333F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.2F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.9F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label15.Text = "State Income Tax WH";
			this.Label15.Top = 3F;
			this.Label15.Width = 1.566667F;
			// 
			// txtStateTaxWH
			// 
			this.txtStateTaxWH.Height = 0.2F;
			this.txtStateTaxWH.Left = 4.533333F;
			this.txtStateTaxWH.Name = "txtStateTaxWH";
			this.txtStateTaxWH.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtStateTaxWH.Text = null;
			this.txtStateTaxWH.Top = 3F;
			this.txtStateTaxWH.Width = 1.133333F;
			// 
			// rpt941
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblMQY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedWithheld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFicaMedWithheld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFicaMedDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWithheld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTaxableGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATaxableGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicareTaxableGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxableGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedWithheld;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFicaMedWithheld;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFicaMedDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWithheld;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTaxableGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATaxableGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicareTaxableGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxableGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxWH;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMQY;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
