//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPurgeTerminatedEmployees.
	/// </summary>
	partial class frmPurgeTerminatedEmployees
	{
		public FCGrid vsData;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsData = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdPrintReport = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReport)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 425);
            this.BottomPanel.Size = new System.Drawing.Size(819, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Size = new System.Drawing.Size(819, 365);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintReport);
            this.TopPanel.Size = new System.Drawing.Size(819, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintReport, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(338, 30);
            this.HeaderText.Text = "Purge Terminated Employees";
            // 
            // vsData
            // 
            this.vsData.AllowSelection = false;
            this.vsData.AllowUserToResizeColumns = false;
            this.vsData.AllowUserToResizeRows = false;
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsData.BackColorBkg = System.Drawing.Color.Empty;
            this.vsData.BackColorFixed = System.Drawing.Color.Empty;
            this.vsData.BackColorSel = System.Drawing.Color.Empty;
            this.vsData.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsData.Cols = 5;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsData.ColumnHeadersHeight = 30;
            this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsData.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsData.DragIcon = null;
            this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsData.FixedCols = 0;
            this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsData.FrozenCols = 0;
            this.vsData.GridColor = System.Drawing.Color.Empty;
            this.vsData.GridColorFixed = System.Drawing.Color.Empty;
            this.vsData.Location = new System.Drawing.Point(30, 30);
            this.vsData.Name = "vsData";
            this.vsData.OutlineCol = 0;
            this.vsData.ReadOnly = true;
            this.vsData.RowHeadersVisible = false;
            this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsData.RowHeightMin = 0;
            this.vsData.Rows = 1;
            this.vsData.ScrollTipText = null;
            this.vsData.ShowColumnVisibilityMenu = false;
            this.vsData.Size = new System.Drawing.Size(761, 327);
            this.vsData.StandardTab = true;
            this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsData.TabIndex = 0;
            this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSP1,
            this.mnuPrintPreview,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue     ";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 2;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print Report";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 3;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(312, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // cmdPrintReport
            // 
            this.cmdPrintReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintReport.AppearanceKey = "toolbarButton";
            this.cmdPrintReport.Location = new System.Drawing.Point(701, 29);
            this.cmdPrintReport.Name = "cmdPrintReport";
            this.cmdPrintReport.Size = new System.Drawing.Size(90, 24);
            this.cmdPrintReport.TabIndex = 1;
            this.cmdPrintReport.Text = "Print Report";
            this.cmdPrintReport.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // frmPurgeTerminatedEmployees
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(819, 533);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPurgeTerminatedEmployees";
            this.Text = "Purge Terminated Employees";
            this.Load += new System.EventHandler(this.frmPurgeTerminatedEmployees_Load);
            this.Activated += new System.EventHandler(this.frmPurgeTerminatedEmployees_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPurgeTerminatedEmployees_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeTerminatedEmployees_KeyPress);
            this.Resize += new System.EventHandler(this.frmPurgeTerminatedEmployees_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReport)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdPrintReport;
	}
}