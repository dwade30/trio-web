//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCheckRecipientsCategories : BaseForm
	{
		public frmCheckRecipientsCategories()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCheckRecipientsCategories InstancePtr
		{
			get
			{
				return (frmCheckRecipientsCategories)Sys.GetInstance(typeof(frmCheckRecipientsCategories));
			}
		}

		protected frmCheckRecipientsCategories _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: DAN C. SOLTESZ
		// DATE:       JUNE 25,2001
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		// Private dbDeductions    As DAO.Database
		private clsDRWrapper rsCategories = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		private DialogResult intDataChanged;
		private clsHistory clsHistoryClass = new clsHistory();
		// used for grid categories
		const int Code = 0;
		const int DESC2 = 1;
		const int RECIPIENT = 2;
		const int HIDDEN2 = 3;
		const int ID = 4;
		const int FREQ = 5;
		const int NAME1 = 6;
		const int Address = 7;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void frmCheckRecipientsCategories_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				clsDRWrapper rsCategories = new clsDRWrapper();
				rsCategories.OpenRecordset("SELECT Name,RecptNumber,ID FROM tblRecipients order by RecptNumber", "TWPY0000.vb1");
				if (!rsCategories.EndOfFile())
				{
					rsCategories.MoveLast();
					rsCategories.MoveFirst();
					vsCategories.ColComboList(RECIPIENT, string.Empty);
					// makes this column work like a combo box.
					// this fills the box with the values from the tblRecipients table
					// This forces this column to work as a combo box
					for (intCounter = 0; intCounter <= (rsCategories.RecordCount()); intCounter++)
					{
						if (intCounter == 0)
						{
							vsCategories.ColComboList(RECIPIENT, vsCategories.ColComboList(RECIPIENT) + "#0;0" + "\t" + "Stop Checks");
						}
						else
						{
							vsCategories.ColComboList(RECIPIENT, vsCategories.ColComboList(RECIPIENT) + "|#" + rsCategories.Get_Fields("ID") + ";" + rsCategories.Get_Fields_Int32("RecptNumber") + "\t" + rsCategories.Get_Fields_String("Name"));
						}
						if (intCounter != 0)
							rsCategories.MoveNext();
					}
				}
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid category properties
				vsCategories.Cols = 8;
				vsCategories.FixedRows = 1;
				vsCategories.ColHidden(HIDDEN2, true);
				vsCategories.ColHidden(ID, true);
				// vsCategories.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsCategories.Cols - 1) = True
				// set column 0 properties
				vsCategories.ColAlignment(Code, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(Code, 900);
				vsCategories.TextMatrix(Code, Code, "Ded Code");
				vsCategories.ColComboList(Code, string.Empty);
				rsCategories.OpenRecordset("SELECT Description,ID,DeductionNumber FROM tblDeductionSetup order by ID", "TWPY0000.vb1");
				if (!rsCategories.EndOfFile())
				{
					rsCategories.MoveLast();
					rsCategories.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblPayCategories table
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsCategories.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							vsCategories.ColComboList(Code, vsCategories.ColComboList(Code) + "#" + rsCategories.Get_Fields("ID") + ";" + rsCategories.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsCategories.Get_Fields("ID"))) != "" ? "\t" + rsCategories.Get_Fields("Description") : ""));
						}
						else
						{
							vsCategories.ColComboList(Code, vsCategories.ColComboList(Code) + "|#" + rsCategories.Get_Fields("ID") + ";" + rsCategories.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsCategories.Get_Fields("ID"))) != "" ? "\t" + rsCategories.Get_Fields("Description") : ""));
						}
						rsCategories.MoveNext();
					}
				}
				// set column 1 properties
				vsCategories.ColAlignment(DESC2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(DESC2, 1500);
				vsCategories.TextMatrix(0, DESC2, "Deduction Name");
				// set column 2 properties
				vsCategories.ColAlignment(RECIPIENT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(RECIPIENT, 850);
				vsCategories.TextMatrix(0, RECIPIENT, "Recipient");
				vsCategories.ColComboList(RECIPIENT, string.Empty);
				rsCategories.OpenRecordset("SELECT Name,RecptNumber,ID FROM tblRecipients order by RecptNumber", "TWPY0000.vb1");
				if (!rsCategories.EndOfFile())
				{
					rsCategories.MoveLast();
					rsCategories.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblRecipients table
					// This forces this column to work as a combo box
					for (intCounter = 0; intCounter <= (rsCategories.RecordCount()); intCounter++)
					{
						if (intCounter == 0)
						{
							vsCategories.ColComboList(RECIPIENT, vsCategories.ColComboList(RECIPIENT) + "#0;0" + "\t" + "Stop Checks");
						}
						else
						{
							vsCategories.ColComboList(RECIPIENT, vsCategories.ColComboList(RECIPIENT) + "|#" + rsCategories.Get_Fields("ID") + ";" + rsCategories.Get_Fields_Int32("RecptNumber") + "\t" + rsCategories.Get_Fields_String("Name"));
						}
						if (intCounter != 0)
							rsCategories.MoveNext();
					}
				}
				vsCategories.ColAlignment(HIDDEN2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(HIDDEN2, 300);
				vsCategories.ColAlignment(FREQ, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(FREQ, 900);
				vsCategories.TextMatrix(0, FREQ, "Freq");
				vsCategories.ColAlignment(NAME1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(NAME1, 2200);
				vsCategories.TextMatrix(0, NAME1, "Name");
				vsCategories.ColAlignment(Address, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsCategories.ColWidth(Address, 2000);
				vsCategories.TextMatrix(0, Address, "Address");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void UpdateCombo()
		{
			rsCategories.OpenRecordset("SELECT RecptNumber,ID FROM tblRecipients order by RecptNumber", "TWPY0000.vb1");
			if (!rsCategories.EndOfFile())
			{
				rsCategories.MoveLast();
				rsCategories.MoveFirst();
				// makes this column work like a combo box.
				// this fills the box with the values from the tblRecipients table
				// This forces this column to work as a combo box
				for (intCounter = 1; intCounter <= (rsCategories.RecordCount()); intCounter++)
				{
					if (intCounter == 1)
					{
						vsCategories.ColComboList(RECIPIENT, vsCategories.ColComboList(RECIPIENT) + "#" + rsCategories.Get_Fields("ID") + ";" + rsCategories.Get_Fields_Int32("RecptNumber"));
						// & IIf(Trim(rsCategories.Fields("ID")) <> "", "        - " & rsCategories.Fields("Description"), "")
					}
					else
					{
						vsCategories.ColComboList(RECIPIENT, vsCategories.ColComboList(RECIPIENT) + "|#" + rsCategories.Get_Fields("ID") + ";" + rsCategories.Get_Fields_Int32("RecptNumber"));
						// & IIf(Trim(rsCategories.Fields("ID")) <> "", "        - " & rsCategories.Fields("Description"), "")
					}
					rsCategories.MoveNext();
				}
			}
		}

		private void LoadGrid()
		{
			try
			{
                //FC:FINAL:SBE - fix exception on client side
                vsCategories.Redraw = false;
                // On Error GoTo CallErrorRoutine
                fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsRecipient = new clsDRWrapper();
				// vbPorter upgrade warning: TempString As object	OnWrite(string(), string)
				string[] TempString = null;
				vsCategories.Rows = 1;
				rsCategories.OpenRecordset("SELECT tblCategories.* FROM tblCategories Order by ID", "TWPY0000.vb1");
				if (!rsCategories.EndOfFile())
				{
					rsCategories.MoveLast();
					rsCategories.MoveFirst();
					for (intCounter = 1; intCounter <= (rsCategories.RecordCount()); intCounter++)
					{
						vsCategories.AddItem("", vsCategories.Rows);
						vsCategories.TextMatrix(intCounter, Code, (FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsCategories.Get_Fields("Code") + " ")) == 0 ? "" : fecherFoundation.Strings.Trim(rsCategories.Get_Fields("Code") + " ")));
						// when user changes code in combo box we want to change the description with it
						if (intCounter > 4)
						{
							TempString = Strings.Split(vsCategories.ColComboList(0), "#" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsCategories.Get_Fields("Code"))), -1, CompareConstants.vbBinaryCompare);
							if (Information.UBound(TempString, 1) > 0)
							{
								TempString = Strings.Split(TempString[1], "|", -1, CompareConstants.vbBinaryCompare);
                                //FC:FINAL:AM:#4193 - combolist contains \t not spaces
                                //string tempString = fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.Trim(FCConvert.ToString(TempString[0])), 21, TempString[0].Length - 3));
                                string tempString = fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.Trim(FCConvert.ToString(TempString[0])), TempString[0].IndexOf("\t") + 2, TempString[0].Length - 3));
                                vsCategories.TextMatrix(intCounter, DESC2, tempString);
							}
							else
							{
								vsCategories.TextMatrix(intCounter, DESC2, fecherFoundation.Strings.Trim(FCConvert.ToString(rsCategories.Get_Fields("Description"))));
							}
						}
						else
						{
							// let the first five hardcoded go thru
							vsCategories.TextMatrix(intCounter, DESC2, fecherFoundation.Strings.Trim(FCConvert.ToString(rsCategories.Get_Fields("Description"))));
						}
						// we want to show the number for include that matches the ID of RecptNumber
						vsCategories.TextMatrix(intCounter, RECIPIENT, fecherFoundation.Strings.Trim(rsCategories.Get_Fields("Include") + " "));
						vsCategories.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsCategories.Get_Fields("ID") + " "));
						rsRecipient.OpenRecordset("Select * from tblRecipients where ID =" + fecherFoundation.Strings.Trim(rsCategories.Get_Fields("Include") + " "), "TWPY0000.vb1");
						if (!rsRecipient.EndOfFile())
						{
							rsRecipient.MoveLast();
							rsRecipient.MoveFirst();
							if (FCConvert.ToString(rsRecipient.Get_Fields("Freq")) == "Weekly")
							{
								vsCategories.TextMatrix(intCounter, FREQ, "Weekly");
							}
							else if (rsRecipient.Get_Fields("Freq") == "Monthly")
							{
								vsCategories.TextMatrix(intCounter, FREQ, "Monthly");
							}
							else if (rsRecipient.Get_Fields("Freq") == "Quarterly")
							{
								vsCategories.TextMatrix(intCounter, FREQ, "Quarterly");
							}
							else
							{
								vsCategories.TextMatrix(intCounter, FREQ, string.Empty);
							}
							vsCategories.TextMatrix(intCounter, NAME1, FCConvert.ToString(rsRecipient.Get_Fields_String("Name")));
							vsCategories.TextMatrix(intCounter, Address, FCConvert.ToString(rsRecipient.Get_Fields_String("Address1")));
						}
						vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, DESC2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, FREQ, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, NAME1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, Address, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						rsCategories.MoveNext();
					}
				}
				if (vsCategories.Rows >= 4)
				{
					vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, Code, 4, Code, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsCategories.Select(vsCategories.Rows - 1, RECIPIENT);
				}
				vsCategories.Cell(FCGrid.CellPropertySettings.flexcpFontName, 1, 0, vsCategories.Rows - 1, vsCategories.Cols - 1, vsCategories.Font.Name);
				//vsCategories.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsCategories.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				clsHistoryClass.Init = this;
                //FC:FINAL:SBE - fix exception on client side
                vsCategories.Redraw = true;
                //FC:FINAL:BSE:#4194 grid view should start at top
                vsCategories.Row = 1;
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
                //FC:FINAL:SBE - fix exception on client side
                vsCategories.Redraw = true;
                return;
			}
		}

		private void frmCheckRecipientsCategories_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmCheckRecipientsCategories_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCheckRecipientsCategories_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int counter;
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				rsCategories.DefaultDB = "TWPY0000.vb1";
				// set grid width/captions etc...
				SetGridProperties();
				clsHistoryClass.SetGridIDColumn("PY", "vsCategories", ID);
				// fill grid
				LoadGrid();
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
				intDataChanged = 0;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmCheckRecipientsCategories_Resize(object sender, System.EventArgs e)
		{
			vsCategories.Cols = 8;
			vsCategories.ColWidth(Code, FCConvert.ToInt32(vsCategories.WidthOriginal * 0.1));
			vsCategories.ColWidth(DESC2, FCConvert.ToInt32(vsCategories.WidthOriginal * 0.17));
			vsCategories.ColWidth(RECIPIENT, FCConvert.ToInt32(vsCategories.WidthOriginal * 0.1));
			// vsCategories.ColWidth(HIDDEN2) = vsCategories.Width * 0.05
			vsCategories.ColWidth(FREQ, FCConvert.ToInt32(vsCategories.WidthOriginal * 0.1));
			vsCategories.ColWidth(NAME1, FCConvert.ToInt32(vsCategories.WidthOriginal * 0.22));
			vsCategories.ColWidth(Address, FCConvert.ToInt32(vsCategories.WidthOriginal * 0.29));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			this.vsCategories.Select(0, 0);
			SaveChanges();
			if (intDataChanged == (DialogResult)2)
			{
				e.Cancel = true;
				return;
			}
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuDeleteCategory_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuDeleteCategory_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsCategories.Row < 0)
					return;
				if (vsCategories.Col < 0)
					return;
				int TempRow = 0;
				TempRow = FCConvert.ToInt32(Math.Round(Conversion.Val(vsCategories.TextMatrix(vsCategories.Row, ID))));
				if (vsCategories.Row <= 4)
				{
					MessageBox.Show("Not Allowed To Delete This Row", "Payroll", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (MessageBox.Show("This action will delete Deduction #" + FCConvert.ToString(vsCategories.Row) + ". Continue?", "Payroll Check Recipient and Categories", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// if new row and not saved then just delete from grid and don't worry about database
					if (Strings.Right(FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCategories.Row, HIDDEN2)), 2) == "..")
					{
						vsCategories.RemoveItem(vsCategories.Row);
						// SetGridHeight
						return;
					}
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsCategories);
					// Dave 12/14/2006---------------------------------------------------
					// Add change record for adding a row to the grid
					clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsCategories.Row) + " from Check Recipient Categories");
					// ------------------------------------------------------------------
					// need to clean up grid and database
					vsCategories.RemoveItem(vsCategories.Row);
					// SetGridHeight
					rsCategories.Execute("Delete from tblCategories where ID = " + FCConvert.ToString(TempRow), "Payroll");
					// load the grid with the new data to show the 'clean out' of the current row
					LoadGrid();
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Check Recipient and Categories", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNewCategory_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuNewCategory";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsCategories.AddItem("", vsCategories.Rows);
				vsCategories.TextMatrix(vsCategories.Rows - 1, Code, "");
				vsCategories.TextMatrix(vsCategories.Rows - 1, DESC2, "");
				vsCategories.TextMatrix(vsCategories.Rows - 1, RECIPIENT, "0");
				vsCategories.TextMatrix(vsCategories.Rows - 1, HIDDEN2, "..");
				vsCategories.TextMatrix(vsCategories.Rows - 1, ID, "0");
				vsCategories.TextMatrix(vsCategories.Rows - 1, FREQ, "");
				vsCategories.TextMatrix(vsCategories.Rows - 1, NAME1, "");
				vsCategories.TextMatrix(vsCategories.Rows - 1, Address, "");
				vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Rows - 1, DESC2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Rows - 1, FREQ, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Rows - 1, NAME1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Rows - 1, Address, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsCategories.Select(vsCategories.Rows - 1, Code);
				intDataChanged += 1;
				// Call SetGridHeight
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Check Recipient Categories");
				// ------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuSave";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				if (NotValidData() == true)
					return;
                //FC:FINAL:SBE - fix exception on client side
                vsCategories.Redraw = false;
				vsCategories.Select(0, 0);
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Check Recipient Categories", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				rsCategories.OpenRecordset("SELECT * FROM tblCategories ORDER BY Code", "TWPY0000.vb1");
				if (!rsCategories.EndOfFile())
				{
					rsCategories.MoveLast();
					rsCategories.MoveFirst();
					for (intCounter = 1; intCounter <= (vsCategories.Rows - 1); intCounter++)
					{
						// If vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, RECIPIENT) <> 0 Then
						// If Right(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, HIDDEN2), 1) = "." Then
						// If vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Code) = vbNullString And vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, RECIPIENT) = 0 Then
						// Else
						if (FCConvert.ToDouble(vsCategories.TextMatrix(intCounter, ID)) != 0)
						{
							rsCategories.FindFirstRecord("ID", vsCategories.TextMatrix(intCounter, ID));
							if (rsCategories.NoMatch)
							{
								// if user added a new record to grid then add a record to database also
								rsCategories.AddNew();
							}
							else
							{
								// just saving data that was there on load
								rsCategories.Edit();
							}
							if (intCounter > 4)
							{
								rsCategories.SetData("Code", Conversion.Val(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Code)));
								rsCategories.SetData("Description", vsCategories.TextMatrix(intCounter, DESC2));
							}
							rsCategories.SetData("Include", vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, RECIPIENT));
							vsCategories.TextMatrix(intCounter, HIDDEN2, "");
							rsCategories.Update();
						}
						else
						{
							rsCategories.AddNew();
							rsCategories.SetData("Code", Conversion.Val(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Code)));
							rsCategories.SetData("Description", vsCategories.TextMatrix(intCounter, DESC2));
							rsCategories.SetData("Include", vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, RECIPIENT));
							vsCategories.TextMatrix(intCounter, HIDDEN2, "");
							rsCategories.Update();
						}
						// End If
						// End If
					}
				}
				else
				{
					for (intCounter = 1; intCounter <= (vsCategories.Rows - 1); intCounter++)
					{
						if (Strings.Right(FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, HIDDEN2)), 1) == ".")
						{
							// if user added a new record to grid then add a record to database also
							rsCategories.AddNew();
						}
						else
						{
							// just saving data that was there on load
							rsCategories.Edit();
						}
						rsCategories.SetData("Code", Conversion.Val(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Code)));
						rsCategories.SetData("Description", vsCategories.TextMatrix(intCounter, DESC2));
						rsCategories.SetData("Include", vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, RECIPIENT));
						vsCategories.TextMatrix(intCounter, HIDDEN2, "");
						// rsCategories.SetData "Freq", vsCategories.TextMatrix(intCounter, FREQ)
						// rsCategories.SetData "Name", vsCategories.TextMatrix(intCounter, NAME1)
						// rsCategories.SetData "Address", vsCategories.TextMatrix(intCounter, ADDRESS)
						rsCategories.Update();
					}
				}
				intDataChanged = 0;
				clsHistoryClass.Compare();
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Check Recipients Categories", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SetGridProperties();
				LoadGrid();
                //FC:FINAL:SBE - fix exception on client side
                vsCategories.Redraw = true;
                // Call ForceFormToResize(Me)			
            }
            catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
                //FC:FINAL:SBE - fix exception on client side
                vsCategories.Redraw = true;
                return;
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}

		private void vsCategories_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (Strings.Right(FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCategories.Row, HIDDEN2)), 1) != ".")
			{
				// increment the counter as to how many rows are dirty
				intDataChanged += 1;
				// Change the row number in the first column to indicate that this record has been edited
				vsCategories.TextMatrix(vsCategories.Row, HIDDEN2, vsCategories.TextMatrix(vsCategories.Row, HIDDEN2) + ".");
			}
			if (vsCategories.Col == Code)
			{
				string TempString = "";
				if (FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCategories.Row, vsCategories.Col)) != string.Empty)
				{
                    // when user changes code in combo box we want to change the description with it
                    //FC:FINAL:AM:#4193 - get the text from the combo
                    //TempString = Strings.Right(FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCategories.Row, vsCategories.Col)), FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCategories.Row, vsCategories.Col)).Length - (Strings.InStr(FCConvert.ToString(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCategories.Row, vsCategories.Col)), "-", CompareConstants.vbBinaryCompare) + 1));
                    TempString = vsCategories.ComboText;
					vsCategories.TextMatrix(vsCategories.Row, DESC2, TempString);
				}
			}
			if (vsCategories.Col == RECIPIENT)
			{
				// when user changes code in combo box we want to change the freq, name, address with it
				rsCategories.OpenRecordset("Select * from tblRecipients where ID =" + vsCategories.Cell(FCGrid.CellPropertySettings.flexcpText, vsCategories.Row, vsCategories.Col), "TWPY0000.vb1");
				if (!rsCategories.EndOfFile())
				{
					rsCategories.MoveLast();
					rsCategories.MoveFirst();
					if (FCConvert.ToString(rsCategories.Get_Fields("Freq")) == "Weekly")
					{
						vsCategories.TextMatrix(vsCategories.Row, FREQ, "Weekly");
					}
					else if (rsCategories.Get_Fields("Freq") == "Monthly")
					{
						vsCategories.TextMatrix(vsCategories.Row, FREQ, "Monthly");
					}
					else if (rsCategories.Get_Fields("Freq") == "Quarterly")
					{
						vsCategories.TextMatrix(vsCategories.Row, FREQ, "Quarterly");
					}
					else
					{
						vsCategories.TextMatrix(vsCategories.Row, FREQ, string.Empty);
					}
					vsCategories.TextMatrix(vsCategories.Row, NAME1, FCConvert.ToString(rsCategories.Get_Fields_String("Name")));
					vsCategories.TextMatrix(vsCategories.Row, Address, FCConvert.ToString(rsCategories.Get_Fields_String("Address1")));
					rsCategories.MoveNext();
				}
				else
				{
					vsCategories.TextMatrix(vsCategories.Row, FREQ, string.Empty);
					vsCategories.TextMatrix(vsCategories.Row, NAME1, string.Empty);
					vsCategories.TextMatrix(vsCategories.Row, Address, string.Empty);
				}
			}
			intDataChanged += 1;
		}

		private void vsCategories_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == (Keys)37)
			{
				if (vsCategories.Col == 2)
				{
					KeyCode = 0;
					if (FCConvert.ToInt32(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Row, 0)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsCategories.Col = 0;
					}
				}
				else if (vsCategories.Col == 0)
				{
					if (vsCategories.Row > 1)
					{
						vsCategories.Row -= 1;
						vsCategories.Col = 2;
						KeyCode = 0;
					}
				}
			}
			else if (KeyCode == (Keys)38)
			{
				if (vsCategories.Col == 0)
				{
					if (FCConvert.ToInt32(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Row - 1, 0)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						KeyCode = 0;
					}
				}
			}
			else if (KeyCode == (Keys)39 || KeyCode == Keys.Tab || KeyCode == Keys.Return)
			{
				if (vsCategories.Col == 0)
				{
					KeyCode = 0;
					vsCategories.Col = 2;
				}
			}
		}

		private void vsCategories_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == (Keys)37)
			{
				if (vsCategories.Col == 2)
				{
					KeyCode = 0;
					vsCategories.Col = 0;
				}
				else if (vsCategories.Col == 0)
				{
					if (vsCategories.Row > 1)
					{
						vsCategories.Row -= 1;
						vsCategories.Col = 2;
						KeyCode = 0;
					}
				}
			}
			else if (KeyCode == (Keys)39 || KeyCode == Keys.Tab || KeyCode == Keys.Return)
			{
				if (vsCategories.Col == 0)
				{
					KeyCode = 0;
					vsCategories.Col = 2;
				}
			}
		}

		private void vsCategories_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsCategories.Col == RECIPIENT && (KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)))
			{
				KeyAscii = 0;
			}
			if (FCConvert.ToInt32(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Row, vsCategories.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				KeyAscii = 0;
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			for (intCounter = 1; intCounter <= (vsCategories.Rows - 1); intCounter++)
			{
				if (vsCategories.TextMatrix(intCounter, Code) != string.Empty || vsCategories.TextMatrix(intCounter, RECIPIENT) != "00")
				{
					if (intCounter > 4)
					{
						for (intRow = 5; intRow <= (vsCategories.Rows); intRow++)
						{
							// CHECK TO SEE IF THERE ARE DUPLICATES
							if (vsCategories.TextMatrix(intCounter, Code) == vsCategories.TextMatrix(intRow, Code))
							{
								if (intCounter == intRow)
								{
									// THIS IS THE SAME ROW SO IT IS OK
									break;
								}
								else
								{
									MessageBox.Show("Duplicate deductions cannot be defined.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
									NotValidData = true;
									vsCategories.Select(intCounter, Code);
									return NotValidData;
								}
							}
						}
					}
					if (vsCategories.TextMatrix(intCounter, DESC2) == string.Empty)
					{
						MessageBox.Show("Please Enter Description", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsCategories.Select(vsCategories.Row, DESC2);
						NotValidData = true;
						return NotValidData;
					}
					if (intCounter > 4)
					{
						if (vsCategories.TextMatrix(intCounter, Code) == string.Empty)
						{
							if (intCounter != vsCategories.Rows - 1)
							{
								MessageBox.Show("Please Make Selection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								vsCategories.Select(vsCategories.Row, Code);
								NotValidData = true;
								return NotValidData;
							}
						}
					}
					if (vsCategories.TextMatrix(intCounter, RECIPIENT) == string.Empty || vsCategories.TextMatrix(intCounter, RECIPIENT) == "00")
					{
						// make this a stop check
						vsCategories.TextMatrix(intCounter, RECIPIENT, FCConvert.ToString(0));
						// MsgBox "Please Make Selection", , "Error"
						// vsCategories.Select vsCategories.Row, RECIPIENT
						// NotValidData = True
						// Exit Function
					}
				}
			}
			NotValidData = false;
			return NotValidData;
		}

		private void vsCategories_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
            //FC:FINAL:SBE - #i2149 - disable editing on gray columns (SendKeys({TAB}) does not work in Wisej)
            //if (vsCategories.Col == 1)
            //	vsCategories.Col = 2;
        }

        private void vsCategories_RowColChange(object sender, System.EventArgs e)
		{
			if (vsCategories.Row < 4 && vsCategories.Col != Code && vsCategories.Col != RECIPIENT)
			{
				vsCategories.Editable = FCGrid.EditableSettings.flexEDNone;
                //FC:FINAL:SBE - #i2149 - disable editing on gray columns (SendKeys({TAB}) does not work in Wisej)
                vsCategories.Col = 2;
            }
            //FC:FINAL:SBE - #i2149 - disable editing on gray columns (SendKeys({TAB}) does not work in Wisej)
            else if (vsCategories.Col > 2)
            {
                vsCategories.Editable = FCGrid.EditableSettings.flexEDNone;
                vsCategories.Col = 0;
            }
            else if (vsCategories.Col == 1)
            {
                vsCategories.Editable = FCGrid.EditableSettings.flexEDNone;
                vsCategories.Col = 2;
            }
			else
			{
				vsCategories.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			if (vsCategories.Col == 0 && FCConvert.ToInt32(vsCategories.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCategories.Row, 0)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				//Support.SendKeys("{TAB}", false);
			}
			if (vsCategories.Col > 2)
			{
				if (vsCategories.Row < vsCategories.Rows - 1)
				{
					//Support.SendKeys("{TAB}", false);
				}
				else
				{
					// Call mnuNewCategory_Click
				}
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsCategories.Rows - 1); intRows++)
			{
				for (intCols = 0; intCols <= (vsCategories.Cols - 1); intCols++)
				{
					if (intCols == 0 || intCols == 2)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsCategories";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsCategories.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}
	}
}
