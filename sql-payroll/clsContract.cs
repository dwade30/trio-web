//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsContract
	{
		//=========================================================
		private int lngContractID;
		private string strDescription = string.Empty;
		private double dblOriginalAmount;
		private double dblPaid;
		// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string, int, DateTime)
		private DateTime dtStartDate;
		// vbPorter upgrade warning: dtEndDate As DateTime	OnWrite(string, int, DateTime)
		private DateTime dtEndDate;
		private string strEmployeeNumber = string.Empty;
		private int lngEncumbranceJournal;
		private int lngEncumbranceID;
		private int lngVendor;

		private struct ContractAccount
		{
			public int lngID;
			public string Account;
			public double OriginalAmount;
			public double Paid;
			public bool Delete;
		};

		private int lngAccountIndex;
		private ContractAccount[] aryAccounts = null;

		public int AddContract(string strEmployee)
		{
			int AddContract = 0;
			int lngID;
			AddContract = 0;
			try
			{
				lngID = frmAddContract.InstancePtr.Init(strEmployee);
				if (lngID > 0)
				{
					lngContractID = lngID;
					LoadContract(lngID);
					AddContract = lngContractID;
					strEmployeeNumber = strEmployee;
				}
				return AddContract;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddContract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return AddContract;
			}
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(int, double)
		public bool LoadContract(int lngID, clsDRWrapper rsTemp = null, DateTime? tempdtAsOf = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtAsOf == null)
			{
				tempdtAsOf = DateTime.FromOADate(0);
			}
			DateTime dtAsOf = tempdtAsOf.Value;
			bool LoadContract = false;
			var rsLoad = new clsDRWrapper();
            clsDRWrapper oneToUse = null;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();

                LoadContract = false;
                if (lngID < 1)
                    return LoadContract;
                if (rsTemp == null)
                {
                    rsLoad.OpenRecordset("select * from contracts where ID = " + FCConvert.ToString(lngID),
                        "twpy0000.vb1");
                    oneToUse = rsLoad;
                }
                else
                {
                    oneToUse = rsTemp;
                    //rsLoad = rsTemp;
                }

                if (oneToUse.EndOfFile())
                {
                    return LoadContract;
                }

                lngContractID = lngID;
                strDescription = FCConvert.ToString(oneToUse.Get_Fields_String("description"));
                dblOriginalAmount = oneToUse.Get_Fields("originalamount");
                if (Information.IsDate(oneToUse.Get_Fields("startdate")))
                {
                    if (Convert.ToDateTime(rsLoad.Get_Fields("startdate")).ToOADate() != 0)
                    {
                        dtStartDate =
                            FCConvert.ToDateTime(Strings.Format(rsLoad.Get_Fields("startdate"), "MM/dd/yyyy"));
                    }
                    else
                    {
                        dtStartDate = DateTime.FromOADate(0);
                    }
                }
                else
                {
                    dtStartDate = DateTime.FromOADate(0);
                }

                if (Information.IsDate(oneToUse.Get_Fields("enddate")))
                {
                    if (Convert.ToDateTime(rsLoad.Get_Fields("enddate")).ToOADate() != 0)
                    {
                        dtEndDate = FCConvert.ToDateTime(Strings.Format(rsLoad.Get_Fields("enddate"), "MM/dd/yyyy"));
                    }
                    else
                    {
                        dtEndDate = DateTime.FromOADate(0);
                    }
                }
                else
                {
                    dtEndDate = DateTime.FromOADate(0);
                }

                lngEncumbranceID = oneToUse.Get_Fields_Int32("encumbranceid");
                lngEncumbranceJournal =
                    FCConvert.ToInt32(Math.Round(Conversion.Val(oneToUse.Get_Fields("encumbrancejournal"))));
                strEmployeeNumber = FCConvert.ToString(oneToUse.Get_Fields("employeenumber"));
                if (LoadAccounts(dtAsOf))
                {
                    LoadContract = true;
                }

                return LoadContract;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadContract", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsLoad?.Dispose();
                rsLoad = null;
                oneToUse = null;
            }
			return LoadContract;
		}

		private bool LoadAccounts(DateTime? tempdtAsOf = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtAsOf == null)
			{
				tempdtAsOf = DateTime.FromOADate(0);
			}
			DateTime dtAsOf = tempdtAsOf.Value;

			bool LoadAccounts = false;
			var rsLoad = new clsDRWrapper();
			var rsPay = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();

                int intCount;
                string strSQL;
                string strAsOf;
                LoadAccounts = false;
                dblPaid = 0;
                strAsOf = "";
                if (!(dtAsOf.ToOADate() == 0))
                {
                    strAsOf = " and paydate <= #" + Strings.Format(dtAsOf, "MM/dd/yyyy") + "# ";
                }

                FCUtils.EraseSafe(aryAccounts);
                aryAccounts = new ContractAccount[1];
                intCount = 0;
                lngAccountIndex = -1;
                strSQL =
                    "(select contractid,sum(distgrosspay) as totgross,distaccountnumber from tblcheckdetail where contractid = " +
                    FCConvert.ToString(lngContractID) + strAsOf +
                    " and checkvoid = 0 group by contractid,distaccountnumber) as tbl1";
                rsLoad.OpenRecordset(
                    "select * from contractaccounts left join " + strSQL +
                    " on (tbl1.contractid = contractaccounts.contractid) and (tbl1.distaccountnumber = contractaccounts.account) where contractaccounts.contractid = " +
                    FCConvert.ToString(lngContractID), "twpy0000.vb1");
                rsPay.OpenRecordset(
                    "select * from " + strSQL +
                    " left join contractaccounts on (tbl1.contractid = contractaccounts.contractid) and (tbl1.distaccountnumber = contractaccounts.account) where  tbl1.contractid = " +
                    FCConvert.ToString(lngContractID) + " and convert(int, isnull(contractaccounts.contractid, 0)) = 0",
                    "twpy0000.vb1");
                while (!rsPay.EndOfFile())
                {
                    if (intCount > 1)
                    {
                        Array.Resize(ref aryAccounts, intCount);
                    }

                    lngAccountIndex = 0;
                    aryAccounts[intCount].Account = FCConvert.ToString(rsPay.Get_Fields("distaccountnumber"));
                    aryAccounts[intCount].lngID = -2;
                    aryAccounts[intCount].OriginalAmount = 0;
                    aryAccounts[intCount].Paid = Conversion.Val(rsPay.Get_Fields("totgross"));
                    dblPaid += Conversion.Val(rsPay.Get_Fields("totgross"));
                    intCount += 1;
                    rsPay.MoveNext();
                }

                while (!rsLoad.EndOfFile())
                {
                    if (intCount > 1)
                    {
                        Array.Resize(ref aryAccounts, intCount);
                    }

                    lngAccountIndex = 0;
                    aryAccounts[intCount].Account = FCConvert.ToString(rsLoad.Get_Fields("account"));
                    aryAccounts[intCount].lngID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
                    aryAccounts[intCount].OriginalAmount = Conversion.Val(rsLoad.Get_Fields("originalamount"));
                    aryAccounts[intCount].Paid = Conversion.Val(rsLoad.Get_Fields("totgross"));
                    dblPaid += Conversion.Val(rsLoad.Get_Fields("totgross"));
                    intCount += 1;
                    rsLoad.MoveNext();
                }

                LoadAccounts = true;
                return LoadAccounts;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadAccounts", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsPay.Dispose();
				rsLoad.Dispose();
                rsLoad = null;
                rsPay = null;
            }
			return LoadAccounts;
		}

		public void MoveFirstAccount()
		{
			if (lngAccountIndex >= 0)
			{
				lngAccountIndex = 0;
			}
		}

		public void MoveLastAccount()
		{
			if (lngAccountIndex >= 0)
			{
				lngAccountIndex = Information.UBound(aryAccounts, 1);
			}
		}

		public void MoveNextAccount()
		{
			if (lngAccountIndex >= 0)
			{
				Again:
				;
				lngAccountIndex += 1;
				if (lngAccountIndex <= Information.UBound(aryAccounts, 1))
				{
					if (aryAccounts[lngAccountIndex].Delete)
					{
						goto Again;
					}
					if (aryAccounts[lngAccountIndex].lngID == 0 && aryAccounts[lngAccountIndex].Account == string.Empty)
					{
						goto Again;
					}
				}
			}
		}

		public bool EndOfAccounts()
		{
			bool EndOfAccounts = false;
			EndOfAccounts = false;
			if (lngAccountIndex < 0)
			{
				EndOfAccounts = true;
				return EndOfAccounts;
			}
			if (lngAccountIndex > Information.UBound(aryAccounts, 1))
			{
				EndOfAccounts = true;
			}
			return EndOfAccounts;
		}

		public void AddAccount(string strAccount, double dblOriginal)
		{
			if (lngAccountIndex < 0)
			{
				aryAccounts = new ContractAccount[1];
				lngAccountIndex = 0;
			}
			else
			{
				if (lngAccountIndex == 0)
				{
				}
				else
				{
					Array.Resize(ref aryAccounts, Information.UBound(aryAccounts, 1) + 1);
					lngAccountIndex = Information.UBound(aryAccounts, 1);
				}
			}
			aryAccounts[lngAccountIndex].lngID = 0;
			aryAccounts[lngAccountIndex].Account = strAccount;
			aryAccounts[lngAccountIndex].OriginalAmount = dblOriginal;
			aryAccounts[lngAccountIndex].Delete = false;
		}

		public string CurAccount
		{
			get
			{
				string CurAccount = "";
				if (lngAccountIndex >= 0)
				{
					CurAccount = aryAccounts[lngAccountIndex].Account;
				}
				else
				{
					CurAccount = "";
				}
				return CurAccount;
			}
		}

		public double CurAccountOriginalAmount
		{
			set
			{
				if (lngAccountIndex >= 0)
				{
					aryAccounts[lngAccountIndex].OriginalAmount = value;
				}
			}
			get
			{
				double CurAccountOriginalAmount = 0;
				if (lngAccountIndex >= 0)
				{
					CurAccountOriginalAmount = aryAccounts[lngAccountIndex].OriginalAmount;
				}
				else
				{
					CurAccountOriginalAmount = 0;
				}
				return CurAccountOriginalAmount;
			}
		}

		public double CurAccountPaid
		{
			get
			{
				double CurAccountPaid = 0;
				if (lngAccountIndex >= 0)
				{
					CurAccountPaid = aryAccounts[lngAccountIndex].Paid;
				}
				else
				{
					CurAccountPaid = 0;
				}
				return CurAccountPaid;
			}
		}

		public int CurAccountID
		{
			get
			{
				int CurAccountID = 0;
				if (lngAccountIndex >= 0)
				{
					CurAccountID = aryAccounts[lngAccountIndex].lngID;
				}
				else
				{
					CurAccountID = 0;
				}
				return CurAccountID;
			}
		}

		public double CurAccountEncumbrance
		{
			get
			{
				double CurAccountEncumbrance = 0;
				if (lngAccountIndex >= 0 && lngEncumbranceJournal > 0)
				{
					// If aryAccounts(lngAccountIndex).longid > 0 Then
                    using (clsDRWrapper rsLoad = new clsDRWrapper())
                    {
                        rsLoad.OpenRecordset(
                            "select * from ENCUMBRANCEDETAIL where recordnumber = " +
                            FCConvert.ToString(lngEncumbranceID) + " and account = '" +
                            aryAccounts[lngAccountIndex].Account + "'", "twbd0000.vb1");
                        if (!rsLoad.EndOfFile())
                        {
                            CurAccountEncumbrance = Conversion.Val(rsLoad.Get_Fields("amount")) +
                                                    Conversion.Val(rsLoad.Get_Fields("adjustments")) -
                                                    Conversion.Val(rsLoad.Get_Fields("liquidated"));
                        }
                        else
                        {
                            CurAccountEncumbrance = 0;
                        }
                    }
                }
				else
				{
					CurAccountEncumbrance = 0;
				}
				return CurAccountEncumbrance;
			}
		}

		private bool SaveAccounts()
		{
			bool SaveAccounts = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveAccounts = false;
                if (lngAccountIndex >= 0)
                {
                    // vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
                    int X;
                    using (clsDRWrapper rsSave = new clsDRWrapper())
                    {

                    

                        for (X = 0; X <= (Information.UBound(aryAccounts, 1)); X++)
                        {
                            if ((aryAccounts[X].lngID > 0 || aryAccounts[X].Account != string.Empty) &&
                                !aryAccounts[X].Delete)
                            {
                                rsSave.OpenRecordset(
                                    "select * from contractaccounts where id = " +
                                    FCConvert.ToString(aryAccounts[X].lngID) + " and contractid = " +
                                    FCConvert.ToString(lngContractID), "twpy0000.vb1");
                                if (!rsSave.EndOfFile())
                                {
                                    rsSave.Edit();
                                }
                                else
                                {
                                    rsSave.AddNew();
                                }

                                rsSave.Set_Fields("contractid", lngContractID);
                                rsSave.Set_Fields("originalamount", aryAccounts[X].OriginalAmount);
                                rsSave.Set_Fields("account", aryAccounts[X].Account);
                                rsSave.Set_Fields("paid", aryAccounts[X].Paid);
                                rsSave.Update();
                                aryAccounts[X].lngID = FCConvert.ToInt32(rsSave.Get_Fields("id"));
                            }
                            else if (aryAccounts[X].Delete)
                            {
                                rsSave.Execute(
                                    "delete from contractaccounts where id = " + FCConvert.ToString(aryAccounts[X].lngID),
                                    "twpy0000.vb1");
                            }
                        }
                    }
                }
				SaveAccounts = true;
				return SaveAccounts;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveAccounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveAccounts;
		}

		public bool SaveContract()
		{
			bool SaveContract = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
                using (clsDRWrapper rsSave = new clsDRWrapper())
                {
                    SaveContract = false;
                    rsSave.OpenRecordset("select * from contracts where ID = " + FCConvert.ToString(lngContractID),
                        "twpy0000.vb1");
                    if (!rsSave.EndOfFile())
                    {
                        rsSave.Edit();
                    }
                    else
                    {
                        rsSave.AddNew();
                    }

                    rsSave.Set_Fields("Description", strDescription);
                    rsSave.Set_Fields("OriginalAmount", dblOriginalAmount);
                    if (dtStartDate.ToOADate() != 0)
                    {
                        rsSave.Set_Fields("startdate", dtStartDate);
                    }
                    else
                    {
                        rsSave.Set_Fields("startdate", 0);
                    }

                    if (dtEndDate.ToOADate() != 0)
                    {
                        rsSave.Set_Fields("enddate", dtEndDate);
                    }
                    else
                    {
                        rsSave.Set_Fields("enddate", 0);
                    }

                    rsSave.Update();
                    lngContractID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
                    if (SaveAccounts())
                    {
                        SaveContract = true;
                    }
                }

                return SaveContract;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveContract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveContract;
		}

		public int ContractID
		{
			get
			{
				int ContractID = 0;
				ContractID = lngContractID;
				return ContractID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public DateTime StartDate
		{
			set
			{
				dtStartDate = value;
			}
			get
			{
				DateTime StartDate = System.DateTime.Now;
				StartDate = dtStartDate;
				return StartDate;
			}
		}

		public DateTime EndDate
		{
			set
			{
				dtEndDate = value;
			}
			get
			{
				DateTime EndDate = System.DateTime.Now;
				EndDate = dtEndDate;
				return EndDate;
			}
		}

		public double OriginalAmount
		{
			set
			{
				dblOriginalAmount = value;
			}
			get
			{
				double OriginalAmount = 0;
				OriginalAmount = dblOriginalAmount;
				return OriginalAmount;
			}
		}

		public double AmountPaid
		{
			set
			{
				dblPaid = value;
			}
			get
			{
				double AmountPaid = 0;
				AmountPaid = dblPaid;
				return AmountPaid;
			}
		}

		public int EncumbranceJournal
		{
			set
			{
				lngEncumbranceJournal = value;
			}
			get
			{
				int EncumbranceJournal = 0;
				EncumbranceJournal = lngEncumbranceJournal;
				return EncumbranceJournal;
			}
		}

		public int EncumbranceID
		{
			set
			{
				lngEncumbranceID = value;
			}
			get
			{
				int EncumbranceID = 0;
				EncumbranceID = lngEncumbranceID;
				return EncumbranceID;
			}
		}

		public int Vendor
		{
			set
			{
				lngVendor = value;
			}
			get
			{
				int Vendor = 0;
				Vendor = lngVendor;
				return Vendor;
			}
		}

		public string EmployeeNumber
		{
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public void DeleteCurAccount()
		{
			if (lngAccountIndex >= 0 && Information.UBound(aryAccounts, 1) >= lngAccountIndex)
			{
				aryAccounts[lngAccountIndex].Delete = true;
			}
		}
	}
}
