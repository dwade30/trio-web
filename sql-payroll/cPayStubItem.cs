﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPayStubItem
	{
		//=========================================================
		private double dblAmount;
		private string strDescription = string.Empty;
		private int lngTypeID;
		private int lngID;
		private double dblTotalAmount;
		private double dblHours;
		private bool boolDollars;
		private double dblRate;

		public double PayRate
		{
			set
			{
				dblRate = value;
			}
			get
			{
				double PayRate = 0;
				PayRate = dblRate;
				return PayRate;
			}
		}

		public bool IsDollars
		{
			set
			{
				boolDollars = value;
			}
			get
			{
				bool IsDollars = false;
				IsDollars = boolDollars;
				return IsDollars;
			}
		}

		public double Hours
		{
			set
			{
				dblHours = value;
			}
			get
			{
				double Hours = 0;
				Hours = dblHours;
				return Hours;
			}
		}

		public double Amount
		{
			set
			{
				dblAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblAmount;
				return Amount;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int TypeID
		{
			set
			{
				lngTypeID = value;
			}
			get
			{
				int TypeID = 0;
				TypeID = lngTypeID;
				return TypeID;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public double total
		{
			set
			{
				dblTotalAmount = value;
			}
			get
			{
				double total = 0;
				total = dblTotalAmount;
				return total;
			}
		}
	}
}
