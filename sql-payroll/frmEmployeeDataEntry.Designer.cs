//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeDataEntry.
	/// </summary>
	partial class frmEmployeeDataEntry
	{
		public FCGrid vsDataEntry;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsDataEntry = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 493);
            this.BottomPanel.Size = new System.Drawing.Size(907, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsDataEntry);
            this.ClientArea.Controls.Add(this.cmdNew);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Size = new System.Drawing.Size(907, 433);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(907, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(237, 30);
            this.HeaderText.Text = "Data Entry Selection";
            // 
            // vsDataEntry
            // 
            this.vsDataEntry.AllowSelection = false;
            this.vsDataEntry.AllowUserToResizeColumns = false;
            this.vsDataEntry.AllowUserToResizeRows = false;
            this.vsDataEntry.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDataEntry.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsDataEntry.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsDataEntry.BackColorBkg = System.Drawing.Color.Empty;
            this.vsDataEntry.BackColorFixed = System.Drawing.Color.Empty;
            this.vsDataEntry.BackColorSel = System.Drawing.Color.Empty;
            this.vsDataEntry.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsDataEntry.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsDataEntry.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsDataEntry.ColumnHeadersHeight = 30;
            this.vsDataEntry.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsDataEntry.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsDataEntry.DragIcon = null;
            this.vsDataEntry.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsDataEntry.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsDataEntry.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsDataEntry.FrozenCols = 0;
            this.vsDataEntry.GridColor = System.Drawing.Color.Empty;
            this.vsDataEntry.GridColorFixed = System.Drawing.Color.Empty;
            this.vsDataEntry.Location = new System.Drawing.Point(30, 30);
            this.vsDataEntry.Name = "vsDataEntry";
            this.vsDataEntry.OutlineCol = 0;
            this.vsDataEntry.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsDataEntry.RowHeightMin = 0;
            this.vsDataEntry.Rows = 50;
            this.vsDataEntry.ScrollTipText = null;
            this.vsDataEntry.ShowColumnVisibilityMenu = false;
            this.vsDataEntry.Size = new System.Drawing.Size(849, 390);
            this.vsDataEntry.StandardTab = true;
            this.vsDataEntry.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsDataEntry.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsDataEntry.TabIndex = 0;
            this.vsDataEntry.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsDataEntry_KeyDownEdit);
            this.vsDataEntry.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDataEntry_AfterEdit);
            this.vsDataEntry.CurrentCellChanged += new System.EventHandler(this.vsDataEntry_RowColChange);
            this.vsDataEntry.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDataEntry_MouseMoveEvent);
            this.vsDataEntry.Click += new System.EventHandler(this.vsDataEntry_ClickEvent);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Enabled = false;
            this.cmdPrint.Location = new System.Drawing.Point(723, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(48, 24);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(402, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.AppearanceKey = "actionButton";
            this.cmdDelete.Location = new System.Drawing.Point(609, 41);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 40);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete";
            // 
            // cmdNew
            // 
            this.cmdNew.AppearanceKey = "actionButton";
            this.cmdNew.Location = new System.Drawing.Point(367, 52);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(78, 40);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.AppearanceKey = "actionButton";
            this.cmdRefresh.Location = new System.Drawing.Point(686, 49);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(93, 40);
            this.cmdRefresh.TabIndex = 4;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = 1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                           ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit          ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 5;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.AppearanceKey = "toolbarButton";
            this.cmdPrintPreview.Enabled = false;
            this.cmdPrintPreview.Location = new System.Drawing.Point(777, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(102, 24);
            this.cmdPrintPreview.TabIndex = 6;
            this.cmdPrintPreview.Text = "Print/Preview";
            // 
            // frmEmployeeDataEntry
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(907, 601);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeeDataEntry";
            this.Text = "Data Entry Selection";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployeeDataEntry_Load);
            this.Activated += new System.EventHandler(this.frmEmployeeDataEntry_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeeDataEntry_KeyPress);
            this.Resize += new System.EventHandler(this.frmEmployeeDataEntry_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
        private FCButton cmdPrintPreview;
    }
}