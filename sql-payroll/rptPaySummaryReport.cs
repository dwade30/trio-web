//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPaySummaryReport.
	/// </summary>
	public partial class rptPaySummaryReport : BaseSectionReport
	{
		public rptPaySummaryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Pay Tax Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
			vsData = new FCGrid();
			vsData.Rows = 1;
			vsData.Cols = 9;
			vsHeader = new FCGrid();
			vsHeader.Rows = 2;
			vsHeader.Cols = 9;
		}

		public static rptPaySummaryReport InstancePtr
		{
			get
			{
				return (rptPaySummaryReport)Sys.GetInstance(typeof(rptPaySummaryReport));
			}
		}

		protected rptPaySummaryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				rsDeds?.Dispose();
				rsOtherPay?.Dispose();
				rsOvPay?.Dispose();
				rsRegPay?.Dispose();
                rsDeds = null;
                rsOtherPay = null;
                rsOvPay = null;
                rsRegPay = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaySummaryReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rs = new clsDRWrapper();
		int intRow;
		int intCol;
		int intPageNumber;
		bool boolDoubleHeader;
		string strEmployeeNumber = "";
		double dblTotal;
		int intCounter;
		double dblRegularTotal;
		double dblOverTimeTotal;
		double dblOtherTotal;
		double dblTotalPayTotal;
		double dblTaxesTotal;
		double dblDeductsTotal;
		double dblNetTotal;
		double dblGroupRegularTotal;
		double dblGroupOverTimeTotal;
		double dblGroupOtherTotal;
		double dblGroupTotalPayTotal;
		double dblGroupTaxesTotal;
		double dblGroupDeductsTotal;
		double dblGroupNetTotal;
		double dblGross;
		double dblNet;
		double dblReg;
		double dblOT;
		double dblOther;
		double dblTax;
		double dblDed;
		bool boolShowLast;
		bool boolGroup;
		int intEmployeeTotal;
		int intSetTotals;
		clsDRWrapper rsRegPay = new clsDRWrapper();
		clsDRWrapper rsOvPay = new clsDRWrapper();
		clsDRWrapper rsOtherPay = new clsDRWrapper();
		//clsDRWrapper rsTotalPay = new clsDRWrapper();
		clsDRWrapper rsDeds = new clsDRWrapper();
		string strOrderBy = "";
		private string strWhereClause = string.Empty;
		FCGrid vsData;
		FCGrid vsHeader;
		public void Init(int intMQYF, DateTime dtToDate, int intPayrun = 0, bool fromModCoreys = false, List<string> batchReports = null)
		{
			// any date will work, but should be a paydate since the titles will display this date
			string strWhere;
			int intMonth;
			int intQuarter = 0;
			
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(DateTime, string)
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			DateTime dtTempDate = DateTime.FromOADate(0);
			string strSQL;
			int lngRegular;
			int lngOvertime;
			string strDedQuery;
			string strTotQuery;
			string strRegQuery;
			string strOTQuery;
			string strOtherQuery;
			string strCombQuery = "";
			string strPayRunWhere;
			dtEndDate = dtToDate;
			strPayRunWhere = "";
			this.Fields.Add("grpHeader");
			switch (intMQYF)
			{
				case 0:
					{
						dtStartDate = dtToDate;
						if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
						{
							strPayRunWhere = " and payrunid = " + FCConvert.ToString(intPayrun) + " ";
							txtTitle2.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						}
						else
						{
							txtTitle2.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
						}
						break;
					}
				case 1:
					{
						// monthly
						dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(dtEndDate.Month) + "/1/" + FCConvert.ToString(dtEndDate.Year));
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "MTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
				case 2:
					{
						// quarterly
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "QTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						if (dtEndDate.Month >= 1 && dtEndDate.Month <= 3)
						{
							intQuarter = 1;
						}
						else if (dtEndDate.Month >= 4 && dtEndDate.Month <= 6)
						{
							intQuarter = 2;
						}
						else if (dtEndDate.Month >= 7 && dtEndDate.Month <= 9)
						{
							intQuarter = 3;
						}
						else if (dtEndDate.Month >= 10 && dtEndDate.Month <= 12)
						{
							intQuarter = 4;
						}
						modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarter, dtToDate.Year);
						break;
					}
				case 3:
					{
						// Calendar Year
						dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtEndDate.Year));
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtEndDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "Calendar YTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
				case 4:
					{
						// Fiscal Year
						dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtToDate);
						dtTempDate = modCoreysSweeterCode.GetHighestPayDate(dtStartDate, dtToDate);
						if (dtTempDate.ToOADate() != 0)
							dtEndDate = dtTempDate;
						txtTitle2.Text = "Fiscal YTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
						break;
					}
			}
			//end switch
			strWhere = " where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' " + strPayRunWhere;
			// regular is 1 and overtime is 2. ID's are most likely the same but just to make sure
			rsData.OpenRecordset("select * from tblpaycategories order by categorynumber", "twpy0000.vb1");
			lngRegular = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("ID"))));
			rsData.MoveNext();
			lngOvertime = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("ID"))));
			rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 0)
			{
				// employee name
				strOrderBy = " order by lastname,firstname ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 1)
			{
				// employee number
				strOrderBy = " order by tblemployeemaster.employeenumber ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 2)
			{
				// sequence
				strOrderBy = " order by seqnumber,lastname,firstname ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 3)
			{
				// dept/div
				strOrderBy = " order by DEPTDIV,lastname,firstname ";
			}
			strWhereClause = strWhere;
			// GET REGULAR FIRST
			strSQL = "select employeenumber,sum(distgrosspay) as RegularPay from tblcheckdetail " + strWhere + " and distpaycategory = " + FCConvert.ToString(lngRegular) + " and checkvoid = 0 group by employeenumber";
			strRegQuery = "(" + strSQL + ") as PaySummaryRegQuery ";
			strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strRegQuery + " on (paysummaryregquery.employeenumber = tblemployeemaster.employeenumber) " + strOrderBy;
			rsRegPay.OpenRecordset(strSQL, "twpy0000.vb1");
			// now overtime
			// 
			strSQL = "select employeenumber,sum(distgrosspay) as OTPay from tblcheckdetail " + strWhere + " and distpaycategory = " + FCConvert.ToString(lngOvertime) + " and checkvoid = 0 group by employeenumber";
			strOTQuery = "(" + strSQL + ") as PaySummaryOTQuery ";
			strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strOTQuery + " on (paysummaryotquery.employeenumber = tblemployeemaster.employeenumber) " + strOrderBy;
			rsOvPay.OpenRecordset(strSQL, "twpy0000.vb1");
			// now other
			strSQL = "select employeenumber,sum(distgrosspay) as OtherPay from tblcheckdetail " + strWhere + " and distpaycategory <> " + FCConvert.ToString(lngOvertime) + " and distpaycategory <> " + FCConvert.ToString(lngRegular) + " and checkvoid = 0 group by employeenumber";
			strOtherQuery = "(" + strSQL + ") as PaySummaryOtherQuery ";
			strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strOtherQuery + " on (paysummaryotherquery.employeenumber = tblemployeemaster.employeenumber) " + strOrderBy;
			rsOtherPay.OpenRecordset(strSQL, "twpy0000.vb1");
			// deductions
			strSQL = "select employeenumber,sum(dedAmount) as Deductions from tblcheckdetail " + strWhere + " and deductionrecord = 1 and checkvoid = 0 group by employeenumber";
			strDedQuery = "(" + strSQL + ") as PaySummaryDedQuery ";
			strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strDedQuery + " on (paysummarydedquery.employeenumber = tblemployeemaster.employeenumber) " + strOrderBy;
			rsDeds.OpenRecordset(strSQL, "twpy0000.vb1");
			// total records
			strSQL = "select employeenumber,sum(GrossPay) as TotGross,sum(netpay) as TotNet,sum(FicaTaxWH) + sum(statetaxwh) + sum(medicaretaxwh) + sum(federaltaxwh) as TotTaxes from tblcheckdetail " + strWhere + " and (TotalRecord = 1 or AdjustRecord = 1) and checkvoid = 0 group by employeenumber";
			strTotQuery = "(" + strSQL + ") as PaySummaryTotQuery ";
			strSQL = "select tblemployeemaster.employeenumber as tblemployeemasteremployeenumber, * from tblemployeemaster inner join " + strTotQuery + " on (paysummarytotquery.employeenumber = tblemployeemaster.employeenumber) " + strOrderBy;
			rsData.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				if (strOrderBy == " order by lastname,firstname ")
				{
					// employee name
					txtGroup.Text = rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
					GroupHeader1.Visible = false;
					GroupFooter1.Visible = false;
				}
				else if (strOrderBy == " order by tblemployeemaster.employeenumber ")
				{
					// employee number
					txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber"));
					GroupHeader1.Visible = false;
					GroupFooter1.Visible = false;
				}
				else if (strOrderBy == " order by seqnumber,lastname,firstname ")
				{
					// sequence
					txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("SeqNumber"));
				}
				else if (strOrderBy == " order by DEPTDIV,lastname,firstname ")
				{
					// dept/div
					txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
				}
				this.Fields["grpHeader"].Value = txtGroup.Text;
			}
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				frmReportViewer.InstancePtr.Init(this, "", 1, boolAllowEmail: true, strAttachmentName: "PaySummary", showModal:true);
			}
			else
			{
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// Me.PrintReport (False)
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			NextRecord:
			;
			Line2.Visible = false;
			if (rsData.EndOfFile())
			{
				Field1.Text = "";
				Field2.Text = "";
				Field3.Text = "";
				Field4.Text = "";
				Field5.Text = "";
				Field6.Text = "";
				Field7.Text = "";
				Field8.Text = "";
				Field9.Text = "";
				// If boolGroup = False Then
				// GroupHeader1.DataField = 0
				// boolGroup = True
				// eof = False
				// Exit Sub
				// End If
				boolShowLast = true;
				vsData.TextMatrix(0, 0, string.Empty);
				vsData.TextMatrix(0, 1, string.Empty);
				vsData.TextMatrix(0, 2, string.Empty);
				vsData.TextMatrix(0, 3, string.Empty);
				vsData.TextMatrix(0, 4, string.Empty);
				vsData.TextMatrix(0, 5, string.Empty);
				vsData.TextMatrix(0, 6, string.Empty);
				vsData.TextMatrix(0, 7, string.Empty);
				vsData.TextMatrix(0, 8, string.Empty);
				vsData.TextMatrix(0, 1, "Employees: " + FCConvert.ToString(intEmployeeTotal));
				vsData.TextMatrix(0, 2, Strings.Format(dblRegularTotal, "#,###,##0.00"));
				vsData.TextMatrix(0, 3, Strings.Format(dblOverTimeTotal, "#,###,##0.00"));
				vsData.TextMatrix(0, 4, Strings.Format(dblOtherTotal, "#,###,##0.00"));
				vsData.TextMatrix(0, 5, Strings.Format(dblTotalPayTotal, "#,###,##0.00"));
				vsData.TextMatrix(0, 6, Strings.Format(dblTaxesTotal, "#,###,##0.00"));
				vsData.TextMatrix(0, 7, Strings.Format(dblDeductsTotal, "#,###,##0.00"));
				vsData.TextMatrix(0, 8, Strings.Format(dblNetTotal, "#,###,##0.00"));
				eArgs.EOF = true;
				return;
			}
			// ADD THE DATA TO THE CORRECT CELL IN THE GRID
			strEmployeeNumber = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber")));
			vsData.TextMatrix(0, 0, fecherFoundation.Strings.Trim(strEmployeeNumber));
			vsData.TextMatrix(0, 1, fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LastName"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("middlename"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Desig"))));
			intEmployeeTotal += 1;
			if (!rsRegPay.EndOfFile())
			{
				if (rsRegPay.Get_Fields("tblemployeemasteremployeenumber") == rsData.Get_Fields("tblemployeemasteremployeenumber"))
				{
					dblReg = Conversion.Val(rsRegPay.Get_Fields("regularpay"));
				}
				else
				{
					rsRegPay.FindFirstRecord("tblemployeemasteremployeenumber", rsData.Get_Fields("tblemployeemasteremployeenumber"));
					if (!rsRegPay.NoMatch)
					{
						dblReg = Conversion.Val(rsRegPay.Get_Fields("RegularPay"));
					}
					else
					{
						dblReg = 0;
					}
				}
			}
			else
			{
				dblReg = 0;
			}
			vsData.TextMatrix(0, 2, Strings.Format(dblReg, "#,###,##0.00"));
			dblRegularTotal += dblReg;
			dblGroupRegularTotal += dblReg;
			if (!rsOvPay.EndOfFile())
			{
				if (rsOvPay.Get_Fields("tblemployeemasteremployeenumber") == rsData.Get_Fields("tblemployeemasteremployeenumber"))
				{
					dblOT = Conversion.Val(rsOvPay.Get_Fields("otpay"));
				}
				else
				{
					rsOvPay.FindFirstRecord("tblemployeemasteremployeenumber", rsData.Get_Fields("tblemployeemasteremployeenumber"));
					if (!rsOvPay.NoMatch)
					{
						dblOT = Conversion.Val(rsOvPay.Get_Fields("otpay"));
					}
					else
					{
						dblOT = 0;
					}
				}
			}
			else
			{
				dblOT = 0;
			}
			vsData.TextMatrix(0, 3, Strings.Format(dblOT, "#,###,##0.00"));
			dblOverTimeTotal += dblOT;
			dblGroupOverTimeTotal += dblOT;
			dblGross = Conversion.Val(rsData.Get_Fields("TotGross"));
			vsData.TextMatrix(0, 5, Strings.Format(dblGross, "#,###,##0.00"));
			dblTotalPayTotal += dblGross;
			dblGroupTotalPayTotal += dblGross;
			if (!rsOtherPay.EndOfFile())
			{
				if (rsOtherPay.Get_Fields("tblemployeemasteremployeenumber") == rsData.Get_Fields("tblemployeemasteremployeenumber"))
				{
					dblOther = Conversion.Val(rsOtherPay.Get_Fields("otherpay"));
				}
				else
				{
					rsOtherPay.FindFirstRecord("tblemployeemasteremployeenumber", rsData.Get_Fields("tblemployeemasteremployeenumber"));
					if (!rsOtherPay.NoMatch)
					{
						dblOther = Conversion.Val(rsOtherPay.Get_Fields("otherpay"));
					}
					else
					{
						dblOther = 0;
					}
				}
			}
			else
			{
				dblOther = 0;
			}
			vsData.TextMatrix(0, 4, Strings.Format(dblOther, "#,###,##0.00"));
			dblOtherTotal += dblOther;
			dblGroupOtherTotal += dblOther;
			dblTotal = 0;
			// GET THE EMPLOYEE DEDUCTIONS
			if (!rsDeds.EndOfFile())
			{
				if (rsDeds.Get_Fields("tblemployeemasteremployeenumber") == rsData.Get_Fields("tblemployeemasteremployeenumber"))
				{
					dblDed = Conversion.Val(rsDeds.Get_Fields("deductions"));
				}
				else
				{
					rsDeds.FindNextRecord("tblemployeemasteremployeenumber", rsData.Get_Fields("tblemployeemasteremployeenumber"));
					if (!rsDeds.NoMatch)
					{
						dblDed = Conversion.Val(rsDeds.Get_Fields("deductions"));
					}
					else
					{
						dblDed = 0;
					}
				}
			}
			else
			{
				dblDed = 0;
			}
			vsData.TextMatrix(0, 7, Strings.Format(dblDed, "#,###,##0.00"));
			dblDeductsTotal += dblDed;
			dblGroupDeductsTotal += dblDed;
			
			dblNet = Conversion.Val(rsData.Get_Fields("TotNet"));
			vsData.TextMatrix(0, 8, Strings.Format(dblNet, "#,###,##0.00"));
			dblNetTotal += dblNet;
			dblGroupNetTotal += dblNet;
			dblTax = Conversion.Val(rsData.Get_Fields("tottaxes"));
			vsData.TextMatrix(0, 6, Strings.Format(dblTax, "#,###,##0.00"));
			dblTaxesTotal += dblTax;
			dblGroupTaxesTotal += dblTax;
			Field1.Text = vsData.TextMatrix(0, 0);
			Field2.Text = vsData.TextMatrix(0, 1);
			Field3.Text = vsData.TextMatrix(0, 2);
			Field4.Text = vsData.TextMatrix(0, 3);
			Field5.Text = vsData.TextMatrix(0, 4);
			Field6.Text = vsData.TextMatrix(0, 5);
			Field7.Text = vsData.TextMatrix(0, 6);
			Field8.Text = vsData.TextMatrix(0, 7);
			Field9.Text = vsData.TextMatrix(0, 8);
			if (strOrderBy == " order by lastname,firstname ")
			{
				// employee name
				txtGroup.Text = rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
			}
			else if (strOrderBy == " order by tblemployeemaster.employeenumber ")
			{
				// employee number
				txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("tblemployeemasteremployeenumber"));
			}
			else if (strOrderBy == " order by seqnumber,lastname,firstname ")
			{
				// sequence
				txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("SeqNumber"));
			}
			else if (strOrderBy == " order by DEPTDIV,lastname,firstname ")
			{
				// dept/div
				txtGroup.Text = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
			}
			this.Fields["grpHeader"].Value = txtGroup.Text;
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intSetTotals = 0;
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			intRow = 0;
			boolDoubleHeader = false;
			vsHeader.TextMatrix(0, 0, "Employee");
			vsHeader.TextMatrix(0, 1, "Employee");
			vsHeader.TextMatrix(0, 2, "- - - -");
			vsHeader.TextMatrix(0, 3, "Pay");
			vsHeader.TextMatrix(0, 4, "- - - -");
			vsHeader.TextMatrix(0, 5, "Total");
			vsHeader.TextMatrix(1, 0, "");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 1, "");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 2, "Regular");
			vsHeader.ColWidth(2, 1000);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 3, "Overtime");
			vsHeader.ColWidth(3, 1000);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 4, "Other");
			vsHeader.ColWidth(4, 1000);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 5, "Gross");
			vsHeader.ColWidth(5, 1000);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 6, "Taxes");
			vsHeader.ColWidth(6, 1000);
			vsHeader.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 7, "Deducts");
			vsHeader.ColWidth(7, 1000);
			vsHeader.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 8, "Net");
			vsHeader.ColWidth(8, 1100);
			vsHeader.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
			// txtHead1.Width = 805
			// txtHead1.Text = .TextMatrix(0, 0)
			// txtHead1.Left = 0
			// 
			// txtHead2.Width = .ColWidth(1)
			// 
			// txtHead2.Left = txtHead1.Left + txtHead1.Width
			// txtHead2.Text = .TextMatrix(1, 1)
			// txtHead3.Width = .ColWidth(2)
			// txtHead3.Left = txtHead2.Left + txtHead2.Width
			// txtHead3.Text = .TextMatrix(1, 2)
			// txtHead4.Width = .ColWidth(3)
			// txtHead4.Left = txtHead3.Left + txtHead3.Width
			// txtHead4.Text = .TextMatrix(1, 3)
			// txtHead5.Width = .ColWidth(4)
			// txtHead5.Left = txtHead4.Left + txtHead4.Width
			// txtHead5.Text = .TextMatrix(1, 4)
			// txtHead6.Width = .ColWidth(5)
			// txtHead6.Left = txtHead5.Left + txtHead5.Width
			// txtHead6.Text = .TextMatrix(1, 5)
			// txtHead7.Width = .ColWidth(6)
			// txtHead7.Left = txtHead6.Left + txtHead6.Width
			// txtHead7.Text = .TextMatrix(1, 6)
			// txtHead8.Width = .ColWidth(7)
			// txtHead8.Left = txtHead7.Left + txtHead7.Width
			// txtHead8.Text = .TextMatrix(1, 7)
			// txtHead9.Width = .ColWidth(8)
			// txtHead9.Left = txtHead8.Left + txtHead8.Width
			// txtHead9.Text = .TextMatrix(1, 8)
			vsData.ColWidth(0, vsHeader.ColWidth(0));
			vsData.ColWidth(1, vsHeader.ColWidth(1));
			vsData.ColWidth(2, vsHeader.ColWidth(2));
			vsData.ColWidth(3, vsHeader.ColWidth(3));
			vsData.ColWidth(4, vsHeader.ColWidth(4));
			vsData.ColWidth(5, vsHeader.ColWidth(5));
			vsData.ColWidth(6, vsHeader.ColWidth(6));
			vsData.ColWidth(7, vsHeader.ColWidth(7));
			vsData.ColWidth(8, vsHeader.ColWidth(8));
			vsData.ColAlignment(0, vsHeader.ColAlignment(0));
			vsData.ColAlignment(1, vsHeader.ColAlignment(1));
			vsData.ColAlignment(2, vsHeader.ColAlignment(2));
			vsData.ColAlignment(3, vsHeader.ColAlignment(3));
			vsData.ColAlignment(4, vsHeader.ColAlignment(4));
			vsData.ColAlignment(5, vsHeader.ColAlignment(5));
			vsData.ColAlignment(6, vsHeader.ColAlignment(6));
			vsData.ColAlignment(7, vsHeader.ColAlignment(7));
			vsData.ColAlignment(8, vsHeader.ColAlignment(8));
			// Field1.Width = 805
			// Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left
			// Field1.Left = vsData.Left
			// Field2.Width = .ColWidth(1)
			// Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left
			// Field2.Left = Field1.Left + Field1.Width
			// Field3.Width = .ColWidth(2)
			// Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field3.Left = Field2.Left + Field2.Width
			// Field4.Width = .ColWidth(3)
			// Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field4.Left = Field3.Left + Field3.Width
			// Field5.Width = .ColWidth(4)
			// Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field5.Left = Field4.Left + Field4.Width
			// Field6.Width = .ColWidth(5)
			// Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field6.Left = Field5.Left + Field5.Width
			// Field7.Width = .ColWidth(6)
			// Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field7.Left = Field6.Left + Field6.Width
			// Field8.Width = .ColWidth(7)
			// Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field8.Left = Field7.Left + Field7.Width
			// 
			// 
			// Field12.Width = .ColWidth(2)
			// Field12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field12.Left = Field2.Left + Field2.Width
			// Field13.Width = .ColWidth(3)
			// Field13.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field13.Left = Field3.Left + Field3.Width
			// Field14.Width = .ColWidth(4)
			// Field14.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field14.Left = Field4.Left + Field4.Width
			// Field15.Width = .ColWidth(5)
			// Field15.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field15.Left = Field5.Left + Field5.Width
			// Field16.Width = .ColWidth(6)
			// Field16.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field16.Left = Field6.Left + Field6.Width
			// Field17.Width = .ColWidth(7)
			// Field17.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field17.Left = Field7.Left + Field7.Width
			// Field19.Width = .ColWidth(2)
			// Field19.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field19.Left = Field2.Left + Field2.Width
			// Field20.Width = .ColWidth(3)
			// Field20.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field20.Left = Field3.Left + Field3.Width
			// Field21.Width = .ColWidth(4)
			// Field21.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field21.Left = Field4.Left + Field4.Width
			// Field22.Width = .ColWidth(5)
			// Field22.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field22.Left = Field5.Left + Field5.Width
			// Field23.Width = .ColWidth(6)
			// Field23.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field23.Left = Field6.Left + Field6.Width
			// Field24.Width = .ColWidth(7)
			// Field24.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field24.Left = Field7.Left + Field7.Width
			// Field25.Width = .ColWidth(8)
			// Field25.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
			// Field25.Left = Field8.Left + Field8.Width
			// Field27.Width = 805
			// Field27.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left
			// Field27.Left = vsData.Left
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			PageHeader.Height += 100 / 1440F;
			Detail.Height = vsData.Height / 1440F + 30 / 1440F;
			intRow = 0;
			modPrintToFile.SetPrintProperties(this);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modGlobalRoutines.UpdatePayrollStepTable("PaySummary");
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// If rsData.EndOfFile And boolShowLast Then
			// Field12 = Format(dblReg, "0.00")
			// Field14 = Format(dblOther, "0.00")
			// Field13 = Format(dblOT, "0.00")
			// Field15 = Format(dblGross, "0.00")
			// Field16 = Format(dblTax, "0.00")
			// Field18 = Format(dblNet, "0.00")
			// Field17 = Format(dblDed, "0.00")
			if (rsData.EndOfFile() && boolShowLast)
			{
				Field12.Text = Strings.Format(dblGroupRegularTotal, "#,###,##0.00");
				Field14.Text = Strings.Format(dblGroupOtherTotal, "#,###,##0.00");
				Field13.Text = Strings.Format(dblGroupOverTimeTotal, "#,###,##0.00");
				Field15.Text = Strings.Format(dblGroupTotalPayTotal, "#,###,##0.00");
				Field16.Text = Strings.Format(dblGroupTaxesTotal, "#,###,##0.00");
				Field18.Text = Strings.Format(dblGroupNetTotal, "#,###,##0.00");
				Field17.Text = Strings.Format(dblGroupDeductsTotal, "#,###,##0.00");
			}
			else
			{
				Field12.Text = Strings.Format((dblGroupRegularTotal - dblReg), "#,###,##0.00");
				Field14.Text = Strings.Format(dblGroupOtherTotal - dblOther, "#,###,##0.00");
				Field13.Text = Strings.Format(dblGroupOverTimeTotal - dblOT, "#,###,##0.00");
				Field15.Text = Strings.Format(dblGroupTotalPayTotal - dblGross, "#,###,##0.00");
				Field16.Text = Strings.Format(dblGroupTaxesTotal - dblTax, "#,###,##0.00");
				Field18.Text = Strings.Format(dblGroupNetTotal - dblNet, "#,###,##0.00");
				Field17.Text = Strings.Format(dblGroupDeductsTotal - dblDed, "#,###,##0.00");
				dblGroupRegularTotal = dblReg;
				dblGroupRegularTotal = dblReg;
				dblGroupOtherTotal = dblOther;
				dblGroupOverTimeTotal = dblOT;
				dblGroupTotalPayTotal = dblGross;
				dblGroupTaxesTotal = dblTax;
				dblGroupNetTotal = dblNet;
				dblGroupDeductsTotal = dblDed;
			}
		}

		private void PageHeader_BeforePrint(object sender, EventArgs e)
		{
			vsHeader.TextMatrix(0, 0, "Employee");
			vsHeader.TextMatrix(0, 1, "Employee");
			vsHeader.TextMatrix(0, 2, "- - - -");
			vsHeader.TextMatrix(0, 3, "Pay");
			vsHeader.TextMatrix(0, 4, "- - - -");
			vsHeader.TextMatrix(0, 5, "Total");
			vsHeader.TextMatrix(1, 0, "ID");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 1, "Name");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 2, "Regular");
			vsHeader.ColWidth(2, 900);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 3, "Overtime");
			vsHeader.ColWidth(3, 900);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 4, "Other");
			vsHeader.ColWidth(4, 900);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 5, "Pay");
			vsHeader.ColWidth(5, 900);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 6, "Taxes");
			vsHeader.ColWidth(6, 900);
			vsHeader.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 7, "Deducts");
			vsHeader.ColWidth(7, 900);
			vsHeader.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 8, "NET");
			vsHeader.ColWidth(8, 900);
			vsHeader.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			Field19.Text = vsData.TextMatrix(0, 2);
			Field20.Text = vsData.TextMatrix(0, 3);
			Field21.Text = vsData.TextMatrix(0, 4);
			Field22.Text = vsData.TextMatrix(0, 5);
			Field23.Text = vsData.TextMatrix(0, 6);
			Field24.Text = vsData.TextMatrix(0, 7);
			Field25.Text = vsData.TextMatrix(0, 8);
			Field27.Text = "Totals";
			Field28.Text = vsData.TextMatrix(0, 1);
		}
	}
}
