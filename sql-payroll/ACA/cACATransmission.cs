﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class cACATransmission
	{
		//=========================================================
		// local variable(s) to hold property value(s)
		private int mvarID;
		// local copy
		private int mvarTaxYear;
		// local copy
		private string mvarTransmissionType = string.Empty;
		// local copy
		private string mvarReceiptID = string.Empty;
		// local copy
		private string mvarFileName = string.Empty;
		// local copy
		private string mvarTransmissionID = string.Empty;
		// local copy
		private string mvarTestFileCode = string.Empty;
		// local copy
		private bool mvarIsUpdated;
		// local copy
		// local variable(s) to hold property value(s)
		private bool mvarIsDeleted;
		// local copy
		private cGenericCollection mdetails = new cGenericCollection();

		public cGenericCollection Details
		{
			get
			{
				cGenericCollection Details = null;
				Details = mdetails;
				return Details;
			}
		}

		public bool IsDeleted
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.IsDeleted = 5
				mvarIsDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.IsDeleted
				IsDeleted = mvarIsDeleted;
				return IsDeleted;
			}
		}

		public bool IsUpdated
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.IsUpdated = 5
				mvarIsUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.IsUpdated
				IsUpdated = mvarIsUpdated;
				return IsUpdated;
			}
		}

		public string TestFileCode
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.TestFileCode = 5
				mvarTestFileCode = value;
				IsUpdated = true;
			}
			get
			{
				string TestFileCode = "";
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.TestFileCode
				TestFileCode = mvarTestFileCode;
				return TestFileCode;
			}
		}

		public string TransmissionID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.TransmissionID = 5
				mvarTransmissionID = value;
				IsUpdated = true;
			}
			get
			{
				string TransmissionID = "";
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.TransmissionID
				TransmissionID = mvarTransmissionID;
				return TransmissionID;
			}
		}

		public string FileName
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.FileName = 5
				mvarFileName = value;
				IsUpdated = true;
			}
			get
			{
				string FileName = "";
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.FileName
				FileName = mvarFileName;
				return FileName;
			}
		}

		public string ReceiptID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.ReceiptID = 5
				mvarReceiptID = value;
				IsUpdated = true;
			}
			get
			{
				string ReceiptID = "";
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.ReceiptID
				ReceiptID = mvarReceiptID;
				return ReceiptID;
			}
		}

		public string TransmissionType
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.TransmissionType = 5
				mvarTransmissionType = value;
				IsUpdated = true;
			}
			get
			{
				string TransmissionType = "";
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.TransmissionType
				TransmissionType = mvarTransmissionType;
				return TransmissionType;
			}
		}

		public int TaxYear
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.TaxYear = 5
				mvarTaxYear = value;
				IsUpdated = true;
			}
			get
			{
				int TaxYear = 0;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.TaxYear
				TaxYear = mvarTaxYear;
				return TaxYear;
			}
		}

		public int ID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.ID = 5
				mvarID = value;
			}
			get
			{
				int ID = 0;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.ID
				ID = mvarID;
				return ID;
			}
		}
	}
}
