﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cACAImportFormatColumn
	{
		//=========================================================
		private int intColOrder;
		private string strColumnName = string.Empty;
		private int intWidth;

		public int ColumnOrder
		{
			set
			{
				intColOrder = value;
			}
			get
			{
				int ColumnOrder = 0;
				ColumnOrder = intColOrder;
				return ColumnOrder;
			}
		}

		public string ColumnName
		{
			set
			{
				strColumnName = value;
			}
			get
			{
				string ColumnName = "";
				ColumnName = strColumnName;
				return ColumnName;
			}
		}

		public int Width
		{
			set
			{
				intWidth = value;
			}
			get
			{
				int Width = 0;
				Width = intWidth;
				return Width;
			}
		}
	}
}
