﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class c1094C
	{
		//=========================================================
		private int lngID;
		private bool boolCorrected;
		private int intYearCovered;
		private string strEmployerName = string.Empty;
		private string strEIN = string.Empty;
		private string straddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strPostalCode = string.Empty;
		private string strContactName = "";
		private string strContactTelephone = string.Empty;
		private string strDesignatedGovernmentEntity = string.Empty;
		private string strEntityEIN = string.Empty;
		private string strEntityAddress = string.Empty;
		private string strEntityCity = string.Empty;
		private string strEntityState = string.Empty;
		private string strEntityPostalCode = string.Empty;
		private string strEntityContact = string.Empty;
		private string strEntityTelephone = string.Empty;
		// vbPorter upgrade warning: intTotal1095CFormsTransmitted As object	OnWrite
		private object intTotal1095CFormsTransmitted;
		private bool boolAuthoritativeTransmittal;
		private int intTotal1095CForMember;
		private bool boolMemberOfAggregatedGroup;
		private bool boolQualifyingOfferMethod;
		private bool boolQualifyingOfferTransitionRelief;
		private bool bool4980HTransitionRelief;
		private bool bool98PercentOfferMethod;
		private cGenericCollection collAggregatedMembers = new cGenericCollection();
		private cALEMonthlyInformation[] aryMonthlyInformation = new cALEMonthlyInformation[12 + 1];
		private bool boolIsChanged;
		private double dblVerticalAlignment;
		private double dblHorizontalAlignment;
		private string strContactMiddle = string.Empty;
		private string strContactLast = string.Empty;
		private string strContactSuffix = string.Empty;
		private string strEntityContactLast = string.Empty;
		private string strEntityContactMiddle = string.Empty;
		private string strEntityContactSuffix = string.Empty;

		public string EntityContactLast
		{
			set
			{
				strEntityContactLast = value;
				IsChanged = true;
			}
			get
			{
				string EntityContactLast = "";
				EntityContactLast = strEntityContactLast;
				return EntityContactLast;
			}
		}

		public string EntityContactMiddle
		{
			set
			{
				strEntityContactMiddle = value;
				IsChanged = true;
			}
			get
			{
				string EntityContactMiddle = "";
				EntityContactMiddle = strEntityContactMiddle;
				return EntityContactMiddle;
			}
		}

		public string EntityContactSuffix
		{
			set
			{
				strEntityContactSuffix = value;
				IsChanged = true;
			}
			get
			{
				string EntityContactSuffix = "";
				EntityContactSuffix = strEntityContactSuffix;
				return EntityContactSuffix;
			}
		}

		public string ContactSuffix
		{
			set
			{
				strContactSuffix = value;
				IsChanged = true;
			}
			get
			{
				string ContactSuffix = "";
				ContactSuffix = strContactSuffix;
				return ContactSuffix;
			}
		}

		public string ContactLastName
		{
			set
			{
				strContactLast = value;
				IsChanged = true;
			}
			get
			{
				string ContactLastName = "";
				ContactLastName = strContactLast;
				return ContactLastName;
			}
		}

		public string ContactMiddleName
		{
			set
			{
				strContactMiddle = value;
				IsChanged = true;
			}
			get
			{
				string ContactMiddleName = "";
				ContactMiddleName = strContactMiddle;
				return ContactMiddleName;
			}
		}

		public string ContactFullName
		{
			get
			{
				string ContactFullName = "";
				ContactFullName = (((strContactName + " " + strContactMiddle).Trim() + " " + strContactLast).Trim() + " " + strContactSuffix).Trim();
				return ContactFullName;
			}
		}

		public double VerticalAlignment
		{
			set
			{
				dblVerticalAlignment = value;
			}
			get
			{
				double VerticalAlignment = 0;
				VerticalAlignment = dblVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public double HorizontalAlignment
		{
			set
			{
				dblHorizontalAlignment = value;
			}
			get
			{
				double HorizontalAlignment = 0;
				HorizontalAlignment = dblHorizontalAlignment;
				return HorizontalAlignment;
			}
		}

		public bool IsChanged
		{
			set
			{
				boolIsChanged = value;
			}
			get
			{
				bool IsChanged = false;
				IsChanged = boolIsChanged;
				return IsChanged;
			}
		}
		// vbPorter upgrade warning: intMonth As object	OnWrite
		public void Set4980HForMonth(int intMonth, string str4980H)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				aryMonthlyInformation[intMonth - 1].Section4980HTransitionRelief = str4980H;
				boolIsChanged = true;
			}
		}
		// vbPorter upgrade warning: intMonth As object	OnWrite
		public void SetAggregatedGroupForMonth(int intMonth, bool boolAggregated)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				aryMonthlyInformation[intMonth - 1].AggregatedGroup = boolAggregated;
				boolIsChanged = true;
			}
		}
		// vbPorter upgrade warning: intMonth As object	OnWrite
		// vbPorter upgrade warning: intCount As int	OnWrite(double, int)
		public void SetTotalEmployeeCountForMonth(int intMonth, int intCount)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				aryMonthlyInformation[intMonth - 1].TotalEmployeeCount = intCount;
				boolIsChanged = true;
			}
		}

		public bool TotalEmployeeCountSameAll12Months
		{
			get
			{
				bool TotalEmployeeCountSameAll12Months = false;
				int x;
				int intCount;
				TotalEmployeeCountSameAll12Months = true;
				intCount = aryMonthlyInformation[0].TotalEmployeeCount;
				for (x = 1; x <= 11; x++)
				{
					if (intCount != aryMonthlyInformation[x].TotalEmployeeCount)
					{
						TotalEmployeeCountSameAll12Months = false;
						break;
					}
				}
				// x
				return TotalEmployeeCountSameAll12Months;
			}
		}
		// vbPorter upgrade warning: intMonth As object	OnWrite
		// vbPorter upgrade warning: intCount As int	OnWrite(double, int)
		public void SetFullTimeEmployeeCountForMonth(int intMonth, int intCount)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				aryMonthlyInformation[intMonth - 1].FullTimeEmployeeCount = intCount;
				boolIsChanged = true;
			}
		}

		public void SetMinimumCoverageForMonth(int intMonth, bool boolCoverage)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				aryMonthlyInformation[intMonth - 1].MinimumEssentialCoverageOffer = boolCoverage;
				boolIsChanged = true;
			}
		}

		public void SetAll12MonthsSection4980H(string strSection4980H)
		{
			int x;
			for (x = 0; x <= 11; x++)
			{
				aryMonthlyInformation[x].Section4980HTransitionRelief = strSection4980H;
			}
			// x
			boolIsChanged = true;
		}

		public bool Section4980HSameAll12Months
		{
			get
			{
				bool Section4980HSameAll12Months = false;
				int x;
				string strSection;
				strSection = aryMonthlyInformation[0].Section4980HTransitionRelief;
				Section4980HSameAll12Months = true;
				if (Section4980HRelief)
				{
					for (x = 1; x <= 11; x++)
					{
						if (strSection != aryMonthlyInformation[x].Section4980HTransitionRelief)
						{
							Section4980HSameAll12Months = false;
							break;
						}
					}
					// x
				}
				return Section4980HSameAll12Months;
			}
		}

		public void SetAll12MonthsAggregatedGroup(bool boolAggregated)
		{
			int x;
			for (x = 0; x <= 11; x++)
			{
				aryMonthlyInformation[x].AggregatedGroup = boolAggregated;
			}
			// x
			boolIsChanged = true;
		}

		public bool InAggregatedGroupSameAll12Months
		{
			get
			{
				bool InAggregatedGroupSameAll12Months = false;
				int x;
				bool boolGroup;
				InAggregatedGroupSameAll12Months = true;
				boolGroup = aryMonthlyInformation[0].AggregatedGroup;
				for (x = 1; x <= 11; x++)
				{
					if (boolGroup != aryMonthlyInformation[x].AggregatedGroup)
					{
						InAggregatedGroupSameAll12Months = false;
						break;
					}
				}
				// x
				return InAggregatedGroupSameAll12Months;
			}
		}
		// vbPorter upgrade warning: intCount As int	OnWrite(double, int)
		public void SetAll12MonthsTotalEmployeeCount(int intCount)
		{
			int x;
			for (x = 0; x <= 11; x++)
			{
				aryMonthlyInformation[x].TotalEmployeeCount = intCount;
			}
			// x
			boolIsChanged = true;
		}
		// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToDouble(
		public void SetAll12MonthsFullTimeEmployeeCount(int intCount)
		{
			int x;
			for (x = 0; x <= 11; x++)
			{
				aryMonthlyInformation[x].FullTimeEmployeeCount = intCount;
			}
			// x
			boolIsChanged = true;
		}

		public bool FullTimeCountSameAll12Months
		{
			get
			{
				bool FullTimeCountSameAll12Months = false;
				int x;
				FullTimeCountSameAll12Months = true;
				int lngCount;
				lngCount = aryMonthlyInformation[0].FullTimeEmployeeCount;
				for (x = 1; x <= 11; x++)
				{
					if (aryMonthlyInformation[x].FullTimeEmployeeCount != lngCount)
					{
						FullTimeCountSameAll12Months = false;
						break;
					}
				}
				// x
				return FullTimeCountSameAll12Months;
			}
		}

		public void SetAll12MonthsMinimumCoverage(bool boolCoverage)
		{
			int x;
			for (x = 0; x <= 11; x++)
			{
				aryMonthlyInformation[x].MinimumEssentialCoverageOffer = boolCoverage;
			}
			// x
			boolIsChanged = true;
		}

		public bool MinimumCoverageSameAll12Months
		{
			get
			{
				bool MinimumCoverageSameAll12Months = false;
				int x;
				bool boolCov;
				boolCov = aryMonthlyInformation[0].MinimumEssentialCoverageOffer;
				MinimumCoverageSameAll12Months = true;
				for (x = 1; x <= 11; x++)
				{
					if (boolCov != aryMonthlyInformation[x].MinimumEssentialCoverageOffer)
					{
						MinimumCoverageSameAll12Months = false;
						break;
					}
				}
				// x
				return MinimumCoverageSameAll12Months;
			}
		}

		public void SetMonthlyInformation(int intMonth, cALEMonthlyInformation monthlyInfo)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				if (!(monthlyInfo == null))
				{
					aryMonthlyInformation[intMonth - 1] = monthlyInfo;
					boolIsChanged = true;
				}
			}
		}

		public cALEMonthlyInformation GetMonthlyInformation(int intMonth)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				return aryMonthlyInformation[intMonth - 1];
			}
			else
			{
				return null;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public bool IsCorrected
		{
			set
			{
				boolCorrected = value;
				boolIsChanged = true;
			}
			get
			{
				bool IsCorrected = false;
				IsCorrected = boolCorrected;
				return IsCorrected;
			}
		}

		public int YearCovered
		{
			set
			{
				intYearCovered = value;
				boolIsChanged = true;
			}
			get
			{
				int YearCovered = 0;
				YearCovered = intYearCovered;
				return YearCovered;
			}
		}

		public string EmployerName
		{
			set
			{
				strEmployerName = value;
				boolIsChanged = true;
			}
			get
			{
				string EmployerName = "";
				EmployerName = strEmployerName;
				return EmployerName;
			}
		}

		public string EIN
		{
			set
			{
				strEIN = value;
				boolIsChanged = true;
			}
			get
			{
				string EIN = "";
				EIN = strEIN;
				return EIN;
			}
		}

		public string Address
		{
			set
			{
				straddress = value;
				boolIsChanged = true;
			}
			get
			{
				string Address = "";
				Address = straddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
				boolIsChanged = true;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
				boolIsChanged = true;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string PostalCode
		{
			set
			{
				strPostalCode = value;
				boolIsChanged = true;
			}
			get
			{
				string PostalCode = "";
				PostalCode = strPostalCode;
				return PostalCode;
			}
		}

		public string ContactName
		{
			set
			{
				strContactName = value;
				boolIsChanged = true;
			}
			get
			{
				string ContactName = "";
				ContactName = strContactName;
				return ContactName;
			}
		}

		public string ContactTelephone
		{
			set
			{
				strContactTelephone = value;
				boolIsChanged = true;
			}
			get
			{
				string ContactTelephone = "";
				ContactTelephone = strContactTelephone;
				return ContactTelephone;
			}
		}

		public string DesignatedGovnernmentEntity
		{
			set
			{
				strDesignatedGovernmentEntity = value;
				boolIsChanged = true;
			}
			get
			{
				string DesignatedGovnernmentEntity = "";
				DesignatedGovnernmentEntity = strDesignatedGovernmentEntity;
				return DesignatedGovnernmentEntity;
			}
		}

		public string EntityEIN
		{
			set
			{
				strEntityEIN = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityEIN = "";
				EntityEIN = strEntityEIN;
				return EntityEIN;
			}
		}

		public string EntityAddress
		{
			set
			{
				strEntityAddress = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityAddress = "";
				EntityAddress = strEntityAddress;
				return EntityAddress;
			}
		}

		public string EntityCity
		{
			set
			{
				strEntityCity = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityCity = "";
				EntityCity = strEntityCity;
				return EntityCity;
			}
		}

		public string EntityState
		{
			set
			{
				strEntityState = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityState = "";
				EntityState = strEntityState;
				return EntityState;
			}
		}

		public string EntityPostalCode
		{
			set
			{
				strEntityPostalCode = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityPostalCode = "";
				EntityPostalCode = strEntityPostalCode;
				return EntityPostalCode;
			}
		}

		public string EntityContact
		{
			set
			{
				strEntityContact = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityContact = "";
				EntityContact = strEntityContact;
				return EntityContact;
			}
		}

		public string EntityTelephone
		{
			set
			{
				strEntityTelephone = value;
				boolIsChanged = true;
			}
			get
			{
				string EntityTelephone = "";
				EntityTelephone = strEntityTelephone;
				return EntityTelephone;
			}
		}

		public int Total1095CTransmitted
		{
			set
			{
				intTotal1095CFormsTransmitted = value;
				boolIsChanged = true;
			}
			get
			{
				int Total1095CTransmitted = 0;
				Total1095CTransmitted = FCConvert.ToInt16(intTotal1095CFormsTransmitted);
				return Total1095CTransmitted;
			}
		}

		public bool IsAuthoritativeTransmittal
		{
			set
			{
				boolAuthoritativeTransmittal = value;
				boolIsChanged = true;
			}
			get
			{
				bool IsAuthoritativeTransmittal = false;
				IsAuthoritativeTransmittal = boolAuthoritativeTransmittal;
				return IsAuthoritativeTransmittal;
			}
		}

		public int Total1095CForMember
		{
			set
			{
				intTotal1095CForMember = value;
				boolIsChanged = true;
			}
			get
			{
				int Total1095CForMember = 0;
				Total1095CForMember = intTotal1095CForMember;
				return Total1095CForMember;
			}
		}

		public bool MemberOfAggregatedGroup
		{
			set
			{
				boolMemberOfAggregatedGroup = value;
				boolIsChanged = true;
			}
			get
			{
				bool MemberOfAggregatedGroup = false;
				MemberOfAggregatedGroup = boolMemberOfAggregatedGroup;
				return MemberOfAggregatedGroup;
			}
		}

		public bool QualifyingOfferMethod
		{
			set
			{
				boolQualifyingOfferMethod = value;
				boolIsChanged = true;
			}
			get
			{
				bool QualifyingOfferMethod = false;
				QualifyingOfferMethod = boolQualifyingOfferMethod;
				return QualifyingOfferMethod;
			}
		}

		public bool QualifyingOfferTransitionRelief
		{
			set
			{
				boolQualifyingOfferTransitionRelief = value;
				boolIsChanged = true;
			}
			get
			{
				bool QualifyingOfferTransitionRelief = false;
				QualifyingOfferTransitionRelief = boolQualifyingOfferTransitionRelief;
				return QualifyingOfferTransitionRelief;
			}
		}

		public bool Section4980HRelief
		{
			set
			{
				bool4980HTransitionRelief = value;
				boolIsChanged = true;
			}
			get
			{
				bool Section4980HRelief = false;
				Section4980HRelief = bool4980HTransitionRelief;
				return Section4980HRelief;
			}
		}

		public bool NinetyEightPercentOfferMethod
		{
			set
			{
				bool98PercentOfferMethod = value;
				boolIsChanged = true;
			}
			get
			{
				bool NinetyEightPercentOfferMethod = false;
				NinetyEightPercentOfferMethod = bool98PercentOfferMethod;
				return NinetyEightPercentOfferMethod;
			}
		}

		public cGenericCollection AggregatedMembers
		{
			get
			{
				cGenericCollection AggregatedMembers = null;
				AggregatedMembers = collAggregatedMembers;
				return AggregatedMembers;
			}
		}

		public c1094C() : base()
		{
			int x;
			for (x = 0; x <= 11; x++)
			{
				aryMonthlyInformation[x] = new cALEMonthlyInformation();
				aryMonthlyInformation[x].CoverageMonth = x + 1;
			}
			// x
		}
	}
}
