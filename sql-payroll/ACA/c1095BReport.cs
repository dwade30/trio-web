﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class c1095BReport
	{
		//=========================================================
		private cGenericCollection formsList;
		private cCoverageProvider providerRecord;
		private bool boolMaskSSNs;
		private bool boolTestPrint;
		private int intReportYear;
		private bool boolHasOverFlows;
		private double dblVerticalAlignment;
		private double dblHorizontalAlignment;
		private bool boolIsCorrection;

		public bool IsCorrection
		{
			set
			{
				boolIsCorrection = value;
			}
			get
			{
				bool IsCorrection = false;
				IsCorrection = boolIsCorrection;
				return IsCorrection;
			}
		}

		public double VerticalAlignment
		{
			set
			{
				dblVerticalAlignment = value;
			}
			get
			{
				double VerticalAlignment = 0;
				VerticalAlignment = dblVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public double HorizontalAlignment
		{
			set
			{
				dblHorizontalAlignment = value;
			}
			get
			{
				double HorizontalAlignment = 0;
				HorizontalAlignment = dblHorizontalAlignment;
				return HorizontalAlignment;
			}
		}

		public cCoverageProvider CoverageProvider
		{
			set
			{
				providerRecord = value;
			}
			get
			{
				cCoverageProvider CoverageProvider = null;
				CoverageProvider = providerRecord;
				return CoverageProvider;
			}
		}

		public bool HasOverflows
		{
			set
			{
				boolHasOverFlows = value;
			}
			get
			{
				bool HasOverflows = false;
				HasOverflows = boolHasOverFlows;
				return HasOverflows;
			}
		}

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public bool TestPrint
		{
			set
			{
				boolTestPrint = value;
			}
			get
			{
				bool TestPrint = false;
				TestPrint = boolTestPrint;
				return TestPrint;
			}
		}

		public bool MaskSSNs
		{
			set
			{
				boolMaskSSNs = value;
			}
			get
			{
				bool MaskSSNs = false;
				MaskSSNs = boolMaskSSNs;
				return MaskSSNs;
			}
		}

		public cGenericCollection ListOfForms
		{
			set
			{
				formsList = value;
			}
			get
			{
				cGenericCollection ListOfForms = null;
				ListOfForms = formsList;
				return ListOfForms;
			}
		}
	}
}
