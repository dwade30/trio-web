//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	public class cACAService
	{
		//=========================================================
		private int lngLastError;
		private string strLastError = string.Empty;
		const int CNSTLatestTaxYear = 2016;
		private const string CNSTVendorContactFirstName = "Corey";
		private const string CNSTVendorContactLastName = "Gray";
		private const string CNSTVendorContactPhone = "2079426222";

		public void ClearErrors()
		{
			lngLastError = 0;
			strLastError = "";
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public string LastErrorDescription
		{
			get
			{
				string LastErrorDescription = "";
				LastErrorDescription = strLastError;
				return LastErrorDescription;
			}
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public cCoverageProvider GetCoverageProvider(int lngID)
		{
			cCoverageProvider GetCoverageProvider = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("Select * from coverageProviders where id = " + FCConvert.ToString(lngID), "twpy0000.vb1");
			cCoverageProvider retProvider = new cCoverageProvider();
			if (!rsLoad.EndOfFile())
			{
				retProvider.Address = FCConvert.ToString(rsLoad.Get_Fields_String("Address"));
				retProvider.City = FCConvert.ToString(rsLoad.Get_Fields_String("City"));
				retProvider.EIN = FCConvert.ToString(rsLoad.Get_Fields_String("EIN"));
				retProvider.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				retProvider.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
				retProvider.PostalCode = FCConvert.ToString(rsLoad.Get_Fields_String("PostalCode"));
				retProvider.State = FCConvert.ToString(rsLoad.Get_Fields("state"));
				retProvider.Telephone = FCConvert.ToString(rsLoad.Get_Fields_String("Telephone"));
				retProvider.PlanMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("PlanStartMonth"))));
				retProvider.Use1095B = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Use1095B"));
			}
			GetCoverageProvider = retProvider;
			return GetCoverageProvider;
		}

		public bool SaveCoverageProvider(ref cCoverageProvider cProvider)
		{
			bool SaveCoverageProvider = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(cProvider == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from CoverageProviders where id = " + cProvider.ID, "Payroll");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("PlanName", cProvider.PlanName);
					rsSave.Set_Fields("PlanStartMonth", cProvider.PlanMonth);
					rsSave.Set_Fields("address", cProvider.Address);
					rsSave.Set_Fields("city", cProvider.City);
					rsSave.Set_Fields("EIN", cProvider.EIN);
					rsSave.Set_Fields("Name", cProvider.Name);
					rsSave.Set_Fields("PostalCode", cProvider.PostalCode);
					rsSave.Set_Fields("State", cProvider.State);
					rsSave.Set_Fields("Telephone", cProvider.Telephone);
					rsSave.Set_Fields("Use1095B", cProvider.Use1095B);
					rsSave.Update();
					cProvider.ID = rsSave.Get_Fields("id");
				}
				SaveCoverageProvider = true;
				return SaveCoverageProvider;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return SaveCoverageProvider;
		}

		public cGenericCollection GetAllProviders()
		{
			cGenericCollection GetAllProviders = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection retList = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCoverageProvider tempProvider;
				rsLoad.OpenRecordset("select * from coverageproviders order by name", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					tempProvider = new cCoverageProvider();
					tempProvider.PlanName = FCConvert.ToString(rsLoad.Get_Fields_String("PlanName"));
					tempProvider.PlanMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("PlanStartMonth"))));
					tempProvider.Address = FCConvert.ToString(rsLoad.Get_Fields("address"));
					tempProvider.City = FCConvert.ToString(rsLoad.Get_Fields("city"));
					tempProvider.EIN = FCConvert.ToString(rsLoad.Get_Fields("ein"));
					tempProvider.ID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
					tempProvider.Name = FCConvert.ToString(rsLoad.Get_Fields("name"));
					tempProvider.PostalCode = FCConvert.ToString(rsLoad.Get_Fields("postalcode"));
					tempProvider.State = FCConvert.ToString(rsLoad.Get_Fields("state"));
					tempProvider.Telephone = FCConvert.ToString(rsLoad.Get_Fields("telephone"));
					tempProvider.Use1095B = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Use1095B"));
					tempProvider.IsChanged = false;
					retList.AddItem(tempProvider);
					rsLoad.MoveNext();
				}
				GetAllProviders = retList;
				return GetAllProviders;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return GetAllProviders;
		}

		public cCoverageProvider LoadPlan(int lngID)
		{
			cCoverageProvider LoadPlan = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cCoverageProvider loadedPlan = new cCoverageProvider();
				if (lngID > 0)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					rsLoad.OpenRecordset("select * from coverageproviders where id = " + FCConvert.ToString(lngID), "Payroll");
					if (!rsLoad.EndOfFile())
					{
						loadedPlan.PlanName = FCConvert.ToString(rsLoad.Get_Fields_String("PlanName"));
						loadedPlan.PlanMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("planstartMonth"))));
						loadedPlan.Address = FCConvert.ToString(rsLoad.Get_Fields("address"));
						loadedPlan.City = FCConvert.ToString(rsLoad.Get_Fields("city"));
						loadedPlan.EIN = FCConvert.ToString(rsLoad.Get_Fields("ein"));
						loadedPlan.ID = lngID;
						loadedPlan.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
						loadedPlan.PostalCode = FCConvert.ToString(rsLoad.Get_Fields("Postalcode"));
						loadedPlan.State = FCConvert.ToString(rsLoad.Get_Fields("state"));
						loadedPlan.Telephone = FCConvert.ToString(rsLoad.Get_Fields("telephone"));
						loadedPlan.Use1095B = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Use1095B"));
						loadedPlan.IsChanged = false;
					}
				}
				LoadPlan = loadedPlan;
				return LoadPlan;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return LoadPlan;
		}

		public cACAEmployeeSetup GetACAEmployeeSetup(int lngID)
		{
			cACAEmployeeSetup GetACAEmployeeSetup = null;
			ClearErrors();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("Select * from ACAEmployeeSetup where id = " + FCConvert.ToString(lngID), "Payroll");
			cACAEmployeeSetup retSetup = new cACAEmployeeSetup();
			if (!rsLoad.EndOfFile())
			{
				FillSetup(ref rsLoad, ref retSetup);
			}
			GetACAEmployeeSetup = retSetup;
			return GetACAEmployeeSetup;
		}

		public cACAEmployeeSetup GetACAEmployeeSetupByEmployee(int lngID, int lngYear)
		{
			cACAEmployeeSetup GetACAEmployeeSetupByEmployee = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from ACAEmployeeSetup where employeeid = " + FCConvert.ToString(lngID) + " and reportyear = " + FCConvert.ToString(lngYear), "Payroll");
				cACAEmployeeSetup retSetup = new cACAEmployeeSetup();
				if (!rsLoad.EndOfFile())
				{
					FillSetup(ref rsLoad, ref retSetup);
				}
				GetACAEmployeeSetupByEmployee = retSetup;
				return GetACAEmployeeSetupByEmployee;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = fecherFoundation.Information.Err(ex).Number;
				strLastError = fecherFoundation.Information.Err(ex).Description + "  In GetACAEmployeeSetupByEmployee line " + fecherFoundation.Information.Erl();
			}
			return GetACAEmployeeSetupByEmployee;
		}

		public void DeleteACAEmployeeSetup(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("Delete from ACAEmployeeSetup where id = " + FCConvert.ToString(lngID), "Payroll");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = fecherFoundation.Information.Err(ex).Number;
				strLastError = fecherFoundation.Information.Err(ex).Description + " In DeleteACAEmployeeSetup";
			}
		}

		public void SaveACASetup(cACAEmployeeSetup emp)
		{
			if (!(emp == null))
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from acaemployeesetup where id = " + emp.ID, "Payroll");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("EmployeeID", emp.EmployeeID);
				rsSave.Set_Fields("ReportYear", emp.ReportYear);
				rsSave.Set_Fields("CoverageDeclined", emp.CoverageDeclined);
				rsSave.Set_Fields("EmployeeNumber", emp.EmployeeNumber);
				rsSave.Set_Fields("OriginOfPolicy", emp.OriginOfPolicy);
				rsSave.Set_Fields("CoverageProviderID", emp.CoverageProviderID);
				int x;
				for (x = 1; x <= 12; x++)
				{
					rsSave.Set_Fields("CoverageRequiredMonth" + x, emp.GetCoverageRequiredCodeForMonth(x));
					rsSave.Set_Fields("EmployeeLowestPremiumMonth" + x, emp.GetEmployeeShareLowestPremiumForMonth(x));
					rsSave.Set_Fields("SafeHarborCodeMonth" + x, emp.GetSafeHarborCodeForMonth(x));
                    rsSave.Set_Fields("ZipCodeMonth" + x, emp.GetZipCodeForMonth(x));
				}
				// x
				rsSave.Update();
				emp.ID = rsSave.Get_Fields("id");
				foreach (cACAEmployeeDependent dep in emp.Dependents)
				{
					if (!dep.Deleted)
					{
						rsSave.OpenRecordset("select * from ACAEmployeeDependents where id = " + FCConvert.ToString(dep.ID), "Payroll");
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
						rsSave.Set_Fields("ACAEmployeeSetupID", emp.ID);
						rsSave.Set_Fields("DateOfBirth", dep.DateOfBirth);
						rsSave.Set_Fields("TerminationDate", dep.HealthCareTerminationDate);
						rsSave.Set_Fields("FirstName", dep.FirstName);
						rsSave.Set_Fields("MiddleName", dep.MiddleName);
						rsSave.Set_Fields("LastName", dep.LastName);
						rsSave.Set_Fields("Suffix", dep.Suffix);
						rsSave.Set_Fields("SSN", dep.SSN);
						for (x = 1; x <= 12; x++)
						{
							rsSave.Set_Fields("CoveredMonth" + x, dep.GetIsCoveredForMonth(x));
						}
						// x
						rsSave.Update();
						dep.ID = FCConvert.ToInt32(rsSave.Get_Fields("id"));
					}
					else
					{
						rsSave.Execute("delete from ACAEmployeeDependents where id = " + FCConvert.ToString(dep.ID), "Payroll");
					}
				}
				// dep
			}
		}

		private void FillSetup(ref clsDRWrapper rsLoad, ref cACAEmployeeSetup retSetup)
		{
			if (rsLoad != null)
			{
				if (!(retSetup == null))
				{
					if (!rsLoad.EndOfFile())
					{
						retSetup.ID = rsLoad.Get_Fields("ID");
						retSetup.EmployeeID = rsLoad.Get_Fields("employeeid");
						retSetup.EmployeeNumber = rsLoad.Get_Fields("EmployeeNumber");
						retSetup.CoverageDeclined = rsLoad.Get_Fields_Boolean("CoverageDeclined");
						retSetup.ReportYear = rsLoad.Get_Fields("reportyear");
						retSetup.OriginOfPolicy = rsLoad.Get_Fields_String("OriginOfPolicy");
						if (!rsLoad.IsFieldNull("CoverageProviderID"))
						{
							retSetup.CoverageProviderID = rsLoad.Get_Fields_Int32("CoverageProviderID");
						}
						int x;
						for (x = 1; x <= 12; x++)
						{
							retSetup.SetCoverageRequiredCodeForMonth(x, rsLoad.Get_Fields_String("CoverageRequiredMonth" + x));
							retSetup.SetEmployeeShareLowestPremiumForMonth(x,rsLoad.Get_Fields_Double("EmployeeLowestPremiumMonth" + x));
							retSetup.SetSafeHarborCodeForMonth(x, rsLoad.Get_Fields("SafeHarborCodeMonth" + x));
                            retSetup.SetZipCodeForMonth(x,rsLoad.Get_Fields_String("ZipCodeMonth" + x));
						}
						// x
						clsDRWrapper rsDetail = new clsDRWrapper();
						rsDetail.OpenRecordset("select * from ACAEmployeeDependents where acaemployeesetupid = " + retSetup.ID, "Payroll");
						cACAEmployeeDependent dep;
						while (!rsDetail.EndOfFile())
						{
							dep = new cACAEmployeeDependent();
							dep.DateOfBirth = FCConvert.ToString(rsDetail.Get_Fields("DateOfBirth"));
							dep.FirstName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDetail.Get_Fields_String("FirstName")));
							dep.HealthCareTerminationDate = FCConvert.ToString(rsDetail.Get_Fields_String("TerminationDate"));
							dep.ID = FCConvert.ToInt32(rsDetail.Get_Fields("ID"));
							dep.LastName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDetail.Get_Fields_String("LastName")));
							dep.MiddleName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDetail.Get_Fields_String("MiddleName")));
							dep.SSN = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDetail.Get_Fields_String("SSN")));
							dep.Suffix = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDetail.Get_Fields("suffix")));
							dep.ACAEmployeeSetupID = FCConvert.ToInt32(rsDetail.Get_Fields("acaemployeesetupid"));
							for (x = 1; x <= 12; x++)
							{
								dep.SetIsCoveredForMonth(x, rsDetail.Get_Fields_Boolean("CoveredMonth" + FCConvert.ToString(x)));
							}
							// x
							retSetup.Dependents.Add(dep);
							rsDetail.MoveNext();
						}
					}
				}
			}
		}

		public cACASetupList GetAll(int intYear)
		{
			cACASetupList GetAll = null;
			cACASetupList colSetup = new cACASetupList();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from ACAEmployeeSetup where reportyear = " + FCConvert.ToString(intYear), "Payroll");
			cACAEmployeeSetup tSetup;
			while (!rsLoad.EndOfFile())
			{
				tSetup = new cACAEmployeeSetup();
				FillSetup(ref rsLoad, ref tSetup);
				colSetup.AddItem(tSetup);
				rsLoad.MoveNext();
			}
			GetAll = colSetup;
			return GetAll;
		}
		// vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
		public cACASetupList GetAllPermissable(int intYear, int lngUserID)
		{
			cACASetupList GetAllPermissable = null;
			cACASetupList colSetup = new cACASetupList();
			clsDRWrapper rsLoad = new clsDRWrapper();
			Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
			cEmployeeService employeeService = new cEmployeeService();
			rsLoad.OpenRecordset("select * from ACAEmployeeSetup where reportyear = " + FCConvert.ToString(intYear), "Payroll");
			cACAEmployeeSetup tSetup;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			while (!rsLoad.EndOfFile())
			{
				if (!employeeDict.ContainsKey(rsLoad.Get_Fields("EmployeeNumber")))
				{
					tSetup = new cACAEmployeeSetup();
					FillSetup(ref rsLoad, ref tSetup);
					colSetup.AddItem(tSetup);
				}
				rsLoad.MoveNext();
			}
			GetAllPermissable = colSetup;
			return GetAllPermissable;
		}

		public bool SaveImportFormat(ref cACADataImportFormat impFormat)
		{
			bool SaveImportFormat = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(impFormat == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					clsDRWrapper rsColumns = new clsDRWrapper();
					rsSave.OpenRecordset("select * from ACAFileFormat where id = " + impFormat.ID, "Payroll");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("formatname", impFormat.Description);
					rsSave.Set_Fields("FirstLineUnreadable", impFormat.FirstLineUnreadable);
					rsSave.Set_Fields("SeparatorChar", impFormat.SeparatorCharacter);
					rsSave.Set_Fields("EmployeeIdentifierType", impFormat.EmployeeIdentifierType);
					rsSave.Update();
					impFormat.ID = rsSave.Get_Fields("ID");
					rsColumns.Execute("Delete from acafileformatcolumns where FileFormatID = " + impFormat.ID, "Payroll");
					int intCount = 0;
					intCount = 0;
					rsColumns.OpenRecordset("select * from acafileformatcolumns where fileformatid = " + impFormat.ID, "Payroll");
					foreach (cACAImportFormatColumn cCol in impFormat.Fields)
					{
						intCount += 1;
						rsColumns.AddNew();
						rsColumns.Set_Fields("ColumnName", cCol.ColumnName);
						rsColumns.Set_Fields("ColumnOrder", intCount);
						rsColumns.Set_Fields("ColumnWidth", cCol.Width);
						rsColumns.Set_Fields("FileFormatID", impFormat.ID);
						rsColumns.Update();
					}
					impFormat.IsChanged = false;
				}
				SaveImportFormat = true;
				return SaveImportFormat;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return SaveImportFormat;
		}

		public cACADataImportFormat LoadImportFormat(int lngID)
		{
			cACADataImportFormat LoadImportFormat = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cACADataImportFormat loadedFormat = new cACADataImportFormat();
				if (lngID > 0)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					clsDRWrapper rsColumns = new clsDRWrapper();
					rsLoad.OpenRecordset("select * from ACAFileFormat where id = " + FCConvert.ToString(lngID), "Payroll");
					if (!rsLoad.EndOfFile())
					{
						loadedFormat.Description = FCConvert.ToString(rsLoad.Get_Fields("formatname"));
						loadedFormat.FirstLineUnreadable = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("FirstLineUnreadable"));
						loadedFormat.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
						loadedFormat.EmployeeIdentifierType = (modGlobalVariables.ACAEmployeeIdentifierType)rsLoad.Get_Fields_Int32("EmployeeIdentifierType");
						loadedFormat.SeparatorCharacter = FCConvert.ToString(rsLoad.Get_Fields_String("SeparatorChar"));
						rsColumns.OpenRecordset("select * from ACAFileFormatColumns where FileFormatID = " + FCConvert.ToString(lngID) + " order by ColumnOrder", "twpy0000.vb1");
						cACAImportFormatColumn cCol;
						while (!rsColumns.EndOfFile())
						{
							cCol = new cACAImportFormatColumn();
							cCol.ColumnName = FCConvert.ToString(rsColumns.Get_Fields_String("ColumnName"));
							cCol.Width = FCConvert.ToInt32(Math.Round(Conversion.Val(rsColumns.Get_Fields_Int32("ColumnWidth"))));
							loadedFormat.AddColumn(cCol);
							rsColumns.MoveNext();
						}
						loadedFormat.IsChanged = false;
						LoadImportFormat = loadedFormat;
					}
				}
				return LoadImportFormat;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return LoadImportFormat;
		}

		public cGenericCollection GetImportFormatsList()
		{
			cGenericCollection GetImportFormatsList = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			cGenericCollection importList = new cGenericCollection();
			rsLoad.OpenRecordset("select * from acafileformat order by formatname", "Payroll");
			cACADataImportFormat tFormat;
			while (!rsLoad.EndOfFile())
			{
				tFormat = new cACADataImportFormat();
				tFormat.Description = FCConvert.ToString(rsLoad.Get_Fields("formatname"));
				tFormat.FirstLineUnreadable = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("firstlineunreadable"));
				tFormat.SeparatorCharacter = FCConvert.ToString(rsLoad.Get_Fields_String("SeparatorChar"));
                tFormat.EmployeeIdentifierType = (modGlobalVariables.ACAEmployeeIdentifierType)rsLoad.Get_Fields_Int32("EmployeeIdentifierType");
                tFormat.ID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
				tFormat.IsChanged = false;
				importList.AddItem(tFormat);
				rsLoad.MoveNext();
			}
			GetImportFormatsList = importList;
			return GetImportFormatsList;
		}

		private bool ParseAsBoolean(string strValue)
		{
			bool ParseAsBoolean = false;
			bool boolReturn;
			boolReturn = false;
			if ((fecherFoundation.Strings.LCase(strValue) == "y") || (fecherFoundation.Strings.LCase(strValue) == "yes"))
			{
				boolReturn = true;
			}
			else if ((fecherFoundation.Strings.LCase(strValue) == "t") || (fecherFoundation.Strings.LCase(strValue) == "true"))
			{
				boolReturn = true;
			}
			else if (fecherFoundation.Strings.LCase(strValue) == "1")
			{
				boolReturn = true;
			}
			ParseAsBoolean = boolReturn;
			return ParseAsBoolean;
		}

		public cGenericCollection ImportCoveredDependentsPreview(ref cACADataImportFormat impFormat, string strFileName, int intLimit)
		{
			cGenericCollection ImportCoveredDependentsPreview = null;
			ImportCoveredDependentsPreview = ImportCoveredDependents(impFormat, strFileName, intLimit);
			return ImportCoveredDependentsPreview;
		}

		public cGenericCollection ImportCoveredDependentsFile(cACADataImportFormat impFormat, string strFileName, int intCoverageYear)
		{
			cGenericCollection ImportCoveredDependentsFile = null;
			cGenericCollection parsedRecords;
			parsedRecords = ImportCoveredDependents(impFormat, strFileName, 0);
			clsDRWrapper rsEmployees = new clsDRWrapper();
			string strSQL;
			cACAEmployeeDependentImported impRecord;
			cACAEmployeeDependent depRecord;
			int lngEmployeeID = 0;
			int lngACAEmployeeSetupID = 0;
			bool boolFound = false;
			cGenericCollection employeeSetups = new cGenericCollection();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			string strEmployeeNumber = "";
			int x;
			string strComma;
			cGenericCollection collBadEmployees = new cGenericCollection();
			strSQL = "select id,employeenumber,ssn from tblemployeemaster";
			rsEmployees.OpenRecordset(strSQL, "Payroll");
			strComma = "";
			if (!(parsedRecords == null))
			{
				parsedRecords.MoveFirst();
				while (parsedRecords.IsCurrent())
				{
					// determine what employee setup the record belongs to
					lngEmployeeID = 0;
					lngACAEmployeeSetupID = 0;
					impRecord = (cACAEmployeeDependentImported)parsedRecords.GetCurrentItem();
					strEmployeeNumber = "";
					if (impFormat.EmployeeIdentifierType == modGlobalVariables.ACAEmployeeIdentifierType.SSN)
					{
						if (rsEmployees.FindFirst("ssn = '" + impRecord.EmployeeIdentifier + "'"))
						{
							lngEmployeeID = FCConvert.ToInt16(rsEmployees.Get_Fields("id"));
							strEmployeeNumber = FCConvert.ToString(rsEmployees.Get_Fields("EmployeeNumber"));
						}
					}
					else
					{
						if (rsEmployees.FindFirst("EmployeeNumber = '" + impRecord.EmployeeIdentifier + "'"))
						{
							lngEmployeeID = FCConvert.ToInt16(rsEmployees.Get_Fields("id"));
							strEmployeeNumber = FCConvert.ToString(rsEmployees.Get_Fields("EmployeeNumber"));
						}
					}
					if (lngEmployeeID > 0)
					{
						// if it hasn't been loaded yet load it and clear it
						boolFound = false;
						employeeSetups.MoveFirst();
						while (employeeSetups.IsCurrent() && !boolFound)
						{
							empSetup = (cACAEmployeeSetup)employeeSetups.GetCurrentItem();
							if (empSetup.EmployeeID == lngEmployeeID)
							{
								boolFound = true;
								break;
							}
							employeeSetups.MoveNext();
						}
						if (boolFound)
						{
						}
						else
						{
							empSetup = GetACAEmployeeSetupByEmployee(lngEmployeeID, intCoverageYear);
							if (empSetup.ID == 0)
							{
								// new
								empSetup.EmployeeID = lngEmployeeID;
								empSetup.EmployeeNumber = strEmployeeNumber;
								empSetup.ReportYear = intCoverageYear;
							}
							else
							{
								foreach (cACAEmployeeDependent depRecord_foreach in empSetup.Dependents)
								{
									depRecord = depRecord_foreach;
									depRecord.Deleted = true;
									depRecord = null;
								}
							}
							employeeSetups.AddItem(empSetup);
						}
						// add this dependent
						depRecord = new cACAEmployeeDependent();
						depRecord.ACAEmployeeSetupID = lngACAEmployeeSetupID;
						if (impRecord.CoveredAll12Months)
						{
							depRecord.CoveredAll12Months = true;
						}
						else
						{
							for (x = 1; x <= 12; x++)
							{
								depRecord.SetIsCoveredForMonth(x, impRecord.GetIsCoveredForMonth(x));
							}
						}
						depRecord.DateOfBirth = impRecord.DateOfBirth;
						depRecord.FirstName = impRecord.FirstName;
						depRecord.HealthCareTerminationDate = impRecord.HealthCareTerminationDate;
						depRecord.LastName = impRecord.LastName;
						depRecord.MiddleName = impRecord.MiddleName;
						depRecord.SSN = impRecord.SSN;
						depRecord.Suffix = impRecord.Suffix;
						empSetup.Dependents.Add(depRecord);
					}
					else
					{
						collBadEmployees.AddItem("Bad Employee Identifier: " + impRecord.EmployeeIdentifier);
					}
					parsedRecords.MoveNext();
				}
				employeeSetups.MoveFirst();
				while (employeeSetups.IsCurrent())
				{
					SaveACASetup((cACAEmployeeSetup)employeeSetups.GetCurrentItem());
					employeeSetups.MoveNext();
				}
			}
			else
			{
				collBadEmployees.AddItem("No records loaded");
			}
			ImportCoveredDependentsFile = collBadEmployees;
			return ImportCoveredDependentsFile;
		}

		private cGenericCollection ImportCoveredDependents(cACADataImportFormat impFormat, string strFileName, int intLimit)
		{
			cGenericCollection ImportCoveredDependents = null;
			StreamReader ts = null;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intCount = 0;
				if (!(impFormat == null))
				{
					FCFileSystem fso = new FCFileSystem();
					if (!FCFileSystem.FileExists(strFileName))
					{
						fecherFoundation.Information.Err().Raise(53, "", "File not found", null, null);
					}
					ts = FCFileSystem.OpenText(strFileName);
					boolOpen = true;
					string strLine = "";
					FCCollection textRecords = new FCCollection();
					strLine = ts.ReadLine();
					intCount = 0;
					if (!impFormat.FirstLineUnreadable)
					{
						textRecords.Add(strLine);
						intCount += 1;
					}
					while (!ts.EndOfStream && (intLimit == 0 || intCount < intLimit))
					{
						strLine = ts.ReadLine();
						textRecords.Add(strLine);
						intCount += 1;
					}
					ts.Close();
					boolOpen = false;
					cGenericCollection parsedRecords;
					parsedRecords = ParseCoveredDependents(impFormat, textRecords);
					ImportCoveredDependents = parsedRecords;
				}
				return ImportCoveredDependents;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolOpen)
					ts.Close();
				fecherFoundation.Information.Err(ex).Raise(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Source, fecherFoundation.Information.Err(ex).Description, null, null);
			}
			return ImportCoveredDependents;
		}

		public cGenericCollection ParseCoveredDependents(cACADataImportFormat impFormat, FCCollection textRecords)
		{
			cGenericCollection ParseCoveredDependents = null;
			// sends back a collection of parsed lines
			string strLine = "";
			cGenericCollection parsedList = new cGenericCollection();
			FCCollection parsedLine = new FCCollection();
			cACAEmployeeDependentImported importedRecord;
			int x;
			cACAImportFormatColumn setCol;
			string strTemp = "";
			int intindex;
			if (!(impFormat == null) && !(textRecords == null))
			{
				for (intindex = 1; intindex <= textRecords.Count; intindex++)
				{
					// Do While textRecords.IsCurrent
					strLine = textRecords[intindex];
					// strLine = textRecords.GetCurrentItem
					parsedLine = ParseTextLine(strLine, impFormat.SeparatorCharacter);
					importedRecord = new cACAEmployeeDependentImported();
					for (x = 1; x <= impFormat.Fields.Count; x++)
					{
						setCol = impFormat.Fields[x];
						if (x <= parsedLine.Count)
						{
							strTemp = parsedLine[x];
							if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "emp ssn")
							{
								importedRecord.EmployeeIdentifier = strTemp;
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "ssn")
							{
								importedRecord.SSN = strTemp.Replace("-", "");
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "full name")
							{
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "first")
							{
								importedRecord.FirstName = strTemp;
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "middle")
							{
								importedRecord.MiddleName = strTemp;
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "last")
							{
								importedRecord.LastName = strTemp;
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "sfx")
							{
								importedRecord.Suffix = strTemp;
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "dob")
							{
								importedRecord.DateOfBirth = strTemp;
							}
							else if (fecherFoundation.Strings.LCase(setCol.ColumnName) == "all 12")
							{
								importedRecord.CoveredAll12Months = ParseAsBoolean(strTemp);
							}
							else if ((fecherFoundation.Strings.LCase(setCol.ColumnName) == "jan") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "feb") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "mar") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "apr") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "may") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "jun") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "jul") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "aug") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "sep") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "oct") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "nov") || (fecherFoundation.Strings.LCase(setCol.ColumnName) == "dec"))
							{
								if (!(importedRecord.CoveredAll12Months && strTemp == ""))
								{
									importedRecord.SetIsCoveredForMonth(GetMonthFromAbbreviation(setCol.ColumnName), ParseAsBoolean(strTemp));
								}
							}
						}
					}
					parsedList.AddItem(importedRecord);
					// textRecords.MoveNext
					// Loop
				}
			}
			ParseCoveredDependents = parsedList;
			return ParseCoveredDependents;
		}

		private int GetMonthFromAbbreviation(string strMonth)
		{
			int GetMonthFromAbbreviation = 0;
			int intMonth = 0;
			if (fecherFoundation.Strings.LCase(strMonth) == "jan")
			{
				intMonth = 1;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "feb")
			{
				intMonth = 2;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "mar")
			{
				intMonth = 3;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "apr")
			{
				intMonth = 4;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "may")
			{
				intMonth = 5;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "jun")
			{
				intMonth = 6;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "jul")
			{
				intMonth = 7;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "aug")
			{
				intMonth = 8;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "sep")
			{
				intMonth = 9;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "oct")
			{
				intMonth = 10;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "nov")
			{
				intMonth = 11;
			}
			else if (fecherFoundation.Strings.LCase(strMonth) == "dec")
			{
				intMonth = 12;
			}
			GetMonthFromAbbreviation = intMonth;
			return GetMonthFromAbbreviation;
		}

		private FCCollection ParseTextLine(string strLine, string strSeparator)
		{
			bool boolInQuotes = false;
			FCCollection parsedLine = new FCCollection();
			string strCurrentString = "";
			string strChar = "";
			int lngIndex;
			if (strSeparator != "")
			{
				for (lngIndex = 1; lngIndex <= strLine.Length; lngIndex++)
				{
					strChar = Strings.Mid(strLine, lngIndex, 1);
					if (strChar == FCConvert.ToString(Convert.ToChar(34)))
					{
						boolInQuotes = !boolInQuotes;
					}
					else if (strChar == strSeparator && !boolInQuotes)
					{
						parsedLine.Add(strCurrentString);
						strCurrentString = "";
					}
					else
					{
						strCurrentString += strChar;
					}
				}
				parsedLine.Add(strCurrentString);
			}
			else
			{
				parsedLine.Add(strLine);
			}
			return parsedLine;
		}

		public cGenericCollection LoadALEMembers(int intYear)
		{
			cGenericCollection LoadALEMembers = null;
			cGenericCollection collReturn = new cGenericCollection();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from AggregatedALEMembers where YearCovered = " + FCConvert.ToString(intYear), "Payroll");
			cAggregatedMember aMember;
			while (!rsLoad.EndOfFile())
			{
				aMember = new cAggregatedMember();
				aMember.EIN = FCConvert.ToString(rsLoad.Get_Fields_String("EIN"));
				aMember.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				aMember.Name = FCConvert.ToString(rsLoad.Get_Fields_String("MemberName"));
				collReturn.AddItem(aMember);
				rsLoad.MoveNext();
			}
			LoadALEMembers = collReturn;
			return LoadALEMembers;
		}

		public void SaveALEMembers(cGenericCollection aleMembers, int intYear)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			if (intYear > 0)
			{
				if (!(aleMembers == null))
				{
					rsSave.Execute("delete from AggregatedALEMembers where yearcovered = " + FCConvert.ToString(intYear), "Payroll");
					aleMembers.MoveFirst();
					rsSave.OpenRecordset("select * from AggregatedALEMembers where yearcovered = " + FCConvert.ToString(intYear), "Payroll");
					cAggregatedMember aMember;
					while (aleMembers.IsCurrent())
					{
						aMember = (cAggregatedMember)aleMembers.GetCurrentItem();
						rsSave.AddNew();
						rsSave.Set_Fields("EIN", aMember.EIN);
						rsSave.Set_Fields("MemberName", aMember.Name);
						rsSave.Set_Fields("YearCovered", intYear);
						rsSave.Update();
						aMember.ID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
						aleMembers.MoveNext();
					}
				}
			}
		}

		private cGenericCollection Load1094CMonthlyDetail(int lngID)
		{
			cGenericCollection Load1094CMonthlyDetail = null;
			cGenericCollection collReturn = new cGenericCollection();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("Select * from ACA1094CMonthlyDetail where parentid = " + FCConvert.ToString(lngID) + " order by CoveredMonth", "Payroll");
			cALEMonthlyInformation mDetail;
			while (!rsLoad.EndOfFile())
			{
				mDetail = new cALEMonthlyInformation();
				mDetail.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				mDetail.CoverageMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CoveredMonth"))));
				mDetail.AggregatedGroup = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AggregatedGroup"));
				mDetail.FullTimeEmployeeCount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("FullTimeEmployeeCount"))));
				mDetail.MinimumEssentialCoverageOffer = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("MinimumEssentialCoverageOffer"));
				mDetail.Section4980HTransitionRelief = FCConvert.ToString(rsLoad.Get_Fields_String("Section4980HTransitionRelief"));
				mDetail.TotalEmployeeCount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("TotalEmployeeCount"))));
				collReturn.AddItem(mDetail);
				rsLoad.MoveNext();
			}
			Load1094CMonthlyDetail = collReturn;
			return Load1094CMonthlyDetail;
		}

		private void Save1094CMonthlyDetail(ref cALEMonthlyInformation monthlyDetail, int lngID)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			if (lngID > 0)
			{
				if (!(monthlyDetail == null))
				{
					rsSave.OpenRecordset("select * from aca1094cmonthlydetail where parentid = " + FCConvert.ToString(lngID) + " and coveredmonth = " + monthlyDetail.CoverageMonth, "Payroll");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("ParentID", lngID);
					rsSave.Set_Fields("CoveredMonth", monthlyDetail.CoverageMonth);
					rsSave.Set_Fields("AggregatedGroup", monthlyDetail.AggregatedGroup);
					rsSave.Set_Fields("FullTimeEmployeeCount", monthlyDetail.FullTimeEmployeeCount);
					rsSave.Set_Fields("MinimumEssentialCoverageOffer", monthlyDetail.MinimumEssentialCoverageOffer);
					rsSave.Set_Fields("Section4980HTransitionRelief", monthlyDetail.Section4980HTransitionRelief);
					rsSave.Set_Fields("TotalEmployeeCount", monthlyDetail.TotalEmployeeCount);
					rsSave.Update();
					monthlyDetail.ID = rsSave.Get_Fields("id");
				}
			}
		}

		public c1094C Load1094C(int intYear)
		{
			c1094C Load1094C = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			c1094C ret1094C = new c1094C();
			rsLoad.OpenRecordset("select * from ACA1094C where yearcovered = " + FCConvert.ToString(intYear), "Payroll");
			if (!rsLoad.EndOfFile())
			{
				ret1094C.YearCovered = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("YearCovered"));
				ret1094C.Address = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerAddress"));
				ret1094C.City = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerCity"));
				ret1094C.ContactName = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerContact"));
				ret1094C.ContactLastName = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerContactLast"));
				ret1094C.ContactMiddleName = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerContactMiddle"));
				ret1094C.ContactSuffix = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerContactSuffix"));
				ret1094C.ContactTelephone = FCConvert.ToString(rsLoad.Get_Fields_String("ContactTelephone"));
				ret1094C.DesignatedGovnernmentEntity = FCConvert.ToString(rsLoad.Get_Fields_String("DesignatedEntityName"));
				ret1094C.EIN = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerEIN"));
				ret1094C.EmployerName = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerName"));
				ret1094C.EntityAddress = FCConvert.ToString(rsLoad.Get_Fields("EntityAddress"));
				ret1094C.EntityCity = FCConvert.ToString(rsLoad.Get_Fields_String("EntityCity"));
				ret1094C.EntityContact = FCConvert.ToString(rsLoad.Get_Fields_String("EntityContact"));
				ret1094C.EntityEIN = FCConvert.ToString(rsLoad.Get_Fields_String("DesignatedEntityEIN"));
				ret1094C.EntityPostalCode = FCConvert.ToString(rsLoad.Get_Fields_String("EntityPostalCode"));
				ret1094C.EntityState = FCConvert.ToString(rsLoad.Get_Fields_String("EntityState"));
				ret1094C.EntityTelephone = FCConvert.ToString(rsLoad.Get_Fields_String("EntityContactTelephone"));
				ret1094C.IsAuthoritativeTransmittal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("IsAuthoritative"));
				ret1094C.IsCorrected = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("iscorrected"));
				ret1094C.MemberOfAggregatedGroup = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("IsInAggregatedGroup"));
				ret1094C.NinetyEightPercentOfferMethod = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("NinetyEightPercentOfferMethod"));
				ret1094C.PostalCode = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerPostalCode"));
				ret1094C.QualifyingOfferMethod = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("QualifyingOfferMethod"));
				ret1094C.QualifyingOfferTransitionRelief = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("QualifyingOfferTransitionRelief"));
				ret1094C.Section4980HRelief = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Section4980HRelief"));
				ret1094C.State = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerState"));
				ret1094C.Total1095CForMember = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("TotalMember1095C"));
				ret1094C.Total1095CTransmitted = FCConvert.ToInt16(rsLoad.Get_Fields("TotalForms1095c"));
				ret1094C.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
				cGenericCollection collMembers;
				collMembers = LoadALEMembers(intYear);
				if (!(collMembers == null))
				{
					collMembers.MoveFirst();
					while (collMembers.IsCurrent())
					{
						ret1094C.AggregatedMembers.AddItem(collMembers.GetCurrentItem());
						collMembers.MoveNext();
					}
				}
				cGenericCollection collDetails;
				collDetails = Load1094CMonthlyDetail(ret1094C.ID);
				if (!(collDetails == null))
				{
					cALEMonthlyInformation mDetail;
					collDetails.MoveFirst();
					while (collDetails.IsCurrent())
					{
						mDetail = (cALEMonthlyInformation)collDetails.GetCurrentItem();
						ret1094C.SetMonthlyInformation(mDetail.CoverageMonth, mDetail);
						collDetails.MoveNext();
					}
				}
			}
			ret1094C.IsChanged = false;
			Load1094C = ret1094C;
			return Load1094C;
		}

		public void Save1094C(ref c1094C theReport)
		{
			if (!(theReport == null))
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from ACA1094C where YearCovered = " + theReport.YearCovered, "Payroll");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("YearCovered", theReport.YearCovered);
				rsSave.Set_Fields("EmployerAddress", theReport.Address);
				rsSave.Set_Fields("EmployerCity", theReport.City);
				rsSave.Set_Fields("EmployerContact", theReport.ContactName);
				rsSave.Set_Fields("EmployerContactMiddle", theReport.ContactMiddleName);
				rsSave.Set_Fields("EmployerContactLast", theReport.ContactLastName);
				rsSave.Set_Fields("EmployerContactSuffix", theReport.ContactSuffix);
				rsSave.Set_Fields("ContactTelephone", theReport.ContactTelephone);
				rsSave.Set_Fields("DesignatedEntityName", theReport.DesignatedGovnernmentEntity);
				rsSave.Set_Fields("EmployerEIN", theReport.EIN);
				rsSave.Set_Fields("EmployerName", theReport.EmployerName);
				rsSave.Set_Fields("EntityAddress", theReport.EntityAddress);
				rsSave.Set_Fields("EntityCity", theReport.EntityCity);
				rsSave.Set_Fields("EntityContact", theReport.EntityContact);
				rsSave.Set_Fields("DesignatedEntityEIN", theReport.EntityEIN);
				rsSave.Set_Fields("EntityPostalCode", theReport.EntityPostalCode);
				rsSave.Set_Fields("EntityState", theReport.EntityState);
				rsSave.Set_Fields("EntityContactTelephone", theReport.EntityTelephone);
				rsSave.Set_Fields("IsAuthoritative", theReport.IsAuthoritativeTransmittal);
				rsSave.Set_Fields("IsCorrected", theReport.IsCorrected);
				rsSave.Set_Fields("IsInAggregatedGroup", theReport.MemberOfAggregatedGroup);
				rsSave.Set_Fields("NinetyEightPercentOfferMethod", theReport.NinetyEightPercentOfferMethod);
				rsSave.Set_Fields("EmployerPostalCode", theReport.PostalCode);
				rsSave.Set_Fields("QualifyingOfferMethod", theReport.QualifyingOfferMethod);
				rsSave.Set_Fields("QualifyingOfferTransitionRelief", theReport.QualifyingOfferTransitionRelief);
				rsSave.Set_Fields("Section4980HRelief", theReport.Section4980HRelief);
				rsSave.Set_Fields("EmployerState", theReport.State);
				rsSave.Set_Fields("TotalMember1095C", theReport.Total1095CForMember);
				rsSave.Set_Fields("TotalForms1095C", theReport.Total1095CTransmitted);
				rsSave.Update();
				theReport.ID = rsSave.Get_Fields("id");
				SaveALEMembers(theReport.AggregatedMembers, theReport.YearCovered);
				int x;
				for (x = 1; x <= 12; x++)
				{
					cALEMonthlyInformation temp = theReport.GetMonthlyInformation(x);
					Save1094CMonthlyDetail(ref temp, theReport.ID);
					theReport.SetMonthlyInformation(x, temp);
				}
				// x
				theReport.IsChanged = false;
			}
		}

		public cGenericCollection GetAll1095CProviders()
		{
			cGenericCollection GetAll1095CProviders = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection retList = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCoverageProvider tempProvider;
				rsLoad.OpenRecordset("select * from coverageproviders where not use1095B = 1 order by name", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					tempProvider = new cCoverageProvider();
					tempProvider.PlanName = FCConvert.ToString(rsLoad.Get_Fields_String("PlanName"));
					tempProvider.PlanMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("PlanStartMonth"))));
					tempProvider.Address = FCConvert.ToString(rsLoad.Get_Fields("address"));
					tempProvider.City = FCConvert.ToString(rsLoad.Get_Fields("city"));
					tempProvider.EIN = FCConvert.ToString(rsLoad.Get_Fields("ein"));
					tempProvider.ID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
					tempProvider.Name = FCConvert.ToString(rsLoad.Get_Fields("name"));
					tempProvider.PostalCode = FCConvert.ToString(rsLoad.Get_Fields("postalcode"));
					tempProvider.State = FCConvert.ToString(rsLoad.Get_Fields("state"));
					tempProvider.Telephone = FCConvert.ToString(rsLoad.Get_Fields("telephone"));
					tempProvider.Use1095B = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Use1095B"));
					tempProvider.IsChanged = false;
					retList.AddItem(tempProvider);
					rsLoad.MoveNext();
				}
				GetAll1095CProviders = retList;
				return GetAll1095CProviders;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return GetAll1095CProviders;
		}

		public cGenericCollection GetAll1095BProviders()
		{
			cGenericCollection GetAll1095BProviders = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection retList = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCoverageProvider tempProvider;
				rsLoad.OpenRecordset("select * from coverageproviders where use1095B = 1 order by name", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					tempProvider = new cCoverageProvider();
					tempProvider.PlanName = FCConvert.ToString(rsLoad.Get_Fields_String("PlanName"));
					tempProvider.PlanMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("PlanStartMonth"))));
					tempProvider.Address = FCConvert.ToString(rsLoad.Get_Fields("address"));
					tempProvider.City = FCConvert.ToString(rsLoad.Get_Fields("city"));
					tempProvider.EIN = FCConvert.ToString(rsLoad.Get_Fields("ein"));
					tempProvider.ID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
					tempProvider.Name = FCConvert.ToString(rsLoad.Get_Fields("name"));
					tempProvider.PostalCode = FCConvert.ToString(rsLoad.Get_Fields("postalcode"));
					tempProvider.State = FCConvert.ToString(rsLoad.Get_Fields("state"));
					tempProvider.Telephone = FCConvert.ToString(rsLoad.Get_Fields("telephone"));
					tempProvider.Use1095B = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Use1095B"));
					tempProvider.IsChanged = false;
					retList.AddItem(tempProvider);
					rsLoad.MoveNext();
				}
				GetAll1095BProviders = retList;
				return GetAll1095BProviders;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return GetAll1095BProviders;
		}

		public cGenericCollection GetAll1095CSetups(int intYear)
		{
			cGenericCollection GetAll1095CSetups = null;
			cGenericCollection providerList;
			// Set providerList = GetAll1095CProviders
			providerList = GetAllProviders();
			GetAll1095CSetups = GetSetupsforProviders(ref providerList, intYear);
			return GetAll1095CSetups;
		}

		public cGenericCollection GetSetupsForUncoveredEmployees(int intYear)
		{
			cGenericCollection GetSetupsForUncoveredEmployees = null;
			cGenericCollection providerList = new cGenericCollection();
			cCoverageProvider prov = new cCoverageProvider();
			prov.ID = 0;
			providerList.AddItem(prov);
			GetSetupsForUncoveredEmployees = GetSetupsforProviders(ref providerList, intYear);
			return GetSetupsForUncoveredEmployees;
		}

		public cGenericCollection GetAll1095BSetups(int intYear)
		{
			cGenericCollection GetAll1095BSetups = null;
			cGenericCollection providerList;
			providerList = GetAll1095BProviders();
			GetAll1095BSetups = GetSetupsforProviders(ref providerList, intYear);
			return GetAll1095BSetups;
		}

		private cGenericCollection GetSetupsforProviders(ref cGenericCollection providerList, int intYear)
		{
			cGenericCollection GetSetupsforProviders = null;
			string strWhere;
			string strSQL = "";
			string strOr;
			string strOrderBy = "";
			cCoverageProvider Provider;
			cGenericCollection retList = new cGenericCollection();
			strWhere = "";
			strOr = "";
			providerList.MoveFirst();
			while (providerList.IsCurrent())
			{
				Provider = (cCoverageProvider)providerList.GetCurrentItem();
				strWhere += strOr + " CoverageProviderID = " + FCConvert.ToString(Provider.ID);
				strOr = " or ";
				providerList.MoveNext();
			}
			if (strWhere != "")
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("Select * from tblDefaultInformation", "Payroll");
				cACAEmployeeSetup tSetup;
				strOrderBy = "FirstName, MiddleName, LastName";
				// Select Case Val(rsLoad.Fields("ReportSequence"))
				// Case 0
				// employee name
				// strOrderBy = "LastName,FirstName,MiddleName"
				// Case 1
				// employee number
				// strOrderBy = "acaemployeesetup.EmployeeNumber"
				// Case 2
				// seq
				// strOrderBy = "seqnumber,lastname,firstname"
				// Case 3
				// strOrderBy = "deptdiv,lastname,firstname"
				// End Select
				strSQL = "Select acaemployeesetup.* from acaemployeesetup inner join tblemployeemaster on (tblemployeemaster.id = acaemployeesetup.employeeid) where reportyear = " + FCConvert.ToString(intYear) + " and (" + strWhere + ") order by " + strOrderBy;
				rsLoad.OpenRecordset(strSQL, "Payroll");
				while (!rsLoad.EndOfFile())
				{
					tSetup = new cACAEmployeeSetup();
					FillSetup(ref rsLoad, ref tSetup);
					retList.AddItem(tSetup);
					rsLoad.MoveNext();
				}
			}
			GetSetupsforProviders = retList;
			return GetSetupsforProviders;
		}

		public c1094B Load1094b(int intYear)
		{
			c1094B Load1094b = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			c1094B ret1094b = new c1094B();
			rsLoad.OpenRecordset("select * from ACA1094B where yearcovered = " + FCConvert.ToString(intYear), "Payroll");
			if (!rsLoad.EndOfFile())
			{
				ret1094b.YearCovered = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("YearCovered"));
				ret1094b.Address = FCConvert.ToString(rsLoad.Get_Fields_String("Address"));
				ret1094b.City = FCConvert.ToString(rsLoad.Get_Fields_String("City"));
				ret1094b.ContactName = FCConvert.ToString(rsLoad.Get_Fields_String("ContactName"));
				ret1094b.ContactLastName = FCConvert.ToString(rsLoad.Get_Fields_String("ContactLastName"));
				ret1094b.ContactMiddleName = FCConvert.ToString(rsLoad.Get_Fields_String("ContactMiddleName"));
				ret1094b.ContactSuffix = FCConvert.ToString(rsLoad.Get_Fields_String("ContactSuffix"));
				ret1094b.ContactPhone = FCConvert.ToString(rsLoad.Get_Fields_String("ContactTelephone"));
				ret1094b.EIN = FCConvert.ToString(rsLoad.Get_Fields_String("EIN"));
				ret1094b.FilerName = FCConvert.ToString(rsLoad.Get_Fields_String("FilerName"));
				ret1094b.PostalCode = FCConvert.ToString(rsLoad.Get_Fields_String("PostalCode"));
				ret1094b.State = FCConvert.ToString(rsLoad.Get_Fields("State"));
				ret1094b.NumberOfSubmissions = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("NumberOfSubmissions"));
				ret1094b.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
			}
			ret1094b.IsChanged = false;
			Load1094b = ret1094b;
			return Load1094b;
		}

		public void Save1094B(ref c1094B theReport)
		{
			if (!(theReport == null))
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from ACA1094B where YearCovered = " + theReport.YearCovered, "Payroll");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("YearCovered", theReport.YearCovered);
				rsSave.Set_Fields("Address", theReport.Address);
				rsSave.Set_Fields("City", theReport.City);
				rsSave.Set_Fields("ContactName", theReport.ContactName);
				rsSave.Set_Fields("ContactMiddleName", theReport.ContactMiddleName);
				rsSave.Set_Fields("ContactLastName", theReport.ContactLastName);
				rsSave.Set_Fields("ContactSuffix", theReport.ContactSuffix);
				rsSave.Set_Fields("ContactTelephone", theReport.ContactPhone);
				rsSave.Set_Fields("EIN", theReport.EIN);
				rsSave.Set_Fields("FilerName", theReport.FilerName);
				rsSave.Set_Fields("PostalCode", theReport.PostalCode);
				rsSave.Set_Fields("State", theReport.State);
				rsSave.Set_Fields("NumberOfSubMissions", theReport.NumberOfSubmissions);
				rsSave.Update();
				theReport.ID = rsSave.Get_Fields("id");
				theReport.IsChanged = false;
			}
		}

		public cACABXMLReport LoadACABXMLReport(int intYear)
		{
			cACABXMLReport LoadACABXMLReport = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			cEmployee empRecord;
			cEmployeeService employeeService = new cEmployeeService();
			cEmployerService employerService = new cEmployerService();
			cCoverageProvider Provider;
			cGenericCollection providers;
			cEmployerRecord Employer;
			providers = GetAllProviders();
			theReport.ReportYear = intYear;
			headerbase = Load1094b(intYear);
			header.BaseInfo = headerbase;
			Employer = employerService.GetEmployerInfo();
			header.Employer.Address = Employer.Address;
			header.Employer.City = Employer.City;
			header.Employer.EIN = Employer.EIN;
			header.Employer.Name = Employer.Name;
			header.Employer.State = Employer.State;
			header.Employer.Telephone = Employer.Telephone;
			header.Employer.Zip = Employer.Zip;
			header.Employer.Zip4 = Employer.Zip4;
			emps = GetAll1095BSetups(intYear);
			emps.MoveFirst();
			int lngCount;
			lngCount = 0;
			while (emps.IsCurrent())
			{
				//App.DoEvents();
				lngCount += 1;
				empXML = new c1095BXMLRecord();
				empXML.RecordID = lngCount;
				// empXML.BaseInfo = emps.GetCurrentItem
				empXML.BaseInfo = new c1095B();
				empXML.BaseInfo.ACAEmployee = (cACAEmployeeSetup)emps.GetCurrentItem();
				empRecord = employeeService.GetEmployeeByID(empXML.BaseInfo.ACAEmployee.EmployeeID);
				empXML.Employee.Address1 = empRecord.Address1;
				empXML.Employee.City = empRecord.City;
				empXML.Employee.State = empRecord.State;
				empXML.Employee.Zip = empRecord.Zip;
				empXML.Employee.Zip4 = empRecord.Zip4;
				empXML.Employee.FirstName = empRecord.FirstName;
				empXML.Employee.MiddleName = empRecord.MiddleName;
				empXML.Employee.LastName = empRecord.LastName;
				empXML.Employee.Designation = empRecord.Designation;
				empXML.Employee.Phone = empRecord.Phone;
				empXML.Employee.SSN = empRecord.SSN;
				empXML.Employee.BirthDate = empRecord.BirthDate;
				empXML.BaseInfo.Address = empRecord.Address1;
				empXML.BaseInfo.City = empRecord.City;
				empXML.BaseInfo.State = empRecord.State;
				empXML.BaseInfo.PostalCode = empRecord.Zip;
				if (fecherFoundation.Strings.Trim(empRecord.Zip4) != "")
				{
					empXML.BaseInfo.PostalCode = empXML.BaseInfo.PostalCode + " " + empRecord.Zip4;
				}
				empXML.BaseInfo.EmployeeName = empRecord.FullName;
				empXML.BaseInfo.SSN = empRecord.SSN;
				if (empXML.BaseInfo.ACAEmployee.CoverageProviderID > 0)
				{
					providers.MoveFirst();
					while (providers.IsCurrent())
					{
						//App.DoEvents();
						Provider = (cCoverageProvider)providers.GetCurrentItem();
						if (Provider.ID == empXML.BaseInfo.ACAEmployee.CoverageProviderID)
						{
							empXML.BaseInfo.CoverageProvider = Provider;
							break;
						}
						providers.MoveNext();
					}
				}
				header.Details.AddItem(empXML);
				emps.MoveNext();
			}
			theReport.List1094s.AddItem(header);
			FillBManifestRecord(ref theReport);
			LoadACABXMLReport = theReport;
			return LoadACABXMLReport;
		}

		public cACACXMLReport LoadACACXMLReport(int intYear)
		{
			cACACXMLReport LoadACACXMLReport = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			cEmployee empRecord;
			cEmployeeService employeeService = new cEmployeeService();
			cEmployerService employerService = new cEmployerService();
			cCoverageProvider Provider;
			cGenericCollection providers;
			cEmployerRecord Employer;
			providers = GetAllProviders();
			theReport.ReportYear = intYear;
			headerbase = Load1094C(intYear);
			header.BaseInfo = headerbase;
			Employer = employerService.GetEmployerInfo();
			// header.employer.Address = employer.Address
			// header.employer.City = employer.City
			// header.employer.EIN = employer.EIN
			// header.employer.Name = employer.Name
			// header.employer.State = employer.State
			// header.employer.Telephone = employer.Telephone
			// header.employer.Zip = employer.Zip
			// header.employer.Zip4 = employer.Zip4
			emps = GetAll1095CSetups(intYear);
			emps.MoveFirst();
			int lngCount;
			lngCount = 0;
			while (emps.IsCurrent())
			{
				lngCount += 1;
				empXML = new c1095CXMLRecord();
				empXML.RecordID = lngCount;
				empXML.BaseInfo = new c1095C();
				empXML.BaseInfo.ACAEmployee = (cACAEmployeeSetup)emps.GetCurrentItem();
				empRecord = employeeService.GetEmployeeByID(empXML.BaseInfo.ACAEmployee.EmployeeID);
				empXML.Employee.Address1 = empRecord.Address1;
				empXML.Employee.City = empRecord.City;
				empXML.Employee.State = empRecord.State;
				empXML.Employee.Zip = empRecord.Zip;
				empXML.Employee.Zip4 = empRecord.Zip4;
				empXML.Employee.FirstName = empRecord.FirstName;
				empXML.Employee.MiddleName = empRecord.MiddleName;
				empXML.Employee.LastName = empRecord.LastName;
				empXML.Employee.Designation = empRecord.Designation;
				empXML.Employee.Phone = empRecord.Phone;
				empXML.Employee.SSN = empRecord.SSN;
				empXML.Employee.BirthDate = empRecord.BirthDate;
				empXML.BaseInfo.Address = empRecord.Address1;
				empXML.BaseInfo.City = empRecord.City;
				empXML.BaseInfo.State = empRecord.State;
				empXML.BaseInfo.PostalCode = empRecord.Zip;
				if (fecherFoundation.Strings.Trim(empRecord.Zip4) != "")
				{
					empXML.BaseInfo.PostalCode = empXML.BaseInfo.PostalCode + " " + empRecord.Zip4;
				}
				empXML.BaseInfo.EmployeeName = empRecord.FullName;
				empXML.BaseInfo.SSN = empRecord.SSN;
				if (empXML.BaseInfo.ACAEmployee.CoverageProviderID > 0)
				{
					providers.MoveFirst();
					while (providers.IsCurrent())
					{
						//App.DoEvents();
						Provider = (cCoverageProvider)providers.GetCurrentItem();
						if (Provider.ID == empXML.BaseInfo.ACAEmployee.CoverageProviderID)
						{
							empXML.BaseInfo.CoverageProvider = Provider;
							break;
						}
						providers.MoveNext();
					}
				}
				header.Details.AddItem(empXML);
				emps.MoveNext();
			}
			theReport.List1094s.AddItem(header);
			FillCManifestRecord(ref theReport);
			LoadACACXMLReport = theReport;
			return LoadACACXMLReport;
		}

		public cACABXMLReport GetTestScenario8(int intYear)
		{
			cACABXMLReport GetTestScenario8 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario8(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario8 = theReport;
			return GetTestScenario8;
		}

		public cACABXMLReport GetTestScenario8C(int intYear)
		{
			cACABXMLReport GetTestScenario8C = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario8C(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario8C = theReport;
			return GetTestScenario8C;
		}

		public cACABXMLReport GetTestScenario6(int intYear)
		{
			cACABXMLReport GetTestScenario6 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario6(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario6 = theReport;
			return GetTestScenario6;
		}

		public cACABXMLReport GetTestScenario6C(int intYear)
		{
			cACABXMLReport GetTestScenario6C = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario6C(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario6C = theReport;
			return GetTestScenario6C;
		}

		public cACABXMLReport GetTestScenario2(int intYear)
		{
			cACABXMLReport GetTestScenario2 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario2(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario2 = theReport;
			return GetTestScenario2;
		}

		public cACABXMLReport GetTestScenario2C(int intYear)
		{
			cACABXMLReport GetTestScenario2C = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario2C(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario2C = theReport;
			return GetTestScenario2C;
		}

		public cACABXMLReport GetTestScenario1(int intYear)
		{
			cACABXMLReport GetTestScenario1 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACABXMLReport theReport;
			theReport = testcont.GetTestScenario1(intYear);
			FillBManifestRecord(ref theReport);
			GetTestScenario1 = theReport;
			return GetTestScenario1;
		}

		public cACACXMLReport GetTestScenario3(int intYear)
		{
			cACACXMLReport GetTestScenario3 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario3(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario3 = theReport;
			return GetTestScenario3;
		}

		public cACACXMLReport GetTestScenario4C(int intYear)
		{
			cACACXMLReport GetTestScenario4C = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario4C(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario4C = theReport;
			return GetTestScenario4C;
		}

		public cACACXMLReport GetTestScenario4(int intYear)
		{
			cACACXMLReport GetTestScenario4 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario4(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario4 = theReport;
			return GetTestScenario4;
		}

		public cACACXMLReport GetTestScenario5C(int intYear)
		{
			cACACXMLReport GetTestScenario5C = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario5C(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario5C = theReport;
			return GetTestScenario5C;
		}

		public cACACXMLReport GetTestScenario5(int intYear)
		{
			cACACXMLReport GetTestScenario5 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario5(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario5 = theReport;
			return GetTestScenario5;
		}

		public cACACXMLReport GetTestScenario7C(int intYear)
		{
			cACACXMLReport GetTestScenario7C = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario7C(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario7C = theReport;
			return GetTestScenario7C;
		}

		public cACACXMLReport GetTestScenario7(int intYear)
		{
			cACACXMLReport GetTestScenario7 = null;
			IcACATestController testcont;
			testcont = GetNewACATestController(intYear);
			cACACXMLReport theReport;
			theReport = testcont.GetTestScenario7(intYear);
			FillCManifestRecord(ref theReport);
			GetTestScenario7 = theReport;
			return GetTestScenario7;
		}

		private void FillCManifestRecord(ref cACACXMLReport theReport)
		{
			if (!(theReport == null))
			{
				theReport.Manifest.PaymentYear = theReport.ReportYear;
				theReport.Manifest.TotalPayerRecordCount = theReport.List1094s.ItemCount();
				if (theReport.IsCorrection)
				{
					theReport.Manifest.TransmissionTypeCode = "C";
				}
				else
				{
					theReport.Manifest.TransmissionTypeCode = "O";
				}

				theReport.Manifest.SoftwareID = "20A0011630";
				theReport.Manifest.VendorCode = "V";
				theReport.Manifest.IsPriorYearData = false;
				if (theReport.ReportYear < CNSTLatestTaxYear)
				{
					theReport.Manifest.IsPriorYearData = true;
				}
				theReport.List1094s.MoveFirst();
				c1094CXMLRecord rec = new c1094CXMLRecord();
				int lngPayees = 0;
				lngPayees = 0;
				while (theReport.List1094s.IsCurrent())
				{
					//App.DoEvents();
					rec = (c1094CXMLRecord)theReport.List1094s.GetCurrentItem();
					lngPayees += rec.Details.ItemCount();
					theReport.List1094s.MoveNext();
				}
				theReport.Manifest.TotalPayeeCount = lngPayees;
				theReport.Manifest.FormType = "1094/1095C";
				theReport.Manifest.BinaryFormatCode = "application/xml";
				theReport.ManifestName = "manifest_" + theReport.FileName;
				theReport.Manifest.VendorContactFirstName = CNSTVendorContactFirstName;
				theReport.Manifest.VendorContactLastName = CNSTVendorContactLastName;
				theReport.Manifest.VendorContactPhone = CNSTVendorContactPhone;
				theReport.Manifest.CompanyAddress = rec.BaseInfo.Address;
				theReport.Manifest.CompanyCity = rec.BaseInfo.City;
				theReport.Manifest.CompanyName = rec.BaseInfo.EmployerName;
				theReport.Manifest.TransmitterName = rec.BaseInfo.EmployerName;
				theReport.Manifest.CompanyPostalCode = rec.BaseInfo.PostalCode;
				theReport.Manifest.CompanyState = rec.BaseInfo.State;
				theReport.Manifest.ContactFirstName = rec.BaseInfo.ContactName;
				theReport.Manifest.ContactLastName = rec.BaseInfo.ContactLastName;
				theReport.Manifest.ContactMiddleName = rec.BaseInfo.ContactMiddleName;
				theReport.Manifest.ContactPhone = rec.BaseInfo.ContactTelephone;
				theReport.Manifest.ContactSuffix = rec.BaseInfo.ContactSuffix;
				theReport.Manifest.EIN = rec.BaseInfo.EIN;
				if (rec.ScenarioID != "")
				{
					if (false)
					{
						theReport.Manifest.TestFileCode = "T";
					}
				}
			}
		}

		private void FillBManifestRecord(ref cACABXMLReport theReport)
		{
			if (!(theReport == null))
			{
				theReport.Manifest.PaymentYear = theReport.ReportYear;
				theReport.Manifest.TotalPayerRecordCount = theReport.List1094s.ItemCount();
				if (theReport.IsCorrection)
				{
					theReport.Manifest.TransmissionTypeCode = "C";
				}
				else
				{
					theReport.Manifest.TransmissionTypeCode = "O";
					// o,c or r
				}
				theReport.Manifest.SoftwareID = "20A0011629";
				theReport.Manifest.VendorCode = "V";
				theReport.Manifest.IsPriorYearData = false;
				if (theReport.ReportYear < CNSTLatestTaxYear)
				{
					theReport.Manifest.IsPriorYearData = true;
				}
				theReport.List1094s.MoveFirst();
				c1094BXMLRecord rec = new c1094BXMLRecord();
				int lngPayees = 0;
				lngPayees = 0;
				while (theReport.List1094s.IsCurrent())
				{
					//App.DoEvents();
					rec = (c1094BXMLRecord)theReport.List1094s.GetCurrentItem();
					lngPayees += rec.Details.ItemCount();
					theReport.List1094s.MoveNext();
				}
				theReport.Manifest.TotalPayeeCount = lngPayees;
				theReport.Manifest.FormType = "1094/1095B";
				theReport.Manifest.BinaryFormatCode = "application/xml";
				theReport.ManifestName = "manifest_" + theReport.FileName;
				theReport.Manifest.VendorContactFirstName = CNSTVendorContactFirstName;
				theReport.Manifest.VendorContactLastName = CNSTVendorContactLastName;
				theReport.Manifest.VendorContactPhone = CNSTVendorContactPhone;
				theReport.Manifest.CompanyAddress = rec.BaseInfo.Address;
				theReport.Manifest.CompanyCity = rec.BaseInfo.City;
				theReport.Manifest.CompanyName = rec.BaseInfo.FilerName;
				theReport.Manifest.TransmitterName = rec.BaseInfo.FilerName;
				theReport.Manifest.CompanyPostalCode = rec.BaseInfo.PostalCode;
				theReport.Manifest.CompanyState = rec.BaseInfo.State;
				theReport.Manifest.ContactFirstName = rec.BaseInfo.ContactName;
				theReport.Manifest.ContactLastName = rec.BaseInfo.ContactLastName;
				theReport.Manifest.ContactMiddleName = rec.BaseInfo.ContactMiddleName;
				theReport.Manifest.ContactPhone = rec.BaseInfo.ContactPhone;
				theReport.Manifest.ContactSuffix = rec.BaseInfo.ContactSuffix;
				theReport.Manifest.EIN = rec.BaseInfo.EIN;
				if (rec.ScenarioID != "")
				{
					if (false)
					{
						theReport.Manifest.TestFileCode = "T";
					}
				}
			}
		}

		public void SaveACABXMLReport(ref cACABXMLReport theReport, string strPath)
		{
			ClearErrors();
			try
			{
				//FC:FINAL:DSE:#i2317 Files should be downloaded as a zip file
				//string zipFilePath = strPath + Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".zip";
				//if (File.Exists(zipFilePath))
				//{
				//	File.Delete(zipFilePath);
				//}
				//var zip = fecherFoundation.ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);

				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCFileSystem fso = new FCFileSystem();
				StreamWriter ts;
				if (strPath != "")
				{
					if (Strings.Right(strPath, 1) != "/" && Strings.Right(strPath, 1) != "\\")
					{
						strPath += "\\";
					}
				}
                if (!FCFileSystem.DirectoryExists(strPath))
                {
                    FCFileSystem.CreateDirectory(strPath);                    
                }
				ts = FCFileSystem.CreateTextFile(strPath + theReport.FileName);
				string strReturn;
				c1095BXMLController xmlController;
				xmlController = GetNew1095BXMLController(theReport.ReportYear);
				strReturn = xmlController.ACABXMLReportToXML(ref theReport);
				ts.Write(strReturn);
				ts.Close();
				//FC:FINAL:DSE:#i2317 Files should be downloaded inside a zip file
				//zip.CreateEntryFromFile(strPath + theReport.FileName, theReport.FileName, System.IO.Compression.CompressionLevel.Optimal);
				cCryptHelper cryptUtil = new cCryptHelper();
				string strHash;
				strHash = FCConvert.ToString(cryptUtil.GetSHA256HashOfFile(strPath + theReport.FileName));
				if (strHash == "" && cryptUtil.LastErrorNumber != 0)
				{
					fecherFoundation.Information.Err().Raise(cryptUtil.LastErrorNumber, "", cryptUtil.LastErrorDescription, null, null);
				}
				theReport.Manifest.CheckSum = strHash;
				theReport.Manifest.AttachmentSize = strReturn.Length;
				strReturn = xmlController.ACABXMLReportToManifestXML(ref theReport);
				ts = FCFileSystem.CreateTextFile(strPath + theReport.ManifestName);
				ts.Write(strReturn);
				ts.Close();
				//FC:FINAL:DSE:#i2317 Files should be downloaded inside a zip file
				//zip.CreateEntryFromFile(strPath + theReport.ManifestName, theReport.ManifestName, System.IO.Compression.CompressionLevel.Optimal);
				cACATransmission aTrans = new cACATransmission();
				cACATransmissionController tranCon = new cACATransmissionController();
				aTrans = ACATransmissionFromACABXMLReport(ref theReport);
				tranCon.SaveACATransmission(ref aTrans);
				if (FCConvert.ToBoolean(tranCon.HadError))
				{
					fecherFoundation.Information.Err().Raise(tranCon.LastErrorNumber, "", tranCon.LastErrorMessage, null, null);
				}
				//FC:FINAL:DSE:#i2317 Files should be downloaded inside a zip file
				//zip.Dispose();
				//FCUtils.Download(zipFilePath, Path.GetFileNameWithoutExtension(theReport.FileName) + ".zip");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

       

		public void SaveACACXMLReport(ref cACACXMLReport theReport, string strPath)
		{
			ClearErrors();
			try
			{
				//FC:FINAL:DSE:#i2317 Files should be downloaded as a zip file
				//string zipFilePath = strPath + Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".zip";
				//if (File.Exists(zipFilePath))
				//{
				//	File.Delete(zipFilePath);
				//}
				//var zip = ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCFileSystem fso = new FCFileSystem();
				StreamWriter ts;
				if (strPath != "")
				{
					if (Strings.Right(strPath, 1) != "/" && Strings.Right(strPath, 1) != "\\")
					{
						strPath += "\\";
					}
				}
				ts = FCFileSystem.CreateTextFile(strPath + theReport.FileName);
				string strReturn;
				c1095CXMLController xmlController;
				xmlController = GetNew1095CXMLController(theReport.ReportYear);
				strReturn = xmlController.ACACXMLReportToXML(ref theReport);
				ts.Write(strReturn);
                ts.Flush();
				ts.Close();
				//FC:FINAL:DSE:#i2317 Files should be downloaded inside a zip file
				//zip.CreateEntryFromFile(strPath + theReport.FileName, theReport.FileName, System.IO.Compression.CompressionLevel.Optimal);
				cCryptHelper cryptUtil = new cCryptHelper();
				string strHash;
				strHash = FCConvert.ToString(cryptUtil.GetSHA256HashOfFile(strPath + theReport.FileName));
				if (strHash == "" && cryptUtil.LastErrorNumber != 0)
				{
					fecherFoundation.Information.Err().Raise(cryptUtil.LastErrorNumber, "", cryptUtil.LastErrorDescription, null, null);
				}
				theReport.Manifest.checkSum = strHash;
				theReport.Manifest.AttachmentSize = strReturn.Length;
				strReturn = xmlController.ACACXMLReportToManifestXML(theReport);
				ts = FCFileSystem.CreateTextFile(strPath + theReport.ManifestName);
				ts.Write(strReturn);
				ts.Close();
				//FC:FINAL:DSE:#i2317 Files should be downloaded inside a zip file
				//zip.CreateEntryFromFile(strPath + theReport.ManifestName, theReport.ManifestName, System.IO.Compression.CompressionLevel.Optimal);
				cACATransmission aTrans = new cACATransmission();
				cACATransmissionController tranCon = new cACATransmissionController();
				aTrans = ACATransmissionFromACACXMLReport(ref theReport);
				tranCon.SaveACATransmission(ref aTrans);
				if (FCConvert.ToBoolean(tranCon.HadError))
				{
					fecherFoundation.Information.Err().Raise(tranCon.LastErrorNumber, "", tranCon.LastErrorMessage, null, null);
				}
				//FC:FINAL:DSE:#i2317 Files should be downloaded inside a zip file
				//zip.Dispose();
				//FCUtils.Download(zipFilePath, Path.GetFileNameWithoutExtension(theReport.FileName) + ".zip");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		private cACATransmission ACATransmissionFromACABXMLReport(ref cACABXMLReport theReport)
		{
			cACATransmission ACATransmissionFromACABXMLReport = null;
			cACATransmission aTrans = new cACATransmission();
			aTrans.FileName = theReport.Manifest.RequestFileName;
			aTrans.ReceiptID = "";
			aTrans.TaxYear = theReport.ReportYear;
			aTrans.TestFileCode = theReport.Manifest.TestFileCode;
			aTrans.TransmissionID = theReport.Manifest.TransmissionID;
			aTrans.TransmissionType = theReport.Manifest.TransmissionTypeCode;
			theReport.List1094s.MoveFirst();
			c1094BXMLRecord rec;
			cACATransmissionDetail tranDetail;
			c1095BXMLRecord recDetail;
			while (theReport.List1094s.IsCurrent())
			{
				rec = (c1094BXMLRecord)theReport.List1094s.GetCurrentItem();
				rec.Details.MoveFirst();
				while (rec.Details.IsCurrent())
				{
					//App.DoEvents();
					recDetail = (c1095BXMLRecord)rec.Details.GetCurrentItem();
					tranDetail = new cACATransmissionDetail();
					tranDetail.ACASetupID = recDetail.BaseInfo.ACAEmployee.ID;
					tranDetail.RecordID = recDetail.RecordID;
					aTrans.Details.AddItem(tranDetail);
					rec.Details.MoveNext();
				}
				theReport.List1094s.MoveNext();
			}
			ACATransmissionFromACABXMLReport = aTrans;
			return ACATransmissionFromACABXMLReport;
		}

		private cACATransmission ACATransmissionFromACACXMLReport(ref cACACXMLReport theReport)
		{
			cACATransmission ACATransmissionFromACACXMLReport = null;
			cACATransmission aTrans = new cACATransmission();
			aTrans.FileName = theReport.Manifest.RequestFileName;
			aTrans.ReceiptID = "";
			aTrans.TaxYear = theReport.ReportYear;
			aTrans.TestFileCode = theReport.Manifest.TestFileCode;
			aTrans.TransmissionID = theReport.Manifest.TransmissionID;
			aTrans.TransmissionType = theReport.Manifest.TransmissionTypeCode;
			theReport.List1094s.MoveFirst();
			c1094CXMLRecord rec;
			cACATransmissionDetail tranDetail;
			c1095CXMLRecord recDetail;
			while (theReport.List1094s.IsCurrent())
			{
				rec = (c1094CXMLRecord)theReport.List1094s.GetCurrentItem();
				rec.Details.MoveFirst();
				while (rec.Details.IsCurrent())
				{
					//App.DoEvents();
					recDetail = (c1095CXMLRecord)rec.Details.GetCurrentItem();
					tranDetail = new cACATransmissionDetail();
					tranDetail.ACASetupID = recDetail.BaseInfo.ACAEmployee.ID;
					tranDetail.RecordID = recDetail.RecordID;
					aTrans.Details.AddItem(tranDetail);
					rec.Details.MoveNext();
				}
				theReport.List1094s.MoveNext();
			}
			ACATransmissionFromACACXMLReport = aTrans;
			return ACATransmissionFromACACXMLReport;
		}

		public c1095BXMLController GetNew1095BXMLController(int intYear)
		{
			// vbPorter upgrade warning: theCont As c1095BXMLController	OnWrite(c1095B2016XMLController, c1095B2017XMLController, c1095BXMLController)
			c1095BXMLController theCont;
			switch (intYear)
			{
				case 2020:
					theCont = new c1095B2020XMLController();
                    break;
				case 2019:
					theCont = new c1095B2019XMLController();
                    break;
				case 2018:
					{
						theCont = new c1095B2018XMLController();
						break;
					}
				case 2017:
					{
						theCont = new c1095B2017XMLController();
						break;
					}
				default:
					{
						theCont = new c1095B2020XMLController();
						break;
					}
			}
			//end switch
			theCont.SetManifestController(new c1095BXMLManifestController());
			return theCont;
		}

		public c1095CXMLController GetNew1095CXMLController(int intYear)
		{
			c1095CXMLController GetNew1095CXMLController = null;
			// vbPorter upgrade warning: theCont As c1095CXMLController	OnWrite(c1095C2016XMLController, c1095C2017XMLController, c1095CXMLController)
			c1095CXMLController theCont;
			switch (intYear)
			{
				case 2020:
					theCont = new c1095C2020XMLController();
                    break;
				case 2019:
					theCont = new c1095C2019XMLController();
                    break;
				case 2018:
					{
						theCont = new c1095C2018XMLController();
						break;
					}
				case 2017:
					{
						theCont = new c1095C2017XMLController();
						break;
					}
				default:
					{
                        theCont = new c1095C2020XMLController();
						break;
					}
			}
			//end switch
			theCont.SetManifestController(new c1095CXMLManifestController());
			GetNew1095CXMLController = theCont;
			return GetNew1095CXMLController;
		}

		public IcACATestController GetNewACATestController(int intYear)
		{
			IcACATestController GetNewACATestController = null;
			// vbPorter upgrade warning: theCont As cACA2017TestController	OnWrite(cACA2016TestController, cACA2017TestController, cACATestController)
			IcACATestController theCont;
			switch (intYear)
			{
				case 2020:
					theCont = new cACA2020TestController();
                    break;
				case 2019:
					theCont = new cACA2019TestController();
                    break;
				case 2018:
					{
						theCont = new cACA2018TestController();
						break;
					}
				case 2017:
					{
						theCont = new cACA2017TestController();
						break;
					}
				default:
					{
						theCont = new cACA2020TestController();
						break;
					}
			}
			//end switch
			GetNewACATestController = theCont;
			return GetNewACATestController;
		}
	}
}
