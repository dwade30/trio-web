﻿//Fecher vbPorter - Version 1.0.0.59
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class cACACorrectionView
	{
		//=========================================================
		private int lngLastError;
		private string strLastError = "";
		private cGenericCollection transmissionsList = new cGenericCollection();
		private int lngCurrentTransmissionID;
		private cACATransmission currTransmission;
		private cACAService acaServ = new cACAService();
		private cACATransmissionController tranControl = new cACATransmissionController();
		// Private contBXML As New c1095BXMLController
		public delegate void TransmissionsUpdatedEventHandler();

		public event TransmissionsUpdatedEventHandler TransmissionsUpdated;

		public delegate void CurrentTransmissionChangedEventHandler(int lngID);

		public event CurrentTransmissionChangedEventHandler CurrentTransmissionChanged;

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public string LastErrorDescription
		{
			get
			{
				string LastErrorDescription = "";
				LastErrorDescription = strLastError;
				return LastErrorDescription;
			}
		}

		public void ClearErrors()
		{
			lngLastError = 0;
			strLastError = "";
		}

		private void SetError(int lngError, string strErrorMessage)
		{
			lngLastError = lngError;
			strLastError = strErrorMessage;
		}

		public cGenericCollection transmissions
		{
			get
			{
				cGenericCollection transmissions = null;
				transmissions = transmissionsList;
				return transmissions;
			}
		}

		public cACATransmission currentTransmission
		{
			get
			{
				cACATransmission currentTransmission = null;
				currentTransmission = currTransmission;
				return currentTransmission;
			}
		}

		private void LoadTransmission()
		{
			if (lngCurrentTransmissionID > 0)
			{
				transmissionsList.MoveFirst();
				cACATransmission tran;
				while (transmissionsList.IsCurrent())
				{
					//App.DoEvents();
					tran = (cACATransmission)transmissionsList.GetCurrentItem();
					if (tran.ID == lngCurrentTransmissionID)
					{
						currTransmission = tran;
						break;
					}
					transmissionsList.MoveNext();
				}
			}
			else
			{
				currTransmission = null;
			}
		}

		public void SetCurrentTransmissionID(int lngID)
		{
			lngCurrentTransmissionID = lngID;
			LoadTransmission();
			if (this.CurrentTransmissionChanged != null)
				this.CurrentTransmissionChanged(lngID);
		}

		public void Load()
		{
			transmissionsList = tranControl.GetFullACATransmissions();
			if (this.TransmissionsUpdated != null)
				this.TransmissionsUpdated();
		}
	}
}
