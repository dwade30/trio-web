﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class c1095C
	{
		//=========================================================
		private string strEmployeeName = string.Empty;
		private string strEmployeeFirstName = string.Empty;
		private string strEmployeeMiddleName = string.Empty;
		private string strEmployeeLastName = string.Empty;
		private string strSSN = string.Empty;
		private string strAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strPostalCode = string.Empty;
		private bool boolVoid;
		private bool boolCorrected;
		private cACAEmployeeSetup EmployeeSetup;
		private int intPlanStartMonth;
		private cCoverageProvider planProvider;

		public cCoverageProvider CoverageProvider
		{
			set
			{
				planProvider = value;
			}
			get
			{
				cCoverageProvider CoverageProvider = null;
				CoverageProvider = planProvider;
				return CoverageProvider;
			}
		}

		public int PlanStartMonth
		{
			set
			{
				intPlanStartMonth = value;
			}
			get
			{
				int PlanStartMonth = 0;
				PlanStartMonth = intPlanStartMonth;
				return PlanStartMonth;
			}
		}

		public bool SafeHarborCodeSameAll12Months
		{
			get
			{
				bool SafeHarborCodeSameAll12Months = false;
				SafeHarborCodeSameAll12Months = EmployeeSetup.SafeHarborCodeSameAll12Months;
				return SafeHarborCodeSameAll12Months;
			}
		}

		public string SafeHarborCodeAll12Months
		{
			get
			{
				string SafeHarborCodeAll12Months = "";
				SafeHarborCodeAll12Months = EmployeeSetup.SafeHarborCodeAll12Months;
				return SafeHarborCodeAll12Months;
			}
		}

        public bool ZipCodeSameAll12Months
        {
            get
            {
                return EmployeeSetup.ZipCodeSameAll12Months;
            }         
        }

        public string ZipCodeAll12Months
        {
            get
            {
                return EmployeeSetup.ZipCodeAll12Months;
            }
        }
        // vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToInt32(
        public int ReportYear
		{
			get
			{
				int ReportYear = 0;
				ReportYear = EmployeeSetup.ReportYear;
				return ReportYear;
			}
		}
		// vbPorter upgrade warning: intMonth As object	OnWrite
		public string GetSafeHarborCodeForMonth(int intMonth)
		{
			string GetSafeHarborCodeForMonth = "";
			GetSafeHarborCodeForMonth = EmployeeSetup.GetSafeHarborCodeForMonth(intMonth);
			return GetSafeHarborCodeForMonth;
		}
        public string GetZipCodeForMonth(int month)
        {
            return EmployeeSetup.GetZipCodeForMonth(month);
        }
		// vbPorter upgrade warning: intMonth As object	OnWrite
		public double GetEmployeeShareLowestPremiumForMonth(int intMonth)
		{
			double GetEmployeeShareLowestPremiumForMonth = 0;
			GetEmployeeShareLowestPremiumForMonth = EmployeeSetup.GetEmployeeShareLowestPremiumForMonth(intMonth);
			return GetEmployeeShareLowestPremiumForMonth;
		}

		public string GetCoverageRequiredCodeForMonth(int intMonth)
		{
			string GetCoverageRequiredCodeForMonth = "";
			GetCoverageRequiredCodeForMonth = EmployeeSetup.GetCoverageRequiredCodeForMonth(intMonth);
			return GetCoverageRequiredCodeForMonth;
		}

		public double EmployeeShareLowestPremiumAll12Months
		{
			get
			{
				double EmployeeShareLowestPremiumAll12Months = 0;
				EmployeeShareLowestPremiumAll12Months = EmployeeSetup.EmployeeShareLowestPremiumAll12Months;
				return EmployeeShareLowestPremiumAll12Months;
			}
		}

		public bool EmployeeShareLowestPremiumSameAll12Months
		{
			get
			{
				bool EmployeeShareLowestPremiumSameAll12Months = false;
				EmployeeShareLowestPremiumSameAll12Months = EmployeeSetup.EmployeeLowestPremiumSameForAll12Months;
				return EmployeeShareLowestPremiumSameAll12Months;
			}
		}

		public FCCollection coveredIndividuals
		{
			get
			{
				return EmployeeSetup.Dependents;
			}
		}

		public string EmployeeNumber
		{
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = EmployeeSetup.EmployeeNumber;
				return EmployeeNumber;
			}
		}

		public int EmployeeID
		{
			get
			{
				int EmployeeID = 0;
				EmployeeID = EmployeeSetup.EmployeeID;
				return EmployeeID;
			}
		}

		public int CoverageProviderID
		{
			get
			{
				int CoverageProviderID = 0;
				CoverageProviderID = EmployeeSetup.CoverageProviderID;
				return CoverageProviderID;
			}
		}

		public bool CoverageDeclined
		{
			get
			{
				bool CoverageDeclined = false;
				CoverageDeclined = EmployeeSetup.CoverageDeclined;
				return CoverageDeclined;
			}
		}

		public string CoverageCodeRequiredAll12Months
		{
			get
			{
				string CoverageCodeRequiredAll12Months = "";
				CoverageCodeRequiredAll12Months = EmployeeSetup.CoverageCodeRequiredAll12Months;
				return CoverageCodeRequiredAll12Months;
			}
		}

		public cACAEmployeeSetup ACAEmployee
		{
			set
			{
				EmployeeSetup = value;
			}
			get
			{
				cACAEmployeeSetup ACAEmployee = null;
				ACAEmployee = EmployeeSetup;
				return ACAEmployee;
			}
		}

		public string EmployeeName
		{
			set
			{
				strEmployeeName = value;
			}
			get
			{
				return strEmployeeName;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public string Address
		{
			set
			{
				strAddress = value;
			}
			get
			{
				string Address = "";
				Address = strAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string PostalCode
		{
			set
			{
				strPostalCode = value;
			}
			get
			{
				string PostalCode = "";
				PostalCode = strPostalCode;
				return PostalCode;
			}
		}

		public bool Void
		{
			set
			{
				boolVoid = value;
			}
			get
			{
				bool Void = false;
				Void = boolVoid;
				return Void;
			}
		}

		public bool Corrected
		{
			set
			{
				boolCorrected = value;
			}
			get
			{
				bool Corrected = false;
				Corrected = boolCorrected;
				return Corrected;
			}
		}

		public string EmployeeFirstName
		{
			set
			{
				strEmployeeFirstName = value;
			}
			get
			{
				return strEmployeeFirstName;
			}
		}

		public string EmployeeMiddleName
		{
			set
			{
				strEmployeeMiddleName = value;
			}
			get
			{
				return strEmployeeMiddleName;
			}
		}

		public string EmployeeLastName
		{
			set
			{
				strEmployeeLastName = value;
			}
			get
			{
				return strEmployeeLastName;
			}
		}

        public int Age { get; set; } = 0;
	}
}
