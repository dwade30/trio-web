﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class c1095BXMLManifestController
	{
		//=========================================================
		private string GetSimpleTag(string strTag, string strInnerXML)
		{
			string GetSimpleTag = "";
			GetSimpleTag = "<" + strTag + ">" + strInnerXML + "</" + strTag + ">";
			return GetSimpleTag;
		}

		private string StripBadChars(string strOrig)
		{
			string StripBadChars = "";
			string strReturn;
			strReturn = strOrig.Replace("'", "");
			strReturn = strReturn.Replace("&", "");
			strReturn = strReturn.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
			strReturn = strReturn.Replace("<", "");
			StripBadChars = strReturn;
			return StripBadChars;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetMailingAddressGroupXML(string strAddress1, string strCity, string strState, string strZip, string strZip4)
		{
			object GetMailingAddressGroupXML = null;
			string strXML = "";
			string strTemp = "";
			string strZip5;
			string strZipExt = "";
			strZip = strZip.Replace("-", " ");
			strZip5 = fecherFoundation.Strings.Trim(Strings.Left(strZip + "     ", 5));
			if (strZip4 != "")
			{
				strZipExt = strZip4;
			}
			else
			{
				if (strZip.Length > 6)
				{
					strZipExt = fecherFoundation.Strings.Trim(Strings.Mid(strZip, 7));
				}
			}
			if (strAddress1 != "" || strCity != "" || strState != "" || strZip != "")
			{
				strXML = "<MailingAddressGrp>";
				strXML += "<USAddressGrp>";
				if (strAddress1 != "")
				{
					strXML += "<AddressLine1Txt>";
					strTemp = strAddress1;
					strTemp = strTemp.Replace("'", "");
					strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
					strTemp = strTemp.Replace("/", "");
					strTemp = strTemp.Replace("\\", "");
					strTemp = strTemp.Replace("#", " ");
					strXML += strTemp;
					strXML += "</AddressLine1Txt>";
				}
				if (strCity != "")
				{
					strXML += "<irs:CityNm>";
					strTemp = strCity.Replace("'", "");
					strTemp = strCity.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
					strXML += strTemp;
					strXML += "</irs:CityNm>";
				}
				strXML += "<USStateCd>";
				strXML += strState;
				strXML += "</USStateCd>";
				strXML += "<irs:USZIPCd>";
				strXML += strZip5.Replace(" ", "");
				strXML += "</irs:USZIPCd>";
				if (strZipExt != "")
				{
					strXML += "<irs:USZIPExtensionCd>";
					strXML += strZipExt;
					strXML += "</irs:USZIPExtensionCd>";
				}
				strXML += "</USAddressGrp>";
				strXML += "</MailingAddressGrp>";
			}
			GetMailingAddressGroupXML = strXML;
			return GetMailingAddressGroupXML;
		}

		private string GetPersonNameXML(string strTag, string strFirstName, string strMiddleName, string strLastName, string strSuffix)
		{
			string GetPersonNameXML = "";
			string strXML = "";
			string strTemp = "";
			if (strFirstName != "" || strLastName != "")
			{
				if (strTag != "")
				{
					strXML = "<" + strTag + ">";
				}
				if (strFirstName != "")
				{
					strXML += GetSimpleTag("PersonFirstNm", StripBadChars(strFirstName));
				}
				if (strMiddleName != "")
				{
					strXML += GetSimpleTag("PersonMiddleNm", StripBadChars(strMiddleName));
				}
				if (strLastName != "")
				{
					strXML += GetSimpleTag("PersonLastNm", StripBadChars(strLastName));
				}
				if (strSuffix != "")
				{
					strXML += GetSimpleTag("SuffixNm", StripBadChars(strSuffix));
				}
				if (strTag != "")
				{
					strXML += "</" + strTag + ">";
				}
			}
			GetPersonNameXML = strXML;
			return GetPersonNameXML;
		}

		public string ACABXMLReportToManifestXML(ref cACABXMLReport theReport)
		{
			string ACABXMLReportToManifestXML = "";
			string strXML;
			string strTemp;
			strXML = "<?xml version=" + FCConvert.ToString(Convert.ToChar(34)) + "1.0" + FCConvert.ToString(Convert.ToChar(34)) + " encoding=" + FCConvert.ToString(Convert.ToChar(34)) + "utf-8" + FCConvert.ToString(Convert.ToChar(34)) + "?>";
			strXML += "<q2:ACAUIBusinessHeader";
			strXML += " xmlns=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:ext:aca:air:ty20" + FCConvert.ToString(Convert.ToChar(34));
			strXML += " xmlns:irs=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:common" + FCConvert.ToString(Convert.ToChar(34));
			strXML += " xmlns:q1=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:msg:acabusinessheader" + FCConvert.ToString(Convert.ToChar(34));
			strXML += " xsi:schemaLocation=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:msg:acauibusinessheader IRS-ACAUserInterfaceHeaderMessage.xsd " + FCConvert.ToString(Convert.ToChar(34));
			strXML += " xmlns:xsi=" + FCConvert.ToString(Convert.ToChar(34)) + "http://www.w3.org/2001/XMLSchema-instance" + FCConvert.ToString(Convert.ToChar(34));
			strXML += " xmlns:q2=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:msg:acauibusinessheader" + FCConvert.ToString(Convert.ToChar(34));
			strXML += " xmlns:p2=" + FCConvert.ToString(Convert.ToChar(34)) + "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wsssecurity-utility-1.0.xsd" + FCConvert.ToString(Convert.ToChar(34));
			strXML += ">";
			strXML += "<q1:ACABusinessHeader>";
			strXML += GetSimpleTag("UniqueTransmissionId", theReport.Manifest.TransmissionID);
			strXML += GetSimpleTag("irs:Timestamp", theReport.Manifest.FileTimeStamp);
			strXML += "</q1:ACABusinessHeader>";
			strXML += "<ACATransmitterManifestReqDtl>";
			strXML += GetSimpleTag("PaymentYr", FCConvert.ToString(theReport.Manifest.PaymentYear));
			strXML += "<PriorYearDataInd>";
			if (theReport.Manifest.IsPriorYearData)
			{
				strXML += "1";
			}
			else
			{
				strXML += "0";
			}
			strXML += "</PriorYearDataInd>";
			strTemp = theReport.Manifest.EIN;
			strTemp = strTemp.Replace("-", "");
			strTemp = strTemp.Replace(" ", "");
			strXML += GetSimpleTag("irs:EIN", strTemp);
			strXML += GetSimpleTag("TransmissionTypeCd", theReport.Manifest.TransmissionTypeCode);
			if (theReport.Manifest.TestFileCode != "")
			{
				strXML += GetSimpleTag("TestFileCd", theReport.Manifest.TestFileCode);
			}
			strXML += "<TransmitterNameGrp>";
			strTemp = StripBadChars(theReport.Manifest.TransmitterName);
			strXML += GetSimpleTag("BusinessNameLine1Txt", strTemp);
			strXML += "</TransmitterNameGrp>";
			strXML += "<CompanyInformationGrp>";
			strTemp = StripBadChars(theReport.Manifest.CompanyName);
			strXML += GetSimpleTag("CompanyNm", strTemp);
			strXML += FCConvert.ToString(GetMailingAddressGroupXML(theReport.Manifest.CompanyAddress, theReport.Manifest.CompanyCity, theReport.Manifest.CompanyState, theReport.Manifest.CompanyPostalCode, ""));
			strXML += GetPersonNameXML("ContactNameGrp", theReport.Manifest.ContactFirstName, theReport.Manifest.ContactMiddleName, theReport.Manifest.ContactLastName, theReport.Manifest.ContactSuffix);
			if (theReport.Manifest.ContactPhone != "")
			{
				strTemp = theReport.Manifest.ContactPhone;
				strTemp = StripBadChars(strTemp);
				strTemp = strTemp.Replace("(", "");
				strTemp = strTemp.Replace(")", "");
				strTemp = strTemp.Replace("-", "");
				strXML += GetSimpleTag("ContactPhoneNum", strTemp);
			}
			strXML += "</CompanyInformationGrp>";
			strXML += "<VendorInformationGrp>";
			strXML += GetSimpleTag("VendorCd", theReport.Manifest.VendorCode);
			strXML += GetPersonNameXML("ContactNameGrp", theReport.Manifest.VendorContactFirstName, theReport.Manifest.VendorContactMiddleName, theReport.Manifest.VendorContactLastName, theReport.Manifest.VendorContactSuffix);
			if (theReport.Manifest.VendorContactPhone != "")
			{
				strTemp = theReport.Manifest.VendorContactPhone;
				strTemp = StripBadChars(strTemp);
				strTemp = strTemp.Replace("(", "");
				strTemp = strTemp.Replace(")", "");
				strTemp = strTemp.Replace("-", "");
				strXML += GetSimpleTag("ContactPhoneNum", strTemp);
			}
			strXML += "</VendorInformationGrp>";
			strXML += GetSimpleTag("TotalPayeeRecordCnt", FCConvert.ToString(theReport.Manifest.TotalPayeeCount));
			strXML += GetSimpleTag("TotalPayerRecordCnt", FCConvert.ToString(theReport.Manifest.TotalPayerRecordCount));
			strXML += GetSimpleTag("SoftwareId", theReport.Manifest.SoftwareID);
			strXML += GetSimpleTag("FormTypeCd", theReport.Manifest.FormType);
			strXML += GetSimpleTag("irs:BinaryFormatCd", theReport.Manifest.BinaryFormatCode);
			strXML += GetSimpleTag("irs:ChecksumAugmentationNum", theReport.Manifest.CheckSum);
			strXML += GetSimpleTag("AttachmentByteSizeNum", theReport.Manifest.AttachmentSize.ToString());
			strXML += GetSimpleTag("DocumentSystemFileNm", theReport.FileName);
			strXML += "</ACATransmitterManifestReqDtl>";
			strXML += "</q2:ACAUIBusinessHeader>";
			ACABXMLReportToManifestXML = strXML;
			return ACABXMLReportToManifestXML;
		}
	}
}
