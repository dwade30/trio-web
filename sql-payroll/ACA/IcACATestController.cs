﻿using Global;

namespace TWPY0000
{
    public interface IcACATestController
    {
        cACABXMLReport GetTestScenario1(int intYear);
       
       // cGenericCollection FillScenario1Details(cGenericCollection listDetails, int intYear);
      
      
      
        cACABXMLReport GetTestScenario2(int intYear);
        cACABXMLReport GetTestScenario2C(int intYear);
        cACABXMLReport GetTestScenario6C(int intYear);
        cACABXMLReport GetTestScenario6(int intYear);
        cACABXMLReport GetTestScenario8C(int intYear);
        cACABXMLReport GetTestScenario8(int intYear);
        cACACXMLReport GetTestScenario3(int intYear);
        cACACXMLReport GetTestScenario4(int intYear);
        cACACXMLReport GetTestScenario4C(int intYear);
        cACACXMLReport GetTestScenario5C(int intYear);
        cACACXMLReport GetTestScenario5(int intYear);
        cACACXMLReport GetTestScenario7C(int intYear);
        cACACXMLReport GetTestScenario7(int intYear);
    }
}