﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class cACASetupList
	{
		//=========================================================
		private FCCollection lstDetails = new FCCollection();
		private int intCurrentIndex;

		public cACASetupList() : base()
		{
			intCurrentIndex = -1;
		}

		public void ClearList()
		{
			if (!(lstDetails == null))
			{
				foreach (cACAEmployeeSetup tRec in lstDetails)
				{
					lstDetails.Remove(1);
				}
				// tRec
			}
		}

		public void AddItem(cACAEmployeeSetup tItem)
		{
			if (!(tItem == null))
			{
				lstDetails.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			if (!(lstDetails == null))
			{
				if (!FCUtils.IsEmpty(lstDetails))
				{
					if (lstDetails.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > lstDetails.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstDetails.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstDetails.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstDetails[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}

		public int ItemCount()
		{
			int ItemCount = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				ItemCount = lstDetails.Count;
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}

		public cACAEmployeeSetup GetCurrentDetail()
		{
			cACAEmployeeSetup GetCurrentDetail = null;
			cACAEmployeeSetup tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstDetails[intCurrentIndex] == null))
					{
						tRec = lstDetails[intCurrentIndex];
					}
				}
			}
			GetCurrentDetail = tRec;
			return GetCurrentDetail;
		}

		public cACAEmployeeSetup GetDetailByIndex(int intIndex)
		{
			cACAEmployeeSetup GetDetailByIndex = null;
			cACAEmployeeSetup tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(lstDetails) && intIndex > 0)
			{
				if (!(lstDetails[intIndex] == null))
				{
					intCurrentIndex = intIndex;
					tRec = lstDetails[intIndex];
				}
			}
			GetDetailByIndex = tRec;
			return GetDetailByIndex;
		}
	}
}
