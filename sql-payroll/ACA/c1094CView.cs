﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class c1094CView
	{
		//=========================================================
		private c1094C current1094C;
		private cACAService aService = new cACAService();
		private int intCoverageYear;
		private bool boolChanged;
		private cEmployerService eService = new cEmployerService();

		public bool Changed
		{
			set
			{
				boolChanged = value;
			}
			get
			{
				bool Changed = false;
				if (IsCurrentForm)
				{
					Changed = current1094C.IsChanged || boolChanged;
				}
				return Changed;
			}
		}

		public bool IsCurrentForm
		{
			get
			{
				bool IsCurrentForm = false;
				IsCurrentForm = !(current1094C == null);
				return IsCurrentForm;
			}
		}

		public int CoverageYear
		{
			set
			{
				intCoverageYear = value;
			}
			get
			{
				int CoverageYear = 0;
				CoverageYear = intCoverageYear;
				return CoverageYear;
			}
		}

		public c1094C GetCurrentForm()
		{
			c1094C GetCurrentForm = null;
			GetCurrentForm = current1094C;
			return GetCurrentForm;
		}

		public void LoadForm(int intYear)
		{
			intCoverageYear = intYear;
			current1094C = aService.Load1094C(intYear);
			if (current1094C.YearCovered == 0)
			{
				current1094C.YearCovered = intYear;
				c1094C old1094C;
				old1094C = aService.Load1094C(intYear - 1);
				if (old1094C.YearCovered == intYear - 1)
				{
					current1094C.Address = old1094C.Address;
					current1094C.City = old1094C.City;
					current1094C.ContactName = old1094C.ContactName;
					current1094C.ContactLastName = old1094C.ContactLastName;
					current1094C.ContactMiddleName = old1094C.ContactMiddleName;
					current1094C.ContactSuffix = old1094C.ContactSuffix;
					current1094C.ContactTelephone = old1094C.ContactTelephone;
					current1094C.DesignatedGovnernmentEntity = old1094C.DesignatedGovnernmentEntity;
					current1094C.EIN = old1094C.EIN;
					current1094C.EmployerName = old1094C.EmployerName;
					current1094C.EntityAddress = old1094C.EntityAddress;
					current1094C.EntityCity = old1094C.EntityCity;
					current1094C.EntityContact = old1094C.EntityContact;
					current1094C.EntityEIN = old1094C.EntityEIN;
					current1094C.EntityPostalCode = old1094C.EntityPostalCode;
					current1094C.EntityState = old1094C.EntityState;
					current1094C.EntityTelephone = old1094C.EntityTelephone;
					current1094C.IsAuthoritativeTransmittal = old1094C.IsAuthoritativeTransmittal;
					current1094C.MemberOfAggregatedGroup = old1094C.MemberOfAggregatedGroup;
					current1094C.PostalCode = old1094C.PostalCode;
					current1094C.State = old1094C.State;
				}
				else
				{
					cEmployerRecord tEmployer;
					tEmployer = eService.GetEmployerInfo();
					current1094C.Address = tEmployer.Address;
					current1094C.City = tEmployer.City;
					current1094C.EIN = tEmployer.EIN;
					current1094C.ContactTelephone = tEmployer.Telephone;
					current1094C.EmployerName = tEmployer.Name;
					current1094C.State = tEmployer.State;
					current1094C.PostalCode = fecherFoundation.Strings.Trim(tEmployer.Zip + " " + tEmployer.Zip4);
				}
			}
			boolChanged = false;
		}

		public void SaveForm()
		{
			if (IsCurrentForm)
			{
				aService.Save1094C(ref current1094C);
			}
			boolChanged = false;
		}
	}
}
