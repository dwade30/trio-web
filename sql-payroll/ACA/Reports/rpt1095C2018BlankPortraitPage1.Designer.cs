﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2017BlankPortraitPage1.
	/// </summary>
	partial class rpt1095C2018BlankPortraitPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2018BlankPortraitPage1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEmployerPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbl12Months = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape46 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape70 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape72 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape73 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape74 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape75 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape76 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape79 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape82 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape83 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape84 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCoveredLastName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label47,
            this.txtEmployerPostalCode,
            this.Label14,
            this.Label13,
            this.Label9,
            this.Image2,
            this.Label83,
            this.Label23,
            this.txtCoveredMonth1_1,
            this.Shape2,
            this.Label4,
            this.txtName,
            this.txtSSN,
            this.txtEmployer,
            this.txtEIN,
            this.txtAddress,
            this.txtEmployerAddress,
            this.txtPhone,
            this.txtStartMonth,
            this.txtAll12Box14,
            this.txtBox14_1,
            this.txtBox14_2,
            this.txtBox14_3,
            this.txtBox14_4,
            this.txtBox14_5,
            this.txtBox14_6,
            this.txtBox14_7,
            this.txtBox14_8,
            this.txtBox14_9,
            this.txtBox14_10,
            this.txtBox14_11,
            this.txtBox14_12,
            this.txtAll12Box15,
            this.txtBox15_1,
            this.txtBox15_2,
            this.txtBox15_3,
            this.txtBox15_4,
            this.txtBox15_5,
            this.txtBox15_6,
            this.txtBox15_7,
            this.txtBox15_8,
            this.txtBox15_9,
            this.txtBox15_10,
            this.txtBox15_11,
            this.txtBox15_12,
            this.txtAll12Box16,
            this.txtBox16_1,
            this.txtBox16_2,
            this.txtBox16_3,
            this.txtBox16_4,
            this.txtBox16_5,
            this.txtBox16_6,
            this.txtBox16_7,
            this.txtBox16_8,
            this.txtBox16_9,
            this.txtBox16_10,
            this.txtBox16_11,
            this.txtBox16_12,
            this.txtCoveredName1,
            this.txtCoveredIndividuals,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtCity,
            this.txtState,
            this.txtPostalCode,
            this.txtEmployerCity,
            this.txtEmployerState,
            this.txtEmployerName,
            this.txtEmployerReturnAddress,
            this.txtEmployerCityStateZip,
            this.txtEmployeeName,
            this.txtEmployeeAddress,
            this.txtEmployeeCityStateZip,
            this.Line1,
            this.Line2,
            this.Line3,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Line8,
            this.Line9,
            this.Line14,
            this.Line18,
            this.Line19,
            this.Line22,
            this.Line23,
            this.lbl12Months,
            this.Label3,
            this.Line24,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Line27,
            this.Shape4,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Shape5,
            this.Shape6,
            this.Shape7,
            this.Shape8,
            this.Shape9,
            this.Shape10,
            this.Shape11,
            this.Shape12,
            this.Shape13,
            this.Shape14,
            this.Shape15,
            this.Shape16,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Shape20,
            this.Shape21,
            this.Shape22,
            this.Shape23,
            this.Shape24,
            this.Shape25,
            this.Shape26,
            this.Shape27,
            this.Shape28,
            this.Shape29,
            this.Shape30,
            this.Shape31,
            this.Shape32,
            this.Shape33,
            this.Shape34,
            this.Shape35,
            this.Shape36,
            this.Shape37,
            this.Shape38,
            this.Shape39,
            this.Shape40,
            this.Shape41,
            this.Shape42,
            this.Shape43,
            this.Shape44,
            this.Shape45,
            this.Shape46,
            this.Shape47,
            this.Shape48,
            this.Shape49,
            this.Shape50,
            this.Shape51,
            this.Shape52,
            this.Shape53,
            this.Shape54,
            this.Shape55,
            this.Shape56,
            this.Shape57,
            this.Shape58,
            this.Shape59,
            this.Shape60,
            this.Shape61,
            this.Shape62,
            this.Shape63,
            this.Shape64,
            this.Shape65,
            this.Shape66,
            this.Shape67,
            this.Shape68,
            this.Shape69,
            this.Shape70,
            this.Shape71,
            this.Shape72,
            this.Shape73,
            this.Shape74,
            this.Shape75,
            this.Shape76,
            this.Shape77,
            this.Shape78,
            this.Shape79,
            this.Shape80,
            this.Shape81,
            this.Shape82,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Line26,
            this.Label35,
            this.Label36,
            this.Label37,
            this.Label38,
            this.Label39,
            this.Label40,
            this.Label41,
            this.Label42,
            this.Label43,
            this.Label44,
            this.Label45,
            this.Label46,
            this.Label48,
            this.Label49,
            this.Label50,
            this.Label51,
            this.Label52,
            this.Label53,
            this.Label54,
            this.Label55,
            this.Label56,
            this.Label57,
            this.Label58,
            this.Label59,
            this.Line41,
            this.Line43,
            this.Label60,
            this.Line42,
            this.Line29,
            this.Line28,
            this.Label61,
            this.Label62,
            this.Label63,
            this.Line44,
            this.Line45,
            this.Label64,
            this.Label65,
            this.Label66,
            this.Label67,
            this.Label68,
            this.Label69,
            this.Label70,
            this.Label71,
            this.Label72,
            this.Label73,
            this.Label74,
            this.Label75,
            this.Label76,
            this.Line47,
            this.Line48,
            this.Line50,
            this.Line51,
            this.Line52,
            this.Line53,
            this.Line54,
            this.Label77,
            this.Line49,
            this.Label78,
            this.Label79,
            this.Line56,
            this.Label80,
            this.Label81,
            this.Label82,
            this.Line55,
            this.Label85,
            this.Shape83,
            this.Shape84,
            this.Line57,
            this.Label86,
            this.Label87,
            this.Label88,
            this.Label89,
            this.Line58,
            this.Label90,
            this.Label92,
            this.Label93,
            this.Label95,
            this.Line59,
            this.Line20,
            this.Line21,
            this.Line17,
            this.Line16,
            this.Line15,
            this.Line13,
            this.Line12,
            this.Line11,
            this.Line10,
            this.Line30,
            this.Line32,
            this.Line31,
            this.Line34,
            this.Line35,
            this.Line36,
            this.Line38,
            this.Line40,
            this.Line33,
            this.Line37,
            this.Line39,
            this.Label96,
            this.Label97,
            this.Line60,
            this.Line61,
            this.txtCoveredLastName1,
            this.txtCoveredLastName2,
            this.txtCoveredLastName3,
            this.txtCoveredLastName4,
            this.txtCoveredLastName5,
            this.txtCoveredLastName6,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.Line62,
            this.Line63,
            this.txtMiddle,
            this.txtLast});
            this.Detail.Height = 10.20833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Label47
            // 
            this.Label47.Height = 0.1145833F;
            this.Label47.HyperLink = null;
            this.Label47.Left = 0.7291667F;
            this.Label47.Name = "Label47";
            this.Label47.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label47.Text = "$";
            this.Label47.Top = 6.791667F;
            this.Label47.Width = 0.1458333F;
            // 
            // txtEmployerPostalCode
            // 
            this.txtEmployerPostalCode.Height = 0.1770833F;
            this.txtEmployerPostalCode.Left = 6.1875F;
            this.txtEmployerPostalCode.Name = "txtEmployerPostalCode";
            this.txtEmployerPostalCode.Style = "font-size: 8.5pt";
            this.txtEmployerPostalCode.Text = null;
            this.txtEmployerPostalCode.Top = 5.645833F;
            this.txtEmployerPostalCode.Width = 1.270833F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.09375F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.572917F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 5.5pt; text-align: center";
            this.Label14.Text = "Oct";
            this.Label14.Top = 7.770833F;
            this.Label14.Width = 0.2951389F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.09375F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 6.277778F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 5.5pt; text-align: center";
            this.Label13.Text = "Sep";
            this.Label13.Top = 7.770833F;
            this.Label13.Width = 0.2951389F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.09375F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 5.097222F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 5.5pt; text-align: center";
            this.Label9.Text = "May";
            this.Label9.Top = 7.770833F;
            this.Label9.Width = 0.2951389F;
            // 
            // Image2
            // 
            this.Image2.Height = 0.1770833F;
            this.Image2.HyperLink = null;
            this.Image2.ImageData = ((System.IO.Stream)(resources.GetObject("Image2.ImageData")));
            this.Image2.Left = 6.604167F;
            this.Image2.LineWeight = 1F;
            this.Image2.Name = "Image2";
            this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image2.Top = 4.6875F;
            this.Image2.Width = 0.5416667F;
            // 
            // Label83
            // 
            this.Label83.Height = 0.25F;
            this.Label83.HyperLink = null;
            this.Label83.Left = 0.1944444F;
            this.Label83.Name = "Label83";
            this.Label83.Style = "font-family: \'Impact\'; font-size: 14.5pt";
            this.Label83.Text = "1095-C";
            this.Label83.Top = 4.493055F;
            this.Label83.Width = 0.7083333F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.1180556F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 0.4444444F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-family: \'Arial\'; font-size: 6.75pt; text-align: left";
            this.Label23.Text = "If Employer provided self-insured coverage, check the box and enter the informati" +
    "on for each covered individual.";
            this.Label23.Top = 7.4375F;
            this.Label23.Width = 4.770833F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 3.996528F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = "X";
            this.txtCoveredMonth1_1.Top = 7.9375F;
            this.txtCoveredMonth1_1.Width = 0.1875F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.1458333F;
            this.Shape2.Left = 3.996528F;
            this.Shape2.LineWeight = 0F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = 7.944445F;
            this.Shape2.Width = 0.1388889F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.15625F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.958333F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 5.5pt; text-align: center";
            this.Label4.Text = "(e) Months of Coverage";
            this.Label4.Top = 7.625F;
            this.Label4.Width = 3.458333F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.02083333F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-size: 8.5pt";
            this.txtName.Text = null;
            this.txtName.Top = 5.131945F;
            this.txtName.Width = 0.9583333F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 2.375F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Style = "font-size: 8.5pt";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 5.131945F;
            this.txtSSN.Width = 1.333333F;
            // 
            // txtEmployer
            // 
            this.txtEmployer.Height = 0.1770833F;
            this.txtEmployer.Left = 3.833333F;
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Style = "font-size: 8.5pt";
            this.txtEmployer.Text = null;
            this.txtEmployer.Top = 5.131945F;
            this.txtEmployer.Width = 2.270833F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 6.1875F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Style = "font-size: 8.5pt";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 5.131945F;
            this.txtEIN.Width = 1.270833F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.02083333F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 8.5pt";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 5.395833F;
            this.txtAddress.Width = 3.645833F;
            // 
            // txtEmployerAddress
            // 
            this.txtEmployerAddress.Height = 0.1770833F;
            this.txtEmployerAddress.Left = 3.833333F;
            this.txtEmployerAddress.Name = "txtEmployerAddress";
            this.txtEmployerAddress.Style = "font-size: 8.5pt";
            this.txtEmployerAddress.Text = null;
            this.txtEmployerAddress.Top = 5.395833F;
            this.txtEmployerAddress.Width = 2.270833F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1770833F;
            this.txtPhone.Left = 6.1875F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Style = "font-size: 8.5pt";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 5.395833F;
            this.txtPhone.Width = 1.270833F;
            // 
            // txtStartMonth
            // 
            this.txtStartMonth.Height = 0.1770833F;
            this.txtStartMonth.Left = 5.875F;
            this.txtStartMonth.Name = "txtStartMonth";
            this.txtStartMonth.Style = "font-size: 8.5pt";
            this.txtStartMonth.Text = null;
            this.txtStartMonth.Top = 5.875F;
            this.txtStartMonth.Width = 0.7916667F;
            // 
            // txtAll12Box14
            // 
            this.txtAll12Box14.Height = 0.1770833F;
            this.txtAll12Box14.Left = 0.75F;
            this.txtAll12Box14.Name = "txtAll12Box14";
            this.txtAll12Box14.Style = "font-size: 8.5pt";
            this.txtAll12Box14.Text = null;
            this.txtAll12Box14.Top = 6.25F;
            this.txtAll12Box14.Width = 0.5F;
            // 
            // txtBox14_1
            // 
            this.txtBox14_1.Height = 0.1770833F;
            this.txtBox14_1.Left = 1.229167F;
            this.txtBox14_1.Name = "txtBox14_1";
            this.txtBox14_1.Style = "font-size: 8.5pt";
            this.txtBox14_1.Text = null;
            this.txtBox14_1.Top = 6.25F;
            this.txtBox14_1.Width = 0.5F;
            // 
            // txtBox14_2
            // 
            this.txtBox14_2.Height = 0.1770833F;
            this.txtBox14_2.Left = 1.770833F;
            this.txtBox14_2.Name = "txtBox14_2";
            this.txtBox14_2.Style = "font-size: 8.5pt";
            this.txtBox14_2.Text = null;
            this.txtBox14_2.Top = 6.25F;
            this.txtBox14_2.Width = 0.5F;
            // 
            // txtBox14_3
            // 
            this.txtBox14_3.Height = 0.1770833F;
            this.txtBox14_3.Left = 2.270833F;
            this.txtBox14_3.Name = "txtBox14_3";
            this.txtBox14_3.Style = "font-size: 8.5pt";
            this.txtBox14_3.Text = null;
            this.txtBox14_3.Top = 6.25F;
            this.txtBox14_3.Width = 0.5F;
            // 
            // txtBox14_4
            // 
            this.txtBox14_4.Height = 0.1770833F;
            this.txtBox14_4.Left = 2.8125F;
            this.txtBox14_4.Name = "txtBox14_4";
            this.txtBox14_4.Style = "font-size: 8.5pt";
            this.txtBox14_4.Text = null;
            this.txtBox14_4.Top = 6.25F;
            this.txtBox14_4.Width = 0.5F;
            // 
            // txtBox14_5
            // 
            this.txtBox14_5.Height = 0.1770833F;
            this.txtBox14_5.Left = 3.3125F;
            this.txtBox14_5.Name = "txtBox14_5";
            this.txtBox14_5.Style = "font-size: 8.5pt";
            this.txtBox14_5.Text = null;
            this.txtBox14_5.Top = 6.25F;
            this.txtBox14_5.Width = 0.5F;
            // 
            // txtBox14_6
            // 
            this.txtBox14_6.Height = 0.1770833F;
            this.txtBox14_6.Left = 3.854167F;
            this.txtBox14_6.Name = "txtBox14_6";
            this.txtBox14_6.Style = "font-size: 8.5pt";
            this.txtBox14_6.Text = null;
            this.txtBox14_6.Top = 6.25F;
            this.txtBox14_6.Width = 0.5F;
            // 
            // txtBox14_7
            // 
            this.txtBox14_7.Height = 0.1770833F;
            this.txtBox14_7.Left = 4.364583F;
            this.txtBox14_7.Name = "txtBox14_7";
            this.txtBox14_7.Style = "font-size: 8.5pt";
            this.txtBox14_7.Text = null;
            this.txtBox14_7.Top = 6.25F;
            this.txtBox14_7.Width = 0.5F;
            // 
            // txtBox14_8
            // 
            this.txtBox14_8.Height = 0.1770833F;
            this.txtBox14_8.Left = 4.895833F;
            this.txtBox14_8.Name = "txtBox14_8";
            this.txtBox14_8.Style = "font-size: 8.5pt";
            this.txtBox14_8.Text = null;
            this.txtBox14_8.Top = 6.25F;
            this.txtBox14_8.Width = 0.5F;
            // 
            // txtBox14_9
            // 
            this.txtBox14_9.Height = 0.1770833F;
            this.txtBox14_9.Left = 5.395833F;
            this.txtBox14_9.Name = "txtBox14_9";
            this.txtBox14_9.Style = "font-size: 8.5pt";
            this.txtBox14_9.Text = null;
            this.txtBox14_9.Top = 6.25F;
            this.txtBox14_9.Width = 0.5F;
            // 
            // txtBox14_10
            // 
            this.txtBox14_10.Height = 0.1770833F;
            this.txtBox14_10.Left = 5.9375F;
            this.txtBox14_10.Name = "txtBox14_10";
            this.txtBox14_10.Style = "font-size: 8.5pt";
            this.txtBox14_10.Text = null;
            this.txtBox14_10.Top = 6.25F;
            this.txtBox14_10.Width = 0.5F;
            // 
            // txtBox14_11
            // 
            this.txtBox14_11.Height = 0.1770833F;
            this.txtBox14_11.Left = 6.4375F;
            this.txtBox14_11.Name = "txtBox14_11";
            this.txtBox14_11.Style = "font-size: 8.5pt";
            this.txtBox14_11.Text = null;
            this.txtBox14_11.Top = 6.25F;
            this.txtBox14_11.Width = 0.5F;
            // 
            // txtBox14_12
            // 
            this.txtBox14_12.Height = 0.1770833F;
            this.txtBox14_12.Left = 6.96875F;
            this.txtBox14_12.Name = "txtBox14_12";
            this.txtBox14_12.Style = "font-size: 8.5pt";
            this.txtBox14_12.Text = null;
            this.txtBox14_12.Top = 6.25F;
            this.txtBox14_12.Width = 0.5F;
            // 
            // txtAll12Box15
            // 
            this.txtAll12Box15.Height = 0.1770833F;
            this.txtAll12Box15.Left = 0.8055556F;
            this.txtAll12Box15.Name = "txtAll12Box15";
            this.txtAll12Box15.Style = "font-size: 8.5pt";
            this.txtAll12Box15.Text = "0.00";
            this.txtAll12Box15.Top = 6.75F;
            this.txtAll12Box15.Width = 0.4166667F;
            // 
            // txtBox15_1
            // 
            this.txtBox15_1.Height = 0.1770833F;
            this.txtBox15_1.Left = 1.326389F;
            this.txtBox15_1.Name = "txtBox15_1";
            this.txtBox15_1.Style = "font-size: 8.5pt";
            this.txtBox15_1.Text = null;
            this.txtBox15_1.Top = 6.75F;
            this.txtBox15_1.Width = 0.4166667F;
            // 
            // txtBox15_2
            // 
            this.txtBox15_2.Height = 0.1770833F;
            this.txtBox15_2.Left = 1.847222F;
            this.txtBox15_2.Name = "txtBox15_2";
            this.txtBox15_2.Style = "font-size: 8.5pt";
            this.txtBox15_2.Text = null;
            this.txtBox15_2.Top = 6.75F;
            this.txtBox15_2.Width = 0.4166667F;
            // 
            // txtBox15_3
            // 
            this.txtBox15_3.Height = 0.1770833F;
            this.txtBox15_3.Left = 2.368056F;
            this.txtBox15_3.Name = "txtBox15_3";
            this.txtBox15_3.Style = "font-size: 8.5pt";
            this.txtBox15_3.Text = null;
            this.txtBox15_3.Top = 6.75F;
            this.txtBox15_3.Width = 0.4166667F;
            // 
            // txtBox15_4
            // 
            this.txtBox15_4.Height = 0.1770833F;
            this.txtBox15_4.Left = 2.888889F;
            this.txtBox15_4.Name = "txtBox15_4";
            this.txtBox15_4.Style = "font-size: 8.5pt";
            this.txtBox15_4.Text = null;
            this.txtBox15_4.Top = 6.75F;
            this.txtBox15_4.Width = 0.4166667F;
            // 
            // txtBox15_5
            // 
            this.txtBox15_5.Height = 0.1770833F;
            this.txtBox15_5.Left = 3.409722F;
            this.txtBox15_5.Name = "txtBox15_5";
            this.txtBox15_5.Style = "font-size: 8.5pt";
            this.txtBox15_5.Text = null;
            this.txtBox15_5.Top = 6.75F;
            this.txtBox15_5.Width = 0.4166667F;
            // 
            // txtBox15_6
            // 
            this.txtBox15_6.Height = 0.1770833F;
            this.txtBox15_6.Left = 3.930556F;
            this.txtBox15_6.Name = "txtBox15_6";
            this.txtBox15_6.Style = "font-size: 8.5pt";
            this.txtBox15_6.Text = null;
            this.txtBox15_6.Top = 6.75F;
            this.txtBox15_6.Width = 0.4166667F;
            // 
            // txtBox15_7
            // 
            this.txtBox15_7.Height = 0.1770833F;
            this.txtBox15_7.Left = 4.451389F;
            this.txtBox15_7.Name = "txtBox15_7";
            this.txtBox15_7.Style = "font-size: 8.5pt";
            this.txtBox15_7.Text = null;
            this.txtBox15_7.Top = 6.75F;
            this.txtBox15_7.Width = 0.4166667F;
            // 
            // txtBox15_8
            // 
            this.txtBox15_8.Height = 0.1770833F;
            this.txtBox15_8.Left = 4.972222F;
            this.txtBox15_8.Name = "txtBox15_8";
            this.txtBox15_8.Style = "font-size: 8.5pt";
            this.txtBox15_8.Text = null;
            this.txtBox15_8.Top = 6.75F;
            this.txtBox15_8.Width = 0.4166667F;
            // 
            // txtBox15_9
            // 
            this.txtBox15_9.Height = 0.1770833F;
            this.txtBox15_9.Left = 5.493055F;
            this.txtBox15_9.Name = "txtBox15_9";
            this.txtBox15_9.Style = "font-size: 8.5pt";
            this.txtBox15_9.Text = null;
            this.txtBox15_9.Top = 6.75F;
            this.txtBox15_9.Width = 0.4166667F;
            // 
            // txtBox15_10
            // 
            this.txtBox15_10.Height = 0.1770833F;
            this.txtBox15_10.Left = 6.013889F;
            this.txtBox15_10.Name = "txtBox15_10";
            this.txtBox15_10.Style = "font-size: 8.5pt";
            this.txtBox15_10.Text = null;
            this.txtBox15_10.Top = 6.75F;
            this.txtBox15_10.Width = 0.4166667F;
            // 
            // txtBox15_11
            // 
            this.txtBox15_11.Height = 0.1770833F;
            this.txtBox15_11.Left = 6.534722F;
            this.txtBox15_11.Name = "txtBox15_11";
            this.txtBox15_11.Style = "font-size: 8.5pt";
            this.txtBox15_11.Text = null;
            this.txtBox15_11.Top = 6.75F;
            this.txtBox15_11.Width = 0.4166667F;
            // 
            // txtBox15_12
            // 
            this.txtBox15_12.Height = 0.1770833F;
            this.txtBox15_12.Left = 7.055555F;
            this.txtBox15_12.Name = "txtBox15_12";
            this.txtBox15_12.Style = "font-size: 8.5pt";
            this.txtBox15_12.Text = null;
            this.txtBox15_12.Top = 6.75F;
            this.txtBox15_12.Width = 0.4166667F;
            // 
            // txtAll12Box16
            // 
            this.txtAll12Box16.Height = 0.1770833F;
            this.txtAll12Box16.Left = 0.75F;
            this.txtAll12Box16.Name = "txtAll12Box16";
            this.txtAll12Box16.Style = "font-size: 8.5pt";
            this.txtAll12Box16.Text = null;
            this.txtAll12Box16.Top = 7.166667F;
            this.txtAll12Box16.Width = 0.5F;
            // 
            // txtBox16_1
            // 
            this.txtBox16_1.Height = 0.1770833F;
            this.txtBox16_1.Left = 1.229167F;
            this.txtBox16_1.Name = "txtBox16_1";
            this.txtBox16_1.Style = "font-size: 8.5pt";
            this.txtBox16_1.Text = null;
            this.txtBox16_1.Top = 7.166667F;
            this.txtBox16_1.Width = 0.5F;
            // 
            // txtBox16_2
            // 
            this.txtBox16_2.Height = 0.1770833F;
            this.txtBox16_2.Left = 1.770833F;
            this.txtBox16_2.Name = "txtBox16_2";
            this.txtBox16_2.Style = "font-size: 8.5pt";
            this.txtBox16_2.Text = null;
            this.txtBox16_2.Top = 7.166667F;
            this.txtBox16_2.Width = 0.5F;
            // 
            // txtBox16_3
            // 
            this.txtBox16_3.Height = 0.1770833F;
            this.txtBox16_3.Left = 2.270833F;
            this.txtBox16_3.Name = "txtBox16_3";
            this.txtBox16_3.Style = "font-size: 8.5pt";
            this.txtBox16_3.Text = null;
            this.txtBox16_3.Top = 7.166667F;
            this.txtBox16_3.Width = 0.5F;
            // 
            // txtBox16_4
            // 
            this.txtBox16_4.Height = 0.1770833F;
            this.txtBox16_4.Left = 2.8125F;
            this.txtBox16_4.Name = "txtBox16_4";
            this.txtBox16_4.Style = "font-size: 8.5pt";
            this.txtBox16_4.Text = null;
            this.txtBox16_4.Top = 7.166667F;
            this.txtBox16_4.Width = 0.5F;
            // 
            // txtBox16_5
            // 
            this.txtBox16_5.Height = 0.1770833F;
            this.txtBox16_5.Left = 3.3125F;
            this.txtBox16_5.Name = "txtBox16_5";
            this.txtBox16_5.Style = "font-size: 8.5pt";
            this.txtBox16_5.Text = "XX";
            this.txtBox16_5.Top = 7.166667F;
            this.txtBox16_5.Width = 0.5F;
            // 
            // txtBox16_6
            // 
            this.txtBox16_6.Height = 0.1770833F;
            this.txtBox16_6.Left = 3.854167F;
            this.txtBox16_6.Name = "txtBox16_6";
            this.txtBox16_6.Style = "font-size: 8.5pt";
            this.txtBox16_6.Text = null;
            this.txtBox16_6.Top = 7.166667F;
            this.txtBox16_6.Width = 0.5F;
            // 
            // txtBox16_7
            // 
            this.txtBox16_7.Height = 0.1770833F;
            this.txtBox16_7.Left = 4.361111F;
            this.txtBox16_7.Name = "txtBox16_7";
            this.txtBox16_7.Style = "font-size: 8.5pt";
            this.txtBox16_7.Text = null;
            this.txtBox16_7.Top = 7.166667F;
            this.txtBox16_7.Width = 0.5F;
            // 
            // txtBox16_8
            // 
            this.txtBox16_8.Height = 0.1770833F;
            this.txtBox16_8.Left = 4.895833F;
            this.txtBox16_8.Name = "txtBox16_8";
            this.txtBox16_8.Style = "font-size: 8.5pt";
            this.txtBox16_8.Text = null;
            this.txtBox16_8.Top = 7.166667F;
            this.txtBox16_8.Width = 0.5F;
            // 
            // txtBox16_9
            // 
            this.txtBox16_9.Height = 0.1770833F;
            this.txtBox16_9.Left = 5.395833F;
            this.txtBox16_9.Name = "txtBox16_9";
            this.txtBox16_9.Style = "font-size: 8.5pt";
            this.txtBox16_9.Text = null;
            this.txtBox16_9.Top = 7.166667F;
            this.txtBox16_9.Width = 0.5F;
            // 
            // txtBox16_10
            // 
            this.txtBox16_10.Height = 0.1770833F;
            this.txtBox16_10.Left = 5.9375F;
            this.txtBox16_10.Name = "txtBox16_10";
            this.txtBox16_10.Style = "font-size: 8.5pt";
            this.txtBox16_10.Text = null;
            this.txtBox16_10.Top = 7.166667F;
            this.txtBox16_10.Width = 0.5F;
            // 
            // txtBox16_11
            // 
            this.txtBox16_11.Height = 0.1770833F;
            this.txtBox16_11.Left = 6.4375F;
            this.txtBox16_11.Name = "txtBox16_11";
            this.txtBox16_11.Style = "font-size: 8.5pt";
            this.txtBox16_11.Text = null;
            this.txtBox16_11.Top = 7.166667F;
            this.txtBox16_11.Width = 0.5F;
            // 
            // txtBox16_12
            // 
            this.txtBox16_12.Height = 0.1770833F;
            this.txtBox16_12.Left = 6.972222F;
            this.txtBox16_12.Name = "txtBox16_12";
            this.txtBox16_12.Style = "font-size: 8.5pt";
            this.txtBox16_12.Text = null;
            this.txtBox16_12.Top = 7.166667F;
            this.txtBox16_12.Width = 0.5F;
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.1666667F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Style = "font-size: 8.5pt";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 7.9375F;
            this.txtCoveredName1.Width = 0.625F;
            // 
            // txtCoveredIndividuals
            // 
            this.txtCoveredIndividuals.Height = 0.1770833F;
            this.txtCoveredIndividuals.Left = 6.791667F;
            this.txtCoveredIndividuals.Name = "txtCoveredIndividuals";
            this.txtCoveredIndividuals.Text = "X";
            this.txtCoveredIndividuals.Top = 7.375F;
            this.txtCoveredIndividuals.Width = 0.25F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 1.833333F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Style = "font-size: 8.5pt";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 7.9375F;
            this.txtCoveredSSN1.Width = 0.8333333F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 2.75F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Style = "font-size: 8.5pt";
            this.txtCoveredDOB1.Text = "99/99/9999";
            this.txtCoveredDOB1.Top = 7.9375F;
            this.txtCoveredDOB1.Width = 0.75F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.1666667F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Style = "font-size: 8.5pt";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 8.3125F;
            this.txtCoveredName2.Width = 0.625F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 1.833333F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Style = "font-size: 8.5pt";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 8.3125F;
            this.txtCoveredSSN2.Width = 0.8333333F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 2.75F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Style = "font-size: 8.5pt";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 8.3125F;
            this.txtCoveredDOB2.Width = 0.75F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.1666667F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Style = "font-size: 8.5pt";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 8.6875F;
            this.txtCoveredName3.Width = 0.625F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 1.833333F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Style = "font-size: 8.5pt";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 8.6875F;
            this.txtCoveredSSN3.Width = 0.8333333F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 2.75F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Style = "font-size: 8.5pt";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 8.6875F;
            this.txtCoveredDOB3.Width = 0.75F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.1666667F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Style = "font-size: 8.5pt";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 9.0625F;
            this.txtCoveredName4.Width = 0.625F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 1.833333F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Style = "font-size: 8.5pt";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 9.0625F;
            this.txtCoveredSSN4.Width = 0.8333333F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 2.75F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Style = "font-size: 8.5pt";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 9.0625F;
            this.txtCoveredDOB4.Width = 0.75F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.1666667F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Style = "font-size: 8.5pt";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 9.4375F;
            this.txtCoveredName5.Width = 0.625F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 1.833333F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Style = "font-size: 8.5pt";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 9.4375F;
            this.txtCoveredSSN5.Width = 0.8333333F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 2.75F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Style = "font-size: 8.5pt";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 9.4375F;
            this.txtCoveredDOB5.Width = 0.75F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.1666667F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Style = "font-size: 8.5pt";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 9.8125F;
            this.txtCoveredName6.Width = 0.625F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 1.833333F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Style = "font-size: 8.5pt";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 9.8125F;
            this.txtCoveredSSN6.Width = 0.8333333F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 2.75F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Style = "font-size: 8.5pt";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 9.8125F;
            this.txtCoveredDOB6.Width = 0.75F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 3.609028F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = "X";
            this.txtCoveredAll12_1.Top = 7.9375F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 4.291667F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = "X";
            this.txtCoveredMonth2_1.Top = 7.9375F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 4.586805F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = "X";
            this.txtCoveredMonth3_1.Top = 7.9375F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 4.881945F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = "X";
            this.txtCoveredMonth4_1.Top = 7.9375F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 5.177083F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = "X";
            this.txtCoveredMonth5_1.Top = 7.9375F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 5.472222F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = "X";
            this.txtCoveredMonth6_1.Top = 7.9375F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 5.767361F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = "X";
            this.txtCoveredMonth7_1.Top = 7.9375F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 6.0625F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = "X";
            this.txtCoveredMonth8_1.Top = 7.9375F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 6.357639F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = "X";
            this.txtCoveredMonth9_1.Top = 7.9375F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 6.652778F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = "X";
            this.txtCoveredMonth10_1.Top = 7.9375F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 6.947917F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = "X";
            this.txtCoveredMonth11_1.Top = 7.9375F;
            this.txtCoveredMonth11_1.Width = 0.25F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 7.243055F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = "X";
            this.txtCoveredMonth12_1.Top = 7.9375F;
            this.txtCoveredMonth12_1.Width = 0.1875F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 3.609028F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = "X";
            this.txtCoveredAll12_2.Top = 8.3125F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 3.996528F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = "X";
            this.txtCoveredMonth1_2.Top = 8.3125F;
            this.txtCoveredMonth1_2.Width = 0.1875F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 4.291667F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = "X";
            this.txtCoveredMonth2_2.Top = 8.3125F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 4.586805F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = "X";
            this.txtCoveredMonth3_2.Top = 8.3125F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 4.881945F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = "X";
            this.txtCoveredMonth4_2.Top = 8.3125F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 5.177083F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = "X";
            this.txtCoveredMonth5_2.Top = 8.3125F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 5.472222F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = "X";
            this.txtCoveredMonth6_2.Top = 8.3125F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 5.767361F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = "X";
            this.txtCoveredMonth7_2.Top = 8.3125F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 6.0625F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = "X";
            this.txtCoveredMonth8_2.Top = 8.3125F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 6.357639F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = "X";
            this.txtCoveredMonth9_2.Top = 8.3125F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 6.652778F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = "X";
            this.txtCoveredMonth10_2.Top = 8.3125F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 6.947917F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = "X";
            this.txtCoveredMonth11_2.Top = 8.3125F;
            this.txtCoveredMonth11_2.Width = 0.25F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 7.243055F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = "X";
            this.txtCoveredMonth12_2.Top = 8.3125F;
            this.txtCoveredMonth12_2.Width = 0.1875F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 3.609028F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = "X";
            this.txtCoveredAll12_3.Top = 8.6875F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 3.996528F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = "X";
            this.txtCoveredMonth1_3.Top = 8.6875F;
            this.txtCoveredMonth1_3.Width = 0.1875F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 4.291667F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = "X";
            this.txtCoveredMonth2_3.Top = 8.6875F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 4.586805F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = "X";
            this.txtCoveredMonth3_3.Top = 8.6875F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 4.881945F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = "X";
            this.txtCoveredMonth4_3.Top = 8.6875F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 5.177083F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = "X";
            this.txtCoveredMonth5_3.Top = 8.6875F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 5.472222F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = "X";
            this.txtCoveredMonth6_3.Top = 8.6875F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 5.767361F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = "X";
            this.txtCoveredMonth7_3.Top = 8.6875F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 6.0625F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = "X";
            this.txtCoveredMonth8_3.Top = 8.6875F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 6.357639F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = "X";
            this.txtCoveredMonth9_3.Top = 8.6875F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 6.652778F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = "X";
            this.txtCoveredMonth10_3.Top = 8.6875F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 6.947917F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = "X";
            this.txtCoveredMonth11_3.Top = 8.6875F;
            this.txtCoveredMonth11_3.Width = 0.25F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 7.243055F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = "X";
            this.txtCoveredMonth12_3.Top = 8.6875F;
            this.txtCoveredMonth12_3.Width = 0.1875F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 3.609028F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = "X";
            this.txtCoveredAll12_4.Top = 9.0625F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 3.996528F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = "X";
            this.txtCoveredMonth1_4.Top = 9.0625F;
            this.txtCoveredMonth1_4.Width = 0.1875F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 4.291667F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = "X";
            this.txtCoveredMonth2_4.Top = 9.0625F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 4.586805F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = "X";
            this.txtCoveredMonth3_4.Top = 9.0625F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 4.881945F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = "X";
            this.txtCoveredMonth4_4.Top = 9.0625F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 5.177083F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = "X";
            this.txtCoveredMonth5_4.Top = 9.0625F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 5.472222F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = "X";
            this.txtCoveredMonth6_4.Top = 9.0625F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 5.767361F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = "X";
            this.txtCoveredMonth7_4.Top = 9.0625F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 6.0625F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = "X";
            this.txtCoveredMonth8_4.Top = 9.0625F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 6.357639F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = "X";
            this.txtCoveredMonth9_4.Top = 9.0625F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 6.652778F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = "X";
            this.txtCoveredMonth10_4.Top = 9.0625F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 6.947917F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = "X";
            this.txtCoveredMonth11_4.Top = 9.0625F;
            this.txtCoveredMonth11_4.Width = 0.25F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 7.243055F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = "X";
            this.txtCoveredMonth12_4.Top = 9.0625F;
            this.txtCoveredMonth12_4.Width = 0.1875F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 3.609028F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = "X";
            this.txtCoveredAll12_5.Top = 9.4375F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 3.996528F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = "X";
            this.txtCoveredMonth1_5.Top = 9.4375F;
            this.txtCoveredMonth1_5.Width = 0.1875F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 4.291667F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = "X";
            this.txtCoveredMonth2_5.Top = 9.4375F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 4.586805F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = "X";
            this.txtCoveredMonth3_5.Top = 9.4375F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 4.881945F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = "X";
            this.txtCoveredMonth4_5.Top = 9.4375F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 5.177083F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = "X";
            this.txtCoveredMonth5_5.Top = 9.4375F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 5.472222F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = "X";
            this.txtCoveredMonth6_5.Top = 9.4375F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 5.767361F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = "X";
            this.txtCoveredMonth7_5.Top = 9.4375F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 6.0625F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = "X";
            this.txtCoveredMonth8_5.Top = 9.4375F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 6.357639F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = "X";
            this.txtCoveredMonth9_5.Top = 9.4375F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 6.652778F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = "X";
            this.txtCoveredMonth10_5.Top = 9.4375F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 6.947917F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = "X";
            this.txtCoveredMonth11_5.Top = 9.4375F;
            this.txtCoveredMonth11_5.Width = 0.25F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 7.243055F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = "X";
            this.txtCoveredMonth12_5.Top = 9.4375F;
            this.txtCoveredMonth12_5.Width = 0.1875F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 3.609028F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = "X";
            this.txtCoveredAll12_6.Top = 9.8125F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 3.996528F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = "X";
            this.txtCoveredMonth1_6.Top = 9.8125F;
            this.txtCoveredMonth1_6.Width = 0.1875F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 4.291667F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = "X";
            this.txtCoveredMonth2_6.Top = 9.8125F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 4.586805F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = "X";
            this.txtCoveredMonth3_6.Top = 9.8125F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 4.881945F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = "X";
            this.txtCoveredMonth4_6.Top = 9.8125F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 5.177083F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = "X";
            this.txtCoveredMonth5_6.Top = 9.8125F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 5.472222F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = "X";
            this.txtCoveredMonth6_6.Top = 9.8125F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 5.767361F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = "X";
            this.txtCoveredMonth7_6.Top = 9.8125F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 6.0625F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = "X";
            this.txtCoveredMonth8_6.Top = 9.8125F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 6.357639F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = "X";
            this.txtCoveredMonth9_6.Top = 9.8125F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 6.652778F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = "X";
            this.txtCoveredMonth10_6.Top = 9.8125F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 6.947917F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = "X";
            this.txtCoveredMonth11_6.Top = 9.8125F;
            this.txtCoveredMonth11_6.Width = 0.25F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 7.243055F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = "X";
            this.txtCoveredMonth12_6.Top = 9.8125F;
            this.txtCoveredMonth12_6.Width = 0.1875F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 0.02083333F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-size: 8.5pt; white-space: nowrap";
            this.txtCity.Text = null;
            this.txtCity.Top = 5.645833F;
            this.txtCity.Width = 1.083333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 1.145833F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-size: 8.5pt";
            this.txtState.Text = null;
            this.txtState.Top = 5.645833F;
            this.txtState.Width = 1.25F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 2.4375F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Style = "font-size: 8.5pt";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 5.645833F;
            this.txtPostalCode.Width = 1.333333F;
            // 
            // txtEmployerCity
            // 
            this.txtEmployerCity.Height = 0.1770833F;
            this.txtEmployerCity.Left = 3.833333F;
            this.txtEmployerCity.Name = "txtEmployerCity";
            this.txtEmployerCity.Style = "font-size: 8.5pt";
            this.txtEmployerCity.Text = null;
            this.txtEmployerCity.Top = 5.645833F;
            this.txtEmployerCity.Width = 1.083333F;
            // 
            // txtEmployerState
            // 
            this.txtEmployerState.Height = 0.1770833F;
            this.txtEmployerState.Left = 4.9375F;
            this.txtEmployerState.Name = "txtEmployerState";
            this.txtEmployerState.Style = "font-size: 8.5pt";
            this.txtEmployerState.Text = null;
            this.txtEmployerState.Top = 5.645833F;
            this.txtEmployerState.Width = 1.1875F;
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.Height = 0.1770833F;
            this.txtEmployerName.Left = 0.25F;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Style = "font-size: 8.5pt";
            this.txtEmployerName.Text = null;
            this.txtEmployerName.Top = 0.3125F;
            this.txtEmployerName.Width = 3.083333F;
            // 
            // txtEmployerReturnAddress
            // 
            this.txtEmployerReturnAddress.Height = 0.1770833F;
            this.txtEmployerReturnAddress.Left = 0.25F;
            this.txtEmployerReturnAddress.Name = "txtEmployerReturnAddress";
            this.txtEmployerReturnAddress.Style = "font-size: 8.5pt";
            this.txtEmployerReturnAddress.Text = null;
            this.txtEmployerReturnAddress.Top = 0.5F;
            this.txtEmployerReturnAddress.Width = 3.083333F;
            // 
            // txtEmployerCityStateZip
            // 
            this.txtEmployerCityStateZip.Height = 0.1770833F;
            this.txtEmployerCityStateZip.Left = 0.25F;
            this.txtEmployerCityStateZip.Name = "txtEmployerCityStateZip";
            this.txtEmployerCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployerCityStateZip.Text = null;
            this.txtEmployerCityStateZip.Top = 0.6875F;
            this.txtEmployerCityStateZip.Width = 3.083333F;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Height = 0.1770833F;
            this.txtEmployeeName.Left = 0.25F;
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Style = "font-size: 8.5pt";
            this.txtEmployeeName.Text = null;
            this.txtEmployeeName.Top = 2.3125F;
            this.txtEmployeeName.Width = 3.083333F;
            // 
            // txtEmployeeAddress
            // 
            this.txtEmployeeAddress.Height = 0.1770833F;
            this.txtEmployeeAddress.Left = 0.25F;
            this.txtEmployeeAddress.Name = "txtEmployeeAddress";
            this.txtEmployeeAddress.Style = "font-size: 8.5pt";
            this.txtEmployeeAddress.Text = null;
            this.txtEmployeeAddress.Top = 2.5F;
            this.txtEmployeeAddress.Width = 3.083333F;
            // 
            // txtEmployeeCityStateZip
            // 
            this.txtEmployeeCityStateZip.Height = 0.1770833F;
            this.txtEmployeeCityStateZip.Left = 0.25F;
            this.txtEmployeeCityStateZip.Name = "txtEmployeeCityStateZip";
            this.txtEmployeeCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployeeCityStateZip.Text = null;
            this.txtEmployeeCityStateZip.Top = 2.6875F;
            this.txtEmployeeCityStateZip.Width = 3.083333F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 0F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 7.590278F;
            this.Line1.Width = 7.46875F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.46875F;
            this.Line1.Y1 = 7.590278F;
            this.Line1.Y2 = 7.590278F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 0F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 7.875F;
            this.Line2.Width = 7.46875F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.46875F;
            this.Line2.Y1 = 7.875F;
            this.Line2.Y2 = 7.875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 0F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 8.1875F;
            this.Line3.Width = 7.46875F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.46875F;
            this.Line3.Y1 = 8.1875F;
            this.Line3.Y2 = 8.1875F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 0F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 8.5625F;
            this.Line4.Width = 7.46875F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.46875F;
            this.Line4.Y1 = 8.5625F;
            this.Line4.Y2 = 8.5625F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 0F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 8.9375F;
            this.Line5.Width = 7.46875F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 7.46875F;
            this.Line5.Y1 = 8.9375F;
            this.Line5.Y2 = 8.9375F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 0F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 9.3125F;
            this.Line6.Width = 7.46875F;
            this.Line6.X1 = 0F;
            this.Line6.X2 = 7.46875F;
            this.Line6.Y1 = 9.3125F;
            this.Line6.Y2 = 9.3125F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0F;
            this.Line7.LineWeight = 0F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 9.6875F;
            this.Line7.Width = 7.447917F;
            this.Line7.X1 = 0F;
            this.Line7.X2 = 7.447917F;
            this.Line7.Y1 = 9.6875F;
            this.Line7.Y2 = 9.6875F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 0F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 10.0625F;
            this.Line8.Width = 7.46875F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 7.46875F;
            this.Line8.Y1 = 10.0625F;
            this.Line8.Y2 = 10.0625F;
            // 
            // Line9
            // 
            this.Line9.Height = 2.472222F;
            this.Line9.Left = 3.4375F;
            this.Line9.LineWeight = 0F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 7.590278F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 3.4375F;
            this.Line9.X2 = 3.4375F;
            this.Line9.Y1 = 7.590278F;
            this.Line9.Y2 = 10.0625F;
            // 
            // Line14
            // 
            this.Line14.Height = 2.333333F;
            this.Line14.Left = 5.097222F;
            this.Line14.LineWeight = 0F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 7.729167F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 5.097222F;
            this.Line14.X2 = 5.097222F;
            this.Line14.Y1 = 7.729167F;
            this.Line14.Y2 = 10.0625F;
            // 
            // Line18
            // 
            this.Line18.Height = 2.333333F;
            this.Line18.Left = 6.277778F;
            this.Line18.LineWeight = 0F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 7.729167F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 6.277778F;
            this.Line18.X2 = 6.277778F;
            this.Line18.Y1 = 7.729167F;
            this.Line18.Y2 = 10.0625F;
            // 
            // Line19
            // 
            this.Line19.Height = 2.333333F;
            this.Line19.Left = 6.572917F;
            this.Line19.LineWeight = 0F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 7.729167F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 6.572917F;
            this.Line19.X2 = 6.572917F;
            this.Line19.Y1 = 7.729167F;
            this.Line19.Y2 = 10.0625F;
            // 
            // Line22
            // 
            this.Line22.Height = 2.472222F;
            this.Line22.Left = 2.6875F;
            this.Line22.LineWeight = 0F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 7.590278F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 2.6875F;
            this.Line22.X2 = 2.6875F;
            this.Line22.Y1 = 7.590278F;
            this.Line22.Y2 = 10.0625F;
            // 
            // Line23
            // 
            this.Line23.Height = 2.472222F;
            this.Line23.Left = 1.75F;
            this.Line23.LineWeight = 0F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 7.590278F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 1.75F;
            this.Line23.X2 = 1.75F;
            this.Line23.Y1 = 7.590278F;
            this.Line23.Y2 = 10.0625F;
            // 
            // lbl12Months
            // 
            this.lbl12Months.Height = 0.28125F;
            this.lbl12Months.HyperLink = null;
            this.lbl12Months.Left = 3.451389F;
            this.lbl12Months.Name = "lbl12Months";
            this.lbl12Months.Style = "font-size: 5.5pt; text-align: center";
            this.lbl12Months.Text = "(d) Covered all 12 months";
            this.lbl12Months.Top = 7.635417F;
            this.lbl12Months.Width = 0.4583333F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.21875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 5.5pt; text-align: center";
            this.Label3.Text = "(a) Name of covered individual(s)";
            this.Label3.Top = 7.6875F;
            this.Label3.Width = 1.708333F;
            // 
            // Line24
            // 
            this.Line24.Height = 0F;
            this.Line24.Left = 3.916667F;
            this.Line24.LineWeight = 0F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 7.729167F;
            this.Line24.Width = 3.552083F;
            this.Line24.X1 = 3.916667F;
            this.Line24.X2 = 7.46875F;
            this.Line24.Y1 = 7.729167F;
            this.Line24.Y2 = 7.729167F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.09375F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.916667F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 5.5pt; text-align: center";
            this.Label5.Text = "Jan";
            this.Label5.Top = 7.770833F;
            this.Label5.Width = 0.2951389F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.09375F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 4.211805F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 5.5pt; text-align: center";
            this.Label6.Text = "Feb";
            this.Label6.Top = 7.770833F;
            this.Label6.Width = 0.2951389F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.09375F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.506945F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 5.5pt; text-align: center";
            this.Label7.Text = "Mar";
            this.Label7.Top = 7.770833F;
            this.Label7.Width = 0.2951389F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.09375F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 4.802083F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 5.5pt; text-align: center";
            this.Label8.Text = "Apr";
            this.Label8.Top = 7.770833F;
            this.Label8.Width = 0.2951389F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.09375F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 5.392361F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 5.5pt; text-align: center";
            this.Label10.Text = "Jun";
            this.Label10.Top = 7.770833F;
            this.Label10.Width = 0.2951389F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.09375F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.6875F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 5.5pt; text-align: center";
            this.Label11.Text = "July";
            this.Label11.Top = 7.770833F;
            this.Label11.Width = 0.2951389F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.09375F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 5.982639F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 5.5pt; text-align: center";
            this.Label12.Text = "Aug";
            this.Label12.Top = 7.770833F;
            this.Label12.Width = 0.2951389F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.09375F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 6.868055F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 5.5pt; text-align: center";
            this.Label15.Text = "Nov";
            this.Label15.Top = 7.770833F;
            this.Label15.Width = 0.2951389F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.09375F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 7.166667F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 5.5pt; text-align: center";
            this.Label16.Text = "Dec";
            this.Label16.Top = 7.770833F;
            this.Label16.Width = 0.2951389F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.09375F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.04166667F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-size: 6pt; font-weight: bold; text-align: left";
            this.Label17.Text = "RAA #1607  For Privacy Act and Paperwork Reduction Act Notice, see separate instr" +
    "uctions.";
            this.Label17.Top = 10.11458F;
            this.Label17.Width = 3.708333F;
            // 
            // Line27
            // 
            this.Line27.Height = 0F;
            this.Line27.Left = 0F;
            this.Line27.LineWeight = 0F;
            this.Line27.Name = "Line27";
            this.Line27.Top = 6.944445F;
            this.Line27.Width = 7.46875F;
            this.Line27.X1 = 0F;
            this.Line27.X2 = 7.46875F;
            this.Line27.Y1 = 6.944445F;
            this.Line27.Y2 = 6.944445F;
            // 
            // Shape4
            // 
            this.Shape4.Height = 0.1458333F;
            this.Shape4.Left = 6.791667F;
            this.Shape4.LineWeight = 0F;
            this.Shape4.Name = "Shape4";
            this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape4.Top = 7.388889F;
            this.Shape4.Width = 0.1354167F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.09375F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 4.708333F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 6pt; text-align: left";
            this.Label18.Text = "41-0852411";
            this.Label18.Top = 10.11458F;
            this.Label18.Width = 0.6458333F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.09375F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 5.895833F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-size: 6pt; text-align: left";
            this.Label19.Text = "1095C";
            this.Label19.Top = 10.11458F;
            this.Label19.Width = 0.3958333F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.09375F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 6.447917F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-size: 6pt; text-align: left";
            this.Label20.Text = "Form";
            this.Label20.Top = 10.11458F;
            this.Label20.Width = 0.2708333F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.125F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 6.677083F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-size: 7pt; font-weight: bold; text-align: left; vertical-align: bottom";
            this.Label21.Text = "1095-C";
            this.Label21.Top = 10.08333F;
            this.Label21.Width = 0.4583333F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.09375F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 7.013889F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-size: 6pt; text-align: left";
            this.Label22.Text = "(2018)";
            this.Label22.Top = 10.11458F;
            this.Label22.Width = 0.3333333F;
            // 
            // Shape5
            // 
            this.Shape5.Height = 0.1458333F;
            this.Shape5.Left = 3.996528F;
            this.Shape5.LineWeight = 0F;
            this.Shape5.Name = "Shape5";
            this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape5.Top = 8.319445F;
            this.Shape5.Width = 0.1354167F;
            // 
            // Shape6
            // 
            this.Shape6.Height = 0.1458333F;
            this.Shape6.Left = 3.996528F;
            this.Shape6.LineWeight = 0F;
            this.Shape6.Name = "Shape6";
            this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape6.Top = 8.697917F;
            this.Shape6.Width = 0.1354167F;
            // 
            // Shape7
            // 
            this.Shape7.Height = 0.1458333F;
            this.Shape7.Left = 3.996528F;
            this.Shape7.LineWeight = 0F;
            this.Shape7.Name = "Shape7";
            this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape7.Top = 9.072917F;
            this.Shape7.Width = 0.1354167F;
            // 
            // Shape8
            // 
            this.Shape8.Height = 0.1458333F;
            this.Shape8.Left = 3.996528F;
            this.Shape8.LineWeight = 0F;
            this.Shape8.Name = "Shape8";
            this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape8.Top = 9.447917F;
            this.Shape8.Width = 0.1354167F;
            // 
            // Shape9
            // 
            this.Shape9.Height = 0.1458333F;
            this.Shape9.Left = 3.996528F;
            this.Shape9.LineWeight = 0F;
            this.Shape9.Name = "Shape9";
            this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape9.Top = 9.822917F;
            this.Shape9.Width = 0.1354167F;
            // 
            // Shape10
            // 
            this.Shape10.Height = 0.1458333F;
            this.Shape10.Left = 4.291667F;
            this.Shape10.LineWeight = 0F;
            this.Shape10.Name = "Shape10";
            this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape10.Top = 8.322917F;
            this.Shape10.Width = 0.1354167F;
            // 
            // Shape11
            // 
            this.Shape11.Height = 0.1458333F;
            this.Shape11.Left = 4.291667F;
            this.Shape11.LineWeight = 0F;
            this.Shape11.Name = "Shape11";
            this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape11.Top = 8.322917F;
            this.Shape11.Width = 0.1354167F;
            // 
            // Shape12
            // 
            this.Shape12.Height = 0.1458333F;
            this.Shape12.Left = 4.291667F;
            this.Shape12.LineWeight = 0F;
            this.Shape12.Name = "Shape12";
            this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape12.Top = 8.697917F;
            this.Shape12.Width = 0.1354167F;
            // 
            // Shape13
            // 
            this.Shape13.Height = 0.1458333F;
            this.Shape13.Left = 4.291667F;
            this.Shape13.LineWeight = 0F;
            this.Shape13.Name = "Shape13";
            this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape13.Top = 7.947917F;
            this.Shape13.Width = 0.1354167F;
            // 
            // Shape14
            // 
            this.Shape14.Height = 0.1458333F;
            this.Shape14.Left = 4.291667F;
            this.Shape14.LineWeight = 0F;
            this.Shape14.Name = "Shape14";
            this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape14.Top = 9.072917F;
            this.Shape14.Width = 0.1354167F;
            // 
            // Shape15
            // 
            this.Shape15.Height = 0.1458333F;
            this.Shape15.Left = 4.291667F;
            this.Shape15.LineWeight = 0F;
            this.Shape15.Name = "Shape15";
            this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape15.Top = 9.447917F;
            this.Shape15.Width = 0.1354167F;
            // 
            // Shape16
            // 
            this.Shape16.Height = 0.1458333F;
            this.Shape16.Left = 4.291667F;
            this.Shape16.LineWeight = 0F;
            this.Shape16.Name = "Shape16";
            this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape16.Top = 9.822917F;
            this.Shape16.Width = 0.1354167F;
            // 
            // Shape17
            // 
            this.Shape17.Height = 0.1458333F;
            this.Shape17.Left = 4.586805F;
            this.Shape17.LineWeight = 0F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 7.947917F;
            this.Shape17.Width = 0.1354167F;
            // 
            // Shape18
            // 
            this.Shape18.Height = 0.1458333F;
            this.Shape18.Left = 4.586805F;
            this.Shape18.LineWeight = 0F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 8.322917F;
            this.Shape18.Width = 0.1354167F;
            // 
            // Shape19
            // 
            this.Shape19.Height = 0.1458333F;
            this.Shape19.Left = 4.586805F;
            this.Shape19.LineWeight = 0F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 8.697917F;
            this.Shape19.Width = 0.1354167F;
            // 
            // Shape20
            // 
            this.Shape20.Height = 0.1458333F;
            this.Shape20.Left = 4.586805F;
            this.Shape20.LineWeight = 0F;
            this.Shape20.Name = "Shape20";
            this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape20.Top = 9.072917F;
            this.Shape20.Width = 0.1354167F;
            // 
            // Shape21
            // 
            this.Shape21.Height = 0.1458333F;
            this.Shape21.Left = 4.586805F;
            this.Shape21.LineWeight = 0F;
            this.Shape21.Name = "Shape21";
            this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape21.Top = 9.447917F;
            this.Shape21.Width = 0.1354167F;
            // 
            // Shape22
            // 
            this.Shape22.Height = 0.1458333F;
            this.Shape22.Left = 4.586805F;
            this.Shape22.LineWeight = 0F;
            this.Shape22.Name = "Shape22";
            this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape22.Top = 9.822917F;
            this.Shape22.Width = 0.1354167F;
            // 
            // Shape23
            // 
            this.Shape23.Height = 0.1458333F;
            this.Shape23.Left = 4.881945F;
            this.Shape23.LineWeight = 0F;
            this.Shape23.Name = "Shape23";
            this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape23.Top = 7.947917F;
            this.Shape23.Width = 0.1354167F;
            // 
            // Shape24
            // 
            this.Shape24.Height = 0.1458333F;
            this.Shape24.Left = 4.881945F;
            this.Shape24.LineWeight = 0F;
            this.Shape24.Name = "Shape24";
            this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape24.Top = 8.322917F;
            this.Shape24.Width = 0.1354167F;
            // 
            // Shape25
            // 
            this.Shape25.Height = 0.1458333F;
            this.Shape25.Left = 4.881945F;
            this.Shape25.LineWeight = 0F;
            this.Shape25.Name = "Shape25";
            this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape25.Top = 8.697917F;
            this.Shape25.Width = 0.1354167F;
            // 
            // Shape26
            // 
            this.Shape26.Height = 0.1458333F;
            this.Shape26.Left = 4.881945F;
            this.Shape26.LineWeight = 0F;
            this.Shape26.Name = "Shape26";
            this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape26.Top = 9.072917F;
            this.Shape26.Width = 0.1354167F;
            // 
            // Shape27
            // 
            this.Shape27.Height = 0.1458333F;
            this.Shape27.Left = 4.881945F;
            this.Shape27.LineWeight = 0F;
            this.Shape27.Name = "Shape27";
            this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape27.Top = 9.447917F;
            this.Shape27.Width = 0.1354167F;
            // 
            // Shape28
            // 
            this.Shape28.Height = 0.1458333F;
            this.Shape28.Left = 4.881945F;
            this.Shape28.LineWeight = 0F;
            this.Shape28.Name = "Shape28";
            this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape28.Top = 9.822917F;
            this.Shape28.Width = 0.1354167F;
            // 
            // Shape29
            // 
            this.Shape29.Height = 0.1458333F;
            this.Shape29.Left = 5.177083F;
            this.Shape29.LineWeight = 0F;
            this.Shape29.Name = "Shape29";
            this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape29.Top = 7.947917F;
            this.Shape29.Width = 0.1354167F;
            // 
            // Shape30
            // 
            this.Shape30.Height = 0.1458333F;
            this.Shape30.Left = 5.177083F;
            this.Shape30.LineWeight = 0F;
            this.Shape30.Name = "Shape30";
            this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape30.Top = 8.322917F;
            this.Shape30.Width = 0.1354167F;
            // 
            // Shape31
            // 
            this.Shape31.Height = 0.1458333F;
            this.Shape31.Left = 5.177083F;
            this.Shape31.LineWeight = 0F;
            this.Shape31.Name = "Shape31";
            this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape31.Top = 8.697917F;
            this.Shape31.Width = 0.1354167F;
            // 
            // Shape32
            // 
            this.Shape32.Height = 0.1458333F;
            this.Shape32.Left = 5.177083F;
            this.Shape32.LineWeight = 0F;
            this.Shape32.Name = "Shape32";
            this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape32.Top = 9.072917F;
            this.Shape32.Width = 0.1354167F;
            // 
            // Shape33
            // 
            this.Shape33.Height = 0.1458333F;
            this.Shape33.Left = 5.177083F;
            this.Shape33.LineWeight = 0F;
            this.Shape33.Name = "Shape33";
            this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape33.Top = 9.447917F;
            this.Shape33.Width = 0.1354167F;
            // 
            // Shape34
            // 
            this.Shape34.Height = 0.1458333F;
            this.Shape34.Left = 5.177083F;
            this.Shape34.LineWeight = 0F;
            this.Shape34.Name = "Shape34";
            this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape34.Top = 9.822917F;
            this.Shape34.Width = 0.1354167F;
            // 
            // Shape35
            // 
            this.Shape35.Height = 0.1458333F;
            this.Shape35.Left = 5.472222F;
            this.Shape35.LineWeight = 0F;
            this.Shape35.Name = "Shape35";
            this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape35.Top = 7.947917F;
            this.Shape35.Width = 0.1354167F;
            // 
            // Shape36
            // 
            this.Shape36.Height = 0.1458333F;
            this.Shape36.Left = 5.472222F;
            this.Shape36.LineWeight = 0F;
            this.Shape36.Name = "Shape36";
            this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape36.Top = 8.322917F;
            this.Shape36.Width = 0.1354167F;
            // 
            // Shape37
            // 
            this.Shape37.Height = 0.1458333F;
            this.Shape37.Left = 5.472222F;
            this.Shape37.LineWeight = 0F;
            this.Shape37.Name = "Shape37";
            this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape37.Top = 8.697917F;
            this.Shape37.Width = 0.1354167F;
            // 
            // Shape38
            // 
            this.Shape38.Height = 0.1458333F;
            this.Shape38.Left = 5.472222F;
            this.Shape38.LineWeight = 0F;
            this.Shape38.Name = "Shape38";
            this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape38.Top = 9.072917F;
            this.Shape38.Width = 0.1354167F;
            // 
            // Shape39
            // 
            this.Shape39.Height = 0.1458333F;
            this.Shape39.Left = 5.472222F;
            this.Shape39.LineWeight = 0F;
            this.Shape39.Name = "Shape39";
            this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape39.Top = 9.447917F;
            this.Shape39.Width = 0.1354167F;
            // 
            // Shape40
            // 
            this.Shape40.Height = 0.1458333F;
            this.Shape40.Left = 5.472222F;
            this.Shape40.LineWeight = 0F;
            this.Shape40.Name = "Shape40";
            this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape40.Top = 9.822917F;
            this.Shape40.Width = 0.1354167F;
            // 
            // Shape41
            // 
            this.Shape41.Height = 0.1458333F;
            this.Shape41.Left = 5.767361F;
            this.Shape41.LineWeight = 0F;
            this.Shape41.Name = "Shape41";
            this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape41.Top = 7.947917F;
            this.Shape41.Width = 0.1354167F;
            // 
            // Shape42
            // 
            this.Shape42.Height = 0.1458333F;
            this.Shape42.Left = 5.767361F;
            this.Shape42.LineWeight = 0F;
            this.Shape42.Name = "Shape42";
            this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape42.Top = 8.322917F;
            this.Shape42.Width = 0.1354167F;
            // 
            // Shape43
            // 
            this.Shape43.Height = 0.1458333F;
            this.Shape43.Left = 5.767361F;
            this.Shape43.LineWeight = 0F;
            this.Shape43.Name = "Shape43";
            this.Shape43.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape43.Top = 8.697917F;
            this.Shape43.Width = 0.1354167F;
            // 
            // Shape44
            // 
            this.Shape44.Height = 0.1458333F;
            this.Shape44.Left = 5.767361F;
            this.Shape44.LineWeight = 0F;
            this.Shape44.Name = "Shape44";
            this.Shape44.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape44.Top = 9.072917F;
            this.Shape44.Width = 0.1354167F;
            // 
            // Shape45
            // 
            this.Shape45.Height = 0.1458333F;
            this.Shape45.Left = 5.767361F;
            this.Shape45.LineWeight = 0F;
            this.Shape45.Name = "Shape45";
            this.Shape45.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape45.Top = 9.447917F;
            this.Shape45.Width = 0.1354167F;
            // 
            // Shape46
            // 
            this.Shape46.Height = 0.1458333F;
            this.Shape46.Left = 5.767361F;
            this.Shape46.LineWeight = 0F;
            this.Shape46.Name = "Shape46";
            this.Shape46.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape46.Top = 9.822917F;
            this.Shape46.Width = 0.1354167F;
            // 
            // Shape47
            // 
            this.Shape47.Height = 0.1458333F;
            this.Shape47.Left = 6.0625F;
            this.Shape47.LineWeight = 0F;
            this.Shape47.Name = "Shape47";
            this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape47.Top = 7.947917F;
            this.Shape47.Width = 0.1354167F;
            // 
            // Shape48
            // 
            this.Shape48.Height = 0.1458333F;
            this.Shape48.Left = 6.0625F;
            this.Shape48.LineWeight = 0F;
            this.Shape48.Name = "Shape48";
            this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape48.Top = 8.322917F;
            this.Shape48.Width = 0.1354167F;
            // 
            // Shape49
            // 
            this.Shape49.Height = 0.1458333F;
            this.Shape49.Left = 6.0625F;
            this.Shape49.LineWeight = 0F;
            this.Shape49.Name = "Shape49";
            this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape49.Top = 8.697917F;
            this.Shape49.Width = 0.1354167F;
            // 
            // Shape50
            // 
            this.Shape50.Height = 0.1458333F;
            this.Shape50.Left = 6.0625F;
            this.Shape50.LineWeight = 0F;
            this.Shape50.Name = "Shape50";
            this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape50.Top = 9.072917F;
            this.Shape50.Width = 0.1354167F;
            // 
            // Shape51
            // 
            this.Shape51.Height = 0.1458333F;
            this.Shape51.Left = 6.0625F;
            this.Shape51.LineWeight = 0F;
            this.Shape51.Name = "Shape51";
            this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape51.Top = 9.447917F;
            this.Shape51.Width = 0.1354167F;
            // 
            // Shape52
            // 
            this.Shape52.Height = 0.1458333F;
            this.Shape52.Left = 6.0625F;
            this.Shape52.LineWeight = 0F;
            this.Shape52.Name = "Shape52";
            this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape52.Top = 9.822917F;
            this.Shape52.Width = 0.1354167F;
            // 
            // Shape53
            // 
            this.Shape53.Height = 0.1458333F;
            this.Shape53.Left = 6.357639F;
            this.Shape53.LineWeight = 0F;
            this.Shape53.Name = "Shape53";
            this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape53.Top = 7.947917F;
            this.Shape53.Width = 0.1354167F;
            // 
            // Shape54
            // 
            this.Shape54.Height = 0.1458333F;
            this.Shape54.Left = 6.357639F;
            this.Shape54.LineWeight = 0F;
            this.Shape54.Name = "Shape54";
            this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape54.Top = 8.322917F;
            this.Shape54.Width = 0.1354167F;
            // 
            // Shape55
            // 
            this.Shape55.Height = 0.1458333F;
            this.Shape55.Left = 6.357639F;
            this.Shape55.LineWeight = 0F;
            this.Shape55.Name = "Shape55";
            this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape55.Top = 8.697917F;
            this.Shape55.Width = 0.1354167F;
            // 
            // Shape56
            // 
            this.Shape56.Height = 0.1458333F;
            this.Shape56.Left = 6.357639F;
            this.Shape56.LineWeight = 0F;
            this.Shape56.Name = "Shape56";
            this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape56.Top = 9.072917F;
            this.Shape56.Width = 0.1354167F;
            // 
            // Shape57
            // 
            this.Shape57.Height = 0.1458333F;
            this.Shape57.Left = 6.357639F;
            this.Shape57.LineWeight = 0F;
            this.Shape57.Name = "Shape57";
            this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape57.Top = 9.447917F;
            this.Shape57.Width = 0.1354167F;
            // 
            // Shape58
            // 
            this.Shape58.Height = 0.1458333F;
            this.Shape58.Left = 6.357639F;
            this.Shape58.LineWeight = 0F;
            this.Shape58.Name = "Shape58";
            this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape58.Top = 9.822917F;
            this.Shape58.Width = 0.1354167F;
            // 
            // Shape59
            // 
            this.Shape59.Height = 0.1458333F;
            this.Shape59.Left = 6.652778F;
            this.Shape59.LineWeight = 0F;
            this.Shape59.Name = "Shape59";
            this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape59.Top = 7.947917F;
            this.Shape59.Width = 0.1354167F;
            // 
            // Shape60
            // 
            this.Shape60.Height = 0.1458333F;
            this.Shape60.Left = 6.652778F;
            this.Shape60.LineWeight = 0F;
            this.Shape60.Name = "Shape60";
            this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape60.Top = 8.322917F;
            this.Shape60.Width = 0.1354167F;
            // 
            // Shape61
            // 
            this.Shape61.Height = 0.1458333F;
            this.Shape61.Left = 6.652778F;
            this.Shape61.LineWeight = 0F;
            this.Shape61.Name = "Shape61";
            this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape61.Top = 8.697917F;
            this.Shape61.Width = 0.1354167F;
            // 
            // Shape62
            // 
            this.Shape62.Height = 0.1458333F;
            this.Shape62.Left = 6.652778F;
            this.Shape62.LineWeight = 0F;
            this.Shape62.Name = "Shape62";
            this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape62.Top = 9.072917F;
            this.Shape62.Width = 0.1354167F;
            // 
            // Shape63
            // 
            this.Shape63.Height = 0.1458333F;
            this.Shape63.Left = 6.652778F;
            this.Shape63.LineWeight = 0F;
            this.Shape63.Name = "Shape63";
            this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape63.Top = 9.447917F;
            this.Shape63.Width = 0.1354167F;
            // 
            // Shape64
            // 
            this.Shape64.Height = 0.1458333F;
            this.Shape64.Left = 6.652778F;
            this.Shape64.LineWeight = 0F;
            this.Shape64.Name = "Shape64";
            this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape64.Top = 9.822917F;
            this.Shape64.Width = 0.1354167F;
            // 
            // Shape65
            // 
            this.Shape65.Height = 0.1458333F;
            this.Shape65.Left = 6.947917F;
            this.Shape65.LineWeight = 0F;
            this.Shape65.Name = "Shape65";
            this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape65.Top = 7.947917F;
            this.Shape65.Width = 0.1354167F;
            // 
            // Shape66
            // 
            this.Shape66.Height = 0.1458333F;
            this.Shape66.Left = 6.947917F;
            this.Shape66.LineWeight = 0F;
            this.Shape66.Name = "Shape66";
            this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape66.Top = 8.3125F;
            this.Shape66.Width = 0.1354167F;
            // 
            // Shape67
            // 
            this.Shape67.Height = 0.1458333F;
            this.Shape67.Left = 6.947917F;
            this.Shape67.LineWeight = 0F;
            this.Shape67.Name = "Shape67";
            this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape67.Top = 8.697917F;
            this.Shape67.Width = 0.1354167F;
            // 
            // Shape68
            // 
            this.Shape68.Height = 0.1458333F;
            this.Shape68.Left = 6.947917F;
            this.Shape68.LineWeight = 0F;
            this.Shape68.Name = "Shape68";
            this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape68.Top = 9.072917F;
            this.Shape68.Width = 0.1354167F;
            // 
            // Shape69
            // 
            this.Shape69.Height = 0.1458333F;
            this.Shape69.Left = 6.947917F;
            this.Shape69.LineWeight = 0F;
            this.Shape69.Name = "Shape69";
            this.Shape69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape69.Top = 9.447917F;
            this.Shape69.Width = 0.1354167F;
            // 
            // Shape70
            // 
            this.Shape70.Height = 0.1458333F;
            this.Shape70.Left = 6.947917F;
            this.Shape70.LineWeight = 0F;
            this.Shape70.Name = "Shape70";
            this.Shape70.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape70.Top = 9.822917F;
            this.Shape70.Width = 0.1354167F;
            // 
            // Shape71
            // 
            this.Shape71.Height = 0.1458333F;
            this.Shape71.Left = 7.243055F;
            this.Shape71.LineWeight = 0F;
            this.Shape71.Name = "Shape71";
            this.Shape71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape71.Top = 7.947917F;
            this.Shape71.Width = 0.1354167F;
            // 
            // Shape72
            // 
            this.Shape72.Height = 0.1458333F;
            this.Shape72.Left = 7.243055F;
            this.Shape72.LineWeight = 0F;
            this.Shape72.Name = "Shape72";
            this.Shape72.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape72.Top = 8.3125F;
            this.Shape72.Width = 0.1354167F;
            // 
            // Shape73
            // 
            this.Shape73.Height = 0.1458333F;
            this.Shape73.Left = 7.243055F;
            this.Shape73.LineWeight = 0F;
            this.Shape73.Name = "Shape73";
            this.Shape73.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape73.Top = 8.697917F;
            this.Shape73.Width = 0.1354167F;
            // 
            // Shape74
            // 
            this.Shape74.Height = 0.1458333F;
            this.Shape74.Left = 7.243055F;
            this.Shape74.LineWeight = 0F;
            this.Shape74.Name = "Shape74";
            this.Shape74.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape74.Top = 9.072917F;
            this.Shape74.Width = 0.1354167F;
            // 
            // Shape75
            // 
            this.Shape75.Height = 0.1458333F;
            this.Shape75.Left = 7.243055F;
            this.Shape75.LineWeight = 0F;
            this.Shape75.Name = "Shape75";
            this.Shape75.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape75.Top = 9.447917F;
            this.Shape75.Width = 0.1354167F;
            // 
            // Shape76
            // 
            this.Shape76.Height = 0.1458333F;
            this.Shape76.Left = 7.243055F;
            this.Shape76.LineWeight = 0F;
            this.Shape76.Name = "Shape76";
            this.Shape76.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape76.Top = 9.822917F;
            this.Shape76.Width = 0.1354167F;
            // 
            // Shape77
            // 
            this.Shape77.Height = 0.1458333F;
            this.Shape77.Left = 3.609028F;
            this.Shape77.LineWeight = 0F;
            this.Shape77.Name = "Shape77";
            this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape77.Top = 7.947917F;
            this.Shape77.Width = 0.1354167F;
            // 
            // Shape78
            // 
            this.Shape78.Height = 0.1458333F;
            this.Shape78.Left = 3.609028F;
            this.Shape78.LineWeight = 0F;
            this.Shape78.Name = "Shape78";
            this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape78.Top = 8.322917F;
            this.Shape78.Width = 0.1354167F;
            // 
            // Shape79
            // 
            this.Shape79.Height = 0.1458333F;
            this.Shape79.Left = 3.609028F;
            this.Shape79.LineWeight = 0F;
            this.Shape79.Name = "Shape79";
            this.Shape79.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape79.Top = 8.697917F;
            this.Shape79.Width = 0.1354167F;
            // 
            // Shape80
            // 
            this.Shape80.Height = 0.1458333F;
            this.Shape80.Left = 3.609028F;
            this.Shape80.LineWeight = 0F;
            this.Shape80.Name = "Shape80";
            this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape80.Top = 9.072917F;
            this.Shape80.Width = 0.1354167F;
            // 
            // Shape81
            // 
            this.Shape81.Height = 0.1458333F;
            this.Shape81.Left = 3.609028F;
            this.Shape81.LineWeight = 0F;
            this.Shape81.Name = "Shape81";
            this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape81.Top = 9.447917F;
            this.Shape81.Width = 0.1354167F;
            // 
            // Shape82
            // 
            this.Shape82.Height = 0.1458333F;
            this.Shape82.Left = 3.609028F;
            this.Shape82.LineWeight = 0F;
            this.Shape82.Name = "Shape82";
            this.Shape82.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape82.Top = 9.822917F;
            this.Shape82.Width = 0.1354167F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1284722F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 0F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label24.Text = "Part III";
            this.Label24.Top = 7.3125F;
            this.Label24.Width = 0.3472222F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1145833F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 0.4479167F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label25.Text = "Covered Individuals";
            this.Label25.Top = 7.3125F;
            this.Label25.Width = 1.020833F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1145833F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0.02083333F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label26.Text = "17";
            this.Label26.Top = 7.979167F;
            this.Label26.Width = 0.2083333F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1145833F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 0.02083333F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label27.Text = "18";
            this.Label27.Top = 8.34375F;
            this.Label27.Width = 0.2083333F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.1145833F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 0.02083333F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label28.Text = "19";
            this.Label28.Top = 8.708333F;
            this.Label28.Width = 0.2083333F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1145833F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 0.02083333F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label29.Text = "20";
            this.Label29.Top = 9.072917F;
            this.Label29.Width = 0.2083333F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.1145833F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 0.02083333F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label30.Text = "21";
            this.Label30.Top = 9.4375F;
            this.Label30.Width = 0.2083333F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1145833F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 0.02083333F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label31.Text = "22";
            this.Label31.Top = 9.864583F;
            this.Label31.Width = 0.2083333F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.34375F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 0F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-size: 5pt; text-align: left";
            this.Label32.Text = "16 Section 4980H Safe Harbor and Other Relief (enter code, if applicable)";
            this.Label32.Top = 6.979167F;
            this.Label32.Width = 0.7083333F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.40625F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 0F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-size: 5pt; text-align: left";
            this.Label33.Text = "15 Employee Required Contribution (see instructions)";
            this.Label33.Top = 6.520833F;
            this.Label33.Width = 0.6875F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.28125F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 0F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-size: 5pt; text-align: left";
            this.Label34.Text = "14 Offer of Coverage (enter required code)";
            this.Label34.Top = 6.1875F;
            this.Label34.Width = 0.6458333F;
            // 
            // Line26
            // 
            this.Line26.Height = 0F;
            this.Line26.Left = 0F;
            this.Line26.LineWeight = 0F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 6.4375F;
            this.Line26.Width = 7.46875F;
            this.Line26.X1 = 0F;
            this.Line26.X2 = 7.46875F;
            this.Line26.Y1 = 6.4375F;
            this.Line26.Y2 = 6.4375F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.1145833F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 1.25F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label35.Text = "$";
            this.Label35.Top = 6.791667F;
            this.Label35.Width = 0.1458333F;
            // 
            // Label36
            // 
            this.Label36.Height = 0.1145833F;
            this.Label36.HyperLink = null;
            this.Label36.Left = 1.770833F;
            this.Label36.Name = "Label36";
            this.Label36.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label36.Text = "$";
            this.Label36.Top = 6.791667F;
            this.Label36.Width = 0.1458333F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.1145833F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 2.270833F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label37.Text = "$";
            this.Label37.Top = 6.791667F;
            this.Label37.Width = 0.1458333F;
            // 
            // Label38
            // 
            this.Label38.Height = 0.1145833F;
            this.Label38.HyperLink = null;
            this.Label38.Left = 2.8125F;
            this.Label38.Name = "Label38";
            this.Label38.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label38.Text = "$";
            this.Label38.Top = 6.791667F;
            this.Label38.Width = 0.1458333F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.1145833F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 3.3125F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label39.Text = "$";
            this.Label39.Top = 6.791667F;
            this.Label39.Width = 0.1458333F;
            // 
            // Label40
            // 
            this.Label40.Height = 0.1145833F;
            this.Label40.HyperLink = null;
            this.Label40.Left = 3.854167F;
            this.Label40.Name = "Label40";
            this.Label40.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label40.Text = "$";
            this.Label40.Top = 6.791667F;
            this.Label40.Width = 0.1458333F;
            // 
            // Label41
            // 
            this.Label41.Height = 0.1145833F;
            this.Label41.HyperLink = null;
            this.Label41.Left = 4.375F;
            this.Label41.Name = "Label41";
            this.Label41.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label41.Text = "$";
            this.Label41.Top = 6.791667F;
            this.Label41.Width = 0.1458333F;
            // 
            // Label42
            // 
            this.Label42.Height = 0.1145833F;
            this.Label42.HyperLink = null;
            this.Label42.Left = 4.895833F;
            this.Label42.Name = "Label42";
            this.Label42.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label42.Text = "$";
            this.Label42.Top = 6.791667F;
            this.Label42.Width = 0.1458333F;
            // 
            // Label43
            // 
            this.Label43.Height = 0.1145833F;
            this.Label43.HyperLink = null;
            this.Label43.Left = 5.395833F;
            this.Label43.Name = "Label43";
            this.Label43.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label43.Text = "$";
            this.Label43.Top = 6.791667F;
            this.Label43.Width = 0.1458333F;
            // 
            // Label44
            // 
            this.Label44.Height = 0.1145833F;
            this.Label44.HyperLink = null;
            this.Label44.Left = 5.9375F;
            this.Label44.Name = "Label44";
            this.Label44.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label44.Text = "$";
            this.Label44.Top = 6.791667F;
            this.Label44.Width = 0.1458333F;
            // 
            // Label45
            // 
            this.Label45.Height = 0.1145833F;
            this.Label45.HyperLink = null;
            this.Label45.Left = 6.4375F;
            this.Label45.Name = "Label45";
            this.Label45.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label45.Text = "$";
            this.Label45.Top = 6.791667F;
            this.Label45.Width = 0.1458333F;
            // 
            // Label46
            // 
            this.Label46.Height = 0.1145833F;
            this.Label46.HyperLink = null;
            this.Label46.Left = 6.96875F;
            this.Label46.Name = "Label46";
            this.Label46.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label46.Text = "$";
            this.Label46.Top = 6.791667F;
            this.Label46.Width = 0.1458333F;
            // 
            // Label48
            // 
            this.Label48.Height = 0.09375F;
            this.Label48.HyperLink = null;
            this.Label48.Left = 1.229167F;
            this.Label48.Name = "Label48";
            this.Label48.Style = "font-size: 5.5pt; text-align: center";
            this.Label48.Text = "Jan";
            this.Label48.Top = 6.09375F;
            this.Label48.Width = 0.5208333F;
            // 
            // Label49
            // 
            this.Label49.Height = 0.09375F;
            this.Label49.HyperLink = null;
            this.Label49.Left = 1.75F;
            this.Label49.Name = "Label49";
            this.Label49.Style = "font-size: 5.5pt; text-align: center";
            this.Label49.Text = "Feb";
            this.Label49.Top = 6.09375F;
            this.Label49.Width = 0.5208333F;
            // 
            // Label50
            // 
            this.Label50.Height = 0.09375F;
            this.Label50.HyperLink = null;
            this.Label50.Left = 2.270833F;
            this.Label50.Name = "Label50";
            this.Label50.Style = "font-size: 5.5pt; text-align: center";
            this.Label50.Text = "Mar";
            this.Label50.Top = 6.09375F;
            this.Label50.Width = 0.5208333F;
            // 
            // Label51
            // 
            this.Label51.Height = 0.09375F;
            this.Label51.HyperLink = null;
            this.Label51.Left = 2.791667F;
            this.Label51.Name = "Label51";
            this.Label51.Style = "font-size: 5.5pt; text-align: center";
            this.Label51.Text = "Apr";
            this.Label51.Top = 6.09375F;
            this.Label51.Width = 0.5208333F;
            // 
            // Label52
            // 
            this.Label52.Height = 0.09375F;
            this.Label52.HyperLink = null;
            this.Label52.Left = 3.3125F;
            this.Label52.Name = "Label52";
            this.Label52.Style = "font-size: 5.5pt; text-align: center";
            this.Label52.Text = "May";
            this.Label52.Top = 6.09375F;
            this.Label52.Width = 0.5208333F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.09375F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 3.833333F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-size: 5.5pt; text-align: center";
            this.Label53.Text = "Jun";
            this.Label53.Top = 6.09375F;
            this.Label53.Width = 0.5208333F;
            // 
            // Label54
            // 
            this.Label54.Height = 0.09375F;
            this.Label54.HyperLink = null;
            this.Label54.Left = 4.354167F;
            this.Label54.Name = "Label54";
            this.Label54.Style = "font-size: 5.5pt; text-align: center";
            this.Label54.Text = "July";
            this.Label54.Top = 6.09375F;
            this.Label54.Width = 0.5208333F;
            // 
            // Label55
            // 
            this.Label55.Height = 0.09375F;
            this.Label55.HyperLink = null;
            this.Label55.Left = 4.875F;
            this.Label55.Name = "Label55";
            this.Label55.Style = "font-size: 5.5pt; text-align: center";
            this.Label55.Text = "Aug";
            this.Label55.Top = 6.09375F;
            this.Label55.Width = 0.5208333F;
            // 
            // Label56
            // 
            this.Label56.Height = 0.09375F;
            this.Label56.HyperLink = null;
            this.Label56.Left = 5.395833F;
            this.Label56.Name = "Label56";
            this.Label56.Style = "font-size: 5.5pt; text-align: center";
            this.Label56.Text = "Sep";
            this.Label56.Top = 6.09375F;
            this.Label56.Width = 0.5208333F;
            // 
            // Label57
            // 
            this.Label57.Height = 0.09375F;
            this.Label57.HyperLink = null;
            this.Label57.Left = 5.916667F;
            this.Label57.Name = "Label57";
            this.Label57.Style = "font-size: 5.5pt; text-align: center";
            this.Label57.Text = "Oct";
            this.Label57.Top = 6.09375F;
            this.Label57.Width = 0.5208333F;
            // 
            // Label58
            // 
            this.Label58.Height = 0.09375F;
            this.Label58.HyperLink = null;
            this.Label58.Left = 6.4375F;
            this.Label58.Name = "Label58";
            this.Label58.Style = "font-size: 5.5pt; text-align: center";
            this.Label58.Text = "Nov";
            this.Label58.Top = 6.09375F;
            this.Label58.Width = 0.5208333F;
            // 
            // Label59
            // 
            this.Label59.Height = 0.09375F;
            this.Label59.HyperLink = null;
            this.Label59.Left = 6.958333F;
            this.Label59.Name = "Label59";
            this.Label59.Style = "font-size: 5.5pt; text-align: center";
            this.Label59.Text = "Dec";
            this.Label59.Top = 6.09375F;
            this.Label59.Width = 0.5208333F;
            // 
            // Line41
            // 
            this.Line41.Height = 0F;
            this.Line41.Left = 0F;
            this.Line41.LineWeight = 0F;
            this.Line41.Name = "Line41";
            this.Line41.Top = 6.069445F;
            this.Line41.Width = 7.46875F;
            this.Line41.X1 = 0F;
            this.Line41.X2 = 7.46875F;
            this.Line41.Y1 = 6.069445F;
            this.Line41.Y2 = 6.069445F;
            // 
            // Line43
            // 
            this.Line43.Height = 0F;
            this.Line43.Left = 0.7083333F;
            this.Line43.LineWeight = 0F;
            this.Line43.Name = "Line43";
            this.Line43.Top = 6.1875F;
            this.Line43.Width = 6.760417F;
            this.Line43.X1 = 0.7083333F;
            this.Line43.X2 = 7.46875F;
            this.Line43.Y1 = 6.1875F;
            this.Line43.Y2 = 6.1875F;
            // 
            // Label60
            // 
            this.Label60.Height = 0.09375F;
            this.Label60.HyperLink = null;
            this.Label60.Left = 0.7152778F;
            this.Label60.Name = "Label60";
            this.Label60.Style = "font-size: 5.5pt; text-align: center";
            this.Label60.Text = "All 12 Months";
            this.Label60.Top = 6.09375F;
            this.Label60.Width = 0.5208333F;
            // 
            // Line42
            // 
            this.Line42.Height = 1.243055F;
            this.Line42.Left = 0.7083333F;
            this.Line42.LineWeight = 0F;
            this.Line42.Name = "Line42";
            this.Line42.Top = 6.069445F;
            this.Line42.Width = 0F;
            this.Line42.X1 = 0.7083333F;
            this.Line42.X2 = 0.7083333F;
            this.Line42.Y1 = 6.069445F;
            this.Line42.Y2 = 7.3125F;
            // 
            // Line29
            // 
            this.Line29.Height = 1.243055F;
            this.Line29.Left = 1.229167F;
            this.Line29.LineWeight = 0F;
            this.Line29.Name = "Line29";
            this.Line29.Top = 6.069445F;
            this.Line29.Width = 0F;
            this.Line29.X1 = 1.229167F;
            this.Line29.X2 = 1.229167F;
            this.Line29.Y1 = 6.069445F;
            this.Line29.Y2 = 7.3125F;
            // 
            // Line28
            // 
            this.Line28.Height = 0F;
            this.Line28.Left = 0F;
            this.Line28.LineWeight = 0F;
            this.Line28.Name = "Line28";
            this.Line28.Top = 7.3125F;
            this.Line28.Width = 7.46875F;
            this.Line28.X1 = 0F;
            this.Line28.X2 = 7.46875F;
            this.Line28.Y1 = 7.3125F;
            this.Line28.Y2 = 7.3125F;
            // 
            // Label61
            // 
            this.Label61.Height = 0.1284722F;
            this.Label61.HyperLink = null;
            this.Label61.Left = 0F;
            this.Label61.Name = "Label61";
            this.Label61.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label61.Text = "Part II";
            this.Label61.Top = 5.888889F;
            this.Label61.Width = 0.3472222F;
            // 
            // Label62
            // 
            this.Label62.Height = 0.1145833F;
            this.Label62.HyperLink = null;
            this.Label62.Left = 0.3854167F;
            this.Label62.Name = "Label62";
            this.Label62.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label62.Text = "Employee Offer and Coverage";
            this.Label62.Top = 5.885417F;
            this.Label62.Width = 1.520833F;
            // 
            // Label63
            // 
            this.Label63.Height = 0.1145833F;
            this.Label63.HyperLink = null;
            this.Label63.Left = 3.833333F;
            this.Label63.Name = "Label63";
            this.Label63.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label63.Text = "Plan Start Month";
            this.Label63.Top = 5.885417F;
            this.Label63.Width = 1.020833F;
            // 
            // Line44
            // 
            this.Line44.Height = 0.927083F;
            this.Line44.Left = 3.791667F;
            this.Line44.LineWeight = 1F;
            this.Line44.Name = "Line44";
            this.Line44.Top = 4.895833F;
            this.Line44.Width = 0F;
            this.Line44.X1 = 3.791667F;
            this.Line44.X2 = 3.791667F;
            this.Line44.Y1 = 4.895833F;
            this.Line44.Y2 = 5.822917F;
            // 
            // Line45
            // 
            this.Line45.Height = 0F;
            this.Line45.Left = 0F;
            this.Line45.LineWeight = 1F;
            this.Line45.Name = "Line45";
            this.Line45.Top = 5.822917F;
            this.Line45.Width = 7.46875F;
            this.Line45.X1 = 0F;
            this.Line45.X2 = 7.46875F;
            this.Line45.Y1 = 5.822917F;
            this.Line45.Y2 = 5.822917F;
            // 
            // Label64
            // 
            this.Label64.Height = 0.09375F;
            this.Label64.HyperLink = null;
            this.Label64.Left = 0.02083333F;
            this.Label64.Name = "Label64";
            this.Label64.Style = "font-size: 5pt; text-align: left";
            this.Label64.Text = "4 City or town";
            this.Label64.Top = 5.5625F;
            this.Label64.Width = 0.6458333F;
            // 
            // Label65
            // 
            this.Label65.Height = 0.09375F;
            this.Label65.HyperLink = null;
            this.Label65.Left = 1.145833F;
            this.Label65.Name = "Label65";
            this.Label65.Style = "font-size: 5pt; text-align: left";
            this.Label65.Text = "5 State or province";
            this.Label65.Top = 5.5625F;
            this.Label65.Width = 0.6458333F;
            // 
            // Label66
            // 
            this.Label66.Height = 0.09375F;
            this.Label66.HyperLink = null;
            this.Label66.Left = 2.402778F;
            this.Label66.Name = "Label66";
            this.Label66.Style = "font-size: 5pt; text-align: left";
            this.Label66.Text = "6 Country and ZIP or foreign postal code";
            this.Label66.Top = 5.5625F;
            this.Label66.Width = 1.333333F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.09375F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 0.02083333F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-size: 5pt; text-align: left";
            this.Label67.Text = "3 Street address (including apartment no.)";
            this.Label67.Top = 5.3125F;
            this.Label67.Width = 1.583333F;
            // 
            // Label68
            // 
            this.Label68.Height = 0.09375F;
            this.Label68.HyperLink = null;
            this.Label68.Left = 3.833333F;
            this.Label68.Name = "Label68";
            this.Label68.Style = "font-size: 5pt; text-align: left";
            this.Label68.Text = "9 Street address (including room or suite no.)";
            this.Label68.Top = 5.3125F;
            this.Label68.Width = 1.645833F;
            // 
            // Label69
            // 
            this.Label69.Height = 0.09375F;
            this.Label69.HyperLink = null;
            this.Label69.Left = 0.02083333F;
            this.Label69.Name = "Label69";
            this.Label69.Style = "font-size: 5pt; text-align: left";
            this.Label69.Text = "1 Name of employee (first name, middle initial, last name)";
            this.Label69.Top = 5.0625F;
            this.Label69.Width = 2.083333F;
            // 
            // Label70
            // 
            this.Label70.Height = 0.09375F;
            this.Label70.HyperLink = null;
            this.Label70.Left = 2.375F;
            this.Label70.Name = "Label70";
            this.Label70.Style = "font-size: 5pt; text-align: left";
            this.Label70.Text = "2 Social Security number (SSN)";
            this.Label70.Top = 5.0625F;
            this.Label70.Width = 1.333333F;
            // 
            // Label71
            // 
            this.Label71.Height = 0.09375F;
            this.Label71.HyperLink = null;
            this.Label71.Left = 3.833333F;
            this.Label71.Name = "Label71";
            this.Label71.Style = "font-size: 5pt; text-align: left";
            this.Label71.Text = "7 Name of employer";
            this.Label71.Top = 5.0625F;
            this.Label71.Width = 1.333333F;
            // 
            // Label72
            // 
            this.Label72.Height = 0.09375F;
            this.Label72.HyperLink = null;
            this.Label72.Left = 6.1875F;
            this.Label72.Name = "Label72";
            this.Label72.Style = "font-size: 5.5pt; text-align: left";
            this.Label72.Text = "8 Employer Identification Number";
            this.Label72.Top = 5.0625F;
            this.Label72.Width = 1.208333F;
            // 
            // Label73
            // 
            this.Label73.Height = 0.09375F;
            this.Label73.HyperLink = null;
            this.Label73.Left = 6.1875F;
            this.Label73.Name = "Label73";
            this.Label73.Style = "font-size: 5pt; text-align: left";
            this.Label73.Text = "10 Contact telephone number";
            this.Label73.Top = 5.3125F;
            this.Label73.Width = 1.333333F;
            // 
            // Label74
            // 
            this.Label74.Height = 0.09375F;
            this.Label74.HyperLink = null;
            this.Label74.Left = 6.1875F;
            this.Label74.Name = "Label74";
            this.Label74.Style = "font-size: 5pt; text-align: left";
            this.Label74.Text = "13 Country and ZIP or foreign postal code";
            this.Label74.Top = 5.5625F;
            this.Label74.Width = 1.3125F;
            // 
            // Label75
            // 
            this.Label75.Height = 0.09375F;
            this.Label75.HyperLink = null;
            this.Label75.Left = 3.833333F;
            this.Label75.Name = "Label75";
            this.Label75.Style = "font-size: 5pt; text-align: left";
            this.Label75.Text = "11 City or town";
            this.Label75.Top = 5.5625F;
            this.Label75.Width = 0.9583333F;
            // 
            // Label76
            // 
            this.Label76.Height = 0.09375F;
            this.Label76.HyperLink = null;
            this.Label76.Left = 4.958333F;
            this.Label76.Name = "Label76";
            this.Label76.Style = "font-size: 5pt; text-align: left";
            this.Label76.Text = "12 State or province";
            this.Label76.Top = 5.5625F;
            this.Label76.Width = 1.145833F;
            // 
            // Line47
            // 
            this.Line47.Height = 0F;
            this.Line47.Left = 0F;
            this.Line47.LineWeight = 0F;
            this.Line47.Name = "Line47";
            this.Line47.Top = 5.53125F;
            this.Line47.Width = 7.46875F;
            this.Line47.X1 = 0F;
            this.Line47.X2 = 7.46875F;
            this.Line47.Y1 = 5.53125F;
            this.Line47.Y2 = 5.53125F;
            // 
            // Line48
            // 
            this.Line48.Height = 0F;
            this.Line48.Left = 0F;
            this.Line48.LineWeight = 0F;
            this.Line48.Name = "Line48";
            this.Line48.Top = 5.28125F;
            this.Line48.Width = 7.46875F;
            this.Line48.X1 = 0F;
            this.Line48.X2 = 7.46875F;
            this.Line48.Y1 = 5.28125F;
            this.Line48.Y2 = 5.28125F;
            // 
            // Line50
            // 
            this.Line50.Height = 0.2916665F;
            this.Line50.Left = 4.916667F;
            this.Line50.LineWeight = 0F;
            this.Line50.Name = "Line50";
            this.Line50.Top = 5.53125F;
            this.Line50.Width = 0F;
            this.Line50.X1 = 4.916667F;
            this.Line50.X2 = 4.916667F;
            this.Line50.Y1 = 5.53125F;
            this.Line50.Y2 = 5.822917F;
            // 
            // Line51
            // 
            this.Line51.Height = 0.2916665F;
            this.Line51.Left = 1.125F;
            this.Line51.LineWeight = 0F;
            this.Line51.Name = "Line51";
            this.Line51.Top = 5.53125F;
            this.Line51.Width = 0F;
            this.Line51.X1 = 1.125F;
            this.Line51.X2 = 1.125F;
            this.Line51.Y1 = 5.53125F;
            this.Line51.Y2 = 5.822917F;
            // 
            // Line52
            // 
            this.Line52.Height = 0.2916665F;
            this.Line52.Left = 2.375F;
            this.Line52.LineWeight = 0F;
            this.Line52.Name = "Line52";
            this.Line52.Top = 5.53125F;
            this.Line52.Width = 0F;
            this.Line52.X1 = 2.375F;
            this.Line52.X2 = 2.375F;
            this.Line52.Y1 = 5.53125F;
            this.Line52.Y2 = 5.822917F;
            // 
            // Line53
            // 
            this.Line53.Height = 0.7916665F;
            this.Line53.Left = 6.125F;
            this.Line53.LineWeight = 0F;
            this.Line53.Name = "Line53";
            this.Line53.Top = 5.03125F;
            this.Line53.Width = 0F;
            this.Line53.X1 = 6.125F;
            this.Line53.X2 = 6.125F;
            this.Line53.Y1 = 5.03125F;
            this.Line53.Y2 = 5.822917F;
            // 
            // Line54
            // 
            this.Line54.Height = 0.2604165F;
            this.Line54.Left = 2.375F;
            this.Line54.LineWeight = 0F;
            this.Line54.Name = "Line54";
            this.Line54.Top = 5.03125F;
            this.Line54.Width = 0F;
            this.Line54.X1 = 2.375F;
            this.Line54.X2 = 2.375F;
            this.Line54.Y1 = 5.03125F;
            this.Line54.Y2 = 5.291667F;
            // 
            // Label77
            // 
            this.Label77.Height = 0.1354167F;
            this.Label77.HyperLink = null;
            this.Label77.Left = 0F;
            this.Label77.Name = "Label77";
            this.Label77.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label77.Text = "Part I";
            this.Label77.Top = 4.895833F;
            this.Label77.Width = 0.34375F;
            // 
            // Line49
            // 
            this.Line49.Height = 0F;
            this.Line49.Left = 0F;
            this.Line49.LineWeight = 0F;
            this.Line49.Name = "Line49";
            this.Line49.Top = 5.03125F;
            this.Line49.Width = 7.46875F;
            this.Line49.X1 = 0F;
            this.Line49.X2 = 7.46875F;
            this.Line49.Y1 = 5.03125F;
            this.Line49.Y2 = 5.03125F;
            // 
            // Label78
            // 
            this.Label78.Height = 0.1145833F;
            this.Label78.HyperLink = null;
            this.Label78.Left = 0.3854167F;
            this.Label78.Name = "Label78";
            this.Label78.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label78.Text = "Employee";
            this.Label78.Top = 4.909722F;
            this.Label78.Width = 0.6458333F;
            // 
            // Label79
            // 
            this.Label79.Height = 0.1145833F;
            this.Label79.HyperLink = null;
            this.Label79.Left = 4.333333F;
            this.Label79.Name = "Label79";
            this.Label79.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label79.Text = "Applicable Large Employer Member (Employer)";
            this.Label79.Top = 4.909722F;
            this.Label79.Width = 2.520833F;
            // 
            // Line56
            // 
            this.Line56.Height = 0.3819447F;
            this.Line56.Left = 1.125F;
            this.Line56.LineWeight = 1F;
            this.Line56.Name = "Line56";
            this.Line56.Top = 4.513889F;
            this.Line56.Width = 0F;
            this.Line56.X1 = 1.125F;
            this.Line56.X2 = 1.125F;
            this.Line56.Y1 = 4.513889F;
            this.Line56.Y2 = 4.895833F;
            // 
            // Label80
            // 
            this.Label80.Height = 0.09375F;
            this.Label80.HyperLink = null;
            this.Label80.Left = 0F;
            this.Label80.Name = "Label80";
            this.Label80.Style = "font-size: 5pt; text-align: left";
            this.Label80.Text = "Form";
            this.Label80.Top = 4.614583F;
            this.Label80.Width = 0.3333333F;
            // 
            // Label81
            // 
            this.Label81.Height = 0.09375F;
            this.Label81.HyperLink = null;
            this.Label81.Left = 0F;
            this.Label81.Name = "Label81";
            this.Label81.Style = "font-size: 5pt; text-align: left";
            this.Label81.Text = "Department of the Treasury";
            this.Label81.Top = 4.708333F;
            this.Label81.Width = 0.9583333F;
            // 
            // Label82
            // 
            this.Label82.Height = 0.09375F;
            this.Label82.HyperLink = null;
            this.Label82.Left = 0F;
            this.Label82.Name = "Label82";
            this.Label82.Style = "font-size: 5pt; text-align: left";
            this.Label82.Text = "Internal Revenue Service";
            this.Label82.Top = 4.802083F;
            this.Label82.Width = 1.020833F;
            // 
            // Line55
            // 
            this.Line55.Height = 0F;
            this.Line55.Left = 0F;
            this.Line55.LineWeight = 1F;
            this.Line55.Name = "Line55";
            this.Line55.Top = 4.895833F;
            this.Line55.Width = 7.46875F;
            this.Line55.X1 = 0F;
            this.Line55.X2 = 7.46875F;
            this.Line55.Y1 = 4.895833F;
            this.Line55.Y2 = 4.895833F;
            // 
            // Label85
            // 
            this.Label85.Height = 0.1770833F;
            this.Label85.HyperLink = null;
            this.Label85.Left = 1.40625F;
            this.Label85.Name = "Label85";
            this.Label85.Style = "font-family: \'Franklin Gothic Medium\'; font-size: 10pt; font-weight: bold; text-a" +
    "lign: left";
            this.Label85.Text = "Employer-Provided Health Insurance Offer and Coverage";
            this.Label85.Top = 4.527778F;
            this.Label85.Width = 3.833333F;
            // 
            // Shape83
            // 
            this.Shape83.Height = 0.1458333F;
            this.Shape83.Left = 5.5F;
            this.Shape83.LineWeight = 0F;
            this.Shape83.Name = "Shape83";
            this.Shape83.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape83.Top = 4.364583F;
            this.Shape83.Width = 0.1388889F;
            // 
            // Shape84
            // 
            this.Shape84.Height = 0.1458333F;
            this.Shape84.Left = 5.5F;
            this.Shape84.LineWeight = 0F;
            this.Shape84.Name = "Shape84";
            this.Shape84.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape84.Top = 4.635417F;
            this.Shape84.Width = 0.1388889F;
            // 
            // Line57
            // 
            this.Line57.Height = 0.385417F;
            this.Line57.Left = 6.34375F;
            this.Line57.LineWeight = 1F;
            this.Line57.Name = "Line57";
            this.Line57.Top = 4.510417F;
            this.Line57.Width = 0F;
            this.Line57.X1 = 6.34375F;
            this.Line57.X2 = 6.34375F;
            this.Line57.Y1 = 4.510417F;
            this.Line57.Y2 = 4.895833F;
            // 
            // Label86
            // 
            this.Label86.Height = 0.09375F;
            this.Label86.HyperLink = null;
            this.Label86.Left = 1.513889F;
            this.Label86.Name = "Label86";
            this.Label86.Style = "font-size: 6pt; font-weight: bold; text-align: left";
            this.Label86.Text = "Information about Form 1095-C and its separate instructions is at";
            this.Label86.Top = 4.756945F;
            this.Label86.Width = 2.645833F;
            // 
            // Label87
            // 
            this.Label87.Height = 0.09375F;
            this.Label87.HyperLink = null;
            this.Label87.Left = 4.145833F;
            this.Label87.Name = "Label87";
            this.Label87.Style = "font-size: 6pt; font-style: italic; font-weight: bold; text-align: left";
            this.Label87.Text = "www.irs.gov/form1095c";
            this.Label87.Top = 4.760417F;
            this.Label87.Width = 1.145833F;
            // 
            // Label88
            // 
            this.Label88.Height = 0.1354167F;
            this.Label88.HyperLink = null;
            this.Label88.Left = 5.645833F;
            this.Label88.Name = "Label88";
            this.Label88.Style = "font-size: 7pt";
            this.Label88.Text = "VOID";
            this.Label88.Top = 4.385417F;
            this.Label88.Width = 0.5625F;
            // 
            // Label89
            // 
            this.Label89.Height = 0.1354167F;
            this.Label89.HyperLink = null;
            this.Label89.Left = 5.645833F;
            this.Label89.Name = "Label89";
            this.Label89.Style = "font-size: 7pt";
            this.Label89.Text = "CORRECTED";
            this.Label89.Top = 4.652778F;
            this.Label89.Width = 0.8125F;
            // 
            // Line58
            // 
            this.Line58.Height = 0F;
            this.Line58.Left = 6.34375F;
            this.Line58.LineWeight = 0F;
            this.Line58.Name = "Line58";
            this.Line58.Top = 4.645833F;
            this.Line58.Width = 1.125F;
            this.Line58.X1 = 6.34375F;
            this.Line58.X2 = 7.46875F;
            this.Line58.Y1 = 4.645833F;
            this.Line58.Y2 = 4.645833F;
            // 
            // Label90
            // 
            this.Label90.Height = 0.09375F;
            this.Label90.HyperLink = null;
            this.Label90.Left = 6.520833F;
            this.Label90.Name = "Label90";
            this.Label90.Style = "font-size: 5.5pt; text-align: left";
            this.Label90.Text = "OMB No. 1545-2251";
            this.Label90.Top = 4.555555F;
            this.Label90.Width = 0.8333333F;
            // 
            // Label92
            // 
            this.Label92.Height = 0.1145833F;
            this.Label92.HyperLink = null;
            this.Label92.Left = 4.6875F;
            this.Label92.Name = "Label92";
            this.Label92.Style = "font-family: \'Arial\'; font-size: 6.75pt; text-align: left";
            this.Label92.Text = "(Enter 2-digit number):";
            this.Label92.Top = 5.885417F;
            this.Label92.Width = 1.083333F;
            // 
            // Label93
            // 
            this.Label93.Height = 0.1354167F;
            this.Label93.HyperLink = null;
            this.Label93.Left = 6.895833F;
            this.Label93.Name = "Label93";
            this.Label93.Style = "font-family: \'OCR A Extended\'; font-size: 8.5pt; text-align: right";
            this.Label93.Text = "600118";
            this.Label93.Top = 4.395833F;
            this.Label93.Width = 0.5625F;
            // 
            // Label95
            // 
            this.Label95.Height = 0.1145833F;
            this.Label95.HyperLink = null;
            this.Label95.Left = 1.375F;
            this.Label95.Name = "Label95";
            this.Label95.Style = "font-family: \'Marlett\'; font-size: 10pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label95.Text = "4";
            this.Label95.Top = 4.739583F;
            this.Label95.Width = 0.2708333F;
            // 
            // Line59
            // 
            this.Line59.Height = 0.2465281F;
            this.Line59.Left = 3.791667F;
            this.Line59.LineWeight = 0F;
            this.Line59.Name = "Line59";
            this.Line59.Top = 5.822917F;
            this.Line59.Width = 0F;
            this.Line59.X1 = 3.791667F;
            this.Line59.X2 = 3.791667F;
            this.Line59.Y1 = 5.822917F;
            this.Line59.Y2 = 6.069445F;
            // 
            // Line20
            // 
            this.Line20.Height = 2.333333F;
            this.Line20.Left = 6.868055F;
            this.Line20.LineWeight = 0F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 7.729167F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 6.868055F;
            this.Line20.X2 = 6.868055F;
            this.Line20.Y1 = 7.729167F;
            this.Line20.Y2 = 10.0625F;
            // 
            // Line21
            // 
            this.Line21.Height = 2.333333F;
            this.Line21.Left = 7.163195F;
            this.Line21.LineWeight = 0F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 7.729167F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 7.163195F;
            this.Line21.X2 = 7.163195F;
            this.Line21.Y1 = 7.729167F;
            this.Line21.Y2 = 10.0625F;
            // 
            // Line17
            // 
            this.Line17.Height = 2.333333F;
            this.Line17.Left = 5.982639F;
            this.Line17.LineWeight = 0F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 7.729167F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 5.982639F;
            this.Line17.X2 = 5.982639F;
            this.Line17.Y1 = 7.729167F;
            this.Line17.Y2 = 10.0625F;
            // 
            // Line16
            // 
            this.Line16.Height = 2.333333F;
            this.Line16.Left = 5.6875F;
            this.Line16.LineWeight = 0F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 7.729167F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 5.6875F;
            this.Line16.X2 = 5.6875F;
            this.Line16.Y1 = 7.729167F;
            this.Line16.Y2 = 10.0625F;
            // 
            // Line15
            // 
            this.Line15.Height = 2.333333F;
            this.Line15.Left = 5.392361F;
            this.Line15.LineWeight = 0F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 7.729167F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 5.392361F;
            this.Line15.X2 = 5.392361F;
            this.Line15.Y1 = 7.729167F;
            this.Line15.Y2 = 10.0625F;
            // 
            // Line13
            // 
            this.Line13.Height = 2.333333F;
            this.Line13.Left = 4.802083F;
            this.Line13.LineWeight = 0F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 7.729167F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 4.802083F;
            this.Line13.X2 = 4.802083F;
            this.Line13.Y1 = 7.729167F;
            this.Line13.Y2 = 10.0625F;
            // 
            // Line12
            // 
            this.Line12.Height = 2.333333F;
            this.Line12.Left = 4.506945F;
            this.Line12.LineWeight = 0F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 7.729167F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 4.506945F;
            this.Line12.X2 = 4.506945F;
            this.Line12.Y1 = 7.729167F;
            this.Line12.Y2 = 10.0625F;
            // 
            // Line11
            // 
            this.Line11.Height = 2.333333F;
            this.Line11.Left = 4.211805F;
            this.Line11.LineWeight = 0F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 7.729167F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 4.211805F;
            this.Line11.X2 = 4.211805F;
            this.Line11.Y1 = 7.729167F;
            this.Line11.Y2 = 10.0625F;
            // 
            // Line10
            // 
            this.Line10.Height = 2.472222F;
            this.Line10.Left = 3.916667F;
            this.Line10.LineWeight = 0F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 7.590278F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 3.916667F;
            this.Line10.X2 = 3.916667F;
            this.Line10.Y1 = 7.590278F;
            this.Line10.Y2 = 10.0625F;
            // 
            // Line30
            // 
            this.Line30.Height = 1.243055F;
            this.Line30.Left = 1.75F;
            this.Line30.LineWeight = 0F;
            this.Line30.Name = "Line30";
            this.Line30.Top = 6.069445F;
            this.Line30.Width = 0F;
            this.Line30.X1 = 1.75F;
            this.Line30.X2 = 1.75F;
            this.Line30.Y1 = 6.069445F;
            this.Line30.Y2 = 7.3125F;
            // 
            // Line32
            // 
            this.Line32.Height = 1.243055F;
            this.Line32.Left = 2.791667F;
            this.Line32.LineWeight = 0F;
            this.Line32.Name = "Line32";
            this.Line32.Top = 6.069445F;
            this.Line32.Width = 0F;
            this.Line32.X1 = 2.791667F;
            this.Line32.X2 = 2.791667F;
            this.Line32.Y1 = 6.069445F;
            this.Line32.Y2 = 7.3125F;
            // 
            // Line31
            // 
            this.Line31.Height = 1.243055F;
            this.Line31.Left = 2.270833F;
            this.Line31.LineWeight = 0F;
            this.Line31.Name = "Line31";
            this.Line31.Top = 6.069445F;
            this.Line31.Width = 0F;
            this.Line31.X1 = 2.270833F;
            this.Line31.X2 = 2.270833F;
            this.Line31.Y1 = 6.069445F;
            this.Line31.Y2 = 7.3125F;
            // 
            // Line34
            // 
            this.Line34.Height = 1.243055F;
            this.Line34.Left = 3.833333F;
            this.Line34.LineWeight = 0F;
            this.Line34.Name = "Line34";
            this.Line34.Top = 6.069445F;
            this.Line34.Width = 0F;
            this.Line34.X1 = 3.833333F;
            this.Line34.X2 = 3.833333F;
            this.Line34.Y1 = 6.069445F;
            this.Line34.Y2 = 7.3125F;
            // 
            // Line35
            // 
            this.Line35.Height = 1.243055F;
            this.Line35.Left = 4.354167F;
            this.Line35.LineWeight = 0F;
            this.Line35.Name = "Line35";
            this.Line35.Top = 6.069445F;
            this.Line35.Width = 0F;
            this.Line35.X1 = 4.354167F;
            this.Line35.X2 = 4.354167F;
            this.Line35.Y1 = 6.069445F;
            this.Line35.Y2 = 7.3125F;
            // 
            // Line36
            // 
            this.Line36.Height = 1.243055F;
            this.Line36.Left = 4.875F;
            this.Line36.LineWeight = 0F;
            this.Line36.Name = "Line36";
            this.Line36.Top = 6.069445F;
            this.Line36.Width = 0F;
            this.Line36.X1 = 4.875F;
            this.Line36.X2 = 4.875F;
            this.Line36.Y1 = 6.069445F;
            this.Line36.Y2 = 7.3125F;
            // 
            // Line38
            // 
            this.Line38.Height = 1.243055F;
            this.Line38.Left = 5.916667F;
            this.Line38.LineWeight = 0F;
            this.Line38.Name = "Line38";
            this.Line38.Top = 6.069445F;
            this.Line38.Width = 0F;
            this.Line38.X1 = 5.916667F;
            this.Line38.X2 = 5.916667F;
            this.Line38.Y1 = 6.069445F;
            this.Line38.Y2 = 7.3125F;
            // 
            // Line40
            // 
            this.Line40.Height = 1.243055F;
            this.Line40.Left = 6.958333F;
            this.Line40.LineWeight = 0F;
            this.Line40.Name = "Line40";
            this.Line40.Top = 6.069445F;
            this.Line40.Width = 0F;
            this.Line40.X1 = 6.958333F;
            this.Line40.X2 = 6.958333F;
            this.Line40.Y1 = 6.069445F;
            this.Line40.Y2 = 7.3125F;
            // 
            // Line33
            // 
            this.Line33.Height = 1.243055F;
            this.Line33.Left = 3.3125F;
            this.Line33.LineWeight = 0F;
            this.Line33.Name = "Line33";
            this.Line33.Top = 6.069445F;
            this.Line33.Width = 0F;
            this.Line33.X1 = 3.3125F;
            this.Line33.X2 = 3.3125F;
            this.Line33.Y1 = 6.069445F;
            this.Line33.Y2 = 7.3125F;
            // 
            // Line37
            // 
            this.Line37.Height = 1.243055F;
            this.Line37.Left = 5.395833F;
            this.Line37.LineWeight = 0F;
            this.Line37.Name = "Line37";
            this.Line37.Top = 6.069445F;
            this.Line37.Width = 0F;
            this.Line37.X1 = 5.395833F;
            this.Line37.X2 = 5.395833F;
            this.Line37.Y1 = 6.069445F;
            this.Line37.Y2 = 7.3125F;
            // 
            // Line39
            // 
            this.Line39.Height = 1.243055F;
            this.Line39.Left = 6.4375F;
            this.Line39.LineWeight = 0F;
            this.Line39.Name = "Line39";
            this.Line39.Top = 6.069445F;
            this.Line39.Width = 0F;
            this.Line39.X1 = 6.4375F;
            this.Line39.X2 = 6.4375F;
            this.Line39.Y1 = 6.069445F;
            this.Line39.Y2 = 7.3125F;
            // 
            // Label96
            // 
            this.Label96.Height = 0.15625F;
            this.Label96.HyperLink = null;
            this.Label96.Left = 1.8125F;
            this.Label96.Name = "Label96";
            this.Label96.Style = "font-size: 5.5pt; text-align: center";
            this.Label96.Text = "(b) SSN or other TIN";
            this.Label96.Top = 7.6875F;
            this.Label96.Width = 0.8333333F;
            // 
            // Label97
            // 
            this.Label97.Height = 0.28125F;
            this.Label97.HyperLink = null;
            this.Label97.Left = 2.75F;
            this.Label97.Name = "Label97";
            this.Label97.Style = "font-size: 5.5pt; text-align: center";
            this.Label97.Text = "(c) DOB (If SSN or other TIN is not available)";
            this.Label97.Top = 7.625F;
            this.Label97.Width = 0.6458333F;
            // 
            // Line60
            // 
            this.Line60.Height = 2.1875F;
            this.Line60.Left = 1.104167F;
            this.Line60.LineWeight = 0F;
            this.Line60.Name = "Line60";
            this.Line60.Top = 7.875F;
            this.Line60.Width = 0F;
            this.Line60.X1 = 1.104167F;
            this.Line60.X2 = 1.104167F;
            this.Line60.Y1 = 7.875F;
            this.Line60.Y2 = 10.0625F;
            // 
            // Line61
            // 
            this.Line61.Height = 2.1875F;
            this.Line61.Left = 0.8125F;
            this.Line61.LineWeight = 0F;
            this.Line61.Name = "Line61";
            this.Line61.Top = 7.875F;
            this.Line61.Width = 0F;
            this.Line61.X1 = 0.8125F;
            this.Line61.X2 = 0.8125F;
            this.Line61.Y1 = 7.875F;
            this.Line61.Y2 = 10.0625F;
            // 
            // txtCoveredLastName1
            // 
            this.txtCoveredLastName1.Height = 0.1770833F;
            this.txtCoveredLastName1.Left = 1.125F;
            this.txtCoveredLastName1.Name = "txtCoveredLastName1";
            this.txtCoveredLastName1.Style = "font-size: 8.5pt";
            this.txtCoveredLastName1.Text = null;
            this.txtCoveredLastName1.Top = 7.9375F;
            this.txtCoveredLastName1.Width = 0.625F;
            // 
            // txtCoveredLastName2
            // 
            this.txtCoveredLastName2.Height = 0.1770833F;
            this.txtCoveredLastName2.Left = 1.125F;
            this.txtCoveredLastName2.Name = "txtCoveredLastName2";
            this.txtCoveredLastName2.Style = "font-size: 8.5pt";
            this.txtCoveredLastName2.Text = null;
            this.txtCoveredLastName2.Top = 8.3125F;
            this.txtCoveredLastName2.Width = 0.625F;
            // 
            // txtCoveredLastName3
            // 
            this.txtCoveredLastName3.Height = 0.1770833F;
            this.txtCoveredLastName3.Left = 1.125F;
            this.txtCoveredLastName3.Name = "txtCoveredLastName3";
            this.txtCoveredLastName3.Style = "font-size: 8.5pt";
            this.txtCoveredLastName3.Text = null;
            this.txtCoveredLastName3.Top = 8.6875F;
            this.txtCoveredLastName3.Width = 0.625F;
            // 
            // txtCoveredLastName4
            // 
            this.txtCoveredLastName4.Height = 0.1770833F;
            this.txtCoveredLastName4.Left = 1.125F;
            this.txtCoveredLastName4.Name = "txtCoveredLastName4";
            this.txtCoveredLastName4.Style = "font-size: 8.5pt";
            this.txtCoveredLastName4.Text = null;
            this.txtCoveredLastName4.Top = 9.0625F;
            this.txtCoveredLastName4.Width = 0.625F;
            // 
            // txtCoveredLastName5
            // 
            this.txtCoveredLastName5.Height = 0.1770833F;
            this.txtCoveredLastName5.Left = 1.125F;
            this.txtCoveredLastName5.Name = "txtCoveredLastName5";
            this.txtCoveredLastName5.Style = "font-size: 8.5pt";
            this.txtCoveredLastName5.Text = null;
            this.txtCoveredLastName5.Top = 9.4375F;
            this.txtCoveredLastName5.Width = 0.625F;
            // 
            // txtCoveredLastName6
            // 
            this.txtCoveredLastName6.Height = 0.1770833F;
            this.txtCoveredLastName6.Left = 1.125F;
            this.txtCoveredLastName6.Name = "txtCoveredLastName6";
            this.txtCoveredLastName6.Style = "font-size: 8.5pt";
            this.txtCoveredLastName6.Text = null;
            this.txtCoveredLastName6.Top = 9.8125F;
            this.txtCoveredLastName6.Width = 0.625F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 0.875F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 7.9375F;
            this.txtCoveredMiddle1.Width = 0.1875F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 0.875F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 8.3125F;
            this.txtCoveredMiddle2.Width = 0.1875F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 0.875F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 8.6875F;
            this.txtCoveredMiddle3.Width = 0.1875F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 0.875F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 9.0625F;
            this.txtCoveredMiddle4.Width = 0.1875F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 0.875F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 9.4375F;
            this.txtCoveredMiddle5.Width = 0.1875F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 0.875F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 9.8125F;
            this.txtCoveredMiddle6.Width = 0.1875F;
            // 
            // Line62
            // 
            this.Line62.Height = 0.1527777F;
            this.Line62.Left = 1.0625F;
            this.Line62.LineWeight = 0F;
            this.Line62.Name = "Line62";
            this.Line62.Top = 5.138889F;
            this.Line62.Width = 0F;
            this.Line62.X1 = 1.0625F;
            this.Line62.X2 = 1.0625F;
            this.Line62.Y1 = 5.138889F;
            this.Line62.Y2 = 5.291667F;
            // 
            // Line63
            // 
            this.Line63.Height = 0.1527777F;
            this.Line63.Left = 1.3125F;
            this.Line63.LineWeight = 0F;
            this.Line63.Name = "Line63";
            this.Line63.Top = 5.138889F;
            this.Line63.Width = 0F;
            this.Line63.X1 = 1.3125F;
            this.Line63.X2 = 1.3125F;
            this.Line63.Y1 = 5.138889F;
            this.Line63.Y2 = 5.291667F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.083333F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Style = "font-size: 8.5pt";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 5.135417F;
            this.txtMiddle.Width = 0.1875F;
            // 
            // txtLast
            // 
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 1.395833F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Style = "font-size: 8.5pt";
            this.txtLast.Text = null;
            this.txtLast.Top = 5.135417F;
            this.txtLast.Width = 0.9375F;
            // 
            // rpt1095C2018BlankPortraitPage1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStartMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredIndividuals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerReturnAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl12Months;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape43;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape44;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape45;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape46;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape69;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape70;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape71;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape72;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape73;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape74;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape75;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape76;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape79;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape82;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape83;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape84;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
    }
}
