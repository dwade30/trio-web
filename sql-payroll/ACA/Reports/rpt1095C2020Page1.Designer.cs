﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016Page1.
	/// </summary>
	partial class rpt1095C2020Page1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2020Page1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Zip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtName,
            this.txtSSN,
            this.txtEmployer,
            this.txtEIN,
            this.txtAddress,
            this.txtEmployerAddress,
            this.txtPhone,
            this.txtStartMonth,
            this.txtAll12Box14,
            this.txtBox14_1,
            this.txtBox14_2,
            this.txtBox14_3,
            this.txtBox14_4,
            this.txtBox14_5,
            this.txtBox14_6,
            this.txtBox14_7,
            this.txtBox14_8,
            this.txtBox14_9,
            this.txtBox14_10,
            this.txtBox14_11,
            this.txtBox14_12,
            this.txtAll12Box15,
            this.txtBox15_1,
            this.txtBox15_2,
            this.txtBox15_3,
            this.txtBox15_4,
            this.txtBox15_5,
            this.txtBox15_6,
            this.txtBox15_7,
            this.txtBox15_8,
            this.txtBox15_9,
            this.txtBox15_10,
            this.txtBox15_11,
            this.txtBox15_12,
            this.txtAll12Box16,
            this.txtBox16_1,
            this.txtBox16_2,
            this.txtBox16_3,
            this.txtBox16_4,
            this.txtBox16_5,
            this.txtBox16_6,
            this.txtBox16_7,
            this.txtBox16_8,
            this.txtBox16_9,
            this.txtBox16_10,
            this.txtBox16_11,
            this.txtBox16_12,
            this.txtCity,
            this.txtState,
            this.txtPostalCode,
            this.txtEmployerCity,
            this.txtEmployerState,
            this.txtEmployerPostalCode,
            this.txtMiddle,
            this.txtLast,
            this.txtAll12Zip,
            this.txtZip_1,
            this.txtZip_2,
            this.txtZip_3,
            this.txtZip_4,
            this.txtZip_5,
            this.txtZip_6,
            this.txtZip_7,
            this.txtZip_8,
            this.txtZip_9,
            this.txtZip_10,
            this.txtZip_11,
            this.txtZip_12});
            this.Detail.Height = 7.208333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.08333335F;
            this.txtName.Name = "txtName";
            this.txtName.Top = 1.292167F;
            this.txtName.Width = 1.395833F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 3.333333F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 1.292167F;
            this.txtSSN.Width = 1.666667F;
            // 
            // txtEmployer
            // 
            this.txtEmployer.Height = 0.1770833F;
            this.txtEmployer.Left = 5.083333F;
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Text = null;
            this.txtEmployer.Top = 1.292167F;
            this.txtEmployer.Width = 3.083333F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 8.364583F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 1.292167F;
            this.txtEIN.Width = 1.666667F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.08333335F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 1.6255F;
            this.txtAddress.Width = 4.833333F;
            // 
            // txtEmployerAddress
            // 
            this.txtEmployerAddress.Height = 0.1770833F;
            this.txtEmployerAddress.Left = 5.083333F;
            this.txtEmployerAddress.Name = "txtEmployerAddress";
            this.txtEmployerAddress.Text = null;
            this.txtEmployerAddress.Top = 1.6255F;
            this.txtEmployerAddress.Width = 3.083333F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1770833F;
            this.txtPhone.Left = 8.364583F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 1.6255F;
            this.txtPhone.Width = 1.604167F;
            // 
            // txtStartMonth
            // 
            this.txtStartMonth.Height = 0.1770833F;
            this.txtStartMonth.Left = 9.323001F;
            this.txtStartMonth.Name = "txtStartMonth";
            this.txtStartMonth.Text = null;
            this.txtStartMonth.Top = 2.106F;
            this.txtStartMonth.Width = 0.3979995F;
            // 
            // txtAll12Box14
            // 
            this.txtAll12Box14.Height = 0.1770833F;
            this.txtAll12Box14.Left = 1.111528F;
            this.txtAll12Box14.Name = "txtAll12Box14";
            this.txtAll12Box14.Text = null;
            this.txtAll12Box14.Top = 2.532995F;
            this.txtAll12Box14.Width = 0.5F;
            // 
            // txtBox14_1
            // 
            this.txtBox14_1.Height = 0.1770833F;
            this.txtBox14_1.Left = 1.801806F;
            this.txtBox14_1.Name = "txtBox14_1";
            this.txtBox14_1.Text = null;
            this.txtBox14_1.Top = 2.532995F;
            this.txtBox14_1.Width = 0.5F;
            // 
            // txtBox14_2
            // 
            this.txtBox14_2.Height = 0.1770833F;
            this.txtBox14_2.Left = 2.492084F;
            this.txtBox14_2.Name = "txtBox14_2";
            this.txtBox14_2.Text = null;
            this.txtBox14_2.Top = 2.532995F;
            this.txtBox14_2.Width = 0.5F;
            // 
            // txtBox14_3
            // 
            this.txtBox14_3.Height = 0.1770833F;
            this.txtBox14_3.Left = 3.182361F;
            this.txtBox14_3.Name = "txtBox14_3";
            this.txtBox14_3.Text = null;
            this.txtBox14_3.Top = 2.532995F;
            this.txtBox14_3.Width = 0.5F;
            // 
            // txtBox14_4
            // 
            this.txtBox14_4.Height = 0.1770833F;
            this.txtBox14_4.Left = 3.872639F;
            this.txtBox14_4.Name = "txtBox14_4";
            this.txtBox14_4.Text = null;
            this.txtBox14_4.Top = 2.532995F;
            this.txtBox14_4.Width = 0.5F;
            // 
            // txtBox14_5
            // 
            this.txtBox14_5.Height = 0.1770833F;
            this.txtBox14_5.Left = 4.562917F;
            this.txtBox14_5.Name = "txtBox14_5";
            this.txtBox14_5.Text = null;
            this.txtBox14_5.Top = 2.532995F;
            this.txtBox14_5.Width = 0.5F;
            // 
            // txtBox14_6
            // 
            this.txtBox14_6.Height = 0.1770833F;
            this.txtBox14_6.Left = 5.253195F;
            this.txtBox14_6.Name = "txtBox14_6";
            this.txtBox14_6.Text = null;
            this.txtBox14_6.Top = 2.532995F;
            this.txtBox14_6.Width = 0.5F;
            // 
            // txtBox14_7
            // 
            this.txtBox14_7.Height = 0.1770833F;
            this.txtBox14_7.Left = 5.964305F;
            this.txtBox14_7.Name = "txtBox14_7";
            this.txtBox14_7.Text = null;
            this.txtBox14_7.Top = 2.532995F;
            this.txtBox14_7.Width = 0.5F;
            // 
            // txtBox14_8
            // 
            this.txtBox14_8.Height = 0.1770833F;
            this.txtBox14_8.Left = 6.63375F;
            this.txtBox14_8.Name = "txtBox14_8";
            this.txtBox14_8.Text = null;
            this.txtBox14_8.Top = 2.532995F;
            this.txtBox14_8.Width = 0.5F;
            // 
            // txtBox14_9
            // 
            this.txtBox14_9.Height = 0.1770833F;
            this.txtBox14_9.Left = 7.324028F;
            this.txtBox14_9.Name = "txtBox14_9";
            this.txtBox14_9.Text = null;
            this.txtBox14_9.Top = 2.532995F;
            this.txtBox14_9.Width = 0.5F;
            // 
            // txtBox14_10
            // 
            this.txtBox14_10.Height = 0.1770833F;
            this.txtBox14_10.Left = 8.014305F;
            this.txtBox14_10.Name = "txtBox14_10";
            this.txtBox14_10.Text = null;
            this.txtBox14_10.Top = 2.532995F;
            this.txtBox14_10.Width = 0.5F;
            // 
            // txtBox14_11
            // 
            this.txtBox14_11.Height = 0.1770833F;
            this.txtBox14_11.Left = 8.704583F;
            this.txtBox14_11.Name = "txtBox14_11";
            this.txtBox14_11.Text = null;
            this.txtBox14_11.Top = 2.532995F;
            this.txtBox14_11.Width = 0.5F;
            // 
            // txtBox14_12
            // 
            this.txtBox14_12.Height = 0.1770833F;
            this.txtBox14_12.Left = 9.394861F;
            this.txtBox14_12.Name = "txtBox14_12";
            this.txtBox14_12.Text = null;
            this.txtBox14_12.Top = 2.532995F;
            this.txtBox14_12.Width = 0.5F;
            // 
            // txtAll12Box15
            // 
            this.txtAll12Box15.Height = 0.1770833F;
            this.txtAll12Box15.Left = 1.188F;
            this.txtAll12Box15.Name = "txtAll12Box15";
            this.txtAll12Box15.Text = null;
            this.txtAll12Box15.Top = 3.055F;
            this.txtAll12Box15.Width = 0.5F;
            // 
            // txtBox15_1
            // 
            this.txtBox15_1.Height = 0.1770833F;
            this.txtBox15_1.Left = 1.878278F;
            this.txtBox15_1.Name = "txtBox15_1";
            this.txtBox15_1.Text = null;
            this.txtBox15_1.Top = 3.055F;
            this.txtBox15_1.Width = 0.5F;
            // 
            // txtBox15_2
            // 
            this.txtBox15_2.Height = 0.1770833F;
            this.txtBox15_2.Left = 2.568556F;
            this.txtBox15_2.Name = "txtBox15_2";
            this.txtBox15_2.Text = null;
            this.txtBox15_2.Top = 3.055F;
            this.txtBox15_2.Width = 0.5F;
            // 
            // txtBox15_3
            // 
            this.txtBox15_3.Height = 0.1770833F;
            this.txtBox15_3.Left = 3.258833F;
            this.txtBox15_3.Name = "txtBox15_3";
            this.txtBox15_3.Text = null;
            this.txtBox15_3.Top = 3.055F;
            this.txtBox15_3.Width = 0.5F;
            // 
            // txtBox15_4
            // 
            this.txtBox15_4.Height = 0.1770833F;
            this.txtBox15_4.Left = 3.949111F;
            this.txtBox15_4.Name = "txtBox15_4";
            this.txtBox15_4.Text = null;
            this.txtBox15_4.Top = 3.055F;
            this.txtBox15_4.Width = 0.5F;
            // 
            // txtBox15_5
            // 
            this.txtBox15_5.Height = 0.1770833F;
            this.txtBox15_5.Left = 4.63939F;
            this.txtBox15_5.Name = "txtBox15_5";
            this.txtBox15_5.Text = null;
            this.txtBox15_5.Top = 3.055F;
            this.txtBox15_5.Width = 0.5F;
            // 
            // txtBox15_6
            // 
            this.txtBox15_6.Height = 0.1770833F;
            this.txtBox15_6.Left = 5.329668F;
            this.txtBox15_6.Name = "txtBox15_6";
            this.txtBox15_6.Text = null;
            this.txtBox15_6.Top = 3.055F;
            this.txtBox15_6.Width = 0.5F;
            // 
            // txtBox15_7
            // 
            this.txtBox15_7.Height = 0.1770833F;
            this.txtBox15_7.Left = 6.019944F;
            this.txtBox15_7.Name = "txtBox15_7";
            this.txtBox15_7.Text = null;
            this.txtBox15_7.Top = 3.055F;
            this.txtBox15_7.Width = 0.5F;
            // 
            // txtBox15_8
            // 
            this.txtBox15_8.Height = 0.1770833F;
            this.txtBox15_8.Left = 6.710222F;
            this.txtBox15_8.Name = "txtBox15_8";
            this.txtBox15_8.Text = null;
            this.txtBox15_8.Top = 3.055F;
            this.txtBox15_8.Width = 0.5F;
            // 
            // txtBox15_9
            // 
            this.txtBox15_9.Height = 0.1770833F;
            this.txtBox15_9.Left = 7.4005F;
            this.txtBox15_9.Name = "txtBox15_9";
            this.txtBox15_9.Text = null;
            this.txtBox15_9.Top = 3.055F;
            this.txtBox15_9.Width = 0.5F;
            // 
            // txtBox15_10
            // 
            this.txtBox15_10.Height = 0.1770833F;
            this.txtBox15_10.Left = 8.090777F;
            this.txtBox15_10.Name = "txtBox15_10";
            this.txtBox15_10.Text = null;
            this.txtBox15_10.Top = 3.055F;
            this.txtBox15_10.Width = 0.5F;
            // 
            // txtBox15_11
            // 
            this.txtBox15_11.Height = 0.1770833F;
            this.txtBox15_11.Left = 8.781055F;
            this.txtBox15_11.Name = "txtBox15_11";
            this.txtBox15_11.Text = null;
            this.txtBox15_11.Top = 3.055F;
            this.txtBox15_11.Width = 0.5F;
            // 
            // txtBox15_12
            // 
            this.txtBox15_12.Height = 0.1770833F;
            this.txtBox15_12.Left = 9.471333F;
            this.txtBox15_12.Name = "txtBox15_12";
            this.txtBox15_12.Text = null;
            this.txtBox15_12.Top = 3.055F;
            this.txtBox15_12.Width = 0.5F;
            // 
            // txtAll12Box16
            // 
            this.txtAll12Box16.Height = 0.1770833F;
            this.txtAll12Box16.Left = 1.115F;
            this.txtAll12Box16.Name = "txtAll12Box16";
            this.txtAll12Box16.Text = null;
            this.txtAll12Box16.Top = 3.491329F;
            this.txtAll12Box16.Width = 0.5F;
            // 
            // txtBox16_1
            // 
            this.txtBox16_1.Height = 0.1770833F;
            this.txtBox16_1.Left = 1.8025F;
            this.txtBox16_1.Name = "txtBox16_1";
            this.txtBox16_1.Text = null;
            this.txtBox16_1.Top = 3.491329F;
            this.txtBox16_1.Width = 0.5F;
            // 
            // txtBox16_2
            // 
            this.txtBox16_2.Height = 0.1770833F;
            this.txtBox16_2.Left = 2.49F;
            this.txtBox16_2.Name = "txtBox16_2";
            this.txtBox16_2.Text = null;
            this.txtBox16_2.Top = 3.491329F;
            this.txtBox16_2.Width = 0.5F;
            // 
            // txtBox16_3
            // 
            this.txtBox16_3.Height = 0.1770833F;
            this.txtBox16_3.Left = 3.1775F;
            this.txtBox16_3.Name = "txtBox16_3";
            this.txtBox16_3.Text = null;
            this.txtBox16_3.Top = 3.491329F;
            this.txtBox16_3.Width = 0.5F;
            // 
            // txtBox16_4
            // 
            this.txtBox16_4.Height = 0.1770833F;
            this.txtBox16_4.Left = 3.875417F;
            this.txtBox16_4.Name = "txtBox16_4";
            this.txtBox16_4.Text = null;
            this.txtBox16_4.Top = 3.491329F;
            this.txtBox16_4.Width = 0.5F;
            // 
            // txtBox16_5
            // 
            this.txtBox16_5.Height = 0.1770833F;
            this.txtBox16_5.Left = 4.562917F;
            this.txtBox16_5.Name = "txtBox16_5";
            this.txtBox16_5.Text = null;
            this.txtBox16_5.Top = 3.491329F;
            this.txtBox16_5.Width = 0.5F;
            // 
            // txtBox16_6
            // 
            this.txtBox16_6.Height = 0.1770833F;
            this.txtBox16_6.Left = 5.250417F;
            this.txtBox16_6.Name = "txtBox16_6";
            this.txtBox16_6.Text = null;
            this.txtBox16_6.Top = 3.491329F;
            this.txtBox16_6.Width = 0.5F;
            // 
            // txtBox16_7
            // 
            this.txtBox16_7.Height = 0.1770833F;
            this.txtBox16_7.Left = 5.969167F;
            this.txtBox16_7.Name = "txtBox16_7";
            this.txtBox16_7.Text = null;
            this.txtBox16_7.Top = 3.491329F;
            this.txtBox16_7.Width = 0.5F;
            // 
            // txtBox16_8
            // 
            this.txtBox16_8.Height = 0.1770833F;
            this.txtBox16_8.Left = 6.635834F;
            this.txtBox16_8.Name = "txtBox16_8";
            this.txtBox16_8.Text = null;
            this.txtBox16_8.Top = 3.491329F;
            this.txtBox16_8.Width = 0.5F;
            // 
            // txtBox16_9
            // 
            this.txtBox16_9.Height = 0.1770833F;
            this.txtBox16_9.Left = 7.323333F;
            this.txtBox16_9.Name = "txtBox16_9";
            this.txtBox16_9.Text = null;
            this.txtBox16_9.Top = 3.491329F;
            this.txtBox16_9.Width = 0.5F;
            // 
            // txtBox16_10
            // 
            this.txtBox16_10.Height = 0.1770833F;
            this.txtBox16_10.Left = 8.010834F;
            this.txtBox16_10.Name = "txtBox16_10";
            this.txtBox16_10.Text = null;
            this.txtBox16_10.Top = 3.491329F;
            this.txtBox16_10.Width = 0.5F;
            // 
            // txtBox16_11
            // 
            this.txtBox16_11.Height = 0.1770833F;
            this.txtBox16_11.Left = 8.70875F;
            this.txtBox16_11.Name = "txtBox16_11";
            this.txtBox16_11.Text = null;
            this.txtBox16_11.Top = 3.491329F;
            this.txtBox16_11.Width = 0.5F;
            // 
            // txtBox16_12
            // 
            this.txtBox16_12.Height = 0.1770833F;
            this.txtBox16_12.Left = 9.39625F;
            this.txtBox16_12.Name = "txtBox16_12";
            this.txtBox16_12.Text = null;
            this.txtBox16_12.Top = 3.491329F;
            this.txtBox16_12.Width = 0.5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 0.08333335F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "white-space: nowrap";
            this.txtCity.Text = null;
            this.txtCity.Top = 1.948416F;
            this.txtCity.Width = 1.416667F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 1.583333F;
            this.txtState.Name = "txtState";
            this.txtState.Text = null;
            this.txtState.Top = 1.948416F;
            this.txtState.Width = 1.583333F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 3.333333F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 1.944944F;
            this.txtPostalCode.Width = 1.666667F;
            // 
            // txtEmployerCity
            // 
            this.txtEmployerCity.Height = 0.1770833F;
            this.txtEmployerCity.Left = 5.083333F;
            this.txtEmployerCity.Name = "txtEmployerCity";
            this.txtEmployerCity.Text = null;
            this.txtEmployerCity.Top = 1.948416F;
            this.txtEmployerCity.Width = 1.354167F;
            // 
            // txtEmployerState
            // 
            this.txtEmployerState.Height = 0.1770833F;
            this.txtEmployerState.Left = 6.5625F;
            this.txtEmployerState.Name = "txtEmployerState";
            this.txtEmployerState.Text = null;
            this.txtEmployerState.Top = 1.948416F;
            this.txtEmployerState.Width = 1.583333F;
            // 
            // txtEmployerPostalCode
            // 
            this.txtEmployerPostalCode.Height = 0.1770833F;
            this.txtEmployerPostalCode.Left = 8.364583F;
            this.txtEmployerPostalCode.Name = "txtEmployerPostalCode";
            this.txtEmployerPostalCode.Text = null;
            this.txtEmployerPostalCode.Top = 1.948416F;
            this.txtEmployerPostalCode.Width = 1.604167F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.CanGrow = false;
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.625F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 1.292167F;
            this.txtMiddle.Width = 0.2083333F;
            // 
            // txtLast
            // 
            this.txtLast.CanGrow = false;
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 1.9375F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Text = null;
            this.txtLast.Top = 1.292167F;
            this.txtLast.Width = 1.208333F;
            // 
            // txtAll12Zip
            // 
            this.txtAll12Zip.Height = 0.1770833F;
            this.txtAll12Zip.Left = 1.115F;
            this.txtAll12Zip.Name = "txtAll12Zip";
            this.txtAll12Zip.Text = null;
            this.txtAll12Zip.Top = 3.992F;
            this.txtAll12Zip.Width = 0.5F;
            // 
            // txtZip_1
            // 
            this.txtZip_1.Height = 0.1770833F;
            this.txtZip_1.Left = 1.8025F;
            this.txtZip_1.Name = "txtZip_1";
            this.txtZip_1.Text = null;
            this.txtZip_1.Top = 3.992F;
            this.txtZip_1.Width = 0.5F;
            // 
            // txtZip_2
            // 
            this.txtZip_2.Height = 0.1770833F;
            this.txtZip_2.Left = 2.49F;
            this.txtZip_2.Name = "txtZip_2";
            this.txtZip_2.Text = null;
            this.txtZip_2.Top = 3.992F;
            this.txtZip_2.Width = 0.5F;
            // 
            // txtZip_3
            // 
            this.txtZip_3.Height = 0.1770833F;
            this.txtZip_3.Left = 3.1775F;
            this.txtZip_3.Name = "txtZip_3";
            this.txtZip_3.Text = null;
            this.txtZip_3.Top = 3.992F;
            this.txtZip_3.Width = 0.5F;
            // 
            // txtZip_4
            // 
            this.txtZip_4.Height = 0.1770833F;
            this.txtZip_4.Left = 3.875417F;
            this.txtZip_4.Name = "txtZip_4";
            this.txtZip_4.Text = null;
            this.txtZip_4.Top = 3.992F;
            this.txtZip_4.Width = 0.5F;
            // 
            // txtZip_5
            // 
            this.txtZip_5.Height = 0.1770833F;
            this.txtZip_5.Left = 4.562917F;
            this.txtZip_5.Name = "txtZip_5";
            this.txtZip_5.Text = null;
            this.txtZip_5.Top = 3.992F;
            this.txtZip_5.Width = 0.5F;
            // 
            // txtZip_6
            // 
            this.txtZip_6.Height = 0.1770833F;
            this.txtZip_6.Left = 5.250417F;
            this.txtZip_6.Name = "txtZip_6";
            this.txtZip_6.Text = null;
            this.txtZip_6.Top = 3.992F;
            this.txtZip_6.Width = 0.5F;
            // 
            // txtZip_7
            // 
            this.txtZip_7.Height = 0.1770833F;
            this.txtZip_7.Left = 5.969167F;
            this.txtZip_7.Name = "txtZip_7";
            this.txtZip_7.Text = null;
            this.txtZip_7.Top = 3.992F;
            this.txtZip_7.Width = 0.5F;
            // 
            // txtZip_8
            // 
            this.txtZip_8.Height = 0.1770833F;
            this.txtZip_8.Left = 6.635833F;
            this.txtZip_8.Name = "txtZip_8";
            this.txtZip_8.Text = null;
            this.txtZip_8.Top = 3.992F;
            this.txtZip_8.Width = 0.5F;
            // 
            // txtZip_9
            // 
            this.txtZip_9.Height = 0.1770833F;
            this.txtZip_9.Left = 7.323333F;
            this.txtZip_9.Name = "txtZip_9";
            this.txtZip_9.Text = null;
            this.txtZip_9.Top = 3.992F;
            this.txtZip_9.Width = 0.5F;
            // 
            // txtZip_10
            // 
            this.txtZip_10.Height = 0.1770833F;
            this.txtZip_10.Left = 8.010834F;
            this.txtZip_10.Name = "txtZip_10";
            this.txtZip_10.Text = null;
            this.txtZip_10.Top = 3.992F;
            this.txtZip_10.Width = 0.5F;
            // 
            // txtZip_11
            // 
            this.txtZip_11.Height = 0.1770833F;
            this.txtZip_11.Left = 8.70875F;
            this.txtZip_11.Name = "txtZip_11";
            this.txtZip_11.Text = null;
            this.txtZip_11.Top = 3.992F;
            this.txtZip_11.Width = 0.5F;
            // 
            // txtZip_12
            // 
            this.txtZip_12.Height = 0.1770833F;
            this.txtZip_12.Left = 9.39625F;
            this.txtZip_12.Name = "txtZip_12";
            this.txtZip_12.Text = null;
            this.txtZip_12.Top = 3.992F;
            this.txtZip_12.Width = 0.5F;
            // 
            // rpt1095C2020Page1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 10.03142F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Zip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStartMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Zip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_12;
    }
}
