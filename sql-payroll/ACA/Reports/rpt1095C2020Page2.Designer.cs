﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016Page2.
	/// </summary>
	partial class rpt1095C2020Page2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2020Page2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCoveredMonth12_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCoveredMonth12_13,
            this.txtCoveredMonth1_12,
            this.txtCoveredMonth3_13,
            this.txtCoveredMonth7_13,
            this.txtCoveredMonth8_13,
            this.txtCoveredName1,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtCoveredName7,
            this.txtCoveredSSN7,
            this.txtCoveredDOB7,
            this.txtCoveredName8,
            this.txtCoveredSSN8,
            this.txtCoveredDOB8,
            this.txtCoveredName9,
            this.txtCoveredSSN9,
            this.txtCoveredDOB9,
            this.txtCoveredName10,
            this.txtCoveredSSN10,
            this.txtCoveredDOB10,
            this.txtCoveredName11,
            this.txtCoveredSSN11,
            this.txtCoveredDOB11,
            this.txtCoveredName12,
            this.txtCoveredSSN12,
            this.txtCoveredDOB12,
            this.txtCoveredAll12_7,
            this.txtCoveredMonth1_7,
            this.txtCoveredMonth2_7,
            this.txtCoveredMonth3_7,
            this.txtCoveredMonth4_7,
            this.txtCoveredMonth5_7,
            this.txtCoveredMonth6_7,
            this.txtCoveredMonth7_7,
            this.txtCoveredMonth8_7,
            this.txtCoveredMonth9_7,
            this.txtCoveredMonth10_7,
            this.txtCoveredMonth11_7,
            this.txtCoveredMonth12_7,
            this.txtCoveredAll12_8,
            this.txtCoveredMonth1_8,
            this.txtCoveredMonth2_8,
            this.txtCoveredMonth3_8,
            this.txtCoveredMonth4_8,
            this.txtCoveredMonth5_8,
            this.txtCoveredMonth6_8,
            this.txtCoveredMonth7_8,
            this.txtCoveredMonth8_8,
            this.txtCoveredMonth9_8,
            this.txtCoveredMonth10_8,
            this.txtCoveredMonth11_8,
            this.txtCoveredMonth12_8,
            this.txtCoveredAll12_9,
            this.txtCoveredMonth1_9,
            this.txtCoveredMonth2_9,
            this.txtCoveredMonth3_9,
            this.txtCoveredMonth4_9,
            this.txtCoveredMonth5_9,
            this.txtCoveredMonth6_9,
            this.txtCoveredMonth7_9,
            this.txtCoveredMonth8_9,
            this.txtCoveredMonth9_9,
            this.txtCoveredMonth10_9,
            this.txtCoveredMonth11_9,
            this.txtCoveredMonth12_9,
            this.txtCoveredAll12_10,
            this.txtCoveredMonth1_10,
            this.txtCoveredMonth2_10,
            this.txtCoveredMonth3_10,
            this.txtCoveredMonth4_10,
            this.txtCoveredMonth5_10,
            this.txtCoveredMonth6_10,
            this.txtCoveredMonth7_10,
            this.txtCoveredMonth8_10,
            this.txtCoveredMonth9_10,
            this.txtCoveredMonth10_10,
            this.txtCoveredMonth11_10,
            this.txtCoveredMonth12_10,
            this.txtCoveredAll12_11,
            this.txtCoveredMonth1_11,
            this.txtCoveredMonth2_11,
            this.txtCoveredMonth3_11,
            this.txtCoveredMonth4_11,
            this.txtCoveredMonth5_11,
            this.txtCoveredMonth6_11,
            this.txtCoveredMonth7_11,
            this.txtCoveredMonth8_11,
            this.txtCoveredMonth9_11,
            this.txtCoveredMonth10_11,
            this.txtCoveredMonth11_11,
            this.txtCoveredMonth12_11,
            this.txtCoveredAll12_12,
            this.txtCoveredMonth2_12,
            this.txtCoveredMonth3_12,
            this.txtCoveredMonth4_12,
            this.txtCoveredMonth5_12,
            this.txtCoveredMonth6_12,
            this.txtCoveredMonth7_12,
            this.txtCoveredMonth8_12,
            this.txtCoveredMonth9_12,
            this.txtCoveredMonth10_12,
            this.txtCoveredMonth11_12,
            this.txtCoveredMonth12_12,
            this.txtCoveredLast1,
            this.txtCoveredLast2,
            this.txtCoveredLast3,
            this.txtCoveredLast4,
            this.txtCoveredLast5,
            this.txtCoveredLast6,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.txtCoveredLast7,
            this.txtCoveredLast8,
            this.txtCoveredLast9,
            this.txtCoveredLast10,
            this.txtCoveredLast11,
            this.txtCoveredLast12,
            this.txtCoveredMiddle7,
            this.txtCoveredMiddle8,
            this.txtCoveredMiddle9,
            this.txtCoveredMiddle10,
            this.txtCoveredMiddle11,
            this.txtCoveredMiddle12,
            this.txtCoveredName13,
            this.txtCoveredSSN13,
            this.txtCoveredDOB13,
            this.txtCoveredAll12_13,
            this.txtCoveredMonth1_13,
            this.txtCoveredMonth2_13,
            this.txtCoveredMonth4_13,
            this.txtCoveredMonth5_13,
            this.txtCoveredMonth6_13,
            this.txtCoveredMonth9_13,
            this.txtCoveredMonth10_13,
            this.txtCoveredMonth11_13,
            this.txtCoveredLast13,
            this.txtCoveredMiddle13,
            this.txtCoveredIndividuals});
            this.Detail.Height = 7.345083F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtCoveredMonth12_13
            // 
            this.txtCoveredMonth12_13.Height = 0.1770833F;
            this.txtCoveredMonth12_13.Left = 9.738001F;
            this.txtCoveredMonth12_13.Name = "txtCoveredMonth12_13";
            this.txtCoveredMonth12_13.Text = "X";
            this.txtCoveredMonth12_13.Top = 6.862F;
            this.txtCoveredMonth12_13.Width = 0.1944447F;
            // 
            // txtCoveredMonth1_12
            // 
            this.txtCoveredMonth1_12.Height = 0.1770833F;
            this.txtCoveredMonth1_12.Left = 5.326F;
            this.txtCoveredMonth1_12.Name = "txtCoveredMonth1_12";
            this.txtCoveredMonth1_12.Text = "X";
            this.txtCoveredMonth1_12.Top = 6.372F;
            this.txtCoveredMonth1_12.Width = 0.25F;
            // 
            // txtCoveredMonth3_13
            // 
            this.txtCoveredMonth3_13.Height = 0.1770833F;
            this.txtCoveredMonth3_13.Left = 6.1335F;
            this.txtCoveredMonth3_13.Name = "txtCoveredMonth3_13";
            this.txtCoveredMonth3_13.Text = "X";
            this.txtCoveredMonth3_13.Top = 6.862001F;
            this.txtCoveredMonth3_13.Width = 0.25F;
            // 
            // txtCoveredMonth7_13
            // 
            this.txtCoveredMonth7_13.Height = 0.1770833F;
            this.txtCoveredMonth7_13.Left = 7.748501F;
            this.txtCoveredMonth7_13.Name = "txtCoveredMonth7_13";
            this.txtCoveredMonth7_13.Text = "X";
            this.txtCoveredMonth7_13.Top = 6.862001F;
            this.txtCoveredMonth7_13.Width = 0.25F;
            // 
            // txtCoveredMonth8_13
            // 
            this.txtCoveredMonth8_13.Height = 0.1770833F;
            this.txtCoveredMonth8_13.Left = 8.152251F;
            this.txtCoveredMonth8_13.Name = "txtCoveredMonth8_13";
            this.txtCoveredMonth8_13.Text = "X";
            this.txtCoveredMonth8_13.Top = 6.862001F;
            this.txtCoveredMonth8_13.Width = 0.25F;
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.188F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 0.9850001F;
            this.txtCoveredName1.Width = 0.9791667F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 2.510918F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 0.9807503F;
            this.txtCoveredSSN1.Width = 1F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 3.740084F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 0.9807503F;
            this.txtCoveredDOB1.Width = 0.9166667F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.188F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 1.480833F;
            this.txtCoveredName2.Width = 0.9791667F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 2.510918F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 1.48075F;
            this.txtCoveredSSN2.Width = 1F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 3.740084F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 1.48075F;
            this.txtCoveredDOB2.Width = 0.9166667F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.188F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 1.980833F;
            this.txtCoveredName3.Width = 0.9791667F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 2.510918F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 1.98075F;
            this.txtCoveredSSN3.Width = 1F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 3.740084F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 1.98075F;
            this.txtCoveredDOB3.Width = 0.9166667F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.188F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 2.470833F;
            this.txtCoveredName4.Width = 0.9791667F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 2.510918F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 2.47075F;
            this.txtCoveredSSN4.Width = 1F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 3.740084F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 2.47075F;
            this.txtCoveredDOB4.Width = 0.9166667F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.188F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 2.960833F;
            this.txtCoveredName5.Width = 0.9791667F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 2.510918F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 2.96075F;
            this.txtCoveredSSN5.Width = 1F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 3.740084F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 2.96075F;
            this.txtCoveredDOB5.Width = 0.9166667F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.188F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 3.440416F;
            this.txtCoveredName6.Width = 0.9791667F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 2.510918F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 3.440333F;
            this.txtCoveredSSN6.Width = 1F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 3.740084F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 3.440333F;
            this.txtCoveredDOB6.Width = 0.9166667F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 4.815584F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = "X";
            this.txtCoveredAll12_1.Top = 0.9849184F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 5.326F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = "X";
            this.txtCoveredMonth1_1.Top = 0.9849184F;
            this.txtCoveredMonth1_1.Width = 0.25F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 5.729752F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = "X";
            this.txtCoveredMonth2_1.Top = 0.9849184F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 6.1335F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = "X";
            this.txtCoveredMonth3_1.Top = 0.9849184F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 6.537251F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = "X";
            this.txtCoveredMonth4_1.Top = 0.9849184F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 6.941001F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = "X";
            this.txtCoveredMonth5_1.Top = 0.9849184F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 7.344751F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = "X";
            this.txtCoveredMonth6_1.Top = 0.9849184F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 7.748501F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = "X";
            this.txtCoveredMonth7_1.Top = 0.9849184F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 8.152251F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = "X";
            this.txtCoveredMonth8_1.Top = 0.9849184F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 8.556751F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = "X";
            this.txtCoveredMonth9_1.Top = 0.9849174F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 8.950501F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = "X";
            this.txtCoveredMonth10_1.Top = 0.9849174F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 9.34425F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = "X";
            this.txtCoveredMonth11_1.Top = 0.9849174F;
            this.txtCoveredMonth11_1.Width = 0.25F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 9.738001F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = "X";
            this.txtCoveredMonth12_1.Top = 0.9849174F;
            this.txtCoveredMonth12_1.Width = 0.1944444F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 4.815584F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = "X";
            this.txtCoveredAll12_2.Top = 1.482835F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 5.326F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = "X";
            this.txtCoveredMonth1_2.Top = 1.482835F;
            this.txtCoveredMonth1_2.Width = 0.25F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 5.729752F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = "X";
            this.txtCoveredMonth2_2.Top = 1.482835F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 6.1335F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = "X";
            this.txtCoveredMonth3_2.Top = 1.482835F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 6.537251F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = "X";
            this.txtCoveredMonth4_2.Top = 1.482835F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 6.941001F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = "X";
            this.txtCoveredMonth5_2.Top = 1.482835F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 7.344751F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = "X";
            this.txtCoveredMonth6_2.Top = 1.482835F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 7.748501F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = "X";
            this.txtCoveredMonth7_2.Top = 1.482835F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 8.152251F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = "X";
            this.txtCoveredMonth8_2.Top = 1.482835F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 8.556751F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = "X";
            this.txtCoveredMonth9_2.Top = 1.482834F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 8.950501F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = "X";
            this.txtCoveredMonth10_2.Top = 1.482834F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 9.34425F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = "X";
            this.txtCoveredMonth11_2.Top = 1.482834F;
            this.txtCoveredMonth11_2.Width = 0.25F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 9.738001F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = "X";
            this.txtCoveredMonth12_2.Top = 1.482834F;
            this.txtCoveredMonth12_2.Width = 0.1944444F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 4.815584F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = "X";
            this.txtCoveredAll12_3.Top = 1.980751F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 5.326F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = "X";
            this.txtCoveredMonth1_3.Top = 1.980751F;
            this.txtCoveredMonth1_3.Width = 0.25F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 5.729752F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = "X";
            this.txtCoveredMonth2_3.Top = 1.980751F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 6.1335F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = "X";
            this.txtCoveredMonth3_3.Top = 1.980751F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 6.537251F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = "X";
            this.txtCoveredMonth4_3.Top = 1.980751F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 6.941001F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = "X";
            this.txtCoveredMonth5_3.Top = 1.980751F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 7.344751F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = "X";
            this.txtCoveredMonth6_3.Top = 1.980751F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 7.748501F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = "X";
            this.txtCoveredMonth7_3.Top = 1.980751F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 8.152251F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = "X";
            this.txtCoveredMonth8_3.Top = 1.980751F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 8.556751F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = "X";
            this.txtCoveredMonth9_3.Top = 1.98075F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 8.950501F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = "X";
            this.txtCoveredMonth10_3.Top = 1.98075F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 9.34425F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = "X";
            this.txtCoveredMonth11_3.Top = 1.98075F;
            this.txtCoveredMonth11_3.Width = 0.25F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 9.738001F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = "X";
            this.txtCoveredMonth12_3.Top = 1.98075F;
            this.txtCoveredMonth12_3.Width = 0.1944444F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 4.815584F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = "X";
            this.txtCoveredAll12_4.Top = 2.468668F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 5.326F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = "X";
            this.txtCoveredMonth1_4.Top = 2.468668F;
            this.txtCoveredMonth1_4.Width = 0.25F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 5.729752F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = "X";
            this.txtCoveredMonth2_4.Top = 2.468668F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 6.1335F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = "X";
            this.txtCoveredMonth3_4.Top = 2.468668F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 6.537251F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = "X";
            this.txtCoveredMonth4_4.Top = 2.468668F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 6.941001F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = "X";
            this.txtCoveredMonth5_4.Top = 2.468668F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 7.344751F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = "X";
            this.txtCoveredMonth6_4.Top = 2.468668F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 7.748501F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = "X";
            this.txtCoveredMonth7_4.Top = 2.468668F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 8.152251F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = "X";
            this.txtCoveredMonth8_4.Top = 2.468668F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 8.556751F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = "X";
            this.txtCoveredMonth9_4.Top = 2.468667F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 8.950501F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = "X";
            this.txtCoveredMonth10_4.Top = 2.468667F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 9.34425F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = "X";
            this.txtCoveredMonth11_4.Top = 2.468667F;
            this.txtCoveredMonth11_4.Width = 0.25F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 9.738001F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = "X";
            this.txtCoveredMonth12_4.Top = 2.468667F;
            this.txtCoveredMonth12_4.Width = 0.1944444F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 4.815584F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = "X";
            this.txtCoveredAll12_5.Top = 2.956585F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 5.326F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = "X";
            this.txtCoveredMonth1_5.Top = 2.956585F;
            this.txtCoveredMonth1_5.Width = 0.25F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 5.729752F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = "X";
            this.txtCoveredMonth2_5.Top = 2.956585F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 6.1335F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = "X";
            this.txtCoveredMonth3_5.Top = 2.956585F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 6.537251F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = "X";
            this.txtCoveredMonth4_5.Top = 2.956585F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 6.941001F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = "X";
            this.txtCoveredMonth5_5.Top = 2.956585F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 7.344751F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = "X";
            this.txtCoveredMonth6_5.Top = 2.956585F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 7.748501F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = "X";
            this.txtCoveredMonth7_5.Top = 2.956585F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 8.152251F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = "X";
            this.txtCoveredMonth8_5.Top = 2.956585F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 8.556751F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = "X";
            this.txtCoveredMonth9_5.Top = 2.956584F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 8.950501F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = "X";
            this.txtCoveredMonth10_5.Top = 2.956584F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 9.34425F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = "X";
            this.txtCoveredMonth11_5.Top = 2.956584F;
            this.txtCoveredMonth11_5.Width = 0.25F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 9.738001F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = "X";
            this.txtCoveredMonth12_5.Top = 2.956584F;
            this.txtCoveredMonth12_5.Width = 0.1944444F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 4.815584F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = "X";
            this.txtCoveredAll12_6.Top = 3.444501F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 5.326F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = "X";
            this.txtCoveredMonth1_6.Top = 3.444501F;
            this.txtCoveredMonth1_6.Width = 0.25F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 5.729752F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = "X";
            this.txtCoveredMonth2_6.Top = 3.444501F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 6.1335F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = "X";
            this.txtCoveredMonth3_6.Top = 3.444501F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 6.537251F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = "X";
            this.txtCoveredMonth4_6.Top = 3.444501F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 6.941001F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = "X";
            this.txtCoveredMonth5_6.Top = 3.444501F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 7.344751F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = "X";
            this.txtCoveredMonth6_6.Top = 3.444501F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 7.748501F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = "X";
            this.txtCoveredMonth7_6.Top = 3.444501F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 8.152251F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = "X";
            this.txtCoveredMonth8_6.Top = 3.444501F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 8.556751F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = "X";
            this.txtCoveredMonth9_6.Top = 3.4445F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 8.950501F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = "X";
            this.txtCoveredMonth10_6.Top = 3.4445F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 9.34425F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = "X";
            this.txtCoveredMonth11_6.Top = 3.4445F;
            this.txtCoveredMonth11_6.Width = 0.25F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 9.738001F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = "X";
            this.txtCoveredMonth12_6.Top = 3.4445F;
            this.txtCoveredMonth12_6.Width = 0.1944444F;
            // 
            // txtCoveredName7
            // 
            this.txtCoveredName7.Height = 0.1770833F;
            this.txtCoveredName7.Left = 0.188F;
            this.txtCoveredName7.Name = "txtCoveredName7";
            this.txtCoveredName7.Text = null;
            this.txtCoveredName7.Top = 3.930416F;
            this.txtCoveredName7.Width = 0.9791667F;
            // 
            // txtCoveredSSN7
            // 
            this.txtCoveredSSN7.Height = 0.1770833F;
            this.txtCoveredSSN7.Left = 2.510918F;
            this.txtCoveredSSN7.Name = "txtCoveredSSN7";
            this.txtCoveredSSN7.Text = null;
            this.txtCoveredSSN7.Top = 3.930334F;
            this.txtCoveredSSN7.Width = 1F;
            // 
            // txtCoveredDOB7
            // 
            this.txtCoveredDOB7.Height = 0.1770833F;
            this.txtCoveredDOB7.Left = 3.740084F;
            this.txtCoveredDOB7.Name = "txtCoveredDOB7";
            this.txtCoveredDOB7.Text = null;
            this.txtCoveredDOB7.Top = 3.930334F;
            this.txtCoveredDOB7.Width = 0.9166667F;
            // 
            // txtCoveredName8
            // 
            this.txtCoveredName8.Height = 0.1770833F;
            this.txtCoveredName8.Left = 0.188F;
            this.txtCoveredName8.Name = "txtCoveredName8";
            this.txtCoveredName8.Text = null;
            this.txtCoveredName8.Top = 4.420416F;
            this.txtCoveredName8.Width = 0.9791667F;
            // 
            // txtCoveredSSN8
            // 
            this.txtCoveredSSN8.Height = 0.1770833F;
            this.txtCoveredSSN8.Left = 2.510918F;
            this.txtCoveredSSN8.Name = "txtCoveredSSN8";
            this.txtCoveredSSN8.Text = null;
            this.txtCoveredSSN8.Top = 4.420333F;
            this.txtCoveredSSN8.Width = 1F;
            // 
            // txtCoveredDOB8
            // 
            this.txtCoveredDOB8.Height = 0.1770833F;
            this.txtCoveredDOB8.Left = 3.740084F;
            this.txtCoveredDOB8.Name = "txtCoveredDOB8";
            this.txtCoveredDOB8.Text = null;
            this.txtCoveredDOB8.Top = 4.420333F;
            this.txtCoveredDOB8.Width = 0.9166667F;
            // 
            // txtCoveredName9
            // 
            this.txtCoveredName9.Height = 0.1770833F;
            this.txtCoveredName9.Left = 0.188F;
            this.txtCoveredName9.Name = "txtCoveredName9";
            this.txtCoveredName9.Text = null;
            this.txtCoveredName9.Top = 4.910416F;
            this.txtCoveredName9.Width = 0.9791667F;
            // 
            // txtCoveredSSN9
            // 
            this.txtCoveredSSN9.Height = 0.1770833F;
            this.txtCoveredSSN9.Left = 2.510918F;
            this.txtCoveredSSN9.Name = "txtCoveredSSN9";
            this.txtCoveredSSN9.Text = null;
            this.txtCoveredSSN9.Top = 4.910334F;
            this.txtCoveredSSN9.Width = 1F;
            // 
            // txtCoveredDOB9
            // 
            this.txtCoveredDOB9.Height = 0.1770833F;
            this.txtCoveredDOB9.Left = 3.740084F;
            this.txtCoveredDOB9.Name = "txtCoveredDOB9";
            this.txtCoveredDOB9.Text = null;
            this.txtCoveredDOB9.Top = 4.910334F;
            this.txtCoveredDOB9.Width = 0.9166667F;
            // 
            // txtCoveredName10
            // 
            this.txtCoveredName10.Height = 0.1770833F;
            this.txtCoveredName10.Left = 0.188F;
            this.txtCoveredName10.Name = "txtCoveredName10";
            this.txtCoveredName10.Text = null;
            this.txtCoveredName10.Top = 5.400416F;
            this.txtCoveredName10.Width = 0.9791667F;
            // 
            // txtCoveredSSN10
            // 
            this.txtCoveredSSN10.Height = 0.1770833F;
            this.txtCoveredSSN10.Left = 2.510918F;
            this.txtCoveredSSN10.Name = "txtCoveredSSN10";
            this.txtCoveredSSN10.Text = null;
            this.txtCoveredSSN10.Top = 5.400333F;
            this.txtCoveredSSN10.Width = 1F;
            // 
            // txtCoveredDOB10
            // 
            this.txtCoveredDOB10.Height = 0.1770833F;
            this.txtCoveredDOB10.Left = 3.740084F;
            this.txtCoveredDOB10.Name = "txtCoveredDOB10";
            this.txtCoveredDOB10.Text = null;
            this.txtCoveredDOB10.Top = 5.400333F;
            this.txtCoveredDOB10.Width = 0.9166667F;
            // 
            // txtCoveredName11
            // 
            this.txtCoveredName11.Height = 0.1770833F;
            this.txtCoveredName11.Left = 0.188F;
            this.txtCoveredName11.Name = "txtCoveredName11";
            this.txtCoveredName11.Text = null;
            this.txtCoveredName11.Top = 5.879999F;
            this.txtCoveredName11.Width = 0.9791667F;
            // 
            // txtCoveredSSN11
            // 
            this.txtCoveredSSN11.Height = 0.1770833F;
            this.txtCoveredSSN11.Left = 2.510918F;
            this.txtCoveredSSN11.Name = "txtCoveredSSN11";
            this.txtCoveredSSN11.Text = null;
            this.txtCoveredSSN11.Top = 5.879917F;
            this.txtCoveredSSN11.Width = 1F;
            // 
            // txtCoveredDOB11
            // 
            this.txtCoveredDOB11.Height = 0.1770833F;
            this.txtCoveredDOB11.Left = 3.740084F;
            this.txtCoveredDOB11.Name = "txtCoveredDOB11";
            this.txtCoveredDOB11.Text = null;
            this.txtCoveredDOB11.Top = 5.879917F;
            this.txtCoveredDOB11.Width = 0.9166667F;
            // 
            // txtCoveredName12
            // 
            this.txtCoveredName12.Height = 0.1770833F;
            this.txtCoveredName12.Left = 0.188F;
            this.txtCoveredName12.Name = "txtCoveredName12";
            this.txtCoveredName12.Text = null;
            this.txtCoveredName12.Top = 6.369999F;
            this.txtCoveredName12.Width = 0.9791667F;
            // 
            // txtCoveredSSN12
            // 
            this.txtCoveredSSN12.Height = 0.1770833F;
            this.txtCoveredSSN12.Left = 2.510918F;
            this.txtCoveredSSN12.Name = "txtCoveredSSN12";
            this.txtCoveredSSN12.Text = null;
            this.txtCoveredSSN12.Top = 6.369916F;
            this.txtCoveredSSN12.Width = 1F;
            // 
            // txtCoveredDOB12
            // 
            this.txtCoveredDOB12.Height = 0.1770833F;
            this.txtCoveredDOB12.Left = 3.740084F;
            this.txtCoveredDOB12.Name = "txtCoveredDOB12";
            this.txtCoveredDOB12.Text = null;
            this.txtCoveredDOB12.Top = 6.369916F;
            this.txtCoveredDOB12.Width = 0.9166667F;
            // 
            // txtCoveredAll12_7
            // 
            this.txtCoveredAll12_7.Height = 0.1770833F;
            this.txtCoveredAll12_7.Left = 4.815584F;
            this.txtCoveredAll12_7.Name = "txtCoveredAll12_7";
            this.txtCoveredAll12_7.Text = "X";
            this.txtCoveredAll12_7.Top = 3.932417F;
            this.txtCoveredAll12_7.Width = 0.25F;
            // 
            // txtCoveredMonth1_7
            // 
            this.txtCoveredMonth1_7.Height = 0.1770833F;
            this.txtCoveredMonth1_7.Left = 5.326F;
            this.txtCoveredMonth1_7.Name = "txtCoveredMonth1_7";
            this.txtCoveredMonth1_7.Text = "X";
            this.txtCoveredMonth1_7.Top = 3.932417F;
            this.txtCoveredMonth1_7.Width = 0.25F;
            // 
            // txtCoveredMonth2_7
            // 
            this.txtCoveredMonth2_7.Height = 0.1770833F;
            this.txtCoveredMonth2_7.Left = 5.729752F;
            this.txtCoveredMonth2_7.Name = "txtCoveredMonth2_7";
            this.txtCoveredMonth2_7.Text = "X";
            this.txtCoveredMonth2_7.Top = 3.932417F;
            this.txtCoveredMonth2_7.Width = 0.25F;
            // 
            // txtCoveredMonth3_7
            // 
            this.txtCoveredMonth3_7.Height = 0.1770833F;
            this.txtCoveredMonth3_7.Left = 6.1335F;
            this.txtCoveredMonth3_7.Name = "txtCoveredMonth3_7";
            this.txtCoveredMonth3_7.Text = "X";
            this.txtCoveredMonth3_7.Top = 3.932417F;
            this.txtCoveredMonth3_7.Width = 0.25F;
            // 
            // txtCoveredMonth4_7
            // 
            this.txtCoveredMonth4_7.Height = 0.1770833F;
            this.txtCoveredMonth4_7.Left = 6.537251F;
            this.txtCoveredMonth4_7.Name = "txtCoveredMonth4_7";
            this.txtCoveredMonth4_7.Text = "X";
            this.txtCoveredMonth4_7.Top = 3.932417F;
            this.txtCoveredMonth4_7.Width = 0.25F;
            // 
            // txtCoveredMonth5_7
            // 
            this.txtCoveredMonth5_7.Height = 0.1770833F;
            this.txtCoveredMonth5_7.Left = 6.941001F;
            this.txtCoveredMonth5_7.Name = "txtCoveredMonth5_7";
            this.txtCoveredMonth5_7.Text = "X";
            this.txtCoveredMonth5_7.Top = 3.932417F;
            this.txtCoveredMonth5_7.Width = 0.25F;
            // 
            // txtCoveredMonth6_7
            // 
            this.txtCoveredMonth6_7.Height = 0.1770833F;
            this.txtCoveredMonth6_7.Left = 7.344751F;
            this.txtCoveredMonth6_7.Name = "txtCoveredMonth6_7";
            this.txtCoveredMonth6_7.Text = "X";
            this.txtCoveredMonth6_7.Top = 3.932417F;
            this.txtCoveredMonth6_7.Width = 0.25F;
            // 
            // txtCoveredMonth7_7
            // 
            this.txtCoveredMonth7_7.Height = 0.1770833F;
            this.txtCoveredMonth7_7.Left = 7.748501F;
            this.txtCoveredMonth7_7.Name = "txtCoveredMonth7_7";
            this.txtCoveredMonth7_7.Text = "X";
            this.txtCoveredMonth7_7.Top = 3.932417F;
            this.txtCoveredMonth7_7.Width = 0.25F;
            // 
            // txtCoveredMonth8_7
            // 
            this.txtCoveredMonth8_7.Height = 0.1770833F;
            this.txtCoveredMonth8_7.Left = 8.152251F;
            this.txtCoveredMonth8_7.Name = "txtCoveredMonth8_7";
            this.txtCoveredMonth8_7.Text = "X";
            this.txtCoveredMonth8_7.Top = 3.932417F;
            this.txtCoveredMonth8_7.Width = 0.25F;
            // 
            // txtCoveredMonth9_7
            // 
            this.txtCoveredMonth9_7.Height = 0.1770833F;
            this.txtCoveredMonth9_7.Left = 8.556751F;
            this.txtCoveredMonth9_7.Name = "txtCoveredMonth9_7";
            this.txtCoveredMonth9_7.Text = "X";
            this.txtCoveredMonth9_7.Top = 3.932416F;
            this.txtCoveredMonth9_7.Width = 0.25F;
            // 
            // txtCoveredMonth10_7
            // 
            this.txtCoveredMonth10_7.Height = 0.1770833F;
            this.txtCoveredMonth10_7.Left = 8.950501F;
            this.txtCoveredMonth10_7.Name = "txtCoveredMonth10_7";
            this.txtCoveredMonth10_7.Text = "X";
            this.txtCoveredMonth10_7.Top = 3.932416F;
            this.txtCoveredMonth10_7.Width = 0.25F;
            // 
            // txtCoveredMonth11_7
            // 
            this.txtCoveredMonth11_7.Height = 0.1770833F;
            this.txtCoveredMonth11_7.Left = 9.34425F;
            this.txtCoveredMonth11_7.Name = "txtCoveredMonth11_7";
            this.txtCoveredMonth11_7.Text = "X";
            this.txtCoveredMonth11_7.Top = 3.932416F;
            this.txtCoveredMonth11_7.Width = 0.25F;
            // 
            // txtCoveredMonth12_7
            // 
            this.txtCoveredMonth12_7.Height = 0.1770833F;
            this.txtCoveredMonth12_7.Left = 9.738001F;
            this.txtCoveredMonth12_7.Name = "txtCoveredMonth12_7";
            this.txtCoveredMonth12_7.Text = "X";
            this.txtCoveredMonth12_7.Top = 3.932416F;
            this.txtCoveredMonth12_7.Width = 0.1944444F;
            // 
            // txtCoveredAll12_8
            // 
            this.txtCoveredAll12_8.Height = 0.1770833F;
            this.txtCoveredAll12_8.Left = 4.815584F;
            this.txtCoveredAll12_8.Name = "txtCoveredAll12_8";
            this.txtCoveredAll12_8.Text = "X";
            this.txtCoveredAll12_8.Top = 4.420334F;
            this.txtCoveredAll12_8.Width = 0.25F;
            // 
            // txtCoveredMonth1_8
            // 
            this.txtCoveredMonth1_8.Height = 0.1770833F;
            this.txtCoveredMonth1_8.Left = 5.326F;
            this.txtCoveredMonth1_8.Name = "txtCoveredMonth1_8";
            this.txtCoveredMonth1_8.Text = "X";
            this.txtCoveredMonth1_8.Top = 4.420334F;
            this.txtCoveredMonth1_8.Width = 0.25F;
            // 
            // txtCoveredMonth2_8
            // 
            this.txtCoveredMonth2_8.Height = 0.1770833F;
            this.txtCoveredMonth2_8.Left = 5.729752F;
            this.txtCoveredMonth2_8.Name = "txtCoveredMonth2_8";
            this.txtCoveredMonth2_8.Text = "X";
            this.txtCoveredMonth2_8.Top = 4.420334F;
            this.txtCoveredMonth2_8.Width = 0.25F;
            // 
            // txtCoveredMonth3_8
            // 
            this.txtCoveredMonth3_8.Height = 0.1770833F;
            this.txtCoveredMonth3_8.Left = 6.1335F;
            this.txtCoveredMonth3_8.Name = "txtCoveredMonth3_8";
            this.txtCoveredMonth3_8.Text = "X";
            this.txtCoveredMonth3_8.Top = 4.420334F;
            this.txtCoveredMonth3_8.Width = 0.25F;
            // 
            // txtCoveredMonth4_8
            // 
            this.txtCoveredMonth4_8.Height = 0.1770833F;
            this.txtCoveredMonth4_8.Left = 6.537251F;
            this.txtCoveredMonth4_8.Name = "txtCoveredMonth4_8";
            this.txtCoveredMonth4_8.Text = "X";
            this.txtCoveredMonth4_8.Top = 4.420334F;
            this.txtCoveredMonth4_8.Width = 0.25F;
            // 
            // txtCoveredMonth5_8
            // 
            this.txtCoveredMonth5_8.Height = 0.1770833F;
            this.txtCoveredMonth5_8.Left = 6.941001F;
            this.txtCoveredMonth5_8.Name = "txtCoveredMonth5_8";
            this.txtCoveredMonth5_8.Text = "X";
            this.txtCoveredMonth5_8.Top = 4.420334F;
            this.txtCoveredMonth5_8.Width = 0.25F;
            // 
            // txtCoveredMonth6_8
            // 
            this.txtCoveredMonth6_8.Height = 0.1770833F;
            this.txtCoveredMonth6_8.Left = 7.344751F;
            this.txtCoveredMonth6_8.Name = "txtCoveredMonth6_8";
            this.txtCoveredMonth6_8.Text = "X";
            this.txtCoveredMonth6_8.Top = 4.420334F;
            this.txtCoveredMonth6_8.Width = 0.25F;
            // 
            // txtCoveredMonth7_8
            // 
            this.txtCoveredMonth7_8.Height = 0.1770833F;
            this.txtCoveredMonth7_8.Left = 7.748501F;
            this.txtCoveredMonth7_8.Name = "txtCoveredMonth7_8";
            this.txtCoveredMonth7_8.Text = "X";
            this.txtCoveredMonth7_8.Top = 4.420334F;
            this.txtCoveredMonth7_8.Width = 0.25F;
            // 
            // txtCoveredMonth8_8
            // 
            this.txtCoveredMonth8_8.Height = 0.1770833F;
            this.txtCoveredMonth8_8.Left = 8.152251F;
            this.txtCoveredMonth8_8.Name = "txtCoveredMonth8_8";
            this.txtCoveredMonth8_8.Text = "X";
            this.txtCoveredMonth8_8.Top = 4.420334F;
            this.txtCoveredMonth8_8.Width = 0.25F;
            // 
            // txtCoveredMonth9_8
            // 
            this.txtCoveredMonth9_8.Height = 0.1770833F;
            this.txtCoveredMonth9_8.Left = 8.556751F;
            this.txtCoveredMonth9_8.Name = "txtCoveredMonth9_8";
            this.txtCoveredMonth9_8.Text = "X";
            this.txtCoveredMonth9_8.Top = 4.420333F;
            this.txtCoveredMonth9_8.Width = 0.25F;
            // 
            // txtCoveredMonth10_8
            // 
            this.txtCoveredMonth10_8.Height = 0.1770833F;
            this.txtCoveredMonth10_8.Left = 8.950501F;
            this.txtCoveredMonth10_8.Name = "txtCoveredMonth10_8";
            this.txtCoveredMonth10_8.Text = "X";
            this.txtCoveredMonth10_8.Top = 4.420333F;
            this.txtCoveredMonth10_8.Width = 0.25F;
            // 
            // txtCoveredMonth11_8
            // 
            this.txtCoveredMonth11_8.Height = 0.1770833F;
            this.txtCoveredMonth11_8.Left = 9.34425F;
            this.txtCoveredMonth11_8.Name = "txtCoveredMonth11_8";
            this.txtCoveredMonth11_8.Text = "X";
            this.txtCoveredMonth11_8.Top = 4.420333F;
            this.txtCoveredMonth11_8.Width = 0.25F;
            // 
            // txtCoveredMonth12_8
            // 
            this.txtCoveredMonth12_8.Height = 0.1770833F;
            this.txtCoveredMonth12_8.Left = 9.738001F;
            this.txtCoveredMonth12_8.Name = "txtCoveredMonth12_8";
            this.txtCoveredMonth12_8.Text = "X";
            this.txtCoveredMonth12_8.Top = 4.420333F;
            this.txtCoveredMonth12_8.Width = 0.1944444F;
            // 
            // txtCoveredAll12_9
            // 
            this.txtCoveredAll12_9.Height = 0.1770833F;
            this.txtCoveredAll12_9.Left = 4.815584F;
            this.txtCoveredAll12_9.Name = "txtCoveredAll12_9";
            this.txtCoveredAll12_9.Text = "X";
            this.txtCoveredAll12_9.Top = 4.90825F;
            this.txtCoveredAll12_9.Width = 0.25F;
            // 
            // txtCoveredMonth1_9
            // 
            this.txtCoveredMonth1_9.Height = 0.1770833F;
            this.txtCoveredMonth1_9.Left = 5.326F;
            this.txtCoveredMonth1_9.Name = "txtCoveredMonth1_9";
            this.txtCoveredMonth1_9.Text = "X";
            this.txtCoveredMonth1_9.Top = 4.90825F;
            this.txtCoveredMonth1_9.Width = 0.25F;
            // 
            // txtCoveredMonth2_9
            // 
            this.txtCoveredMonth2_9.Height = 0.1770833F;
            this.txtCoveredMonth2_9.Left = 5.729752F;
            this.txtCoveredMonth2_9.Name = "txtCoveredMonth2_9";
            this.txtCoveredMonth2_9.Text = "X";
            this.txtCoveredMonth2_9.Top = 4.90825F;
            this.txtCoveredMonth2_9.Width = 0.25F;
            // 
            // txtCoveredMonth3_9
            // 
            this.txtCoveredMonth3_9.Height = 0.1770833F;
            this.txtCoveredMonth3_9.Left = 6.1335F;
            this.txtCoveredMonth3_9.Name = "txtCoveredMonth3_9";
            this.txtCoveredMonth3_9.Text = "X";
            this.txtCoveredMonth3_9.Top = 4.90825F;
            this.txtCoveredMonth3_9.Width = 0.25F;
            // 
            // txtCoveredMonth4_9
            // 
            this.txtCoveredMonth4_9.Height = 0.1770833F;
            this.txtCoveredMonth4_9.Left = 6.537251F;
            this.txtCoveredMonth4_9.Name = "txtCoveredMonth4_9";
            this.txtCoveredMonth4_9.Text = "X";
            this.txtCoveredMonth4_9.Top = 4.90825F;
            this.txtCoveredMonth4_9.Width = 0.25F;
            // 
            // txtCoveredMonth5_9
            // 
            this.txtCoveredMonth5_9.Height = 0.1770833F;
            this.txtCoveredMonth5_9.Left = 6.941001F;
            this.txtCoveredMonth5_9.Name = "txtCoveredMonth5_9";
            this.txtCoveredMonth5_9.Text = "X";
            this.txtCoveredMonth5_9.Top = 4.90825F;
            this.txtCoveredMonth5_9.Width = 0.25F;
            // 
            // txtCoveredMonth6_9
            // 
            this.txtCoveredMonth6_9.Height = 0.1770833F;
            this.txtCoveredMonth6_9.Left = 7.344751F;
            this.txtCoveredMonth6_9.Name = "txtCoveredMonth6_9";
            this.txtCoveredMonth6_9.Text = "X";
            this.txtCoveredMonth6_9.Top = 4.90825F;
            this.txtCoveredMonth6_9.Width = 0.25F;
            // 
            // txtCoveredMonth7_9
            // 
            this.txtCoveredMonth7_9.Height = 0.1770833F;
            this.txtCoveredMonth7_9.Left = 7.748501F;
            this.txtCoveredMonth7_9.Name = "txtCoveredMonth7_9";
            this.txtCoveredMonth7_9.Text = "X";
            this.txtCoveredMonth7_9.Top = 4.90825F;
            this.txtCoveredMonth7_9.Width = 0.25F;
            // 
            // txtCoveredMonth8_9
            // 
            this.txtCoveredMonth8_9.Height = 0.1770833F;
            this.txtCoveredMonth8_9.Left = 8.152251F;
            this.txtCoveredMonth8_9.Name = "txtCoveredMonth8_9";
            this.txtCoveredMonth8_9.Text = "X";
            this.txtCoveredMonth8_9.Top = 4.90825F;
            this.txtCoveredMonth8_9.Width = 0.25F;
            // 
            // txtCoveredMonth9_9
            // 
            this.txtCoveredMonth9_9.Height = 0.1770833F;
            this.txtCoveredMonth9_9.Left = 8.556751F;
            this.txtCoveredMonth9_9.Name = "txtCoveredMonth9_9";
            this.txtCoveredMonth9_9.Text = "X";
            this.txtCoveredMonth9_9.Top = 4.908249F;
            this.txtCoveredMonth9_9.Width = 0.25F;
            // 
            // txtCoveredMonth10_9
            // 
            this.txtCoveredMonth10_9.Height = 0.1770833F;
            this.txtCoveredMonth10_9.Left = 8.950501F;
            this.txtCoveredMonth10_9.Name = "txtCoveredMonth10_9";
            this.txtCoveredMonth10_9.Text = "X";
            this.txtCoveredMonth10_9.Top = 4.908249F;
            this.txtCoveredMonth10_9.Width = 0.25F;
            // 
            // txtCoveredMonth11_9
            // 
            this.txtCoveredMonth11_9.Height = 0.1770833F;
            this.txtCoveredMonth11_9.Left = 9.34425F;
            this.txtCoveredMonth11_9.Name = "txtCoveredMonth11_9";
            this.txtCoveredMonth11_9.Text = "X";
            this.txtCoveredMonth11_9.Top = 4.908249F;
            this.txtCoveredMonth11_9.Width = 0.25F;
            // 
            // txtCoveredMonth12_9
            // 
            this.txtCoveredMonth12_9.Height = 0.1770833F;
            this.txtCoveredMonth12_9.Left = 9.738001F;
            this.txtCoveredMonth12_9.Name = "txtCoveredMonth12_9";
            this.txtCoveredMonth12_9.Text = "X";
            this.txtCoveredMonth12_9.Top = 4.908249F;
            this.txtCoveredMonth12_9.Width = 0.1944444F;
            // 
            // txtCoveredAll12_10
            // 
            this.txtCoveredAll12_10.Height = 0.1770833F;
            this.txtCoveredAll12_10.Left = 4.815584F;
            this.txtCoveredAll12_10.Name = "txtCoveredAll12_10";
            this.txtCoveredAll12_10.Text = "X";
            this.txtCoveredAll12_10.Top = 5.396168F;
            this.txtCoveredAll12_10.Width = 0.25F;
            // 
            // txtCoveredMonth1_10
            // 
            this.txtCoveredMonth1_10.Height = 0.1770833F;
            this.txtCoveredMonth1_10.Left = 5.326F;
            this.txtCoveredMonth1_10.Name = "txtCoveredMonth1_10";
            this.txtCoveredMonth1_10.Text = "X";
            this.txtCoveredMonth1_10.Top = 5.396168F;
            this.txtCoveredMonth1_10.Width = 0.25F;
            // 
            // txtCoveredMonth2_10
            // 
            this.txtCoveredMonth2_10.Height = 0.1770833F;
            this.txtCoveredMonth2_10.Left = 5.729752F;
            this.txtCoveredMonth2_10.Name = "txtCoveredMonth2_10";
            this.txtCoveredMonth2_10.Text = "X";
            this.txtCoveredMonth2_10.Top = 5.396168F;
            this.txtCoveredMonth2_10.Width = 0.25F;
            // 
            // txtCoveredMonth3_10
            // 
            this.txtCoveredMonth3_10.Height = 0.1770833F;
            this.txtCoveredMonth3_10.Left = 6.1335F;
            this.txtCoveredMonth3_10.Name = "txtCoveredMonth3_10";
            this.txtCoveredMonth3_10.Text = "X";
            this.txtCoveredMonth3_10.Top = 5.396168F;
            this.txtCoveredMonth3_10.Width = 0.25F;
            // 
            // txtCoveredMonth4_10
            // 
            this.txtCoveredMonth4_10.Height = 0.1770833F;
            this.txtCoveredMonth4_10.Left = 6.537251F;
            this.txtCoveredMonth4_10.Name = "txtCoveredMonth4_10";
            this.txtCoveredMonth4_10.Text = "X";
            this.txtCoveredMonth4_10.Top = 5.396168F;
            this.txtCoveredMonth4_10.Width = 0.25F;
            // 
            // txtCoveredMonth5_10
            // 
            this.txtCoveredMonth5_10.Height = 0.1770833F;
            this.txtCoveredMonth5_10.Left = 6.941001F;
            this.txtCoveredMonth5_10.Name = "txtCoveredMonth5_10";
            this.txtCoveredMonth5_10.Text = "X";
            this.txtCoveredMonth5_10.Top = 5.396168F;
            this.txtCoveredMonth5_10.Width = 0.25F;
            // 
            // txtCoveredMonth6_10
            // 
            this.txtCoveredMonth6_10.Height = 0.1770833F;
            this.txtCoveredMonth6_10.Left = 7.344751F;
            this.txtCoveredMonth6_10.Name = "txtCoveredMonth6_10";
            this.txtCoveredMonth6_10.Text = "X";
            this.txtCoveredMonth6_10.Top = 5.396168F;
            this.txtCoveredMonth6_10.Width = 0.25F;
            // 
            // txtCoveredMonth7_10
            // 
            this.txtCoveredMonth7_10.Height = 0.1770833F;
            this.txtCoveredMonth7_10.Left = 7.748501F;
            this.txtCoveredMonth7_10.Name = "txtCoveredMonth7_10";
            this.txtCoveredMonth7_10.Text = "X";
            this.txtCoveredMonth7_10.Top = 5.396168F;
            this.txtCoveredMonth7_10.Width = 0.25F;
            // 
            // txtCoveredMonth8_10
            // 
            this.txtCoveredMonth8_10.Height = 0.1770833F;
            this.txtCoveredMonth8_10.Left = 8.152251F;
            this.txtCoveredMonth8_10.Name = "txtCoveredMonth8_10";
            this.txtCoveredMonth8_10.Text = "X";
            this.txtCoveredMonth8_10.Top = 5.396168F;
            this.txtCoveredMonth8_10.Width = 0.25F;
            // 
            // txtCoveredMonth9_10
            // 
            this.txtCoveredMonth9_10.Height = 0.1770833F;
            this.txtCoveredMonth9_10.Left = 8.556751F;
            this.txtCoveredMonth9_10.Name = "txtCoveredMonth9_10";
            this.txtCoveredMonth9_10.Text = "X";
            this.txtCoveredMonth9_10.Top = 5.396167F;
            this.txtCoveredMonth9_10.Width = 0.25F;
            // 
            // txtCoveredMonth10_10
            // 
            this.txtCoveredMonth10_10.Height = 0.1770833F;
            this.txtCoveredMonth10_10.Left = 8.950501F;
            this.txtCoveredMonth10_10.Name = "txtCoveredMonth10_10";
            this.txtCoveredMonth10_10.Text = "X";
            this.txtCoveredMonth10_10.Top = 5.396167F;
            this.txtCoveredMonth10_10.Width = 0.25F;
            // 
            // txtCoveredMonth11_10
            // 
            this.txtCoveredMonth11_10.Height = 0.1770833F;
            this.txtCoveredMonth11_10.Left = 9.34425F;
            this.txtCoveredMonth11_10.Name = "txtCoveredMonth11_10";
            this.txtCoveredMonth11_10.Text = "X";
            this.txtCoveredMonth11_10.Top = 5.396167F;
            this.txtCoveredMonth11_10.Width = 0.25F;
            // 
            // txtCoveredMonth12_10
            // 
            this.txtCoveredMonth12_10.Height = 0.1770833F;
            this.txtCoveredMonth12_10.Left = 9.738001F;
            this.txtCoveredMonth12_10.Name = "txtCoveredMonth12_10";
            this.txtCoveredMonth12_10.Text = "X";
            this.txtCoveredMonth12_10.Top = 5.396167F;
            this.txtCoveredMonth12_10.Width = 0.1944444F;
            // 
            // txtCoveredAll12_11
            // 
            this.txtCoveredAll12_11.Height = 0.1770833F;
            this.txtCoveredAll12_11.Left = 4.815584F;
            this.txtCoveredAll12_11.Name = "txtCoveredAll12_11";
            this.txtCoveredAll12_11.Text = "X";
            this.txtCoveredAll12_11.Top = 5.884085F;
            this.txtCoveredAll12_11.Width = 0.25F;
            // 
            // txtCoveredMonth1_11
            // 
            this.txtCoveredMonth1_11.Height = 0.1770833F;
            this.txtCoveredMonth1_11.Left = 5.326F;
            this.txtCoveredMonth1_11.Name = "txtCoveredMonth1_11";
            this.txtCoveredMonth1_11.Text = "X";
            this.txtCoveredMonth1_11.Top = 5.884085F;
            this.txtCoveredMonth1_11.Width = 0.25F;
            // 
            // txtCoveredMonth2_11
            // 
            this.txtCoveredMonth2_11.Height = 0.1770833F;
            this.txtCoveredMonth2_11.Left = 5.729752F;
            this.txtCoveredMonth2_11.Name = "txtCoveredMonth2_11";
            this.txtCoveredMonth2_11.Text = "X";
            this.txtCoveredMonth2_11.Top = 5.884085F;
            this.txtCoveredMonth2_11.Width = 0.25F;
            // 
            // txtCoveredMonth3_11
            // 
            this.txtCoveredMonth3_11.Height = 0.1770833F;
            this.txtCoveredMonth3_11.Left = 6.1335F;
            this.txtCoveredMonth3_11.Name = "txtCoveredMonth3_11";
            this.txtCoveredMonth3_11.Text = "X";
            this.txtCoveredMonth3_11.Top = 5.884085F;
            this.txtCoveredMonth3_11.Width = 0.25F;
            // 
            // txtCoveredMonth4_11
            // 
            this.txtCoveredMonth4_11.Height = 0.1770833F;
            this.txtCoveredMonth4_11.Left = 6.537251F;
            this.txtCoveredMonth4_11.Name = "txtCoveredMonth4_11";
            this.txtCoveredMonth4_11.Text = "X";
            this.txtCoveredMonth4_11.Top = 5.884085F;
            this.txtCoveredMonth4_11.Width = 0.25F;
            // 
            // txtCoveredMonth5_11
            // 
            this.txtCoveredMonth5_11.Height = 0.1770833F;
            this.txtCoveredMonth5_11.Left = 6.941001F;
            this.txtCoveredMonth5_11.Name = "txtCoveredMonth5_11";
            this.txtCoveredMonth5_11.Text = "X";
            this.txtCoveredMonth5_11.Top = 5.884085F;
            this.txtCoveredMonth5_11.Width = 0.25F;
            // 
            // txtCoveredMonth6_11
            // 
            this.txtCoveredMonth6_11.Height = 0.1770833F;
            this.txtCoveredMonth6_11.Left = 7.344751F;
            this.txtCoveredMonth6_11.Name = "txtCoveredMonth6_11";
            this.txtCoveredMonth6_11.Text = "X";
            this.txtCoveredMonth6_11.Top = 5.884085F;
            this.txtCoveredMonth6_11.Width = 0.25F;
            // 
            // txtCoveredMonth7_11
            // 
            this.txtCoveredMonth7_11.Height = 0.1770833F;
            this.txtCoveredMonth7_11.Left = 7.748501F;
            this.txtCoveredMonth7_11.Name = "txtCoveredMonth7_11";
            this.txtCoveredMonth7_11.Text = "X";
            this.txtCoveredMonth7_11.Top = 5.884085F;
            this.txtCoveredMonth7_11.Width = 0.25F;
            // 
            // txtCoveredMonth8_11
            // 
            this.txtCoveredMonth8_11.Height = 0.1770833F;
            this.txtCoveredMonth8_11.Left = 8.152251F;
            this.txtCoveredMonth8_11.Name = "txtCoveredMonth8_11";
            this.txtCoveredMonth8_11.Text = "X";
            this.txtCoveredMonth8_11.Top = 5.884085F;
            this.txtCoveredMonth8_11.Width = 0.25F;
            // 
            // txtCoveredMonth9_11
            // 
            this.txtCoveredMonth9_11.Height = 0.1770833F;
            this.txtCoveredMonth9_11.Left = 8.556751F;
            this.txtCoveredMonth9_11.Name = "txtCoveredMonth9_11";
            this.txtCoveredMonth9_11.Text = "X";
            this.txtCoveredMonth9_11.Top = 5.884084F;
            this.txtCoveredMonth9_11.Width = 0.25F;
            // 
            // txtCoveredMonth10_11
            // 
            this.txtCoveredMonth10_11.Height = 0.1770833F;
            this.txtCoveredMonth10_11.Left = 8.950501F;
            this.txtCoveredMonth10_11.Name = "txtCoveredMonth10_11";
            this.txtCoveredMonth10_11.Text = "X";
            this.txtCoveredMonth10_11.Top = 5.884084F;
            this.txtCoveredMonth10_11.Width = 0.25F;
            // 
            // txtCoveredMonth11_11
            // 
            this.txtCoveredMonth11_11.Height = 0.1770833F;
            this.txtCoveredMonth11_11.Left = 9.34425F;
            this.txtCoveredMonth11_11.Name = "txtCoveredMonth11_11";
            this.txtCoveredMonth11_11.Text = "X";
            this.txtCoveredMonth11_11.Top = 5.884084F;
            this.txtCoveredMonth11_11.Width = 0.25F;
            // 
            // txtCoveredMonth12_11
            // 
            this.txtCoveredMonth12_11.Height = 0.1770833F;
            this.txtCoveredMonth12_11.Left = 9.738001F;
            this.txtCoveredMonth12_11.Name = "txtCoveredMonth12_11";
            this.txtCoveredMonth12_11.Text = "X";
            this.txtCoveredMonth12_11.Top = 5.884084F;
            this.txtCoveredMonth12_11.Width = 0.1944444F;
            // 
            // txtCoveredAll12_12
            // 
            this.txtCoveredAll12_12.Height = 0.1770833F;
            this.txtCoveredAll12_12.Left = 4.815584F;
            this.txtCoveredAll12_12.Name = "txtCoveredAll12_12";
            this.txtCoveredAll12_12.Text = "X";
            this.txtCoveredAll12_12.Top = 6.372F;
            this.txtCoveredAll12_12.Width = 0.25F;
            // 
            // txtCoveredMonth2_12
            // 
            this.txtCoveredMonth2_12.Height = 0.1770833F;
            this.txtCoveredMonth2_12.Left = 5.729752F;
            this.txtCoveredMonth2_12.Name = "txtCoveredMonth2_12";
            this.txtCoveredMonth2_12.Text = "X";
            this.txtCoveredMonth2_12.Top = 6.372F;
            this.txtCoveredMonth2_12.Width = 0.25F;
            // 
            // txtCoveredMonth3_12
            // 
            this.txtCoveredMonth3_12.Height = 0.1770833F;
            this.txtCoveredMonth3_12.Left = 6.1335F;
            this.txtCoveredMonth3_12.Name = "txtCoveredMonth3_12";
            this.txtCoveredMonth3_12.Text = "X";
            this.txtCoveredMonth3_12.Top = 6.372F;
            this.txtCoveredMonth3_12.Width = 0.25F;
            // 
            // txtCoveredMonth4_12
            // 
            this.txtCoveredMonth4_12.Height = 0.1770833F;
            this.txtCoveredMonth4_12.Left = 6.537251F;
            this.txtCoveredMonth4_12.Name = "txtCoveredMonth4_12";
            this.txtCoveredMonth4_12.Text = "X";
            this.txtCoveredMonth4_12.Top = 6.372F;
            this.txtCoveredMonth4_12.Width = 0.25F;
            // 
            // txtCoveredMonth5_12
            // 
            this.txtCoveredMonth5_12.Height = 0.1770833F;
            this.txtCoveredMonth5_12.Left = 6.941001F;
            this.txtCoveredMonth5_12.Name = "txtCoveredMonth5_12";
            this.txtCoveredMonth5_12.Text = "X";
            this.txtCoveredMonth5_12.Top = 6.372F;
            this.txtCoveredMonth5_12.Width = 0.25F;
            // 
            // txtCoveredMonth6_12
            // 
            this.txtCoveredMonth6_12.Height = 0.1770833F;
            this.txtCoveredMonth6_12.Left = 7.344751F;
            this.txtCoveredMonth6_12.Name = "txtCoveredMonth6_12";
            this.txtCoveredMonth6_12.Text = "X";
            this.txtCoveredMonth6_12.Top = 6.372F;
            this.txtCoveredMonth6_12.Width = 0.25F;
            // 
            // txtCoveredMonth7_12
            // 
            this.txtCoveredMonth7_12.Height = 0.1770833F;
            this.txtCoveredMonth7_12.Left = 7.748501F;
            this.txtCoveredMonth7_12.Name = "txtCoveredMonth7_12";
            this.txtCoveredMonth7_12.Text = "X";
            this.txtCoveredMonth7_12.Top = 6.372F;
            this.txtCoveredMonth7_12.Width = 0.25F;
            // 
            // txtCoveredMonth8_12
            // 
            this.txtCoveredMonth8_12.Height = 0.1770833F;
            this.txtCoveredMonth8_12.Left = 8.152251F;
            this.txtCoveredMonth8_12.Name = "txtCoveredMonth8_12";
            this.txtCoveredMonth8_12.Text = "X";
            this.txtCoveredMonth8_12.Top = 6.372F;
            this.txtCoveredMonth8_12.Width = 0.25F;
            // 
            // txtCoveredMonth9_12
            // 
            this.txtCoveredMonth9_12.Height = 0.1770833F;
            this.txtCoveredMonth9_12.Left = 8.556751F;
            this.txtCoveredMonth9_12.Name = "txtCoveredMonth9_12";
            this.txtCoveredMonth9_12.Text = "X";
            this.txtCoveredMonth9_12.Top = 6.371999F;
            this.txtCoveredMonth9_12.Width = 0.25F;
            // 
            // txtCoveredMonth10_12
            // 
            this.txtCoveredMonth10_12.Height = 0.1770833F;
            this.txtCoveredMonth10_12.Left = 8.950501F;
            this.txtCoveredMonth10_12.Name = "txtCoveredMonth10_12";
            this.txtCoveredMonth10_12.Text = "X";
            this.txtCoveredMonth10_12.Top = 6.371999F;
            this.txtCoveredMonth10_12.Width = 0.25F;
            // 
            // txtCoveredMonth11_12
            // 
            this.txtCoveredMonth11_12.Height = 0.1770833F;
            this.txtCoveredMonth11_12.Left = 9.34425F;
            this.txtCoveredMonth11_12.Name = "txtCoveredMonth11_12";
            this.txtCoveredMonth11_12.Text = "X";
            this.txtCoveredMonth11_12.Top = 6.371999F;
            this.txtCoveredMonth11_12.Width = 0.25F;
            // 
            // txtCoveredMonth12_12
            // 
            this.txtCoveredMonth12_12.Height = 0.1770833F;
            this.txtCoveredMonth12_12.Left = 9.738001F;
            this.txtCoveredMonth12_12.Name = "txtCoveredMonth12_12";
            this.txtCoveredMonth12_12.Text = "X";
            this.txtCoveredMonth12_12.Top = 6.371999F;
            this.txtCoveredMonth12_12.Width = 0.1944447F;
            // 
            // txtCoveredLast1
            // 
            this.txtCoveredLast1.CanGrow = false;
            this.txtCoveredLast1.Height = 0.1770833F;
            this.txtCoveredLast1.Left = 1.605F;
            this.txtCoveredLast1.Name = "txtCoveredLast1";
            this.txtCoveredLast1.Text = null;
            this.txtCoveredLast1.Top = 0.9810001F;
            this.txtCoveredLast1.Width = 0.7916667F;
            // 
            // txtCoveredLast2
            // 
            this.txtCoveredLast2.CanGrow = false;
            this.txtCoveredLast2.Height = 0.1770833F;
            this.txtCoveredLast2.Left = 1.605F;
            this.txtCoveredLast2.Name = "txtCoveredLast2";
            this.txtCoveredLast2.Text = null;
            this.txtCoveredLast2.Top = 1.481F;
            this.txtCoveredLast2.Width = 0.7916667F;
            // 
            // txtCoveredLast3
            // 
            this.txtCoveredLast3.CanGrow = false;
            this.txtCoveredLast3.Height = 0.1770833F;
            this.txtCoveredLast3.Left = 1.605F;
            this.txtCoveredLast3.Name = "txtCoveredLast3";
            this.txtCoveredLast3.Text = null;
            this.txtCoveredLast3.Top = 1.981F;
            this.txtCoveredLast3.Width = 0.7916667F;
            // 
            // txtCoveredLast4
            // 
            this.txtCoveredLast4.CanGrow = false;
            this.txtCoveredLast4.Height = 0.1770833F;
            this.txtCoveredLast4.Left = 1.605F;
            this.txtCoveredLast4.Name = "txtCoveredLast4";
            this.txtCoveredLast4.Text = null;
            this.txtCoveredLast4.Top = 2.471F;
            this.txtCoveredLast4.Width = 0.7916667F;
            // 
            // txtCoveredLast5
            // 
            this.txtCoveredLast5.CanGrow = false;
            this.txtCoveredLast5.Height = 0.1770833F;
            this.txtCoveredLast5.Left = 1.605F;
            this.txtCoveredLast5.Name = "txtCoveredLast5";
            this.txtCoveredLast5.Text = null;
            this.txtCoveredLast5.Top = 2.961F;
            this.txtCoveredLast5.Width = 0.7916667F;
            // 
            // txtCoveredLast6
            // 
            this.txtCoveredLast6.CanGrow = false;
            this.txtCoveredLast6.Height = 0.1770833F;
            this.txtCoveredLast6.Left = 1.605F;
            this.txtCoveredLast6.Name = "txtCoveredLast6";
            this.txtCoveredLast6.Text = null;
            this.txtCoveredLast6.Top = 3.440583F;
            this.txtCoveredLast6.Width = 0.7916667F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.CanGrow = false;
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 1.240084F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 0.9807503F;
            this.txtCoveredMiddle1.Width = 0.2916667F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.CanGrow = false;
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 1.240084F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 1.48075F;
            this.txtCoveredMiddle2.Width = 0.2916667F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.CanGrow = false;
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 1.240084F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 1.98075F;
            this.txtCoveredMiddle3.Width = 0.2916667F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.CanGrow = false;
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 1.240084F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 2.47075F;
            this.txtCoveredMiddle4.Width = 0.2916667F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.CanGrow = false;
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 1.240084F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 2.96075F;
            this.txtCoveredMiddle5.Width = 0.2916667F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.CanGrow = false;
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 1.240084F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 3.440333F;
            this.txtCoveredMiddle6.Width = 0.2916667F;
            // 
            // txtCoveredLast7
            // 
            this.txtCoveredLast7.CanGrow = false;
            this.txtCoveredLast7.Height = 0.1770833F;
            this.txtCoveredLast7.Left = 1.605F;
            this.txtCoveredLast7.Name = "txtCoveredLast7";
            this.txtCoveredLast7.Text = null;
            this.txtCoveredLast7.Top = 3.930583F;
            this.txtCoveredLast7.Width = 0.7916667F;
            // 
            // txtCoveredLast8
            // 
            this.txtCoveredLast8.CanGrow = false;
            this.txtCoveredLast8.Height = 0.1770833F;
            this.txtCoveredLast8.Left = 1.605F;
            this.txtCoveredLast8.Name = "txtCoveredLast8";
            this.txtCoveredLast8.Text = null;
            this.txtCoveredLast8.Top = 4.420583F;
            this.txtCoveredLast8.Width = 0.7916667F;
            // 
            // txtCoveredLast9
            // 
            this.txtCoveredLast9.CanGrow = false;
            this.txtCoveredLast9.Height = 0.1770833F;
            this.txtCoveredLast9.Left = 1.605F;
            this.txtCoveredLast9.Name = "txtCoveredLast9";
            this.txtCoveredLast9.Text = null;
            this.txtCoveredLast9.Top = 4.910583F;
            this.txtCoveredLast9.Width = 0.7916667F;
            // 
            // txtCoveredLast10
            // 
            this.txtCoveredLast10.CanGrow = false;
            this.txtCoveredLast10.Height = 0.1770833F;
            this.txtCoveredLast10.Left = 1.605F;
            this.txtCoveredLast10.Name = "txtCoveredLast10";
            this.txtCoveredLast10.Text = null;
            this.txtCoveredLast10.Top = 5.400583F;
            this.txtCoveredLast10.Width = 0.7916667F;
            // 
            // txtCoveredLast11
            // 
            this.txtCoveredLast11.CanGrow = false;
            this.txtCoveredLast11.Height = 0.1770833F;
            this.txtCoveredLast11.Left = 1.605F;
            this.txtCoveredLast11.Name = "txtCoveredLast11";
            this.txtCoveredLast11.Text = null;
            this.txtCoveredLast11.Top = 5.880167F;
            this.txtCoveredLast11.Width = 0.7916667F;
            // 
            // txtCoveredLast12
            // 
            this.txtCoveredLast12.CanGrow = false;
            this.txtCoveredLast12.Height = 0.1770833F;
            this.txtCoveredLast12.Left = 1.605F;
            this.txtCoveredLast12.Name = "txtCoveredLast12";
            this.txtCoveredLast12.Text = null;
            this.txtCoveredLast12.Top = 6.370166F;
            this.txtCoveredLast12.Width = 0.7916667F;
            // 
            // txtCoveredMiddle7
            // 
            this.txtCoveredMiddle7.CanGrow = false;
            this.txtCoveredMiddle7.Height = 0.1770833F;
            this.txtCoveredMiddle7.Left = 1.240084F;
            this.txtCoveredMiddle7.Name = "txtCoveredMiddle7";
            this.txtCoveredMiddle7.Text = null;
            this.txtCoveredMiddle7.Top = 3.930334F;
            this.txtCoveredMiddle7.Width = 0.2916667F;
            // 
            // txtCoveredMiddle8
            // 
            this.txtCoveredMiddle8.CanGrow = false;
            this.txtCoveredMiddle8.Height = 0.1770833F;
            this.txtCoveredMiddle8.Left = 1.240084F;
            this.txtCoveredMiddle8.Name = "txtCoveredMiddle8";
            this.txtCoveredMiddle8.Text = null;
            this.txtCoveredMiddle8.Top = 4.420333F;
            this.txtCoveredMiddle8.Width = 0.2916667F;
            // 
            // txtCoveredMiddle9
            // 
            this.txtCoveredMiddle9.CanGrow = false;
            this.txtCoveredMiddle9.Height = 0.1770833F;
            this.txtCoveredMiddle9.Left = 1.240084F;
            this.txtCoveredMiddle9.Name = "txtCoveredMiddle9";
            this.txtCoveredMiddle9.Text = null;
            this.txtCoveredMiddle9.Top = 4.910334F;
            this.txtCoveredMiddle9.Width = 0.2916667F;
            // 
            // txtCoveredMiddle10
            // 
            this.txtCoveredMiddle10.CanGrow = false;
            this.txtCoveredMiddle10.Height = 0.1770833F;
            this.txtCoveredMiddle10.Left = 1.240084F;
            this.txtCoveredMiddle10.Name = "txtCoveredMiddle10";
            this.txtCoveredMiddle10.Text = null;
            this.txtCoveredMiddle10.Top = 5.400333F;
            this.txtCoveredMiddle10.Width = 0.2916667F;
            // 
            // txtCoveredMiddle11
            // 
            this.txtCoveredMiddle11.CanGrow = false;
            this.txtCoveredMiddle11.Height = 0.1770833F;
            this.txtCoveredMiddle11.Left = 1.240084F;
            this.txtCoveredMiddle11.Name = "txtCoveredMiddle11";
            this.txtCoveredMiddle11.Text = null;
            this.txtCoveredMiddle11.Top = 5.879917F;
            this.txtCoveredMiddle11.Width = 0.2916667F;
            // 
            // txtCoveredMiddle12
            // 
            this.txtCoveredMiddle12.CanGrow = false;
            this.txtCoveredMiddle12.Height = 0.1770833F;
            this.txtCoveredMiddle12.Left = 1.240084F;
            this.txtCoveredMiddle12.Name = "txtCoveredMiddle12";
            this.txtCoveredMiddle12.Text = null;
            this.txtCoveredMiddle12.Top = 6.369916F;
            this.txtCoveredMiddle12.Width = 0.2916667F;
            // 
            // txtCoveredName13
            // 
            this.txtCoveredName13.Height = 0.1770833F;
            this.txtCoveredName13.Left = 0.1880002F;
            this.txtCoveredName13.Name = "txtCoveredName13";
            this.txtCoveredName13.Text = null;
            this.txtCoveredName13.Top = 6.86F;
            this.txtCoveredName13.Width = 0.9791667F;
            // 
            // txtCoveredSSN13
            // 
            this.txtCoveredSSN13.Height = 0.1770833F;
            this.txtCoveredSSN13.Left = 2.510918F;
            this.txtCoveredSSN13.Name = "txtCoveredSSN13";
            this.txtCoveredSSN13.Text = null;
            this.txtCoveredSSN13.Top = 6.859918F;
            this.txtCoveredSSN13.Width = 1F;
            // 
            // txtCoveredDOB13
            // 
            this.txtCoveredDOB13.Height = 0.1770833F;
            this.txtCoveredDOB13.Left = 3.740084F;
            this.txtCoveredDOB13.Name = "txtCoveredDOB13";
            this.txtCoveredDOB13.Text = null;
            this.txtCoveredDOB13.Top = 6.859918F;
            this.txtCoveredDOB13.Width = 0.9166667F;
            // 
            // txtCoveredAll12_13
            // 
            this.txtCoveredAll12_13.Height = 0.1770833F;
            this.txtCoveredAll12_13.Left = 4.815584F;
            this.txtCoveredAll12_13.Name = "txtCoveredAll12_13";
            this.txtCoveredAll12_13.Text = "X";
            this.txtCoveredAll12_13.Top = 6.862001F;
            this.txtCoveredAll12_13.Width = 0.25F;
            // 
            // txtCoveredMonth1_13
            // 
            this.txtCoveredMonth1_13.Height = 0.1770833F;
            this.txtCoveredMonth1_13.Left = 5.326F;
            this.txtCoveredMonth1_13.Name = "txtCoveredMonth1_13";
            this.txtCoveredMonth1_13.Text = "X";
            this.txtCoveredMonth1_13.Top = 6.862001F;
            this.txtCoveredMonth1_13.Width = 0.25F;
            // 
            // txtCoveredMonth2_13
            // 
            this.txtCoveredMonth2_13.Height = 0.1770833F;
            this.txtCoveredMonth2_13.Left = 5.729752F;
            this.txtCoveredMonth2_13.Name = "txtCoveredMonth2_13";
            this.txtCoveredMonth2_13.Text = "X";
            this.txtCoveredMonth2_13.Top = 6.862001F;
            this.txtCoveredMonth2_13.Width = 0.25F;
            // 
            // txtCoveredMonth4_13
            // 
            this.txtCoveredMonth4_13.Height = 0.1770833F;
            this.txtCoveredMonth4_13.Left = 6.537251F;
            this.txtCoveredMonth4_13.Name = "txtCoveredMonth4_13";
            this.txtCoveredMonth4_13.Text = "X";
            this.txtCoveredMonth4_13.Top = 6.862001F;
            this.txtCoveredMonth4_13.Width = 0.25F;
            // 
            // txtCoveredMonth5_13
            // 
            this.txtCoveredMonth5_13.Height = 0.1770833F;
            this.txtCoveredMonth5_13.Left = 6.941001F;
            this.txtCoveredMonth5_13.Name = "txtCoveredMonth5_13";
            this.txtCoveredMonth5_13.Text = "X";
            this.txtCoveredMonth5_13.Top = 6.862001F;
            this.txtCoveredMonth5_13.Width = 0.25F;
            // 
            // txtCoveredMonth6_13
            // 
            this.txtCoveredMonth6_13.Height = 0.1770833F;
            this.txtCoveredMonth6_13.Left = 7.344751F;
            this.txtCoveredMonth6_13.Name = "txtCoveredMonth6_13";
            this.txtCoveredMonth6_13.Text = "X";
            this.txtCoveredMonth6_13.Top = 6.862001F;
            this.txtCoveredMonth6_13.Width = 0.25F;
            // 
            // txtCoveredMonth9_13
            // 
            this.txtCoveredMonth9_13.Height = 0.1770833F;
            this.txtCoveredMonth9_13.Left = 8.556751F;
            this.txtCoveredMonth9_13.Name = "txtCoveredMonth9_13";
            this.txtCoveredMonth9_13.Text = "X";
            this.txtCoveredMonth9_13.Top = 6.862F;
            this.txtCoveredMonth9_13.Width = 0.25F;
            // 
            // txtCoveredMonth10_13
            // 
            this.txtCoveredMonth10_13.Height = 0.1770833F;
            this.txtCoveredMonth10_13.Left = 8.950501F;
            this.txtCoveredMonth10_13.Name = "txtCoveredMonth10_13";
            this.txtCoveredMonth10_13.Text = "X";
            this.txtCoveredMonth10_13.Top = 6.862F;
            this.txtCoveredMonth10_13.Width = 0.25F;
            // 
            // txtCoveredMonth11_13
            // 
            this.txtCoveredMonth11_13.Height = 0.1770833F;
            this.txtCoveredMonth11_13.Left = 9.34425F;
            this.txtCoveredMonth11_13.Name = "txtCoveredMonth11_13";
            this.txtCoveredMonth11_13.Text = "X";
            this.txtCoveredMonth11_13.Top = 6.862F;
            this.txtCoveredMonth11_13.Width = 0.25F;
            // 
            // txtCoveredLast13
            // 
            this.txtCoveredLast13.CanGrow = false;
            this.txtCoveredLast13.Height = 0.1770833F;
            this.txtCoveredLast13.Left = 1.605001F;
            this.txtCoveredLast13.Name = "txtCoveredLast13";
            this.txtCoveredLast13.Text = null;
            this.txtCoveredLast13.Top = 6.860168F;
            this.txtCoveredLast13.Width = 0.7916667F;
            // 
            // txtCoveredMiddle13
            // 
            this.txtCoveredMiddle13.CanGrow = false;
            this.txtCoveredMiddle13.Height = 0.1770833F;
            this.txtCoveredMiddle13.Left = 1.240084F;
            this.txtCoveredMiddle13.Name = "txtCoveredMiddle13";
            this.txtCoveredMiddle13.Text = null;
            this.txtCoveredMiddle13.Top = 6.859918F;
            this.txtCoveredMiddle13.Width = 0.2916667F;
            // 
            // txtCoveredIndividuals
            // 
            this.txtCoveredIndividuals.Height = 0.1770833F;
            this.txtCoveredIndividuals.Left = 9.103584F;
            this.txtCoveredIndividuals.Name = "txtCoveredIndividuals";
            this.txtCoveredIndividuals.Text = "X";
            this.txtCoveredIndividuals.Top = 0.2497504F;
            this.txtCoveredIndividuals.Width = 0.25F;
            // 
            // rpt1095C2020Page2
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 10.01133F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredIndividuals;
    }
}
