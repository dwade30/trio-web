﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2017BlankPortraitPage2.
	/// </summary>
	partial class rpt1095C2020BlankPortraitPage2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2020BlankPortraitPage2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbl12Months = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape82 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape83 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape84 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape85 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape90 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape91 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape92 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape93 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape94 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape95 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape96 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape97 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape102 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape103 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape104 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape105 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape106 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape107 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape108 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape109 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape114 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape115 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape116 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape117 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape118 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape119 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape120 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape121 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape126 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape127 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape128 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape129 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape130 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape131 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape132 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape133 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape138 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape139 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape140 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape141 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape142 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape143 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape144 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape145 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape150 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape151 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape152 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape153 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape154 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape155 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape156 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape157 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape162 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape163 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape164 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape165 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape166 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape167 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape168 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape169 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape174 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape175 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape176 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape177 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape178 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape179 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape180 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape181 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape186 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape187 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape188 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape189 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape190 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape191 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape192 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape193 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape198 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape199 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape200 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape201 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape202 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape203 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape204 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape205 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape210 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape211 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape212 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape213 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape214 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape215 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape216 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape217 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape222 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape223 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape224 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape225 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape226 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape227 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape228 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape229 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCoveredName1,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtCoveredName7,
            this.txtCoveredSSN7,
            this.txtCoveredDOB7,
            this.txtCoveredName8,
            this.txtCoveredSSN8,
            this.txtCoveredDOB8,
            this.txtCoveredAll12_7,
            this.txtCoveredMonth1_7,
            this.txtCoveredMonth2_7,
            this.txtCoveredMonth3_7,
            this.txtCoveredMonth4_7,
            this.txtCoveredMonth5_7,
            this.txtCoveredMonth6_7,
            this.txtCoveredMonth7_7,
            this.txtCoveredMonth8_7,
            this.txtCoveredMonth9_7,
            this.txtCoveredMonth10_7,
            this.txtCoveredMonth11_7,
            this.txtCoveredMonth12_7,
            this.txtCoveredAll12_8,
            this.txtCoveredMonth1_8,
            this.txtCoveredMonth2_8,
            this.txtCoveredMonth3_8,
            this.txtCoveredMonth4_8,
            this.txtCoveredMonth5_8,
            this.txtCoveredMonth6_8,
            this.txtCoveredMonth7_8,
            this.txtCoveredMonth8_8,
            this.txtCoveredMonth9_8,
            this.txtCoveredMonth10_8,
            this.txtCoveredMonth11_8,
            this.txtCoveredMonth12_8,
            this.Label14,
            this.Label13,
            this.Label9,
            this.Label4,
            this.Line2,
            this.Line9,
            this.Line14,
            this.Line18,
            this.Line19,
            this.Line22,
            this.lbl12Months,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label15,
            this.Label16,
            this.Shape77,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Line20,
            this.Line21,
            this.Line17,
            this.Line16,
            this.Line15,
            this.Line13,
            this.Line12,
            this.Line11,
            this.Line10,
            this.Line24,
            this.Line3,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Line8,
            this.Line26,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Shape78,
            this.Shape80,
            this.Shape81,
            this.Shape82,
            this.Shape83,
            this.Shape84,
            this.Shape85,
            this.Shape90,
            this.Shape91,
            this.Shape92,
            this.Shape93,
            this.Shape94,
            this.Shape95,
            this.Shape96,
            this.Shape97,
            this.Shape102,
            this.Shape103,
            this.Shape104,
            this.Shape105,
            this.Shape106,
            this.Shape107,
            this.Shape108,
            this.Shape109,
            this.Shape114,
            this.Shape115,
            this.Shape116,
            this.Shape117,
            this.Shape118,
            this.Shape119,
            this.Shape120,
            this.Shape121,
            this.Shape126,
            this.Shape127,
            this.Shape128,
            this.Shape129,
            this.Shape130,
            this.Shape131,
            this.Shape132,
            this.Shape133,
            this.Shape138,
            this.Shape139,
            this.Shape140,
            this.Shape141,
            this.Shape142,
            this.Shape143,
            this.Shape144,
            this.Shape145,
            this.Shape150,
            this.Shape151,
            this.Shape152,
            this.Shape153,
            this.Shape154,
            this.Shape155,
            this.Shape156,
            this.Shape157,
            this.Shape162,
            this.Shape163,
            this.Shape164,
            this.Shape165,
            this.Shape166,
            this.Shape167,
            this.Shape168,
            this.Shape169,
            this.Shape174,
            this.Shape175,
            this.Shape176,
            this.Shape177,
            this.Shape178,
            this.Shape179,
            this.Shape180,
            this.Shape181,
            this.Shape186,
            this.Shape187,
            this.Shape188,
            this.Shape189,
            this.Shape190,
            this.Shape191,
            this.Shape192,
            this.Shape193,
            this.Shape198,
            this.Shape199,
            this.Shape200,
            this.Shape201,
            this.Shape202,
            this.Shape203,
            this.Shape204,
            this.Shape205,
            this.Shape210,
            this.Shape211,
            this.Shape212,
            this.Shape213,
            this.Shape214,
            this.Shape215,
            this.Shape216,
            this.Shape217,
            this.Shape222,
            this.Shape223,
            this.Shape224,
            this.Shape225,
            this.Shape226,
            this.Shape227,
            this.Shape228,
            this.Shape229,
            this.Label24,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Label35,
            this.Label93,
            this.Label94,
            this.Label95,
            this.Line34,
            this.Line35,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.txtCoveredMiddle7,
            this.txtCoveredMiddle8,
            this.txtCoveredLastName1,
            this.txtCoveredLastName2,
            this.txtCoveredLastName3,
            this.txtCoveredLastName4,
            this.txtCoveredLastName5,
            this.txtCoveredLastName6,
            this.txtCoveredLastName7,
            this.txtCoveredLastName8,
            this.txtCoveredIndividuals,
            this.Line1,
            this.Label25,
            this.Line31,
            this.Line25,
            this.Line23,
            this.line27,
            this.shape1});
            this.Detail.Height = 9.979167F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.CanGrow = false;
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.1666667F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Style = "font-size: 8.5pt";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 0.9130001F;
            this.txtCoveredName1.Width = 0.625F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 1.833333F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Style = "font-size: 8.5pt";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 0.9130001F;
            this.txtCoveredSSN1.Width = 0.8333333F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 2.75F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Style = "font-size: 8.5pt";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 0.9130001F;
            this.txtCoveredDOB1.Width = 0.75F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.CanGrow = false;
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.1666667F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Style = "font-size: 8.5pt";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 1.277584F;
            this.txtCoveredName2.Width = 0.625F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 1.833333F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Style = "font-size: 8.5pt";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 1.281056F;
            this.txtCoveredSSN2.Width = 0.8333333F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 2.75F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Style = "font-size: 8.5pt";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 1.281056F;
            this.txtCoveredDOB2.Width = 0.75F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.CanGrow = false;
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.1666667F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Style = "font-size: 8.5pt";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 1.652584F;
            this.txtCoveredName3.Width = 0.625F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 1.833333F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Style = "font-size: 8.5pt";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 1.656056F;
            this.txtCoveredSSN3.Width = 0.8333333F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 2.75F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Style = "font-size: 8.5pt";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 1.656056F;
            this.txtCoveredDOB3.Width = 0.75F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.CanGrow = false;
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.1666667F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Style = "font-size: 8.5pt";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 2.027584F;
            this.txtCoveredName4.Width = 0.625F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 1.833333F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Style = "font-size: 8.5pt";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 2.031056F;
            this.txtCoveredSSN4.Width = 0.8333333F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 2.75F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Style = "font-size: 8.5pt";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 2.031056F;
            this.txtCoveredDOB4.Width = 0.75F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.CanGrow = false;
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.1666667F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Style = "font-size: 8.5pt";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 2.402584F;
            this.txtCoveredName5.Width = 0.625F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 1.833333F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Style = "font-size: 8.5pt";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 2.406056F;
            this.txtCoveredSSN5.Width = 0.8333333F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 2.75F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Style = "font-size: 8.5pt";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 2.406056F;
            this.txtCoveredDOB5.Width = 0.75F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.CanGrow = false;
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.1666667F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Style = "font-size: 8.5pt";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 2.777584F;
            this.txtCoveredName6.Width = 0.625F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 1.833333F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Style = "font-size: 8.5pt";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 2.781056F;
            this.txtCoveredSSN6.Width = 0.8333333F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 2.75F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Style = "font-size: 8.5pt";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 2.781056F;
            this.txtCoveredDOB6.Width = 0.75F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 3.609028F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = null;
            this.txtCoveredAll12_1.Top = 0.906056F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 3.996528F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = null;
            this.txtCoveredMonth1_1.Top = 0.9130001F;
            this.txtCoveredMonth1_1.Width = 0.25F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 4.291667F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = null;
            this.txtCoveredMonth2_1.Top = 0.9130001F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 4.586805F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = null;
            this.txtCoveredMonth3_1.Top = 0.9130001F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 4.881945F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = null;
            this.txtCoveredMonth4_1.Top = 0.9130001F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 5.177083F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = null;
            this.txtCoveredMonth5_1.Top = 0.9130001F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 5.472222F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = null;
            this.txtCoveredMonth6_1.Top = 0.9130001F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 5.767361F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = null;
            this.txtCoveredMonth7_1.Top = 0.9130001F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 6.0625F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = null;
            this.txtCoveredMonth8_1.Top = 0.9130001F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 6.357639F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = null;
            this.txtCoveredMonth9_1.Top = 0.9130001F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 6.652778F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = null;
            this.txtCoveredMonth10_1.Top = 0.9130001F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 6.947917F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = null;
            this.txtCoveredMonth11_1.Top = 0.9130001F;
            this.txtCoveredMonth11_1.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 7.243055F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = null;
            this.txtCoveredMonth12_1.Top = 0.9130001F;
            this.txtCoveredMonth12_1.Width = 0.1875F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 3.609028F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = null;
            this.txtCoveredAll12_2.Top = 1.281056F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 3.996528F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = null;
            this.txtCoveredMonth1_2.Top = 1.281056F;
            this.txtCoveredMonth1_2.Width = 0.25F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 4.291667F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = null;
            this.txtCoveredMonth2_2.Top = 1.281056F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 4.586805F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = null;
            this.txtCoveredMonth3_2.Top = 1.281056F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 4.881945F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = null;
            this.txtCoveredMonth4_2.Top = 1.281056F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 5.177083F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = null;
            this.txtCoveredMonth5_2.Top = 1.281056F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 5.472222F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = null;
            this.txtCoveredMonth6_2.Top = 1.281056F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 5.767361F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = null;
            this.txtCoveredMonth7_2.Top = 1.281056F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 6.0625F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = null;
            this.txtCoveredMonth8_2.Top = 1.281056F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 6.357639F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = null;
            this.txtCoveredMonth9_2.Top = 1.281056F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 6.652778F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = null;
            this.txtCoveredMonth10_2.Top = 1.281056F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 6.947917F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = null;
            this.txtCoveredMonth11_2.Top = 1.281056F;
            this.txtCoveredMonth11_2.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 7.243055F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = null;
            this.txtCoveredMonth12_2.Top = 1.281056F;
            this.txtCoveredMonth12_2.Width = 0.1875F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 3.609028F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = null;
            this.txtCoveredAll12_3.Top = 1.656056F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 3.996528F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = null;
            this.txtCoveredMonth1_3.Top = 1.656056F;
            this.txtCoveredMonth1_3.Width = 0.25F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 4.291667F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = null;
            this.txtCoveredMonth2_3.Top = 1.656056F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 4.586805F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = null;
            this.txtCoveredMonth3_3.Top = 1.656056F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 4.881945F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = null;
            this.txtCoveredMonth4_3.Top = 1.656056F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 5.177083F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = null;
            this.txtCoveredMonth5_3.Top = 1.656056F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 5.472222F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = null;
            this.txtCoveredMonth6_3.Top = 1.656056F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 5.767361F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = null;
            this.txtCoveredMonth7_3.Top = 1.656056F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 6.0625F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = null;
            this.txtCoveredMonth8_3.Top = 1.656056F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 6.357639F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = null;
            this.txtCoveredMonth9_3.Top = 1.656056F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 6.652778F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = null;
            this.txtCoveredMonth10_3.Top = 1.656056F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 6.947917F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = null;
            this.txtCoveredMonth11_3.Top = 1.656056F;
            this.txtCoveredMonth11_3.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 7.243055F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = null;
            this.txtCoveredMonth12_3.Top = 1.656056F;
            this.txtCoveredMonth12_3.Width = 0.1875F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 3.609028F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = null;
            this.txtCoveredAll12_4.Top = 2.031056F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 3.996528F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = null;
            this.txtCoveredMonth1_4.Top = 2.031056F;
            this.txtCoveredMonth1_4.Width = 0.25F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 4.291667F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = null;
            this.txtCoveredMonth2_4.Top = 2.031056F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 4.586805F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = null;
            this.txtCoveredMonth3_4.Top = 2.031056F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 4.881945F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = null;
            this.txtCoveredMonth4_4.Top = 2.031056F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 5.177083F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = null;
            this.txtCoveredMonth5_4.Top = 2.031056F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 5.472222F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = null;
            this.txtCoveredMonth6_4.Top = 2.031056F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 5.767361F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = null;
            this.txtCoveredMonth7_4.Top = 2.031056F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 6.0625F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = null;
            this.txtCoveredMonth8_4.Top = 2.031056F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 6.357639F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = null;
            this.txtCoveredMonth9_4.Top = 2.031056F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 6.652778F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = null;
            this.txtCoveredMonth10_4.Top = 2.031056F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 6.947917F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = null;
            this.txtCoveredMonth11_4.Top = 2.031056F;
            this.txtCoveredMonth11_4.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 7.243055F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = null;
            this.txtCoveredMonth12_4.Top = 2.031056F;
            this.txtCoveredMonth12_4.Width = 0.1875F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 3.609028F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = null;
            this.txtCoveredAll12_5.Top = 2.406056F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 3.996528F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = null;
            this.txtCoveredMonth1_5.Top = 2.406056F;
            this.txtCoveredMonth1_5.Width = 0.25F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 4.291667F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = null;
            this.txtCoveredMonth2_5.Top = 2.406056F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 4.586805F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = null;
            this.txtCoveredMonth3_5.Top = 2.406056F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 4.881945F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = null;
            this.txtCoveredMonth4_5.Top = 2.406056F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 5.177083F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = null;
            this.txtCoveredMonth5_5.Top = 2.406056F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 5.472222F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = null;
            this.txtCoveredMonth6_5.Top = 2.406056F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 5.767361F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = null;
            this.txtCoveredMonth7_5.Top = 2.406056F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 6.0625F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = null;
            this.txtCoveredMonth8_5.Top = 2.406056F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 6.357639F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = null;
            this.txtCoveredMonth9_5.Top = 2.406056F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 6.652778F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = null;
            this.txtCoveredMonth10_5.Top = 2.406056F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 6.947917F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = null;
            this.txtCoveredMonth11_5.Top = 2.406056F;
            this.txtCoveredMonth11_5.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 7.243055F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = null;
            this.txtCoveredMonth12_5.Top = 2.406056F;
            this.txtCoveredMonth12_5.Width = 0.1875F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 3.609028F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = null;
            this.txtCoveredAll12_6.Top = 2.781056F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 3.996528F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = null;
            this.txtCoveredMonth1_6.Top = 2.781056F;
            this.txtCoveredMonth1_6.Width = 0.25F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 4.291667F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = null;
            this.txtCoveredMonth2_6.Top = 2.781056F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 4.586805F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = null;
            this.txtCoveredMonth3_6.Top = 2.781056F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 4.881945F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = null;
            this.txtCoveredMonth4_6.Top = 2.781056F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 5.177083F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = null;
            this.txtCoveredMonth5_6.Top = 2.781056F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 5.472222F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = null;
            this.txtCoveredMonth6_6.Top = 2.781056F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 5.767361F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = null;
            this.txtCoveredMonth7_6.Top = 2.781056F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 6.0625F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = null;
            this.txtCoveredMonth8_6.Top = 2.781056F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 6.357639F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = null;
            this.txtCoveredMonth9_6.Top = 2.781056F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 6.652778F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = null;
            this.txtCoveredMonth10_6.Top = 2.781056F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 6.947917F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = null;
            this.txtCoveredMonth11_6.Top = 2.781056F;
            this.txtCoveredMonth11_6.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 7.243055F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = null;
            this.txtCoveredMonth12_6.Top = 2.781056F;
            this.txtCoveredMonth12_6.Width = 0.1875F;
            // 
            // txtCoveredName7
            // 
            this.txtCoveredName7.CanGrow = false;
            this.txtCoveredName7.Height = 0.1770833F;
            this.txtCoveredName7.Left = 0.1666667F;
            this.txtCoveredName7.Name = "txtCoveredName7";
            this.txtCoveredName7.Style = "font-size: 8.5pt";
            this.txtCoveredName7.Text = null;
            this.txtCoveredName7.Top = 3.152584F;
            this.txtCoveredName7.Width = 0.625F;
            // 
            // txtCoveredSSN7
            // 
            this.txtCoveredSSN7.Height = 0.1770833F;
            this.txtCoveredSSN7.Left = 1.833333F;
            this.txtCoveredSSN7.Name = "txtCoveredSSN7";
            this.txtCoveredSSN7.Style = "font-size: 8.5pt";
            this.txtCoveredSSN7.Text = null;
            this.txtCoveredSSN7.Top = 3.156056F;
            this.txtCoveredSSN7.Width = 0.8333333F;
            // 
            // txtCoveredDOB7
            // 
            this.txtCoveredDOB7.Height = 0.1770833F;
            this.txtCoveredDOB7.Left = 2.75F;
            this.txtCoveredDOB7.Name = "txtCoveredDOB7";
            this.txtCoveredDOB7.Style = "font-size: 8.5pt";
            this.txtCoveredDOB7.Text = null;
            this.txtCoveredDOB7.Top = 3.156056F;
            this.txtCoveredDOB7.Width = 0.75F;
            // 
            // txtCoveredName8
            // 
            this.txtCoveredName8.CanGrow = false;
            this.txtCoveredName8.Height = 0.1770833F;
            this.txtCoveredName8.Left = 0.1666667F;
            this.txtCoveredName8.Name = "txtCoveredName8";
            this.txtCoveredName8.Style = "font-size: 8.5pt";
            this.txtCoveredName8.Text = null;
            this.txtCoveredName8.Top = 3.527584F;
            this.txtCoveredName8.Width = 0.625F;
            // 
            // txtCoveredSSN8
            // 
            this.txtCoveredSSN8.Height = 0.1770833F;
            this.txtCoveredSSN8.Left = 1.833333F;
            this.txtCoveredSSN8.Name = "txtCoveredSSN8";
            this.txtCoveredSSN8.Style = "font-size: 8.5pt";
            this.txtCoveredSSN8.Text = null;
            this.txtCoveredSSN8.Top = 3.531056F;
            this.txtCoveredSSN8.Width = 0.8333333F;
            // 
            // txtCoveredDOB8
            // 
            this.txtCoveredDOB8.Height = 0.1770833F;
            this.txtCoveredDOB8.Left = 2.75F;
            this.txtCoveredDOB8.Name = "txtCoveredDOB8";
            this.txtCoveredDOB8.Style = "font-size: 8.5pt";
            this.txtCoveredDOB8.Text = null;
            this.txtCoveredDOB8.Top = 3.531056F;
            this.txtCoveredDOB8.Width = 0.75F;
            // 
            // txtCoveredAll12_7
            // 
            this.txtCoveredAll12_7.Height = 0.1770833F;
            this.txtCoveredAll12_7.Left = 3.609028F;
            this.txtCoveredAll12_7.Name = "txtCoveredAll12_7";
            this.txtCoveredAll12_7.Text = null;
            this.txtCoveredAll12_7.Top = 3.156056F;
            this.txtCoveredAll12_7.Width = 0.25F;
            // 
            // txtCoveredMonth1_7
            // 
            this.txtCoveredMonth1_7.Height = 0.1770833F;
            this.txtCoveredMonth1_7.Left = 3.996528F;
            this.txtCoveredMonth1_7.Name = "txtCoveredMonth1_7";
            this.txtCoveredMonth1_7.Text = null;
            this.txtCoveredMonth1_7.Top = 3.156056F;
            this.txtCoveredMonth1_7.Width = 0.25F;
            // 
            // txtCoveredMonth2_7
            // 
            this.txtCoveredMonth2_7.Height = 0.1770833F;
            this.txtCoveredMonth2_7.Left = 4.291667F;
            this.txtCoveredMonth2_7.Name = "txtCoveredMonth2_7";
            this.txtCoveredMonth2_7.Text = null;
            this.txtCoveredMonth2_7.Top = 3.156056F;
            this.txtCoveredMonth2_7.Width = 0.25F;
            // 
            // txtCoveredMonth3_7
            // 
            this.txtCoveredMonth3_7.Height = 0.1770833F;
            this.txtCoveredMonth3_7.Left = 4.586805F;
            this.txtCoveredMonth3_7.Name = "txtCoveredMonth3_7";
            this.txtCoveredMonth3_7.Text = null;
            this.txtCoveredMonth3_7.Top = 3.156056F;
            this.txtCoveredMonth3_7.Width = 0.25F;
            // 
            // txtCoveredMonth4_7
            // 
            this.txtCoveredMonth4_7.Height = 0.1770833F;
            this.txtCoveredMonth4_7.Left = 4.881945F;
            this.txtCoveredMonth4_7.Name = "txtCoveredMonth4_7";
            this.txtCoveredMonth4_7.Text = null;
            this.txtCoveredMonth4_7.Top = 3.156056F;
            this.txtCoveredMonth4_7.Width = 0.25F;
            // 
            // txtCoveredMonth5_7
            // 
            this.txtCoveredMonth5_7.Height = 0.1770833F;
            this.txtCoveredMonth5_7.Left = 5.177083F;
            this.txtCoveredMonth5_7.Name = "txtCoveredMonth5_7";
            this.txtCoveredMonth5_7.Text = null;
            this.txtCoveredMonth5_7.Top = 3.156056F;
            this.txtCoveredMonth5_7.Width = 0.25F;
            // 
            // txtCoveredMonth6_7
            // 
            this.txtCoveredMonth6_7.Height = 0.1770833F;
            this.txtCoveredMonth6_7.Left = 5.472222F;
            this.txtCoveredMonth6_7.Name = "txtCoveredMonth6_7";
            this.txtCoveredMonth6_7.Text = null;
            this.txtCoveredMonth6_7.Top = 3.156056F;
            this.txtCoveredMonth6_7.Width = 0.25F;
            // 
            // txtCoveredMonth7_7
            // 
            this.txtCoveredMonth7_7.Height = 0.1770833F;
            this.txtCoveredMonth7_7.Left = 5.767361F;
            this.txtCoveredMonth7_7.Name = "txtCoveredMonth7_7";
            this.txtCoveredMonth7_7.Text = null;
            this.txtCoveredMonth7_7.Top = 3.156056F;
            this.txtCoveredMonth7_7.Width = 0.25F;
            // 
            // txtCoveredMonth8_7
            // 
            this.txtCoveredMonth8_7.Height = 0.1770833F;
            this.txtCoveredMonth8_7.Left = 6.0625F;
            this.txtCoveredMonth8_7.Name = "txtCoveredMonth8_7";
            this.txtCoveredMonth8_7.Text = null;
            this.txtCoveredMonth8_7.Top = 3.156056F;
            this.txtCoveredMonth8_7.Width = 0.25F;
            // 
            // txtCoveredMonth9_7
            // 
            this.txtCoveredMonth9_7.Height = 0.1770833F;
            this.txtCoveredMonth9_7.Left = 6.357639F;
            this.txtCoveredMonth9_7.Name = "txtCoveredMonth9_7";
            this.txtCoveredMonth9_7.Text = null;
            this.txtCoveredMonth9_7.Top = 3.156056F;
            this.txtCoveredMonth9_7.Width = 0.25F;
            // 
            // txtCoveredMonth10_7
            // 
            this.txtCoveredMonth10_7.Height = 0.1770833F;
            this.txtCoveredMonth10_7.Left = 6.652778F;
            this.txtCoveredMonth10_7.Name = "txtCoveredMonth10_7";
            this.txtCoveredMonth10_7.Text = null;
            this.txtCoveredMonth10_7.Top = 3.156056F;
            this.txtCoveredMonth10_7.Width = 0.25F;
            // 
            // txtCoveredMonth11_7
            // 
            this.txtCoveredMonth11_7.Height = 0.1770833F;
            this.txtCoveredMonth11_7.Left = 6.947917F;
            this.txtCoveredMonth11_7.Name = "txtCoveredMonth11_7";
            this.txtCoveredMonth11_7.Text = null;
            this.txtCoveredMonth11_7.Top = 3.156056F;
            this.txtCoveredMonth11_7.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_7
            // 
            this.txtCoveredMonth12_7.Height = 0.1770833F;
            this.txtCoveredMonth12_7.Left = 7.243055F;
            this.txtCoveredMonth12_7.Name = "txtCoveredMonth12_7";
            this.txtCoveredMonth12_7.Text = null;
            this.txtCoveredMonth12_7.Top = 3.156056F;
            this.txtCoveredMonth12_7.Width = 0.1875F;
            // 
            // txtCoveredAll12_8
            // 
            this.txtCoveredAll12_8.Height = 0.1770833F;
            this.txtCoveredAll12_8.Left = 3.609028F;
            this.txtCoveredAll12_8.Name = "txtCoveredAll12_8";
            this.txtCoveredAll12_8.Text = null;
            this.txtCoveredAll12_8.Top = 3.531056F;
            this.txtCoveredAll12_8.Width = 0.25F;
            // 
            // txtCoveredMonth1_8
            // 
            this.txtCoveredMonth1_8.Height = 0.1770833F;
            this.txtCoveredMonth1_8.Left = 3.996528F;
            this.txtCoveredMonth1_8.Name = "txtCoveredMonth1_8";
            this.txtCoveredMonth1_8.Text = null;
            this.txtCoveredMonth1_8.Top = 3.531056F;
            this.txtCoveredMonth1_8.Width = 0.25F;
            // 
            // txtCoveredMonth2_8
            // 
            this.txtCoveredMonth2_8.Height = 0.1770833F;
            this.txtCoveredMonth2_8.Left = 4.291667F;
            this.txtCoveredMonth2_8.Name = "txtCoveredMonth2_8";
            this.txtCoveredMonth2_8.Text = null;
            this.txtCoveredMonth2_8.Top = 3.531056F;
            this.txtCoveredMonth2_8.Width = 0.25F;
            // 
            // txtCoveredMonth3_8
            // 
            this.txtCoveredMonth3_8.Height = 0.1770833F;
            this.txtCoveredMonth3_8.Left = 4.586805F;
            this.txtCoveredMonth3_8.Name = "txtCoveredMonth3_8";
            this.txtCoveredMonth3_8.Text = null;
            this.txtCoveredMonth3_8.Top = 3.531056F;
            this.txtCoveredMonth3_8.Width = 0.25F;
            // 
            // txtCoveredMonth4_8
            // 
            this.txtCoveredMonth4_8.Height = 0.1770833F;
            this.txtCoveredMonth4_8.Left = 4.881945F;
            this.txtCoveredMonth4_8.Name = "txtCoveredMonth4_8";
            this.txtCoveredMonth4_8.Text = null;
            this.txtCoveredMonth4_8.Top = 3.531056F;
            this.txtCoveredMonth4_8.Width = 0.25F;
            // 
            // txtCoveredMonth5_8
            // 
            this.txtCoveredMonth5_8.Height = 0.1770833F;
            this.txtCoveredMonth5_8.Left = 5.177083F;
            this.txtCoveredMonth5_8.Name = "txtCoveredMonth5_8";
            this.txtCoveredMonth5_8.Text = null;
            this.txtCoveredMonth5_8.Top = 3.531056F;
            this.txtCoveredMonth5_8.Width = 0.25F;
            // 
            // txtCoveredMonth6_8
            // 
            this.txtCoveredMonth6_8.Height = 0.1770833F;
            this.txtCoveredMonth6_8.Left = 5.472222F;
            this.txtCoveredMonth6_8.Name = "txtCoveredMonth6_8";
            this.txtCoveredMonth6_8.Text = null;
            this.txtCoveredMonth6_8.Top = 3.531056F;
            this.txtCoveredMonth6_8.Width = 0.25F;
            // 
            // txtCoveredMonth7_8
            // 
            this.txtCoveredMonth7_8.Height = 0.1770833F;
            this.txtCoveredMonth7_8.Left = 5.767361F;
            this.txtCoveredMonth7_8.Name = "txtCoveredMonth7_8";
            this.txtCoveredMonth7_8.Text = null;
            this.txtCoveredMonth7_8.Top = 3.531056F;
            this.txtCoveredMonth7_8.Width = 0.25F;
            // 
            // txtCoveredMonth8_8
            // 
            this.txtCoveredMonth8_8.Height = 0.1770833F;
            this.txtCoveredMonth8_8.Left = 6.0625F;
            this.txtCoveredMonth8_8.Name = "txtCoveredMonth8_8";
            this.txtCoveredMonth8_8.Text = null;
            this.txtCoveredMonth8_8.Top = 3.531056F;
            this.txtCoveredMonth8_8.Width = 0.25F;
            // 
            // txtCoveredMonth9_8
            // 
            this.txtCoveredMonth9_8.Height = 0.1770833F;
            this.txtCoveredMonth9_8.Left = 6.357639F;
            this.txtCoveredMonth9_8.Name = "txtCoveredMonth9_8";
            this.txtCoveredMonth9_8.Text = null;
            this.txtCoveredMonth9_8.Top = 3.531056F;
            this.txtCoveredMonth9_8.Width = 0.25F;
            // 
            // txtCoveredMonth10_8
            // 
            this.txtCoveredMonth10_8.Height = 0.1770833F;
            this.txtCoveredMonth10_8.Left = 6.652778F;
            this.txtCoveredMonth10_8.Name = "txtCoveredMonth10_8";
            this.txtCoveredMonth10_8.Text = null;
            this.txtCoveredMonth10_8.Top = 3.531056F;
            this.txtCoveredMonth10_8.Width = 0.25F;
            // 
            // txtCoveredMonth11_8
            // 
            this.txtCoveredMonth11_8.Height = 0.1770833F;
            this.txtCoveredMonth11_8.Left = 6.947917F;
            this.txtCoveredMonth11_8.Name = "txtCoveredMonth11_8";
            this.txtCoveredMonth11_8.Text = null;
            this.txtCoveredMonth11_8.Top = 3.531056F;
            this.txtCoveredMonth11_8.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_8
            // 
            this.txtCoveredMonth12_8.Height = 0.1770833F;
            this.txtCoveredMonth12_8.Left = 7.243055F;
            this.txtCoveredMonth12_8.Name = "txtCoveredMonth12_8";
            this.txtCoveredMonth12_8.Text = null;
            this.txtCoveredMonth12_8.Top = 3.531056F;
            this.txtCoveredMonth12_8.Width = 0.1875F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.09375F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.572917F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 5.5pt; text-align: center";
            this.Label14.Text = "Oct";
            this.Label14.Top = 0.7255004F;
            this.Label14.Width = 0.2951389F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.09375F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 6.277778F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 5.5pt; text-align: center";
            this.Label13.Text = "Sep";
            this.Label13.Top = 0.7255004F;
            this.Label13.Width = 0.2951389F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.09375F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 5.097222F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 5.5pt; text-align: center";
            this.Label9.Text = "May";
            this.Label9.Top = 0.7255004F;
            this.Label9.Width = 0.2951389F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.09408295F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.958F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 5.5pt; text-align: center; vertical-align: middle";
            this.Label4.Text = "(e) Months of Coverage";
            this.Label4.Top = 0.526F;
            this.Label4.Width = 3.458333F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 0F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.8296671F;
            this.Line2.Width = 7.46875F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.46875F;
            this.Line2.Y1 = 0.8296671F;
            this.Line2.Y2 = 0.8296671F;
            // 
            // Line9
            // 
            this.Line9.Height = 3.281083F;
            this.Line9.Left = 3.4375F;
            this.Line9.LineWeight = 0F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0.4859171F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 3.4375F;
            this.Line9.X2 = 3.4375F;
            this.Line9.Y1 = 0.4859171F;
            this.Line9.Y2 = 3.767F;
            // 
            // Line14
            // 
            this.Line14.Height = 3.083166F;
            this.Line14.Left = 5.097222F;
            this.Line14.LineWeight = 0F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0.6838338F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 5.097222F;
            this.Line14.X2 = 5.097222F;
            this.Line14.Y1 = 0.6838338F;
            this.Line14.Y2 = 3.767F;
            // 
            // Line18
            // 
            this.Line18.Height = 3.083166F;
            this.Line18.Left = 6.277778F;
            this.Line18.LineWeight = 0F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0.6838338F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 6.277778F;
            this.Line18.X2 = 6.277778F;
            this.Line18.Y1 = 0.6838338F;
            this.Line18.Y2 = 3.767F;
            // 
            // Line19
            // 
            this.Line19.Height = 3.083166F;
            this.Line19.Left = 6.572917F;
            this.Line19.LineWeight = 0F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0.6838338F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 6.572917F;
            this.Line19.X2 = 6.572917F;
            this.Line19.Y1 = 0.6838338F;
            this.Line19.Y2 = 3.767F;
            // 
            // Line22
            // 
            this.Line22.Height = 3.281083F;
            this.Line22.Left = 2.6875F;
            this.Line22.LineWeight = 0F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0.4859171F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 2.6875F;
            this.Line22.X2 = 2.6875F;
            this.Line22.Y1 = 0.4859171F;
            this.Line22.Y2 = 3.767F;
            // 
            // Line23
            // 
            this.Line23.Height = 3.281083F;
            this.Line23.Left = 1.75F;
            this.Line23.LineWeight = 0F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 0.4859171F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 1.75F;
            this.Line23.X2 = 1.75F;
            this.Line23.Y1 = 0.4859171F;
            this.Line23.Y2 = 3.767F;
            // 
            // lbl12Months
            // 
            this.lbl12Months.Height = 0.28125F;
            this.lbl12Months.HyperLink = null;
            this.lbl12Months.Left = 3.451389F;
            this.lbl12Months.Name = "lbl12Months";
            this.lbl12Months.Style = "font-size: 5.5pt; text-align: center";
            this.lbl12Months.Text = "(d) Covered all 12 months";
            this.lbl12Months.Top = 0.5275838F;
            this.lbl12Months.Width = 0.4583333F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.28125F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.75F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 5.5pt; text-align: center";
            this.Label1.Text = "(c) DOB (If SSN or other TIN is not available)";
            this.Label1.Top = 0.5310559F;
            this.Label1.Width = 0.6458333F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.15625F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1.812F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 5.5pt; text-align: center";
            this.Label2.Text = "(b) SSN or other TIN";
            this.Label2.Top = 0.569F;
            this.Label2.Width = 0.8333333F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.21875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.14F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 5.5pt; text-align: center";
            this.Label3.Text = "(a) Name of covered individual(s)         First name, middle initial, last name";
            this.Label3.Top = 0.549F;
            this.Label3.Width = 1.469F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.09375F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.916667F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 5.5pt; text-align: center";
            this.Label5.Text = "Jan";
            this.Label5.Top = 0.7255004F;
            this.Label5.Width = 0.2951389F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.09375F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 4.211805F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 5.5pt; text-align: center";
            this.Label6.Text = "Feb";
            this.Label6.Top = 0.7255004F;
            this.Label6.Width = 0.2951389F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.09375F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.506945F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 5.5pt; text-align: center";
            this.Label7.Text = "Mar";
            this.Label7.Top = 0.7255004F;
            this.Label7.Width = 0.2951389F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.09375F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 4.802083F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 5.5pt; text-align: center";
            this.Label8.Text = "Apr";
            this.Label8.Top = 0.7255004F;
            this.Label8.Width = 0.2951389F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.09375F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 5.392361F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 5.5pt; text-align: center";
            this.Label10.Text = "Jun";
            this.Label10.Top = 0.7255004F;
            this.Label10.Width = 0.2951389F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.09375F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.6875F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 5.5pt; text-align: center";
            this.Label11.Text = "July";
            this.Label11.Top = 0.7255004F;
            this.Label11.Width = 0.2951389F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.09375F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 5.982639F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 5.5pt; text-align: center";
            this.Label12.Text = "Aug";
            this.Label12.Top = 0.7255004F;
            this.Label12.Width = 0.2951389F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.09375F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 6.868055F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 5.5pt; text-align: center";
            this.Label15.Text = "Nov";
            this.Label15.Top = 0.7255004F;
            this.Label15.Width = 0.2951389F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.09375F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 7.166667F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 5.5pt; text-align: center";
            this.Label16.Text = "Dec";
            this.Label16.Top = 0.7255004F;
            this.Label16.Width = 0.2951389F;
            // 
            // Shape77
            // 
            this.Shape77.Height = 0.1458333F;
            this.Shape77.Left = 3.609028F;
            this.Shape77.LineWeight = 0F;
            this.Shape77.Name = "Shape77";
            this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape77.Top = 0.9130001F;
            this.Shape77.Width = 0.1354167F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1145833F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0.02083334F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label26.Text = "24";
            this.Label26.Top = 0.9338341F;
            this.Label26.Width = 0.2083333F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1145833F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 0.02083334F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label27.Text = "25";
            this.Label27.Top = 1.291473F;
            this.Label27.Width = 0.2083333F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.1145833F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 0.02083334F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label28.Text = "26";
            this.Label28.Top = 1.656056F;
            this.Label28.Width = 0.2083333F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1145833F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 0.02083334F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label29.Text = "27";
            this.Label29.Top = 2.027584F;
            this.Label29.Width = 0.2083333F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.1145833F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 0.02083334F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label30.Text = "28";
            this.Label30.Top = 2.392167F;
            this.Label30.Width = 0.2083333F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1145833F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 0.02083334F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label31.Text = "29";
            this.Label31.Top = 2.81925F;
            this.Label31.Width = 0.2083333F;
            // 
            // Line20
            // 
            this.Line20.Height = 3.083166F;
            this.Line20.Left = 6.868055F;
            this.Line20.LineWeight = 0F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0.6838338F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 6.868055F;
            this.Line20.X2 = 6.868055F;
            this.Line20.Y1 = 0.6838338F;
            this.Line20.Y2 = 3.767F;
            // 
            // Line21
            // 
            this.Line21.Height = 3.083166F;
            this.Line21.Left = 7.163195F;
            this.Line21.LineWeight = 0F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 0.6838338F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 7.163195F;
            this.Line21.X2 = 7.163195F;
            this.Line21.Y1 = 0.6838338F;
            this.Line21.Y2 = 3.767F;
            // 
            // Line17
            // 
            this.Line17.Height = 3.083166F;
            this.Line17.Left = 5.982639F;
            this.Line17.LineWeight = 0F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0.6838338F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 5.982639F;
            this.Line17.X2 = 5.982639F;
            this.Line17.Y1 = 0.6838338F;
            this.Line17.Y2 = 3.767F;
            // 
            // Line16
            // 
            this.Line16.Height = 3.083166F;
            this.Line16.Left = 5.6875F;
            this.Line16.LineWeight = 0F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 0.6838338F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 5.6875F;
            this.Line16.X2 = 5.6875F;
            this.Line16.Y1 = 0.6838338F;
            this.Line16.Y2 = 3.767F;
            // 
            // Line15
            // 
            this.Line15.Height = 3.083166F;
            this.Line15.Left = 5.392361F;
            this.Line15.LineWeight = 0F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.6838338F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 5.392361F;
            this.Line15.X2 = 5.392361F;
            this.Line15.Y1 = 0.6838338F;
            this.Line15.Y2 = 3.767F;
            // 
            // Line13
            // 
            this.Line13.Height = 3.083166F;
            this.Line13.Left = 4.802083F;
            this.Line13.LineWeight = 0F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0.6838338F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 4.802083F;
            this.Line13.X2 = 4.802083F;
            this.Line13.Y1 = 0.6838338F;
            this.Line13.Y2 = 3.767F;
            // 
            // Line12
            // 
            this.Line12.Height = 3.083166F;
            this.Line12.Left = 4.506945F;
            this.Line12.LineWeight = 0F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0.6838338F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 4.506945F;
            this.Line12.X2 = 4.506945F;
            this.Line12.Y1 = 0.6838338F;
            this.Line12.Y2 = 3.767F;
            // 
            // Line11
            // 
            this.Line11.Height = 3.083166F;
            this.Line11.Left = 4.211805F;
            this.Line11.LineWeight = 0F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.6838338F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 4.211805F;
            this.Line11.X2 = 4.211805F;
            this.Line11.Y1 = 0.6838338F;
            this.Line11.Y2 = 3.767F;
            // 
            // Line10
            // 
            this.Line10.Height = 3.281083F;
            this.Line10.Left = 3.916667F;
            this.Line10.LineWeight = 0F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0.4859171F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 3.916667F;
            this.Line10.X2 = 3.916667F;
            this.Line10.Y1 = 0.4859171F;
            this.Line10.Y2 = 3.767F;
            // 
            // Line24
            // 
            this.Line24.Height = 0F;
            this.Line24.Left = 3.916667F;
            this.Line24.LineWeight = 0F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 0.6838338F;
            this.Line24.Width = 3.552083F;
            this.Line24.X1 = 3.916667F;
            this.Line24.X2 = 7.46875F;
            this.Line24.Y1 = 0.6838338F;
            this.Line24.Y2 = 0.6838338F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 0F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 1.142167F;
            this.Line3.Width = 7.46875F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.46875F;
            this.Line3.Y1 = 1.142167F;
            this.Line3.Y2 = 1.142167F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 0F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 1.517167F;
            this.Line4.Width = 7.46875F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.46875F;
            this.Line4.Y1 = 1.517167F;
            this.Line4.Y2 = 1.517167F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 0F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 1.892167F;
            this.Line5.Width = 7.46875F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 7.46875F;
            this.Line5.Y1 = 1.892167F;
            this.Line5.Y2 = 1.892167F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 0F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 2.267167F;
            this.Line6.Width = 7.46875F;
            this.Line6.X1 = 0F;
            this.Line6.X2 = 7.46875F;
            this.Line6.Y1 = 2.267167F;
            this.Line6.Y2 = 2.267167F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0F;
            this.Line7.LineWeight = 0F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 2.642167F;
            this.Line7.Width = 7.447917F;
            this.Line7.X1 = 0F;
            this.Line7.X2 = 7.447917F;
            this.Line7.Y1 = 2.642167F;
            this.Line7.Y2 = 2.642167F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 0F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 3.017167F;
            this.Line8.Width = 7.46875F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 7.46875F;
            this.Line8.Y1 = 3.017167F;
            this.Line8.Y2 = 3.017167F;
            // 
            // Line25
            // 
            this.Line25.Height = 0F;
            this.Line25.Left = 0F;
            this.Line25.LineWeight = 0F;
            this.Line25.Name = "Line25";
            this.Line25.Top = 3.392167F;
            this.Line25.Width = 7.46875F;
            this.Line25.X1 = 0F;
            this.Line25.X2 = 7.46875F;
            this.Line25.Y1 = 3.392167F;
            this.Line25.Y2 = 3.392167F;
            // 
            // Line26
            // 
            this.Line26.Height = 0F;
            this.Line26.Left = 0F;
            this.Line26.LineWeight = 0F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 3.767167F;
            this.Line26.Width = 7.46875F;
            this.Line26.X1 = 0F;
            this.Line26.X2 = 7.46875F;
            this.Line26.Y1 = 3.767167F;
            this.Line26.Y2 = 3.767167F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.09375F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.042F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-size: 6pt; font-weight: bold; text-align: left";
            this.Label17.Text = "RAA #1607";
            this.Label17.Top = 3.819F;
            this.Label17.Width = 0.8958333F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.09375F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 4.708666F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 6pt; text-align: left";
            this.Label18.Text = "41-0852411";
            this.Label18.Top = 3.819F;
            this.Label18.Width = 0.6458333F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.09375F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 5.896166F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-size: 6pt; text-align: left";
            this.Label19.Text = "1095CC";
            this.Label19.Top = 3.819F;
            this.Label19.Width = 0.3958333F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.09375F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 6.44825F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-size: 6pt; text-align: left";
            this.Label20.Text = "Form";
            this.Label20.Top = 3.819F;
            this.Label20.Width = 0.2708333F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.125F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 6.677001F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-size: 7pt; font-weight: bold; text-align: left; vertical-align: bottom";
            this.Label21.Text = "1095-C";
            this.Label21.Top = 3.807F;
            this.Label21.Width = 0.4583333F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.09375F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 7.014222F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-size: 6pt; text-align: left";
            this.Label22.Text = "(2020)";
            this.Label22.Top = 3.819F;
            this.Label22.Width = 0.3333333F;
            // 
            // Shape78
            // 
            this.Shape78.Height = 0.1458333F;
            this.Shape78.Left = 3.604167F;
            this.Shape78.LineWeight = 0F;
            this.Shape78.Name = "Shape78";
            this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape78.Top = 1.288F;
            this.Shape78.Width = 0.1354167F;
            // 
            // Shape80
            // 
            this.Shape80.Height = 0.1458333F;
            this.Shape80.Left = 3.604167F;
            this.Shape80.LineWeight = 0F;
            this.Shape80.Name = "Shape80";
            this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape80.Top = 1.663F;
            this.Shape80.Width = 0.1354167F;
            // 
            // Shape81
            // 
            this.Shape81.Height = 0.1458333F;
            this.Shape81.Left = 3.604167F;
            this.Shape81.LineWeight = 0F;
            this.Shape81.Name = "Shape81";
            this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape81.Top = 2.038F;
            this.Shape81.Width = 0.1354167F;
            // 
            // Shape82
            // 
            this.Shape82.Height = 0.1458333F;
            this.Shape82.Left = 3.604167F;
            this.Shape82.LineWeight = 0F;
            this.Shape82.Name = "Shape82";
            this.Shape82.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape82.Top = 2.413F;
            this.Shape82.Width = 0.1354167F;
            // 
            // Shape83
            // 
            this.Shape83.Height = 0.1458333F;
            this.Shape83.Left = 3.604167F;
            this.Shape83.LineWeight = 0F;
            this.Shape83.Name = "Shape83";
            this.Shape83.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape83.Top = 2.788F;
            this.Shape83.Width = 0.1354167F;
            // 
            // Shape84
            // 
            this.Shape84.Height = 0.1458333F;
            this.Shape84.Left = 3.604167F;
            this.Shape84.LineWeight = 0F;
            this.Shape84.Name = "Shape84";
            this.Shape84.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape84.Top = 3.163F;
            this.Shape84.Width = 0.1354167F;
            // 
            // Shape85
            // 
            this.Shape85.Height = 0.1458333F;
            this.Shape85.Left = 3.604167F;
            this.Shape85.LineWeight = 0F;
            this.Shape85.Name = "Shape85";
            this.Shape85.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape85.Top = 3.538F;
            this.Shape85.Width = 0.1354167F;
            // 
            // Shape90
            // 
            this.Shape90.Height = 0.1458333F;
            this.Shape90.Left = 4F;
            this.Shape90.LineWeight = 0F;
            this.Shape90.Name = "Shape90";
            this.Shape90.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape90.Top = 0.9130001F;
            this.Shape90.Width = 0.1354167F;
            // 
            // Shape91
            // 
            this.Shape91.Height = 0.1458333F;
            this.Shape91.Left = 4F;
            this.Shape91.LineWeight = 0F;
            this.Shape91.Name = "Shape91";
            this.Shape91.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape91.Top = 1.288F;
            this.Shape91.Width = 0.1354167F;
            // 
            // Shape92
            // 
            this.Shape92.Height = 0.1458333F;
            this.Shape92.Left = 4F;
            this.Shape92.LineWeight = 0F;
            this.Shape92.Name = "Shape92";
            this.Shape92.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape92.Top = 1.663F;
            this.Shape92.Width = 0.1354167F;
            // 
            // Shape93
            // 
            this.Shape93.Height = 0.1458333F;
            this.Shape93.Left = 4F;
            this.Shape93.LineWeight = 0F;
            this.Shape93.Name = "Shape93";
            this.Shape93.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape93.Top = 2.038F;
            this.Shape93.Width = 0.1354167F;
            // 
            // Shape94
            // 
            this.Shape94.Height = 0.1458333F;
            this.Shape94.Left = 4F;
            this.Shape94.LineWeight = 0F;
            this.Shape94.Name = "Shape94";
            this.Shape94.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape94.Top = 2.413F;
            this.Shape94.Width = 0.1354167F;
            // 
            // Shape95
            // 
            this.Shape95.Height = 0.1458333F;
            this.Shape95.Left = 4F;
            this.Shape95.LineWeight = 0F;
            this.Shape95.Name = "Shape95";
            this.Shape95.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape95.Top = 2.788F;
            this.Shape95.Width = 0.1354167F;
            // 
            // Shape96
            // 
            this.Shape96.Height = 0.1458333F;
            this.Shape96.Left = 4F;
            this.Shape96.LineWeight = 0F;
            this.Shape96.Name = "Shape96";
            this.Shape96.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape96.Top = 3.163F;
            this.Shape96.Width = 0.1354167F;
            // 
            // Shape97
            // 
            this.Shape97.Height = 0.1458333F;
            this.Shape97.Left = 4F;
            this.Shape97.LineWeight = 0F;
            this.Shape97.Name = "Shape97";
            this.Shape97.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape97.Top = 3.538F;
            this.Shape97.Width = 0.1354167F;
            // 
            // Shape102
            // 
            this.Shape102.Height = 0.1458333F;
            this.Shape102.Left = 4.291667F;
            this.Shape102.LineWeight = 0F;
            this.Shape102.Name = "Shape102";
            this.Shape102.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape102.Top = 0.9130001F;
            this.Shape102.Width = 0.1354167F;
            // 
            // Shape103
            // 
            this.Shape103.Height = 0.1458333F;
            this.Shape103.Left = 4.291667F;
            this.Shape103.LineWeight = 0F;
            this.Shape103.Name = "Shape103";
            this.Shape103.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape103.Top = 1.288F;
            this.Shape103.Width = 0.1354167F;
            // 
            // Shape104
            // 
            this.Shape104.Height = 0.1458333F;
            this.Shape104.Left = 4.291667F;
            this.Shape104.LineWeight = 0F;
            this.Shape104.Name = "Shape104";
            this.Shape104.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape104.Top = 1.663F;
            this.Shape104.Width = 0.1354167F;
            // 
            // Shape105
            // 
            this.Shape105.Height = 0.1458333F;
            this.Shape105.Left = 4.291667F;
            this.Shape105.LineWeight = 0F;
            this.Shape105.Name = "Shape105";
            this.Shape105.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape105.Top = 2.038F;
            this.Shape105.Width = 0.1354167F;
            // 
            // Shape106
            // 
            this.Shape106.Height = 0.1458333F;
            this.Shape106.Left = 4.291667F;
            this.Shape106.LineWeight = 0F;
            this.Shape106.Name = "Shape106";
            this.Shape106.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape106.Top = 2.413F;
            this.Shape106.Width = 0.1354167F;
            // 
            // Shape107
            // 
            this.Shape107.Height = 0.1458333F;
            this.Shape107.Left = 4.291667F;
            this.Shape107.LineWeight = 0F;
            this.Shape107.Name = "Shape107";
            this.Shape107.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape107.Top = 2.788F;
            this.Shape107.Width = 0.1354167F;
            // 
            // Shape108
            // 
            this.Shape108.Height = 0.1458333F;
            this.Shape108.Left = 4.291667F;
            this.Shape108.LineWeight = 0F;
            this.Shape108.Name = "Shape108";
            this.Shape108.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape108.Top = 3.163F;
            this.Shape108.Width = 0.1354167F;
            // 
            // Shape109
            // 
            this.Shape109.Height = 0.1458333F;
            this.Shape109.Left = 4.291667F;
            this.Shape109.LineWeight = 0F;
            this.Shape109.Name = "Shape109";
            this.Shape109.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape109.Top = 3.538F;
            this.Shape109.Width = 0.1354167F;
            // 
            // Shape114
            // 
            this.Shape114.Height = 0.1458333F;
            this.Shape114.Left = 4.583333F;
            this.Shape114.LineWeight = 0F;
            this.Shape114.Name = "Shape114";
            this.Shape114.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape114.Top = 0.9130001F;
            this.Shape114.Width = 0.1354167F;
            // 
            // Shape115
            // 
            this.Shape115.Height = 0.1458333F;
            this.Shape115.Left = 4.583333F;
            this.Shape115.LineWeight = 0F;
            this.Shape115.Name = "Shape115";
            this.Shape115.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape115.Top = 1.288F;
            this.Shape115.Width = 0.1354167F;
            // 
            // Shape116
            // 
            this.Shape116.Height = 0.1458333F;
            this.Shape116.Left = 4.583333F;
            this.Shape116.LineWeight = 0F;
            this.Shape116.Name = "Shape116";
            this.Shape116.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape116.Top = 1.663F;
            this.Shape116.Width = 0.1354167F;
            // 
            // Shape117
            // 
            this.Shape117.Height = 0.1458333F;
            this.Shape117.Left = 4.583333F;
            this.Shape117.LineWeight = 0F;
            this.Shape117.Name = "Shape117";
            this.Shape117.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape117.Top = 2.038F;
            this.Shape117.Width = 0.1354167F;
            // 
            // Shape118
            // 
            this.Shape118.Height = 0.1458333F;
            this.Shape118.Left = 4.583333F;
            this.Shape118.LineWeight = 0F;
            this.Shape118.Name = "Shape118";
            this.Shape118.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape118.Top = 2.413F;
            this.Shape118.Width = 0.1354167F;
            // 
            // Shape119
            // 
            this.Shape119.Height = 0.1458333F;
            this.Shape119.Left = 4.583333F;
            this.Shape119.LineWeight = 0F;
            this.Shape119.Name = "Shape119";
            this.Shape119.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape119.Top = 2.788F;
            this.Shape119.Width = 0.1354167F;
            // 
            // Shape120
            // 
            this.Shape120.Height = 0.1458333F;
            this.Shape120.Left = 4.583333F;
            this.Shape120.LineWeight = 0F;
            this.Shape120.Name = "Shape120";
            this.Shape120.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape120.Top = 3.163F;
            this.Shape120.Width = 0.1354167F;
            // 
            // Shape121
            // 
            this.Shape121.Height = 0.1458333F;
            this.Shape121.Left = 4.583333F;
            this.Shape121.LineWeight = 0F;
            this.Shape121.Name = "Shape121";
            this.Shape121.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape121.Top = 3.538F;
            this.Shape121.Width = 0.1354167F;
            // 
            // Shape126
            // 
            this.Shape126.Height = 0.1458333F;
            this.Shape126.Left = 4.885417F;
            this.Shape126.LineWeight = 0F;
            this.Shape126.Name = "Shape126";
            this.Shape126.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape126.Top = 0.9130001F;
            this.Shape126.Width = 0.1354167F;
            // 
            // Shape127
            // 
            this.Shape127.Height = 0.1458333F;
            this.Shape127.Left = 4.885417F;
            this.Shape127.LineWeight = 0F;
            this.Shape127.Name = "Shape127";
            this.Shape127.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape127.Top = 1.288F;
            this.Shape127.Width = 0.1354167F;
            // 
            // Shape128
            // 
            this.Shape128.Height = 0.1458333F;
            this.Shape128.Left = 4.885417F;
            this.Shape128.LineWeight = 0F;
            this.Shape128.Name = "Shape128";
            this.Shape128.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape128.Top = 1.663F;
            this.Shape128.Width = 0.1354167F;
            // 
            // Shape129
            // 
            this.Shape129.Height = 0.1458333F;
            this.Shape129.Left = 4.885417F;
            this.Shape129.LineWeight = 0F;
            this.Shape129.Name = "Shape129";
            this.Shape129.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape129.Top = 2.038F;
            this.Shape129.Width = 0.1354167F;
            // 
            // Shape130
            // 
            this.Shape130.Height = 0.1458333F;
            this.Shape130.Left = 4.885417F;
            this.Shape130.LineWeight = 0F;
            this.Shape130.Name = "Shape130";
            this.Shape130.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape130.Top = 2.413F;
            this.Shape130.Width = 0.1354167F;
            // 
            // Shape131
            // 
            this.Shape131.Height = 0.1458333F;
            this.Shape131.Left = 4.885417F;
            this.Shape131.LineWeight = 0F;
            this.Shape131.Name = "Shape131";
            this.Shape131.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape131.Top = 2.788F;
            this.Shape131.Width = 0.1354167F;
            // 
            // Shape132
            // 
            this.Shape132.Height = 0.1458333F;
            this.Shape132.Left = 4.885417F;
            this.Shape132.LineWeight = 0F;
            this.Shape132.Name = "Shape132";
            this.Shape132.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape132.Top = 3.163F;
            this.Shape132.Width = 0.1354167F;
            // 
            // Shape133
            // 
            this.Shape133.Height = 0.1458333F;
            this.Shape133.Left = 4.885417F;
            this.Shape133.LineWeight = 0F;
            this.Shape133.Name = "Shape133";
            this.Shape133.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape133.Top = 3.538F;
            this.Shape133.Width = 0.1354167F;
            // 
            // Shape138
            // 
            this.Shape138.Height = 0.1458333F;
            this.Shape138.Left = 5.177083F;
            this.Shape138.LineWeight = 0F;
            this.Shape138.Name = "Shape138";
            this.Shape138.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape138.Top = 0.9130001F;
            this.Shape138.Width = 0.1354167F;
            // 
            // Shape139
            // 
            this.Shape139.Height = 0.1458333F;
            this.Shape139.Left = 5.177083F;
            this.Shape139.LineWeight = 0F;
            this.Shape139.Name = "Shape139";
            this.Shape139.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape139.Top = 1.288F;
            this.Shape139.Width = 0.1354167F;
            // 
            // Shape140
            // 
            this.Shape140.Height = 0.1458333F;
            this.Shape140.Left = 5.177083F;
            this.Shape140.LineWeight = 0F;
            this.Shape140.Name = "Shape140";
            this.Shape140.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape140.Top = 1.663F;
            this.Shape140.Width = 0.1354167F;
            // 
            // Shape141
            // 
            this.Shape141.Height = 0.1458333F;
            this.Shape141.Left = 5.177083F;
            this.Shape141.LineWeight = 0F;
            this.Shape141.Name = "Shape141";
            this.Shape141.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape141.Top = 2.038F;
            this.Shape141.Width = 0.1354167F;
            // 
            // Shape142
            // 
            this.Shape142.Height = 0.1458333F;
            this.Shape142.Left = 5.177083F;
            this.Shape142.LineWeight = 0F;
            this.Shape142.Name = "Shape142";
            this.Shape142.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape142.Top = 2.413F;
            this.Shape142.Width = 0.1354167F;
            // 
            // Shape143
            // 
            this.Shape143.Height = 0.1458333F;
            this.Shape143.Left = 5.177083F;
            this.Shape143.LineWeight = 0F;
            this.Shape143.Name = "Shape143";
            this.Shape143.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape143.Top = 2.788F;
            this.Shape143.Width = 0.1354167F;
            // 
            // Shape144
            // 
            this.Shape144.Height = 0.1458333F;
            this.Shape144.Left = 5.177083F;
            this.Shape144.LineWeight = 0F;
            this.Shape144.Name = "Shape144";
            this.Shape144.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape144.Top = 3.163F;
            this.Shape144.Width = 0.1354167F;
            // 
            // Shape145
            // 
            this.Shape145.Height = 0.1458333F;
            this.Shape145.Left = 5.177083F;
            this.Shape145.LineWeight = 0F;
            this.Shape145.Name = "Shape145";
            this.Shape145.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape145.Top = 3.538F;
            this.Shape145.Width = 0.1354167F;
            // 
            // Shape150
            // 
            this.Shape150.Height = 0.1458333F;
            this.Shape150.Left = 5.46875F;
            this.Shape150.LineWeight = 0F;
            this.Shape150.Name = "Shape150";
            this.Shape150.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape150.Top = 0.9130001F;
            this.Shape150.Width = 0.1354167F;
            // 
            // Shape151
            // 
            this.Shape151.Height = 0.1458333F;
            this.Shape151.Left = 5.46875F;
            this.Shape151.LineWeight = 0F;
            this.Shape151.Name = "Shape151";
            this.Shape151.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape151.Top = 1.288F;
            this.Shape151.Width = 0.1354167F;
            // 
            // Shape152
            // 
            this.Shape152.Height = 0.1458333F;
            this.Shape152.Left = 5.46875F;
            this.Shape152.LineWeight = 0F;
            this.Shape152.Name = "Shape152";
            this.Shape152.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape152.Top = 1.663F;
            this.Shape152.Width = 0.1354167F;
            // 
            // Shape153
            // 
            this.Shape153.Height = 0.1458333F;
            this.Shape153.Left = 5.46875F;
            this.Shape153.LineWeight = 0F;
            this.Shape153.Name = "Shape153";
            this.Shape153.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape153.Top = 2.038F;
            this.Shape153.Width = 0.1354167F;
            // 
            // Shape154
            // 
            this.Shape154.Height = 0.1458333F;
            this.Shape154.Left = 5.46875F;
            this.Shape154.LineWeight = 0F;
            this.Shape154.Name = "Shape154";
            this.Shape154.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape154.Top = 2.413F;
            this.Shape154.Width = 0.1354167F;
            // 
            // Shape155
            // 
            this.Shape155.Height = 0.1458333F;
            this.Shape155.Left = 5.46875F;
            this.Shape155.LineWeight = 0F;
            this.Shape155.Name = "Shape155";
            this.Shape155.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape155.Top = 2.788F;
            this.Shape155.Width = 0.1354167F;
            // 
            // Shape156
            // 
            this.Shape156.Height = 0.1458333F;
            this.Shape156.Left = 5.46875F;
            this.Shape156.LineWeight = 0F;
            this.Shape156.Name = "Shape156";
            this.Shape156.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape156.Top = 3.163F;
            this.Shape156.Width = 0.1354167F;
            // 
            // Shape157
            // 
            this.Shape157.Height = 0.1458333F;
            this.Shape157.Left = 5.46875F;
            this.Shape157.LineWeight = 0F;
            this.Shape157.Name = "Shape157";
            this.Shape157.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape157.Top = 3.538F;
            this.Shape157.Width = 0.1354167F;
            // 
            // Shape162
            // 
            this.Shape162.Height = 0.1458333F;
            this.Shape162.Left = 5.770833F;
            this.Shape162.LineWeight = 0F;
            this.Shape162.Name = "Shape162";
            this.Shape162.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape162.Top = 0.9130001F;
            this.Shape162.Width = 0.1354167F;
            // 
            // Shape163
            // 
            this.Shape163.Height = 0.1458333F;
            this.Shape163.Left = 5.770833F;
            this.Shape163.LineWeight = 0F;
            this.Shape163.Name = "Shape163";
            this.Shape163.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape163.Top = 1.288F;
            this.Shape163.Width = 0.1354167F;
            // 
            // Shape164
            // 
            this.Shape164.Height = 0.1458333F;
            this.Shape164.Left = 5.770833F;
            this.Shape164.LineWeight = 0F;
            this.Shape164.Name = "Shape164";
            this.Shape164.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape164.Top = 1.663F;
            this.Shape164.Width = 0.1354167F;
            // 
            // Shape165
            // 
            this.Shape165.Height = 0.1458333F;
            this.Shape165.Left = 5.770833F;
            this.Shape165.LineWeight = 0F;
            this.Shape165.Name = "Shape165";
            this.Shape165.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape165.Top = 2.038F;
            this.Shape165.Width = 0.1354167F;
            // 
            // Shape166
            // 
            this.Shape166.Height = 0.1458333F;
            this.Shape166.Left = 5.770833F;
            this.Shape166.LineWeight = 0F;
            this.Shape166.Name = "Shape166";
            this.Shape166.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape166.Top = 2.413F;
            this.Shape166.Width = 0.1354167F;
            // 
            // Shape167
            // 
            this.Shape167.Height = 0.1458333F;
            this.Shape167.Left = 5.770833F;
            this.Shape167.LineWeight = 0F;
            this.Shape167.Name = "Shape167";
            this.Shape167.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape167.Top = 2.788F;
            this.Shape167.Width = 0.1354167F;
            // 
            // Shape168
            // 
            this.Shape168.Height = 0.1458333F;
            this.Shape168.Left = 5.770833F;
            this.Shape168.LineWeight = 0F;
            this.Shape168.Name = "Shape168";
            this.Shape168.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape168.Top = 3.163F;
            this.Shape168.Width = 0.1354167F;
            // 
            // Shape169
            // 
            this.Shape169.Height = 0.1458333F;
            this.Shape169.Left = 5.770833F;
            this.Shape169.LineWeight = 0F;
            this.Shape169.Name = "Shape169";
            this.Shape169.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape169.Top = 3.538F;
            this.Shape169.Width = 0.1354167F;
            // 
            // Shape174
            // 
            this.Shape174.Height = 0.1458333F;
            this.Shape174.Left = 6.0625F;
            this.Shape174.LineWeight = 0F;
            this.Shape174.Name = "Shape174";
            this.Shape174.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape174.Top = 0.9130001F;
            this.Shape174.Width = 0.1354167F;
            // 
            // Shape175
            // 
            this.Shape175.Height = 0.1458333F;
            this.Shape175.Left = 6.0625F;
            this.Shape175.LineWeight = 0F;
            this.Shape175.Name = "Shape175";
            this.Shape175.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape175.Top = 1.288F;
            this.Shape175.Width = 0.1354167F;
            // 
            // Shape176
            // 
            this.Shape176.Height = 0.1458333F;
            this.Shape176.Left = 6.0625F;
            this.Shape176.LineWeight = 0F;
            this.Shape176.Name = "Shape176";
            this.Shape176.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape176.Top = 1.663F;
            this.Shape176.Width = 0.1354167F;
            // 
            // Shape177
            // 
            this.Shape177.Height = 0.1458333F;
            this.Shape177.Left = 6.0625F;
            this.Shape177.LineWeight = 0F;
            this.Shape177.Name = "Shape177";
            this.Shape177.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape177.Top = 2.038F;
            this.Shape177.Width = 0.1354167F;
            // 
            // Shape178
            // 
            this.Shape178.Height = 0.1458333F;
            this.Shape178.Left = 6.0625F;
            this.Shape178.LineWeight = 0F;
            this.Shape178.Name = "Shape178";
            this.Shape178.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape178.Top = 2.413F;
            this.Shape178.Width = 0.1354167F;
            // 
            // Shape179
            // 
            this.Shape179.Height = 0.1458333F;
            this.Shape179.Left = 6.0625F;
            this.Shape179.LineWeight = 0F;
            this.Shape179.Name = "Shape179";
            this.Shape179.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape179.Top = 2.788F;
            this.Shape179.Width = 0.1354167F;
            // 
            // Shape180
            // 
            this.Shape180.Height = 0.1458333F;
            this.Shape180.Left = 6.0625F;
            this.Shape180.LineWeight = 0F;
            this.Shape180.Name = "Shape180";
            this.Shape180.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape180.Top = 3.163F;
            this.Shape180.Width = 0.1354167F;
            // 
            // Shape181
            // 
            this.Shape181.Height = 0.1458333F;
            this.Shape181.Left = 6.0625F;
            this.Shape181.LineWeight = 0F;
            this.Shape181.Name = "Shape181";
            this.Shape181.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape181.Top = 3.538F;
            this.Shape181.Width = 0.1354167F;
            // 
            // Shape186
            // 
            this.Shape186.Height = 0.1458333F;
            this.Shape186.Left = 6.354167F;
            this.Shape186.LineWeight = 0F;
            this.Shape186.Name = "Shape186";
            this.Shape186.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape186.Top = 0.9130001F;
            this.Shape186.Width = 0.1354167F;
            // 
            // Shape187
            // 
            this.Shape187.Height = 0.1458333F;
            this.Shape187.Left = 6.354167F;
            this.Shape187.LineWeight = 0F;
            this.Shape187.Name = "Shape187";
            this.Shape187.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape187.Top = 1.288F;
            this.Shape187.Width = 0.1354167F;
            // 
            // Shape188
            // 
            this.Shape188.Height = 0.1458333F;
            this.Shape188.Left = 6.354167F;
            this.Shape188.LineWeight = 0F;
            this.Shape188.Name = "Shape188";
            this.Shape188.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape188.Top = 1.663F;
            this.Shape188.Width = 0.1354167F;
            // 
            // Shape189
            // 
            this.Shape189.Height = 0.1458333F;
            this.Shape189.Left = 6.354167F;
            this.Shape189.LineWeight = 0F;
            this.Shape189.Name = "Shape189";
            this.Shape189.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape189.Top = 2.038F;
            this.Shape189.Width = 0.1354167F;
            // 
            // Shape190
            // 
            this.Shape190.Height = 0.1458333F;
            this.Shape190.Left = 6.354167F;
            this.Shape190.LineWeight = 0F;
            this.Shape190.Name = "Shape190";
            this.Shape190.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape190.Top = 2.413F;
            this.Shape190.Width = 0.1354167F;
            // 
            // Shape191
            // 
            this.Shape191.Height = 0.1458333F;
            this.Shape191.Left = 6.354167F;
            this.Shape191.LineWeight = 0F;
            this.Shape191.Name = "Shape191";
            this.Shape191.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape191.Top = 2.788F;
            this.Shape191.Width = 0.1354167F;
            // 
            // Shape192
            // 
            this.Shape192.Height = 0.1458333F;
            this.Shape192.Left = 6.354167F;
            this.Shape192.LineWeight = 0F;
            this.Shape192.Name = "Shape192";
            this.Shape192.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape192.Top = 3.163F;
            this.Shape192.Width = 0.1354167F;
            // 
            // Shape193
            // 
            this.Shape193.Height = 0.1458333F;
            this.Shape193.Left = 6.354167F;
            this.Shape193.LineWeight = 0F;
            this.Shape193.Name = "Shape193";
            this.Shape193.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape193.Top = 3.538F;
            this.Shape193.Width = 0.1354167F;
            // 
            // Shape198
            // 
            this.Shape198.Height = 0.1458333F;
            this.Shape198.Left = 6.65625F;
            this.Shape198.LineWeight = 0F;
            this.Shape198.Name = "Shape198";
            this.Shape198.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape198.Top = 0.9130001F;
            this.Shape198.Width = 0.1354167F;
            // 
            // Shape199
            // 
            this.Shape199.Height = 0.1458333F;
            this.Shape199.Left = 6.65625F;
            this.Shape199.LineWeight = 0F;
            this.Shape199.Name = "Shape199";
            this.Shape199.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape199.Top = 1.288F;
            this.Shape199.Width = 0.1354167F;
            // 
            // Shape200
            // 
            this.Shape200.Height = 0.1458333F;
            this.Shape200.Left = 6.65625F;
            this.Shape200.LineWeight = 0F;
            this.Shape200.Name = "Shape200";
            this.Shape200.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape200.Top = 1.663F;
            this.Shape200.Width = 0.1354167F;
            // 
            // Shape201
            // 
            this.Shape201.Height = 0.1458333F;
            this.Shape201.Left = 6.65625F;
            this.Shape201.LineWeight = 0F;
            this.Shape201.Name = "Shape201";
            this.Shape201.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape201.Top = 2.038F;
            this.Shape201.Width = 0.1354167F;
            // 
            // Shape202
            // 
            this.Shape202.Height = 0.1458333F;
            this.Shape202.Left = 6.65625F;
            this.Shape202.LineWeight = 0F;
            this.Shape202.Name = "Shape202";
            this.Shape202.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape202.Top = 2.413F;
            this.Shape202.Width = 0.1354167F;
            // 
            // Shape203
            // 
            this.Shape203.Height = 0.1458333F;
            this.Shape203.Left = 6.65625F;
            this.Shape203.LineWeight = 0F;
            this.Shape203.Name = "Shape203";
            this.Shape203.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape203.Top = 2.788F;
            this.Shape203.Width = 0.1354167F;
            // 
            // Shape204
            // 
            this.Shape204.Height = 0.1458333F;
            this.Shape204.Left = 6.65625F;
            this.Shape204.LineWeight = 0F;
            this.Shape204.Name = "Shape204";
            this.Shape204.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape204.Top = 3.163F;
            this.Shape204.Width = 0.1354167F;
            // 
            // Shape205
            // 
            this.Shape205.Height = 0.1458333F;
            this.Shape205.Left = 6.65625F;
            this.Shape205.LineWeight = 0F;
            this.Shape205.Name = "Shape205";
            this.Shape205.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape205.Top = 3.538F;
            this.Shape205.Width = 0.1354167F;
            // 
            // Shape210
            // 
            this.Shape210.Height = 0.1458333F;
            this.Shape210.Left = 6.947917F;
            this.Shape210.LineWeight = 0F;
            this.Shape210.Name = "Shape210";
            this.Shape210.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape210.Top = 0.9130001F;
            this.Shape210.Width = 0.1354167F;
            // 
            // Shape211
            // 
            this.Shape211.Height = 0.1458333F;
            this.Shape211.Left = 6.947917F;
            this.Shape211.LineWeight = 0F;
            this.Shape211.Name = "Shape211";
            this.Shape211.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape211.Top = 1.288F;
            this.Shape211.Width = 0.1354167F;
            // 
            // Shape212
            // 
            this.Shape212.Height = 0.1458333F;
            this.Shape212.Left = 6.947917F;
            this.Shape212.LineWeight = 0F;
            this.Shape212.Name = "Shape212";
            this.Shape212.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape212.Top = 1.663F;
            this.Shape212.Width = 0.1354167F;
            // 
            // Shape213
            // 
            this.Shape213.Height = 0.1458333F;
            this.Shape213.Left = 6.947917F;
            this.Shape213.LineWeight = 0F;
            this.Shape213.Name = "Shape213";
            this.Shape213.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape213.Top = 2.038F;
            this.Shape213.Width = 0.1354167F;
            // 
            // Shape214
            // 
            this.Shape214.Height = 0.1458333F;
            this.Shape214.Left = 6.947917F;
            this.Shape214.LineWeight = 0F;
            this.Shape214.Name = "Shape214";
            this.Shape214.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape214.Top = 2.413F;
            this.Shape214.Width = 0.1354167F;
            // 
            // Shape215
            // 
            this.Shape215.Height = 0.1458333F;
            this.Shape215.Left = 6.947917F;
            this.Shape215.LineWeight = 0F;
            this.Shape215.Name = "Shape215";
            this.Shape215.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape215.Top = 2.788F;
            this.Shape215.Width = 0.1354167F;
            // 
            // Shape216
            // 
            this.Shape216.Height = 0.1458333F;
            this.Shape216.Left = 6.947917F;
            this.Shape216.LineWeight = 0F;
            this.Shape216.Name = "Shape216";
            this.Shape216.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape216.Top = 3.163F;
            this.Shape216.Width = 0.1354167F;
            // 
            // Shape217
            // 
            this.Shape217.Height = 0.1458333F;
            this.Shape217.Left = 6.947917F;
            this.Shape217.LineWeight = 0F;
            this.Shape217.Name = "Shape217";
            this.Shape217.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape217.Top = 3.538F;
            this.Shape217.Width = 0.1354167F;
            // 
            // Shape222
            // 
            this.Shape222.Height = 0.1458333F;
            this.Shape222.Left = 7.239583F;
            this.Shape222.LineWeight = 0F;
            this.Shape222.Name = "Shape222";
            this.Shape222.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape222.Top = 1.288F;
            this.Shape222.Width = 0.1354167F;
            // 
            // Shape223
            // 
            this.Shape223.Height = 0.1458333F;
            this.Shape223.Left = 7.239583F;
            this.Shape223.LineWeight = 0F;
            this.Shape223.Name = "Shape223";
            this.Shape223.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape223.Top = 0.9130001F;
            this.Shape223.Width = 0.1354167F;
            // 
            // Shape224
            // 
            this.Shape224.Height = 0.1458333F;
            this.Shape224.Left = 7.239583F;
            this.Shape224.LineWeight = 0F;
            this.Shape224.Name = "Shape224";
            this.Shape224.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape224.Top = 1.663F;
            this.Shape224.Width = 0.1354167F;
            // 
            // Shape225
            // 
            this.Shape225.Height = 0.1458333F;
            this.Shape225.Left = 7.239583F;
            this.Shape225.LineWeight = 0F;
            this.Shape225.Name = "Shape225";
            this.Shape225.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape225.Top = 2.038F;
            this.Shape225.Width = 0.1354167F;
            // 
            // Shape226
            // 
            this.Shape226.Height = 0.1458333F;
            this.Shape226.Left = 7.239583F;
            this.Shape226.LineWeight = 0F;
            this.Shape226.Name = "Shape226";
            this.Shape226.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape226.Top = 2.413F;
            this.Shape226.Width = 0.1354167F;
            // 
            // Shape227
            // 
            this.Shape227.Height = 0.1458333F;
            this.Shape227.Left = 7.239583F;
            this.Shape227.LineWeight = 0F;
            this.Shape227.Name = "Shape227";
            this.Shape227.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape227.Top = 2.788F;
            this.Shape227.Width = 0.1354167F;
            // 
            // Shape228
            // 
            this.Shape228.Height = 0.1458333F;
            this.Shape228.Left = 7.239583F;
            this.Shape228.LineWeight = 0F;
            this.Shape228.Name = "Shape228";
            this.Shape228.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape228.Top = 3.163F;
            this.Shape228.Width = 0.1354167F;
            // 
            // Shape229
            // 
            this.Shape229.Height = 0.1458333F;
            this.Shape229.Left = 7.239583F;
            this.Shape229.LineWeight = 0F;
            this.Shape229.Name = "Shape229";
            this.Shape229.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape229.Top = 3.538F;
            this.Shape229.Width = 0.1354167F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1145833F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 0.448F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label25.Text = "Covered Individuals -";
            this.Label25.Top = 0.251F;
            this.Label25.Width = 1.020833F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.125F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 8.332729E-05F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label24.Text = "Part III";
            this.Label24.Top = 0.251F;
            this.Label24.Width = 0.34375F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.14F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 0.448F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-family: \'Arial\'; font-size: 6pt; text-align: left";
            this.Label32.Text = "If Employer provided self-insured coverage, check the box and enter the informati" +
    "on for each individual for each individual enrolled in coverage, including the e" +
    "mployee";
            this.Label32.Top = 0.381F;
            this.Label32.Width = 6.205F;
            // 
            // Line31
            // 
            this.Line31.Height = 0F;
            this.Line31.Left = 8.332729E-05F;
            this.Line31.LineWeight = 1F;
            this.Line31.Name = "Line31";
            this.Line31.Top = 0.251F;
            this.Line31.Width = 7.46875F;
            this.Line31.X1 = 8.332729E-05F;
            this.Line31.X2 = 7.468833F;
            this.Line31.Y1 = 0.251F;
            this.Line31.Y2 = 0.251F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.09375F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 0F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-size: 6pt; text-align: left";
            this.Label33.Text = "Form 1095-C (2020)";
            this.Label33.Top = 0.125F;
            this.Label33.Width = 0.8333333F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.09375F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 7.0625F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-size: 6pt; text-align: left";
            this.Label34.Text = "Page";
            this.Label34.Top = 0.125F;
            this.Label34.Width = 0.2708333F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.125F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 7.3125F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-size: 7pt; font-weight: bold; text-align: right; vertical-align: bottom";
            this.Label35.Text = "3";
            this.Label35.Top = 0.09375F;
            this.Label35.Width = 0.1458333F;
            // 
            // Label93
            // 
            this.Label93.Height = 0.1354167F;
            this.Label93.HyperLink = null;
            this.Label93.Left = 6.895833F;
            this.Label93.Name = "Label93";
            this.Label93.Style = "font-family: \'OCR A Extended\'; font-size: 8.5pt; text-align: right";
            this.Label93.Text = "600320";
            this.Label93.Top = 0F;
            this.Label93.Width = 0.5625F;
            // 
            // Label94
            // 
            this.Label94.Height = 0.1145833F;
            this.Label94.HyperLink = null;
            this.Label94.Left = 0.02083334F;
            this.Label94.Name = "Label94";
            this.Label94.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label94.Text = "30";
            this.Label94.Top = 3.183834F;
            this.Label94.Width = 0.2083333F;
            // 
            // Label95
            // 
            this.Label95.Height = 0.1145833F;
            this.Label95.HyperLink = null;
            this.Label95.Left = 0.02083334F;
            this.Label95.Name = "Label95";
            this.Label95.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label95.Text = "30";
            this.Label95.Top = 3.558834F;
            this.Label95.Width = 0.2083333F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 0F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.4859171F;
            this.Line1.Width = 7.46875F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.46875F;
            this.Line1.Y1 = 0.4859171F;
            this.Line1.Y2 = 0.4859171F;
            // 
            // Line34
            // 
            this.Line34.Height = 2.937333F;
            this.Line34.Left = 0.8125F;
            this.Line34.LineWeight = 0F;
            this.Line34.Name = "Line34";
            this.Line34.Top = 0.8296671F;
            this.Line34.Width = 0F;
            this.Line34.X1 = 0.8125F;
            this.Line34.X2 = 0.8125F;
            this.Line34.Y1 = 0.8296671F;
            this.Line34.Y2 = 3.767F;
            // 
            // Line35
            // 
            this.Line35.Height = 2.937333F;
            this.Line35.Left = 1.104167F;
            this.Line35.LineWeight = 0F;
            this.Line35.Name = "Line35";
            this.Line35.Top = 0.8296671F;
            this.Line35.Width = 0F;
            this.Line35.X1 = 1.104167F;
            this.Line35.X2 = 1.104167F;
            this.Line35.Y1 = 0.8296671F;
            this.Line35.Y2 = 3.767F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.CanGrow = false;
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 0.875F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 0.9130001F;
            this.txtCoveredMiddle1.Width = 0.1875F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.CanGrow = false;
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 0.875F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 1.277584F;
            this.txtCoveredMiddle2.Width = 0.1875F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.CanGrow = false;
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 0.875F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 1.652584F;
            this.txtCoveredMiddle3.Width = 0.1875F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.CanGrow = false;
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 0.875F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 2.027584F;
            this.txtCoveredMiddle4.Width = 0.1875F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.CanGrow = false;
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 0.875F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 2.402584F;
            this.txtCoveredMiddle5.Width = 0.1875F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.CanGrow = false;
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 0.875F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 2.777584F;
            this.txtCoveredMiddle6.Width = 0.1875F;
            // 
            // txtCoveredMiddle7
            // 
            this.txtCoveredMiddle7.CanGrow = false;
            this.txtCoveredMiddle7.Height = 0.1770833F;
            this.txtCoveredMiddle7.Left = 0.875F;
            this.txtCoveredMiddle7.Name = "txtCoveredMiddle7";
            this.txtCoveredMiddle7.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle7.Text = null;
            this.txtCoveredMiddle7.Top = 3.152584F;
            this.txtCoveredMiddle7.Width = 0.1875F;
            // 
            // txtCoveredMiddle8
            // 
            this.txtCoveredMiddle8.CanGrow = false;
            this.txtCoveredMiddle8.Height = 0.1770833F;
            this.txtCoveredMiddle8.Left = 0.875F;
            this.txtCoveredMiddle8.Name = "txtCoveredMiddle8";
            this.txtCoveredMiddle8.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle8.Text = null;
            this.txtCoveredMiddle8.Top = 3.527584F;
            this.txtCoveredMiddle8.Width = 0.1875F;
            // 
            // txtCoveredLastName1
            // 
            this.txtCoveredLastName1.CanGrow = false;
            this.txtCoveredLastName1.Height = 0.1770833F;
            this.txtCoveredLastName1.Left = 1.125F;
            this.txtCoveredLastName1.Name = "txtCoveredLastName1";
            this.txtCoveredLastName1.Style = "font-size: 8.5pt";
            this.txtCoveredLastName1.Text = null;
            this.txtCoveredLastName1.Top = 0.9130001F;
            this.txtCoveredLastName1.Width = 0.625F;
            // 
            // txtCoveredLastName2
            // 
            this.txtCoveredLastName2.CanGrow = false;
            this.txtCoveredLastName2.Height = 0.1770833F;
            this.txtCoveredLastName2.Left = 1.125F;
            this.txtCoveredLastName2.Name = "txtCoveredLastName2";
            this.txtCoveredLastName2.Style = "font-size: 8.5pt";
            this.txtCoveredLastName2.Text = null;
            this.txtCoveredLastName2.Top = 1.277584F;
            this.txtCoveredLastName2.Width = 0.625F;
            // 
            // txtCoveredLastName3
            // 
            this.txtCoveredLastName3.CanGrow = false;
            this.txtCoveredLastName3.Height = 0.1770833F;
            this.txtCoveredLastName3.Left = 1.125F;
            this.txtCoveredLastName3.Name = "txtCoveredLastName3";
            this.txtCoveredLastName3.Style = "font-size: 8.5pt";
            this.txtCoveredLastName3.Text = null;
            this.txtCoveredLastName3.Top = 1.652584F;
            this.txtCoveredLastName3.Width = 0.625F;
            // 
            // txtCoveredLastName4
            // 
            this.txtCoveredLastName4.CanGrow = false;
            this.txtCoveredLastName4.Height = 0.1770833F;
            this.txtCoveredLastName4.Left = 1.125F;
            this.txtCoveredLastName4.Name = "txtCoveredLastName4";
            this.txtCoveredLastName4.Style = "font-size: 8.5pt";
            this.txtCoveredLastName4.Text = null;
            this.txtCoveredLastName4.Top = 2.027584F;
            this.txtCoveredLastName4.Width = 0.625F;
            // 
            // txtCoveredLastName5
            // 
            this.txtCoveredLastName5.CanGrow = false;
            this.txtCoveredLastName5.Height = 0.1770833F;
            this.txtCoveredLastName5.Left = 1.125F;
            this.txtCoveredLastName5.Name = "txtCoveredLastName5";
            this.txtCoveredLastName5.Style = "font-size: 8.5pt";
            this.txtCoveredLastName5.Text = null;
            this.txtCoveredLastName5.Top = 2.402584F;
            this.txtCoveredLastName5.Width = 0.625F;
            // 
            // txtCoveredLastName6
            // 
            this.txtCoveredLastName6.CanGrow = false;
            this.txtCoveredLastName6.Height = 0.1770833F;
            this.txtCoveredLastName6.Left = 1.125F;
            this.txtCoveredLastName6.Name = "txtCoveredLastName6";
            this.txtCoveredLastName6.Style = "font-size: 8.5pt";
            this.txtCoveredLastName6.Text = null;
            this.txtCoveredLastName6.Top = 2.777584F;
            this.txtCoveredLastName6.Width = 0.625F;
            // 
            // txtCoveredLastName7
            // 
            this.txtCoveredLastName7.CanGrow = false;
            this.txtCoveredLastName7.Height = 0.1770833F;
            this.txtCoveredLastName7.Left = 1.125F;
            this.txtCoveredLastName7.Name = "txtCoveredLastName7";
            this.txtCoveredLastName7.Style = "font-size: 8.5pt";
            this.txtCoveredLastName7.Text = null;
            this.txtCoveredLastName7.Top = 3.152584F;
            this.txtCoveredLastName7.Width = 0.625F;
            // 
            // txtCoveredLastName8
            // 
            this.txtCoveredLastName8.CanGrow = false;
            this.txtCoveredLastName8.Height = 0.1770833F;
            this.txtCoveredLastName8.Left = 1.125F;
            this.txtCoveredLastName8.Name = "txtCoveredLastName8";
            this.txtCoveredLastName8.Style = "font-size: 8.5pt";
            this.txtCoveredLastName8.Text = null;
            this.txtCoveredLastName8.Top = 3.528F;
            this.txtCoveredLastName8.Width = 0.625F;
            // 
            // txtCoveredIndividuals
            // 
            this.txtCoveredIndividuals.Height = 0.1770833F;
            this.txtCoveredIndividuals.Left = 6.771F;
            this.txtCoveredIndividuals.Name = "txtCoveredIndividuals";
            this.txtCoveredIndividuals.Text = null;
            this.txtCoveredIndividuals.Top = 0.309F;
            this.txtCoveredIndividuals.Width = 0.1875F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.1458333F;
            this.shape1.Left = 6.767529F;
            this.shape1.LineWeight = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.shape1.Top = 0.309F;
            this.shape1.Width = 0.1354167F;
            // 
            // line27
            // 
            this.line27.Height = 2.937F;
            this.line27.Left = 0.16F;
            this.line27.LineWeight = 0F;
            this.line27.Name = "line27";
            this.line27.Top = 0.83F;
            this.line27.Width = 0F;
            this.line27.X1 = 0.16F;
            this.line27.X2 = 0.16F;
            this.line27.Y1 = 0.83F;
            this.line27.Y2 = 3.767F;
            // 
            // rpt1095C2020BlankPortraitPage2
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl12Months;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape82;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape83;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape84;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape85;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape90;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape91;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape92;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape93;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape94;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape95;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape96;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape97;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape102;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape103;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape104;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape105;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape106;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape107;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape108;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape109;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape114;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape115;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape116;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape117;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape118;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape119;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape120;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape121;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape126;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape127;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape128;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape129;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape130;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape131;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape132;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape133;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape138;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape139;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape140;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape141;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape142;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape143;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape144;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape145;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape150;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape151;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape152;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape153;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape154;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape155;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape156;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape157;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape162;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape163;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape164;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape165;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape166;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape167;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape168;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape169;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape174;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape175;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape176;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape177;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape178;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape179;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape180;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape181;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape186;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape187;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape188;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape189;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape190;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape191;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape192;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape193;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape198;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape199;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape200;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape201;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape202;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape203;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape204;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape205;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape210;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape211;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape212;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape213;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape214;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape215;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape216;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape217;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape222;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape223;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape224;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape225;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape226;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape227;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape228;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape229;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredIndividuals;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
    }
}
