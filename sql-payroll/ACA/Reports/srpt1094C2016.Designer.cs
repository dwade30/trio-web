﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt1094C2016.
	/// </summary>
	partial class srpt1094C2016
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srpt1094C2016));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCorrected = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContactName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContactTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalTransmitted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAuthoritative = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroupYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroupNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQualifyingOfferMethod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQualifyingTransitionRelief = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt98Percent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorrected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTransmitted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuthoritative)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroupYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroupNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifyingOfferMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifyingTransitionRelief)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt98Percent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCorrected,
            this.txtEmployer,
            this.txtEIN,
            this.txtAddress,
            this.txtCity,
            this.txtPostalCode,
            this.txtState,
            this.txtContactName,
            this.txtContactTelephone,
            this.txtTotalTransmitted,
            this.txtFiled,
            this.txtAuthoritative,
            this.txtInGroupYes,
            this.txtInGroupNo,
            this.txtQualifyingOfferMethod,
            this.txtQualifyingTransitionRelief,
            this.txt4980H,
            this.txt98Percent});
            this.Detail.Height = 6.447917F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtCorrected
            // 
            this.txtCorrected.Height = 0.1979167F;
            this.txtCorrected.Left = 7.362F;
            this.txtCorrected.Name = "txtCorrected";
            this.txtCorrected.Text = "X";
            this.txtCorrected.Top = 0.181F;
            this.txtCorrected.Width = 0.1875F;
            // 
            // txtEmployer
            // 
            this.txtEmployer.Height = 0.1666667F;
            this.txtEmployer.Left = 0.08338866F;
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Text = null;
            this.txtEmployer.Top = 1.149333F;
            this.txtEmployer.Width = 5.5F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1666667F;
            this.txtEIN.Left = 6.020888F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 1.149333F;
            this.txtEIN.Width = 1.583333F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1666667F;
            this.txtAddress.Left = 0.083F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 1.472F;
            this.txtAddress.Width = 7.125F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1666667F;
            this.txtCity.Left = 0.083F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Text = null;
            this.txtCity.Top = 1.763666F;
            this.txtCity.Width = 3.833333F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1666667F;
            this.txtPostalCode.Left = 6.0205F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 1.763666F;
            this.txtPostalCode.Width = 1.583333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1666667F;
            this.txtState.Left = 4.1455F;
            this.txtState.Name = "txtState";
            this.txtState.Text = null;
            this.txtState.Top = 1.763666F;
            this.txtState.Width = 1.645833F;
            // 
            // txtContactName
            // 
            this.txtContactName.Height = 0.1666667F;
            this.txtContactName.Left = 0.083F;
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Text = null;
            this.txtContactName.Top = 2.096999F;
            this.txtContactName.Width = 5.5F;
            // 
            // txtContactTelephone
            // 
            this.txtContactTelephone.Height = 0.1666667F;
            this.txtContactTelephone.Left = 6.0205F;
            this.txtContactTelephone.Name = "txtContactTelephone";
            this.txtContactTelephone.Text = null;
            this.txtContactTelephone.Top = 2.096999F;
            this.txtContactTelephone.Width = 1.583333F;
            // 
            // txtTotalTransmitted
            // 
            this.txtTotalTransmitted.Height = 0.1666667F;
            this.txtTotalTransmitted.Left = 8.666722F;
            this.txtTotalTransmitted.Name = "txtTotalTransmitted";
            this.txtTotalTransmitted.Style = "text-align: right";
            this.txtTotalTransmitted.Text = null;
            this.txtTotalTransmitted.Top = 4.024331F;
            this.txtTotalTransmitted.Width = 1F;
            // 
            // txtFiled
            // 
            this.txtFiled.Height = 0.1666667F;
            this.txtFiled.Left = 8.667001F;
            this.txtFiled.Name = "txtFiled";
            this.txtFiled.Style = "text-align: right";
            this.txtFiled.Text = null;
            this.txtFiled.Top = 4.847F;
            this.txtFiled.Width = 1F;
            // 
            // txtAuthoritative
            // 
            this.txtAuthoritative.Height = 0.1979167F;
            this.txtAuthoritative.Left = 9.772F;
            this.txtAuthoritative.Name = "txtAuthoritative";
            this.txtAuthoritative.Text = "X";
            this.txtAuthoritative.Top = 4.278F;
            this.txtAuthoritative.Width = 0.1875F;
            // 
            // txtInGroupYes
            // 
            this.txtInGroupYes.Height = 0.1979167F;
            this.txtInGroupYes.Left = 8.967F;
            this.txtInGroupYes.Name = "txtInGroupYes";
            this.txtInGroupYes.Text = "X";
            this.txtInGroupYes.Top = 5.171F;
            this.txtInGroupYes.Width = 0.1875F;
            // 
            // txtInGroupNo
            // 
            this.txtInGroupNo.Height = 0.1979167F;
            this.txtInGroupNo.Left = 9.647555F;
            this.txtInGroupNo.Name = "txtInGroupNo";
            this.txtInGroupNo.Text = "X";
            this.txtInGroupNo.Top = 5.171F;
            this.txtInGroupNo.Width = 0.1875F;
            // 
            // txtQualifyingOfferMethod
            // 
            this.txtQualifyingOfferMethod.Height = 0.1979167F;
            this.txtQualifyingOfferMethod.Left = 0.156F;
            this.txtQualifyingOfferMethod.Name = "txtQualifyingOfferMethod";
            this.txtQualifyingOfferMethod.Text = "X";
            this.txtQualifyingOfferMethod.Top = 6.004F;
            this.txtQualifyingOfferMethod.Width = 0.1875F;
            // 
            // txtQualifyingTransitionRelief
            // 
            this.txtQualifyingTransitionRelief.Height = 0.1979167F;
            this.txtQualifyingTransitionRelief.Left = 2.500055F;
            this.txtQualifyingTransitionRelief.Name = "txtQualifyingTransitionRelief";
            this.txtQualifyingTransitionRelief.Text = null;
            this.txtQualifyingTransitionRelief.Top = 6.004331F;
            this.txtQualifyingTransitionRelief.Visible = false;
            this.txtQualifyingTransitionRelief.Width = 0.1875F;
            // 
            // txt4980H
            // 
            this.txt4980H.Height = 0.1979167F;
            this.txt4980H.Left = 3.812555F;
            this.txt4980H.Name = "txt4980H";
            this.txt4980H.Text = null;
            this.txt4980H.Top = 6.004331F;
            this.txt4980H.Visible = false;
            this.txt4980H.Width = 0.1875F;
            // 
            // txt98Percent
            // 
            this.txt98Percent.Height = 0.1979167F;
            this.txt98Percent.Left = 7.135472F;
            this.txt98Percent.Name = "txt98Percent";
            this.txt98Percent.Text = "X";
            this.txt98Percent.Top = 6.004331F;
            this.txt98Percent.Width = 0.1875F;
            // 
            // srpt1094C2016
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 10F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtCorrected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTransmitted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuthoritative)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroupYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroupNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifyingOfferMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifyingTransitionRelief)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt98Percent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCorrected;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactTelephone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTransmitted;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFiled;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAuthoritative;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroupYes;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroupNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualifyingOfferMethod;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualifyingTransitionRelief;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt98Percent;
    }
}
