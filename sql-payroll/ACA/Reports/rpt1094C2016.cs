﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1094C2016.
	/// </summary>
	public partial class rpt1094C2016 : BaseSectionReport
	{
		public rpt1094C2016()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "1094-C";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt1094C2016 InstancePtr
		{
			get
			{
				return (rpt1094C2016)Sys.GetInstance(typeof(rpt1094C2016));
			}
		}

		protected rpt1094C2016 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1094C2016	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c1094C theReport;
		private bool boolTestPrint;
		private bool boolFirstPage;

		public c1094C ReportModel
		{
			get
			{
				c1094C ReportModel = null;
				ReportModel = theReport;
				return ReportModel;
			}
		}

		public bool TestPrint
		{
			get
			{
				bool TestPrint = false;
				TestPrint = boolTestPrint;
				return TestPrint;
			}
		}

		public void Init(c1094C theModel, bool boolPrintTest)
		{
			theReport = theModel;
			boolTestPrint = boolPrintTest;
			frmReportViewer.InstancePtr.Init(this, "", 0, false, boolOverRideDuplex: true);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !boolFirstPage;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolFirstPage = true;
			// If theReport.HorizontalAlignment <> 0 Or theReport.VerticalAlignment <> 0 Then
			// Dim ControlName As Object
			// For Each ControlName In Me.Detail.Controls
			// If theReport.VerticalAlignment <> 0 Then
			// ControlName.Top = ControlName.Top + (120 * theReport.VerticalAlignment)
			// End If
			// If theReport.HorizontalAlignment <> 0 Then
			// ControlName.Left = ControlName.Left + (120 * theReport.HorizontalAlignment)
			// End If
			// Next
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolFirstPage)
			{
				SubReport1.Report = new srpt1094C2016();
				SubReport2.Report = new srpt1094C2016MonthlyDetail();
				if (!boolTestPrint)
				{
					if (theReport.MemberOfAggregatedGroup)
					{
						SubReport3.Report = new srpt1094C2016GroupMembers();
					}
				}
				else
				{
					SubReport3.Report = new srpt1094C2016GroupMembers();
				}
			}
			boolFirstPage = false;
		}

		private void rpt1094C2016_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt1094C2016 properties;
			//rpt1094C2016.Caption	= "1094-C";
			//rpt1094C2016.Left	= 0;
			//rpt1094C2016.Top	= 0;
			//rpt1094C2016.Width	= 20235;
			//rpt1094C2016.Height	= 8445;
			//rpt1094C2016.StartUpPosition	= 3;
			//rpt1094C2016.SectionData	= "rpt1094C2016.dsx":0000;
			//End Unmaped Properties
		}
	}
}
