﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt1094C2016GroupMembers.
	/// </summary>
	partial class srpt1094C2016GroupMembers
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srpt1094C2016GroupMembers));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtName1,
            this.txtName16,
            this.txtEIN1,
            this.txtEIN16,
            this.txtName2,
            this.txtName17,
            this.txtEIN2,
            this.txtEIN17,
            this.txtName3,
            this.txtName18,
            this.txtEIN3,
            this.txtEIN18,
            this.txtName4,
            this.txtName19,
            this.txtEIN4,
            this.txtEIN19,
            this.txtName5,
            this.txtName20,
            this.txtEIN5,
            this.txtEIN20,
            this.txtName6,
            this.txtName21,
            this.txtEIN6,
            this.txtEIN21,
            this.txtName7,
            this.txtName22,
            this.txtEIN7,
            this.txtEIN22,
            this.txtName8,
            this.txtName23,
            this.txtEIN8,
            this.txtEIN23,
            this.txtName9,
            this.txtName24,
            this.txtEIN9,
            this.txtEIN24,
            this.txtName10,
            this.txtName25,
            this.txtEIN10,
            this.txtEIN25,
            this.txtName11,
            this.txtName26,
            this.txtEIN11,
            this.txtEIN26,
            this.txtName12,
            this.txtName27,
            this.txtEIN12,
            this.txtEIN27,
            this.txtName13,
            this.txtName28,
            this.txtEIN13,
            this.txtEIN28,
            this.txtName14,
            this.txtName29,
            this.txtEIN14,
            this.txtEIN29,
            this.txtName15,
            this.txtName30,
            this.txtEIN15,
            this.txtEIN30});
            this.Detail.Height = 6.166667F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtName1
            // 
            this.txtName1.Height = 0.1770833F;
            this.txtName1.Left = 0.417F;
            this.txtName1.Name = "txtName1";
            this.txtName1.Text = null;
            this.txtName1.Top = 1.272F;
            this.txtName1.Width = 2.916667F;
            // 
            // txtName16
            // 
            this.txtName16.Height = 0.1770833F;
            this.txtName16.Left = 5.312833F;
            this.txtName16.Name = "txtName16";
            this.txtName16.Text = null;
            this.txtName16.Top = 1.272F;
            this.txtName16.Width = 2.916667F;
            // 
            // txtEIN1
            // 
            this.txtEIN1.Height = 0.1770833F;
            this.txtEIN1.Left = 3.500333F;
            this.txtEIN1.Name = "txtEIN1";
            this.txtEIN1.Text = null;
            this.txtEIN1.Top = 1.272F;
            this.txtEIN1.Width = 1.416667F;
            // 
            // txtEIN16
            // 
            this.txtEIN16.Height = 0.1770833F;
            this.txtEIN16.Left = 8.521167F;
            this.txtEIN16.Name = "txtEIN16";
            this.txtEIN16.Text = null;
            this.txtEIN16.Top = 1.272F;
            this.txtEIN16.Width = 1.416667F;
            // 
            // txtName2
            // 
            this.txtName2.Height = 0.1770833F;
            this.txtName2.Left = 0.417F;
            this.txtName2.Name = "txtName2";
            this.txtName2.Text = null;
            this.txtName2.Top = 1.595F;
            this.txtName2.Width = 2.916667F;
            // 
            // txtName17
            // 
            this.txtName17.Height = 0.1770833F;
            this.txtName17.Left = 5.312833F;
            this.txtName17.Name = "txtName17";
            this.txtName17.Text = null;
            this.txtName17.Top = 1.595F;
            this.txtName17.Width = 2.916667F;
            // 
            // txtEIN2
            // 
            this.txtEIN2.Height = 0.1770833F;
            this.txtEIN2.Left = 3.500333F;
            this.txtEIN2.Name = "txtEIN2";
            this.txtEIN2.Text = null;
            this.txtEIN2.Top = 1.595F;
            this.txtEIN2.Width = 1.416667F;
            // 
            // txtEIN17
            // 
            this.txtEIN17.Height = 0.1770833F;
            this.txtEIN17.Left = 8.521167F;
            this.txtEIN17.Name = "txtEIN17";
            this.txtEIN17.Text = null;
            this.txtEIN17.Top = 1.595F;
            this.txtEIN17.Width = 1.416667F;
            // 
            // txtName3
            // 
            this.txtName3.Height = 0.1770833F;
            this.txtName3.Left = 0.417F;
            this.txtName3.Name = "txtName3";
            this.txtName3.Text = null;
            this.txtName3.Top = 1.918F;
            this.txtName3.Width = 2.916667F;
            // 
            // txtName18
            // 
            this.txtName18.Height = 0.1770833F;
            this.txtName18.Left = 5.312833F;
            this.txtName18.Name = "txtName18";
            this.txtName18.Text = null;
            this.txtName18.Top = 1.918F;
            this.txtName18.Width = 2.916667F;
            // 
            // txtEIN3
            // 
            this.txtEIN3.Height = 0.1770833F;
            this.txtEIN3.Left = 3.500333F;
            this.txtEIN3.Name = "txtEIN3";
            this.txtEIN3.Text = null;
            this.txtEIN3.Top = 1.918F;
            this.txtEIN3.Width = 1.416667F;
            // 
            // txtEIN18
            // 
            this.txtEIN18.Height = 0.1770833F;
            this.txtEIN18.Left = 8.521167F;
            this.txtEIN18.Name = "txtEIN18";
            this.txtEIN18.Text = null;
            this.txtEIN18.Top = 1.918F;
            this.txtEIN18.Width = 1.416667F;
            // 
            // txtName4
            // 
            this.txtName4.Height = 0.1770833F;
            this.txtName4.Left = 0.417F;
            this.txtName4.Name = "txtName4";
            this.txtName4.Text = null;
            this.txtName4.Top = 2.22F;
            this.txtName4.Width = 2.916667F;
            // 
            // txtName19
            // 
            this.txtName19.Height = 0.1770833F;
            this.txtName19.Left = 5.312833F;
            this.txtName19.Name = "txtName19";
            this.txtName19.Text = null;
            this.txtName19.Top = 2.22F;
            this.txtName19.Width = 2.916667F;
            // 
            // txtEIN4
            // 
            this.txtEIN4.Height = 0.1770833F;
            this.txtEIN4.Left = 3.500333F;
            this.txtEIN4.Name = "txtEIN4";
            this.txtEIN4.Text = null;
            this.txtEIN4.Top = 2.22F;
            this.txtEIN4.Width = 1.416667F;
            // 
            // txtEIN19
            // 
            this.txtEIN19.Height = 0.1770833F;
            this.txtEIN19.Left = 8.521167F;
            this.txtEIN19.Name = "txtEIN19";
            this.txtEIN19.Text = null;
            this.txtEIN19.Top = 2.22F;
            this.txtEIN19.Width = 1.416667F;
            // 
            // txtName5
            // 
            this.txtName5.Height = 0.1770833F;
            this.txtName5.Left = 0.417F;
            this.txtName5.Name = "txtName5";
            this.txtName5.Text = null;
            this.txtName5.Top = 2.543F;
            this.txtName5.Width = 2.916667F;
            // 
            // txtName20
            // 
            this.txtName20.Height = 0.1770833F;
            this.txtName20.Left = 5.312833F;
            this.txtName20.Name = "txtName20";
            this.txtName20.Text = null;
            this.txtName20.Top = 2.543F;
            this.txtName20.Width = 2.916667F;
            // 
            // txtEIN5
            // 
            this.txtEIN5.Height = 0.1770833F;
            this.txtEIN5.Left = 3.500333F;
            this.txtEIN5.Name = "txtEIN5";
            this.txtEIN5.Text = null;
            this.txtEIN5.Top = 2.543F;
            this.txtEIN5.Width = 1.416667F;
            // 
            // txtEIN20
            // 
            this.txtEIN20.Height = 0.1770833F;
            this.txtEIN20.Left = 8.521167F;
            this.txtEIN20.Name = "txtEIN20";
            this.txtEIN20.Text = null;
            this.txtEIN20.Top = 2.543F;
            this.txtEIN20.Width = 1.416667F;
            // 
            // txtName6
            // 
            this.txtName6.Height = 0.1770833F;
            this.txtName6.Left = 0.417F;
            this.txtName6.Name = "txtName6";
            this.txtName6.Text = null;
            this.txtName6.Top = 2.852F;
            this.txtName6.Width = 2.916667F;
            // 
            // txtName21
            // 
            this.txtName21.Height = 0.1770833F;
            this.txtName21.Left = 5.312833F;
            this.txtName21.Name = "txtName21";
            this.txtName21.Text = null;
            this.txtName21.Top = 2.852F;
            this.txtName21.Width = 2.916667F;
            // 
            // txtEIN6
            // 
            this.txtEIN6.Height = 0.1770833F;
            this.txtEIN6.Left = 3.500333F;
            this.txtEIN6.Name = "txtEIN6";
            this.txtEIN6.Text = null;
            this.txtEIN6.Top = 2.852F;
            this.txtEIN6.Width = 1.416667F;
            // 
            // txtEIN21
            // 
            this.txtEIN21.Height = 0.1770833F;
            this.txtEIN21.Left = 8.521167F;
            this.txtEIN21.Name = "txtEIN21";
            this.txtEIN21.Text = null;
            this.txtEIN21.Top = 2.852F;
            this.txtEIN21.Width = 1.416667F;
            // 
            // txtName7
            // 
            this.txtName7.Height = 0.1770833F;
            this.txtName7.Left = 0.417F;
            this.txtName7.Name = "txtName7";
            this.txtName7.Text = null;
            this.txtName7.Top = 3.161F;
            this.txtName7.Width = 2.916667F;
            // 
            // txtName22
            // 
            this.txtName22.Height = 0.1770833F;
            this.txtName22.Left = 5.312833F;
            this.txtName22.Name = "txtName22";
            this.txtName22.Text = null;
            this.txtName22.Top = 3.161F;
            this.txtName22.Width = 2.916667F;
            // 
            // txtEIN7
            // 
            this.txtEIN7.Height = 0.1770833F;
            this.txtEIN7.Left = 3.500333F;
            this.txtEIN7.Name = "txtEIN7";
            this.txtEIN7.Text = null;
            this.txtEIN7.Top = 3.161F;
            this.txtEIN7.Width = 1.416667F;
            // 
            // txtEIN22
            // 
            this.txtEIN22.Height = 0.1770833F;
            this.txtEIN22.Left = 8.521167F;
            this.txtEIN22.Name = "txtEIN22";
            this.txtEIN22.Text = null;
            this.txtEIN22.Top = 3.161F;
            this.txtEIN22.Width = 1.416667F;
            // 
            // txtName8
            // 
            this.txtName8.Height = 0.1770833F;
            this.txtName8.Left = 0.417F;
            this.txtName8.Name = "txtName8";
            this.txtName8.Text = null;
            this.txtName8.Top = 3.484F;
            this.txtName8.Width = 2.916667F;
            // 
            // txtName23
            // 
            this.txtName23.Height = 0.1770833F;
            this.txtName23.Left = 5.312833F;
            this.txtName23.Name = "txtName23";
            this.txtName23.Text = null;
            this.txtName23.Top = 3.484F;
            this.txtName23.Width = 2.916667F;
            // 
            // txtEIN8
            // 
            this.txtEIN8.Height = 0.1770833F;
            this.txtEIN8.Left = 3.500333F;
            this.txtEIN8.Name = "txtEIN8";
            this.txtEIN8.Text = null;
            this.txtEIN8.Top = 3.484F;
            this.txtEIN8.Width = 1.416667F;
            // 
            // txtEIN23
            // 
            this.txtEIN23.Height = 0.1770833F;
            this.txtEIN23.Left = 8.521167F;
            this.txtEIN23.Name = "txtEIN23";
            this.txtEIN23.Text = null;
            this.txtEIN23.Top = 3.484F;
            this.txtEIN23.Width = 1.416667F;
            // 
            // txtName9
            // 
            this.txtName9.Height = 0.1770833F;
            this.txtName9.Left = 0.417F;
            this.txtName9.Name = "txtName9";
            this.txtName9.Text = null;
            this.txtName9.Top = 3.772F;
            this.txtName9.Width = 2.916667F;
            // 
            // txtName24
            // 
            this.txtName24.Height = 0.1770833F;
            this.txtName24.Left = 5.312833F;
            this.txtName24.Name = "txtName24";
            this.txtName24.Text = null;
            this.txtName24.Top = 3.772F;
            this.txtName24.Width = 2.916667F;
            // 
            // txtEIN9
            // 
            this.txtEIN9.Height = 0.1770833F;
            this.txtEIN9.Left = 3.500333F;
            this.txtEIN9.Name = "txtEIN9";
            this.txtEIN9.Text = null;
            this.txtEIN9.Top = 3.772F;
            this.txtEIN9.Width = 1.416667F;
            // 
            // txtEIN24
            // 
            this.txtEIN24.Height = 0.1770833F;
            this.txtEIN24.Left = 8.521167F;
            this.txtEIN24.Name = "txtEIN24";
            this.txtEIN24.Text = null;
            this.txtEIN24.Top = 3.772F;
            this.txtEIN24.Width = 1.416667F;
            // 
            // txtName10
            // 
            this.txtName10.Height = 0.1770833F;
            this.txtName10.Left = 0.417F;
            this.txtName10.Name = "txtName10";
            this.txtName10.Text = null;
            this.txtName10.Top = 4.074F;
            this.txtName10.Width = 2.916667F;
            // 
            // txtName25
            // 
            this.txtName25.Height = 0.1770833F;
            this.txtName25.Left = 5.312833F;
            this.txtName25.Name = "txtName25";
            this.txtName25.Text = null;
            this.txtName25.Top = 4.074F;
            this.txtName25.Width = 2.916667F;
            // 
            // txtEIN10
            // 
            this.txtEIN10.Height = 0.1770833F;
            this.txtEIN10.Left = 3.500333F;
            this.txtEIN10.Name = "txtEIN10";
            this.txtEIN10.Text = null;
            this.txtEIN10.Top = 4.074F;
            this.txtEIN10.Width = 1.416667F;
            // 
            // txtEIN25
            // 
            this.txtEIN25.Height = 0.1770833F;
            this.txtEIN25.Left = 8.521167F;
            this.txtEIN25.Name = "txtEIN25";
            this.txtEIN25.Text = null;
            this.txtEIN25.Top = 4.074F;
            this.txtEIN25.Width = 1.416667F;
            // 
            // txtName11
            // 
            this.txtName11.Height = 0.1770833F;
            this.txtName11.Left = 0.417F;
            this.txtName11.Name = "txtName11";
            this.txtName11.Text = null;
            this.txtName11.Top = 4.397F;
            this.txtName11.Width = 2.916667F;
            // 
            // txtName26
            // 
            this.txtName26.Height = 0.1770833F;
            this.txtName26.Left = 5.312833F;
            this.txtName26.Name = "txtName26";
            this.txtName26.Text = null;
            this.txtName26.Top = 4.397F;
            this.txtName26.Width = 2.916667F;
            // 
            // txtEIN11
            // 
            this.txtEIN11.Height = 0.1770833F;
            this.txtEIN11.Left = 3.500333F;
            this.txtEIN11.Name = "txtEIN11";
            this.txtEIN11.Text = null;
            this.txtEIN11.Top = 4.397F;
            this.txtEIN11.Width = 1.416667F;
            // 
            // txtEIN26
            // 
            this.txtEIN26.Height = 0.1770833F;
            this.txtEIN26.Left = 8.521167F;
            this.txtEIN26.Name = "txtEIN26";
            this.txtEIN26.Text = null;
            this.txtEIN26.Top = 4.397F;
            this.txtEIN26.Width = 1.416667F;
            // 
            // txtName12
            // 
            this.txtName12.Height = 0.1770833F;
            this.txtName12.Left = 0.417F;
            this.txtName12.Name = "txtName12";
            this.txtName12.Text = null;
            this.txtName12.Top = 4.72F;
            this.txtName12.Width = 2.916667F;
            // 
            // txtName27
            // 
            this.txtName27.Height = 0.1770833F;
            this.txtName27.Left = 5.312833F;
            this.txtName27.Name = "txtName27";
            this.txtName27.Text = null;
            this.txtName27.Top = 4.72F;
            this.txtName27.Width = 2.916667F;
            // 
            // txtEIN12
            // 
            this.txtEIN12.Height = 0.1770833F;
            this.txtEIN12.Left = 3.500333F;
            this.txtEIN12.Name = "txtEIN12";
            this.txtEIN12.Text = null;
            this.txtEIN12.Top = 4.72F;
            this.txtEIN12.Width = 1.416667F;
            // 
            // txtEIN27
            // 
            this.txtEIN27.Height = 0.1770833F;
            this.txtEIN27.Left = 8.521167F;
            this.txtEIN27.Name = "txtEIN27";
            this.txtEIN27.Text = null;
            this.txtEIN27.Top = 4.72F;
            this.txtEIN27.Width = 1.416667F;
            // 
            // txtName13
            // 
            this.txtName13.Height = 0.1770833F;
            this.txtName13.Left = 0.417F;
            this.txtName13.Name = "txtName13";
            this.txtName13.Text = null;
            this.txtName13.Top = 5.015F;
            this.txtName13.Width = 2.916667F;
            // 
            // txtName28
            // 
            this.txtName28.Height = 0.1770833F;
            this.txtName28.Left = 5.312833F;
            this.txtName28.Name = "txtName28";
            this.txtName28.Text = null;
            this.txtName28.Top = 5.015F;
            this.txtName28.Width = 2.916667F;
            // 
            // txtEIN13
            // 
            this.txtEIN13.Height = 0.1770833F;
            this.txtEIN13.Left = 3.500333F;
            this.txtEIN13.Name = "txtEIN13";
            this.txtEIN13.Text = null;
            this.txtEIN13.Top = 5.015F;
            this.txtEIN13.Width = 1.416667F;
            // 
            // txtEIN28
            // 
            this.txtEIN28.Height = 0.1770833F;
            this.txtEIN28.Left = 8.521167F;
            this.txtEIN28.Name = "txtEIN28";
            this.txtEIN28.Text = null;
            this.txtEIN28.Top = 5.015F;
            this.txtEIN28.Width = 1.416667F;
            // 
            // txtName14
            // 
            this.txtName14.Height = 0.1770833F;
            this.txtName14.Left = 0.417F;
            this.txtName14.Name = "txtName14";
            this.txtName14.Text = null;
            this.txtName14.Top = 5.338F;
            this.txtName14.Width = 2.916667F;
            // 
            // txtName29
            // 
            this.txtName29.Height = 0.1770833F;
            this.txtName29.Left = 5.312833F;
            this.txtName29.Name = "txtName29";
            this.txtName29.Text = null;
            this.txtName29.Top = 5.338F;
            this.txtName29.Width = 2.916667F;
            // 
            // txtEIN14
            // 
            this.txtEIN14.Height = 0.1770833F;
            this.txtEIN14.Left = 3.500333F;
            this.txtEIN14.Name = "txtEIN14";
            this.txtEIN14.Text = null;
            this.txtEIN14.Top = 5.338F;
            this.txtEIN14.Width = 1.416667F;
            // 
            // txtEIN29
            // 
            this.txtEIN29.Height = 0.1770833F;
            this.txtEIN29.Left = 8.521167F;
            this.txtEIN29.Name = "txtEIN29";
            this.txtEIN29.Text = null;
            this.txtEIN29.Top = 5.338F;
            this.txtEIN29.Width = 1.416667F;
            // 
            // txtName15
            // 
            this.txtName15.Height = 0.1770833F;
            this.txtName15.Left = 0.417F;
            this.txtName15.Name = "txtName15";
            this.txtName15.Text = null;
            this.txtName15.Top = 5.661F;
            this.txtName15.Width = 2.916667F;
            // 
            // txtName30
            // 
            this.txtName30.Height = 0.1770833F;
            this.txtName30.Left = 5.312833F;
            this.txtName30.Name = "txtName30";
            this.txtName30.Text = null;
            this.txtName30.Top = 5.661F;
            this.txtName30.Width = 2.916667F;
            // 
            // txtEIN15
            // 
            this.txtEIN15.Height = 0.1770833F;
            this.txtEIN15.Left = 3.500333F;
            this.txtEIN15.Name = "txtEIN15";
            this.txtEIN15.Text = null;
            this.txtEIN15.Top = 5.661F;
            this.txtEIN15.Width = 1.416667F;
            // 
            // txtEIN30
            // 
            this.txtEIN30.Height = 0.1770833F;
            this.txtEIN30.Left = 8.521167F;
            this.txtEIN30.Name = "txtEIN30";
            this.txtEIN30.Text = null;
            this.txtEIN30.Top = 5.661F;
            this.txtEIN30.Width = 1.416667F;
            // 
            // srpt1094C2016GroupMembers
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.9375F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN30;
	}
}
