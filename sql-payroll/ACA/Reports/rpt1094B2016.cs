﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1094B2016.
	/// </summary>
	public partial class rpt1094B2016 : BaseSectionReport
	{
		public rpt1094B2016()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "1094-B";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt1094B2016 InstancePtr
		{
			get
			{
				return (rpt1094B2016)Sys.GetInstance(typeof(rpt1094B2016));
			}
		}

		protected rpt1094B2016 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1094B2016	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c1094B theReport;
		private bool boolTestPrint;
		private bool boolFirstPage;

		public bool TestPrint
		{
			get
			{
				bool TestPrint = false;
				TestPrint = boolTestPrint;
				return TestPrint;
			}
		}

		public void Init(c1094B theModel, bool boolPrintTest)
		{
			theReport = theModel;
			boolTestPrint = boolPrintTest;
			frmReportViewer.InstancePtr.Init(this, "", 0, false, boolOverRideDuplex: true);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !boolFirstPage;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolFirstPage = true;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtContactTelephone As object	OnWrite(string)
			if (boolFirstPage)
			{
				if (!boolTestPrint)
				{
					txtAddress.Text = theReport.Address;
					txtCity.Text = theReport.City;
					txtContactName.Text = theReport.ContactFullName;
					txtContactTelephone.Text = theReport.ContactPhone;
					txtEIN.Text = theReport.EIN;
					txtFilerName.Text = theReport.FilerName;
					txtNumberSubmissions.Text = FCConvert.ToString(theReport.NumberOfSubmissions);
					txtPostalCode.Text = theReport.PostalCode;
					txtState.Text = theReport.State;
				}
				else
				{
					txtAddress.Text = "Street Address";
					txtCity.Text = "City or Town";
					txtState.Text = "State";
					txtPostalCode.Text = "12345";
					txtFilerName.Text = "Filer Name";
					txtEIN.Text = "00 0000000";
					txtContactName.Text = "Contact Person";
					txtContactTelephone.Text = "000-000-0000";
					txtNumberSubmissions.Text = "000";
				}
			}
			boolFirstPage = false;
		}

		private void rpt1094B2016_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt1094B2016 properties;
			//rpt1094B2016.Caption	= "1094-B";
			//rpt1094B2016.Left	= 0;
			//rpt1094B2016.Top	= 0;
			//rpt1094B2016.Width	= 20565;
			//rpt1094B2016.Height	= 8490;
			//rpt1094B2016.StartUpPosition	= 3;
			//rpt1094B2016.SectionData	= "rpt1094B2016.dsx":0000;
			//End Unmaped Properties
		}
	}
}
