﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016PortraitPage1.
	/// </summary>
	public partial class rpt1095C2020PortraitPage1 : FCSectionReport
	{
		public rpt1095C2020PortraitPage1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "1095-C";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt1095C2020PortraitPage1 InstancePtr
		{
			get
			{
				return (rpt1095C2020PortraitPage1)Sys.GetInstance(typeof(rpt1095C2020PortraitPage1));
			}
		}

		protected rpt1095C2020PortraitPage1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1095C2016PortraitPage1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolTestPrint;
		private bool boolMaskSSNs;
		private bool boolFirstRecord;
		private c1095CReport theReport;

		public void Init(ref c1095CReport reportObject, bool modalDialog)
		{
			theReport = reportObject;
			boolTestPrint = theReport.TestPrint;
			boolMaskSSNs = theReport.MaskSSNs;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", true, showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolTestPrint)
			{
				eArgs.EOF = !theReport.ListOfForms.IsCurrent();
			}
			else
			{
				eArgs.EOF = !boolFirstRecord;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (!theReport.TestPrint)
			{
				theReport.ListOfForms.MoveFirst();
			}
			boolFirstRecord = true;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			int intLine;
			string strTemp = "";
			double dblTemp = 0;
			int intCoveredCount = 0;
			ClearFields();
			if (!boolTestPrint)
			{
				c1095C employeerecord;
				cACAEmployeeDependent coveredIndividual;
				if (theReport.ListOfForms.IsCurrent())
				{
					employeerecord = (c1095C)theReport.ListOfForms.GetCurrentItem();
					if (employeerecord.PlanStartMonth > 0)
					{
						txtStartMonth.Text = Strings.Format(employeerecord.PlanStartMonth, "00");
					}
					txtEmployer.Text = theReport.EmployerInfo.Name;
					txtEmployerAddress.Text = theReport.EmployerInfo.Address;
					txtEmployerCity.Text = theReport.EmployerInfo.City;
					txtEmployerPostalCode.Text = theReport.EmployerInfo.PostalCode;
					txtEmployerState.Text = theReport.EmployerInfo.State;
					txtEmployerName.Text = theReport.EmployerInfo.Name;
					txtEmployerReturnAddress.Text = theReport.EmployerInfo.Address;
					txtEmployerCityStateZip.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(theReport.EmployerInfo.City + " " + theReport.EmployerInfo.State) + " " + theReport.EmployerInfo.PostalCode);
					txtEIN.Text = theReport.EmployerInfo.EIN;
					txtPhone.Text = theReport.EmployerInfo.Telephone;
					if (!theReport.MaskSSNs)
					{
						txtSSN.Text = employeerecord.SSN;
					}
					else
					{
						if (employeerecord.SSN.Length >= 4)
						{
							txtSSN.Text = "XXX-XX-" + Strings.Right(employeerecord.SSN, 4);
						}
					}
					txtName.Text = employeerecord.EmployeeFirstName;
                    txtMiddle.Text = (employeerecord.EmployeeMiddleName + " ").Left(1);
                    txtLast.Text = employeerecord.EmployeeLastName;
					txtAddress.Text = employeerecord.Address;
					txtCity.Text = employeerecord.City;
					txtState.Text = employeerecord.State;
					txtPostalCode.Text = employeerecord.PostalCode;
					txtEmployeeName.Text = employeerecord.EmployeeName;
					txtEmployeeAddress.Text = employeerecord.Address;
					txtEmployeeCityStateZip.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(employeerecord.City + " " + employeerecord.State) + " " + employeerecord.PostalCode);
					txtAll12Box14.Text = employeerecord.CoverageCodeRequiredAll12Months;
					dblTemp = employeerecord.EmployeeShareLowestPremiumAll12Months;
					if (dblTemp > 0)
					{
						txtAll12Box15.Text = Strings.Format(dblTemp, "###0.00");
					}
					txtAll12Box16.Text = employeerecord.SafeHarborCodeAll12Months;
					for (x = 1; x <= 12; x++)
					{
						if (txtAll12Box14.Text == "")
						{
							(this.Detail.Controls["txtBox14_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = employeerecord.GetCoverageRequiredCodeForMonth(x);
						}
						if (dblTemp == 0)
						{
							strTemp = employeerecord.GetCoverageRequiredCodeForMonth(x);
							if ((fecherFoundation.Strings.LCase(strTemp) == "1b") || (fecherFoundation.Strings.LCase(strTemp) == "1c") || (fecherFoundation.Strings.LCase(strTemp) == "1d") || (fecherFoundation.Strings.LCase(strTemp) == "1e"))
							{
								(this.Detail.Controls["txtBox15_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(employeerecord.GetEmployeeShareLowestPremiumForMonth(x), "###0.00");
							}
						}
						if (txtAll12Box16.Text == "")
						{
							(this.Detail.Controls["txtBox16_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = employeerecord.GetSafeHarborCodeForMonth(x);
						}
					}
					intCoveredCount = employeerecord.coveredIndividuals.Count;
                    if (employeerecord.Age > 0)
                    {
                        txtAge.Text = employeerecord.Age.ToString();
                    }
					if (intCoveredCount > 0)
					{
						txtCoveredIndividuals.Text = "X";
						intLine = 0;
						for (intLine = 1; intLine <= intCoveredCount; intLine++)
						{
							if (intLine > 6)
								break;
							coveredIndividual = employeerecord.coveredIndividuals[intLine];
							if (Information.IsDate(coveredIndividual.DateOfBirth) && coveredIndividual.SSN == "")
							{
								(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Year) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Day), 2);
							}
							(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.FirstName;
                            (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.MiddleName;
                            (this.Detail.Controls["txtCoveredLastName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.LastName;
                            strTemp = coveredIndividual.SSN;
							if (theReport.MaskSSNs)
							{
								if (strTemp.Length >= 4)
								{
									strTemp = "XXX-XX-" + Strings.Right(strTemp, 4);
								}
							}
							(this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
							if (coveredIndividual.CoveredAll12Months)
							{
								(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
							}
							else
							{
								for (x = 1; x <= 12; x++)
								{
									if (coveredIndividual.GetIsCoveredForMonth(x))
									{
										(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
									}
								}
							}
						}
						// intLine
					}
					theReport.ListOfForms.MoveNext();
				}
			}
			else
			{
				txtEmployerName.Text = "Employer Name";
				txtEmployerReturnAddress.Text = "Return Address";
				txtEmployerCityStateZip.Text = "City State Zip";
				txtEmployeeName.Text = "Employee Name";
				txtEmployeeAddress.Text = "Employee Address";
				txtEmployeeCityStateZip.Text = "City State Zip";
				txtName.Text = "First";
                txtMiddle.Text = "M";
                txtLast.Text = "Last";
				txtSSN.Text = "123-45-6789";
				txtAddress.Text = "Street Address";
				txtCity.Text = "City";
				txtState.Text = "State";
				txtPostalCode.Text = "Postal Code";
				txtEmployer.Text = "Employer Name";
				txtEIN.Text = "12-3456789";
				txtEmployerAddress.Text = "Employer Address";
				txtEmployerCity.Text = "City";
				txtEmployerPostalCode.Text = "Postal Code";
				txtEmployerState.Text = "State";
				txtPhone.Text = "555-555-5555";
				txtStartMonth.Text = "01";
				txtAll12Box14.Text = "XX";
				txtAll12Box15.Text = "0.00";
				txtAll12Box16.Text = "XX";
                txtAll12Zip.Text = "XXXXX";
				for (x = 1; x <= 12; x++)
				{
					(this.Detail.Controls["txtBox14_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "XX";
					(this.Detail.Controls["txtBox15_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "0.00";
					(this.Detail.Controls["txtBox16_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "XX";
                    (this.Detail.Controls["txtZip_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        "XXXXX";
                }
				txtCoveredIndividuals.Text = "X";
                txtAge.Text = "00";
				for (intLine = 1; intLine <= 6; intLine++)
				{
					(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "9999-99-99";
					(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "First";
                    (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "M";
                    (this.Detail.Controls["txtCoveredLastName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Last " + FCConvert.ToString(intLine);
                    (this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "000-00-0000";
					for (x = 1; x <= 12; x++)
					{
						(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					}
					// x
				}
				boolFirstRecord = false;
			}
		}

		private void ClearFields()
		{
			int x;
			int intLine;
			txtAddress.Text = "";
			txtAll12Box14.Text = "";
			txtAll12Box15.Text = "";
			txtAll12Box16.Text = "";
			txtEIN.Text = "";
			txtEmployer.Text = "";
			txtEmployerAddress.Text = "";
			txtName.Text = "";
			txtPhone.Text = "";
			txtSSN.Text = "";
			txtStartMonth.Text = "";
			txtCity.Text = "";
			txtState.Text = "";
			txtPostalCode.Text = "";
			txtEmployerCity.Text = "";
			txtEmployerState.Text = "";
			txtEmployerPostalCode.Text = "";
			txtCoveredIndividuals.Text = "";
            txtAge.Text = "";
			for (x = 1; x <= 12; x++)
			{
				(this.Detail.Controls["txtBox14_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtBox15_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtBox16_" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			}
			for (intLine = 1; intLine <= 6; intLine++)
			{
				(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredLastName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				for (x = 1; x <= 12; x++)
				{
					(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
		}

		private void rpt1095C2016PortraitPage1_Load(object sender, System.EventArgs e)
		{

		}
	}
}
