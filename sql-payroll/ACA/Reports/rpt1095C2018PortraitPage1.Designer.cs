﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016PortraitPage1.
	/// </summary>
	partial class rpt1095C2018PortraitPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2018PortraitPage1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtName,
            this.txtSSN,
            this.txtEmployer,
            this.txtEIN,
            this.txtAddress,
            this.txtEmployerAddress,
            this.txtPhone,
            this.txtStartMonth,
            this.txtAll12Box14,
            this.txtBox14_1,
            this.txtBox14_2,
            this.txtBox14_3,
            this.txtBox14_4,
            this.txtBox14_5,
            this.txtBox14_6,
            this.txtBox14_7,
            this.txtBox14_8,
            this.txtBox14_9,
            this.txtBox14_10,
            this.txtBox14_11,
            this.txtBox14_12,
            this.txtAll12Box15,
            this.txtBox15_1,
            this.txtBox15_2,
            this.txtBox15_3,
            this.txtBox15_4,
            this.txtBox15_5,
            this.txtBox15_6,
            this.txtBox15_7,
            this.txtBox15_8,
            this.txtBox15_9,
            this.txtBox15_10,
            this.txtBox15_11,
            this.txtBox15_12,
            this.txtAll12Box16,
            this.txtBox16_1,
            this.txtBox16_2,
            this.txtBox16_3,
            this.txtBox16_4,
            this.txtBox16_5,
            this.txtBox16_6,
            this.txtBox16_7,
            this.txtBox16_8,
            this.txtBox16_9,
            this.txtBox16_10,
            this.txtBox16_11,
            this.txtBox16_12,
            this.txtCoveredName1,
            this.txtCoveredIndividuals,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtCity,
            this.txtState,
            this.txtPostalCode,
            this.txtEmployerCity,
            this.txtEmployerState,
            this.txtEmployerPostalCode,
            this.txtEmployerName,
            this.txtEmployerReturnAddress,
            this.txtEmployerCityStateZip,
            this.txtEmployeeName,
            this.txtEmployeeAddress,
            this.txtEmployeeCityStateZip,
            this.txtCoveredLastName1,
            this.txtCoveredLastName2,
            this.txtCoveredLastName3,
            this.txtCoveredLastName4,
            this.txtCoveredLastName5,
            this.txtCoveredLastName6,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.txtMiddle,
            this.txtLast});
            this.Detail.Height = 9.989583F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtName
            // 
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.02083333F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-size: 8.5pt";
            this.txtName.Text = null;
            this.txtName.Top = 5.131945F;
            this.txtName.Width = 0.9583333F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 2.375F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Style = "font-size: 8.5pt";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 5.131945F;
            this.txtSSN.Width = 1.333333F;
            // 
            // txtEmployer
            // 
            this.txtEmployer.Height = 0.1770833F;
            this.txtEmployer.Left = 3.833333F;
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Style = "font-size: 8.5pt";
            this.txtEmployer.Text = null;
            this.txtEmployer.Top = 5.131945F;
            this.txtEmployer.Width = 2.270833F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 6.1875F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Style = "font-size: 8.5pt";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 5.131945F;
            this.txtEIN.Width = 1.270833F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.02083333F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 8.5pt";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 5.409722F;
            this.txtAddress.Width = 3.645833F;
            // 
            // txtEmployerAddress
            // 
            this.txtEmployerAddress.Height = 0.1770833F;
            this.txtEmployerAddress.Left = 3.833333F;
            this.txtEmployerAddress.Name = "txtEmployerAddress";
            this.txtEmployerAddress.Style = "font-size: 8.5pt";
            this.txtEmployerAddress.Text = null;
            this.txtEmployerAddress.Top = 5.409722F;
            this.txtEmployerAddress.Width = 2.270833F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1770833F;
            this.txtPhone.Left = 6.1875F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Style = "font-size: 8.5pt";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 5.409722F;
            this.txtPhone.Width = 1.270833F;
            // 
            // txtStartMonth
            // 
            this.txtStartMonth.Height = 0.1770833F;
            this.txtStartMonth.Left = 5.875F;
            this.txtStartMonth.Name = "txtStartMonth";
            this.txtStartMonth.Style = "font-size: 8.5pt";
            this.txtStartMonth.Text = null;
            this.txtStartMonth.Top = 5.875F;
            this.txtStartMonth.Width = 0.7916667F;
            // 
            // txtAll12Box14
            // 
            this.txtAll12Box14.Height = 0.1770833F;
            this.txtAll12Box14.Left = 0.75F;
            this.txtAll12Box14.Name = "txtAll12Box14";
            this.txtAll12Box14.Style = "font-size: 8.5pt";
            this.txtAll12Box14.Text = null;
            this.txtAll12Box14.Top = 6.25F;
            this.txtAll12Box14.Width = 0.5F;
            // 
            // txtBox14_1
            // 
            this.txtBox14_1.Height = 0.1770833F;
            this.txtBox14_1.Left = 1.229167F;
            this.txtBox14_1.Name = "txtBox14_1";
            this.txtBox14_1.Style = "font-size: 8.5pt";
            this.txtBox14_1.Text = null;
            this.txtBox14_1.Top = 6.25F;
            this.txtBox14_1.Width = 0.5F;
            // 
            // txtBox14_2
            // 
            this.txtBox14_2.Height = 0.1770833F;
            this.txtBox14_2.Left = 1.770833F;
            this.txtBox14_2.Name = "txtBox14_2";
            this.txtBox14_2.Style = "font-size: 8.5pt";
            this.txtBox14_2.Text = null;
            this.txtBox14_2.Top = 6.25F;
            this.txtBox14_2.Width = 0.5F;
            // 
            // txtBox14_3
            // 
            this.txtBox14_3.Height = 0.1770833F;
            this.txtBox14_3.Left = 2.270833F;
            this.txtBox14_3.Name = "txtBox14_3";
            this.txtBox14_3.Style = "font-size: 8.5pt";
            this.txtBox14_3.Text = null;
            this.txtBox14_3.Top = 6.25F;
            this.txtBox14_3.Width = 0.5F;
            // 
            // txtBox14_4
            // 
            this.txtBox14_4.Height = 0.1770833F;
            this.txtBox14_4.Left = 2.8125F;
            this.txtBox14_4.Name = "txtBox14_4";
            this.txtBox14_4.Style = "font-size: 8.5pt";
            this.txtBox14_4.Text = null;
            this.txtBox14_4.Top = 6.25F;
            this.txtBox14_4.Width = 0.5F;
            // 
            // txtBox14_5
            // 
            this.txtBox14_5.Height = 0.1770833F;
            this.txtBox14_5.Left = 3.3125F;
            this.txtBox14_5.Name = "txtBox14_5";
            this.txtBox14_5.Style = "font-size: 8.5pt";
            this.txtBox14_5.Text = null;
            this.txtBox14_5.Top = 6.25F;
            this.txtBox14_5.Width = 0.5F;
            // 
            // txtBox14_6
            // 
            this.txtBox14_6.Height = 0.1770833F;
            this.txtBox14_6.Left = 3.854167F;
            this.txtBox14_6.Name = "txtBox14_6";
            this.txtBox14_6.Style = "font-size: 8.5pt";
            this.txtBox14_6.Text = null;
            this.txtBox14_6.Top = 6.25F;
            this.txtBox14_6.Width = 0.5F;
            // 
            // txtBox14_7
            // 
            this.txtBox14_7.Height = 0.1770833F;
            this.txtBox14_7.Left = 4.333333F;
            this.txtBox14_7.Name = "txtBox14_7";
            this.txtBox14_7.Style = "font-size: 8.5pt";
            this.txtBox14_7.Text = null;
            this.txtBox14_7.Top = 6.25F;
            this.txtBox14_7.Width = 0.5F;
            // 
            // txtBox14_8
            // 
            this.txtBox14_8.Height = 0.1770833F;
            this.txtBox14_8.Left = 4.895833F;
            this.txtBox14_8.Name = "txtBox14_8";
            this.txtBox14_8.Style = "font-size: 8.5pt";
            this.txtBox14_8.Text = null;
            this.txtBox14_8.Top = 6.25F;
            this.txtBox14_8.Width = 0.5F;
            // 
            // txtBox14_9
            // 
            this.txtBox14_9.Height = 0.1770833F;
            this.txtBox14_9.Left = 5.395833F;
            this.txtBox14_9.Name = "txtBox14_9";
            this.txtBox14_9.Style = "font-size: 8.5pt";
            this.txtBox14_9.Text = null;
            this.txtBox14_9.Top = 6.25F;
            this.txtBox14_9.Width = 0.5F;
            // 
            // txtBox14_10
            // 
            this.txtBox14_10.Height = 0.1770833F;
            this.txtBox14_10.Left = 5.9375F;
            this.txtBox14_10.Name = "txtBox14_10";
            this.txtBox14_10.Style = "font-size: 8.5pt";
            this.txtBox14_10.Text = null;
            this.txtBox14_10.Top = 6.25F;
            this.txtBox14_10.Width = 0.5F;
            // 
            // txtBox14_11
            // 
            this.txtBox14_11.Height = 0.1770833F;
            this.txtBox14_11.Left = 6.4375F;
            this.txtBox14_11.Name = "txtBox14_11";
            this.txtBox14_11.Style = "font-size: 8.5pt";
            this.txtBox14_11.Text = null;
            this.txtBox14_11.Top = 6.25F;
            this.txtBox14_11.Width = 0.5F;
            // 
            // txtBox14_12
            // 
            this.txtBox14_12.Height = 0.1770833F;
            this.txtBox14_12.Left = 6.96875F;
            this.txtBox14_12.Name = "txtBox14_12";
            this.txtBox14_12.Style = "font-size: 8.5pt";
            this.txtBox14_12.Text = null;
            this.txtBox14_12.Top = 6.25F;
            this.txtBox14_12.Width = 0.5F;
            // 
            // txtAll12Box15
            // 
            this.txtAll12Box15.Height = 0.1770833F;
            this.txtAll12Box15.Left = 0.75F;
            this.txtAll12Box15.Name = "txtAll12Box15";
            this.txtAll12Box15.Style = "font-size: 8.5pt";
            this.txtAll12Box15.Text = null;
            this.txtAll12Box15.Top = 6.75F;
            this.txtAll12Box15.Width = 0.5F;
            // 
            // txtBox15_1
            // 
            this.txtBox15_1.Height = 0.1770833F;
            this.txtBox15_1.Left = 1.229167F;
            this.txtBox15_1.Name = "txtBox15_1";
            this.txtBox15_1.Style = "font-size: 8.5pt";
            this.txtBox15_1.Text = null;
            this.txtBox15_1.Top = 6.75F;
            this.txtBox15_1.Width = 0.5F;
            // 
            // txtBox15_2
            // 
            this.txtBox15_2.Height = 0.1770833F;
            this.txtBox15_2.Left = 1.770833F;
            this.txtBox15_2.Name = "txtBox15_2";
            this.txtBox15_2.Style = "font-size: 8.5pt";
            this.txtBox15_2.Text = null;
            this.txtBox15_2.Top = 6.75F;
            this.txtBox15_2.Width = 0.5F;
            // 
            // txtBox15_3
            // 
            this.txtBox15_3.Height = 0.1770833F;
            this.txtBox15_3.Left = 2.270833F;
            this.txtBox15_3.Name = "txtBox15_3";
            this.txtBox15_3.Style = "font-size: 8.5pt";
            this.txtBox15_3.Text = null;
            this.txtBox15_3.Top = 6.75F;
            this.txtBox15_3.Width = 0.5F;
            // 
            // txtBox15_4
            // 
            this.txtBox15_4.Height = 0.1770833F;
            this.txtBox15_4.Left = 2.8125F;
            this.txtBox15_4.Name = "txtBox15_4";
            this.txtBox15_4.Style = "font-size: 8.5pt";
            this.txtBox15_4.Text = null;
            this.txtBox15_4.Top = 6.75F;
            this.txtBox15_4.Width = 0.5F;
            // 
            // txtBox15_5
            // 
            this.txtBox15_5.Height = 0.1770833F;
            this.txtBox15_5.Left = 3.3125F;
            this.txtBox15_5.Name = "txtBox15_5";
            this.txtBox15_5.Style = "font-size: 8.5pt";
            this.txtBox15_5.Text = null;
            this.txtBox15_5.Top = 6.75F;
            this.txtBox15_5.Width = 0.5F;
            // 
            // txtBox15_6
            // 
            this.txtBox15_6.Height = 0.1770833F;
            this.txtBox15_6.Left = 3.854167F;
            this.txtBox15_6.Name = "txtBox15_6";
            this.txtBox15_6.Style = "font-size: 8.5pt";
            this.txtBox15_6.Text = null;
            this.txtBox15_6.Top = 6.75F;
            this.txtBox15_6.Width = 0.5F;
            // 
            // txtBox15_7
            // 
            this.txtBox15_7.Height = 0.1770833F;
            this.txtBox15_7.Left = 4.333333F;
            this.txtBox15_7.Name = "txtBox15_7";
            this.txtBox15_7.Style = "font-size: 8.5pt";
            this.txtBox15_7.Text = null;
            this.txtBox15_7.Top = 6.75F;
            this.txtBox15_7.Width = 0.5F;
            // 
            // txtBox15_8
            // 
            this.txtBox15_8.Height = 0.1770833F;
            this.txtBox15_8.Left = 4.895833F;
            this.txtBox15_8.Name = "txtBox15_8";
            this.txtBox15_8.Style = "font-size: 8.5pt";
            this.txtBox15_8.Text = null;
            this.txtBox15_8.Top = 6.75F;
            this.txtBox15_8.Width = 0.5F;
            // 
            // txtBox15_9
            // 
            this.txtBox15_9.Height = 0.1770833F;
            this.txtBox15_9.Left = 5.395833F;
            this.txtBox15_9.Name = "txtBox15_9";
            this.txtBox15_9.Style = "font-size: 8.5pt";
            this.txtBox15_9.Text = null;
            this.txtBox15_9.Top = 6.75F;
            this.txtBox15_9.Width = 0.5F;
            // 
            // txtBox15_10
            // 
            this.txtBox15_10.Height = 0.1770833F;
            this.txtBox15_10.Left = 5.9375F;
            this.txtBox15_10.Name = "txtBox15_10";
            this.txtBox15_10.Style = "font-size: 8.5pt";
            this.txtBox15_10.Text = null;
            this.txtBox15_10.Top = 6.75F;
            this.txtBox15_10.Width = 0.5F;
            // 
            // txtBox15_11
            // 
            this.txtBox15_11.Height = 0.1770833F;
            this.txtBox15_11.Left = 6.4375F;
            this.txtBox15_11.Name = "txtBox15_11";
            this.txtBox15_11.Style = "font-size: 8.5pt";
            this.txtBox15_11.Text = null;
            this.txtBox15_11.Top = 6.75F;
            this.txtBox15_11.Width = 0.5F;
            // 
            // txtBox15_12
            // 
            this.txtBox15_12.Height = 0.1770833F;
            this.txtBox15_12.Left = 6.972222F;
            this.txtBox15_12.Name = "txtBox15_12";
            this.txtBox15_12.Style = "font-size: 8.5pt";
            this.txtBox15_12.Text = null;
            this.txtBox15_12.Top = 6.75F;
            this.txtBox15_12.Width = 0.5F;
            // 
            // txtAll12Box16
            // 
            this.txtAll12Box16.Height = 0.1770833F;
            this.txtAll12Box16.Left = 0.75F;
            this.txtAll12Box16.Name = "txtAll12Box16";
            this.txtAll12Box16.Style = "font-size: 8.5pt";
            this.txtAll12Box16.Text = null;
            this.txtAll12Box16.Top = 7.166667F;
            this.txtAll12Box16.Width = 0.5F;
            // 
            // txtBox16_1
            // 
            this.txtBox16_1.Height = 0.1770833F;
            this.txtBox16_1.Left = 1.229167F;
            this.txtBox16_1.Name = "txtBox16_1";
            this.txtBox16_1.Style = "font-size: 8.5pt";
            this.txtBox16_1.Text = null;
            this.txtBox16_1.Top = 7.166667F;
            this.txtBox16_1.Width = 0.5F;
            // 
            // txtBox16_2
            // 
            this.txtBox16_2.Height = 0.1770833F;
            this.txtBox16_2.Left = 1.770833F;
            this.txtBox16_2.Name = "txtBox16_2";
            this.txtBox16_2.Style = "font-size: 8.5pt";
            this.txtBox16_2.Text = null;
            this.txtBox16_2.Top = 7.166667F;
            this.txtBox16_2.Width = 0.5F;
            // 
            // txtBox16_3
            // 
            this.txtBox16_3.Height = 0.1770833F;
            this.txtBox16_3.Left = 2.270833F;
            this.txtBox16_3.Name = "txtBox16_3";
            this.txtBox16_3.Style = "font-size: 8.5pt";
            this.txtBox16_3.Text = null;
            this.txtBox16_3.Top = 7.166667F;
            this.txtBox16_3.Width = 0.5F;
            // 
            // txtBox16_4
            // 
            this.txtBox16_4.Height = 0.1770833F;
            this.txtBox16_4.Left = 2.8125F;
            this.txtBox16_4.Name = "txtBox16_4";
            this.txtBox16_4.Style = "font-size: 8.5pt";
            this.txtBox16_4.Text = null;
            this.txtBox16_4.Top = 7.166667F;
            this.txtBox16_4.Width = 0.5F;
            // 
            // txtBox16_5
            // 
            this.txtBox16_5.Height = 0.1770833F;
            this.txtBox16_5.Left = 3.3125F;
            this.txtBox16_5.Name = "txtBox16_5";
            this.txtBox16_5.Style = "font-size: 8.5pt";
            this.txtBox16_5.Text = null;
            this.txtBox16_5.Top = 7.166667F;
            this.txtBox16_5.Width = 0.5F;
            // 
            // txtBox16_6
            // 
            this.txtBox16_6.Height = 0.1770833F;
            this.txtBox16_6.Left = 3.854167F;
            this.txtBox16_6.Name = "txtBox16_6";
            this.txtBox16_6.Style = "font-size: 8.5pt";
            this.txtBox16_6.Text = null;
            this.txtBox16_6.Top = 7.166667F;
            this.txtBox16_6.Width = 0.5F;
            // 
            // txtBox16_7
            // 
            this.txtBox16_7.Height = 0.1770833F;
            this.txtBox16_7.Left = 4.333333F;
            this.txtBox16_7.Name = "txtBox16_7";
            this.txtBox16_7.Style = "font-size: 8.5pt";
            this.txtBox16_7.Text = null;
            this.txtBox16_7.Top = 7.166667F;
            this.txtBox16_7.Width = 0.5F;
            // 
            // txtBox16_8
            // 
            this.txtBox16_8.Height = 0.1770833F;
            this.txtBox16_8.Left = 4.895833F;
            this.txtBox16_8.Name = "txtBox16_8";
            this.txtBox16_8.Style = "font-size: 8.5pt";
            this.txtBox16_8.Text = null;
            this.txtBox16_8.Top = 7.166667F;
            this.txtBox16_8.Width = 0.5F;
            // 
            // txtBox16_9
            // 
            this.txtBox16_9.Height = 0.1770833F;
            this.txtBox16_9.Left = 5.395833F;
            this.txtBox16_9.Name = "txtBox16_9";
            this.txtBox16_9.Style = "font-size: 8.5pt";
            this.txtBox16_9.Text = null;
            this.txtBox16_9.Top = 7.166667F;
            this.txtBox16_9.Width = 0.5F;
            // 
            // txtBox16_10
            // 
            this.txtBox16_10.Height = 0.1770833F;
            this.txtBox16_10.Left = 5.9375F;
            this.txtBox16_10.Name = "txtBox16_10";
            this.txtBox16_10.Style = "font-size: 8.5pt";
            this.txtBox16_10.Text = null;
            this.txtBox16_10.Top = 7.166667F;
            this.txtBox16_10.Width = 0.5F;
            // 
            // txtBox16_11
            // 
            this.txtBox16_11.Height = 0.1770833F;
            this.txtBox16_11.Left = 6.4375F;
            this.txtBox16_11.Name = "txtBox16_11";
            this.txtBox16_11.Style = "font-size: 8.5pt";
            this.txtBox16_11.Text = null;
            this.txtBox16_11.Top = 7.166667F;
            this.txtBox16_11.Width = 0.5F;
            // 
            // txtBox16_12
            // 
            this.txtBox16_12.Height = 0.1770833F;
            this.txtBox16_12.Left = 6.972222F;
            this.txtBox16_12.Name = "txtBox16_12";
            this.txtBox16_12.Style = "font-size: 8.5pt";
            this.txtBox16_12.Text = null;
            this.txtBox16_12.Top = 7.166667F;
            this.txtBox16_12.Width = 0.5F;
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.CanGrow = false;
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.1666667F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Style = "font-size: 8.5pt";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 7.9375F;
            this.txtCoveredName1.Width = 0.625F;
            // 
            // txtCoveredIndividuals
            // 
            this.txtCoveredIndividuals.Height = 0.1770833F;
            this.txtCoveredIndividuals.Left = 6.791667F;
            this.txtCoveredIndividuals.Name = "txtCoveredIndividuals";
            this.txtCoveredIndividuals.Text = null;
            this.txtCoveredIndividuals.Top = 7.395833F;
            this.txtCoveredIndividuals.Width = 0.25F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 1.833333F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Style = "font-size: 8.5pt";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 7.9375F;
            this.txtCoveredSSN1.Width = 0.8333333F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 2.75F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Style = "font-size: 8.5pt";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 7.9375F;
            this.txtCoveredDOB1.Width = 0.75F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.CanGrow = false;
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.1666667F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Style = "font-size: 8.5pt";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 8.3125F;
            this.txtCoveredName2.Width = 0.625F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 1.833333F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Style = "font-size: 8.5pt";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 8.3125F;
            this.txtCoveredSSN2.Width = 0.8333333F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 2.75F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Style = "font-size: 8.5pt";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 8.3125F;
            this.txtCoveredDOB2.Width = 0.75F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.CanGrow = false;
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.1666667F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Style = "font-size: 8.5pt";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 8.6875F;
            this.txtCoveredName3.Width = 0.625F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 1.833333F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Style = "font-size: 8.5pt";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 8.6875F;
            this.txtCoveredSSN3.Width = 0.8333333F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 2.75F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Style = "font-size: 8.5pt";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 8.6875F;
            this.txtCoveredDOB3.Width = 0.75F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.CanGrow = false;
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.1666667F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Style = "font-size: 8.5pt";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 9.0625F;
            this.txtCoveredName4.Width = 0.625F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 1.833333F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Style = "font-size: 8.5pt";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 9.0625F;
            this.txtCoveredSSN4.Width = 0.8333333F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 2.75F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Style = "font-size: 8.5pt";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 9.0625F;
            this.txtCoveredDOB4.Width = 0.75F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.CanGrow = false;
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.1666667F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Style = "font-size: 8.5pt";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 9.4375F;
            this.txtCoveredName5.Width = 0.625F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 1.833333F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Style = "font-size: 8.5pt";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 9.4375F;
            this.txtCoveredSSN5.Width = 0.8333333F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 2.75F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Style = "font-size: 8.5pt";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 9.4375F;
            this.txtCoveredDOB5.Width = 0.75F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.CanGrow = false;
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.1666667F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Style = "font-size: 8.5pt";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 9.8125F;
            this.txtCoveredName6.Width = 0.625F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.CanGrow = false;
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 1.833333F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Style = "font-size: 8.5pt";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 9.8125F;
            this.txtCoveredSSN6.Width = 0.8333333F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.CanGrow = false;
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 2.75F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Style = "font-size: 8.5pt";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 9.8125F;
            this.txtCoveredDOB6.Width = 0.75F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 3.625F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = null;
            this.txtCoveredAll12_1.Top = 7.9375F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 3.958333F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = null;
            this.txtCoveredMonth1_1.Top = 7.9375F;
            this.txtCoveredMonth1_1.Width = 0.1875F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 4.291667F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = null;
            this.txtCoveredMonth2_1.Top = 7.9375F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 4.604167F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = null;
            this.txtCoveredMonth3_1.Top = 7.9375F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 4.895833F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = null;
            this.txtCoveredMonth4_1.Top = 7.9375F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 5.177083F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = null;
            this.txtCoveredMonth5_1.Top = 7.9375F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 5.458333F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = null;
            this.txtCoveredMonth6_1.Top = 7.9375F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 5.791667F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = null;
            this.txtCoveredMonth7_1.Top = 7.9375F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 6.104167F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = null;
            this.txtCoveredMonth8_1.Top = 7.9375F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 6.385417F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = null;
            this.txtCoveredMonth9_1.Top = 7.9375F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 6.677083F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = null;
            this.txtCoveredMonth10_1.Top = 7.9375F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 6.958333F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = null;
            this.txtCoveredMonth11_1.Top = 7.9375F;
            this.txtCoveredMonth11_1.Width = 0.25F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 7.270833F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = null;
            this.txtCoveredMonth12_1.Top = 7.9375F;
            this.txtCoveredMonth12_1.Width = 0.1875F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 3.625F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = null;
            this.txtCoveredAll12_2.Top = 8.3125F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 3.958333F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = null;
            this.txtCoveredMonth1_2.Top = 8.3125F;
            this.txtCoveredMonth1_2.Width = 0.1875F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 4.291667F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = null;
            this.txtCoveredMonth2_2.Top = 8.3125F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 4.604167F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = null;
            this.txtCoveredMonth3_2.Top = 8.3125F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 4.895833F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = null;
            this.txtCoveredMonth4_2.Top = 8.3125F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 5.177083F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = null;
            this.txtCoveredMonth5_2.Top = 8.3125F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 5.458333F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = null;
            this.txtCoveredMonth6_2.Top = 8.3125F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 5.791667F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = null;
            this.txtCoveredMonth7_2.Top = 8.3125F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 6.104167F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = null;
            this.txtCoveredMonth8_2.Top = 8.3125F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 6.385417F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = null;
            this.txtCoveredMonth9_2.Top = 8.3125F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 6.677083F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = null;
            this.txtCoveredMonth10_2.Top = 8.3125F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 6.958333F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = null;
            this.txtCoveredMonth11_2.Top = 8.3125F;
            this.txtCoveredMonth11_2.Width = 0.25F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 7.270833F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = null;
            this.txtCoveredMonth12_2.Top = 8.3125F;
            this.txtCoveredMonth12_2.Width = 0.1875F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 3.625F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = null;
            this.txtCoveredAll12_3.Top = 8.6875F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 3.958333F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = null;
            this.txtCoveredMonth1_3.Top = 8.6875F;
            this.txtCoveredMonth1_3.Width = 0.1875F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 4.291667F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = null;
            this.txtCoveredMonth2_3.Top = 8.6875F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 4.604167F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = null;
            this.txtCoveredMonth3_3.Top = 8.6875F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 4.895833F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = null;
            this.txtCoveredMonth4_3.Top = 8.6875F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 5.177083F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = null;
            this.txtCoveredMonth5_3.Top = 8.6875F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 5.458333F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = null;
            this.txtCoveredMonth6_3.Top = 8.6875F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 5.791667F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = null;
            this.txtCoveredMonth7_3.Top = 8.6875F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 6.104167F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = null;
            this.txtCoveredMonth8_3.Top = 8.6875F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 6.385417F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = null;
            this.txtCoveredMonth9_3.Top = 8.6875F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 6.677083F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = null;
            this.txtCoveredMonth10_3.Top = 8.6875F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 6.958333F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = null;
            this.txtCoveredMonth11_3.Top = 8.6875F;
            this.txtCoveredMonth11_3.Width = 0.25F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 7.270833F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = null;
            this.txtCoveredMonth12_3.Top = 8.6875F;
            this.txtCoveredMonth12_3.Width = 0.1875F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 3.625F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = null;
            this.txtCoveredAll12_4.Top = 9.0625F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 3.958333F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = null;
            this.txtCoveredMonth1_4.Top = 9.0625F;
            this.txtCoveredMonth1_4.Width = 0.1875F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 4.291667F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = null;
            this.txtCoveredMonth2_4.Top = 9.0625F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 4.604167F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = null;
            this.txtCoveredMonth3_4.Top = 9.0625F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 4.895833F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = null;
            this.txtCoveredMonth4_4.Top = 9.0625F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 5.177083F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = null;
            this.txtCoveredMonth5_4.Top = 9.0625F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 5.458333F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = null;
            this.txtCoveredMonth6_4.Top = 9.0625F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 5.791667F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = null;
            this.txtCoveredMonth7_4.Top = 9.0625F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 6.104167F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = null;
            this.txtCoveredMonth8_4.Top = 9.0625F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 6.388889F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = null;
            this.txtCoveredMonth9_4.Top = 9.0625F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 6.680555F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = null;
            this.txtCoveredMonth10_4.Top = 9.0625F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 6.958333F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = null;
            this.txtCoveredMonth11_4.Top = 9.0625F;
            this.txtCoveredMonth11_4.Width = 0.25F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 7.270833F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = null;
            this.txtCoveredMonth12_4.Top = 9.0625F;
            this.txtCoveredMonth12_4.Width = 0.1875F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 3.625F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = null;
            this.txtCoveredAll12_5.Top = 9.4375F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 3.958333F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = null;
            this.txtCoveredMonth1_5.Top = 9.4375F;
            this.txtCoveredMonth1_5.Width = 0.1875F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 4.291667F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = null;
            this.txtCoveredMonth2_5.Top = 9.4375F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 4.604167F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = null;
            this.txtCoveredMonth3_5.Top = 9.4375F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 4.895833F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = null;
            this.txtCoveredMonth4_5.Top = 9.4375F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 5.177083F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = null;
            this.txtCoveredMonth5_5.Top = 9.4375F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 5.458333F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = null;
            this.txtCoveredMonth6_5.Top = 9.4375F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 5.791667F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = null;
            this.txtCoveredMonth7_5.Top = 9.4375F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 6.104167F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = null;
            this.txtCoveredMonth8_5.Top = 9.4375F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 6.388889F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = null;
            this.txtCoveredMonth9_5.Top = 9.4375F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 6.680555F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = null;
            this.txtCoveredMonth10_5.Top = 9.4375F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 6.958333F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = null;
            this.txtCoveredMonth11_5.Top = 9.4375F;
            this.txtCoveredMonth11_5.Width = 0.25F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 7.270833F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = null;
            this.txtCoveredMonth12_5.Top = 9.4375F;
            this.txtCoveredMonth12_5.Width = 0.1875F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.CanGrow = false;
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 3.625F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = null;
            this.txtCoveredAll12_6.Top = 9.8125F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.CanGrow = false;
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 3.958333F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = null;
            this.txtCoveredMonth1_6.Top = 9.8125F;
            this.txtCoveredMonth1_6.Width = 0.1875F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.CanGrow = false;
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 4.291667F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = null;
            this.txtCoveredMonth2_6.Top = 9.8125F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.CanGrow = false;
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 4.604167F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = null;
            this.txtCoveredMonth3_6.Top = 9.8125F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.CanGrow = false;
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 4.895833F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = null;
            this.txtCoveredMonth4_6.Top = 9.8125F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.CanGrow = false;
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 5.177083F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = null;
            this.txtCoveredMonth5_6.Top = 9.8125F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.CanGrow = false;
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 5.458333F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = null;
            this.txtCoveredMonth6_6.Top = 9.8125F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.CanGrow = false;
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 5.791667F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = null;
            this.txtCoveredMonth7_6.Top = 9.8125F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.CanGrow = false;
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 6.104167F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = null;
            this.txtCoveredMonth8_6.Top = 9.8125F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.CanGrow = false;
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 6.388889F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = null;
            this.txtCoveredMonth9_6.Top = 9.8125F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.CanGrow = false;
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 6.680555F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = null;
            this.txtCoveredMonth10_6.Top = 9.8125F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.CanGrow = false;
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 6.958333F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = null;
            this.txtCoveredMonth11_6.Top = 9.8125F;
            this.txtCoveredMonth11_6.Width = 0.25F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.CanGrow = false;
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 7.270833F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = null;
            this.txtCoveredMonth12_6.Top = 9.8125F;
            this.txtCoveredMonth12_6.Width = 0.1875F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 0.02083333F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-size: 8.5pt; white-space: nowrap";
            this.txtCity.Text = null;
            this.txtCity.Top = 5.611111F;
            this.txtCity.Width = 1.083333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 1.145833F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-size: 8.5pt";
            this.txtState.Text = null;
            this.txtState.Top = 5.611111F;
            this.txtState.Width = 1.25F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 2.4375F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Style = "font-size: 8.5pt";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 5.611111F;
            this.txtPostalCode.Width = 1.333333F;
            // 
            // txtEmployerCity
            // 
            this.txtEmployerCity.Height = 0.1770833F;
            this.txtEmployerCity.Left = 3.833333F;
            this.txtEmployerCity.Name = "txtEmployerCity";
            this.txtEmployerCity.Style = "font-size: 8.5pt";
            this.txtEmployerCity.Text = null;
            this.txtEmployerCity.Top = 5.611111F;
            this.txtEmployerCity.Width = 1.083333F;
            // 
            // txtEmployerState
            // 
            this.txtEmployerState.Height = 0.1770833F;
            this.txtEmployerState.Left = 4.9375F;
            this.txtEmployerState.Name = "txtEmployerState";
            this.txtEmployerState.Style = "font-size: 8.5pt";
            this.txtEmployerState.Text = null;
            this.txtEmployerState.Top = 5.611111F;
            this.txtEmployerState.Width = 1.1875F;
            // 
            // txtEmployerPostalCode
            // 
            this.txtEmployerPostalCode.Height = 0.1770833F;
            this.txtEmployerPostalCode.Left = 6.1875F;
            this.txtEmployerPostalCode.Name = "txtEmployerPostalCode";
            this.txtEmployerPostalCode.Style = "font-size: 8.5pt";
            this.txtEmployerPostalCode.Text = null;
            this.txtEmployerPostalCode.Top = 5.611111F;
            this.txtEmployerPostalCode.Width = 1.270833F;
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.Height = 0.1770833F;
            this.txtEmployerName.Left = 0.25F;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Style = "font-size: 8.5pt";
            this.txtEmployerName.Text = null;
            this.txtEmployerName.Top = 0.3125F;
            this.txtEmployerName.Width = 3.083333F;
            // 
            // txtEmployerReturnAddress
            // 
            this.txtEmployerReturnAddress.Height = 0.1770833F;
            this.txtEmployerReturnAddress.Left = 0.25F;
            this.txtEmployerReturnAddress.Name = "txtEmployerReturnAddress";
            this.txtEmployerReturnAddress.Style = "font-size: 8.5pt";
            this.txtEmployerReturnAddress.Text = null;
            this.txtEmployerReturnAddress.Top = 0.5F;
            this.txtEmployerReturnAddress.Width = 3.083333F;
            // 
            // txtEmployerCityStateZip
            // 
            this.txtEmployerCityStateZip.Height = 0.1770833F;
            this.txtEmployerCityStateZip.Left = 0.25F;
            this.txtEmployerCityStateZip.Name = "txtEmployerCityStateZip";
            this.txtEmployerCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployerCityStateZip.Text = null;
            this.txtEmployerCityStateZip.Top = 0.6875F;
            this.txtEmployerCityStateZip.Width = 3.083333F;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Height = 0.1770833F;
            this.txtEmployeeName.Left = 0.25F;
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Style = "font-size: 8.5pt";
            this.txtEmployeeName.Text = null;
            this.txtEmployeeName.Top = 2.3125F;
            this.txtEmployeeName.Width = 3.083333F;
            // 
            // txtEmployeeAddress
            // 
            this.txtEmployeeAddress.Height = 0.1770833F;
            this.txtEmployeeAddress.Left = 0.25F;
            this.txtEmployeeAddress.Name = "txtEmployeeAddress";
            this.txtEmployeeAddress.Style = "font-size: 8.5pt";
            this.txtEmployeeAddress.Text = null;
            this.txtEmployeeAddress.Top = 2.5F;
            this.txtEmployeeAddress.Width = 3.083333F;
            // 
            // txtEmployeeCityStateZip
            // 
            this.txtEmployeeCityStateZip.Height = 0.1770833F;
            this.txtEmployeeCityStateZip.Left = 0.25F;
            this.txtEmployeeCityStateZip.Name = "txtEmployeeCityStateZip";
            this.txtEmployeeCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployeeCityStateZip.Text = null;
            this.txtEmployeeCityStateZip.Top = 2.6875F;
            this.txtEmployeeCityStateZip.Width = 3.083333F;
            // 
            // txtCoveredLastName1
            // 
            this.txtCoveredLastName1.Height = 0.1770833F;
            this.txtCoveredLastName1.Left = 1.125F;
            this.txtCoveredLastName1.Name = "txtCoveredLastName1";
            this.txtCoveredLastName1.Style = "font-size: 8.5pt";
            this.txtCoveredLastName1.Text = null;
            this.txtCoveredLastName1.Top = 7.9375F;
            this.txtCoveredLastName1.Width = 0.625F;
            // 
            // txtCoveredLastName2
            // 
            this.txtCoveredLastName2.Height = 0.1770833F;
            this.txtCoveredLastName2.Left = 1.125F;
            this.txtCoveredLastName2.Name = "txtCoveredLastName2";
            this.txtCoveredLastName2.Style = "font-size: 8.5pt";
            this.txtCoveredLastName2.Text = null;
            this.txtCoveredLastName2.Top = 8.3125F;
            this.txtCoveredLastName2.Width = 0.625F;
            // 
            // txtCoveredLastName3
            // 
            this.txtCoveredLastName3.Height = 0.1770833F;
            this.txtCoveredLastName3.Left = 1.125F;
            this.txtCoveredLastName3.Name = "txtCoveredLastName3";
            this.txtCoveredLastName3.Style = "font-size: 8.5pt";
            this.txtCoveredLastName3.Text = null;
            this.txtCoveredLastName3.Top = 8.6875F;
            this.txtCoveredLastName3.Width = 0.625F;
            // 
            // txtCoveredLastName4
            // 
            this.txtCoveredLastName4.Height = 0.1770833F;
            this.txtCoveredLastName4.Left = 1.125F;
            this.txtCoveredLastName4.Name = "txtCoveredLastName4";
            this.txtCoveredLastName4.Style = "font-size: 8.5pt";
            this.txtCoveredLastName4.Text = null;
            this.txtCoveredLastName4.Top = 9.0625F;
            this.txtCoveredLastName4.Width = 0.625F;
            // 
            // txtCoveredLastName5
            // 
            this.txtCoveredLastName5.Height = 0.1770833F;
            this.txtCoveredLastName5.Left = 1.125F;
            this.txtCoveredLastName5.Name = "txtCoveredLastName5";
            this.txtCoveredLastName5.Style = "font-size: 8.5pt";
            this.txtCoveredLastName5.Text = null;
            this.txtCoveredLastName5.Top = 9.4375F;
            this.txtCoveredLastName5.Width = 0.625F;
            // 
            // txtCoveredLastName6
            // 
            this.txtCoveredLastName6.Height = 0.1770833F;
            this.txtCoveredLastName6.Left = 1.125F;
            this.txtCoveredLastName6.Name = "txtCoveredLastName6";
            this.txtCoveredLastName6.Style = "font-size: 8.5pt";
            this.txtCoveredLastName6.Text = null;
            this.txtCoveredLastName6.Top = 9.8125F;
            this.txtCoveredLastName6.Width = 0.625F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 0.875F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 7.9375F;
            this.txtCoveredMiddle1.Width = 0.1875F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 0.875F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 8.3125F;
            this.txtCoveredMiddle2.Width = 0.1875F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 0.875F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 8.6875F;
            this.txtCoveredMiddle3.Width = 0.1875F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 0.875F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 9.0625F;
            this.txtCoveredMiddle4.Width = 0.1875F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 0.875F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 9.4375F;
            this.txtCoveredMiddle5.Width = 0.1875F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 0.875F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 9.8125F;
            this.txtCoveredMiddle6.Width = 0.1875F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.083333F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Style = "font-size: 8.5pt";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 5.135417F;
            this.txtMiddle.Width = 0.1875F;
            // 
            // txtLast
            // 
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 1.395833F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Style = "font-size: 8.5pt";
            this.txtLast.Text = null;
            this.txtLast.Top = 5.135417F;
            this.txtLast.Width = 0.9375F;
            // 
            // rpt1095C2018PortraitPage1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.472222F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStartMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredIndividuals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerReturnAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
    }
}
