﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptACAHealthCareDependents.
	/// </summary>
	public partial class srptACAHealthCareDependents : FCSectionReport
	{
		public srptACAHealthCareDependents()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptACAHealthCareDependents InstancePtr
		{
			get
			{
				return (srptACAHealthCareDependents)Sys.GetInstance(typeof(srptACAHealthCareDependents));
			}
		}

		protected srptACAHealthCareDependents _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptACAHealthCareDependents	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cACAEmployeeSetup theEmployee;
		int intCurrentRecord;

		public cACAEmployeeSetup EmployeeSetup
		{
			set
			{
				theEmployee = value;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = intCurrentRecord > theEmployee.Dependents.Count;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intCurrentRecord = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intCurrentRecord <= theEmployee.Dependents.Count)
			{
				cACAEmployeeDependent dep;
				string strName = "";
				dep = theEmployee.Dependents[intCurrentRecord];
				strName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(dep.FirstName + " " + dep.MiddleName) + " " + dep.LastName) + " " + dep.Suffix);
				txtName.Text = strName;
				string strSSN = "";
				strSSN = dep.SSN.Replace("-", "");
				if (strSSN.Length == 9)
				{
					txtSSN.Text = "XXX-XX-" + Strings.Right(strSSN, 4);
				}
				else
				{
					txtSSN.Text = dep.SSN;
				}
				txtDOB.Text = dep.DateOfBirth;
				txtTerminationDate.Text = dep.HealthCareTerminationDate;
				int x;
				bool boolIsCovered = false;
				for (x = 1; x <= 12; x++)
				{
					boolIsCovered = dep.GetIsCoveredForMonth(x);
					switch (x)
					{
						case 1:
							{
								chkJan.Checked = boolIsCovered;
								break;
							}
						case 2:
							{
								chkFeb.Checked = boolIsCovered;
								break;
							}
						case 3:
							{
								chkMar.Checked = boolIsCovered;
								break;
							}
						case 4:
							{
								chkApr.Checked = boolIsCovered;
								break;
							}
						case 5:
							{
								chkMay.Checked = boolIsCovered;
								break;
							}
						case 6:
							{
								chkJun.Checked = boolIsCovered;
								break;
							}
						case 7:
							{
								chkJul.Checked = boolIsCovered;
								break;
							}
						case 8:
							{
								chkAug.Checked = boolIsCovered;
								break;
							}
						case 9:
							{
								chkSep.Checked = boolIsCovered;
								break;
							}
						case 10:
							{
								chkOct.Checked = boolIsCovered;
								break;
							}
						case 11:
							{
								chkNov.Checked = boolIsCovered;
								break;
							}
						case 12:
							{
								chkDec.Checked = boolIsCovered;
								break;
							}
					}
					//end switch
				}
				// x
				intCurrentRecord += 1;
			}
		}

		private void srptACAHealthCareDependents_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptACAHealthCareDependents properties;
			//srptACAHealthCareDependents.Caption	= "ActiveReport1";
			//srptACAHealthCareDependents.Left	= 0;
			//srptACAHealthCareDependents.Top	= 0;
			//srptACAHealthCareDependents.Width	= 17070;
			//srptACAHealthCareDependents.Height	= 7335;
			//srptACAHealthCareDependents.StartUpPosition	= 3;
			//srptACAHealthCareDependents.SectionData	= "srptACAHealthCareDependents.dsx":0000;
			//End Unmaped Properties
		}
	}
}
