﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2016PortraitPage1.
	/// </summary>
	partial class rpt1095B2018PortraitPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095B2018PortraitPage1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOriginOfPolicy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployerName,
            this.txtEmployerReturnAddress,
            this.txtEmployerCityStateZip,
            this.txtEmployeeName,
            this.txtEmployeeAddress,
            this.txtEmployeeCityStateZip,
            this.txtCoveredName1,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtName,
            this.txtSSN,
            this.txtAddress,
            this.txtCity,
            this.txtState,
            this.txtOriginOfPolicy,
            this.txtPostalCode,
            this.txtProviderName,
            this.txtProviderAddress,
            this.txtProviderCity,
            this.txtProviderState,
            this.txtEIN,
            this.txtProviderTelephone,
            this.txtProviderPostalCode,
            this.txtMiddle,
            this.txtLast,
            this.txtCoveredLast1,
            this.txtCoveredLast2,
            this.txtCoveredLast3,
            this.txtCoveredLast4,
            this.txtCoveredLast5,
            this.txtCoveredLast6,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6});
            this.Detail.Height = 9.989583F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.Height = 0.1770833F;
            this.txtEmployerName.Left = 0.234F;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Style = "font-size: 8.5pt";
            this.txtEmployerName.Text = null;
            this.txtEmployerName.Top = 0.297F;
            this.txtEmployerName.Width = 3.083333F;
            // 
            // txtEmployerReturnAddress
            // 
            this.txtEmployerReturnAddress.Height = 0.1770833F;
            this.txtEmployerReturnAddress.Left = 0.234F;
            this.txtEmployerReturnAddress.Name = "txtEmployerReturnAddress";
            this.txtEmployerReturnAddress.Style = "font-size: 8.5pt";
            this.txtEmployerReturnAddress.Text = null;
            this.txtEmployerReturnAddress.Top = 0.4844999F;
            this.txtEmployerReturnAddress.Width = 3.083333F;
            // 
            // txtEmployerCityStateZip
            // 
            this.txtEmployerCityStateZip.Height = 0.1770833F;
            this.txtEmployerCityStateZip.Left = 0.234F;
            this.txtEmployerCityStateZip.Name = "txtEmployerCityStateZip";
            this.txtEmployerCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployerCityStateZip.Text = null;
            this.txtEmployerCityStateZip.Top = 0.6719999F;
            this.txtEmployerCityStateZip.Width = 3.083333F;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Height = 0.1770833F;
            this.txtEmployeeName.Left = 0.234F;
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Style = "font-size: 8.5pt";
            this.txtEmployeeName.Text = null;
            this.txtEmployeeName.Top = 2.297F;
            this.txtEmployeeName.Width = 3.083333F;
            // 
            // txtEmployeeAddress
            // 
            this.txtEmployeeAddress.Height = 0.1770833F;
            this.txtEmployeeAddress.Left = 0.234F;
            this.txtEmployeeAddress.Name = "txtEmployeeAddress";
            this.txtEmployeeAddress.Style = "font-size: 8.5pt";
            this.txtEmployeeAddress.Text = null;
            this.txtEmployeeAddress.Top = 2.4845F;
            this.txtEmployeeAddress.Width = 3.083333F;
            // 
            // txtEmployeeCityStateZip
            // 
            this.txtEmployeeCityStateZip.Height = 0.1770833F;
            this.txtEmployeeCityStateZip.Left = 0.234F;
            this.txtEmployeeCityStateZip.Name = "txtEmployeeCityStateZip";
            this.txtEmployeeCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployeeCityStateZip.Text = null;
            this.txtEmployeeCityStateZip.Top = 2.672F;
            this.txtEmployeeCityStateZip.Width = 3.083333F;
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.CanGrow = false;
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.02566668F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Style = "font-size: 8.5pt";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 7.901169F;
            this.txtCoveredName1.Width = 0.75F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 1.879833F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Style = "font-size: 8.5pt";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 7.901169F;
            this.txtCoveredSSN1.Width = 0.8333333F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 2.7965F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Style = "font-size: 8.5pt";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 7.901169F;
            this.txtCoveredDOB1.Width = 0.75F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.CanGrow = false;
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.02566668F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Style = "font-size: 8.5pt";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 8.27617F;
            this.txtCoveredName2.Width = 0.75F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 1.879833F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Style = "font-size: 8.5pt";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 8.27617F;
            this.txtCoveredSSN2.Width = 0.8333333F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 2.7965F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Style = "font-size: 8.5pt";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 8.27617F;
            this.txtCoveredDOB2.Width = 0.75F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.CanGrow = false;
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.02566668F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Style = "font-size: 8.5pt";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 8.65117F;
            this.txtCoveredName3.Width = 0.75F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 1.879833F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Style = "font-size: 8.5pt";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 8.65117F;
            this.txtCoveredSSN3.Width = 0.8333333F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 2.7965F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Style = "font-size: 8.5pt";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 8.65117F;
            this.txtCoveredDOB3.Width = 0.75F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.CanGrow = false;
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.02566668F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Style = "font-size: 8.5pt";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 9.02617F;
            this.txtCoveredName4.Width = 0.75F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 1.879833F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Style = "font-size: 8.5pt";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 9.02617F;
            this.txtCoveredSSN4.Width = 0.8333333F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 2.7965F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Style = "font-size: 8.5pt";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 9.02617F;
            this.txtCoveredDOB4.Width = 0.75F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.CanGrow = false;
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.02566668F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Style = "font-size: 8.5pt";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 9.40117F;
            this.txtCoveredName5.Width = 0.75F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 1.879833F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Style = "font-size: 8.5pt";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 9.40117F;
            this.txtCoveredSSN5.Width = 0.8333333F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 2.7965F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Style = "font-size: 8.5pt";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 9.40117F;
            this.txtCoveredDOB5.Width = 0.75F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.02566668F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Style = "font-size: 8.5pt";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 9.77617F;
            this.txtCoveredName6.Width = 0.75F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 1.879833F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Style = "font-size: 8.5pt";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 9.77617F;
            this.txtCoveredSSN6.Width = 0.8333333F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 2.7965F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Style = "font-size: 8.5pt";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 9.77617F;
            this.txtCoveredDOB6.Width = 0.75F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 3.609F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = null;
            this.txtCoveredAll12_1.Top = 7.901169F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 3.970111F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = null;
            this.txtCoveredMonth1_1.Top = 7.901169F;
            this.txtCoveredMonth1_1.Width = 0.1875F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 4.275667F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = null;
            this.txtCoveredMonth2_1.Top = 7.901169F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 4.588167F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = null;
            this.txtCoveredMonth3_1.Top = 7.901169F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 4.879834F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = null;
            this.txtCoveredMonth4_1.Top = 7.901169F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 5.161084F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = null;
            this.txtCoveredMonth5_1.Top = 7.901169F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 5.442334F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = null;
            this.txtCoveredMonth6_1.Top = 7.901169F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 5.775667F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = null;
            this.txtCoveredMonth7_1.Top = 7.901169F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 6.088167F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = null;
            this.txtCoveredMonth8_1.Top = 7.901169F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 6.369417F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = null;
            this.txtCoveredMonth9_1.Top = 7.901169F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 6.661084F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = null;
            this.txtCoveredMonth10_1.Top = 7.901169F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 6.942334F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = null;
            this.txtCoveredMonth11_1.Top = 7.901169F;
            this.txtCoveredMonth11_1.Width = 0.25F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 7.254834F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = null;
            this.txtCoveredMonth12_1.Top = 7.901169F;
            this.txtCoveredMonth12_1.Width = 0.1875F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 3.609F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = null;
            this.txtCoveredAll12_2.Top = 8.27617F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 3.970111F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = null;
            this.txtCoveredMonth1_2.Top = 8.27617F;
            this.txtCoveredMonth1_2.Width = 0.1875F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 4.275667F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = null;
            this.txtCoveredMonth2_2.Top = 8.27617F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 4.588167F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = null;
            this.txtCoveredMonth3_2.Top = 8.27617F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 4.879834F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = null;
            this.txtCoveredMonth4_2.Top = 8.27617F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 5.161084F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = null;
            this.txtCoveredMonth5_2.Top = 8.27617F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 5.442334F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = null;
            this.txtCoveredMonth6_2.Top = 8.27617F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 5.775667F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = null;
            this.txtCoveredMonth7_2.Top = 8.27617F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 6.088167F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = null;
            this.txtCoveredMonth8_2.Top = 8.27617F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 6.369417F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = null;
            this.txtCoveredMonth9_2.Top = 8.27617F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 6.661084F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = null;
            this.txtCoveredMonth10_2.Top = 8.27617F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 6.942334F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = null;
            this.txtCoveredMonth11_2.Top = 8.27617F;
            this.txtCoveredMonth11_2.Width = 0.25F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 7.254834F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = null;
            this.txtCoveredMonth12_2.Top = 8.27617F;
            this.txtCoveredMonth12_2.Width = 0.1875F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 3.609F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = null;
            this.txtCoveredAll12_3.Top = 8.65117F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 3.970111F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = null;
            this.txtCoveredMonth1_3.Top = 8.65117F;
            this.txtCoveredMonth1_3.Width = 0.1875F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 4.275667F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = null;
            this.txtCoveredMonth2_3.Top = 8.65117F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 4.588167F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = null;
            this.txtCoveredMonth3_3.Top = 8.65117F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 4.879834F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = null;
            this.txtCoveredMonth4_3.Top = 8.65117F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 5.161084F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = null;
            this.txtCoveredMonth5_3.Top = 8.65117F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 5.442334F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = null;
            this.txtCoveredMonth6_3.Top = 8.65117F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 5.775667F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = null;
            this.txtCoveredMonth7_3.Top = 8.65117F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 6.088167F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = null;
            this.txtCoveredMonth8_3.Top = 8.65117F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 6.369417F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = null;
            this.txtCoveredMonth9_3.Top = 8.65117F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 6.661084F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = null;
            this.txtCoveredMonth10_3.Top = 8.65117F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 6.942334F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = null;
            this.txtCoveredMonth11_3.Top = 8.65117F;
            this.txtCoveredMonth11_3.Width = 0.25F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 7.254834F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = null;
            this.txtCoveredMonth12_3.Top = 8.65117F;
            this.txtCoveredMonth12_3.Width = 0.1875F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 3.609F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = null;
            this.txtCoveredAll12_4.Top = 9.02617F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 3.970111F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = null;
            this.txtCoveredMonth1_4.Top = 9.02617F;
            this.txtCoveredMonth1_4.Width = 0.1875F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 4.275667F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = null;
            this.txtCoveredMonth2_4.Top = 9.02617F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 4.588167F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = null;
            this.txtCoveredMonth3_4.Top = 9.02617F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 4.879834F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = null;
            this.txtCoveredMonth4_4.Top = 9.02617F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 5.161084F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = null;
            this.txtCoveredMonth5_4.Top = 9.02617F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 5.442334F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = null;
            this.txtCoveredMonth6_4.Top = 9.02617F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 5.775667F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = null;
            this.txtCoveredMonth7_4.Top = 9.02617F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 6.088167F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = null;
            this.txtCoveredMonth8_4.Top = 9.02617F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 6.372889F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = null;
            this.txtCoveredMonth9_4.Top = 9.02617F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 6.664556F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = null;
            this.txtCoveredMonth10_4.Top = 9.02617F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 6.942334F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = null;
            this.txtCoveredMonth11_4.Top = 9.02617F;
            this.txtCoveredMonth11_4.Width = 0.25F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 7.254834F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = null;
            this.txtCoveredMonth12_4.Top = 9.02617F;
            this.txtCoveredMonth12_4.Width = 0.1875F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 3.609F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = null;
            this.txtCoveredAll12_5.Top = 9.40117F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 3.970111F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = null;
            this.txtCoveredMonth1_5.Top = 9.40117F;
            this.txtCoveredMonth1_5.Width = 0.1875F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 4.275667F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = null;
            this.txtCoveredMonth2_5.Top = 9.40117F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 4.588167F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = null;
            this.txtCoveredMonth3_5.Top = 9.40117F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 4.879834F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = null;
            this.txtCoveredMonth4_5.Top = 9.40117F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 5.161084F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = null;
            this.txtCoveredMonth5_5.Top = 9.40117F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 5.442334F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = null;
            this.txtCoveredMonth6_5.Top = 9.40117F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 5.775667F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = null;
            this.txtCoveredMonth7_5.Top = 9.40117F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 6.088167F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = null;
            this.txtCoveredMonth8_5.Top = 9.40117F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 6.372889F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = null;
            this.txtCoveredMonth9_5.Top = 9.40117F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 6.664556F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = null;
            this.txtCoveredMonth10_5.Top = 9.40117F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 6.942334F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = null;
            this.txtCoveredMonth11_5.Top = 9.40117F;
            this.txtCoveredMonth11_5.Width = 0.25F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 7.254834F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = null;
            this.txtCoveredMonth12_5.Top = 9.40117F;
            this.txtCoveredMonth12_5.Width = 0.1875F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 3.609F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = null;
            this.txtCoveredAll12_6.Top = 9.77617F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 3.970111F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = null;
            this.txtCoveredMonth1_6.Top = 9.77617F;
            this.txtCoveredMonth1_6.Width = 0.1875F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 4.275667F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = null;
            this.txtCoveredMonth2_6.Top = 9.77617F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 4.588167F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = null;
            this.txtCoveredMonth3_6.Top = 9.77617F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 4.879834F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = null;
            this.txtCoveredMonth4_6.Top = 9.77617F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 5.161084F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = null;
            this.txtCoveredMonth5_6.Top = 9.77617F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 5.442334F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = null;
            this.txtCoveredMonth6_6.Top = 9.77617F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 5.775667F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = null;
            this.txtCoveredMonth7_6.Top = 9.77617F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 6.088167F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = null;
            this.txtCoveredMonth8_6.Top = 9.77617F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 6.372889F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = null;
            this.txtCoveredMonth9_6.Top = 9.77617F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 6.664556F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = null;
            this.txtCoveredMonth10_6.Top = 9.77617F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 6.942334F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = null;
            this.txtCoveredMonth11_6.Top = 9.77617F;
            this.txtCoveredMonth11_6.Width = 0.25F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 7.254834F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = null;
            this.txtCoveredMonth12_6.Top = 9.77617F;
            this.txtCoveredMonth12_6.Width = 0.1875F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.02566668F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-size: 8.5pt";
            this.txtName.Text = null;
            this.txtName.Top = 5.394225F;
            this.txtName.Width = 1.3125F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 4.109F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Style = "font-size: 8.5pt";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 5.394225F;
            this.txtSSN.Width = 1.5F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.02566668F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 8.5pt";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 5.630336F;
            this.txtAddress.Width = 2.5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 2.629833F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-size: 8.5pt";
            this.txtCity.Text = null;
            this.txtCity.Top = 5.630336F;
            this.txtCity.Width = 1.333333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 4.109F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-size: 8.5pt";
            this.txtState.Text = null;
            this.txtState.Top = 5.630336F;
            this.txtState.Width = 1.5F;
            // 
            // txtOriginOfPolicy
            // 
            this.txtOriginOfPolicy.Height = 0.1770833F;
            this.txtOriginOfPolicy.Left = 3.7965F;
            this.txtOriginOfPolicy.Name = "txtOriginOfPolicy";
            this.txtOriginOfPolicy.Text = "X";
            this.txtOriginOfPolicy.Top = 5.859503F;
            this.txtOriginOfPolicy.Width = 0.25F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 5.6715F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Style = "font-size: 8.5pt";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 5.630336F;
            this.txtPostalCode.Width = 1.6875F;
            // 
            // txtProviderName
            // 
            this.txtProviderName.Height = 0.1770833F;
            this.txtProviderName.Left = 0.02566668F;
            this.txtProviderName.Name = "txtProviderName";
            this.txtProviderName.Style = "font-size: 8.5pt";
            this.txtProviderName.Text = null;
            this.txtProviderName.Top = 6.901169F;
            this.txtProviderName.Width = 4F;
            // 
            // txtProviderAddress
            // 
            this.txtProviderAddress.Height = 0.1770833F;
            this.txtProviderAddress.Left = 0.02566668F;
            this.txtProviderAddress.Name = "txtProviderAddress";
            this.txtProviderAddress.Style = "font-size: 8.5pt";
            this.txtProviderAddress.Text = null;
            this.txtProviderAddress.Top = 7.109503F;
            this.txtProviderAddress.Width = 2.583333F;
            // 
            // txtProviderCity
            // 
            this.txtProviderCity.Height = 0.1770833F;
            this.txtProviderCity.Left = 2.629833F;
            this.txtProviderCity.Name = "txtProviderCity";
            this.txtProviderCity.Style = "font-size: 8.5pt";
            this.txtProviderCity.Text = null;
            this.txtProviderCity.Top = 7.109503F;
            this.txtProviderCity.Width = 1.333333F;
            // 
            // txtProviderState
            // 
            this.txtProviderState.Height = 0.1770833F;
            this.txtProviderState.Left = 4.109F;
            this.txtProviderState.Name = "txtProviderState";
            this.txtProviderState.Style = "font-size: 8.5pt";
            this.txtProviderState.Text = null;
            this.txtProviderState.Top = 7.109503F;
            this.txtProviderState.Width = 1.5F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 4.109F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Style = "font-size: 8.5pt";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 6.901169F;
            this.txtEIN.Width = 1.5F;
            // 
            // txtProviderTelephone
            // 
            this.txtProviderTelephone.Height = 0.1770833F;
            this.txtProviderTelephone.Left = 5.6715F;
            this.txtProviderTelephone.Name = "txtProviderTelephone";
            this.txtProviderTelephone.Style = "font-size: 8.5pt";
            this.txtProviderTelephone.Text = null;
            this.txtProviderTelephone.Top = 6.901169F;
            this.txtProviderTelephone.Width = 1.75F;
            // 
            // txtProviderPostalCode
            // 
            this.txtProviderPostalCode.Height = 0.1770833F;
            this.txtProviderPostalCode.Left = 5.6715F;
            this.txtProviderPostalCode.Name = "txtProviderPostalCode";
            this.txtProviderPostalCode.Style = "font-size: 8.5pt";
            this.txtProviderPostalCode.Text = null;
            this.txtProviderPostalCode.Top = 7.109503F;
            this.txtProviderPostalCode.Width = 1.75F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.CanGrow = false;
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.4215F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Style = "font-size: 8.5pt";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 5.390753F;
            this.txtMiddle.Width = 1.1875F;
            // 
            // txtLast
            // 
            this.txtLast.CanGrow = false;
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 2.70275F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Style = "font-size: 8.5pt";
            this.txtLast.Text = null;
            this.txtLast.Top = 5.390753F;
            this.txtLast.Width = 1.3125F;
            // 
            // txtCoveredLast1
            // 
            this.txtCoveredLast1.CanGrow = false;
            this.txtCoveredLast1.Height = 0.1770833F;
            this.txtCoveredLast1.Left = 1.109F;
            this.txtCoveredLast1.Name = "txtCoveredLast1";
            this.txtCoveredLast1.Style = "font-size: 8.5pt";
            this.txtCoveredLast1.Text = null;
            this.txtCoveredLast1.Top = 7.901169F;
            this.txtCoveredLast1.Width = 0.625F;
            // 
            // txtCoveredLast2
            // 
            this.txtCoveredLast2.CanGrow = false;
            this.txtCoveredLast2.Height = 0.1770833F;
            this.txtCoveredLast2.Left = 1.109F;
            this.txtCoveredLast2.Name = "txtCoveredLast2";
            this.txtCoveredLast2.Style = "font-size: 8.5pt";
            this.txtCoveredLast2.Text = null;
            this.txtCoveredLast2.Top = 8.27617F;
            this.txtCoveredLast2.Width = 0.625F;
            // 
            // txtCoveredLast3
            // 
            this.txtCoveredLast3.CanGrow = false;
            this.txtCoveredLast3.Height = 0.1770833F;
            this.txtCoveredLast3.Left = 1.109F;
            this.txtCoveredLast3.Name = "txtCoveredLast3";
            this.txtCoveredLast3.Style = "font-size: 8.5pt";
            this.txtCoveredLast3.Text = null;
            this.txtCoveredLast3.Top = 8.65117F;
            this.txtCoveredLast3.Width = 0.625F;
            // 
            // txtCoveredLast4
            // 
            this.txtCoveredLast4.CanGrow = false;
            this.txtCoveredLast4.Height = 0.1770833F;
            this.txtCoveredLast4.Left = 1.109F;
            this.txtCoveredLast4.Name = "txtCoveredLast4";
            this.txtCoveredLast4.Style = "font-size: 8.5pt";
            this.txtCoveredLast4.Text = null;
            this.txtCoveredLast4.Top = 9.02617F;
            this.txtCoveredLast4.Width = 0.625F;
            // 
            // txtCoveredLast5
            // 
            this.txtCoveredLast5.CanGrow = false;
            this.txtCoveredLast5.Height = 0.1770833F;
            this.txtCoveredLast5.Left = 1.109F;
            this.txtCoveredLast5.Name = "txtCoveredLast5";
            this.txtCoveredLast5.Style = "font-size: 8.5pt";
            this.txtCoveredLast5.Text = null;
            this.txtCoveredLast5.Top = 9.40117F;
            this.txtCoveredLast5.Width = 0.625F;
            // 
            // txtCoveredLast6
            // 
            this.txtCoveredLast6.CanGrow = false;
            this.txtCoveredLast6.Height = 0.1770833F;
            this.txtCoveredLast6.Left = 1.109F;
            this.txtCoveredLast6.Name = "txtCoveredLast6";
            this.txtCoveredLast6.Style = "font-size: 8.5pt";
            this.txtCoveredLast6.Text = null;
            this.txtCoveredLast6.Top = 9.77617F;
            this.txtCoveredLast6.Width = 0.625F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.CanGrow = false;
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 0.8798333F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 7.901169F;
            this.txtCoveredMiddle1.Width = 0.1875F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.CanGrow = false;
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 0.8798333F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 8.27617F;
            this.txtCoveredMiddle2.Width = 0.1875F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.CanGrow = false;
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 0.8798333F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 8.65117F;
            this.txtCoveredMiddle3.Width = 0.1875F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.CanGrow = false;
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 0.8798333F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 9.02617F;
            this.txtCoveredMiddle4.Width = 0.1875F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.CanGrow = false;
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 0.8798333F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 9.40117F;
            this.txtCoveredMiddle5.Width = 0.1875F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.CanGrow = false;
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 0.8798333F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 9.77617F;
            this.txtCoveredMiddle6.Width = 0.1875F;
            // 
            // rpt1095B2018PortraitPage1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.46875F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerReturnAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginOfPolicy;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderTelephone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
    }
}
