﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt1094C2016MonthlyDetail.
	/// </summary>
	public partial class srpt1094C2016MonthlyDetail : FCSectionReport
	{
		public srpt1094C2016MonthlyDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srpt1094C2016MonthlyDetail InstancePtr
		{
			get
			{
				return (srpt1094C2016MonthlyDetail)Sys.GetInstance(typeof(srpt1094C2016MonthlyDetail));
			}
		}

		protected srpt1094C2016MonthlyDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srpt1094C2016MonthlyDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c1094C theReport;
		private bool boolTestPrint;
		private bool boolFirstPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !boolFirstPage;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolFirstPage = true;
			theReport = (this.ParentReport as rpt1094C2016).ReportModel;
			boolTestPrint = (this.ParentReport as rpt1094C2016).TestPrint;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			bool boolAll12MinimumOffer = false;
			bool boolAll12FullTimeCount = false;
			bool boolAll12EmployeeCount = false;
			bool boolAll12InGroup = false;
			bool boolAll124980H = false;
			bool boolLastMinimumOffer = false;
			int intLastFullTimeCount = 0;
			int intLastEmployeeCount = 0;
			bool boolLastInGroup = false;
			string strLast4980H = "";
			cALEMonthlyInformation minfo;
			if (!boolTestPrint)
			{
				boolAll12MinimumOffer = true;
				boolAll12FullTimeCount = true;
				boolAll12EmployeeCount = true;
				boolAll12InGroup = true;
				boolAll124980H = true;
				minfo = theReport.GetMonthlyInformation(1);
				boolLastMinimumOffer = minfo.MinimumEssentialCoverageOffer;
				intLastFullTimeCount = minfo.FullTimeEmployeeCount;
				intLastEmployeeCount = minfo.TotalEmployeeCount;
				boolLastInGroup = minfo.AggregatedGroup;
				strLast4980H = minfo.Section4980HTransitionRelief;
				for (x = 2; x <= 12; x++)
				{
					minfo = theReport.GetMonthlyInformation(x);
					if (minfo.MinimumEssentialCoverageOffer != boolLastMinimumOffer)
					{
						boolAll12MinimumOffer = false;
					}
					if (minfo.FullTimeEmployeeCount != intLastFullTimeCount)
					{
						boolAll12FullTimeCount = false;
					}
					if (minfo.TotalEmployeeCount != intLastEmployeeCount)
					{
						boolAll12EmployeeCount = false;
					}
					if (minfo.AggregatedGroup != boolLastInGroup)
					{
						boolAll12InGroup = false;
					}
					if (minfo.Section4980HTransitionRelief != strLast4980H)
					{
						boolAll124980H = false;
					}
				}
				// x
				if (boolAll12MinimumOffer)
				{
					if (boolLastMinimumOffer)
					{
						txtAll12MinimumOfferYes.Text = "X";
						txtAll12MinimumOfferNo.Text = "";
					}
					else
					{
						txtAll12MinimumOfferYes.Text = "";
						txtAll12MinimumOfferNo.Text = "X";
					}
				}
				else
				{
					txtAll12MinimumOfferNo.Text = "";
					txtAll12MinimumOfferYes.Text = "";
				}
				if (boolAll12FullTimeCount)
				{
					txtAll12FullTimeCount.Text = Strings.Format(intLastFullTimeCount, "#,##0");
				}
				else
				{
					txtAll12FullTimeCount.Text = "";
				}
				if (boolAll12EmployeeCount)
				{
					txtAll12EmployeeCount.Text = Strings.Format(intLastEmployeeCount, "#,##0");
				}
				else
				{
					txtAll12EmployeeCount.Text = "";
				}
				if (boolAll12InGroup)
				{
					if (boolLastInGroup)
					{
						txtAll12InGroup.Text = "X";
					}
					else
					{
						txtAll12InGroup.Text = "";
					}
				}
				else
				{
					txtAll12InGroup.Text = "";
				}
				if (boolAll124980H)
				{
					txtAll12Months4980H.Text = strLast4980H;
				}
				else
				{
					txtAll12Months4980H.Text = "";
				}
				for (x = 1; x <= 12; x++)
				{
					minfo = theReport.GetMonthlyInformation(x);
					if (!boolAll12MinimumOffer)
					{
						if (minfo.MinimumEssentialCoverageOffer)
						{
							(this.Detail.Controls["txtMinimumOfferYes" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
							(this.Detail.Controls["txtMinimumOfferNo" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						}
						else
						{
							(this.Detail.Controls["txtMinimumOfferYes" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							(this.Detail.Controls["txtMinimumOfferNo" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
						}
					}
					else
					{
						(this.Detail.Controls["txtMinimumOfferYes" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(this.Detail.Controls["txtMinimumOfferNo" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
					if (!boolAll12FullTimeCount)
					{
						(this.Detail.Controls["txtFullCount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(minfo.FullTimeEmployeeCount, "#,##0");
					}
					else
					{
						(this.Detail.Controls["txtFullCount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
					if (!boolAll12EmployeeCount)
					{
						(this.Detail.Controls["txtEmployeeCount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(minfo.TotalEmployeeCount, "#,##0");
					}
					else
					{
						(this.Detail.Controls["txtEmployeeCount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
					if (!boolAll12InGroup)
					{
						if (minfo.AggregatedGroup)
						{
							(this.Detail.Controls["txtInGroup" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
						}
						else
						{
							(this.Detail.Controls["txtInGroup" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						}
					}
					else
					{
						(this.Detail.Controls["txtInGroup" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
					if (!boolAll124980H)
					{
						(this.Detail.Controls["txt4980H" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = minfo.Section4980HTransitionRelief;
					}
					else
					{
						(this.Detail.Controls["txt4980H" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
				}
				// x
			}
			else
			{
				txtAll12EmployeeCount.Text = "999";
				txtAll12FullTimeCount.Text = "999";
				txtAll12InGroup.Text = "X";
				txtAll12MinimumOfferNo.Text = "X";
				txtAll12MinimumOfferYes.Text = "X";
				txtAll12Months4980H.Text = "4980H";
				for (x = 1; x <= 12; x++)
				{
					(this.Detail.Controls["txtEmployeeCount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "999";
					(this.Detail.Controls["txtFullCount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "999";
					(this.Detail.Controls["txtInGroup" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txtMinimumOfferYes" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txtMinimumOfferNo" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txtMinimumOfferYes" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txt4980H" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "4980H";
				}
				// x
			}
			boolFirstPage = false;
		}

		private void srpt1094C2016MonthlyDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srpt1094C2016MonthlyDetail properties;
			//srpt1094C2016MonthlyDetail.Caption	= "ActiveReport1";
			//srpt1094C2016MonthlyDetail.Left	= 0;
			//srpt1094C2016MonthlyDetail.Top	= 0;
			//srpt1094C2016MonthlyDetail.Width	= 20025;
			//srpt1094C2016MonthlyDetail.Height	= 10575;
			//srpt1094C2016MonthlyDetail.StartUpPosition	= 3;
			//srpt1094C2016MonthlyDetail.SectionData	= "srpt1094C2016MonthlyDetail.dsx":0000;
			//End Unmaped Properties
		}
	}
}
