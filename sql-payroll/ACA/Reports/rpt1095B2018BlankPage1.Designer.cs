﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2017BlankPage1.
	/// </summary>
	partial class rpt1095B2018BlankPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095B2018BlankPage1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOriginOfPolicy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label165 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label166 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label167 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape46 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape70 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape72 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape73 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape74 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape75 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape76 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape79 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label188 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label189 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label190 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label191 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label192 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label193 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label194 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label195 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label196 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label197 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label198 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label199 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label200 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape82 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label202 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label204 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label205 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label211 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label213 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label214 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label215 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label216 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label218 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label220 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label221 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label222 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label223 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label224 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label225 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label226 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label227 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label228 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label229 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label230 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label231 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label232 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label233 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCoveredLast1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label191)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label192)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label195)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label197)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label198)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label199)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label202)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label207)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label208)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label213)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label214)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label215)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label216)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label218)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label219)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label221)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label222)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label223)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label224)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label225)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label226)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label227)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label228)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label229)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label233)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCoveredName1,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtName,
            this.txtSSN,
            this.txtAddress,
            this.txtCity,
            this.txtState,
            this.txtPostalCode,
            this.txtProviderName,
            this.txtProviderAddress,
            this.txtProviderCity,
            this.txtProviderState,
            this.txtProviderPostalCode,
            this.txtProviderTelephone,
            this.txtOriginOfPolicy,
            this.txtEIN,
            this.Line15,
            this.Line16,
            this.Line17,
            this.Line19,
            this.Line20,
            this.Line21,
            this.Line22,
            this.Line36,
            this.Label118,
            this.Label130,
            this.Label131,
            this.Label132,
            this.Label133,
            this.Label134,
            this.Label135,
            this.Label136,
            this.Label137,
            this.Label165,
            this.Label166,
            this.Label167,
            this.Label168,
            this.Label169,
            this.Label170,
            this.Label171,
            this.Label172,
            this.Label173,
            this.Label3,
            this.Label174,
            this.Label175,
            this.Label176,
            this.Label177,
            this.Label178,
            this.Label179,
            this.Label180,
            this.Label181,
            this.Label182,
            this.Line57,
            this.Line25,
            this.Line26,
            this.Line27,
            this.Line28,
            this.Line29,
            this.Line30,
            this.Line31,
            this.Line32,
            this.Line33,
            this.Line34,
            this.Line35,
            this.Line37,
            this.Line23,
            this.Line24,
            this.Line18,
            this.Image2,
            this.Label77,
            this.Label78,
            this.Line2,
            this.Label83,
            this.Label84,
            this.Line3,
            this.Line4,
            this.Label85,
            this.Label86,
            this.Line6,
            this.Label92,
            this.Label96,
            this.Shape2,
            this.Shape3,
            this.Label97,
            this.Label98,
            this.Line56,
            this.Label99,
            this.Label100,
            this.Label102,
            this.Line8,
            this.Line7,
            this.Shape4,
            this.Shape5,
            this.Shape6,
            this.Shape7,
            this.Shape8,
            this.Shape9,
            this.Shape10,
            this.Shape11,
            this.Shape12,
            this.Shape13,
            this.Shape14,
            this.Shape15,
            this.Shape16,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Shape20,
            this.Shape21,
            this.Shape22,
            this.Shape23,
            this.Shape24,
            this.Shape25,
            this.Shape26,
            this.Shape27,
            this.Shape28,
            this.Shape29,
            this.Shape30,
            this.Shape32,
            this.Shape31,
            this.Shape33,
            this.Shape34,
            this.Shape35,
            this.Shape36,
            this.Shape37,
            this.Shape38,
            this.Shape39,
            this.Shape40,
            this.Shape41,
            this.Shape42,
            this.Shape43,
            this.Shape44,
            this.Shape45,
            this.Shape46,
            this.Shape47,
            this.Shape48,
            this.Shape49,
            this.Shape50,
            this.Shape51,
            this.Shape52,
            this.Shape53,
            this.Shape54,
            this.Shape55,
            this.Shape56,
            this.Shape57,
            this.Shape58,
            this.Shape59,
            this.Shape60,
            this.Shape61,
            this.Shape62,
            this.Shape63,
            this.Shape64,
            this.Shape65,
            this.Shape66,
            this.Shape67,
            this.Shape68,
            this.Shape69,
            this.Shape70,
            this.Shape71,
            this.Shape72,
            this.Shape73,
            this.Shape74,
            this.Shape75,
            this.Shape76,
            this.Shape77,
            this.Shape78,
            this.Shape79,
            this.Shape80,
            this.Shape81,
            this.Label88,
            this.Label91,
            this.Label183,
            this.Line58,
            this.Line59,
            this.Label184,
            this.Label185,
            this.Label186,
            this.Line60,
            this.Label187,
            this.Label188,
            this.Line63,
            this.Line65,
            this.Line66,
            this.Line67,
            this.Line68,
            this.Line69,
            this.Line70,
            this.Line71,
            this.Label189,
            this.Line62,
            this.Label107,
            this.Label152,
            this.Label108,
            this.Label155,
            this.Label190,
            this.Label191,
            this.Label192,
            this.Label193,
            this.Label194,
            this.Label195,
            this.Line5,
            this.Label196,
            this.Label197,
            this.Label198,
            this.Label199,
            this.Label200,
            this.Label201,
            this.Shape82,
            this.Label202,
            this.Label203,
            this.Label204,
            this.Label205,
            this.Label206,
            this.Label207,
            this.Label208,
            this.Label209,
            this.Label210,
            this.Label211,
            this.Label212,
            this.Label213,
            this.Label214,
            this.Label215,
            this.Label216,
            this.Label217,
            this.Label218,
            this.Label219,
            this.Label220,
            this.Line64,
            this.Label221,
            this.Label222,
            this.Label223,
            this.Label224,
            this.Line61,
            this.Label225,
            this.Label226,
            this.Line72,
            this.Label227,
            this.Label228,
            this.Label229,
            this.Label230,
            this.Label231,
            this.Label232,
            this.Label233,
            this.Line73,
            this.txtCoveredLast1,
            this.txtCoveredLast2,
            this.txtCoveredLast3,
            this.txtCoveredLast4,
            this.txtCoveredLast5,
            this.txtCoveredLast6,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.Line75,
            this.Line76,
            this.txtMiddle,
            this.txtLast,
            this.Line74});
            this.Detail.Height = 7.6875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.CanGrow = false;
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.1666667F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 4.597917F;
            this.txtCoveredName1.Width = 1.041667F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 2.625F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 4.59375F;
            this.txtCoveredSSN1.Width = 1F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 3.791667F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 4.59375F;
            this.txtCoveredDOB1.Width = 0.9166667F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.CanGrow = false;
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.1666667F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 5.09375F;
            this.txtCoveredName2.Width = 1.041667F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 2.625F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 5.095833F;
            this.txtCoveredSSN2.Width = 1F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 3.791667F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 5.095833F;
            this.txtCoveredDOB2.Width = 0.9166667F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.CanGrow = false;
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.1666667F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 5.59375F;
            this.txtCoveredName3.Width = 1.041667F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 2.625F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 5.59375F;
            this.txtCoveredSSN3.Width = 1F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 3.791667F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 5.59375F;
            this.txtCoveredDOB3.Width = 0.9166667F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.CanGrow = false;
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.1666667F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 6.09375F;
            this.txtCoveredName4.Width = 1.041667F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 2.625F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 6.091667F;
            this.txtCoveredSSN4.Width = 1F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 3.791667F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 6.091667F;
            this.txtCoveredDOB4.Width = 0.9166667F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.CanGrow = false;
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.1666667F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 6.59375F;
            this.txtCoveredName5.Width = 1.041667F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 2.625F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 6.589583F;
            this.txtCoveredSSN5.Width = 1F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 3.791667F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 6.589583F;
            this.txtCoveredDOB5.Width = 0.9166667F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.CanGrow = false;
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.1666667F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 7.083333F;
            this.txtCoveredName6.Width = 1.041667F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 2.625F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 7.0875F;
            this.txtCoveredSSN6.Width = 1F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 3.791667F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 7.0875F;
            this.txtCoveredDOB6.Width = 0.9166667F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 4.916667F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = null;
            this.txtCoveredAll12_1.Top = 4.59375F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 5.395833F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = null;
            this.txtCoveredMonth1_1.Top = 4.59375F;
            this.txtCoveredMonth1_1.Width = 0.25F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 5.789583F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = null;
            this.txtCoveredMonth2_1.Top = 4.59375F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 6.183333F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = null;
            this.txtCoveredMonth3_1.Top = 4.59375F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 6.577083F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = null;
            this.txtCoveredMonth4_1.Top = 4.59375F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 6.970833F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = null;
            this.txtCoveredMonth5_1.Top = 4.59375F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 7.364583F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = null;
            this.txtCoveredMonth6_1.Top = 4.59375F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 7.758333F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = null;
            this.txtCoveredMonth7_1.Top = 4.59375F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 8.152083F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = null;
            this.txtCoveredMonth8_1.Top = 4.59375F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 8.545834F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = null;
            this.txtCoveredMonth9_1.Top = 4.59375F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 8.939584F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = null;
            this.txtCoveredMonth10_1.Top = 4.59375F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 9.333333F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = null;
            this.txtCoveredMonth11_1.Top = 4.59375F;
            this.txtCoveredMonth11_1.Width = 0.25F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 9.729167F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = null;
            this.txtCoveredMonth12_1.Top = 4.59375F;
            this.txtCoveredMonth12_1.Width = 0.1979167F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 4.916667F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = null;
            this.txtCoveredAll12_2.Top = 5.09375F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 5.395833F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = null;
            this.txtCoveredMonth1_2.Top = 5.095833F;
            this.txtCoveredMonth1_2.Width = 0.25F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 5.789583F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = null;
            this.txtCoveredMonth2_2.Top = 5.095833F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 6.183333F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = null;
            this.txtCoveredMonth3_2.Top = 5.095833F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 6.577083F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = null;
            this.txtCoveredMonth4_2.Top = 5.095833F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 6.970833F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = null;
            this.txtCoveredMonth5_2.Top = 5.095833F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 7.364583F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = null;
            this.txtCoveredMonth6_2.Top = 5.095833F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 7.758333F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = null;
            this.txtCoveredMonth7_2.Top = 5.095833F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 8.152083F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = null;
            this.txtCoveredMonth8_2.Top = 5.095833F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 8.545834F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = null;
            this.txtCoveredMonth9_2.Top = 5.095833F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 8.939584F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = null;
            this.txtCoveredMonth10_2.Top = 5.095833F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 9.333333F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = null;
            this.txtCoveredMonth11_2.Top = 5.095833F;
            this.txtCoveredMonth11_2.Width = 0.25F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 9.729167F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = null;
            this.txtCoveredMonth12_2.Top = 5.095833F;
            this.txtCoveredMonth12_2.Width = 0.1944444F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 4.916667F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = null;
            this.txtCoveredAll12_3.Top = 5.59375F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 5.395833F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = null;
            this.txtCoveredMonth1_3.Top = 5.59375F;
            this.txtCoveredMonth1_3.Width = 0.25F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 5.789583F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = null;
            this.txtCoveredMonth2_3.Top = 5.59375F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 6.183333F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = null;
            this.txtCoveredMonth3_3.Top = 5.59375F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 6.577083F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = null;
            this.txtCoveredMonth4_3.Top = 5.59375F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 6.970833F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = null;
            this.txtCoveredMonth5_3.Top = 5.59375F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 7.364583F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = null;
            this.txtCoveredMonth6_3.Top = 5.59375F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 7.758333F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = null;
            this.txtCoveredMonth7_3.Top = 5.59375F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 8.152083F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = null;
            this.txtCoveredMonth8_3.Top = 5.59375F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 8.545834F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = null;
            this.txtCoveredMonth9_3.Top = 5.59375F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 8.939584F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = null;
            this.txtCoveredMonth10_3.Top = 5.59375F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 9.333333F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = null;
            this.txtCoveredMonth11_3.Top = 5.59375F;
            this.txtCoveredMonth11_3.Width = 0.25F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 9.729167F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = null;
            this.txtCoveredMonth12_3.Top = 5.59375F;
            this.txtCoveredMonth12_3.Width = 0.1944444F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 4.916667F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = null;
            this.txtCoveredAll12_4.Top = 6.09375F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 5.395833F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = null;
            this.txtCoveredMonth1_4.Top = 6.091667F;
            this.txtCoveredMonth1_4.Width = 0.25F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 5.789583F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = null;
            this.txtCoveredMonth2_4.Top = 6.091667F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 6.183333F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = null;
            this.txtCoveredMonth3_4.Top = 6.091667F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 6.577083F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = null;
            this.txtCoveredMonth4_4.Top = 6.091667F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 6.970833F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = null;
            this.txtCoveredMonth5_4.Top = 6.091667F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 7.364583F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = null;
            this.txtCoveredMonth6_4.Top = 6.091667F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 7.758333F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = null;
            this.txtCoveredMonth7_4.Top = 6.091667F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 8.152083F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = null;
            this.txtCoveredMonth8_4.Top = 6.091667F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 8.545834F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = null;
            this.txtCoveredMonth9_4.Top = 6.091667F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 8.939584F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = null;
            this.txtCoveredMonth10_4.Top = 6.091667F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 9.333333F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = null;
            this.txtCoveredMonth11_4.Top = 6.091667F;
            this.txtCoveredMonth11_4.Width = 0.25F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 9.729167F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = null;
            this.txtCoveredMonth12_4.Top = 6.091667F;
            this.txtCoveredMonth12_4.Width = 0.1944444F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 4.916667F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = null;
            this.txtCoveredAll12_5.Top = 6.59375F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 5.395833F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = null;
            this.txtCoveredMonth1_5.Top = 6.589583F;
            this.txtCoveredMonth1_5.Width = 0.25F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 5.789583F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = null;
            this.txtCoveredMonth2_5.Top = 6.589583F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 6.183333F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = null;
            this.txtCoveredMonth3_5.Top = 6.589583F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 6.577083F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = null;
            this.txtCoveredMonth4_5.Top = 6.589583F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 6.970833F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = null;
            this.txtCoveredMonth5_5.Top = 6.589583F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 7.364583F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = null;
            this.txtCoveredMonth6_5.Top = 6.589583F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 7.758333F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = null;
            this.txtCoveredMonth7_5.Top = 6.589583F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 8.152083F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = null;
            this.txtCoveredMonth8_5.Top = 6.589583F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 8.545834F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = null;
            this.txtCoveredMonth9_5.Top = 6.589583F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 8.939584F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = null;
            this.txtCoveredMonth10_5.Top = 6.589583F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 9.333333F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = null;
            this.txtCoveredMonth11_5.Top = 6.589583F;
            this.txtCoveredMonth11_5.Width = 0.25F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 9.729167F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = null;
            this.txtCoveredMonth12_5.Top = 6.589583F;
            this.txtCoveredMonth12_5.Width = 0.1944444F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 4.916667F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = null;
            this.txtCoveredAll12_6.Top = 7.083333F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 5.395833F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = null;
            this.txtCoveredMonth1_6.Top = 7.0875F;
            this.txtCoveredMonth1_6.Width = 0.25F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 5.789583F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = null;
            this.txtCoveredMonth2_6.Top = 7.0875F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 6.183333F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = null;
            this.txtCoveredMonth3_6.Top = 7.0875F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 6.577083F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = null;
            this.txtCoveredMonth4_6.Top = 7.0875F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 6.970833F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = null;
            this.txtCoveredMonth5_6.Top = 7.0875F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 7.364583F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = null;
            this.txtCoveredMonth6_6.Top = 7.0875F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 7.758333F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = null;
            this.txtCoveredMonth7_6.Top = 7.0875F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 8.152083F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = null;
            this.txtCoveredMonth8_6.Top = 7.0875F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 8.545834F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = null;
            this.txtCoveredMonth9_6.Top = 7.0875F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 8.939584F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = null;
            this.txtCoveredMonth10_6.Top = 7.0875F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 9.333333F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = null;
            this.txtCoveredMonth11_6.Top = 7.0875F;
            this.txtCoveredMonth11_6.Width = 0.25F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 9.729167F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = null;
            this.txtCoveredMonth12_6.Top = 7.0875F;
            this.txtCoveredMonth12_6.Width = 0.1944444F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.08333334F;
            this.txtName.Name = "txtName";
            this.txtName.Text = null;
            this.txtName.Top = 1.28125F;
            this.txtName.Width = 1.6875F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 5.5F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 1.28125F;
            this.txtSSN.Width = 1.916667F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.08333334F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 1.625F;
            this.txtAddress.Width = 3.416667F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 3.583333F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Text = null;
            this.txtCity.Top = 1.625F;
            this.txtCity.Width = 1.75F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 5.5F;
            this.txtState.Name = "txtState";
            this.txtState.Text = null;
            this.txtState.Top = 1.625F;
            this.txtState.Width = 1.916667F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 7.666667F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 1.625F;
            this.txtPostalCode.Width = 2.083333F;
            // 
            // txtProviderName
            // 
            this.txtProviderName.Height = 0.1770833F;
            this.txtProviderName.Left = 0.08333334F;
            this.txtProviderName.Name = "txtProviderName";
            this.txtProviderName.Text = null;
            this.txtProviderName.Top = 3.291667F;
            this.txtProviderName.Width = 5.25F;
            // 
            // txtProviderAddress
            // 
            this.txtProviderAddress.Height = 0.1770833F;
            this.txtProviderAddress.Left = 0.08333334F;
            this.txtProviderAddress.Name = "txtProviderAddress";
            this.txtProviderAddress.Text = null;
            this.txtProviderAddress.Top = 3.604167F;
            this.txtProviderAddress.Width = 3.416667F;
            // 
            // txtProviderCity
            // 
            this.txtProviderCity.Height = 0.1770833F;
            this.txtProviderCity.Left = 3.583333F;
            this.txtProviderCity.Name = "txtProviderCity";
            this.txtProviderCity.Text = null;
            this.txtProviderCity.Top = 3.604167F;
            this.txtProviderCity.Width = 1.75F;
            // 
            // txtProviderState
            // 
            this.txtProviderState.Height = 0.1770833F;
            this.txtProviderState.Left = 5.5F;
            this.txtProviderState.Name = "txtProviderState";
            this.txtProviderState.Text = null;
            this.txtProviderState.Top = 3.604167F;
            this.txtProviderState.Width = 1.916667F;
            // 
            // txtProviderPostalCode
            // 
            this.txtProviderPostalCode.Height = 0.1770833F;
            this.txtProviderPostalCode.Left = 7.666667F;
            this.txtProviderPostalCode.Name = "txtProviderPostalCode";
            this.txtProviderPostalCode.Text = null;
            this.txtProviderPostalCode.Top = 3.604167F;
            this.txtProviderPostalCode.Width = 2.083333F;
            // 
            // txtProviderTelephone
            // 
            this.txtProviderTelephone.Height = 0.1770833F;
            this.txtProviderTelephone.Left = 7.666667F;
            this.txtProviderTelephone.Name = "txtProviderTelephone";
            this.txtProviderTelephone.Text = null;
            this.txtProviderTelephone.Top = 3.291667F;
            this.txtProviderTelephone.Width = 2.083333F;
            // 
            // txtOriginOfPolicy
            // 
            this.txtOriginOfPolicy.Height = 0.1770833F;
            this.txtOriginOfPolicy.Left = 5.145833F;
            this.txtOriginOfPolicy.Name = "txtOriginOfPolicy";
            this.txtOriginOfPolicy.Text = "X";
            this.txtOriginOfPolicy.Top = 1.958333F;
            this.txtOriginOfPolicy.Width = 0.25F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 5.5F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 3.291667F;
            this.txtEIN.Width = 1.916667F;
            // 
            // Line15
            // 
            this.Line15.Height = 0F;
            this.Line15.Left = 0F;
            this.Line15.LineWeight = 0F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 4.4375F;
            this.Line15.Width = 9.999306F;
            this.Line15.X1 = 0F;
            this.Line15.X2 = 9.999306F;
            this.Line15.Y1 = 4.4375F;
            this.Line15.Y2 = 4.4375F;
            // 
            // Line16
            // 
            this.Line16.Height = 0F;
            this.Line16.Left = 0F;
            this.Line16.LineWeight = 0F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 4.935417F;
            this.Line16.Width = 9.999306F;
            this.Line16.X1 = 0F;
            this.Line16.X2 = 9.999306F;
            this.Line16.Y1 = 4.935417F;
            this.Line16.Y2 = 4.935417F;
            // 
            // Line17
            // 
            this.Line17.Height = 0F;
            this.Line17.Left = 0F;
            this.Line17.LineWeight = 0F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 5.433333F;
            this.Line17.Width = 9.999306F;
            this.Line17.X1 = 0F;
            this.Line17.X2 = 9.999306F;
            this.Line17.Y1 = 5.433333F;
            this.Line17.Y2 = 5.433333F;
            // 
            // Line19
            // 
            this.Line19.Height = 0F;
            this.Line19.Left = 0F;
            this.Line19.LineWeight = 0F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 6.429167F;
            this.Line19.Width = 9.999306F;
            this.Line19.X1 = 0F;
            this.Line19.X2 = 9.999306F;
            this.Line19.Y1 = 6.429167F;
            this.Line19.Y2 = 6.429167F;
            // 
            // Line20
            // 
            this.Line20.Height = 0F;
            this.Line20.Left = 0F;
            this.Line20.LineWeight = 0F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 6.927083F;
            this.Line20.Width = 9.999306F;
            this.Line20.X1 = 0F;
            this.Line20.X2 = 9.999306F;
            this.Line20.Y1 = 6.927083F;
            this.Line20.Y2 = 6.927083F;
            // 
            // Line21
            // 
            this.Line21.Height = 3.427083F;
            this.Line21.Left = 2.53125F;
            this.Line21.LineWeight = 0F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 4F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 2.53125F;
            this.Line21.X2 = 2.53125F;
            this.Line21.Y1 = 4F;
            this.Line21.Y2 = 7.427083F;
            // 
            // Line22
            // 
            this.Line22.Height = 3.427083F;
            this.Line22.Left = 3.697917F;
            this.Line22.LineWeight = 0F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 4F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 3.697917F;
            this.Line22.X2 = 3.697917F;
            this.Line22.Y1 = 4F;
            this.Line22.Y2 = 7.427083F;
            // 
            // Line36
            // 
            this.Line36.Height = 0F;
            this.Line36.Left = 0F;
            this.Line36.LineWeight = 0F;
            this.Line36.Name = "Line36";
            this.Line36.Top = 4F;
            this.Line36.Width = 9.999306F;
            this.Line36.X1 = 0F;
            this.Line36.X2 = 9.999306F;
            this.Line36.Y1 = 4F;
            this.Line36.Y2 = 4F;
            // 
            // Label118
            // 
            this.Label118.Height = 0.1770833F;
            this.Label118.HyperLink = null;
            this.Label118.Left = 5.260417F;
            this.Label118.Name = "Label118";
            this.Label118.Style = "font-size: 8.5pt; text-align: center";
            this.Label118.Text = "Jan";
            this.Label118.Top = 4.28125F;
            this.Label118.Width = 0.40625F;
            // 
            // Label130
            // 
            this.Label130.Height = 0.1770833F;
            this.Label130.HyperLink = null;
            this.Label130.Left = 0F;
            this.Label130.Name = "Label130";
            this.Label130.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label130.Text = "23";
            this.Label130.Top = 4.78125F;
            this.Label130.Width = 0.28125F;
            // 
            // Label131
            // 
            this.Label131.Height = 0.1770833F;
            this.Label131.HyperLink = null;
            this.Label131.Left = 0F;
            this.Label131.Name = "Label131";
            this.Label131.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label131.Text = "24";
            this.Label131.Top = 5.279167F;
            this.Label131.Width = 0.28125F;
            // 
            // Label132
            // 
            this.Label132.Height = 0.1770833F;
            this.Label132.HyperLink = null;
            this.Label132.Left = 0F;
            this.Label132.Name = "Label132";
            this.Label132.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label132.Text = "25";
            this.Label132.Top = 5.777083F;
            this.Label132.Width = 0.28125F;
            // 
            // Label133
            // 
            this.Label133.Height = 0.1770833F;
            this.Label133.HyperLink = null;
            this.Label133.Left = 0F;
            this.Label133.Name = "Label133";
            this.Label133.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label133.Text = "26";
            this.Label133.Top = 6.275F;
            this.Label133.Width = 0.28125F;
            // 
            // Label134
            // 
            this.Label134.Height = 0.1770833F;
            this.Label134.HyperLink = null;
            this.Label134.Left = 0F;
            this.Label134.Name = "Label134";
            this.Label134.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label134.Text = "27";
            this.Label134.Top = 6.772917F;
            this.Label134.Width = 0.28125F;
            // 
            // Label135
            // 
            this.Label135.Height = 0.1770833F;
            this.Label135.HyperLink = null;
            this.Label135.Left = 0F;
            this.Label135.Name = "Label135";
            this.Label135.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label135.Text = "28";
            this.Label135.Top = 7.270833F;
            this.Label135.Width = 0.28125F;
            // 
            // Label136
            // 
            this.Label136.Height = 0.1770833F;
            this.Label136.HyperLink = null;
            this.Label136.Left = 5.65625F;
            this.Label136.Name = "Label136";
            this.Label136.Style = "font-size: 8.5pt; text-align: center";
            this.Label136.Text = "Feb";
            this.Label136.Top = 4.28125F;
            this.Label136.Width = 0.40625F;
            // 
            // Label137
            // 
            this.Label137.Height = 0.1770833F;
            this.Label137.HyperLink = null;
            this.Label137.Left = 6.052083F;
            this.Label137.Name = "Label137";
            this.Label137.Style = "font-size: 8.5pt; text-align: center";
            this.Label137.Text = "Mar";
            this.Label137.Top = 4.28125F;
            this.Label137.Width = 0.40625F;
            // 
            // Label165
            // 
            this.Label165.Height = 0.1770833F;
            this.Label165.HyperLink = null;
            this.Label165.Left = 6.4375F;
            this.Label165.Name = "Label165";
            this.Label165.Style = "font-size: 8.5pt; text-align: center";
            this.Label165.Text = "Apr";
            this.Label165.Top = 4.28125F;
            this.Label165.Width = 0.40625F;
            // 
            // Label166
            // 
            this.Label166.Height = 0.1770833F;
            this.Label166.HyperLink = null;
            this.Label166.Left = 6.833333F;
            this.Label166.Name = "Label166";
            this.Label166.Style = "font-size: 8.5pt; text-align: center";
            this.Label166.Text = "May";
            this.Label166.Top = 4.28125F;
            this.Label166.Width = 0.40625F;
            // 
            // Label167
            // 
            this.Label167.Height = 0.1770833F;
            this.Label167.HyperLink = null;
            this.Label167.Left = 7.229167F;
            this.Label167.Name = "Label167";
            this.Label167.Style = "font-size: 8.5pt; text-align: center";
            this.Label167.Text = "June";
            this.Label167.Top = 4.28125F;
            this.Label167.Width = 0.40625F;
            // 
            // Label168
            // 
            this.Label168.Height = 0.1770833F;
            this.Label168.HyperLink = null;
            this.Label168.Left = 7.625F;
            this.Label168.Name = "Label168";
            this.Label168.Style = "font-size: 8.5pt; text-align: center";
            this.Label168.Text = "July";
            this.Label168.Top = 4.28125F;
            this.Label168.Width = 0.40625F;
            // 
            // Label169
            // 
            this.Label169.Height = 0.1770833F;
            this.Label169.HyperLink = null;
            this.Label169.Left = 8.020833F;
            this.Label169.Name = "Label169";
            this.Label169.Style = "font-size: 8.5pt; text-align: center";
            this.Label169.Text = "Aug";
            this.Label169.Top = 4.28125F;
            this.Label169.Width = 0.40625F;
            // 
            // Label170
            // 
            this.Label170.Height = 0.1770833F;
            this.Label170.HyperLink = null;
            this.Label170.Left = 8.40625F;
            this.Label170.Name = "Label170";
            this.Label170.Style = "font-size: 8.5pt; text-align: center";
            this.Label170.Text = "Sept";
            this.Label170.Top = 4.28125F;
            this.Label170.Width = 0.40625F;
            // 
            // Label171
            // 
            this.Label171.Height = 0.1770833F;
            this.Label171.HyperLink = null;
            this.Label171.Left = 8.802083F;
            this.Label171.Name = "Label171";
            this.Label171.Style = "font-size: 8.5pt; text-align: center";
            this.Label171.Text = "Oct";
            this.Label171.Top = 4.28125F;
            this.Label171.Width = 0.40625F;
            // 
            // Label172
            // 
            this.Label172.Height = 0.1770833F;
            this.Label172.HyperLink = null;
            this.Label172.Left = 9.197917F;
            this.Label172.Name = "Label172";
            this.Label172.Style = "font-size: 8.5pt; text-align: center";
            this.Label172.Text = "Nov";
            this.Label172.Top = 4.28125F;
            this.Label172.Width = 0.40625F;
            // 
            // Label173
            // 
            this.Label173.Height = 0.1770833F;
            this.Label173.HyperLink = null;
            this.Label173.Left = 9.625F;
            this.Label173.Name = "Label173";
            this.Label173.Style = "font-size: 8.5pt; text-align: center";
            this.Label173.Text = "Dec";
            this.Label173.Top = 4.28125F;
            this.Label173.Width = 0.34375F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.21875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.4166667F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 6.5pt; text-align: center";
            this.Label3.Text = "(a) Name of covered individual(s) First name, middle initial, last name";
            this.Label3.Top = 4.083333F;
            this.Label3.Width = 1.520833F;
            // 
            // Label174
            // 
            this.Label174.Height = 0.21875F;
            this.Label174.HyperLink = null;
            this.Label174.Left = 2.625F;
            this.Label174.Name = "Label174";
            this.Label174.Style = "font-size: 6.5pt; text-align: center";
            this.Label174.Text = "(b) SSN or other TIN";
            this.Label174.Top = 4.083333F;
            this.Label174.Width = 1.020833F;
            // 
            // Label175
            // 
            this.Label175.Height = 0.28125F;
            this.Label175.HyperLink = null;
            this.Label175.Left = 3.715278F;
            this.Label175.Name = "Label175";
            this.Label175.Style = "font-size: 6.5pt; text-align: center";
            this.Label175.Text = "(c) DOB (If SSN or other TIN  is not available)";
            this.Label175.Top = 4.083333F;
            this.Label175.Width = 0.9930556F;
            // 
            // Label176
            // 
            this.Label176.Height = 0.28125F;
            this.Label176.HyperLink = null;
            this.Label176.Left = 4.697917F;
            this.Label176.Name = "Label176";
            this.Label176.Style = "font-size: 6.5pt; text-align: center";
            this.Label176.Text = "(d) Covered all 12 months";
            this.Label176.Top = 4.083333F;
            this.Label176.Width = 0.5833333F;
            // 
            // Label177
            // 
            this.Label177.Height = 0.1770833F;
            this.Label177.HyperLink = null;
            this.Label177.Left = 0F;
            this.Label177.Name = "Label177";
            this.Label177.Style = "font-size: 8pt; font-weight: bold";
            this.Label177.Text = "RAA #1607  For Privacy Act and Paperwork Reduction Act Notice, see separate Instr" +
    "uctions";
            this.Label177.Top = 7.447917F;
            this.Label177.Width = 5.09375F;
            // 
            // Label178
            // 
            this.Label178.Height = 0.21875F;
            this.Label178.HyperLink = null;
            this.Label178.Left = 6.604167F;
            this.Label178.Name = "Label178";
            this.Label178.Style = "font-size: 7pt; text-align: center";
            this.Label178.Text = "(e) Months of Coverage";
            this.Label178.Top = 4.083333F;
            this.Label178.Width = 1.708333F;
            // 
            // Label179
            // 
            this.Label179.Height = 0.1770833F;
            this.Label179.HyperLink = null;
            this.Label179.Left = 6.25F;
            this.Label179.Name = "Label179";
            this.Label179.Style = "font-size: 8pt";
            this.Label179.Text = "41-0852411";
            this.Label179.Top = 7.447917F;
            this.Label179.Width = 0.78125F;
            // 
            // Label180
            // 
            this.Label180.Height = 0.1770833F;
            this.Label180.HyperLink = null;
            this.Label180.Left = 8.666667F;
            this.Label180.Name = "Label180";
            this.Label180.Style = "font-size: 8pt";
            this.Label180.Text = "Form";
            this.Label180.Top = 7.447917F;
            this.Label180.Width = 0.40625F;
            // 
            // Label181
            // 
            this.Label181.Height = 0.1770833F;
            this.Label181.HyperLink = null;
            this.Label181.Left = 9.489583F;
            this.Label181.Name = "Label181";
            this.Label181.Style = "font-size: 8pt";
            this.Label181.Text = "(2018)";
            this.Label181.Top = 7.447917F;
            this.Label181.Width = 0.40625F;
            // 
            // Label182
            // 
            this.Label182.Height = 0.1770833F;
            this.Label182.HyperLink = null;
            this.Label182.Left = 8.989583F;
            this.Label182.Name = "Label182";
            this.Label182.Style = "font-size: 10pt; font-weight: bold";
            this.Label182.Text = "1095-B";
            this.Label182.Top = 7.427083F;
            this.Label182.Width = 0.53125F;
            // 
            // Line57
            // 
            this.Line57.Height = 0F;
            this.Line57.Left = 0F;
            this.Line57.LineWeight = 0F;
            this.Line57.Name = "Line57";
            this.Line57.Top = 7.427083F;
            this.Line57.Width = 9.999306F;
            this.Line57.X1 = 0F;
            this.Line57.X2 = 9.999306F;
            this.Line57.Y1 = 7.427083F;
            this.Line57.Y2 = 7.427083F;
            // 
            // Line25
            // 
            this.Line25.Height = 3.145833F;
            this.Line25.Left = 5.664583F;
            this.Line25.LineWeight = 0F;
            this.Line25.Name = "Line25";
            this.Line25.Top = 4.28125F;
            this.Line25.Width = 0F;
            this.Line25.X1 = 5.664583F;
            this.Line25.X2 = 5.664583F;
            this.Line25.Y1 = 4.28125F;
            this.Line25.Y2 = 7.427083F;
            // 
            // Line26
            // 
            this.Line26.Height = 3.145833F;
            this.Line26.Left = 6.058333F;
            this.Line26.LineWeight = 0F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 4.28125F;
            this.Line26.Width = 0F;
            this.Line26.X1 = 6.058333F;
            this.Line26.X2 = 6.058333F;
            this.Line26.Y1 = 4.28125F;
            this.Line26.Y2 = 7.427083F;
            // 
            // Line27
            // 
            this.Line27.Height = 3.145833F;
            this.Line27.Left = 6.452083F;
            this.Line27.LineWeight = 0F;
            this.Line27.Name = "Line27";
            this.Line27.Top = 4.28125F;
            this.Line27.Width = 0F;
            this.Line27.X1 = 6.452083F;
            this.Line27.X2 = 6.452083F;
            this.Line27.Y1 = 4.28125F;
            this.Line27.Y2 = 7.427083F;
            // 
            // Line28
            // 
            this.Line28.Height = 3.145833F;
            this.Line28.Left = 6.845833F;
            this.Line28.LineWeight = 0F;
            this.Line28.Name = "Line28";
            this.Line28.Top = 4.28125F;
            this.Line28.Width = 0F;
            this.Line28.X1 = 6.845833F;
            this.Line28.X2 = 6.845833F;
            this.Line28.Y1 = 4.28125F;
            this.Line28.Y2 = 7.427083F;
            // 
            // Line29
            // 
            this.Line29.Height = 3.145833F;
            this.Line29.Left = 7.239583F;
            this.Line29.LineWeight = 0F;
            this.Line29.Name = "Line29";
            this.Line29.Top = 4.28125F;
            this.Line29.Width = 0F;
            this.Line29.X1 = 7.239583F;
            this.Line29.X2 = 7.239583F;
            this.Line29.Y1 = 4.28125F;
            this.Line29.Y2 = 7.427083F;
            // 
            // Line30
            // 
            this.Line30.Height = 3.145833F;
            this.Line30.Left = 7.633333F;
            this.Line30.LineWeight = 0F;
            this.Line30.Name = "Line30";
            this.Line30.Top = 4.28125F;
            this.Line30.Width = 0F;
            this.Line30.X1 = 7.633333F;
            this.Line30.X2 = 7.633333F;
            this.Line30.Y1 = 4.28125F;
            this.Line30.Y2 = 7.427083F;
            // 
            // Line31
            // 
            this.Line31.Height = 3.145833F;
            this.Line31.Left = 8.027083F;
            this.Line31.LineWeight = 0F;
            this.Line31.Name = "Line31";
            this.Line31.Top = 4.28125F;
            this.Line31.Width = 0F;
            this.Line31.X1 = 8.027083F;
            this.Line31.X2 = 8.027083F;
            this.Line31.Y1 = 4.28125F;
            this.Line31.Y2 = 7.427083F;
            // 
            // Line32
            // 
            this.Line32.Height = 3.145833F;
            this.Line32.Left = 8.420834F;
            this.Line32.LineWeight = 0F;
            this.Line32.Name = "Line32";
            this.Line32.Top = 4.28125F;
            this.Line32.Width = 0F;
            this.Line32.X1 = 8.420834F;
            this.Line32.X2 = 8.420834F;
            this.Line32.Y1 = 4.28125F;
            this.Line32.Y2 = 7.427083F;
            // 
            // Line33
            // 
            this.Line33.Height = 3.145833F;
            this.Line33.Left = 8.814584F;
            this.Line33.LineWeight = 0F;
            this.Line33.Name = "Line33";
            this.Line33.Top = 4.28125F;
            this.Line33.Width = 0F;
            this.Line33.X1 = 8.814584F;
            this.Line33.X2 = 8.814584F;
            this.Line33.Y1 = 4.28125F;
            this.Line33.Y2 = 7.427083F;
            // 
            // Line34
            // 
            this.Line34.Height = 3.145833F;
            this.Line34.Left = 9.208333F;
            this.Line34.LineWeight = 0F;
            this.Line34.Name = "Line34";
            this.Line34.Top = 4.28125F;
            this.Line34.Width = 0F;
            this.Line34.X1 = 9.208333F;
            this.Line34.X2 = 9.208333F;
            this.Line34.Y1 = 4.28125F;
            this.Line34.Y2 = 7.427083F;
            // 
            // Line35
            // 
            this.Line35.Height = 3.145833F;
            this.Line35.Left = 9.602083F;
            this.Line35.LineWeight = 0F;
            this.Line35.Name = "Line35";
            this.Line35.Top = 4.28125F;
            this.Line35.Width = 0F;
            this.Line35.X1 = 9.602083F;
            this.Line35.X2 = 9.602083F;
            this.Line35.Y1 = 4.28125F;
            this.Line35.Y2 = 7.427083F;
            // 
            // Line37
            // 
            this.Line37.Height = 0F;
            this.Line37.Left = 5.270833F;
            this.Line37.LineWeight = 0F;
            this.Line37.Name = "Line37";
            this.Line37.Top = 4.28125F;
            this.Line37.Width = 4.729167F;
            this.Line37.X1 = 5.270833F;
            this.Line37.X2 = 10F;
            this.Line37.Y1 = 4.28125F;
            this.Line37.Y2 = 4.28125F;
            // 
            // Line23
            // 
            this.Line23.Height = 3.427083F;
            this.Line23.Left = 4.697917F;
            this.Line23.LineWeight = 0F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 4F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 4.697917F;
            this.Line23.X2 = 4.697917F;
            this.Line23.Y1 = 4F;
            this.Line23.Y2 = 7.427083F;
            // 
            // Line24
            // 
            this.Line24.Height = 3.427083F;
            this.Line24.Left = 5.270833F;
            this.Line24.LineWeight = 0F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 4F;
            this.Line24.Width = 0F;
            this.Line24.X1 = 5.270833F;
            this.Line24.X2 = 5.270833F;
            this.Line24.Y1 = 4F;
            this.Line24.Y2 = 7.427083F;
            // 
            // Line18
            // 
            this.Line18.Height = 0F;
            this.Line18.Left = 0F;
            this.Line18.LineWeight = 0F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 5.93125F;
            this.Line18.Width = 9.999306F;
            this.Line18.X1 = 0F;
            this.Line18.X2 = 9.999306F;
            this.Line18.Y1 = 5.93125F;
            this.Line18.Y2 = 5.93125F;
            // 
            // Image2
            // 
            this.Image2.Height = 0.2395833F;
            this.Image2.HyperLink = null;
            this.Image2.ImageData = ((System.IO.Stream)(resources.GetObject("Image2.ImageData")));
            this.Image2.Left = 8.875F;
            this.Image2.LineWeight = 1F;
            this.Image2.Name = "Image2";
            this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image2.Top = 0.5625F;
            this.Image2.Width = 0.7291667F;
            // 
            // Label77
            // 
            this.Label77.Height = 0.1770833F;
            this.Label77.HyperLink = null;
            this.Label77.Left = 0F;
            this.Label77.Name = "Label77";
            this.Label77.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" +
    "-size: 10pt; font-weight: bold; vertical-align: middle";
            this.Label77.Text = "Part I";
            this.Label77.Top = 0.9791667F;
            this.Label77.Width = 0.53125F;
            // 
            // Label78
            // 
            this.Label78.Height = 0.1770833F;
            this.Label78.HyperLink = null;
            this.Label78.Left = 0.6354167F;
            this.Label78.Name = "Label78";
            this.Label78.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label78.Text = "Responsible Individual";
            this.Label78.Top = 0.9791667F;
            this.Label78.Width = 1.885417F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.9791667F;
            this.Line2.Width = 9.999306F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 9.999306F;
            this.Line2.Y1 = 0.9791667F;
            this.Line2.Y2 = 0.9791667F;
            // 
            // Label83
            // 
            this.Label83.Height = 0.3125F;
            this.Label83.HyperLink = null;
            this.Label83.Left = 0.3125F;
            this.Label83.Name = "Label83";
            this.Label83.Style = "font-family: \'Impact\'; font-size: 20.5pt";
            this.Label83.Text = "1095-B";
            this.Label83.Top = 0.3125F;
            this.Label83.Width = 0.9583333F;
            // 
            // Label84
            // 
            this.Label84.Height = 0.1666667F;
            this.Label84.HyperLink = null;
            this.Label84.Left = 0F;
            this.Label84.Name = "Label84";
            this.Label84.Style = "font-size: 8pt";
            this.Label84.Text = "Form";
            this.Label84.Top = 0.4791667F;
            this.Label84.Width = 0.5F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.6527778F;
            this.Line3.Left = 1.635417F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.3263889F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 1.635417F;
            this.Line3.X2 = 1.635417F;
            this.Line3.Y1 = 0.3263889F;
            this.Line3.Y2 = 0.9791667F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.6527778F;
            this.Line4.Left = 8.5F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.3263889F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 8.5F;
            this.Line4.X2 = 8.5F;
            this.Line4.Y1 = 0.3263889F;
            this.Line4.Y2 = 0.9791667F;
            // 
            // Label85
            // 
            this.Label85.Height = 0.1666667F;
            this.Label85.HyperLink = null;
            this.Label85.Left = 0F;
            this.Label85.Name = "Label85";
            this.Label85.Style = "font-size: 8pt";
            this.Label85.Text = "Department of the Treasury";
            this.Label85.Top = 0.6458333F;
            this.Label85.Width = 1.625F;
            // 
            // Label86
            // 
            this.Label86.Height = 0.1666667F;
            this.Label86.HyperLink = null;
            this.Label86.Left = 0F;
            this.Label86.Name = "Label86";
            this.Label86.Style = "font-size: 8pt";
            this.Label86.Text = "Internal Revenue Service";
            this.Label86.Top = 0.8125F;
            this.Label86.Width = 1.625F;
            // 
            // Line6
            // 
            this.Line6.Height = 1.177083F;
            this.Line6.Left = 5.375F;
            this.Line6.LineWeight = 0F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.9791667F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 5.375F;
            this.Line6.X2 = 5.375F;
            this.Line6.Y1 = 0.9791667F;
            this.Line6.Y2 = 2.15625F;
            // 
            // Label92
            // 
            this.Label92.Height = 0.2395833F;
            this.Label92.HyperLink = null;
            this.Label92.Left = 1.9375F;
            this.Label92.Name = "Label92";
            this.Label92.Style = "font-family: \'Franklin Gothic Medium\'; font-size: 14.5pt; font-weight: bold; text" +
    "-align: center";
            this.Label92.Text = "Health Coverage";
            this.Label92.Top = 0.375F;
            this.Label92.Width = 5.395833F;
            // 
            // Label96
            // 
            this.Label96.Height = 0.1979167F;
            this.Label96.HyperLink = null;
            this.Label96.Left = 9.229167F;
            this.Label96.Name = "Label96";
            this.Label96.Style = "font-family: \'OCR A Extended\'; font-size: 12pt; text-align: right";
            this.Label96.Text = "560118";
            this.Label96.Top = 0.0625F;
            this.Label96.Width = 0.6875F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.1770833F;
            this.Shape2.Left = 7.3125F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = 0.3125F;
            this.Shape2.Width = 0.1770833F;
            // 
            // Shape3
            // 
            this.Shape3.Height = 0.1770833F;
            this.Shape3.Left = 7.3125F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Top = 0.625F;
            this.Shape3.Width = 0.1770833F;
            // 
            // Label97
            // 
            this.Label97.Height = 0.1666667F;
            this.Label97.HyperLink = null;
            this.Label97.Left = 7.520833F;
            this.Label97.Name = "Label97";
            this.Label97.Style = "";
            this.Label97.Text = "VOID";
            this.Label97.Top = 0.3229167F;
            this.Label97.Width = 0.6875F;
            // 
            // Label98
            // 
            this.Label98.Height = 0.1666667F;
            this.Label98.HyperLink = null;
            this.Label98.Left = 7.520833F;
            this.Label98.Name = "Label98";
            this.Label98.Style = "";
            this.Label98.Text = "CORRECTED";
            this.Label98.Top = 0.6354167F;
            this.Label98.Width = 0.9375F;
            // 
            // Line56
            // 
            this.Line56.Height = 0F;
            this.Line56.Left = 8.489583F;
            this.Line56.LineWeight = 0F;
            this.Line56.Name = "Line56";
            this.Line56.Top = 0.46875F;
            this.Line56.Width = 1.385417F;
            this.Line56.X1 = 8.489583F;
            this.Line56.X2 = 9.875F;
            this.Line56.Y1 = 0.46875F;
            this.Line56.Y2 = 0.46875F;
            // 
            // Label99
            // 
            this.Label99.Height = 0.1770833F;
            this.Label99.HyperLink = null;
            this.Label99.Left = 8.708333F;
            this.Label99.Name = "Label99";
            this.Label99.Style = "font-size: 8pt";
            this.Label99.Text = "OMB No. 1545-2252";
            this.Label99.Top = 0.2708333F;
            this.Label99.Width = 1.15625F;
            // 
            // Label100
            // 
            this.Label100.Height = 0.1770833F;
            this.Label100.HyperLink = null;
            this.Label100.Left = 2.5625F;
            this.Label100.Name = "Label100";
            this.Label100.Style = "font-family: \'Arial\'; font-size: 8.5pt; font-weight: bold";
            this.Label100.Text = "Go to www.irs.gov/Form1095B for instructions and the latest information.";
            this.Label100.Top = 0.8020833F;
            this.Label100.Width = 4.21875F;
            // 
            // Label102
            // 
            this.Label102.Height = 0.1770833F;
            this.Label102.HyperLink = null;
            this.Label102.Left = 2.375F;
            this.Label102.Name = "Label102";
            this.Label102.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label102.Text = "4";
            this.Label102.Top = 0.8020833F;
            this.Label102.Width = 0.3333333F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 0F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 1.822917F;
            this.Line8.Width = 9.999306F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 9.999306F;
            this.Line8.Y1 = 1.822917F;
            this.Line8.Y2 = 1.822917F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0F;
            this.Line7.LineWeight = 0F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 1.489583F;
            this.Line7.Width = 9.999306F;
            this.Line7.X1 = 0F;
            this.Line7.X2 = 9.999306F;
            this.Line7.Y1 = 1.489583F;
            this.Line7.Y2 = 1.489583F;
            // 
            // Shape4
            // 
            this.Shape4.Height = 0.1770833F;
            this.Shape4.Left = 4.895833F;
            this.Shape4.Name = "Shape4";
            this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape4.Top = 4.597917F;
            this.Shape4.Width = 0.1770833F;
            // 
            // Shape5
            // 
            this.Shape5.Height = 0.1770833F;
            this.Shape5.Left = 5.375F;
            this.Shape5.Name = "Shape5";
            this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape5.Top = 4.59375F;
            this.Shape5.Width = 0.1770833F;
            // 
            // Shape6
            // 
            this.Shape6.Height = 0.1770833F;
            this.Shape6.Left = 4.895833F;
            this.Shape6.Name = "Shape6";
            this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape6.Top = 5.095833F;
            this.Shape6.Width = 0.1770833F;
            // 
            // Shape7
            // 
            this.Shape7.Height = 0.1770833F;
            this.Shape7.Left = 4.895833F;
            this.Shape7.Name = "Shape7";
            this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape7.Top = 5.59375F;
            this.Shape7.Width = 0.1770833F;
            // 
            // Shape8
            // 
            this.Shape8.Height = 0.1770833F;
            this.Shape8.Left = 4.895833F;
            this.Shape8.Name = "Shape8";
            this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape8.Top = 6.091667F;
            this.Shape8.Width = 0.1770833F;
            // 
            // Shape9
            // 
            this.Shape9.Height = 0.1770833F;
            this.Shape9.Left = 4.895833F;
            this.Shape9.Name = "Shape9";
            this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape9.Top = 6.589583F;
            this.Shape9.Width = 0.1770833F;
            // 
            // Shape10
            // 
            this.Shape10.Height = 0.1770833F;
            this.Shape10.Left = 4.895833F;
            this.Shape10.Name = "Shape10";
            this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape10.Top = 7.0875F;
            this.Shape10.Width = 0.1770833F;
            // 
            // Shape11
            // 
            this.Shape11.Height = 0.1770833F;
            this.Shape11.Left = 5.375F;
            this.Shape11.Name = "Shape11";
            this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape11.Top = 5.09375F;
            this.Shape11.Width = 0.1770833F;
            // 
            // Shape12
            // 
            this.Shape12.Height = 0.1770833F;
            this.Shape12.Left = 5.375F;
            this.Shape12.Name = "Shape12";
            this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape12.Top = 5.59375F;
            this.Shape12.Width = 0.1770833F;
            // 
            // Shape13
            // 
            this.Shape13.Height = 0.1770833F;
            this.Shape13.Left = 5.375F;
            this.Shape13.Name = "Shape13";
            this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape13.Top = 6.09375F;
            this.Shape13.Width = 0.1770833F;
            // 
            // Shape14
            // 
            this.Shape14.Height = 0.1770833F;
            this.Shape14.Left = 5.375F;
            this.Shape14.Name = "Shape14";
            this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape14.Top = 6.59375F;
            this.Shape14.Width = 0.1770833F;
            // 
            // Shape15
            // 
            this.Shape15.Height = 0.1770833F;
            this.Shape15.Left = 5.375F;
            this.Shape15.Name = "Shape15";
            this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape15.Top = 7.083333F;
            this.Shape15.Width = 0.1770833F;
            // 
            // Shape16
            // 
            this.Shape16.Height = 0.1770833F;
            this.Shape16.Left = 5.770833F;
            this.Shape16.Name = "Shape16";
            this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape16.Top = 4.59375F;
            this.Shape16.Width = 0.1770833F;
            // 
            // Shape17
            // 
            this.Shape17.Height = 0.1770833F;
            this.Shape17.Left = 5.770833F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 5.09375F;
            this.Shape17.Width = 0.1770833F;
            // 
            // Shape18
            // 
            this.Shape18.Height = 0.1770833F;
            this.Shape18.Left = 5.770833F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 5.59375F;
            this.Shape18.Width = 0.1770833F;
            // 
            // Shape19
            // 
            this.Shape19.Height = 0.1770833F;
            this.Shape19.Left = 5.770833F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 6.09375F;
            this.Shape19.Width = 0.1770833F;
            // 
            // Shape20
            // 
            this.Shape20.Height = 0.1770833F;
            this.Shape20.Left = 5.770833F;
            this.Shape20.Name = "Shape20";
            this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape20.Top = 6.59375F;
            this.Shape20.Width = 0.1770833F;
            // 
            // Shape21
            // 
            this.Shape21.Height = 0.1770833F;
            this.Shape21.Left = 5.770833F;
            this.Shape21.Name = "Shape21";
            this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape21.Top = 7.083333F;
            this.Shape21.Width = 0.1770833F;
            // 
            // Shape22
            // 
            this.Shape22.Height = 0.1770833F;
            this.Shape22.Left = 6.1625F;
            this.Shape22.Name = "Shape22";
            this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape22.Top = 4.59375F;
            this.Shape22.Width = 0.1770833F;
            // 
            // Shape23
            // 
            this.Shape23.Height = 0.1770833F;
            this.Shape23.Left = 6.166667F;
            this.Shape23.Name = "Shape23";
            this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape23.Top = 5.09375F;
            this.Shape23.Width = 0.1770833F;
            // 
            // Shape24
            // 
            this.Shape24.Height = 0.1770833F;
            this.Shape24.Left = 6.166667F;
            this.Shape24.Name = "Shape24";
            this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape24.Top = 5.59375F;
            this.Shape24.Width = 0.1770833F;
            // 
            // Shape25
            // 
            this.Shape25.Height = 0.1770833F;
            this.Shape25.Left = 6.166667F;
            this.Shape25.Name = "Shape25";
            this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape25.Top = 6.09375F;
            this.Shape25.Width = 0.1770833F;
            // 
            // Shape26
            // 
            this.Shape26.Height = 0.1770833F;
            this.Shape26.Left = 6.166667F;
            this.Shape26.Name = "Shape26";
            this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape26.Top = 6.59375F;
            this.Shape26.Width = 0.1770833F;
            // 
            // Shape27
            // 
            this.Shape27.Height = 0.1770833F;
            this.Shape27.Left = 6.166667F;
            this.Shape27.Name = "Shape27";
            this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape27.Top = 7.083333F;
            this.Shape27.Width = 0.1770833F;
            // 
            // Shape28
            // 
            this.Shape28.Height = 0.1770833F;
            this.Shape28.Left = 6.552083F;
            this.Shape28.Name = "Shape28";
            this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape28.Top = 4.59375F;
            this.Shape28.Width = 0.1770833F;
            // 
            // Shape29
            // 
            this.Shape29.Height = 0.1770833F;
            this.Shape29.Left = 6.552083F;
            this.Shape29.Name = "Shape29";
            this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape29.Top = 5.09375F;
            this.Shape29.Width = 0.1770833F;
            // 
            // Shape30
            // 
            this.Shape30.Height = 0.1770833F;
            this.Shape30.Left = 6.552083F;
            this.Shape30.Name = "Shape30";
            this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape30.Top = 5.59375F;
            this.Shape30.Width = 0.1770833F;
            // 
            // Shape32
            // 
            this.Shape32.Height = 0.1770833F;
            this.Shape32.Left = 6.552083F;
            this.Shape32.Name = "Shape32";
            this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape32.Top = 6.59375F;
            this.Shape32.Width = 0.1770833F;
            // 
            // Shape31
            // 
            this.Shape31.Height = 0.1770833F;
            this.Shape31.Left = 6.552083F;
            this.Shape31.Name = "Shape31";
            this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape31.Top = 6.09375F;
            this.Shape31.Width = 0.1770833F;
            // 
            // Shape33
            // 
            this.Shape33.Height = 0.1770833F;
            this.Shape33.Left = 6.552083F;
            this.Shape33.Name = "Shape33";
            this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape33.Top = 7.083333F;
            this.Shape33.Width = 0.1770833F;
            // 
            // Shape34
            // 
            this.Shape34.Height = 0.1770833F;
            this.Shape34.Left = 6.947917F;
            this.Shape34.Name = "Shape34";
            this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape34.Top = 4.59375F;
            this.Shape34.Width = 0.1770833F;
            // 
            // Shape35
            // 
            this.Shape35.Height = 0.1770833F;
            this.Shape35.Left = 6.947917F;
            this.Shape35.Name = "Shape35";
            this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape35.Top = 5.09375F;
            this.Shape35.Width = 0.1770833F;
            // 
            // Shape36
            // 
            this.Shape36.Height = 0.1770833F;
            this.Shape36.Left = 6.947917F;
            this.Shape36.Name = "Shape36";
            this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape36.Top = 5.59375F;
            this.Shape36.Width = 0.1770833F;
            // 
            // Shape37
            // 
            this.Shape37.Height = 0.1770833F;
            this.Shape37.Left = 6.947917F;
            this.Shape37.Name = "Shape37";
            this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape37.Top = 6.09375F;
            this.Shape37.Width = 0.1770833F;
            // 
            // Shape38
            // 
            this.Shape38.Height = 0.1770833F;
            this.Shape38.Left = 6.947917F;
            this.Shape38.Name = "Shape38";
            this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape38.Top = 6.59375F;
            this.Shape38.Width = 0.1770833F;
            // 
            // Shape39
            // 
            this.Shape39.Height = 0.1770833F;
            this.Shape39.Left = 6.947917F;
            this.Shape39.Name = "Shape39";
            this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape39.Top = 7.083333F;
            this.Shape39.Width = 0.1770833F;
            // 
            // Shape40
            // 
            this.Shape40.Height = 0.1770833F;
            this.Shape40.Left = 7.34375F;
            this.Shape40.Name = "Shape40";
            this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape40.Top = 4.59375F;
            this.Shape40.Width = 0.1770833F;
            // 
            // Shape41
            // 
            this.Shape41.Height = 0.1770833F;
            this.Shape41.Left = 7.34375F;
            this.Shape41.Name = "Shape41";
            this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape41.Top = 5.09375F;
            this.Shape41.Width = 0.1770833F;
            // 
            // Shape42
            // 
            this.Shape42.Height = 0.1770833F;
            this.Shape42.Left = 7.34375F;
            this.Shape42.Name = "Shape42";
            this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape42.Top = 5.59375F;
            this.Shape42.Width = 0.1770833F;
            // 
            // Shape43
            // 
            this.Shape43.Height = 0.1770833F;
            this.Shape43.Left = 7.34375F;
            this.Shape43.Name = "Shape43";
            this.Shape43.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape43.Top = 6.09375F;
            this.Shape43.Width = 0.1770833F;
            // 
            // Shape44
            // 
            this.Shape44.Height = 0.1770833F;
            this.Shape44.Left = 7.34375F;
            this.Shape44.Name = "Shape44";
            this.Shape44.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape44.Top = 6.59375F;
            this.Shape44.Width = 0.1770833F;
            // 
            // Shape45
            // 
            this.Shape45.Height = 0.1770833F;
            this.Shape45.Left = 7.34375F;
            this.Shape45.Name = "Shape45";
            this.Shape45.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape45.Top = 7.083333F;
            this.Shape45.Width = 0.1770833F;
            // 
            // Shape46
            // 
            this.Shape46.Height = 0.1770833F;
            this.Shape46.Left = 7.739583F;
            this.Shape46.Name = "Shape46";
            this.Shape46.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape46.Top = 4.59375F;
            this.Shape46.Width = 0.1770833F;
            // 
            // Shape47
            // 
            this.Shape47.Height = 0.1770833F;
            this.Shape47.Left = 7.739583F;
            this.Shape47.Name = "Shape47";
            this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape47.Top = 5.09375F;
            this.Shape47.Width = 0.1770833F;
            // 
            // Shape48
            // 
            this.Shape48.Height = 0.1770833F;
            this.Shape48.Left = 7.739583F;
            this.Shape48.Name = "Shape48";
            this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape48.Top = 5.59375F;
            this.Shape48.Width = 0.1770833F;
            // 
            // Shape49
            // 
            this.Shape49.Height = 0.1770833F;
            this.Shape49.Left = 7.739583F;
            this.Shape49.Name = "Shape49";
            this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape49.Top = 6.09375F;
            this.Shape49.Width = 0.1770833F;
            // 
            // Shape50
            // 
            this.Shape50.Height = 0.1770833F;
            this.Shape50.Left = 7.739583F;
            this.Shape50.Name = "Shape50";
            this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape50.Top = 6.59375F;
            this.Shape50.Width = 0.1770833F;
            // 
            // Shape51
            // 
            this.Shape51.Height = 0.1770833F;
            this.Shape51.Left = 7.739583F;
            this.Shape51.Name = "Shape51";
            this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape51.Top = 7.083333F;
            this.Shape51.Width = 0.1770833F;
            // 
            // Shape52
            // 
            this.Shape52.Height = 0.1770833F;
            this.Shape52.Left = 8.135417F;
            this.Shape52.Name = "Shape52";
            this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape52.Top = 4.59375F;
            this.Shape52.Width = 0.1770833F;
            // 
            // Shape53
            // 
            this.Shape53.Height = 0.1770833F;
            this.Shape53.Left = 8.135417F;
            this.Shape53.Name = "Shape53";
            this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape53.Top = 5.09375F;
            this.Shape53.Width = 0.1770833F;
            // 
            // Shape54
            // 
            this.Shape54.Height = 0.1770833F;
            this.Shape54.Left = 8.135417F;
            this.Shape54.Name = "Shape54";
            this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape54.Top = 5.59375F;
            this.Shape54.Width = 0.1770833F;
            // 
            // Shape55
            // 
            this.Shape55.Height = 0.1770833F;
            this.Shape55.Left = 8.135417F;
            this.Shape55.Name = "Shape55";
            this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape55.Top = 6.09375F;
            this.Shape55.Width = 0.1770833F;
            // 
            // Shape56
            // 
            this.Shape56.Height = 0.1770833F;
            this.Shape56.Left = 8.135417F;
            this.Shape56.Name = "Shape56";
            this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape56.Top = 6.59375F;
            this.Shape56.Width = 0.1770833F;
            // 
            // Shape57
            // 
            this.Shape57.Height = 0.1770833F;
            this.Shape57.Left = 8.135417F;
            this.Shape57.Name = "Shape57";
            this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape57.Top = 7.083333F;
            this.Shape57.Width = 0.1770833F;
            // 
            // Shape58
            // 
            this.Shape58.Height = 0.1770833F;
            this.Shape58.Left = 8.520833F;
            this.Shape58.Name = "Shape58";
            this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape58.Top = 4.59375F;
            this.Shape58.Width = 0.1770833F;
            // 
            // Shape59
            // 
            this.Shape59.Height = 0.1770833F;
            this.Shape59.Left = 8.520833F;
            this.Shape59.Name = "Shape59";
            this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape59.Top = 5.09375F;
            this.Shape59.Width = 0.1770833F;
            // 
            // Shape60
            // 
            this.Shape60.Height = 0.1770833F;
            this.Shape60.Left = 8.520833F;
            this.Shape60.Name = "Shape60";
            this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape60.Top = 5.59375F;
            this.Shape60.Width = 0.1770833F;
            // 
            // Shape61
            // 
            this.Shape61.Height = 0.1770833F;
            this.Shape61.Left = 8.520833F;
            this.Shape61.Name = "Shape61";
            this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape61.Top = 6.09375F;
            this.Shape61.Width = 0.1770833F;
            // 
            // Shape62
            // 
            this.Shape62.Height = 0.1770833F;
            this.Shape62.Left = 8.520833F;
            this.Shape62.Name = "Shape62";
            this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape62.Top = 6.59375F;
            this.Shape62.Width = 0.1770833F;
            // 
            // Shape63
            // 
            this.Shape63.Height = 0.1770833F;
            this.Shape63.Left = 8.520833F;
            this.Shape63.Name = "Shape63";
            this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape63.Top = 7.083333F;
            this.Shape63.Width = 0.1770833F;
            // 
            // Shape64
            // 
            this.Shape64.Height = 0.1770833F;
            this.Shape64.Left = 8.916667F;
            this.Shape64.Name = "Shape64";
            this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape64.Top = 4.59375F;
            this.Shape64.Width = 0.1770833F;
            // 
            // Shape65
            // 
            this.Shape65.Height = 0.1770833F;
            this.Shape65.Left = 8.916667F;
            this.Shape65.Name = "Shape65";
            this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape65.Top = 5.09375F;
            this.Shape65.Width = 0.1770833F;
            // 
            // Shape66
            // 
            this.Shape66.Height = 0.1770833F;
            this.Shape66.Left = 8.916667F;
            this.Shape66.Name = "Shape66";
            this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape66.Top = 5.59375F;
            this.Shape66.Width = 0.1770833F;
            // 
            // Shape67
            // 
            this.Shape67.Height = 0.1770833F;
            this.Shape67.Left = 8.916667F;
            this.Shape67.Name = "Shape67";
            this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape67.Top = 6.09375F;
            this.Shape67.Width = 0.1770833F;
            // 
            // Shape68
            // 
            this.Shape68.Height = 0.1770833F;
            this.Shape68.Left = 8.916667F;
            this.Shape68.Name = "Shape68";
            this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape68.Top = 6.59375F;
            this.Shape68.Width = 0.1770833F;
            // 
            // Shape69
            // 
            this.Shape69.Height = 0.1770833F;
            this.Shape69.Left = 8.916667F;
            this.Shape69.Name = "Shape69";
            this.Shape69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape69.Top = 7.083333F;
            this.Shape69.Width = 0.1770833F;
            // 
            // Shape70
            // 
            this.Shape70.Height = 0.1770833F;
            this.Shape70.Left = 9.3125F;
            this.Shape70.Name = "Shape70";
            this.Shape70.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape70.Top = 4.59375F;
            this.Shape70.Width = 0.1770833F;
            // 
            // Shape71
            // 
            this.Shape71.Height = 0.1770833F;
            this.Shape71.Left = 9.3125F;
            this.Shape71.Name = "Shape71";
            this.Shape71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape71.Top = 5.09375F;
            this.Shape71.Width = 0.1770833F;
            // 
            // Shape72
            // 
            this.Shape72.Height = 0.1770833F;
            this.Shape72.Left = 9.3125F;
            this.Shape72.Name = "Shape72";
            this.Shape72.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape72.Top = 5.59375F;
            this.Shape72.Width = 0.1770833F;
            // 
            // Shape73
            // 
            this.Shape73.Height = 0.1770833F;
            this.Shape73.Left = 9.3125F;
            this.Shape73.Name = "Shape73";
            this.Shape73.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape73.Top = 6.09375F;
            this.Shape73.Width = 0.1770833F;
            // 
            // Shape74
            // 
            this.Shape74.Height = 0.1770833F;
            this.Shape74.Left = 9.3125F;
            this.Shape74.Name = "Shape74";
            this.Shape74.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape74.Top = 6.59375F;
            this.Shape74.Width = 0.1770833F;
            // 
            // Shape75
            // 
            this.Shape75.Height = 0.1770833F;
            this.Shape75.Left = 9.3125F;
            this.Shape75.Name = "Shape75";
            this.Shape75.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape75.Top = 7.083333F;
            this.Shape75.Width = 0.1770833F;
            // 
            // Shape76
            // 
            this.Shape76.Height = 0.1770833F;
            this.Shape76.Left = 9.708333F;
            this.Shape76.Name = "Shape76";
            this.Shape76.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape76.Top = 4.59375F;
            this.Shape76.Width = 0.1770833F;
            // 
            // Shape77
            // 
            this.Shape77.Height = 0.1770833F;
            this.Shape77.Left = 9.708333F;
            this.Shape77.Name = "Shape77";
            this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape77.Top = 5.09375F;
            this.Shape77.Width = 0.1770833F;
            // 
            // Shape78
            // 
            this.Shape78.Height = 0.1770833F;
            this.Shape78.Left = 9.708333F;
            this.Shape78.Name = "Shape78";
            this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape78.Top = 5.59375F;
            this.Shape78.Width = 0.1770833F;
            // 
            // Shape79
            // 
            this.Shape79.Height = 0.1770833F;
            this.Shape79.Left = 9.708333F;
            this.Shape79.Name = "Shape79";
            this.Shape79.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape79.Top = 6.09375F;
            this.Shape79.Width = 0.1770833F;
            // 
            // Shape80
            // 
            this.Shape80.Height = 0.1770833F;
            this.Shape80.Left = 9.708333F;
            this.Shape80.Name = "Shape80";
            this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape80.Top = 6.59375F;
            this.Shape80.Width = 0.1770833F;
            // 
            // Shape81
            // 
            this.Shape81.Height = 0.1770833F;
            this.Shape81.Left = 9.708333F;
            this.Shape81.Name = "Shape81";
            this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape81.Top = 7.083333F;
            this.Shape81.Width = 0.1770833F;
            // 
            // Label88
            // 
            this.Label88.Height = 0.1770833F;
            this.Label88.HyperLink = null;
            this.Label88.Left = 0F;
            this.Label88.Name = "Label88";
            this.Label88.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" +
    "-size: 10pt; font-weight: bold; vertical-align: middle";
            this.Label88.Text = "Part II";
            this.Label88.Top = 2.15625F;
            this.Label88.Width = 0.53125F;
            // 
            // Label91
            // 
            this.Label91.Height = 0.1770833F;
            this.Label91.HyperLink = null;
            this.Label91.Left = 0.65625F;
            this.Label91.Name = "Label91";
            this.Label91.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label91.Text = "Information about Certain Employer-Sponsored Coverage";
            this.Label91.Top = 2.15625F;
            this.Label91.Width = 4.010417F;
            // 
            // Label183
            // 
            this.Label183.Height = 0.1770833F;
            this.Label183.HyperLink = null;
            this.Label183.Left = 4.53125F;
            this.Label183.Name = "Label183";
            this.Label183.Style = "font-family: \'Arial\'";
            this.Label183.Text = "(see instructions)";
            this.Label183.Top = 2.15625F;
            this.Label183.Width = 2.260417F;
            // 
            // Line58
            // 
            this.Line58.Height = 0F;
            this.Line58.Left = 0F;
            this.Line58.LineWeight = 1F;
            this.Line58.Name = "Line58";
            this.Line58.Top = 2.333333F;
            this.Line58.Width = 10F;
            this.Line58.X1 = 0F;
            this.Line58.X2 = 10F;
            this.Line58.Y1 = 2.333333F;
            this.Line58.Y2 = 2.333333F;
            // 
            // Line59
            // 
            this.Line59.Height = 0F;
            this.Line59.Left = 0F;
            this.Line59.LineWeight = 0F;
            this.Line59.Name = "Line59";
            this.Line59.Top = 1.15625F;
            this.Line59.Width = 10F;
            this.Line59.X1 = 0F;
            this.Line59.X2 = 10F;
            this.Line59.Y1 = 1.15625F;
            this.Line59.Y2 = 1.15625F;
            // 
            // Label184
            // 
            this.Label184.Height = 0.1770833F;
            this.Label184.HyperLink = null;
            this.Label184.Left = 0F;
            this.Label184.Name = "Label184";
            this.Label184.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" +
    "-size: 10pt; font-weight: bold; vertical-align: middle";
            this.Label184.Text = "Part III";
            this.Label184.Top = 2.989583F;
            this.Label184.Width = 0.53125F;
            // 
            // Label185
            // 
            this.Label185.Height = 0.1770833F;
            this.Label185.HyperLink = null;
            this.Label185.Left = 0.65625F;
            this.Label185.Name = "Label185";
            this.Label185.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label185.Text = "Issuer or Other Coverage Provider";
            this.Label185.Top = 2.989583F;
            this.Label185.Width = 2.385417F;
            // 
            // Label186
            // 
            this.Label186.Height = 0.1770833F;
            this.Label186.HyperLink = null;
            this.Label186.Left = 3.03125F;
            this.Label186.Name = "Label186";
            this.Label186.Style = "font-family: \'Arial\'";
            this.Label186.Text = "(see instructions)";
            this.Label186.Top = 2.989583F;
            this.Label186.Width = 2.260417F;
            // 
            // Line60
            // 
            this.Line60.Height = 0F;
            this.Line60.Left = 0F;
            this.Line60.LineWeight = 1F;
            this.Line60.Name = "Line60";
            this.Line60.Top = 2.989583F;
            this.Line60.Width = 10F;
            this.Line60.X1 = 0F;
            this.Line60.X2 = 10F;
            this.Line60.Y1 = 2.989583F;
            this.Line60.Y2 = 2.989583F;
            // 
            // Label187
            // 
            this.Label187.Height = 0.1770833F;
            this.Label187.HyperLink = null;
            this.Label187.Left = 0F;
            this.Label187.Name = "Label187";
            this.Label187.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" +
    "-size: 10pt; font-weight: bold; vertical-align: middle";
            this.Label187.Text = "Part IV";
            this.Label187.Top = 3.822917F;
            this.Label187.Width = 0.53125F;
            // 
            // Label188
            // 
            this.Label188.Height = 0.1770833F;
            this.Label188.HyperLink = null;
            this.Label188.Left = 0.65625F;
            this.Label188.Name = "Label188";
            this.Label188.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label188.Text = "Covered Individuals";
            this.Label188.Top = 3.822917F;
            this.Label188.Width = 1.635417F;
            // 
            // Line63
            // 
            this.Line63.Height = 0.65625F;
            this.Line63.Left = 5.375F;
            this.Line63.LineWeight = 0F;
            this.Line63.Name = "Line63";
            this.Line63.Top = 3.166667F;
            this.Line63.Width = 0F;
            this.Line63.X1 = 5.375F;
            this.Line63.X2 = 5.375F;
            this.Line63.Y1 = 3.166667F;
            this.Line63.Y2 = 3.822917F;
            // 
            // Line65
            // 
            this.Line65.Height = 0.65625F;
            this.Line65.Left = 7.5625F;
            this.Line65.LineWeight = 0F;
            this.Line65.Name = "Line65";
            this.Line65.Top = 3.166667F;
            this.Line65.Width = 0F;
            this.Line65.X1 = 7.5625F;
            this.Line65.X2 = 7.5625F;
            this.Line65.Y1 = 3.166667F;
            this.Line65.Y2 = 3.822917F;
            // 
            // Line66
            // 
            this.Line66.Height = 0.6666666F;
            this.Line66.Left = 7.5625F;
            this.Line66.LineWeight = 0F;
            this.Line66.Name = "Line66";
            this.Line66.Top = 1.15625F;
            this.Line66.Width = 0F;
            this.Line66.X1 = 7.5625F;
            this.Line66.X2 = 7.5625F;
            this.Line66.Y1 = 1.15625F;
            this.Line66.Y2 = 1.822917F;
            // 
            // Line67
            // 
            this.Line67.Height = 0.3333333F;
            this.Line67.Left = 3.520833F;
            this.Line67.LineWeight = 0F;
            this.Line67.Name = "Line67";
            this.Line67.Top = 1.489583F;
            this.Line67.Width = 0F;
            this.Line67.X1 = 3.520833F;
            this.Line67.X2 = 3.520833F;
            this.Line67.Y1 = 1.489583F;
            this.Line67.Y2 = 1.822917F;
            // 
            // Line68
            // 
            this.Line68.Height = 0.65625F;
            this.Line68.Left = 7.5625F;
            this.Line68.LineWeight = 0F;
            this.Line68.Name = "Line68";
            this.Line68.Top = 2.333333F;
            this.Line68.Width = 0F;
            this.Line68.X1 = 7.5625F;
            this.Line68.X2 = 7.5625F;
            this.Line68.Y1 = 2.333333F;
            this.Line68.Y2 = 2.989583F;
            // 
            // Line69
            // 
            this.Line69.Height = 0F;
            this.Line69.Left = 0F;
            this.Line69.LineWeight = 1F;
            this.Line69.Name = "Line69";
            this.Line69.Top = 2.661111F;
            this.Line69.Width = 10F;
            this.Line69.X1 = 0F;
            this.Line69.X2 = 10F;
            this.Line69.Y1 = 2.661111F;
            this.Line69.Y2 = 2.661111F;
            // 
            // Line70
            // 
            this.Line70.Height = 0.3284721F;
            this.Line70.Left = 3.5F;
            this.Line70.LineWeight = 0F;
            this.Line70.Name = "Line70";
            this.Line70.Top = 2.661111F;
            this.Line70.Width = 0F;
            this.Line70.X1 = 3.5F;
            this.Line70.X2 = 3.5F;
            this.Line70.Y1 = 2.661111F;
            this.Line70.Y2 = 2.989583F;
            // 
            // Line71
            // 
            this.Line71.Height = 0.3284721F;
            this.Line71.Left = 5.375F;
            this.Line71.LineWeight = 0F;
            this.Line71.Name = "Line71";
            this.Line71.Top = 2.661111F;
            this.Line71.Width = 0F;
            this.Line71.X1 = 5.375F;
            this.Line71.X2 = 5.375F;
            this.Line71.Y1 = 2.661111F;
            this.Line71.Y2 = 2.989583F;
            // 
            // Label189
            // 
            this.Label189.Height = 0.1770833F;
            this.Label189.HyperLink = null;
            this.Label189.Left = 2.09375F;
            this.Label189.Name = "Label189";
            this.Label189.Style = "font-family: \'Arial\'";
            this.Label189.Text = "(Enter the information for each covered individual.)";
            this.Label189.Top = 3.822917F;
            this.Label189.Width = 3.510417F;
            // 
            // Line62
            // 
            this.Line62.Height = 0F;
            this.Line62.Left = 0F;
            this.Line62.LineWeight = 1F;
            this.Line62.Name = "Line62";
            this.Line62.Top = 3.822917F;
            this.Line62.Width = 10F;
            this.Line62.X1 = 0F;
            this.Line62.X2 = 10F;
            this.Line62.Y1 = 3.822917F;
            this.Line62.Y2 = 3.822917F;
            // 
            // Label107
            // 
            this.Label107.Height = 0.1770833F;
            this.Label107.HyperLink = null;
            this.Label107.Left = 0.1458333F;
            this.Label107.Name = "Label107";
            this.Label107.Style = "font-size: 6.5pt";
            this.Label107.Text = "Name of responsible individual-First name, middle name, last name";
            this.Label107.Top = 1.166667F;
            this.Label107.Width = 3.21875F;
            // 
            // Label152
            // 
            this.Label152.Height = 0.1770833F;
            this.Label152.HyperLink = null;
            this.Label152.Left = 0.02083333F;
            this.Label152.Name = "Label152";
            this.Label152.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label152.Text = "1";
            this.Label152.Top = 1.166667F;
            this.Label152.Width = 0.15625F;
            // 
            // Label108
            // 
            this.Label108.Height = 0.1770833F;
            this.Label108.HyperLink = null;
            this.Label108.Left = 5.520833F;
            this.Label108.Name = "Label108";
            this.Label108.Style = "font-size: 6.5pt";
            this.Label108.Text = "Social security number (SSN or other TIN)";
            this.Label108.Top = 1.166667F;
            this.Label108.Width = 1.84375F;
            // 
            // Label155
            // 
            this.Label155.Height = 0.1770833F;
            this.Label155.HyperLink = null;
            this.Label155.Left = 5.40625F;
            this.Label155.Name = "Label155";
            this.Label155.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label155.Text = "2";
            this.Label155.Top = 1.166667F;
            this.Label155.Width = 0.15625F;
            // 
            // Label190
            // 
            this.Label190.Height = 0.1770833F;
            this.Label190.HyperLink = null;
            this.Label190.Left = 7.708333F;
            this.Label190.Name = "Label190";
            this.Label190.Style = "font-size: 6.5pt";
            this.Label190.Text = "Date of birth (If SSN or other TIN is not available)";
            this.Label190.Top = 1.166667F;
            this.Label190.Width = 2.09375F;
            // 
            // Label191
            // 
            this.Label191.Height = 0.1770833F;
            this.Label191.HyperLink = null;
            this.Label191.Left = 7.59375F;
            this.Label191.Name = "Label191";
            this.Label191.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label191.Text = "3";
            this.Label191.Top = 1.166667F;
            this.Label191.Width = 0.15625F;
            // 
            // Label192
            // 
            this.Label192.Height = 0.1770833F;
            this.Label192.HyperLink = null;
            this.Label192.Left = 0.1458333F;
            this.Label192.Name = "Label192";
            this.Label192.Style = "font-size: 6.5pt";
            this.Label192.Text = "Street address (including apartment no.)";
            this.Label192.Top = 1.520833F;
            this.Label192.Width = 1.78125F;
            // 
            // Label193
            // 
            this.Label193.Height = 0.1770833F;
            this.Label193.HyperLink = null;
            this.Label193.Left = 0.02083333F;
            this.Label193.Name = "Label193";
            this.Label193.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label193.Text = "4";
            this.Label193.Top = 1.520833F;
            this.Label193.Width = 0.15625F;
            // 
            // Label194
            // 
            this.Label194.Height = 0.1770833F;
            this.Label194.HyperLink = null;
            this.Label194.Left = 3.645833F;
            this.Label194.Name = "Label194";
            this.Label194.Style = "font-size: 6.5pt";
            this.Label194.Text = "City or town";
            this.Label194.Top = 1.520833F;
            this.Label194.Width = 1.53125F;
            // 
            // Label195
            // 
            this.Label195.Height = 0.1770833F;
            this.Label195.HyperLink = null;
            this.Label195.Left = 3.53125F;
            this.Label195.Name = "Label195";
            this.Label195.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label195.Text = "5";
            this.Label195.Top = 1.520833F;
            this.Label195.Width = 0.15625F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 2.15625F;
            this.Line5.Width = 9.999306F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 9.999306F;
            this.Line5.Y1 = 2.15625F;
            this.Line5.Y2 = 2.15625F;
            // 
            // Label196
            // 
            this.Label196.Height = 0.1770833F;
            this.Label196.HyperLink = null;
            this.Label196.Left = 5.520833F;
            this.Label196.Name = "Label196";
            this.Label196.Style = "font-size: 6.5pt";
            this.Label196.Text = "State or province";
            this.Label196.Top = 1.520833F;
            this.Label196.Width = 1.53125F;
            // 
            // Label197
            // 
            this.Label197.Height = 0.1770833F;
            this.Label197.HyperLink = null;
            this.Label197.Left = 5.40625F;
            this.Label197.Name = "Label197";
            this.Label197.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label197.Text = "6";
            this.Label197.Top = 1.520833F;
            this.Label197.Width = 0.15625F;
            // 
            // Label198
            // 
            this.Label198.Height = 0.1770833F;
            this.Label198.HyperLink = null;
            this.Label198.Left = 7.708333F;
            this.Label198.Name = "Label198";
            this.Label198.Style = "font-size: 6.5pt";
            this.Label198.Text = "Country and ZIP or foreign postal code";
            this.Label198.Top = 1.520833F;
            this.Label198.Width = 1.84375F;
            // 
            // Label199
            // 
            this.Label199.Height = 0.1770833F;
            this.Label199.HyperLink = null;
            this.Label199.Left = 7.59375F;
            this.Label199.Name = "Label199";
            this.Label199.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label199.Text = "7";
            this.Label199.Top = 1.520833F;
            this.Label199.Width = 0.15625F;
            // 
            // Label200
            // 
            this.Label200.Height = 0.1770833F;
            this.Label200.HyperLink = null;
            this.Label200.Left = 5.520833F;
            this.Label200.Name = "Label200";
            this.Label200.Style = "font-size: 6.5pt";
            this.Label200.Text = "Small Business Health Options Program (SHOP) Marketplace identifier, if applicabl" +
    "e";
            this.Label200.Top = 1.833333F;
            this.Label200.Width = 3.84375F;
            // 
            // Label201
            // 
            this.Label201.Height = 0.1770833F;
            this.Label201.HyperLink = null;
            this.Label201.Left = 5.40625F;
            this.Label201.Name = "Label201";
            this.Label201.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label201.Text = "9";
            this.Label201.Top = 1.833333F;
            this.Label201.Width = 0.15625F;
            // 
            // Shape82
            // 
            this.Shape82.Height = 0.1770833F;
            this.Shape82.Left = 5.135417F;
            this.Shape82.Name = "Shape82";
            this.Shape82.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape82.Top = 1.958333F;
            this.Shape82.Width = 0.1770833F;
            // 
            // Label202
            // 
            this.Label202.Height = 0.1770833F;
            this.Label202.HyperLink = null;
            this.Label202.Left = 0.1458333F;
            this.Label202.Name = "Label202";
            this.Label202.Style = "font-size: 8.5pt";
            this.Label202.Text = "Enter letter identifying Origin of the Health Coverage (see instructions for code" +
    "s):";
            this.Label202.Top = 1.958333F;
            this.Label202.Width = 4.28125F;
            // 
            // Label203
            // 
            this.Label203.Height = 0.1770833F;
            this.Label203.HyperLink = null;
            this.Label203.Left = 0.02083333F;
            this.Label203.Name = "Label203";
            this.Label203.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label203.Text = "8";
            this.Label203.Top = 1.958333F;
            this.Label203.Width = 0.15625F;
            // 
            // Label204
            // 
            this.Label204.Height = 0.1770833F;
            this.Label204.HyperLink = null;
            this.Label204.Left = 4.822917F;
            this.Label204.Name = "Label204";
            this.Label204.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label204.Text = "4";
            this.Label204.Top = 1.958333F;
            this.Label204.Width = 0.3333333F;
            // 
            // Label205
            // 
            this.Label205.Height = 0.1770833F;
            this.Label205.HyperLink = null;
            this.Label205.Left = 0.1458333F;
            this.Label205.Name = "Label205";
            this.Label205.Style = "font-size: 6.5pt";
            this.Label205.Text = "Employer name";
            this.Label205.Top = 2.354167F;
            this.Label205.Width = 2.21875F;
            // 
            // Label206
            // 
            this.Label206.Height = 0.1770833F;
            this.Label206.HyperLink = null;
            this.Label206.Left = 0.01041667F;
            this.Label206.Name = "Label206";
            this.Label206.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label206.Text = "10";
            this.Label206.Top = 2.354167F;
            this.Label206.Width = 0.15625F;
            // 
            // Label207
            // 
            this.Label207.Height = 0.1770833F;
            this.Label207.HyperLink = null;
            this.Label207.Left = 7.708333F;
            this.Label207.Name = "Label207";
            this.Label207.Style = "font-size: 6.5pt";
            this.Label207.Text = "Employer identification number (EIN)";
            this.Label207.Top = 2.354167F;
            this.Label207.Width = 2.15625F;
            // 
            // Label208
            // 
            this.Label208.Height = 0.1770833F;
            this.Label208.HyperLink = null;
            this.Label208.Left = 7.572917F;
            this.Label208.Name = "Label208";
            this.Label208.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label208.Text = "11";
            this.Label208.Top = 2.354167F;
            this.Label208.Width = 0.15625F;
            // 
            // Label209
            // 
            this.Label209.Height = 0.1770833F;
            this.Label209.HyperLink = null;
            this.Label209.Left = 0.1458333F;
            this.Label209.Name = "Label209";
            this.Label209.Style = "font-size: 6.5pt";
            this.Label209.Text = "Street address (including room or suite no.)";
            this.Label209.Top = 2.666667F;
            this.Label209.Width = 2.21875F;
            // 
            // Label210
            // 
            this.Label210.Height = 0.1770833F;
            this.Label210.HyperLink = null;
            this.Label210.Left = 0.01041667F;
            this.Label210.Name = "Label210";
            this.Label210.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label210.Text = "12";
            this.Label210.Top = 2.666667F;
            this.Label210.Width = 0.15625F;
            // 
            // Label211
            // 
            this.Label211.Height = 0.1770833F;
            this.Label211.HyperLink = null;
            this.Label211.Left = 3.645833F;
            this.Label211.Name = "Label211";
            this.Label211.Style = "font-size: 6.5pt";
            this.Label211.Text = "City or town";
            this.Label211.Top = 2.666667F;
            this.Label211.Width = 1.65625F;
            // 
            // Label212
            // 
            this.Label212.Height = 0.1770833F;
            this.Label212.HyperLink = null;
            this.Label212.Left = 3.510417F;
            this.Label212.Name = "Label212";
            this.Label212.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label212.Text = "13";
            this.Label212.Top = 2.666667F;
            this.Label212.Width = 0.15625F;
            // 
            // Label213
            // 
            this.Label213.Height = 0.1770833F;
            this.Label213.HyperLink = null;
            this.Label213.Left = 5.520833F;
            this.Label213.Name = "Label213";
            this.Label213.Style = "font-size: 6.5pt";
            this.Label213.Text = "State or province";
            this.Label213.Top = 2.666667F;
            this.Label213.Width = 1.84375F;
            // 
            // Label214
            // 
            this.Label214.Height = 0.1770833F;
            this.Label214.HyperLink = null;
            this.Label214.Left = 5.385417F;
            this.Label214.Name = "Label214";
            this.Label214.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label214.Text = "14";
            this.Label214.Top = 2.666667F;
            this.Label214.Width = 0.15625F;
            // 
            // Label215
            // 
            this.Label215.Height = 0.1770833F;
            this.Label215.HyperLink = null;
            this.Label215.Left = 7.708333F;
            this.Label215.Name = "Label215";
            this.Label215.Style = "font-size: 6.5pt";
            this.Label215.Text = "Country and ZIP or foreign postal code";
            this.Label215.Top = 2.666667F;
            this.Label215.Width = 2.21875F;
            // 
            // Label216
            // 
            this.Label216.Height = 0.1770833F;
            this.Label216.HyperLink = null;
            this.Label216.Left = 7.572917F;
            this.Label216.Name = "Label216";
            this.Label216.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label216.Text = "15";
            this.Label216.Top = 2.666667F;
            this.Label216.Width = 0.15625F;
            // 
            // Label217
            // 
            this.Label217.Height = 0.1770833F;
            this.Label217.HyperLink = null;
            this.Label217.Left = 0.1458333F;
            this.Label217.Name = "Label217";
            this.Label217.Style = "font-size: 6.5pt";
            this.Label217.Text = "Name";
            this.Label217.Top = 3.1875F;
            this.Label217.Width = 2.21875F;
            // 
            // Label218
            // 
            this.Label218.Height = 0.1770833F;
            this.Label218.HyperLink = null;
            this.Label218.Left = 0.01041667F;
            this.Label218.Name = "Label218";
            this.Label218.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label218.Text = "16";
            this.Label218.Top = 3.1875F;
            this.Label218.Width = 0.15625F;
            // 
            // Label219
            // 
            this.Label219.Height = 0.1770833F;
            this.Label219.HyperLink = null;
            this.Label219.Left = 0.1458333F;
            this.Label219.Name = "Label219";
            this.Label219.Style = "font-size: 6.5pt";
            this.Label219.Text = "Street address (including room or suite no.)";
            this.Label219.Top = 3.504861F;
            this.Label219.Width = 2.21875F;
            // 
            // Label220
            // 
            this.Label220.Height = 0.1770833F;
            this.Label220.HyperLink = null;
            this.Label220.Left = 0.01041667F;
            this.Label220.Name = "Label220";
            this.Label220.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label220.Text = "19";
            this.Label220.Top = 3.504861F;
            this.Label220.Width = 0.15625F;
            // 
            // Line64
            // 
            this.Line64.Height = 0F;
            this.Line64.Left = 0F;
            this.Line64.LineWeight = 0F;
            this.Line64.Name = "Line64";
            this.Line64.Top = 3.484028F;
            this.Line64.Width = 9.999306F;
            this.Line64.X1 = 0F;
            this.Line64.X2 = 9.999306F;
            this.Line64.Y1 = 3.484028F;
            this.Line64.Y2 = 3.484028F;
            // 
            // Label221
            // 
            this.Label221.Height = 0.1770833F;
            this.Label221.HyperLink = null;
            this.Label221.Left = 5.520833F;
            this.Label221.Name = "Label221";
            this.Label221.Style = "font-size: 6.5pt";
            this.Label221.Text = "Employer identification number (EIN)";
            this.Label221.Top = 3.1875F;
            this.Label221.Width = 2.21875F;
            // 
            // Label222
            // 
            this.Label222.Height = 0.1770833F;
            this.Label222.HyperLink = null;
            this.Label222.Left = 5.385417F;
            this.Label222.Name = "Label222";
            this.Label222.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label222.Text = "17";
            this.Label222.Top = 3.1875F;
            this.Label222.Width = 0.15625F;
            // 
            // Label223
            // 
            this.Label223.Height = 0.1770833F;
            this.Label223.HyperLink = null;
            this.Label223.Left = 7.708333F;
            this.Label223.Name = "Label223";
            this.Label223.Style = "font-size: 6.5pt";
            this.Label223.Text = "Contact telephone number";
            this.Label223.Top = 3.1875F;
            this.Label223.Width = 2.21875F;
            // 
            // Label224
            // 
            this.Label224.Height = 0.1770833F;
            this.Label224.HyperLink = null;
            this.Label224.Left = 7.572917F;
            this.Label224.Name = "Label224";
            this.Label224.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label224.Text = "18";
            this.Label224.Top = 3.1875F;
            this.Label224.Width = 0.15625F;
            // 
            // Line61
            // 
            this.Line61.Height = 0F;
            this.Line61.Left = 0F;
            this.Line61.LineWeight = 1F;
            this.Line61.Name = "Line61";
            this.Line61.Top = 3.166667F;
            this.Line61.Width = 10F;
            this.Line61.X1 = 0F;
            this.Line61.X2 = 10F;
            this.Line61.Y1 = 3.166667F;
            this.Line61.Y2 = 3.166667F;
            // 
            // Label225
            // 
            this.Label225.Height = 0.1770833F;
            this.Label225.HyperLink = null;
            this.Label225.Left = 5.520833F;
            this.Label225.Name = "Label225";
            this.Label225.Style = "font-size: 6.5pt";
            this.Label225.Text = "State or province";
            this.Label225.Top = 3.5F;
            this.Label225.Width = 2.21875F;
            // 
            // Label226
            // 
            this.Label226.Height = 0.1770833F;
            this.Label226.HyperLink = null;
            this.Label226.Left = 5.385417F;
            this.Label226.Name = "Label226";
            this.Label226.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label226.Text = "21";
            this.Label226.Top = 3.5F;
            this.Label226.Width = 0.15625F;
            // 
            // Line72
            // 
            this.Line72.Height = 0.34375F;
            this.Line72.Left = 3.5F;
            this.Line72.LineWeight = 0F;
            this.Line72.Name = "Line72";
            this.Line72.Top = 3.479167F;
            this.Line72.Width = 0F;
            this.Line72.X1 = 3.5F;
            this.Line72.X2 = 3.5F;
            this.Line72.Y1 = 3.479167F;
            this.Line72.Y2 = 3.822917F;
            // 
            // Label227
            // 
            this.Label227.Height = 0.1770833F;
            this.Label227.HyperLink = null;
            this.Label227.Left = 3.645833F;
            this.Label227.Name = "Label227";
            this.Label227.Style = "font-size: 6.5pt";
            this.Label227.Text = "City or town";
            this.Label227.Top = 3.5F;
            this.Label227.Width = 1.65625F;
            // 
            // Label228
            // 
            this.Label228.Height = 0.1770833F;
            this.Label228.HyperLink = null;
            this.Label228.Left = 3.510417F;
            this.Label228.Name = "Label228";
            this.Label228.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label228.Text = "20";
            this.Label228.Top = 3.5F;
            this.Label228.Width = 0.15625F;
            // 
            // Label229
            // 
            this.Label229.Height = 0.1770833F;
            this.Label229.HyperLink = null;
            this.Label229.Left = 7.708333F;
            this.Label229.Name = "Label229";
            this.Label229.Style = "font-size: 6.5pt";
            this.Label229.Text = "Country and ZIP or foreign postal code";
            this.Label229.Top = 3.5F;
            this.Label229.Width = 2.21875F;
            // 
            // Label230
            // 
            this.Label230.Height = 0.1770833F;
            this.Label230.HyperLink = null;
            this.Label230.Left = 7.572917F;
            this.Label230.Name = "Label230";
            this.Label230.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label230.Text = "22";
            this.Label230.Top = 3.5F;
            this.Label230.Width = 0.15625F;
            // 
            // Label231
            // 
            this.Label231.Height = 0.1770833F;
            this.Label231.HyperLink = null;
            this.Label231.Left = 3.25F;
            this.Label231.Name = "Label231";
            this.Label231.Style = "font-family: \'Arial\'; font-size: 8.5pt; font-weight: bold";
            this.Label231.Text = "Do not attach to your tax return. Keep for your records.";
            this.Label231.Top = 0.6145833F;
            this.Label231.Width = 3.96875F;
            // 
            // Label232
            // 
            this.Label232.Height = 0.1770833F;
            this.Label232.HyperLink = null;
            this.Label232.Left = 3.0625F;
            this.Label232.Name = "Label232";
            this.Label232.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label232.Text = "4";
            this.Label232.Top = 0.6145833F;
            this.Label232.Width = 0.3333333F;
            // 
            // Label233
            // 
            this.Label233.Height = 0.1770833F;
            this.Label233.HyperLink = null;
            this.Label233.Left = 7.760417F;
            this.Label233.Name = "Label233";
            this.Label233.Style = "font-size: 8pt";
            this.Label233.Text = "1095BIRS";
            this.Label233.Top = 7.447917F;
            this.Label233.Visible = false;
            this.Label233.Width = 0.78125F;
            // 
            // Line73
            // 
            this.Line73.Height = 2.989583F;
            this.Line73.Left = 1.25F;
            this.Line73.LineWeight = 1F;
            this.Line73.Name = "Line73";
            this.Line73.Top = 4.4375F;
            this.Line73.Width = 0F;
            this.Line73.X1 = 1.25F;
            this.Line73.X2 = 1.25F;
            this.Line73.Y1 = 4.4375F;
            this.Line73.Y2 = 7.427083F;
            // 
            // txtCoveredLast1
            // 
            this.txtCoveredLast1.CanGrow = false;
            this.txtCoveredLast1.Height = 0.1770833F;
            this.txtCoveredLast1.Left = 1.604167F;
            this.txtCoveredLast1.Name = "txtCoveredLast1";
            this.txtCoveredLast1.Text = null;
            this.txtCoveredLast1.Top = 4.597917F;
            this.txtCoveredLast1.Width = 0.8541667F;
            // 
            // txtCoveredLast2
            // 
            this.txtCoveredLast2.CanGrow = false;
            this.txtCoveredLast2.Height = 0.1770833F;
            this.txtCoveredLast2.Left = 1.604167F;
            this.txtCoveredLast2.Name = "txtCoveredLast2";
            this.txtCoveredLast2.Text = null;
            this.txtCoveredLast2.Top = 5.09375F;
            this.txtCoveredLast2.Width = 0.8541667F;
            // 
            // txtCoveredLast3
            // 
            this.txtCoveredLast3.CanGrow = false;
            this.txtCoveredLast3.Height = 0.1770833F;
            this.txtCoveredLast3.Left = 1.604167F;
            this.txtCoveredLast3.Name = "txtCoveredLast3";
            this.txtCoveredLast3.Text = null;
            this.txtCoveredLast3.Top = 5.59375F;
            this.txtCoveredLast3.Width = 0.8541667F;
            // 
            // txtCoveredLast4
            // 
            this.txtCoveredLast4.CanGrow = false;
            this.txtCoveredLast4.Height = 0.1770833F;
            this.txtCoveredLast4.Left = 1.604167F;
            this.txtCoveredLast4.Name = "txtCoveredLast4";
            this.txtCoveredLast4.Text = null;
            this.txtCoveredLast4.Top = 6.09375F;
            this.txtCoveredLast4.Width = 0.8541667F;
            // 
            // txtCoveredLast5
            // 
            this.txtCoveredLast5.CanGrow = false;
            this.txtCoveredLast5.Height = 0.1770833F;
            this.txtCoveredLast5.Left = 1.604167F;
            this.txtCoveredLast5.Name = "txtCoveredLast5";
            this.txtCoveredLast5.Text = null;
            this.txtCoveredLast5.Top = 6.59375F;
            this.txtCoveredLast5.Width = 0.8541667F;
            // 
            // txtCoveredLast6
            // 
            this.txtCoveredLast6.CanGrow = false;
            this.txtCoveredLast6.Height = 0.1770833F;
            this.txtCoveredLast6.Left = 1.604167F;
            this.txtCoveredLast6.Name = "txtCoveredLast6";
            this.txtCoveredLast6.Text = null;
            this.txtCoveredLast6.Top = 7.083333F;
            this.txtCoveredLast6.Width = 0.8541667F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.CanGrow = false;
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 1.291667F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 4.597917F;
            this.txtCoveredMiddle1.Width = 0.2916667F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.CanGrow = false;
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 1.291667F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 5.09375F;
            this.txtCoveredMiddle2.Width = 0.2916667F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.CanGrow = false;
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 1.291667F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 5.59375F;
            this.txtCoveredMiddle3.Width = 0.2916667F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.CanGrow = false;
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 1.291667F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 6.09375F;
            this.txtCoveredMiddle4.Width = 0.2916667F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.CanGrow = false;
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 1.291667F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 6.59375F;
            this.txtCoveredMiddle5.Width = 0.2916667F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.CanGrow = false;
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 1.291667F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 7.083333F;
            this.txtCoveredMiddle6.Width = 0.2916667F;
            // 
            // Line75
            // 
            this.Line75.Height = 0.2013888F;
            this.Line75.Left = 1.840278F;
            this.Line75.LineWeight = 1F;
            this.Line75.Name = "Line75";
            this.Line75.Top = 1.298611F;
            this.Line75.Width = 0F;
            this.Line75.X1 = 1.840278F;
            this.Line75.X2 = 1.840278F;
            this.Line75.Y1 = 1.298611F;
            this.Line75.Y2 = 1.5F;
            // 
            // Line76
            // 
            this.Line76.Height = 0.1979166F;
            this.Line76.Left = 3.625F;
            this.Line76.LineWeight = 1F;
            this.Line76.Name = "Line76";
            this.Line76.Top = 1.302083F;
            this.Line76.Width = 0F;
            this.Line76.X1 = 3.625F;
            this.Line76.X2 = 3.625F;
            this.Line76.Y1 = 1.302083F;
            this.Line76.Y2 = 1.5F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.CanGrow = false;
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.9375F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 1.291667F;
            this.txtMiddle.Width = 1.583333F;
            // 
            // txtLast
            // 
            this.txtLast.CanGrow = false;
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 3.6875F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Text = null;
            this.txtLast.Top = 1.291667F;
            this.txtLast.Width = 1.520833F;
            // 
            // Line74
            // 
            this.Line74.Height = 2.989583F;
            this.Line74.Left = 1.5625F;
            this.Line74.LineWeight = 1F;
            this.Line74.Name = "Line74";
            this.Line74.Top = 4.4375F;
            this.Line74.Width = 0F;
            this.Line74.X1 = 1.5625F;
            this.Line74.X2 = 1.5625F;
            this.Line74.Y1 = 4.4375F;
            this.Line74.Y2 = 7.427083F;
            // 
            // rpt1095B2018BlankPage1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 10F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label191)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label192)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label195)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label197)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label198)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label199)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label202)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label207)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label208)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label213)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label214)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label215)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label216)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label218)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label219)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label221)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label222)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label223)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label224)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label225)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label226)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label227)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label228)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label229)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label233)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderTelephone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginOfPolicy;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label165;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label166;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label167;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label168;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label169;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label170;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label171;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label172;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label173;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label174;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label181;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
        private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape43;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape44;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape45;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape46;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape69;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape70;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape71;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape72;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape73;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape74;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape75;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape76;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape79;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label183;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label184;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label185;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label186;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label187;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label188;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label189;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label155;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label190;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label191;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label192;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label193;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label194;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label195;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label196;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label197;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label198;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label199;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label200;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label201;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape82;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label202;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label203;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label204;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label205;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label207;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label208;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label210;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label211;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label213;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label214;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label215;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label216;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label218;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label219;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label220;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label221;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label222;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label223;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label224;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label225;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label226;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label227;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label228;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label229;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label230;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label231;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label232;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label233;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
    }
}
