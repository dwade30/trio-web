﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2016PortraitPage2.
	/// </summary>
	public partial class rpt1095B2018PortraitPage2 : BaseSectionReport
	{
		public rpt1095B2018PortraitPage2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt1095B2018PortraitPage2 InstancePtr
		{
			get
			{
				return (rpt1095B2018PortraitPage2)Sys.GetInstance(typeof(rpt1095B2018PortraitPage2));
			}
		}

		protected rpt1095B2018PortraitPage2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1095B2016PortraitPage2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolTestPrint;
		private bool boolMaskSSNs;
		private bool boolFirstRecord;
		private c1095BReport theReport;

		public void Init(ref c1095BReport reportObject, bool modalDialog)
		{
			theReport = reportObject;
			boolTestPrint = theReport.TestPrint;
			boolMaskSSNs = theReport.MaskSSNs;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", true, showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolTestPrint)
			{
				c1095B employeerecord;
				// skip any that don't have overflow pages
				while (theReport.ListOfForms.IsCurrent())
				{
					employeerecord = (c1095B)theReport.ListOfForms.GetCurrentItem();
					if (employeerecord.coveredIndividuals.Count > 6)
					{
						break;
					}
					theReport.ListOfForms.MoveNext();
				}
				eArgs.EOF = !theReport.ListOfForms.IsCurrent();
			}
			else
			{
				eArgs.EOF = !boolFirstRecord;
				boolFirstRecord = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (!theReport.TestPrint)
			{
				theReport.ListOfForms.MoveFirst();
			}
			boolFirstRecord = true;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void ClearFields()
		{
			int x;
			int intLine;
			txtName.Text = "";
            txtMiddle.Text = "";
            txtLast.Text = "";
			txtSSN.Text = "";
			for (intLine = 1; intLine <= 12; intLine++)
			{
				(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredLastName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				for (x = 1; x <= 12; x++)
				{
					(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			int intLine;
			string strTemp = "";
			double dblTemp;
			int intCoveredCount = 0;
			ClearFields();
			if (!boolTestPrint)
			{
				c1095B employeerecord;
				cACAEmployeeDependent coveredIndividual;
				if (theReport.ListOfForms.IsCurrent())
				{
					employeerecord = (c1095B)theReport.ListOfForms.GetCurrentItem();
					txtName.Text = employeerecord.EmployeeFirstName;
                    txtMiddle.Text = employeerecord.EmployeeMiddleName;
                    txtLast.Text = employeerecord.EmployeeLastName;
					if (!theReport.MaskSSNs)
					{
						txtSSN.Text = employeerecord.SSN;
					}
					else
					{
						if (employeerecord.SSN.Length >= 4)
						{
							txtSSN.Text = "XXX-XX-" + Strings.Right(employeerecord.SSN, 4);
						}
					}
					intLine = 0;
					intCoveredCount = employeerecord.coveredIndividuals.Count;
					for (intLine = 7; intLine <= intCoveredCount; intLine++)
					{
						if (intLine > 18)
							break;
						coveredIndividual = employeerecord.coveredIndividuals[intLine];
						if (Information.IsDate(coveredIndividual.DateOfBirth) && coveredIndividual.SSN == "")
						{
							(this.Detail.Controls["txtCoveredDOB" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Year) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Day), 2);
						}
						(this.Detail.Controls["txtCoveredName" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.FirstName;
                        (this.Detail.Controls["txtCoveredMiddle" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.MiddleName;
                        (this.Detail.Controls["txtCoveredLastName" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.LastName;
                        strTemp = coveredIndividual.SSN;
						if (theReport.MaskSSNs)
						{
							if (strTemp.Length >= 4)
							{
								strTemp = "XXX-XX-" + Strings.Right(strTemp, 4);
							}
						}
						(this.Detail.Controls["txtCoveredSSN" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
						if (coveredIndividual.CoveredAll12Months)
						{
							(this.Detail.Controls["txtCoveredAll12_" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
						}
						else
						{
							for (x = 1; x <= 12; x++)
							{
								if (coveredIndividual.GetIsCoveredForMonth(x))
								{
									(this.Detail.Controls["txtCoveredMonth" + x + "_" + (intLine - 6)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
								}
							}
						}
					}
					// intLine
					theReport.ListOfForms.MoveNext();
				}
			}
			else
			{
				txtName.Text = "First";
                txtMiddle.Text = "Middle";
                txtLast.Text = "Last";
				txtSSN.Text = "000-00-0000";
				for (intLine = 1; intLine <= 12; intLine++)
				{
					(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "0000-00-00";
					(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "First";
                    (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "M";
                    (this.Detail.Controls["txtCoveredLastName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Last " + FCConvert.ToString(intLine + 6);
                    (this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "000-00-0000";
					for (x = 1; x <= 12; x++)
					{
						(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					}
				}
			}
		}

		private void rpt1095B2016PortraitPage2_Load(object sender, System.EventArgs e)
		{

		}
	}
}
