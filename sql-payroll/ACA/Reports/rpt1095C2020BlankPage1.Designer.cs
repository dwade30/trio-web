﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2017BlankPage1.
	/// </summary>
	partial class rpt1095C2020BlankPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2020BlankPage1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox14_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox15_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Box16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBox16_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label139 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label144 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label145 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label146 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label147 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label148 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label149 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label150 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label153 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label154 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label156 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label157 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label158 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label160 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label161 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label162 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label163 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label164 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Zip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAge,
            this.label2,
            this.Label107,
            this.Label117,
            this.Label151,
            this.Label119,
            this.Label120,
            this.Label121,
            this.Label122,
            this.Label123,
            this.Label124,
            this.Label125,
            this.Label126,
            this.Label127,
            this.Label128,
            this.Label129,
            this.txtName,
            this.txtSSN,
            this.txtEmployer,
            this.txtEIN,
            this.txtAddress,
            this.txtEmployerAddress,
            this.txtPhone,
            this.txtStartMonth,
            this.txtAll12Box14,
            this.txtBox14_1,
            this.txtBox14_2,
            this.txtBox14_3,
            this.txtBox14_4,
            this.txtBox14_5,
            this.txtBox14_6,
            this.txtBox14_7,
            this.txtBox14_8,
            this.txtBox14_9,
            this.txtBox14_10,
            this.txtBox14_11,
            this.txtBox14_12,
            this.txtAll12Box15,
            this.txtBox15_1,
            this.txtBox15_2,
            this.txtBox15_3,
            this.txtBox15_4,
            this.txtBox15_5,
            this.txtBox15_6,
            this.txtBox15_7,
            this.txtBox15_8,
            this.txtBox15_9,
            this.txtBox15_10,
            this.txtBox15_11,
            this.txtBox15_12,
            this.txtAll12Box16,
            this.txtBox16_1,
            this.txtBox16_2,
            this.txtBox16_3,
            this.txtBox16_4,
            this.txtBox16_5,
            this.txtBox16_6,
            this.txtBox16_7,
            this.txtBox16_8,
            this.txtBox16_9,
            this.txtBox16_10,
            this.txtBox16_11,
            this.txtBox16_12,
            this.txtCity,
            this.txtState,
            this.txtPostalCode,
            this.txtEmployerCity,
            this.txtEmployerState,
            this.txtEmployerPostalCode,
            this.Image2,
            this.Label77,
            this.Label78,
            this.Label83,
            this.Label84,
            this.Line3,
            this.Line4,
            this.Label85,
            this.Label86,
            this.Label87,
            this.Line6,
            this.Line10,
            this.Label88,
            this.Line11,
            this.Line12,
            this.Label91,
            this.Label92,
            this.Line14,
            this.Label93,
            this.Label94,
            this.Line38,
            this.Line51,
            this.Line54,
            this.Line55,
            this.Label96,
            this.Shape2,
            this.Shape3,
            this.Label97,
            this.Label98,
            this.Line56,
            this.Label99,
            this.Label100,
            this.Label101,
            this.Label102,
            this.Label104,
            this.Label105,
            this.Label106,
            this.Label108,
            this.Label109,
            this.Label110,
            this.Label111,
            this.Label112,
            this.Label113,
            this.Label114,
            this.Label115,
            this.Label116,
            this.Label138,
            this.Label139,
            this.Label140,
            this.Label141,
            this.Label142,
            this.Label143,
            this.Label144,
            this.Label145,
            this.Label146,
            this.Label147,
            this.Label148,
            this.Label149,
            this.Label150,
            this.Line40,
            this.Line41,
            this.Line42,
            this.Line43,
            this.Line47,
            this.Line48,
            this.Line49,
            this.Line50,
            this.Line45,
            this.Line44,
            this.Label34,
            this.Label33,
            this.Label152,
            this.Label153,
            this.Label154,
            this.Label155,
            this.Label156,
            this.Label157,
            this.Line53,
            this.Label158,
            this.Label159,
            this.Label160,
            this.Label161,
            this.Label162,
            this.Label163,
            this.Label164,
            this.Label32,
            this.Label177,
            this.Label179,
            this.Label180,
            this.Label182,
            this.Line1,
            this.Line39,
            this.Line8,
            this.Line7,
            this.Line52,
            this.Label183,
            this.Label184,
            this.Line58,
            this.Line5,
            this.Line61,
            this.Line62,
            this.txtMiddle,
            this.txtLast,
            this.Line9,
            this.txtAll12Zip,
            this.txtZip_1,
            this.txtZip_2,
            this.txtZip_3,
            this.txtZip_4,
            this.txtZip_5,
            this.txtZip_6,
            this.txtZip_7,
            this.txtZip_8,
            this.txtZip_9,
            this.txtZip_10,
            this.txtZip_11,
            this.txtZip_12,
            this.line2,
            this.label1,
            this.Line13,
            this.Label181,
            this.line15});
            this.Detail.Height = 7.708333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.08350006F;
            this.txtName.Name = "txtName";
            this.txtName.Text = "Name of Employee";
            this.txtName.Top = 1.564167F;
            this.txtName.Width = 1.395833F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 3.3335F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 1.564167F;
            this.txtSSN.Width = 1.666667F;
            // 
            // txtEmployer
            // 
            this.txtEmployer.Height = 0.1770833F;
            this.txtEmployer.Left = 5.0835F;
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Text = null;
            this.txtEmployer.Top = 1.564167F;
            this.txtEmployer.Width = 3.083333F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 8.36475F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 1.564167F;
            this.txtEIN.Width = 1.666667F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.08350006F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 1.8975F;
            this.txtAddress.Width = 4.833333F;
            // 
            // txtEmployerAddress
            // 
            this.txtEmployerAddress.Height = 0.1770833F;
            this.txtEmployerAddress.Left = 5.0835F;
            this.txtEmployerAddress.Name = "txtEmployerAddress";
            this.txtEmployerAddress.Text = null;
            this.txtEmployerAddress.Top = 1.8975F;
            this.txtEmployerAddress.Width = 3.083333F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1770833F;
            this.txtPhone.Left = 8.36475F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 1.8975F;
            this.txtPhone.Width = 1.604167F;
            // 
            // txtStartMonth
            // 
            this.txtStartMonth.Height = 0.1770833F;
            this.txtStartMonth.Left = 9.323001F;
            this.txtStartMonth.Name = "txtStartMonth";
            this.txtStartMonth.Text = null;
            this.txtStartMonth.Top = 2.408F;
            this.txtStartMonth.Width = 0.3979995F;
            // 
            // txtAll12Box14
            // 
            this.txtAll12Box14.Height = 0.1770833F;
            this.txtAll12Box14.Left = 1.111278F;
            this.txtAll12Box14.Name = "txtAll12Box14";
            this.txtAll12Box14.Text = null;
            this.txtAll12Box14.Top = 2.855833F;
            this.txtAll12Box14.Width = 0.5F;
            // 
            // txtBox14_1
            // 
            this.txtBox14_1.Height = 0.1770833F;
            this.txtBox14_1.Left = 1.801556F;
            this.txtBox14_1.Name = "txtBox14_1";
            this.txtBox14_1.Text = null;
            this.txtBox14_1.Top = 2.855833F;
            this.txtBox14_1.Width = 0.5F;
            // 
            // txtBox14_2
            // 
            this.txtBox14_2.Height = 0.1770833F;
            this.txtBox14_2.Left = 2.491834F;
            this.txtBox14_2.Name = "txtBox14_2";
            this.txtBox14_2.Text = null;
            this.txtBox14_2.Top = 2.855833F;
            this.txtBox14_2.Width = 0.5F;
            // 
            // txtBox14_3
            // 
            this.txtBox14_3.Height = 0.1770833F;
            this.txtBox14_3.Left = 3.182111F;
            this.txtBox14_3.Name = "txtBox14_3";
            this.txtBox14_3.Text = null;
            this.txtBox14_3.Top = 2.855833F;
            this.txtBox14_3.Width = 0.5F;
            // 
            // txtBox14_4
            // 
            this.txtBox14_4.Height = 0.1770833F;
            this.txtBox14_4.Left = 3.872389F;
            this.txtBox14_4.Name = "txtBox14_4";
            this.txtBox14_4.Text = null;
            this.txtBox14_4.Top = 2.855833F;
            this.txtBox14_4.Width = 0.5F;
            // 
            // txtBox14_5
            // 
            this.txtBox14_5.Height = 0.1770833F;
            this.txtBox14_5.Left = 4.562667F;
            this.txtBox14_5.Name = "txtBox14_5";
            this.txtBox14_5.Text = null;
            this.txtBox14_5.Top = 2.855833F;
            this.txtBox14_5.Width = 0.5F;
            // 
            // txtBox14_6
            // 
            this.txtBox14_6.Height = 0.1770833F;
            this.txtBox14_6.Left = 5.252945F;
            this.txtBox14_6.Name = "txtBox14_6";
            this.txtBox14_6.Text = null;
            this.txtBox14_6.Top = 2.855833F;
            this.txtBox14_6.Width = 0.5F;
            // 
            // txtBox14_7
            // 
            this.txtBox14_7.Height = 0.1770833F;
            this.txtBox14_7.Left = 5.964056F;
            this.txtBox14_7.Name = "txtBox14_7";
            this.txtBox14_7.Text = null;
            this.txtBox14_7.Top = 2.855833F;
            this.txtBox14_7.Width = 0.5F;
            // 
            // txtBox14_8
            // 
            this.txtBox14_8.Height = 0.1770833F;
            this.txtBox14_8.Left = 6.6335F;
            this.txtBox14_8.Name = "txtBox14_8";
            this.txtBox14_8.Text = null;
            this.txtBox14_8.Top = 2.855833F;
            this.txtBox14_8.Width = 0.5F;
            // 
            // txtBox14_9
            // 
            this.txtBox14_9.Height = 0.1770833F;
            this.txtBox14_9.Left = 7.323778F;
            this.txtBox14_9.Name = "txtBox14_9";
            this.txtBox14_9.Text = null;
            this.txtBox14_9.Top = 2.855833F;
            this.txtBox14_9.Width = 0.5F;
            // 
            // txtBox14_10
            // 
            this.txtBox14_10.Height = 0.1770833F;
            this.txtBox14_10.Left = 8.014056F;
            this.txtBox14_10.Name = "txtBox14_10";
            this.txtBox14_10.Text = null;
            this.txtBox14_10.Top = 2.855833F;
            this.txtBox14_10.Width = 0.5F;
            // 
            // txtBox14_11
            // 
            this.txtBox14_11.Height = 0.1770833F;
            this.txtBox14_11.Left = 8.704333F;
            this.txtBox14_11.Name = "txtBox14_11";
            this.txtBox14_11.Text = null;
            this.txtBox14_11.Top = 2.855833F;
            this.txtBox14_11.Width = 0.5F;
            // 
            // txtBox14_12
            // 
            this.txtBox14_12.Height = 0.1770833F;
            this.txtBox14_12.Left = 9.394611F;
            this.txtBox14_12.Name = "txtBox14_12";
            this.txtBox14_12.Text = null;
            this.txtBox14_12.Top = 2.855833F;
            this.txtBox14_12.Width = 0.5F;
            // 
            // txtAll12Box15
            // 
            this.txtAll12Box15.Height = 0.1770833F;
            this.txtAll12Box15.Left = 1.187667F;
            this.txtAll12Box15.Name = "txtAll12Box15";
            this.txtAll12Box15.Text = null;
            this.txtAll12Box15.Top = 3.42875F;
            this.txtAll12Box15.Width = 0.5F;
            // 
            // txtBox15_1
            // 
            this.txtBox15_1.Height = 0.1770833F;
            this.txtBox15_1.Left = 1.877945F;
            this.txtBox15_1.Name = "txtBox15_1";
            this.txtBox15_1.Text = null;
            this.txtBox15_1.Top = 3.42875F;
            this.txtBox15_1.Width = 0.5F;
            // 
            // txtBox15_2
            // 
            this.txtBox15_2.Height = 0.1770833F;
            this.txtBox15_2.Left = 2.568223F;
            this.txtBox15_2.Name = "txtBox15_2";
            this.txtBox15_2.Text = null;
            this.txtBox15_2.Top = 3.42875F;
            this.txtBox15_2.Width = 0.5F;
            // 
            // txtBox15_3
            // 
            this.txtBox15_3.Height = 0.1770833F;
            this.txtBox15_3.Left = 3.2585F;
            this.txtBox15_3.Name = "txtBox15_3";
            this.txtBox15_3.Text = null;
            this.txtBox15_3.Top = 3.42875F;
            this.txtBox15_3.Width = 0.5F;
            // 
            // txtBox15_4
            // 
            this.txtBox15_4.Height = 0.1770833F;
            this.txtBox15_4.Left = 3.948778F;
            this.txtBox15_4.Name = "txtBox15_4";
            this.txtBox15_4.Text = null;
            this.txtBox15_4.Top = 3.42875F;
            this.txtBox15_4.Width = 0.5F;
            // 
            // txtBox15_5
            // 
            this.txtBox15_5.Height = 0.1770833F;
            this.txtBox15_5.Left = 4.639056F;
            this.txtBox15_5.Name = "txtBox15_5";
            this.txtBox15_5.Text = null;
            this.txtBox15_5.Top = 3.42875F;
            this.txtBox15_5.Width = 0.5F;
            // 
            // txtBox15_6
            // 
            this.txtBox15_6.Height = 0.1770833F;
            this.txtBox15_6.Left = 5.329334F;
            this.txtBox15_6.Name = "txtBox15_6";
            this.txtBox15_6.Text = null;
            this.txtBox15_6.Top = 3.42875F;
            this.txtBox15_6.Width = 0.5F;
            // 
            // txtBox15_7
            // 
            this.txtBox15_7.Height = 0.1770833F;
            this.txtBox15_7.Left = 6.019611F;
            this.txtBox15_7.Name = "txtBox15_7";
            this.txtBox15_7.Text = null;
            this.txtBox15_7.Top = 3.42875F;
            this.txtBox15_7.Width = 0.5F;
            // 
            // txtBox15_8
            // 
            this.txtBox15_8.Height = 0.1770833F;
            this.txtBox15_8.Left = 6.709889F;
            this.txtBox15_8.Name = "txtBox15_8";
            this.txtBox15_8.Text = null;
            this.txtBox15_8.Top = 3.42875F;
            this.txtBox15_8.Width = 0.5F;
            // 
            // txtBox15_9
            // 
            this.txtBox15_9.Height = 0.1770833F;
            this.txtBox15_9.Left = 7.400167F;
            this.txtBox15_9.Name = "txtBox15_9";
            this.txtBox15_9.Text = null;
            this.txtBox15_9.Top = 3.42875F;
            this.txtBox15_9.Width = 0.5F;
            // 
            // txtBox15_10
            // 
            this.txtBox15_10.Height = 0.1770833F;
            this.txtBox15_10.Left = 8.090445F;
            this.txtBox15_10.Name = "txtBox15_10";
            this.txtBox15_10.Text = null;
            this.txtBox15_10.Top = 3.42875F;
            this.txtBox15_10.Width = 0.5F;
            // 
            // txtBox15_11
            // 
            this.txtBox15_11.Height = 0.1770833F;
            this.txtBox15_11.Left = 8.780723F;
            this.txtBox15_11.Name = "txtBox15_11";
            this.txtBox15_11.Text = null;
            this.txtBox15_11.Top = 3.42875F;
            this.txtBox15_11.Width = 0.5F;
            // 
            // txtBox15_12
            // 
            this.txtBox15_12.Height = 0.1770833F;
            this.txtBox15_12.Left = 9.471001F;
            this.txtBox15_12.Name = "txtBox15_12";
            this.txtBox15_12.Text = null;
            this.txtBox15_12.Top = 3.42875F;
            this.txtBox15_12.Width = 0.5F;
            // 
            // txtAll12Box16
            // 
            this.txtAll12Box16.Height = 0.1770833F;
            this.txtAll12Box16.Left = 1.11475F;
            this.txtAll12Box16.Name = "txtAll12Box16";
            this.txtAll12Box16.Text = null;
            this.txtAll12Box16.Top = 3.814167F;
            this.txtAll12Box16.Width = 0.5F;
            // 
            // txtBox16_1
            // 
            this.txtBox16_1.Height = 0.1770833F;
            this.txtBox16_1.Left = 1.80225F;
            this.txtBox16_1.Name = "txtBox16_1";
            this.txtBox16_1.Text = null;
            this.txtBox16_1.Top = 3.814167F;
            this.txtBox16_1.Width = 0.5F;
            // 
            // txtBox16_2
            // 
            this.txtBox16_2.Height = 0.1770833F;
            this.txtBox16_2.Left = 2.48975F;
            this.txtBox16_2.Name = "txtBox16_2";
            this.txtBox16_2.Text = null;
            this.txtBox16_2.Top = 3.814167F;
            this.txtBox16_2.Width = 0.5F;
            // 
            // txtBox16_3
            // 
            this.txtBox16_3.Height = 0.1770833F;
            this.txtBox16_3.Left = 3.17725F;
            this.txtBox16_3.Name = "txtBox16_3";
            this.txtBox16_3.Text = null;
            this.txtBox16_3.Top = 3.814167F;
            this.txtBox16_3.Width = 0.5F;
            // 
            // txtBox16_4
            // 
            this.txtBox16_4.Height = 0.1770833F;
            this.txtBox16_4.Left = 3.875167F;
            this.txtBox16_4.Name = "txtBox16_4";
            this.txtBox16_4.Text = null;
            this.txtBox16_4.Top = 3.814167F;
            this.txtBox16_4.Width = 0.5F;
            // 
            // txtBox16_5
            // 
            this.txtBox16_5.Height = 0.1770833F;
            this.txtBox16_5.Left = 4.562667F;
            this.txtBox16_5.Name = "txtBox16_5";
            this.txtBox16_5.Text = null;
            this.txtBox16_5.Top = 3.814167F;
            this.txtBox16_5.Width = 0.5F;
            // 
            // txtBox16_6
            // 
            this.txtBox16_6.Height = 0.1770833F;
            this.txtBox16_6.Left = 5.250167F;
            this.txtBox16_6.Name = "txtBox16_6";
            this.txtBox16_6.Text = null;
            this.txtBox16_6.Top = 3.814167F;
            this.txtBox16_6.Width = 0.5F;
            // 
            // txtBox16_7
            // 
            this.txtBox16_7.Height = 0.1770833F;
            this.txtBox16_7.Left = 5.968917F;
            this.txtBox16_7.Name = "txtBox16_7";
            this.txtBox16_7.Text = null;
            this.txtBox16_7.Top = 3.814167F;
            this.txtBox16_7.Width = 0.5F;
            // 
            // txtBox16_8
            // 
            this.txtBox16_8.Height = 0.1770833F;
            this.txtBox16_8.Left = 6.635584F;
            this.txtBox16_8.Name = "txtBox16_8";
            this.txtBox16_8.Text = null;
            this.txtBox16_8.Top = 3.814167F;
            this.txtBox16_8.Width = 0.5F;
            // 
            // txtBox16_9
            // 
            this.txtBox16_9.Height = 0.1770833F;
            this.txtBox16_9.Left = 7.323084F;
            this.txtBox16_9.Name = "txtBox16_9";
            this.txtBox16_9.Text = null;
            this.txtBox16_9.Top = 3.814167F;
            this.txtBox16_9.Width = 0.5F;
            // 
            // txtBox16_10
            // 
            this.txtBox16_10.Height = 0.1770833F;
            this.txtBox16_10.Left = 8.010584F;
            this.txtBox16_10.Name = "txtBox16_10";
            this.txtBox16_10.Text = null;
            this.txtBox16_10.Top = 3.814167F;
            this.txtBox16_10.Width = 0.5F;
            // 
            // txtBox16_11
            // 
            this.txtBox16_11.Height = 0.1770833F;
            this.txtBox16_11.Left = 8.7085F;
            this.txtBox16_11.Name = "txtBox16_11";
            this.txtBox16_11.Text = null;
            this.txtBox16_11.Top = 3.814167F;
            this.txtBox16_11.Width = 0.5F;
            // 
            // txtBox16_12
            // 
            this.txtBox16_12.Height = 0.1770833F;
            this.txtBox16_12.Left = 9.396F;
            this.txtBox16_12.Name = "txtBox16_12";
            this.txtBox16_12.Text = null;
            this.txtBox16_12.Top = 3.814167F;
            this.txtBox16_12.Width = 0.5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 0.08350006F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "white-space: nowrap";
            this.txtCity.Text = null;
            this.txtCity.Top = 2.220417F;
            this.txtCity.Width = 1.416667F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 1.5835F;
            this.txtState.Name = "txtState";
            this.txtState.Text = null;
            this.txtState.Top = 2.220417F;
            this.txtState.Width = 1.583333F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 3.3335F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 2.216944F;
            this.txtPostalCode.Width = 1.666667F;
            // 
            // txtEmployerCity
            // 
            this.txtEmployerCity.Height = 0.1770833F;
            this.txtEmployerCity.Left = 5.0835F;
            this.txtEmployerCity.Name = "txtEmployerCity";
            this.txtEmployerCity.Text = null;
            this.txtEmployerCity.Top = 2.220417F;
            this.txtEmployerCity.Width = 1.354167F;
            // 
            // txtEmployerState
            // 
            this.txtEmployerState.Height = 0.1770833F;
            this.txtEmployerState.Left = 6.562667F;
            this.txtEmployerState.Name = "txtEmployerState";
            this.txtEmployerState.Text = null;
            this.txtEmployerState.Top = 2.220417F;
            this.txtEmployerState.Width = 1.583333F;
            // 
            // txtEmployerPostalCode
            // 
            this.txtEmployerPostalCode.Height = 0.1770833F;
            this.txtEmployerPostalCode.Left = 8.36475F;
            this.txtEmployerPostalCode.Name = "txtEmployerPostalCode";
            this.txtEmployerPostalCode.Text = null;
            this.txtEmployerPostalCode.Top = 2.220417F;
            this.txtEmployerPostalCode.Width = 1.604167F;
            // 
            // Image2
            // 
            this.Image2.Height = 0.2395833F;
            this.Image2.HyperLink = null;
            this.Image2.ImageData = ((System.IO.Stream)(resources.GetObject("Image2.ImageData")));
            this.Image2.Left = 8.875167F;
            this.Image2.LineWeight = 1F;
            this.Image2.Name = "Image2";
            this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image2.Top = 0.96F;
            this.Image2.Width = 0.7291667F;
            // 
            // Label77
            // 
            this.Label77.Height = 0.1770833F;
            this.Label77.HyperLink = null;
            this.Label77.Left = 0.0001667142F;
            this.Label77.Name = "Label77";
            this.Label77.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" +
    "-size: 10pt; font-weight: bold; vertical-align: middle";
            this.Label77.Text = "Part I";
            this.Label77.Top = 1.282917F;
            this.Label77.Width = 0.53125F;
            // 
            // Label78
            // 
            this.Label78.Height = 0.1770833F;
            this.Label78.HyperLink = null;
            this.Label78.Left = 0.6355834F;
            this.Label78.Name = "Label78";
            this.Label78.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label78.Text = "Employee";
            this.Label78.Top = 1.282917F;
            this.Label78.Width = 0.9479167F;
            // 
            // Label83
            // 
            this.Label83.Height = 0.3125F;
            this.Label83.HyperLink = null;
            this.Label83.Left = 0.3126667F;
            this.Label83.Name = "Label83";
            this.Label83.Style = "font-family: \'Impact\'; font-size: 20.5pt";
            this.Label83.Text = "1095-C";
            this.Label83.Top = 0.585F;
            this.Label83.Width = 0.9583333F;
            // 
            // Label84
            // 
            this.Label84.Height = 0.1666667F;
            this.Label84.HyperLink = null;
            this.Label84.Left = 0.0001667142F;
            this.Label84.Name = "Label84";
            this.Label84.Style = "font-size: 8pt";
            this.Label84.Text = "Form";
            this.Label84.Top = 0.7516667F;
            this.Label84.Width = 0.5F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.6840281F;
            this.Line3.Left = 1.635584F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.5988889F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 1.635584F;
            this.Line3.X2 = 1.635584F;
            this.Line3.Y1 = 0.5988889F;
            this.Line3.Y2 = 1.282917F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.6840281F;
            this.Line4.Left = 8.500167F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.5988889F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 8.500167F;
            this.Line4.X2 = 8.500167F;
            this.Line4.Y1 = 0.5988889F;
            this.Line4.Y2 = 1.282917F;
            // 
            // Label85
            // 
            this.Label85.Height = 0.1666667F;
            this.Label85.HyperLink = null;
            this.Label85.Left = 0.0001667142F;
            this.Label85.Name = "Label85";
            this.Label85.Style = "font-size: 8pt";
            this.Label85.Text = "Department of the Treasury";
            this.Label85.Top = 0.9183334F;
            this.Label85.Width = 1.625F;
            // 
            // Label86
            // 
            this.Label86.Height = 0.1666667F;
            this.Label86.HyperLink = null;
            this.Label86.Left = 0.0001667142F;
            this.Label86.Name = "Label86";
            this.Label86.Style = "font-size: 8pt";
            this.Label86.Text = "Internal Revenue Service";
            this.Label86.Top = 1.085F;
            this.Label86.Width = 1.625F;
            // 
            // Label87
            // 
            this.Label87.Height = 0.1770833F;
            this.Label87.HyperLink = null;
            this.Label87.Left = 5.375167F;
            this.Label87.Name = "Label87";
            this.Label87.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label87.Text = "Applicable Large Employer Member (Employer)";
            this.Label87.Top = 1.282917F;
            this.Label87.Width = 3.635417F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.802083F;
            this.Line6.Left = 5.000167F;
            this.Line6.LineWeight = 2F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 1.282917F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 5.000167F;
            this.Line6.X2 = 5.000167F;
            this.Line6.Y1 = 1.282917F;
            this.Line6.Y2 = 2.085F;
            // 
            // Line9
            // 
            this.Line9.Height = 0F;
            this.Line9.Left = 0.0001667142F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 2.585F;
            this.Line9.Width = 9.999306F;
            this.Line9.X1 = 0.0001667142F;
            this.Line9.X2 = 9.999473F;
            this.Line9.Y1 = 2.585F;
            this.Line9.Y2 = 2.585F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 1.021F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 2.793333F;
            this.Line10.Width = 8.978473F;
            this.Line10.X1 = 1.021F;
            this.Line10.X2 = 9.999473F;
            this.Line10.Y1 = 2.793333F;
            this.Line10.Y2 = 2.793333F;
            // 
            // Label88
            // 
            this.Label88.Height = 0.1770833F;
            this.Label88.HyperLink = null;
            this.Label88.Left = 0.0001667142F;
            this.Label88.Name = "Label88";
            this.Label88.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" +
    "-size: 10pt; font-weight: bold; vertical-align: middle";
            this.Label88.Text = "Part II";
            this.Label88.Top = 2.407222F;
            this.Label88.Width = 0.53125F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.0001667142F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 3.1475F;
            this.Line11.Width = 9.999306F;
            this.Line11.X1 = 0.0001667142F;
            this.Line11.X2 = 9.999473F;
            this.Line11.Y1 = 3.1475F;
            this.Line11.Y2 = 3.1475F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0.0001667142F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 3.6475F;
            this.Line12.Width = 9.999306F;
            this.Line12.X1 = 0.0001667142F;
            this.Line12.X2 = 9.999473F;
            this.Line12.Y1 = 3.6475F;
            this.Line12.Y2 = 3.6475F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 0.0001667142F;
            this.Line13.LineWeight = 2F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 4.148003F;
            this.Line13.Width = 9.999306F;
            this.Line13.X1 = 0.0001667142F;
            this.Line13.X2 = 9.999473F;
            this.Line13.Y1 = 4.148003F;
            this.Line13.Y2 = 4.148003F;
            // 
            // Label91
            // 
            this.Label91.Height = 0.1770833F;
            this.Label91.HyperLink = null;
            this.Label91.Left = 0.6564167F;
            this.Label91.Name = "Label91";
            this.Label91.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label91.Text = "Employee Offer and Coverage";
            this.Label91.Top = 2.407917F;
            this.Label91.Width = 2.260417F;
            // 
            // Label92
            // 
            this.Label92.Height = 0.2395833F;
            this.Label92.HyperLink = null;
            this.Label92.Left = 1.937667F;
            this.Label92.Name = "Label92";
            this.Label92.Style = "font-family: \'Franklin Gothic Medium\'; font-size: 14.5pt; font-weight: bold; text" +
    "-align: left";
            this.Label92.Text = "Employer-Provided Health Insurance Offer and Coverage";
            this.Label92.Top = 0.585F;
            this.Label92.Width = 5.395833F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.322F;
            this.Line14.Left = 5.000167F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 2.085F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 5.000167F;
            this.Line14.X2 = 5.000167F;
            this.Line14.Y1 = 2.085F;
            this.Line14.Y2 = 2.407F;
            // 
            // Label93
            // 
            this.Label93.Height = 0.1770833F;
            this.Label93.HyperLink = null;
            this.Label93.Left = 6.593834F;
            this.Label93.Name = "Label93";
            this.Label93.Style = "font-family: \'Arial\'; font-weight: bold";
            this.Label93.Text = "Plan Start Month";
            this.Label93.Top = 2.408F;
            this.Label93.Width = 1.260417F;
            // 
            // Label94
            // 
            this.Label94.Height = 0.1770833F;
            this.Label94.HyperLink = null;
            this.Label94.Left = 7.823001F;
            this.Label94.Name = "Label94";
            this.Label94.Style = "font-family: \'Arial\'";
            this.Label94.Text = "(Enter 2-digit number):";
            this.Label94.Top = 2.408F;
            this.Label94.Width = 1.510417F;
            // 
            // Line38
            // 
            this.Line38.Height = 2.063004F;
            this.Line38.Left = 1.021F;
            this.Line38.LineWeight = 1F;
            this.Line38.Name = "Line38";
            this.Line38.Top = 2.585F;
            this.Line38.Width = 0F;
            this.Line38.X1 = 1.021F;
            this.Line38.X2 = 1.021F;
            this.Line38.Y1 = 2.585F;
            this.Line38.Y2 = 4.648005F;
            // 
            // Line51
            // 
            this.Line51.Height = 0.3125F;
            this.Line51.Left = 3.187667F;
            this.Line51.LineWeight = 1F;
            this.Line51.Name = "Line51";
            this.Line51.Top = 1.46F;
            this.Line51.Width = 0F;
            this.Line51.X1 = 3.187667F;
            this.Line51.X2 = 3.187667F;
            this.Line51.Y1 = 1.46F;
            this.Line51.Y2 = 1.7725F;
            // 
            // Line54
            // 
            this.Line54.Height = 0.322222F;
            this.Line54.Left = 3.187667F;
            this.Line54.LineWeight = 1F;
            this.Line54.Name = "Line54";
            this.Line54.Top = 2.085F;
            this.Line54.Width = 0F;
            this.Line54.X1 = 3.187667F;
            this.Line54.X2 = 3.187667F;
            this.Line54.Y1 = 2.085F;
            this.Line54.Y2 = 2.407222F;
            // 
            // Line55
            // 
            this.Line55.Height = 2.563F;
            this.Line55.Left = 6.500167F;
            this.Line55.LineWeight = 1F;
            this.Line55.Name = "Line55";
            this.Line55.Top = 2.085F;
            this.Line55.Width = 0F;
            this.Line55.X1 = 6.500167F;
            this.Line55.X2 = 6.500167F;
            this.Line55.Y1 = 2.085F;
            this.Line55.Y2 = 4.648F;
            // 
            // Label96
            // 
            this.Label96.Height = 0.1979167F;
            this.Label96.HyperLink = null;
            this.Label96.Left = 9.229334F;
            this.Label96.Name = "Label96";
            this.Label96.Style = "font-family: \'OCR A Extended\'; font-size: 12pt; text-align: right";
            this.Label96.Text = "600120";
            this.Label96.Top = 0.21F;
            this.Label96.Width = 0.6875F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.1770833F;
            this.Shape2.Left = 7.312667F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = 0.585F;
            this.Shape2.Width = 0.1770833F;
            // 
            // Shape3
            // 
            this.Shape3.Height = 0.1770833F;
            this.Shape3.Left = 7.312667F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Top = 0.8975F;
            this.Shape3.Width = 0.1770833F;
            // 
            // Label97
            // 
            this.Label97.Height = 0.1666667F;
            this.Label97.HyperLink = null;
            this.Label97.Left = 7.521F;
            this.Label97.Name = "Label97";
            this.Label97.Style = "";
            this.Label97.Text = "VOID";
            this.Label97.Top = 0.5954167F;
            this.Label97.Width = 0.6875F;
            // 
            // Label98
            // 
            this.Label98.Height = 0.1666667F;
            this.Label98.HyperLink = null;
            this.Label98.Left = 7.521F;
            this.Label98.Name = "Label98";
            this.Label98.Style = "";
            this.Label98.Text = "CORRECTED";
            this.Label98.Top = 0.9079167F;
            this.Label98.Width = 0.9375F;
            // 
            // Line56
            // 
            this.Line56.Height = 0F;
            this.Line56.Left = 8.500167F;
            this.Line56.LineWeight = 1F;
            this.Line56.Name = "Line56";
            this.Line56.Top = 0.86625F;
            this.Line56.Width = 1.375F;
            this.Line56.X1 = 8.500167F;
            this.Line56.X2 = 9.875167F;
            this.Line56.Y1 = 0.86625F;
            this.Line56.Y2 = 0.86625F;
            // 
            // Label99
            // 
            this.Label99.Height = 0.1770833F;
            this.Label99.HyperLink = null;
            this.Label99.Left = 8.7085F;
            this.Label99.Name = "Label99";
            this.Label99.Style = "font-size: 8pt";
            this.Label99.Text = "OMB No. 1545-2251";
            this.Label99.Top = 0.6683334F;
            this.Label99.Width = 1.15625F;
            // 
            // Label100
            // 
            this.Label100.Height = 0.1770833F;
            this.Label100.HyperLink = null;
            this.Label100.Left = 2.062667F;
            this.Label100.Name = "Label100";
            this.Label100.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label100.Text = "Information about Form 1095-C and its separate instructions is at";
            this.Label100.Top = 1.012083F;
            this.Label100.Width = 3.71875F;
            // 
            // Label101
            // 
            this.Label101.Height = 0.1770833F;
            this.Label101.HyperLink = null;
            this.Label101.Left = 5.812667F;
            this.Label101.Name = "Label101";
            this.Label101.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label101.Text = "www.irs.gov/form1095c";
            this.Label101.Top = 1.012083F;
            this.Label101.Width = 1.59375F;
            // 
            // Label102
            // 
            this.Label102.Height = 0.1770833F;
            this.Label102.HyperLink = null;
            this.Label102.Left = 1.875167F;
            this.Label102.Name = "Label102";
            this.Label102.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label102.Text = "4";
            this.Label102.Top = 1.012083F;
            this.Label102.Width = 0.3333333F;
            // 
            // Label104
            // 
            this.Label104.Height = 0.1770833F;
            this.Label104.HyperLink = null;
            this.Label104.Left = 0.146F;
            this.Label104.Name = "Label104";
            this.Label104.Style = "font-size: 6.5pt";
            this.Label104.Text = "City or town";
            this.Label104.Top = 2.095417F;
            this.Label104.Width = 1.15625F;
            // 
            // Label105
            // 
            this.Label105.Height = 0.1770833F;
            this.Label105.HyperLink = null;
            this.Label105.Left = 1.7085F;
            this.Label105.Name = "Label105";
            this.Label105.Style = "font-size: 6.5pt";
            this.Label105.Text = "State or province";
            this.Label105.Top = 2.095417F;
            this.Label105.Width = 1.40625F;
            // 
            // Label106
            // 
            this.Label106.Height = 0.1770833F;
            this.Label106.HyperLink = null;
            this.Label106.Left = 3.3335F;
            this.Label106.Name = "Label106";
            this.Label106.Style = "font-size: 6.5pt";
            this.Label106.Text = "Country and ZIP or foreign postal code";
            this.Label106.Top = 2.091944F;
            this.Label106.Width = 1.65625F;
            // 
            // Label107
            // 
            this.Label107.Height = 0.1770833F;
            this.Label107.HyperLink = null;
            this.Label107.Left = 0.146F;
            this.Label107.Name = "Label107";
            this.Label107.Style = "font-size: 6.5pt";
            this.Label107.Text = "Name of employee (first name, middle initial, last name)";
            this.Label107.Top = 1.46F;
            this.Label107.Width = 2.46875F;
            // 
            // Label108
            // 
            this.Label108.Height = 0.1770833F;
            this.Label108.HyperLink = null;
            this.Label108.Left = 3.3335F;
            this.Label108.Name = "Label108";
            this.Label108.Style = "font-size: 6.5pt";
            this.Label108.Text = "Social security number (SSN)";
            this.Label108.Top = 1.46F;
            this.Label108.Width = 1.53125F;
            // 
            // Label109
            // 
            this.Label109.Height = 0.1770833F;
            this.Label109.HyperLink = null;
            this.Label109.Left = 5.250167F;
            this.Label109.Name = "Label109";
            this.Label109.Style = "font-size: 6.5pt";
            this.Label109.Text = "Name of employer";
            this.Label109.Top = 1.46F;
            this.Label109.Width = 2.15625F;
            // 
            // Label110
            // 
            this.Label110.Height = 0.1770833F;
            this.Label110.HyperLink = null;
            this.Label110.Left = 5.250167F;
            this.Label110.Name = "Label110";
            this.Label110.Style = "font-size: 6.5pt";
            this.Label110.Text = "City or town";
            this.Label110.Top = 2.095417F;
            this.Label110.Width = 1.03125F;
            // 
            // Label111
            // 
            this.Label111.Height = 0.1770833F;
            this.Label111.HyperLink = null;
            this.Label111.Left = 6.687667F;
            this.Label111.Name = "Label111";
            this.Label111.Style = "font-size: 6.5pt";
            this.Label111.Text = "State or province";
            this.Label111.Top = 2.095417F;
            this.Label111.Width = 1.03125F;
            // 
            // Label112
            // 
            this.Label112.Height = 0.1770833F;
            this.Label112.HyperLink = null;
            this.Label112.Left = 8.36475F;
            this.Label112.Name = "Label112";
            this.Label112.Style = "font-size: 6.5pt";
            this.Label112.Text = "Country and ZIP or foreign postal code";
            this.Label112.Top = 2.095417F;
            this.Label112.Width = 1.635417F;
            // 
            // Label113
            // 
            this.Label113.Height = 0.1770833F;
            this.Label113.HyperLink = null;
            this.Label113.Left = 5.250167F;
            this.Label113.Name = "Label113";
            this.Label113.Style = "font-size: 6.5pt";
            this.Label113.Text = "Street address (including room or suite no.)";
            this.Label113.Top = 1.7725F;
            this.Label113.Width = 2.46875F;
            // 
            // Label114
            // 
            this.Label114.Height = 0.1770833F;
            this.Label114.HyperLink = null;
            this.Label114.Left = 0.146F;
            this.Label114.Name = "Label114";
            this.Label114.Style = "font-size: 6.5pt";
            this.Label114.Text = "Street address (including apartment no.)";
            this.Label114.Top = 1.7725F;
            this.Label114.Width = 1.84375F;
            // 
            // Label115
            // 
            this.Label115.Height = 0.1770833F;
            this.Label115.HyperLink = null;
            this.Label115.Left = 8.36475F;
            this.Label115.Name = "Label115";
            this.Label115.Style = "font-size: 6.5pt";
            this.Label115.Text = "Contact telephone number";
            this.Label115.Top = 1.7725F;
            this.Label115.Width = 1.34375F;
            // 
            // Label116
            // 
            this.Label116.Height = 0.1770833F;
            this.Label116.HyperLink = null;
            this.Label116.Left = 8.36475F;
            this.Label116.Name = "Label116";
            this.Label116.Style = "font-size: 6.5pt";
            this.Label116.Text = "Employer identification number (EIN)";
            this.Label116.Top = 1.46F;
            this.Label116.Width = 1.59375F;
            // 
            // Label117
            // 
            this.Label117.Height = 0.1770833F;
            this.Label117.HyperLink = null;
            this.Label117.Left = 1.062667F;
            this.Label117.Name = "Label117";
            this.Label117.Style = "font-size: 7pt";
            this.Label117.Text = "All 12 Months";
            this.Label117.Top = 2.61625F;
            this.Label117.Width = 0.65625F;
            // 
            // Label119
            // 
            this.Label119.Height = 0.1770833F;
            this.Label119.HyperLink = null;
            this.Label119.Left = 2.416834F;
            this.Label119.Name = "Label119";
            this.Label119.Style = "font-size: 8.5pt; text-align: center";
            this.Label119.Text = "Feb";
            this.Label119.Top = 2.61625F;
            this.Label119.Width = 0.65625F;
            // 
            // Label120
            // 
            this.Label120.Height = 0.1770833F;
            this.Label120.HyperLink = null;
            this.Label120.Left = 3.11475F;
            this.Label120.Name = "Label120";
            this.Label120.Style = "font-size: 8.5pt; text-align: center";
            this.Label120.Text = "Mar";
            this.Label120.Top = 2.61625F;
            this.Label120.Width = 0.65625F;
            // 
            // Label121
            // 
            this.Label121.Height = 0.1770833F;
            this.Label121.HyperLink = null;
            this.Label121.Left = 3.80225F;
            this.Label121.Name = "Label121";
            this.Label121.Style = "font-size: 8.5pt; text-align: center";
            this.Label121.Text = "Apr";
            this.Label121.Top = 2.61625F;
            this.Label121.Width = 0.65625F;
            // 
            // Label122
            // 
            this.Label122.Height = 0.1770833F;
            this.Label122.HyperLink = null;
            this.Label122.Left = 4.48975F;
            this.Label122.Name = "Label122";
            this.Label122.Style = "font-size: 8.5pt; text-align: center";
            this.Label122.Text = "May";
            this.Label122.Top = 2.61625F;
            this.Label122.Width = 0.65625F;
            // 
            // Label123
            // 
            this.Label123.Height = 0.1770833F;
            this.Label123.HyperLink = null;
            this.Label123.Left = 5.16F;
            this.Label123.Name = "Label123";
            this.Label123.Style = "font-size: 8.5pt; text-align: center";
            this.Label123.Text = "June";
            this.Label123.Top = 2.61625F;
            this.Label123.Width = 0.65625F;
            // 
            // Label124
            // 
            this.Label124.Height = 0.1770833F;
            this.Label124.HyperLink = null;
            this.Label124.Left = 5.86F;
            this.Label124.Name = "Label124";
            this.Label124.Style = "font-size: 8.5pt; text-align: center";
            this.Label124.Text = "July";
            this.Label124.Top = 2.61625F;
            this.Label124.Width = 0.65625F;
            // 
            // Label125
            // 
            this.Label125.Height = 0.1770833F;
            this.Label125.HyperLink = null;
            this.Label125.Left = 6.55F;
            this.Label125.Name = "Label125";
            this.Label125.Style = "font-size: 8.5pt; text-align: center";
            this.Label125.Text = "Aug";
            this.Label125.Top = 2.61625F;
            this.Label125.Width = 0.6360002F;
            // 
            // Label126
            // 
            this.Label126.Height = 0.1770833F;
            this.Label126.HyperLink = null;
            this.Label126.Left = 7.250167F;
            this.Label126.Name = "Label126";
            this.Label126.Style = "font-size: 8.5pt; text-align: center";
            this.Label126.Text = "Sept";
            this.Label126.Top = 2.61625F;
            this.Label126.Width = 0.65625F;
            // 
            // Label127
            // 
            this.Label127.Height = 0.1770833F;
            this.Label127.HyperLink = null;
            this.Label127.Left = 7.937667F;
            this.Label127.Name = "Label127";
            this.Label127.Style = "font-size: 8.5pt; text-align: center";
            this.Label127.Text = "Oct";
            this.Label127.Top = 2.61625F;
            this.Label127.Width = 0.65625F;
            // 
            // Label128
            // 
            this.Label128.Height = 0.1770833F;
            this.Label128.HyperLink = null;
            this.Label128.Left = 8.635584F;
            this.Label128.Name = "Label128";
            this.Label128.Style = "font-size: 8.5pt; text-align: center";
            this.Label128.Text = "Nov";
            this.Label128.Top = 2.61625F;
            this.Label128.Width = 0.65625F;
            // 
            // Label129
            // 
            this.Label129.Height = 0.1770833F;
            this.Label129.HyperLink = null;
            this.Label129.Left = 9.406417F;
            this.Label129.Name = "Label129";
            this.Label129.Style = "font-size: 8.5pt; text-align: center";
            this.Label129.Text = "Dec";
            this.Label129.Top = 2.61625F;
            this.Label129.Width = 0.53125F;
            // 
            // Label138
            // 
            this.Label138.Height = 0.1770833F;
            this.Label138.HyperLink = null;
            this.Label138.Left = 1.039056F;
            this.Label138.Name = "Label138";
            this.Label138.Style = "font-size: 9pt; text-align: left";
            this.Label138.Text = "$";
            this.Label138.Top = 3.42875F;
            this.Label138.Width = 0.21875F;
            // 
            // Label139
            // 
            this.Label139.Height = 0.1770833F;
            this.Label139.HyperLink = null;
            this.Label139.Left = 1.729334F;
            this.Label139.Name = "Label139";
            this.Label139.Style = "font-size: 9pt; text-align: left";
            this.Label139.Text = "$";
            this.Label139.Top = 3.42875F;
            this.Label139.Width = 0.21875F;
            // 
            // Label140
            // 
            this.Label140.Height = 0.1770833F;
            this.Label140.HyperLink = null;
            this.Label140.Left = 2.419612F;
            this.Label140.Name = "Label140";
            this.Label140.Style = "font-size: 9pt; text-align: left";
            this.Label140.Text = "$";
            this.Label140.Top = 3.42875F;
            this.Label140.Width = 0.21875F;
            // 
            // Label141
            // 
            this.Label141.Height = 0.1770833F;
            this.Label141.HyperLink = null;
            this.Label141.Left = 3.109889F;
            this.Label141.Name = "Label141";
            this.Label141.Style = "font-size: 9pt; text-align: left";
            this.Label141.Text = "$";
            this.Label141.Top = 3.42875F;
            this.Label141.Width = 0.21875F;
            // 
            // Label142
            // 
            this.Label142.Height = 0.1770833F;
            this.Label142.HyperLink = null;
            this.Label142.Left = 3.800167F;
            this.Label142.Name = "Label142";
            this.Label142.Style = "font-size: 9pt; text-align: left";
            this.Label142.Text = "$";
            this.Label142.Top = 3.42875F;
            this.Label142.Width = 0.21875F;
            // 
            // Label143
            // 
            this.Label143.Height = 0.1770833F;
            this.Label143.HyperLink = null;
            this.Label143.Left = 4.490445F;
            this.Label143.Name = "Label143";
            this.Label143.Style = "font-size: 9pt; text-align: left";
            this.Label143.Text = "$";
            this.Label143.Top = 3.42875F;
            this.Label143.Width = 0.21875F;
            // 
            // Label144
            // 
            this.Label144.Height = 0.1770833F;
            this.Label144.HyperLink = null;
            this.Label144.Left = 5.180722F;
            this.Label144.Name = "Label144";
            this.Label144.Style = "font-size: 9pt; text-align: left";
            this.Label144.Text = "$";
            this.Label144.Top = 3.42875F;
            this.Label144.Width = 0.21875F;
            // 
            // Label145
            // 
            this.Label145.Height = 0.1770833F;
            this.Label145.HyperLink = null;
            this.Label145.Left = 5.871F;
            this.Label145.Name = "Label145";
            this.Label145.Style = "font-size: 9pt; text-align: left";
            this.Label145.Text = "$";
            this.Label145.Top = 3.42875F;
            this.Label145.Width = 0.21875F;
            // 
            // Label146
            // 
            this.Label146.Height = 0.1770833F;
            this.Label146.HyperLink = null;
            this.Label146.Left = 6.561278F;
            this.Label146.Name = "Label146";
            this.Label146.Style = "font-size: 9pt; text-align: left";
            this.Label146.Text = "$";
            this.Label146.Top = 3.42875F;
            this.Label146.Width = 0.21875F;
            // 
            // Label147
            // 
            this.Label147.Height = 0.1770833F;
            this.Label147.HyperLink = null;
            this.Label147.Left = 7.251556F;
            this.Label147.Name = "Label147";
            this.Label147.Style = "font-size: 9pt; text-align: left";
            this.Label147.Text = "$";
            this.Label147.Top = 3.42875F;
            this.Label147.Width = 0.21875F;
            // 
            // Label148
            // 
            this.Label148.Height = 0.1770833F;
            this.Label148.HyperLink = null;
            this.Label148.Left = 7.941834F;
            this.Label148.Name = "Label148";
            this.Label148.Style = "font-size: 9pt; text-align: left";
            this.Label148.Text = "$";
            this.Label148.Top = 3.42875F;
            this.Label148.Width = 0.21875F;
            // 
            // Label149
            // 
            this.Label149.Height = 0.1770833F;
            this.Label149.HyperLink = null;
            this.Label149.Left = 8.632112F;
            this.Label149.Name = "Label149";
            this.Label149.Style = "font-size: 9pt; text-align: left";
            this.Label149.Text = "$";
            this.Label149.Top = 3.42875F;
            this.Label149.Width = 0.21875F;
            // 
            // Label150
            // 
            this.Label150.Height = 0.1770833F;
            this.Label150.HyperLink = null;
            this.Label150.Left = 9.322389F;
            this.Label150.Name = "Label150";
            this.Label150.Style = "font-size: 9pt; text-align: left";
            this.Label150.Text = "$";
            this.Label150.Top = 3.42875F;
            this.Label150.Width = 0.21875F;
            // 
            // Line40
            // 
            this.Line40.Height = 2.063004F;
            this.Line40.Left = 2.401556F;
            this.Line40.LineWeight = 1F;
            this.Line40.Name = "Line40";
            this.Line40.Top = 2.585F;
            this.Line40.Width = 0F;
            this.Line40.X1 = 2.401556F;
            this.Line40.X2 = 2.401556F;
            this.Line40.Y1 = 2.585F;
            this.Line40.Y2 = 4.648005F;
            // 
            // Line41
            // 
            this.Line41.Height = 2.063004F;
            this.Line41.Left = 3.091834F;
            this.Line41.LineWeight = 1F;
            this.Line41.Name = "Line41";
            this.Line41.Top = 2.585F;
            this.Line41.Width = 0F;
            this.Line41.X1 = 3.091834F;
            this.Line41.X2 = 3.091834F;
            this.Line41.Y1 = 2.585F;
            this.Line41.Y2 = 4.648005F;
            // 
            // Line42
            // 
            this.Line42.Height = 2.063004F;
            this.Line42.Left = 3.782112F;
            this.Line42.LineWeight = 1F;
            this.Line42.Name = "Line42";
            this.Line42.Top = 2.585F;
            this.Line42.Width = 0F;
            this.Line42.X1 = 3.782112F;
            this.Line42.X2 = 3.782112F;
            this.Line42.Y1 = 2.585F;
            this.Line42.Y2 = 4.648005F;
            // 
            // Line43
            // 
            this.Line43.Height = 2.063004F;
            this.Line43.Left = 4.46F;
            this.Line43.LineWeight = 1F;
            this.Line43.Name = "Line43";
            this.Line43.Top = 2.585F;
            this.Line43.Width = 0F;
            this.Line43.X1 = 4.46F;
            this.Line43.X2 = 4.46F;
            this.Line43.Y1 = 2.585F;
            this.Line43.Y2 = 4.648005F;
            // 
            // Line47
            // 
            this.Line47.Height = 2.063004F;
            this.Line47.Left = 7.210001F;
            this.Line47.LineWeight = 1F;
            this.Line47.Name = "Line47";
            this.Line47.Top = 2.584F;
            this.Line47.Width = 0F;
            this.Line47.X1 = 7.210001F;
            this.Line47.X2 = 7.210001F;
            this.Line47.Y1 = 2.584F;
            this.Line47.Y2 = 4.647004F;
            // 
            // Line48
            // 
            this.Line48.Height = 2.063004F;
            this.Line48.Left = 7.923778F;
            this.Line48.LineWeight = 1F;
            this.Line48.Name = "Line48";
            this.Line48.Top = 2.585F;
            this.Line48.Width = 0F;
            this.Line48.X1 = 7.923778F;
            this.Line48.X2 = 7.923778F;
            this.Line48.Y1 = 2.585F;
            this.Line48.Y2 = 4.648005F;
            // 
            // Line49
            // 
            this.Line49.Height = 2.063004F;
            this.Line49.Left = 8.614056F;
            this.Line49.LineWeight = 1F;
            this.Line49.Name = "Line49";
            this.Line49.Top = 2.585F;
            this.Line49.Width = 0F;
            this.Line49.X1 = 8.614056F;
            this.Line49.X2 = 8.614056F;
            this.Line49.Y1 = 2.585F;
            this.Line49.Y2 = 4.648005F;
            // 
            // Line50
            // 
            this.Line50.Height = 2.063004F;
            this.Line50.Left = 9.304334F;
            this.Line50.LineWeight = 1F;
            this.Line50.Name = "Line50";
            this.Line50.Top = 2.585F;
            this.Line50.Width = 0F;
            this.Line50.X1 = 9.304334F;
            this.Line50.X2 = 9.304334F;
            this.Line50.Y1 = 2.585F;
            this.Line50.Y2 = 4.648005F;
            // 
            // Line45
            // 
            this.Line45.Height = 2.063004F;
            this.Line45.Left = 5.81F;
            this.Line45.LineWeight = 1F;
            this.Line45.Name = "Line45";
            this.Line45.Top = 2.585F;
            this.Line45.Width = 0F;
            this.Line45.X1 = 5.81F;
            this.Line45.X2 = 5.81F;
            this.Line45.Y1 = 2.585F;
            this.Line45.Y2 = 4.648005F;
            // 
            // Line44
            // 
            this.Line44.Height = 2.063004F;
            this.Line44.Left = 5.12F;
            this.Line44.LineWeight = 1F;
            this.Line44.Name = "Line44";
            this.Line44.Top = 2.585F;
            this.Line44.Width = 0F;
            this.Line44.X1 = 5.12F;
            this.Line44.X2 = 5.12F;
            this.Line44.Y1 = 2.585F;
            this.Line44.Y2 = 4.648005F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.34375F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 0.0001667142F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-size: 6.5pt; text-align: left";
            this.Label34.Text = "14 Offer of Coverage (enter required code)";
            this.Label34.Top = 2.657917F;
            this.Label34.Width = 0.8333333F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.46875F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 0.0001667142F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-size: 6.5pt; text-align: left";
            this.Label33.Text = "15 Employee Required Contribution (see instructions)";
            this.Label33.Top = 3.199583F;
            this.Label33.Width = 0.875F;
            // 
            // Label151
            // 
            this.Label151.Height = 0.1770833F;
            this.Label151.HyperLink = null;
            this.Label151.Left = 1.729334F;
            this.Label151.Name = "Label151";
            this.Label151.Style = "font-size: 8.5pt; text-align: center";
            this.Label151.Text = "Jan";
            this.Label151.Top = 2.61625F;
            this.Label151.Width = 0.65625F;
            // 
            // Label152
            // 
            this.Label152.Height = 0.1770833F;
            this.Label152.HyperLink = null;
            this.Label152.Left = 0.02100004F;
            this.Label152.Name = "Label152";
            this.Label152.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label152.Text = "1";
            this.Label152.Top = 1.46F;
            this.Label152.Width = 0.15625F;
            // 
            // Label153
            // 
            this.Label153.Height = 0.1770833F;
            this.Label153.HyperLink = null;
            this.Label153.Left = 0.02100004F;
            this.Label153.Name = "Label153";
            this.Label153.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label153.Text = "3";
            this.Label153.Top = 1.7725F;
            this.Label153.Width = 0.15625F;
            // 
            // Label154
            // 
            this.Label154.Height = 0.1770833F;
            this.Label154.HyperLink = null;
            this.Label154.Left = 0.02100004F;
            this.Label154.Name = "Label154";
            this.Label154.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label154.Text = "4";
            this.Label154.Top = 2.095417F;
            this.Label154.Width = 0.15625F;
            // 
            // Label155
            // 
            this.Label155.Height = 0.1770833F;
            this.Label155.HyperLink = null;
            this.Label155.Left = 3.218917F;
            this.Label155.Name = "Label155";
            this.Label155.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label155.Text = "2";
            this.Label155.Top = 1.46F;
            this.Label155.Width = 0.15625F;
            // 
            // Label156
            // 
            this.Label156.Height = 0.1770833F;
            this.Label156.HyperLink = null;
            this.Label156.Left = 3.218917F;
            this.Label156.Name = "Label156";
            this.Label156.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label156.Text = "6";
            this.Label156.Top = 2.091944F;
            this.Label156.Width = 0.15625F;
            // 
            // Label157
            // 
            this.Label157.Height = 0.1770833F;
            this.Label157.HyperLink = null;
            this.Label157.Left = 1.5835F;
            this.Label157.Name = "Label157";
            this.Label157.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label157.Text = "5";
            this.Label157.Top = 2.095417F;
            this.Label157.Width = 0.15625F;
            // 
            // Line53
            // 
            this.Line53.Height = 0.322222F;
            this.Line53.Left = 1.521F;
            this.Line53.LineWeight = 1F;
            this.Line53.Name = "Line53";
            this.Line53.Top = 2.085F;
            this.Line53.Width = 0F;
            this.Line53.X1 = 1.521F;
            this.Line53.X2 = 1.521F;
            this.Line53.Y1 = 2.085F;
            this.Line53.Y2 = 2.407222F;
            // 
            // Label158
            // 
            this.Label158.Height = 0.1770833F;
            this.Label158.HyperLink = null;
            this.Label158.Left = 5.0835F;
            this.Label158.Name = "Label158";
            this.Label158.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label158.Text = "11";
            this.Label158.Top = 2.095417F;
            this.Label158.Width = 0.15625F;
            // 
            // Label159
            // 
            this.Label159.Height = 0.1770833F;
            this.Label159.HyperLink = null;
            this.Label159.Left = 6.562667F;
            this.Label159.Name = "Label159";
            this.Label159.Style = "font-size: 7pt; font-weight: bold";
            this.Label159.Text = "12";
            this.Label159.Top = 2.095417F;
            this.Label159.Width = 0.15625F;
            // 
            // Label160
            // 
            this.Label160.Height = 0.1770833F;
            this.Label160.HyperLink = null;
            this.Label160.Left = 8.218917F;
            this.Label160.Name = "Label160";
            this.Label160.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label160.Text = "13";
            this.Label160.Top = 2.095417F;
            this.Label160.Width = 0.15625F;
            // 
            // Label161
            // 
            this.Label161.Height = 0.1770833F;
            this.Label161.HyperLink = null;
            this.Label161.Left = 5.0835F;
            this.Label161.Name = "Label161";
            this.Label161.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label161.Text = "7";
            this.Label161.Top = 1.46F;
            this.Label161.Width = 0.15625F;
            // 
            // Label162
            // 
            this.Label162.Height = 0.1770833F;
            this.Label162.HyperLink = null;
            this.Label162.Left = 5.0835F;
            this.Label162.Name = "Label162";
            this.Label162.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label162.Text = "9";
            this.Label162.Top = 1.7725F;
            this.Label162.Width = 0.15625F;
            // 
            // Label163
            // 
            this.Label163.Height = 0.1770833F;
            this.Label163.HyperLink = null;
            this.Label163.Left = 8.218917F;
            this.Label163.Name = "Label163";
            this.Label163.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label163.Text = "8";
            this.Label163.Top = 1.46F;
            this.Label163.Width = 0.15625F;
            // 
            // Label164
            // 
            this.Label164.Height = 0.1770833F;
            this.Label164.HyperLink = null;
            this.Label164.Left = 8.218917F;
            this.Label164.Name = "Label164";
            this.Label164.Style = "font-size: 6.5pt; font-weight: bold";
            this.Label164.Text = "10";
            this.Label164.Top = 1.7725F;
            this.Label164.Width = 0.15625F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.4796672F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 0.0001667142F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-size: 6.5pt; text-align: left";
            this.Label32.Text = "16 Section 4980H Safe Harbor and Other Relief (enter code, if applicable)";
            this.Label32.Top = 3.668333F;
            this.Label32.Width = 0.8958333F;
            // 
            // Label177
            // 
            this.Label177.Height = 0.1770833F;
            this.Label177.HyperLink = null;
            this.Label177.Left = 0.0001667142F;
            this.Label177.Name = "Label177";
            this.Label177.Style = "font-size: 8pt; font-weight: bold";
            this.Label177.Text = "RAA #1607  For Privacy Act and Paperwork Reduction Act Notice, see separate Instr" +
    "uctions";
            this.Label177.Top = 4.710005F;
            this.Label177.Width = 4.96875F;
            // 
            // Label179
            // 
            this.Label179.Height = 0.1770833F;
            this.Label179.HyperLink = null;
            this.Label179.Left = 6.250167F;
            this.Label179.Name = "Label179";
            this.Label179.Style = "font-size: 8pt";
            this.Label179.Text = "41-0852411";
            this.Label179.Top = 4.710005F;
            this.Label179.Width = 0.78125F;
            // 
            // Label180
            // 
            this.Label180.Height = 0.1770833F;
            this.Label180.HyperLink = null;
            this.Label180.Left = 8.666834F;
            this.Label180.Name = "Label180";
            this.Label180.Style = "font-size: 8pt";
            this.Label180.Text = "Form";
            this.Label180.Top = 4.710005F;
            this.Label180.Width = 0.40625F;
            // 
            // Label181
            // 
            this.Label181.Height = 0.1770833F;
            this.Label181.HyperLink = null;
            this.Label181.Left = 9.48975F;
            this.Label181.Name = "Label181";
            this.Label181.Style = "font-size: 8pt";
            this.Label181.Text = "(2020)";
            this.Label181.Top = 4.710005F;
            this.Label181.Width = 0.40625F;
            // 
            // Label182
            // 
            this.Label182.Height = 0.1770833F;
            this.Label182.HyperLink = null;
            this.Label182.Left = 8.98975F;
            this.Label182.Name = "Label182";
            this.Label182.Style = "font-size: 10pt; font-weight: bold";
            this.Label182.Text = "1095-C";
            this.Label182.Top = 4.689171F;
            this.Label182.Width = 0.53125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.0001667142F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.282917F;
            this.Line1.Width = 10F;
            this.Line1.X1 = 0.0001667142F;
            this.Line1.X2 = 10.00017F;
            this.Line1.Y1 = 1.282917F;
            this.Line1.Y2 = 1.282917F;
            // 
            // Line39
            // 
            this.Line39.Height = 2.063004F;
            this.Line39.Left = 1.711278F;
            this.Line39.LineWeight = 1F;
            this.Line39.Name = "Line39";
            this.Line39.Top = 2.585F;
            this.Line39.Width = 0F;
            this.Line39.X1 = 1.711278F;
            this.Line39.X2 = 1.711278F;
            this.Line39.Y1 = 2.585F;
            this.Line39.Y2 = 4.648005F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0.0001667142F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 1.7725F;
            this.Line8.Width = 9.999306F;
            this.Line8.X1 = 0.0001667142F;
            this.Line8.X2 = 9.999473F;
            this.Line8.Y1 = 1.7725F;
            this.Line8.Y2 = 1.7725F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0.0001667142F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 1.46F;
            this.Line7.Width = 9.999306F;
            this.Line7.X1 = 0.0001667142F;
            this.Line7.X2 = 9.999473F;
            this.Line7.Y1 = 1.46F;
            this.Line7.Y2 = 1.46F;
            // 
            // Line52
            // 
            this.Line52.Height = 0.947222F;
            this.Line52.Left = 8.187667F;
            this.Line52.LineWeight = 1F;
            this.Line52.Name = "Line52";
            this.Line52.Top = 1.46F;
            this.Line52.Width = 0F;
            this.Line52.X1 = 8.187667F;
            this.Line52.X2 = 8.187667F;
            this.Line52.Y1 = 1.46F;
            this.Line52.Y2 = 2.407222F;
            // 
            // Label183
            // 
            this.Label183.Height = 0.1770833F;
            this.Label183.HyperLink = null;
            this.Label183.Left = 3.000167F;
            this.Label183.Name = "Label183";
            this.Label183.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label183.Text = "Do not attach to your tax return. Keep for your records.";
            this.Label183.Top = 0.835F;
            this.Label183.Width = 3.71875F;
            // 
            // Label184
            // 
            this.Label184.Height = 0.1770833F;
            this.Label184.HyperLink = null;
            this.Label184.Left = 2.812667F;
            this.Label184.Name = "Label184";
            this.Label184.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label184.Text = "4";
            this.Label184.Top = 0.835F;
            this.Label184.Width = 0.3333333F;
            // 
            // Line58
            // 
            this.Line58.Height = 0F;
            this.Line58.Left = 0.0001667142F;
            this.Line58.LineWeight = 2F;
            this.Line58.Name = "Line58";
            this.Line58.Top = 2.407222F;
            this.Line58.Width = 9.999306F;
            this.Line58.X1 = 0.0001667142F;
            this.Line58.X2 = 9.999473F;
            this.Line58.Y1 = 2.407222F;
            this.Line58.Y2 = 2.407222F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0.0001667142F;
            this.Line5.LineWeight = 2F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 2.085F;
            this.Line5.Width = 9.999306F;
            this.Line5.X1 = 0.0001667142F;
            this.Line5.X2 = 9.999473F;
            this.Line5.Y1 = 2.085F;
            this.Line5.Y2 = 2.085F;
            // 
            // Line61
            // 
            this.Line61.Height = 0.201389F;
            this.Line61.Left = 1.527945F;
            this.Line61.LineWeight = 1F;
            this.Line61.Name = "Line61";
            this.Line61.Top = 1.571111F;
            this.Line61.Width = 0F;
            this.Line61.X1 = 1.527945F;
            this.Line61.X2 = 1.527945F;
            this.Line61.Y1 = 1.571111F;
            this.Line61.Y2 = 1.7725F;
            // 
            // Line62
            // 
            this.Line62.Height = 0.197917F;
            this.Line62.Left = 1.875167F;
            this.Line62.LineWeight = 1F;
            this.Line62.Name = "Line62";
            this.Line62.Top = 1.574583F;
            this.Line62.Width = 0F;
            this.Line62.X1 = 1.875167F;
            this.Line62.X2 = 1.875167F;
            this.Line62.Y1 = 1.574583F;
            this.Line62.Y2 = 1.7725F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.CanGrow = false;
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.625167F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 1.564167F;
            this.txtMiddle.Width = 0.2083333F;
            // 
            // txtLast
            // 
            this.txtLast.CanGrow = false;
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 1.937667F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Top = 1.564167F;
            this.txtLast.Width = 1.208333F;
            // 
            // txtAll12Zip
            // 
            this.txtAll12Zip.Height = 0.1770833F;
            this.txtAll12Zip.Left = 1.11475F;
            this.txtAll12Zip.Name = "txtAll12Zip";
            this.txtAll12Zip.Text = null;
            this.txtAll12Zip.Top = 4.344838F;
            this.txtAll12Zip.Width = 0.5F;
            // 
            // txtZip_1
            // 
            this.txtZip_1.Height = 0.1770833F;
            this.txtZip_1.Left = 1.80225F;
            this.txtZip_1.Name = "txtZip_1";
            this.txtZip_1.Text = null;
            this.txtZip_1.Top = 4.344838F;
            this.txtZip_1.Width = 0.5F;
            // 
            // txtZip_2
            // 
            this.txtZip_2.Height = 0.1770833F;
            this.txtZip_2.Left = 2.48975F;
            this.txtZip_2.Name = "txtZip_2";
            this.txtZip_2.Text = null;
            this.txtZip_2.Top = 4.344838F;
            this.txtZip_2.Width = 0.5F;
            // 
            // txtZip_3
            // 
            this.txtZip_3.Height = 0.1770833F;
            this.txtZip_3.Left = 3.17725F;
            this.txtZip_3.Name = "txtZip_3";
            this.txtZip_3.Text = null;
            this.txtZip_3.Top = 4.344838F;
            this.txtZip_3.Width = 0.5F;
            // 
            // txtZip_4
            // 
            this.txtZip_4.Height = 0.1770833F;
            this.txtZip_4.Left = 3.875167F;
            this.txtZip_4.Name = "txtZip_4";
            this.txtZip_4.Text = null;
            this.txtZip_4.Top = 4.344838F;
            this.txtZip_4.Width = 0.5F;
            // 
            // txtZip_5
            // 
            this.txtZip_5.Height = 0.1770833F;
            this.txtZip_5.Left = 4.562667F;
            this.txtZip_5.Name = "txtZip_5";
            this.txtZip_5.Text = null;
            this.txtZip_5.Top = 4.344838F;
            this.txtZip_5.Width = 0.5F;
            // 
            // txtZip_6
            // 
            this.txtZip_6.Height = 0.1770833F;
            this.txtZip_6.Left = 5.250167F;
            this.txtZip_6.Name = "txtZip_6";
            this.txtZip_6.Text = null;
            this.txtZip_6.Top = 4.344838F;
            this.txtZip_6.Width = 0.5F;
            // 
            // txtZip_7
            // 
            this.txtZip_7.Height = 0.1770833F;
            this.txtZip_7.Left = 5.968917F;
            this.txtZip_7.Name = "txtZip_7";
            this.txtZip_7.Text = null;
            this.txtZip_7.Top = 4.344838F;
            this.txtZip_7.Width = 0.5F;
            // 
            // txtZip_8
            // 
            this.txtZip_8.Height = 0.1770833F;
            this.txtZip_8.Left = 6.635583F;
            this.txtZip_8.Name = "txtZip_8";
            this.txtZip_8.Text = null;
            this.txtZip_8.Top = 4.344838F;
            this.txtZip_8.Width = 0.5F;
            // 
            // txtZip_9
            // 
            this.txtZip_9.Height = 0.1770833F;
            this.txtZip_9.Left = 7.323083F;
            this.txtZip_9.Name = "txtZip_9";
            this.txtZip_9.Text = null;
            this.txtZip_9.Top = 4.344838F;
            this.txtZip_9.Width = 0.5F;
            // 
            // txtZip_10
            // 
            this.txtZip_10.Height = 0.1770833F;
            this.txtZip_10.Left = 8.010584F;
            this.txtZip_10.Name = "txtZip_10";
            this.txtZip_10.Text = null;
            this.txtZip_10.Top = 4.344838F;
            this.txtZip_10.Width = 0.5F;
            // 
            // txtZip_11
            // 
            this.txtZip_11.Height = 0.1770833F;
            this.txtZip_11.Left = 8.7085F;
            this.txtZip_11.Name = "txtZip_11";
            this.txtZip_11.Text = null;
            this.txtZip_11.Top = 4.344838F;
            this.txtZip_11.Width = 0.5F;
            // 
            // txtZip_12
            // 
            this.txtZip_12.Height = 0.1770833F;
            this.txtZip_12.Left = 9.396F;
            this.txtZip_12.Name = "txtZip_12";
            this.txtZip_12.Text = null;
            this.txtZip_12.Top = 4.344838F;
            this.txtZip_12.Width = 0.5F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.0001667142F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 4.648005F;
            this.line2.Width = 9.999306F;
            this.line2.X1 = 0.0001667142F;
            this.line2.X2 = 9.999473F;
            this.line2.Y1 = 4.648005F;
            this.line2.Y2 = 4.648005F;
            // 
            // label1
            // 
            this.label1.Height = 0.14625F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.0001667142F;
            this.label1.Name = "label1";
            this.label1.Style = "font-size: 6.5pt; text-align: left";
            this.label1.Text = "17 ZIP Code";
            this.label1.Top = 4.459004F;
            this.label1.Width = 0.8958333F;
            // 
            // line15
            // 
            this.line15.Height = 0.178F;
            this.line15.Left = 3.677F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 2.407F;
            this.line15.Width = 0F;
            this.line15.X1 = 3.677F;
            this.line15.X2 = 3.677F;
            this.line15.Y1 = 2.407F;
            this.line15.Y2 = 2.585F;
            // 
            // txtAge
            // 
            this.txtAge.Height = 0.1770833F;
            this.txtAge.Left = 6.04F;
            this.txtAge.Name = "txtAge";
            this.txtAge.Top = 2.408F;
            this.txtAge.Width = 0.3979994F;
            // 
            // label2
            // 
            this.label2.Height = 0.1770833F;
            this.label2.HyperLink = null;
            this.label2.Left = 3.729F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: \'Arial\'; font-weight: bold";
            this.label2.Text = "Employee\'s Age on January 1";
            this.label2.Top = 2.408F;
            this.label2.Width = 2.131F;
            // 
            // rpt1095C2020BlankPage1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 10.03125F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Zip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStartMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label139;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label140;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label144;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label145;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label146;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label147;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label148;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label149;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label150;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label151;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label153;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label154;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label155;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label156;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label157;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label158;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label159;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label160;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label161;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label162;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label163;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label164;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label181;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label183;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label184;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Zip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip_12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
    }
}
