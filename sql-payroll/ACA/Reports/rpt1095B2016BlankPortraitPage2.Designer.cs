﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2016BlankPortraitPage2.
	/// </summary>
	partial class rpt1095B2016BlankPortraitPage2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095B2016BlankPortraitPage2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lbl12Months = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape82 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape83 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape84 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape85 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape86 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape87 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape88 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape89 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape90 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape91 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape92 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape93 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape94 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape95 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape96 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape97 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape98 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape99 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape100 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape101 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape102 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape103 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape104 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape105 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape106 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape107 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape108 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape109 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape110 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape111 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape112 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape113 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape114 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape115 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape116 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape117 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape118 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape119 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape120 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape121 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape122 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape123 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape124 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape125 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape126 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape127 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape128 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape129 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape130 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape131 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape132 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape133 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape134 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape135 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape136 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape137 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape138 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape139 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape140 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape141 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape142 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape143 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape144 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape145 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape146 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape147 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape148 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape149 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape150 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape151 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape152 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape153 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape154 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape155 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape156 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape157 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape158 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape159 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape160 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape161 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape162 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape163 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape164 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape165 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape166 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape167 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape168 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape169 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape170 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape171 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape172 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape173 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape174 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape175 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape176 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape177 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape178 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape179 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape180 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape181 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape182 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape183 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape184 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape185 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape186 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape187 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape188 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape189 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape190 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape191 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape192 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape193 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape194 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape195 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape196 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape197 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape198 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape199 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape200 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape201 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape202 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape203 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape204 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape205 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape206 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape207 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape208 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape209 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape210 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape211 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape212 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape213 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape214 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape215 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape216 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape217 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape218 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape219 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape220 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape221 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape222 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape223 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape224 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape225 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape226 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape227 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape228 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape229 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape230 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape231 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape232 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape233 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.txtSSN,
				this.txtCoveredName1,
				this.txtCoveredSSN1,
				this.txtCoveredDOB1,
				this.txtCoveredName2,
				this.txtCoveredSSN2,
				this.txtCoveredDOB2,
				this.txtCoveredName3,
				this.txtCoveredSSN3,
				this.txtCoveredDOB3,
				this.txtCoveredName4,
				this.txtCoveredSSN4,
				this.txtCoveredDOB4,
				this.txtCoveredName5,
				this.txtCoveredSSN5,
				this.txtCoveredDOB5,
				this.txtCoveredName6,
				this.txtCoveredSSN6,
				this.txtCoveredDOB6,
				this.txtCoveredAll12_1,
				this.txtCoveredMonth1_1,
				this.txtCoveredMonth2_1,
				this.txtCoveredMonth3_1,
				this.txtCoveredMonth4_1,
				this.txtCoveredMonth5_1,
				this.txtCoveredMonth6_1,
				this.txtCoveredMonth7_1,
				this.txtCoveredMonth8_1,
				this.txtCoveredMonth9_1,
				this.txtCoveredMonth10_1,
				this.txtCoveredMonth11_1,
				this.txtCoveredMonth12_1,
				this.txtCoveredAll12_2,
				this.txtCoveredMonth1_2,
				this.txtCoveredMonth2_2,
				this.txtCoveredMonth3_2,
				this.txtCoveredMonth4_2,
				this.txtCoveredMonth5_2,
				this.txtCoveredMonth6_2,
				this.txtCoveredMonth7_2,
				this.txtCoveredMonth8_2,
				this.txtCoveredMonth9_2,
				this.txtCoveredMonth10_2,
				this.txtCoveredMonth11_2,
				this.txtCoveredMonth12_2,
				this.txtCoveredAll12_3,
				this.txtCoveredMonth1_3,
				this.txtCoveredMonth2_3,
				this.txtCoveredMonth3_3,
				this.txtCoveredMonth4_3,
				this.txtCoveredMonth5_3,
				this.txtCoveredMonth6_3,
				this.txtCoveredMonth7_3,
				this.txtCoveredMonth8_3,
				this.txtCoveredMonth9_3,
				this.txtCoveredMonth10_3,
				this.txtCoveredMonth11_3,
				this.txtCoveredMonth12_3,
				this.txtCoveredAll12_4,
				this.txtCoveredMonth1_4,
				this.txtCoveredMonth2_4,
				this.txtCoveredMonth3_4,
				this.txtCoveredMonth4_4,
				this.txtCoveredMonth5_4,
				this.txtCoveredMonth6_4,
				this.txtCoveredMonth7_4,
				this.txtCoveredMonth8_4,
				this.txtCoveredMonth9_4,
				this.txtCoveredMonth10_4,
				this.txtCoveredMonth11_4,
				this.txtCoveredMonth12_4,
				this.txtCoveredAll12_5,
				this.txtCoveredMonth1_5,
				this.txtCoveredMonth2_5,
				this.txtCoveredMonth3_5,
				this.txtCoveredMonth4_5,
				this.txtCoveredMonth5_5,
				this.txtCoveredMonth6_5,
				this.txtCoveredMonth7_5,
				this.txtCoveredMonth8_5,
				this.txtCoveredMonth9_5,
				this.txtCoveredMonth10_5,
				this.txtCoveredMonth11_5,
				this.txtCoveredMonth12_5,
				this.txtCoveredAll12_6,
				this.txtCoveredMonth1_6,
				this.txtCoveredMonth2_6,
				this.txtCoveredMonth3_6,
				this.txtCoveredMonth4_6,
				this.txtCoveredMonth5_6,
				this.txtCoveredMonth6_6,
				this.txtCoveredMonth7_6,
				this.txtCoveredMonth8_6,
				this.txtCoveredMonth9_6,
				this.txtCoveredMonth10_6,
				this.txtCoveredMonth11_6,
				this.txtCoveredMonth12_6,
				this.txtCoveredName7,
				this.txtCoveredSSN7,
				this.txtCoveredDOB7,
				this.txtCoveredName8,
				this.txtCoveredSSN8,
				this.txtCoveredDOB8,
				this.txtCoveredName9,
				this.txtCoveredSSN9,
				this.txtCoveredDOB9,
				this.txtCoveredName10,
				this.txtCoveredSSN10,
				this.txtCoveredDOB10,
				this.txtCoveredName11,
				this.txtCoveredSSN11,
				this.txtCoveredDOB11,
				this.txtCoveredName12,
				this.txtCoveredSSN12,
				this.txtCoveredDOB12,
				this.txtCoveredAll12_7,
				this.txtCoveredMonth1_7,
				this.txtCoveredMonth2_7,
				this.txtCoveredMonth3_7,
				this.txtCoveredMonth4_7,
				this.txtCoveredMonth5_7,
				this.txtCoveredMonth6_7,
				this.txtCoveredMonth7_7,
				this.txtCoveredMonth8_7,
				this.txtCoveredMonth9_7,
				this.txtCoveredMonth10_7,
				this.txtCoveredMonth11_7,
				this.txtCoveredMonth12_7,
				this.txtCoveredAll12_8,
				this.txtCoveredMonth1_8,
				this.txtCoveredMonth2_8,
				this.txtCoveredMonth3_8,
				this.txtCoveredMonth4_8,
				this.txtCoveredMonth5_8,
				this.txtCoveredMonth6_8,
				this.txtCoveredMonth7_8,
				this.txtCoveredMonth8_8,
				this.txtCoveredMonth9_8,
				this.txtCoveredMonth10_8,
				this.txtCoveredMonth11_8,
				this.txtCoveredMonth12_8,
				this.txtCoveredAll12_9,
				this.txtCoveredMonth1_9,
				this.txtCoveredMonth2_9,
				this.txtCoveredMonth3_9,
				this.txtCoveredMonth4_9,
				this.txtCoveredMonth5_9,
				this.txtCoveredMonth6_9,
				this.txtCoveredMonth7_9,
				this.txtCoveredMonth8_9,
				this.txtCoveredMonth9_9,
				this.txtCoveredMonth10_9,
				this.txtCoveredMonth11_9,
				this.txtCoveredMonth12_9,
				this.txtCoveredAll12_10,
				this.txtCoveredMonth1_10,
				this.txtCoveredMonth2_10,
				this.txtCoveredMonth3_10,
				this.txtCoveredMonth4_10,
				this.txtCoveredMonth5_10,
				this.txtCoveredMonth6_10,
				this.txtCoveredMonth7_10,
				this.txtCoveredMonth8_10,
				this.txtCoveredMonth9_10,
				this.txtCoveredMonth10_10,
				this.txtCoveredMonth11_10,
				this.txtCoveredMonth12_10,
				this.txtCoveredAll12_11,
				this.txtCoveredMonth1_11,
				this.txtCoveredMonth2_11,
				this.txtCoveredMonth3_11,
				this.txtCoveredMonth4_11,
				this.txtCoveredMonth5_11,
				this.txtCoveredMonth6_11,
				this.txtCoveredMonth7_11,
				this.txtCoveredMonth8_11,
				this.txtCoveredMonth9_11,
				this.txtCoveredMonth10_11,
				this.txtCoveredMonth11_11,
				this.txtCoveredMonth12_11,
				this.txtCoveredAll12_12,
				this.txtCoveredMonth1_12,
				this.txtCoveredMonth2_12,
				this.txtCoveredMonth3_12,
				this.txtCoveredMonth4_12,
				this.txtCoveredMonth5_12,
				this.txtCoveredMonth6_12,
				this.txtCoveredMonth7_12,
				this.txtCoveredMonth8_12,
				this.txtCoveredMonth9_12,
				this.txtCoveredMonth10_12,
				this.txtCoveredMonth11_12,
				this.txtCoveredMonth12_12,
				this.Label14,
				this.Label13,
				this.Label9,
				this.Label4,
				this.Line2,
				this.Line9,
				this.Line14,
				this.Line18,
				this.Line19,
				this.Line22,
				this.Line23,
				this.lbl12Months,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label15,
				this.Label16,
				this.Shape77,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Line20,
				this.Line21,
				this.Line17,
				this.Line16,
				this.Line15,
				this.Line13,
				this.Line12,
				this.Line11,
				this.Line10,
				this.Line24,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Line6,
				this.Line7,
				this.Line8,
				this.Line25,
				this.Line26,
				this.Line27,
				this.Line28,
				this.Line29,
				this.Line30,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Shape78,
				this.Shape80,
				this.Shape81,
				this.Shape82,
				this.Shape83,
				this.Shape84,
				this.Shape85,
				this.Shape86,
				this.Shape87,
				this.Shape88,
				this.Shape89,
				this.Shape90,
				this.Shape91,
				this.Shape92,
				this.Shape93,
				this.Shape94,
				this.Shape95,
				this.Shape96,
				this.Shape97,
				this.Shape98,
				this.Shape99,
				this.Shape100,
				this.Shape101,
				this.Shape102,
				this.Shape103,
				this.Shape104,
				this.Shape105,
				this.Shape106,
				this.Shape107,
				this.Shape108,
				this.Shape109,
				this.Shape110,
				this.Shape111,
				this.Shape112,
				this.Shape113,
				this.Shape114,
				this.Shape115,
				this.Shape116,
				this.Shape117,
				this.Shape118,
				this.Shape119,
				this.Shape120,
				this.Shape121,
				this.Shape122,
				this.Shape123,
				this.Shape124,
				this.Shape125,
				this.Shape126,
				this.Shape127,
				this.Shape128,
				this.Shape129,
				this.Shape130,
				this.Shape131,
				this.Shape132,
				this.Shape133,
				this.Shape134,
				this.Shape135,
				this.Shape136,
				this.Shape137,
				this.Shape138,
				this.Shape139,
				this.Shape140,
				this.Shape141,
				this.Shape142,
				this.Shape143,
				this.Shape144,
				this.Shape145,
				this.Shape146,
				this.Shape147,
				this.Shape148,
				this.Shape149,
				this.Shape150,
				this.Shape151,
				this.Shape152,
				this.Shape153,
				this.Shape154,
				this.Shape155,
				this.Shape156,
				this.Shape157,
				this.Shape158,
				this.Shape159,
				this.Shape160,
				this.Shape161,
				this.Shape162,
				this.Shape163,
				this.Shape164,
				this.Shape165,
				this.Shape166,
				this.Shape167,
				this.Shape168,
				this.Shape169,
				this.Shape170,
				this.Shape171,
				this.Shape172,
				this.Shape173,
				this.Shape174,
				this.Shape175,
				this.Shape176,
				this.Shape177,
				this.Shape178,
				this.Shape179,
				this.Shape180,
				this.Shape181,
				this.Shape182,
				this.Shape183,
				this.Shape184,
				this.Shape185,
				this.Shape186,
				this.Shape187,
				this.Shape188,
				this.Shape189,
				this.Shape190,
				this.Shape191,
				this.Shape192,
				this.Shape193,
				this.Shape194,
				this.Shape195,
				this.Shape196,
				this.Shape197,
				this.Shape198,
				this.Shape199,
				this.Shape200,
				this.Shape201,
				this.Shape202,
				this.Shape203,
				this.Shape204,
				this.Shape205,
				this.Shape206,
				this.Shape207,
				this.Shape208,
				this.Shape209,
				this.Shape210,
				this.Shape211,
				this.Shape212,
				this.Shape213,
				this.Shape214,
				this.Shape215,
				this.Shape216,
				this.Shape217,
				this.Shape218,
				this.Shape219,
				this.Shape220,
				this.Shape221,
				this.Shape222,
				this.Shape223,
				this.Shape224,
				this.Shape225,
				this.Shape226,
				this.Shape227,
				this.Shape228,
				this.Shape229,
				this.Shape230,
				this.Shape231,
				this.Shape232,
				this.Shape233,
				this.Label25,
				this.Label24,
				this.Label32,
				this.Line31,
				this.Line32,
				this.Line33,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label93,
				this.Label94,
				this.Label95,
				this.Label96,
				this.Label97,
				this.Label98,
				this.Label99,
				this.Label69,
				this.Label70,
				this.Line1,
				this.Label100,
				this.Line34
			});
			this.Detail.Height = 9.927083F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtName
			// 
			this.txtName.Height = 0.1770833F;
			this.txtName.Left = 0.04166667F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8.5pt";
			this.txtName.Text = null;
			this.txtName.Top = 0.3541667F;
			this.txtName.Width = 4F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1770833F;
			this.txtSSN.Left = 4.1875F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Style = "font-size: 8.5pt";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0.3541667F;
			this.txtSSN.Width = 1.604167F;
			// 
			// txtCoveredName1
			// 
			this.txtCoveredName1.Height = 0.1770833F;
			this.txtCoveredName1.Left = 0.1666667F;
			this.txtCoveredName1.Name = "txtCoveredName1";
			this.txtCoveredName1.Style = "font-size: 8.5pt";
			this.txtCoveredName1.Text = null;
			this.txtCoveredName1.Top = 1.083333F;
			this.txtCoveredName1.Width = 1.583333F;
			// 
			// txtCoveredSSN1
			// 
			this.txtCoveredSSN1.Height = 0.1770833F;
			this.txtCoveredSSN1.Left = 1.833333F;
			this.txtCoveredSSN1.Name = "txtCoveredSSN1";
			this.txtCoveredSSN1.Style = "font-size: 8.5pt";
			this.txtCoveredSSN1.Text = null;
			this.txtCoveredSSN1.Top = 1.083333F;
			this.txtCoveredSSN1.Width = 0.8333333F;
			// 
			// txtCoveredDOB1
			// 
			this.txtCoveredDOB1.Height = 0.1770833F;
			this.txtCoveredDOB1.Left = 2.75F;
			this.txtCoveredDOB1.Name = "txtCoveredDOB1";
			this.txtCoveredDOB1.Style = "font-size: 8.5pt";
			this.txtCoveredDOB1.Text = null;
			this.txtCoveredDOB1.Top = 1.083333F;
			this.txtCoveredDOB1.Width = 0.75F;
			// 
			// txtCoveredName2
			// 
			this.txtCoveredName2.Height = 0.1770833F;
			this.txtCoveredName2.Left = 0.1666667F;
			this.txtCoveredName2.Name = "txtCoveredName2";
			this.txtCoveredName2.Style = "font-size: 8.5pt";
			this.txtCoveredName2.Text = null;
			this.txtCoveredName2.Top = 1.451389F;
			this.txtCoveredName2.Width = 1.583333F;
			// 
			// txtCoveredSSN2
			// 
			this.txtCoveredSSN2.Height = 0.1770833F;
			this.txtCoveredSSN2.Left = 1.833333F;
			this.txtCoveredSSN2.Name = "txtCoveredSSN2";
			this.txtCoveredSSN2.Style = "font-size: 8.5pt";
			this.txtCoveredSSN2.Text = null;
			this.txtCoveredSSN2.Top = 1.451389F;
			this.txtCoveredSSN2.Width = 0.8333333F;
			// 
			// txtCoveredDOB2
			// 
			this.txtCoveredDOB2.Height = 0.1770833F;
			this.txtCoveredDOB2.Left = 2.75F;
			this.txtCoveredDOB2.Name = "txtCoveredDOB2";
			this.txtCoveredDOB2.Style = "font-size: 8.5pt";
			this.txtCoveredDOB2.Text = null;
			this.txtCoveredDOB2.Top = 1.451389F;
			this.txtCoveredDOB2.Width = 0.75F;
			// 
			// txtCoveredName3
			// 
			this.txtCoveredName3.Height = 0.1770833F;
			this.txtCoveredName3.Left = 0.1666667F;
			this.txtCoveredName3.Name = "txtCoveredName3";
			this.txtCoveredName3.Style = "font-size: 8.5pt";
			this.txtCoveredName3.Text = null;
			this.txtCoveredName3.Top = 1.826389F;
			this.txtCoveredName3.Width = 1.583333F;
			// 
			// txtCoveredSSN3
			// 
			this.txtCoveredSSN3.Height = 0.1770833F;
			this.txtCoveredSSN3.Left = 1.833333F;
			this.txtCoveredSSN3.Name = "txtCoveredSSN3";
			this.txtCoveredSSN3.Style = "font-size: 8.5pt";
			this.txtCoveredSSN3.Text = null;
			this.txtCoveredSSN3.Top = 1.826389F;
			this.txtCoveredSSN3.Width = 0.8333333F;
			// 
			// txtCoveredDOB3
			// 
			this.txtCoveredDOB3.Height = 0.1770833F;
			this.txtCoveredDOB3.Left = 2.75F;
			this.txtCoveredDOB3.Name = "txtCoveredDOB3";
			this.txtCoveredDOB3.Style = "font-size: 8.5pt";
			this.txtCoveredDOB3.Text = null;
			this.txtCoveredDOB3.Top = 1.826389F;
			this.txtCoveredDOB3.Width = 0.75F;
			// 
			// txtCoveredName4
			// 
			this.txtCoveredName4.Height = 0.1770833F;
			this.txtCoveredName4.Left = 0.1666667F;
			this.txtCoveredName4.Name = "txtCoveredName4";
			this.txtCoveredName4.Style = "font-size: 8.5pt";
			this.txtCoveredName4.Text = null;
			this.txtCoveredName4.Top = 2.201389F;
			this.txtCoveredName4.Width = 1.583333F;
			// 
			// txtCoveredSSN4
			// 
			this.txtCoveredSSN4.Height = 0.1770833F;
			this.txtCoveredSSN4.Left = 1.833333F;
			this.txtCoveredSSN4.Name = "txtCoveredSSN4";
			this.txtCoveredSSN4.Style = "font-size: 8.5pt";
			this.txtCoveredSSN4.Text = null;
			this.txtCoveredSSN4.Top = 2.201389F;
			this.txtCoveredSSN4.Width = 0.8333333F;
			// 
			// txtCoveredDOB4
			// 
			this.txtCoveredDOB4.Height = 0.1770833F;
			this.txtCoveredDOB4.Left = 2.75F;
			this.txtCoveredDOB4.Name = "txtCoveredDOB4";
			this.txtCoveredDOB4.Style = "font-size: 8.5pt";
			this.txtCoveredDOB4.Text = null;
			this.txtCoveredDOB4.Top = 2.201389F;
			this.txtCoveredDOB4.Width = 0.75F;
			// 
			// txtCoveredName5
			// 
			this.txtCoveredName5.Height = 0.1770833F;
			this.txtCoveredName5.Left = 0.1666667F;
			this.txtCoveredName5.Name = "txtCoveredName5";
			this.txtCoveredName5.Style = "font-size: 8.5pt";
			this.txtCoveredName5.Text = null;
			this.txtCoveredName5.Top = 2.576389F;
			this.txtCoveredName5.Width = 1.583333F;
			// 
			// txtCoveredSSN5
			// 
			this.txtCoveredSSN5.Height = 0.1770833F;
			this.txtCoveredSSN5.Left = 1.833333F;
			this.txtCoveredSSN5.Name = "txtCoveredSSN5";
			this.txtCoveredSSN5.Style = "font-size: 8.5pt";
			this.txtCoveredSSN5.Text = null;
			this.txtCoveredSSN5.Top = 2.576389F;
			this.txtCoveredSSN5.Width = 0.8333333F;
			// 
			// txtCoveredDOB5
			// 
			this.txtCoveredDOB5.Height = 0.1770833F;
			this.txtCoveredDOB5.Left = 2.75F;
			this.txtCoveredDOB5.Name = "txtCoveredDOB5";
			this.txtCoveredDOB5.Style = "font-size: 8.5pt";
			this.txtCoveredDOB5.Text = null;
			this.txtCoveredDOB5.Top = 2.576389F;
			this.txtCoveredDOB5.Width = 0.75F;
			// 
			// txtCoveredName6
			// 
			this.txtCoveredName6.Height = 0.1770833F;
			this.txtCoveredName6.Left = 0.1666667F;
			this.txtCoveredName6.Name = "txtCoveredName6";
			this.txtCoveredName6.Style = "font-size: 8.5pt";
			this.txtCoveredName6.Text = null;
			this.txtCoveredName6.Top = 2.951389F;
			this.txtCoveredName6.Width = 1.583333F;
			// 
			// txtCoveredSSN6
			// 
			this.txtCoveredSSN6.Height = 0.1770833F;
			this.txtCoveredSSN6.Left = 1.833333F;
			this.txtCoveredSSN6.Name = "txtCoveredSSN6";
			this.txtCoveredSSN6.Style = "font-size: 8.5pt";
			this.txtCoveredSSN6.Text = null;
			this.txtCoveredSSN6.Top = 2.951389F;
			this.txtCoveredSSN6.Width = 0.8333333F;
			// 
			// txtCoveredDOB6
			// 
			this.txtCoveredDOB6.Height = 0.1770833F;
			this.txtCoveredDOB6.Left = 2.75F;
			this.txtCoveredDOB6.Name = "txtCoveredDOB6";
			this.txtCoveredDOB6.Style = "font-size: 8.5pt";
			this.txtCoveredDOB6.Text = null;
			this.txtCoveredDOB6.Top = 2.951389F;
			this.txtCoveredDOB6.Width = 0.75F;
			// 
			// txtCoveredAll12_1
			// 
			this.txtCoveredAll12_1.Height = 0.1770833F;
			this.txtCoveredAll12_1.Left = 3.609028F;
			this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
			this.txtCoveredAll12_1.Text = null;
			this.txtCoveredAll12_1.Top = 1.076389F;
			this.txtCoveredAll12_1.Width = 0.25F;
			// 
			// txtCoveredMonth1_1
			// 
			this.txtCoveredMonth1_1.Height = 0.1770833F;
			this.txtCoveredMonth1_1.Left = 3.996528F;
			this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
			this.txtCoveredMonth1_1.Text = null;
			this.txtCoveredMonth1_1.Top = 1.083333F;
			this.txtCoveredMonth1_1.Width = 0.25F;
			// 
			// txtCoveredMonth2_1
			// 
			this.txtCoveredMonth2_1.Height = 0.1770833F;
			this.txtCoveredMonth2_1.Left = 4.291667F;
			this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
			this.txtCoveredMonth2_1.Text = null;
			this.txtCoveredMonth2_1.Top = 1.083333F;
			this.txtCoveredMonth2_1.Width = 0.25F;
			// 
			// txtCoveredMonth3_1
			// 
			this.txtCoveredMonth3_1.Height = 0.1770833F;
			this.txtCoveredMonth3_1.Left = 4.586805F;
			this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
			this.txtCoveredMonth3_1.Text = null;
			this.txtCoveredMonth3_1.Top = 1.083333F;
			this.txtCoveredMonth3_1.Width = 0.25F;
			// 
			// txtCoveredMonth4_1
			// 
			this.txtCoveredMonth4_1.Height = 0.1770833F;
			this.txtCoveredMonth4_1.Left = 4.881945F;
			this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
			this.txtCoveredMonth4_1.Text = null;
			this.txtCoveredMonth4_1.Top = 1.083333F;
			this.txtCoveredMonth4_1.Width = 0.25F;
			// 
			// txtCoveredMonth5_1
			// 
			this.txtCoveredMonth5_1.Height = 0.1770833F;
			this.txtCoveredMonth5_1.Left = 5.177083F;
			this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
			this.txtCoveredMonth5_1.Text = null;
			this.txtCoveredMonth5_1.Top = 1.083333F;
			this.txtCoveredMonth5_1.Width = 0.25F;
			// 
			// txtCoveredMonth6_1
			// 
			this.txtCoveredMonth6_1.Height = 0.1770833F;
			this.txtCoveredMonth6_1.Left = 5.472222F;
			this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
			this.txtCoveredMonth6_1.Text = null;
			this.txtCoveredMonth6_1.Top = 1.083333F;
			this.txtCoveredMonth6_1.Width = 0.25F;
			// 
			// txtCoveredMonth7_1
			// 
			this.txtCoveredMonth7_1.Height = 0.1770833F;
			this.txtCoveredMonth7_1.Left = 5.767361F;
			this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
			this.txtCoveredMonth7_1.Text = null;
			this.txtCoveredMonth7_1.Top = 1.083333F;
			this.txtCoveredMonth7_1.Width = 0.25F;
			// 
			// txtCoveredMonth8_1
			// 
			this.txtCoveredMonth8_1.Height = 0.1770833F;
			this.txtCoveredMonth8_1.Left = 6.0625F;
			this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
			this.txtCoveredMonth8_1.Text = null;
			this.txtCoveredMonth8_1.Top = 1.083333F;
			this.txtCoveredMonth8_1.Width = 0.25F;
			// 
			// txtCoveredMonth9_1
			// 
			this.txtCoveredMonth9_1.Height = 0.1770833F;
			this.txtCoveredMonth9_1.Left = 6.357639F;
			this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
			this.txtCoveredMonth9_1.Text = null;
			this.txtCoveredMonth9_1.Top = 1.083333F;
			this.txtCoveredMonth9_1.Width = 0.25F;
			// 
			// txtCoveredMonth10_1
			// 
			this.txtCoveredMonth10_1.Height = 0.1770833F;
			this.txtCoveredMonth10_1.Left = 6.652778F;
			this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
			this.txtCoveredMonth10_1.Text = null;
			this.txtCoveredMonth10_1.Top = 1.083333F;
			this.txtCoveredMonth10_1.Width = 0.25F;
			// 
			// txtCoveredMonth11_1
			// 
			this.txtCoveredMonth11_1.Height = 0.1770833F;
			this.txtCoveredMonth11_1.Left = 6.947917F;
			this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
			this.txtCoveredMonth11_1.Text = null;
			this.txtCoveredMonth11_1.Top = 1.083333F;
			this.txtCoveredMonth11_1.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_1
			// 
			this.txtCoveredMonth12_1.Height = 0.1770833F;
			this.txtCoveredMonth12_1.Left = 7.243055F;
			this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
			this.txtCoveredMonth12_1.Text = null;
			this.txtCoveredMonth12_1.Top = 1.083333F;
			this.txtCoveredMonth12_1.Width = 0.1875F;
			// 
			// txtCoveredAll12_2
			// 
			this.txtCoveredAll12_2.Height = 0.1770833F;
			this.txtCoveredAll12_2.Left = 3.609028F;
			this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
			this.txtCoveredAll12_2.Text = null;
			this.txtCoveredAll12_2.Top = 1.451389F;
			this.txtCoveredAll12_2.Width = 0.25F;
			// 
			// txtCoveredMonth1_2
			// 
			this.txtCoveredMonth1_2.Height = 0.1770833F;
			this.txtCoveredMonth1_2.Left = 3.996528F;
			this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
			this.txtCoveredMonth1_2.Text = null;
			this.txtCoveredMonth1_2.Top = 1.451389F;
			this.txtCoveredMonth1_2.Width = 0.25F;
			// 
			// txtCoveredMonth2_2
			// 
			this.txtCoveredMonth2_2.Height = 0.1770833F;
			this.txtCoveredMonth2_2.Left = 4.291667F;
			this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
			this.txtCoveredMonth2_2.Text = null;
			this.txtCoveredMonth2_2.Top = 1.451389F;
			this.txtCoveredMonth2_2.Width = 0.25F;
			// 
			// txtCoveredMonth3_2
			// 
			this.txtCoveredMonth3_2.Height = 0.1770833F;
			this.txtCoveredMonth3_2.Left = 4.586805F;
			this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
			this.txtCoveredMonth3_2.Text = null;
			this.txtCoveredMonth3_2.Top = 1.451389F;
			this.txtCoveredMonth3_2.Width = 0.25F;
			// 
			// txtCoveredMonth4_2
			// 
			this.txtCoveredMonth4_2.Height = 0.1770833F;
			this.txtCoveredMonth4_2.Left = 4.881945F;
			this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
			this.txtCoveredMonth4_2.Text = null;
			this.txtCoveredMonth4_2.Top = 1.451389F;
			this.txtCoveredMonth4_2.Width = 0.25F;
			// 
			// txtCoveredMonth5_2
			// 
			this.txtCoveredMonth5_2.Height = 0.1770833F;
			this.txtCoveredMonth5_2.Left = 5.177083F;
			this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
			this.txtCoveredMonth5_2.Text = null;
			this.txtCoveredMonth5_2.Top = 1.451389F;
			this.txtCoveredMonth5_2.Width = 0.25F;
			// 
			// txtCoveredMonth6_2
			// 
			this.txtCoveredMonth6_2.Height = 0.1770833F;
			this.txtCoveredMonth6_2.Left = 5.472222F;
			this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
			this.txtCoveredMonth6_2.Text = null;
			this.txtCoveredMonth6_2.Top = 1.451389F;
			this.txtCoveredMonth6_2.Width = 0.25F;
			// 
			// txtCoveredMonth7_2
			// 
			this.txtCoveredMonth7_2.Height = 0.1770833F;
			this.txtCoveredMonth7_2.Left = 5.767361F;
			this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
			this.txtCoveredMonth7_2.Text = null;
			this.txtCoveredMonth7_2.Top = 1.451389F;
			this.txtCoveredMonth7_2.Width = 0.25F;
			// 
			// txtCoveredMonth8_2
			// 
			this.txtCoveredMonth8_2.Height = 0.1770833F;
			this.txtCoveredMonth8_2.Left = 6.0625F;
			this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
			this.txtCoveredMonth8_2.Text = null;
			this.txtCoveredMonth8_2.Top = 1.451389F;
			this.txtCoveredMonth8_2.Width = 0.25F;
			// 
			// txtCoveredMonth9_2
			// 
			this.txtCoveredMonth9_2.Height = 0.1770833F;
			this.txtCoveredMonth9_2.Left = 6.357639F;
			this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
			this.txtCoveredMonth9_2.Text = null;
			this.txtCoveredMonth9_2.Top = 1.451389F;
			this.txtCoveredMonth9_2.Width = 0.25F;
			// 
			// txtCoveredMonth10_2
			// 
			this.txtCoveredMonth10_2.Height = 0.1770833F;
			this.txtCoveredMonth10_2.Left = 6.652778F;
			this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
			this.txtCoveredMonth10_2.Text = null;
			this.txtCoveredMonth10_2.Top = 1.451389F;
			this.txtCoveredMonth10_2.Width = 0.25F;
			// 
			// txtCoveredMonth11_2
			// 
			this.txtCoveredMonth11_2.Height = 0.1770833F;
			this.txtCoveredMonth11_2.Left = 6.947917F;
			this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
			this.txtCoveredMonth11_2.Text = null;
			this.txtCoveredMonth11_2.Top = 1.451389F;
			this.txtCoveredMonth11_2.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_2
			// 
			this.txtCoveredMonth12_2.Height = 0.1770833F;
			this.txtCoveredMonth12_2.Left = 7.243055F;
			this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
			this.txtCoveredMonth12_2.Text = null;
			this.txtCoveredMonth12_2.Top = 1.451389F;
			this.txtCoveredMonth12_2.Width = 0.1875F;
			// 
			// txtCoveredAll12_3
			// 
			this.txtCoveredAll12_3.Height = 0.1770833F;
			this.txtCoveredAll12_3.Left = 3.609028F;
			this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
			this.txtCoveredAll12_3.Text = null;
			this.txtCoveredAll12_3.Top = 1.826389F;
			this.txtCoveredAll12_3.Width = 0.25F;
			// 
			// txtCoveredMonth1_3
			// 
			this.txtCoveredMonth1_3.Height = 0.1770833F;
			this.txtCoveredMonth1_3.Left = 3.996528F;
			this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
			this.txtCoveredMonth1_3.Text = null;
			this.txtCoveredMonth1_3.Top = 1.826389F;
			this.txtCoveredMonth1_3.Width = 0.25F;
			// 
			// txtCoveredMonth2_3
			// 
			this.txtCoveredMonth2_3.Height = 0.1770833F;
			this.txtCoveredMonth2_3.Left = 4.291667F;
			this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
			this.txtCoveredMonth2_3.Text = null;
			this.txtCoveredMonth2_3.Top = 1.826389F;
			this.txtCoveredMonth2_3.Width = 0.25F;
			// 
			// txtCoveredMonth3_3
			// 
			this.txtCoveredMonth3_3.Height = 0.1770833F;
			this.txtCoveredMonth3_3.Left = 4.586805F;
			this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
			this.txtCoveredMonth3_3.Text = null;
			this.txtCoveredMonth3_3.Top = 1.826389F;
			this.txtCoveredMonth3_3.Width = 0.25F;
			// 
			// txtCoveredMonth4_3
			// 
			this.txtCoveredMonth4_3.Height = 0.1770833F;
			this.txtCoveredMonth4_3.Left = 4.881945F;
			this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
			this.txtCoveredMonth4_3.Text = null;
			this.txtCoveredMonth4_3.Top = 1.826389F;
			this.txtCoveredMonth4_3.Width = 0.25F;
			// 
			// txtCoveredMonth5_3
			// 
			this.txtCoveredMonth5_3.Height = 0.1770833F;
			this.txtCoveredMonth5_3.Left = 5.177083F;
			this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
			this.txtCoveredMonth5_3.Text = null;
			this.txtCoveredMonth5_3.Top = 1.826389F;
			this.txtCoveredMonth5_3.Width = 0.25F;
			// 
			// txtCoveredMonth6_3
			// 
			this.txtCoveredMonth6_3.Height = 0.1770833F;
			this.txtCoveredMonth6_3.Left = 5.472222F;
			this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
			this.txtCoveredMonth6_3.Text = null;
			this.txtCoveredMonth6_3.Top = 1.826389F;
			this.txtCoveredMonth6_3.Width = 0.25F;
			// 
			// txtCoveredMonth7_3
			// 
			this.txtCoveredMonth7_3.Height = 0.1770833F;
			this.txtCoveredMonth7_3.Left = 5.767361F;
			this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
			this.txtCoveredMonth7_3.Text = null;
			this.txtCoveredMonth7_3.Top = 1.826389F;
			this.txtCoveredMonth7_3.Width = 0.25F;
			// 
			// txtCoveredMonth8_3
			// 
			this.txtCoveredMonth8_3.Height = 0.1770833F;
			this.txtCoveredMonth8_3.Left = 6.0625F;
			this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
			this.txtCoveredMonth8_3.Text = null;
			this.txtCoveredMonth8_3.Top = 1.826389F;
			this.txtCoveredMonth8_3.Width = 0.25F;
			// 
			// txtCoveredMonth9_3
			// 
			this.txtCoveredMonth9_3.Height = 0.1770833F;
			this.txtCoveredMonth9_3.Left = 6.357639F;
			this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
			this.txtCoveredMonth9_3.Text = null;
			this.txtCoveredMonth9_3.Top = 1.826389F;
			this.txtCoveredMonth9_3.Width = 0.25F;
			// 
			// txtCoveredMonth10_3
			// 
			this.txtCoveredMonth10_3.Height = 0.1770833F;
			this.txtCoveredMonth10_3.Left = 6.652778F;
			this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
			this.txtCoveredMonth10_3.Text = null;
			this.txtCoveredMonth10_3.Top = 1.826389F;
			this.txtCoveredMonth10_3.Width = 0.25F;
			// 
			// txtCoveredMonth11_3
			// 
			this.txtCoveredMonth11_3.Height = 0.1770833F;
			this.txtCoveredMonth11_3.Left = 6.947917F;
			this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
			this.txtCoveredMonth11_3.Text = null;
			this.txtCoveredMonth11_3.Top = 1.826389F;
			this.txtCoveredMonth11_3.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_3
			// 
			this.txtCoveredMonth12_3.Height = 0.1770833F;
			this.txtCoveredMonth12_3.Left = 7.243055F;
			this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
			this.txtCoveredMonth12_3.Text = null;
			this.txtCoveredMonth12_3.Top = 1.826389F;
			this.txtCoveredMonth12_3.Width = 0.1875F;
			// 
			// txtCoveredAll12_4
			// 
			this.txtCoveredAll12_4.Height = 0.1770833F;
			this.txtCoveredAll12_4.Left = 3.609028F;
			this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
			this.txtCoveredAll12_4.Text = null;
			this.txtCoveredAll12_4.Top = 2.201389F;
			this.txtCoveredAll12_4.Width = 0.25F;
			// 
			// txtCoveredMonth1_4
			// 
			this.txtCoveredMonth1_4.Height = 0.1770833F;
			this.txtCoveredMonth1_4.Left = 3.996528F;
			this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
			this.txtCoveredMonth1_4.Text = null;
			this.txtCoveredMonth1_4.Top = 2.201389F;
			this.txtCoveredMonth1_4.Width = 0.25F;
			// 
			// txtCoveredMonth2_4
			// 
			this.txtCoveredMonth2_4.Height = 0.1770833F;
			this.txtCoveredMonth2_4.Left = 4.291667F;
			this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
			this.txtCoveredMonth2_4.Text = null;
			this.txtCoveredMonth2_4.Top = 2.201389F;
			this.txtCoveredMonth2_4.Width = 0.25F;
			// 
			// txtCoveredMonth3_4
			// 
			this.txtCoveredMonth3_4.Height = 0.1770833F;
			this.txtCoveredMonth3_4.Left = 4.586805F;
			this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
			this.txtCoveredMonth3_4.Text = null;
			this.txtCoveredMonth3_4.Top = 2.201389F;
			this.txtCoveredMonth3_4.Width = 0.25F;
			// 
			// txtCoveredMonth4_4
			// 
			this.txtCoveredMonth4_4.Height = 0.1770833F;
			this.txtCoveredMonth4_4.Left = 4.881945F;
			this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
			this.txtCoveredMonth4_4.Text = null;
			this.txtCoveredMonth4_4.Top = 2.201389F;
			this.txtCoveredMonth4_4.Width = 0.25F;
			// 
			// txtCoveredMonth5_4
			// 
			this.txtCoveredMonth5_4.Height = 0.1770833F;
			this.txtCoveredMonth5_4.Left = 5.177083F;
			this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
			this.txtCoveredMonth5_4.Text = null;
			this.txtCoveredMonth5_4.Top = 2.201389F;
			this.txtCoveredMonth5_4.Width = 0.25F;
			// 
			// txtCoveredMonth6_4
			// 
			this.txtCoveredMonth6_4.Height = 0.1770833F;
			this.txtCoveredMonth6_4.Left = 5.472222F;
			this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
			this.txtCoveredMonth6_4.Text = null;
			this.txtCoveredMonth6_4.Top = 2.201389F;
			this.txtCoveredMonth6_4.Width = 0.25F;
			// 
			// txtCoveredMonth7_4
			// 
			this.txtCoveredMonth7_4.Height = 0.1770833F;
			this.txtCoveredMonth7_4.Left = 5.767361F;
			this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
			this.txtCoveredMonth7_4.Text = null;
			this.txtCoveredMonth7_4.Top = 2.201389F;
			this.txtCoveredMonth7_4.Width = 0.25F;
			// 
			// txtCoveredMonth8_4
			// 
			this.txtCoveredMonth8_4.Height = 0.1770833F;
			this.txtCoveredMonth8_4.Left = 6.0625F;
			this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
			this.txtCoveredMonth8_4.Text = null;
			this.txtCoveredMonth8_4.Top = 2.201389F;
			this.txtCoveredMonth8_4.Width = 0.25F;
			// 
			// txtCoveredMonth9_4
			// 
			this.txtCoveredMonth9_4.Height = 0.1770833F;
			this.txtCoveredMonth9_4.Left = 6.357639F;
			this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
			this.txtCoveredMonth9_4.Text = null;
			this.txtCoveredMonth9_4.Top = 2.201389F;
			this.txtCoveredMonth9_4.Width = 0.25F;
			// 
			// txtCoveredMonth10_4
			// 
			this.txtCoveredMonth10_4.Height = 0.1770833F;
			this.txtCoveredMonth10_4.Left = 6.652778F;
			this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
			this.txtCoveredMonth10_4.Text = null;
			this.txtCoveredMonth10_4.Top = 2.201389F;
			this.txtCoveredMonth10_4.Width = 0.25F;
			// 
			// txtCoveredMonth11_4
			// 
			this.txtCoveredMonth11_4.Height = 0.1770833F;
			this.txtCoveredMonth11_4.Left = 6.947917F;
			this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
			this.txtCoveredMonth11_4.Text = null;
			this.txtCoveredMonth11_4.Top = 2.201389F;
			this.txtCoveredMonth11_4.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_4
			// 
			this.txtCoveredMonth12_4.Height = 0.1770833F;
			this.txtCoveredMonth12_4.Left = 7.243055F;
			this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
			this.txtCoveredMonth12_4.Text = null;
			this.txtCoveredMonth12_4.Top = 2.201389F;
			this.txtCoveredMonth12_4.Width = 0.1875F;
			// 
			// txtCoveredAll12_5
			// 
			this.txtCoveredAll12_5.Height = 0.1770833F;
			this.txtCoveredAll12_5.Left = 3.609028F;
			this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
			this.txtCoveredAll12_5.Text = null;
			this.txtCoveredAll12_5.Top = 2.576389F;
			this.txtCoveredAll12_5.Width = 0.25F;
			// 
			// txtCoveredMonth1_5
			// 
			this.txtCoveredMonth1_5.Height = 0.1770833F;
			this.txtCoveredMonth1_5.Left = 3.996528F;
			this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
			this.txtCoveredMonth1_5.Text = null;
			this.txtCoveredMonth1_5.Top = 2.576389F;
			this.txtCoveredMonth1_5.Width = 0.25F;
			// 
			// txtCoveredMonth2_5
			// 
			this.txtCoveredMonth2_5.Height = 0.1770833F;
			this.txtCoveredMonth2_5.Left = 4.291667F;
			this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
			this.txtCoveredMonth2_5.Text = null;
			this.txtCoveredMonth2_5.Top = 2.576389F;
			this.txtCoveredMonth2_5.Width = 0.25F;
			// 
			// txtCoveredMonth3_5
			// 
			this.txtCoveredMonth3_5.Height = 0.1770833F;
			this.txtCoveredMonth3_5.Left = 4.586805F;
			this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
			this.txtCoveredMonth3_5.Text = null;
			this.txtCoveredMonth3_5.Top = 2.576389F;
			this.txtCoveredMonth3_5.Width = 0.25F;
			// 
			// txtCoveredMonth4_5
			// 
			this.txtCoveredMonth4_5.Height = 0.1770833F;
			this.txtCoveredMonth4_5.Left = 4.881945F;
			this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
			this.txtCoveredMonth4_5.Text = null;
			this.txtCoveredMonth4_5.Top = 2.576389F;
			this.txtCoveredMonth4_5.Width = 0.25F;
			// 
			// txtCoveredMonth5_5
			// 
			this.txtCoveredMonth5_5.Height = 0.1770833F;
			this.txtCoveredMonth5_5.Left = 5.177083F;
			this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
			this.txtCoveredMonth5_5.Text = null;
			this.txtCoveredMonth5_5.Top = 2.576389F;
			this.txtCoveredMonth5_5.Width = 0.25F;
			// 
			// txtCoveredMonth6_5
			// 
			this.txtCoveredMonth6_5.Height = 0.1770833F;
			this.txtCoveredMonth6_5.Left = 5.472222F;
			this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
			this.txtCoveredMonth6_5.Text = null;
			this.txtCoveredMonth6_5.Top = 2.576389F;
			this.txtCoveredMonth6_5.Width = 0.25F;
			// 
			// txtCoveredMonth7_5
			// 
			this.txtCoveredMonth7_5.Height = 0.1770833F;
			this.txtCoveredMonth7_5.Left = 5.767361F;
			this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
			this.txtCoveredMonth7_5.Text = null;
			this.txtCoveredMonth7_5.Top = 2.576389F;
			this.txtCoveredMonth7_5.Width = 0.25F;
			// 
			// txtCoveredMonth8_5
			// 
			this.txtCoveredMonth8_5.Height = 0.1770833F;
			this.txtCoveredMonth8_5.Left = 6.0625F;
			this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
			this.txtCoveredMonth8_5.Text = null;
			this.txtCoveredMonth8_5.Top = 2.576389F;
			this.txtCoveredMonth8_5.Width = 0.25F;
			// 
			// txtCoveredMonth9_5
			// 
			this.txtCoveredMonth9_5.Height = 0.1770833F;
			this.txtCoveredMonth9_5.Left = 6.357639F;
			this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
			this.txtCoveredMonth9_5.Text = null;
			this.txtCoveredMonth9_5.Top = 2.576389F;
			this.txtCoveredMonth9_5.Width = 0.25F;
			// 
			// txtCoveredMonth10_5
			// 
			this.txtCoveredMonth10_5.Height = 0.1770833F;
			this.txtCoveredMonth10_5.Left = 6.652778F;
			this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
			this.txtCoveredMonth10_5.Text = null;
			this.txtCoveredMonth10_5.Top = 2.576389F;
			this.txtCoveredMonth10_5.Width = 0.25F;
			// 
			// txtCoveredMonth11_5
			// 
			this.txtCoveredMonth11_5.Height = 0.1770833F;
			this.txtCoveredMonth11_5.Left = 6.947917F;
			this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
			this.txtCoveredMonth11_5.Text = null;
			this.txtCoveredMonth11_5.Top = 2.576389F;
			this.txtCoveredMonth11_5.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_5
			// 
			this.txtCoveredMonth12_5.Height = 0.1770833F;
			this.txtCoveredMonth12_5.Left = 7.243055F;
			this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
			this.txtCoveredMonth12_5.Text = null;
			this.txtCoveredMonth12_5.Top = 2.576389F;
			this.txtCoveredMonth12_5.Width = 0.1875F;
			// 
			// txtCoveredAll12_6
			// 
			this.txtCoveredAll12_6.Height = 0.1770833F;
			this.txtCoveredAll12_6.Left = 3.609028F;
			this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
			this.txtCoveredAll12_6.Text = null;
			this.txtCoveredAll12_6.Top = 2.951389F;
			this.txtCoveredAll12_6.Width = 0.25F;
			// 
			// txtCoveredMonth1_6
			// 
			this.txtCoveredMonth1_6.Height = 0.1770833F;
			this.txtCoveredMonth1_6.Left = 3.996528F;
			this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
			this.txtCoveredMonth1_6.Text = null;
			this.txtCoveredMonth1_6.Top = 2.951389F;
			this.txtCoveredMonth1_6.Width = 0.25F;
			// 
			// txtCoveredMonth2_6
			// 
			this.txtCoveredMonth2_6.Height = 0.1770833F;
			this.txtCoveredMonth2_6.Left = 4.291667F;
			this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
			this.txtCoveredMonth2_6.Text = null;
			this.txtCoveredMonth2_6.Top = 2.951389F;
			this.txtCoveredMonth2_6.Width = 0.25F;
			// 
			// txtCoveredMonth3_6
			// 
			this.txtCoveredMonth3_6.Height = 0.1770833F;
			this.txtCoveredMonth3_6.Left = 4.586805F;
			this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
			this.txtCoveredMonth3_6.Text = null;
			this.txtCoveredMonth3_6.Top = 2.951389F;
			this.txtCoveredMonth3_6.Width = 0.25F;
			// 
			// txtCoveredMonth4_6
			// 
			this.txtCoveredMonth4_6.Height = 0.1770833F;
			this.txtCoveredMonth4_6.Left = 4.881945F;
			this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
			this.txtCoveredMonth4_6.Text = null;
			this.txtCoveredMonth4_6.Top = 2.951389F;
			this.txtCoveredMonth4_6.Width = 0.25F;
			// 
			// txtCoveredMonth5_6
			// 
			this.txtCoveredMonth5_6.Height = 0.1770833F;
			this.txtCoveredMonth5_6.Left = 5.177083F;
			this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
			this.txtCoveredMonth5_6.Text = null;
			this.txtCoveredMonth5_6.Top = 2.951389F;
			this.txtCoveredMonth5_6.Width = 0.25F;
			// 
			// txtCoveredMonth6_6
			// 
			this.txtCoveredMonth6_6.Height = 0.1770833F;
			this.txtCoveredMonth6_6.Left = 5.472222F;
			this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
			this.txtCoveredMonth6_6.Text = null;
			this.txtCoveredMonth6_6.Top = 2.951389F;
			this.txtCoveredMonth6_6.Width = 0.25F;
			// 
			// txtCoveredMonth7_6
			// 
			this.txtCoveredMonth7_6.Height = 0.1770833F;
			this.txtCoveredMonth7_6.Left = 5.767361F;
			this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
			this.txtCoveredMonth7_6.Text = null;
			this.txtCoveredMonth7_6.Top = 2.951389F;
			this.txtCoveredMonth7_6.Width = 0.25F;
			// 
			// txtCoveredMonth8_6
			// 
			this.txtCoveredMonth8_6.Height = 0.1770833F;
			this.txtCoveredMonth8_6.Left = 6.0625F;
			this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
			this.txtCoveredMonth8_6.Text = null;
			this.txtCoveredMonth8_6.Top = 2.951389F;
			this.txtCoveredMonth8_6.Width = 0.25F;
			// 
			// txtCoveredMonth9_6
			// 
			this.txtCoveredMonth9_6.Height = 0.1770833F;
			this.txtCoveredMonth9_6.Left = 6.357639F;
			this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
			this.txtCoveredMonth9_6.Text = null;
			this.txtCoveredMonth9_6.Top = 2.951389F;
			this.txtCoveredMonth9_6.Width = 0.25F;
			// 
			// txtCoveredMonth10_6
			// 
			this.txtCoveredMonth10_6.Height = 0.1770833F;
			this.txtCoveredMonth10_6.Left = 6.652778F;
			this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
			this.txtCoveredMonth10_6.Text = null;
			this.txtCoveredMonth10_6.Top = 2.951389F;
			this.txtCoveredMonth10_6.Width = 0.25F;
			// 
			// txtCoveredMonth11_6
			// 
			this.txtCoveredMonth11_6.Height = 0.1770833F;
			this.txtCoveredMonth11_6.Left = 6.947917F;
			this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
			this.txtCoveredMonth11_6.Text = null;
			this.txtCoveredMonth11_6.Top = 2.951389F;
			this.txtCoveredMonth11_6.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_6
			// 
			this.txtCoveredMonth12_6.Height = 0.1770833F;
			this.txtCoveredMonth12_6.Left = 7.243055F;
			this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
			this.txtCoveredMonth12_6.Text = null;
			this.txtCoveredMonth12_6.Top = 2.951389F;
			this.txtCoveredMonth12_6.Width = 0.1875F;
			// 
			// txtCoveredName7
			// 
			this.txtCoveredName7.Height = 0.1770833F;
			this.txtCoveredName7.Left = 0.1666667F;
			this.txtCoveredName7.Name = "txtCoveredName7";
			this.txtCoveredName7.Style = "font-size: 8.5pt";
			this.txtCoveredName7.Text = null;
			this.txtCoveredName7.Top = 3.326389F;
			this.txtCoveredName7.Width = 1.583333F;
			// 
			// txtCoveredSSN7
			// 
			this.txtCoveredSSN7.Height = 0.1770833F;
			this.txtCoveredSSN7.Left = 1.833333F;
			this.txtCoveredSSN7.Name = "txtCoveredSSN7";
			this.txtCoveredSSN7.Style = "font-size: 8.5pt";
			this.txtCoveredSSN7.Text = null;
			this.txtCoveredSSN7.Top = 3.326389F;
			this.txtCoveredSSN7.Width = 0.8333333F;
			// 
			// txtCoveredDOB7
			// 
			this.txtCoveredDOB7.Height = 0.1770833F;
			this.txtCoveredDOB7.Left = 2.75F;
			this.txtCoveredDOB7.Name = "txtCoveredDOB7";
			this.txtCoveredDOB7.Style = "font-size: 8.5pt";
			this.txtCoveredDOB7.Text = null;
			this.txtCoveredDOB7.Top = 3.326389F;
			this.txtCoveredDOB7.Width = 0.75F;
			// 
			// txtCoveredName8
			// 
			this.txtCoveredName8.Height = 0.1770833F;
			this.txtCoveredName8.Left = 0.1666667F;
			this.txtCoveredName8.Name = "txtCoveredName8";
			this.txtCoveredName8.Style = "font-size: 8.5pt";
			this.txtCoveredName8.Text = null;
			this.txtCoveredName8.Top = 3.701389F;
			this.txtCoveredName8.Width = 1.583333F;
			// 
			// txtCoveredSSN8
			// 
			this.txtCoveredSSN8.Height = 0.1770833F;
			this.txtCoveredSSN8.Left = 1.833333F;
			this.txtCoveredSSN8.Name = "txtCoveredSSN8";
			this.txtCoveredSSN8.Style = "font-size: 8.5pt";
			this.txtCoveredSSN8.Text = null;
			this.txtCoveredSSN8.Top = 3.701389F;
			this.txtCoveredSSN8.Width = 0.8333333F;
			// 
			// txtCoveredDOB8
			// 
			this.txtCoveredDOB8.Height = 0.1770833F;
			this.txtCoveredDOB8.Left = 2.75F;
			this.txtCoveredDOB8.Name = "txtCoveredDOB8";
			this.txtCoveredDOB8.Style = "font-size: 8.5pt";
			this.txtCoveredDOB8.Text = null;
			this.txtCoveredDOB8.Top = 3.701389F;
			this.txtCoveredDOB8.Width = 0.75F;
			// 
			// txtCoveredName9
			// 
			this.txtCoveredName9.Height = 0.1770833F;
			this.txtCoveredName9.Left = 0.1666667F;
			this.txtCoveredName9.Name = "txtCoveredName9";
			this.txtCoveredName9.Style = "font-size: 8.5pt";
			this.txtCoveredName9.Text = null;
			this.txtCoveredName9.Top = 4.076389F;
			this.txtCoveredName9.Width = 1.583333F;
			// 
			// txtCoveredSSN9
			// 
			this.txtCoveredSSN9.Height = 0.1770833F;
			this.txtCoveredSSN9.Left = 1.833333F;
			this.txtCoveredSSN9.Name = "txtCoveredSSN9";
			this.txtCoveredSSN9.Style = "font-size: 8.5pt";
			this.txtCoveredSSN9.Text = null;
			this.txtCoveredSSN9.Top = 4.076389F;
			this.txtCoveredSSN9.Width = 0.8333333F;
			// 
			// txtCoveredDOB9
			// 
			this.txtCoveredDOB9.Height = 0.1770833F;
			this.txtCoveredDOB9.Left = 2.75F;
			this.txtCoveredDOB9.Name = "txtCoveredDOB9";
			this.txtCoveredDOB9.Style = "font-size: 8.5pt";
			this.txtCoveredDOB9.Text = null;
			this.txtCoveredDOB9.Top = 4.076389F;
			this.txtCoveredDOB9.Width = 0.75F;
			// 
			// txtCoveredName10
			// 
			this.txtCoveredName10.Height = 0.1770833F;
			this.txtCoveredName10.Left = 0.1666667F;
			this.txtCoveredName10.Name = "txtCoveredName10";
			this.txtCoveredName10.Style = "font-size: 8.5pt";
			this.txtCoveredName10.Text = null;
			this.txtCoveredName10.Top = 4.458333F;
			this.txtCoveredName10.Width = 1.583333F;
			// 
			// txtCoveredSSN10
			// 
			this.txtCoveredSSN10.Height = 0.1770833F;
			this.txtCoveredSSN10.Left = 1.833333F;
			this.txtCoveredSSN10.Name = "txtCoveredSSN10";
			this.txtCoveredSSN10.Style = "font-size: 8.5pt";
			this.txtCoveredSSN10.Text = null;
			this.txtCoveredSSN10.Top = 4.458333F;
			this.txtCoveredSSN10.Width = 0.8333333F;
			// 
			// txtCoveredDOB10
			// 
			this.txtCoveredDOB10.Height = 0.1770833F;
			this.txtCoveredDOB10.Left = 2.75F;
			this.txtCoveredDOB10.Name = "txtCoveredDOB10";
			this.txtCoveredDOB10.Style = "font-size: 8.5pt";
			this.txtCoveredDOB10.Text = null;
			this.txtCoveredDOB10.Top = 4.458333F;
			this.txtCoveredDOB10.Width = 0.75F;
			// 
			// txtCoveredName11
			// 
			this.txtCoveredName11.Height = 0.1770833F;
			this.txtCoveredName11.Left = 0.1666667F;
			this.txtCoveredName11.Name = "txtCoveredName11";
			this.txtCoveredName11.Style = "font-size: 8.5pt";
			this.txtCoveredName11.Text = null;
			this.txtCoveredName11.Top = 4.833333F;
			this.txtCoveredName11.Width = 1.583333F;
			// 
			// txtCoveredSSN11
			// 
			this.txtCoveredSSN11.Height = 0.1770833F;
			this.txtCoveredSSN11.Left = 1.833333F;
			this.txtCoveredSSN11.Name = "txtCoveredSSN11";
			this.txtCoveredSSN11.Style = "font-size: 8.5pt";
			this.txtCoveredSSN11.Text = null;
			this.txtCoveredSSN11.Top = 4.833333F;
			this.txtCoveredSSN11.Width = 0.8333333F;
			// 
			// txtCoveredDOB11
			// 
			this.txtCoveredDOB11.Height = 0.1770833F;
			this.txtCoveredDOB11.Left = 2.75F;
			this.txtCoveredDOB11.Name = "txtCoveredDOB11";
			this.txtCoveredDOB11.Style = "font-size: 8.5pt";
			this.txtCoveredDOB11.Text = null;
			this.txtCoveredDOB11.Top = 4.833333F;
			this.txtCoveredDOB11.Width = 0.75F;
			// 
			// txtCoveredName12
			// 
			this.txtCoveredName12.Height = 0.1770833F;
			this.txtCoveredName12.Left = 0.1666667F;
			this.txtCoveredName12.Name = "txtCoveredName12";
			this.txtCoveredName12.Style = "font-size: 8.5pt";
			this.txtCoveredName12.Text = null;
			this.txtCoveredName12.Top = 5.208333F;
			this.txtCoveredName12.Width = 1.583333F;
			// 
			// txtCoveredSSN12
			// 
			this.txtCoveredSSN12.Height = 0.1770833F;
			this.txtCoveredSSN12.Left = 1.833333F;
			this.txtCoveredSSN12.Name = "txtCoveredSSN12";
			this.txtCoveredSSN12.Style = "font-size: 8.5pt";
			this.txtCoveredSSN12.Text = null;
			this.txtCoveredSSN12.Top = 5.208333F;
			this.txtCoveredSSN12.Width = 0.8333333F;
			// 
			// txtCoveredDOB12
			// 
			this.txtCoveredDOB12.Height = 0.1770833F;
			this.txtCoveredDOB12.Left = 2.75F;
			this.txtCoveredDOB12.Name = "txtCoveredDOB12";
			this.txtCoveredDOB12.Style = "font-size: 8.5pt";
			this.txtCoveredDOB12.Text = null;
			this.txtCoveredDOB12.Top = 5.208333F;
			this.txtCoveredDOB12.Width = 0.75F;
			// 
			// txtCoveredAll12_7
			// 
			this.txtCoveredAll12_7.Height = 0.1770833F;
			this.txtCoveredAll12_7.Left = 3.609028F;
			this.txtCoveredAll12_7.Name = "txtCoveredAll12_7";
			this.txtCoveredAll12_7.Text = null;
			this.txtCoveredAll12_7.Top = 3.326389F;
			this.txtCoveredAll12_7.Width = 0.25F;
			// 
			// txtCoveredMonth1_7
			// 
			this.txtCoveredMonth1_7.Height = 0.1770833F;
			this.txtCoveredMonth1_7.Left = 3.996528F;
			this.txtCoveredMonth1_7.Name = "txtCoveredMonth1_7";
			this.txtCoveredMonth1_7.Text = null;
			this.txtCoveredMonth1_7.Top = 3.326389F;
			this.txtCoveredMonth1_7.Width = 0.25F;
			// 
			// txtCoveredMonth2_7
			// 
			this.txtCoveredMonth2_7.Height = 0.1770833F;
			this.txtCoveredMonth2_7.Left = 4.291667F;
			this.txtCoveredMonth2_7.Name = "txtCoveredMonth2_7";
			this.txtCoveredMonth2_7.Text = null;
			this.txtCoveredMonth2_7.Top = 3.326389F;
			this.txtCoveredMonth2_7.Width = 0.25F;
			// 
			// txtCoveredMonth3_7
			// 
			this.txtCoveredMonth3_7.Height = 0.1770833F;
			this.txtCoveredMonth3_7.Left = 4.586805F;
			this.txtCoveredMonth3_7.Name = "txtCoveredMonth3_7";
			this.txtCoveredMonth3_7.Text = null;
			this.txtCoveredMonth3_7.Top = 3.326389F;
			this.txtCoveredMonth3_7.Width = 0.25F;
			// 
			// txtCoveredMonth4_7
			// 
			this.txtCoveredMonth4_7.Height = 0.1770833F;
			this.txtCoveredMonth4_7.Left = 4.881945F;
			this.txtCoveredMonth4_7.Name = "txtCoveredMonth4_7";
			this.txtCoveredMonth4_7.Text = null;
			this.txtCoveredMonth4_7.Top = 3.326389F;
			this.txtCoveredMonth4_7.Width = 0.25F;
			// 
			// txtCoveredMonth5_7
			// 
			this.txtCoveredMonth5_7.Height = 0.1770833F;
			this.txtCoveredMonth5_7.Left = 5.177083F;
			this.txtCoveredMonth5_7.Name = "txtCoveredMonth5_7";
			this.txtCoveredMonth5_7.Text = null;
			this.txtCoveredMonth5_7.Top = 3.326389F;
			this.txtCoveredMonth5_7.Width = 0.25F;
			// 
			// txtCoveredMonth6_7
			// 
			this.txtCoveredMonth6_7.Height = 0.1770833F;
			this.txtCoveredMonth6_7.Left = 5.472222F;
			this.txtCoveredMonth6_7.Name = "txtCoveredMonth6_7";
			this.txtCoveredMonth6_7.Text = null;
			this.txtCoveredMonth6_7.Top = 3.326389F;
			this.txtCoveredMonth6_7.Width = 0.25F;
			// 
			// txtCoveredMonth7_7
			// 
			this.txtCoveredMonth7_7.Height = 0.1770833F;
			this.txtCoveredMonth7_7.Left = 5.767361F;
			this.txtCoveredMonth7_7.Name = "txtCoveredMonth7_7";
			this.txtCoveredMonth7_7.Text = null;
			this.txtCoveredMonth7_7.Top = 3.326389F;
			this.txtCoveredMonth7_7.Width = 0.25F;
			// 
			// txtCoveredMonth8_7
			// 
			this.txtCoveredMonth8_7.Height = 0.1770833F;
			this.txtCoveredMonth8_7.Left = 6.0625F;
			this.txtCoveredMonth8_7.Name = "txtCoveredMonth8_7";
			this.txtCoveredMonth8_7.Text = null;
			this.txtCoveredMonth8_7.Top = 3.326389F;
			this.txtCoveredMonth8_7.Width = 0.25F;
			// 
			// txtCoveredMonth9_7
			// 
			this.txtCoveredMonth9_7.Height = 0.1770833F;
			this.txtCoveredMonth9_7.Left = 6.357639F;
			this.txtCoveredMonth9_7.Name = "txtCoveredMonth9_7";
			this.txtCoveredMonth9_7.Text = null;
			this.txtCoveredMonth9_7.Top = 3.326389F;
			this.txtCoveredMonth9_7.Width = 0.25F;
			// 
			// txtCoveredMonth10_7
			// 
			this.txtCoveredMonth10_7.Height = 0.1770833F;
			this.txtCoveredMonth10_7.Left = 6.652778F;
			this.txtCoveredMonth10_7.Name = "txtCoveredMonth10_7";
			this.txtCoveredMonth10_7.Text = null;
			this.txtCoveredMonth10_7.Top = 3.326389F;
			this.txtCoveredMonth10_7.Width = 0.25F;
			// 
			// txtCoveredMonth11_7
			// 
			this.txtCoveredMonth11_7.Height = 0.1770833F;
			this.txtCoveredMonth11_7.Left = 6.947917F;
			this.txtCoveredMonth11_7.Name = "txtCoveredMonth11_7";
			this.txtCoveredMonth11_7.Text = null;
			this.txtCoveredMonth11_7.Top = 3.326389F;
			this.txtCoveredMonth11_7.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_7
			// 
			this.txtCoveredMonth12_7.Height = 0.1770833F;
			this.txtCoveredMonth12_7.Left = 7.243055F;
			this.txtCoveredMonth12_7.Name = "txtCoveredMonth12_7";
			this.txtCoveredMonth12_7.Text = null;
			this.txtCoveredMonth12_7.Top = 3.326389F;
			this.txtCoveredMonth12_7.Width = 0.1875F;
			// 
			// txtCoveredAll12_8
			// 
			this.txtCoveredAll12_8.Height = 0.1770833F;
			this.txtCoveredAll12_8.Left = 3.609028F;
			this.txtCoveredAll12_8.Name = "txtCoveredAll12_8";
			this.txtCoveredAll12_8.Text = null;
			this.txtCoveredAll12_8.Top = 3.701389F;
			this.txtCoveredAll12_8.Width = 0.25F;
			// 
			// txtCoveredMonth1_8
			// 
			this.txtCoveredMonth1_8.Height = 0.1770833F;
			this.txtCoveredMonth1_8.Left = 3.996528F;
			this.txtCoveredMonth1_8.Name = "txtCoveredMonth1_8";
			this.txtCoveredMonth1_8.Text = null;
			this.txtCoveredMonth1_8.Top = 3.701389F;
			this.txtCoveredMonth1_8.Width = 0.25F;
			// 
			// txtCoveredMonth2_8
			// 
			this.txtCoveredMonth2_8.Height = 0.1770833F;
			this.txtCoveredMonth2_8.Left = 4.291667F;
			this.txtCoveredMonth2_8.Name = "txtCoveredMonth2_8";
			this.txtCoveredMonth2_8.Text = null;
			this.txtCoveredMonth2_8.Top = 3.701389F;
			this.txtCoveredMonth2_8.Width = 0.25F;
			// 
			// txtCoveredMonth3_8
			// 
			this.txtCoveredMonth3_8.Height = 0.1770833F;
			this.txtCoveredMonth3_8.Left = 4.586805F;
			this.txtCoveredMonth3_8.Name = "txtCoveredMonth3_8";
			this.txtCoveredMonth3_8.Text = null;
			this.txtCoveredMonth3_8.Top = 3.701389F;
			this.txtCoveredMonth3_8.Width = 0.25F;
			// 
			// txtCoveredMonth4_8
			// 
			this.txtCoveredMonth4_8.Height = 0.1770833F;
			this.txtCoveredMonth4_8.Left = 4.881945F;
			this.txtCoveredMonth4_8.Name = "txtCoveredMonth4_8";
			this.txtCoveredMonth4_8.Text = null;
			this.txtCoveredMonth4_8.Top = 3.701389F;
			this.txtCoveredMonth4_8.Width = 0.25F;
			// 
			// txtCoveredMonth5_8
			// 
			this.txtCoveredMonth5_8.Height = 0.1770833F;
			this.txtCoveredMonth5_8.Left = 5.177083F;
			this.txtCoveredMonth5_8.Name = "txtCoveredMonth5_8";
			this.txtCoveredMonth5_8.Text = null;
			this.txtCoveredMonth5_8.Top = 3.701389F;
			this.txtCoveredMonth5_8.Width = 0.25F;
			// 
			// txtCoveredMonth6_8
			// 
			this.txtCoveredMonth6_8.Height = 0.1770833F;
			this.txtCoveredMonth6_8.Left = 5.472222F;
			this.txtCoveredMonth6_8.Name = "txtCoveredMonth6_8";
			this.txtCoveredMonth6_8.Text = null;
			this.txtCoveredMonth6_8.Top = 3.701389F;
			this.txtCoveredMonth6_8.Width = 0.25F;
			// 
			// txtCoveredMonth7_8
			// 
			this.txtCoveredMonth7_8.Height = 0.1770833F;
			this.txtCoveredMonth7_8.Left = 5.767361F;
			this.txtCoveredMonth7_8.Name = "txtCoveredMonth7_8";
			this.txtCoveredMonth7_8.Text = null;
			this.txtCoveredMonth7_8.Top = 3.701389F;
			this.txtCoveredMonth7_8.Width = 0.25F;
			// 
			// txtCoveredMonth8_8
			// 
			this.txtCoveredMonth8_8.Height = 0.1770833F;
			this.txtCoveredMonth8_8.Left = 6.0625F;
			this.txtCoveredMonth8_8.Name = "txtCoveredMonth8_8";
			this.txtCoveredMonth8_8.Text = null;
			this.txtCoveredMonth8_8.Top = 3.701389F;
			this.txtCoveredMonth8_8.Width = 0.25F;
			// 
			// txtCoveredMonth9_8
			// 
			this.txtCoveredMonth9_8.Height = 0.1770833F;
			this.txtCoveredMonth9_8.Left = 6.357639F;
			this.txtCoveredMonth9_8.Name = "txtCoveredMonth9_8";
			this.txtCoveredMonth9_8.Text = null;
			this.txtCoveredMonth9_8.Top = 3.701389F;
			this.txtCoveredMonth9_8.Width = 0.25F;
			// 
			// txtCoveredMonth10_8
			// 
			this.txtCoveredMonth10_8.Height = 0.1770833F;
			this.txtCoveredMonth10_8.Left = 6.652778F;
			this.txtCoveredMonth10_8.Name = "txtCoveredMonth10_8";
			this.txtCoveredMonth10_8.Text = null;
			this.txtCoveredMonth10_8.Top = 3.701389F;
			this.txtCoveredMonth10_8.Width = 0.25F;
			// 
			// txtCoveredMonth11_8
			// 
			this.txtCoveredMonth11_8.Height = 0.1770833F;
			this.txtCoveredMonth11_8.Left = 6.947917F;
			this.txtCoveredMonth11_8.Name = "txtCoveredMonth11_8";
			this.txtCoveredMonth11_8.Text = null;
			this.txtCoveredMonth11_8.Top = 3.701389F;
			this.txtCoveredMonth11_8.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_8
			// 
			this.txtCoveredMonth12_8.Height = 0.1770833F;
			this.txtCoveredMonth12_8.Left = 7.243055F;
			this.txtCoveredMonth12_8.Name = "txtCoveredMonth12_8";
			this.txtCoveredMonth12_8.Text = null;
			this.txtCoveredMonth12_8.Top = 3.701389F;
			this.txtCoveredMonth12_8.Width = 0.1875F;
			// 
			// txtCoveredAll12_9
			// 
			this.txtCoveredAll12_9.Height = 0.1770833F;
			this.txtCoveredAll12_9.Left = 3.609028F;
			this.txtCoveredAll12_9.Name = "txtCoveredAll12_9";
			this.txtCoveredAll12_9.Text = null;
			this.txtCoveredAll12_9.Top = 4.076389F;
			this.txtCoveredAll12_9.Width = 0.25F;
			// 
			// txtCoveredMonth1_9
			// 
			this.txtCoveredMonth1_9.Height = 0.1770833F;
			this.txtCoveredMonth1_9.Left = 3.996528F;
			this.txtCoveredMonth1_9.Name = "txtCoveredMonth1_9";
			this.txtCoveredMonth1_9.Text = null;
			this.txtCoveredMonth1_9.Top = 4.076389F;
			this.txtCoveredMonth1_9.Width = 0.25F;
			// 
			// txtCoveredMonth2_9
			// 
			this.txtCoveredMonth2_9.Height = 0.1770833F;
			this.txtCoveredMonth2_9.Left = 4.291667F;
			this.txtCoveredMonth2_9.Name = "txtCoveredMonth2_9";
			this.txtCoveredMonth2_9.Text = null;
			this.txtCoveredMonth2_9.Top = 4.076389F;
			this.txtCoveredMonth2_9.Width = 0.25F;
			// 
			// txtCoveredMonth3_9
			// 
			this.txtCoveredMonth3_9.Height = 0.1770833F;
			this.txtCoveredMonth3_9.Left = 4.586805F;
			this.txtCoveredMonth3_9.Name = "txtCoveredMonth3_9";
			this.txtCoveredMonth3_9.Text = null;
			this.txtCoveredMonth3_9.Top = 4.076389F;
			this.txtCoveredMonth3_9.Width = 0.25F;
			// 
			// txtCoveredMonth4_9
			// 
			this.txtCoveredMonth4_9.Height = 0.1770833F;
			this.txtCoveredMonth4_9.Left = 4.881945F;
			this.txtCoveredMonth4_9.Name = "txtCoveredMonth4_9";
			this.txtCoveredMonth4_9.Text = null;
			this.txtCoveredMonth4_9.Top = 4.076389F;
			this.txtCoveredMonth4_9.Width = 0.25F;
			// 
			// txtCoveredMonth5_9
			// 
			this.txtCoveredMonth5_9.Height = 0.1770833F;
			this.txtCoveredMonth5_9.Left = 5.177083F;
			this.txtCoveredMonth5_9.Name = "txtCoveredMonth5_9";
			this.txtCoveredMonth5_9.Text = null;
			this.txtCoveredMonth5_9.Top = 4.076389F;
			this.txtCoveredMonth5_9.Width = 0.25F;
			// 
			// txtCoveredMonth6_9
			// 
			this.txtCoveredMonth6_9.Height = 0.1770833F;
			this.txtCoveredMonth6_9.Left = 5.472222F;
			this.txtCoveredMonth6_9.Name = "txtCoveredMonth6_9";
			this.txtCoveredMonth6_9.Text = null;
			this.txtCoveredMonth6_9.Top = 4.076389F;
			this.txtCoveredMonth6_9.Width = 0.25F;
			// 
			// txtCoveredMonth7_9
			// 
			this.txtCoveredMonth7_9.Height = 0.1770833F;
			this.txtCoveredMonth7_9.Left = 5.767361F;
			this.txtCoveredMonth7_9.Name = "txtCoveredMonth7_9";
			this.txtCoveredMonth7_9.Text = null;
			this.txtCoveredMonth7_9.Top = 4.076389F;
			this.txtCoveredMonth7_9.Width = 0.25F;
			// 
			// txtCoveredMonth8_9
			// 
			this.txtCoveredMonth8_9.Height = 0.1770833F;
			this.txtCoveredMonth8_9.Left = 6.0625F;
			this.txtCoveredMonth8_9.Name = "txtCoveredMonth8_9";
			this.txtCoveredMonth8_9.Text = null;
			this.txtCoveredMonth8_9.Top = 4.076389F;
			this.txtCoveredMonth8_9.Width = 0.25F;
			// 
			// txtCoveredMonth9_9
			// 
			this.txtCoveredMonth9_9.Height = 0.1770833F;
			this.txtCoveredMonth9_9.Left = 6.357639F;
			this.txtCoveredMonth9_9.Name = "txtCoveredMonth9_9";
			this.txtCoveredMonth9_9.Text = null;
			this.txtCoveredMonth9_9.Top = 4.076389F;
			this.txtCoveredMonth9_9.Width = 0.25F;
			// 
			// txtCoveredMonth10_9
			// 
			this.txtCoveredMonth10_9.Height = 0.1770833F;
			this.txtCoveredMonth10_9.Left = 6.652778F;
			this.txtCoveredMonth10_9.Name = "txtCoveredMonth10_9";
			this.txtCoveredMonth10_9.Text = null;
			this.txtCoveredMonth10_9.Top = 4.076389F;
			this.txtCoveredMonth10_9.Width = 0.25F;
			// 
			// txtCoveredMonth11_9
			// 
			this.txtCoveredMonth11_9.Height = 0.1770833F;
			this.txtCoveredMonth11_9.Left = 6.947917F;
			this.txtCoveredMonth11_9.Name = "txtCoveredMonth11_9";
			this.txtCoveredMonth11_9.Text = null;
			this.txtCoveredMonth11_9.Top = 4.076389F;
			this.txtCoveredMonth11_9.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_9
			// 
			this.txtCoveredMonth12_9.Height = 0.1770833F;
			this.txtCoveredMonth12_9.Left = 7.243055F;
			this.txtCoveredMonth12_9.Name = "txtCoveredMonth12_9";
			this.txtCoveredMonth12_9.Text = null;
			this.txtCoveredMonth12_9.Top = 4.076389F;
			this.txtCoveredMonth12_9.Width = 0.1875F;
			// 
			// txtCoveredAll12_10
			// 
			this.txtCoveredAll12_10.Height = 0.1770833F;
			this.txtCoveredAll12_10.Left = 3.609028F;
			this.txtCoveredAll12_10.Name = "txtCoveredAll12_10";
			this.txtCoveredAll12_10.Text = null;
			this.txtCoveredAll12_10.Top = 4.458333F;
			this.txtCoveredAll12_10.Width = 0.25F;
			// 
			// txtCoveredMonth1_10
			// 
			this.txtCoveredMonth1_10.Height = 0.1770833F;
			this.txtCoveredMonth1_10.Left = 3.996528F;
			this.txtCoveredMonth1_10.Name = "txtCoveredMonth1_10";
			this.txtCoveredMonth1_10.Text = null;
			this.txtCoveredMonth1_10.Top = 4.458333F;
			this.txtCoveredMonth1_10.Width = 0.25F;
			// 
			// txtCoveredMonth2_10
			// 
			this.txtCoveredMonth2_10.Height = 0.1770833F;
			this.txtCoveredMonth2_10.Left = 4.291667F;
			this.txtCoveredMonth2_10.Name = "txtCoveredMonth2_10";
			this.txtCoveredMonth2_10.Text = null;
			this.txtCoveredMonth2_10.Top = 4.458333F;
			this.txtCoveredMonth2_10.Width = 0.25F;
			// 
			// txtCoveredMonth3_10
			// 
			this.txtCoveredMonth3_10.Height = 0.1770833F;
			this.txtCoveredMonth3_10.Left = 4.586805F;
			this.txtCoveredMonth3_10.Name = "txtCoveredMonth3_10";
			this.txtCoveredMonth3_10.Text = null;
			this.txtCoveredMonth3_10.Top = 4.458333F;
			this.txtCoveredMonth3_10.Width = 0.25F;
			// 
			// txtCoveredMonth4_10
			// 
			this.txtCoveredMonth4_10.Height = 0.1770833F;
			this.txtCoveredMonth4_10.Left = 4.881945F;
			this.txtCoveredMonth4_10.Name = "txtCoveredMonth4_10";
			this.txtCoveredMonth4_10.Text = null;
			this.txtCoveredMonth4_10.Top = 4.458333F;
			this.txtCoveredMonth4_10.Width = 0.25F;
			// 
			// txtCoveredMonth5_10
			// 
			this.txtCoveredMonth5_10.Height = 0.1770833F;
			this.txtCoveredMonth5_10.Left = 5.177083F;
			this.txtCoveredMonth5_10.Name = "txtCoveredMonth5_10";
			this.txtCoveredMonth5_10.Text = null;
			this.txtCoveredMonth5_10.Top = 4.458333F;
			this.txtCoveredMonth5_10.Width = 0.25F;
			// 
			// txtCoveredMonth6_10
			// 
			this.txtCoveredMonth6_10.Height = 0.1770833F;
			this.txtCoveredMonth6_10.Left = 5.472222F;
			this.txtCoveredMonth6_10.Name = "txtCoveredMonth6_10";
			this.txtCoveredMonth6_10.Text = null;
			this.txtCoveredMonth6_10.Top = 4.458333F;
			this.txtCoveredMonth6_10.Width = 0.25F;
			// 
			// txtCoveredMonth7_10
			// 
			this.txtCoveredMonth7_10.Height = 0.1770833F;
			this.txtCoveredMonth7_10.Left = 5.767361F;
			this.txtCoveredMonth7_10.Name = "txtCoveredMonth7_10";
			this.txtCoveredMonth7_10.Text = null;
			this.txtCoveredMonth7_10.Top = 4.458333F;
			this.txtCoveredMonth7_10.Width = 0.25F;
			// 
			// txtCoveredMonth8_10
			// 
			this.txtCoveredMonth8_10.Height = 0.1770833F;
			this.txtCoveredMonth8_10.Left = 6.0625F;
			this.txtCoveredMonth8_10.Name = "txtCoveredMonth8_10";
			this.txtCoveredMonth8_10.Text = null;
			this.txtCoveredMonth8_10.Top = 4.458333F;
			this.txtCoveredMonth8_10.Width = 0.25F;
			// 
			// txtCoveredMonth9_10
			// 
			this.txtCoveredMonth9_10.Height = 0.1770833F;
			this.txtCoveredMonth9_10.Left = 6.357639F;
			this.txtCoveredMonth9_10.Name = "txtCoveredMonth9_10";
			this.txtCoveredMonth9_10.Text = null;
			this.txtCoveredMonth9_10.Top = 4.458333F;
			this.txtCoveredMonth9_10.Width = 0.25F;
			// 
			// txtCoveredMonth10_10
			// 
			this.txtCoveredMonth10_10.Height = 0.1770833F;
			this.txtCoveredMonth10_10.Left = 6.652778F;
			this.txtCoveredMonth10_10.Name = "txtCoveredMonth10_10";
			this.txtCoveredMonth10_10.Text = null;
			this.txtCoveredMonth10_10.Top = 4.458333F;
			this.txtCoveredMonth10_10.Width = 0.25F;
			// 
			// txtCoveredMonth11_10
			// 
			this.txtCoveredMonth11_10.Height = 0.1770833F;
			this.txtCoveredMonth11_10.Left = 6.947917F;
			this.txtCoveredMonth11_10.Name = "txtCoveredMonth11_10";
			this.txtCoveredMonth11_10.Text = null;
			this.txtCoveredMonth11_10.Top = 4.458333F;
			this.txtCoveredMonth11_10.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_10
			// 
			this.txtCoveredMonth12_10.Height = 0.1770833F;
			this.txtCoveredMonth12_10.Left = 7.243055F;
			this.txtCoveredMonth12_10.Name = "txtCoveredMonth12_10";
			this.txtCoveredMonth12_10.Text = null;
			this.txtCoveredMonth12_10.Top = 4.458333F;
			this.txtCoveredMonth12_10.Width = 0.1875F;
			// 
			// txtCoveredAll12_11
			// 
			this.txtCoveredAll12_11.Height = 0.1770833F;
			this.txtCoveredAll12_11.Left = 3.609028F;
			this.txtCoveredAll12_11.Name = "txtCoveredAll12_11";
			this.txtCoveredAll12_11.Text = null;
			this.txtCoveredAll12_11.Top = 4.833333F;
			this.txtCoveredAll12_11.Width = 0.25F;
			// 
			// txtCoveredMonth1_11
			// 
			this.txtCoveredMonth1_11.Height = 0.1770833F;
			this.txtCoveredMonth1_11.Left = 3.996528F;
			this.txtCoveredMonth1_11.Name = "txtCoveredMonth1_11";
			this.txtCoveredMonth1_11.Text = null;
			this.txtCoveredMonth1_11.Top = 4.833333F;
			this.txtCoveredMonth1_11.Width = 0.25F;
			// 
			// txtCoveredMonth2_11
			// 
			this.txtCoveredMonth2_11.Height = 0.1770833F;
			this.txtCoveredMonth2_11.Left = 4.291667F;
			this.txtCoveredMonth2_11.Name = "txtCoveredMonth2_11";
			this.txtCoveredMonth2_11.Text = null;
			this.txtCoveredMonth2_11.Top = 4.833333F;
			this.txtCoveredMonth2_11.Width = 0.25F;
			// 
			// txtCoveredMonth3_11
			// 
			this.txtCoveredMonth3_11.Height = 0.1770833F;
			this.txtCoveredMonth3_11.Left = 4.586805F;
			this.txtCoveredMonth3_11.Name = "txtCoveredMonth3_11";
			this.txtCoveredMonth3_11.Text = null;
			this.txtCoveredMonth3_11.Top = 4.833333F;
			this.txtCoveredMonth3_11.Width = 0.25F;
			// 
			// txtCoveredMonth4_11
			// 
			this.txtCoveredMonth4_11.Height = 0.1770833F;
			this.txtCoveredMonth4_11.Left = 4.881945F;
			this.txtCoveredMonth4_11.Name = "txtCoveredMonth4_11";
			this.txtCoveredMonth4_11.Text = null;
			this.txtCoveredMonth4_11.Top = 4.833333F;
			this.txtCoveredMonth4_11.Width = 0.25F;
			// 
			// txtCoveredMonth5_11
			// 
			this.txtCoveredMonth5_11.Height = 0.1770833F;
			this.txtCoveredMonth5_11.Left = 5.177083F;
			this.txtCoveredMonth5_11.Name = "txtCoveredMonth5_11";
			this.txtCoveredMonth5_11.Text = null;
			this.txtCoveredMonth5_11.Top = 4.833333F;
			this.txtCoveredMonth5_11.Width = 0.25F;
			// 
			// txtCoveredMonth6_11
			// 
			this.txtCoveredMonth6_11.Height = 0.1770833F;
			this.txtCoveredMonth6_11.Left = 5.472222F;
			this.txtCoveredMonth6_11.Name = "txtCoveredMonth6_11";
			this.txtCoveredMonth6_11.Text = null;
			this.txtCoveredMonth6_11.Top = 4.833333F;
			this.txtCoveredMonth6_11.Width = 0.25F;
			// 
			// txtCoveredMonth7_11
			// 
			this.txtCoveredMonth7_11.Height = 0.1770833F;
			this.txtCoveredMonth7_11.Left = 5.767361F;
			this.txtCoveredMonth7_11.Name = "txtCoveredMonth7_11";
			this.txtCoveredMonth7_11.Text = null;
			this.txtCoveredMonth7_11.Top = 4.833333F;
			this.txtCoveredMonth7_11.Width = 0.25F;
			// 
			// txtCoveredMonth8_11
			// 
			this.txtCoveredMonth8_11.Height = 0.1770833F;
			this.txtCoveredMonth8_11.Left = 6.0625F;
			this.txtCoveredMonth8_11.Name = "txtCoveredMonth8_11";
			this.txtCoveredMonth8_11.Text = null;
			this.txtCoveredMonth8_11.Top = 4.833333F;
			this.txtCoveredMonth8_11.Width = 0.25F;
			// 
			// txtCoveredMonth9_11
			// 
			this.txtCoveredMonth9_11.Height = 0.1770833F;
			this.txtCoveredMonth9_11.Left = 6.357639F;
			this.txtCoveredMonth9_11.Name = "txtCoveredMonth9_11";
			this.txtCoveredMonth9_11.Text = null;
			this.txtCoveredMonth9_11.Top = 4.833333F;
			this.txtCoveredMonth9_11.Width = 0.25F;
			// 
			// txtCoveredMonth10_11
			// 
			this.txtCoveredMonth10_11.Height = 0.1770833F;
			this.txtCoveredMonth10_11.Left = 6.652778F;
			this.txtCoveredMonth10_11.Name = "txtCoveredMonth10_11";
			this.txtCoveredMonth10_11.Text = null;
			this.txtCoveredMonth10_11.Top = 4.833333F;
			this.txtCoveredMonth10_11.Width = 0.25F;
			// 
			// txtCoveredMonth11_11
			// 
			this.txtCoveredMonth11_11.Height = 0.1770833F;
			this.txtCoveredMonth11_11.Left = 6.947917F;
			this.txtCoveredMonth11_11.Name = "txtCoveredMonth11_11";
			this.txtCoveredMonth11_11.Text = null;
			this.txtCoveredMonth11_11.Top = 4.833333F;
			this.txtCoveredMonth11_11.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_11
			// 
			this.txtCoveredMonth12_11.Height = 0.1770833F;
			this.txtCoveredMonth12_11.Left = 7.243055F;
			this.txtCoveredMonth12_11.Name = "txtCoveredMonth12_11";
			this.txtCoveredMonth12_11.Text = null;
			this.txtCoveredMonth12_11.Top = 4.833333F;
			this.txtCoveredMonth12_11.Width = 0.1875F;
			// 
			// txtCoveredAll12_12
			// 
			this.txtCoveredAll12_12.Height = 0.1770833F;
			this.txtCoveredAll12_12.Left = 3.609028F;
			this.txtCoveredAll12_12.Name = "txtCoveredAll12_12";
			this.txtCoveredAll12_12.Text = null;
			this.txtCoveredAll12_12.Top = 5.208333F;
			this.txtCoveredAll12_12.Width = 0.25F;
			// 
			// txtCoveredMonth1_12
			// 
			this.txtCoveredMonth1_12.Height = 0.1770833F;
			this.txtCoveredMonth1_12.Left = 3.996528F;
			this.txtCoveredMonth1_12.Name = "txtCoveredMonth1_12";
			this.txtCoveredMonth1_12.Text = null;
			this.txtCoveredMonth1_12.Top = 5.208333F;
			this.txtCoveredMonth1_12.Width = 0.25F;
			// 
			// txtCoveredMonth2_12
			// 
			this.txtCoveredMonth2_12.Height = 0.1770833F;
			this.txtCoveredMonth2_12.Left = 4.291667F;
			this.txtCoveredMonth2_12.Name = "txtCoveredMonth2_12";
			this.txtCoveredMonth2_12.Text = null;
			this.txtCoveredMonth2_12.Top = 5.208333F;
			this.txtCoveredMonth2_12.Width = 0.25F;
			// 
			// txtCoveredMonth3_12
			// 
			this.txtCoveredMonth3_12.Height = 0.1770833F;
			this.txtCoveredMonth3_12.Left = 4.586805F;
			this.txtCoveredMonth3_12.Name = "txtCoveredMonth3_12";
			this.txtCoveredMonth3_12.Text = null;
			this.txtCoveredMonth3_12.Top = 5.208333F;
			this.txtCoveredMonth3_12.Width = 0.25F;
			// 
			// txtCoveredMonth4_12
			// 
			this.txtCoveredMonth4_12.Height = 0.1770833F;
			this.txtCoveredMonth4_12.Left = 4.881945F;
			this.txtCoveredMonth4_12.Name = "txtCoveredMonth4_12";
			this.txtCoveredMonth4_12.Text = null;
			this.txtCoveredMonth4_12.Top = 5.208333F;
			this.txtCoveredMonth4_12.Width = 0.25F;
			// 
			// txtCoveredMonth5_12
			// 
			this.txtCoveredMonth5_12.Height = 0.1770833F;
			this.txtCoveredMonth5_12.Left = 5.177083F;
			this.txtCoveredMonth5_12.Name = "txtCoveredMonth5_12";
			this.txtCoveredMonth5_12.Text = null;
			this.txtCoveredMonth5_12.Top = 5.208333F;
			this.txtCoveredMonth5_12.Width = 0.25F;
			// 
			// txtCoveredMonth6_12
			// 
			this.txtCoveredMonth6_12.Height = 0.1770833F;
			this.txtCoveredMonth6_12.Left = 5.472222F;
			this.txtCoveredMonth6_12.Name = "txtCoveredMonth6_12";
			this.txtCoveredMonth6_12.Text = null;
			this.txtCoveredMonth6_12.Top = 5.208333F;
			this.txtCoveredMonth6_12.Width = 0.25F;
			// 
			// txtCoveredMonth7_12
			// 
			this.txtCoveredMonth7_12.Height = 0.1770833F;
			this.txtCoveredMonth7_12.Left = 5.767361F;
			this.txtCoveredMonth7_12.Name = "txtCoveredMonth7_12";
			this.txtCoveredMonth7_12.Text = null;
			this.txtCoveredMonth7_12.Top = 5.208333F;
			this.txtCoveredMonth7_12.Width = 0.25F;
			// 
			// txtCoveredMonth8_12
			// 
			this.txtCoveredMonth8_12.Height = 0.1770833F;
			this.txtCoveredMonth8_12.Left = 6.0625F;
			this.txtCoveredMonth8_12.Name = "txtCoveredMonth8_12";
			this.txtCoveredMonth8_12.Text = null;
			this.txtCoveredMonth8_12.Top = 5.208333F;
			this.txtCoveredMonth8_12.Width = 0.25F;
			// 
			// txtCoveredMonth9_12
			// 
			this.txtCoveredMonth9_12.Height = 0.1770833F;
			this.txtCoveredMonth9_12.Left = 6.357639F;
			this.txtCoveredMonth9_12.Name = "txtCoveredMonth9_12";
			this.txtCoveredMonth9_12.Text = null;
			this.txtCoveredMonth9_12.Top = 5.208333F;
			this.txtCoveredMonth9_12.Width = 0.25F;
			// 
			// txtCoveredMonth10_12
			// 
			this.txtCoveredMonth10_12.Height = 0.1770833F;
			this.txtCoveredMonth10_12.Left = 6.652778F;
			this.txtCoveredMonth10_12.Name = "txtCoveredMonth10_12";
			this.txtCoveredMonth10_12.Text = null;
			this.txtCoveredMonth10_12.Top = 5.208333F;
			this.txtCoveredMonth10_12.Width = 0.25F;
			// 
			// txtCoveredMonth11_12
			// 
			this.txtCoveredMonth11_12.Height = 0.1770833F;
			this.txtCoveredMonth11_12.Left = 6.947917F;
			this.txtCoveredMonth11_12.Name = "txtCoveredMonth11_12";
			this.txtCoveredMonth11_12.Text = null;
			this.txtCoveredMonth11_12.Top = 5.208333F;
			this.txtCoveredMonth11_12.Width = 0.1666667F;
			// 
			// txtCoveredMonth12_12
			// 
			this.txtCoveredMonth12_12.Height = 0.1770833F;
			this.txtCoveredMonth12_12.Left = 7.243055F;
			this.txtCoveredMonth12_12.Name = "txtCoveredMonth12_12";
			this.txtCoveredMonth12_12.Text = null;
			this.txtCoveredMonth12_12.Top = 5.208333F;
			this.txtCoveredMonth12_12.Width = 0.1875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.09375F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.572917F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 5.5pt; text-align: center";
			this.Label14.Text = "Oct";
			this.Label14.Top = 0.8958333F;
			this.Label14.Width = 0.2951389F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.09375F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.277778F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 5.5pt; text-align: center";
			this.Label13.Text = "Sep";
			this.Label13.Top = 0.8958333F;
			this.Label13.Width = 0.2951389F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.09375F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5.097222F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 5.5pt; text-align: center";
			this.Label9.Text = "May";
			this.Label9.Top = 0.8958333F;
			this.Label9.Width = 0.2951389F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.958333F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 5.5pt; text-align: center; vertical-align: middle";
			this.Label4.Text = "(e) Months of Coverage";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 3.458333F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 0F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1F;
			this.Line2.Width = 7.46875F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.46875F;
			this.Line2.Y1 = 1F;
			this.Line2.Y2 = 1F;
			// 
			// Line9
			// 
			this.Line9.Height = 4.78125F;
			this.Line9.Left = 3.4375F;
			this.Line9.LineWeight = 0F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 0.65625F;
			this.Line9.Width = 0F;
			this.Line9.X1 = 3.4375F;
			this.Line9.X2 = 3.4375F;
			this.Line9.Y1 = 0.65625F;
			this.Line9.Y2 = 5.4375F;
			// 
			// Line14
			// 
			this.Line14.Height = 4.583333F;
			this.Line14.Left = 5.097222F;
			this.Line14.LineWeight = 0F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0.8541667F;
			this.Line14.Width = 0F;
			this.Line14.X1 = 5.097222F;
			this.Line14.X2 = 5.097222F;
			this.Line14.Y1 = 0.8541667F;
			this.Line14.Y2 = 5.4375F;
			// 
			// Line18
			// 
			this.Line18.Height = 4.583333F;
			this.Line18.Left = 6.277778F;
			this.Line18.LineWeight = 0F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 0.8541667F;
			this.Line18.Width = 0F;
			this.Line18.X1 = 6.277778F;
			this.Line18.X2 = 6.277778F;
			this.Line18.Y1 = 0.8541667F;
			this.Line18.Y2 = 5.4375F;
			// 
			// Line19
			// 
			this.Line19.Height = 4.583333F;
			this.Line19.Left = 6.572917F;
			this.Line19.LineWeight = 0F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 0.8541667F;
			this.Line19.Width = 0F;
			this.Line19.X1 = 6.572917F;
			this.Line19.X2 = 6.572917F;
			this.Line19.Y1 = 0.8541667F;
			this.Line19.Y2 = 5.4375F;
			// 
			// Line22
			// 
			this.Line22.Height = 4.78125F;
			this.Line22.Left = 2.6875F;
			this.Line22.LineWeight = 0F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 0.65625F;
			this.Line22.Width = 0F;
			this.Line22.X1 = 2.6875F;
			this.Line22.X2 = 2.6875F;
			this.Line22.Y1 = 0.65625F;
			this.Line22.Y2 = 5.4375F;
			// 
			// Line23
			// 
			this.Line23.Height = 4.78125F;
			this.Line23.Left = 1.75F;
			this.Line23.LineWeight = 0F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 0.65625F;
			this.Line23.Width = 0F;
			this.Line23.X1 = 1.75F;
			this.Line23.X2 = 1.75F;
			this.Line23.Y1 = 0.65625F;
			this.Line23.Y2 = 5.4375F;
			// 
			// lbl12Months
			// 
			this.lbl12Months.Height = 0.28125F;
			this.lbl12Months.HyperLink = null;
			this.lbl12Months.Left = 3.451389F;
			this.lbl12Months.Name = "lbl12Months";
			this.lbl12Months.Style = "font-size: 5.5pt; text-align: center";
			this.lbl12Months.Text = "(d) Covered all 12 months";
			this.lbl12Months.Top = 0.6979167F;
			this.lbl12Months.Width = 0.4583333F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.28125F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 5.5pt; text-align: center";
			this.Label1.Text = "(c) DOB (If SSN or other TIN is not available)";
			this.Label1.Top = 0.71875F;
			this.Label1.Width = 0.6458333F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.8125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 5.5pt; text-align: center";
			this.Label2.Text = "(b) SSN or other TIN";
			this.Label2.Top = 0.75F;
			this.Label2.Width = 0.8333333F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.21875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 5.5pt; text-align: center";
			this.Label3.Text = "(a) Name of covered individual(s)";
			this.Label3.Top = 0.75F;
			this.Label3.Width = 1.708333F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.09375F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.916667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 5.5pt; text-align: center";
			this.Label5.Text = "Jan";
			this.Label5.Top = 0.8958333F;
			this.Label5.Width = 0.2951389F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.09375F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.211805F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 5.5pt; text-align: center";
			this.Label6.Text = "Feb";
			this.Label6.Top = 0.8958333F;
			this.Label6.Width = 0.2951389F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.09375F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.506945F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 5.5pt; text-align: center";
			this.Label7.Text = "Mar";
			this.Label7.Top = 0.8958333F;
			this.Label7.Width = 0.2951389F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.09375F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.802083F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 5.5pt; text-align: center";
			this.Label8.Text = "Apr";
			this.Label8.Top = 0.8958333F;
			this.Label8.Width = 0.2951389F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.09375F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.392361F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 5.5pt; text-align: center";
			this.Label10.Text = "Jun";
			this.Label10.Top = 0.8958333F;
			this.Label10.Width = 0.2951389F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.09375F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.6875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 5.5pt; text-align: center";
			this.Label11.Text = "July";
			this.Label11.Top = 0.8958333F;
			this.Label11.Width = 0.2951389F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.09375F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.982639F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 5.5pt; text-align: center";
			this.Label12.Text = "Aug";
			this.Label12.Top = 0.8958333F;
			this.Label12.Width = 0.2951389F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.09375F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 6.868055F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 5.5pt; text-align: center";
			this.Label15.Text = "Nov";
			this.Label15.Top = 0.8958333F;
			this.Label15.Width = 0.2951389F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.09375F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 7.166667F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 5.5pt; text-align: center";
			this.Label16.Text = "Dec";
			this.Label16.Top = 0.8958333F;
			this.Label16.Width = 0.2951389F;
			// 
			// Shape77
			// 
			this.Shape77.Height = 0.1458333F;
			this.Shape77.Left = 3.609028F;
			this.Shape77.LineWeight = 0F;
			this.Shape77.Name = "Shape77";
			this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape77.Top = 1.083333F;
			this.Shape77.Width = 0.1354167F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1145833F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.02083333F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label26.Text = "29";
			this.Label26.Top = 1.104167F;
			this.Label26.Width = 0.2083333F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1145833F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.02083333F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label27.Text = "30";
			this.Label27.Top = 1.461806F;
			this.Label27.Width = 0.2083333F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1145833F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.02083333F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label28.Text = "31";
			this.Label28.Top = 1.854167F;
			this.Label28.Width = 0.2083333F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1145833F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.02083333F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label29.Text = "32";
			this.Label29.Top = 2.229167F;
			this.Label29.Width = 0.2083333F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1145833F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.02083333F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label30.Text = "33";
			this.Label30.Top = 2.604167F;
			this.Label30.Width = 0.2083333F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1145833F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.02083333F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label31.Text = "34";
			this.Label31.Top = 2.979167F;
			this.Label31.Width = 0.2083333F;
			// 
			// Line20
			// 
			this.Line20.Height = 4.583333F;
			this.Line20.Left = 6.868055F;
			this.Line20.LineWeight = 0F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 0.8541667F;
			this.Line20.Width = 0F;
			this.Line20.X1 = 6.868055F;
			this.Line20.X2 = 6.868055F;
			this.Line20.Y1 = 0.8541667F;
			this.Line20.Y2 = 5.4375F;
			// 
			// Line21
			// 
			this.Line21.Height = 4.583333F;
			this.Line21.Left = 7.163195F;
			this.Line21.LineWeight = 0F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 0.8541667F;
			this.Line21.Width = 0F;
			this.Line21.X1 = 7.163195F;
			this.Line21.X2 = 7.163195F;
			this.Line21.Y1 = 0.8541667F;
			this.Line21.Y2 = 5.4375F;
			// 
			// Line17
			// 
			this.Line17.Height = 4.583333F;
			this.Line17.Left = 5.982639F;
			this.Line17.LineWeight = 0F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 0.8541667F;
			this.Line17.Width = 0F;
			this.Line17.X1 = 5.982639F;
			this.Line17.X2 = 5.982639F;
			this.Line17.Y1 = 0.8541667F;
			this.Line17.Y2 = 5.4375F;
			// 
			// Line16
			// 
			this.Line16.Height = 4.583333F;
			this.Line16.Left = 5.6875F;
			this.Line16.LineWeight = 0F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 0.8541667F;
			this.Line16.Width = 0F;
			this.Line16.X1 = 5.6875F;
			this.Line16.X2 = 5.6875F;
			this.Line16.Y1 = 0.8541667F;
			this.Line16.Y2 = 5.4375F;
			// 
			// Line15
			// 
			this.Line15.Height = 4.583333F;
			this.Line15.Left = 5.392361F;
			this.Line15.LineWeight = 0F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 0.8541667F;
			this.Line15.Width = 0F;
			this.Line15.X1 = 5.392361F;
			this.Line15.X2 = 5.392361F;
			this.Line15.Y1 = 0.8541667F;
			this.Line15.Y2 = 5.4375F;
			// 
			// Line13
			// 
			this.Line13.Height = 4.583333F;
			this.Line13.Left = 4.802083F;
			this.Line13.LineWeight = 0F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0.8541667F;
			this.Line13.Width = 0F;
			this.Line13.X1 = 4.802083F;
			this.Line13.X2 = 4.802083F;
			this.Line13.Y1 = 0.8541667F;
			this.Line13.Y2 = 5.4375F;
			// 
			// Line12
			// 
			this.Line12.Height = 4.583333F;
			this.Line12.Left = 4.506945F;
			this.Line12.LineWeight = 0F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0.8541667F;
			this.Line12.Width = 0F;
			this.Line12.X1 = 4.506945F;
			this.Line12.X2 = 4.506945F;
			this.Line12.Y1 = 0.8541667F;
			this.Line12.Y2 = 5.4375F;
			// 
			// Line11
			// 
			this.Line11.Height = 4.583333F;
			this.Line11.Left = 4.211805F;
			this.Line11.LineWeight = 0F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 0.8541667F;
			this.Line11.Width = 0F;
			this.Line11.X1 = 4.211805F;
			this.Line11.X2 = 4.211805F;
			this.Line11.Y1 = 0.8541667F;
			this.Line11.Y2 = 5.4375F;
			// 
			// Line10
			// 
			this.Line10.Height = 4.78125F;
			this.Line10.Left = 3.916667F;
			this.Line10.LineWeight = 0F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 0.65625F;
			this.Line10.Width = 0F;
			this.Line10.X1 = 3.916667F;
			this.Line10.X2 = 3.916667F;
			this.Line10.Y1 = 0.65625F;
			this.Line10.Y2 = 5.4375F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 3.916667F;
			this.Line24.LineWeight = 0F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 0.8541667F;
			this.Line24.Width = 3.552083F;
			this.Line24.X1 = 3.916667F;
			this.Line24.X2 = 7.46875F;
			this.Line24.Y1 = 0.8541667F;
			this.Line24.Y2 = 0.8541667F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 0F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.3125F;
			this.Line3.Width = 7.46875F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.46875F;
			this.Line3.Y1 = 1.3125F;
			this.Line3.Y2 = 1.3125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 0F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.6875F;
			this.Line4.Width = 7.46875F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.46875F;
			this.Line4.Y1 = 1.6875F;
			this.Line4.Y2 = 1.6875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 0F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.0625F;
			this.Line5.Width = 7.46875F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.46875F;
			this.Line5.Y1 = 2.0625F;
			this.Line5.Y2 = 2.0625F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0F;
			this.Line6.LineWeight = 0F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 2.4375F;
			this.Line6.Width = 7.46875F;
			this.Line6.X1 = 0F;
			this.Line6.X2 = 7.46875F;
			this.Line6.Y1 = 2.4375F;
			this.Line6.Y2 = 2.4375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 0F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 2.8125F;
			this.Line7.Width = 7.447917F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 7.447917F;
			this.Line7.Y1 = 2.8125F;
			this.Line7.Y2 = 2.8125F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0F;
			this.Line8.LineWeight = 0F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 3.1875F;
			this.Line8.Width = 7.46875F;
			this.Line8.X1 = 0F;
			this.Line8.X2 = 7.46875F;
			this.Line8.Y1 = 3.1875F;
			this.Line8.Y2 = 3.1875F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 0F;
			this.Line25.LineWeight = 0F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 3.5625F;
			this.Line25.Width = 7.46875F;
			this.Line25.X1 = 0F;
			this.Line25.X2 = 7.46875F;
			this.Line25.Y1 = 3.5625F;
			this.Line25.Y2 = 3.5625F;
			// 
			// Line26
			// 
			this.Line26.Height = 0F;
			this.Line26.Left = 0F;
			this.Line26.LineWeight = 0F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 3.9375F;
			this.Line26.Width = 7.46875F;
			this.Line26.X1 = 0F;
			this.Line26.X2 = 7.46875F;
			this.Line26.Y1 = 3.9375F;
			this.Line26.Y2 = 3.9375F;
			// 
			// Line27
			// 
			this.Line27.Height = 0F;
			this.Line27.Left = 0F;
			this.Line27.LineWeight = 0F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 4.3125F;
			this.Line27.Width = 7.46875F;
			this.Line27.X1 = 0F;
			this.Line27.X2 = 7.46875F;
			this.Line27.Y1 = 4.3125F;
			this.Line27.Y2 = 4.3125F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 0F;
			this.Line28.LineWeight = 0F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 4.6875F;
			this.Line28.Width = 7.46875F;
			this.Line28.X1 = 0F;
			this.Line28.X2 = 7.46875F;
			this.Line28.Y1 = 4.6875F;
			this.Line28.Y2 = 4.6875F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 0F;
			this.Line29.LineWeight = 0F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 5.0625F;
			this.Line29.Width = 7.46875F;
			this.Line29.X1 = 0F;
			this.Line29.X2 = 7.46875F;
			this.Line29.Y1 = 5.0625F;
			this.Line29.Y2 = 5.0625F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 0F;
			this.Line30.LineWeight = 0F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 5.4375F;
			this.Line30.Width = 7.46875F;
			this.Line30.X1 = 0F;
			this.Line30.X2 = 7.46875F;
			this.Line30.Y1 = 5.4375F;
			this.Line30.Y2 = 5.4375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.09375F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.04166667F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label17.Text = "XID #1607";
			this.Label17.Top = 5.489583F;
			this.Label17.Width = 0.8958333F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.09375F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 4.708333F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 6pt; text-align: left";
			this.Label18.Text = "41-0852411";
			this.Label18.Top = 5.489583F;
			this.Label18.Width = 0.6458333F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.09375F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.895833F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 6pt; text-align: left";
			this.Label19.Text = "1095BC";
			this.Label19.Top = 5.489583F;
			this.Label19.Width = 0.3958333F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.09375F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 6.447917F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 6pt; text-align: left";
			this.Label20.Text = "Form";
			this.Label20.Top = 5.489583F;
			this.Label20.Width = 0.2708333F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.125F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 6.677083F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 7pt; font-weight: bold; text-align: left; vertical-align: bottom";
			this.Label21.Text = "1095-B";
			this.Label21.Top = 5.458333F;
			this.Label21.Width = 0.4583333F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.09375F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 7.013889F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 6pt; text-align: left";
			this.Label22.Text = "(2016)";
			this.Label22.Top = 5.489583F;
			this.Label22.Width = 0.3333333F;
			// 
			// Shape78
			// 
			this.Shape78.Height = 0.1458333F;
			this.Shape78.Left = 3.604167F;
			this.Shape78.LineWeight = 0F;
			this.Shape78.Name = "Shape78";
			this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape78.Top = 1.458333F;
			this.Shape78.Width = 0.1354167F;
			// 
			// Shape80
			// 
			this.Shape80.Height = 0.1458333F;
			this.Shape80.Left = 3.604167F;
			this.Shape80.LineWeight = 0F;
			this.Shape80.Name = "Shape80";
			this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape80.Top = 1.833333F;
			this.Shape80.Width = 0.1354167F;
			// 
			// Shape81
			// 
			this.Shape81.Height = 0.1458333F;
			this.Shape81.Left = 3.604167F;
			this.Shape81.LineWeight = 0F;
			this.Shape81.Name = "Shape81";
			this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape81.Top = 2.208333F;
			this.Shape81.Width = 0.1354167F;
			// 
			// Shape82
			// 
			this.Shape82.Height = 0.1458333F;
			this.Shape82.Left = 3.604167F;
			this.Shape82.LineWeight = 0F;
			this.Shape82.Name = "Shape82";
			this.Shape82.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape82.Top = 2.583333F;
			this.Shape82.Width = 0.1354167F;
			// 
			// Shape83
			// 
			this.Shape83.Height = 0.1458333F;
			this.Shape83.Left = 3.604167F;
			this.Shape83.LineWeight = 0F;
			this.Shape83.Name = "Shape83";
			this.Shape83.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape83.Top = 2.958333F;
			this.Shape83.Width = 0.1354167F;
			// 
			// Shape84
			// 
			this.Shape84.Height = 0.1458333F;
			this.Shape84.Left = 3.604167F;
			this.Shape84.LineWeight = 0F;
			this.Shape84.Name = "Shape84";
			this.Shape84.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape84.Top = 3.333333F;
			this.Shape84.Width = 0.1354167F;
			// 
			// Shape85
			// 
			this.Shape85.Height = 0.1458333F;
			this.Shape85.Left = 3.604167F;
			this.Shape85.LineWeight = 0F;
			this.Shape85.Name = "Shape85";
			this.Shape85.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape85.Top = 3.708333F;
			this.Shape85.Width = 0.1354167F;
			// 
			// Shape86
			// 
			this.Shape86.Height = 0.1458333F;
			this.Shape86.Left = 3.604167F;
			this.Shape86.LineWeight = 0F;
			this.Shape86.Name = "Shape86";
			this.Shape86.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape86.Top = 4.083333F;
			this.Shape86.Width = 0.1354167F;
			// 
			// Shape87
			// 
			this.Shape87.Height = 0.1458333F;
			this.Shape87.Left = 3.604167F;
			this.Shape87.LineWeight = 0F;
			this.Shape87.Name = "Shape87";
			this.Shape87.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape87.Top = 4.465278F;
			this.Shape87.Width = 0.1354167F;
			// 
			// Shape88
			// 
			this.Shape88.Height = 0.1458333F;
			this.Shape88.Left = 3.604167F;
			this.Shape88.LineWeight = 0F;
			this.Shape88.Name = "Shape88";
			this.Shape88.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape88.Top = 4.840278F;
			this.Shape88.Width = 0.1354167F;
			// 
			// Shape89
			// 
			this.Shape89.Height = 0.1458333F;
			this.Shape89.Left = 3.604167F;
			this.Shape89.LineWeight = 0F;
			this.Shape89.Name = "Shape89";
			this.Shape89.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape89.Top = 5.215278F;
			this.Shape89.Width = 0.1354167F;
			// 
			// Shape90
			// 
			this.Shape90.Height = 0.1458333F;
			this.Shape90.Left = 4F;
			this.Shape90.LineWeight = 0F;
			this.Shape90.Name = "Shape90";
			this.Shape90.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape90.Top = 1.083333F;
			this.Shape90.Width = 0.1354167F;
			// 
			// Shape91
			// 
			this.Shape91.Height = 0.1458333F;
			this.Shape91.Left = 4F;
			this.Shape91.LineWeight = 0F;
			this.Shape91.Name = "Shape91";
			this.Shape91.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape91.Top = 1.458333F;
			this.Shape91.Width = 0.1354167F;
			// 
			// Shape92
			// 
			this.Shape92.Height = 0.1458333F;
			this.Shape92.Left = 4F;
			this.Shape92.LineWeight = 0F;
			this.Shape92.Name = "Shape92";
			this.Shape92.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape92.Top = 1.833333F;
			this.Shape92.Width = 0.1354167F;
			// 
			// Shape93
			// 
			this.Shape93.Height = 0.1458333F;
			this.Shape93.Left = 4F;
			this.Shape93.LineWeight = 0F;
			this.Shape93.Name = "Shape93";
			this.Shape93.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape93.Top = 2.208333F;
			this.Shape93.Width = 0.1354167F;
			// 
			// Shape94
			// 
			this.Shape94.Height = 0.1458333F;
			this.Shape94.Left = 4F;
			this.Shape94.LineWeight = 0F;
			this.Shape94.Name = "Shape94";
			this.Shape94.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape94.Top = 2.583333F;
			this.Shape94.Width = 0.1354167F;
			// 
			// Shape95
			// 
			this.Shape95.Height = 0.1458333F;
			this.Shape95.Left = 4F;
			this.Shape95.LineWeight = 0F;
			this.Shape95.Name = "Shape95";
			this.Shape95.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape95.Top = 2.958333F;
			this.Shape95.Width = 0.1354167F;
			// 
			// Shape96
			// 
			this.Shape96.Height = 0.1458333F;
			this.Shape96.Left = 4F;
			this.Shape96.LineWeight = 0F;
			this.Shape96.Name = "Shape96";
			this.Shape96.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape96.Top = 3.333333F;
			this.Shape96.Width = 0.1354167F;
			// 
			// Shape97
			// 
			this.Shape97.Height = 0.1458333F;
			this.Shape97.Left = 4F;
			this.Shape97.LineWeight = 0F;
			this.Shape97.Name = "Shape97";
			this.Shape97.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape97.Top = 3.708333F;
			this.Shape97.Width = 0.1354167F;
			// 
			// Shape98
			// 
			this.Shape98.Height = 0.1458333F;
			this.Shape98.Left = 4F;
			this.Shape98.LineWeight = 0F;
			this.Shape98.Name = "Shape98";
			this.Shape98.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape98.Top = 4.083333F;
			this.Shape98.Width = 0.1354167F;
			// 
			// Shape99
			// 
			this.Shape99.Height = 0.1458333F;
			this.Shape99.Left = 4F;
			this.Shape99.LineWeight = 0F;
			this.Shape99.Name = "Shape99";
			this.Shape99.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape99.Top = 4.46875F;
			this.Shape99.Width = 0.1354167F;
			// 
			// Shape100
			// 
			this.Shape100.Height = 0.1458333F;
			this.Shape100.Left = 4F;
			this.Shape100.LineWeight = 0F;
			this.Shape100.Name = "Shape100";
			this.Shape100.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape100.Top = 4.84375F;
			this.Shape100.Width = 0.1354167F;
			// 
			// Shape101
			// 
			this.Shape101.Height = 0.1458333F;
			this.Shape101.Left = 4F;
			this.Shape101.LineWeight = 0F;
			this.Shape101.Name = "Shape101";
			this.Shape101.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape101.Top = 5.21875F;
			this.Shape101.Width = 0.1354167F;
			// 
			// Shape102
			// 
			this.Shape102.Height = 0.1458333F;
			this.Shape102.Left = 4.291667F;
			this.Shape102.LineWeight = 0F;
			this.Shape102.Name = "Shape102";
			this.Shape102.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape102.Top = 1.083333F;
			this.Shape102.Width = 0.1354167F;
			// 
			// Shape103
			// 
			this.Shape103.Height = 0.1458333F;
			this.Shape103.Left = 4.291667F;
			this.Shape103.LineWeight = 0F;
			this.Shape103.Name = "Shape103";
			this.Shape103.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape103.Top = 1.458333F;
			this.Shape103.Width = 0.1354167F;
			// 
			// Shape104
			// 
			this.Shape104.Height = 0.1458333F;
			this.Shape104.Left = 4.291667F;
			this.Shape104.LineWeight = 0F;
			this.Shape104.Name = "Shape104";
			this.Shape104.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape104.Top = 1.833333F;
			this.Shape104.Width = 0.1354167F;
			// 
			// Shape105
			// 
			this.Shape105.Height = 0.1458333F;
			this.Shape105.Left = 4.291667F;
			this.Shape105.LineWeight = 0F;
			this.Shape105.Name = "Shape105";
			this.Shape105.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape105.Top = 2.208333F;
			this.Shape105.Width = 0.1354167F;
			// 
			// Shape106
			// 
			this.Shape106.Height = 0.1458333F;
			this.Shape106.Left = 4.291667F;
			this.Shape106.LineWeight = 0F;
			this.Shape106.Name = "Shape106";
			this.Shape106.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape106.Top = 2.583333F;
			this.Shape106.Width = 0.1354167F;
			// 
			// Shape107
			// 
			this.Shape107.Height = 0.1458333F;
			this.Shape107.Left = 4.291667F;
			this.Shape107.LineWeight = 0F;
			this.Shape107.Name = "Shape107";
			this.Shape107.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape107.Top = 2.958333F;
			this.Shape107.Width = 0.1354167F;
			// 
			// Shape108
			// 
			this.Shape108.Height = 0.1458333F;
			this.Shape108.Left = 4.291667F;
			this.Shape108.LineWeight = 0F;
			this.Shape108.Name = "Shape108";
			this.Shape108.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape108.Top = 3.333333F;
			this.Shape108.Width = 0.1354167F;
			// 
			// Shape109
			// 
			this.Shape109.Height = 0.1458333F;
			this.Shape109.Left = 4.291667F;
			this.Shape109.LineWeight = 0F;
			this.Shape109.Name = "Shape109";
			this.Shape109.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape109.Top = 3.708333F;
			this.Shape109.Width = 0.1354167F;
			// 
			// Shape110
			// 
			this.Shape110.Height = 0.1458333F;
			this.Shape110.Left = 4.291667F;
			this.Shape110.LineWeight = 0F;
			this.Shape110.Name = "Shape110";
			this.Shape110.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape110.Top = 4.083333F;
			this.Shape110.Width = 0.1354167F;
			// 
			// Shape111
			// 
			this.Shape111.Height = 0.1458333F;
			this.Shape111.Left = 4.291667F;
			this.Shape111.LineWeight = 0F;
			this.Shape111.Name = "Shape111";
			this.Shape111.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape111.Top = 4.46875F;
			this.Shape111.Width = 0.1354167F;
			// 
			// Shape112
			// 
			this.Shape112.Height = 0.1458333F;
			this.Shape112.Left = 4.291667F;
			this.Shape112.LineWeight = 0F;
			this.Shape112.Name = "Shape112";
			this.Shape112.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape112.Top = 4.84375F;
			this.Shape112.Width = 0.1354167F;
			// 
			// Shape113
			// 
			this.Shape113.Height = 0.1458333F;
			this.Shape113.Left = 4.291667F;
			this.Shape113.LineWeight = 0F;
			this.Shape113.Name = "Shape113";
			this.Shape113.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape113.Top = 5.21875F;
			this.Shape113.Width = 0.1354167F;
			// 
			// Shape114
			// 
			this.Shape114.Height = 0.1458333F;
			this.Shape114.Left = 4.583333F;
			this.Shape114.LineWeight = 0F;
			this.Shape114.Name = "Shape114";
			this.Shape114.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape114.Top = 1.083333F;
			this.Shape114.Width = 0.1354167F;
			// 
			// Shape115
			// 
			this.Shape115.Height = 0.1458333F;
			this.Shape115.Left = 4.583333F;
			this.Shape115.LineWeight = 0F;
			this.Shape115.Name = "Shape115";
			this.Shape115.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape115.Top = 1.458333F;
			this.Shape115.Width = 0.1354167F;
			// 
			// Shape116
			// 
			this.Shape116.Height = 0.1458333F;
			this.Shape116.Left = 4.583333F;
			this.Shape116.LineWeight = 0F;
			this.Shape116.Name = "Shape116";
			this.Shape116.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape116.Top = 1.833333F;
			this.Shape116.Width = 0.1354167F;
			// 
			// Shape117
			// 
			this.Shape117.Height = 0.1458333F;
			this.Shape117.Left = 4.583333F;
			this.Shape117.LineWeight = 0F;
			this.Shape117.Name = "Shape117";
			this.Shape117.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape117.Top = 2.208333F;
			this.Shape117.Width = 0.1354167F;
			// 
			// Shape118
			// 
			this.Shape118.Height = 0.1458333F;
			this.Shape118.Left = 4.583333F;
			this.Shape118.LineWeight = 0F;
			this.Shape118.Name = "Shape118";
			this.Shape118.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape118.Top = 2.583333F;
			this.Shape118.Width = 0.1354167F;
			// 
			// Shape119
			// 
			this.Shape119.Height = 0.1458333F;
			this.Shape119.Left = 4.583333F;
			this.Shape119.LineWeight = 0F;
			this.Shape119.Name = "Shape119";
			this.Shape119.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape119.Top = 2.958333F;
			this.Shape119.Width = 0.1354167F;
			// 
			// Shape120
			// 
			this.Shape120.Height = 0.1458333F;
			this.Shape120.Left = 4.583333F;
			this.Shape120.LineWeight = 0F;
			this.Shape120.Name = "Shape120";
			this.Shape120.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape120.Top = 3.333333F;
			this.Shape120.Width = 0.1354167F;
			// 
			// Shape121
			// 
			this.Shape121.Height = 0.1458333F;
			this.Shape121.Left = 4.583333F;
			this.Shape121.LineWeight = 0F;
			this.Shape121.Name = "Shape121";
			this.Shape121.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape121.Top = 3.708333F;
			this.Shape121.Width = 0.1354167F;
			// 
			// Shape122
			// 
			this.Shape122.Height = 0.1458333F;
			this.Shape122.Left = 4.583333F;
			this.Shape122.LineWeight = 0F;
			this.Shape122.Name = "Shape122";
			this.Shape122.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape122.Top = 4.083333F;
			this.Shape122.Width = 0.1354167F;
			// 
			// Shape123
			// 
			this.Shape123.Height = 0.1458333F;
			this.Shape123.Left = 4.583333F;
			this.Shape123.LineWeight = 0F;
			this.Shape123.Name = "Shape123";
			this.Shape123.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape123.Top = 4.46875F;
			this.Shape123.Width = 0.1354167F;
			// 
			// Shape124
			// 
			this.Shape124.Height = 0.1458333F;
			this.Shape124.Left = 4.583333F;
			this.Shape124.LineWeight = 0F;
			this.Shape124.Name = "Shape124";
			this.Shape124.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape124.Top = 4.84375F;
			this.Shape124.Width = 0.1354167F;
			// 
			// Shape125
			// 
			this.Shape125.Height = 0.1458333F;
			this.Shape125.Left = 4.583333F;
			this.Shape125.LineWeight = 0F;
			this.Shape125.Name = "Shape125";
			this.Shape125.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape125.Top = 5.21875F;
			this.Shape125.Width = 0.1354167F;
			// 
			// Shape126
			// 
			this.Shape126.Height = 0.1458333F;
			this.Shape126.Left = 4.885417F;
			this.Shape126.LineWeight = 0F;
			this.Shape126.Name = "Shape126";
			this.Shape126.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape126.Top = 1.083333F;
			this.Shape126.Width = 0.1354167F;
			// 
			// Shape127
			// 
			this.Shape127.Height = 0.1458333F;
			this.Shape127.Left = 4.885417F;
			this.Shape127.LineWeight = 0F;
			this.Shape127.Name = "Shape127";
			this.Shape127.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape127.Top = 1.458333F;
			this.Shape127.Width = 0.1354167F;
			// 
			// Shape128
			// 
			this.Shape128.Height = 0.1458333F;
			this.Shape128.Left = 4.885417F;
			this.Shape128.LineWeight = 0F;
			this.Shape128.Name = "Shape128";
			this.Shape128.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape128.Top = 1.833333F;
			this.Shape128.Width = 0.1354167F;
			// 
			// Shape129
			// 
			this.Shape129.Height = 0.1458333F;
			this.Shape129.Left = 4.885417F;
			this.Shape129.LineWeight = 0F;
			this.Shape129.Name = "Shape129";
			this.Shape129.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape129.Top = 2.208333F;
			this.Shape129.Width = 0.1354167F;
			// 
			// Shape130
			// 
			this.Shape130.Height = 0.1458333F;
			this.Shape130.Left = 4.885417F;
			this.Shape130.LineWeight = 0F;
			this.Shape130.Name = "Shape130";
			this.Shape130.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape130.Top = 2.583333F;
			this.Shape130.Width = 0.1354167F;
			// 
			// Shape131
			// 
			this.Shape131.Height = 0.1458333F;
			this.Shape131.Left = 4.885417F;
			this.Shape131.LineWeight = 0F;
			this.Shape131.Name = "Shape131";
			this.Shape131.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape131.Top = 2.958333F;
			this.Shape131.Width = 0.1354167F;
			// 
			// Shape132
			// 
			this.Shape132.Height = 0.1458333F;
			this.Shape132.Left = 4.885417F;
			this.Shape132.LineWeight = 0F;
			this.Shape132.Name = "Shape132";
			this.Shape132.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape132.Top = 3.333333F;
			this.Shape132.Width = 0.1354167F;
			// 
			// Shape133
			// 
			this.Shape133.Height = 0.1458333F;
			this.Shape133.Left = 4.885417F;
			this.Shape133.LineWeight = 0F;
			this.Shape133.Name = "Shape133";
			this.Shape133.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape133.Top = 3.708333F;
			this.Shape133.Width = 0.1354167F;
			// 
			// Shape134
			// 
			this.Shape134.Height = 0.1458333F;
			this.Shape134.Left = 4.885417F;
			this.Shape134.LineWeight = 0F;
			this.Shape134.Name = "Shape134";
			this.Shape134.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape134.Top = 4.083333F;
			this.Shape134.Width = 0.1354167F;
			// 
			// Shape135
			// 
			this.Shape135.Height = 0.1458333F;
			this.Shape135.Left = 4.885417F;
			this.Shape135.LineWeight = 0F;
			this.Shape135.Name = "Shape135";
			this.Shape135.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape135.Top = 4.46875F;
			this.Shape135.Width = 0.1354167F;
			// 
			// Shape136
			// 
			this.Shape136.Height = 0.1458333F;
			this.Shape136.Left = 4.885417F;
			this.Shape136.LineWeight = 0F;
			this.Shape136.Name = "Shape136";
			this.Shape136.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape136.Top = 4.84375F;
			this.Shape136.Width = 0.1354167F;
			// 
			// Shape137
			// 
			this.Shape137.Height = 0.1458333F;
			this.Shape137.Left = 4.885417F;
			this.Shape137.LineWeight = 0F;
			this.Shape137.Name = "Shape137";
			this.Shape137.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape137.Top = 5.21875F;
			this.Shape137.Width = 0.1354167F;
			// 
			// Shape138
			// 
			this.Shape138.Height = 0.1458333F;
			this.Shape138.Left = 5.177083F;
			this.Shape138.LineWeight = 0F;
			this.Shape138.Name = "Shape138";
			this.Shape138.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape138.Top = 1.083333F;
			this.Shape138.Width = 0.1354167F;
			// 
			// Shape139
			// 
			this.Shape139.Height = 0.1458333F;
			this.Shape139.Left = 5.177083F;
			this.Shape139.LineWeight = 0F;
			this.Shape139.Name = "Shape139";
			this.Shape139.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape139.Top = 1.458333F;
			this.Shape139.Width = 0.1354167F;
			// 
			// Shape140
			// 
			this.Shape140.Height = 0.1458333F;
			this.Shape140.Left = 5.177083F;
			this.Shape140.LineWeight = 0F;
			this.Shape140.Name = "Shape140";
			this.Shape140.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape140.Top = 1.833333F;
			this.Shape140.Width = 0.1354167F;
			// 
			// Shape141
			// 
			this.Shape141.Height = 0.1458333F;
			this.Shape141.Left = 5.177083F;
			this.Shape141.LineWeight = 0F;
			this.Shape141.Name = "Shape141";
			this.Shape141.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape141.Top = 2.208333F;
			this.Shape141.Width = 0.1354167F;
			// 
			// Shape142
			// 
			this.Shape142.Height = 0.1458333F;
			this.Shape142.Left = 5.177083F;
			this.Shape142.LineWeight = 0F;
			this.Shape142.Name = "Shape142";
			this.Shape142.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape142.Top = 2.583333F;
			this.Shape142.Width = 0.1354167F;
			// 
			// Shape143
			// 
			this.Shape143.Height = 0.1458333F;
			this.Shape143.Left = 5.177083F;
			this.Shape143.LineWeight = 0F;
			this.Shape143.Name = "Shape143";
			this.Shape143.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape143.Top = 2.958333F;
			this.Shape143.Width = 0.1354167F;
			// 
			// Shape144
			// 
			this.Shape144.Height = 0.1458333F;
			this.Shape144.Left = 5.177083F;
			this.Shape144.LineWeight = 0F;
			this.Shape144.Name = "Shape144";
			this.Shape144.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape144.Top = 3.333333F;
			this.Shape144.Width = 0.1354167F;
			// 
			// Shape145
			// 
			this.Shape145.Height = 0.1458333F;
			this.Shape145.Left = 5.177083F;
			this.Shape145.LineWeight = 0F;
			this.Shape145.Name = "Shape145";
			this.Shape145.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape145.Top = 3.708333F;
			this.Shape145.Width = 0.1354167F;
			// 
			// Shape146
			// 
			this.Shape146.Height = 0.1458333F;
			this.Shape146.Left = 5.177083F;
			this.Shape146.LineWeight = 0F;
			this.Shape146.Name = "Shape146";
			this.Shape146.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape146.Top = 4.083333F;
			this.Shape146.Width = 0.1354167F;
			// 
			// Shape147
			// 
			this.Shape147.Height = 0.1458333F;
			this.Shape147.Left = 5.177083F;
			this.Shape147.LineWeight = 0F;
			this.Shape147.Name = "Shape147";
			this.Shape147.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape147.Top = 4.46875F;
			this.Shape147.Width = 0.1354167F;
			// 
			// Shape148
			// 
			this.Shape148.Height = 0.1458333F;
			this.Shape148.Left = 5.177083F;
			this.Shape148.LineWeight = 0F;
			this.Shape148.Name = "Shape148";
			this.Shape148.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape148.Top = 4.84375F;
			this.Shape148.Width = 0.1354167F;
			// 
			// Shape149
			// 
			this.Shape149.Height = 0.1458333F;
			this.Shape149.Left = 5.177083F;
			this.Shape149.LineWeight = 0F;
			this.Shape149.Name = "Shape149";
			this.Shape149.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape149.Top = 5.21875F;
			this.Shape149.Width = 0.1354167F;
			// 
			// Shape150
			// 
			this.Shape150.Height = 0.1458333F;
			this.Shape150.Left = 5.46875F;
			this.Shape150.LineWeight = 0F;
			this.Shape150.Name = "Shape150";
			this.Shape150.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape150.Top = 1.083333F;
			this.Shape150.Width = 0.1354167F;
			// 
			// Shape151
			// 
			this.Shape151.Height = 0.1458333F;
			this.Shape151.Left = 5.46875F;
			this.Shape151.LineWeight = 0F;
			this.Shape151.Name = "Shape151";
			this.Shape151.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape151.Top = 1.458333F;
			this.Shape151.Width = 0.1354167F;
			// 
			// Shape152
			// 
			this.Shape152.Height = 0.1458333F;
			this.Shape152.Left = 5.46875F;
			this.Shape152.LineWeight = 0F;
			this.Shape152.Name = "Shape152";
			this.Shape152.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape152.Top = 1.833333F;
			this.Shape152.Width = 0.1354167F;
			// 
			// Shape153
			// 
			this.Shape153.Height = 0.1458333F;
			this.Shape153.Left = 5.46875F;
			this.Shape153.LineWeight = 0F;
			this.Shape153.Name = "Shape153";
			this.Shape153.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape153.Top = 2.208333F;
			this.Shape153.Width = 0.1354167F;
			// 
			// Shape154
			// 
			this.Shape154.Height = 0.1458333F;
			this.Shape154.Left = 5.46875F;
			this.Shape154.LineWeight = 0F;
			this.Shape154.Name = "Shape154";
			this.Shape154.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape154.Top = 2.583333F;
			this.Shape154.Width = 0.1354167F;
			// 
			// Shape155
			// 
			this.Shape155.Height = 0.1458333F;
			this.Shape155.Left = 5.46875F;
			this.Shape155.LineWeight = 0F;
			this.Shape155.Name = "Shape155";
			this.Shape155.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape155.Top = 2.958333F;
			this.Shape155.Width = 0.1354167F;
			// 
			// Shape156
			// 
			this.Shape156.Height = 0.1458333F;
			this.Shape156.Left = 5.46875F;
			this.Shape156.LineWeight = 0F;
			this.Shape156.Name = "Shape156";
			this.Shape156.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape156.Top = 3.333333F;
			this.Shape156.Width = 0.1354167F;
			// 
			// Shape157
			// 
			this.Shape157.Height = 0.1458333F;
			this.Shape157.Left = 5.46875F;
			this.Shape157.LineWeight = 0F;
			this.Shape157.Name = "Shape157";
			this.Shape157.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape157.Top = 3.708333F;
			this.Shape157.Width = 0.1354167F;
			// 
			// Shape158
			// 
			this.Shape158.Height = 0.1458333F;
			this.Shape158.Left = 5.46875F;
			this.Shape158.LineWeight = 0F;
			this.Shape158.Name = "Shape158";
			this.Shape158.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape158.Top = 4.083333F;
			this.Shape158.Width = 0.1354167F;
			// 
			// Shape159
			// 
			this.Shape159.Height = 0.1458333F;
			this.Shape159.Left = 5.46875F;
			this.Shape159.LineWeight = 0F;
			this.Shape159.Name = "Shape159";
			this.Shape159.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape159.Top = 4.46875F;
			this.Shape159.Width = 0.1354167F;
			// 
			// Shape160
			// 
			this.Shape160.Height = 0.1458333F;
			this.Shape160.Left = 5.46875F;
			this.Shape160.LineWeight = 0F;
			this.Shape160.Name = "Shape160";
			this.Shape160.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape160.Top = 4.84375F;
			this.Shape160.Width = 0.1354167F;
			// 
			// Shape161
			// 
			this.Shape161.Height = 0.1458333F;
			this.Shape161.Left = 5.46875F;
			this.Shape161.LineWeight = 0F;
			this.Shape161.Name = "Shape161";
			this.Shape161.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape161.Top = 5.21875F;
			this.Shape161.Width = 0.1354167F;
			// 
			// Shape162
			// 
			this.Shape162.Height = 0.1458333F;
			this.Shape162.Left = 5.770833F;
			this.Shape162.LineWeight = 0F;
			this.Shape162.Name = "Shape162";
			this.Shape162.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape162.Top = 1.083333F;
			this.Shape162.Width = 0.1354167F;
			// 
			// Shape163
			// 
			this.Shape163.Height = 0.1458333F;
			this.Shape163.Left = 5.770833F;
			this.Shape163.LineWeight = 0F;
			this.Shape163.Name = "Shape163";
			this.Shape163.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape163.Top = 1.458333F;
			this.Shape163.Width = 0.1354167F;
			// 
			// Shape164
			// 
			this.Shape164.Height = 0.1458333F;
			this.Shape164.Left = 5.770833F;
			this.Shape164.LineWeight = 0F;
			this.Shape164.Name = "Shape164";
			this.Shape164.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape164.Top = 1.833333F;
			this.Shape164.Width = 0.1354167F;
			// 
			// Shape165
			// 
			this.Shape165.Height = 0.1458333F;
			this.Shape165.Left = 5.770833F;
			this.Shape165.LineWeight = 0F;
			this.Shape165.Name = "Shape165";
			this.Shape165.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape165.Top = 2.208333F;
			this.Shape165.Width = 0.1354167F;
			// 
			// Shape166
			// 
			this.Shape166.Height = 0.1458333F;
			this.Shape166.Left = 5.770833F;
			this.Shape166.LineWeight = 0F;
			this.Shape166.Name = "Shape166";
			this.Shape166.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape166.Top = 2.583333F;
			this.Shape166.Width = 0.1354167F;
			// 
			// Shape167
			// 
			this.Shape167.Height = 0.1458333F;
			this.Shape167.Left = 5.770833F;
			this.Shape167.LineWeight = 0F;
			this.Shape167.Name = "Shape167";
			this.Shape167.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape167.Top = 2.958333F;
			this.Shape167.Width = 0.1354167F;
			// 
			// Shape168
			// 
			this.Shape168.Height = 0.1458333F;
			this.Shape168.Left = 5.770833F;
			this.Shape168.LineWeight = 0F;
			this.Shape168.Name = "Shape168";
			this.Shape168.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape168.Top = 3.333333F;
			this.Shape168.Width = 0.1354167F;
			// 
			// Shape169
			// 
			this.Shape169.Height = 0.1458333F;
			this.Shape169.Left = 5.770833F;
			this.Shape169.LineWeight = 0F;
			this.Shape169.Name = "Shape169";
			this.Shape169.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape169.Top = 3.708333F;
			this.Shape169.Width = 0.1354167F;
			// 
			// Shape170
			// 
			this.Shape170.Height = 0.1458333F;
			this.Shape170.Left = 5.770833F;
			this.Shape170.LineWeight = 0F;
			this.Shape170.Name = "Shape170";
			this.Shape170.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape170.Top = 4.083333F;
			this.Shape170.Width = 0.1354167F;
			// 
			// Shape171
			// 
			this.Shape171.Height = 0.1458333F;
			this.Shape171.Left = 5.770833F;
			this.Shape171.LineWeight = 0F;
			this.Shape171.Name = "Shape171";
			this.Shape171.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape171.Top = 4.46875F;
			this.Shape171.Width = 0.1354167F;
			// 
			// Shape172
			// 
			this.Shape172.Height = 0.1458333F;
			this.Shape172.Left = 5.770833F;
			this.Shape172.LineWeight = 0F;
			this.Shape172.Name = "Shape172";
			this.Shape172.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape172.Top = 4.84375F;
			this.Shape172.Width = 0.1354167F;
			// 
			// Shape173
			// 
			this.Shape173.Height = 0.1458333F;
			this.Shape173.Left = 5.770833F;
			this.Shape173.LineWeight = 0F;
			this.Shape173.Name = "Shape173";
			this.Shape173.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape173.Top = 5.21875F;
			this.Shape173.Width = 0.1354167F;
			// 
			// Shape174
			// 
			this.Shape174.Height = 0.1458333F;
			this.Shape174.Left = 6.0625F;
			this.Shape174.LineWeight = 0F;
			this.Shape174.Name = "Shape174";
			this.Shape174.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape174.Top = 1.083333F;
			this.Shape174.Width = 0.1354167F;
			// 
			// Shape175
			// 
			this.Shape175.Height = 0.1458333F;
			this.Shape175.Left = 6.0625F;
			this.Shape175.LineWeight = 0F;
			this.Shape175.Name = "Shape175";
			this.Shape175.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape175.Top = 1.458333F;
			this.Shape175.Width = 0.1354167F;
			// 
			// Shape176
			// 
			this.Shape176.Height = 0.1458333F;
			this.Shape176.Left = 6.0625F;
			this.Shape176.LineWeight = 0F;
			this.Shape176.Name = "Shape176";
			this.Shape176.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape176.Top = 1.833333F;
			this.Shape176.Width = 0.1354167F;
			// 
			// Shape177
			// 
			this.Shape177.Height = 0.1458333F;
			this.Shape177.Left = 6.0625F;
			this.Shape177.LineWeight = 0F;
			this.Shape177.Name = "Shape177";
			this.Shape177.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape177.Top = 2.208333F;
			this.Shape177.Width = 0.1354167F;
			// 
			// Shape178
			// 
			this.Shape178.Height = 0.1458333F;
			this.Shape178.Left = 6.0625F;
			this.Shape178.LineWeight = 0F;
			this.Shape178.Name = "Shape178";
			this.Shape178.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape178.Top = 2.583333F;
			this.Shape178.Width = 0.1354167F;
			// 
			// Shape179
			// 
			this.Shape179.Height = 0.1458333F;
			this.Shape179.Left = 6.0625F;
			this.Shape179.LineWeight = 0F;
			this.Shape179.Name = "Shape179";
			this.Shape179.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape179.Top = 2.958333F;
			this.Shape179.Width = 0.1354167F;
			// 
			// Shape180
			// 
			this.Shape180.Height = 0.1458333F;
			this.Shape180.Left = 6.0625F;
			this.Shape180.LineWeight = 0F;
			this.Shape180.Name = "Shape180";
			this.Shape180.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape180.Top = 3.333333F;
			this.Shape180.Width = 0.1354167F;
			// 
			// Shape181
			// 
			this.Shape181.Height = 0.1458333F;
			this.Shape181.Left = 6.0625F;
			this.Shape181.LineWeight = 0F;
			this.Shape181.Name = "Shape181";
			this.Shape181.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape181.Top = 3.708333F;
			this.Shape181.Width = 0.1354167F;
			// 
			// Shape182
			// 
			this.Shape182.Height = 0.1458333F;
			this.Shape182.Left = 6.0625F;
			this.Shape182.LineWeight = 0F;
			this.Shape182.Name = "Shape182";
			this.Shape182.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape182.Top = 4.083333F;
			this.Shape182.Width = 0.1354167F;
			// 
			// Shape183
			// 
			this.Shape183.Height = 0.1458333F;
			this.Shape183.Left = 6.0625F;
			this.Shape183.LineWeight = 0F;
			this.Shape183.Name = "Shape183";
			this.Shape183.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape183.Top = 4.46875F;
			this.Shape183.Width = 0.1354167F;
			// 
			// Shape184
			// 
			this.Shape184.Height = 0.1458333F;
			this.Shape184.Left = 6.0625F;
			this.Shape184.LineWeight = 0F;
			this.Shape184.Name = "Shape184";
			this.Shape184.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape184.Top = 4.84375F;
			this.Shape184.Width = 0.1354167F;
			// 
			// Shape185
			// 
			this.Shape185.Height = 0.1458333F;
			this.Shape185.Left = 6.0625F;
			this.Shape185.LineWeight = 0F;
			this.Shape185.Name = "Shape185";
			this.Shape185.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape185.Top = 5.21875F;
			this.Shape185.Width = 0.1354167F;
			// 
			// Shape186
			// 
			this.Shape186.Height = 0.1458333F;
			this.Shape186.Left = 6.354167F;
			this.Shape186.LineWeight = 0F;
			this.Shape186.Name = "Shape186";
			this.Shape186.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape186.Top = 1.083333F;
			this.Shape186.Width = 0.1354167F;
			// 
			// Shape187
			// 
			this.Shape187.Height = 0.1458333F;
			this.Shape187.Left = 6.354167F;
			this.Shape187.LineWeight = 0F;
			this.Shape187.Name = "Shape187";
			this.Shape187.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape187.Top = 1.458333F;
			this.Shape187.Width = 0.1354167F;
			// 
			// Shape188
			// 
			this.Shape188.Height = 0.1458333F;
			this.Shape188.Left = 6.354167F;
			this.Shape188.LineWeight = 0F;
			this.Shape188.Name = "Shape188";
			this.Shape188.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape188.Top = 1.833333F;
			this.Shape188.Width = 0.1354167F;
			// 
			// Shape189
			// 
			this.Shape189.Height = 0.1458333F;
			this.Shape189.Left = 6.354167F;
			this.Shape189.LineWeight = 0F;
			this.Shape189.Name = "Shape189";
			this.Shape189.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape189.Top = 2.208333F;
			this.Shape189.Width = 0.1354167F;
			// 
			// Shape190
			// 
			this.Shape190.Height = 0.1458333F;
			this.Shape190.Left = 6.354167F;
			this.Shape190.LineWeight = 0F;
			this.Shape190.Name = "Shape190";
			this.Shape190.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape190.Top = 2.583333F;
			this.Shape190.Width = 0.1354167F;
			// 
			// Shape191
			// 
			this.Shape191.Height = 0.1458333F;
			this.Shape191.Left = 6.354167F;
			this.Shape191.LineWeight = 0F;
			this.Shape191.Name = "Shape191";
			this.Shape191.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape191.Top = 2.958333F;
			this.Shape191.Width = 0.1354167F;
			// 
			// Shape192
			// 
			this.Shape192.Height = 0.1458333F;
			this.Shape192.Left = 6.354167F;
			this.Shape192.LineWeight = 0F;
			this.Shape192.Name = "Shape192";
			this.Shape192.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape192.Top = 3.333333F;
			this.Shape192.Width = 0.1354167F;
			// 
			// Shape193
			// 
			this.Shape193.Height = 0.1458333F;
			this.Shape193.Left = 6.354167F;
			this.Shape193.LineWeight = 0F;
			this.Shape193.Name = "Shape193";
			this.Shape193.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape193.Top = 3.708333F;
			this.Shape193.Width = 0.1354167F;
			// 
			// Shape194
			// 
			this.Shape194.Height = 0.1458333F;
			this.Shape194.Left = 6.354167F;
			this.Shape194.LineWeight = 0F;
			this.Shape194.Name = "Shape194";
			this.Shape194.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape194.Top = 4.083333F;
			this.Shape194.Width = 0.1354167F;
			// 
			// Shape195
			// 
			this.Shape195.Height = 0.1458333F;
			this.Shape195.Left = 6.354167F;
			this.Shape195.LineWeight = 0F;
			this.Shape195.Name = "Shape195";
			this.Shape195.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape195.Top = 4.46875F;
			this.Shape195.Width = 0.1354167F;
			// 
			// Shape196
			// 
			this.Shape196.Height = 0.1458333F;
			this.Shape196.Left = 6.354167F;
			this.Shape196.LineWeight = 0F;
			this.Shape196.Name = "Shape196";
			this.Shape196.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape196.Top = 4.84375F;
			this.Shape196.Width = 0.1354167F;
			// 
			// Shape197
			// 
			this.Shape197.Height = 0.1458333F;
			this.Shape197.Left = 6.354167F;
			this.Shape197.LineWeight = 0F;
			this.Shape197.Name = "Shape197";
			this.Shape197.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape197.Top = 5.21875F;
			this.Shape197.Width = 0.1354167F;
			// 
			// Shape198
			// 
			this.Shape198.Height = 0.1458333F;
			this.Shape198.Left = 6.65625F;
			this.Shape198.LineWeight = 0F;
			this.Shape198.Name = "Shape198";
			this.Shape198.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape198.Top = 1.083333F;
			this.Shape198.Width = 0.1354167F;
			// 
			// Shape199
			// 
			this.Shape199.Height = 0.1458333F;
			this.Shape199.Left = 6.65625F;
			this.Shape199.LineWeight = 0F;
			this.Shape199.Name = "Shape199";
			this.Shape199.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape199.Top = 1.458333F;
			this.Shape199.Width = 0.1354167F;
			// 
			// Shape200
			// 
			this.Shape200.Height = 0.1458333F;
			this.Shape200.Left = 6.65625F;
			this.Shape200.LineWeight = 0F;
			this.Shape200.Name = "Shape200";
			this.Shape200.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape200.Top = 1.833333F;
			this.Shape200.Width = 0.1354167F;
			// 
			// Shape201
			// 
			this.Shape201.Height = 0.1458333F;
			this.Shape201.Left = 6.65625F;
			this.Shape201.LineWeight = 0F;
			this.Shape201.Name = "Shape201";
			this.Shape201.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape201.Top = 2.208333F;
			this.Shape201.Width = 0.1354167F;
			// 
			// Shape202
			// 
			this.Shape202.Height = 0.1458333F;
			this.Shape202.Left = 6.65625F;
			this.Shape202.LineWeight = 0F;
			this.Shape202.Name = "Shape202";
			this.Shape202.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape202.Top = 2.583333F;
			this.Shape202.Width = 0.1354167F;
			// 
			// Shape203
			// 
			this.Shape203.Height = 0.1458333F;
			this.Shape203.Left = 6.65625F;
			this.Shape203.LineWeight = 0F;
			this.Shape203.Name = "Shape203";
			this.Shape203.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape203.Top = 2.958333F;
			this.Shape203.Width = 0.1354167F;
			// 
			// Shape204
			// 
			this.Shape204.Height = 0.1458333F;
			this.Shape204.Left = 6.65625F;
			this.Shape204.LineWeight = 0F;
			this.Shape204.Name = "Shape204";
			this.Shape204.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape204.Top = 3.333333F;
			this.Shape204.Width = 0.1354167F;
			// 
			// Shape205
			// 
			this.Shape205.Height = 0.1458333F;
			this.Shape205.Left = 6.65625F;
			this.Shape205.LineWeight = 0F;
			this.Shape205.Name = "Shape205";
			this.Shape205.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape205.Top = 3.708333F;
			this.Shape205.Width = 0.1354167F;
			// 
			// Shape206
			// 
			this.Shape206.Height = 0.1458333F;
			this.Shape206.Left = 6.65625F;
			this.Shape206.LineWeight = 0F;
			this.Shape206.Name = "Shape206";
			this.Shape206.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape206.Top = 4.083333F;
			this.Shape206.Width = 0.1354167F;
			// 
			// Shape207
			// 
			this.Shape207.Height = 0.1458333F;
			this.Shape207.Left = 6.65625F;
			this.Shape207.LineWeight = 0F;
			this.Shape207.Name = "Shape207";
			this.Shape207.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape207.Top = 4.46875F;
			this.Shape207.Width = 0.1354167F;
			// 
			// Shape208
			// 
			this.Shape208.Height = 0.1458333F;
			this.Shape208.Left = 6.65625F;
			this.Shape208.LineWeight = 0F;
			this.Shape208.Name = "Shape208";
			this.Shape208.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape208.Top = 4.84375F;
			this.Shape208.Width = 0.1354167F;
			// 
			// Shape209
			// 
			this.Shape209.Height = 0.1458333F;
			this.Shape209.Left = 6.65625F;
			this.Shape209.LineWeight = 0F;
			this.Shape209.Name = "Shape209";
			this.Shape209.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape209.Top = 5.21875F;
			this.Shape209.Width = 0.1354167F;
			// 
			// Shape210
			// 
			this.Shape210.Height = 0.1458333F;
			this.Shape210.Left = 6.947917F;
			this.Shape210.LineWeight = 0F;
			this.Shape210.Name = "Shape210";
			this.Shape210.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape210.Top = 1.083333F;
			this.Shape210.Width = 0.1354167F;
			// 
			// Shape211
			// 
			this.Shape211.Height = 0.1458333F;
			this.Shape211.Left = 6.947917F;
			this.Shape211.LineWeight = 0F;
			this.Shape211.Name = "Shape211";
			this.Shape211.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape211.Top = 1.458333F;
			this.Shape211.Width = 0.1354167F;
			// 
			// Shape212
			// 
			this.Shape212.Height = 0.1458333F;
			this.Shape212.Left = 6.947917F;
			this.Shape212.LineWeight = 0F;
			this.Shape212.Name = "Shape212";
			this.Shape212.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape212.Top = 1.833333F;
			this.Shape212.Width = 0.1354167F;
			// 
			// Shape213
			// 
			this.Shape213.Height = 0.1458333F;
			this.Shape213.Left = 6.947917F;
			this.Shape213.LineWeight = 0F;
			this.Shape213.Name = "Shape213";
			this.Shape213.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape213.Top = 2.208333F;
			this.Shape213.Width = 0.1354167F;
			// 
			// Shape214
			// 
			this.Shape214.Height = 0.1458333F;
			this.Shape214.Left = 6.947917F;
			this.Shape214.LineWeight = 0F;
			this.Shape214.Name = "Shape214";
			this.Shape214.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape214.Top = 2.583333F;
			this.Shape214.Width = 0.1354167F;
			// 
			// Shape215
			// 
			this.Shape215.Height = 0.1458333F;
			this.Shape215.Left = 6.947917F;
			this.Shape215.LineWeight = 0F;
			this.Shape215.Name = "Shape215";
			this.Shape215.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape215.Top = 2.958333F;
			this.Shape215.Width = 0.1354167F;
			// 
			// Shape216
			// 
			this.Shape216.Height = 0.1458333F;
			this.Shape216.Left = 6.947917F;
			this.Shape216.LineWeight = 0F;
			this.Shape216.Name = "Shape216";
			this.Shape216.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape216.Top = 3.333333F;
			this.Shape216.Width = 0.1354167F;
			// 
			// Shape217
			// 
			this.Shape217.Height = 0.1458333F;
			this.Shape217.Left = 6.947917F;
			this.Shape217.LineWeight = 0F;
			this.Shape217.Name = "Shape217";
			this.Shape217.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape217.Top = 3.708333F;
			this.Shape217.Width = 0.1354167F;
			// 
			// Shape218
			// 
			this.Shape218.Height = 0.1458333F;
			this.Shape218.Left = 6.947917F;
			this.Shape218.LineWeight = 0F;
			this.Shape218.Name = "Shape218";
			this.Shape218.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape218.Top = 4.083333F;
			this.Shape218.Width = 0.1354167F;
			// 
			// Shape219
			// 
			this.Shape219.Height = 0.1458333F;
			this.Shape219.Left = 6.947917F;
			this.Shape219.LineWeight = 0F;
			this.Shape219.Name = "Shape219";
			this.Shape219.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape219.Top = 4.46875F;
			this.Shape219.Width = 0.1354167F;
			// 
			// Shape220
			// 
			this.Shape220.Height = 0.1458333F;
			this.Shape220.Left = 6.947917F;
			this.Shape220.LineWeight = 0F;
			this.Shape220.Name = "Shape220";
			this.Shape220.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape220.Top = 4.84375F;
			this.Shape220.Width = 0.1354167F;
			// 
			// Shape221
			// 
			this.Shape221.Height = 0.1458333F;
			this.Shape221.Left = 6.947917F;
			this.Shape221.LineWeight = 0F;
			this.Shape221.Name = "Shape221";
			this.Shape221.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape221.Top = 5.21875F;
			this.Shape221.Width = 0.1354167F;
			// 
			// Shape222
			// 
			this.Shape222.Height = 0.1458333F;
			this.Shape222.Left = 7.239583F;
			this.Shape222.LineWeight = 0F;
			this.Shape222.Name = "Shape222";
			this.Shape222.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape222.Top = 1.458333F;
			this.Shape222.Width = 0.1354167F;
			// 
			// Shape223
			// 
			this.Shape223.Height = 0.1458333F;
			this.Shape223.Left = 7.239583F;
			this.Shape223.LineWeight = 0F;
			this.Shape223.Name = "Shape223";
			this.Shape223.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape223.Top = 1.083333F;
			this.Shape223.Width = 0.1354167F;
			// 
			// Shape224
			// 
			this.Shape224.Height = 0.1458333F;
			this.Shape224.Left = 7.239583F;
			this.Shape224.LineWeight = 0F;
			this.Shape224.Name = "Shape224";
			this.Shape224.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape224.Top = 1.833333F;
			this.Shape224.Width = 0.1354167F;
			// 
			// Shape225
			// 
			this.Shape225.Height = 0.1458333F;
			this.Shape225.Left = 7.239583F;
			this.Shape225.LineWeight = 0F;
			this.Shape225.Name = "Shape225";
			this.Shape225.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape225.Top = 2.208333F;
			this.Shape225.Width = 0.1354167F;
			// 
			// Shape226
			// 
			this.Shape226.Height = 0.1458333F;
			this.Shape226.Left = 7.239583F;
			this.Shape226.LineWeight = 0F;
			this.Shape226.Name = "Shape226";
			this.Shape226.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape226.Top = 2.583333F;
			this.Shape226.Width = 0.1354167F;
			// 
			// Shape227
			// 
			this.Shape227.Height = 0.1458333F;
			this.Shape227.Left = 7.239583F;
			this.Shape227.LineWeight = 0F;
			this.Shape227.Name = "Shape227";
			this.Shape227.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape227.Top = 2.958333F;
			this.Shape227.Width = 0.1354167F;
			// 
			// Shape228
			// 
			this.Shape228.Height = 0.1458333F;
			this.Shape228.Left = 7.239583F;
			this.Shape228.LineWeight = 0F;
			this.Shape228.Name = "Shape228";
			this.Shape228.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape228.Top = 3.333333F;
			this.Shape228.Width = 0.1354167F;
			// 
			// Shape229
			// 
			this.Shape229.Height = 0.1458333F;
			this.Shape229.Left = 7.239583F;
			this.Shape229.LineWeight = 0F;
			this.Shape229.Name = "Shape229";
			this.Shape229.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape229.Top = 3.708333F;
			this.Shape229.Width = 0.1354167F;
			// 
			// Shape230
			// 
			this.Shape230.Height = 0.1458333F;
			this.Shape230.Left = 7.239583F;
			this.Shape230.LineWeight = 0F;
			this.Shape230.Name = "Shape230";
			this.Shape230.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape230.Top = 4.083333F;
			this.Shape230.Width = 0.1354167F;
			// 
			// Shape231
			// 
			this.Shape231.Height = 0.1458333F;
			this.Shape231.Left = 7.239583F;
			this.Shape231.LineWeight = 0F;
			this.Shape231.Name = "Shape231";
			this.Shape231.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape231.Top = 4.46875F;
			this.Shape231.Width = 0.1354167F;
			// 
			// Shape232
			// 
			this.Shape232.Height = 0.1458333F;
			this.Shape232.Left = 7.239583F;
			this.Shape232.LineWeight = 0F;
			this.Shape232.Name = "Shape232";
			this.Shape232.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape232.Top = 4.84375F;
			this.Shape232.Width = 0.1354167F;
			// 
			// Shape233
			// 
			this.Shape233.Height = 0.1458333F;
			this.Shape233.Left = 7.239583F;
			this.Shape233.LineWeight = 0F;
			this.Shape233.Name = "Shape233";
			this.Shape233.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape233.Top = 5.21875F;
			this.Shape233.Width = 0.1354167F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1145833F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.4479167F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label25.Text = "Covered Individuals -";
			this.Label25.Top = 0.53125F;
			this.Label25.Width = 1.020833F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.125F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" + "t: bold; vertical-align: middle";
			this.Label24.Text = "Part IV";
			this.Label24.Top = 0.53125F;
			this.Label24.Width = 0.34375F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1145833F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 1.572917F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
			this.Label32.Text = "Continuation Sheet";
			this.Label32.Top = 0.53125F;
			this.Label32.Width = 1.020833F;
			// 
			// Line31
			// 
			this.Line31.Height = 0F;
			this.Line31.Left = 0F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 0.53125F;
			this.Line31.Width = 7.46875F;
			this.Line31.X1 = 0F;
			this.Line31.X2 = 7.46875F;
			this.Line31.Y1 = 0.53125F;
			this.Line31.Y2 = 0.53125F;
			// 
			// Line32
			// 
			this.Line32.Height = 0F;
			this.Line32.Left = 0F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 0.25F;
			this.Line32.Width = 7.46875F;
			this.Line32.X1 = 0F;
			this.Line32.X2 = 7.46875F;
			this.Line32.Y1 = 0.25F;
			this.Line32.Y2 = 0.25F;
			// 
			// Line33
			// 
			this.Line33.Height = 0.28125F;
			this.Line33.Left = 5.729167F;
			this.Line33.LineWeight = 0F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 0.25F;
			this.Line33.Width = 0F;
			this.Line33.X1 = 5.729167F;
			this.Line33.X2 = 5.729167F;
			this.Line33.Y1 = 0.25F;
			this.Line33.Y2 = 0.53125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.09375F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 6pt; text-align: left";
			this.Label33.Text = "Form 1095-B (2016)";
			this.Label33.Top = 0.125F;
			this.Label33.Width = 0.8333333F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.09375F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 7.0625F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 6pt; text-align: left";
			this.Label34.Text = "Page";
			this.Label34.Top = 0.125F;
			this.Label34.Width = 0.2708333F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.125F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 7.3125F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 7pt; font-weight: bold; text-align: right; vertical-align: bottom";
			this.Label35.Text = "3";
			this.Label35.Top = 0.09375F;
			this.Label35.Width = 0.1458333F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1354167F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 6.895833F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'OCR A Extended\'; font-size: 8.5pt; text-align: right";
			this.Label93.Text = "560317";
			this.Label93.Top = 0F;
			this.Label93.Width = 0.5625F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1145833F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 0.02083333F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label94.Text = "35";
			this.Label94.Top = 3.354167F;
			this.Label94.Width = 0.2083333F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1145833F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 0.02083333F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label95.Text = "36";
			this.Label95.Top = 3.729167F;
			this.Label95.Width = 0.2083333F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1145833F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 0.02083333F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label96.Text = "37";
			this.Label96.Top = 4.104167F;
			this.Label96.Width = 0.2083333F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1145833F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 0.02083333F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label97.Text = "38";
			this.Label97.Top = 4.489583F;
			this.Label97.Width = 0.2083333F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1145833F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 0.02083333F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label98.Text = "39";
			this.Label98.Top = 4.864583F;
			this.Label98.Width = 0.2083333F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1145833F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.02083333F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
			this.Label99.Text = "40";
			this.Label99.Top = 5.239583F;
			this.Label99.Width = 0.2083333F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.09375F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-size: 5pt; text-align: left";
			this.Label69.Text = "Name of employee";
			this.Label69.Top = 0.28125F;
			this.Label69.Width = 1.583333F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.09375F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 5.75F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-size: 5pt; text-align: left";
			this.Label70.Text = "Date of birth (If SSN or other TIN is not available)";
			this.Label70.Top = 0.28125F;
			this.Label70.Width = 1.645833F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 0F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.65625F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.65625F;
			this.Line1.Y2 = 0.65625F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.09375F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 4.1875F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-size: 5pt; text-align: left";
			this.Label100.Text = "Social Security number (SSN or other TIN)";
			this.Label100.Top = 0.28125F;
			this.Label100.Width = 1.458333F;
			// 
			// Line34
			// 
			this.Line34.Height = 0.28125F;
			this.Line34.Left = 4.125F;
			this.Line34.LineWeight = 0F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 0.25F;
			this.Line34.Width = 0F;
			this.Line34.X1 = 4.125F;
			this.Line34.X2 = 4.125F;
			this.Line34.Y1 = 0.25F;
			this.Line34.Y2 = 0.53125F;
			// 
			// rpt1095B2016BlankPortraitPage2
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl12Months;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape82;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape83;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape84;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape85;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape86;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape87;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape88;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape89;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape90;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape91;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape92;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape93;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape94;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape95;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape96;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape97;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape98;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape99;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape100;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape101;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape102;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape103;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape104;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape105;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape106;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape107;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape108;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape109;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape110;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape111;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape112;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape113;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape114;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape115;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape116;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape117;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape118;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape119;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape120;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape121;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape122;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape123;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape124;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape125;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape126;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape127;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape128;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape129;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape130;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape131;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape132;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape133;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape134;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape135;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape136;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape137;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape138;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape139;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape140;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape141;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape142;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape143;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape144;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape145;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape146;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape147;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape148;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape149;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape150;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape151;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape152;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape153;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape154;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape155;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape156;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape157;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape158;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape159;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape160;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape161;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape162;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape163;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape164;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape165;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape166;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape167;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape168;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape169;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape170;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape171;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape172;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape173;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape174;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape175;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape176;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape177;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape178;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape179;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape180;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape181;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape182;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape183;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape184;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape185;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape186;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape187;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape188;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape189;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape190;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape191;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape192;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape193;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape194;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape195;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape196;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape197;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape198;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape199;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape200;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape201;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape202;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape203;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape204;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape205;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape206;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape207;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape208;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape209;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape210;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape211;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape212;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape213;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape214;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape215;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape216;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape217;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape218;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape219;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape220;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape221;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape222;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape223;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape224;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape225;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape226;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape227;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape228;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape229;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape230;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape231;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape232;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape233;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
	}
}
