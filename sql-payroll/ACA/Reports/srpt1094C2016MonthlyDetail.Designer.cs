﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt1094C2016MonthlyDetail.
	/// </summary>
	partial class srpt1094C2016MonthlyDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srpt1094C2016MonthlyDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtAll12MinimumOfferYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12MinimumOfferNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12InGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12FullTimeCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12EmployeeCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferYes12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinimumOfferNo12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInGroup12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFullCount12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCount12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAll12Months4980H = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt4980H12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12MinimumOfferYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12MinimumOfferNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12InGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12FullTimeCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12EmployeeCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Months4980H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAll12MinimumOfferYes,
            this.txtAll12MinimumOfferNo,
            this.txtAll12InGroup,
            this.txtMinimumOfferYes1,
            this.txtMinimumOfferNo1,
            this.txtInGroup1,
            this.txtMinimumOfferYes2,
            this.txtMinimumOfferNo2,
            this.txtInGroup2,
            this.txtMinimumOfferYes3,
            this.txtMinimumOfferNo3,
            this.txtInGroup3,
            this.txtMinimumOfferYes4,
            this.txtMinimumOfferNo4,
            this.txtInGroup4,
            this.txtAll12FullTimeCount,
            this.txtAll12EmployeeCount,
            this.txtFullCount1,
            this.txtEmployeeCount1,
            this.txtFullCount2,
            this.txtEmployeeCount2,
            this.txtFullCount3,
            this.txtEmployeeCount3,
            this.txtFullCount4,
            this.txtEmployeeCount4,
            this.txtMinimumOfferYes5,
            this.txtMinimumOfferNo5,
            this.txtInGroup5,
            this.txtMinimumOfferYes6,
            this.txtMinimumOfferNo6,
            this.txtInGroup6,
            this.txtMinimumOfferYes7,
            this.txtMinimumOfferNo7,
            this.txtInGroup7,
            this.txtMinimumOfferYes8,
            this.txtMinimumOfferNo8,
            this.txtInGroup8,
            this.txtMinimumOfferYes9,
            this.txtMinimumOfferNo9,
            this.txtInGroup9,
            this.txtFullCount5,
            this.txtEmployeeCount5,
            this.txtFullCount6,
            this.txtEmployeeCount6,
            this.txtFullCount7,
            this.txtEmployeeCount7,
            this.txtFullCount8,
            this.txtEmployeeCount8,
            this.txtFullCount9,
            this.txtEmployeeCount9,
            this.txtMinimumOfferYes10,
            this.txtMinimumOfferNo10,
            this.txtInGroup10,
            this.txtMinimumOfferYes11,
            this.txtMinimumOfferNo11,
            this.txtInGroup11,
            this.txtMinimumOfferYes12,
            this.txtMinimumOfferNo12,
            this.txtInGroup12,
            this.txtFullCount10,
            this.txtEmployeeCount10,
            this.txtFullCount11,
            this.txtEmployeeCount11,
            this.txtFullCount12,
            this.txtEmployeeCount12,
            this.txtAll12Months4980H,
            this.txt4980H1,
            this.txt4980H2,
            this.txt4980H3,
            this.txt4980H4,
            this.txt4980H5,
            this.txt4980H6,
            this.txt4980H7,
            this.txt4980H8,
            this.txt4980H9,
            this.txt4980H10,
            this.txt4980H11,
            this.txt4980H12});
            this.Detail.Height = 7.291667F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtAll12MinimumOfferYes
            // 
            this.txtAll12MinimumOfferYes.Height = 0.1770833F;
            this.txtAll12MinimumOfferYes.Left = 1.9F;
            this.txtAll12MinimumOfferYes.Name = "txtAll12MinimumOfferYes";
            this.txtAll12MinimumOfferYes.Text = "X";
            this.txtAll12MinimumOfferYes.Top = 1.178F;
            this.txtAll12MinimumOfferYes.Width = 0.1875F;
            // 
            // txtAll12MinimumOfferNo
            // 
            this.txtAll12MinimumOfferNo.Height = 0.1770833F;
            this.txtAll12MinimumOfferNo.Left = 3.001F;
            this.txtAll12MinimumOfferNo.Name = "txtAll12MinimumOfferNo";
            this.txtAll12MinimumOfferNo.Text = "X";
            this.txtAll12MinimumOfferNo.Top = 1.178F;
            this.txtAll12MinimumOfferNo.Width = 0.1875F;
            // 
            // txtAll12InGroup
            // 
            this.txtAll12InGroup.Height = 0.1770833F;
            this.txtAll12InGroup.Left = 7.511F;
            this.txtAll12InGroup.Name = "txtAll12InGroup";
            this.txtAll12InGroup.Text = "X";
            this.txtAll12InGroup.Top = 1.178F;
            this.txtAll12InGroup.Width = 0.1875F;
            // 
            // txtMinimumOfferYes1
            // 
            this.txtMinimumOfferYes1.Height = 0.1770833F;
            this.txtMinimumOfferYes1.Left = 1.9F;
            this.txtMinimumOfferYes1.Name = "txtMinimumOfferYes1";
            this.txtMinimumOfferYes1.Text = "X";
            this.txtMinimumOfferYes1.Top = 1.656334F;
            this.txtMinimumOfferYes1.Width = 0.1875F;
            // 
            // txtMinimumOfferNo1
            // 
            this.txtMinimumOfferNo1.Height = 0.1770833F;
            this.txtMinimumOfferNo1.Left = 3.001F;
            this.txtMinimumOfferNo1.Name = "txtMinimumOfferNo1";
            this.txtMinimumOfferNo1.Text = "X";
            this.txtMinimumOfferNo1.Top = 1.656334F;
            this.txtMinimumOfferNo1.Width = 0.1875F;
            // 
            // txtInGroup1
            // 
            this.txtInGroup1.Height = 0.1770833F;
            this.txtInGroup1.Left = 7.511F;
            this.txtInGroup1.Name = "txtInGroup1";
            this.txtInGroup1.Text = "X";
            this.txtInGroup1.Top = 1.656334F;
            this.txtInGroup1.Width = 0.1875F;
            // 
            // txtMinimumOfferYes2
            // 
            this.txtMinimumOfferYes2.Height = 0.1770833F;
            this.txtMinimumOfferYes2.Left = 1.9F;
            this.txtMinimumOfferYes2.Name = "txtMinimumOfferYes2";
            this.txtMinimumOfferYes2.Text = "X";
            this.txtMinimumOfferYes2.Top = 2.136334F;
            this.txtMinimumOfferYes2.Width = 0.1875F;
            // 
            // txtMinimumOfferNo2
            // 
            this.txtMinimumOfferNo2.Height = 0.1770833F;
            this.txtMinimumOfferNo2.Left = 3.001F;
            this.txtMinimumOfferNo2.Name = "txtMinimumOfferNo2";
            this.txtMinimumOfferNo2.Text = "X";
            this.txtMinimumOfferNo2.Top = 2.136334F;
            this.txtMinimumOfferNo2.Width = 0.1875F;
            // 
            // txtInGroup2
            // 
            this.txtInGroup2.Height = 0.1770833F;
            this.txtInGroup2.Left = 7.511F;
            this.txtInGroup2.Name = "txtInGroup2";
            this.txtInGroup2.Text = "X";
            this.txtInGroup2.Top = 2.136334F;
            this.txtInGroup2.Width = 0.1875F;
            // 
            // txtMinimumOfferYes3
            // 
            this.txtMinimumOfferYes3.Height = 0.1770833F;
            this.txtMinimumOfferYes3.Left = 1.9F;
            this.txtMinimumOfferYes3.Name = "txtMinimumOfferYes3";
            this.txtMinimumOfferYes3.Text = "X";
            this.txtMinimumOfferYes3.Top = 2.616F;
            this.txtMinimumOfferYes3.Width = 0.1875F;
            // 
            // txtMinimumOfferNo3
            // 
            this.txtMinimumOfferNo3.Height = 0.1770833F;
            this.txtMinimumOfferNo3.Left = 3.001F;
            this.txtMinimumOfferNo3.Name = "txtMinimumOfferNo3";
            this.txtMinimumOfferNo3.Text = "X";
            this.txtMinimumOfferNo3.Top = 2.616F;
            this.txtMinimumOfferNo3.Width = 0.1875F;
            // 
            // txtInGroup3
            // 
            this.txtInGroup3.Height = 0.1770833F;
            this.txtInGroup3.Left = 7.511F;
            this.txtInGroup3.Name = "txtInGroup3";
            this.txtInGroup3.Text = "X";
            this.txtInGroup3.Top = 2.616F;
            this.txtInGroup3.Width = 0.1875F;
            // 
            // txtMinimumOfferYes4
            // 
            this.txtMinimumOfferYes4.Height = 0.1770833F;
            this.txtMinimumOfferYes4.Left = 1.9F;
            this.txtMinimumOfferYes4.Name = "txtMinimumOfferYes4";
            this.txtMinimumOfferYes4.Text = "X";
            this.txtMinimumOfferYes4.Top = 3.096F;
            this.txtMinimumOfferYes4.Width = 0.1875F;
            // 
            // txtMinimumOfferNo4
            // 
            this.txtMinimumOfferNo4.Height = 0.1770833F;
            this.txtMinimumOfferNo4.Left = 3.001F;
            this.txtMinimumOfferNo4.Name = "txtMinimumOfferNo4";
            this.txtMinimumOfferNo4.Text = "X";
            this.txtMinimumOfferNo4.Top = 3.096F;
            this.txtMinimumOfferNo4.Width = 0.1875F;
            // 
            // txtInGroup4
            // 
            this.txtInGroup4.Height = 0.1770833F;
            this.txtInGroup4.Left = 7.511F;
            this.txtInGroup4.Name = "txtInGroup4";
            this.txtInGroup4.Text = "X";
            this.txtInGroup4.Top = 3.096F;
            this.txtInGroup4.Width = 0.1875F;
            // 
            // txtAll12FullTimeCount
            // 
            this.txtAll12FullTimeCount.Height = 0.1770833F;
            this.txtAll12FullTimeCount.Left = 4.333333F;
            this.txtAll12FullTimeCount.Name = "txtAll12FullTimeCount";
            this.txtAll12FullTimeCount.Style = "text-align: right";
            this.txtAll12FullTimeCount.Text = null;
            this.txtAll12FullTimeCount.Top = 1.178333F;
            this.txtAll12FullTimeCount.Width = 1F;
            // 
            // txtAll12EmployeeCount
            // 
            this.txtAll12EmployeeCount.Height = 0.1770833F;
            this.txtAll12EmployeeCount.Left = 5.75F;
            this.txtAll12EmployeeCount.Name = "txtAll12EmployeeCount";
            this.txtAll12EmployeeCount.Style = "text-align: right";
            this.txtAll12EmployeeCount.Text = null;
            this.txtAll12EmployeeCount.Top = 1.178333F;
            this.txtAll12EmployeeCount.Width = 1F;
            // 
            // txtFullCount1
            // 
            this.txtFullCount1.Height = 0.1770833F;
            this.txtFullCount1.Left = 4.333333F;
            this.txtFullCount1.Name = "txtFullCount1";
            this.txtFullCount1.Style = "text-align: right";
            this.txtFullCount1.Text = null;
            this.txtFullCount1.Top = 1.656667F;
            this.txtFullCount1.Width = 1F;
            // 
            // txtEmployeeCount1
            // 
            this.txtEmployeeCount1.Height = 0.1770833F;
            this.txtEmployeeCount1.Left = 5.75F;
            this.txtEmployeeCount1.Name = "txtEmployeeCount1";
            this.txtEmployeeCount1.Style = "text-align: right";
            this.txtEmployeeCount1.Text = null;
            this.txtEmployeeCount1.Top = 1.656667F;
            this.txtEmployeeCount1.Width = 1F;
            // 
            // txtFullCount2
            // 
            this.txtFullCount2.Height = 0.1770833F;
            this.txtFullCount2.Left = 4.333333F;
            this.txtFullCount2.Name = "txtFullCount2";
            this.txtFullCount2.Style = "text-align: right";
            this.txtFullCount2.Text = null;
            this.txtFullCount2.Top = 2.136667F;
            this.txtFullCount2.Width = 1F;
            // 
            // txtEmployeeCount2
            // 
            this.txtEmployeeCount2.Height = 0.1770833F;
            this.txtEmployeeCount2.Left = 5.75F;
            this.txtEmployeeCount2.Name = "txtEmployeeCount2";
            this.txtEmployeeCount2.Style = "text-align: right";
            this.txtEmployeeCount2.Text = null;
            this.txtEmployeeCount2.Top = 2.136667F;
            this.txtEmployeeCount2.Width = 1F;
            // 
            // txtFullCount3
            // 
            this.txtFullCount3.Height = 0.1770833F;
            this.txtFullCount3.Left = 4.333333F;
            this.txtFullCount3.Name = "txtFullCount3";
            this.txtFullCount3.Style = "text-align: right";
            this.txtFullCount3.Text = null;
            this.txtFullCount3.Top = 2.616333F;
            this.txtFullCount3.Width = 1F;
            // 
            // txtEmployeeCount3
            // 
            this.txtEmployeeCount3.Height = 0.1770833F;
            this.txtEmployeeCount3.Left = 5.75F;
            this.txtEmployeeCount3.Name = "txtEmployeeCount3";
            this.txtEmployeeCount3.Style = "text-align: right";
            this.txtEmployeeCount3.Text = null;
            this.txtEmployeeCount3.Top = 2.616333F;
            this.txtEmployeeCount3.Width = 1F;
            // 
            // txtFullCount4
            // 
            this.txtFullCount4.Height = 0.1770833F;
            this.txtFullCount4.Left = 4.333333F;
            this.txtFullCount4.Name = "txtFullCount4";
            this.txtFullCount4.Style = "text-align: right";
            this.txtFullCount4.Text = null;
            this.txtFullCount4.Top = 3.096333F;
            this.txtFullCount4.Width = 1F;
            // 
            // txtEmployeeCount4
            // 
            this.txtEmployeeCount4.Height = 0.1770833F;
            this.txtEmployeeCount4.Left = 5.75F;
            this.txtEmployeeCount4.Name = "txtEmployeeCount4";
            this.txtEmployeeCount4.Style = "text-align: right";
            this.txtEmployeeCount4.Text = null;
            this.txtEmployeeCount4.Top = 3.096333F;
            this.txtEmployeeCount4.Width = 1F;
            // 
            // txtMinimumOfferYes5
            // 
            this.txtMinimumOfferYes5.Height = 0.1770833F;
            this.txtMinimumOfferYes5.Left = 1.9F;
            this.txtMinimumOfferYes5.Name = "txtMinimumOfferYes5";
            this.txtMinimumOfferYes5.Text = "X";
            this.txtMinimumOfferYes5.Top = 3.564F;
            this.txtMinimumOfferYes5.Width = 0.1875F;
            // 
            // txtMinimumOfferNo5
            // 
            this.txtMinimumOfferNo5.Height = 0.1770833F;
            this.txtMinimumOfferNo5.Left = 3.001F;
            this.txtMinimumOfferNo5.Name = "txtMinimumOfferNo5";
            this.txtMinimumOfferNo5.Text = "X";
            this.txtMinimumOfferNo5.Top = 3.564F;
            this.txtMinimumOfferNo5.Width = 0.1875F;
            // 
            // txtInGroup5
            // 
            this.txtInGroup5.Height = 0.1770833F;
            this.txtInGroup5.Left = 7.511F;
            this.txtInGroup5.Name = "txtInGroup5";
            this.txtInGroup5.Text = "X";
            this.txtInGroup5.Top = 3.564F;
            this.txtInGroup5.Width = 0.1875F;
            // 
            // txtMinimumOfferYes6
            // 
            this.txtMinimumOfferYes6.Height = 0.1770833F;
            this.txtMinimumOfferYes6.Left = 1.9F;
            this.txtMinimumOfferYes6.Name = "txtMinimumOfferYes6";
            this.txtMinimumOfferYes6.Text = "X";
            this.txtMinimumOfferYes6.Top = 4.034F;
            this.txtMinimumOfferYes6.Width = 0.1875F;
            // 
            // txtMinimumOfferNo6
            // 
            this.txtMinimumOfferNo6.Height = 0.1770833F;
            this.txtMinimumOfferNo6.Left = 3.001F;
            this.txtMinimumOfferNo6.Name = "txtMinimumOfferNo6";
            this.txtMinimumOfferNo6.Text = "X";
            this.txtMinimumOfferNo6.Top = 4.034F;
            this.txtMinimumOfferNo6.Width = 0.1875F;
            // 
            // txtInGroup6
            // 
            this.txtInGroup6.Height = 0.1770833F;
            this.txtInGroup6.Left = 7.511F;
            this.txtInGroup6.Name = "txtInGroup6";
            this.txtInGroup6.Text = "X";
            this.txtInGroup6.Top = 4.034F;
            this.txtInGroup6.Width = 0.1875F;
            // 
            // txtMinimumOfferYes7
            // 
            this.txtMinimumOfferYes7.Height = 0.1770833F;
            this.txtMinimumOfferYes7.Left = 1.9F;
            this.txtMinimumOfferYes7.Name = "txtMinimumOfferYes7";
            this.txtMinimumOfferYes7.Text = "X";
            this.txtMinimumOfferYes7.Top = 4.524F;
            this.txtMinimumOfferYes7.Width = 0.1875F;
            // 
            // txtMinimumOfferNo7
            // 
            this.txtMinimumOfferNo7.Height = 0.1770833F;
            this.txtMinimumOfferNo7.Left = 3.001F;
            this.txtMinimumOfferNo7.Name = "txtMinimumOfferNo7";
            this.txtMinimumOfferNo7.Text = "X";
            this.txtMinimumOfferNo7.Top = 4.524F;
            this.txtMinimumOfferNo7.Width = 0.1875F;
            // 
            // txtInGroup7
            // 
            this.txtInGroup7.Height = 0.1770833F;
            this.txtInGroup7.Left = 7.511F;
            this.txtInGroup7.Name = "txtInGroup7";
            this.txtInGroup7.Text = "X";
            this.txtInGroup7.Top = 4.524F;
            this.txtInGroup7.Width = 0.1875F;
            // 
            // txtMinimumOfferYes8
            // 
            this.txtMinimumOfferYes8.Height = 0.1770833F;
            this.txtMinimumOfferYes8.Left = 1.9F;
            this.txtMinimumOfferYes8.Name = "txtMinimumOfferYes8";
            this.txtMinimumOfferYes8.Text = "X";
            this.txtMinimumOfferYes8.Top = 4.996F;
            this.txtMinimumOfferYes8.Width = 0.1875F;
            // 
            // txtMinimumOfferNo8
            // 
            this.txtMinimumOfferNo8.Height = 0.1770833F;
            this.txtMinimumOfferNo8.Left = 3.001F;
            this.txtMinimumOfferNo8.Name = "txtMinimumOfferNo8";
            this.txtMinimumOfferNo8.Text = "X";
            this.txtMinimumOfferNo8.Top = 4.996F;
            this.txtMinimumOfferNo8.Width = 0.1875F;
            // 
            // txtInGroup8
            // 
            this.txtInGroup8.Height = 0.1770833F;
            this.txtInGroup8.Left = 7.511F;
            this.txtInGroup8.Name = "txtInGroup8";
            this.txtInGroup8.Text = "X";
            this.txtInGroup8.Top = 4.996F;
            this.txtInGroup8.Width = 0.1875F;
            // 
            // txtMinimumOfferYes9
            // 
            this.txtMinimumOfferYes9.Height = 0.1770833F;
            this.txtMinimumOfferYes9.Left = 1.9F;
            this.txtMinimumOfferYes9.Name = "txtMinimumOfferYes9";
            this.txtMinimumOfferYes9.Text = "X";
            this.txtMinimumOfferYes9.Top = 5.483F;
            this.txtMinimumOfferYes9.Width = 0.1875F;
            // 
            // txtMinimumOfferNo9
            // 
            this.txtMinimumOfferNo9.Height = 0.1770833F;
            this.txtMinimumOfferNo9.Left = 3.001F;
            this.txtMinimumOfferNo9.Name = "txtMinimumOfferNo9";
            this.txtMinimumOfferNo9.Text = "X";
            this.txtMinimumOfferNo9.Top = 5.483F;
            this.txtMinimumOfferNo9.Width = 0.1875F;
            // 
            // txtInGroup9
            // 
            this.txtInGroup9.Height = 0.1770833F;
            this.txtInGroup9.Left = 7.511F;
            this.txtInGroup9.Name = "txtInGroup9";
            this.txtInGroup9.Text = "X";
            this.txtInGroup9.Top = 5.483F;
            this.txtInGroup9.Width = 0.1875F;
            // 
            // txtFullCount5
            // 
            this.txtFullCount5.Height = 0.1770833F;
            this.txtFullCount5.Left = 4.333333F;
            this.txtFullCount5.Name = "txtFullCount5";
            this.txtFullCount5.Style = "text-align: right";
            this.txtFullCount5.Text = null;
            this.txtFullCount5.Top = 3.564333F;
            this.txtFullCount5.Width = 1F;
            // 
            // txtEmployeeCount5
            // 
            this.txtEmployeeCount5.Height = 0.1770833F;
            this.txtEmployeeCount5.Left = 5.75F;
            this.txtEmployeeCount5.Name = "txtEmployeeCount5";
            this.txtEmployeeCount5.Style = "text-align: right";
            this.txtEmployeeCount5.Text = null;
            this.txtEmployeeCount5.Top = 3.564333F;
            this.txtEmployeeCount5.Width = 1F;
            // 
            // txtFullCount6
            // 
            this.txtFullCount6.Height = 0.1770833F;
            this.txtFullCount6.Left = 4.333333F;
            this.txtFullCount6.Name = "txtFullCount6";
            this.txtFullCount6.Style = "text-align: right";
            this.txtFullCount6.Text = null;
            this.txtFullCount6.Top = 4.034333F;
            this.txtFullCount6.Width = 1F;
            // 
            // txtEmployeeCount6
            // 
            this.txtEmployeeCount6.Height = 0.1770833F;
            this.txtEmployeeCount6.Left = 5.75F;
            this.txtEmployeeCount6.Name = "txtEmployeeCount6";
            this.txtEmployeeCount6.Style = "text-align: right";
            this.txtEmployeeCount6.Text = null;
            this.txtEmployeeCount6.Top = 4.034333F;
            this.txtEmployeeCount6.Width = 1F;
            // 
            // txtFullCount7
            // 
            this.txtFullCount7.Height = 0.1770833F;
            this.txtFullCount7.Left = 4.333333F;
            this.txtFullCount7.Name = "txtFullCount7";
            this.txtFullCount7.Style = "text-align: right";
            this.txtFullCount7.Text = null;
            this.txtFullCount7.Top = 4.524333F;
            this.txtFullCount7.Width = 1F;
            // 
            // txtEmployeeCount7
            // 
            this.txtEmployeeCount7.Height = 0.1770833F;
            this.txtEmployeeCount7.Left = 5.75F;
            this.txtEmployeeCount7.Name = "txtEmployeeCount7";
            this.txtEmployeeCount7.Style = "text-align: right";
            this.txtEmployeeCount7.Text = null;
            this.txtEmployeeCount7.Top = 4.524333F;
            this.txtEmployeeCount7.Width = 1F;
            // 
            // txtFullCount8
            // 
            this.txtFullCount8.Height = 0.1770833F;
            this.txtFullCount8.Left = 4.333333F;
            this.txtFullCount8.Name = "txtFullCount8";
            this.txtFullCount8.Style = "text-align: right";
            this.txtFullCount8.Text = null;
            this.txtFullCount8.Top = 4.996333F;
            this.txtFullCount8.Width = 1F;
            // 
            // txtEmployeeCount8
            // 
            this.txtEmployeeCount8.Height = 0.1770833F;
            this.txtEmployeeCount8.Left = 5.75F;
            this.txtEmployeeCount8.Name = "txtEmployeeCount8";
            this.txtEmployeeCount8.Style = "text-align: right";
            this.txtEmployeeCount8.Text = null;
            this.txtEmployeeCount8.Top = 4.996333F;
            this.txtEmployeeCount8.Width = 1F;
            // 
            // txtFullCount9
            // 
            this.txtFullCount9.Height = 0.1770833F;
            this.txtFullCount9.Left = 4.333333F;
            this.txtFullCount9.Name = "txtFullCount9";
            this.txtFullCount9.Style = "text-align: right";
            this.txtFullCount9.Text = null;
            this.txtFullCount9.Top = 5.483334F;
            this.txtFullCount9.Width = 1F;
            // 
            // txtEmployeeCount9
            // 
            this.txtEmployeeCount9.Height = 0.1770833F;
            this.txtEmployeeCount9.Left = 5.75F;
            this.txtEmployeeCount9.Name = "txtEmployeeCount9";
            this.txtEmployeeCount9.Style = "text-align: right";
            this.txtEmployeeCount9.Text = null;
            this.txtEmployeeCount9.Top = 5.483334F;
            this.txtEmployeeCount9.Width = 1F;
            // 
            // txtMinimumOfferYes10
            // 
            this.txtMinimumOfferYes10.Height = 0.1770833F;
            this.txtMinimumOfferYes10.Left = 1.9F;
            this.txtMinimumOfferYes10.Name = "txtMinimumOfferYes10";
            this.txtMinimumOfferYes10.Text = "X";
            this.txtMinimumOfferYes10.Top = 5.962F;
            this.txtMinimumOfferYes10.Width = 0.1875F;
            // 
            // txtMinimumOfferNo10
            // 
            this.txtMinimumOfferNo10.Height = 0.1770833F;
            this.txtMinimumOfferNo10.Left = 3.001F;
            this.txtMinimumOfferNo10.Name = "txtMinimumOfferNo10";
            this.txtMinimumOfferNo10.Text = "X";
            this.txtMinimumOfferNo10.Top = 5.962F;
            this.txtMinimumOfferNo10.Width = 0.1875F;
            // 
            // txtInGroup10
            // 
            this.txtInGroup10.Height = 0.1770833F;
            this.txtInGroup10.Left = 7.511F;
            this.txtInGroup10.Name = "txtInGroup10";
            this.txtInGroup10.Text = "X";
            this.txtInGroup10.Top = 5.962F;
            this.txtInGroup10.Width = 0.1875F;
            // 
            // txtMinimumOfferYes11
            // 
            this.txtMinimumOfferYes11.Height = 0.1770833F;
            this.txtMinimumOfferYes11.Left = 1.9F;
            this.txtMinimumOfferYes11.Name = "txtMinimumOfferYes11";
            this.txtMinimumOfferYes11.Text = "X";
            this.txtMinimumOfferYes11.Top = 6.442F;
            this.txtMinimumOfferYes11.Width = 0.1875F;
            // 
            // txtMinimumOfferNo11
            // 
            this.txtMinimumOfferNo11.Height = 0.1770833F;
            this.txtMinimumOfferNo11.Left = 3.001F;
            this.txtMinimumOfferNo11.Name = "txtMinimumOfferNo11";
            this.txtMinimumOfferNo11.Text = "X";
            this.txtMinimumOfferNo11.Top = 6.442F;
            this.txtMinimumOfferNo11.Width = 0.1875F;
            // 
            // txtInGroup11
            // 
            this.txtInGroup11.Height = 0.1770833F;
            this.txtInGroup11.Left = 7.511F;
            this.txtInGroup11.Name = "txtInGroup11";
            this.txtInGroup11.Text = "X";
            this.txtInGroup11.Top = 6.442F;
            this.txtInGroup11.Width = 0.1875F;
            // 
            // txtMinimumOfferYes12
            // 
            this.txtMinimumOfferYes12.Height = 0.1770833F;
            this.txtMinimumOfferYes12.Left = 1.9F;
            this.txtMinimumOfferYes12.Name = "txtMinimumOfferYes12";
            this.txtMinimumOfferYes12.Text = "X";
            this.txtMinimumOfferYes12.Top = 6.932F;
            this.txtMinimumOfferYes12.Width = 0.1875F;
            // 
            // txtMinimumOfferNo12
            // 
            this.txtMinimumOfferNo12.Height = 0.1770833F;
            this.txtMinimumOfferNo12.Left = 3.001F;
            this.txtMinimumOfferNo12.Name = "txtMinimumOfferNo12";
            this.txtMinimumOfferNo12.Text = "X";
            this.txtMinimumOfferNo12.Top = 6.932F;
            this.txtMinimumOfferNo12.Width = 0.1875F;
            // 
            // txtInGroup12
            // 
            this.txtInGroup12.Height = 0.1770833F;
            this.txtInGroup12.Left = 7.51111F;
            this.txtInGroup12.Name = "txtInGroup12";
            this.txtInGroup12.Text = "X";
            this.txtInGroup12.Top = 6.932F;
            this.txtInGroup12.Width = 0.1875F;
            // 
            // txtFullCount10
            // 
            this.txtFullCount10.Height = 0.1770833F;
            this.txtFullCount10.Left = 4.333333F;
            this.txtFullCount10.Name = "txtFullCount10";
            this.txtFullCount10.Style = "text-align: right";
            this.txtFullCount10.Text = null;
            this.txtFullCount10.Top = 5.962333F;
            this.txtFullCount10.Width = 1F;
            // 
            // txtEmployeeCount10
            // 
            this.txtEmployeeCount10.Height = 0.1770833F;
            this.txtEmployeeCount10.Left = 5.75F;
            this.txtEmployeeCount10.Name = "txtEmployeeCount10";
            this.txtEmployeeCount10.Style = "text-align: right";
            this.txtEmployeeCount10.Text = null;
            this.txtEmployeeCount10.Top = 5.962333F;
            this.txtEmployeeCount10.Width = 1F;
            // 
            // txtFullCount11
            // 
            this.txtFullCount11.Height = 0.1770833F;
            this.txtFullCount11.Left = 4.333333F;
            this.txtFullCount11.Name = "txtFullCount11";
            this.txtFullCount11.Style = "text-align: right";
            this.txtFullCount11.Text = null;
            this.txtFullCount11.Top = 6.442333F;
            this.txtFullCount11.Width = 1F;
            // 
            // txtEmployeeCount11
            // 
            this.txtEmployeeCount11.Height = 0.1770833F;
            this.txtEmployeeCount11.Left = 5.75F;
            this.txtEmployeeCount11.Name = "txtEmployeeCount11";
            this.txtEmployeeCount11.Style = "text-align: right";
            this.txtEmployeeCount11.Text = null;
            this.txtEmployeeCount11.Top = 6.442333F;
            this.txtEmployeeCount11.Width = 1F;
            // 
            // txtFullCount12
            // 
            this.txtFullCount12.Height = 0.1770833F;
            this.txtFullCount12.Left = 4.333333F;
            this.txtFullCount12.Name = "txtFullCount12";
            this.txtFullCount12.Style = "text-align: right";
            this.txtFullCount12.Text = null;
            this.txtFullCount12.Top = 6.932333F;
            this.txtFullCount12.Width = 1F;
            // 
            // txtEmployeeCount12
            // 
            this.txtEmployeeCount12.Height = 0.1770833F;
            this.txtEmployeeCount12.Left = 5.75F;
            this.txtEmployeeCount12.Name = "txtEmployeeCount12";
            this.txtEmployeeCount12.Style = "text-align: right";
            this.txtEmployeeCount12.Text = null;
            this.txtEmployeeCount12.Top = 6.932333F;
            this.txtEmployeeCount12.Width = 1F;
            // 
            // txtAll12Months4980H
            // 
            this.txtAll12Months4980H.Height = 0.1770833F;
            this.txtAll12Months4980H.Left = 8.25F;
            this.txtAll12Months4980H.Name = "txtAll12Months4980H";
            this.txtAll12Months4980H.Style = "text-align: left";
            this.txtAll12Months4980H.Text = null;
            this.txtAll12Months4980H.Top = 1.178333F;
            this.txtAll12Months4980H.Width = 1F;
            // 
            // txt4980H1
            // 
            this.txt4980H1.Height = 0.1770833F;
            this.txt4980H1.Left = 8.25F;
            this.txt4980H1.Name = "txt4980H1";
            this.txt4980H1.Style = "text-align: left";
            this.txt4980H1.Text = null;
            this.txt4980H1.Top = 1.656667F;
            this.txt4980H1.Width = 1F;
            // 
            // txt4980H2
            // 
            this.txt4980H2.Height = 0.1770833F;
            this.txt4980H2.Left = 8.25F;
            this.txt4980H2.Name = "txt4980H2";
            this.txt4980H2.Style = "text-align: left";
            this.txt4980H2.Text = null;
            this.txt4980H2.Top = 2.136667F;
            this.txt4980H2.Width = 1F;
            // 
            // txt4980H3
            // 
            this.txt4980H3.Height = 0.1770833F;
            this.txt4980H3.Left = 8.25F;
            this.txt4980H3.Name = "txt4980H3";
            this.txt4980H3.Style = "text-align: left";
            this.txt4980H3.Text = null;
            this.txt4980H3.Top = 2.616333F;
            this.txt4980H3.Width = 1F;
            // 
            // txt4980H4
            // 
            this.txt4980H4.Height = 0.1770833F;
            this.txt4980H4.Left = 8.25F;
            this.txt4980H4.Name = "txt4980H4";
            this.txt4980H4.Style = "text-align: left";
            this.txt4980H4.Text = null;
            this.txt4980H4.Top = 3.096333F;
            this.txt4980H4.Width = 1F;
            // 
            // txt4980H5
            // 
            this.txt4980H5.Height = 0.1770833F;
            this.txt4980H5.Left = 8.25F;
            this.txt4980H5.Name = "txt4980H5";
            this.txt4980H5.Style = "text-align: left";
            this.txt4980H5.Text = null;
            this.txt4980H5.Top = 3.564333F;
            this.txt4980H5.Width = 1F;
            // 
            // txt4980H6
            // 
            this.txt4980H6.Height = 0.1770833F;
            this.txt4980H6.Left = 8.25F;
            this.txt4980H6.Name = "txt4980H6";
            this.txt4980H6.Style = "text-align: left";
            this.txt4980H6.Text = null;
            this.txt4980H6.Top = 4.034333F;
            this.txt4980H6.Width = 1F;
            // 
            // txt4980H7
            // 
            this.txt4980H7.Height = 0.1770833F;
            this.txt4980H7.Left = 8.25F;
            this.txt4980H7.Name = "txt4980H7";
            this.txt4980H7.Style = "text-align: left";
            this.txt4980H7.Text = null;
            this.txt4980H7.Top = 4.524333F;
            this.txt4980H7.Width = 1F;
            // 
            // txt4980H8
            // 
            this.txt4980H8.Height = 0.1770833F;
            this.txt4980H8.Left = 8.25F;
            this.txt4980H8.Name = "txt4980H8";
            this.txt4980H8.Style = "text-align: left";
            this.txt4980H8.Text = null;
            this.txt4980H8.Top = 4.996333F;
            this.txt4980H8.Width = 1F;
            // 
            // txt4980H9
            // 
            this.txt4980H9.Height = 0.1770833F;
            this.txt4980H9.Left = 8.25F;
            this.txt4980H9.Name = "txt4980H9";
            this.txt4980H9.Style = "text-align: left";
            this.txt4980H9.Text = null;
            this.txt4980H9.Top = 5.483334F;
            this.txt4980H9.Width = 1F;
            // 
            // txt4980H10
            // 
            this.txt4980H10.Height = 0.1770833F;
            this.txt4980H10.Left = 8.25F;
            this.txt4980H10.Name = "txt4980H10";
            this.txt4980H10.Style = "text-align: left";
            this.txt4980H10.Text = null;
            this.txt4980H10.Top = 5.962333F;
            this.txt4980H10.Width = 1F;
            // 
            // txt4980H11
            // 
            this.txt4980H11.Height = 0.1770833F;
            this.txt4980H11.Left = 8.25F;
            this.txt4980H11.Name = "txt4980H11";
            this.txt4980H11.Style = "text-align: left";
            this.txt4980H11.Text = null;
            this.txt4980H11.Top = 6.442333F;
            this.txt4980H11.Width = 1F;
            // 
            // txt4980H12
            // 
            this.txt4980H12.Height = 0.1770833F;
            this.txt4980H12.Left = 8.25F;
            this.txt4980H12.Name = "txt4980H12";
            this.txt4980H12.Style = "text-align: left";
            this.txt4980H12.Text = null;
            this.txt4980H12.Top = 6.932333F;
            this.txt4980H12.Width = 1F;
            // 
            // srpt1094C2016MonthlyDetail
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.90625F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12MinimumOfferYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12MinimumOfferNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12InGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12FullTimeCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12EmployeeCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferYes12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinimumOfferNo12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullCount12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCount12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll12Months4980H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4980H12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12MinimumOfferYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12MinimumOfferNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12InGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12FullTimeCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12EmployeeCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferYes12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinimumOfferNo12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInGroup12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullCount12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCount12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Months4980H;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4980H12;
	}
}
