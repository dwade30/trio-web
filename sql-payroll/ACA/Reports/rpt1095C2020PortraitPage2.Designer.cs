﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016PortraitPage2.
	/// </summary>
	partial class rpt1095C2020PortraitPage2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2020PortraitPage2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLastName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCoveredName1,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtCoveredName7,
            this.txtCoveredSSN7,
            this.txtCoveredDOB7,
            this.txtCoveredAll12_7,
            this.txtCoveredMonth1_7,
            this.txtCoveredMonth2_7,
            this.txtCoveredMonth3_7,
            this.txtCoveredMonth4_7,
            this.txtCoveredMonth5_7,
            this.txtCoveredMonth6_7,
            this.txtCoveredMonth7_7,
            this.txtCoveredMonth8_7,
            this.txtCoveredMonth9_7,
            this.txtCoveredMonth10_7,
            this.txtCoveredMonth11_7,
            this.txtCoveredMonth12_7,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.txtCoveredMiddle7,
            this.txtCoveredLastName1,
            this.txtCoveredLastName2,
            this.txtCoveredLastName3,
            this.txtCoveredLastName4,
            this.txtCoveredLastName5,
            this.txtCoveredLastName6,
            this.txtCoveredLastName7,
            this.txtCoveredIndividuals});
            this.Detail.Height = 9.927083F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.1666667F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Style = "font-size: 8.5pt";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 1.083333F;
            this.txtCoveredName1.Width = 0.625F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 1.895833F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Style = "font-size: 8.5pt";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 1.083333F;
            this.txtCoveredSSN1.Width = 0.8333333F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 2.802F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Style = "font-size: 8.5pt";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 1.083F;
            this.txtCoveredDOB1.Width = 0.75F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.167F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Style = "font-size: 8.5pt";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 1.448F;
            this.txtCoveredName2.Width = 0.625F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 1.896166F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Style = "font-size: 8.5pt";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 1.448F;
            this.txtCoveredSSN2.Width = 0.8333333F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 2.802333F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Style = "font-size: 8.5pt";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 1.447667F;
            this.txtCoveredDOB2.Width = 0.75F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.167F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Style = "font-size: 8.5pt";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 1.813F;
            this.txtCoveredName3.Width = 0.625F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 1.896166F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Style = "font-size: 8.5pt";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 1.813F;
            this.txtCoveredSSN3.Width = 0.8333333F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 2.802333F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Style = "font-size: 8.5pt";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 1.812667F;
            this.txtCoveredDOB3.Width = 0.75F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.167F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Style = "font-size: 8.5pt";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 2.178F;
            this.txtCoveredName4.Width = 0.625F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 1.896166F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Style = "font-size: 8.5pt";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 2.178F;
            this.txtCoveredSSN4.Width = 0.8333333F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 2.802333F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Style = "font-size: 8.5pt";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 2.177667F;
            this.txtCoveredDOB4.Width = 0.75F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.167F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Style = "font-size: 8.5pt";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 2.543F;
            this.txtCoveredName5.Width = 0.625F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 1.896166F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Style = "font-size: 8.5pt";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 2.543F;
            this.txtCoveredSSN5.Width = 0.8333333F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 2.802333F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Style = "font-size: 8.5pt";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 2.542667F;
            this.txtCoveredDOB5.Width = 0.75F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.167F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Style = "font-size: 8.5pt";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 2.908F;
            this.txtCoveredName6.Width = 0.625F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 1.896166F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Style = "font-size: 8.5pt";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 2.908F;
            this.txtCoveredSSN6.Width = 0.8333333F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 2.802333F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Style = "font-size: 8.5pt";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 2.907667F;
            this.txtCoveredDOB6.Width = 0.75F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 3.625F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = null;
            this.txtCoveredAll12_1.Top = 1.083333F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 3.979F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = null;
            this.txtCoveredMonth1_1.Top = 1.083F;
            this.txtCoveredMonth1_1.Width = 0.25F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 4.281F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = null;
            this.txtCoveredMonth2_1.Top = 1.083F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 4.594F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = null;
            this.txtCoveredMonth3_1.Top = 1.083F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 4.895833F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = null;
            this.txtCoveredMonth4_1.Top = 1.083333F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 5.177083F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = null;
            this.txtCoveredMonth5_1.Top = 1.083333F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 5.469F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = null;
            this.txtCoveredMonth6_1.Top = 1.083F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 5.791666F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = null;
            this.txtCoveredMonth7_1.Top = 1.083333F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 6.094F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = null;
            this.txtCoveredMonth8_1.Top = 1.083F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 6.385416F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = null;
            this.txtCoveredMonth9_1.Top = 1.083333F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 6.677083F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = null;
            this.txtCoveredMonth10_1.Top = 1.083333F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 6.979001F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = null;
            this.txtCoveredMonth11_1.Top = 1.083F;
            this.txtCoveredMonth11_1.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 7.270833F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = null;
            this.txtCoveredMonth12_1.Top = 1.083333F;
            this.txtCoveredMonth12_1.Width = 0.1875F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 3.625333F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = null;
            this.txtCoveredAll12_2.Top = 1.448F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 3.979333F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = null;
            this.txtCoveredMonth1_2.Top = 1.447667F;
            this.txtCoveredMonth1_2.Width = 0.25F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 4.281333F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = null;
            this.txtCoveredMonth2_2.Top = 1.447667F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 4.594334F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = null;
            this.txtCoveredMonth3_2.Top = 1.447667F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 4.896166F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = null;
            this.txtCoveredMonth4_2.Top = 1.448F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 5.177416F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = null;
            this.txtCoveredMonth5_2.Top = 1.448F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 5.469334F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = null;
            this.txtCoveredMonth6_2.Top = 1.447667F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 5.791999F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = null;
            this.txtCoveredMonth7_2.Top = 1.448F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 6.094334F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = null;
            this.txtCoveredMonth8_2.Top = 1.447667F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 6.385749F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = null;
            this.txtCoveredMonth9_2.Top = 1.448F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 6.677416F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = null;
            this.txtCoveredMonth10_2.Top = 1.448F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 6.979334F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = null;
            this.txtCoveredMonth11_2.Top = 1.447667F;
            this.txtCoveredMonth11_2.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 7.271166F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = null;
            this.txtCoveredMonth12_2.Top = 1.448F;
            this.txtCoveredMonth12_2.Width = 0.1875F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 3.625333F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = null;
            this.txtCoveredAll12_3.Top = 1.813F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 3.979333F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = null;
            this.txtCoveredMonth1_3.Top = 1.812667F;
            this.txtCoveredMonth1_3.Width = 0.25F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 4.281333F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = null;
            this.txtCoveredMonth2_3.Top = 1.812667F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 4.594334F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = null;
            this.txtCoveredMonth3_3.Top = 1.812667F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 4.896166F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = null;
            this.txtCoveredMonth4_3.Top = 1.813F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 5.177416F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = null;
            this.txtCoveredMonth5_3.Top = 1.813F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 5.469334F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = null;
            this.txtCoveredMonth6_3.Top = 1.812667F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 5.791999F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = null;
            this.txtCoveredMonth7_3.Top = 1.813F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 6.094334F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = null;
            this.txtCoveredMonth8_3.Top = 1.812667F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 6.385749F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = null;
            this.txtCoveredMonth9_3.Top = 1.813F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 6.677416F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = null;
            this.txtCoveredMonth10_3.Top = 1.813F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 6.979334F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = null;
            this.txtCoveredMonth11_3.Top = 1.812667F;
            this.txtCoveredMonth11_3.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 7.271166F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = null;
            this.txtCoveredMonth12_3.Top = 1.813F;
            this.txtCoveredMonth12_3.Width = 0.1875F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 3.625333F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = null;
            this.txtCoveredAll12_4.Top = 2.178F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 3.979333F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = null;
            this.txtCoveredMonth1_4.Top = 2.177667F;
            this.txtCoveredMonth1_4.Width = 0.25F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 4.281333F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = null;
            this.txtCoveredMonth2_4.Top = 2.177667F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 4.594334F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = null;
            this.txtCoveredMonth3_4.Top = 2.177667F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 4.896166F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = null;
            this.txtCoveredMonth4_4.Top = 2.178F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 5.177416F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = null;
            this.txtCoveredMonth5_4.Top = 2.178F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 5.469334F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = null;
            this.txtCoveredMonth6_4.Top = 2.177667F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 5.791999F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = null;
            this.txtCoveredMonth7_4.Top = 2.178F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 6.094334F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = null;
            this.txtCoveredMonth8_4.Top = 2.177667F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 6.385749F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = null;
            this.txtCoveredMonth9_4.Top = 2.178F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 6.677416F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = null;
            this.txtCoveredMonth10_4.Top = 2.178F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 6.979334F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = null;
            this.txtCoveredMonth11_4.Top = 2.177667F;
            this.txtCoveredMonth11_4.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 7.271166F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = null;
            this.txtCoveredMonth12_4.Top = 2.178F;
            this.txtCoveredMonth12_4.Width = 0.1875F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 3.625333F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = null;
            this.txtCoveredAll12_5.Top = 2.543F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 3.979333F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = null;
            this.txtCoveredMonth1_5.Top = 2.542667F;
            this.txtCoveredMonth1_5.Width = 0.25F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 4.281333F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = null;
            this.txtCoveredMonth2_5.Top = 2.542667F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 4.594334F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = null;
            this.txtCoveredMonth3_5.Top = 2.542667F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 4.896166F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = null;
            this.txtCoveredMonth4_5.Top = 2.543F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 5.177416F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = null;
            this.txtCoveredMonth5_5.Top = 2.543F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 5.469334F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = null;
            this.txtCoveredMonth6_5.Top = 2.542667F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 5.791999F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = null;
            this.txtCoveredMonth7_5.Top = 2.543F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 6.094334F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = null;
            this.txtCoveredMonth8_5.Top = 2.542667F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 6.385749F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = null;
            this.txtCoveredMonth9_5.Top = 2.543F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 6.677416F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = null;
            this.txtCoveredMonth10_5.Top = 2.543F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 6.979334F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = null;
            this.txtCoveredMonth11_5.Top = 2.542667F;
            this.txtCoveredMonth11_5.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 7.271166F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = null;
            this.txtCoveredMonth12_5.Top = 2.543F;
            this.txtCoveredMonth12_5.Width = 0.1875F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 3.625333F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = null;
            this.txtCoveredAll12_6.Top = 2.908F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 3.979333F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = null;
            this.txtCoveredMonth1_6.Top = 2.907667F;
            this.txtCoveredMonth1_6.Width = 0.25F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 4.281333F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = null;
            this.txtCoveredMonth2_6.Top = 2.907667F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 4.594334F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = null;
            this.txtCoveredMonth3_6.Top = 2.907667F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 4.896166F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = null;
            this.txtCoveredMonth4_6.Top = 2.908F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 5.177416F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = null;
            this.txtCoveredMonth5_6.Top = 2.908F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 5.469334F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = null;
            this.txtCoveredMonth6_6.Top = 2.907667F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 5.791999F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = null;
            this.txtCoveredMonth7_6.Top = 2.908F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 6.094334F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = null;
            this.txtCoveredMonth8_6.Top = 2.907667F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 6.385749F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = null;
            this.txtCoveredMonth9_6.Top = 2.908F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 6.677416F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = null;
            this.txtCoveredMonth10_6.Top = 2.908F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 6.979334F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = null;
            this.txtCoveredMonth11_6.Top = 2.907667F;
            this.txtCoveredMonth11_6.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 7.271166F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = null;
            this.txtCoveredMonth12_6.Top = 2.908F;
            this.txtCoveredMonth12_6.Width = 0.1875F;
            // 
            // txtCoveredName7
            // 
            this.txtCoveredName7.Height = 0.1770833F;
            this.txtCoveredName7.Left = 0.167F;
            this.txtCoveredName7.Name = "txtCoveredName7";
            this.txtCoveredName7.Style = "font-size: 8.5pt";
            this.txtCoveredName7.Text = null;
            this.txtCoveredName7.Top = 3.273F;
            this.txtCoveredName7.Width = 0.625F;
            // 
            // txtCoveredSSN7
            // 
            this.txtCoveredSSN7.Height = 0.1770833F;
            this.txtCoveredSSN7.Left = 1.896166F;
            this.txtCoveredSSN7.Name = "txtCoveredSSN7";
            this.txtCoveredSSN7.Style = "font-size: 8.5pt";
            this.txtCoveredSSN7.Text = null;
            this.txtCoveredSSN7.Top = 3.273F;
            this.txtCoveredSSN7.Width = 0.8333333F;
            // 
            // txtCoveredDOB7
            // 
            this.txtCoveredDOB7.Height = 0.1770833F;
            this.txtCoveredDOB7.Left = 2.802333F;
            this.txtCoveredDOB7.Name = "txtCoveredDOB7";
            this.txtCoveredDOB7.Style = "font-size: 8.5pt";
            this.txtCoveredDOB7.Text = null;
            this.txtCoveredDOB7.Top = 3.272667F;
            this.txtCoveredDOB7.Width = 0.75F;
            // 
            // txtCoveredAll12_7
            // 
            this.txtCoveredAll12_7.Height = 0.1770833F;
            this.txtCoveredAll12_7.Left = 3.625333F;
            this.txtCoveredAll12_7.Name = "txtCoveredAll12_7";
            this.txtCoveredAll12_7.Text = null;
            this.txtCoveredAll12_7.Top = 3.273F;
            this.txtCoveredAll12_7.Width = 0.25F;
            // 
            // txtCoveredMonth1_7
            // 
            this.txtCoveredMonth1_7.Height = 0.1770833F;
            this.txtCoveredMonth1_7.Left = 3.979333F;
            this.txtCoveredMonth1_7.Name = "txtCoveredMonth1_7";
            this.txtCoveredMonth1_7.Text = null;
            this.txtCoveredMonth1_7.Top = 3.272667F;
            this.txtCoveredMonth1_7.Width = 0.25F;
            // 
            // txtCoveredMonth2_7
            // 
            this.txtCoveredMonth2_7.Height = 0.1770833F;
            this.txtCoveredMonth2_7.Left = 4.281333F;
            this.txtCoveredMonth2_7.Name = "txtCoveredMonth2_7";
            this.txtCoveredMonth2_7.Text = null;
            this.txtCoveredMonth2_7.Top = 3.272667F;
            this.txtCoveredMonth2_7.Width = 0.25F;
            // 
            // txtCoveredMonth3_7
            // 
            this.txtCoveredMonth3_7.Height = 0.1770833F;
            this.txtCoveredMonth3_7.Left = 4.594334F;
            this.txtCoveredMonth3_7.Name = "txtCoveredMonth3_7";
            this.txtCoveredMonth3_7.Text = null;
            this.txtCoveredMonth3_7.Top = 3.272667F;
            this.txtCoveredMonth3_7.Width = 0.25F;
            // 
            // txtCoveredMonth4_7
            // 
            this.txtCoveredMonth4_7.Height = 0.1770833F;
            this.txtCoveredMonth4_7.Left = 4.896166F;
            this.txtCoveredMonth4_7.Name = "txtCoveredMonth4_7";
            this.txtCoveredMonth4_7.Text = null;
            this.txtCoveredMonth4_7.Top = 3.273F;
            this.txtCoveredMonth4_7.Width = 0.25F;
            // 
            // txtCoveredMonth5_7
            // 
            this.txtCoveredMonth5_7.Height = 0.1770833F;
            this.txtCoveredMonth5_7.Left = 5.177416F;
            this.txtCoveredMonth5_7.Name = "txtCoveredMonth5_7";
            this.txtCoveredMonth5_7.Text = null;
            this.txtCoveredMonth5_7.Top = 3.273F;
            this.txtCoveredMonth5_7.Width = 0.25F;
            // 
            // txtCoveredMonth6_7
            // 
            this.txtCoveredMonth6_7.Height = 0.1770833F;
            this.txtCoveredMonth6_7.Left = 5.469334F;
            this.txtCoveredMonth6_7.Name = "txtCoveredMonth6_7";
            this.txtCoveredMonth6_7.Text = null;
            this.txtCoveredMonth6_7.Top = 3.272667F;
            this.txtCoveredMonth6_7.Width = 0.25F;
            // 
            // txtCoveredMonth7_7
            // 
            this.txtCoveredMonth7_7.Height = 0.1770833F;
            this.txtCoveredMonth7_7.Left = 5.791999F;
            this.txtCoveredMonth7_7.Name = "txtCoveredMonth7_7";
            this.txtCoveredMonth7_7.Text = null;
            this.txtCoveredMonth7_7.Top = 3.273F;
            this.txtCoveredMonth7_7.Width = 0.25F;
            // 
            // txtCoveredMonth8_7
            // 
            this.txtCoveredMonth8_7.Height = 0.1770833F;
            this.txtCoveredMonth8_7.Left = 6.094334F;
            this.txtCoveredMonth8_7.Name = "txtCoveredMonth8_7";
            this.txtCoveredMonth8_7.Text = null;
            this.txtCoveredMonth8_7.Top = 3.272667F;
            this.txtCoveredMonth8_7.Width = 0.25F;
            // 
            // txtCoveredMonth9_7
            // 
            this.txtCoveredMonth9_7.Height = 0.1770833F;
            this.txtCoveredMonth9_7.Left = 6.385749F;
            this.txtCoveredMonth9_7.Name = "txtCoveredMonth9_7";
            this.txtCoveredMonth9_7.Text = null;
            this.txtCoveredMonth9_7.Top = 3.273F;
            this.txtCoveredMonth9_7.Width = 0.25F;
            // 
            // txtCoveredMonth10_7
            // 
            this.txtCoveredMonth10_7.Height = 0.1770833F;
            this.txtCoveredMonth10_7.Left = 6.677416F;
            this.txtCoveredMonth10_7.Name = "txtCoveredMonth10_7";
            this.txtCoveredMonth10_7.Text = null;
            this.txtCoveredMonth10_7.Top = 3.273F;
            this.txtCoveredMonth10_7.Width = 0.25F;
            // 
            // txtCoveredMonth11_7
            // 
            this.txtCoveredMonth11_7.Height = 0.1770833F;
            this.txtCoveredMonth11_7.Left = 6.979334F;
            this.txtCoveredMonth11_7.Name = "txtCoveredMonth11_7";
            this.txtCoveredMonth11_7.Text = null;
            this.txtCoveredMonth11_7.Top = 3.272667F;
            this.txtCoveredMonth11_7.Width = 0.1666667F;
            // 
            // txtCoveredMonth12_7
            // 
            this.txtCoveredMonth12_7.Height = 0.1770833F;
            this.txtCoveredMonth12_7.Left = 7.271166F;
            this.txtCoveredMonth12_7.Name = "txtCoveredMonth12_7";
            this.txtCoveredMonth12_7.Text = null;
            this.txtCoveredMonth12_7.Top = 3.273F;
            this.txtCoveredMonth12_7.Width = 0.1875F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 0.875F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 1.083333F;
            this.txtCoveredMiddle1.Width = 0.1875F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 0.8753333F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 1.437584F;
            this.txtCoveredMiddle2.Width = 0.1875F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 0.8753333F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 1.802584F;
            this.txtCoveredMiddle3.Width = 0.1875F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 0.8753333F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 2.167584F;
            this.txtCoveredMiddle4.Width = 0.1875F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 0.8753333F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 2.532584F;
            this.txtCoveredMiddle5.Width = 0.1875F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 0.8753333F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 2.897584F;
            this.txtCoveredMiddle6.Width = 0.1875F;
            // 
            // txtCoveredMiddle7
            // 
            this.txtCoveredMiddle7.Height = 0.1770833F;
            this.txtCoveredMiddle7.Left = 0.8753333F;
            this.txtCoveredMiddle7.Name = "txtCoveredMiddle7";
            this.txtCoveredMiddle7.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle7.Text = null;
            this.txtCoveredMiddle7.Top = 3.262584F;
            this.txtCoveredMiddle7.Width = 0.1875F;
            // 
            // txtCoveredLastName1
            // 
            this.txtCoveredLastName1.Height = 0.1770833F;
            this.txtCoveredLastName1.Left = 1.125F;
            this.txtCoveredLastName1.Name = "txtCoveredLastName1";
            this.txtCoveredLastName1.Style = "font-size: 8.5pt";
            this.txtCoveredLastName1.Text = null;
            this.txtCoveredLastName1.Top = 1.083333F;
            this.txtCoveredLastName1.Width = 0.625F;
            // 
            // txtCoveredLastName2
            // 
            this.txtCoveredLastName2.Height = 0.1770833F;
            this.txtCoveredLastName2.Left = 1.125333F;
            this.txtCoveredLastName2.Name = "txtCoveredLastName2";
            this.txtCoveredLastName2.Style = "font-size: 8.5pt";
            this.txtCoveredLastName2.Text = null;
            this.txtCoveredLastName2.Top = 1.437584F;
            this.txtCoveredLastName2.Width = 0.625F;
            // 
            // txtCoveredLastName3
            // 
            this.txtCoveredLastName3.Height = 0.1770833F;
            this.txtCoveredLastName3.Left = 1.125333F;
            this.txtCoveredLastName3.Name = "txtCoveredLastName3";
            this.txtCoveredLastName3.Style = "font-size: 8.5pt";
            this.txtCoveredLastName3.Text = null;
            this.txtCoveredLastName3.Top = 1.802584F;
            this.txtCoveredLastName3.Width = 0.625F;
            // 
            // txtCoveredLastName4
            // 
            this.txtCoveredLastName4.Height = 0.1770833F;
            this.txtCoveredLastName4.Left = 1.125333F;
            this.txtCoveredLastName4.Name = "txtCoveredLastName4";
            this.txtCoveredLastName4.Style = "font-size: 8.5pt";
            this.txtCoveredLastName4.Text = null;
            this.txtCoveredLastName4.Top = 2.167584F;
            this.txtCoveredLastName4.Width = 0.625F;
            // 
            // txtCoveredLastName5
            // 
            this.txtCoveredLastName5.Height = 0.1770833F;
            this.txtCoveredLastName5.Left = 1.125333F;
            this.txtCoveredLastName5.Name = "txtCoveredLastName5";
            this.txtCoveredLastName5.Style = "font-size: 8.5pt";
            this.txtCoveredLastName5.Text = null;
            this.txtCoveredLastName5.Top = 2.532584F;
            this.txtCoveredLastName5.Width = 0.625F;
            // 
            // txtCoveredLastName6
            // 
            this.txtCoveredLastName6.Height = 0.1770833F;
            this.txtCoveredLastName6.Left = 1.125333F;
            this.txtCoveredLastName6.Name = "txtCoveredLastName6";
            this.txtCoveredLastName6.Style = "font-size: 8.5pt";
            this.txtCoveredLastName6.Text = null;
            this.txtCoveredLastName6.Top = 2.897584F;
            this.txtCoveredLastName6.Width = 0.625F;
            // 
            // txtCoveredLastName7
            // 
            this.txtCoveredLastName7.Height = 0.1770833F;
            this.txtCoveredLastName7.Left = 1.125333F;
            this.txtCoveredLastName7.Name = "txtCoveredLastName7";
            this.txtCoveredLastName7.Style = "font-size: 8.5pt";
            this.txtCoveredLastName7.Text = null;
            this.txtCoveredLastName7.Top = 3.262584F;
            this.txtCoveredLastName7.Width = 0.625F;
            // 
            // txtCoveredIndividuals
            // 
            this.txtCoveredIndividuals.Height = 0.1770833F;
            this.txtCoveredIndividuals.Left = 6.804F;
            this.txtCoveredIndividuals.Name = "txtCoveredIndividuals";
            this.txtCoveredIndividuals.Text = null;
            this.txtCoveredIndividuals.Top = 0.545F;
            this.txtCoveredIndividuals.Width = 0.1666667F;
            // 
            // rpt1095C2020PortraitPage2
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.479166F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLastName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLastName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredIndividuals;
    }
}
