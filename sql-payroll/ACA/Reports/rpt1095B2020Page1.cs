﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2016Page1.
	/// </summary>
	public partial class rpt1095B2020Page1 : FCSectionReport
	{
		public rpt1095B2020Page1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "1095-B";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt1095B2020Page1 InstancePtr
		{
			get
			{
				return (rpt1095B2020Page1)Sys.GetInstance(typeof(rpt1095B2020Page1));
			}
		}

		protected rpt1095B2020Page1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1095B2016Page1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolTestPrint;
		private bool boolMaskSSNs;
		private bool boolFirstRecord;
		private c1095BReport theReport;

		public void Init(ref c1095BReport reportObject, bool modalDialog)
		{
			theReport = reportObject;
			boolTestPrint = theReport.TestPrint;
			boolMaskSSNs = theReport.MaskSSNs;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", true, showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolTestPrint)
			{
				eArgs.EOF = !theReport.ListOfForms.IsCurrent();
			}
			else
			{
				eArgs.EOF = !boolFirstRecord;
				boolFirstRecord = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (!theReport.TestPrint)
			{
				theReport.ListOfForms.MoveFirst();
			}
			boolFirstRecord = true;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void ClearFields()
		{
			int x;
			int intLine;
			txtAddress.Text = "";
			txtEIN.Text = "";            
			txtName.Text = "";
            txtMiddle.Text = "";
            txtLast.Text = "";
			txtSSN.Text = "";
			txtOriginOfPolicy.Text = "";
			txtCity.Text = "";
			txtState.Text = "";
			txtPostalCode.Text = "";
			txtProviderAddress.Text = "";
			txtProviderCity.Text = "";
			txtProviderName.Text = "";
			txtProviderPostalCode.Text = "";
			txtProviderState.Text = "";
			txtProviderTelephone.Text = "";
			for (intLine = 1; intLine <= 6; intLine++)
			{
				(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredLast" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                (this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				for (x = 1; x <= 12; x++)
				{
					(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int intLine;
			int x;
			int intCoveredCount = 0;
			string strTemp = "";
			ClearFields();
			if (!boolTestPrint)
			{
				c1095B employeerecord;
				cACAEmployeeDependent coveredIndividual;
				cCoverageProvider CoverageProvider;
				if (theReport.ListOfForms.IsCurrent())
				{
					employeerecord = (c1095B)theReport.ListOfForms.GetCurrentItem();
					CoverageProvider = employeerecord.CoverageProvider;
					txtProviderAddress.Text = CoverageProvider.Address;
					txtProviderCity.Text = CoverageProvider.City;
					txtProviderName.Text = CoverageProvider.Name;
					txtProviderPostalCode.Text = CoverageProvider.PostalCode;
					txtProviderState.Text = CoverageProvider.State;
					txtProviderTelephone.Text = CoverageProvider.Telephone;
					txtEIN.Text = CoverageProvider.EIN;
					if (!theReport.MaskSSNs)
					{
						txtSSN.Text = employeerecord.SSN;
					}
					else
					{
						if (employeerecord.SSN.Length >= 4)
						{
							txtSSN.Text = "XXX-XX-" + Strings.Right(employeerecord.SSN, 4);
						}
					}
					txtName.Text = employeerecord.EmployeeFirstName;
                    txtMiddle.Text = employeerecord.EmployeeMiddleName;
                    txtLast.Text = employeerecord.EmployeeLastName;
					txtAddress.Text = employeerecord.Address;
					txtCity.Text = employeerecord.City;
					txtState.Text = employeerecord.State;
					txtPostalCode.Text = employeerecord.PostalCode;
					txtOriginOfPolicy.Text = employeerecord.OriginOfPolicy;
					intCoveredCount = employeerecord.coveredIndividuals.Count;
					if (intCoveredCount > 0)
					{
						intLine = 0;
						for (intLine = 1; intLine <= intCoveredCount; intLine++)
						{
							if (intLine > 6)
								break;
							coveredIndividual = employeerecord.coveredIndividuals[intLine];
							if (Information.IsDate(coveredIndividual.DateOfBirth))
							{
								(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Year) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(coveredIndividual.DateOfBirth).Day), 2);
							}
							(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.FirstName;
                            (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.MiddleName;
                            (this.Detail.Controls["txtCoveredLast" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = coveredIndividual.LastName;
                            strTemp = coveredIndividual.SSN;
							if (theReport.MaskSSNs)
							{
								if (strTemp.Length >= 4)
								{
									strTemp = "XXX-XX-" + Strings.Right(strTemp, 4);
								}
							}
							(this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
							if (coveredIndividual.CoveredAll12Months)
							{
								(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
							}
							else
							{
								for (x = 1; x <= 12; x++)
								{
									if (coveredIndividual.GetIsCoveredForMonth(x))
									{
										(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
									}
								}
							}
						}
						// intLine
					}
					theReport.ListOfForms.MoveNext();
				}
			}
			else
			{
				txtName.Text = "First";
                txtMiddle.Text = "Middle";
                txtLast.Text = "Last";
				txtSSN.Text = "123-45-6789";
				txtAddress.Text = "Street Address";
				txtCity.Text = "City";
				txtState.Text = "State";
				txtPostalCode.Text = "Postal Code";
				txtOriginOfPolicy.Text = "X";
				txtEIN.Text = "12-3456789";
				txtProviderAddress.Text = "Provider Address";
				txtProviderCity.Text = "City";
				txtProviderName.Text = "Provider Name";
				txtProviderPostalCode.Text = "Postal Code";
				txtProviderState.Text = "State";
				txtProviderTelephone.Text = "555-555-5555";
				for (intLine = 1; intLine <= 6; intLine++)
				{
					(this.Detail.Controls["txtCoveredAll12_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					(this.Detail.Controls["txtCoveredDOB" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "9999-99-99";
					(this.Detail.Controls["txtCoveredName" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "First";
                    (this.Detail.Controls["txtCoveredMiddle" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "M";
                    (this.Detail.Controls["txtCoveredLast" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Last " + FCConvert.ToString(intLine);
                    (this.Detail.Controls["txtCoveredSSN" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "000-00-0000";
					for (x = 1; x <= 12; x++)
					{
						(this.Detail.Controls["txtCoveredMonth" + x + "_" + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "X";
					}
					// x
				}
				boolFirstRecord = false;
			}
		}

		private void rpt1095B2016Page1_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt1095B2016Page1 properties;
			//rpt1095B2016Page1.Caption	= "1095-B";
			//rpt1095B2016Page1.Left	= 0;
			//rpt1095B2016Page1.Top	= 0;
			//rpt1095B2016Page1.Width	= 20475;
			//rpt1095B2016Page1.Height	= 12660;
			//rpt1095B2016Page1.StartUpPosition	= 3;
			//rpt1095B2016Page1.SectionData	= "rpt1095B2016Page1.dsx":0000;
			//End Unmaped Properties
		}
	}
}
