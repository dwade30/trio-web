﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt1094C2016GroupMembers.
	/// </summary>
	public partial class srpt1094C2016GroupMembers : FCSectionReport
	{
		public srpt1094C2016GroupMembers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srpt1094C2016GroupMembers InstancePtr
		{
			get
			{
				return (srpt1094C2016GroupMembers)Sys.GetInstance(typeof(srpt1094C2016GroupMembers));
			}
		}

		protected srpt1094C2016GroupMembers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srpt1094C2016GroupMembers	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c1094C theReport;
		private bool boolTestPrint;
		private bool boolFirstPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !boolFirstPage;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolFirstPage = true;
			theReport = (this.ParentReport as rpt1094C2016).ReportModel;
			boolTestPrint = (this.ParentReport as rpt1094C2016).TestPrint;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int intCount = 0;
			if (!boolTestPrint)
			{
				theReport.AggregatedMembers.MoveFirst();
				cAggregatedMember aMember;
				intCount = 1;
				while (theReport.AggregatedMembers.IsCurrent())
				{
					aMember = (cAggregatedMember)theReport.AggregatedMembers.GetCurrentItem();
					(this.Detail.Controls["txtName" + intCount] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = aMember.Name;
					(this.Detail.Controls["txtEIN" + intCount] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = aMember.EIN;
					intCount += 1;
					theReport.AggregatedMembers.MoveNext();
				}
				int x;
				for (x = intCount; x <= 30; x++)
				{
					(this.Detail.Controls["txtName" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtEIN" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
				// x
			}
			else
			{
				for (intCount = 1; intCount <= 30; intCount++)
				{
					(this.Detail.Controls["txtName" + intCount] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Name " + FCConvert.ToString(intCount);
					(this.Detail.Controls["txtEIN" + intCount] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "EIN " + FCConvert.ToString(intCount);
				}
			}
			boolFirstPage = false;
		}

		private void srpt1094C2016GroupMembers_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srpt1094C2016GroupMembers properties;
			//srpt1094C2016GroupMembers.Caption	= "ActiveReport1";
			//srpt1094C2016GroupMembers.Left	= 0;
			//srpt1094C2016GroupMembers.Top	= 0;
			//srpt1094C2016GroupMembers.Width	= 20160;
			//srpt1094C2016GroupMembers.Height	= 12165;
			//srpt1094C2016GroupMembers.StartUpPosition	= 3;
			//srpt1094C2016GroupMembers.SectionData	= "srpt1094C2016GroupMembers.dsx":0000;
			//End Unmaped Properties
		}
	}
}
