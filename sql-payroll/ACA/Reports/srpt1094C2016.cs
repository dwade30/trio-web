﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srpt1094C2016.
	/// </summary>
	public partial class srpt1094C2016 : FCSectionReport
	{
		public srpt1094C2016()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "1094-C";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srpt1094C2016 InstancePtr
		{
			get
			{
				return (srpt1094C2016)Sys.GetInstance(typeof(srpt1094C2016));
			}
		}

		protected srpt1094C2016 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srpt1094C2016	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c1094C theReport;
		private bool boolTestPrint;
		private bool boolFirstPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !boolFirstPage;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolFirstPage = true;
			theReport = (this.ParentReport as rpt1094C2016).ReportModel;
			boolTestPrint = (this.ParentReport as rpt1094C2016).TestPrint;
			if (theReport.HorizontalAlignment != 0 || theReport.VerticalAlignment != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					if (theReport.VerticalAlignment != 0)
					{
						ControlName.Top += FCConvert.ToSingle(120 * theReport.VerticalAlignment) / 1440F;
					}
					if (theReport.HorizontalAlignment != 0)
					{
						ControlName.Left += FCConvert.ToSingle(120 * theReport.HorizontalAlignment) / 1440F;
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolTestPrint)
			{
				txtCorrected.Text = "";
				if (theReport.Section4980HRelief)
				{
					txt4980H.Text = "X";
				}
				else
				{
					txt4980H.Text = "";
				}
				if (theReport.NinetyEightPercentOfferMethod)
				{
					txt98Percent.Text = "X";
				}
				else
				{
					txt98Percent.Text = "";
				}
				txtAddress.Text = theReport.Address;
				if (theReport.IsAuthoritativeTransmittal)
				{
					txtAuthoritative.Text = "X";
				}
				else
				{
					txtAuthoritative.Text = "";
				}
				txtCity.Text = theReport.City;
				txtContactName.Text = theReport.ContactFullName;
				txtContactTelephone.Text = theReport.ContactTelephone;
				txtEIN.Text = theReport.EIN;
				txtEmployer.Text = theReport.EmployerName;
				txtFiled.Text = theReport.Total1095CForMember.ToString();
				if (theReport.MemberOfAggregatedGroup)
				{
					txtInGroupYes.Text = "X";
					txtInGroupNo.Text = "";
				}
				else
				{
					txtInGroupYes.Text = "";
					txtInGroupNo.Text = "X";
				}
				txtPostalCode.Text = theReport.PostalCode;
				if (theReport.QualifyingOfferMethod)
				{
					txtQualifyingOfferMethod.Text = "X";
				}
				else
				{
					txtQualifyingOfferMethod.Text = "";
				}
				if (theReport.QualifyingOfferTransitionRelief)
				{
					txtQualifyingTransitionRelief.Text = "X";
				}
				else
				{
					txtQualifyingTransitionRelief.Text = "";
				}
				txtState.Text = theReport.State;
				txtTotalTransmitted.Text = theReport.Total1095CTransmitted.ToString();
			}
			else
			{
				txt4980H.Text = "X";
				txt98Percent.Text = "X";
				txtAddress.Text = "Address";
				txtAuthoritative.Text = "X";
				txtCity.Text = "City or town";
				txtContactName.Text = "Contact Name";
				txtContactTelephone.Text = "Telephone";
				txtCorrected.Text = "X";
				txtEIN.Text = "Employer EIN";
				txtEmployer.Text = "Employer Name";
				txtFiled.Text = "999";
				txtInGroupNo.Text = "X";
				txtInGroupYes.Text = "X";
				txtPostalCode.Text = "Postal Code";
				txtQualifyingOfferMethod.Text = "X";
				txtQualifyingTransitionRelief.Text = "X";
				txtState.Text = "State";
				txtTotalTransmitted.Text = "999";
			}
			boolFirstPage = false;
		}

		private void srpt1094C2016_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srpt1094C2016 properties;
			//srpt1094C2016.Caption	= "1094-C";
			//srpt1094C2016.Left	= 0;
			//srpt1094C2016.Top	= 0;
			//srpt1094C2016.Width	= 28680;
			//srpt1094C2016.Height	= 15690;
			//srpt1094C2016.StartUpPosition	= 3;
			//srpt1094C2016.SectionData	= "srpt1094C2016.dsx":0000;
			//End Unmaped Properties
		}
	}
}
