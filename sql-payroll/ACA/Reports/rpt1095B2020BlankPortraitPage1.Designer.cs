﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2017BlankPortraitPage1.
	/// </summary>
	partial class rpt1095B2020BlankPortraitPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095B2020BlankPortraitPage1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeeCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOriginOfPolicy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProviderPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape83 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape84 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbl12Months = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape46 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape70 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape72 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape73 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape74 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape75 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape76 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape85 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape79 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape82 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCoveredLast1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredLast6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoveredMiddle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployerName,
            this.txtEmployerReturnAddress,
            this.txtEmployerCityStateZip,
            this.txtEmployeeName,
            this.txtEmployeeAddress,
            this.txtEmployeeCityStateZip,
            this.txtCoveredName1,
            this.txtCoveredSSN1,
            this.txtCoveredDOB1,
            this.txtCoveredName2,
            this.txtCoveredSSN2,
            this.txtCoveredDOB2,
            this.txtCoveredName3,
            this.txtCoveredSSN3,
            this.txtCoveredDOB3,
            this.txtCoveredName4,
            this.txtCoveredSSN4,
            this.txtCoveredDOB4,
            this.txtCoveredName5,
            this.txtCoveredSSN5,
            this.txtCoveredDOB5,
            this.txtCoveredName6,
            this.txtCoveredSSN6,
            this.txtCoveredDOB6,
            this.txtCoveredAll12_1,
            this.txtCoveredMonth1_1,
            this.txtCoveredMonth2_1,
            this.txtCoveredMonth3_1,
            this.txtCoveredMonth4_1,
            this.txtCoveredMonth5_1,
            this.txtCoveredMonth6_1,
            this.txtCoveredMonth7_1,
            this.txtCoveredMonth8_1,
            this.txtCoveredMonth9_1,
            this.txtCoveredMonth10_1,
            this.txtCoveredMonth11_1,
            this.txtCoveredMonth12_1,
            this.txtCoveredAll12_2,
            this.txtCoveredMonth1_2,
            this.txtCoveredMonth2_2,
            this.txtCoveredMonth3_2,
            this.txtCoveredMonth4_2,
            this.txtCoveredMonth5_2,
            this.txtCoveredMonth6_2,
            this.txtCoveredMonth7_2,
            this.txtCoveredMonth8_2,
            this.txtCoveredMonth9_2,
            this.txtCoveredMonth10_2,
            this.txtCoveredMonth11_2,
            this.txtCoveredMonth12_2,
            this.txtCoveredAll12_3,
            this.txtCoveredMonth1_3,
            this.txtCoveredMonth2_3,
            this.txtCoveredMonth3_3,
            this.txtCoveredMonth4_3,
            this.txtCoveredMonth5_3,
            this.txtCoveredMonth6_3,
            this.txtCoveredMonth7_3,
            this.txtCoveredMonth8_3,
            this.txtCoveredMonth9_3,
            this.txtCoveredMonth10_3,
            this.txtCoveredMonth11_3,
            this.txtCoveredMonth12_3,
            this.txtCoveredAll12_4,
            this.txtCoveredMonth1_4,
            this.txtCoveredMonth2_4,
            this.txtCoveredMonth3_4,
            this.txtCoveredMonth4_4,
            this.txtCoveredMonth5_4,
            this.txtCoveredMonth6_4,
            this.txtCoveredMonth7_4,
            this.txtCoveredMonth8_4,
            this.txtCoveredMonth9_4,
            this.txtCoveredMonth10_4,
            this.txtCoveredMonth11_4,
            this.txtCoveredMonth12_4,
            this.txtCoveredAll12_5,
            this.txtCoveredMonth1_5,
            this.txtCoveredMonth2_5,
            this.txtCoveredMonth3_5,
            this.txtCoveredMonth4_5,
            this.txtCoveredMonth5_5,
            this.txtCoveredMonth6_5,
            this.txtCoveredMonth7_5,
            this.txtCoveredMonth8_5,
            this.txtCoveredMonth9_5,
            this.txtCoveredMonth10_5,
            this.txtCoveredMonth11_5,
            this.txtCoveredMonth12_5,
            this.txtCoveredAll12_6,
            this.txtCoveredMonth1_6,
            this.txtCoveredMonth2_6,
            this.txtCoveredMonth3_6,
            this.txtCoveredMonth4_6,
            this.txtCoveredMonth5_6,
            this.txtCoveredMonth6_6,
            this.txtCoveredMonth7_6,
            this.txtCoveredMonth8_6,
            this.txtCoveredMonth9_6,
            this.txtCoveredMonth10_6,
            this.txtCoveredMonth11_6,
            this.txtCoveredMonth12_6,
            this.txtName,
            this.txtSSN,
            this.txtAddress,
            this.txtCity,
            this.txtState,
            this.txtOriginOfPolicy,
            this.txtPostalCode,
            this.txtProviderName,
            this.txtProviderAddress,
            this.txtProviderCity,
            this.txtProviderState,
            this.txtEIN,
            this.txtProviderTelephone,
            this.txtProviderPostalCode,
            this.Image2,
            this.Label83,
            this.Label62,
            this.Line44,
            this.Label67,
            this.Label69,
            this.Line48,
            this.Label77,
            this.Line49,
            this.Label78,
            this.Line56,
            this.Label80,
            this.Label81,
            this.Label82,
            this.Line55,
            this.Label85,
            this.Shape83,
            this.Shape84,
            this.Line57,
            this.Label86,
            this.Label88,
            this.Label89,
            this.Line58,
            this.Label90,
            this.Label93,
            this.Label95,
            this.Shape77,
            this.Line59,
            this.Label96,
            this.Label97,
            this.Label98,
            this.Label99,
            this.Label100,
            this.Label101,
            this.Label102,
            this.Label103,
            this.Label104,
            this.Line61,
            this.Line62,
            this.Label106,
            this.Line63,
            this.Label107,
            this.Line64,
            this.Label108,
            this.Label14,
            this.Label13,
            this.Label9,
            this.Shape2,
            this.Label4,
            this.Line1,
            this.Line2,
            this.Line3,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Line8,
            this.Line9,
            this.Line14,
            this.Line18,
            this.Line19,
            this.Line22,
            this.lbl12Months,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Line24,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Shape5,
            this.Shape6,
            this.Shape7,
            this.Shape8,
            this.Shape9,
            this.Shape10,
            this.Shape11,
            this.Shape12,
            this.Shape13,
            this.Shape14,
            this.Shape15,
            this.Shape16,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Shape20,
            this.Shape21,
            this.Shape22,
            this.Shape23,
            this.Shape24,
            this.Shape25,
            this.Shape26,
            this.Shape27,
            this.Shape28,
            this.Shape29,
            this.Shape30,
            this.Shape31,
            this.Shape32,
            this.Shape33,
            this.Shape34,
            this.Shape35,
            this.Shape36,
            this.Shape37,
            this.Shape38,
            this.Shape39,
            this.Shape40,
            this.Shape41,
            this.Shape42,
            this.Shape43,
            this.Shape44,
            this.Shape45,
            this.Shape46,
            this.Shape47,
            this.Shape48,
            this.Shape49,
            this.Shape50,
            this.Shape51,
            this.Shape52,
            this.Shape53,
            this.Shape54,
            this.Shape55,
            this.Shape56,
            this.Shape57,
            this.Shape58,
            this.Shape59,
            this.Shape60,
            this.Shape61,
            this.Shape62,
            this.Shape63,
            this.Shape64,
            this.Shape65,
            this.Shape66,
            this.Shape67,
            this.Shape68,
            this.Shape69,
            this.Shape70,
            this.Shape71,
            this.Shape72,
            this.Shape73,
            this.Shape74,
            this.Shape75,
            this.Shape76,
            this.Shape85,
            this.Shape78,
            this.Shape79,
            this.Shape80,
            this.Shape81,
            this.Shape82,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Line20,
            this.Line21,
            this.Line17,
            this.Line16,
            this.Line15,
            this.Line13,
            this.Line12,
            this.Line11,
            this.Line10,
            this.Label109,
            this.Line65,
            this.Line66,
            this.Line67,
            this.Line68,
            this.Line69,
            this.Label110,
            this.Label111,
            this.Label112,
            this.Label113,
            this.Label114,
            this.Label115,
            this.Line70,
            this.Label116,
            this.Line71,
            this.Line72,
            this.Line73,
            this.Label117,
            this.Label118,
            this.Label119,
            this.Label120,
            this.Label121,
            this.Line60,
            this.Label61,
            this.Label105,
            this.Label122,
            this.Label123,
            this.Line74,
            this.Line75,
            this.txtCoveredLast1,
            this.txtCoveredLast2,
            this.txtCoveredLast3,
            this.txtCoveredLast4,
            this.txtCoveredLast5,
            this.txtCoveredLast6,
            this.txtCoveredMiddle1,
            this.txtCoveredMiddle2,
            this.txtCoveredMiddle3,
            this.txtCoveredMiddle4,
            this.txtCoveredMiddle5,
            this.txtCoveredMiddle6,
            this.Line76,
            this.Line77,
            this.txtMiddle,
            this.txtLast,
            this.Line78,
            this.Line23});
            this.Detail.Height = 10.20833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.Height = 0.1770833F;
            this.txtEmployerName.Left = 0.25F;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Style = "font-size: 8.5pt";
            this.txtEmployerName.Text = null;
            this.txtEmployerName.Top = 0.1562486F;
            this.txtEmployerName.Width = 3.083333F;
            // 
            // txtEmployerReturnAddress
            // 
            this.txtEmployerReturnAddress.Height = 0.1770833F;
            this.txtEmployerReturnAddress.Left = 0.25F;
            this.txtEmployerReturnAddress.Name = "txtEmployerReturnAddress";
            this.txtEmployerReturnAddress.Style = "font-size: 8.5pt";
            this.txtEmployerReturnAddress.Text = null;
            this.txtEmployerReturnAddress.Top = 0.3437486F;
            this.txtEmployerReturnAddress.Width = 3.083333F;
            // 
            // txtEmployerCityStateZip
            // 
            this.txtEmployerCityStateZip.Height = 0.1770833F;
            this.txtEmployerCityStateZip.Left = 0.25F;
            this.txtEmployerCityStateZip.Name = "txtEmployerCityStateZip";
            this.txtEmployerCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployerCityStateZip.Text = null;
            this.txtEmployerCityStateZip.Top = 0.5312486F;
            this.txtEmployerCityStateZip.Width = 3.083333F;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Height = 0.1770833F;
            this.txtEmployeeName.Left = 0.25F;
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Style = "font-size: 8.5pt";
            this.txtEmployeeName.Text = null;
            this.txtEmployeeName.Top = 2.156249F;
            this.txtEmployeeName.Width = 3.083333F;
            // 
            // txtEmployeeAddress
            // 
            this.txtEmployeeAddress.Height = 0.1770833F;
            this.txtEmployeeAddress.Left = 0.25F;
            this.txtEmployeeAddress.Name = "txtEmployeeAddress";
            this.txtEmployeeAddress.Style = "font-size: 8.5pt";
            this.txtEmployeeAddress.Text = null;
            this.txtEmployeeAddress.Top = 2.343749F;
            this.txtEmployeeAddress.Width = 3.083333F;
            // 
            // txtEmployeeCityStateZip
            // 
            this.txtEmployeeCityStateZip.Height = 0.1770833F;
            this.txtEmployeeCityStateZip.Left = 0.25F;
            this.txtEmployeeCityStateZip.Name = "txtEmployeeCityStateZip";
            this.txtEmployeeCityStateZip.Style = "font-size: 8.5pt";
            this.txtEmployeeCityStateZip.Text = null;
            this.txtEmployeeCityStateZip.Top = 2.531249F;
            this.txtEmployeeCityStateZip.Width = 3.083333F;
            // 
            // txtCoveredName1
            // 
            this.txtCoveredName1.CanGrow = false;
            this.txtCoveredName1.Height = 0.1770833F;
            this.txtCoveredName1.Left = 0.1666667F;
            this.txtCoveredName1.Name = "txtCoveredName1";
            this.txtCoveredName1.Style = "font-size: 8.5pt";
            this.txtCoveredName1.Text = null;
            this.txtCoveredName1.Top = 7.784721F;
            this.txtCoveredName1.Width = 0.6875F;
            // 
            // txtCoveredSSN1
            // 
            this.txtCoveredSSN1.Height = 0.1770833F;
            this.txtCoveredSSN1.Left = 1.8125F;
            this.txtCoveredSSN1.Name = "txtCoveredSSN1";
            this.txtCoveredSSN1.Style = "font-size: 8.5pt";
            this.txtCoveredSSN1.Text = null;
            this.txtCoveredSSN1.Top = 7.781249F;
            this.txtCoveredSSN1.Width = 0.8333333F;
            // 
            // txtCoveredDOB1
            // 
            this.txtCoveredDOB1.Height = 0.1770833F;
            this.txtCoveredDOB1.Left = 2.75F;
            this.txtCoveredDOB1.Name = "txtCoveredDOB1";
            this.txtCoveredDOB1.Style = "font-size: 8.5pt";
            this.txtCoveredDOB1.Text = null;
            this.txtCoveredDOB1.Top = 7.781249F;
            this.txtCoveredDOB1.Width = 0.6875F;
            // 
            // txtCoveredName2
            // 
            this.txtCoveredName2.CanGrow = false;
            this.txtCoveredName2.Height = 0.1770833F;
            this.txtCoveredName2.Left = 0.1666667F;
            this.txtCoveredName2.Name = "txtCoveredName2";
            this.txtCoveredName2.Style = "font-size: 8.5pt";
            this.txtCoveredName2.Text = null;
            this.txtCoveredName2.Top = 8.156248F;
            this.txtCoveredName2.Width = 0.6875F;
            // 
            // txtCoveredSSN2
            // 
            this.txtCoveredSSN2.Height = 0.1770833F;
            this.txtCoveredSSN2.Left = 1.8125F;
            this.txtCoveredSSN2.Name = "txtCoveredSSN2";
            this.txtCoveredSSN2.Style = "font-size: 8.5pt";
            this.txtCoveredSSN2.Text = null;
            this.txtCoveredSSN2.Top = 8.156248F;
            this.txtCoveredSSN2.Width = 0.8333333F;
            // 
            // txtCoveredDOB2
            // 
            this.txtCoveredDOB2.Height = 0.1770833F;
            this.txtCoveredDOB2.Left = 2.75F;
            this.txtCoveredDOB2.Name = "txtCoveredDOB2";
            this.txtCoveredDOB2.Style = "font-size: 8.5pt";
            this.txtCoveredDOB2.Text = null;
            this.txtCoveredDOB2.Top = 8.156248F;
            this.txtCoveredDOB2.Width = 0.6875F;
            // 
            // txtCoveredName3
            // 
            this.txtCoveredName3.CanGrow = false;
            this.txtCoveredName3.Height = 0.1770833F;
            this.txtCoveredName3.Left = 0.1666667F;
            this.txtCoveredName3.Name = "txtCoveredName3";
            this.txtCoveredName3.Style = "font-size: 8.5pt";
            this.txtCoveredName3.Text = null;
            this.txtCoveredName3.Top = 8.531248F;
            this.txtCoveredName3.Width = 0.6875F;
            // 
            // txtCoveredSSN3
            // 
            this.txtCoveredSSN3.Height = 0.1770833F;
            this.txtCoveredSSN3.Left = 1.8125F;
            this.txtCoveredSSN3.Name = "txtCoveredSSN3";
            this.txtCoveredSSN3.Style = "font-size: 8.5pt";
            this.txtCoveredSSN3.Text = null;
            this.txtCoveredSSN3.Top = 8.531248F;
            this.txtCoveredSSN3.Width = 0.8333333F;
            // 
            // txtCoveredDOB3
            // 
            this.txtCoveredDOB3.Height = 0.1770833F;
            this.txtCoveredDOB3.Left = 2.75F;
            this.txtCoveredDOB3.Name = "txtCoveredDOB3";
            this.txtCoveredDOB3.Style = "font-size: 8.5pt";
            this.txtCoveredDOB3.Text = null;
            this.txtCoveredDOB3.Top = 8.531248F;
            this.txtCoveredDOB3.Width = 0.6875F;
            // 
            // txtCoveredName4
            // 
            this.txtCoveredName4.CanGrow = false;
            this.txtCoveredName4.Height = 0.1770833F;
            this.txtCoveredName4.Left = 0.1666667F;
            this.txtCoveredName4.Name = "txtCoveredName4";
            this.txtCoveredName4.Style = "font-size: 8.5pt";
            this.txtCoveredName4.Text = null;
            this.txtCoveredName4.Top = 8.906248F;
            this.txtCoveredName4.Width = 0.6875F;
            // 
            // txtCoveredSSN4
            // 
            this.txtCoveredSSN4.Height = 0.1770833F;
            this.txtCoveredSSN4.Left = 1.8125F;
            this.txtCoveredSSN4.Name = "txtCoveredSSN4";
            this.txtCoveredSSN4.Style = "font-size: 8.5pt";
            this.txtCoveredSSN4.Text = null;
            this.txtCoveredSSN4.Top = 8.906248F;
            this.txtCoveredSSN4.Width = 0.8333333F;
            // 
            // txtCoveredDOB4
            // 
            this.txtCoveredDOB4.Height = 0.1770833F;
            this.txtCoveredDOB4.Left = 2.75F;
            this.txtCoveredDOB4.Name = "txtCoveredDOB4";
            this.txtCoveredDOB4.Style = "font-size: 8.5pt";
            this.txtCoveredDOB4.Text = null;
            this.txtCoveredDOB4.Top = 8.906248F;
            this.txtCoveredDOB4.Width = 0.6875F;
            // 
            // txtCoveredName5
            // 
            this.txtCoveredName5.CanGrow = false;
            this.txtCoveredName5.Height = 0.1770833F;
            this.txtCoveredName5.Left = 0.1666667F;
            this.txtCoveredName5.Name = "txtCoveredName5";
            this.txtCoveredName5.Style = "font-size: 8.5pt";
            this.txtCoveredName5.Text = null;
            this.txtCoveredName5.Top = 9.281248F;
            this.txtCoveredName5.Width = 0.6875F;
            // 
            // txtCoveredSSN5
            // 
            this.txtCoveredSSN5.Height = 0.1770833F;
            this.txtCoveredSSN5.Left = 1.8125F;
            this.txtCoveredSSN5.Name = "txtCoveredSSN5";
            this.txtCoveredSSN5.Style = "font-size: 8.5pt";
            this.txtCoveredSSN5.Text = null;
            this.txtCoveredSSN5.Top = 9.284721F;
            this.txtCoveredSSN5.Width = 0.8333333F;
            // 
            // txtCoveredDOB5
            // 
            this.txtCoveredDOB5.Height = 0.1770833F;
            this.txtCoveredDOB5.Left = 2.75F;
            this.txtCoveredDOB5.Name = "txtCoveredDOB5";
            this.txtCoveredDOB5.Style = "font-size: 8.5pt";
            this.txtCoveredDOB5.Text = null;
            this.txtCoveredDOB5.Top = 9.281248F;
            this.txtCoveredDOB5.Width = 0.6875F;
            // 
            // txtCoveredName6
            // 
            this.txtCoveredName6.CanGrow = false;
            this.txtCoveredName6.Height = 0.1770833F;
            this.txtCoveredName6.Left = 0.1666667F;
            this.txtCoveredName6.Name = "txtCoveredName6";
            this.txtCoveredName6.Style = "font-size: 8.5pt";
            this.txtCoveredName6.Text = null;
            this.txtCoveredName6.Top = 9.645832F;
            this.txtCoveredName6.Width = 0.6875F;
            // 
            // txtCoveredSSN6
            // 
            this.txtCoveredSSN6.Height = 0.1770833F;
            this.txtCoveredSSN6.Left = 1.8125F;
            this.txtCoveredSSN6.Name = "txtCoveredSSN6";
            this.txtCoveredSSN6.Style = "font-size: 8.5pt";
            this.txtCoveredSSN6.Text = null;
            this.txtCoveredSSN6.Top = 9.649303F;
            this.txtCoveredSSN6.Width = 0.8333333F;
            // 
            // txtCoveredDOB6
            // 
            this.txtCoveredDOB6.Height = 0.1770833F;
            this.txtCoveredDOB6.Left = 2.75F;
            this.txtCoveredDOB6.Name = "txtCoveredDOB6";
            this.txtCoveredDOB6.Style = "font-size: 8.5pt";
            this.txtCoveredDOB6.Text = null;
            this.txtCoveredDOB6.Top = 9.645832F;
            this.txtCoveredDOB6.Width = 0.6875F;
            // 
            // txtCoveredAll12_1
            // 
            this.txtCoveredAll12_1.Height = 0.1770833F;
            this.txtCoveredAll12_1.Left = 3.604167F;
            this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
            this.txtCoveredAll12_1.Text = null;
            this.txtCoveredAll12_1.Top = 7.781249F;
            this.txtCoveredAll12_1.Width = 0.25F;
            // 
            // txtCoveredMonth1_1
            // 
            this.txtCoveredMonth1_1.Height = 0.1770833F;
            this.txtCoveredMonth1_1.Left = 4F;
            this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
            this.txtCoveredMonth1_1.Text = null;
            this.txtCoveredMonth1_1.Top = 7.781249F;
            this.txtCoveredMonth1_1.Width = 0.1875F;
            // 
            // txtCoveredMonth2_1
            // 
            this.txtCoveredMonth2_1.Height = 0.1770833F;
            this.txtCoveredMonth2_1.Left = 4.291667F;
            this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
            this.txtCoveredMonth2_1.Text = null;
            this.txtCoveredMonth2_1.Top = 7.781249F;
            this.txtCoveredMonth2_1.Width = 0.25F;
            // 
            // txtCoveredMonth3_1
            // 
            this.txtCoveredMonth3_1.Height = 0.1770833F;
            this.txtCoveredMonth3_1.Left = 4.583333F;
            this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
            this.txtCoveredMonth3_1.Text = null;
            this.txtCoveredMonth3_1.Top = 7.781249F;
            this.txtCoveredMonth3_1.Width = 0.25F;
            // 
            // txtCoveredMonth4_1
            // 
            this.txtCoveredMonth4_1.Height = 0.1770833F;
            this.txtCoveredMonth4_1.Left = 4.885417F;
            this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
            this.txtCoveredMonth4_1.Text = null;
            this.txtCoveredMonth4_1.Top = 7.781249F;
            this.txtCoveredMonth4_1.Width = 0.25F;
            // 
            // txtCoveredMonth5_1
            // 
            this.txtCoveredMonth5_1.Height = 0.1770833F;
            this.txtCoveredMonth5_1.Left = 5.177083F;
            this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
            this.txtCoveredMonth5_1.Text = null;
            this.txtCoveredMonth5_1.Top = 7.781249F;
            this.txtCoveredMonth5_1.Width = 0.25F;
            // 
            // txtCoveredMonth6_1
            // 
            this.txtCoveredMonth6_1.Height = 0.1770833F;
            this.txtCoveredMonth6_1.Left = 5.46875F;
            this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
            this.txtCoveredMonth6_1.Text = null;
            this.txtCoveredMonth6_1.Top = 7.781249F;
            this.txtCoveredMonth6_1.Width = 0.25F;
            // 
            // txtCoveredMonth7_1
            // 
            this.txtCoveredMonth7_1.Height = 0.1770833F;
            this.txtCoveredMonth7_1.Left = 5.770833F;
            this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
            this.txtCoveredMonth7_1.Text = null;
            this.txtCoveredMonth7_1.Top = 7.781249F;
            this.txtCoveredMonth7_1.Width = 0.25F;
            // 
            // txtCoveredMonth8_1
            // 
            this.txtCoveredMonth8_1.Height = 0.1770833F;
            this.txtCoveredMonth8_1.Left = 6.0625F;
            this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
            this.txtCoveredMonth8_1.Text = null;
            this.txtCoveredMonth8_1.Top = 7.781249F;
            this.txtCoveredMonth8_1.Width = 0.25F;
            // 
            // txtCoveredMonth9_1
            // 
            this.txtCoveredMonth9_1.Height = 0.1770833F;
            this.txtCoveredMonth9_1.Left = 6.354167F;
            this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
            this.txtCoveredMonth9_1.Text = null;
            this.txtCoveredMonth9_1.Top = 7.781249F;
            this.txtCoveredMonth9_1.Width = 0.25F;
            // 
            // txtCoveredMonth10_1
            // 
            this.txtCoveredMonth10_1.Height = 0.1770833F;
            this.txtCoveredMonth10_1.Left = 6.65625F;
            this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
            this.txtCoveredMonth10_1.Text = null;
            this.txtCoveredMonth10_1.Top = 7.781249F;
            this.txtCoveredMonth10_1.Width = 0.25F;
            // 
            // txtCoveredMonth11_1
            // 
            this.txtCoveredMonth11_1.Height = 0.1770833F;
            this.txtCoveredMonth11_1.Left = 6.947917F;
            this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
            this.txtCoveredMonth11_1.Text = null;
            this.txtCoveredMonth11_1.Top = 7.781249F;
            this.txtCoveredMonth11_1.Width = 0.25F;
            // 
            // txtCoveredMonth12_1
            // 
            this.txtCoveredMonth12_1.Height = 0.1770833F;
            this.txtCoveredMonth12_1.Left = 7.239583F;
            this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
            this.txtCoveredMonth12_1.Text = null;
            this.txtCoveredMonth12_1.Top = 7.781249F;
            this.txtCoveredMonth12_1.Width = 0.1875F;
            // 
            // txtCoveredAll12_2
            // 
            this.txtCoveredAll12_2.Height = 0.1770833F;
            this.txtCoveredAll12_2.Left = 3.604167F;
            this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
            this.txtCoveredAll12_2.Text = null;
            this.txtCoveredAll12_2.Top = 8.156248F;
            this.txtCoveredAll12_2.Width = 0.25F;
            // 
            // txtCoveredMonth1_2
            // 
            this.txtCoveredMonth1_2.Height = 0.1770833F;
            this.txtCoveredMonth1_2.Left = 4F;
            this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
            this.txtCoveredMonth1_2.Text = null;
            this.txtCoveredMonth1_2.Top = 8.156248F;
            this.txtCoveredMonth1_2.Width = 0.1875F;
            // 
            // txtCoveredMonth2_2
            // 
            this.txtCoveredMonth2_2.Height = 0.1770833F;
            this.txtCoveredMonth2_2.Left = 4.291667F;
            this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
            this.txtCoveredMonth2_2.Text = null;
            this.txtCoveredMonth2_2.Top = 8.156248F;
            this.txtCoveredMonth2_2.Width = 0.25F;
            // 
            // txtCoveredMonth3_2
            // 
            this.txtCoveredMonth3_2.Height = 0.1770833F;
            this.txtCoveredMonth3_2.Left = 4.583333F;
            this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
            this.txtCoveredMonth3_2.Text = null;
            this.txtCoveredMonth3_2.Top = 8.156248F;
            this.txtCoveredMonth3_2.Width = 0.25F;
            // 
            // txtCoveredMonth4_2
            // 
            this.txtCoveredMonth4_2.Height = 0.1770833F;
            this.txtCoveredMonth4_2.Left = 4.885417F;
            this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
            this.txtCoveredMonth4_2.Text = null;
            this.txtCoveredMonth4_2.Top = 8.156248F;
            this.txtCoveredMonth4_2.Width = 0.25F;
            // 
            // txtCoveredMonth5_2
            // 
            this.txtCoveredMonth5_2.Height = 0.1770833F;
            this.txtCoveredMonth5_2.Left = 5.177083F;
            this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
            this.txtCoveredMonth5_2.Text = null;
            this.txtCoveredMonth5_2.Top = 8.156248F;
            this.txtCoveredMonth5_2.Width = 0.25F;
            // 
            // txtCoveredMonth6_2
            // 
            this.txtCoveredMonth6_2.Height = 0.1770833F;
            this.txtCoveredMonth6_2.Left = 5.46875F;
            this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
            this.txtCoveredMonth6_2.Text = null;
            this.txtCoveredMonth6_2.Top = 8.156248F;
            this.txtCoveredMonth6_2.Width = 0.25F;
            // 
            // txtCoveredMonth7_2
            // 
            this.txtCoveredMonth7_2.Height = 0.1770833F;
            this.txtCoveredMonth7_2.Left = 5.770833F;
            this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
            this.txtCoveredMonth7_2.Text = null;
            this.txtCoveredMonth7_2.Top = 8.156248F;
            this.txtCoveredMonth7_2.Width = 0.25F;
            // 
            // txtCoveredMonth8_2
            // 
            this.txtCoveredMonth8_2.Height = 0.1770833F;
            this.txtCoveredMonth8_2.Left = 6.0625F;
            this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
            this.txtCoveredMonth8_2.Text = null;
            this.txtCoveredMonth8_2.Top = 8.156248F;
            this.txtCoveredMonth8_2.Width = 0.25F;
            // 
            // txtCoveredMonth9_2
            // 
            this.txtCoveredMonth9_2.Height = 0.1770833F;
            this.txtCoveredMonth9_2.Left = 6.354167F;
            this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
            this.txtCoveredMonth9_2.Text = null;
            this.txtCoveredMonth9_2.Top = 8.156248F;
            this.txtCoveredMonth9_2.Width = 0.25F;
            // 
            // txtCoveredMonth10_2
            // 
            this.txtCoveredMonth10_2.Height = 0.1770833F;
            this.txtCoveredMonth10_2.Left = 6.65625F;
            this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
            this.txtCoveredMonth10_2.Text = null;
            this.txtCoveredMonth10_2.Top = 8.156248F;
            this.txtCoveredMonth10_2.Width = 0.25F;
            // 
            // txtCoveredMonth11_2
            // 
            this.txtCoveredMonth11_2.Height = 0.1770833F;
            this.txtCoveredMonth11_2.Left = 6.947917F;
            this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
            this.txtCoveredMonth11_2.Text = null;
            this.txtCoveredMonth11_2.Top = 8.156248F;
            this.txtCoveredMonth11_2.Width = 0.25F;
            // 
            // txtCoveredMonth12_2
            // 
            this.txtCoveredMonth12_2.Height = 0.1770833F;
            this.txtCoveredMonth12_2.Left = 7.239583F;
            this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
            this.txtCoveredMonth12_2.Text = null;
            this.txtCoveredMonth12_2.Top = 8.156248F;
            this.txtCoveredMonth12_2.Width = 0.1875F;
            // 
            // txtCoveredAll12_3
            // 
            this.txtCoveredAll12_3.Height = 0.1770833F;
            this.txtCoveredAll12_3.Left = 3.604167F;
            this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
            this.txtCoveredAll12_3.Text = null;
            this.txtCoveredAll12_3.Top = 8.531248F;
            this.txtCoveredAll12_3.Width = 0.25F;
            // 
            // txtCoveredMonth1_3
            // 
            this.txtCoveredMonth1_3.Height = 0.1770833F;
            this.txtCoveredMonth1_3.Left = 4F;
            this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
            this.txtCoveredMonth1_3.Text = null;
            this.txtCoveredMonth1_3.Top = 8.531248F;
            this.txtCoveredMonth1_3.Width = 0.1875F;
            // 
            // txtCoveredMonth2_3
            // 
            this.txtCoveredMonth2_3.Height = 0.1770833F;
            this.txtCoveredMonth2_3.Left = 4.291667F;
            this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
            this.txtCoveredMonth2_3.Text = null;
            this.txtCoveredMonth2_3.Top = 8.531248F;
            this.txtCoveredMonth2_3.Width = 0.25F;
            // 
            // txtCoveredMonth3_3
            // 
            this.txtCoveredMonth3_3.Height = 0.1770833F;
            this.txtCoveredMonth3_3.Left = 4.583333F;
            this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
            this.txtCoveredMonth3_3.Text = null;
            this.txtCoveredMonth3_3.Top = 8.531248F;
            this.txtCoveredMonth3_3.Width = 0.25F;
            // 
            // txtCoveredMonth4_3
            // 
            this.txtCoveredMonth4_3.Height = 0.1770833F;
            this.txtCoveredMonth4_3.Left = 4.885417F;
            this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
            this.txtCoveredMonth4_3.Text = null;
            this.txtCoveredMonth4_3.Top = 8.531248F;
            this.txtCoveredMonth4_3.Width = 0.25F;
            // 
            // txtCoveredMonth5_3
            // 
            this.txtCoveredMonth5_3.Height = 0.1770833F;
            this.txtCoveredMonth5_3.Left = 5.177083F;
            this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
            this.txtCoveredMonth5_3.Text = null;
            this.txtCoveredMonth5_3.Top = 8.531248F;
            this.txtCoveredMonth5_3.Width = 0.25F;
            // 
            // txtCoveredMonth6_3
            // 
            this.txtCoveredMonth6_3.Height = 0.1770833F;
            this.txtCoveredMonth6_3.Left = 5.46875F;
            this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
            this.txtCoveredMonth6_3.Text = null;
            this.txtCoveredMonth6_3.Top = 8.531248F;
            this.txtCoveredMonth6_3.Width = 0.25F;
            // 
            // txtCoveredMonth7_3
            // 
            this.txtCoveredMonth7_3.Height = 0.1770833F;
            this.txtCoveredMonth7_3.Left = 5.770833F;
            this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
            this.txtCoveredMonth7_3.Text = null;
            this.txtCoveredMonth7_3.Top = 8.531248F;
            this.txtCoveredMonth7_3.Width = 0.25F;
            // 
            // txtCoveredMonth8_3
            // 
            this.txtCoveredMonth8_3.Height = 0.1770833F;
            this.txtCoveredMonth8_3.Left = 6.0625F;
            this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
            this.txtCoveredMonth8_3.Text = null;
            this.txtCoveredMonth8_3.Top = 8.531248F;
            this.txtCoveredMonth8_3.Width = 0.25F;
            // 
            // txtCoveredMonth9_3
            // 
            this.txtCoveredMonth9_3.Height = 0.1770833F;
            this.txtCoveredMonth9_3.Left = 6.354167F;
            this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
            this.txtCoveredMonth9_3.Text = null;
            this.txtCoveredMonth9_3.Top = 8.531248F;
            this.txtCoveredMonth9_3.Width = 0.25F;
            // 
            // txtCoveredMonth10_3
            // 
            this.txtCoveredMonth10_3.Height = 0.1770833F;
            this.txtCoveredMonth10_3.Left = 6.65625F;
            this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
            this.txtCoveredMonth10_3.Text = null;
            this.txtCoveredMonth10_3.Top = 8.531248F;
            this.txtCoveredMonth10_3.Width = 0.25F;
            // 
            // txtCoveredMonth11_3
            // 
            this.txtCoveredMonth11_3.Height = 0.1770833F;
            this.txtCoveredMonth11_3.Left = 6.947917F;
            this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
            this.txtCoveredMonth11_3.Text = null;
            this.txtCoveredMonth11_3.Top = 8.531248F;
            this.txtCoveredMonth11_3.Width = 0.25F;
            // 
            // txtCoveredMonth12_3
            // 
            this.txtCoveredMonth12_3.Height = 0.1770833F;
            this.txtCoveredMonth12_3.Left = 7.239583F;
            this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
            this.txtCoveredMonth12_3.Text = null;
            this.txtCoveredMonth12_3.Top = 8.531248F;
            this.txtCoveredMonth12_3.Width = 0.1875F;
            // 
            // txtCoveredAll12_4
            // 
            this.txtCoveredAll12_4.Height = 0.1770833F;
            this.txtCoveredAll12_4.Left = 3.604167F;
            this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
            this.txtCoveredAll12_4.Text = null;
            this.txtCoveredAll12_4.Top = 8.906248F;
            this.txtCoveredAll12_4.Width = 0.25F;
            // 
            // txtCoveredMonth1_4
            // 
            this.txtCoveredMonth1_4.Height = 0.1770833F;
            this.txtCoveredMonth1_4.Left = 4F;
            this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
            this.txtCoveredMonth1_4.Text = null;
            this.txtCoveredMonth1_4.Top = 8.906248F;
            this.txtCoveredMonth1_4.Width = 0.1875F;
            // 
            // txtCoveredMonth2_4
            // 
            this.txtCoveredMonth2_4.Height = 0.1770833F;
            this.txtCoveredMonth2_4.Left = 4.291667F;
            this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
            this.txtCoveredMonth2_4.Text = null;
            this.txtCoveredMonth2_4.Top = 8.906248F;
            this.txtCoveredMonth2_4.Width = 0.25F;
            // 
            // txtCoveredMonth3_4
            // 
            this.txtCoveredMonth3_4.Height = 0.1770833F;
            this.txtCoveredMonth3_4.Left = 4.583333F;
            this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
            this.txtCoveredMonth3_4.Text = null;
            this.txtCoveredMonth3_4.Top = 8.906248F;
            this.txtCoveredMonth3_4.Width = 0.25F;
            // 
            // txtCoveredMonth4_4
            // 
            this.txtCoveredMonth4_4.Height = 0.1770833F;
            this.txtCoveredMonth4_4.Left = 4.885417F;
            this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
            this.txtCoveredMonth4_4.Text = null;
            this.txtCoveredMonth4_4.Top = 8.906248F;
            this.txtCoveredMonth4_4.Width = 0.25F;
            // 
            // txtCoveredMonth5_4
            // 
            this.txtCoveredMonth5_4.Height = 0.1770833F;
            this.txtCoveredMonth5_4.Left = 5.177083F;
            this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
            this.txtCoveredMonth5_4.Text = null;
            this.txtCoveredMonth5_4.Top = 8.906248F;
            this.txtCoveredMonth5_4.Width = 0.25F;
            // 
            // txtCoveredMonth6_4
            // 
            this.txtCoveredMonth6_4.Height = 0.1770833F;
            this.txtCoveredMonth6_4.Left = 5.46875F;
            this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
            this.txtCoveredMonth6_4.Text = null;
            this.txtCoveredMonth6_4.Top = 8.906248F;
            this.txtCoveredMonth6_4.Width = 0.25F;
            // 
            // txtCoveredMonth7_4
            // 
            this.txtCoveredMonth7_4.Height = 0.1770833F;
            this.txtCoveredMonth7_4.Left = 5.770833F;
            this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
            this.txtCoveredMonth7_4.Text = null;
            this.txtCoveredMonth7_4.Top = 8.906248F;
            this.txtCoveredMonth7_4.Width = 0.25F;
            // 
            // txtCoveredMonth8_4
            // 
            this.txtCoveredMonth8_4.Height = 0.1770833F;
            this.txtCoveredMonth8_4.Left = 6.0625F;
            this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
            this.txtCoveredMonth8_4.Text = null;
            this.txtCoveredMonth8_4.Top = 8.906248F;
            this.txtCoveredMonth8_4.Width = 0.25F;
            // 
            // txtCoveredMonth9_4
            // 
            this.txtCoveredMonth9_4.Height = 0.1770833F;
            this.txtCoveredMonth9_4.Left = 6.354167F;
            this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
            this.txtCoveredMonth9_4.Text = null;
            this.txtCoveredMonth9_4.Top = 8.906248F;
            this.txtCoveredMonth9_4.Width = 0.25F;
            // 
            // txtCoveredMonth10_4
            // 
            this.txtCoveredMonth10_4.Height = 0.1770833F;
            this.txtCoveredMonth10_4.Left = 6.65625F;
            this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
            this.txtCoveredMonth10_4.Text = null;
            this.txtCoveredMonth10_4.Top = 8.906248F;
            this.txtCoveredMonth10_4.Width = 0.25F;
            // 
            // txtCoveredMonth11_4
            // 
            this.txtCoveredMonth11_4.Height = 0.1770833F;
            this.txtCoveredMonth11_4.Left = 6.947917F;
            this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
            this.txtCoveredMonth11_4.Text = null;
            this.txtCoveredMonth11_4.Top = 8.906248F;
            this.txtCoveredMonth11_4.Width = 0.25F;
            // 
            // txtCoveredMonth12_4
            // 
            this.txtCoveredMonth12_4.Height = 0.1770833F;
            this.txtCoveredMonth12_4.Left = 7.239583F;
            this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
            this.txtCoveredMonth12_4.Text = null;
            this.txtCoveredMonth12_4.Top = 8.906248F;
            this.txtCoveredMonth12_4.Width = 0.1875F;
            // 
            // txtCoveredAll12_5
            // 
            this.txtCoveredAll12_5.Height = 0.1770833F;
            this.txtCoveredAll12_5.Left = 3.604167F;
            this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
            this.txtCoveredAll12_5.Text = null;
            this.txtCoveredAll12_5.Top = 9.284721F;
            this.txtCoveredAll12_5.Width = 0.25F;
            // 
            // txtCoveredMonth1_5
            // 
            this.txtCoveredMonth1_5.Height = 0.1770833F;
            this.txtCoveredMonth1_5.Left = 4F;
            this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
            this.txtCoveredMonth1_5.Text = null;
            this.txtCoveredMonth1_5.Top = 9.284721F;
            this.txtCoveredMonth1_5.Width = 0.1875F;
            // 
            // txtCoveredMonth2_5
            // 
            this.txtCoveredMonth2_5.Height = 0.1770833F;
            this.txtCoveredMonth2_5.Left = 4.291667F;
            this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
            this.txtCoveredMonth2_5.Text = null;
            this.txtCoveredMonth2_5.Top = 9.284721F;
            this.txtCoveredMonth2_5.Width = 0.25F;
            // 
            // txtCoveredMonth3_5
            // 
            this.txtCoveredMonth3_5.Height = 0.1770833F;
            this.txtCoveredMonth3_5.Left = 4.583333F;
            this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
            this.txtCoveredMonth3_5.Text = null;
            this.txtCoveredMonth3_5.Top = 9.281248F;
            this.txtCoveredMonth3_5.Width = 0.25F;
            // 
            // txtCoveredMonth4_5
            // 
            this.txtCoveredMonth4_5.Height = 0.1770833F;
            this.txtCoveredMonth4_5.Left = 4.885417F;
            this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
            this.txtCoveredMonth4_5.Text = null;
            this.txtCoveredMonth4_5.Top = 9.281248F;
            this.txtCoveredMonth4_5.Width = 0.25F;
            // 
            // txtCoveredMonth5_5
            // 
            this.txtCoveredMonth5_5.Height = 0.1770833F;
            this.txtCoveredMonth5_5.Left = 5.177083F;
            this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
            this.txtCoveredMonth5_5.Text = null;
            this.txtCoveredMonth5_5.Top = 9.284721F;
            this.txtCoveredMonth5_5.Width = 0.25F;
            // 
            // txtCoveredMonth6_5
            // 
            this.txtCoveredMonth6_5.Height = 0.1770833F;
            this.txtCoveredMonth6_5.Left = 5.46875F;
            this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
            this.txtCoveredMonth6_5.Text = null;
            this.txtCoveredMonth6_5.Top = 9.281248F;
            this.txtCoveredMonth6_5.Width = 0.25F;
            // 
            // txtCoveredMonth7_5
            // 
            this.txtCoveredMonth7_5.Height = 0.1770833F;
            this.txtCoveredMonth7_5.Left = 5.770833F;
            this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
            this.txtCoveredMonth7_5.Text = null;
            this.txtCoveredMonth7_5.Top = 9.281248F;
            this.txtCoveredMonth7_5.Width = 0.25F;
            // 
            // txtCoveredMonth8_5
            // 
            this.txtCoveredMonth8_5.Height = 0.1770833F;
            this.txtCoveredMonth8_5.Left = 6.0625F;
            this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
            this.txtCoveredMonth8_5.Text = null;
            this.txtCoveredMonth8_5.Top = 9.281248F;
            this.txtCoveredMonth8_5.Width = 0.25F;
            // 
            // txtCoveredMonth9_5
            // 
            this.txtCoveredMonth9_5.Height = 0.1770833F;
            this.txtCoveredMonth9_5.Left = 6.354167F;
            this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
            this.txtCoveredMonth9_5.Text = null;
            this.txtCoveredMonth9_5.Top = 9.281248F;
            this.txtCoveredMonth9_5.Width = 0.25F;
            // 
            // txtCoveredMonth10_5
            // 
            this.txtCoveredMonth10_5.Height = 0.1770833F;
            this.txtCoveredMonth10_5.Left = 6.65625F;
            this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
            this.txtCoveredMonth10_5.Text = null;
            this.txtCoveredMonth10_5.Top = 9.281248F;
            this.txtCoveredMonth10_5.Width = 0.25F;
            // 
            // txtCoveredMonth11_5
            // 
            this.txtCoveredMonth11_5.Height = 0.1770833F;
            this.txtCoveredMonth11_5.Left = 6.947917F;
            this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
            this.txtCoveredMonth11_5.Text = null;
            this.txtCoveredMonth11_5.Top = 9.281248F;
            this.txtCoveredMonth11_5.Width = 0.25F;
            // 
            // txtCoveredMonth12_5
            // 
            this.txtCoveredMonth12_5.Height = 0.1770833F;
            this.txtCoveredMonth12_5.Left = 7.239583F;
            this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
            this.txtCoveredMonth12_5.Text = null;
            this.txtCoveredMonth12_5.Top = 9.281248F;
            this.txtCoveredMonth12_5.Width = 0.1875F;
            // 
            // txtCoveredAll12_6
            // 
            this.txtCoveredAll12_6.Height = 0.1770833F;
            this.txtCoveredAll12_6.Left = 3.604167F;
            this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
            this.txtCoveredAll12_6.Text = null;
            this.txtCoveredAll12_6.Top = 9.649303F;
            this.txtCoveredAll12_6.Width = 0.25F;
            // 
            // txtCoveredMonth1_6
            // 
            this.txtCoveredMonth1_6.Height = 0.1770833F;
            this.txtCoveredMonth1_6.Left = 4F;
            this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
            this.txtCoveredMonth1_6.Text = null;
            this.txtCoveredMonth1_6.Top = 9.649303F;
            this.txtCoveredMonth1_6.Width = 0.1875F;
            // 
            // txtCoveredMonth2_6
            // 
            this.txtCoveredMonth2_6.Height = 0.1770833F;
            this.txtCoveredMonth2_6.Left = 4.291667F;
            this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
            this.txtCoveredMonth2_6.Text = null;
            this.txtCoveredMonth2_6.Top = 9.649303F;
            this.txtCoveredMonth2_6.Width = 0.25F;
            // 
            // txtCoveredMonth3_6
            // 
            this.txtCoveredMonth3_6.Height = 0.1770833F;
            this.txtCoveredMonth3_6.Left = 4.583333F;
            this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
            this.txtCoveredMonth3_6.Text = null;
            this.txtCoveredMonth3_6.Top = 9.645832F;
            this.txtCoveredMonth3_6.Width = 0.25F;
            // 
            // txtCoveredMonth4_6
            // 
            this.txtCoveredMonth4_6.Height = 0.1770833F;
            this.txtCoveredMonth4_6.Left = 4.885417F;
            this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
            this.txtCoveredMonth4_6.Text = null;
            this.txtCoveredMonth4_6.Top = 9.645832F;
            this.txtCoveredMonth4_6.Width = 0.25F;
            // 
            // txtCoveredMonth5_6
            // 
            this.txtCoveredMonth5_6.Height = 0.1770833F;
            this.txtCoveredMonth5_6.Left = 5.177083F;
            this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
            this.txtCoveredMonth5_6.Text = null;
            this.txtCoveredMonth5_6.Top = 9.649303F;
            this.txtCoveredMonth5_6.Width = 0.25F;
            // 
            // txtCoveredMonth6_6
            // 
            this.txtCoveredMonth6_6.Height = 0.1770833F;
            this.txtCoveredMonth6_6.Left = 5.46875F;
            this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
            this.txtCoveredMonth6_6.Text = null;
            this.txtCoveredMonth6_6.Top = 9.645832F;
            this.txtCoveredMonth6_6.Width = 0.25F;
            // 
            // txtCoveredMonth7_6
            // 
            this.txtCoveredMonth7_6.Height = 0.1770833F;
            this.txtCoveredMonth7_6.Left = 5.770833F;
            this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
            this.txtCoveredMonth7_6.Text = null;
            this.txtCoveredMonth7_6.Top = 9.645832F;
            this.txtCoveredMonth7_6.Width = 0.25F;
            // 
            // txtCoveredMonth8_6
            // 
            this.txtCoveredMonth8_6.Height = 0.1770833F;
            this.txtCoveredMonth8_6.Left = 6.0625F;
            this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
            this.txtCoveredMonth8_6.Text = null;
            this.txtCoveredMonth8_6.Top = 9.645832F;
            this.txtCoveredMonth8_6.Width = 0.25F;
            // 
            // txtCoveredMonth9_6
            // 
            this.txtCoveredMonth9_6.Height = 0.1770833F;
            this.txtCoveredMonth9_6.Left = 6.354167F;
            this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
            this.txtCoveredMonth9_6.Text = null;
            this.txtCoveredMonth9_6.Top = 9.645832F;
            this.txtCoveredMonth9_6.Width = 0.25F;
            // 
            // txtCoveredMonth10_6
            // 
            this.txtCoveredMonth10_6.Height = 0.1770833F;
            this.txtCoveredMonth10_6.Left = 6.65625F;
            this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
            this.txtCoveredMonth10_6.Text = null;
            this.txtCoveredMonth10_6.Top = 9.645832F;
            this.txtCoveredMonth10_6.Width = 0.25F;
            // 
            // txtCoveredMonth11_6
            // 
            this.txtCoveredMonth11_6.Height = 0.1770833F;
            this.txtCoveredMonth11_6.Left = 6.947917F;
            this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
            this.txtCoveredMonth11_6.Text = null;
            this.txtCoveredMonth11_6.Top = 9.645832F;
            this.txtCoveredMonth11_6.Width = 0.25F;
            // 
            // txtCoveredMonth12_6
            // 
            this.txtCoveredMonth12_6.Height = 0.1770833F;
            this.txtCoveredMonth12_6.Left = 7.239583F;
            this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
            this.txtCoveredMonth12_6.Text = null;
            this.txtCoveredMonth12_6.Top = 9.645832F;
            this.txtCoveredMonth12_6.Width = 0.1875F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 0.04166667F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-size: 8.5pt";
            this.txtName.Text = null;
            this.txtName.Top = 5.232637F;
            this.txtName.Width = 1.3125F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1770833F;
            this.txtSSN.Left = 4.0625F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Style = "font-size: 8.5pt";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 5.229165F;
            this.txtSSN.Width = 1.5F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1770833F;
            this.txtAddress.Left = 0.04166667F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 8.5pt";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 5.489582F;
            this.txtAddress.Width = 2.5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1770833F;
            this.txtCity.Left = 2.645833F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-size: 8.5pt";
            this.txtCity.Text = null;
            this.txtCity.Top = 5.489582F;
            this.txtCity.Width = 1.333333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1770833F;
            this.txtState.Left = 4.0625F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-size: 8.5pt";
            this.txtState.Text = null;
            this.txtState.Top = 5.489582F;
            this.txtState.Width = 1.5F;
            // 
            // txtOriginOfPolicy
            // 
            this.txtOriginOfPolicy.Height = 0.1770833F;
            this.txtOriginOfPolicy.Left = 3.8125F;
            this.txtOriginOfPolicy.Name = "txtOriginOfPolicy";
            this.txtOriginOfPolicy.Text = "X";
            this.txtOriginOfPolicy.Top = 5.718749F;
            this.txtOriginOfPolicy.Width = 0.25F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1770833F;
            this.txtPostalCode.Left = 5.6875F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Style = "font-size: 8.5pt";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 5.489582F;
            this.txtPostalCode.Width = 1.6875F;
            // 
            // txtProviderName
            // 
            this.txtProviderName.Height = 0.1770833F;
            this.txtProviderName.Left = 0.04166667F;
            this.txtProviderName.Name = "txtProviderName";
            this.txtProviderName.Style = "font-size: 8.5pt";
            this.txtProviderName.Text = null;
            this.txtProviderName.Top = 6.781249F;
            this.txtProviderName.Width = 4F;
            // 
            // txtProviderAddress
            // 
            this.txtProviderAddress.Height = 0.1770833F;
            this.txtProviderAddress.Left = 0.04166667F;
            this.txtProviderAddress.Name = "txtProviderAddress";
            this.txtProviderAddress.Style = "font-size: 8.5pt";
            this.txtProviderAddress.Text = null;
            this.txtProviderAddress.Top = 7.031249F;
            this.txtProviderAddress.Width = 2.583333F;
            // 
            // txtProviderCity
            // 
            this.txtProviderCity.Height = 0.1770833F;
            this.txtProviderCity.Left = 2.645833F;
            this.txtProviderCity.Name = "txtProviderCity";
            this.txtProviderCity.Style = "font-size: 8.5pt";
            this.txtProviderCity.Text = null;
            this.txtProviderCity.Top = 7.031249F;
            this.txtProviderCity.Width = 1.333333F;
            // 
            // txtProviderState
            // 
            this.txtProviderState.Height = 0.1770833F;
            this.txtProviderState.Left = 4.0625F;
            this.txtProviderState.Name = "txtProviderState";
            this.txtProviderState.Style = "font-size: 8.5pt";
            this.txtProviderState.Text = null;
            this.txtProviderState.Top = 7.031249F;
            this.txtProviderState.Width = 1.5F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1770833F;
            this.txtEIN.Left = 4.0625F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Style = "font-size: 8.5pt";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 6.781249F;
            this.txtEIN.Width = 1.5F;
            // 
            // txtProviderTelephone
            // 
            this.txtProviderTelephone.Height = 0.1770833F;
            this.txtProviderTelephone.Left = 5.6875F;
            this.txtProviderTelephone.Name = "txtProviderTelephone";
            this.txtProviderTelephone.Style = "font-size: 8.5pt";
            this.txtProviderTelephone.Text = null;
            this.txtProviderTelephone.Top = 6.781249F;
            this.txtProviderTelephone.Width = 1.75F;
            // 
            // txtProviderPostalCode
            // 
            this.txtProviderPostalCode.Height = 0.1770833F;
            this.txtProviderPostalCode.Left = 5.6875F;
            this.txtProviderPostalCode.Name = "txtProviderPostalCode";
            this.txtProviderPostalCode.Style = "font-size: 8.5pt";
            this.txtProviderPostalCode.Text = null;
            this.txtProviderPostalCode.Top = 7.031249F;
            this.txtProviderPostalCode.Width = 1.75F;
            // 
            // Image2
            // 
            this.Image2.Height = 0.1770833F;
            this.Image2.HyperLink = null;
            this.Image2.ImageData = ((System.IO.Stream)(resources.GetObject("Image2.ImageData")));
            this.Image2.Left = 6.604167F;
            this.Image2.LineWeight = 1F;
            this.Image2.Name = "Image2";
            this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image2.Top = 4.781249F;
            this.Image2.Width = 0.5416667F;
            // 
            // Label83
            // 
            this.Label83.Height = 0.25F;
            this.Label83.HyperLink = null;
            this.Label83.Left = 0.1944444F;
            this.Label83.Name = "Label83";
            this.Label83.Style = "font-family: \'Impact\'; font-size: 14.5pt";
            this.Label83.Text = "1095-B";
            this.Label83.Top = 4.586804F;
            this.Label83.Width = 0.7083333F;
            // 
            // Label62
            // 
            this.Label62.Height = 0.1145833F;
            this.Label62.HyperLink = null;
            this.Label62.Left = 0.3854167F;
            this.Label62.Name = "Label62";
            this.Label62.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label62.Text = "Information about Certain Employer-Sponsored Coverage";
            this.Label62.Top = 5.916665F;
            this.Label62.Width = 2.895833F;
            // 
            // Line44
            // 
            this.Line44.Height = 0.927083F;
            this.Line44.Left = 4.041667F;
            this.Line44.LineWeight = 0F;
            this.Line44.Name = "Line44";
            this.Line44.Top = 4.989582F;
            this.Line44.Width = 0F;
            this.Line44.X1 = 4.041667F;
            this.Line44.X2 = 4.041667F;
            this.Line44.Y1 = 4.989582F;
            this.Line44.Y2 = 5.916665F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.09375F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 0.02083333F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-size: 5pt; text-align: left";
            this.Label67.Text = "4 Street address (including apartment no.)";
            this.Label67.Top = 5.406249F;
            this.Label67.Width = 1.583333F;
            // 
            // Label69
            // 
            this.Label69.Height = 0.09375F;
            this.Label69.HyperLink = null;
            this.Label69.Left = 0.02083333F;
            this.Label69.Name = "Label69";
            this.Label69.Style = "font-size: 5pt; text-align: left";
            this.Label69.Text = "1 Name of responsible individual-First name, middle name, last name";
            this.Label69.Top = 5.156249F;
            this.Label69.Width = 2.458333F;
            // 
            // Line48
            // 
            this.Line48.Height = 0F;
            this.Line48.Left = 0F;
            this.Line48.LineWeight = 0F;
            this.Line48.Name = "Line48";
            this.Line48.Top = 5.374999F;
            this.Line48.Width = 7.46875F;
            this.Line48.X1 = 0F;
            this.Line48.X2 = 7.46875F;
            this.Line48.Y1 = 5.374999F;
            this.Line48.Y2 = 5.374999F;
            // 
            // Label77
            // 
            this.Label77.Height = 0.1354167F;
            this.Label77.HyperLink = null;
            this.Label77.Left = 0F;
            this.Label77.Name = "Label77";
            this.Label77.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label77.Text = "Part I";
            this.Label77.Top = 4.989582F;
            this.Label77.Width = 0.34375F;
            // 
            // Line49
            // 
            this.Line49.Height = 0F;
            this.Line49.Left = 0F;
            this.Line49.LineWeight = 0F;
            this.Line49.Name = "Line49";
            this.Line49.Top = 5.124999F;
            this.Line49.Width = 7.46875F;
            this.Line49.X1 = 0F;
            this.Line49.X2 = 7.46875F;
            this.Line49.Y1 = 5.124999F;
            this.Line49.Y2 = 5.124999F;
            // 
            // Label78
            // 
            this.Label78.Height = 0.1145833F;
            this.Label78.HyperLink = null;
            this.Label78.Left = 0.3854167F;
            this.Label78.Name = "Label78";
            this.Label78.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label78.Text = "Responsible Individual";
            this.Label78.Top = 5.003471F;
            this.Label78.Width = 1.333333F;
            // 
            // Line56
            // 
            this.Line56.Height = 0.3819451F;
            this.Line56.Left = 1.125F;
            this.Line56.LineWeight = 1F;
            this.Line56.Name = "Line56";
            this.Line56.Top = 4.607637F;
            this.Line56.Width = 0F;
            this.Line56.X1 = 1.125F;
            this.Line56.X2 = 1.125F;
            this.Line56.Y1 = 4.607637F;
            this.Line56.Y2 = 4.989582F;
            // 
            // Label80
            // 
            this.Label80.Height = 0.09375F;
            this.Label80.HyperLink = null;
            this.Label80.Left = 0F;
            this.Label80.Name = "Label80";
            this.Label80.Style = "font-size: 5pt; text-align: left";
            this.Label80.Text = "Form";
            this.Label80.Top = 4.708332F;
            this.Label80.Width = 0.3333333F;
            // 
            // Label81
            // 
            this.Label81.Height = 0.09375F;
            this.Label81.HyperLink = null;
            this.Label81.Left = 0F;
            this.Label81.Name = "Label81";
            this.Label81.Style = "font-size: 5pt; text-align: left";
            this.Label81.Text = "Department of the Treasury";
            this.Label81.Top = 4.802082F;
            this.Label81.Width = 0.9583333F;
            // 
            // Label82
            // 
            this.Label82.Height = 0.09375F;
            this.Label82.HyperLink = null;
            this.Label82.Left = 0F;
            this.Label82.Name = "Label82";
            this.Label82.Style = "font-size: 5pt; text-align: left";
            this.Label82.Text = "Internal Revenue Service";
            this.Label82.Top = 4.895832F;
            this.Label82.Width = 1.020833F;
            // 
            // Line55
            // 
            this.Line55.Height = 0F;
            this.Line55.Left = 0F;
            this.Line55.LineWeight = 1F;
            this.Line55.Name = "Line55";
            this.Line55.Top = 4.989582F;
            this.Line55.Width = 7.46875F;
            this.Line55.X1 = 0F;
            this.Line55.X2 = 7.46875F;
            this.Line55.Y1 = 4.989582F;
            this.Line55.Y2 = 4.989582F;
            // 
            // Label85
            // 
            this.Label85.Height = 0.1770833F;
            this.Label85.HyperLink = null;
            this.Label85.Left = 1.40625F;
            this.Label85.Name = "Label85";
            this.Label85.Style = "font-family: \'Franklin Gothic Medium\'; font-size: 10pt; font-weight: bold; text-a" +
    "lign: center";
            this.Label85.Text = "Health Coverage";
            this.Label85.Top = 4.51736F;
            this.Label85.Width = 3.833333F;
            // 
            // Shape83
            // 
            this.Shape83.Height = 0.1458333F;
            this.Shape83.Left = 5.5F;
            this.Shape83.LineWeight = 0F;
            this.Shape83.Name = "Shape83";
            this.Shape83.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape83.Top = 4.583332F;
            this.Shape83.Width = 0.1388889F;
            // 
            // Shape84
            // 
            this.Shape84.Height = 0.1458333F;
            this.Shape84.Left = 5.5F;
            this.Shape84.LineWeight = 0F;
            this.Shape84.Name = "Shape84";
            this.Shape84.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape84.Top = 4.791665F;
            this.Shape84.Width = 0.1388889F;
            // 
            // Line57
            // 
            this.Line57.Height = 0.385417F;
            this.Line57.Left = 6.34375F;
            this.Line57.LineWeight = 1F;
            this.Line57.Name = "Line57";
            this.Line57.Top = 4.604165F;
            this.Line57.Width = 0F;
            this.Line57.X1 = 6.34375F;
            this.Line57.X2 = 6.34375F;
            this.Line57.Y1 = 4.604165F;
            this.Line57.Y2 = 4.989582F;
            // 
            // Label86
            // 
            this.Label86.Height = 0.09375F;
            this.Label86.HyperLink = null;
            this.Label86.Left = 1.888889F;
            this.Label86.Name = "Label86";
            this.Label86.Style = "font-size: 6pt; font-weight: bold; text-align: left";
            this.Label86.Text = "Go to www.irs.gov/Form1095B for instructions and the latest information.";
            this.Label86.Top = 4.850693F;
            this.Label86.Width = 3.020833F;
            // 
            // Label88
            // 
            this.Label88.Height = 0.1354167F;
            this.Label88.HyperLink = null;
            this.Label88.Left = 5.645833F;
            this.Label88.Name = "Label88";
            this.Label88.Style = "font-size: 7pt";
            this.Label88.Text = "VOID";
            this.Label88.Top = 4.604165F;
            this.Label88.Width = 0.5625F;
            // 
            // Label89
            // 
            this.Label89.Height = 0.1354167F;
            this.Label89.HyperLink = null;
            this.Label89.Left = 5.645833F;
            this.Label89.Name = "Label89";
            this.Label89.Style = "font-size: 7pt";
            this.Label89.Text = "CORRECTED";
            this.Label89.Top = 4.809026F;
            this.Label89.Width = 0.8125F;
            // 
            // Line58
            // 
            this.Line58.Height = 0F;
            this.Line58.Left = 6.34375F;
            this.Line58.LineWeight = 0F;
            this.Line58.Name = "Line58";
            this.Line58.Top = 4.739582F;
            this.Line58.Width = 1.125F;
            this.Line58.X1 = 6.34375F;
            this.Line58.X2 = 7.46875F;
            this.Line58.Y1 = 4.739582F;
            this.Line58.Y2 = 4.739582F;
            // 
            // Label90
            // 
            this.Label90.Height = 0.09375F;
            this.Label90.HyperLink = null;
            this.Label90.Left = 6.520833F;
            this.Label90.Name = "Label90";
            this.Label90.Style = "font-size: 5.5pt; text-align: left";
            this.Label90.Text = "OMB No. 1545-2252";
            this.Label90.Top = 4.649304F;
            this.Label90.Width = 0.8333333F;
            // 
            // Label93
            // 
            this.Label93.Height = 0.1354167F;
            this.Label93.HyperLink = null;
            this.Label93.Left = 6.895833F;
            this.Label93.Name = "Label93";
            this.Label93.Style = "font-family: \'OCR A Extended\'; font-size: 8.5pt; text-align: right";
            this.Label93.Text = "560118";
            this.Label93.Top = 4.489582F;
            this.Label93.Width = 0.5625F;
            // 
            // Label95
            // 
            this.Label95.Height = 0.1145833F;
            this.Label95.HyperLink = null;
            this.Label95.Left = 1.75F;
            this.Label95.Name = "Label95";
            this.Label95.Style = "font-family: \'Marlett\'; font-size: 10pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label95.Text = "4";
            this.Label95.Top = 4.833332F;
            this.Label95.Width = 0.2708333F;
            // 
            // Shape77
            // 
            this.Shape77.Height = 0.1458333F;
            this.Shape77.Left = 3.8125F;
            this.Shape77.LineWeight = 0F;
            this.Shape77.Name = "Shape77";
            this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape77.Top = 5.725693F;
            this.Shape77.Width = 0.1354167F;
            // 
            // Line59
            // 
            this.Line59.Height = 0F;
            this.Line59.Left = 0F;
            this.Line59.LineWeight = 0F;
            this.Line59.Name = "Line59";
            this.Line59.Top = 5.656249F;
            this.Line59.Width = 7.46875F;
            this.Line59.X1 = 0F;
            this.Line59.X2 = 7.46875F;
            this.Line59.Y1 = 5.656249F;
            this.Line59.Y2 = 5.656249F;
            // 
            // Label96
            // 
            this.Label96.Height = 0.09375F;
            this.Label96.HyperLink = null;
            this.Label96.Left = 4.0625F;
            this.Label96.Name = "Label96";
            this.Label96.Style = "font-size: 5pt; text-align: left";
            this.Label96.Text = "2 Social security number (SSN or other TIN)";
            this.Label96.Top = 5.156249F;
            this.Label96.Width = 1.583333F;
            // 
            // Label97
            // 
            this.Label97.Height = 0.09375F;
            this.Label97.HyperLink = null;
            this.Label97.Left = 5.6875F;
            this.Label97.Name = "Label97";
            this.Label97.Style = "font-size: 5pt; text-align: left";
            this.Label97.Text = "3 Date of birth (If SSN is not available)";
            this.Label97.Top = 5.156249F;
            this.Label97.Width = 1.583333F;
            // 
            // Label98
            // 
            this.Label98.Height = 0.09375F;
            this.Label98.HyperLink = null;
            this.Label98.Left = 2.645833F;
            this.Label98.Name = "Label98";
            this.Label98.Style = "font-size: 5pt; text-align: left";
            this.Label98.Text = "5 City or town";
            this.Label98.Top = 5.406249F;
            this.Label98.Width = 1.583333F;
            // 
            // Label99
            // 
            this.Label99.Height = 0.09375F;
            this.Label99.HyperLink = null;
            this.Label99.Left = 4.0625F;
            this.Label99.Name = "Label99";
            this.Label99.Style = "font-size: 5pt; text-align: left";
            this.Label99.Text = "6 State or province";
            this.Label99.Top = 5.406249F;
            this.Label99.Width = 1.583333F;
            // 
            // Label100
            // 
            this.Label100.Height = 0.09375F;
            this.Label100.HyperLink = null;
            this.Label100.Left = 5.6875F;
            this.Label100.Name = "Label100";
            this.Label100.Style = "font-size: 5pt; text-align: left";
            this.Label100.Text = "7 Country and ZIP or foreign postal code";
            this.Label100.Top = 5.406249F;
            this.Label100.Width = 1.583333F;
            // 
            // Label101
            // 
            this.Label101.Height = 0.09375F;
            this.Label101.HyperLink = null;
            this.Label101.Left = 4.0625F;
            this.Label101.Name = "Label101";
            this.Label101.Style = "font-size: 5pt; text-align: left";
            this.Label101.Text = "9 Small Business Health Options Program (SHOP) Marketplace identifier, if applica" +
    "ble";
            this.Label101.Top = 5.687499F;
            this.Label101.Width = 2.958333F;
            // 
            // Label102
            // 
            this.Label102.Height = 0.09375F;
            this.Label102.HyperLink = null;
            this.Label102.Left = 0.02083333F;
            this.Label102.Name = "Label102";
            this.Label102.Style = "font-size: 6pt; text-align: left";
            this.Label102.Text = "8 Enter letter identifying Origin of the Policy (see instructions for codes):";
            this.Label102.Top = 5.749999F;
            this.Label102.Width = 3.020833F;
            // 
            // Label103
            // 
            this.Label103.Height = 0.1145833F;
            this.Label103.HyperLink = null;
            this.Label103.Left = 3.458333F;
            this.Label103.Name = "Label103";
            this.Label103.Style = "font-family: \'Marlett\'; font-size: 10pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label103.Text = "4";
            this.Label103.Top = 5.729165F;
            this.Label103.Width = 0.2708333F;
            // 
            // Label104
            // 
            this.Label104.Height = 0.1145833F;
            this.Label104.HyperLink = null;
            this.Label104.Left = 3.135417F;
            this.Label104.Name = "Label104";
            this.Label104.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label104.Text = "(see instructions)";
            this.Label104.Top = 5.916665F;
            this.Label104.Width = 1.520833F;
            // 
            // Line61
            // 
            this.Line61.Height = 0F;
            this.Line61.Left = 0F;
            this.Line61.LineWeight = 0F;
            this.Line61.Name = "Line61";
            this.Line61.Top = 6.041665F;
            this.Line61.Width = 7.46875F;
            this.Line61.X1 = 0F;
            this.Line61.X2 = 7.46875F;
            this.Line61.Y1 = 6.041665F;
            this.Line61.Y2 = 6.041665F;
            // 
            // Line62
            // 
            this.Line62.Height = 0.53125F;
            this.Line62.Left = 5.625F;
            this.Line62.LineWeight = 0F;
            this.Line62.Name = "Line62";
            this.Line62.Top = 5.124999F;
            this.Line62.Width = 0F;
            this.Line62.X1 = 5.625F;
            this.Line62.X2 = 5.625F;
            this.Line62.Y1 = 5.124999F;
            this.Line62.Y2 = 5.656249F;
            // 
            // Label106
            // 
            this.Label106.Height = 0.1145833F;
            this.Label106.HyperLink = null;
            this.Label106.Left = 0.3854167F;
            this.Label106.Name = "Label106";
            this.Label106.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label106.Text = "Issuer or Other Coverage Provider";
            this.Label106.Top = 6.541665F;
            this.Label106.Width = 1.770833F;
            // 
            // Line63
            // 
            this.Line63.Height = 0F;
            this.Line63.Left = 0F;
            this.Line63.LineWeight = 0F;
            this.Line63.Name = "Line63";
            this.Line63.Top = 6.670137F;
            this.Line63.Width = 7.46875F;
            this.Line63.X1 = 0F;
            this.Line63.X2 = 7.46875F;
            this.Line63.Y1 = 6.670137F;
            this.Line63.Y2 = 6.670137F;
            // 
            // Label107
            // 
            this.Label107.Height = 0.1145833F;
            this.Label107.HyperLink = null;
            this.Label107.Left = 2.072917F;
            this.Label107.Name = "Label107";
            this.Label107.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label107.Text = "(see instructions)";
            this.Label107.Top = 6.541665F;
            this.Label107.Width = 1.520833F;
            // 
            // Line64
            // 
            this.Line64.Height = 0F;
            this.Line64.Left = 0F;
            this.Line64.LineWeight = 0F;
            this.Line64.Name = "Line64";
            this.Line64.Top = 6.326387F;
            this.Line64.Width = 7.46875F;
            this.Line64.X1 = 0F;
            this.Line64.X2 = 7.46875F;
            this.Line64.Y1 = 6.326387F;
            this.Line64.Y2 = 6.326387F;
            // 
            // Label108
            // 
            this.Label108.Height = 0.09375F;
            this.Label108.HyperLink = null;
            this.Label108.Left = 0F;
            this.Label108.Name = "Label108";
            this.Label108.Style = "font-size: 5pt; text-align: left";
            this.Label108.Text = "16 Name";
            this.Label108.Top = 6.687499F;
            this.Label108.Width = 1.583333F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.09375F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.572917F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 5.5pt; text-align: center";
            this.Label14.Text = "Oct";
            this.Label14.Top = 7.614582F;
            this.Label14.Width = 0.2951389F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.09375F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 6.277778F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 5.5pt; text-align: center";
            this.Label13.Text = "Sep";
            this.Label13.Top = 7.614582F;
            this.Label13.Width = 0.2951389F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.09375F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 5.097222F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 5.5pt; text-align: center";
            this.Label9.Text = "May";
            this.Label9.Top = 7.614582F;
            this.Label9.Width = 0.2951389F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.1458333F;
            this.Shape2.Left = 3.996528F;
            this.Shape2.LineWeight = 0F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = 7.788193F;
            this.Shape2.Width = 0.1388889F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.15625F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.958333F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 5.5pt; text-align: center";
            this.Label4.Text = "(e) Months of Coverage";
            this.Label4.Top = 7.406249F;
            this.Label4.Width = 3.458333F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 0F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 7.305554F;
            this.Line1.Width = 7.46875F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.46875F;
            this.Line1.Y1 = 7.305554F;
            this.Line1.Y2 = 7.305554F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 0F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 7.718749F;
            this.Line2.Width = 7.46875F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.46875F;
            this.Line2.Y1 = 7.718749F;
            this.Line2.Y2 = 7.718749F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 0F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 8.031248F;
            this.Line3.Width = 7.46875F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.46875F;
            this.Line3.Y1 = 8.031248F;
            this.Line3.Y2 = 8.031248F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 0F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 8.406248F;
            this.Line4.Width = 7.46875F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.46875F;
            this.Line4.Y1 = 8.406248F;
            this.Line4.Y2 = 8.406248F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 0F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 8.781248F;
            this.Line5.Width = 7.46875F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 7.46875F;
            this.Line5.Y1 = 8.781248F;
            this.Line5.Y2 = 8.781248F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 0F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 9.156248F;
            this.Line6.Width = 7.46875F;
            this.Line6.X1 = 0F;
            this.Line6.X2 = 7.46875F;
            this.Line6.Y1 = 9.156248F;
            this.Line6.Y2 = 9.156248F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0F;
            this.Line7.LineWeight = 0F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 9.531248F;
            this.Line7.Width = 7.447917F;
            this.Line7.X1 = 0F;
            this.Line7.X2 = 7.447917F;
            this.Line7.Y1 = 9.531248F;
            this.Line7.Y2 = 9.531248F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 0F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 9.906248F;
            this.Line8.Width = 7.46875F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 7.46875F;
            this.Line8.Y1 = 9.906248F;
            this.Line8.Y2 = 9.906248F;
            // 
            // Line9
            // 
            this.Line9.Height = 2.600694F;
            this.Line9.Left = 3.4375F;
            this.Line9.LineWeight = 0F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 7.305554F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 3.4375F;
            this.Line9.X2 = 3.4375F;
            this.Line9.Y1 = 7.305554F;
            this.Line9.Y2 = 9.906248F;
            // 
            // Line14
            // 
            this.Line14.Height = 2.333333F;
            this.Line14.Left = 5.097222F;
            this.Line14.LineWeight = 0F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 7.572915F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 5.097222F;
            this.Line14.X2 = 5.097222F;
            this.Line14.Y1 = 7.572915F;
            this.Line14.Y2 = 9.906248F;
            // 
            // Line18
            // 
            this.Line18.Height = 2.333333F;
            this.Line18.Left = 6.277778F;
            this.Line18.LineWeight = 0F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 7.572915F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 6.277778F;
            this.Line18.X2 = 6.277778F;
            this.Line18.Y1 = 7.572915F;
            this.Line18.Y2 = 9.906248F;
            // 
            // Line19
            // 
            this.Line19.Height = 2.333333F;
            this.Line19.Left = 6.572917F;
            this.Line19.LineWeight = 0F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 7.572915F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 6.572917F;
            this.Line19.X2 = 6.572917F;
            this.Line19.Y1 = 7.572915F;
            this.Line19.Y2 = 9.906248F;
            // 
            // Line22
            // 
            this.Line22.Height = 2.600694F;
            this.Line22.Left = 2.6875F;
            this.Line22.LineWeight = 0F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 7.305554F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 2.6875F;
            this.Line22.X2 = 2.6875F;
            this.Line22.Y1 = 7.305554F;
            this.Line22.Y2 = 9.906248F;
            // 
            // Line23
            // 
            this.Line23.Height = 2.600694F;
            this.Line23.Left = 1.75F;
            this.Line23.LineWeight = 0F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 7.305554F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 1.75F;
            this.Line23.X2 = 1.75F;
            this.Line23.Y1 = 7.305554F;
            this.Line23.Y2 = 9.906248F;
            // 
            // lbl12Months
            // 
            this.lbl12Months.Height = 0.28125F;
            this.lbl12Months.HyperLink = null;
            this.lbl12Months.Left = 3.447917F;
            this.lbl12Months.Name = "lbl12Months";
            this.lbl12Months.Style = "font-size: 5.5pt; text-align: center";
            this.lbl12Months.Text = "(d) Covered all 12 months";
            this.lbl12Months.Top = 7.406249F;
            this.lbl12Months.Width = 0.4583333F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.28125F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.701389F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 5.5pt; text-align: center";
            this.Label1.Text = "(c) DOB (If SSN or other TIN is not available)";
            this.Label1.Top = 7.406249F;
            this.Label1.Width = 0.7777778F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.15625F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1.8125F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 5.5pt; text-align: center";
            this.Label2.Text = "(b) SSN";
            this.Label2.Top = 7.406249F;
            this.Label2.Width = 0.8333333F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.21875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.25F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 5.5pt; text-align: center";
            this.Label3.Text = "(a) Name of covered individual(s) First name, middle initial, last name";
            this.Label3.Top = 7.406249F;
            this.Label3.Width = 1.270833F;
            // 
            // Line24
            // 
            this.Line24.Height = 0F;
            this.Line24.Left = 3.916667F;
            this.Line24.LineWeight = 0F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 7.572915F;
            this.Line24.Width = 3.552083F;
            this.Line24.X1 = 3.916667F;
            this.Line24.X2 = 7.46875F;
            this.Line24.Y1 = 7.572915F;
            this.Line24.Y2 = 7.572915F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.09375F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.916667F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 5.5pt; text-align: center";
            this.Label5.Text = "Jan";
            this.Label5.Top = 7.614582F;
            this.Label5.Width = 0.2951389F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.09375F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 4.211805F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 5.5pt; text-align: center";
            this.Label6.Text = "Feb";
            this.Label6.Top = 7.614582F;
            this.Label6.Width = 0.2951389F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.09375F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.506945F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 5.5pt; text-align: center";
            this.Label7.Text = "Mar";
            this.Label7.Top = 7.614582F;
            this.Label7.Width = 0.2951389F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.09375F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 4.802083F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 5.5pt; text-align: center";
            this.Label8.Text = "Apr";
            this.Label8.Top = 7.614582F;
            this.Label8.Width = 0.2951389F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.09375F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 5.392361F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 5.5pt; text-align: center";
            this.Label10.Text = "Jun";
            this.Label10.Top = 7.614582F;
            this.Label10.Width = 0.2951389F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.09375F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.6875F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 5.5pt; text-align: center";
            this.Label11.Text = "July";
            this.Label11.Top = 7.614582F;
            this.Label11.Width = 0.2951389F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.09375F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 5.982639F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 5.5pt; text-align: center";
            this.Label12.Text = "Aug";
            this.Label12.Top = 7.614582F;
            this.Label12.Width = 0.2951389F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.09375F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 6.868055F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 5.5pt; text-align: center";
            this.Label15.Text = "Nov";
            this.Label15.Top = 7.614582F;
            this.Label15.Width = 0.2951389F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.09375F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 7.166667F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 5.5pt; text-align: center";
            this.Label16.Text = "Dec";
            this.Label16.Top = 7.614582F;
            this.Label16.Width = 0.2951389F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.09375F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.04166667F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-size: 6pt; font-weight: bold; text-align: left";
            this.Label17.Text = "RAA #1607  For Privacy Act and Paperwork Reduction Act Notice, see separate instr" +
    "uctions.";
            this.Label17.Top = 9.958332F;
            this.Label17.Width = 3.708333F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.09375F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 4.708333F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 6pt; text-align: left";
            this.Label18.Text = "41-0852411";
            this.Label18.Top = 9.958332F;
            this.Label18.Width = 0.6458333F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.09375F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 5.895833F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-size: 6pt; text-align: left";
            this.Label19.Text = "1095B";
            this.Label19.Top = 9.958332F;
            this.Label19.Width = 0.3958333F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.09375F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 6.447917F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-size: 6pt; text-align: left";
            this.Label20.Text = "Form";
            this.Label20.Top = 9.958332F;
            this.Label20.Width = 0.2708333F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.147F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 6.677083F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-size: 7pt; font-weight: bold; text-align: left; vertical-align: middle";
            this.Label21.Text = "1095-B";
            this.Label21.Top = 9.958F;
            this.Label21.Width = 0.4583333F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.09375F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 7.013889F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-size: 6pt; text-align: left";
            this.Label22.Text = "(2020)";
            this.Label22.Top = 9.958332F;
            this.Label22.Width = 0.3333333F;
            // 
            // Shape5
            // 
            this.Shape5.Height = 0.1458333F;
            this.Shape5.Left = 3.996528F;
            this.Shape5.LineWeight = 0F;
            this.Shape5.Name = "Shape5";
            this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape5.Top = 8.156248F;
            this.Shape5.Width = 0.1354167F;
            // 
            // Shape6
            // 
            this.Shape6.Height = 0.1458333F;
            this.Shape6.Left = 3.996528F;
            this.Shape6.LineWeight = 0F;
            this.Shape6.Name = "Shape6";
            this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape6.Top = 8.541666F;
            this.Shape6.Width = 0.1354167F;
            // 
            // Shape7
            // 
            this.Shape7.Height = 0.1458333F;
            this.Shape7.Left = 3.996528F;
            this.Shape7.LineWeight = 0F;
            this.Shape7.Name = "Shape7";
            this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape7.Top = 8.916666F;
            this.Shape7.Width = 0.1354167F;
            // 
            // Shape8
            // 
            this.Shape8.Height = 0.1458333F;
            this.Shape8.Left = 3.996528F;
            this.Shape8.LineWeight = 0F;
            this.Shape8.Name = "Shape8";
            this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape8.Top = 9.291666F;
            this.Shape8.Width = 0.1354167F;
            // 
            // Shape9
            // 
            this.Shape9.Height = 0.1458333F;
            this.Shape9.Left = 4F;
            this.Shape9.LineWeight = 0F;
            this.Shape9.Name = "Shape9";
            this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape9.Top = 9.656248F;
            this.Shape9.Width = 0.1354167F;
            // 
            // Shape10
            // 
            this.Shape10.Height = 0.1458333F;
            this.Shape10.Left = 4.291667F;
            this.Shape10.LineWeight = 0F;
            this.Shape10.Name = "Shape10";
            this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape10.Top = 8.166666F;
            this.Shape10.Width = 0.1354167F;
            // 
            // Shape11
            // 
            this.Shape11.Height = 0.1458333F;
            this.Shape11.Left = 4.291667F;
            this.Shape11.LineWeight = 0F;
            this.Shape11.Name = "Shape11";
            this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape11.Top = 8.166666F;
            this.Shape11.Width = 0.1354167F;
            // 
            // Shape12
            // 
            this.Shape12.Height = 0.1458333F;
            this.Shape12.Left = 4.291667F;
            this.Shape12.LineWeight = 0F;
            this.Shape12.Name = "Shape12";
            this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape12.Top = 8.541666F;
            this.Shape12.Width = 0.1354167F;
            // 
            // Shape13
            // 
            this.Shape13.Height = 0.1458333F;
            this.Shape13.Left = 4.291667F;
            this.Shape13.LineWeight = 0F;
            this.Shape13.Name = "Shape13";
            this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape13.Top = 7.791665F;
            this.Shape13.Width = 0.1354167F;
            // 
            // Shape14
            // 
            this.Shape14.Height = 0.1458333F;
            this.Shape14.Left = 4.291667F;
            this.Shape14.LineWeight = 0F;
            this.Shape14.Name = "Shape14";
            this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape14.Top = 8.916666F;
            this.Shape14.Width = 0.1354167F;
            // 
            // Shape15
            // 
            this.Shape15.Height = 0.1458333F;
            this.Shape15.Left = 4.291667F;
            this.Shape15.LineWeight = 0F;
            this.Shape15.Name = "Shape15";
            this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape15.Top = 9.291666F;
            this.Shape15.Width = 0.1354167F;
            // 
            // Shape16
            // 
            this.Shape16.Height = 0.1458333F;
            this.Shape16.Left = 4.291667F;
            this.Shape16.LineWeight = 0F;
            this.Shape16.Name = "Shape16";
            this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape16.Top = 9.656248F;
            this.Shape16.Width = 0.1354167F;
            // 
            // Shape17
            // 
            this.Shape17.Height = 0.1458333F;
            this.Shape17.Left = 4.586805F;
            this.Shape17.LineWeight = 0F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 7.791665F;
            this.Shape17.Width = 0.1354167F;
            // 
            // Shape18
            // 
            this.Shape18.Height = 0.1458333F;
            this.Shape18.Left = 4.586805F;
            this.Shape18.LineWeight = 0F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 8.166666F;
            this.Shape18.Width = 0.1354167F;
            // 
            // Shape19
            // 
            this.Shape19.Height = 0.1458333F;
            this.Shape19.Left = 4.586805F;
            this.Shape19.LineWeight = 0F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 8.541666F;
            this.Shape19.Width = 0.1354167F;
            // 
            // Shape20
            // 
            this.Shape20.Height = 0.1458333F;
            this.Shape20.Left = 4.586805F;
            this.Shape20.LineWeight = 0F;
            this.Shape20.Name = "Shape20";
            this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape20.Top = 8.916666F;
            this.Shape20.Width = 0.1354167F;
            // 
            // Shape21
            // 
            this.Shape21.Height = 0.1458333F;
            this.Shape21.Left = 4.586805F;
            this.Shape21.LineWeight = 0F;
            this.Shape21.Name = "Shape21";
            this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape21.Top = 9.291666F;
            this.Shape21.Width = 0.1354167F;
            // 
            // Shape22
            // 
            this.Shape22.Height = 0.1458333F;
            this.Shape22.Left = 4.586805F;
            this.Shape22.LineWeight = 0F;
            this.Shape22.Name = "Shape22";
            this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape22.Top = 9.656248F;
            this.Shape22.Width = 0.1354167F;
            // 
            // Shape23
            // 
            this.Shape23.Height = 0.1458333F;
            this.Shape23.Left = 4.881945F;
            this.Shape23.LineWeight = 0F;
            this.Shape23.Name = "Shape23";
            this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape23.Top = 7.791665F;
            this.Shape23.Width = 0.1354167F;
            // 
            // Shape24
            // 
            this.Shape24.Height = 0.1458333F;
            this.Shape24.Left = 4.881945F;
            this.Shape24.LineWeight = 0F;
            this.Shape24.Name = "Shape24";
            this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape24.Top = 8.166666F;
            this.Shape24.Width = 0.1354167F;
            // 
            // Shape25
            // 
            this.Shape25.Height = 0.1458333F;
            this.Shape25.Left = 4.881945F;
            this.Shape25.LineWeight = 0F;
            this.Shape25.Name = "Shape25";
            this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape25.Top = 8.541666F;
            this.Shape25.Width = 0.1354167F;
            // 
            // Shape26
            // 
            this.Shape26.Height = 0.1458333F;
            this.Shape26.Left = 4.881945F;
            this.Shape26.LineWeight = 0F;
            this.Shape26.Name = "Shape26";
            this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape26.Top = 8.916666F;
            this.Shape26.Width = 0.1354167F;
            // 
            // Shape27
            // 
            this.Shape27.Height = 0.1458333F;
            this.Shape27.Left = 4.881945F;
            this.Shape27.LineWeight = 0F;
            this.Shape27.Name = "Shape27";
            this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape27.Top = 9.291666F;
            this.Shape27.Width = 0.1354167F;
            // 
            // Shape28
            // 
            this.Shape28.Height = 0.1458333F;
            this.Shape28.Left = 4.881945F;
            this.Shape28.LineWeight = 0F;
            this.Shape28.Name = "Shape28";
            this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape28.Top = 9.656248F;
            this.Shape28.Width = 0.1354167F;
            // 
            // Shape29
            // 
            this.Shape29.Height = 0.1458333F;
            this.Shape29.Left = 5.177083F;
            this.Shape29.LineWeight = 0F;
            this.Shape29.Name = "Shape29";
            this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape29.Top = 7.791665F;
            this.Shape29.Width = 0.1354167F;
            // 
            // Shape30
            // 
            this.Shape30.Height = 0.1458333F;
            this.Shape30.Left = 5.177083F;
            this.Shape30.LineWeight = 0F;
            this.Shape30.Name = "Shape30";
            this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape30.Top = 8.166666F;
            this.Shape30.Width = 0.1354167F;
            // 
            // Shape31
            // 
            this.Shape31.Height = 0.1458333F;
            this.Shape31.Left = 5.177083F;
            this.Shape31.LineWeight = 0F;
            this.Shape31.Name = "Shape31";
            this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape31.Top = 8.541666F;
            this.Shape31.Width = 0.1354167F;
            // 
            // Shape32
            // 
            this.Shape32.Height = 0.1458333F;
            this.Shape32.Left = 5.177083F;
            this.Shape32.LineWeight = 0F;
            this.Shape32.Name = "Shape32";
            this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape32.Top = 8.916666F;
            this.Shape32.Width = 0.1354167F;
            // 
            // Shape33
            // 
            this.Shape33.Height = 0.1458333F;
            this.Shape33.Left = 5.177083F;
            this.Shape33.LineWeight = 0F;
            this.Shape33.Name = "Shape33";
            this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape33.Top = 9.291666F;
            this.Shape33.Width = 0.1354167F;
            // 
            // Shape34
            // 
            this.Shape34.Height = 0.1458333F;
            this.Shape34.Left = 5.177083F;
            this.Shape34.LineWeight = 0F;
            this.Shape34.Name = "Shape34";
            this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape34.Top = 9.656248F;
            this.Shape34.Width = 0.1354167F;
            // 
            // Shape35
            // 
            this.Shape35.Height = 0.1458333F;
            this.Shape35.Left = 5.472222F;
            this.Shape35.LineWeight = 0F;
            this.Shape35.Name = "Shape35";
            this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape35.Top = 7.791665F;
            this.Shape35.Width = 0.1354167F;
            // 
            // Shape36
            // 
            this.Shape36.Height = 0.1458333F;
            this.Shape36.Left = 5.472222F;
            this.Shape36.LineWeight = 0F;
            this.Shape36.Name = "Shape36";
            this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape36.Top = 8.166666F;
            this.Shape36.Width = 0.1354167F;
            // 
            // Shape37
            // 
            this.Shape37.Height = 0.1458333F;
            this.Shape37.Left = 5.472222F;
            this.Shape37.LineWeight = 0F;
            this.Shape37.Name = "Shape37";
            this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape37.Top = 8.541666F;
            this.Shape37.Width = 0.1354167F;
            // 
            // Shape38
            // 
            this.Shape38.Height = 0.1458333F;
            this.Shape38.Left = 5.472222F;
            this.Shape38.LineWeight = 0F;
            this.Shape38.Name = "Shape38";
            this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape38.Top = 8.916666F;
            this.Shape38.Width = 0.1354167F;
            // 
            // Shape39
            // 
            this.Shape39.Height = 0.1458333F;
            this.Shape39.Left = 5.472222F;
            this.Shape39.LineWeight = 0F;
            this.Shape39.Name = "Shape39";
            this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape39.Top = 9.291666F;
            this.Shape39.Width = 0.1354167F;
            // 
            // Shape40
            // 
            this.Shape40.Height = 0.1458333F;
            this.Shape40.Left = 5.472222F;
            this.Shape40.LineWeight = 0F;
            this.Shape40.Name = "Shape40";
            this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape40.Top = 9.656248F;
            this.Shape40.Width = 0.1354167F;
            // 
            // Shape41
            // 
            this.Shape41.Height = 0.1458333F;
            this.Shape41.Left = 5.767361F;
            this.Shape41.LineWeight = 0F;
            this.Shape41.Name = "Shape41";
            this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape41.Top = 7.791665F;
            this.Shape41.Width = 0.1354167F;
            // 
            // Shape42
            // 
            this.Shape42.Height = 0.1458333F;
            this.Shape42.Left = 5.767361F;
            this.Shape42.LineWeight = 0F;
            this.Shape42.Name = "Shape42";
            this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape42.Top = 8.166666F;
            this.Shape42.Width = 0.1354167F;
            // 
            // Shape43
            // 
            this.Shape43.Height = 0.1458333F;
            this.Shape43.Left = 5.767361F;
            this.Shape43.LineWeight = 0F;
            this.Shape43.Name = "Shape43";
            this.Shape43.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape43.Top = 8.541666F;
            this.Shape43.Width = 0.1354167F;
            // 
            // Shape44
            // 
            this.Shape44.Height = 0.1458333F;
            this.Shape44.Left = 5.767361F;
            this.Shape44.LineWeight = 0F;
            this.Shape44.Name = "Shape44";
            this.Shape44.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape44.Top = 8.916666F;
            this.Shape44.Width = 0.1354167F;
            // 
            // Shape45
            // 
            this.Shape45.Height = 0.1458333F;
            this.Shape45.Left = 5.767361F;
            this.Shape45.LineWeight = 0F;
            this.Shape45.Name = "Shape45";
            this.Shape45.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape45.Top = 9.291666F;
            this.Shape45.Width = 0.1354167F;
            // 
            // Shape46
            // 
            this.Shape46.Height = 0.1458333F;
            this.Shape46.Left = 5.767361F;
            this.Shape46.LineWeight = 0F;
            this.Shape46.Name = "Shape46";
            this.Shape46.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape46.Top = 9.656248F;
            this.Shape46.Width = 0.1354167F;
            // 
            // Shape47
            // 
            this.Shape47.Height = 0.1458333F;
            this.Shape47.Left = 6.0625F;
            this.Shape47.LineWeight = 0F;
            this.Shape47.Name = "Shape47";
            this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape47.Top = 7.791665F;
            this.Shape47.Width = 0.1354167F;
            // 
            // Shape48
            // 
            this.Shape48.Height = 0.1458333F;
            this.Shape48.Left = 6.0625F;
            this.Shape48.LineWeight = 0F;
            this.Shape48.Name = "Shape48";
            this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape48.Top = 8.166666F;
            this.Shape48.Width = 0.1354167F;
            // 
            // Shape49
            // 
            this.Shape49.Height = 0.1458333F;
            this.Shape49.Left = 6.0625F;
            this.Shape49.LineWeight = 0F;
            this.Shape49.Name = "Shape49";
            this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape49.Top = 8.541666F;
            this.Shape49.Width = 0.1354167F;
            // 
            // Shape50
            // 
            this.Shape50.Height = 0.1458333F;
            this.Shape50.Left = 6.0625F;
            this.Shape50.LineWeight = 0F;
            this.Shape50.Name = "Shape50";
            this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape50.Top = 8.916666F;
            this.Shape50.Width = 0.1354167F;
            // 
            // Shape51
            // 
            this.Shape51.Height = 0.1458333F;
            this.Shape51.Left = 6.0625F;
            this.Shape51.LineWeight = 0F;
            this.Shape51.Name = "Shape51";
            this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape51.Top = 9.291666F;
            this.Shape51.Width = 0.1354167F;
            // 
            // Shape52
            // 
            this.Shape52.Height = 0.1458333F;
            this.Shape52.Left = 6.0625F;
            this.Shape52.LineWeight = 0F;
            this.Shape52.Name = "Shape52";
            this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape52.Top = 9.656248F;
            this.Shape52.Width = 0.1354167F;
            // 
            // Shape53
            // 
            this.Shape53.Height = 0.1458333F;
            this.Shape53.Left = 6.357639F;
            this.Shape53.LineWeight = 0F;
            this.Shape53.Name = "Shape53";
            this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape53.Top = 7.791665F;
            this.Shape53.Width = 0.1354167F;
            // 
            // Shape54
            // 
            this.Shape54.Height = 0.1458333F;
            this.Shape54.Left = 6.357639F;
            this.Shape54.LineWeight = 0F;
            this.Shape54.Name = "Shape54";
            this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape54.Top = 8.166666F;
            this.Shape54.Width = 0.1354167F;
            // 
            // Shape55
            // 
            this.Shape55.Height = 0.1458333F;
            this.Shape55.Left = 6.357639F;
            this.Shape55.LineWeight = 0F;
            this.Shape55.Name = "Shape55";
            this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape55.Top = 8.541666F;
            this.Shape55.Width = 0.1354167F;
            // 
            // Shape56
            // 
            this.Shape56.Height = 0.1458333F;
            this.Shape56.Left = 6.357639F;
            this.Shape56.LineWeight = 0F;
            this.Shape56.Name = "Shape56";
            this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape56.Top = 8.916666F;
            this.Shape56.Width = 0.1354167F;
            // 
            // Shape57
            // 
            this.Shape57.Height = 0.1458333F;
            this.Shape57.Left = 6.357639F;
            this.Shape57.LineWeight = 0F;
            this.Shape57.Name = "Shape57";
            this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape57.Top = 9.291666F;
            this.Shape57.Width = 0.1354167F;
            // 
            // Shape58
            // 
            this.Shape58.Height = 0.1458333F;
            this.Shape58.Left = 6.357639F;
            this.Shape58.LineWeight = 0F;
            this.Shape58.Name = "Shape58";
            this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape58.Top = 9.656248F;
            this.Shape58.Width = 0.1354167F;
            // 
            // Shape59
            // 
            this.Shape59.Height = 0.1458333F;
            this.Shape59.Left = 6.652778F;
            this.Shape59.LineWeight = 0F;
            this.Shape59.Name = "Shape59";
            this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape59.Top = 7.791665F;
            this.Shape59.Width = 0.1354167F;
            // 
            // Shape60
            // 
            this.Shape60.Height = 0.1458333F;
            this.Shape60.Left = 6.652778F;
            this.Shape60.LineWeight = 0F;
            this.Shape60.Name = "Shape60";
            this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape60.Top = 8.166666F;
            this.Shape60.Width = 0.1354167F;
            // 
            // Shape61
            // 
            this.Shape61.Height = 0.1458333F;
            this.Shape61.Left = 6.652778F;
            this.Shape61.LineWeight = 0F;
            this.Shape61.Name = "Shape61";
            this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape61.Top = 8.541666F;
            this.Shape61.Width = 0.1354167F;
            // 
            // Shape62
            // 
            this.Shape62.Height = 0.1458333F;
            this.Shape62.Left = 6.652778F;
            this.Shape62.LineWeight = 0F;
            this.Shape62.Name = "Shape62";
            this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape62.Top = 8.916666F;
            this.Shape62.Width = 0.1354167F;
            // 
            // Shape63
            // 
            this.Shape63.Height = 0.1458333F;
            this.Shape63.Left = 6.652778F;
            this.Shape63.LineWeight = 0F;
            this.Shape63.Name = "Shape63";
            this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape63.Top = 9.291666F;
            this.Shape63.Width = 0.1354167F;
            // 
            // Shape64
            // 
            this.Shape64.Height = 0.1458333F;
            this.Shape64.Left = 6.652778F;
            this.Shape64.LineWeight = 0F;
            this.Shape64.Name = "Shape64";
            this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape64.Top = 9.656248F;
            this.Shape64.Width = 0.1354167F;
            // 
            // Shape65
            // 
            this.Shape65.Height = 0.1458333F;
            this.Shape65.Left = 6.947917F;
            this.Shape65.LineWeight = 0F;
            this.Shape65.Name = "Shape65";
            this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape65.Top = 7.791665F;
            this.Shape65.Width = 0.1354167F;
            // 
            // Shape66
            // 
            this.Shape66.Height = 0.1458333F;
            this.Shape66.Left = 6.947917F;
            this.Shape66.LineWeight = 0F;
            this.Shape66.Name = "Shape66";
            this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape66.Top = 8.156248F;
            this.Shape66.Width = 0.1354167F;
            // 
            // Shape67
            // 
            this.Shape67.Height = 0.1458333F;
            this.Shape67.Left = 6.947917F;
            this.Shape67.LineWeight = 0F;
            this.Shape67.Name = "Shape67";
            this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape67.Top = 8.541666F;
            this.Shape67.Width = 0.1354167F;
            // 
            // Shape68
            // 
            this.Shape68.Height = 0.1458333F;
            this.Shape68.Left = 6.947917F;
            this.Shape68.LineWeight = 0F;
            this.Shape68.Name = "Shape68";
            this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape68.Top = 8.916666F;
            this.Shape68.Width = 0.1354167F;
            // 
            // Shape69
            // 
            this.Shape69.Height = 0.1458333F;
            this.Shape69.Left = 6.947917F;
            this.Shape69.LineWeight = 0F;
            this.Shape69.Name = "Shape69";
            this.Shape69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape69.Top = 9.291666F;
            this.Shape69.Width = 0.1354167F;
            // 
            // Shape70
            // 
            this.Shape70.Height = 0.1458333F;
            this.Shape70.Left = 6.947917F;
            this.Shape70.LineWeight = 0F;
            this.Shape70.Name = "Shape70";
            this.Shape70.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape70.Top = 9.656248F;
            this.Shape70.Width = 0.1354167F;
            // 
            // Shape71
            // 
            this.Shape71.Height = 0.1458333F;
            this.Shape71.Left = 7.243055F;
            this.Shape71.LineWeight = 0F;
            this.Shape71.Name = "Shape71";
            this.Shape71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape71.Top = 7.791665F;
            this.Shape71.Width = 0.1354167F;
            // 
            // Shape72
            // 
            this.Shape72.Height = 0.1458333F;
            this.Shape72.Left = 7.243055F;
            this.Shape72.LineWeight = 0F;
            this.Shape72.Name = "Shape72";
            this.Shape72.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape72.Top = 8.156248F;
            this.Shape72.Width = 0.1354167F;
            // 
            // Shape73
            // 
            this.Shape73.Height = 0.1458333F;
            this.Shape73.Left = 7.243055F;
            this.Shape73.LineWeight = 0F;
            this.Shape73.Name = "Shape73";
            this.Shape73.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape73.Top = 8.541666F;
            this.Shape73.Width = 0.1354167F;
            // 
            // Shape74
            // 
            this.Shape74.Height = 0.1458333F;
            this.Shape74.Left = 7.243055F;
            this.Shape74.LineWeight = 0F;
            this.Shape74.Name = "Shape74";
            this.Shape74.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape74.Top = 8.916666F;
            this.Shape74.Width = 0.1354167F;
            // 
            // Shape75
            // 
            this.Shape75.Height = 0.1458333F;
            this.Shape75.Left = 7.243055F;
            this.Shape75.LineWeight = 0F;
            this.Shape75.Name = "Shape75";
            this.Shape75.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape75.Top = 9.291666F;
            this.Shape75.Width = 0.1354167F;
            // 
            // Shape76
            // 
            this.Shape76.Height = 0.1458333F;
            this.Shape76.Left = 7.243055F;
            this.Shape76.LineWeight = 0F;
            this.Shape76.Name = "Shape76";
            this.Shape76.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape76.Top = 9.656248F;
            this.Shape76.Width = 0.1354167F;
            // 
            // Shape85
            // 
            this.Shape85.Height = 0.1458333F;
            this.Shape85.Left = 3.609028F;
            this.Shape85.LineWeight = 0F;
            this.Shape85.Name = "Shape85";
            this.Shape85.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape85.Top = 7.791665F;
            this.Shape85.Width = 0.1354167F;
            // 
            // Shape78
            // 
            this.Shape78.Height = 0.1458333F;
            this.Shape78.Left = 3.609028F;
            this.Shape78.LineWeight = 0F;
            this.Shape78.Name = "Shape78";
            this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape78.Top = 8.166666F;
            this.Shape78.Width = 0.1354167F;
            // 
            // Shape79
            // 
            this.Shape79.Height = 0.1458333F;
            this.Shape79.Left = 3.609028F;
            this.Shape79.LineWeight = 0F;
            this.Shape79.Name = "Shape79";
            this.Shape79.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape79.Top = 8.541666F;
            this.Shape79.Width = 0.1354167F;
            // 
            // Shape80
            // 
            this.Shape80.Height = 0.1458333F;
            this.Shape80.Left = 3.609028F;
            this.Shape80.LineWeight = 0F;
            this.Shape80.Name = "Shape80";
            this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape80.Top = 8.916666F;
            this.Shape80.Width = 0.1354167F;
            // 
            // Shape81
            // 
            this.Shape81.Height = 0.1458333F;
            this.Shape81.Left = 3.609028F;
            this.Shape81.LineWeight = 0F;
            this.Shape81.Name = "Shape81";
            this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape81.Top = 9.291666F;
            this.Shape81.Width = 0.1354167F;
            // 
            // Shape82
            // 
            this.Shape82.Height = 0.1458333F;
            this.Shape82.Left = 3.604167F;
            this.Shape82.LineWeight = 0F;
            this.Shape82.Name = "Shape82";
            this.Shape82.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape82.Top = 9.656248F;
            this.Shape82.Width = 0.1354167F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.125F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 0F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label24.Text = "Part IV";
            this.Label24.Top = 7.177082F;
            this.Label24.Width = 0.34375F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1145833F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 0.4479167F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label25.Text = "Covered Individuals";
            this.Label25.Top = 7.177082F;
            this.Label25.Width = 1.020833F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1145833F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0.02083333F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label26.Text = "23";
            this.Label26.Top = 7.822915F;
            this.Label26.Width = 0.2083333F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1145833F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 0.02083333F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label27.Text = "24";
            this.Label27.Top = 8.187498F;
            this.Label27.Width = 0.2083333F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.1145833F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 0.02083333F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label28.Text = "25";
            this.Label28.Top = 8.552082F;
            this.Label28.Width = 0.2083333F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1145833F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 0.02083333F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label29.Text = "26";
            this.Label29.Top = 8.916666F;
            this.Label29.Width = 0.2083333F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.1145833F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 0.02083333F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label30.Text = "27";
            this.Label30.Top = 9.281248F;
            this.Label30.Width = 0.2083333F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1145833F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 0.02083333F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left";
            this.Label31.Text = "28";
            this.Label31.Top = 9.708332F;
            this.Label31.Width = 0.2083333F;
            // 
            // Line20
            // 
            this.Line20.Height = 2.333333F;
            this.Line20.Left = 6.868055F;
            this.Line20.LineWeight = 0F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 7.572915F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 6.868055F;
            this.Line20.X2 = 6.868055F;
            this.Line20.Y1 = 7.572915F;
            this.Line20.Y2 = 9.906248F;
            // 
            // Line21
            // 
            this.Line21.Height = 2.333333F;
            this.Line21.Left = 7.163195F;
            this.Line21.LineWeight = 0F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 7.572915F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 7.163195F;
            this.Line21.X2 = 7.163195F;
            this.Line21.Y1 = 7.572915F;
            this.Line21.Y2 = 9.906248F;
            // 
            // Line17
            // 
            this.Line17.Height = 2.333333F;
            this.Line17.Left = 5.982639F;
            this.Line17.LineWeight = 0F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 7.572915F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 5.982639F;
            this.Line17.X2 = 5.982639F;
            this.Line17.Y1 = 7.572915F;
            this.Line17.Y2 = 9.906248F;
            // 
            // Line16
            // 
            this.Line16.Height = 2.333333F;
            this.Line16.Left = 5.6875F;
            this.Line16.LineWeight = 0F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 7.572915F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 5.6875F;
            this.Line16.X2 = 5.6875F;
            this.Line16.Y1 = 7.572915F;
            this.Line16.Y2 = 9.906248F;
            // 
            // Line15
            // 
            this.Line15.Height = 2.333333F;
            this.Line15.Left = 5.392361F;
            this.Line15.LineWeight = 0F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 7.572915F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 5.392361F;
            this.Line15.X2 = 5.392361F;
            this.Line15.Y1 = 7.572915F;
            this.Line15.Y2 = 9.906248F;
            // 
            // Line13
            // 
            this.Line13.Height = 2.333333F;
            this.Line13.Left = 4.802083F;
            this.Line13.LineWeight = 0F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 7.572915F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 4.802083F;
            this.Line13.X2 = 4.802083F;
            this.Line13.Y1 = 7.572915F;
            this.Line13.Y2 = 9.906248F;
            // 
            // Line12
            // 
            this.Line12.Height = 2.333333F;
            this.Line12.Left = 4.506945F;
            this.Line12.LineWeight = 0F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 7.572915F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 4.506945F;
            this.Line12.X2 = 4.506945F;
            this.Line12.Y1 = 7.572915F;
            this.Line12.Y2 = 9.906248F;
            // 
            // Line11
            // 
            this.Line11.Height = 2.333333F;
            this.Line11.Left = 4.211805F;
            this.Line11.LineWeight = 0F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 7.572915F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 4.211805F;
            this.Line11.X2 = 4.211805F;
            this.Line11.Y1 = 7.572915F;
            this.Line11.Y2 = 9.906248F;
            // 
            // Line10
            // 
            this.Line10.Height = 2.600694F;
            this.Line10.Left = 3.916667F;
            this.Line10.LineWeight = 0F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 7.305554F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 3.916667F;
            this.Line10.X2 = 3.916667F;
            this.Line10.Y1 = 7.305554F;
            this.Line10.Y2 = 9.906248F;
            // 
            // Label109
            // 
            this.Label109.Height = 0.1145833F;
            this.Label109.HyperLink = null;
            this.Label109.Left = 1.447917F;
            this.Label109.Name = "Label109";
            this.Label109.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left";
            this.Label109.Text = "(Enter the information for each covered individual.)";
            this.Label109.Top = 7.177082F;
            this.Label109.Width = 3.083333F;
            // 
            // Line65
            // 
            this.Line65.Height = 0F;
            this.Line65.Left = 0F;
            this.Line65.LineWeight = 0F;
            this.Line65.Name = "Line65";
            this.Line65.Top = 7.177082F;
            this.Line65.Width = 7.46875F;
            this.Line65.X1 = 0F;
            this.Line65.X2 = 7.46875F;
            this.Line65.Y1 = 7.177082F;
            this.Line65.Y2 = 7.177082F;
            // 
            // Line66
            // 
            this.Line66.Height = 0.5069451F;
            this.Line66.Left = 5.625F;
            this.Line66.LineWeight = 0F;
            this.Line66.Name = "Line66";
            this.Line66.Top = 6.670137F;
            this.Line66.Width = 0F;
            this.Line66.X1 = 5.625F;
            this.Line66.X2 = 5.625F;
            this.Line66.Y1 = 6.670137F;
            this.Line66.Y2 = 7.177082F;
            // 
            // Line67
            // 
            this.Line67.Height = 0.5069451F;
            this.Line67.Left = 4.041667F;
            this.Line67.LineWeight = 0F;
            this.Line67.Name = "Line67";
            this.Line67.Top = 6.670137F;
            this.Line67.Width = 0F;
            this.Line67.X1 = 4.041667F;
            this.Line67.X2 = 4.041667F;
            this.Line67.Y1 = 6.670137F;
            this.Line67.Y2 = 7.177082F;
            // 
            // Line68
            // 
            this.Line68.Height = 0F;
            this.Line68.Left = 0F;
            this.Line68.LineWeight = 0F;
            this.Line68.Name = "Line68";
            this.Line68.Top = 6.927082F;
            this.Line68.Width = 7.46875F;
            this.Line68.X1 = 0F;
            this.Line68.X2 = 7.46875F;
            this.Line68.Y1 = 6.927082F;
            this.Line68.Y2 = 6.927082F;
            // 
            // Line69
            // 
            this.Line69.Height = 0.25F;
            this.Line69.Left = 2.635417F;
            this.Line69.LineWeight = 0F;
            this.Line69.Name = "Line69";
            this.Line69.Top = 6.927082F;
            this.Line69.Width = 0F;
            this.Line69.X1 = 2.635417F;
            this.Line69.X2 = 2.635417F;
            this.Line69.Y1 = 6.927082F;
            this.Line69.Y2 = 7.177082F;
            // 
            // Label110
            // 
            this.Label110.Height = 0.09375F;
            this.Label110.HyperLink = null;
            this.Label110.Left = 0F;
            this.Label110.Name = "Label110";
            this.Label110.Style = "font-size: 5pt; text-align: left";
            this.Label110.Text = "19 Street address (including room or suite no>)";
            this.Label110.Top = 6.947915F;
            this.Label110.Width = 1.583333F;
            // 
            // Label111
            // 
            this.Label111.Height = 0.09375F;
            this.Label111.HyperLink = null;
            this.Label111.Left = 2.6875F;
            this.Label111.Name = "Label111";
            this.Label111.Style = "font-size: 5pt; text-align: left";
            this.Label111.Text = "20 City or town";
            this.Label111.Top = 6.947915F;
            this.Label111.Width = 1.270833F;
            // 
            // Label112
            // 
            this.Label112.Height = 0.09375F;
            this.Label112.HyperLink = null;
            this.Label112.Left = 4.0625F;
            this.Label112.Name = "Label112";
            this.Label112.Style = "font-size: 5pt; text-align: left";
            this.Label112.Text = "18 Contact telephone number";
            this.Label112.Top = 6.687499F;
            this.Label112.Width = 1.583333F;
            // 
            // Label113
            // 
            this.Label113.Height = 0.09375F;
            this.Label113.HyperLink = null;
            this.Label113.Left = 5.6875F;
            this.Label113.Name = "Label113";
            this.Label113.Style = "font-size: 5pt; text-align: left";
            this.Label113.Text = "18 Contact telephone number";
            this.Label113.Top = 6.687499F;
            this.Label113.Width = 1.583333F;
            // 
            // Label114
            // 
            this.Label114.Height = 0.09375F;
            this.Label114.HyperLink = null;
            this.Label114.Left = 4.0625F;
            this.Label114.Name = "Label114";
            this.Label114.Style = "font-size: 5pt; text-align: left";
            this.Label114.Text = "21 State or province";
            this.Label114.Top = 6.947915F;
            this.Label114.Width = 1.583333F;
            // 
            // Label115
            // 
            this.Label115.Height = 0.09375F;
            this.Label115.HyperLink = null;
            this.Label115.Left = 5.6875F;
            this.Label115.Name = "Label115";
            this.Label115.Style = "font-size: 5pt; text-align: left";
            this.Label115.Text = "22 Country and ZIP or foreign postal code";
            this.Label115.Top = 6.947915F;
            this.Label115.Width = 1.583333F;
            // 
            // Line70
            // 
            this.Line70.Height = 0.28125F;
            this.Line70.Left = 2.635417F;
            this.Line70.LineWeight = 0F;
            this.Line70.Name = "Line70";
            this.Line70.Top = 5.374999F;
            this.Line70.Width = 0F;
            this.Line70.X1 = 2.635417F;
            this.Line70.X2 = 2.635417F;
            this.Line70.Y1 = 5.374999F;
            this.Line70.Y2 = 5.656249F;
            // 
            // Label116
            // 
            this.Label116.Height = 0.09375F;
            this.Label116.HyperLink = null;
            this.Label116.Left = 0F;
            this.Label116.Name = "Label116";
            this.Label116.Style = "font-size: 5pt; text-align: left";
            this.Label116.Text = "16 Name";
            this.Label116.Top = 6.072915F;
            this.Label116.Width = 1.583333F;
            // 
            // Line71
            // 
            this.Line71.Height = 0.5F;
            this.Line71.Left = 5.625F;
            this.Line71.LineWeight = 0F;
            this.Line71.Name = "Line71";
            this.Line71.Top = 6.041665F;
            this.Line71.Width = 0F;
            this.Line71.X1 = 5.625F;
            this.Line71.X2 = 5.625F;
            this.Line71.Y1 = 6.041665F;
            this.Line71.Y2 = 6.541665F;
            // 
            // Line72
            // 
            this.Line72.Height = 0.489583F;
            this.Line72.Left = 4.041667F;
            this.Line72.LineWeight = 0F;
            this.Line72.Name = "Line72";
            this.Line72.Top = 6.052082F;
            this.Line72.Width = 0F;
            this.Line72.X1 = 4.041667F;
            this.Line72.X2 = 4.041667F;
            this.Line72.Y1 = 6.052082F;
            this.Line72.Y2 = 6.541665F;
            // 
            // Line73
            // 
            this.Line73.Height = 0F;
            this.Line73.Left = 0F;
            this.Line73.LineWeight = 0F;
            this.Line73.Name = "Line73";
            this.Line73.Top = 6.541665F;
            this.Line73.Width = 7.46875F;
            this.Line73.X1 = 0F;
            this.Line73.X2 = 7.46875F;
            this.Line73.Y1 = 6.541665F;
            this.Line73.Y2 = 6.541665F;
            // 
            // Label117
            // 
            this.Label117.Height = 0.09375F;
            this.Label117.HyperLink = null;
            this.Label117.Left = 0F;
            this.Label117.Name = "Label117";
            this.Label117.Style = "font-size: 5pt; text-align: left";
            this.Label117.Text = "19 Street address (including room or suite no>)";
            this.Label117.Top = 6.343749F;
            this.Label117.Width = 1.583333F;
            // 
            // Label118
            // 
            this.Label118.Height = 0.09375F;
            this.Label118.HyperLink = null;
            this.Label118.Left = 2.6875F;
            this.Label118.Name = "Label118";
            this.Label118.Style = "font-size: 5pt; text-align: left";
            this.Label118.Text = "20 City or town";
            this.Label118.Top = 6.343749F;
            this.Label118.Width = 1.270833F;
            // 
            // Label119
            // 
            this.Label119.Height = 0.09375F;
            this.Label119.HyperLink = null;
            this.Label119.Left = 5.6875F;
            this.Label119.Name = "Label119";
            this.Label119.Style = "font-size: 5pt; text-align: left";
            this.Label119.Text = "18 Contact telephone number";
            this.Label119.Top = 6.072915F;
            this.Label119.Width = 1.583333F;
            // 
            // Label120
            // 
            this.Label120.Height = 0.09375F;
            this.Label120.HyperLink = null;
            this.Label120.Left = 4.0625F;
            this.Label120.Name = "Label120";
            this.Label120.Style = "font-size: 5pt; text-align: left";
            this.Label120.Text = "21 State or province";
            this.Label120.Top = 6.343749F;
            this.Label120.Width = 1.583333F;
            // 
            // Label121
            // 
            this.Label121.Height = 0.09375F;
            this.Label121.HyperLink = null;
            this.Label121.Left = 5.6875F;
            this.Label121.Name = "Label121";
            this.Label121.Style = "font-size: 5pt; text-align: left";
            this.Label121.Text = "22 Country and ZIP or foreign postal code";
            this.Label121.Top = 6.343749F;
            this.Label121.Width = 1.583333F;
            // 
            // Line60
            // 
            this.Line60.Height = 0F;
            this.Line60.Left = 0F;
            this.Line60.LineWeight = 0F;
            this.Line60.Name = "Line60";
            this.Line60.Top = 5.916665F;
            this.Line60.Width = 7.46875F;
            this.Line60.X1 = 0F;
            this.Line60.X2 = 7.46875F;
            this.Line60.Y1 = 5.916665F;
            this.Line60.Y2 = 5.916665F;
            // 
            // Label61
            // 
            this.Label61.Height = 0.125F;
            this.Label61.HyperLink = null;
            this.Label61.Left = 0F;
            this.Label61.Name = "Label61";
            this.Label61.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label61.Text = "Part II";
            this.Label61.Top = 5.916665F;
            this.Label61.Width = 0.34375F;
            // 
            // Label105
            // 
            this.Label105.Height = 0.125F;
            this.Label105.HyperLink = null;
            this.Label105.Left = 0F;
            this.Label105.Name = "Label105";
            this.Label105.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-size: 7pt; font-weigh" +
    "t: bold; vertical-align: middle";
            this.Label105.Text = "Part III";
            this.Label105.Top = 6.541665F;
            this.Label105.Width = 0.34375F;
            // 
            // Label122
            // 
            this.Label122.Height = 0.09375F;
            this.Label122.HyperLink = null;
            this.Label122.Left = 2.388889F;
            this.Label122.Name = "Label122";
            this.Label122.Style = "font-size: 6pt; font-weight: bold; text-align: left";
            this.Label122.Text = "Do not attach to your tax return. Keep for your records.";
            this.Label122.Top = 4.725693F;
            this.Label122.Width = 2.645833F;
            // 
            // Label123
            // 
            this.Label123.Height = 0.1145833F;
            this.Label123.HyperLink = null;
            this.Label123.Left = 2.25F;
            this.Label123.Name = "Label123";
            this.Label123.Style = "font-family: \'Marlett\'; font-size: 10pt; font-weight: bold; text-align: left; ddo" +
    "-char-set: 2";
            this.Label123.Text = "4";
            this.Label123.Top = 4.708332F;
            this.Label123.Width = 0.2708333F;
            // 
            // Line74
            // 
            this.Line74.Height = 2.187499F;
            this.Line74.Left = 1.104167F;
            this.Line74.LineWeight = 0F;
            this.Line74.Name = "Line74";
            this.Line74.Top = 7.718749F;
            this.Line74.Width = 0F;
            this.Line74.X1 = 1.104167F;
            this.Line74.X2 = 1.104167F;
            this.Line74.Y1 = 7.718749F;
            this.Line74.Y2 = 9.906248F;
            // 
            // Line75
            // 
            this.Line75.Height = 2.187499F;
            this.Line75.Left = 0.875F;
            this.Line75.LineWeight = 0F;
            this.Line75.Name = "Line75";
            this.Line75.Top = 7.718749F;
            this.Line75.Width = 0F;
            this.Line75.X1 = 0.875F;
            this.Line75.X2 = 0.875F;
            this.Line75.Y1 = 7.718749F;
            this.Line75.Y2 = 9.906248F;
            // 
            // txtCoveredLast1
            // 
            this.txtCoveredLast1.CanGrow = false;
            this.txtCoveredLast1.Height = 0.1770833F;
            this.txtCoveredLast1.Left = 1.125F;
            this.txtCoveredLast1.Name = "txtCoveredLast1";
            this.txtCoveredLast1.Style = "font-size: 8.5pt";
            this.txtCoveredLast1.Text = null;
            this.txtCoveredLast1.Top = 7.781249F;
            this.txtCoveredLast1.Width = 0.625F;
            // 
            // txtCoveredLast2
            // 
            this.txtCoveredLast2.CanGrow = false;
            this.txtCoveredLast2.Height = 0.1770833F;
            this.txtCoveredLast2.Left = 1.125F;
            this.txtCoveredLast2.Name = "txtCoveredLast2";
            this.txtCoveredLast2.Style = "font-size: 8.5pt";
            this.txtCoveredLast2.Text = null;
            this.txtCoveredLast2.Top = 8.156248F;
            this.txtCoveredLast2.Width = 0.625F;
            // 
            // txtCoveredLast3
            // 
            this.txtCoveredLast3.CanGrow = false;
            this.txtCoveredLast3.Height = 0.1770833F;
            this.txtCoveredLast3.Left = 1.125F;
            this.txtCoveredLast3.Name = "txtCoveredLast3";
            this.txtCoveredLast3.Style = "font-size: 8.5pt";
            this.txtCoveredLast3.Text = null;
            this.txtCoveredLast3.Top = 8.531248F;
            this.txtCoveredLast3.Width = 0.625F;
            // 
            // txtCoveredLast4
            // 
            this.txtCoveredLast4.CanGrow = false;
            this.txtCoveredLast4.Height = 0.1770833F;
            this.txtCoveredLast4.Left = 1.125F;
            this.txtCoveredLast4.Name = "txtCoveredLast4";
            this.txtCoveredLast4.Style = "font-size: 8.5pt";
            this.txtCoveredLast4.Text = null;
            this.txtCoveredLast4.Top = 8.906248F;
            this.txtCoveredLast4.Width = 0.625F;
            // 
            // txtCoveredLast5
            // 
            this.txtCoveredLast5.CanGrow = false;
            this.txtCoveredLast5.Height = 0.1770833F;
            this.txtCoveredLast5.Left = 1.125F;
            this.txtCoveredLast5.Name = "txtCoveredLast5";
            this.txtCoveredLast5.Style = "font-size: 8.5pt";
            this.txtCoveredLast5.Text = null;
            this.txtCoveredLast5.Top = 9.281248F;
            this.txtCoveredLast5.Width = 0.625F;
            // 
            // txtCoveredLast6
            // 
            this.txtCoveredLast6.CanGrow = false;
            this.txtCoveredLast6.Height = 0.1770833F;
            this.txtCoveredLast6.Left = 1.125F;
            this.txtCoveredLast6.Name = "txtCoveredLast6";
            this.txtCoveredLast6.Style = "font-size: 8.5pt";
            this.txtCoveredLast6.Text = null;
            this.txtCoveredLast6.Top = 9.656248F;
            this.txtCoveredLast6.Width = 0.625F;
            // 
            // txtCoveredMiddle1
            // 
            this.txtCoveredMiddle1.CanGrow = false;
            this.txtCoveredMiddle1.Height = 0.1770833F;
            this.txtCoveredMiddle1.Left = 0.8958333F;
            this.txtCoveredMiddle1.Name = "txtCoveredMiddle1";
            this.txtCoveredMiddle1.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle1.Text = null;
            this.txtCoveredMiddle1.Top = 7.781249F;
            this.txtCoveredMiddle1.Width = 0.1875F;
            // 
            // txtCoveredMiddle2
            // 
            this.txtCoveredMiddle2.CanGrow = false;
            this.txtCoveredMiddle2.Height = 0.1770833F;
            this.txtCoveredMiddle2.Left = 0.8958333F;
            this.txtCoveredMiddle2.Name = "txtCoveredMiddle2";
            this.txtCoveredMiddle2.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle2.Text = null;
            this.txtCoveredMiddle2.Top = 8.156248F;
            this.txtCoveredMiddle2.Width = 0.1875F;
            // 
            // txtCoveredMiddle3
            // 
            this.txtCoveredMiddle3.CanGrow = false;
            this.txtCoveredMiddle3.Height = 0.1770833F;
            this.txtCoveredMiddle3.Left = 0.8958333F;
            this.txtCoveredMiddle3.Name = "txtCoveredMiddle3";
            this.txtCoveredMiddle3.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle3.Text = null;
            this.txtCoveredMiddle3.Top = 8.531248F;
            this.txtCoveredMiddle3.Width = 0.1875F;
            // 
            // txtCoveredMiddle4
            // 
            this.txtCoveredMiddle4.CanGrow = false;
            this.txtCoveredMiddle4.Height = 0.1770833F;
            this.txtCoveredMiddle4.Left = 0.8958333F;
            this.txtCoveredMiddle4.Name = "txtCoveredMiddle4";
            this.txtCoveredMiddle4.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle4.Text = null;
            this.txtCoveredMiddle4.Top = 8.906248F;
            this.txtCoveredMiddle4.Width = 0.1875F;
            // 
            // txtCoveredMiddle5
            // 
            this.txtCoveredMiddle5.CanGrow = false;
            this.txtCoveredMiddle5.Height = 0.1770833F;
            this.txtCoveredMiddle5.Left = 0.8958333F;
            this.txtCoveredMiddle5.Name = "txtCoveredMiddle5";
            this.txtCoveredMiddle5.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle5.Text = null;
            this.txtCoveredMiddle5.Top = 9.281248F;
            this.txtCoveredMiddle5.Width = 0.1875F;
            // 
            // txtCoveredMiddle6
            // 
            this.txtCoveredMiddle6.CanGrow = false;
            this.txtCoveredMiddle6.Height = 0.1770833F;
            this.txtCoveredMiddle6.Left = 0.8958333F;
            this.txtCoveredMiddle6.Name = "txtCoveredMiddle6";
            this.txtCoveredMiddle6.Style = "font-size: 8.5pt";
            this.txtCoveredMiddle6.Text = null;
            this.txtCoveredMiddle6.Top = 9.656248F;
            this.txtCoveredMiddle6.Width = 0.1875F;
            // 
            // Line76
            // 
            this.Line76.Height = 0.1423621F;
            this.Line76.Left = 2.6875F;
            this.Line76.LineWeight = 0F;
            this.Line76.Name = "Line76";
            this.Line76.Top = 5.232637F;
            this.Line76.Width = 0F;
            this.Line76.X1 = 2.6875F;
            this.Line76.X2 = 2.6875F;
            this.Line76.Y1 = 5.232637F;
            this.Line76.Y2 = 5.374999F;
            // 
            // Line77
            // 
            this.Line77.Height = 0.1423621F;
            this.Line77.Left = 1.402778F;
            this.Line77.LineWeight = 0F;
            this.Line77.Name = "Line77";
            this.Line77.Top = 5.232637F;
            this.Line77.Width = 0F;
            this.Line77.X1 = 1.402778F;
            this.Line77.X2 = 1.402778F;
            this.Line77.Y1 = 5.232637F;
            this.Line77.Y2 = 5.374999F;
            // 
            // txtMiddle
            // 
            this.txtMiddle.CanGrow = false;
            this.txtMiddle.Height = 0.1770833F;
            this.txtMiddle.Left = 1.4375F;
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Style = "font-size: 8.5pt";
            this.txtMiddle.Text = null;
            this.txtMiddle.Top = 5.229165F;
            this.txtMiddle.Width = 1.1875F;
            // 
            // txtLast
            // 
            this.txtLast.CanGrow = false;
            this.txtLast.Height = 0.1770833F;
            this.txtLast.Left = 2.71875F;
            this.txtLast.Name = "txtLast";
            this.txtLast.Style = "font-size: 8.5pt";
            this.txtLast.Text = null;
            this.txtLast.Top = 5.229165F;
            this.txtLast.Width = 1.3125F;
            // 
            // Line78
            // 
            this.Line78.Height = 0.2152781F;
            this.Line78.Left = 2.635417F;
            this.Line78.LineWeight = 0F;
            this.Line78.Name = "Line78";
            this.Line78.Top = 6.326387F;
            this.Line78.Width = 0F;
            this.Line78.X1 = 2.635417F;
            this.Line78.X2 = 2.635417F;
            this.Line78.Y1 = 6.326387F;
            this.Line78.Y2 = 6.541665F;
            // 
            // rpt1095B2020BlankPortraitPage1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.46875F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerReturnAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl12Months)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredLast6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoveredMiddle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerReturnAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCityStateZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginOfPolicy;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderTelephone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderPostalCode;
        private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape83;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape84;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl12Months;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape43;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape44;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape45;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape46;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape69;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape70;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape71;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape72;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape73;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape74;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape75;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape76;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape85;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape79;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape82;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredLast6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMiddle6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMiddle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line78;
    }
}
