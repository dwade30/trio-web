﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptACAHealthCareDependents.
	/// </summary>
	partial class srptACAHealthCareDependents
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptACAHealthCareDependents));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTerminationDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkJan = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkFeb = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkMar = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkApr = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkMay = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkJun = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkJul = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkAug = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkSep = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkOct = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkNov = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkDec = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTerminationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkJan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFeb)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkJun)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkJul)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAug)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSep)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNov)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.Label1,
				this.Label5,
				this.txtSSN,
				this.Label6,
				this.txtDOB,
				this.Label7,
				this.txtTerminationDate,
				this.chkJan,
				this.chkFeb,
				this.chkMar,
				this.chkApr,
				this.chkMay,
				this.chkJun,
				this.chkJul,
				this.chkAug,
				this.chkSep,
				this.chkOct,
				this.chkNov,
				this.chkDec
			});
			this.Detail.Height = 0.5F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8
			});
			this.ReportHeader.Height = 0.3229167F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label8
			// 
			this.Label8.Height = 0.21875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.01041667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Dependents";
			this.Label8.Top = 0.08333334F;
			this.Label8.Width = 1F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1979167F;
			this.txtName.Left = 0.4791667F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.71875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1979167F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Name";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.5F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1979167F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.25F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "SSN";
			this.Label5.Top = 0F;
			this.Label5.Width = 0.375F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1979167F;
			this.txtSSN.Left = 3.625F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0F;
			this.txtSSN.Width = 0.90625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1979167F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.5625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold";
			this.Label6.Text = "DOB";
			this.Label6.Top = 0F;
			this.Label6.Width = 0.375F;
			// 
			// txtDOB
			// 
			this.txtDOB.Height = 0.1979167F;
			this.txtDOB.Left = 4.9375F;
			this.txtDOB.Name = "txtDOB";
			this.txtDOB.Text = null;
			this.txtDOB.Top = 0F;
			this.txtDOB.Width = 0.84375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1979167F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.8125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Term Date";
			this.Label7.Top = 0F;
			this.Label7.Width = 0.875F;
			// 
			// txtTerminationDate
			// 
			this.txtTerminationDate.Height = 0.1979167F;
			this.txtTerminationDate.Left = 6.625F;
			this.txtTerminationDate.Name = "txtTerminationDate";
			this.txtTerminationDate.Text = null;
			this.txtTerminationDate.Top = 0F;
			this.txtTerminationDate.Width = 0.84375F;
			// 
			// chkJan
			// 
			this.chkJan.Height = 0.2916667F;
			this.chkJan.Left = 1.0625F;
			this.chkJan.Name = "chkJan";
			this.chkJan.Style = "text-align: right";
			this.chkJan.Text = "Jan";
			this.chkJan.Top = 0.1875F;
			this.chkJan.Width = 0.4583333F;
			// 
			// chkFeb
			// 
			this.chkFeb.Height = 0.2916667F;
			this.chkFeb.Left = 1.5625F;
			this.chkFeb.Name = "chkFeb";
			this.chkFeb.Style = "text-align: right";
			this.chkFeb.Text = "Feb";
			this.chkFeb.Top = 0.1875F;
			this.chkFeb.Width = 0.4583333F;
			// 
			// chkMar
			// 
			this.chkMar.Height = 0.2916667F;
			this.chkMar.Left = 2.0625F;
			this.chkMar.Name = "chkMar";
			this.chkMar.Style = "text-align: right";
			this.chkMar.Text = "Mar";
			this.chkMar.Top = 0.1875F;
			this.chkMar.Width = 0.4583333F;
			// 
			// chkApr
			// 
			this.chkApr.Height = 0.2916667F;
			this.chkApr.Left = 2.5625F;
			this.chkApr.Name = "chkApr";
			this.chkApr.Style = "text-align: right";
			this.chkApr.Text = "Apr";
			this.chkApr.Top = 0.1875F;
			this.chkApr.Width = 0.4583333F;
			// 
			// chkMay
			// 
			this.chkMay.Height = 0.2916667F;
			this.chkMay.Left = 3.0625F;
			this.chkMay.Name = "chkMay";
			this.chkMay.Style = "text-align: right";
			this.chkMay.Text = "May";
			this.chkMay.Top = 0.1875F;
			this.chkMay.Width = 0.4583333F;
			// 
			// chkJun
			// 
			this.chkJun.Height = 0.2916667F;
			this.chkJun.Left = 3.5625F;
			this.chkJun.Name = "chkJun";
			this.chkJun.Style = "text-align: right";
			this.chkJun.Text = "Jun";
			this.chkJun.Top = 0.1875F;
			this.chkJun.Width = 0.4583333F;
			// 
			// chkJul
			// 
			this.chkJul.Height = 0.2916667F;
			this.chkJul.Left = 4.0625F;
			this.chkJul.Name = "chkJul";
			this.chkJul.Style = "text-align: right";
			this.chkJul.Text = "Jul";
			this.chkJul.Top = 0.1875F;
			this.chkJul.Width = 0.4583333F;
			// 
			// chkAug
			// 
			this.chkAug.Height = 0.2916667F;
			this.chkAug.Left = 4.5625F;
			this.chkAug.Name = "chkAug";
			this.chkAug.Style = "text-align: right";
			this.chkAug.Text = "Aug";
			this.chkAug.Top = 0.1875F;
			this.chkAug.Width = 0.4583333F;
			// 
			// chkSep
			// 
			this.chkSep.Height = 0.2916667F;
			this.chkSep.Left = 5.0625F;
			this.chkSep.Name = "chkSep";
			this.chkSep.Style = "text-align: right";
			this.chkSep.Text = "Sep";
			this.chkSep.Top = 0.1875F;
			this.chkSep.Width = 0.4583333F;
			// 
			// chkOct
			// 
			this.chkOct.Height = 0.2916667F;
			this.chkOct.Left = 5.5625F;
			this.chkOct.Name = "chkOct";
			this.chkOct.Style = "text-align: right";
			this.chkOct.Text = "Oct";
			this.chkOct.Top = 0.1875F;
			this.chkOct.Width = 0.4583333F;
			// 
			// chkNov
			// 
			this.chkNov.Height = 0.2916667F;
			this.chkNov.Left = 6.0625F;
			this.chkNov.Name = "chkNov";
			this.chkNov.Style = "text-align: right";
			this.chkNov.Text = "Nov";
			this.chkNov.Top = 0.1875F;
			this.chkNov.Width = 0.4583333F;
			// 
			// chkDec
			// 
			this.chkDec.Height = 0.2916667F;
			this.chkDec.Left = 6.5625F;
			this.chkDec.Name = "chkDec";
			this.chkDec.Style = "text-align: right";
			this.chkDec.Text = "Dec";
			this.chkDec.Top = 0.1875F;
			this.chkDec.Width = 0.4583333F;
			// 
			// srptACAHealthCareDependents
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTerminationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkJan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFeb)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkJun)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkJul)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAug)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSep)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNov)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTerminationDate;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkJan;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFeb;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkMar;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkApr;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkMay;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkJun;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkJul;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkAug;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkSep;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkOct;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkNov;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkDec;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
