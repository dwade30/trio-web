﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016BlankPage1.
	/// </summary>
	partial class rpt1095C2016BlankPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2016BlankPage1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStartMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAll12Box14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox14_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAll12Box15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox15_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAll12Box16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox16_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label139 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label144 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label145 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label146 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label147 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label148 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label149 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label150 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label153 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label154 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label156 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label157 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label158 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label160 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label161 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label162 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label163 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label164 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label165 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label166 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label167 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape46 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape70 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape72 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape73 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape74 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape75 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape76 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape79 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label139)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label144)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label145)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label146)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label147)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label148)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label149)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label150)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label151)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label153)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label155)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label157)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.txtSSN,
				this.txtEmployer,
				this.txtEIN,
				this.txtAddress,
				this.txtEmployerAddress,
				this.txtPhone,
				this.txtStartMonth,
				this.txtAll12Box14,
				this.txtBox14_1,
				this.txtBox14_2,
				this.txtBox14_3,
				this.txtBox14_4,
				this.txtBox14_5,
				this.txtBox14_6,
				this.txtBox14_7,
				this.txtBox14_8,
				this.txtBox14_9,
				this.txtBox14_10,
				this.txtBox14_11,
				this.txtBox14_12,
				this.txtAll12Box15,
				this.txtBox15_1,
				this.txtBox15_2,
				this.txtBox15_3,
				this.txtBox15_4,
				this.txtBox15_5,
				this.txtBox15_6,
				this.txtBox15_7,
				this.txtBox15_8,
				this.txtBox15_9,
				this.txtBox15_10,
				this.txtBox15_11,
				this.txtBox15_12,
				this.txtAll12Box16,
				this.txtBox16_1,
				this.txtBox16_2,
				this.txtBox16_3,
				this.txtBox16_4,
				this.txtBox16_5,
				this.txtBox16_6,
				this.txtBox16_7,
				this.txtBox16_8,
				this.txtBox16_9,
				this.txtBox16_10,
				this.txtBox16_11,
				this.txtBox16_12,
				this.txtCoveredName1,
				this.txtCoveredIndividuals,
				this.txtCoveredSSN1,
				this.txtCoveredDOB1,
				this.txtCoveredName2,
				this.txtCoveredSSN2,
				this.txtCoveredDOB2,
				this.txtCoveredName3,
				this.txtCoveredSSN3,
				this.txtCoveredDOB3,
				this.txtCoveredName4,
				this.txtCoveredSSN4,
				this.txtCoveredDOB4,
				this.txtCoveredName5,
				this.txtCoveredSSN5,
				this.txtCoveredDOB5,
				this.txtCoveredName6,
				this.txtCoveredSSN6,
				this.txtCoveredDOB6,
				this.txtCoveredAll12_1,
				this.txtCoveredMonth1_1,
				this.txtCoveredMonth2_1,
				this.txtCoveredMonth3_1,
				this.txtCoveredMonth4_1,
				this.txtCoveredMonth5_1,
				this.txtCoveredMonth6_1,
				this.txtCoveredMonth7_1,
				this.txtCoveredMonth8_1,
				this.txtCoveredMonth9_1,
				this.txtCoveredMonth10_1,
				this.txtCoveredMonth11_1,
				this.txtCoveredMonth12_1,
				this.txtCoveredAll12_2,
				this.txtCoveredMonth1_2,
				this.txtCoveredMonth2_2,
				this.txtCoveredMonth3_2,
				this.txtCoveredMonth4_2,
				this.txtCoveredMonth5_2,
				this.txtCoveredMonth6_2,
				this.txtCoveredMonth7_2,
				this.txtCoveredMonth8_2,
				this.txtCoveredMonth9_2,
				this.txtCoveredMonth10_2,
				this.txtCoveredMonth11_2,
				this.txtCoveredMonth12_2,
				this.txtCoveredAll12_3,
				this.txtCoveredMonth1_3,
				this.txtCoveredMonth2_3,
				this.txtCoveredMonth3_3,
				this.txtCoveredMonth4_3,
				this.txtCoveredMonth5_3,
				this.txtCoveredMonth6_3,
				this.txtCoveredMonth7_3,
				this.txtCoveredMonth8_3,
				this.txtCoveredMonth9_3,
				this.txtCoveredMonth10_3,
				this.txtCoveredMonth11_3,
				this.txtCoveredMonth12_3,
				this.txtCoveredAll12_4,
				this.txtCoveredMonth1_4,
				this.txtCoveredMonth2_4,
				this.txtCoveredMonth3_4,
				this.txtCoveredMonth4_4,
				this.txtCoveredMonth5_4,
				this.txtCoveredMonth6_4,
				this.txtCoveredMonth7_4,
				this.txtCoveredMonth8_4,
				this.txtCoveredMonth9_4,
				this.txtCoveredMonth10_4,
				this.txtCoveredMonth11_4,
				this.txtCoveredMonth12_4,
				this.txtCoveredAll12_5,
				this.txtCoveredMonth1_5,
				this.txtCoveredMonth2_5,
				this.txtCoveredMonth3_5,
				this.txtCoveredMonth4_5,
				this.txtCoveredMonth5_5,
				this.txtCoveredMonth6_5,
				this.txtCoveredMonth7_5,
				this.txtCoveredMonth8_5,
				this.txtCoveredMonth9_5,
				this.txtCoveredMonth10_5,
				this.txtCoveredMonth11_5,
				this.txtCoveredMonth12_5,
				this.txtCoveredAll12_6,
				this.txtCoveredMonth1_6,
				this.txtCoveredMonth2_6,
				this.txtCoveredMonth3_6,
				this.txtCoveredMonth4_6,
				this.txtCoveredMonth5_6,
				this.txtCoveredMonth6_6,
				this.txtCoveredMonth7_6,
				this.txtCoveredMonth8_6,
				this.txtCoveredMonth9_6,
				this.txtCoveredMonth10_6,
				this.txtCoveredMonth11_6,
				this.txtCoveredMonth12_6,
				this.txtCity,
				this.txtState,
				this.txtPostalCode,
				this.txtEmployerCity,
				this.txtEmployerState,
				this.txtEmployerPostalCode,
				this.Image2,
				this.Label77,
				this.Label78,
				this.Label83,
				this.Label84,
				this.Line3,
				this.Line4,
				this.Label85,
				this.Label86,
				this.Label87,
				this.Line6,
				this.Line9,
				this.Line10,
				this.Label88,
				this.Line11,
				this.Line12,
				this.Line13,
				this.Label89,
				this.Label90,
				this.Label91,
				this.Label92,
				this.Line14,
				this.Label93,
				this.Label94,
				this.Label95,
				this.Line15,
				this.Line16,
				this.Line17,
				this.Line18,
				this.Line19,
				this.Line20,
				this.Line21,
				this.Line22,
				this.Line36,
				this.Shape1,
				this.Line38,
				this.Line51,
				this.Line54,
				this.Line55,
				this.Label96,
				this.Shape2,
				this.Shape3,
				this.Label97,
				this.Label98,
				this.Line56,
				this.Label99,
				this.Label100,
				this.Label101,
				this.Label102,
				this.Label104,
				this.Label105,
				this.Label106,
				this.Label107,
				this.Label108,
				this.Label109,
				this.Label110,
				this.Label111,
				this.Label112,
				this.Label113,
				this.Label114,
				this.Label115,
				this.Label116,
				this.Label117,
				this.Label118,
				this.Label119,
				this.Label120,
				this.Label121,
				this.Label122,
				this.Label123,
				this.Label124,
				this.Label125,
				this.Label126,
				this.Label127,
				this.Label128,
				this.Label129,
				this.Label130,
				this.Label131,
				this.Label132,
				this.Label133,
				this.Label134,
				this.Label135,
				this.Label136,
				this.Label137,
				this.Label138,
				this.Label139,
				this.Label140,
				this.Label141,
				this.Label142,
				this.Label143,
				this.Label144,
				this.Label145,
				this.Label146,
				this.Label147,
				this.Label148,
				this.Label149,
				this.Label150,
				this.Line40,
				this.Line41,
				this.Line42,
				this.Line43,
				this.Line46,
				this.Line47,
				this.Line48,
				this.Line49,
				this.Line50,
				this.Line45,
				this.Line44,
				this.Label34,
				this.Label33,
				this.Label151,
				this.Label152,
				this.Label153,
				this.Label154,
				this.Label155,
				this.Label156,
				this.Label157,
				this.Line53,
				this.Label158,
				this.Label159,
				this.Label160,
				this.Label161,
				this.Label162,
				this.Label163,
				this.Label164,
				this.Label32,
				this.Label165,
				this.Label166,
				this.Label167,
				this.Label168,
				this.Label169,
				this.Label170,
				this.Label171,
				this.Label172,
				this.Label173,
				this.Label3,
				this.Label174,
				this.Label175,
				this.Label176,
				this.Label177,
				this.Label178,
				this.Label179,
				this.Label180,
				this.Label181,
				this.Label182,
				this.Line57,
				this.Line1,
				this.Line39,
				this.Line25,
				this.Line26,
				this.Line27,
				this.Line28,
				this.Line29,
				this.Line30,
				this.Line31,
				this.Line32,
				this.Line33,
				this.Line34,
				this.Line35,
				this.Shape4,
				this.Shape5,
				this.Shape6,
				this.Shape7,
				this.Shape8,
				this.Shape9,
				this.Shape10,
				this.Shape11,
				this.Shape12,
				this.Shape13,
				this.Shape14,
				this.Shape15,
				this.Shape16,
				this.Shape17,
				this.Shape18,
				this.Shape19,
				this.Shape20,
				this.Shape21,
				this.Shape22,
				this.Shape23,
				this.Shape24,
				this.Shape25,
				this.Shape26,
				this.Shape27,
				this.Shape28,
				this.Shape29,
				this.Shape30,
				this.Shape32,
				this.Shape31,
				this.Shape33,
				this.Shape34,
				this.Shape35,
				this.Shape36,
				this.Shape37,
				this.Shape38,
				this.Shape39,
				this.Shape40,
				this.Shape41,
				this.Shape42,
				this.Shape43,
				this.Shape44,
				this.Shape45,
				this.Shape46,
				this.Shape47,
				this.Shape48,
				this.Shape49,
				this.Shape50,
				this.Shape51,
				this.Shape52,
				this.Shape53,
				this.Shape54,
				this.Shape55,
				this.Shape56,
				this.Shape57,
				this.Shape58,
				this.Shape59,
				this.Shape60,
				this.Shape61,
				this.Shape62,
				this.Shape63,
				this.Shape64,
				this.Shape65,
				this.Shape66,
				this.Shape67,
				this.Shape68,
				this.Shape69,
				this.Shape70,
				this.Shape71,
				this.Shape72,
				this.Shape73,
				this.Shape74,
				this.Shape75,
				this.Shape76,
				this.Shape77,
				this.Shape78,
				this.Shape79,
				this.Shape80,
				this.Shape81,
				this.Line8,
				this.Line7,
				this.Line37,
				this.Line52,
				this.Line23,
				this.Line24,
				this.Label183,
				this.Label184,
				this.Line58,
				this.Line5
			});
			this.Detail.Height = 7.708333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtName
			// 
			this.txtName.Height = 0.1770833F;
			this.txtName.Left = 0.08333334F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Name of Employee";
			this.txtName.Top = 1.354167F;
			this.txtName.Width = 3.083333F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1770833F;
			this.txtSSN.Left = 3.333333F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 1.354167F;
			this.txtSSN.Width = 1.666667F;
			// 
			// txtEmployer
			// 
			this.txtEmployer.Height = 0.1770833F;
			this.txtEmployer.Left = 5.083333F;
			this.txtEmployer.Name = "txtEmployer";
			this.txtEmployer.Text = null;
			this.txtEmployer.Top = 1.354167F;
			this.txtEmployer.Width = 3.083333F;
			// 
			// txtEIN
			// 
			this.txtEIN.Height = 0.1770833F;
			this.txtEIN.Left = 8.364583F;
			this.txtEIN.Name = "txtEIN";
			this.txtEIN.Text = null;
			this.txtEIN.Top = 1.354167F;
			this.txtEIN.Width = 1.666667F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1770833F;
			this.txtAddress.Left = 0.08333334F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.6875F;
			this.txtAddress.Width = 4.833333F;
			// 
			// txtEmployerAddress
			// 
			this.txtEmployerAddress.Height = 0.1770833F;
			this.txtEmployerAddress.Left = 5.083333F;
			this.txtEmployerAddress.Name = "txtEmployerAddress";
			this.txtEmployerAddress.Text = null;
			this.txtEmployerAddress.Top = 1.6875F;
			this.txtEmployerAddress.Width = 3.083333F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1770833F;
			this.txtPhone.Left = 8.364583F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 1.6875F;
			this.txtPhone.Width = 1.604167F;
			// 
			// txtStartMonth
			// 
			this.txtStartMonth.Height = 0.1770833F;
			this.txtStartMonth.Left = 7.8125F;
			this.txtStartMonth.Name = "txtStartMonth";
			this.txtStartMonth.Text = null;
			this.txtStartMonth.Top = 2.197917F;
			this.txtStartMonth.Width = 0.7916667F;
			// 
			// txtAll12Box14
			// 
			this.txtAll12Box14.Height = 0.1770833F;
			this.txtAll12Box14.Left = 1.111111F;
			this.txtAll12Box14.Name = "txtAll12Box14";
			this.txtAll12Box14.Text = null;
			this.txtAll12Box14.Top = 2.645833F;
			this.txtAll12Box14.Width = 0.5F;
			// 
			// txtBox14_1
			// 
			this.txtBox14_1.Height = 0.1770833F;
			this.txtBox14_1.Left = 1.801389F;
			this.txtBox14_1.Name = "txtBox14_1";
			this.txtBox14_1.Text = null;
			this.txtBox14_1.Top = 2.645833F;
			this.txtBox14_1.Width = 0.5F;
			// 
			// txtBox14_2
			// 
			this.txtBox14_2.Height = 0.1770833F;
			this.txtBox14_2.Left = 2.491667F;
			this.txtBox14_2.Name = "txtBox14_2";
			this.txtBox14_2.Text = null;
			this.txtBox14_2.Top = 2.645833F;
			this.txtBox14_2.Width = 0.5F;
			// 
			// txtBox14_3
			// 
			this.txtBox14_3.Height = 0.1770833F;
			this.txtBox14_3.Left = 3.181944F;
			this.txtBox14_3.Name = "txtBox14_3";
			this.txtBox14_3.Text = null;
			this.txtBox14_3.Top = 2.645833F;
			this.txtBox14_3.Width = 0.5F;
			// 
			// txtBox14_4
			// 
			this.txtBox14_4.Height = 0.1770833F;
			this.txtBox14_4.Left = 3.872222F;
			this.txtBox14_4.Name = "txtBox14_4";
			this.txtBox14_4.Text = null;
			this.txtBox14_4.Top = 2.645833F;
			this.txtBox14_4.Width = 0.5F;
			// 
			// txtBox14_5
			// 
			this.txtBox14_5.Height = 0.1770833F;
			this.txtBox14_5.Left = 4.5625F;
			this.txtBox14_5.Name = "txtBox14_5";
			this.txtBox14_5.Text = null;
			this.txtBox14_5.Top = 2.645833F;
			this.txtBox14_5.Width = 0.5F;
			// 
			// txtBox14_6
			// 
			this.txtBox14_6.Height = 0.1770833F;
			this.txtBox14_6.Left = 5.252778F;
			this.txtBox14_6.Name = "txtBox14_6";
			this.txtBox14_6.Text = null;
			this.txtBox14_6.Top = 2.645833F;
			this.txtBox14_6.Width = 0.5F;
			// 
			// txtBox14_7
			// 
			this.txtBox14_7.Height = 0.1770833F;
			this.txtBox14_7.Left = 5.963889F;
			this.txtBox14_7.Name = "txtBox14_7";
			this.txtBox14_7.Text = null;
			this.txtBox14_7.Top = 2.645833F;
			this.txtBox14_7.Width = 0.5F;
			// 
			// txtBox14_8
			// 
			this.txtBox14_8.Height = 0.1770833F;
			this.txtBox14_8.Left = 6.633333F;
			this.txtBox14_8.Name = "txtBox14_8";
			this.txtBox14_8.Text = null;
			this.txtBox14_8.Top = 2.645833F;
			this.txtBox14_8.Width = 0.5F;
			// 
			// txtBox14_9
			// 
			this.txtBox14_9.Height = 0.1770833F;
			this.txtBox14_9.Left = 7.323611F;
			this.txtBox14_9.Name = "txtBox14_9";
			this.txtBox14_9.Text = null;
			this.txtBox14_9.Top = 2.645833F;
			this.txtBox14_9.Width = 0.5F;
			// 
			// txtBox14_10
			// 
			this.txtBox14_10.Height = 0.1770833F;
			this.txtBox14_10.Left = 8.013889F;
			this.txtBox14_10.Name = "txtBox14_10";
			this.txtBox14_10.Text = null;
			this.txtBox14_10.Top = 2.645833F;
			this.txtBox14_10.Width = 0.5F;
			// 
			// txtBox14_11
			// 
			this.txtBox14_11.Height = 0.1770833F;
			this.txtBox14_11.Left = 8.704166F;
			this.txtBox14_11.Name = "txtBox14_11";
			this.txtBox14_11.Text = null;
			this.txtBox14_11.Top = 2.645833F;
			this.txtBox14_11.Width = 0.5F;
			// 
			// txtBox14_12
			// 
			this.txtBox14_12.Height = 0.1770833F;
			this.txtBox14_12.Left = 9.394444F;
			this.txtBox14_12.Name = "txtBox14_12";
			this.txtBox14_12.Text = null;
			this.txtBox14_12.Top = 2.645833F;
			this.txtBox14_12.Width = 0.5F;
			// 
			// txtAll12Box15
			// 
			this.txtAll12Box15.Height = 0.1770833F;
			this.txtAll12Box15.Left = 1.1875F;
			this.txtAll12Box15.Name = "txtAll12Box15";
			this.txtAll12Box15.Text = null;
			this.txtAll12Box15.Top = 3.21875F;
			this.txtAll12Box15.Width = 0.5F;
			// 
			// txtBox15_1
			// 
			this.txtBox15_1.Height = 0.1770833F;
			this.txtBox15_1.Left = 1.877778F;
			this.txtBox15_1.Name = "txtBox15_1";
			this.txtBox15_1.Text = null;
			this.txtBox15_1.Top = 3.21875F;
			this.txtBox15_1.Width = 0.5F;
			// 
			// txtBox15_2
			// 
			this.txtBox15_2.Height = 0.1770833F;
			this.txtBox15_2.Left = 2.568056F;
			this.txtBox15_2.Name = "txtBox15_2";
			this.txtBox15_2.Text = null;
			this.txtBox15_2.Top = 3.21875F;
			this.txtBox15_2.Width = 0.5F;
			// 
			// txtBox15_3
			// 
			this.txtBox15_3.Height = 0.1770833F;
			this.txtBox15_3.Left = 3.258333F;
			this.txtBox15_3.Name = "txtBox15_3";
			this.txtBox15_3.Text = null;
			this.txtBox15_3.Top = 3.21875F;
			this.txtBox15_3.Width = 0.5F;
			// 
			// txtBox15_4
			// 
			this.txtBox15_4.Height = 0.1770833F;
			this.txtBox15_4.Left = 3.948611F;
			this.txtBox15_4.Name = "txtBox15_4";
			this.txtBox15_4.Text = null;
			this.txtBox15_4.Top = 3.21875F;
			this.txtBox15_4.Width = 0.5F;
			// 
			// txtBox15_5
			// 
			this.txtBox15_5.Height = 0.1770833F;
			this.txtBox15_5.Left = 4.638889F;
			this.txtBox15_5.Name = "txtBox15_5";
			this.txtBox15_5.Text = null;
			this.txtBox15_5.Top = 3.21875F;
			this.txtBox15_5.Width = 0.5F;
			// 
			// txtBox15_6
			// 
			this.txtBox15_6.Height = 0.1770833F;
			this.txtBox15_6.Left = 5.329167F;
			this.txtBox15_6.Name = "txtBox15_6";
			this.txtBox15_6.Text = null;
			this.txtBox15_6.Top = 3.21875F;
			this.txtBox15_6.Width = 0.5F;
			// 
			// txtBox15_7
			// 
			this.txtBox15_7.Height = 0.1770833F;
			this.txtBox15_7.Left = 6.019444F;
			this.txtBox15_7.Name = "txtBox15_7";
			this.txtBox15_7.Text = null;
			this.txtBox15_7.Top = 3.21875F;
			this.txtBox15_7.Width = 0.5F;
			// 
			// txtBox15_8
			// 
			this.txtBox15_8.Height = 0.1770833F;
			this.txtBox15_8.Left = 6.709722F;
			this.txtBox15_8.Name = "txtBox15_8";
			this.txtBox15_8.Text = null;
			this.txtBox15_8.Top = 3.21875F;
			this.txtBox15_8.Width = 0.5F;
			// 
			// txtBox15_9
			// 
			this.txtBox15_9.Height = 0.1770833F;
			this.txtBox15_9.Left = 7.4F;
			this.txtBox15_9.Name = "txtBox15_9";
			this.txtBox15_9.Text = null;
			this.txtBox15_9.Top = 3.21875F;
			this.txtBox15_9.Width = 0.5F;
			// 
			// txtBox15_10
			// 
			this.txtBox15_10.Height = 0.1770833F;
			this.txtBox15_10.Left = 8.090278F;
			this.txtBox15_10.Name = "txtBox15_10";
			this.txtBox15_10.Text = null;
			this.txtBox15_10.Top = 3.21875F;
			this.txtBox15_10.Width = 0.5F;
			// 
			// txtBox15_11
			// 
			this.txtBox15_11.Height = 0.1770833F;
			this.txtBox15_11.Left = 8.780556F;
			this.txtBox15_11.Name = "txtBox15_11";
			this.txtBox15_11.Text = null;
			this.txtBox15_11.Top = 3.21875F;
			this.txtBox15_11.Width = 0.5F;
			// 
			// txtBox15_12
			// 
			this.txtBox15_12.Height = 0.1770833F;
			this.txtBox15_12.Left = 9.470834F;
			this.txtBox15_12.Name = "txtBox15_12";
			this.txtBox15_12.Text = null;
			this.txtBox15_12.Top = 3.21875F;
			this.txtBox15_12.Width = 0.5F;
			// 
			// txtAll12Box16
			// 
			this.txtAll12Box16.Height = 0.1770833F;
			this.txtAll12Box16.Left = 1.114583F;
			this.txtAll12Box16.Name = "txtAll12Box16";
			this.txtAll12Box16.Text = null;
			this.txtAll12Box16.Top = 3.604167F;
			this.txtAll12Box16.Width = 0.5F;
			// 
			// txtBox16_1
			// 
			this.txtBox16_1.Height = 0.1770833F;
			this.txtBox16_1.Left = 1.802083F;
			this.txtBox16_1.Name = "txtBox16_1";
			this.txtBox16_1.Text = null;
			this.txtBox16_1.Top = 3.604167F;
			this.txtBox16_1.Width = 0.5F;
			// 
			// txtBox16_2
			// 
			this.txtBox16_2.Height = 0.1770833F;
			this.txtBox16_2.Left = 2.489583F;
			this.txtBox16_2.Name = "txtBox16_2";
			this.txtBox16_2.Text = null;
			this.txtBox16_2.Top = 3.604167F;
			this.txtBox16_2.Width = 0.5F;
			// 
			// txtBox16_3
			// 
			this.txtBox16_3.Height = 0.1770833F;
			this.txtBox16_3.Left = 3.177083F;
			this.txtBox16_3.Name = "txtBox16_3";
			this.txtBox16_3.Text = null;
			this.txtBox16_3.Top = 3.604167F;
			this.txtBox16_3.Width = 0.5F;
			// 
			// txtBox16_4
			// 
			this.txtBox16_4.Height = 0.1770833F;
			this.txtBox16_4.Left = 3.875F;
			this.txtBox16_4.Name = "txtBox16_4";
			this.txtBox16_4.Text = null;
			this.txtBox16_4.Top = 3.604167F;
			this.txtBox16_4.Width = 0.5F;
			// 
			// txtBox16_5
			// 
			this.txtBox16_5.Height = 0.1770833F;
			this.txtBox16_5.Left = 4.5625F;
			this.txtBox16_5.Name = "txtBox16_5";
			this.txtBox16_5.Text = null;
			this.txtBox16_5.Top = 3.604167F;
			this.txtBox16_5.Width = 0.5F;
			// 
			// txtBox16_6
			// 
			this.txtBox16_6.Height = 0.1770833F;
			this.txtBox16_6.Left = 5.25F;
			this.txtBox16_6.Name = "txtBox16_6";
			this.txtBox16_6.Text = null;
			this.txtBox16_6.Top = 3.604167F;
			this.txtBox16_6.Width = 0.5F;
			// 
			// txtBox16_7
			// 
			this.txtBox16_7.Height = 0.1770833F;
			this.txtBox16_7.Left = 5.96875F;
			this.txtBox16_7.Name = "txtBox16_7";
			this.txtBox16_7.Text = null;
			this.txtBox16_7.Top = 3.604167F;
			this.txtBox16_7.Width = 0.5F;
			// 
			// txtBox16_8
			// 
			this.txtBox16_8.Height = 0.1770833F;
			this.txtBox16_8.Left = 6.635417F;
			this.txtBox16_8.Name = "txtBox16_8";
			this.txtBox16_8.Text = null;
			this.txtBox16_8.Top = 3.604167F;
			this.txtBox16_8.Width = 0.5F;
			// 
			// txtBox16_9
			// 
			this.txtBox16_9.Height = 0.1770833F;
			this.txtBox16_9.Left = 7.322917F;
			this.txtBox16_9.Name = "txtBox16_9";
			this.txtBox16_9.Text = null;
			this.txtBox16_9.Top = 3.604167F;
			this.txtBox16_9.Width = 0.5F;
			// 
			// txtBox16_10
			// 
			this.txtBox16_10.Height = 0.1770833F;
			this.txtBox16_10.Left = 8.010417F;
			this.txtBox16_10.Name = "txtBox16_10";
			this.txtBox16_10.Text = null;
			this.txtBox16_10.Top = 3.604167F;
			this.txtBox16_10.Width = 0.5F;
			// 
			// txtBox16_11
			// 
			this.txtBox16_11.Height = 0.1770833F;
			this.txtBox16_11.Left = 8.708333F;
			this.txtBox16_11.Name = "txtBox16_11";
			this.txtBox16_11.Text = null;
			this.txtBox16_11.Top = 3.604167F;
			this.txtBox16_11.Width = 0.5F;
			// 
			// txtBox16_12
			// 
			this.txtBox16_12.Height = 0.1770833F;
			this.txtBox16_12.Left = 9.395833F;
			this.txtBox16_12.Name = "txtBox16_12";
			this.txtBox16_12.Text = null;
			this.txtBox16_12.Top = 3.604167F;
			this.txtBox16_12.Width = 0.5F;
			// 
			// txtCoveredName1
			// 
			this.txtCoveredName1.Height = 0.1770833F;
			this.txtCoveredName1.Left = 0.1666667F;
			this.txtCoveredName1.Name = "txtCoveredName1";
			this.txtCoveredName1.Text = null;
			this.txtCoveredName1.Top = 4.660417F;
			this.txtCoveredName1.Width = 2.166667F;
			// 
			// txtCoveredIndividuals
			// 
			this.txtCoveredIndividuals.Height = 0.1770833F;
			this.txtCoveredIndividuals.Left = 9.104167F;
			this.txtCoveredIndividuals.Name = "txtCoveredIndividuals";
			this.txtCoveredIndividuals.Text = "X";
			this.txtCoveredIndividuals.Top = 4F;
			this.txtCoveredIndividuals.Width = 0.25F;
			// 
			// txtCoveredSSN1
			// 
			this.txtCoveredSSN1.Height = 0.1770833F;
			this.txtCoveredSSN1.Left = 2.5F;
			this.txtCoveredSSN1.Name = "txtCoveredSSN1";
			this.txtCoveredSSN1.Text = null;
			this.txtCoveredSSN1.Top = 4.660417F;
			this.txtCoveredSSN1.Width = 1F;
			// 
			// txtCoveredDOB1
			// 
			this.txtCoveredDOB1.Height = 0.1770833F;
			this.txtCoveredDOB1.Left = 3.666667F;
			this.txtCoveredDOB1.Name = "txtCoveredDOB1";
			this.txtCoveredDOB1.Text = null;
			this.txtCoveredDOB1.Top = 4.660417F;
			this.txtCoveredDOB1.Width = 0.9166667F;
			// 
			// txtCoveredName2
			// 
			this.txtCoveredName2.Height = 0.1770833F;
			this.txtCoveredName2.Left = 0.1666667F;
			this.txtCoveredName2.Name = "txtCoveredName2";
			this.txtCoveredName2.Text = null;
			this.txtCoveredName2.Top = 5.158333F;
			this.txtCoveredName2.Width = 2.166667F;
			// 
			// txtCoveredSSN2
			// 
			this.txtCoveredSSN2.Height = 0.1770833F;
			this.txtCoveredSSN2.Left = 2.5F;
			this.txtCoveredSSN2.Name = "txtCoveredSSN2";
			this.txtCoveredSSN2.Text = null;
			this.txtCoveredSSN2.Top = 5.158333F;
			this.txtCoveredSSN2.Width = 1F;
			// 
			// txtCoveredDOB2
			// 
			this.txtCoveredDOB2.Height = 0.1770833F;
			this.txtCoveredDOB2.Left = 3.666667F;
			this.txtCoveredDOB2.Name = "txtCoveredDOB2";
			this.txtCoveredDOB2.Text = null;
			this.txtCoveredDOB2.Top = 5.158333F;
			this.txtCoveredDOB2.Width = 0.9166667F;
			// 
			// txtCoveredName3
			// 
			this.txtCoveredName3.Height = 0.1770833F;
			this.txtCoveredName3.Left = 0.1666667F;
			this.txtCoveredName3.Name = "txtCoveredName3";
			this.txtCoveredName3.Text = null;
			this.txtCoveredName3.Top = 5.65625F;
			this.txtCoveredName3.Width = 2.166667F;
			// 
			// txtCoveredSSN3
			// 
			this.txtCoveredSSN3.Height = 0.1770833F;
			this.txtCoveredSSN3.Left = 2.5F;
			this.txtCoveredSSN3.Name = "txtCoveredSSN3";
			this.txtCoveredSSN3.Text = null;
			this.txtCoveredSSN3.Top = 5.65625F;
			this.txtCoveredSSN3.Width = 1F;
			// 
			// txtCoveredDOB3
			// 
			this.txtCoveredDOB3.Height = 0.1770833F;
			this.txtCoveredDOB3.Left = 3.666667F;
			this.txtCoveredDOB3.Name = "txtCoveredDOB3";
			this.txtCoveredDOB3.Text = null;
			this.txtCoveredDOB3.Top = 5.65625F;
			this.txtCoveredDOB3.Width = 0.9166667F;
			// 
			// txtCoveredName4
			// 
			this.txtCoveredName4.Height = 0.1770833F;
			this.txtCoveredName4.Left = 0.1666667F;
			this.txtCoveredName4.Name = "txtCoveredName4";
			this.txtCoveredName4.Text = null;
			this.txtCoveredName4.Top = 6.154167F;
			this.txtCoveredName4.Width = 2.166667F;
			// 
			// txtCoveredSSN4
			// 
			this.txtCoveredSSN4.Height = 0.1770833F;
			this.txtCoveredSSN4.Left = 2.5F;
			this.txtCoveredSSN4.Name = "txtCoveredSSN4";
			this.txtCoveredSSN4.Text = null;
			this.txtCoveredSSN4.Top = 6.154167F;
			this.txtCoveredSSN4.Width = 1F;
			// 
			// txtCoveredDOB4
			// 
			this.txtCoveredDOB4.Height = 0.1770833F;
			this.txtCoveredDOB4.Left = 3.666667F;
			this.txtCoveredDOB4.Name = "txtCoveredDOB4";
			this.txtCoveredDOB4.Text = null;
			this.txtCoveredDOB4.Top = 6.154167F;
			this.txtCoveredDOB4.Width = 0.9166667F;
			// 
			// txtCoveredName5
			// 
			this.txtCoveredName5.Height = 0.1770833F;
			this.txtCoveredName5.Left = 0.1666667F;
			this.txtCoveredName5.Name = "txtCoveredName5";
			this.txtCoveredName5.Text = null;
			this.txtCoveredName5.Top = 6.652083F;
			this.txtCoveredName5.Width = 2.166667F;
			// 
			// txtCoveredSSN5
			// 
			this.txtCoveredSSN5.Height = 0.1770833F;
			this.txtCoveredSSN5.Left = 2.5F;
			this.txtCoveredSSN5.Name = "txtCoveredSSN5";
			this.txtCoveredSSN5.Text = null;
			this.txtCoveredSSN5.Top = 6.652083F;
			this.txtCoveredSSN5.Width = 1F;
			// 
			// txtCoveredDOB5
			// 
			this.txtCoveredDOB5.Height = 0.1770833F;
			this.txtCoveredDOB5.Left = 3.666667F;
			this.txtCoveredDOB5.Name = "txtCoveredDOB5";
			this.txtCoveredDOB5.Text = null;
			this.txtCoveredDOB5.Top = 6.652083F;
			this.txtCoveredDOB5.Width = 0.9166667F;
			// 
			// txtCoveredName6
			// 
			this.txtCoveredName6.Height = 0.1770833F;
			this.txtCoveredName6.Left = 0.1666667F;
			this.txtCoveredName6.Name = "txtCoveredName6";
			this.txtCoveredName6.Text = null;
			this.txtCoveredName6.Top = 7.145833F;
			this.txtCoveredName6.Width = 2.166667F;
			// 
			// txtCoveredSSN6
			// 
			this.txtCoveredSSN6.Height = 0.1770833F;
			this.txtCoveredSSN6.Left = 2.5F;
			this.txtCoveredSSN6.Name = "txtCoveredSSN6";
			this.txtCoveredSSN6.Text = null;
			this.txtCoveredSSN6.Top = 7.145833F;
			this.txtCoveredSSN6.Width = 1F;
			// 
			// txtCoveredDOB6
			// 
			this.txtCoveredDOB6.Height = 0.1770833F;
			this.txtCoveredDOB6.Left = 3.666667F;
			this.txtCoveredDOB6.Name = "txtCoveredDOB6";
			this.txtCoveredDOB6.Text = null;
			this.txtCoveredDOB6.Top = 7.145833F;
			this.txtCoveredDOB6.Width = 0.9166667F;
			// 
			// txtCoveredAll12_1
			// 
			this.txtCoveredAll12_1.Height = 0.1770833F;
			this.txtCoveredAll12_1.Left = 4.885417F;
			this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
			this.txtCoveredAll12_1.Text = "X";
			this.txtCoveredAll12_1.Top = 4.660417F;
			this.txtCoveredAll12_1.Width = 0.25F;
			// 
			// txtCoveredMonth1_1
			// 
			this.txtCoveredMonth1_1.Height = 0.1770833F;
			this.txtCoveredMonth1_1.Left = 5.395833F;
			this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
			this.txtCoveredMonth1_1.Text = "X";
			this.txtCoveredMonth1_1.Top = 4.660417F;
			this.txtCoveredMonth1_1.Width = 0.25F;
			// 
			// txtCoveredMonth2_1
			// 
			this.txtCoveredMonth2_1.Height = 0.1770833F;
			this.txtCoveredMonth2_1.Left = 5.789583F;
			this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
			this.txtCoveredMonth2_1.Text = null;
			this.txtCoveredMonth2_1.Top = 4.660417F;
			this.txtCoveredMonth2_1.Width = 0.25F;
			// 
			// txtCoveredMonth3_1
			// 
			this.txtCoveredMonth3_1.Height = 0.1770833F;
			this.txtCoveredMonth3_1.Left = 6.183333F;
			this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
			this.txtCoveredMonth3_1.Text = null;
			this.txtCoveredMonth3_1.Top = 4.660417F;
			this.txtCoveredMonth3_1.Width = 0.25F;
			// 
			// txtCoveredMonth4_1
			// 
			this.txtCoveredMonth4_1.Height = 0.1770833F;
			this.txtCoveredMonth4_1.Left = 6.577083F;
			this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
			this.txtCoveredMonth4_1.Text = null;
			this.txtCoveredMonth4_1.Top = 4.660417F;
			this.txtCoveredMonth4_1.Width = 0.25F;
			// 
			// txtCoveredMonth5_1
			// 
			this.txtCoveredMonth5_1.Height = 0.1770833F;
			this.txtCoveredMonth5_1.Left = 6.970833F;
			this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
			this.txtCoveredMonth5_1.Text = null;
			this.txtCoveredMonth5_1.Top = 4.660417F;
			this.txtCoveredMonth5_1.Width = 0.25F;
			// 
			// txtCoveredMonth6_1
			// 
			this.txtCoveredMonth6_1.Height = 0.1770833F;
			this.txtCoveredMonth6_1.Left = 7.364583F;
			this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
			this.txtCoveredMonth6_1.Text = null;
			this.txtCoveredMonth6_1.Top = 4.660417F;
			this.txtCoveredMonth6_1.Width = 0.25F;
			// 
			// txtCoveredMonth7_1
			// 
			this.txtCoveredMonth7_1.Height = 0.1770833F;
			this.txtCoveredMonth7_1.Left = 7.758333F;
			this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
			this.txtCoveredMonth7_1.Text = null;
			this.txtCoveredMonth7_1.Top = 4.660417F;
			this.txtCoveredMonth7_1.Width = 0.25F;
			// 
			// txtCoveredMonth8_1
			// 
			this.txtCoveredMonth8_1.Height = 0.1770833F;
			this.txtCoveredMonth8_1.Left = 8.152083F;
			this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
			this.txtCoveredMonth8_1.Text = null;
			this.txtCoveredMonth8_1.Top = 4.660417F;
			this.txtCoveredMonth8_1.Width = 0.25F;
			// 
			// txtCoveredMonth9_1
			// 
			this.txtCoveredMonth9_1.Height = 0.1770833F;
			this.txtCoveredMonth9_1.Left = 8.545834F;
			this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
			this.txtCoveredMonth9_1.Text = null;
			this.txtCoveredMonth9_1.Top = 4.660417F;
			this.txtCoveredMonth9_1.Width = 0.25F;
			// 
			// txtCoveredMonth10_1
			// 
			this.txtCoveredMonth10_1.Height = 0.1770833F;
			this.txtCoveredMonth10_1.Left = 8.939584F;
			this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
			this.txtCoveredMonth10_1.Text = null;
			this.txtCoveredMonth10_1.Top = 4.660417F;
			this.txtCoveredMonth10_1.Width = 0.25F;
			// 
			// txtCoveredMonth11_1
			// 
			this.txtCoveredMonth11_1.Height = 0.1770833F;
			this.txtCoveredMonth11_1.Left = 9.333333F;
			this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
			this.txtCoveredMonth11_1.Text = null;
			this.txtCoveredMonth11_1.Top = 4.660417F;
			this.txtCoveredMonth11_1.Width = 0.25F;
			// 
			// txtCoveredMonth12_1
			// 
			this.txtCoveredMonth12_1.Height = 0.1770833F;
			this.txtCoveredMonth12_1.Left = 9.729167F;
			this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
			this.txtCoveredMonth12_1.Text = null;
			this.txtCoveredMonth12_1.Top = 4.65625F;
			this.txtCoveredMonth12_1.Width = 0.25F;
			// 
			// txtCoveredAll12_2
			// 
			this.txtCoveredAll12_2.Height = 0.1770833F;
			this.txtCoveredAll12_2.Left = 4.885417F;
			this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
			this.txtCoveredAll12_2.Text = null;
			this.txtCoveredAll12_2.Top = 5.15625F;
			this.txtCoveredAll12_2.Width = 0.25F;
			// 
			// txtCoveredMonth1_2
			// 
			this.txtCoveredMonth1_2.Height = 0.1770833F;
			this.txtCoveredMonth1_2.Left = 5.395833F;
			this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
			this.txtCoveredMonth1_2.Text = null;
			this.txtCoveredMonth1_2.Top = 5.15625F;
			this.txtCoveredMonth1_2.Width = 0.25F;
			// 
			// txtCoveredMonth2_2
			// 
			this.txtCoveredMonth2_2.Height = 0.1770833F;
			this.txtCoveredMonth2_2.Left = 5.791667F;
			this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
			this.txtCoveredMonth2_2.Text = null;
			this.txtCoveredMonth2_2.Top = 5.15625F;
			this.txtCoveredMonth2_2.Width = 0.25F;
			// 
			// txtCoveredMonth3_2
			// 
			this.txtCoveredMonth3_2.Height = 0.1770833F;
			this.txtCoveredMonth3_2.Left = 6.183333F;
			this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
			this.txtCoveredMonth3_2.Text = null;
			this.txtCoveredMonth3_2.Top = 5.158333F;
			this.txtCoveredMonth3_2.Width = 0.25F;
			// 
			// txtCoveredMonth4_2
			// 
			this.txtCoveredMonth4_2.Height = 0.1770833F;
			this.txtCoveredMonth4_2.Left = 6.572917F;
			this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
			this.txtCoveredMonth4_2.Text = null;
			this.txtCoveredMonth4_2.Top = 5.15625F;
			this.txtCoveredMonth4_2.Width = 0.25F;
			// 
			// txtCoveredMonth5_2
			// 
			this.txtCoveredMonth5_2.Height = 0.1770833F;
			this.txtCoveredMonth5_2.Left = 6.96875F;
			this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
			this.txtCoveredMonth5_2.Text = null;
			this.txtCoveredMonth5_2.Top = 5.15625F;
			this.txtCoveredMonth5_2.Width = 0.25F;
			// 
			// txtCoveredMonth6_2
			// 
			this.txtCoveredMonth6_2.Height = 0.1770833F;
			this.txtCoveredMonth6_2.Left = 7.364583F;
			this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
			this.txtCoveredMonth6_2.Text = null;
			this.txtCoveredMonth6_2.Top = 5.15625F;
			this.txtCoveredMonth6_2.Width = 0.25F;
			// 
			// txtCoveredMonth7_2
			// 
			this.txtCoveredMonth7_2.Height = 0.1770833F;
			this.txtCoveredMonth7_2.Left = 7.760417F;
			this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
			this.txtCoveredMonth7_2.Text = null;
			this.txtCoveredMonth7_2.Top = 5.15625F;
			this.txtCoveredMonth7_2.Width = 0.25F;
			// 
			// txtCoveredMonth8_2
			// 
			this.txtCoveredMonth8_2.Height = 0.1770833F;
			this.txtCoveredMonth8_2.Left = 8.15625F;
			this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
			this.txtCoveredMonth8_2.Text = null;
			this.txtCoveredMonth8_2.Top = 5.15625F;
			this.txtCoveredMonth8_2.Width = 0.25F;
			// 
			// txtCoveredMonth9_2
			// 
			this.txtCoveredMonth9_2.Height = 0.1770833F;
			this.txtCoveredMonth9_2.Left = 8.545834F;
			this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
			this.txtCoveredMonth9_2.Text = null;
			this.txtCoveredMonth9_2.Top = 5.158333F;
			this.txtCoveredMonth9_2.Width = 0.25F;
			// 
			// txtCoveredMonth10_2
			// 
			this.txtCoveredMonth10_2.Height = 0.1770833F;
			this.txtCoveredMonth10_2.Left = 8.939584F;
			this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
			this.txtCoveredMonth10_2.Text = null;
			this.txtCoveredMonth10_2.Top = 5.158333F;
			this.txtCoveredMonth10_2.Width = 0.25F;
			// 
			// txtCoveredMonth11_2
			// 
			this.txtCoveredMonth11_2.Height = 0.1770833F;
			this.txtCoveredMonth11_2.Left = 9.333333F;
			this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
			this.txtCoveredMonth11_2.Text = null;
			this.txtCoveredMonth11_2.Top = 5.158333F;
			this.txtCoveredMonth11_2.Width = 0.25F;
			// 
			// txtCoveredMonth12_2
			// 
			this.txtCoveredMonth12_2.Height = 0.1770833F;
			this.txtCoveredMonth12_2.Left = 9.729167F;
			this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
			this.txtCoveredMonth12_2.Text = null;
			this.txtCoveredMonth12_2.Top = 5.15625F;
			this.txtCoveredMonth12_2.Width = 0.25F;
			// 
			// txtCoveredAll12_3
			// 
			this.txtCoveredAll12_3.Height = 0.1770833F;
			this.txtCoveredAll12_3.Left = 4.885417F;
			this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
			this.txtCoveredAll12_3.Text = null;
			this.txtCoveredAll12_3.Top = 5.65625F;
			this.txtCoveredAll12_3.Width = 0.25F;
			// 
			// txtCoveredMonth1_3
			// 
			this.txtCoveredMonth1_3.Height = 0.1770833F;
			this.txtCoveredMonth1_3.Left = 5.395833F;
			this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
			this.txtCoveredMonth1_3.Text = null;
			this.txtCoveredMonth1_3.Top = 5.65625F;
			this.txtCoveredMonth1_3.Width = 0.25F;
			// 
			// txtCoveredMonth2_3
			// 
			this.txtCoveredMonth2_3.Height = 0.1770833F;
			this.txtCoveredMonth2_3.Left = 5.791667F;
			this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
			this.txtCoveredMonth2_3.Text = null;
			this.txtCoveredMonth2_3.Top = 5.65625F;
			this.txtCoveredMonth2_3.Width = 0.25F;
			// 
			// txtCoveredMonth3_3
			// 
			this.txtCoveredMonth3_3.Height = 0.1770833F;
			this.txtCoveredMonth3_3.Left = 6.183333F;
			this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
			this.txtCoveredMonth3_3.Text = null;
			this.txtCoveredMonth3_3.Top = 5.65625F;
			this.txtCoveredMonth3_3.Width = 0.25F;
			// 
			// txtCoveredMonth4_3
			// 
			this.txtCoveredMonth4_3.Height = 0.1770833F;
			this.txtCoveredMonth4_3.Left = 6.572917F;
			this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
			this.txtCoveredMonth4_3.Text = null;
			this.txtCoveredMonth4_3.Top = 5.65625F;
			this.txtCoveredMonth4_3.Width = 0.25F;
			// 
			// txtCoveredMonth5_3
			// 
			this.txtCoveredMonth5_3.Height = 0.1770833F;
			this.txtCoveredMonth5_3.Left = 6.96875F;
			this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
			this.txtCoveredMonth5_3.Text = null;
			this.txtCoveredMonth5_3.Top = 5.65625F;
			this.txtCoveredMonth5_3.Width = 0.25F;
			// 
			// txtCoveredMonth6_3
			// 
			this.txtCoveredMonth6_3.Height = 0.1770833F;
			this.txtCoveredMonth6_3.Left = 7.364583F;
			this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
			this.txtCoveredMonth6_3.Text = null;
			this.txtCoveredMonth6_3.Top = 5.65625F;
			this.txtCoveredMonth6_3.Width = 0.25F;
			// 
			// txtCoveredMonth7_3
			// 
			this.txtCoveredMonth7_3.Height = 0.1770833F;
			this.txtCoveredMonth7_3.Left = 7.760417F;
			this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
			this.txtCoveredMonth7_3.Text = null;
			this.txtCoveredMonth7_3.Top = 5.65625F;
			this.txtCoveredMonth7_3.Width = 0.25F;
			// 
			// txtCoveredMonth8_3
			// 
			this.txtCoveredMonth8_3.Height = 0.1770833F;
			this.txtCoveredMonth8_3.Left = 8.15625F;
			this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
			this.txtCoveredMonth8_3.Text = null;
			this.txtCoveredMonth8_3.Top = 5.65625F;
			this.txtCoveredMonth8_3.Width = 0.25F;
			// 
			// txtCoveredMonth9_3
			// 
			this.txtCoveredMonth9_3.Height = 0.1770833F;
			this.txtCoveredMonth9_3.Left = 8.545834F;
			this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
			this.txtCoveredMonth9_3.Text = null;
			this.txtCoveredMonth9_3.Top = 5.65625F;
			this.txtCoveredMonth9_3.Width = 0.25F;
			// 
			// txtCoveredMonth10_3
			// 
			this.txtCoveredMonth10_3.Height = 0.1770833F;
			this.txtCoveredMonth10_3.Left = 8.939584F;
			this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
			this.txtCoveredMonth10_3.Text = null;
			this.txtCoveredMonth10_3.Top = 5.65625F;
			this.txtCoveredMonth10_3.Width = 0.25F;
			// 
			// txtCoveredMonth11_3
			// 
			this.txtCoveredMonth11_3.Height = 0.1770833F;
			this.txtCoveredMonth11_3.Left = 9.333333F;
			this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
			this.txtCoveredMonth11_3.Text = null;
			this.txtCoveredMonth11_3.Top = 5.65625F;
			this.txtCoveredMonth11_3.Width = 0.25F;
			// 
			// txtCoveredMonth12_3
			// 
			this.txtCoveredMonth12_3.Height = 0.1770833F;
			this.txtCoveredMonth12_3.Left = 9.727083F;
			this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
			this.txtCoveredMonth12_3.Text = null;
			this.txtCoveredMonth12_3.Top = 5.65625F;
			this.txtCoveredMonth12_3.Width = 0.25F;
			// 
			// txtCoveredAll12_4
			// 
			this.txtCoveredAll12_4.Height = 0.1770833F;
			this.txtCoveredAll12_4.Left = 4.885417F;
			this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
			this.txtCoveredAll12_4.Text = null;
			this.txtCoveredAll12_4.Top = 6.15625F;
			this.txtCoveredAll12_4.Width = 0.25F;
			// 
			// txtCoveredMonth1_4
			// 
			this.txtCoveredMonth1_4.Height = 0.1770833F;
			this.txtCoveredMonth1_4.Left = 5.395833F;
			this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
			this.txtCoveredMonth1_4.Text = null;
			this.txtCoveredMonth1_4.Top = 6.15625F;
			this.txtCoveredMonth1_4.Width = 0.25F;
			// 
			// txtCoveredMonth2_4
			// 
			this.txtCoveredMonth2_4.Height = 0.1770833F;
			this.txtCoveredMonth2_4.Left = 5.791667F;
			this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
			this.txtCoveredMonth2_4.Text = null;
			this.txtCoveredMonth2_4.Top = 6.15625F;
			this.txtCoveredMonth2_4.Width = 0.25F;
			// 
			// txtCoveredMonth3_4
			// 
			this.txtCoveredMonth3_4.Height = 0.1770833F;
			this.txtCoveredMonth3_4.Left = 6.183333F;
			this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
			this.txtCoveredMonth3_4.Text = null;
			this.txtCoveredMonth3_4.Top = 6.154167F;
			this.txtCoveredMonth3_4.Width = 0.25F;
			// 
			// txtCoveredMonth4_4
			// 
			this.txtCoveredMonth4_4.Height = 0.1770833F;
			this.txtCoveredMonth4_4.Left = 6.572917F;
			this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
			this.txtCoveredMonth4_4.Text = null;
			this.txtCoveredMonth4_4.Top = 6.15625F;
			this.txtCoveredMonth4_4.Width = 0.25F;
			// 
			// txtCoveredMonth5_4
			// 
			this.txtCoveredMonth5_4.Height = 0.1770833F;
			this.txtCoveredMonth5_4.Left = 6.96875F;
			this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
			this.txtCoveredMonth5_4.Text = null;
			this.txtCoveredMonth5_4.Top = 6.15625F;
			this.txtCoveredMonth5_4.Width = 0.25F;
			// 
			// txtCoveredMonth6_4
			// 
			this.txtCoveredMonth6_4.Height = 0.1770833F;
			this.txtCoveredMonth6_4.Left = 7.364583F;
			this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
			this.txtCoveredMonth6_4.Text = null;
			this.txtCoveredMonth6_4.Top = 6.15625F;
			this.txtCoveredMonth6_4.Width = 0.25F;
			// 
			// txtCoveredMonth7_4
			// 
			this.txtCoveredMonth7_4.Height = 0.1770833F;
			this.txtCoveredMonth7_4.Left = 7.760417F;
			this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
			this.txtCoveredMonth7_4.Text = null;
			this.txtCoveredMonth7_4.Top = 6.15625F;
			this.txtCoveredMonth7_4.Width = 0.25F;
			// 
			// txtCoveredMonth8_4
			// 
			this.txtCoveredMonth8_4.Height = 0.1770833F;
			this.txtCoveredMonth8_4.Left = 8.15625F;
			this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
			this.txtCoveredMonth8_4.Text = null;
			this.txtCoveredMonth8_4.Top = 6.15625F;
			this.txtCoveredMonth8_4.Width = 0.25F;
			// 
			// txtCoveredMonth9_4
			// 
			this.txtCoveredMonth9_4.Height = 0.1770833F;
			this.txtCoveredMonth9_4.Left = 8.545834F;
			this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
			this.txtCoveredMonth9_4.Text = null;
			this.txtCoveredMonth9_4.Top = 6.154167F;
			this.txtCoveredMonth9_4.Width = 0.25F;
			// 
			// txtCoveredMonth10_4
			// 
			this.txtCoveredMonth10_4.Height = 0.1770833F;
			this.txtCoveredMonth10_4.Left = 8.939584F;
			this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
			this.txtCoveredMonth10_4.Text = null;
			this.txtCoveredMonth10_4.Top = 6.154167F;
			this.txtCoveredMonth10_4.Width = 0.25F;
			// 
			// txtCoveredMonth11_4
			// 
			this.txtCoveredMonth11_4.Height = 0.1770833F;
			this.txtCoveredMonth11_4.Left = 9.333333F;
			this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
			this.txtCoveredMonth11_4.Text = null;
			this.txtCoveredMonth11_4.Top = 6.154167F;
			this.txtCoveredMonth11_4.Width = 0.25F;
			// 
			// txtCoveredMonth12_4
			// 
			this.txtCoveredMonth12_4.Height = 0.1770833F;
			this.txtCoveredMonth12_4.Left = 9.727083F;
			this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
			this.txtCoveredMonth12_4.Text = null;
			this.txtCoveredMonth12_4.Top = 6.154167F;
			this.txtCoveredMonth12_4.Width = 0.25F;
			// 
			// txtCoveredAll12_5
			// 
			this.txtCoveredAll12_5.Height = 0.1770833F;
			this.txtCoveredAll12_5.Left = 4.885417F;
			this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
			this.txtCoveredAll12_5.Text = null;
			this.txtCoveredAll12_5.Top = 6.65625F;
			this.txtCoveredAll12_5.Width = 0.25F;
			// 
			// txtCoveredMonth1_5
			// 
			this.txtCoveredMonth1_5.Height = 0.1770833F;
			this.txtCoveredMonth1_5.Left = 5.395833F;
			this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
			this.txtCoveredMonth1_5.Text = null;
			this.txtCoveredMonth1_5.Top = 6.65625F;
			this.txtCoveredMonth1_5.Width = 0.25F;
			// 
			// txtCoveredMonth2_5
			// 
			this.txtCoveredMonth2_5.Height = 0.1770833F;
			this.txtCoveredMonth2_5.Left = 5.791667F;
			this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
			this.txtCoveredMonth2_5.Text = null;
			this.txtCoveredMonth2_5.Top = 6.65625F;
			this.txtCoveredMonth2_5.Width = 0.25F;
			// 
			// txtCoveredMonth3_5
			// 
			this.txtCoveredMonth3_5.Height = 0.1770833F;
			this.txtCoveredMonth3_5.Left = 6.183333F;
			this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
			this.txtCoveredMonth3_5.Text = null;
			this.txtCoveredMonth3_5.Top = 6.652083F;
			this.txtCoveredMonth3_5.Width = 0.25F;
			// 
			// txtCoveredMonth4_5
			// 
			this.txtCoveredMonth4_5.Height = 0.1770833F;
			this.txtCoveredMonth4_5.Left = 6.572917F;
			this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
			this.txtCoveredMonth4_5.Text = null;
			this.txtCoveredMonth4_5.Top = 6.65625F;
			this.txtCoveredMonth4_5.Width = 0.25F;
			// 
			// txtCoveredMonth5_5
			// 
			this.txtCoveredMonth5_5.Height = 0.1770833F;
			this.txtCoveredMonth5_5.Left = 6.96875F;
			this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
			this.txtCoveredMonth5_5.Text = null;
			this.txtCoveredMonth5_5.Top = 6.65625F;
			this.txtCoveredMonth5_5.Width = 0.25F;
			// 
			// txtCoveredMonth6_5
			// 
			this.txtCoveredMonth6_5.Height = 0.1770833F;
			this.txtCoveredMonth6_5.Left = 7.364583F;
			this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
			this.txtCoveredMonth6_5.Text = null;
			this.txtCoveredMonth6_5.Top = 6.65625F;
			this.txtCoveredMonth6_5.Width = 0.25F;
			// 
			// txtCoveredMonth7_5
			// 
			this.txtCoveredMonth7_5.Height = 0.1770833F;
			this.txtCoveredMonth7_5.Left = 7.760417F;
			this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
			this.txtCoveredMonth7_5.Text = null;
			this.txtCoveredMonth7_5.Top = 6.65625F;
			this.txtCoveredMonth7_5.Width = 0.25F;
			// 
			// txtCoveredMonth8_5
			// 
			this.txtCoveredMonth8_5.Height = 0.1770833F;
			this.txtCoveredMonth8_5.Left = 8.15625F;
			this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
			this.txtCoveredMonth8_5.Text = null;
			this.txtCoveredMonth8_5.Top = 6.65625F;
			this.txtCoveredMonth8_5.Width = 0.25F;
			// 
			// txtCoveredMonth9_5
			// 
			this.txtCoveredMonth9_5.Height = 0.1770833F;
			this.txtCoveredMonth9_5.Left = 8.545834F;
			this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
			this.txtCoveredMonth9_5.Text = null;
			this.txtCoveredMonth9_5.Top = 6.652083F;
			this.txtCoveredMonth9_5.Width = 0.25F;
			// 
			// txtCoveredMonth10_5
			// 
			this.txtCoveredMonth10_5.Height = 0.1770833F;
			this.txtCoveredMonth10_5.Left = 8.939584F;
			this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
			this.txtCoveredMonth10_5.Text = null;
			this.txtCoveredMonth10_5.Top = 6.652083F;
			this.txtCoveredMonth10_5.Width = 0.25F;
			// 
			// txtCoveredMonth11_5
			// 
			this.txtCoveredMonth11_5.Height = 0.1770833F;
			this.txtCoveredMonth11_5.Left = 9.333333F;
			this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
			this.txtCoveredMonth11_5.Text = null;
			this.txtCoveredMonth11_5.Top = 6.652083F;
			this.txtCoveredMonth11_5.Width = 0.25F;
			// 
			// txtCoveredMonth12_5
			// 
			this.txtCoveredMonth12_5.Height = 0.1770833F;
			this.txtCoveredMonth12_5.Left = 9.727083F;
			this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
			this.txtCoveredMonth12_5.Text = null;
			this.txtCoveredMonth12_5.Top = 6.652083F;
			this.txtCoveredMonth12_5.Width = 0.25F;
			// 
			// txtCoveredAll12_6
			// 
			this.txtCoveredAll12_6.Height = 0.1770833F;
			this.txtCoveredAll12_6.Left = 4.885417F;
			this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
			this.txtCoveredAll12_6.Text = null;
			this.txtCoveredAll12_6.Top = 7.145833F;
			this.txtCoveredAll12_6.Width = 0.25F;
			// 
			// txtCoveredMonth1_6
			// 
			this.txtCoveredMonth1_6.Height = 0.1770833F;
			this.txtCoveredMonth1_6.Left = 5.395833F;
			this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
			this.txtCoveredMonth1_6.Text = null;
			this.txtCoveredMonth1_6.Top = 7.145833F;
			this.txtCoveredMonth1_6.Width = 0.25F;
			// 
			// txtCoveredMonth2_6
			// 
			this.txtCoveredMonth2_6.Height = 0.1770833F;
			this.txtCoveredMonth2_6.Left = 5.791667F;
			this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
			this.txtCoveredMonth2_6.Text = null;
			this.txtCoveredMonth2_6.Top = 7.145833F;
			this.txtCoveredMonth2_6.Width = 0.25F;
			// 
			// txtCoveredMonth3_6
			// 
			this.txtCoveredMonth3_6.Height = 0.1770833F;
			this.txtCoveredMonth3_6.Left = 6.183333F;
			this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
			this.txtCoveredMonth3_6.Text = null;
			this.txtCoveredMonth3_6.Top = 7.15F;
			this.txtCoveredMonth3_6.Width = 0.25F;
			// 
			// txtCoveredMonth4_6
			// 
			this.txtCoveredMonth4_6.Height = 0.1770833F;
			this.txtCoveredMonth4_6.Left = 6.572917F;
			this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
			this.txtCoveredMonth4_6.Text = null;
			this.txtCoveredMonth4_6.Top = 7.145833F;
			this.txtCoveredMonth4_6.Width = 0.25F;
			// 
			// txtCoveredMonth5_6
			// 
			this.txtCoveredMonth5_6.Height = 0.1770833F;
			this.txtCoveredMonth5_6.Left = 6.96875F;
			this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
			this.txtCoveredMonth5_6.Text = null;
			this.txtCoveredMonth5_6.Top = 7.145833F;
			this.txtCoveredMonth5_6.Width = 0.25F;
			// 
			// txtCoveredMonth6_6
			// 
			this.txtCoveredMonth6_6.Height = 0.1770833F;
			this.txtCoveredMonth6_6.Left = 7.364583F;
			this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
			this.txtCoveredMonth6_6.Text = null;
			this.txtCoveredMonth6_6.Top = 7.145833F;
			this.txtCoveredMonth6_6.Width = 0.25F;
			// 
			// txtCoveredMonth7_6
			// 
			this.txtCoveredMonth7_6.Height = 0.1770833F;
			this.txtCoveredMonth7_6.Left = 7.760417F;
			this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
			this.txtCoveredMonth7_6.Text = null;
			this.txtCoveredMonth7_6.Top = 7.145833F;
			this.txtCoveredMonth7_6.Width = 0.25F;
			// 
			// txtCoveredMonth8_6
			// 
			this.txtCoveredMonth8_6.Height = 0.1770833F;
			this.txtCoveredMonth8_6.Left = 8.15625F;
			this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
			this.txtCoveredMonth8_6.Text = null;
			this.txtCoveredMonth8_6.Top = 7.145833F;
			this.txtCoveredMonth8_6.Width = 0.25F;
			// 
			// txtCoveredMonth9_6
			// 
			this.txtCoveredMonth9_6.Height = 0.1770833F;
			this.txtCoveredMonth9_6.Left = 8.545834F;
			this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
			this.txtCoveredMonth9_6.Text = null;
			this.txtCoveredMonth9_6.Top = 7.15F;
			this.txtCoveredMonth9_6.Width = 0.25F;
			// 
			// txtCoveredMonth10_6
			// 
			this.txtCoveredMonth10_6.Height = 0.1770833F;
			this.txtCoveredMonth10_6.Left = 8.939584F;
			this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
			this.txtCoveredMonth10_6.Text = null;
			this.txtCoveredMonth10_6.Top = 7.15F;
			this.txtCoveredMonth10_6.Width = 0.25F;
			// 
			// txtCoveredMonth11_6
			// 
			this.txtCoveredMonth11_6.Height = 0.1770833F;
			this.txtCoveredMonth11_6.Left = 9.333333F;
			this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
			this.txtCoveredMonth11_6.Text = null;
			this.txtCoveredMonth11_6.Top = 7.15F;
			this.txtCoveredMonth11_6.Width = 0.25F;
			// 
			// txtCoveredMonth12_6
			// 
			this.txtCoveredMonth12_6.Height = 0.1770833F;
			this.txtCoveredMonth12_6.Left = 9.727083F;
			this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
			this.txtCoveredMonth12_6.Text = null;
			this.txtCoveredMonth12_6.Top = 7.15F;
			this.txtCoveredMonth12_6.Width = 0.25F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1770833F;
			this.txtCity.Left = 0.08333334F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "white-space: nowrap";
			this.txtCity.Text = null;
			this.txtCity.Top = 2.010417F;
			this.txtCity.Width = 1.416667F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1770833F;
			this.txtState.Left = 1.583333F;
			this.txtState.Name = "txtState";
			this.txtState.Text = null;
			this.txtState.Top = 2.010417F;
			this.txtState.Width = 1.583333F;
			// 
			// txtPostalCode
			// 
			this.txtPostalCode.Height = 0.1770833F;
			this.txtPostalCode.Left = 3.333333F;
			this.txtPostalCode.Name = "txtPostalCode";
			this.txtPostalCode.Text = null;
			this.txtPostalCode.Top = 2.006944F;
			this.txtPostalCode.Width = 1.666667F;
			// 
			// txtEmployerCity
			// 
			this.txtEmployerCity.Height = 0.1770833F;
			this.txtEmployerCity.Left = 5.083333F;
			this.txtEmployerCity.Name = "txtEmployerCity";
			this.txtEmployerCity.Text = null;
			this.txtEmployerCity.Top = 2.010417F;
			this.txtEmployerCity.Width = 1.354167F;
			// 
			// txtEmployerState
			// 
			this.txtEmployerState.Height = 0.1770833F;
			this.txtEmployerState.Left = 6.5625F;
			this.txtEmployerState.Name = "txtEmployerState";
			this.txtEmployerState.Text = null;
			this.txtEmployerState.Top = 2.010417F;
			this.txtEmployerState.Width = 1.583333F;
			// 
			// txtEmployerPostalCode
			// 
			this.txtEmployerPostalCode.Height = 0.1770833F;
			this.txtEmployerPostalCode.Left = 8.364583F;
			this.txtEmployerPostalCode.Name = "txtEmployerPostalCode";
			this.txtEmployerPostalCode.Text = null;
			this.txtEmployerPostalCode.Top = 2.010417F;
			this.txtEmployerPostalCode.Width = 1.604167F;
			// 
			// Image2
			// 
			this.Image2.Height = 0.2395833F;
			this.Image2.HyperLink = null;
			this.Image2.ImageData = ((System.IO.Stream)(resources.GetObject("Image2.ImageData")));
			this.Image2.Left = 8.875F;
			this.Image2.LineWeight = 1F;
			this.Image2.Name = "Image2";
			this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.Image2.Top = 0.75F;
			this.Image2.Width = 0.7291667F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1770833F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 0F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" + "-size: 10pt; font-weight: bold; vertical-align: middle";
			this.Label77.Text = "Part I";
			this.Label77.Top = 1.072917F;
			this.Label77.Width = 0.53125F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1770833F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 0.6354167F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "font-family: \'Arial\'; font-weight: bold";
			this.Label78.Text = "Employee";
			this.Label78.Top = 1.072917F;
			this.Label78.Width = 0.9479167F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.3125F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 0.3125F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-family: \'Impact\'; font-size: 20.5pt";
			this.Label83.Text = "1095-C";
			this.Label83.Top = 0.375F;
			this.Label83.Width = 0.9583333F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1666667F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 0F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "font-size: 8pt";
			this.Label84.Text = "Form";
			this.Label84.Top = 0.5416667F;
			this.Label84.Width = 0.5F;
			// 
			// Line3
			// 
			this.Line3.Height = 0.6840277F;
			this.Line3.Left = 1.635417F;
			this.Line3.LineWeight = 2F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.3888889F;
			this.Line3.Width = 0F;
			this.Line3.X1 = 1.635417F;
			this.Line3.X2 = 1.635417F;
			this.Line3.Y1 = 0.3888889F;
			this.Line3.Y2 = 1.072917F;
			// 
			// Line4
			// 
			this.Line4.Height = 0.6840277F;
			this.Line4.Left = 8.5F;
			this.Line4.LineWeight = 2F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.3888889F;
			this.Line4.Width = 0F;
			this.Line4.X1 = 8.5F;
			this.Line4.X2 = 8.5F;
			this.Line4.Y1 = 0.3888889F;
			this.Line4.Y2 = 1.072917F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1666667F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-size: 8pt";
			this.Label85.Text = "Department of the Treasury";
			this.Label85.Top = 0.7083333F;
			this.Label85.Width = 1.625F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1666667F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 0F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "font-size: 8pt";
			this.Label86.Text = "Internal Revenue Service";
			this.Label86.Top = 0.875F;
			this.Label86.Width = 1.625F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1770833F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 5.375F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "font-family: \'Arial\'; font-weight: bold";
			this.Label87.Text = "Applicable Large Employer Member (Employer)";
			this.Label87.Top = 1.072917F;
			this.Label87.Width = 3.635417F;
			// 
			// Line6
			// 
			this.Line6.Height = 0.8020834F;
			this.Line6.Left = 5F;
			this.Line6.LineWeight = 2F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.072917F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 5F;
			this.Line6.X2 = 5F;
			this.Line6.Y1 = 1.072917F;
			this.Line6.Y2 = 1.875F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 0F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 2.375F;
			this.Line9.Width = 9.999306F;
			this.Line9.X1 = 0F;
			this.Line9.X2 = 9.999306F;
			this.Line9.Y1 = 2.375F;
			this.Line9.Y2 = 2.375F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 1.020833F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 2.583333F;
			this.Line10.Width = 8.978473F;
			this.Line10.X1 = 1.020833F;
			this.Line10.X2 = 9.999306F;
			this.Line10.Y1 = 2.583333F;
			this.Line10.Y2 = 2.583333F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1770833F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 0F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" + "-size: 10pt; font-weight: bold; vertical-align: middle";
			this.Label88.Text = "Part II";
			this.Label88.Top = 2.197222F;
			this.Label88.Width = 0.53125F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 0F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 2.9375F;
			this.Line11.Width = 9.999306F;
			this.Line11.X1 = 0F;
			this.Line11.X2 = 9.999306F;
			this.Line11.Y1 = 2.9375F;
			this.Line11.Y2 = 2.9375F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 3.4375F;
			this.Line12.Width = 9.999306F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 9.999306F;
			this.Line12.Y1 = 3.4375F;
			this.Line12.Y2 = 3.4375F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0F;
			this.Line13.LineWeight = 2F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 3.875F;
			this.Line13.Width = 9.999306F;
			this.Line13.X1 = 0F;
			this.Line13.X2 = 9.999306F;
			this.Line13.Y1 = 3.875F;
			this.Line13.Y2 = 3.875F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1770833F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 0F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" + "-size: 10pt; font-weight: bold; vertical-align: middle";
			this.Label89.Text = "Part III";
			this.Label89.Top = 3.875F;
			this.Label89.Width = 0.53125F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1770833F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 0.6354167F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-family: \'Arial\'; font-weight: bold";
			this.Label90.Text = "Covered Individuals";
			this.Label90.Top = 3.885417F;
			this.Label90.Width = 1.572917F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1770833F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 0.65625F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "font-family: \'Arial\'; font-weight: bold";
			this.Label91.Text = "Employee Offer and Coverage";
			this.Label91.Top = 2.197917F;
			this.Label91.Width = 2.260417F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.2395833F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 1.9375F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "font-family: \'Franklin Gothic Medium\'; font-size: 14.5pt; font-weight: bold; text" + "-align: left";
			this.Label92.Text = "Employer-Provided Health Insurance Offer and Coverage";
			this.Label92.Top = 0.375F;
			this.Label92.Width = 5.395833F;
			// 
			// Line14
			// 
			this.Line14.Height = 0.5F;
			this.Line14.Left = 5F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 1.875F;
			this.Line14.Width = 0F;
			this.Line14.X1 = 5F;
			this.Line14.X2 = 5F;
			this.Line14.Y1 = 1.875F;
			this.Line14.Y2 = 2.375F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1770833F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 5.083333F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'Arial\'; font-weight: bold";
			this.Label93.Text = "Plan Start Month";
			this.Label93.Top = 2.197917F;
			this.Label93.Width = 1.260417F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1770833F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 6.3125F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-family: \'Arial\'";
			this.Label94.Text = "(Enter 2-digit number):";
			this.Label94.Top = 2.197917F;
			this.Label94.Width = 1.510417F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1770833F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 0.6354167F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-family: \'Arial\'; font-size: 9pt";
			this.Label95.Text = "If Employer provided self-insured coverage, check the box and enter the informati" + "on for each individual enrolled in coverage, including the employee.";
			this.Label95.Top = 4.03125F;
			this.Label95.Width = 8.385417F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 0F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 4.5F;
			this.Line15.Width = 9.999306F;
			this.Line15.X1 = 0F;
			this.Line15.X2 = 9.999306F;
			this.Line15.Y1 = 4.5F;
			this.Line15.Y2 = 4.5F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 4.997917F;
			this.Line16.Width = 9.999306F;
			this.Line16.X1 = 0F;
			this.Line16.X2 = 9.999306F;
			this.Line16.Y1 = 4.997917F;
			this.Line16.Y2 = 4.997917F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 5.495833F;
			this.Line17.Width = 9.999306F;
			this.Line17.X1 = 0F;
			this.Line17.X2 = 9.999306F;
			this.Line17.Y1 = 5.495833F;
			this.Line17.Y2 = 5.495833F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 0F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 5.99375F;
			this.Line18.Width = 9.999306F;
			this.Line18.X1 = 0F;
			this.Line18.X2 = 9.999306F;
			this.Line18.Y1 = 5.99375F;
			this.Line18.Y2 = 5.99375F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 0F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 6.491667F;
			this.Line19.Width = 9.999306F;
			this.Line19.X1 = 0F;
			this.Line19.X2 = 9.999306F;
			this.Line19.Y1 = 6.491667F;
			this.Line19.Y2 = 6.491667F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 0F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 6.989583F;
			this.Line20.Width = 9.999306F;
			this.Line20.X1 = 0F;
			this.Line20.X2 = 9.999306F;
			this.Line20.Y1 = 6.989583F;
			this.Line20.Y2 = 6.989583F;
			// 
			// Line21
			// 
			this.Line21.Height = 3.302083F;
			this.Line21.Left = 2.40625F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 4.1875F;
			this.Line21.Width = 0F;
			this.Line21.X1 = 2.40625F;
			this.Line21.X2 = 2.40625F;
			this.Line21.Y1 = 4.1875F;
			this.Line21.Y2 = 7.489583F;
			// 
			// Line22
			// 
			this.Line22.Height = 3.302083F;
			this.Line22.Left = 3.572917F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 4.1875F;
			this.Line22.Width = 0.01041651F;
			this.Line22.X1 = 3.572917F;
			this.Line22.X2 = 3.583333F;
			this.Line22.Y1 = 4.1875F;
			this.Line22.Y2 = 7.489583F;
			// 
			// Line36
			// 
			this.Line36.Height = 0F;
			this.Line36.Left = 0F;
			this.Line36.LineWeight = 1F;
			this.Line36.Name = "Line36";
			this.Line36.Top = 4.1875F;
			this.Line36.Width = 9.999306F;
			this.Line36.X1 = 0F;
			this.Line36.X2 = 9.999306F;
			this.Line36.Y1 = 4.1875F;
			this.Line36.Y2 = 4.1875F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.1770833F;
			this.Shape1.Left = 9.072917F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 4F;
			this.Shape1.Width = 0.1770833F;
			// 
			// Line38
			// 
			this.Line38.Height = 1.5F;
			this.Line38.Left = 1.020833F;
			this.Line38.LineWeight = 1F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 2.375F;
			this.Line38.Width = 0F;
			this.Line38.X1 = 1.020833F;
			this.Line38.X2 = 1.020833F;
			this.Line38.Y1 = 2.375F;
			this.Line38.Y2 = 3.875F;
			// 
			// Line51
			// 
			this.Line51.Height = 0.3125F;
			this.Line51.Left = 3.1875F;
			this.Line51.LineWeight = 1F;
			this.Line51.Name = "Line51";
			this.Line51.Top = 1.25F;
			this.Line51.Width = 0F;
			this.Line51.X1 = 3.1875F;
			this.Line51.X2 = 3.1875F;
			this.Line51.Y1 = 1.25F;
			this.Line51.Y2 = 1.5625F;
			// 
			// Line54
			// 
			this.Line54.Height = 0.3222222F;
			this.Line54.Left = 3.1875F;
			this.Line54.LineWeight = 1F;
			this.Line54.Name = "Line54";
			this.Line54.Top = 1.875F;
			this.Line54.Width = 0F;
			this.Line54.X1 = 3.1875F;
			this.Line54.X2 = 3.1875F;
			this.Line54.Y1 = 1.875F;
			this.Line54.Y2 = 2.197222F;
			// 
			// Line55
			// 
			this.Line55.Height = 0.3125F;
			this.Line55.Left = 6.5F;
			this.Line55.LineWeight = 1F;
			this.Line55.Name = "Line55";
			this.Line55.Top = 1.875F;
			this.Line55.Width = 0F;
			this.Line55.X1 = 6.5F;
			this.Line55.X2 = 6.5F;
			this.Line55.Y1 = 1.875F;
			this.Line55.Y2 = 2.1875F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1979167F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 9.229167F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-family: \'OCR A Extended\'; font-size: 12pt; text-align: right";
			this.Label96.Text = "600117";
			this.Label96.Top = 0F;
			this.Label96.Width = 0.6875F;
			// 
			// Shape2
			// 
			this.Shape2.Height = 0.1770833F;
			this.Shape2.Left = 7.3125F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 0.375F;
			this.Shape2.Width = 0.1770833F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.1770833F;
			this.Shape3.Left = 7.3125F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.6875F;
			this.Shape3.Width = 0.1770833F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1666667F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 7.520833F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "";
			this.Label97.Text = "VOID";
			this.Label97.Top = 0.3854167F;
			this.Label97.Width = 0.6875F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1666667F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 7.520833F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "";
			this.Label98.Text = "CORRECTED";
			this.Label98.Top = 0.6979167F;
			this.Label98.Width = 0.9375F;
			// 
			// Line56
			// 
			this.Line56.Height = 0F;
			this.Line56.Left = 8.5F;
			this.Line56.LineWeight = 1F;
			this.Line56.Name = "Line56";
			this.Line56.Top = 0.65625F;
			this.Line56.Width = 1.375F;
			this.Line56.X1 = 8.5F;
			this.Line56.X2 = 9.875F;
			this.Line56.Y1 = 0.65625F;
			this.Line56.Y2 = 0.65625F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1770833F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 8.708333F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-size: 8pt";
			this.Label99.Text = "OMB No. 1545-2251";
			this.Label99.Top = 0.4583333F;
			this.Label99.Width = 1.15625F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1770833F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 2.0625F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label100.Text = "Information about Form 1095-C and its separate instructions is at";
			this.Label100.Top = 0.8020833F;
			this.Label100.Width = 3.71875F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1770833F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 5.8125F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label101.Text = "www.irs.gov/form1095c";
			this.Label101.Top = 0.8020833F;
			this.Label101.Width = 1.59375F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1770833F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 1.875F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" + "-char-set: 2";
			this.Label102.Text = "4";
			this.Label102.Top = 0.8020833F;
			this.Label102.Width = 0.3333333F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1770833F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 0.1458333F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "font-size: 6.5pt";
			this.Label104.Text = "City or town";
			this.Label104.Top = 1.885417F;
			this.Label104.Width = 1.15625F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1770833F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 1.708333F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "font-size: 6.5pt";
			this.Label105.Text = "State or province";
			this.Label105.Top = 1.885417F;
			this.Label105.Width = 1.40625F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.1770833F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 3.333333F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "font-size: 6.5pt";
			this.Label106.Text = "Country and ZIP or foreign postal code";
			this.Label106.Top = 1.881944F;
			this.Label106.Width = 1.65625F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1770833F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 0.1458333F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-size: 6.5pt";
			this.Label107.Text = "Name of employee";
			this.Label107.Top = 1.25F;
			this.Label107.Width = 1.15625F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1770833F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 3.333333F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "font-size: 6.5pt";
			this.Label108.Text = "Social security number (SSN)";
			this.Label108.Top = 1.25F;
			this.Label108.Width = 1.53125F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.1770833F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 5.25F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-size: 6.5pt";
			this.Label109.Text = "Name of employer";
			this.Label109.Top = 1.25F;
			this.Label109.Width = 2.15625F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1770833F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 5.25F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "font-size: 6.5pt";
			this.Label110.Text = "City or town";
			this.Label110.Top = 1.885417F;
			this.Label110.Width = 1.03125F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1770833F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 6.6875F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-size: 6.5pt";
			this.Label111.Text = "State or province";
			this.Label111.Top = 1.885417F;
			this.Label111.Width = 1.03125F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1770833F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 8.364583F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-size: 6.5pt";
			this.Label112.Text = "Country and ZIP or foreign postal code";
			this.Label112.Top = 1.885417F;
			this.Label112.Width = 1.635417F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1770833F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 5.25F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-size: 6.5pt";
			this.Label113.Text = "Street address (including room or suite no.)";
			this.Label113.Top = 1.5625F;
			this.Label113.Width = 2.46875F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1770833F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 0.1458333F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-size: 6.5pt";
			this.Label114.Text = "Street address (including apartment no.)";
			this.Label114.Top = 1.5625F;
			this.Label114.Width = 1.84375F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1770833F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 8.364583F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-size: 6.5pt";
			this.Label115.Text = "Contact telephone number";
			this.Label115.Top = 1.5625F;
			this.Label115.Width = 1.34375F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1770833F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 8.364583F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-size: 6.5pt";
			this.Label116.Text = "Employer identification number (EIN)";
			this.Label116.Top = 1.25F;
			this.Label116.Width = 1.59375F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1770833F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 1.0625F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-size: 7pt";
			this.Label117.Text = "All 12 Months";
			this.Label117.Top = 2.40625F;
			this.Label117.Width = 0.65625F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1770833F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 5.260417F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-size: 8.5pt; text-align: center";
			this.Label118.Text = "Jan";
			this.Label118.Top = 4.34375F;
			this.Label118.Width = 0.40625F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1770833F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 2.416667F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-size: 8.5pt; text-align: center";
			this.Label119.Text = "Feb";
			this.Label119.Top = 2.40625F;
			this.Label119.Width = 0.65625F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1770833F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 3.114583F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-size: 8.5pt; text-align: center";
			this.Label120.Text = "Mar";
			this.Label120.Top = 2.40625F;
			this.Label120.Width = 0.65625F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1770833F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 3.802083F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-size: 8.5pt; text-align: center";
			this.Label121.Text = "Apr";
			this.Label121.Top = 2.40625F;
			this.Label121.Width = 0.65625F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1770833F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 4.489583F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-size: 8.5pt; text-align: center";
			this.Label122.Text = "May";
			this.Label122.Top = 2.40625F;
			this.Label122.Width = 0.65625F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1770833F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 5.177083F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-size: 8.5pt; text-align: center";
			this.Label123.Text = "June";
			this.Label123.Top = 2.40625F;
			this.Label123.Width = 0.65625F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1770833F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 5.875F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-size: 8.5pt; text-align: center";
			this.Label124.Text = "July";
			this.Label124.Top = 2.40625F;
			this.Label124.Width = 0.65625F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1770833F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 6.5625F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-size: 8.5pt; text-align: center";
			this.Label125.Text = "Aug";
			this.Label125.Top = 2.40625F;
			this.Label125.Width = 0.65625F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1770833F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 7.25F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-size: 8.5pt; text-align: center";
			this.Label126.Text = "Sept";
			this.Label126.Top = 2.40625F;
			this.Label126.Width = 0.65625F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1770833F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 7.9375F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-size: 8.5pt; text-align: center";
			this.Label127.Text = "Oct";
			this.Label127.Top = 2.40625F;
			this.Label127.Width = 0.65625F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1770833F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 8.635417F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-size: 8.5pt; text-align: center";
			this.Label128.Text = "Nov";
			this.Label128.Top = 2.40625F;
			this.Label128.Width = 0.65625F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1770833F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 9.40625F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-size: 8.5pt; text-align: center";
			this.Label129.Text = "Dec";
			this.Label129.Top = 2.40625F;
			this.Label129.Width = 0.53125F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1770833F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 0F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label130.Text = "17";
			this.Label130.Top = 4.65625F;
			this.Label130.Width = 0.28125F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1770833F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 0F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label131.Text = "18";
			this.Label131.Top = 5.154167F;
			this.Label131.Width = 0.28125F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1770833F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label132.Text = "19";
			this.Label132.Top = 5.652083F;
			this.Label132.Width = 0.28125F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1770833F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label133.Text = "20";
			this.Label133.Top = 6.15F;
			this.Label133.Width = 0.28125F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1770833F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 0F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label134.Text = "21";
			this.Label134.Top = 6.647917F;
			this.Label134.Width = 0.28125F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1770833F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 0F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label135.Text = "22";
			this.Label135.Top = 7.145833F;
			this.Label135.Width = 0.28125F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1770833F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 5.65625F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-size: 8.5pt; text-align: center";
			this.Label136.Text = "Feb";
			this.Label136.Top = 4.34375F;
			this.Label136.Width = 0.40625F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1770833F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 6.052083F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-size: 8.5pt; text-align: center";
			this.Label137.Text = "Mar";
			this.Label137.Top = 4.34375F;
			this.Label137.Width = 0.40625F;
			// 
			// Label138
			// 
			this.Label138.Height = 0.1770833F;
			this.Label138.HyperLink = null;
			this.Label138.Left = 1.038889F;
			this.Label138.Name = "Label138";
			this.Label138.Style = "font-size: 9pt; text-align: left";
			this.Label138.Text = "$";
			this.Label138.Top = 3.21875F;
			this.Label138.Width = 0.21875F;
			// 
			// Label139
			// 
			this.Label139.Height = 0.1770833F;
			this.Label139.HyperLink = null;
			this.Label139.Left = 1.729167F;
			this.Label139.Name = "Label139";
			this.Label139.Style = "font-size: 9pt; text-align: left";
			this.Label139.Text = "$";
			this.Label139.Top = 3.21875F;
			this.Label139.Width = 0.21875F;
			// 
			// Label140
			// 
			this.Label140.Height = 0.1770833F;
			this.Label140.HyperLink = null;
			this.Label140.Left = 2.419445F;
			this.Label140.Name = "Label140";
			this.Label140.Style = "font-size: 9pt; text-align: left";
			this.Label140.Text = "$";
			this.Label140.Top = 3.21875F;
			this.Label140.Width = 0.21875F;
			// 
			// Label141
			// 
			this.Label141.Height = 0.1770833F;
			this.Label141.HyperLink = null;
			this.Label141.Left = 3.109722F;
			this.Label141.Name = "Label141";
			this.Label141.Style = "font-size: 9pt; text-align: left";
			this.Label141.Text = "$";
			this.Label141.Top = 3.21875F;
			this.Label141.Width = 0.21875F;
			// 
			// Label142
			// 
			this.Label142.Height = 0.1770833F;
			this.Label142.HyperLink = null;
			this.Label142.Left = 3.8F;
			this.Label142.Name = "Label142";
			this.Label142.Style = "font-size: 9pt; text-align: left";
			this.Label142.Text = "$";
			this.Label142.Top = 3.21875F;
			this.Label142.Width = 0.21875F;
			// 
			// Label143
			// 
			this.Label143.Height = 0.1770833F;
			this.Label143.HyperLink = null;
			this.Label143.Left = 4.490278F;
			this.Label143.Name = "Label143";
			this.Label143.Style = "font-size: 9pt; text-align: left";
			this.Label143.Text = "$";
			this.Label143.Top = 3.21875F;
			this.Label143.Width = 0.21875F;
			// 
			// Label144
			// 
			this.Label144.Height = 0.1770833F;
			this.Label144.HyperLink = null;
			this.Label144.Left = 5.180555F;
			this.Label144.Name = "Label144";
			this.Label144.Style = "font-size: 9pt; text-align: left";
			this.Label144.Text = "$";
			this.Label144.Top = 3.21875F;
			this.Label144.Width = 0.21875F;
			// 
			// Label145
			// 
			this.Label145.Height = 0.1770833F;
			this.Label145.HyperLink = null;
			this.Label145.Left = 5.870833F;
			this.Label145.Name = "Label145";
			this.Label145.Style = "font-size: 9pt; text-align: left";
			this.Label145.Text = "$";
			this.Label145.Top = 3.21875F;
			this.Label145.Width = 0.21875F;
			// 
			// Label146
			// 
			this.Label146.Height = 0.1770833F;
			this.Label146.HyperLink = null;
			this.Label146.Left = 6.561111F;
			this.Label146.Name = "Label146";
			this.Label146.Style = "font-size: 9pt; text-align: left";
			this.Label146.Text = "$";
			this.Label146.Top = 3.21875F;
			this.Label146.Width = 0.21875F;
			// 
			// Label147
			// 
			this.Label147.Height = 0.1770833F;
			this.Label147.HyperLink = null;
			this.Label147.Left = 7.251389F;
			this.Label147.Name = "Label147";
			this.Label147.Style = "font-size: 9pt; text-align: left";
			this.Label147.Text = "$";
			this.Label147.Top = 3.21875F;
			this.Label147.Width = 0.21875F;
			// 
			// Label148
			// 
			this.Label148.Height = 0.1770833F;
			this.Label148.HyperLink = null;
			this.Label148.Left = 7.941667F;
			this.Label148.Name = "Label148";
			this.Label148.Style = "font-size: 9pt; text-align: left";
			this.Label148.Text = "$";
			this.Label148.Top = 3.21875F;
			this.Label148.Width = 0.21875F;
			// 
			// Label149
			// 
			this.Label149.Height = 0.1770833F;
			this.Label149.HyperLink = null;
			this.Label149.Left = 8.631945F;
			this.Label149.Name = "Label149";
			this.Label149.Style = "font-size: 9pt; text-align: left";
			this.Label149.Text = "$";
			this.Label149.Top = 3.21875F;
			this.Label149.Width = 0.21875F;
			// 
			// Label150
			// 
			this.Label150.Height = 0.1770833F;
			this.Label150.HyperLink = null;
			this.Label150.Left = 9.322222F;
			this.Label150.Name = "Label150";
			this.Label150.Style = "font-size: 9pt; text-align: left";
			this.Label150.Text = "$";
			this.Label150.Top = 3.21875F;
			this.Label150.Width = 0.21875F;
			// 
			// Line40
			// 
			this.Line40.Height = 1.5F;
			this.Line40.Left = 2.401389F;
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 2.375F;
			this.Line40.Width = 0F;
			this.Line40.X1 = 2.401389F;
			this.Line40.X2 = 2.401389F;
			this.Line40.Y1 = 2.375F;
			this.Line40.Y2 = 3.875F;
			// 
			// Line41
			// 
			this.Line41.Height = 1.5F;
			this.Line41.Left = 3.091667F;
			this.Line41.LineWeight = 1F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 2.375F;
			this.Line41.Width = 0F;
			this.Line41.X1 = 3.091667F;
			this.Line41.X2 = 3.091667F;
			this.Line41.Y1 = 2.375F;
			this.Line41.Y2 = 3.875F;
			// 
			// Line42
			// 
			this.Line42.Height = 1.5F;
			this.Line42.Left = 3.781945F;
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 2.375F;
			this.Line42.Width = 0F;
			this.Line42.X1 = 3.781945F;
			this.Line42.X2 = 3.781945F;
			this.Line42.Y1 = 2.375F;
			this.Line42.Y2 = 3.875F;
			// 
			// Line43
			// 
			this.Line43.Height = 1.5F;
			this.Line43.Left = 4.472222F;
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 2.375F;
			this.Line43.Width = 0F;
			this.Line43.X1 = 4.472222F;
			this.Line43.X2 = 4.472222F;
			this.Line43.Y1 = 2.375F;
			this.Line43.Y2 = 3.875F;
			// 
			// Line46
			// 
			this.Line46.Height = 1.5F;
			this.Line46.Left = 6.543056F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 2.375F;
			this.Line46.Width = 0F;
			this.Line46.X1 = 6.543056F;
			this.Line46.X2 = 6.543056F;
			this.Line46.Y1 = 2.375F;
			this.Line46.Y2 = 3.875F;
			// 
			// Line47
			// 
			this.Line47.Height = 1.5F;
			this.Line47.Left = 7.233333F;
			this.Line47.LineWeight = 1F;
			this.Line47.Name = "Line47";
			this.Line47.Top = 2.375F;
			this.Line47.Width = 0F;
			this.Line47.X1 = 7.233333F;
			this.Line47.X2 = 7.233333F;
			this.Line47.Y1 = 2.375F;
			this.Line47.Y2 = 3.875F;
			// 
			// Line48
			// 
			this.Line48.Height = 1.5F;
			this.Line48.Left = 7.923611F;
			this.Line48.LineWeight = 1F;
			this.Line48.Name = "Line48";
			this.Line48.Top = 2.375F;
			this.Line48.Width = 0F;
			this.Line48.X1 = 7.923611F;
			this.Line48.X2 = 7.923611F;
			this.Line48.Y1 = 2.375F;
			this.Line48.Y2 = 3.875F;
			// 
			// Line49
			// 
			this.Line49.Height = 1.5F;
			this.Line49.Left = 8.613889F;
			this.Line49.LineWeight = 1F;
			this.Line49.Name = "Line49";
			this.Line49.Top = 2.375F;
			this.Line49.Width = 0F;
			this.Line49.X1 = 8.613889F;
			this.Line49.X2 = 8.613889F;
			this.Line49.Y1 = 2.375F;
			this.Line49.Y2 = 3.875F;
			// 
			// Line50
			// 
			this.Line50.Height = 1.5F;
			this.Line50.Left = 9.304167F;
			this.Line50.LineWeight = 1F;
			this.Line50.Name = "Line50";
			this.Line50.Top = 2.375F;
			this.Line50.Width = 0F;
			this.Line50.X1 = 9.304167F;
			this.Line50.X2 = 9.304167F;
			this.Line50.Y1 = 2.375F;
			this.Line50.Y2 = 3.875F;
			// 
			// Line45
			// 
			this.Line45.Height = 1.5F;
			this.Line45.Left = 5.852778F;
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 2.375F;
			this.Line45.Width = 0F;
			this.Line45.X1 = 5.852778F;
			this.Line45.X2 = 5.852778F;
			this.Line45.Y1 = 2.375F;
			this.Line45.Y2 = 3.875F;
			// 
			// Line44
			// 
			this.Line44.Height = 1.5F;
			this.Line44.Left = 5.1625F;
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 2.375F;
			this.Line44.Width = 0F;
			this.Line44.X1 = 5.1625F;
			this.Line44.X2 = 5.1625F;
			this.Line44.Y1 = 2.375F;
			this.Line44.Y2 = 3.875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.34375F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 6.5pt; text-align: left";
			this.Label34.Text = "14 Offer of Coverage (enter required code)";
			this.Label34.Top = 2.447917F;
			this.Label34.Width = 0.8333333F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.46875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 6.5pt; text-align: left";
			this.Label33.Text = "15 Employee Required Contribution (see instructions)";
			this.Label33.Top = 2.989583F;
			this.Label33.Width = 0.875F;
			// 
			// Label151
			// 
			this.Label151.Height = 0.1770833F;
			this.Label151.HyperLink = null;
			this.Label151.Left = 1.729167F;
			this.Label151.Name = "Label151";
			this.Label151.Style = "font-size: 8.5pt; text-align: center";
			this.Label151.Text = "Jan";
			this.Label151.Top = 2.40625F;
			this.Label151.Width = 0.65625F;
			// 
			// Label152
			// 
			this.Label152.Height = 0.1770833F;
			this.Label152.HyperLink = null;
			this.Label152.Left = 0.02083333F;
			this.Label152.Name = "Label152";
			this.Label152.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label152.Text = "1";
			this.Label152.Top = 1.25F;
			this.Label152.Width = 0.15625F;
			// 
			// Label153
			// 
			this.Label153.Height = 0.1770833F;
			this.Label153.HyperLink = null;
			this.Label153.Left = 0.02083333F;
			this.Label153.Name = "Label153";
			this.Label153.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label153.Text = "3";
			this.Label153.Top = 1.5625F;
			this.Label153.Width = 0.15625F;
			// 
			// Label154
			// 
			this.Label154.Height = 0.1770833F;
			this.Label154.HyperLink = null;
			this.Label154.Left = 0.02083333F;
			this.Label154.Name = "Label154";
			this.Label154.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label154.Text = "4";
			this.Label154.Top = 1.885417F;
			this.Label154.Width = 0.15625F;
			// 
			// Label155
			// 
			this.Label155.Height = 0.1770833F;
			this.Label155.HyperLink = null;
			this.Label155.Left = 3.21875F;
			this.Label155.Name = "Label155";
			this.Label155.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label155.Text = "2";
			this.Label155.Top = 1.25F;
			this.Label155.Width = 0.15625F;
			// 
			// Label156
			// 
			this.Label156.Height = 0.1770833F;
			this.Label156.HyperLink = null;
			this.Label156.Left = 3.21875F;
			this.Label156.Name = "Label156";
			this.Label156.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label156.Text = "6";
			this.Label156.Top = 1.881944F;
			this.Label156.Width = 0.15625F;
			// 
			// Label157
			// 
			this.Label157.Height = 0.1770833F;
			this.Label157.HyperLink = null;
			this.Label157.Left = 1.583333F;
			this.Label157.Name = "Label157";
			this.Label157.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label157.Text = "5";
			this.Label157.Top = 1.885417F;
			this.Label157.Width = 0.15625F;
			// 
			// Line53
			// 
			this.Line53.Height = 0.3222222F;
			this.Line53.Left = 1.520833F;
			this.Line53.LineWeight = 1F;
			this.Line53.Name = "Line53";
			this.Line53.Top = 1.875F;
			this.Line53.Width = 0F;
			this.Line53.X1 = 1.520833F;
			this.Line53.X2 = 1.520833F;
			this.Line53.Y1 = 1.875F;
			this.Line53.Y2 = 2.197222F;
			// 
			// Label158
			// 
			this.Label158.Height = 0.1770833F;
			this.Label158.HyperLink = null;
			this.Label158.Left = 5.083333F;
			this.Label158.Name = "Label158";
			this.Label158.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label158.Text = "11";
			this.Label158.Top = 1.885417F;
			this.Label158.Width = 0.15625F;
			// 
			// Label159
			// 
			this.Label159.Height = 0.1770833F;
			this.Label159.HyperLink = null;
			this.Label159.Left = 6.5625F;
			this.Label159.Name = "Label159";
			this.Label159.Style = "font-size: 7pt; font-weight: bold";
			this.Label159.Text = "12";
			this.Label159.Top = 1.885417F;
			this.Label159.Width = 0.15625F;
			// 
			// Label160
			// 
			this.Label160.Height = 0.1770833F;
			this.Label160.HyperLink = null;
			this.Label160.Left = 8.21875F;
			this.Label160.Name = "Label160";
			this.Label160.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label160.Text = "13";
			this.Label160.Top = 1.885417F;
			this.Label160.Width = 0.15625F;
			// 
			// Label161
			// 
			this.Label161.Height = 0.1770833F;
			this.Label161.HyperLink = null;
			this.Label161.Left = 5.083333F;
			this.Label161.Name = "Label161";
			this.Label161.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label161.Text = "7";
			this.Label161.Top = 1.25F;
			this.Label161.Width = 0.15625F;
			// 
			// Label162
			// 
			this.Label162.Height = 0.1770833F;
			this.Label162.HyperLink = null;
			this.Label162.Left = 5.083333F;
			this.Label162.Name = "Label162";
			this.Label162.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label162.Text = "9";
			this.Label162.Top = 1.5625F;
			this.Label162.Width = 0.15625F;
			// 
			// Label163
			// 
			this.Label163.Height = 0.1770833F;
			this.Label163.HyperLink = null;
			this.Label163.Left = 8.21875F;
			this.Label163.Name = "Label163";
			this.Label163.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label163.Text = "8";
			this.Label163.Top = 1.25F;
			this.Label163.Width = 0.15625F;
			// 
			// Label164
			// 
			this.Label164.Height = 0.1770833F;
			this.Label164.HyperLink = null;
			this.Label164.Left = 8.21875F;
			this.Label164.Name = "Label164";
			this.Label164.Style = "font-size: 6.5pt; font-weight: bold";
			this.Label164.Text = "10";
			this.Label164.Top = 1.5625F;
			this.Label164.Width = 0.15625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.40625F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 6.5pt; text-align: left";
			this.Label32.Text = "16 Section 4980H Safe Harbor and Other Relief (enter code, if applicable)";
			this.Label32.Top = 3.458333F;
			this.Label32.Width = 0.8958333F;
			// 
			// Label165
			// 
			this.Label165.Height = 0.1770833F;
			this.Label165.HyperLink = null;
			this.Label165.Left = 6.4375F;
			this.Label165.Name = "Label165";
			this.Label165.Style = "font-size: 8.5pt; text-align: center";
			this.Label165.Text = "Apr";
			this.Label165.Top = 4.34375F;
			this.Label165.Width = 0.40625F;
			// 
			// Label166
			// 
			this.Label166.Height = 0.1770833F;
			this.Label166.HyperLink = null;
			this.Label166.Left = 6.833333F;
			this.Label166.Name = "Label166";
			this.Label166.Style = "font-size: 8.5pt; text-align: center";
			this.Label166.Text = "May";
			this.Label166.Top = 4.34375F;
			this.Label166.Width = 0.40625F;
			// 
			// Label167
			// 
			this.Label167.Height = 0.1770833F;
			this.Label167.HyperLink = null;
			this.Label167.Left = 7.229167F;
			this.Label167.Name = "Label167";
			this.Label167.Style = "font-size: 8.5pt; text-align: center";
			this.Label167.Text = "June";
			this.Label167.Top = 4.34375F;
			this.Label167.Width = 0.40625F;
			// 
			// Label168
			// 
			this.Label168.Height = 0.1770833F;
			this.Label168.HyperLink = null;
			this.Label168.Left = 7.625F;
			this.Label168.Name = "Label168";
			this.Label168.Style = "font-size: 8.5pt; text-align: center";
			this.Label168.Text = "July";
			this.Label168.Top = 4.34375F;
			this.Label168.Width = 0.40625F;
			// 
			// Label169
			// 
			this.Label169.Height = 0.1770833F;
			this.Label169.HyperLink = null;
			this.Label169.Left = 8.020833F;
			this.Label169.Name = "Label169";
			this.Label169.Style = "font-size: 8.5pt; text-align: center";
			this.Label169.Text = "Aug";
			this.Label169.Top = 4.34375F;
			this.Label169.Width = 0.40625F;
			// 
			// Label170
			// 
			this.Label170.Height = 0.1770833F;
			this.Label170.HyperLink = null;
			this.Label170.Left = 8.40625F;
			this.Label170.Name = "Label170";
			this.Label170.Style = "font-size: 8.5pt; text-align: center";
			this.Label170.Text = "Sept";
			this.Label170.Top = 4.34375F;
			this.Label170.Width = 0.40625F;
			// 
			// Label171
			// 
			this.Label171.Height = 0.1770833F;
			this.Label171.HyperLink = null;
			this.Label171.Left = 8.802083F;
			this.Label171.Name = "Label171";
			this.Label171.Style = "font-size: 8.5pt; text-align: center";
			this.Label171.Text = "Oct";
			this.Label171.Top = 4.34375F;
			this.Label171.Width = 0.40625F;
			// 
			// Label172
			// 
			this.Label172.Height = 0.1770833F;
			this.Label172.HyperLink = null;
			this.Label172.Left = 9.197917F;
			this.Label172.Name = "Label172";
			this.Label172.Style = "font-size: 8.5pt; text-align: center";
			this.Label172.Text = "Nov";
			this.Label172.Top = 4.34375F;
			this.Label172.Width = 0.40625F;
			// 
			// Label173
			// 
			this.Label173.Height = 0.1770833F;
			this.Label173.HyperLink = null;
			this.Label173.Left = 9.625F;
			this.Label173.Name = "Label173";
			this.Label173.Style = "font-size: 8.5pt; text-align: center";
			this.Label173.Text = "Dec";
			this.Label173.Top = 4.34375F;
			this.Label173.Width = 0.34375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.21875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.3541667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 6.5pt; text-align: center";
			this.Label3.Text = "(a) Name of covered individual(s)";
			this.Label3.Top = 4.302083F;
			this.Label3.Width = 1.708333F;
			// 
			// Label174
			// 
			this.Label174.Height = 0.21875F;
			this.Label174.HyperLink = null;
			this.Label174.Left = 2.5F;
			this.Label174.Name = "Label174";
			this.Label174.Style = "font-size: 6.5pt; text-align: center";
			this.Label174.Text = "(b) SSN or other TIN";
			this.Label174.Top = 4.302083F;
			this.Label174.Width = 1.020833F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.28125F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.729167F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-size: 6.5pt; text-align: center";
			this.Label175.Text = "(c) DOB (If SSN or other TIN is not available)";
			this.Label175.Top = 4.208333F;
			this.Label175.Width = 0.8958333F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.28125F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 4.697917F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-size: 6.5pt; text-align: center";
			this.Label176.Text = "(d) Covered all 12 months";
			this.Label176.Top = 4.239583F;
			this.Label176.Width = 0.5833333F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1770833F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 0F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-size: 8pt; font-weight: bold";
			this.Label177.Text = "XID #1607  For Privacy Act and Paperwork Reduction Act Notice, see separate Instr" + "uctions";
			this.Label177.Top = 7.510417F;
			this.Label177.Width = 4.96875F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.21875F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 6.604167F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-size: 7pt; text-align: center";
			this.Label178.Text = "(e) Months of Coverage";
			this.Label178.Top = 4.208333F;
			this.Label178.Width = 1.708333F;
			// 
			// Label179
			// 
			this.Label179.Height = 0.1770833F;
			this.Label179.HyperLink = null;
			this.Label179.Left = 6.25F;
			this.Label179.Name = "Label179";
			this.Label179.Style = "font-size: 8pt";
			this.Label179.Text = "41-0852411";
			this.Label179.Top = 7.510417F;
			this.Label179.Width = 0.78125F;
			// 
			// Label180
			// 
			this.Label180.Height = 0.1770833F;
			this.Label180.HyperLink = null;
			this.Label180.Left = 8.666667F;
			this.Label180.Name = "Label180";
			this.Label180.Style = "font-size: 8pt";
			this.Label180.Text = "Form";
			this.Label180.Top = 7.510417F;
			this.Label180.Width = 0.40625F;
			// 
			// Label181
			// 
			this.Label181.Height = 0.1770833F;
			this.Label181.HyperLink = null;
			this.Label181.Left = 9.489583F;
			this.Label181.Name = "Label181";
			this.Label181.Style = "font-size: 8pt";
			this.Label181.Text = "(2016)";
			this.Label181.Top = 7.510417F;
			this.Label181.Width = 0.40625F;
			// 
			// Label182
			// 
			this.Label182.Height = 0.1770833F;
			this.Label182.HyperLink = null;
			this.Label182.Left = 8.989583F;
			this.Label182.Name = "Label182";
			this.Label182.Style = "font-size: 10pt; font-weight: bold";
			this.Label182.Text = "1095-C";
			this.Label182.Top = 7.489583F;
			this.Label182.Width = 0.53125F;
			// 
			// Line57
			// 
			this.Line57.Height = 0F;
			this.Line57.Left = 0F;
			this.Line57.LineWeight = 1F;
			this.Line57.Name = "Line57";
			this.Line57.Top = 7.489583F;
			this.Line57.Width = 9.999306F;
			this.Line57.X1 = 0F;
			this.Line57.X2 = 9.999306F;
			this.Line57.Y1 = 7.489583F;
			this.Line57.Y2 = 7.489583F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.072917F;
			this.Line1.Width = 10F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10F;
			this.Line1.Y1 = 1.072917F;
			this.Line1.Y2 = 1.072917F;
			// 
			// Line39
			// 
			this.Line39.Height = 1.5F;
			this.Line39.Left = 1.711111F;
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 2.375F;
			this.Line39.Width = 0F;
			this.Line39.X1 = 1.711111F;
			this.Line39.X2 = 1.711111F;
			this.Line39.Y1 = 2.375F;
			this.Line39.Y2 = 3.875F;
			// 
			// Line25
			// 
			this.Line25.Height = 3.145833F;
			this.Line25.Left = 5.664583F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 4.34375F;
			this.Line25.Width = 0F;
			this.Line25.X1 = 5.664583F;
			this.Line25.X2 = 5.664583F;
			this.Line25.Y1 = 4.34375F;
			this.Line25.Y2 = 7.489583F;
			// 
			// Line26
			// 
			this.Line26.Height = 3.145833F;
			this.Line26.Left = 6.058333F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 4.34375F;
			this.Line26.Width = 0F;
			this.Line26.X1 = 6.058333F;
			this.Line26.X2 = 6.058333F;
			this.Line26.Y1 = 4.34375F;
			this.Line26.Y2 = 7.489583F;
			// 
			// Line27
			// 
			this.Line27.Height = 3.145833F;
			this.Line27.Left = 6.452083F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 4.34375F;
			this.Line27.Width = 0F;
			this.Line27.X1 = 6.452083F;
			this.Line27.X2 = 6.452083F;
			this.Line27.Y1 = 4.34375F;
			this.Line27.Y2 = 7.489583F;
			// 
			// Line28
			// 
			this.Line28.Height = 3.145833F;
			this.Line28.Left = 6.845833F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 4.34375F;
			this.Line28.Width = 0F;
			this.Line28.X1 = 6.845833F;
			this.Line28.X2 = 6.845833F;
			this.Line28.Y1 = 4.34375F;
			this.Line28.Y2 = 7.489583F;
			// 
			// Line29
			// 
			this.Line29.Height = 3.145833F;
			this.Line29.Left = 7.239583F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 4.34375F;
			this.Line29.Width = 0F;
			this.Line29.X1 = 7.239583F;
			this.Line29.X2 = 7.239583F;
			this.Line29.Y1 = 4.34375F;
			this.Line29.Y2 = 7.489583F;
			// 
			// Line30
			// 
			this.Line30.Height = 3.145833F;
			this.Line30.Left = 7.633333F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 4.34375F;
			this.Line30.Width = 0F;
			this.Line30.X1 = 7.633333F;
			this.Line30.X2 = 7.633333F;
			this.Line30.Y1 = 4.34375F;
			this.Line30.Y2 = 7.489583F;
			// 
			// Line31
			// 
			this.Line31.Height = 3.145833F;
			this.Line31.Left = 8.027083F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 4.34375F;
			this.Line31.Width = 0F;
			this.Line31.X1 = 8.027083F;
			this.Line31.X2 = 8.027083F;
			this.Line31.Y1 = 4.34375F;
			this.Line31.Y2 = 7.489583F;
			// 
			// Line32
			// 
			this.Line32.Height = 3.145833F;
			this.Line32.Left = 8.420834F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 4.34375F;
			this.Line32.Width = 0F;
			this.Line32.X1 = 8.420834F;
			this.Line32.X2 = 8.420834F;
			this.Line32.Y1 = 4.34375F;
			this.Line32.Y2 = 7.489583F;
			// 
			// Line33
			// 
			this.Line33.Height = 3.145833F;
			this.Line33.Left = 8.814584F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 4.34375F;
			this.Line33.Width = 0F;
			this.Line33.X1 = 8.814584F;
			this.Line33.X2 = 8.814584F;
			this.Line33.Y1 = 4.34375F;
			this.Line33.Y2 = 7.489583F;
			// 
			// Line34
			// 
			this.Line34.Height = 3.145833F;
			this.Line34.Left = 9.208333F;
			this.Line34.LineWeight = 1F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 4.34375F;
			this.Line34.Width = 0F;
			this.Line34.X1 = 9.208333F;
			this.Line34.X2 = 9.208333F;
			this.Line34.Y1 = 4.34375F;
			this.Line34.Y2 = 7.489583F;
			// 
			// Line35
			// 
			this.Line35.Height = 3.145833F;
			this.Line35.Left = 9.602083F;
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 4.34375F;
			this.Line35.Width = 0F;
			this.Line35.X1 = 9.602083F;
			this.Line35.X2 = 9.602083F;
			this.Line35.Y1 = 4.34375F;
			this.Line35.Y2 = 7.489583F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.1770833F;
			this.Shape4.Left = 4.864583F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 4.660417F;
			this.Shape4.Width = 0.1770833F;
			// 
			// Shape5
			// 
			this.Shape5.Height = 0.1770833F;
			this.Shape5.Left = 5.375F;
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 4.65625F;
			this.Shape5.Width = 0.1770833F;
			// 
			// Shape6
			// 
			this.Shape6.Height = 0.1770833F;
			this.Shape6.Left = 4.864583F;
			this.Shape6.Name = "Shape6";
			this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape6.Top = 5.158333F;
			this.Shape6.Width = 0.1770833F;
			// 
			// Shape7
			// 
			this.Shape7.Height = 0.1770833F;
			this.Shape7.Left = 4.864583F;
			this.Shape7.Name = "Shape7";
			this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape7.Top = 5.65625F;
			this.Shape7.Width = 0.1770833F;
			// 
			// Shape8
			// 
			this.Shape8.Height = 0.1770833F;
			this.Shape8.Left = 4.864583F;
			this.Shape8.Name = "Shape8";
			this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape8.Top = 6.154167F;
			this.Shape8.Width = 0.1770833F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1770833F;
			this.Shape9.Left = 4.864583F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 6.652083F;
			this.Shape9.Width = 0.1770833F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.1770833F;
			this.Shape10.Left = 4.864583F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 7.15F;
			this.Shape10.Width = 0.1770833F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.1770833F;
			this.Shape11.Left = 5.375F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 5.15625F;
			this.Shape11.Width = 0.1770833F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.1770833F;
			this.Shape12.Left = 5.375F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 5.65625F;
			this.Shape12.Width = 0.1770833F;
			// 
			// Shape13
			// 
			this.Shape13.Height = 0.1770833F;
			this.Shape13.Left = 5.375F;
			this.Shape13.Name = "Shape13";
			this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape13.Top = 6.15625F;
			this.Shape13.Width = 0.1770833F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 0.1770833F;
			this.Shape14.Left = 5.375F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 6.65625F;
			this.Shape14.Width = 0.1770833F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1770833F;
			this.Shape15.Left = 5.375F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 7.145833F;
			this.Shape15.Width = 0.1770833F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.1770833F;
			this.Shape16.Left = 5.770833F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 4.65625F;
			this.Shape16.Width = 0.1770833F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.1770833F;
			this.Shape17.Left = 5.770833F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 5.15625F;
			this.Shape17.Width = 0.1770833F;
			// 
			// Shape18
			// 
			this.Shape18.Height = 0.1770833F;
			this.Shape18.Left = 5.770833F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 5.65625F;
			this.Shape18.Width = 0.1770833F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.1770833F;
			this.Shape19.Left = 5.770833F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 6.15625F;
			this.Shape19.Width = 0.1770833F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.1770833F;
			this.Shape20.Left = 5.770833F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 6.65625F;
			this.Shape20.Width = 0.1770833F;
			// 
			// Shape21
			// 
			this.Shape21.Height = 0.1770833F;
			this.Shape21.Left = 5.770833F;
			this.Shape21.Name = "Shape21";
			this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape21.Top = 7.145833F;
			this.Shape21.Width = 0.1770833F;
			// 
			// Shape22
			// 
			this.Shape22.Height = 0.1770833F;
			this.Shape22.Left = 6.1625F;
			this.Shape22.Name = "Shape22";
			this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape22.Top = 4.65625F;
			this.Shape22.Width = 0.1770833F;
			// 
			// Shape23
			// 
			this.Shape23.Height = 0.1770833F;
			this.Shape23.Left = 6.166667F;
			this.Shape23.Name = "Shape23";
			this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape23.Top = 5.15625F;
			this.Shape23.Width = 0.1770833F;
			// 
			// Shape24
			// 
			this.Shape24.Height = 0.1770833F;
			this.Shape24.Left = 6.166667F;
			this.Shape24.Name = "Shape24";
			this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape24.Top = 5.65625F;
			this.Shape24.Width = 0.1770833F;
			// 
			// Shape25
			// 
			this.Shape25.Height = 0.1770833F;
			this.Shape25.Left = 6.166667F;
			this.Shape25.Name = "Shape25";
			this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape25.Top = 6.15625F;
			this.Shape25.Width = 0.1770833F;
			// 
			// Shape26
			// 
			this.Shape26.Height = 0.1770833F;
			this.Shape26.Left = 6.166667F;
			this.Shape26.Name = "Shape26";
			this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape26.Top = 6.65625F;
			this.Shape26.Width = 0.1770833F;
			// 
			// Shape27
			// 
			this.Shape27.Height = 0.1770833F;
			this.Shape27.Left = 6.166667F;
			this.Shape27.Name = "Shape27";
			this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape27.Top = 7.145833F;
			this.Shape27.Width = 0.1770833F;
			// 
			// Shape28
			// 
			this.Shape28.Height = 0.1770833F;
			this.Shape28.Left = 6.552083F;
			this.Shape28.Name = "Shape28";
			this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape28.Top = 4.65625F;
			this.Shape28.Width = 0.1770833F;
			// 
			// Shape29
			// 
			this.Shape29.Height = 0.1770833F;
			this.Shape29.Left = 6.552083F;
			this.Shape29.Name = "Shape29";
			this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape29.Top = 5.15625F;
			this.Shape29.Width = 0.1770833F;
			// 
			// Shape30
			// 
			this.Shape30.Height = 0.1770833F;
			this.Shape30.Left = 6.552083F;
			this.Shape30.Name = "Shape30";
			this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape30.Top = 5.65625F;
			this.Shape30.Width = 0.1770833F;
			// 
			// Shape32
			// 
			this.Shape32.Height = 0.1770833F;
			this.Shape32.Left = 6.552083F;
			this.Shape32.Name = "Shape32";
			this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape32.Top = 6.65625F;
			this.Shape32.Width = 0.1770833F;
			// 
			// Shape31
			// 
			this.Shape31.Height = 0.1770833F;
			this.Shape31.Left = 6.552083F;
			this.Shape31.Name = "Shape31";
			this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape31.Top = 6.15625F;
			this.Shape31.Width = 0.1770833F;
			// 
			// Shape33
			// 
			this.Shape33.Height = 0.1770833F;
			this.Shape33.Left = 6.552083F;
			this.Shape33.Name = "Shape33";
			this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape33.Top = 7.145833F;
			this.Shape33.Width = 0.1770833F;
			// 
			// Shape34
			// 
			this.Shape34.Height = 0.1770833F;
			this.Shape34.Left = 6.947917F;
			this.Shape34.Name = "Shape34";
			this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape34.Top = 4.65625F;
			this.Shape34.Width = 0.1770833F;
			// 
			// Shape35
			// 
			this.Shape35.Height = 0.1770833F;
			this.Shape35.Left = 6.947917F;
			this.Shape35.Name = "Shape35";
			this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape35.Top = 5.15625F;
			this.Shape35.Width = 0.1770833F;
			// 
			// Shape36
			// 
			this.Shape36.Height = 0.1770833F;
			this.Shape36.Left = 6.947917F;
			this.Shape36.Name = "Shape36";
			this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape36.Top = 5.65625F;
			this.Shape36.Width = 0.1770833F;
			// 
			// Shape37
			// 
			this.Shape37.Height = 0.1770833F;
			this.Shape37.Left = 6.947917F;
			this.Shape37.Name = "Shape37";
			this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape37.Top = 6.15625F;
			this.Shape37.Width = 0.1770833F;
			// 
			// Shape38
			// 
			this.Shape38.Height = 0.1770833F;
			this.Shape38.Left = 6.947917F;
			this.Shape38.Name = "Shape38";
			this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape38.Top = 6.65625F;
			this.Shape38.Width = 0.1770833F;
			// 
			// Shape39
			// 
			this.Shape39.Height = 0.1770833F;
			this.Shape39.Left = 6.947917F;
			this.Shape39.Name = "Shape39";
			this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape39.Top = 7.145833F;
			this.Shape39.Width = 0.1770833F;
			// 
			// Shape40
			// 
			this.Shape40.Height = 0.1770833F;
			this.Shape40.Left = 7.34375F;
			this.Shape40.Name = "Shape40";
			this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape40.Top = 4.65625F;
			this.Shape40.Width = 0.1770833F;
			// 
			// Shape41
			// 
			this.Shape41.Height = 0.1770833F;
			this.Shape41.Left = 7.34375F;
			this.Shape41.Name = "Shape41";
			this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape41.Top = 5.15625F;
			this.Shape41.Width = 0.1770833F;
			// 
			// Shape42
			// 
			this.Shape42.Height = 0.1770833F;
			this.Shape42.Left = 7.34375F;
			this.Shape42.Name = "Shape42";
			this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape42.Top = 5.65625F;
			this.Shape42.Width = 0.1770833F;
			// 
			// Shape43
			// 
			this.Shape43.Height = 0.1770833F;
			this.Shape43.Left = 7.34375F;
			this.Shape43.Name = "Shape43";
			this.Shape43.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape43.Top = 6.15625F;
			this.Shape43.Width = 0.1770833F;
			// 
			// Shape44
			// 
			this.Shape44.Height = 0.1770833F;
			this.Shape44.Left = 7.34375F;
			this.Shape44.Name = "Shape44";
			this.Shape44.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape44.Top = 6.65625F;
			this.Shape44.Width = 0.1770833F;
			// 
			// Shape45
			// 
			this.Shape45.Height = 0.1770833F;
			this.Shape45.Left = 7.34375F;
			this.Shape45.Name = "Shape45";
			this.Shape45.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape45.Top = 7.145833F;
			this.Shape45.Width = 0.1770833F;
			// 
			// Shape46
			// 
			this.Shape46.Height = 0.1770833F;
			this.Shape46.Left = 7.739583F;
			this.Shape46.Name = "Shape46";
			this.Shape46.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape46.Top = 4.65625F;
			this.Shape46.Width = 0.1770833F;
			// 
			// Shape47
			// 
			this.Shape47.Height = 0.1770833F;
			this.Shape47.Left = 7.739583F;
			this.Shape47.Name = "Shape47";
			this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape47.Top = 5.15625F;
			this.Shape47.Width = 0.1770833F;
			// 
			// Shape48
			// 
			this.Shape48.Height = 0.1770833F;
			this.Shape48.Left = 7.739583F;
			this.Shape48.Name = "Shape48";
			this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape48.Top = 5.65625F;
			this.Shape48.Width = 0.1770833F;
			// 
			// Shape49
			// 
			this.Shape49.Height = 0.1770833F;
			this.Shape49.Left = 7.739583F;
			this.Shape49.Name = "Shape49";
			this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape49.Top = 6.15625F;
			this.Shape49.Width = 0.1770833F;
			// 
			// Shape50
			// 
			this.Shape50.Height = 0.1770833F;
			this.Shape50.Left = 7.739583F;
			this.Shape50.Name = "Shape50";
			this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape50.Top = 6.65625F;
			this.Shape50.Width = 0.1770833F;
			// 
			// Shape51
			// 
			this.Shape51.Height = 0.1770833F;
			this.Shape51.Left = 7.739583F;
			this.Shape51.Name = "Shape51";
			this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape51.Top = 7.145833F;
			this.Shape51.Width = 0.1770833F;
			// 
			// Shape52
			// 
			this.Shape52.Height = 0.1770833F;
			this.Shape52.Left = 8.135417F;
			this.Shape52.Name = "Shape52";
			this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape52.Top = 4.65625F;
			this.Shape52.Width = 0.1770833F;
			// 
			// Shape53
			// 
			this.Shape53.Height = 0.1770833F;
			this.Shape53.Left = 8.135417F;
			this.Shape53.Name = "Shape53";
			this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape53.Top = 5.15625F;
			this.Shape53.Width = 0.1770833F;
			// 
			// Shape54
			// 
			this.Shape54.Height = 0.1770833F;
			this.Shape54.Left = 8.135417F;
			this.Shape54.Name = "Shape54";
			this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape54.Top = 5.65625F;
			this.Shape54.Width = 0.1770833F;
			// 
			// Shape55
			// 
			this.Shape55.Height = 0.1770833F;
			this.Shape55.Left = 8.135417F;
			this.Shape55.Name = "Shape55";
			this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape55.Top = 6.15625F;
			this.Shape55.Width = 0.1770833F;
			// 
			// Shape56
			// 
			this.Shape56.Height = 0.1770833F;
			this.Shape56.Left = 8.135417F;
			this.Shape56.Name = "Shape56";
			this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape56.Top = 6.65625F;
			this.Shape56.Width = 0.1770833F;
			// 
			// Shape57
			// 
			this.Shape57.Height = 0.1770833F;
			this.Shape57.Left = 8.135417F;
			this.Shape57.Name = "Shape57";
			this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape57.Top = 7.145833F;
			this.Shape57.Width = 0.1770833F;
			// 
			// Shape58
			// 
			this.Shape58.Height = 0.1770833F;
			this.Shape58.Left = 8.520833F;
			this.Shape58.Name = "Shape58";
			this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape58.Top = 4.65625F;
			this.Shape58.Width = 0.1770833F;
			// 
			// Shape59
			// 
			this.Shape59.Height = 0.1770833F;
			this.Shape59.Left = 8.520833F;
			this.Shape59.Name = "Shape59";
			this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape59.Top = 5.15625F;
			this.Shape59.Width = 0.1770833F;
			// 
			// Shape60
			// 
			this.Shape60.Height = 0.1770833F;
			this.Shape60.Left = 8.520833F;
			this.Shape60.Name = "Shape60";
			this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape60.Top = 5.65625F;
			this.Shape60.Width = 0.1770833F;
			// 
			// Shape61
			// 
			this.Shape61.Height = 0.1770833F;
			this.Shape61.Left = 8.520833F;
			this.Shape61.Name = "Shape61";
			this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape61.Top = 6.15625F;
			this.Shape61.Width = 0.1770833F;
			// 
			// Shape62
			// 
			this.Shape62.Height = 0.1770833F;
			this.Shape62.Left = 8.520833F;
			this.Shape62.Name = "Shape62";
			this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape62.Top = 6.65625F;
			this.Shape62.Width = 0.1770833F;
			// 
			// Shape63
			// 
			this.Shape63.Height = 0.1770833F;
			this.Shape63.Left = 8.520833F;
			this.Shape63.Name = "Shape63";
			this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape63.Top = 7.145833F;
			this.Shape63.Width = 0.1770833F;
			// 
			// Shape64
			// 
			this.Shape64.Height = 0.1770833F;
			this.Shape64.Left = 8.916667F;
			this.Shape64.Name = "Shape64";
			this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape64.Top = 4.65625F;
			this.Shape64.Width = 0.1770833F;
			// 
			// Shape65
			// 
			this.Shape65.Height = 0.1770833F;
			this.Shape65.Left = 8.916667F;
			this.Shape65.Name = "Shape65";
			this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape65.Top = 5.15625F;
			this.Shape65.Width = 0.1770833F;
			// 
			// Shape66
			// 
			this.Shape66.Height = 0.1770833F;
			this.Shape66.Left = 8.916667F;
			this.Shape66.Name = "Shape66";
			this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape66.Top = 5.65625F;
			this.Shape66.Width = 0.1770833F;
			// 
			// Shape67
			// 
			this.Shape67.Height = 0.1770833F;
			this.Shape67.Left = 8.916667F;
			this.Shape67.Name = "Shape67";
			this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape67.Top = 6.15625F;
			this.Shape67.Width = 0.1770833F;
			// 
			// Shape68
			// 
			this.Shape68.Height = 0.1770833F;
			this.Shape68.Left = 8.916667F;
			this.Shape68.Name = "Shape68";
			this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape68.Top = 6.65625F;
			this.Shape68.Width = 0.1770833F;
			// 
			// Shape69
			// 
			this.Shape69.Height = 0.1770833F;
			this.Shape69.Left = 8.916667F;
			this.Shape69.Name = "Shape69";
			this.Shape69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape69.Top = 7.145833F;
			this.Shape69.Width = 0.1770833F;
			// 
			// Shape70
			// 
			this.Shape70.Height = 0.1770833F;
			this.Shape70.Left = 9.3125F;
			this.Shape70.Name = "Shape70";
			this.Shape70.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape70.Top = 4.65625F;
			this.Shape70.Width = 0.1770833F;
			// 
			// Shape71
			// 
			this.Shape71.Height = 0.1770833F;
			this.Shape71.Left = 9.3125F;
			this.Shape71.Name = "Shape71";
			this.Shape71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape71.Top = 5.15625F;
			this.Shape71.Width = 0.1770833F;
			// 
			// Shape72
			// 
			this.Shape72.Height = 0.1770833F;
			this.Shape72.Left = 9.3125F;
			this.Shape72.Name = "Shape72";
			this.Shape72.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape72.Top = 5.65625F;
			this.Shape72.Width = 0.1770833F;
			// 
			// Shape73
			// 
			this.Shape73.Height = 0.1770833F;
			this.Shape73.Left = 9.3125F;
			this.Shape73.Name = "Shape73";
			this.Shape73.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape73.Top = 6.15625F;
			this.Shape73.Width = 0.1770833F;
			// 
			// Shape74
			// 
			this.Shape74.Height = 0.1770833F;
			this.Shape74.Left = 9.3125F;
			this.Shape74.Name = "Shape74";
			this.Shape74.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape74.Top = 6.65625F;
			this.Shape74.Width = 0.1770833F;
			// 
			// Shape75
			// 
			this.Shape75.Height = 0.1770833F;
			this.Shape75.Left = 9.3125F;
			this.Shape75.Name = "Shape75";
			this.Shape75.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape75.Top = 7.145833F;
			this.Shape75.Width = 0.1770833F;
			// 
			// Shape76
			// 
			this.Shape76.Height = 0.1770833F;
			this.Shape76.Left = 9.708333F;
			this.Shape76.Name = "Shape76";
			this.Shape76.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape76.Top = 4.65625F;
			this.Shape76.Width = 0.1770833F;
			// 
			// Shape77
			// 
			this.Shape77.Height = 0.1770833F;
			this.Shape77.Left = 9.708333F;
			this.Shape77.Name = "Shape77";
			this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape77.Top = 5.15625F;
			this.Shape77.Width = 0.1770833F;
			// 
			// Shape78
			// 
			this.Shape78.Height = 0.1770833F;
			this.Shape78.Left = 9.708333F;
			this.Shape78.Name = "Shape78";
			this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape78.Top = 5.65625F;
			this.Shape78.Width = 0.1770833F;
			// 
			// Shape79
			// 
			this.Shape79.Height = 0.1770833F;
			this.Shape79.Left = 9.708333F;
			this.Shape79.Name = "Shape79";
			this.Shape79.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape79.Top = 6.15625F;
			this.Shape79.Width = 0.1770833F;
			// 
			// Shape80
			// 
			this.Shape80.Height = 0.1770833F;
			this.Shape80.Left = 9.708333F;
			this.Shape80.Name = "Shape80";
			this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape80.Top = 6.65625F;
			this.Shape80.Width = 0.1770833F;
			// 
			// Shape81
			// 
			this.Shape81.Height = 0.1770833F;
			this.Shape81.Left = 9.708333F;
			this.Shape81.Name = "Shape81";
			this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape81.Top = 7.145833F;
			this.Shape81.Width = 0.1770833F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.5625F;
			this.Line8.Width = 9.999306F;
			this.Line8.X1 = 0F;
			this.Line8.X2 = 9.999306F;
			this.Line8.Y1 = 1.5625F;
			this.Line8.Y2 = 1.5625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 1.25F;
			this.Line7.Width = 9.999306F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 9.999306F;
			this.Line7.Y1 = 1.25F;
			this.Line7.Y2 = 1.25F;
			// 
			// Line37
			// 
			this.Line37.Height = 0F;
			this.Line37.Left = 5.270833F;
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 4.34375F;
			this.Line37.Width = 4.729167F;
			this.Line37.X1 = 5.270833F;
			this.Line37.X2 = 10F;
			this.Line37.Y1 = 4.34375F;
			this.Line37.Y2 = 4.34375F;
			// 
			// Line52
			// 
			this.Line52.Height = 0.9472222F;
			this.Line52.Left = 8.1875F;
			this.Line52.LineWeight = 1F;
			this.Line52.Name = "Line52";
			this.Line52.Top = 1.25F;
			this.Line52.Width = 0F;
			this.Line52.X1 = 8.1875F;
			this.Line52.X2 = 8.1875F;
			this.Line52.Y1 = 1.25F;
			this.Line52.Y2 = 2.197222F;
			// 
			// Line23
			// 
			this.Line23.Height = 3.302083F;
			this.Line23.Left = 4.697917F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 4.1875F;
			this.Line23.Width = 0F;
			this.Line23.X1 = 4.697917F;
			this.Line23.X2 = 4.697917F;
			this.Line23.Y1 = 4.1875F;
			this.Line23.Y2 = 7.489583F;
			// 
			// Line24
			// 
			this.Line24.Height = 3.302083F;
			this.Line24.Left = 5.270833F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 4.1875F;
			this.Line24.Width = 0F;
			this.Line24.X1 = 5.270833F;
			this.Line24.X2 = 5.270833F;
			this.Line24.Y1 = 4.1875F;
			this.Line24.Y2 = 7.489583F;
			// 
			// Label183
			// 
			this.Label183.Height = 0.1770833F;
			this.Label183.HyperLink = null;
			this.Label183.Left = 3F;
			this.Label183.Name = "Label183";
			this.Label183.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label183.Text = "Do not attach to your tax return. Keep for your records.";
			this.Label183.Top = 0.625F;
			this.Label183.Width = 3.71875F;
			// 
			// Label184
			// 
			this.Label184.Height = 0.1770833F;
			this.Label184.HyperLink = null;
			this.Label184.Left = 2.8125F;
			this.Label184.Name = "Label184";
			this.Label184.Style = "font-family: \'Marlett\'; font-size: 12pt; font-weight: bold; text-align: left; ddo" + "-char-set: 2";
			this.Label184.Text = "4";
			this.Label184.Top = 0.625F;
			this.Label184.Width = 0.3333333F;
			// 
			// Line58
			// 
			this.Line58.Height = 0F;
			this.Line58.Left = 0F;
			this.Line58.LineWeight = 2F;
			this.Line58.Name = "Line58";
			this.Line58.Top = 2.197222F;
			this.Line58.Width = 9.999306F;
			this.Line58.X1 = 0F;
			this.Line58.X2 = 9.999306F;
			this.Line58.Y1 = 2.197222F;
			this.Line58.Y2 = 2.197222F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 2F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.875F;
			this.Line5.Width = 9.999306F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 9.999306F;
			this.Line5.Y1 = 1.875F;
			this.Line5.Y2 = 1.875F;
			// 
			// rpt1095C2016BlankPage1
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAll12Box14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAll12Box15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox15_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAll12Box16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox16_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredIndividuals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerPostalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label139)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label144)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label145)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label146)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label147)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label148)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label149)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label150)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label151)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label153)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label155)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label157)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStartMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox15_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAll12Box16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox16_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredIndividuals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerPostalCode;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label139;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label140;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label144;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label145;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label146;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label147;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label148;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label149;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label150;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label151;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label153;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label154;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label155;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label156;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label157;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label158;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label159;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label160;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label161;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label162;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label163;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label164;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label165;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label166;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label167;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label168;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label169;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label170;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label171;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label172;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label173;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label174;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label181;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape43;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape44;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape45;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape46;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape69;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape70;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape71;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape72;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape73;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape74;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape75;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape76;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape79;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label183;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label184;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
	}
}
