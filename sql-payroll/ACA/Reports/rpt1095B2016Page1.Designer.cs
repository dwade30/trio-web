﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2016Page1.
	/// </summary>
	partial class rpt1095B2016Page1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095B2016Page1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProviderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProviderAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProviderCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProviderState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProviderPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProviderTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginOfPolicy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCoveredName1,
				this.txtCoveredSSN1,
				this.txtCoveredDOB1,
				this.txtCoveredName2,
				this.txtCoveredSSN2,
				this.txtCoveredDOB2,
				this.txtCoveredName3,
				this.txtCoveredSSN3,
				this.txtCoveredDOB3,
				this.txtCoveredName4,
				this.txtCoveredSSN4,
				this.txtCoveredDOB4,
				this.txtCoveredName5,
				this.txtCoveredSSN5,
				this.txtCoveredDOB5,
				this.txtCoveredName6,
				this.txtCoveredSSN6,
				this.txtCoveredDOB6,
				this.txtCoveredAll12_1,
				this.txtCoveredMonth1_1,
				this.txtCoveredMonth2_1,
				this.txtCoveredMonth3_1,
				this.txtCoveredMonth4_1,
				this.txtCoveredMonth5_1,
				this.txtCoveredMonth6_1,
				this.txtCoveredMonth7_1,
				this.txtCoveredMonth8_1,
				this.txtCoveredMonth9_1,
				this.txtCoveredMonth10_1,
				this.txtCoveredMonth11_1,
				this.txtCoveredMonth12_1,
				this.txtCoveredAll12_2,
				this.txtCoveredMonth1_2,
				this.txtCoveredMonth2_2,
				this.txtCoveredMonth3_2,
				this.txtCoveredMonth4_2,
				this.txtCoveredMonth5_2,
				this.txtCoveredMonth6_2,
				this.txtCoveredMonth7_2,
				this.txtCoveredMonth8_2,
				this.txtCoveredMonth9_2,
				this.txtCoveredMonth10_2,
				this.txtCoveredMonth11_2,
				this.txtCoveredMonth12_2,
				this.txtCoveredAll12_3,
				this.txtCoveredMonth1_3,
				this.txtCoveredMonth2_3,
				this.txtCoveredMonth3_3,
				this.txtCoveredMonth4_3,
				this.txtCoveredMonth5_3,
				this.txtCoveredMonth6_3,
				this.txtCoveredMonth7_3,
				this.txtCoveredMonth8_3,
				this.txtCoveredMonth9_3,
				this.txtCoveredMonth10_3,
				this.txtCoveredMonth11_3,
				this.txtCoveredMonth12_3,
				this.txtCoveredAll12_4,
				this.txtCoveredMonth1_4,
				this.txtCoveredMonth2_4,
				this.txtCoveredMonth3_4,
				this.txtCoveredMonth4_4,
				this.txtCoveredMonth5_4,
				this.txtCoveredMonth6_4,
				this.txtCoveredMonth7_4,
				this.txtCoveredMonth8_4,
				this.txtCoveredMonth9_4,
				this.txtCoveredMonth10_4,
				this.txtCoveredMonth11_4,
				this.txtCoveredMonth12_4,
				this.txtCoveredAll12_5,
				this.txtCoveredMonth1_5,
				this.txtCoveredMonth2_5,
				this.txtCoveredMonth3_5,
				this.txtCoveredMonth4_5,
				this.txtCoveredMonth5_5,
				this.txtCoveredMonth6_5,
				this.txtCoveredMonth7_5,
				this.txtCoveredMonth8_5,
				this.txtCoveredMonth9_5,
				this.txtCoveredMonth10_5,
				this.txtCoveredMonth11_5,
				this.txtCoveredMonth12_5,
				this.txtCoveredAll12_6,
				this.txtCoveredMonth1_6,
				this.txtCoveredMonth2_6,
				this.txtCoveredMonth3_6,
				this.txtCoveredMonth4_6,
				this.txtCoveredMonth5_6,
				this.txtCoveredMonth6_6,
				this.txtCoveredMonth7_6,
				this.txtCoveredMonth8_6,
				this.txtCoveredMonth9_6,
				this.txtCoveredMonth10_6,
				this.txtCoveredMonth11_6,
				this.txtCoveredMonth12_6,
				this.txtName,
				this.txtSSN,
				this.txtAddress,
				this.txtCity,
				this.txtState,
				this.txtPostalCode,
				this.txtProviderName,
				this.txtProviderAddress,
				this.txtProviderCity,
				this.txtProviderState,
				this.txtProviderPostalCode,
				this.txtProviderTelephone,
				this.txtOriginOfPolicy,
				this.txtEIN
			});
			this.Detail.Height = 7.052083F;
			this.Detail.Name = "Detail";
			// 
			// txtCoveredName1
			// 
			this.txtCoveredName1.Height = 0.1770833F;
			this.txtCoveredName1.Left = 0.1666667F;
			this.txtCoveredName1.Name = "txtCoveredName1";
			this.txtCoveredName1.Text = null;
			this.txtCoveredName1.Top = 4.270833F;
			this.txtCoveredName1.Width = 2.166667F;
			// 
			// txtCoveredSSN1
			// 
			this.txtCoveredSSN1.Height = 0.1770833F;
			this.txtCoveredSSN1.Left = 2.625F;
			this.txtCoveredSSN1.Name = "txtCoveredSSN1";
			this.txtCoveredSSN1.Text = null;
			this.txtCoveredSSN1.Top = 4.270833F;
			this.txtCoveredSSN1.Width = 1F;
			// 
			// txtCoveredDOB1
			// 
			this.txtCoveredDOB1.Height = 0.1770833F;
			this.txtCoveredDOB1.Left = 3.791667F;
			this.txtCoveredDOB1.Name = "txtCoveredDOB1";
			this.txtCoveredDOB1.Text = null;
			this.txtCoveredDOB1.Top = 4.270833F;
			this.txtCoveredDOB1.Width = 0.9166667F;
			// 
			// txtCoveredName2
			// 
			this.txtCoveredName2.Height = 0.1770833F;
			this.txtCoveredName2.Left = 0.1666667F;
			this.txtCoveredName2.Name = "txtCoveredName2";
			this.txtCoveredName2.Text = null;
			this.txtCoveredName2.Top = 4.770833F;
			this.txtCoveredName2.Width = 2.166667F;
			// 
			// txtCoveredSSN2
			// 
			this.txtCoveredSSN2.Height = 0.1770833F;
			this.txtCoveredSSN2.Left = 2.625F;
			this.txtCoveredSSN2.Name = "txtCoveredSSN2";
			this.txtCoveredSSN2.Text = null;
			this.txtCoveredSSN2.Top = 4.770833F;
			this.txtCoveredSSN2.Width = 1F;
			// 
			// txtCoveredDOB2
			// 
			this.txtCoveredDOB2.Height = 0.1770833F;
			this.txtCoveredDOB2.Left = 3.791667F;
			this.txtCoveredDOB2.Name = "txtCoveredDOB2";
			this.txtCoveredDOB2.Text = null;
			this.txtCoveredDOB2.Top = 4.770833F;
			this.txtCoveredDOB2.Width = 0.9166667F;
			// 
			// txtCoveredName3
			// 
			this.txtCoveredName3.Height = 0.1770833F;
			this.txtCoveredName3.Left = 0.1666667F;
			this.txtCoveredName3.Name = "txtCoveredName3";
			this.txtCoveredName3.Text = null;
			this.txtCoveredName3.Top = 5.25F;
			this.txtCoveredName3.Width = 2.166667F;
			// 
			// txtCoveredSSN3
			// 
			this.txtCoveredSSN3.Height = 0.1770833F;
			this.txtCoveredSSN3.Left = 2.625F;
			this.txtCoveredSSN3.Name = "txtCoveredSSN3";
			this.txtCoveredSSN3.Text = null;
			this.txtCoveredSSN3.Top = 5.25F;
			this.txtCoveredSSN3.Width = 1F;
			// 
			// txtCoveredDOB3
			// 
			this.txtCoveredDOB3.Height = 0.1770833F;
			this.txtCoveredDOB3.Left = 3.791667F;
			this.txtCoveredDOB3.Name = "txtCoveredDOB3";
			this.txtCoveredDOB3.Text = null;
			this.txtCoveredDOB3.Top = 5.25F;
			this.txtCoveredDOB3.Width = 0.9166667F;
			// 
			// txtCoveredName4
			// 
			this.txtCoveredName4.Height = 0.1770833F;
			this.txtCoveredName4.Left = 0.1666667F;
			this.txtCoveredName4.Name = "txtCoveredName4";
			this.txtCoveredName4.Text = null;
			this.txtCoveredName4.Top = 5.75F;
			this.txtCoveredName4.Width = 2.166667F;
			// 
			// txtCoveredSSN4
			// 
			this.txtCoveredSSN4.Height = 0.1770833F;
			this.txtCoveredSSN4.Left = 2.625F;
			this.txtCoveredSSN4.Name = "txtCoveredSSN4";
			this.txtCoveredSSN4.Text = null;
			this.txtCoveredSSN4.Top = 5.75F;
			this.txtCoveredSSN4.Width = 1F;
			// 
			// txtCoveredDOB4
			// 
			this.txtCoveredDOB4.Height = 0.1770833F;
			this.txtCoveredDOB4.Left = 3.791667F;
			this.txtCoveredDOB4.Name = "txtCoveredDOB4";
			this.txtCoveredDOB4.Text = null;
			this.txtCoveredDOB4.Top = 5.75F;
			this.txtCoveredDOB4.Width = 0.9166667F;
			// 
			// txtCoveredName5
			// 
			this.txtCoveredName5.Height = 0.1770833F;
			this.txtCoveredName5.Left = 0.1666667F;
			this.txtCoveredName5.Name = "txtCoveredName5";
			this.txtCoveredName5.Text = null;
			this.txtCoveredName5.Top = 6.25F;
			this.txtCoveredName5.Width = 2.166667F;
			// 
			// txtCoveredSSN5
			// 
			this.txtCoveredSSN5.Height = 0.1770833F;
			this.txtCoveredSSN5.Left = 2.625F;
			this.txtCoveredSSN5.Name = "txtCoveredSSN5";
			this.txtCoveredSSN5.Text = null;
			this.txtCoveredSSN5.Top = 6.25F;
			this.txtCoveredSSN5.Width = 1F;
			// 
			// txtCoveredDOB5
			// 
			this.txtCoveredDOB5.Height = 0.1770833F;
			this.txtCoveredDOB5.Left = 3.791667F;
			this.txtCoveredDOB5.Name = "txtCoveredDOB5";
			this.txtCoveredDOB5.Text = null;
			this.txtCoveredDOB5.Top = 6.25F;
			this.txtCoveredDOB5.Width = 0.9166667F;
			// 
			// txtCoveredName6
			// 
			this.txtCoveredName6.Height = 0.1770833F;
			this.txtCoveredName6.Left = 0.1666667F;
			this.txtCoveredName6.Name = "txtCoveredName6";
			this.txtCoveredName6.Text = null;
			this.txtCoveredName6.Top = 6.729167F;
			this.txtCoveredName6.Width = 2.166667F;
			// 
			// txtCoveredSSN6
			// 
			this.txtCoveredSSN6.Height = 0.1770833F;
			this.txtCoveredSSN6.Left = 2.625F;
			this.txtCoveredSSN6.Name = "txtCoveredSSN6";
			this.txtCoveredSSN6.Text = null;
			this.txtCoveredSSN6.Top = 6.729167F;
			this.txtCoveredSSN6.Width = 1F;
			// 
			// txtCoveredDOB6
			// 
			this.txtCoveredDOB6.Height = 0.1770833F;
			this.txtCoveredDOB6.Left = 3.791667F;
			this.txtCoveredDOB6.Name = "txtCoveredDOB6";
			this.txtCoveredDOB6.Text = null;
			this.txtCoveredDOB6.Top = 6.729167F;
			this.txtCoveredDOB6.Width = 0.9166667F;
			// 
			// txtCoveredAll12_1
			// 
			this.txtCoveredAll12_1.Height = 0.1770833F;
			this.txtCoveredAll12_1.Left = 4.9375F;
			this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
			this.txtCoveredAll12_1.Text = null;
			this.txtCoveredAll12_1.Top = 4.270833F;
			this.txtCoveredAll12_1.Width = 0.25F;
			// 
			// txtCoveredMonth1_1
			// 
			this.txtCoveredMonth1_1.Height = 0.1770833F;
			this.txtCoveredMonth1_1.Left = 5.395833F;
			this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
			this.txtCoveredMonth1_1.Text = null;
			this.txtCoveredMonth1_1.Top = 4.270833F;
			this.txtCoveredMonth1_1.Width = 0.25F;
			// 
			// txtCoveredMonth2_1
			// 
			this.txtCoveredMonth2_1.Height = 0.1770833F;
			this.txtCoveredMonth2_1.Left = 5.805555F;
			this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
			this.txtCoveredMonth2_1.Text = null;
			this.txtCoveredMonth2_1.Top = 4.270833F;
			this.txtCoveredMonth2_1.Width = 0.25F;
			// 
			// txtCoveredMonth3_1
			// 
			this.txtCoveredMonth3_1.Height = 0.1770833F;
			this.txtCoveredMonth3_1.Left = 6.1875F;
			this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
			this.txtCoveredMonth3_1.Text = null;
			this.txtCoveredMonth3_1.Top = 4.270833F;
			this.txtCoveredMonth3_1.Width = 0.25F;
			// 
			// txtCoveredMonth4_1
			// 
			this.txtCoveredMonth4_1.Height = 0.1770833F;
			this.txtCoveredMonth4_1.Left = 6.597222F;
			this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
			this.txtCoveredMonth4_1.Text = null;
			this.txtCoveredMonth4_1.Top = 4.270833F;
			this.txtCoveredMonth4_1.Width = 0.25F;
			// 
			// txtCoveredMonth5_1
			// 
			this.txtCoveredMonth5_1.Height = 0.1770833F;
			this.txtCoveredMonth5_1.Left = 7.006945F;
			this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
			this.txtCoveredMonth5_1.Text = null;
			this.txtCoveredMonth5_1.Top = 4.270833F;
			this.txtCoveredMonth5_1.Width = 0.25F;
			// 
			// txtCoveredMonth6_1
			// 
			this.txtCoveredMonth6_1.Height = 0.1770833F;
			this.txtCoveredMonth6_1.Left = 7.409722F;
			this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
			this.txtCoveredMonth6_1.Text = null;
			this.txtCoveredMonth6_1.Top = 4.270833F;
			this.txtCoveredMonth6_1.Width = 0.25F;
			// 
			// txtCoveredMonth7_1
			// 
			this.txtCoveredMonth7_1.Height = 0.1770833F;
			this.txtCoveredMonth7_1.Left = 7.8125F;
			this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
			this.txtCoveredMonth7_1.Text = null;
			this.txtCoveredMonth7_1.Top = 4.270833F;
			this.txtCoveredMonth7_1.Width = 0.25F;
			// 
			// txtCoveredMonth8_1
			// 
			this.txtCoveredMonth8_1.Height = 0.1770833F;
			this.txtCoveredMonth8_1.Left = 8.194445F;
			this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
			this.txtCoveredMonth8_1.Text = null;
			this.txtCoveredMonth8_1.Top = 4.270833F;
			this.txtCoveredMonth8_1.Width = 0.25F;
			// 
			// txtCoveredMonth9_1
			// 
			this.txtCoveredMonth9_1.Height = 0.1770833F;
			this.txtCoveredMonth9_1.Left = 8.604167F;
			this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
			this.txtCoveredMonth9_1.Text = null;
			this.txtCoveredMonth9_1.Top = 4.270833F;
			this.txtCoveredMonth9_1.Width = 0.25F;
			// 
			// txtCoveredMonth10_1
			// 
			this.txtCoveredMonth10_1.Height = 0.1770833F;
			this.txtCoveredMonth10_1.Left = 9.020833F;
			this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
			this.txtCoveredMonth10_1.Text = null;
			this.txtCoveredMonth10_1.Top = 4.270833F;
			this.txtCoveredMonth10_1.Width = 0.25F;
			// 
			// txtCoveredMonth11_1
			// 
			this.txtCoveredMonth11_1.Height = 0.1770833F;
			this.txtCoveredMonth11_1.Left = 9.423611F;
			this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
			this.txtCoveredMonth11_1.Text = null;
			this.txtCoveredMonth11_1.Top = 4.270833F;
			this.txtCoveredMonth11_1.Width = 0.25F;
			// 
			// txtCoveredMonth12_1
			// 
			this.txtCoveredMonth12_1.Height = 0.1770833F;
			this.txtCoveredMonth12_1.Left = 9.805555F;
			this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
			this.txtCoveredMonth12_1.Text = null;
			this.txtCoveredMonth12_1.Top = 4.270833F;
			this.txtCoveredMonth12_1.Width = 0.1944444F;
			// 
			// txtCoveredAll12_2
			// 
			this.txtCoveredAll12_2.Height = 0.1770833F;
			this.txtCoveredAll12_2.Left = 4.9375F;
			this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
			this.txtCoveredAll12_2.Text = null;
			this.txtCoveredAll12_2.Top = 4.770833F;
			this.txtCoveredAll12_2.Width = 0.25F;
			// 
			// txtCoveredMonth1_2
			// 
			this.txtCoveredMonth1_2.Height = 0.1770833F;
			this.txtCoveredMonth1_2.Left = 5.395833F;
			this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
			this.txtCoveredMonth1_2.Text = null;
			this.txtCoveredMonth1_2.Top = 4.770833F;
			this.txtCoveredMonth1_2.Width = 0.25F;
			// 
			// txtCoveredMonth2_2
			// 
			this.txtCoveredMonth2_2.Height = 0.1770833F;
			this.txtCoveredMonth2_2.Left = 5.805555F;
			this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
			this.txtCoveredMonth2_2.Text = null;
			this.txtCoveredMonth2_2.Top = 4.770833F;
			this.txtCoveredMonth2_2.Width = 0.25F;
			// 
			// txtCoveredMonth3_2
			// 
			this.txtCoveredMonth3_2.Height = 0.1770833F;
			this.txtCoveredMonth3_2.Left = 6.1875F;
			this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
			this.txtCoveredMonth3_2.Text = null;
			this.txtCoveredMonth3_2.Top = 4.770833F;
			this.txtCoveredMonth3_2.Width = 0.25F;
			// 
			// txtCoveredMonth4_2
			// 
			this.txtCoveredMonth4_2.Height = 0.1770833F;
			this.txtCoveredMonth4_2.Left = 6.597222F;
			this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
			this.txtCoveredMonth4_2.Text = null;
			this.txtCoveredMonth4_2.Top = 4.770833F;
			this.txtCoveredMonth4_2.Width = 0.25F;
			// 
			// txtCoveredMonth5_2
			// 
			this.txtCoveredMonth5_2.Height = 0.1770833F;
			this.txtCoveredMonth5_2.Left = 7.006945F;
			this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
			this.txtCoveredMonth5_2.Text = null;
			this.txtCoveredMonth5_2.Top = 4.770833F;
			this.txtCoveredMonth5_2.Width = 0.25F;
			// 
			// txtCoveredMonth6_2
			// 
			this.txtCoveredMonth6_2.Height = 0.1770833F;
			this.txtCoveredMonth6_2.Left = 7.409722F;
			this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
			this.txtCoveredMonth6_2.Text = null;
			this.txtCoveredMonth6_2.Top = 4.770833F;
			this.txtCoveredMonth6_2.Width = 0.25F;
			// 
			// txtCoveredMonth7_2
			// 
			this.txtCoveredMonth7_2.Height = 0.1770833F;
			this.txtCoveredMonth7_2.Left = 7.8125F;
			this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
			this.txtCoveredMonth7_2.Text = null;
			this.txtCoveredMonth7_2.Top = 4.770833F;
			this.txtCoveredMonth7_2.Width = 0.25F;
			// 
			// txtCoveredMonth8_2
			// 
			this.txtCoveredMonth8_2.Height = 0.1770833F;
			this.txtCoveredMonth8_2.Left = 8.194445F;
			this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
			this.txtCoveredMonth8_2.Text = null;
			this.txtCoveredMonth8_2.Top = 4.770833F;
			this.txtCoveredMonth8_2.Width = 0.25F;
			// 
			// txtCoveredMonth9_2
			// 
			this.txtCoveredMonth9_2.Height = 0.1770833F;
			this.txtCoveredMonth9_2.Left = 8.604167F;
			this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
			this.txtCoveredMonth9_2.Text = null;
			this.txtCoveredMonth9_2.Top = 4.770833F;
			this.txtCoveredMonth9_2.Width = 0.25F;
			// 
			// txtCoveredMonth10_2
			// 
			this.txtCoveredMonth10_2.Height = 0.1770833F;
			this.txtCoveredMonth10_2.Left = 9.020833F;
			this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
			this.txtCoveredMonth10_2.Text = null;
			this.txtCoveredMonth10_2.Top = 4.770833F;
			this.txtCoveredMonth10_2.Width = 0.25F;
			// 
			// txtCoveredMonth11_2
			// 
			this.txtCoveredMonth11_2.Height = 0.1770833F;
			this.txtCoveredMonth11_2.Left = 9.423611F;
			this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
			this.txtCoveredMonth11_2.Text = null;
			this.txtCoveredMonth11_2.Top = 4.770833F;
			this.txtCoveredMonth11_2.Width = 0.25F;
			// 
			// txtCoveredMonth12_2
			// 
			this.txtCoveredMonth12_2.Height = 0.1770833F;
			this.txtCoveredMonth12_2.Left = 9.805555F;
			this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
			this.txtCoveredMonth12_2.Text = null;
			this.txtCoveredMonth12_2.Top = 4.770833F;
			this.txtCoveredMonth12_2.Width = 0.1944444F;
			// 
			// txtCoveredAll12_3
			// 
			this.txtCoveredAll12_3.Height = 0.1770833F;
			this.txtCoveredAll12_3.Left = 4.9375F;
			this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
			this.txtCoveredAll12_3.Text = null;
			this.txtCoveredAll12_3.Top = 5.25F;
			this.txtCoveredAll12_3.Width = 0.25F;
			// 
			// txtCoveredMonth1_3
			// 
			this.txtCoveredMonth1_3.Height = 0.1770833F;
			this.txtCoveredMonth1_3.Left = 5.395833F;
			this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
			this.txtCoveredMonth1_3.Text = null;
			this.txtCoveredMonth1_3.Top = 5.25F;
			this.txtCoveredMonth1_3.Width = 0.25F;
			// 
			// txtCoveredMonth2_3
			// 
			this.txtCoveredMonth2_3.Height = 0.1770833F;
			this.txtCoveredMonth2_3.Left = 5.805555F;
			this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
			this.txtCoveredMonth2_3.Text = null;
			this.txtCoveredMonth2_3.Top = 5.25F;
			this.txtCoveredMonth2_3.Width = 0.25F;
			// 
			// txtCoveredMonth3_3
			// 
			this.txtCoveredMonth3_3.Height = 0.1770833F;
			this.txtCoveredMonth3_3.Left = 6.1875F;
			this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
			this.txtCoveredMonth3_3.Text = null;
			this.txtCoveredMonth3_3.Top = 5.25F;
			this.txtCoveredMonth3_3.Width = 0.25F;
			// 
			// txtCoveredMonth4_3
			// 
			this.txtCoveredMonth4_3.Height = 0.1770833F;
			this.txtCoveredMonth4_3.Left = 6.597222F;
			this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
			this.txtCoveredMonth4_3.Text = null;
			this.txtCoveredMonth4_3.Top = 5.25F;
			this.txtCoveredMonth4_3.Width = 0.25F;
			// 
			// txtCoveredMonth5_3
			// 
			this.txtCoveredMonth5_3.Height = 0.1770833F;
			this.txtCoveredMonth5_3.Left = 7.006945F;
			this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
			this.txtCoveredMonth5_3.Text = null;
			this.txtCoveredMonth5_3.Top = 5.25F;
			this.txtCoveredMonth5_3.Width = 0.25F;
			// 
			// txtCoveredMonth6_3
			// 
			this.txtCoveredMonth6_3.Height = 0.1770833F;
			this.txtCoveredMonth6_3.Left = 7.409722F;
			this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
			this.txtCoveredMonth6_3.Text = null;
			this.txtCoveredMonth6_3.Top = 5.25F;
			this.txtCoveredMonth6_3.Width = 0.25F;
			// 
			// txtCoveredMonth7_3
			// 
			this.txtCoveredMonth7_3.Height = 0.1770833F;
			this.txtCoveredMonth7_3.Left = 7.8125F;
			this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
			this.txtCoveredMonth7_3.Text = null;
			this.txtCoveredMonth7_3.Top = 5.25F;
			this.txtCoveredMonth7_3.Width = 0.25F;
			// 
			// txtCoveredMonth8_3
			// 
			this.txtCoveredMonth8_3.Height = 0.1770833F;
			this.txtCoveredMonth8_3.Left = 8.194445F;
			this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
			this.txtCoveredMonth8_3.Text = null;
			this.txtCoveredMonth8_3.Top = 5.25F;
			this.txtCoveredMonth8_3.Width = 0.25F;
			// 
			// txtCoveredMonth9_3
			// 
			this.txtCoveredMonth9_3.Height = 0.1770833F;
			this.txtCoveredMonth9_3.Left = 8.604167F;
			this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
			this.txtCoveredMonth9_3.Text = null;
			this.txtCoveredMonth9_3.Top = 5.25F;
			this.txtCoveredMonth9_3.Width = 0.25F;
			// 
			// txtCoveredMonth10_3
			// 
			this.txtCoveredMonth10_3.Height = 0.1770833F;
			this.txtCoveredMonth10_3.Left = 9.020833F;
			this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
			this.txtCoveredMonth10_3.Text = null;
			this.txtCoveredMonth10_3.Top = 5.25F;
			this.txtCoveredMonth10_3.Width = 0.25F;
			// 
			// txtCoveredMonth11_3
			// 
			this.txtCoveredMonth11_3.Height = 0.1770833F;
			this.txtCoveredMonth11_3.Left = 9.423611F;
			this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
			this.txtCoveredMonth11_3.Text = null;
			this.txtCoveredMonth11_3.Top = 5.25F;
			this.txtCoveredMonth11_3.Width = 0.25F;
			// 
			// txtCoveredMonth12_3
			// 
			this.txtCoveredMonth12_3.Height = 0.1770833F;
			this.txtCoveredMonth12_3.Left = 9.805555F;
			this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
			this.txtCoveredMonth12_3.Text = null;
			this.txtCoveredMonth12_3.Top = 5.25F;
			this.txtCoveredMonth12_3.Width = 0.1944444F;
			// 
			// txtCoveredAll12_4
			// 
			this.txtCoveredAll12_4.Height = 0.1770833F;
			this.txtCoveredAll12_4.Left = 4.9375F;
			this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
			this.txtCoveredAll12_4.Text = null;
			this.txtCoveredAll12_4.Top = 5.75F;
			this.txtCoveredAll12_4.Width = 0.25F;
			// 
			// txtCoveredMonth1_4
			// 
			this.txtCoveredMonth1_4.Height = 0.1770833F;
			this.txtCoveredMonth1_4.Left = 5.395833F;
			this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
			this.txtCoveredMonth1_4.Text = null;
			this.txtCoveredMonth1_4.Top = 5.75F;
			this.txtCoveredMonth1_4.Width = 0.25F;
			// 
			// txtCoveredMonth2_4
			// 
			this.txtCoveredMonth2_4.Height = 0.1770833F;
			this.txtCoveredMonth2_4.Left = 5.805555F;
			this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
			this.txtCoveredMonth2_4.Text = null;
			this.txtCoveredMonth2_4.Top = 5.75F;
			this.txtCoveredMonth2_4.Width = 0.25F;
			// 
			// txtCoveredMonth3_4
			// 
			this.txtCoveredMonth3_4.Height = 0.1770833F;
			this.txtCoveredMonth3_4.Left = 6.1875F;
			this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
			this.txtCoveredMonth3_4.Text = null;
			this.txtCoveredMonth3_4.Top = 5.75F;
			this.txtCoveredMonth3_4.Width = 0.25F;
			// 
			// txtCoveredMonth4_4
			// 
			this.txtCoveredMonth4_4.Height = 0.1770833F;
			this.txtCoveredMonth4_4.Left = 6.597222F;
			this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
			this.txtCoveredMonth4_4.Text = null;
			this.txtCoveredMonth4_4.Top = 5.75F;
			this.txtCoveredMonth4_4.Width = 0.25F;
			// 
			// txtCoveredMonth5_4
			// 
			this.txtCoveredMonth5_4.Height = 0.1770833F;
			this.txtCoveredMonth5_4.Left = 7.006945F;
			this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
			this.txtCoveredMonth5_4.Text = null;
			this.txtCoveredMonth5_4.Top = 5.75F;
			this.txtCoveredMonth5_4.Width = 0.25F;
			// 
			// txtCoveredMonth6_4
			// 
			this.txtCoveredMonth6_4.Height = 0.1770833F;
			this.txtCoveredMonth6_4.Left = 7.409722F;
			this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
			this.txtCoveredMonth6_4.Text = null;
			this.txtCoveredMonth6_4.Top = 5.75F;
			this.txtCoveredMonth6_4.Width = 0.25F;
			// 
			// txtCoveredMonth7_4
			// 
			this.txtCoveredMonth7_4.Height = 0.1770833F;
			this.txtCoveredMonth7_4.Left = 7.8125F;
			this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
			this.txtCoveredMonth7_4.Text = null;
			this.txtCoveredMonth7_4.Top = 5.75F;
			this.txtCoveredMonth7_4.Width = 0.25F;
			// 
			// txtCoveredMonth8_4
			// 
			this.txtCoveredMonth8_4.Height = 0.1770833F;
			this.txtCoveredMonth8_4.Left = 8.194445F;
			this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
			this.txtCoveredMonth8_4.Text = null;
			this.txtCoveredMonth8_4.Top = 5.75F;
			this.txtCoveredMonth8_4.Width = 0.25F;
			// 
			// txtCoveredMonth9_4
			// 
			this.txtCoveredMonth9_4.Height = 0.1770833F;
			this.txtCoveredMonth9_4.Left = 8.604167F;
			this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
			this.txtCoveredMonth9_4.Text = null;
			this.txtCoveredMonth9_4.Top = 5.75F;
			this.txtCoveredMonth9_4.Width = 0.25F;
			// 
			// txtCoveredMonth10_4
			// 
			this.txtCoveredMonth10_4.Height = 0.1770833F;
			this.txtCoveredMonth10_4.Left = 9.020833F;
			this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
			this.txtCoveredMonth10_4.Text = null;
			this.txtCoveredMonth10_4.Top = 5.75F;
			this.txtCoveredMonth10_4.Width = 0.25F;
			// 
			// txtCoveredMonth11_4
			// 
			this.txtCoveredMonth11_4.Height = 0.1770833F;
			this.txtCoveredMonth11_4.Left = 9.423611F;
			this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
			this.txtCoveredMonth11_4.Text = null;
			this.txtCoveredMonth11_4.Top = 5.75F;
			this.txtCoveredMonth11_4.Width = 0.25F;
			// 
			// txtCoveredMonth12_4
			// 
			this.txtCoveredMonth12_4.Height = 0.1770833F;
			this.txtCoveredMonth12_4.Left = 9.805555F;
			this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
			this.txtCoveredMonth12_4.Text = null;
			this.txtCoveredMonth12_4.Top = 5.75F;
			this.txtCoveredMonth12_4.Width = 0.1944444F;
			// 
			// txtCoveredAll12_5
			// 
			this.txtCoveredAll12_5.Height = 0.1770833F;
			this.txtCoveredAll12_5.Left = 4.9375F;
			this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
			this.txtCoveredAll12_5.Text = null;
			this.txtCoveredAll12_5.Top = 6.25F;
			this.txtCoveredAll12_5.Width = 0.25F;
			// 
			// txtCoveredMonth1_5
			// 
			this.txtCoveredMonth1_5.Height = 0.1770833F;
			this.txtCoveredMonth1_5.Left = 5.395833F;
			this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
			this.txtCoveredMonth1_5.Text = null;
			this.txtCoveredMonth1_5.Top = 6.25F;
			this.txtCoveredMonth1_5.Width = 0.25F;
			// 
			// txtCoveredMonth2_5
			// 
			this.txtCoveredMonth2_5.Height = 0.1770833F;
			this.txtCoveredMonth2_5.Left = 5.805555F;
			this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
			this.txtCoveredMonth2_5.Text = null;
			this.txtCoveredMonth2_5.Top = 6.25F;
			this.txtCoveredMonth2_5.Width = 0.25F;
			// 
			// txtCoveredMonth3_5
			// 
			this.txtCoveredMonth3_5.Height = 0.1770833F;
			this.txtCoveredMonth3_5.Left = 6.1875F;
			this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
			this.txtCoveredMonth3_5.Text = null;
			this.txtCoveredMonth3_5.Top = 6.25F;
			this.txtCoveredMonth3_5.Width = 0.25F;
			// 
			// txtCoveredMonth4_5
			// 
			this.txtCoveredMonth4_5.Height = 0.1770833F;
			this.txtCoveredMonth4_5.Left = 6.597222F;
			this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
			this.txtCoveredMonth4_5.Text = null;
			this.txtCoveredMonth4_5.Top = 6.25F;
			this.txtCoveredMonth4_5.Width = 0.25F;
			// 
			// txtCoveredMonth5_5
			// 
			this.txtCoveredMonth5_5.Height = 0.1770833F;
			this.txtCoveredMonth5_5.Left = 7.006945F;
			this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
			this.txtCoveredMonth5_5.Text = null;
			this.txtCoveredMonth5_5.Top = 6.25F;
			this.txtCoveredMonth5_5.Width = 0.25F;
			// 
			// txtCoveredMonth6_5
			// 
			this.txtCoveredMonth6_5.Height = 0.1770833F;
			this.txtCoveredMonth6_5.Left = 7.409722F;
			this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
			this.txtCoveredMonth6_5.Text = null;
			this.txtCoveredMonth6_5.Top = 6.25F;
			this.txtCoveredMonth6_5.Width = 0.25F;
			// 
			// txtCoveredMonth7_5
			// 
			this.txtCoveredMonth7_5.Height = 0.1770833F;
			this.txtCoveredMonth7_5.Left = 7.8125F;
			this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
			this.txtCoveredMonth7_5.Text = null;
			this.txtCoveredMonth7_5.Top = 6.25F;
			this.txtCoveredMonth7_5.Width = 0.25F;
			// 
			// txtCoveredMonth8_5
			// 
			this.txtCoveredMonth8_5.Height = 0.1770833F;
			this.txtCoveredMonth8_5.Left = 8.194445F;
			this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
			this.txtCoveredMonth8_5.Text = null;
			this.txtCoveredMonth8_5.Top = 6.25F;
			this.txtCoveredMonth8_5.Width = 0.25F;
			// 
			// txtCoveredMonth9_5
			// 
			this.txtCoveredMonth9_5.Height = 0.1770833F;
			this.txtCoveredMonth9_5.Left = 8.604167F;
			this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
			this.txtCoveredMonth9_5.Text = null;
			this.txtCoveredMonth9_5.Top = 6.25F;
			this.txtCoveredMonth9_5.Width = 0.25F;
			// 
			// txtCoveredMonth10_5
			// 
			this.txtCoveredMonth10_5.Height = 0.1770833F;
			this.txtCoveredMonth10_5.Left = 9.020833F;
			this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
			this.txtCoveredMonth10_5.Text = null;
			this.txtCoveredMonth10_5.Top = 6.25F;
			this.txtCoveredMonth10_5.Width = 0.25F;
			// 
			// txtCoveredMonth11_5
			// 
			this.txtCoveredMonth11_5.Height = 0.1770833F;
			this.txtCoveredMonth11_5.Left = 9.423611F;
			this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
			this.txtCoveredMonth11_5.Text = null;
			this.txtCoveredMonth11_5.Top = 6.25F;
			this.txtCoveredMonth11_5.Width = 0.25F;
			// 
			// txtCoveredMonth12_5
			// 
			this.txtCoveredMonth12_5.Height = 0.1770833F;
			this.txtCoveredMonth12_5.Left = 9.805555F;
			this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
			this.txtCoveredMonth12_5.Text = null;
			this.txtCoveredMonth12_5.Top = 6.25F;
			this.txtCoveredMonth12_5.Width = 0.1944444F;
			// 
			// txtCoveredAll12_6
			// 
			this.txtCoveredAll12_6.Height = 0.1770833F;
			this.txtCoveredAll12_6.Left = 4.9375F;
			this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
			this.txtCoveredAll12_6.Text = null;
			this.txtCoveredAll12_6.Top = 6.729167F;
			this.txtCoveredAll12_6.Width = 0.25F;
			// 
			// txtCoveredMonth1_6
			// 
			this.txtCoveredMonth1_6.Height = 0.1770833F;
			this.txtCoveredMonth1_6.Left = 5.395833F;
			this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
			this.txtCoveredMonth1_6.Text = null;
			this.txtCoveredMonth1_6.Top = 6.729167F;
			this.txtCoveredMonth1_6.Width = 0.25F;
			// 
			// txtCoveredMonth2_6
			// 
			this.txtCoveredMonth2_6.Height = 0.1770833F;
			this.txtCoveredMonth2_6.Left = 5.805555F;
			this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
			this.txtCoveredMonth2_6.Text = null;
			this.txtCoveredMonth2_6.Top = 6.729167F;
			this.txtCoveredMonth2_6.Width = 0.25F;
			// 
			// txtCoveredMonth3_6
			// 
			this.txtCoveredMonth3_6.Height = 0.1770833F;
			this.txtCoveredMonth3_6.Left = 6.1875F;
			this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
			this.txtCoveredMonth3_6.Text = null;
			this.txtCoveredMonth3_6.Top = 6.729167F;
			this.txtCoveredMonth3_6.Width = 0.25F;
			// 
			// txtCoveredMonth4_6
			// 
			this.txtCoveredMonth4_6.Height = 0.1770833F;
			this.txtCoveredMonth4_6.Left = 6.597222F;
			this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
			this.txtCoveredMonth4_6.Text = null;
			this.txtCoveredMonth4_6.Top = 6.729167F;
			this.txtCoveredMonth4_6.Width = 0.25F;
			// 
			// txtCoveredMonth5_6
			// 
			this.txtCoveredMonth5_6.Height = 0.1770833F;
			this.txtCoveredMonth5_6.Left = 7.006945F;
			this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
			this.txtCoveredMonth5_6.Text = null;
			this.txtCoveredMonth5_6.Top = 6.729167F;
			this.txtCoveredMonth5_6.Width = 0.25F;
			// 
			// txtCoveredMonth6_6
			// 
			this.txtCoveredMonth6_6.Height = 0.1770833F;
			this.txtCoveredMonth6_6.Left = 7.409722F;
			this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
			this.txtCoveredMonth6_6.Text = null;
			this.txtCoveredMonth6_6.Top = 6.729167F;
			this.txtCoveredMonth6_6.Width = 0.25F;
			// 
			// txtCoveredMonth7_6
			// 
			this.txtCoveredMonth7_6.Height = 0.1770833F;
			this.txtCoveredMonth7_6.Left = 7.8125F;
			this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
			this.txtCoveredMonth7_6.Text = null;
			this.txtCoveredMonth7_6.Top = 6.729167F;
			this.txtCoveredMonth7_6.Width = 0.25F;
			// 
			// txtCoveredMonth8_6
			// 
			this.txtCoveredMonth8_6.Height = 0.1770833F;
			this.txtCoveredMonth8_6.Left = 8.194445F;
			this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
			this.txtCoveredMonth8_6.Text = null;
			this.txtCoveredMonth8_6.Top = 6.729167F;
			this.txtCoveredMonth8_6.Width = 0.25F;
			// 
			// txtCoveredMonth9_6
			// 
			this.txtCoveredMonth9_6.Height = 0.1770833F;
			this.txtCoveredMonth9_6.Left = 8.604167F;
			this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
			this.txtCoveredMonth9_6.Text = null;
			this.txtCoveredMonth9_6.Top = 6.729167F;
			this.txtCoveredMonth9_6.Width = 0.25F;
			// 
			// txtCoveredMonth10_6
			// 
			this.txtCoveredMonth10_6.Height = 0.1770833F;
			this.txtCoveredMonth10_6.Left = 9.020833F;
			this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
			this.txtCoveredMonth10_6.Text = null;
			this.txtCoveredMonth10_6.Top = 6.729167F;
			this.txtCoveredMonth10_6.Width = 0.25F;
			// 
			// txtCoveredMonth11_6
			// 
			this.txtCoveredMonth11_6.Height = 0.1770833F;
			this.txtCoveredMonth11_6.Left = 9.423611F;
			this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
			this.txtCoveredMonth11_6.Text = null;
			this.txtCoveredMonth11_6.Top = 6.729167F;
			this.txtCoveredMonth11_6.Width = 0.25F;
			// 
			// txtCoveredMonth12_6
			// 
			this.txtCoveredMonth12_6.Height = 0.1770833F;
			this.txtCoveredMonth12_6.Left = 9.805555F;
			this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
			this.txtCoveredMonth12_6.Text = null;
			this.txtCoveredMonth12_6.Top = 6.729167F;
			this.txtCoveredMonth12_6.Width = 0.1944444F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1770833F;
			this.txtName.Left = 0.08333334F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 1F;
			this.txtName.Width = 5.25F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1770833F;
			this.txtSSN.Left = 5.5F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 1F;
			this.txtSSN.Width = 1.916667F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1770833F;
			this.txtAddress.Left = 0.08333334F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.291667F;
			this.txtAddress.Width = 3.416667F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1770833F;
			this.txtCity.Left = 3.583333F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Text = null;
			this.txtCity.Top = 1.291667F;
			this.txtCity.Width = 1.75F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1770833F;
			this.txtState.Left = 5.5F;
			this.txtState.Name = "txtState";
			this.txtState.Text = null;
			this.txtState.Top = 1.291667F;
			this.txtState.Width = 1.916667F;
			// 
			// txtPostalCode
			// 
			this.txtPostalCode.Height = 0.1770833F;
			this.txtPostalCode.Left = 7.666667F;
			this.txtPostalCode.Name = "txtPostalCode";
			this.txtPostalCode.Text = null;
			this.txtPostalCode.Top = 1.291667F;
			this.txtPostalCode.Width = 2.083333F;
			// 
			// txtProviderName
			// 
			this.txtProviderName.Height = 0.1770833F;
			this.txtProviderName.Left = 0.08333334F;
			this.txtProviderName.Name = "txtProviderName";
			this.txtProviderName.Text = null;
			this.txtProviderName.Top = 2.916667F;
			this.txtProviderName.Width = 5.25F;
			// 
			// txtProviderAddress
			// 
			this.txtProviderAddress.Height = 0.1770833F;
			this.txtProviderAddress.Left = 0.08333334F;
			this.txtProviderAddress.Name = "txtProviderAddress";
			this.txtProviderAddress.Text = null;
			this.txtProviderAddress.Top = 3.229167F;
			this.txtProviderAddress.Width = 3.416667F;
			// 
			// txtProviderCity
			// 
			this.txtProviderCity.Height = 0.1770833F;
			this.txtProviderCity.Left = 3.583333F;
			this.txtProviderCity.Name = "txtProviderCity";
			this.txtProviderCity.Text = null;
			this.txtProviderCity.Top = 3.229167F;
			this.txtProviderCity.Width = 1.75F;
			// 
			// txtProviderState
			// 
			this.txtProviderState.Height = 0.1770833F;
			this.txtProviderState.Left = 5.5F;
			this.txtProviderState.Name = "txtProviderState";
			this.txtProviderState.Text = null;
			this.txtProviderState.Top = 3.229167F;
			this.txtProviderState.Width = 1.916667F;
			// 
			// txtProviderPostalCode
			// 
			this.txtProviderPostalCode.Height = 0.1770833F;
			this.txtProviderPostalCode.Left = 7.666667F;
			this.txtProviderPostalCode.Name = "txtProviderPostalCode";
			this.txtProviderPostalCode.Text = null;
			this.txtProviderPostalCode.Top = 3.229167F;
			this.txtProviderPostalCode.Width = 2.083333F;
			// 
			// txtProviderTelephone
			// 
			this.txtProviderTelephone.Height = 0.1770833F;
			this.txtProviderTelephone.Left = 7.666667F;
			this.txtProviderTelephone.Name = "txtProviderTelephone";
			this.txtProviderTelephone.Text = null;
			this.txtProviderTelephone.Top = 2.916667F;
			this.txtProviderTelephone.Width = 2.083333F;
			// 
			// txtOriginOfPolicy
			// 
			this.txtOriginOfPolicy.Height = 0.1770833F;
			this.txtOriginOfPolicy.Left = 5.208333F;
			this.txtOriginOfPolicy.Name = "txtOriginOfPolicy";
			this.txtOriginOfPolicy.Text = null;
			this.txtOriginOfPolicy.Top = 1.583333F;
			this.txtOriginOfPolicy.Width = 0.25F;
			// 
			// txtEIN
			// 
			this.txtEIN.Height = 0.1770833F;
			this.txtEIN.Left = 5.5F;
			this.txtEIN.Name = "txtEIN";
			this.txtEIN.Text = null;
			this.txtEIN.Top = 2.916667F;
			this.txtEIN.Width = 1.916667F;
			// 
			// rpt1095B2016Page1
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderPostalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProviderTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginOfPolicy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderPostalCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProviderTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginOfPolicy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
	}
}
