﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095C2016Page2.
	/// </summary>
	partial class rpt1095C2016Page2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095C2016Page2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCoveredName1,
				this.txtCoveredSSN1,
				this.txtCoveredDOB1,
				this.txtCoveredName2,
				this.txtCoveredSSN2,
				this.txtCoveredDOB2,
				this.txtCoveredName3,
				this.txtCoveredSSN3,
				this.txtCoveredDOB3,
				this.txtCoveredName4,
				this.txtCoveredSSN4,
				this.txtCoveredDOB4,
				this.txtCoveredName5,
				this.txtCoveredSSN5,
				this.txtCoveredDOB5,
				this.txtCoveredName6,
				this.txtCoveredSSN6,
				this.txtCoveredDOB6,
				this.txtCoveredAll12_1,
				this.txtCoveredMonth1_1,
				this.txtCoveredMonth2_1,
				this.txtCoveredMonth3_1,
				this.txtCoveredMonth4_1,
				this.txtCoveredMonth5_1,
				this.txtCoveredMonth6_1,
				this.txtCoveredMonth7_1,
				this.txtCoveredMonth8_1,
				this.txtCoveredMonth9_1,
				this.txtCoveredMonth10_1,
				this.txtCoveredMonth11_1,
				this.txtCoveredMonth12_1,
				this.txtCoveredAll12_2,
				this.txtCoveredMonth1_2,
				this.txtCoveredMonth2_2,
				this.txtCoveredMonth3_2,
				this.txtCoveredMonth4_2,
				this.txtCoveredMonth5_2,
				this.txtCoveredMonth6_2,
				this.txtCoveredMonth7_2,
				this.txtCoveredMonth8_2,
				this.txtCoveredMonth9_2,
				this.txtCoveredMonth10_2,
				this.txtCoveredMonth11_2,
				this.txtCoveredMonth12_2,
				this.txtCoveredAll12_3,
				this.txtCoveredMonth1_3,
				this.txtCoveredMonth2_3,
				this.txtCoveredMonth3_3,
				this.txtCoveredMonth4_3,
				this.txtCoveredMonth5_3,
				this.txtCoveredMonth6_3,
				this.txtCoveredMonth7_3,
				this.txtCoveredMonth8_3,
				this.txtCoveredMonth9_3,
				this.txtCoveredMonth10_3,
				this.txtCoveredMonth11_3,
				this.txtCoveredMonth12_3,
				this.txtCoveredAll12_4,
				this.txtCoveredMonth1_4,
				this.txtCoveredMonth2_4,
				this.txtCoveredMonth3_4,
				this.txtCoveredMonth4_4,
				this.txtCoveredMonth5_4,
				this.txtCoveredMonth6_4,
				this.txtCoveredMonth7_4,
				this.txtCoveredMonth8_4,
				this.txtCoveredMonth9_4,
				this.txtCoveredMonth10_4,
				this.txtCoveredMonth11_4,
				this.txtCoveredMonth12_4,
				this.txtCoveredAll12_5,
				this.txtCoveredMonth1_5,
				this.txtCoveredMonth2_5,
				this.txtCoveredMonth3_5,
				this.txtCoveredMonth4_5,
				this.txtCoveredMonth5_5,
				this.txtCoveredMonth6_5,
				this.txtCoveredMonth7_5,
				this.txtCoveredMonth8_5,
				this.txtCoveredMonth9_5,
				this.txtCoveredMonth10_5,
				this.txtCoveredMonth11_5,
				this.txtCoveredMonth12_5,
				this.txtCoveredAll12_6,
				this.txtCoveredMonth1_6,
				this.txtCoveredMonth2_6,
				this.txtCoveredMonth3_6,
				this.txtCoveredMonth4_6,
				this.txtCoveredMonth5_6,
				this.txtCoveredMonth6_6,
				this.txtCoveredMonth7_6,
				this.txtCoveredMonth8_6,
				this.txtCoveredMonth9_6,
				this.txtCoveredMonth10_6,
				this.txtCoveredMonth11_6,
				this.txtCoveredMonth12_6,
				this.txtName,
				this.txtSSN,
				this.txtCoveredName7,
				this.txtCoveredSSN7,
				this.txtCoveredDOB7,
				this.txtCoveredName8,
				this.txtCoveredSSN8,
				this.txtCoveredDOB8,
				this.txtCoveredName9,
				this.txtCoveredSSN9,
				this.txtCoveredDOB9,
				this.txtCoveredName10,
				this.txtCoveredSSN10,
				this.txtCoveredDOB10,
				this.txtCoveredName11,
				this.txtCoveredSSN11,
				this.txtCoveredDOB11,
				this.txtCoveredName12,
				this.txtCoveredSSN12,
				this.txtCoveredDOB12,
				this.txtCoveredAll12_7,
				this.txtCoveredMonth1_7,
				this.txtCoveredMonth2_7,
				this.txtCoveredMonth3_7,
				this.txtCoveredMonth4_7,
				this.txtCoveredMonth5_7,
				this.txtCoveredMonth6_7,
				this.txtCoveredMonth7_7,
				this.txtCoveredMonth8_7,
				this.txtCoveredMonth9_7,
				this.txtCoveredMonth10_7,
				this.txtCoveredMonth11_7,
				this.txtCoveredMonth12_7,
				this.txtCoveredAll12_8,
				this.txtCoveredMonth1_8,
				this.txtCoveredMonth2_8,
				this.txtCoveredMonth3_8,
				this.txtCoveredMonth4_8,
				this.txtCoveredMonth5_8,
				this.txtCoveredMonth6_8,
				this.txtCoveredMonth7_8,
				this.txtCoveredMonth8_8,
				this.txtCoveredMonth9_8,
				this.txtCoveredMonth10_8,
				this.txtCoveredMonth11_8,
				this.txtCoveredMonth12_8,
				this.txtCoveredAll12_9,
				this.txtCoveredMonth1_9,
				this.txtCoveredMonth2_9,
				this.txtCoveredMonth3_9,
				this.txtCoveredMonth4_9,
				this.txtCoveredMonth5_9,
				this.txtCoveredMonth6_9,
				this.txtCoveredMonth7_9,
				this.txtCoveredMonth8_9,
				this.txtCoveredMonth9_9,
				this.txtCoveredMonth10_9,
				this.txtCoveredMonth11_9,
				this.txtCoveredMonth12_9,
				this.txtCoveredAll12_10,
				this.txtCoveredMonth1_10,
				this.txtCoveredMonth2_10,
				this.txtCoveredMonth3_10,
				this.txtCoveredMonth4_10,
				this.txtCoveredMonth5_10,
				this.txtCoveredMonth6_10,
				this.txtCoveredMonth7_10,
				this.txtCoveredMonth8_10,
				this.txtCoveredMonth9_10,
				this.txtCoveredMonth10_10,
				this.txtCoveredMonth11_10,
				this.txtCoveredMonth12_10,
				this.txtCoveredAll12_11,
				this.txtCoveredMonth1_11,
				this.txtCoveredMonth2_11,
				this.txtCoveredMonth3_11,
				this.txtCoveredMonth4_11,
				this.txtCoveredMonth5_11,
				this.txtCoveredMonth6_11,
				this.txtCoveredMonth7_11,
				this.txtCoveredMonth8_11,
				this.txtCoveredMonth9_11,
				this.txtCoveredMonth10_11,
				this.txtCoveredMonth11_11,
				this.txtCoveredMonth12_11,
				this.txtCoveredAll12_12,
				this.txtCoveredMonth1_12,
				this.txtCoveredMonth2_12,
				this.txtCoveredMonth3_12,
				this.txtCoveredMonth4_12,
				this.txtCoveredMonth5_12,
				this.txtCoveredMonth6_12,
				this.txtCoveredMonth7_12,
				this.txtCoveredMonth8_12,
				this.txtCoveredMonth9_12,
				this.txtCoveredMonth10_12,
				this.txtCoveredMonth11_12,
				this.txtCoveredMonth12_12
			});
			this.Detail.Height = 7.114583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtCoveredName1
			// 
			this.txtCoveredName1.Height = 0.1770833F;
			this.txtCoveredName1.Left = 0.1666667F;
			this.txtCoveredName1.Name = "txtCoveredName1";
			this.txtCoveredName1.Text = null;
			this.txtCoveredName1.Top = 1.472222F;
			this.txtCoveredName1.Width = 2.166667F;
			// 
			// txtCoveredSSN1
			// 
			this.txtCoveredSSN1.Height = 0.1770833F;
			this.txtCoveredSSN1.Left = 2.625F;
			this.txtCoveredSSN1.Name = "txtCoveredSSN1";
			this.txtCoveredSSN1.Text = null;
			this.txtCoveredSSN1.Top = 1.472222F;
			this.txtCoveredSSN1.Width = 1F;
			// 
			// txtCoveredDOB1
			// 
			this.txtCoveredDOB1.Height = 0.1770833F;
			this.txtCoveredDOB1.Left = 3.791667F;
			this.txtCoveredDOB1.Name = "txtCoveredDOB1";
			this.txtCoveredDOB1.Text = null;
			this.txtCoveredDOB1.Top = 1.472222F;
			this.txtCoveredDOB1.Width = 0.9166667F;
			// 
			// txtCoveredName2
			// 
			this.txtCoveredName2.Height = 0.1770833F;
			this.txtCoveredName2.Left = 0.1666667F;
			this.txtCoveredName2.Name = "txtCoveredName2";
			this.txtCoveredName2.Text = null;
			this.txtCoveredName2.Top = 1.972222F;
			this.txtCoveredName2.Width = 2.166667F;
			// 
			// txtCoveredSSN2
			// 
			this.txtCoveredSSN2.Height = 0.1770833F;
			this.txtCoveredSSN2.Left = 2.625F;
			this.txtCoveredSSN2.Name = "txtCoveredSSN2";
			this.txtCoveredSSN2.Text = null;
			this.txtCoveredSSN2.Top = 1.972222F;
			this.txtCoveredSSN2.Width = 1F;
			// 
			// txtCoveredDOB2
			// 
			this.txtCoveredDOB2.Height = 0.1770833F;
			this.txtCoveredDOB2.Left = 3.791667F;
			this.txtCoveredDOB2.Name = "txtCoveredDOB2";
			this.txtCoveredDOB2.Text = null;
			this.txtCoveredDOB2.Top = 1.972222F;
			this.txtCoveredDOB2.Width = 0.9166667F;
			// 
			// txtCoveredName3
			// 
			this.txtCoveredName3.Height = 0.1770833F;
			this.txtCoveredName3.Left = 0.1666667F;
			this.txtCoveredName3.Name = "txtCoveredName3";
			this.txtCoveredName3.Text = null;
			this.txtCoveredName3.Top = 2.472222F;
			this.txtCoveredName3.Width = 2.166667F;
			// 
			// txtCoveredSSN3
			// 
			this.txtCoveredSSN3.Height = 0.1770833F;
			this.txtCoveredSSN3.Left = 2.625F;
			this.txtCoveredSSN3.Name = "txtCoveredSSN3";
			this.txtCoveredSSN3.Text = null;
			this.txtCoveredSSN3.Top = 2.472222F;
			this.txtCoveredSSN3.Width = 1F;
			// 
			// txtCoveredDOB3
			// 
			this.txtCoveredDOB3.Height = 0.1770833F;
			this.txtCoveredDOB3.Left = 3.791667F;
			this.txtCoveredDOB3.Name = "txtCoveredDOB3";
			this.txtCoveredDOB3.Text = null;
			this.txtCoveredDOB3.Top = 2.472222F;
			this.txtCoveredDOB3.Width = 0.9166667F;
			// 
			// txtCoveredName4
			// 
			this.txtCoveredName4.Height = 0.1770833F;
			this.txtCoveredName4.Left = 0.1666667F;
			this.txtCoveredName4.Name = "txtCoveredName4";
			this.txtCoveredName4.Text = null;
			this.txtCoveredName4.Top = 2.972222F;
			this.txtCoveredName4.Width = 2.166667F;
			// 
			// txtCoveredSSN4
			// 
			this.txtCoveredSSN4.Height = 0.1770833F;
			this.txtCoveredSSN4.Left = 2.625F;
			this.txtCoveredSSN4.Name = "txtCoveredSSN4";
			this.txtCoveredSSN4.Text = null;
			this.txtCoveredSSN4.Top = 2.972222F;
			this.txtCoveredSSN4.Width = 1F;
			// 
			// txtCoveredDOB4
			// 
			this.txtCoveredDOB4.Height = 0.1770833F;
			this.txtCoveredDOB4.Left = 3.791667F;
			this.txtCoveredDOB4.Name = "txtCoveredDOB4";
			this.txtCoveredDOB4.Text = null;
			this.txtCoveredDOB4.Top = 2.972222F;
			this.txtCoveredDOB4.Width = 0.9166667F;
			// 
			// txtCoveredName5
			// 
			this.txtCoveredName5.Height = 0.1770833F;
			this.txtCoveredName5.Left = 0.1666667F;
			this.txtCoveredName5.Name = "txtCoveredName5";
			this.txtCoveredName5.Text = null;
			this.txtCoveredName5.Top = 3.472222F;
			this.txtCoveredName5.Width = 2.166667F;
			// 
			// txtCoveredSSN5
			// 
			this.txtCoveredSSN5.Height = 0.1770833F;
			this.txtCoveredSSN5.Left = 2.625F;
			this.txtCoveredSSN5.Name = "txtCoveredSSN5";
			this.txtCoveredSSN5.Text = null;
			this.txtCoveredSSN5.Top = 3.472222F;
			this.txtCoveredSSN5.Width = 1F;
			// 
			// txtCoveredDOB5
			// 
			this.txtCoveredDOB5.Height = 0.1770833F;
			this.txtCoveredDOB5.Left = 3.791667F;
			this.txtCoveredDOB5.Name = "txtCoveredDOB5";
			this.txtCoveredDOB5.Text = null;
			this.txtCoveredDOB5.Top = 3.472222F;
			this.txtCoveredDOB5.Width = 0.9166667F;
			// 
			// txtCoveredName6
			// 
			this.txtCoveredName6.Height = 0.1770833F;
			this.txtCoveredName6.Left = 0.1666667F;
			this.txtCoveredName6.Name = "txtCoveredName6";
			this.txtCoveredName6.Text = null;
			this.txtCoveredName6.Top = 3.986111F;
			this.txtCoveredName6.Width = 2.166667F;
			// 
			// txtCoveredSSN6
			// 
			this.txtCoveredSSN6.Height = 0.1770833F;
			this.txtCoveredSSN6.Left = 2.625F;
			this.txtCoveredSSN6.Name = "txtCoveredSSN6";
			this.txtCoveredSSN6.Text = null;
			this.txtCoveredSSN6.Top = 3.986111F;
			this.txtCoveredSSN6.Width = 1F;
			// 
			// txtCoveredDOB6
			// 
			this.txtCoveredDOB6.Height = 0.1770833F;
			this.txtCoveredDOB6.Left = 3.791667F;
			this.txtCoveredDOB6.Name = "txtCoveredDOB6";
			this.txtCoveredDOB6.Text = null;
			this.txtCoveredDOB6.Top = 3.986111F;
			this.txtCoveredDOB6.Width = 0.9166667F;
			// 
			// txtCoveredAll12_1
			// 
			this.txtCoveredAll12_1.Height = 0.1770833F;
			this.txtCoveredAll12_1.Left = 4.916667F;
			this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
			this.txtCoveredAll12_1.Text = null;
			this.txtCoveredAll12_1.Top = 1.472222F;
			this.txtCoveredAll12_1.Width = 0.25F;
			// 
			// txtCoveredMonth1_1
			// 
			this.txtCoveredMonth1_1.Height = 0.1770833F;
			this.txtCoveredMonth1_1.Left = 5.354167F;
			this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
			this.txtCoveredMonth1_1.Text = null;
			this.txtCoveredMonth1_1.Top = 1.472222F;
			this.txtCoveredMonth1_1.Width = 0.25F;
			// 
			// txtCoveredMonth2_1
			// 
			this.txtCoveredMonth2_1.Height = 0.1770833F;
			this.txtCoveredMonth2_1.Left = 5.75F;
			this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
			this.txtCoveredMonth2_1.Text = null;
			this.txtCoveredMonth2_1.Top = 1.472222F;
			this.txtCoveredMonth2_1.Width = 0.25F;
			// 
			// txtCoveredMonth3_1
			// 
			this.txtCoveredMonth3_1.Height = 0.1770833F;
			this.txtCoveredMonth3_1.Left = 6.145833F;
			this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
			this.txtCoveredMonth3_1.Text = null;
			this.txtCoveredMonth3_1.Top = 1.472222F;
			this.txtCoveredMonth3_1.Width = 0.25F;
			// 
			// txtCoveredMonth4_1
			// 
			this.txtCoveredMonth4_1.Height = 0.1770833F;
			this.txtCoveredMonth4_1.Left = 6.5625F;
			this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
			this.txtCoveredMonth4_1.Text = null;
			this.txtCoveredMonth4_1.Top = 1.472222F;
			this.txtCoveredMonth4_1.Width = 0.25F;
			// 
			// txtCoveredMonth5_1
			// 
			this.txtCoveredMonth5_1.Height = 0.1770833F;
			this.txtCoveredMonth5_1.Left = 6.951389F;
			this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
			this.txtCoveredMonth5_1.Text = null;
			this.txtCoveredMonth5_1.Top = 1.472222F;
			this.txtCoveredMonth5_1.Width = 0.25F;
			// 
			// txtCoveredMonth6_1
			// 
			this.txtCoveredMonth6_1.Height = 0.1770833F;
			this.txtCoveredMonth6_1.Left = 7.347222F;
			this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
			this.txtCoveredMonth6_1.Text = null;
			this.txtCoveredMonth6_1.Top = 1.472222F;
			this.txtCoveredMonth6_1.Width = 0.25F;
			// 
			// txtCoveredMonth7_1
			// 
			this.txtCoveredMonth7_1.Height = 0.1770833F;
			this.txtCoveredMonth7_1.Left = 7.763889F;
			this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
			this.txtCoveredMonth7_1.Text = null;
			this.txtCoveredMonth7_1.Top = 1.472222F;
			this.txtCoveredMonth7_1.Width = 0.25F;
			// 
			// txtCoveredMonth8_1
			// 
			this.txtCoveredMonth8_1.Height = 0.1770833F;
			this.txtCoveredMonth8_1.Left = 8.138889F;
			this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
			this.txtCoveredMonth8_1.Text = null;
			this.txtCoveredMonth8_1.Top = 1.472222F;
			this.txtCoveredMonth8_1.Width = 0.25F;
			// 
			// txtCoveredMonth9_1
			// 
			this.txtCoveredMonth9_1.Height = 0.1770833F;
			this.txtCoveredMonth9_1.Left = 8.548611F;
			this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
			this.txtCoveredMonth9_1.Text = null;
			this.txtCoveredMonth9_1.Top = 1.472222F;
			this.txtCoveredMonth9_1.Width = 0.25F;
			// 
			// txtCoveredMonth10_1
			// 
			this.txtCoveredMonth10_1.Height = 0.1770833F;
			this.txtCoveredMonth10_1.Left = 8.951389F;
			this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
			this.txtCoveredMonth10_1.Text = null;
			this.txtCoveredMonth10_1.Top = 1.472222F;
			this.txtCoveredMonth10_1.Width = 0.25F;
			// 
			// txtCoveredMonth11_1
			// 
			this.txtCoveredMonth11_1.Height = 0.1770833F;
			this.txtCoveredMonth11_1.Left = 9.354167F;
			this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
			this.txtCoveredMonth11_1.Text = null;
			this.txtCoveredMonth11_1.Top = 1.472222F;
			this.txtCoveredMonth11_1.Width = 0.25F;
			// 
			// txtCoveredMonth12_1
			// 
			this.txtCoveredMonth12_1.Height = 0.1770833F;
			this.txtCoveredMonth12_1.Left = 9.756945F;
			this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
			this.txtCoveredMonth12_1.Text = null;
			this.txtCoveredMonth12_1.Top = 1.472222F;
			this.txtCoveredMonth12_1.Width = 0.1944444F;
			// 
			// txtCoveredAll12_2
			// 
			this.txtCoveredAll12_2.Height = 0.1770833F;
			this.txtCoveredAll12_2.Left = 4.916667F;
			this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
			this.txtCoveredAll12_2.Text = null;
			this.txtCoveredAll12_2.Top = 1.972222F;
			this.txtCoveredAll12_2.Width = 0.25F;
			// 
			// txtCoveredMonth1_2
			// 
			this.txtCoveredMonth1_2.Height = 0.1770833F;
			this.txtCoveredMonth1_2.Left = 5.354167F;
			this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
			this.txtCoveredMonth1_2.Text = null;
			this.txtCoveredMonth1_2.Top = 1.972222F;
			this.txtCoveredMonth1_2.Width = 0.25F;
			// 
			// txtCoveredMonth2_2
			// 
			this.txtCoveredMonth2_2.Height = 0.1770833F;
			this.txtCoveredMonth2_2.Left = 5.75F;
			this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
			this.txtCoveredMonth2_2.Text = null;
			this.txtCoveredMonth2_2.Top = 1.972222F;
			this.txtCoveredMonth2_2.Width = 0.25F;
			// 
			// txtCoveredMonth3_2
			// 
			this.txtCoveredMonth3_2.Height = 0.1770833F;
			this.txtCoveredMonth3_2.Left = 6.145833F;
			this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
			this.txtCoveredMonth3_2.Text = null;
			this.txtCoveredMonth3_2.Top = 1.972222F;
			this.txtCoveredMonth3_2.Width = 0.25F;
			// 
			// txtCoveredMonth4_2
			// 
			this.txtCoveredMonth4_2.Height = 0.1770833F;
			this.txtCoveredMonth4_2.Left = 6.5625F;
			this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
			this.txtCoveredMonth4_2.Text = null;
			this.txtCoveredMonth4_2.Top = 1.972222F;
			this.txtCoveredMonth4_2.Width = 0.25F;
			// 
			// txtCoveredMonth5_2
			// 
			this.txtCoveredMonth5_2.Height = 0.1770833F;
			this.txtCoveredMonth5_2.Left = 6.951389F;
			this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
			this.txtCoveredMonth5_2.Text = null;
			this.txtCoveredMonth5_2.Top = 1.972222F;
			this.txtCoveredMonth5_2.Width = 0.25F;
			// 
			// txtCoveredMonth6_2
			// 
			this.txtCoveredMonth6_2.Height = 0.1770833F;
			this.txtCoveredMonth6_2.Left = 7.347222F;
			this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
			this.txtCoveredMonth6_2.Text = null;
			this.txtCoveredMonth6_2.Top = 1.972222F;
			this.txtCoveredMonth6_2.Width = 0.25F;
			// 
			// txtCoveredMonth7_2
			// 
			this.txtCoveredMonth7_2.Height = 0.1770833F;
			this.txtCoveredMonth7_2.Left = 7.763889F;
			this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
			this.txtCoveredMonth7_2.Text = null;
			this.txtCoveredMonth7_2.Top = 1.972222F;
			this.txtCoveredMonth7_2.Width = 0.25F;
			// 
			// txtCoveredMonth8_2
			// 
			this.txtCoveredMonth8_2.Height = 0.1770833F;
			this.txtCoveredMonth8_2.Left = 8.138889F;
			this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
			this.txtCoveredMonth8_2.Text = null;
			this.txtCoveredMonth8_2.Top = 1.972222F;
			this.txtCoveredMonth8_2.Width = 0.25F;
			// 
			// txtCoveredMonth9_2
			// 
			this.txtCoveredMonth9_2.Height = 0.1770833F;
			this.txtCoveredMonth9_2.Left = 8.548611F;
			this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
			this.txtCoveredMonth9_2.Text = null;
			this.txtCoveredMonth9_2.Top = 1.972222F;
			this.txtCoveredMonth9_2.Width = 0.25F;
			// 
			// txtCoveredMonth10_2
			// 
			this.txtCoveredMonth10_2.Height = 0.1770833F;
			this.txtCoveredMonth10_2.Left = 8.951389F;
			this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
			this.txtCoveredMonth10_2.Text = null;
			this.txtCoveredMonth10_2.Top = 1.972222F;
			this.txtCoveredMonth10_2.Width = 0.25F;
			// 
			// txtCoveredMonth11_2
			// 
			this.txtCoveredMonth11_2.Height = 0.1770833F;
			this.txtCoveredMonth11_2.Left = 9.354167F;
			this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
			this.txtCoveredMonth11_2.Text = null;
			this.txtCoveredMonth11_2.Top = 1.972222F;
			this.txtCoveredMonth11_2.Width = 0.25F;
			// 
			// txtCoveredMonth12_2
			// 
			this.txtCoveredMonth12_2.Height = 0.1770833F;
			this.txtCoveredMonth12_2.Left = 9.756945F;
			this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
			this.txtCoveredMonth12_2.Text = null;
			this.txtCoveredMonth12_2.Top = 1.972222F;
			this.txtCoveredMonth12_2.Width = 0.1944444F;
			// 
			// txtCoveredAll12_3
			// 
			this.txtCoveredAll12_3.Height = 0.1770833F;
			this.txtCoveredAll12_3.Left = 4.916667F;
			this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
			this.txtCoveredAll12_3.Text = null;
			this.txtCoveredAll12_3.Top = 2.472222F;
			this.txtCoveredAll12_3.Width = 0.25F;
			// 
			// txtCoveredMonth1_3
			// 
			this.txtCoveredMonth1_3.Height = 0.1770833F;
			this.txtCoveredMonth1_3.Left = 5.354167F;
			this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
			this.txtCoveredMonth1_3.Text = null;
			this.txtCoveredMonth1_3.Top = 2.472222F;
			this.txtCoveredMonth1_3.Width = 0.25F;
			// 
			// txtCoveredMonth2_3
			// 
			this.txtCoveredMonth2_3.Height = 0.1770833F;
			this.txtCoveredMonth2_3.Left = 5.75F;
			this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
			this.txtCoveredMonth2_3.Text = null;
			this.txtCoveredMonth2_3.Top = 2.472222F;
			this.txtCoveredMonth2_3.Width = 0.25F;
			// 
			// txtCoveredMonth3_3
			// 
			this.txtCoveredMonth3_3.Height = 0.1770833F;
			this.txtCoveredMonth3_3.Left = 6.145833F;
			this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
			this.txtCoveredMonth3_3.Text = null;
			this.txtCoveredMonth3_3.Top = 2.472222F;
			this.txtCoveredMonth3_3.Width = 0.25F;
			// 
			// txtCoveredMonth4_3
			// 
			this.txtCoveredMonth4_3.Height = 0.1770833F;
			this.txtCoveredMonth4_3.Left = 6.5625F;
			this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
			this.txtCoveredMonth4_3.Text = null;
			this.txtCoveredMonth4_3.Top = 2.472222F;
			this.txtCoveredMonth4_3.Width = 0.25F;
			// 
			// txtCoveredMonth5_3
			// 
			this.txtCoveredMonth5_3.Height = 0.1770833F;
			this.txtCoveredMonth5_3.Left = 6.951389F;
			this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
			this.txtCoveredMonth5_3.Text = null;
			this.txtCoveredMonth5_3.Top = 2.472222F;
			this.txtCoveredMonth5_3.Width = 0.25F;
			// 
			// txtCoveredMonth6_3
			// 
			this.txtCoveredMonth6_3.Height = 0.1770833F;
			this.txtCoveredMonth6_3.Left = 7.347222F;
			this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
			this.txtCoveredMonth6_3.Text = null;
			this.txtCoveredMonth6_3.Top = 2.472222F;
			this.txtCoveredMonth6_3.Width = 0.25F;
			// 
			// txtCoveredMonth7_3
			// 
			this.txtCoveredMonth7_3.Height = 0.1770833F;
			this.txtCoveredMonth7_3.Left = 7.763889F;
			this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
			this.txtCoveredMonth7_3.Text = null;
			this.txtCoveredMonth7_3.Top = 2.472222F;
			this.txtCoveredMonth7_3.Width = 0.25F;
			// 
			// txtCoveredMonth8_3
			// 
			this.txtCoveredMonth8_3.Height = 0.1770833F;
			this.txtCoveredMonth8_3.Left = 8.138889F;
			this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
			this.txtCoveredMonth8_3.Text = null;
			this.txtCoveredMonth8_3.Top = 2.472222F;
			this.txtCoveredMonth8_3.Width = 0.25F;
			// 
			// txtCoveredMonth9_3
			// 
			this.txtCoveredMonth9_3.Height = 0.1770833F;
			this.txtCoveredMonth9_3.Left = 8.548611F;
			this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
			this.txtCoveredMonth9_3.Text = null;
			this.txtCoveredMonth9_3.Top = 2.472222F;
			this.txtCoveredMonth9_3.Width = 0.25F;
			// 
			// txtCoveredMonth10_3
			// 
			this.txtCoveredMonth10_3.Height = 0.1770833F;
			this.txtCoveredMonth10_3.Left = 8.951389F;
			this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
			this.txtCoveredMonth10_3.Text = null;
			this.txtCoveredMonth10_3.Top = 2.472222F;
			this.txtCoveredMonth10_3.Width = 0.25F;
			// 
			// txtCoveredMonth11_3
			// 
			this.txtCoveredMonth11_3.Height = 0.1770833F;
			this.txtCoveredMonth11_3.Left = 9.354167F;
			this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
			this.txtCoveredMonth11_3.Text = null;
			this.txtCoveredMonth11_3.Top = 2.472222F;
			this.txtCoveredMonth11_3.Width = 0.25F;
			// 
			// txtCoveredMonth12_3
			// 
			this.txtCoveredMonth12_3.Height = 0.1770833F;
			this.txtCoveredMonth12_3.Left = 9.756945F;
			this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
			this.txtCoveredMonth12_3.Text = null;
			this.txtCoveredMonth12_3.Top = 2.472222F;
			this.txtCoveredMonth12_3.Width = 0.1944444F;
			// 
			// txtCoveredAll12_4
			// 
			this.txtCoveredAll12_4.Height = 0.1770833F;
			this.txtCoveredAll12_4.Left = 4.916667F;
			this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
			this.txtCoveredAll12_4.Text = null;
			this.txtCoveredAll12_4.Top = 2.972222F;
			this.txtCoveredAll12_4.Width = 0.25F;
			// 
			// txtCoveredMonth1_4
			// 
			this.txtCoveredMonth1_4.Height = 0.1770833F;
			this.txtCoveredMonth1_4.Left = 5.354167F;
			this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
			this.txtCoveredMonth1_4.Text = null;
			this.txtCoveredMonth1_4.Top = 2.972222F;
			this.txtCoveredMonth1_4.Width = 0.25F;
			// 
			// txtCoveredMonth2_4
			// 
			this.txtCoveredMonth2_4.Height = 0.1770833F;
			this.txtCoveredMonth2_4.Left = 5.75F;
			this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
			this.txtCoveredMonth2_4.Text = null;
			this.txtCoveredMonth2_4.Top = 2.972222F;
			this.txtCoveredMonth2_4.Width = 0.25F;
			// 
			// txtCoveredMonth3_4
			// 
			this.txtCoveredMonth3_4.Height = 0.1770833F;
			this.txtCoveredMonth3_4.Left = 6.145833F;
			this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
			this.txtCoveredMonth3_4.Text = null;
			this.txtCoveredMonth3_4.Top = 2.972222F;
			this.txtCoveredMonth3_4.Width = 0.25F;
			// 
			// txtCoveredMonth4_4
			// 
			this.txtCoveredMonth4_4.Height = 0.1770833F;
			this.txtCoveredMonth4_4.Left = 6.5625F;
			this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
			this.txtCoveredMonth4_4.Text = null;
			this.txtCoveredMonth4_4.Top = 2.972222F;
			this.txtCoveredMonth4_4.Width = 0.25F;
			// 
			// txtCoveredMonth5_4
			// 
			this.txtCoveredMonth5_4.Height = 0.1770833F;
			this.txtCoveredMonth5_4.Left = 6.951389F;
			this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
			this.txtCoveredMonth5_4.Text = null;
			this.txtCoveredMonth5_4.Top = 2.972222F;
			this.txtCoveredMonth5_4.Width = 0.25F;
			// 
			// txtCoveredMonth6_4
			// 
			this.txtCoveredMonth6_4.Height = 0.1770833F;
			this.txtCoveredMonth6_4.Left = 7.347222F;
			this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
			this.txtCoveredMonth6_4.Text = null;
			this.txtCoveredMonth6_4.Top = 2.972222F;
			this.txtCoveredMonth6_4.Width = 0.25F;
			// 
			// txtCoveredMonth7_4
			// 
			this.txtCoveredMonth7_4.Height = 0.1770833F;
			this.txtCoveredMonth7_4.Left = 7.763889F;
			this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
			this.txtCoveredMonth7_4.Text = null;
			this.txtCoveredMonth7_4.Top = 2.972222F;
			this.txtCoveredMonth7_4.Width = 0.25F;
			// 
			// txtCoveredMonth8_4
			// 
			this.txtCoveredMonth8_4.Height = 0.1770833F;
			this.txtCoveredMonth8_4.Left = 8.138889F;
			this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
			this.txtCoveredMonth8_4.Text = null;
			this.txtCoveredMonth8_4.Top = 2.972222F;
			this.txtCoveredMonth8_4.Width = 0.25F;
			// 
			// txtCoveredMonth9_4
			// 
			this.txtCoveredMonth9_4.Height = 0.1770833F;
			this.txtCoveredMonth9_4.Left = 8.548611F;
			this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
			this.txtCoveredMonth9_4.Text = null;
			this.txtCoveredMonth9_4.Top = 2.972222F;
			this.txtCoveredMonth9_4.Width = 0.25F;
			// 
			// txtCoveredMonth10_4
			// 
			this.txtCoveredMonth10_4.Height = 0.1770833F;
			this.txtCoveredMonth10_4.Left = 8.951389F;
			this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
			this.txtCoveredMonth10_4.Text = null;
			this.txtCoveredMonth10_4.Top = 2.972222F;
			this.txtCoveredMonth10_4.Width = 0.25F;
			// 
			// txtCoveredMonth11_4
			// 
			this.txtCoveredMonth11_4.Height = 0.1770833F;
			this.txtCoveredMonth11_4.Left = 9.354167F;
			this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
			this.txtCoveredMonth11_4.Text = null;
			this.txtCoveredMonth11_4.Top = 2.972222F;
			this.txtCoveredMonth11_4.Width = 0.25F;
			// 
			// txtCoveredMonth12_4
			// 
			this.txtCoveredMonth12_4.Height = 0.1770833F;
			this.txtCoveredMonth12_4.Left = 9.756945F;
			this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
			this.txtCoveredMonth12_4.Text = null;
			this.txtCoveredMonth12_4.Top = 2.972222F;
			this.txtCoveredMonth12_4.Width = 0.1944444F;
			// 
			// txtCoveredAll12_5
			// 
			this.txtCoveredAll12_5.Height = 0.1770833F;
			this.txtCoveredAll12_5.Left = 4.916667F;
			this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
			this.txtCoveredAll12_5.Text = null;
			this.txtCoveredAll12_5.Top = 3.472222F;
			this.txtCoveredAll12_5.Width = 0.25F;
			// 
			// txtCoveredMonth1_5
			// 
			this.txtCoveredMonth1_5.Height = 0.1770833F;
			this.txtCoveredMonth1_5.Left = 5.354167F;
			this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
			this.txtCoveredMonth1_5.Text = null;
			this.txtCoveredMonth1_5.Top = 3.472222F;
			this.txtCoveredMonth1_5.Width = 0.25F;
			// 
			// txtCoveredMonth2_5
			// 
			this.txtCoveredMonth2_5.Height = 0.1770833F;
			this.txtCoveredMonth2_5.Left = 5.75F;
			this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
			this.txtCoveredMonth2_5.Text = null;
			this.txtCoveredMonth2_5.Top = 3.472222F;
			this.txtCoveredMonth2_5.Width = 0.25F;
			// 
			// txtCoveredMonth3_5
			// 
			this.txtCoveredMonth3_5.Height = 0.1770833F;
			this.txtCoveredMonth3_5.Left = 6.145833F;
			this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
			this.txtCoveredMonth3_5.Text = null;
			this.txtCoveredMonth3_5.Top = 3.472222F;
			this.txtCoveredMonth3_5.Width = 0.25F;
			// 
			// txtCoveredMonth4_5
			// 
			this.txtCoveredMonth4_5.Height = 0.1770833F;
			this.txtCoveredMonth4_5.Left = 6.5625F;
			this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
			this.txtCoveredMonth4_5.Text = null;
			this.txtCoveredMonth4_5.Top = 3.472222F;
			this.txtCoveredMonth4_5.Width = 0.25F;
			// 
			// txtCoveredMonth5_5
			// 
			this.txtCoveredMonth5_5.Height = 0.1770833F;
			this.txtCoveredMonth5_5.Left = 6.951389F;
			this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
			this.txtCoveredMonth5_5.Text = null;
			this.txtCoveredMonth5_5.Top = 3.472222F;
			this.txtCoveredMonth5_5.Width = 0.25F;
			// 
			// txtCoveredMonth6_5
			// 
			this.txtCoveredMonth6_5.Height = 0.1770833F;
			this.txtCoveredMonth6_5.Left = 7.347222F;
			this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
			this.txtCoveredMonth6_5.Text = null;
			this.txtCoveredMonth6_5.Top = 3.472222F;
			this.txtCoveredMonth6_5.Width = 0.25F;
			// 
			// txtCoveredMonth7_5
			// 
			this.txtCoveredMonth7_5.Height = 0.1770833F;
			this.txtCoveredMonth7_5.Left = 7.763889F;
			this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
			this.txtCoveredMonth7_5.Text = null;
			this.txtCoveredMonth7_5.Top = 3.472222F;
			this.txtCoveredMonth7_5.Width = 0.25F;
			// 
			// txtCoveredMonth8_5
			// 
			this.txtCoveredMonth8_5.Height = 0.1770833F;
			this.txtCoveredMonth8_5.Left = 8.138889F;
			this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
			this.txtCoveredMonth8_5.Text = null;
			this.txtCoveredMonth8_5.Top = 3.472222F;
			this.txtCoveredMonth8_5.Width = 0.25F;
			// 
			// txtCoveredMonth9_5
			// 
			this.txtCoveredMonth9_5.Height = 0.1770833F;
			this.txtCoveredMonth9_5.Left = 8.548611F;
			this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
			this.txtCoveredMonth9_5.Text = null;
			this.txtCoveredMonth9_5.Top = 3.472222F;
			this.txtCoveredMonth9_5.Width = 0.25F;
			// 
			// txtCoveredMonth10_5
			// 
			this.txtCoveredMonth10_5.Height = 0.1770833F;
			this.txtCoveredMonth10_5.Left = 8.951389F;
			this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
			this.txtCoveredMonth10_5.Text = null;
			this.txtCoveredMonth10_5.Top = 3.472222F;
			this.txtCoveredMonth10_5.Width = 0.25F;
			// 
			// txtCoveredMonth11_5
			// 
			this.txtCoveredMonth11_5.Height = 0.1770833F;
			this.txtCoveredMonth11_5.Left = 9.354167F;
			this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
			this.txtCoveredMonth11_5.Text = null;
			this.txtCoveredMonth11_5.Top = 3.472222F;
			this.txtCoveredMonth11_5.Width = 0.25F;
			// 
			// txtCoveredMonth12_5
			// 
			this.txtCoveredMonth12_5.Height = 0.1770833F;
			this.txtCoveredMonth12_5.Left = 9.756945F;
			this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
			this.txtCoveredMonth12_5.Text = null;
			this.txtCoveredMonth12_5.Top = 3.472222F;
			this.txtCoveredMonth12_5.Width = 0.1944444F;
			// 
			// txtCoveredAll12_6
			// 
			this.txtCoveredAll12_6.Height = 0.1770833F;
			this.txtCoveredAll12_6.Left = 4.916667F;
			this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
			this.txtCoveredAll12_6.Text = null;
			this.txtCoveredAll12_6.Top = 3.986111F;
			this.txtCoveredAll12_6.Width = 0.25F;
			// 
			// txtCoveredMonth1_6
			// 
			this.txtCoveredMonth1_6.Height = 0.1770833F;
			this.txtCoveredMonth1_6.Left = 5.354167F;
			this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
			this.txtCoveredMonth1_6.Text = null;
			this.txtCoveredMonth1_6.Top = 3.986111F;
			this.txtCoveredMonth1_6.Width = 0.25F;
			// 
			// txtCoveredMonth2_6
			// 
			this.txtCoveredMonth2_6.Height = 0.1770833F;
			this.txtCoveredMonth2_6.Left = 5.75F;
			this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
			this.txtCoveredMonth2_6.Text = null;
			this.txtCoveredMonth2_6.Top = 3.986111F;
			this.txtCoveredMonth2_6.Width = 0.25F;
			// 
			// txtCoveredMonth3_6
			// 
			this.txtCoveredMonth3_6.Height = 0.1770833F;
			this.txtCoveredMonth3_6.Left = 6.145833F;
			this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
			this.txtCoveredMonth3_6.Text = null;
			this.txtCoveredMonth3_6.Top = 3.986111F;
			this.txtCoveredMonth3_6.Width = 0.25F;
			// 
			// txtCoveredMonth4_6
			// 
			this.txtCoveredMonth4_6.Height = 0.1770833F;
			this.txtCoveredMonth4_6.Left = 6.5625F;
			this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
			this.txtCoveredMonth4_6.Text = null;
			this.txtCoveredMonth4_6.Top = 3.986111F;
			this.txtCoveredMonth4_6.Width = 0.25F;
			// 
			// txtCoveredMonth5_6
			// 
			this.txtCoveredMonth5_6.Height = 0.1770833F;
			this.txtCoveredMonth5_6.Left = 6.951389F;
			this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
			this.txtCoveredMonth5_6.Text = null;
			this.txtCoveredMonth5_6.Top = 3.986111F;
			this.txtCoveredMonth5_6.Width = 0.25F;
			// 
			// txtCoveredMonth6_6
			// 
			this.txtCoveredMonth6_6.Height = 0.1770833F;
			this.txtCoveredMonth6_6.Left = 7.347222F;
			this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
			this.txtCoveredMonth6_6.Text = null;
			this.txtCoveredMonth6_6.Top = 3.986111F;
			this.txtCoveredMonth6_6.Width = 0.25F;
			// 
			// txtCoveredMonth7_6
			// 
			this.txtCoveredMonth7_6.Height = 0.1770833F;
			this.txtCoveredMonth7_6.Left = 7.763889F;
			this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
			this.txtCoveredMonth7_6.Text = null;
			this.txtCoveredMonth7_6.Top = 3.986111F;
			this.txtCoveredMonth7_6.Width = 0.25F;
			// 
			// txtCoveredMonth8_6
			// 
			this.txtCoveredMonth8_6.Height = 0.1770833F;
			this.txtCoveredMonth8_6.Left = 8.138889F;
			this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
			this.txtCoveredMonth8_6.Text = null;
			this.txtCoveredMonth8_6.Top = 3.986111F;
			this.txtCoveredMonth8_6.Width = 0.25F;
			// 
			// txtCoveredMonth9_6
			// 
			this.txtCoveredMonth9_6.Height = 0.1770833F;
			this.txtCoveredMonth9_6.Left = 8.548611F;
			this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
			this.txtCoveredMonth9_6.Text = null;
			this.txtCoveredMonth9_6.Top = 3.986111F;
			this.txtCoveredMonth9_6.Width = 0.25F;
			// 
			// txtCoveredMonth10_6
			// 
			this.txtCoveredMonth10_6.Height = 0.1770833F;
			this.txtCoveredMonth10_6.Left = 8.951389F;
			this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
			this.txtCoveredMonth10_6.Text = null;
			this.txtCoveredMonth10_6.Top = 3.986111F;
			this.txtCoveredMonth10_6.Width = 0.25F;
			// 
			// txtCoveredMonth11_6
			// 
			this.txtCoveredMonth11_6.Height = 0.1770833F;
			this.txtCoveredMonth11_6.Left = 9.354167F;
			this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
			this.txtCoveredMonth11_6.Text = null;
			this.txtCoveredMonth11_6.Top = 3.986111F;
			this.txtCoveredMonth11_6.Width = 0.25F;
			// 
			// txtCoveredMonth12_6
			// 
			this.txtCoveredMonth12_6.Height = 0.1770833F;
			this.txtCoveredMonth12_6.Left = 9.756945F;
			this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
			this.txtCoveredMonth12_6.Text = null;
			this.txtCoveredMonth12_6.Top = 3.986111F;
			this.txtCoveredMonth12_6.Width = 0.1944444F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1770833F;
			this.txtName.Left = 0.1666667F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0.4722222F;
			this.txtName.Width = 6.791667F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1770833F;
			this.txtSSN.Left = 7.75F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0.4722222F;
			this.txtSSN.Width = 1.729167F;
			// 
			// txtCoveredName7
			// 
			this.txtCoveredName7.Height = 0.1770833F;
			this.txtCoveredName7.Left = 0.1666667F;
			this.txtCoveredName7.Name = "txtCoveredName7";
			this.txtCoveredName7.Text = null;
			this.txtCoveredName7.Top = 4.472222F;
			this.txtCoveredName7.Width = 2.166667F;
			// 
			// txtCoveredSSN7
			// 
			this.txtCoveredSSN7.Height = 0.1770833F;
			this.txtCoveredSSN7.Left = 2.625F;
			this.txtCoveredSSN7.Name = "txtCoveredSSN7";
			this.txtCoveredSSN7.Text = null;
			this.txtCoveredSSN7.Top = 4.472222F;
			this.txtCoveredSSN7.Width = 1F;
			// 
			// txtCoveredDOB7
			// 
			this.txtCoveredDOB7.Height = 0.1770833F;
			this.txtCoveredDOB7.Left = 3.791667F;
			this.txtCoveredDOB7.Name = "txtCoveredDOB7";
			this.txtCoveredDOB7.Text = null;
			this.txtCoveredDOB7.Top = 4.472222F;
			this.txtCoveredDOB7.Width = 0.9166667F;
			// 
			// txtCoveredName8
			// 
			this.txtCoveredName8.Height = 0.1770833F;
			this.txtCoveredName8.Left = 0.1666667F;
			this.txtCoveredName8.Name = "txtCoveredName8";
			this.txtCoveredName8.Text = null;
			this.txtCoveredName8.Top = 4.972222F;
			this.txtCoveredName8.Width = 2.166667F;
			// 
			// txtCoveredSSN8
			// 
			this.txtCoveredSSN8.Height = 0.1770833F;
			this.txtCoveredSSN8.Left = 2.625F;
			this.txtCoveredSSN8.Name = "txtCoveredSSN8";
			this.txtCoveredSSN8.Text = null;
			this.txtCoveredSSN8.Top = 4.972222F;
			this.txtCoveredSSN8.Width = 1F;
			// 
			// txtCoveredDOB8
			// 
			this.txtCoveredDOB8.Height = 0.1770833F;
			this.txtCoveredDOB8.Left = 3.791667F;
			this.txtCoveredDOB8.Name = "txtCoveredDOB8";
			this.txtCoveredDOB8.Text = null;
			this.txtCoveredDOB8.Top = 4.972222F;
			this.txtCoveredDOB8.Width = 0.9166667F;
			// 
			// txtCoveredName9
			// 
			this.txtCoveredName9.Height = 0.1770833F;
			this.txtCoveredName9.Left = 0.1666667F;
			this.txtCoveredName9.Name = "txtCoveredName9";
			this.txtCoveredName9.Text = null;
			this.txtCoveredName9.Top = 5.472222F;
			this.txtCoveredName9.Width = 2.166667F;
			// 
			// txtCoveredSSN9
			// 
			this.txtCoveredSSN9.Height = 0.1770833F;
			this.txtCoveredSSN9.Left = 2.625F;
			this.txtCoveredSSN9.Name = "txtCoveredSSN9";
			this.txtCoveredSSN9.Text = null;
			this.txtCoveredSSN9.Top = 5.472222F;
			this.txtCoveredSSN9.Width = 1F;
			// 
			// txtCoveredDOB9
			// 
			this.txtCoveredDOB9.Height = 0.1770833F;
			this.txtCoveredDOB9.Left = 3.791667F;
			this.txtCoveredDOB9.Name = "txtCoveredDOB9";
			this.txtCoveredDOB9.Text = null;
			this.txtCoveredDOB9.Top = 5.472222F;
			this.txtCoveredDOB9.Width = 0.9166667F;
			// 
			// txtCoveredName10
			// 
			this.txtCoveredName10.Height = 0.1770833F;
			this.txtCoveredName10.Left = 0.1666667F;
			this.txtCoveredName10.Name = "txtCoveredName10";
			this.txtCoveredName10.Text = null;
			this.txtCoveredName10.Top = 5.972222F;
			this.txtCoveredName10.Width = 2.166667F;
			// 
			// txtCoveredSSN10
			// 
			this.txtCoveredSSN10.Height = 0.1770833F;
			this.txtCoveredSSN10.Left = 2.625F;
			this.txtCoveredSSN10.Name = "txtCoveredSSN10";
			this.txtCoveredSSN10.Text = null;
			this.txtCoveredSSN10.Top = 5.972222F;
			this.txtCoveredSSN10.Width = 1F;
			// 
			// txtCoveredDOB10
			// 
			this.txtCoveredDOB10.Height = 0.1770833F;
			this.txtCoveredDOB10.Left = 3.791667F;
			this.txtCoveredDOB10.Name = "txtCoveredDOB10";
			this.txtCoveredDOB10.Text = null;
			this.txtCoveredDOB10.Top = 5.972222F;
			this.txtCoveredDOB10.Width = 0.9166667F;
			// 
			// txtCoveredName11
			// 
			this.txtCoveredName11.Height = 0.1770833F;
			this.txtCoveredName11.Left = 0.1666667F;
			this.txtCoveredName11.Name = "txtCoveredName11";
			this.txtCoveredName11.Text = null;
			this.txtCoveredName11.Top = 6.472222F;
			this.txtCoveredName11.Width = 2.166667F;
			// 
			// txtCoveredSSN11
			// 
			this.txtCoveredSSN11.Height = 0.1770833F;
			this.txtCoveredSSN11.Left = 2.625F;
			this.txtCoveredSSN11.Name = "txtCoveredSSN11";
			this.txtCoveredSSN11.Text = null;
			this.txtCoveredSSN11.Top = 6.472222F;
			this.txtCoveredSSN11.Width = 1F;
			// 
			// txtCoveredDOB11
			// 
			this.txtCoveredDOB11.Height = 0.1770833F;
			this.txtCoveredDOB11.Left = 3.791667F;
			this.txtCoveredDOB11.Name = "txtCoveredDOB11";
			this.txtCoveredDOB11.Text = null;
			this.txtCoveredDOB11.Top = 6.472222F;
			this.txtCoveredDOB11.Width = 0.9166667F;
			// 
			// txtCoveredName12
			// 
			this.txtCoveredName12.Height = 0.1770833F;
			this.txtCoveredName12.Left = 0.1666667F;
			this.txtCoveredName12.Name = "txtCoveredName12";
			this.txtCoveredName12.Text = null;
			this.txtCoveredName12.Top = 6.972222F;
			this.txtCoveredName12.Width = 2.166667F;
			// 
			// txtCoveredSSN12
			// 
			this.txtCoveredSSN12.Height = 0.1770833F;
			this.txtCoveredSSN12.Left = 2.625F;
			this.txtCoveredSSN12.Name = "txtCoveredSSN12";
			this.txtCoveredSSN12.Text = null;
			this.txtCoveredSSN12.Top = 6.972222F;
			this.txtCoveredSSN12.Width = 1F;
			// 
			// txtCoveredDOB12
			// 
			this.txtCoveredDOB12.Height = 0.1770833F;
			this.txtCoveredDOB12.Left = 3.791667F;
			this.txtCoveredDOB12.Name = "txtCoveredDOB12";
			this.txtCoveredDOB12.Text = null;
			this.txtCoveredDOB12.Top = 6.972222F;
			this.txtCoveredDOB12.Width = 0.9166667F;
			// 
			// txtCoveredAll12_7
			// 
			this.txtCoveredAll12_7.Height = 0.1770833F;
			this.txtCoveredAll12_7.Left = 4.916667F;
			this.txtCoveredAll12_7.Name = "txtCoveredAll12_7";
			this.txtCoveredAll12_7.Text = null;
			this.txtCoveredAll12_7.Top = 4.472222F;
			this.txtCoveredAll12_7.Width = 0.25F;
			// 
			// txtCoveredMonth1_7
			// 
			this.txtCoveredMonth1_7.Height = 0.1770833F;
			this.txtCoveredMonth1_7.Left = 5.354167F;
			this.txtCoveredMonth1_7.Name = "txtCoveredMonth1_7";
			this.txtCoveredMonth1_7.Text = null;
			this.txtCoveredMonth1_7.Top = 4.472222F;
			this.txtCoveredMonth1_7.Width = 0.25F;
			// 
			// txtCoveredMonth2_7
			// 
			this.txtCoveredMonth2_7.Height = 0.1770833F;
			this.txtCoveredMonth2_7.Left = 5.75F;
			this.txtCoveredMonth2_7.Name = "txtCoveredMonth2_7";
			this.txtCoveredMonth2_7.Text = null;
			this.txtCoveredMonth2_7.Top = 4.472222F;
			this.txtCoveredMonth2_7.Width = 0.25F;
			// 
			// txtCoveredMonth3_7
			// 
			this.txtCoveredMonth3_7.Height = 0.1770833F;
			this.txtCoveredMonth3_7.Left = 6.145833F;
			this.txtCoveredMonth3_7.Name = "txtCoveredMonth3_7";
			this.txtCoveredMonth3_7.Text = null;
			this.txtCoveredMonth3_7.Top = 4.472222F;
			this.txtCoveredMonth3_7.Width = 0.25F;
			// 
			// txtCoveredMonth4_7
			// 
			this.txtCoveredMonth4_7.Height = 0.1770833F;
			this.txtCoveredMonth4_7.Left = 6.5625F;
			this.txtCoveredMonth4_7.Name = "txtCoveredMonth4_7";
			this.txtCoveredMonth4_7.Text = null;
			this.txtCoveredMonth4_7.Top = 4.472222F;
			this.txtCoveredMonth4_7.Width = 0.25F;
			// 
			// txtCoveredMonth5_7
			// 
			this.txtCoveredMonth5_7.Height = 0.1770833F;
			this.txtCoveredMonth5_7.Left = 6.951389F;
			this.txtCoveredMonth5_7.Name = "txtCoveredMonth5_7";
			this.txtCoveredMonth5_7.Text = null;
			this.txtCoveredMonth5_7.Top = 4.472222F;
			this.txtCoveredMonth5_7.Width = 0.25F;
			// 
			// txtCoveredMonth6_7
			// 
			this.txtCoveredMonth6_7.Height = 0.1770833F;
			this.txtCoveredMonth6_7.Left = 7.347222F;
			this.txtCoveredMonth6_7.Name = "txtCoveredMonth6_7";
			this.txtCoveredMonth6_7.Text = null;
			this.txtCoveredMonth6_7.Top = 4.472222F;
			this.txtCoveredMonth6_7.Width = 0.25F;
			// 
			// txtCoveredMonth7_7
			// 
			this.txtCoveredMonth7_7.Height = 0.1770833F;
			this.txtCoveredMonth7_7.Left = 7.763889F;
			this.txtCoveredMonth7_7.Name = "txtCoveredMonth7_7";
			this.txtCoveredMonth7_7.Text = null;
			this.txtCoveredMonth7_7.Top = 4.472222F;
			this.txtCoveredMonth7_7.Width = 0.25F;
			// 
			// txtCoveredMonth8_7
			// 
			this.txtCoveredMonth8_7.Height = 0.1770833F;
			this.txtCoveredMonth8_7.Left = 8.138889F;
			this.txtCoveredMonth8_7.Name = "txtCoveredMonth8_7";
			this.txtCoveredMonth8_7.Text = null;
			this.txtCoveredMonth8_7.Top = 4.472222F;
			this.txtCoveredMonth8_7.Width = 0.25F;
			// 
			// txtCoveredMonth9_7
			// 
			this.txtCoveredMonth9_7.Height = 0.1770833F;
			this.txtCoveredMonth9_7.Left = 8.548611F;
			this.txtCoveredMonth9_7.Name = "txtCoveredMonth9_7";
			this.txtCoveredMonth9_7.Text = null;
			this.txtCoveredMonth9_7.Top = 4.472222F;
			this.txtCoveredMonth9_7.Width = 0.25F;
			// 
			// txtCoveredMonth10_7
			// 
			this.txtCoveredMonth10_7.Height = 0.1770833F;
			this.txtCoveredMonth10_7.Left = 8.951389F;
			this.txtCoveredMonth10_7.Name = "txtCoveredMonth10_7";
			this.txtCoveredMonth10_7.Text = null;
			this.txtCoveredMonth10_7.Top = 4.472222F;
			this.txtCoveredMonth10_7.Width = 0.25F;
			// 
			// txtCoveredMonth11_7
			// 
			this.txtCoveredMonth11_7.Height = 0.1770833F;
			this.txtCoveredMonth11_7.Left = 9.354167F;
			this.txtCoveredMonth11_7.Name = "txtCoveredMonth11_7";
			this.txtCoveredMonth11_7.Text = null;
			this.txtCoveredMonth11_7.Top = 4.472222F;
			this.txtCoveredMonth11_7.Width = 0.25F;
			// 
			// txtCoveredMonth12_7
			// 
			this.txtCoveredMonth12_7.Height = 0.1770833F;
			this.txtCoveredMonth12_7.Left = 9.756945F;
			this.txtCoveredMonth12_7.Name = "txtCoveredMonth12_7";
			this.txtCoveredMonth12_7.Text = null;
			this.txtCoveredMonth12_7.Top = 4.472222F;
			this.txtCoveredMonth12_7.Width = 0.1944444F;
			// 
			// txtCoveredAll12_8
			// 
			this.txtCoveredAll12_8.Height = 0.1770833F;
			this.txtCoveredAll12_8.Left = 4.916667F;
			this.txtCoveredAll12_8.Name = "txtCoveredAll12_8";
			this.txtCoveredAll12_8.Text = null;
			this.txtCoveredAll12_8.Top = 4.972222F;
			this.txtCoveredAll12_8.Width = 0.25F;
			// 
			// txtCoveredMonth1_8
			// 
			this.txtCoveredMonth1_8.Height = 0.1770833F;
			this.txtCoveredMonth1_8.Left = 5.354167F;
			this.txtCoveredMonth1_8.Name = "txtCoveredMonth1_8";
			this.txtCoveredMonth1_8.Text = null;
			this.txtCoveredMonth1_8.Top = 4.972222F;
			this.txtCoveredMonth1_8.Width = 0.25F;
			// 
			// txtCoveredMonth2_8
			// 
			this.txtCoveredMonth2_8.Height = 0.1770833F;
			this.txtCoveredMonth2_8.Left = 5.75F;
			this.txtCoveredMonth2_8.Name = "txtCoveredMonth2_8";
			this.txtCoveredMonth2_8.Text = null;
			this.txtCoveredMonth2_8.Top = 4.972222F;
			this.txtCoveredMonth2_8.Width = 0.25F;
			// 
			// txtCoveredMonth3_8
			// 
			this.txtCoveredMonth3_8.Height = 0.1770833F;
			this.txtCoveredMonth3_8.Left = 6.145833F;
			this.txtCoveredMonth3_8.Name = "txtCoveredMonth3_8";
			this.txtCoveredMonth3_8.Text = null;
			this.txtCoveredMonth3_8.Top = 4.972222F;
			this.txtCoveredMonth3_8.Width = 0.25F;
			// 
			// txtCoveredMonth4_8
			// 
			this.txtCoveredMonth4_8.Height = 0.1770833F;
			this.txtCoveredMonth4_8.Left = 6.5625F;
			this.txtCoveredMonth4_8.Name = "txtCoveredMonth4_8";
			this.txtCoveredMonth4_8.Text = null;
			this.txtCoveredMonth4_8.Top = 4.972222F;
			this.txtCoveredMonth4_8.Width = 0.25F;
			// 
			// txtCoveredMonth5_8
			// 
			this.txtCoveredMonth5_8.Height = 0.1770833F;
			this.txtCoveredMonth5_8.Left = 6.951389F;
			this.txtCoveredMonth5_8.Name = "txtCoveredMonth5_8";
			this.txtCoveredMonth5_8.Text = null;
			this.txtCoveredMonth5_8.Top = 4.972222F;
			this.txtCoveredMonth5_8.Width = 0.25F;
			// 
			// txtCoveredMonth6_8
			// 
			this.txtCoveredMonth6_8.Height = 0.1770833F;
			this.txtCoveredMonth6_8.Left = 7.347222F;
			this.txtCoveredMonth6_8.Name = "txtCoveredMonth6_8";
			this.txtCoveredMonth6_8.Text = null;
			this.txtCoveredMonth6_8.Top = 4.972222F;
			this.txtCoveredMonth6_8.Width = 0.25F;
			// 
			// txtCoveredMonth7_8
			// 
			this.txtCoveredMonth7_8.Height = 0.1770833F;
			this.txtCoveredMonth7_8.Left = 7.763889F;
			this.txtCoveredMonth7_8.Name = "txtCoveredMonth7_8";
			this.txtCoveredMonth7_8.Text = null;
			this.txtCoveredMonth7_8.Top = 4.972222F;
			this.txtCoveredMonth7_8.Width = 0.25F;
			// 
			// txtCoveredMonth8_8
			// 
			this.txtCoveredMonth8_8.Height = 0.1770833F;
			this.txtCoveredMonth8_8.Left = 8.138889F;
			this.txtCoveredMonth8_8.Name = "txtCoveredMonth8_8";
			this.txtCoveredMonth8_8.Text = null;
			this.txtCoveredMonth8_8.Top = 4.972222F;
			this.txtCoveredMonth8_8.Width = 0.25F;
			// 
			// txtCoveredMonth9_8
			// 
			this.txtCoveredMonth9_8.Height = 0.1770833F;
			this.txtCoveredMonth9_8.Left = 8.548611F;
			this.txtCoveredMonth9_8.Name = "txtCoveredMonth9_8";
			this.txtCoveredMonth9_8.Text = null;
			this.txtCoveredMonth9_8.Top = 4.972222F;
			this.txtCoveredMonth9_8.Width = 0.25F;
			// 
			// txtCoveredMonth10_8
			// 
			this.txtCoveredMonth10_8.Height = 0.1770833F;
			this.txtCoveredMonth10_8.Left = 8.951389F;
			this.txtCoveredMonth10_8.Name = "txtCoveredMonth10_8";
			this.txtCoveredMonth10_8.Text = null;
			this.txtCoveredMonth10_8.Top = 4.972222F;
			this.txtCoveredMonth10_8.Width = 0.25F;
			// 
			// txtCoveredMonth11_8
			// 
			this.txtCoveredMonth11_8.Height = 0.1770833F;
			this.txtCoveredMonth11_8.Left = 9.354167F;
			this.txtCoveredMonth11_8.Name = "txtCoveredMonth11_8";
			this.txtCoveredMonth11_8.Text = null;
			this.txtCoveredMonth11_8.Top = 4.972222F;
			this.txtCoveredMonth11_8.Width = 0.25F;
			// 
			// txtCoveredMonth12_8
			// 
			this.txtCoveredMonth12_8.Height = 0.1770833F;
			this.txtCoveredMonth12_8.Left = 9.756945F;
			this.txtCoveredMonth12_8.Name = "txtCoveredMonth12_8";
			this.txtCoveredMonth12_8.Text = null;
			this.txtCoveredMonth12_8.Top = 4.972222F;
			this.txtCoveredMonth12_8.Width = 0.1944444F;
			// 
			// txtCoveredAll12_9
			// 
			this.txtCoveredAll12_9.Height = 0.1770833F;
			this.txtCoveredAll12_9.Left = 4.916667F;
			this.txtCoveredAll12_9.Name = "txtCoveredAll12_9";
			this.txtCoveredAll12_9.Text = null;
			this.txtCoveredAll12_9.Top = 5.472222F;
			this.txtCoveredAll12_9.Width = 0.25F;
			// 
			// txtCoveredMonth1_9
			// 
			this.txtCoveredMonth1_9.Height = 0.1770833F;
			this.txtCoveredMonth1_9.Left = 5.354167F;
			this.txtCoveredMonth1_9.Name = "txtCoveredMonth1_9";
			this.txtCoveredMonth1_9.Text = null;
			this.txtCoveredMonth1_9.Top = 5.472222F;
			this.txtCoveredMonth1_9.Width = 0.25F;
			// 
			// txtCoveredMonth2_9
			// 
			this.txtCoveredMonth2_9.Height = 0.1770833F;
			this.txtCoveredMonth2_9.Left = 5.75F;
			this.txtCoveredMonth2_9.Name = "txtCoveredMonth2_9";
			this.txtCoveredMonth2_9.Text = null;
			this.txtCoveredMonth2_9.Top = 5.472222F;
			this.txtCoveredMonth2_9.Width = 0.25F;
			// 
			// txtCoveredMonth3_9
			// 
			this.txtCoveredMonth3_9.Height = 0.1770833F;
			this.txtCoveredMonth3_9.Left = 6.145833F;
			this.txtCoveredMonth3_9.Name = "txtCoveredMonth3_9";
			this.txtCoveredMonth3_9.Text = null;
			this.txtCoveredMonth3_9.Top = 5.472222F;
			this.txtCoveredMonth3_9.Width = 0.25F;
			// 
			// txtCoveredMonth4_9
			// 
			this.txtCoveredMonth4_9.Height = 0.1770833F;
			this.txtCoveredMonth4_9.Left = 6.5625F;
			this.txtCoveredMonth4_9.Name = "txtCoveredMonth4_9";
			this.txtCoveredMonth4_9.Text = null;
			this.txtCoveredMonth4_9.Top = 5.472222F;
			this.txtCoveredMonth4_9.Width = 0.25F;
			// 
			// txtCoveredMonth5_9
			// 
			this.txtCoveredMonth5_9.Height = 0.1770833F;
			this.txtCoveredMonth5_9.Left = 6.951389F;
			this.txtCoveredMonth5_9.Name = "txtCoveredMonth5_9";
			this.txtCoveredMonth5_9.Text = null;
			this.txtCoveredMonth5_9.Top = 5.472222F;
			this.txtCoveredMonth5_9.Width = 0.25F;
			// 
			// txtCoveredMonth6_9
			// 
			this.txtCoveredMonth6_9.Height = 0.1770833F;
			this.txtCoveredMonth6_9.Left = 7.347222F;
			this.txtCoveredMonth6_9.Name = "txtCoveredMonth6_9";
			this.txtCoveredMonth6_9.Text = null;
			this.txtCoveredMonth6_9.Top = 5.472222F;
			this.txtCoveredMonth6_9.Width = 0.25F;
			// 
			// txtCoveredMonth7_9
			// 
			this.txtCoveredMonth7_9.Height = 0.1770833F;
			this.txtCoveredMonth7_9.Left = 7.763889F;
			this.txtCoveredMonth7_9.Name = "txtCoveredMonth7_9";
			this.txtCoveredMonth7_9.Text = null;
			this.txtCoveredMonth7_9.Top = 5.472222F;
			this.txtCoveredMonth7_9.Width = 0.25F;
			// 
			// txtCoveredMonth8_9
			// 
			this.txtCoveredMonth8_9.Height = 0.1770833F;
			this.txtCoveredMonth8_9.Left = 8.138889F;
			this.txtCoveredMonth8_9.Name = "txtCoveredMonth8_9";
			this.txtCoveredMonth8_9.Text = null;
			this.txtCoveredMonth8_9.Top = 5.472222F;
			this.txtCoveredMonth8_9.Width = 0.25F;
			// 
			// txtCoveredMonth9_9
			// 
			this.txtCoveredMonth9_9.Height = 0.1770833F;
			this.txtCoveredMonth9_9.Left = 8.548611F;
			this.txtCoveredMonth9_9.Name = "txtCoveredMonth9_9";
			this.txtCoveredMonth9_9.Text = null;
			this.txtCoveredMonth9_9.Top = 5.472222F;
			this.txtCoveredMonth9_9.Width = 0.25F;
			// 
			// txtCoveredMonth10_9
			// 
			this.txtCoveredMonth10_9.Height = 0.1770833F;
			this.txtCoveredMonth10_9.Left = 8.951389F;
			this.txtCoveredMonth10_9.Name = "txtCoveredMonth10_9";
			this.txtCoveredMonth10_9.Text = null;
			this.txtCoveredMonth10_9.Top = 5.472222F;
			this.txtCoveredMonth10_9.Width = 0.25F;
			// 
			// txtCoveredMonth11_9
			// 
			this.txtCoveredMonth11_9.Height = 0.1770833F;
			this.txtCoveredMonth11_9.Left = 9.354167F;
			this.txtCoveredMonth11_9.Name = "txtCoveredMonth11_9";
			this.txtCoveredMonth11_9.Text = null;
			this.txtCoveredMonth11_9.Top = 5.472222F;
			this.txtCoveredMonth11_9.Width = 0.25F;
			// 
			// txtCoveredMonth12_9
			// 
			this.txtCoveredMonth12_9.Height = 0.1770833F;
			this.txtCoveredMonth12_9.Left = 9.756945F;
			this.txtCoveredMonth12_9.Name = "txtCoveredMonth12_9";
			this.txtCoveredMonth12_9.Text = null;
			this.txtCoveredMonth12_9.Top = 5.472222F;
			this.txtCoveredMonth12_9.Width = 0.1944444F;
			// 
			// txtCoveredAll12_10
			// 
			this.txtCoveredAll12_10.Height = 0.1770833F;
			this.txtCoveredAll12_10.Left = 4.916667F;
			this.txtCoveredAll12_10.Name = "txtCoveredAll12_10";
			this.txtCoveredAll12_10.Text = null;
			this.txtCoveredAll12_10.Top = 5.972222F;
			this.txtCoveredAll12_10.Width = 0.25F;
			// 
			// txtCoveredMonth1_10
			// 
			this.txtCoveredMonth1_10.Height = 0.1770833F;
			this.txtCoveredMonth1_10.Left = 5.354167F;
			this.txtCoveredMonth1_10.Name = "txtCoveredMonth1_10";
			this.txtCoveredMonth1_10.Text = null;
			this.txtCoveredMonth1_10.Top = 5.972222F;
			this.txtCoveredMonth1_10.Width = 0.25F;
			// 
			// txtCoveredMonth2_10
			// 
			this.txtCoveredMonth2_10.Height = 0.1770833F;
			this.txtCoveredMonth2_10.Left = 5.75F;
			this.txtCoveredMonth2_10.Name = "txtCoveredMonth2_10";
			this.txtCoveredMonth2_10.Text = null;
			this.txtCoveredMonth2_10.Top = 5.972222F;
			this.txtCoveredMonth2_10.Width = 0.25F;
			// 
			// txtCoveredMonth3_10
			// 
			this.txtCoveredMonth3_10.Height = 0.1770833F;
			this.txtCoveredMonth3_10.Left = 6.145833F;
			this.txtCoveredMonth3_10.Name = "txtCoveredMonth3_10";
			this.txtCoveredMonth3_10.Text = null;
			this.txtCoveredMonth3_10.Top = 5.972222F;
			this.txtCoveredMonth3_10.Width = 0.25F;
			// 
			// txtCoveredMonth4_10
			// 
			this.txtCoveredMonth4_10.Height = 0.1770833F;
			this.txtCoveredMonth4_10.Left = 6.5625F;
			this.txtCoveredMonth4_10.Name = "txtCoveredMonth4_10";
			this.txtCoveredMonth4_10.Text = null;
			this.txtCoveredMonth4_10.Top = 5.972222F;
			this.txtCoveredMonth4_10.Width = 0.25F;
			// 
			// txtCoveredMonth5_10
			// 
			this.txtCoveredMonth5_10.Height = 0.1770833F;
			this.txtCoveredMonth5_10.Left = 6.951389F;
			this.txtCoveredMonth5_10.Name = "txtCoveredMonth5_10";
			this.txtCoveredMonth5_10.Text = null;
			this.txtCoveredMonth5_10.Top = 5.972222F;
			this.txtCoveredMonth5_10.Width = 0.25F;
			// 
			// txtCoveredMonth6_10
			// 
			this.txtCoveredMonth6_10.Height = 0.1770833F;
			this.txtCoveredMonth6_10.Left = 7.347222F;
			this.txtCoveredMonth6_10.Name = "txtCoveredMonth6_10";
			this.txtCoveredMonth6_10.Text = null;
			this.txtCoveredMonth6_10.Top = 5.972222F;
			this.txtCoveredMonth6_10.Width = 0.25F;
			// 
			// txtCoveredMonth7_10
			// 
			this.txtCoveredMonth7_10.Height = 0.1770833F;
			this.txtCoveredMonth7_10.Left = 7.763889F;
			this.txtCoveredMonth7_10.Name = "txtCoveredMonth7_10";
			this.txtCoveredMonth7_10.Text = null;
			this.txtCoveredMonth7_10.Top = 5.972222F;
			this.txtCoveredMonth7_10.Width = 0.25F;
			// 
			// txtCoveredMonth8_10
			// 
			this.txtCoveredMonth8_10.Height = 0.1770833F;
			this.txtCoveredMonth8_10.Left = 8.138889F;
			this.txtCoveredMonth8_10.Name = "txtCoveredMonth8_10";
			this.txtCoveredMonth8_10.Text = null;
			this.txtCoveredMonth8_10.Top = 5.972222F;
			this.txtCoveredMonth8_10.Width = 0.25F;
			// 
			// txtCoveredMonth9_10
			// 
			this.txtCoveredMonth9_10.Height = 0.1770833F;
			this.txtCoveredMonth9_10.Left = 8.548611F;
			this.txtCoveredMonth9_10.Name = "txtCoveredMonth9_10";
			this.txtCoveredMonth9_10.Text = null;
			this.txtCoveredMonth9_10.Top = 5.972222F;
			this.txtCoveredMonth9_10.Width = 0.25F;
			// 
			// txtCoveredMonth10_10
			// 
			this.txtCoveredMonth10_10.Height = 0.1770833F;
			this.txtCoveredMonth10_10.Left = 8.951389F;
			this.txtCoveredMonth10_10.Name = "txtCoveredMonth10_10";
			this.txtCoveredMonth10_10.Text = null;
			this.txtCoveredMonth10_10.Top = 5.972222F;
			this.txtCoveredMonth10_10.Width = 0.25F;
			// 
			// txtCoveredMonth11_10
			// 
			this.txtCoveredMonth11_10.Height = 0.1770833F;
			this.txtCoveredMonth11_10.Left = 9.354167F;
			this.txtCoveredMonth11_10.Name = "txtCoveredMonth11_10";
			this.txtCoveredMonth11_10.Text = null;
			this.txtCoveredMonth11_10.Top = 5.972222F;
			this.txtCoveredMonth11_10.Width = 0.25F;
			// 
			// txtCoveredMonth12_10
			// 
			this.txtCoveredMonth12_10.Height = 0.1770833F;
			this.txtCoveredMonth12_10.Left = 9.756945F;
			this.txtCoveredMonth12_10.Name = "txtCoveredMonth12_10";
			this.txtCoveredMonth12_10.Text = null;
			this.txtCoveredMonth12_10.Top = 5.972222F;
			this.txtCoveredMonth12_10.Width = 0.1944444F;
			// 
			// txtCoveredAll12_11
			// 
			this.txtCoveredAll12_11.Height = 0.1770833F;
			this.txtCoveredAll12_11.Left = 4.916667F;
			this.txtCoveredAll12_11.Name = "txtCoveredAll12_11";
			this.txtCoveredAll12_11.Text = null;
			this.txtCoveredAll12_11.Top = 6.472222F;
			this.txtCoveredAll12_11.Width = 0.25F;
			// 
			// txtCoveredMonth1_11
			// 
			this.txtCoveredMonth1_11.Height = 0.1770833F;
			this.txtCoveredMonth1_11.Left = 5.354167F;
			this.txtCoveredMonth1_11.Name = "txtCoveredMonth1_11";
			this.txtCoveredMonth1_11.Text = null;
			this.txtCoveredMonth1_11.Top = 6.472222F;
			this.txtCoveredMonth1_11.Width = 0.25F;
			// 
			// txtCoveredMonth2_11
			// 
			this.txtCoveredMonth2_11.Height = 0.1770833F;
			this.txtCoveredMonth2_11.Left = 5.75F;
			this.txtCoveredMonth2_11.Name = "txtCoveredMonth2_11";
			this.txtCoveredMonth2_11.Text = null;
			this.txtCoveredMonth2_11.Top = 6.472222F;
			this.txtCoveredMonth2_11.Width = 0.25F;
			// 
			// txtCoveredMonth3_11
			// 
			this.txtCoveredMonth3_11.Height = 0.1770833F;
			this.txtCoveredMonth3_11.Left = 6.145833F;
			this.txtCoveredMonth3_11.Name = "txtCoveredMonth3_11";
			this.txtCoveredMonth3_11.Text = null;
			this.txtCoveredMonth3_11.Top = 6.472222F;
			this.txtCoveredMonth3_11.Width = 0.25F;
			// 
			// txtCoveredMonth4_11
			// 
			this.txtCoveredMonth4_11.Height = 0.1770833F;
			this.txtCoveredMonth4_11.Left = 6.5625F;
			this.txtCoveredMonth4_11.Name = "txtCoveredMonth4_11";
			this.txtCoveredMonth4_11.Text = null;
			this.txtCoveredMonth4_11.Top = 6.472222F;
			this.txtCoveredMonth4_11.Width = 0.25F;
			// 
			// txtCoveredMonth5_11
			// 
			this.txtCoveredMonth5_11.Height = 0.1770833F;
			this.txtCoveredMonth5_11.Left = 6.951389F;
			this.txtCoveredMonth5_11.Name = "txtCoveredMonth5_11";
			this.txtCoveredMonth5_11.Text = null;
			this.txtCoveredMonth5_11.Top = 6.472222F;
			this.txtCoveredMonth5_11.Width = 0.25F;
			// 
			// txtCoveredMonth6_11
			// 
			this.txtCoveredMonth6_11.Height = 0.1770833F;
			this.txtCoveredMonth6_11.Left = 7.347222F;
			this.txtCoveredMonth6_11.Name = "txtCoveredMonth6_11";
			this.txtCoveredMonth6_11.Text = null;
			this.txtCoveredMonth6_11.Top = 6.472222F;
			this.txtCoveredMonth6_11.Width = 0.25F;
			// 
			// txtCoveredMonth7_11
			// 
			this.txtCoveredMonth7_11.Height = 0.1770833F;
			this.txtCoveredMonth7_11.Left = 7.763889F;
			this.txtCoveredMonth7_11.Name = "txtCoveredMonth7_11";
			this.txtCoveredMonth7_11.Text = null;
			this.txtCoveredMonth7_11.Top = 6.472222F;
			this.txtCoveredMonth7_11.Width = 0.25F;
			// 
			// txtCoveredMonth8_11
			// 
			this.txtCoveredMonth8_11.Height = 0.1770833F;
			this.txtCoveredMonth8_11.Left = 8.138889F;
			this.txtCoveredMonth8_11.Name = "txtCoveredMonth8_11";
			this.txtCoveredMonth8_11.Text = null;
			this.txtCoveredMonth8_11.Top = 6.472222F;
			this.txtCoveredMonth8_11.Width = 0.25F;
			// 
			// txtCoveredMonth9_11
			// 
			this.txtCoveredMonth9_11.Height = 0.1770833F;
			this.txtCoveredMonth9_11.Left = 8.548611F;
			this.txtCoveredMonth9_11.Name = "txtCoveredMonth9_11";
			this.txtCoveredMonth9_11.Text = null;
			this.txtCoveredMonth9_11.Top = 6.472222F;
			this.txtCoveredMonth9_11.Width = 0.25F;
			// 
			// txtCoveredMonth10_11
			// 
			this.txtCoveredMonth10_11.Height = 0.1770833F;
			this.txtCoveredMonth10_11.Left = 8.951389F;
			this.txtCoveredMonth10_11.Name = "txtCoveredMonth10_11";
			this.txtCoveredMonth10_11.Text = null;
			this.txtCoveredMonth10_11.Top = 6.472222F;
			this.txtCoveredMonth10_11.Width = 0.25F;
			// 
			// txtCoveredMonth11_11
			// 
			this.txtCoveredMonth11_11.Height = 0.1770833F;
			this.txtCoveredMonth11_11.Left = 9.354167F;
			this.txtCoveredMonth11_11.Name = "txtCoveredMonth11_11";
			this.txtCoveredMonth11_11.Text = null;
			this.txtCoveredMonth11_11.Top = 6.472222F;
			this.txtCoveredMonth11_11.Width = 0.25F;
			// 
			// txtCoveredMonth12_11
			// 
			this.txtCoveredMonth12_11.Height = 0.1770833F;
			this.txtCoveredMonth12_11.Left = 9.756945F;
			this.txtCoveredMonth12_11.Name = "txtCoveredMonth12_11";
			this.txtCoveredMonth12_11.Text = null;
			this.txtCoveredMonth12_11.Top = 6.472222F;
			this.txtCoveredMonth12_11.Width = 0.1944444F;
			// 
			// txtCoveredAll12_12
			// 
			this.txtCoveredAll12_12.Height = 0.1770833F;
			this.txtCoveredAll12_12.Left = 4.916667F;
			this.txtCoveredAll12_12.Name = "txtCoveredAll12_12";
			this.txtCoveredAll12_12.Text = null;
			this.txtCoveredAll12_12.Top = 6.972222F;
			this.txtCoveredAll12_12.Width = 0.25F;
			// 
			// txtCoveredMonth1_12
			// 
			this.txtCoveredMonth1_12.Height = 0.1770833F;
			this.txtCoveredMonth1_12.Left = 5.354167F;
			this.txtCoveredMonth1_12.Name = "txtCoveredMonth1_12";
			this.txtCoveredMonth1_12.Text = null;
			this.txtCoveredMonth1_12.Top = 6.972222F;
			this.txtCoveredMonth1_12.Width = 0.25F;
			// 
			// txtCoveredMonth2_12
			// 
			this.txtCoveredMonth2_12.Height = 0.1770833F;
			this.txtCoveredMonth2_12.Left = 5.75F;
			this.txtCoveredMonth2_12.Name = "txtCoveredMonth2_12";
			this.txtCoveredMonth2_12.Text = null;
			this.txtCoveredMonth2_12.Top = 6.972222F;
			this.txtCoveredMonth2_12.Width = 0.25F;
			// 
			// txtCoveredMonth3_12
			// 
			this.txtCoveredMonth3_12.Height = 0.1770833F;
			this.txtCoveredMonth3_12.Left = 6.145833F;
			this.txtCoveredMonth3_12.Name = "txtCoveredMonth3_12";
			this.txtCoveredMonth3_12.Text = null;
			this.txtCoveredMonth3_12.Top = 6.972222F;
			this.txtCoveredMonth3_12.Width = 0.25F;
			// 
			// txtCoveredMonth4_12
			// 
			this.txtCoveredMonth4_12.Height = 0.1770833F;
			this.txtCoveredMonth4_12.Left = 6.5625F;
			this.txtCoveredMonth4_12.Name = "txtCoveredMonth4_12";
			this.txtCoveredMonth4_12.Text = null;
			this.txtCoveredMonth4_12.Top = 6.972222F;
			this.txtCoveredMonth4_12.Width = 0.25F;
			// 
			// txtCoveredMonth5_12
			// 
			this.txtCoveredMonth5_12.Height = 0.1770833F;
			this.txtCoveredMonth5_12.Left = 6.951389F;
			this.txtCoveredMonth5_12.Name = "txtCoveredMonth5_12";
			this.txtCoveredMonth5_12.Text = null;
			this.txtCoveredMonth5_12.Top = 6.972222F;
			this.txtCoveredMonth5_12.Width = 0.25F;
			// 
			// txtCoveredMonth6_12
			// 
			this.txtCoveredMonth6_12.Height = 0.1770833F;
			this.txtCoveredMonth6_12.Left = 7.347222F;
			this.txtCoveredMonth6_12.Name = "txtCoveredMonth6_12";
			this.txtCoveredMonth6_12.Text = null;
			this.txtCoveredMonth6_12.Top = 6.972222F;
			this.txtCoveredMonth6_12.Width = 0.25F;
			// 
			// txtCoveredMonth7_12
			// 
			this.txtCoveredMonth7_12.Height = 0.1770833F;
			this.txtCoveredMonth7_12.Left = 7.763889F;
			this.txtCoveredMonth7_12.Name = "txtCoveredMonth7_12";
			this.txtCoveredMonth7_12.Text = null;
			this.txtCoveredMonth7_12.Top = 6.972222F;
			this.txtCoveredMonth7_12.Width = 0.25F;
			// 
			// txtCoveredMonth8_12
			// 
			this.txtCoveredMonth8_12.Height = 0.1770833F;
			this.txtCoveredMonth8_12.Left = 8.138889F;
			this.txtCoveredMonth8_12.Name = "txtCoveredMonth8_12";
			this.txtCoveredMonth8_12.Text = null;
			this.txtCoveredMonth8_12.Top = 6.972222F;
			this.txtCoveredMonth8_12.Width = 0.25F;
			// 
			// txtCoveredMonth9_12
			// 
			this.txtCoveredMonth9_12.Height = 0.1770833F;
			this.txtCoveredMonth9_12.Left = 8.548611F;
			this.txtCoveredMonth9_12.Name = "txtCoveredMonth9_12";
			this.txtCoveredMonth9_12.Text = null;
			this.txtCoveredMonth9_12.Top = 6.972222F;
			this.txtCoveredMonth9_12.Width = 0.25F;
			// 
			// txtCoveredMonth10_12
			// 
			this.txtCoveredMonth10_12.Height = 0.1770833F;
			this.txtCoveredMonth10_12.Left = 8.951389F;
			this.txtCoveredMonth10_12.Name = "txtCoveredMonth10_12";
			this.txtCoveredMonth10_12.Text = null;
			this.txtCoveredMonth10_12.Top = 6.972222F;
			this.txtCoveredMonth10_12.Width = 0.25F;
			// 
			// txtCoveredMonth11_12
			// 
			this.txtCoveredMonth11_12.Height = 0.1770833F;
			this.txtCoveredMonth11_12.Left = 9.354167F;
			this.txtCoveredMonth11_12.Name = "txtCoveredMonth11_12";
			this.txtCoveredMonth11_12.Text = null;
			this.txtCoveredMonth11_12.Top = 6.972222F;
			this.txtCoveredMonth11_12.Width = 0.25F;
			// 
			// txtCoveredMonth12_12
			// 
			this.txtCoveredMonth12_12.Height = 0.1770833F;
			this.txtCoveredMonth12_12.Left = 9.756945F;
			this.txtCoveredMonth12_12.Name = "txtCoveredMonth12_12";
			this.txtCoveredMonth12_12.Text = null;
			this.txtCoveredMonth12_12.Top = 6.972222F;
			this.txtCoveredMonth12_12.Width = 0.1944444F;
			// 
			// rpt1095C2016Page2
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.989583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_12;
	}
}
