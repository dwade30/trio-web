﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1094B2016.
	/// </summary>
	partial class rpt1094B2016
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1094B2016));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtFilerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPostalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContactName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContactTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumberSubmissions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberSubmissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFilerName,
            this.txtEIN,
            this.txtAddress,
            this.txtCity,
            this.txtPostalCode,
            this.txtState,
            this.txtContactName,
            this.txtContactTelephone,
            this.txtNumberSubmissions});
            this.Detail.Height = 7.364583F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtFilerName
            // 
            this.txtFilerName.Height = 0.1666667F;
            this.txtFilerName.Left = 0.083F;
            this.txtFilerName.Name = "txtFilerName";
            this.txtFilerName.Text = null;
            this.txtFilerName.Top = 1.137F;
            this.txtFilerName.Width = 5.166667F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1666667F;
            this.txtEIN.Left = 5.416334F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Text = null;
            this.txtEIN.Top = 1.137F;
            this.txtEIN.Width = 1.833333F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1666667F;
            this.txtAddress.Left = 0.083F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 2.137F;
            this.txtAddress.Width = 4F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1666667F;
            this.txtCity.Left = 4.333F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Text = null;
            this.txtCity.Top = 2.137F;
            this.txtCity.Width = 3.833333F;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Height = 0.1666667F;
            this.txtPostalCode.Left = 4.333F;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Text = null;
            this.txtPostalCode.Top = 2.637F;
            this.txtPostalCode.Width = 3.833333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1666667F;
            this.txtState.Left = 0.083F;
            this.txtState.Name = "txtState";
            this.txtState.Text = null;
            this.txtState.Top = 2.637F;
            this.txtState.Width = 4F;
            // 
            // txtContactName
            // 
            this.txtContactName.Height = 0.1666667F;
            this.txtContactName.Left = 0.083F;
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Text = null;
            this.txtContactName.Top = 1.637F;
            this.txtContactName.Width = 5.166667F;
            // 
            // txtContactTelephone
            // 
            this.txtContactTelephone.Height = 0.1666667F;
            this.txtContactTelephone.Left = 5.416334F;
            this.txtContactTelephone.Name = "txtContactTelephone";
            this.txtContactTelephone.Text = null;
            this.txtContactTelephone.Top = 1.637F;
            this.txtContactTelephone.Width = 1.833333F;
            // 
            // txtNumberSubmissions
            // 
            this.txtNumberSubmissions.Height = 0.1666667F;
            this.txtNumberSubmissions.Left = 6.166334F;
            this.txtNumberSubmissions.Name = "txtNumberSubmissions";
            this.txtNumberSubmissions.Text = null;
            this.txtNumberSubmissions.Top = 2.970333F;
            this.txtNumberSubmissions.Width = 1.5F;
            // 
            // rpt1094B2016
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.979167F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtFilerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberSubmissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFilerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPostalCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumberSubmissions;
	}
}
